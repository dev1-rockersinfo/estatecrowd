<!-- <base href="http://localhost/estate-crowd/"> -->

<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

<!-- Favicon -->
<link rel="icon" href="images/favicon.png" sizes="32x32">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">

<!-- Bootstrap Select -->
<link rel="stylesheet" href="assets/bootstrap-select/css/bootstrap-select.css">

<!-- Simple Line Icons -->
<link rel="stylesheet" href="assets/simple-line-icons/css/simple-line-icons.css">

<!-- Font Awesome Icons -->
<link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">

<!-- Push Menu -->
<link href="assets/push-menu/css/push-menu.css" rel="stylesheet" />

<!-- Carousel -->
<link href="assets/owl-carousel/css/owl.carousel.css" rel="stylesheet" />

<!-- Style CSS -->
<link rel="stylesheet" href="css/style.css">

<!-- Responsive CSS -->
<link rel="stylesheet" href="css/responsive.css">

<link rel="stylesheet" href="css/circle.css">

 <link href="css/tabdrop.css" rel="stylesheet">
 
 <link rel="stylesheet" href="css/redactor.css" />
 <link rel="stylesheet" type="text/css" href="css/jquery.tokenize.css" />
 
  <link rel="stylesheet" href="css/datepicker.css">
  <link rel="stylesheet" href="css/bootstrap-switch.css">

  <!-- Gallery  -->
<link href="js/google-code-prettify/prettify.css" rel="stylesheet" />
<link href="css/demo_ajax_stevejobs.css" rel="stylesheet" />
<link href="css/ilightbox.css" rel="stylesheet" />
<link href="asset/metro-black-skin/skin.css" rel="stylesheet" />

  <link href="https://fonts.googleapis.com/css?family=BenchNine" rel="stylesheet"> 