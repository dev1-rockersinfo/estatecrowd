<nav id="affix-nav" class="navbar navbar-default"  data-spy="affix" data-offset-top="62">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="push" data-target="#navbar" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand ec-navbar-brand" href="index.php"><img src="images/inner-logo.png"></a>
    </div>

    <div class="collapse navbar-collapse" id="navbar">
      <ul class="nav navbar-nav navbar-right ec-navbar-right">
        <li><a href="login.php">Log in</a></li>
        <li><a href="join-now.php">Join Now</a></li>
      </ul>
      <ul class="nav navbar-nav ec-navbar">
        <li><a href="about-us.php" class="hidden-md">About Us</a></li>
        <li><a href="how-it-works.php">How It Works</a></li>
        <li><a href="faqs.php" class="hidden-md">FAQ's</a></li>
        <li><a href="property-search.php">Explore Campaigns</a></li>
        <li><a href="property-search.php">Property Search</a></li>
      </ul>
    </div>
  </div>
</nav>