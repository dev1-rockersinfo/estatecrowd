<footer>
 

  <div class="third-footer">
    <div class="container">
      <div class="footer-copyright">
      © 2016 <strong>CoVESTA Pty Ltd. </strong>  All rights reserved. Contact us.       
      </div>
    </div>
  </div>
</footer>

<!-- jQuery first, then Bootstrap JS. -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap-datepicker.js"></script>  
<script src="js/bootstrap-filestyle.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.js"></script>    
<script src="assets/push-menu/js/push-menu.js"></script>
<script type="text/javascript">
  function cal() {
    $i = $('#invest_no').val();
    if($i && $.isNumeric($i) && $i > 0) {
        $j = (($i) - ($i * 7.5 / 100)) * 20;
        $j = commaSeparateNumber($j);
        $j = '$'+$j;
        $('#cl_result').html($j);
        $("#btnex").html('Re-Calculate');
    } else {
        alert("Please enter the number only.");
    }
  }
  function commaSeparateNumber(val){
    while (/(\d+)(\d{3})/.test(val.toString())){
      val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
    }
    return val;
  }
     $(document).ready(function(){
    $(".dropdown").hover(            
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeIn("slow");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");                
            },
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeOut("slow");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");                
            });
    });

     
</script>