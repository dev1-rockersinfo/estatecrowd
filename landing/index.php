<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Covesta</title>

    <?php require('include/common.php'); ?>
    
  </head>
  <body   >
  <?php $msg=''; 
        if($_GET) {
          $msg=$_GET['mail_sent'];
        }
        ?>
        <?php if($msg!=''){
          ?><div class="covesta-msg">
          <div class="alert alert-success alert-messege"><?php echo $msg;?></div>
          </div>
         <?php  } ?>

    <section  class="operation-sec">
      
        <div class="masthead">

            <div class="ec-home-brand">
               <img src="images/covesta-logo.png">
            </div>
            <div class="ec-banner">
               <img class=" hidden-xs" src="images/banner.png">
<img class="img-responsive visible-xs " src="images/banner-mob.png">
  <div class="slider-text">
              <h2>Invest. Any property. Any where.</h2>
            
            </div>
            </div>
          
          
        
        </div>
       
       <div class="subscribe-area">              
              <div class="container">             
            
             <form class="form-inline cov-form" name="register" action="../home/landing_subscribe" method="post" onsubmit="return isValid()">
                <lable>REGISTER FOR 2017</lable> 
                <div class="form-group">
             <input class="form-control" type="text" placeholder="Full name"  name="name" id="name"></div>
               <div class="form-group">  <input class="form-control" type="email" placeholder="Email" name="email"  id="email"></div>
              <button class="btn btn-subscribe" type="submit">Subscribe</button>
             </form>
              </div>

            </div>
    </section>
   
   
  
    <section>
      <div class="covesta-slider">
        <div class="container">
         <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>
   
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
      <p>
         My living expenses and the rent keep going up. There is no way I can save a deposit to buy something!

        </p>
        <img src="images/bg-family.png" alt="Chania" width="486" height="345">
        
      </div>
        <div class="item ">
      <p>
        On my income, how could i ever afford to buy a property? I mean, what bank is going to lend me money?

        </p>
        <img src="images/bg-senior.png" alt="Chania" width="486" height="345">
        
      </div>
        <div class="item ">
      <p>
        By the time I save a deposit the property price will have already gone up...

        </p>
        <img src="images/bg-middle.png" alt="Chania" width="486" height="345">
        
      </div>
 <div class="item ">
      <p>
        Without cash savings, regular income or other assets, it feels impossible to enter the property market.
        </p>
        <img src="images/bg-youth.png" alt="Chania" width="486" height="345">
        
      </div>

 
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
      
        </div>
      </div>    
    </section>
    <section class="why-covesta">
       <div class="container pr">
          <div class="col-md-8 col-md-offset-2">
          <div class="whyc-box">
    
      <h3>With CoVESTA, investment becomes a reality</h3>
      <p>
        The CoVESTA platform lets you start or join an investor syndicate on any property, in any asset class, anywhere in Australia.</p>
<p>
You choose the property or area you want to invest in and invite your family and friends to join your syndicate and we will take care of the rest.      </p>
       
          </div>

          </div>
           <div class="dollar-flower">
          <img src="images/dollar-flower.png">
          </div>
       </div>
    </section>
    <section class="where-invest">
    <div class="container text-center">
<div class="w-i-box">
<h1>Decide where you want to invest and
CoVESTA does the rest!</h1>
</div>


    </div>
    </section>
<section class="become-covesta">
<div class="container">
<div class="title">
<h1>Become a CoVESTOR</h1>
</div>

<div class="row">
<div class="col-md-3 col-sm-6">
<div class="b-cov">
<div class="b-cov-img"><img class="img-responsive" src="images/discover-ico.png"></div>
<div class="b-cov-txt">
<h4>Discover</h4>
<p>
  Finding your next investment is super easy! Enter an investment amount or enter a suburb or join an open property syndicate.
</p>
</div>
<div class="b-cov-badge">
  <span>1</span>
</div>
</div>
</div>
<div class="col-md-3 col-sm-6">
<div class="b-cov">
<div class="b-cov-img"><img class="img-responsive" src="images/power-ico.png"></div>
<div class="b-cov-txt">
<h4>Follow, Chat, Invest</h4>
<p>
  With CoVESTA you can access the experience and local knowledge of other investors. Follow, chat and invest along side experienced property investors.
</p>
</div>
<div class="b-cov-badge">
  <span>2</span>
</div>
</div>
</div>
<div class="col-md-3 col-sm-6">
<div class="b-cov">
<div class="b-cov-img"><img class="img-responsive" src="images/follow-chart-ico.png"></div>
<div class="b-cov-txt">
<h4>Collaborate</h4>
<p>
 Invite your family/friends and start your own property syndicate and CoVESTA will take care everything. Otherwise, you can join an open syndicate. Its that easy!
</p>
</div>
<div class="b-cov-badge">
  <span>3</span>
</div>
</div>
</div>
<div class="col-md-3 col-sm-6">
<div class="b-cov">
<div class="b-cov-img"><img class="img-responsive" src="images/collaborate-ico.png"></div>
<div class="b-cov-txt">
<h4>Power of Syndication</h4>
<p>
CoVESTA lets you Invest in any property, anywhere in Australia. Register your interest today....
</p>
</div>
<div class="b-cov-badge">
  <span>4</span>
</div>
</div>
</div>
</div>
  </div>
</section>
<section class="inv-calc  ">
<div class="container">
  <div class="title">
 <h1> Investment Calculator</h1>
</div>
 <div class="row">
 <div class="col-md-4 col-md-offset-1 inv-cal-bx-wrp col-sm-6">
 <div class="whyc-box">
 <div class="inv-cal-bx">
<span>
 Find out what kind of property you can invest in
</span>
 <label>ENTER INVESTMENT AMOUNT</label>
 <div class="input-symbol-dollar">
<input type="text" name="" maxlength="7"  placeholder="0" id="invest_no">
</div>
 <button class="btn btn-calculate" onclick="cal(this);" id="btnex">Calculate</button>
 </div>
 </div>
 </div>
 <div class="col-md-4 col-md-offset-1 col-sm-6">
 <div class="inv-amnt">
 <img class="img-responsive" src="images/inv-amnt-img.png">
<span>INVESTMENT PROPERTY VALUE</span>
<span class="value" id="cl_result">???</span>
 </div>
 </div>
 </div>

  </div>

</div>

</section>
<section class="inc-inv">
<div class="container">
<div class="title">
<h1>What's included in your investment:</h1>
</div>
<div class="row tc">
<div class="inv-fea-wrp clearfix">
<ul class="inv-fea clearfix">
<li>
<div class="inc-inv-wrp clearfix">
    <div class="inc-inv-left clearfix">
<img src="images/ico-01.png">
</div>
    <div class="inc-inv-right clearfix">
<span>
The latest Australian property market research
</span>
    </div>
  
</div>
</li>
<li>
<div class="inc-inv-wrp clearfix">
    <div class="inc-inv-left clearfix">
<img src="images/ico-02.png">
</div>
    <div class="inc-inv-right clearfix">
<span>
Syndicate Setup  
</span>
    </div>
  
</div>
</li>
<li>
<div class="inc-inv-wrp clearfix">
    <div class="inc-inv-left clearfix">
<img src="images/ico-03.png">
</div>
    <div class="inc-inv-right clearfix">
<span>
Access detailed property reports 

</span>
    </div>
  
</div>
</li>
<li>
<div class="inc-inv-wrp clearfix">
    <div class="inc-inv-left clearfix">
<img src="images/ico-04.png">
</div>
    <div class="inc-inv-right clearfix">
<span>
Property Acquisition
</span>
    </div>
  
</div>
</li>

<li>
<div class="inc-inv-wrp clearfix">
    <div class="inc-inv-left clearfix">
<img src="images/ico-05.png">
</div>
    <div class="inc-inv-right clearfix">
<span>
Collaborate & invest with other experienced investors


</span>
    </div>
  
</div>
</li>
<li>
<div class="inc-inv-wrp clearfix">
    <div class="inc-inv-left clearfix">
<img src="images/ico-06.png">
</div>
    <div class="inc-inv-right clearfix">
<span>
Property Leasing & Management*
</span>
    </div>
  
</div>
</li>
<li>
<div class="inc-inv-wrp clearfix">
    <div class="inc-inv-left clearfix">
<img src="images/ico-07.png">
</div>
    <div class="inc-inv-right clearfix">
<span>
Personal CoVESTA Buyers Liaison
(Get help along the way)

</span>
    </div>
  
</div>
</li>
<li>
<div class="inc-inv-wrp clearfix">
    <div class="inc-inv-left clearfix">
<img src="images/ico-08.png">
</div>
    <div class="inc-inv-right clearfix">
<span>
Investment Management* 

</span>
    </div>
  
</div>
</li>
</ul>
</div>
</div>
</div>
</section>
<section class="registerdiv tc">
      <div class="container ">
        <div class="col-md-10 col-md-offset-1">
          <h1>Want to start or join a property Syndicate?</h1>
        
          <h4>REGISTER FOR 2017</h4>
      
          <p>To unclock the power of syndication and make property investment easy and affordable for everyone: CoVESTA team is working 24/7 so that you can join the Australian property market as a new breed of investor - the CoVESTOR.</p>
        
          <p class="ex-text">We will let you know when our first syndicate are open for investment.</p>
          
          <form class="form-inline cov-form"  action="../home/landing_subscribe" method="post" onsubmit="return isValid_re()">
            <div class="form-group">
              <input class="form-control" type="text" placeholder="Full Name" name="name" id="name_re" />
            </div>
            <div class="form-group">
              <input class="form-control" type="email" placeholder="Email" name="email" id="email_re" />
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-subs" >Subscribe</button>
            </div>
          </form>
        </div>
      </div>
    </section>
    <?php require('include/footer.php'); ?>

    <script src="assets/bootstrap-select/js/bootstrap-select.js"></script>
    <script src="assets/owl-carousel/owl.carousel.min.js"></script>
<script type="text/javascript">
  function isValid() {
        var name = $('#name').val();
        var email = $('#email').val();
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(name==''){
          alert('Enter Your name first.');
          return false;

        }
        if(email==''){
          alert('Enter Your email first.');
          return false;

        }
      if(!re.test(email)){
        alert('Enter email valid.');
          return false;
      }

    return true;

  }
   function isValid_re() {
        var name = $('#name_re').val();
        var email = $('#email_re').val();
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(name==''){
          alert('Enter Your name first.');
          return false;

        }
        if(email==''){
          alert('Enter Your email first.');
          return false;

        }
      if(!re.test(email)){
        alert('Enter email valid.');
          return false;
      }

    return true;

  }
</script> 
<style type="text/css">
  
.covesta-msg .alert-success {
    color: #3c763d;
    background-color: rgba(223, 240, 215, 0.81);
    position: fixed;
    width: 100%;
    text-align: center;
    padding: 6px;
    border-radius: 0;
    border: 0;
    z-index: 999;
    top: 0;
}
</style>
    
  </body>
</html>
