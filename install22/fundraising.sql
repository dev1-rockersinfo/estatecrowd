-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 12, 2014 at 06:25 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rockers_indigogoclone`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `admin_type` int(10) NOT NULL DEFAULT '2',
  `company` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `active` varchar(255) DEFAULT NULL,
  `date_added` date NOT NULL,
  `login_ip` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `username`, `password`, `admin_type`, `company`, `email`, `active`, `date_added`, `login_ip`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, 'Rockers Technology', 'alpesh.rockersinfo@gmail.com', '1', '2014-09-06', '1.22.81.242'),
(8, 'nilesh', '25d55ad283aa400af464c76d713c07ad', 2, NULL, 'nileshrockersinfo@gmail.com', '1', '2014-09-06', '1.22.81.242'),
(27, 'jayshree', '25d55ad283aa400af464c76d713c07ad', 1, NULL, 'jayshree.rockersinfo@gmail.com', '1', '2014-09-06', '1.22.81.242'),
(32, 'Jayu', '25d55ad283aa400af464c76d713c07ad', 1, NULL, 'jayshree.test.rockersinfo@gmail.com', '0', '2014-09-06', '1.22.81.242');

-- --------------------------------------------------------

--
-- Table structure for table `admin_login`
--

CREATE TABLE IF NOT EXISTS `admin_login` (
  `login_id` int(100) NOT NULL AUTO_INCREMENT,
  `admin_id` int(100) NOT NULL,
  `login_ip` varchar(255) DEFAULT NULL,
  `login_date` datetime NOT NULL,
  PRIMARY KEY (`login_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=43 ;

--
-- Dumping data for table `admin_login`
--

INSERT INTO `admin_login` (`login_id`, `admin_id`, `login_ip`, `login_date`) VALUES
(1, 1, '1.22.83.150', '2014-10-20 00:04:58'),
(2, 1, '27.109.7.130', '2014-10-20 01:52:02'),
(3, 1, '1.22.83.150', '2014-10-20 01:55:36'),
(4, 1, '1.22.81.148', '2014-10-20 03:05:15'),
(5, 1, '1.22.81.148', '2014-10-20 03:18:44'),
(6, 1, '1.22.81.148', '2014-10-20 03:24:01'),
(7, 1, '1.22.83.188', '2014-10-20 05:24:58'),
(8, 1, '1.22.83.188', '2014-10-20 05:27:17'),
(9, 1, '1.22.83.188', '2014-10-20 05:27:18'),
(10, 1, '27.109.7.130', '2014-10-20 06:36:41'),
(11, 1, '27.109.7.130', '2014-10-20 07:00:00'),
(12, 1, '27.109.7.130', '2014-10-21 00:29:53'),
(13, 1, '1.22.83.176', '2014-10-21 03:17:19'),
(14, 1, '1.22.83.176', '2014-10-21 04:27:07'),
(15, 1, '1.22.83.176', '2014-10-21 05:25:11'),
(16, 1, '27.109.7.130', '2014-10-21 05:32:51'),
(17, 1, '27.109.7.130', '2014-10-21 23:17:17'),
(18, 1, '27.109.7.130', '2014-10-22 00:21:18'),
(19, 1, '27.109.7.130', '2014-10-22 00:32:33'),
(20, 1, '27.109.7.130', '2014-10-22 01:33:42'),
(21, 1, '27.109.7.130', '2014-10-22 03:01:32'),
(22, 1, '27.109.7.130', '2014-10-22 03:03:43'),
(23, 1, '27.109.7.130', '2014-10-22 03:14:36'),
(24, 1, '27.109.7.130', '2014-10-27 05:17:53'),
(25, 1, '1.22.80.241', '2014-10-27 07:02:57'),
(26, 1, '1.22.83.132', '2014-10-27 23:48:47'),
(27, 1, '1.22.83.132', '2014-10-28 00:52:11'),
(28, 1, '1.22.80.198', '2014-10-29 00:45:00'),
(29, 1, '27.109.7.130', '2014-10-29 01:36:55'),
(30, 1, '1.22.82.216', '2014-10-30 00:10:36'),
(31, 1, '1.22.82.216', '2014-10-30 00:12:46'),
(32, 1, '1.22.82.216', '2014-10-30 00:14:56'),
(33, 1, '127.0.0.1', '2014-10-31 05:47:28'),
(34, 1, '127.0.0.1', '2014-11-07 23:45:34'),
(35, 1, '127.0.0.1', '2014-11-12 02:55:01'),
(36, 1, '127.0.0.1', '2014-11-12 05:14:42'),
(37, 1, '127.0.0.1', '2014-11-12 05:15:31'),
(38, 1, '127.0.0.1', '2014-11-12 05:46:26'),
(39, 1, '127.0.0.1', '2014-11-14 03:38:12'),
(40, 1, '127.0.0.1', '2014-11-26 01:54:04'),
(41, 1, '127.0.0.1', '2014-12-01 01:37:08'),
(42, 1, '127.0.0.1', '2014-12-11 22:54:57');

-- --------------------------------------------------------

--
-- Table structure for table `affiliate_commission_settings`
--

CREATE TABLE IF NOT EXISTS `affiliate_commission_settings` (
  `commission_settings_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `commission` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `active` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`commission_settings_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `affiliate_commission_settings`
--

INSERT INTO `affiliate_commission_settings` (`commission_settings_id`, `name`, `commission`, `type`, `active`) VALUES
(1, 'Sign Up', '10', '$', '1'),
(2, 'Pledge', '50', '$', '1'),
(3, 'Project Listing', '30', '$', '1');

-- --------------------------------------------------------

--
-- Table structure for table `affiliate_common_settings`
--

CREATE TABLE IF NOT EXISTS `affiliate_common_settings` (
  `common_settings_id` int(11) NOT NULL AUTO_INCREMENT,
  `cookie_expire_time` int(50) NOT NULL DEFAULT '0',
  `commission_holding_period` int(50) NOT NULL DEFAULT '0',
  `pay_commission_pledge` int(50) NOT NULL DEFAULT '0',
  `pay_commission_project_listing` int(50) NOT NULL DEFAULT '0',
  `minimum_withdrawal_threshold` double(10,2) NOT NULL DEFAULT '0.00',
  `transaction_fee` double(10,2) NOT NULL DEFAULT '0.00',
  `fee_type` varchar(255) DEFAULT NULL,
  `affiliate_enable` int(20) NOT NULL DEFAULT '1',
  `affiliate_content` longtext,
  PRIMARY KEY (`common_settings_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `affiliate_common_settings`
--

INSERT INTO `affiliate_common_settings` (`common_settings_id`, `cookie_expire_time`, `commission_holding_period`, `pay_commission_pledge`, `pay_commission_project_listing`, `minimum_withdrawal_threshold`, `transaction_fee`, `fee_type`, `affiliate_enable`, `affiliate_content`) VALUES
(1, 24, 0, 1, 0, 2.00, 2.00, 'percentage', 1, '<p><strong><img alt=KSYDOUKSYDOU src=KSYDOUfile:///home/rocku7/Desktop/Affiliate-Marketing.jpgKSYDOU />&nbsp;<img alt=KSYDOUKSYDOU src=KSYDOUhttp://cdn.techgyd.com/Affiliate-Marketing.jpgKSYDOU style=KSYDOUfloat:right; height:310px; width:466pxKSYDOU /></strong></p>\n\n<p>&nbsp;</p>\n\n<p><strong>Affiliate marketing</strong>&nbsp;is a type of&nbsp;<a class=KSYDOUmw-redirectKSYDOU href=KSYDOUhttp://en.wikipedia.org/wiki/Performance-based_marketingKSYDOU style=KSYDOUtext-decoration: none; color: rgb(11 0 128); background-image: none; background-position: initial initial; background-repeat: initial initial;KSYDOU title=KSYDOUPerformance-based marketingKSYDOU>performance-based &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; marketing</a>&nbsp;in which a business rewards one or &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; more&nbsp;<a href=KSYDOUhttp://en.wikipedia.org/wiki/Affiliate_(commerce)KSYDOU style=KSYDOUtext-decoration: none; color: rgb(11 0 128); background-image: none; background-position: initial initial; background-repeat: initial initial;KSYDOU title=KSYDOUAffiliate (commerce)KSYDOU>affiliates</a>&nbsp;for each visitor or customer brought &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;by the &nbsp; affiliate&#39;s own marketing efforts. The &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;industry has four &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;core &nbsp;players: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;the&nbsp;<a href=KSYDOUhttp://en.wikipedia.org/wiki/MerchantKSYDOU style=KSYDOUtext-decoration: none; color: rgb(11 0 128); background-image: none; background-position: initial initial; background-repeat: initial initial;KSYDOU title=KSYDOUMerchantKSYDOU>merchant</a>&nbsp;(also known as &#39;retailer&#39; or &nbsp; &nbsp; &nbsp; &#39;brand&#39;) &nbsp;the&nbsp;<a href=KSYDOUhttp://en.wikipedia.org/wiki/Affiliate_networkKSYDOU style=KSYDOUtext-decoration: none; color: rgb(11 0 128); background-image: none; background-position: initial initial; background-repeat: initial initial;KSYDOU title=KSYDOUAffiliate networkKSYDOU>network</a>&nbsp;(that contains offers for the affiliate &nbsp; &nbsp; &nbsp;to choose &nbsp;from and also &nbsp; &nbsp; &nbsp;takes care of the payments) the publisher (also known as &#39;the affiliate&#39;) and the customer. The market has grown in complexity resulting in the emergence of a secondary tier of players including affiliate management agencies super-affiliates and specialized third party&nbsp;<a class=KSYDOUmw-redirectKSYDOU href=KSYDOUhttp://en.wikipedia.org/wiki/Vendor_(supply_chain)KSYDOU style=KSYDOUtext-decoration: none; color: rgb(11 0 128); background-image: none; background-position: initial initial; background-repeat: initial initial;KSYDOU title=KSYDOUVendor (supply chain)KSYDOU>vendors</a>.</p>\n\n<p>Affiliate marketing overlaps with other&nbsp;<a class=KSYDOUmw-redirectKSYDOU href=KSYDOUhttp://en.wikipedia.org/wiki/Internet_marketingKSYDOU style=KSYDOUtext-decoration: none; color: rgb(11 0 128); background-image: none; background-position: initial initial; background-repeat: initial initial;KSYDOU title=KSYDOUInternet marketingKSYDOU>Internet marketing</a>&nbsp;methods to some degree because affiliates often use regular&nbsp;<a href=KSYDOUhttp://en.wikipedia.org/wiki/AdvertisingKSYDOU style=KSYDOUtext-decoration: none; color: rgb(11 0 128); background-image: none; background-position: initial initial; background-repeat: initial initial;KSYDOU title=KSYDOUAdvertisingKSYDOU>advertising</a>&nbsp;methods. Those methods include&nbsp;<a href=KSYDOUhttp://en.wikipedia.org/wiki/Organic_searchKSYDOU style=KSYDOUtext-decoration: none; color: rgb(11 0 128); background-image: none; background-position: initial initial; background-repeat: initial initial;KSYDOU title=KSYDOUOrganic searchKSYDOU>organic</a>&nbsp;<a href=KSYDOUhttp://en.wikipedia.org/wiki/Search_engine_optimizationKSYDOU style=KSYDOUtext-decoration: none; color: rgb(11 0 128); background-image: none; background-position: initial initial; background-repeat: initial initial;KSYDOU title=KSYDOUSearch engine optimizationKSYDOU>search engine optimization</a>&nbsp;(SEO) paid&nbsp;<a href=KSYDOUhttp://en.wikipedia.org/wiki/Search_engine_marketingKSYDOU style=KSYDOUtext-decoration: none; color: rgb(11 0 128); background-image: none; background-position: initial initial; background-repeat: initial initial;KSYDOU title=KSYDOUSearch engine marketingKSYDOU>search engine marketing</a>&nbsp;(PPC - Pay Per Click)&nbsp;<a class=KSYDOUmw-redirectKSYDOU href=KSYDOUhttp://en.wikipedia.org/wiki/E-mail_marketingKSYDOU style=KSYDOUtext-decoration: none; color: rgb(11 0 128); background-image: none; background-position: initial initial; background-repeat: initial initial;KSYDOU title=KSYDOUE-mail marketingKSYDOU>e-mail marketing</a>&nbsp;<a href=KSYDOUhttp://en.wikipedia.org/wiki/Content_marketingKSYDOU style=KSYDOUtext-decoration: none; color: rgb(11 0 128); background-image: none; background-position: initial initial; background-repeat: initial initial;KSYDOU title=KSYDOUContent marketingKSYDOU>content marketing</a>and in some sense&nbsp;<a href=KSYDOUhttp://en.wikipedia.org/wiki/Display_advertisingKSYDOU style=KSYDOUtext-decoration: none; color: rgb(11 0 128); background-image: none; background-position: initial initial; background-repeat: initial initial;KSYDOU title=KSYDOUDisplay advertisingKSYDOU>display advertising</a>. On the other hand affiliates sometimes use less orthodox techniques such as publishing reviews of products or services offered by a partner.</p>\n');

-- --------------------------------------------------------

--
-- Table structure for table `affiliate_request`
--

CREATE TABLE IF NOT EXISTS `affiliate_request` (
  `affiliate_request_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(100) NOT NULL,
  `site_category` varchar(255) DEFAULT NULL,
  `site_name` varchar(255) DEFAULT NULL,
  `site_description` varchar(255) DEFAULT NULL,
  `site_url` varchar(255) DEFAULT NULL,
  `why_affiliate` varchar(255) DEFAULT NULL,
  `web_site_marketing` int(50) DEFAULT NULL,
  `search_engine_marketing` int(50) DEFAULT NULL,
  `email_marketing` int(50) DEFAULT NULL,
  `special_promotional_method` varchar(255) DEFAULT NULL,
  `special_promotional_description` varchar(255) DEFAULT NULL,
  `approved` int(50) NOT NULL DEFAULT '0' COMMENT '0=waiting,1=approved,2=rejected',
  `request_date` datetime NOT NULL,
  `request_ip` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`affiliate_request_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `affiliate_user_earn`
--

CREATE TABLE IF NOT EXISTS `affiliate_user_earn` (
  `user_earn_id` int(100) NOT NULL AUTO_INCREMENT,
  `user_id` int(100) DEFAULT NULL,
  `project_id` int(100) DEFAULT NULL,
  `perk_id` int(100) DEFAULT NULL,
  `referral_user_id` int(100) DEFAULT NULL,
  `transaction_id` int(100) DEFAULT NULL,
  `earn_amount` double(10,2) NOT NULL,
  `earn_date` datetime NOT NULL,
  `earn_status` int(20) NOT NULL DEFAULT '0' COMMENT '0=pending,1=completed,2=cancelled',
  `currency_code` varchar(255) NOT NULL,
  `currency_symbol` varchar(255) NOT NULL,
  PRIMARY KEY (`user_earn_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `affiliate_withdraw_request`
--

CREATE TABLE IF NOT EXISTS `affiliate_withdraw_request` (
  `affiliate_withdraw_id` int(100) NOT NULL AUTO_INCREMENT,
  `user_id` int(100) NOT NULL,
  `withdraw_amount` double(10,2) NOT NULL,
  `withdraw_date` datetime NOT NULL,
  `withdraw_ip` varchar(255) DEFAULT NULL,
  `withdraw_status` int(50) NOT NULL DEFAULT '0' COMMENT '0=pending,1=approved,2=success,3=reject,4=fail',
  `currency_code` varchar(255) NOT NULL,
  `currency_symbol` varchar(255) NOT NULL,
  PRIMARY KEY (`affiliate_withdraw_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `amazon`
--

CREATE TABLE IF NOT EXISTS `amazon` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `site_status` varchar(25) DEFAULT NULL,
  `aws_access_key_id` varchar(255) DEFAULT NULL,
  `aws_secret_access_key` varchar(255) DEFAULT NULL,
  `variable_fees` double(10,2) NOT NULL,
  `fixed_fees` double(10,2) NOT NULL,
  `preapproval` int(2) NOT NULL COMMENT 'instant = 0,preapprove =1',
  `gateway_status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `amazon`
--

INSERT INTO `amazon` (`id`, `site_status`, `aws_access_key_id`, `aws_secret_access_key`, `variable_fees`, `fixed_fees`, `preapproval`, `gateway_status`) VALUES
(1, 'sand box', 'AKIAJHRGJTWJ6TZAYFRA', 'mGEiblOo7xnZYOPlWHnmoo3/p5MRhicQBFm4CZ/X', 5.00, 5.00, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `automation_paypal`
--

CREATE TABLE IF NOT EXISTS `automation_paypal` (
  `automation_paypal_id` int(50) NOT NULL AUTO_INCREMENT,
  `site_status` varchar(255) NOT NULL,
  `application_id` varchar(255) NOT NULL,
  `paypal_email` varchar(255) NOT NULL,
  `paypal_username` varchar(255) NOT NULL,
  `paypal_password` varchar(255) NOT NULL,
  `paypal_signature` varchar(255) NOT NULL,
  `preapproval` int(20) NOT NULL DEFAULT '0' COMMENT 'instant = 0,preapprove =1',
  `fees_taken_from` varchar(255) NOT NULL,
  `project_id` int(50) DEFAULT NULL,
  `perk_id` int(50) DEFAULT '0',
  `donate_amount` double(10,2) DEFAULT NULL,
  `user_id` int(50) DEFAULT NULL,
  PRIMARY KEY (`automation_paypal_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `automation_paypal`
--

INSERT INTO `automation_paypal` (`automation_paypal_id`, `site_status`, `application_id`, `paypal_email`, `paypal_username`, `paypal_password`, `paypal_signature`, `preapproval`, `fees_taken_from`, `project_id`, `perk_id`, `donate_amount`, `user_id`) VALUES
(1, 'sandbox', 'APP-80W284485P519543T', 'mihir.test.rockersinfo@gmail.com', 'mihir.test.rockersinfo_api1.gmail.com', '1377498678', 'AFcWxV21C7fd0v3bYYYRCpSSRl31AWGT4755TL-t5vWTRKimjZgwA-3y', 1, 'PRIMARYRECEIVER', 133, 0, 10.00, 91);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
  `city_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `city_name` varchar(255) DEFAULT NULL,
  `active` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`city_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`city_id`, `country_id`, `state_id`, `city_name`, `active`) VALUES
(1, 2, 11, 'Vadodara', '1'),
(2, 1, 9, 'San Jose', '1'),
(3, 1, 9, 'Milpitus', '1'),
(4, 1, 9, 'San Diago', '1'),
(5, 2, 155, 'patna', '1'),
(6, 2, 11, 'Baroda', '1'),
(7, 2, 11, 'Anand', '1'),
(8, 2, 11, 'bharuch', '1');

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
  `comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `user_id` int(100) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `comments` text,
  `status` varchar(255) DEFAULT '0',
  `date_added` datetime NOT NULL,
  `comment_ip` varchar(255) DEFAULT NULL,
  `comment_type` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=342 ;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`comment_id`, `project_id`, `user_id`, `username`, `email`, `photo`, `comments`, `status`, `date_added`, `comment_ip`, `comment_type`) VALUES
(23, 25, 7, NULL, NULL, NULL, 'Hello Please check ', '1', '2013-09-10 17:08:24', '1.22.83.211', 0),
(26, 33, 3, NULL, NULL, NULL, 'sdsdsdd sd', '0', '2013-09-17 16:19:16', '210.89.56.198', 1),
(27, 49, 35, NULL, NULL, NULL, 'hii hwo wre fdskf lsdfds fsf ', '0', '2013-09-21 16:45:42', '210.89.56.198', 0),
(28, 49, 36, NULL, NULL, NULL, 'dsagasgasdgsdagg', '0', '2013-09-21 17:35:50', '1.22.83.185', 0),
(29, 49, 36, NULL, NULL, NULL, 'dsagasgasdgsdagg', '0', '2013-09-21 17:36:08', '1.22.83.185', 0),
(30, 49, 36, NULL, NULL, NULL, 'dsagasgasdgsdagg', '0', '2013-09-21 17:36:19', '1.22.83.185', 0),
(31, 49, 36, NULL, NULL, NULL, 'dsagasgasdgsdagg', '0', '2013-09-21 17:36:31', '1.22.83.185', 0),
(32, 49, 36, NULL, NULL, NULL, 'dsagasgasdgsdagg', '0', '2013-09-21 17:36:43', '1.22.83.185', 0),
(33, 49, 36, NULL, NULL, NULL, 'dsagasgasdgsdagg', '0', '2013-09-21 17:37:00', '1.22.83.185', 0),
(34, 49, 36, NULL, NULL, NULL, 'dsagasgasdgsdagg', '0', '2013-09-21 17:37:09', '1.22.83.185', 0),
(35, 49, 36, NULL, NULL, NULL, 'dsagasgasdgsdagg', '0', '2013-09-21 17:37:21', '1.22.83.185', 0),
(36, 49, 36, NULL, NULL, NULL, 'dsagasgasdgsdagg', '0', '2013-09-21 17:37:34', '1.22.83.185', 0),
(37, 49, 36, NULL, NULL, NULL, 'dsagasgasdgsdagg', '0', '2013-09-21 17:37:39', '1.22.83.185', 0),
(38, 49, 2, NULL, NULL, NULL, 'ghello', '0', '2013-09-21 17:40:19', '210.89.56.198', 0),
(39, 49, 2, NULL, NULL, NULL, 'mehul', '0', '2013-09-21 17:41:06', '210.89.56.198', 0),
(40, 49, 2, NULL, NULL, NULL, 'asdasdasd', '0', '2013-10-01 13:55:07', '210.89.56.198', 0),
(41, 33, 3, NULL, NULL, NULL, 'adasdasd', '0', '2013-10-03 14:24:41', '210.89.56.198', 0),
(42, 89, 6, NULL, NULL, NULL, 'hiii', '1', '2013-11-10 23:59:26', '210.89.56.198', 0),
(43, 89, 3, NULL, NULL, NULL, '2', '1', '2013-11-11 00:02:31', '210.89.56.198', 0),
(44, 89, 3, NULL, NULL, NULL, '3', '1', '2013-11-11 00:02:38', '210.89.56.198', 0),
(45, 89, 3, NULL, NULL, NULL, '4', '1', '2013-11-11 00:02:45', '210.89.56.198', 0),
(46, 89, 3, NULL, NULL, NULL, '5', '1', '2013-11-11 00:02:50', '210.89.56.198', 0),
(47, 89, 3, NULL, NULL, NULL, 'fdsafdsafdsafsaf', '1', '2013-11-11 00:03:00', '210.89.56.198', 0),
(48, 89, 3, NULL, NULL, NULL, 'This is test', '1', '2013-11-11 03:42:02', '210.89.56.198', 0),
(49, 89, 3, NULL, NULL, NULL, 'This is test', '1', '2013-11-11 03:42:13', '210.89.56.198', 0),
(50, 89, 3, NULL, NULL, NULL, 'test ', '1', '2013-11-11 04:02:26', '1.22.80.217', 0),
(51, 89, 36, NULL, NULL, NULL, 'Hi This is nxt comment to come.', '1', '2013-11-11 08:02:30', '210.89.56.198', 0),
(52, 89, 36, NULL, NULL, NULL, 'A sci-fi noir, a mystery, a first person puzzle game. Mind boggling puzzles, a grippingly haunting but beautiful story and an unparalleled immersive atmosphereA sci-fi noir, a mystery, a first person puzzle game. Mind boggling puzzles, a grippingly haunting but beautiful story and an unparalleled immersive atmosphereA sci-fi noir, a mystery, a first person puzzle game. Mind boggling puzzles, a grippingly haunting but beautiful story and an unparalleled immersive atmosphereA sci-fi noir, a myster', '1', '2013-11-11 08:02:59', '210.89.56.198', 0),
(53, 107, 3, NULL, NULL, NULL, 'Right now, no one else can see your campaign. You can edit anything you want. When you are satisfied and ready to get funded, click the ''s''s''s''s"Go Live''s" button on the', '1', '2013-11-25 04:04:52', '210.89.56.198', 0),
(54, 109, 56, NULL, NULL, NULL, 'dsadas', '1', '2013-12-02 05:19:50', '210.89.56.198', 0),
(55, 109, 3, NULL, NULL, NULL, 'Hello Please please ', '0', '2013-12-02 07:36:00', '1.22.80.182', 0),
(56, 109, 3, NULL, NULL, NULL, 'Hello Please please ', '0', '2013-12-02 07:36:00', '1.22.80.182', 0),
(57, 113, 2, NULL, NULL, NULL, 'This my comment', '1', '2013-12-04 05:01:31', '210.89.56.198', 0),
(58, 113, 2, NULL, NULL, NULL, 'This my comment', '1', '2013-12-04 05:01:33', '210.89.56.198', 0),
(59, 89, 6, NULL, NULL, NULL, 'Hello', '0', '2013-12-23 05:05:29', '210.89.56.198', 0),
(60, 127, 36, NULL, NULL, NULL, 'hghg', '0', '2013-12-27 04:22:49', '1.22.83.98', 0),
(63, 96, 56, NULL, NULL, NULL, 'Comment posted successfully owner', '1', '2014-01-03 03:20:26', '210.89.56.198', 0),
(64, 96, 56, NULL, NULL, NULL, '2nd owner comment now check wat haps', '1', '2014-01-03 03:23:55', '210.89.56.198', 0),
(75, 96, 3, NULL, NULL, NULL, 'other user cmt ', '1', '2014-01-03 03:43:48', '210.89.56.198', 0),
(76, 96, 3, NULL, NULL, NULL, '2nd other user cmt', '1', '2014-01-03 03:45:21', '210.89.56.198', 0),
(79, 154, 3, NULL, NULL, NULL, 'hey my 1st comment', '1', '2014-01-06 23:33:20', '210.89.56.198', 0),
(80, 154, 3, NULL, NULL, NULL, '2nd comment', '1', '2014-01-06 23:33:33', '210.89.56.198', 0),
(82, 154, 3, NULL, NULL, NULL, '4th cmt', '1', '2014-01-06 23:34:12', '210.89.56.198', 0),
(85, 154, 3, NULL, NULL, NULL, 'dasdasddasd', '1', '2014-01-07 00:00:43', '210.89.56.198', 1),
(86, 154, 2, NULL, NULL, NULL, 'hey new user comment', '1', '2014-01-07 00:53:40', '210.89.56.198', 0),
(88, 28, 6, NULL, NULL, NULL, 'guig 8guigui guigui giug', '1', '2014-01-07 01:06:51', '210.89.56.198', 0),
(89, 25, 6, NULL, NULL, NULL, 'Ahshgsushshsugsh', '1', '2014-01-07 01:07:34', '210.89.56.198', 0),
(90, 154, 2, NULL, NULL, NULL, 'hey private comment', '1', '2014-01-07 01:28:28', '210.89.56.198', 1),
(91, 154, 3, NULL, NULL, NULL, 'hey reply to private comment', '1', '2014-01-07 02:41:03', '1.22.83.67', 1),
(92, 154, 3, NULL, NULL, NULL, 'reply to private comment', '1', '2014-01-07 02:43:28', '1.22.83.67', 1),
(93, 154, 3, NULL, NULL, NULL, 'normal comment', '1', '2014-01-07 02:44:08', '1.22.83.67', 0),
(94, 190, 120, NULL, NULL, NULL, 'hello i am honer of this project.', '1', '2014-01-17 00:37:44', '1.22.81.171', 0),
(95, 10, 127, NULL, NULL, NULL, 'qwertyuiopasdfghjklzxcvbnqwertyuiopasdfghjklzxcvbnqwertyuiopasdfghjklzxcvbnqwertyuiopasdfghjklzxcvbnqwertyuiopasdfghjklzxcvbnqwertyuiopasdfghjklzxcvbnqwertyuiopasdfghjklzxcvbnqwertyuiopasdfghjklzxcvbnqwertyuiopasdfghjklzxcvbnqwertyuiopasdfghjklzxcvbnqwertyuiopasdfghjklzxcvbnqwertyuiopasdfghjklzxcvbnqwertyuiopasdfghjklzxcvbnqwertyuiopasdfghjklzxcvbnqwertyuiopasdfghjklzxcvbnqwertyuiopasdfghjklzxcvbnqwertyuiopasdfghjklzxcvbnqwertyuiopasdfghjklzxcvbnqwertyuiopasdfghjklzxcvbnqwertyuiopasdfghjklzxcvbn', '0', '2014-02-06 06:40:04', '1.22.82.138', 1),
(96, 10, 127, NULL, NULL, NULL, ' mngkopgknb,mfgbpotkhmm gkjibhmiohlkhjoih', '0', '2014-02-06 06:40:19', '1.22.82.138', 0),
(97, 10, 127, NULL, NULL, NULL, ' mngkopgknb,mfgbpotkhmm gkjibhmiohlkhjoih', '0', '2014-02-06 06:40:19', '1.22.82.138', 0),
(98, 245, 168, NULL, NULL, NULL, 'my first comment, my first comment, my first comment', '1', '2014-02-14 06:51:13', '103.254.202.17', 0),
(99, 245, 168, NULL, NULL, NULL, 'dfgdfg fdgfdgdfgd gfg', '1', '2014-02-14 06:52:14', '103.254.202.17', 0),
(100, 245, 168, NULL, NULL, NULL, 'my first comment, my first comment, my first comment - See more at: http://www.hireiphone.com/indiegogoclone/projects/tryry#.Uv4DHWKSzfImy first comment, my first comment, my first comment - See more at: http://www.hireiphone.com/indiegogoclone/projects/tryry#.Uv4DHWKSzfI', '1', '2014-02-14 06:52:53', '103.254.202.17', 0),
(101, 245, 168, NULL, NULL, NULL, 'my first comment, my first comment, my first commen - See more at: http://www.hireiphone.com/indiegogoclone/projects/tryry#.Uv4E9s5sbIUmy first comment, my first comment, my first commen - See more at: http://www.hireiphone.com/indiegogoclone/projects/tryry#.Uv4E9s5sbIU', '1', '2014-02-14 07:02:09', '103.254.202.17', 0),
(102, 245, 168, NULL, NULL, NULL, 'my first comment, my first comment, my first comment', '1', '2014-02-14 07:05:36', '103.254.202.17', 0),
(103, 216, 168, NULL, NULL, NULL, 'my first comment, my first comment', '0', '2014-02-14 07:08:18', '103.254.202.17', 0),
(104, 222, 168, NULL, NULL, NULL, 'There is comment for this project', '0', '2014-02-14 07:57:56', '103.254.202.17', 0),
(105, 222, 168, NULL, NULL, NULL, 'There is comment for this project.', '0', '2014-02-14 07:59:47', '103.254.202.17', 0),
(106, 222, 168, NULL, NULL, NULL, 'There is no comment for this project.', '0', '2014-02-14 08:06:57', '103.254.202.17', 0),
(107, 222, 168, NULL, NULL, NULL, 'There is no comment for this project.', '0', '2014-02-14 08:07:41', '103.254.202.17', 0),
(108, 222, 168, NULL, NULL, NULL, 'There is no comment for this project.', '0', '2014-02-14 08:08:06', '103.254.202.17', 0),
(109, 1, 111, NULL, NULL, NULL, 'good job.', '0', '2014-02-14 16:33:31', '108.201.19.142', 0),
(110, 1, 111, NULL, NULL, NULL, 'that''s really great.', '0', '2014-02-14 16:35:21', '108.201.19.142', 0),
(111, 1, 111, NULL, NULL, NULL, 'hello again. testing.', '0', '2014-02-14 16:36:32', '108.201.19.142', 0),
(112, 222, 111, NULL, NULL, NULL, 'amazing', '0', '2014-02-14 16:45:41', '108.201.19.142', 1),
(113, 1, 111, NULL, NULL, NULL, 'amazing test 2', '0', '2014-02-14 16:46:22', '108.201.19.142', 0),
(114, 1, 111, NULL, NULL, NULL, 'amazing test 3', '0', '2014-02-14 16:46:32', '108.201.19.142', 1),
(115, 245, 114, NULL, NULL, NULL, 'This campaign will receive all of the funds ', '0', '2014-02-14 23:29:01', '210.89.56.198', 0),
(116, 245, 114, NULL, NULL, NULL, 'This campaign will receive all of the funds ', '0', '2014-02-14 23:32:07', '210.89.56.198', 0),
(117, 245, 114, NULL, NULL, NULL, 'This campaign will receive all of the funds ', '0', '2014-02-14 23:32:48', '210.89.56.198', 0),
(118, 245, 114, NULL, NULL, NULL, 'This campaign will receive all of the funds ', '0', '2014-02-14 23:34:12', '210.89.56.198', 0),
(119, 245, 114, NULL, NULL, NULL, 'This campaign will receive all of the funds ', '0', '2014-02-14 23:36:46', '210.89.56.198', 0),
(120, 245, 114, NULL, NULL, NULL, 'This campaign will receive all of the funds ', '0', '2014-02-14 23:37:28', '210.89.56.198', 0),
(121, 245, 114, NULL, NULL, NULL, 'This campaign will receive all of the funds ', '0', '2014-02-14 23:38:04', '210.89.56.198', 0),
(122, 245, 114, NULL, NULL, NULL, 'my first comment, my first comment, my first comment', '0', '2014-02-14 23:40:11', '210.89.56.198', 0),
(123, 245, 114, NULL, NULL, NULL, 'my first comment, my first comment, my first comment', '0', '2014-02-14 23:43:33', '210.89.56.198', 0),
(124, 245, 114, NULL, NULL, NULL, 'i m kinjal, u r ritu', '0', '2014-02-14 23:45:12', '210.89.56.198', 0),
(125, 222, 126, NULL, NULL, NULL, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. ', '1', '2014-03-22 01:49:48', '1.22.80.139', 0),
(126, 222, 126, NULL, NULL, NULL, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. ', '1', '2014-03-22 01:49:57', '1.22.80.139', 0),
(127, 285, 127, NULL, NULL, NULL, 'Post Latest Updates.', '1', '2014-05-09 02:43:11', '1.22.81.217', 0),
(130, 293, 213, NULL, NULL, NULL, 'Add updates', '1', '2014-05-16 07:31:30', '1.22.80.255', 0),
(139, 293, 127, NULL, NULL, NULL, 'Ok', '1', '2014-05-16 08:34:56', '1.22.80.255', 0),
(146, 293, 213, NULL, NULL, NULL, 'add', '1', '2014-05-16 08:47:57', '1.22.80.255', 1),
(147, 293, 127, NULL, NULL, NULL, 'Add1', '1', '2014-05-16 08:48:58', '1.22.80.255', 1),
(190, 309, 127, NULL, NULL, NULL, 'Add Updates', '0', '2014-05-26 03:26:00', '1.22.83.166', 0),
(191, 309, 127, NULL, NULL, NULL, 'Add Updates', '0', '2014-05-26 03:26:00', '1.22.83.166', 0),
(192, 309, 127, NULL, NULL, NULL, 'Add Updates', '0', '2014-05-26 03:26:00', '1.22.83.166', 0),
(193, 309, 127, NULL, NULL, NULL, 'Add Updates', '0', '2014-05-26 03:26:01', '1.22.83.166', 0),
(194, 309, 127, NULL, NULL, NULL, 'Add Updates', '0', '2014-05-26 03:26:01', '1.22.83.166', 0),
(195, 309, 127, NULL, NULL, NULL, 'Add Updates', '0', '2014-05-26 03:26:02', '1.22.83.166', 0),
(196, 309, 127, NULL, NULL, NULL, 'Add Updates', '0', '2014-05-26 03:26:02', '1.22.83.166', 0),
(197, 309, 127, NULL, NULL, NULL, 'Add Updates', '0', '2014-05-26 03:26:14', '1.22.83.166', 0),
(198, 309, 127, NULL, NULL, NULL, 'Add Updates', '0', '2014-05-26 03:26:14', '1.22.83.166', 0),
(199, 309, 127, NULL, NULL, NULL, 'Add Updates', '0', '2014-05-26 03:26:15', '1.22.83.166', 0),
(200, 309, 127, NULL, NULL, NULL, 'Add Updates', '0', '2014-05-26 03:26:15', '1.22.83.166', 0),
(201, 309, 127, NULL, NULL, NULL, 'Add Updates', '0', '2014-05-26 03:26:15', '1.22.83.166', 0),
(202, 309, 127, NULL, NULL, NULL, 'Add Updates', '0', '2014-05-26 03:26:15', '1.22.83.166', 0),
(203, 309, 127, NULL, NULL, NULL, 'Add Updates', '0', '2014-05-26 03:26:15', '1.22.83.166', 0),
(204, 309, 127, NULL, NULL, NULL, 'Add Updates', '0', '2014-05-26 03:26:16', '1.22.83.166', 0),
(205, 309, 127, NULL, NULL, NULL, 'Add Updates', '0', '2014-05-26 03:26:16', '1.22.83.166', 0),
(206, 309, 127, NULL, NULL, NULL, 'Add Updates', '0', '2014-05-26 03:26:16', '1.22.83.166', 0),
(207, 309, 127, NULL, NULL, NULL, 'Add Updates', '0', '2014-05-26 03:26:17', '1.22.83.166', 0),
(208, 309, 127, NULL, NULL, NULL, 'Add Updates', '0', '2014-05-26 03:26:17', '1.22.83.166', 0),
(209, 309, 127, NULL, NULL, NULL, 'Add Updates', '0', '2014-05-26 03:26:17', '1.22.83.166', 0),
(210, 309, 127, NULL, NULL, NULL, 'Add Updates', '0', '2014-05-26 03:26:18', '1.22.83.166', 0),
(211, 309, 127, NULL, NULL, NULL, 'Add Updates', '0', '2014-05-26 03:26:19', '1.22.83.166', 0),
(212, 309, 127, NULL, NULL, NULL, 'Add Updates', '0', '2014-05-26 03:26:19', '1.22.83.166', 0),
(213, 309, 127, NULL, NULL, NULL, 'Add Updates', '0', '2014-05-26 03:26:20', '1.22.83.166', 0),
(214, 309, 127, NULL, NULL, NULL, 'Add Updates', '0', '2014-05-26 03:26:21', '1.22.83.166', 0),
(215, 309, 127, NULL, NULL, NULL, 'Add Updates', '0', '2014-05-26 04:11:06', '1.22.83.166', 0),
(216, 309, 127, NULL, NULL, NULL, 'Add Updates', '0', '2014-05-26 04:11:07', '1.22.83.166', 0),
(217, 309, 127, NULL, NULL, NULL, 'Add Updates', '0', '2014-05-26 04:11:08', '1.22.83.166', 0),
(218, 309, 127, NULL, NULL, NULL, 'Add Updates', '0', '2014-05-26 04:11:08', '1.22.83.166', 0),
(219, 309, 127, NULL, NULL, NULL, 'Add Updates', '0', '2014-05-26 04:11:08', '1.22.83.166', 0),
(220, 309, 127, NULL, NULL, NULL, 'Add Updates', '0', '2014-05-26 04:11:08', '1.22.83.166', 0),
(221, 309, 127, NULL, NULL, NULL, 'Add Updates', '0', '2014-05-26 04:11:09', '1.22.83.166', 0),
(222, 309, 127, NULL, NULL, NULL, 'Add Updates', '0', '2014-05-26 04:11:09', '1.22.83.166', 0),
(223, 309, 127, NULL, NULL, NULL, 'Add Updates', '0', '2014-05-26 04:11:10', '1.22.83.166', 0),
(244, 309, 127, NULL, NULL, NULL, 'Add', '1', '2014-05-27 07:26:08', '1.22.81.200', 0),
(245, 309, 216, NULL, NULL, NULL, 'OKK.', '1', '2014-05-27 07:26:44', '1.22.81.200', 0),
(246, 309, 127, NULL, NULL, NULL, 'Add', '1', '2014-05-27 07:27:31', '1.22.81.200', 1),
(247, 309, 216, NULL, NULL, NULL, 'Ok', '1', '2014-05-27 07:27:57', '1.22.81.200', 1),
(249, 309, 36, NULL, NULL, NULL, 'Test1', '1', '2014-05-27 07:41:03', '1.22.81.200', 0),
(250, 309, 213, NULL, NULL, NULL, 'Test2', '1', '2014-05-27 07:47:02', '1.22.81.200', 0),
(251, 309, 216, NULL, NULL, NULL, 'Test3', '1', '2014-05-27 07:48:08', '1.22.81.200', 0),
(253, 309, 213, NULL, NULL, NULL, 'Test4', '1', '2014-05-27 07:57:28', '1.22.81.200', 0),
(254, 314, 219, NULL, NULL, NULL, 'Hi testing comments bugs', '0', '2014-05-29 07:23:59', '1.22.81.138', 0),
(255, 314, 219, NULL, NULL, NULL, 'testing new messges', '0', '2014-05-29 07:26:51', '1.22.81.138', 0),
(256, 321, 216, NULL, NULL, NULL, 'Add', '0', '2014-05-29 07:27:54', '210.89.56.198', 0),
(257, 325, 219, NULL, NULL, NULL, 'Comment on test4', '0', '2014-05-30 02:41:10', '1.22.81.175', 0),
(258, 311, 219, NULL, NULL, NULL, 'Add', '1', '2014-05-30 16:13:16', '14.96.165.72', 0),
(259, 313, 219, NULL, NULL, NULL, 'TEST', '1', '2014-05-30 16:18:58', '14.96.165.72', 0),
(260, 339, 127, NULL, NULL, NULL, 'Add updates', '0', '2014-06-03 04:15:25', '1.22.80.178', 0),
(261, 415, 127, NULL, NULL, NULL, 'Comment added for Testing purpose only.', '1', '2014-06-13 08:34:27', '1.22.81.184', 0),
(262, 415, 82, NULL, NULL, NULL, 'Ok', '1', '2014-06-13 08:34:56', '1.22.81.184', 0),
(263, 284, 127, NULL, NULL, NULL, 'test comments by owner', '0', '2014-06-18 05:03:21', '210.89.56.198', 0),
(264, 16, 2, NULL, NULL, NULL, 'Add updates for project.', '1', '2014-06-26 07:42:56', '1.22.82.64', 0),
(265, 16, 1, NULL, NULL, NULL, 'I have added updates.', '1', '2014-06-26 07:43:29', '1.22.82.64', 0),
(268, 35, 2, NULL, NULL, NULL, 'Add new updates.', '1', '2014-07-19 03:49:02', '1.22.82.111', 0),
(269, 35, 1, NULL, NULL, NULL, 'New updates have been added.', '1', '2014-07-19 03:50:36', '1.22.82.111', 0),
(270, 58, 2, NULL, NULL, NULL, 'testing comemnt langauge issue', '1', '2014-07-23 02:08:20', '1.22.80.178', 0),
(271, 58, 1, NULL, NULL, NULL, 'testinf comments respone edited french', '1', '2014-07-23 02:12:23', '1.22.80.178', 0),
(272, 58, 2, NULL, NULL, NULL, 'testinf or jayshree', '0', '2014-07-23 02:17:44', '1.22.80.178', 0),
(277, 34, 1, NULL, NULL, NULL, 'Comment3', '1', '2014-08-12 04:11:38', '1.22.81.191', 0),
(294, 92, 2, NULL, NULL, NULL, 'Add updates', '1', '2014-08-27 05:35:36', '1.22.81.138', 0),
(295, 92, 2, NULL, NULL, NULL, 'Comment2', '1', '2014-08-27 05:35:50', '1.22.81.138', 0),
(296, 92, 1, NULL, NULL, NULL, 'OK', '1', '2014-08-27 05:38:41', '1.22.81.138', 0),
(297, 92, 1, NULL, NULL, NULL, 'OK', '1', '2014-08-27 05:38:49', '1.22.81.138', 0),
(298, 92, 1, NULL, NULL, NULL, 'OK', '1', '2014-08-27 06:22:19', '1.22.81.138', 0),
(299, 92, 1, NULL, NULL, NULL, 'OK', '1', '2014-08-27 06:22:29', '1.22.81.138', 0),
(300, 92, 1, NULL, NULL, NULL, 'Okn', '1', '2014-08-27 06:24:39', '1.22.81.138', 0),
(302, 92, 2, NULL, NULL, NULL, 'New Member Test', '0', '2014-08-27 06:47:07', '1.22.81.138', 0),
(305, 59, 2, NULL, NULL, NULL, 'Spam test', '3', '2014-08-27 07:13:02', '1.22.81.138', 0),
(306, 59, 2, NULL, NULL, NULL, 'Spam test2222', '3', '2014-08-27 07:15:36', '1.22.81.138', 0),
(310, 60, 2, NULL, NULL, NULL, 'Comment private', '3', '2014-08-27 08:19:11', '1.22.81.138', 1),
(311, 60, 1, NULL, NULL, NULL, 'Private reply', '1', '2014-08-27 08:20:25', '1.22.81.138', 1),
(312, 60, 2, NULL, NULL, NULL, 'Comment 1', '3', '2014-08-27 08:43:44', '1.22.81.138', 0),
(313, 60, 2, NULL, NULL, NULL, 'Comment1n', '3', '2014-08-27 08:53:36', '1.22.81.138', 0),
(314, 77, 2, NULL, NULL, NULL, 'Add updates', '1', '2014-08-30 06:07:06', '1.22.80.161', 0),
(316, 107, 1, NULL, NULL, NULL, 'gfhf', '1', '2014-09-04 06:37:45', '1.22.80.195', 0),
(317, 146, 9, NULL, NULL, NULL, 'This is demoasdfasdfasdf', '0', '2014-09-18 00:13:16', '1.22.81.225', 0),
(318, 159, 9, NULL, NULL, NULL, 'This fentastic awsome', '1', '2014-10-07 03:16:33', '113.193.174.146', 0),
(319, 159, 9, NULL, NULL, NULL, 'asdfasdfasdfasdfasdfasdfasdfadsfadsf', '1', '2014-10-07 03:17:27', '113.193.174.146', 0),
(320, 159, 9, NULL, NULL, NULL, 'asdfasdfasdfasdfasdfasdfasdfadsfadsf', '1', '2014-10-07 03:17:30', '113.193.174.146', 0),
(321, 159, 9, NULL, NULL, NULL, 'This is private comment', '1', '2014-10-07 03:19:49', '113.193.174.146', 1),
(325, 5, 2, NULL, NULL, NULL, 'We provide a specialized-niche exchange process that is proven to work. ?', '1', '2014-10-29 03:38:06', '1.22.80.198', 0),
(326, 5, 5, NULL, NULL, NULL, 'Offer anything you feel a growing nonprofit organization may need. Your offer to donate remains "an offer to donate" while you remain anonymous. When you initiate a reply to a requesting participant, your identity is only then revealed. This allows you the convenience of anonymously browsing through a list of pre-screened nonprofit applicants who have requested your gift specifically and who are direct receiver-recipient or end-user requestees.nnYou are never obligated to act upon any single req', '1', '2014-10-29 22:10:38', '1.22.82.216', 0),
(328, 5, 4, NULL, NULL, NULL, 'The project provides financial assistance Class VIII to Class XII with the potential to excel by providing them necessary support ....', '1', '2014-10-30 00:22:11', '1.22.82.216', 0),
(329, 5, 3, NULL, NULL, NULL, ' crowdfunding sites that have different models and ... Kickstarter is a site where creative projects raise donation-based funding.', '1', '2014-10-30 00:36:11', '1.22.82.216', 0),
(331, 6, 2, NULL, NULL, NULL, 'What is the issue, problem, or challenge?', '1', '2014-10-30 01:08:05', '1.22.82.216', 0),
(332, 6, 3, NULL, NULL, NULL, 'UNAIDS estimates that to date, 14 million children have lost one or both parents to AIDS in Sub-Saharan Africa. Even Ground''s projects aim to address the needs of young people impacted by the epidemic. Because the areas where we work are affected not only by the HIV epidemic, but also by poverty, unemployment, and longstanding inequality, our programs aim not only to provide an education for young people, but also to support families and communities to strengthen local networks of care.n', '1', '2014-10-30 01:11:08', '1.22.82.216', 0),
(333, 6, 5, NULL, NULL, NULL, 'How will this project solve this problem?', '1', '2014-10-30 01:18:16', '1.22.82.216', 0),
(334, 6, 4, NULL, NULL, NULL, 'Even Ground has brought together a group of small, like-minded organizations, all started by young social entrepreneurs, to address the impacts of the HIV epidemic on children and youth. Even Ground works to facilitate the operations of these projects, and to cultivate a network of support and collaboration between them. Unlike larger organizations, our partnership model allows for local ownership and local innovation in each of our partner project''s program sites.n', '1', '2014-10-30 01:24:27', '1.22.82.216', 0),
(335, 4, 4, NULL, NULL, NULL, 'What is the issue, problem, or challenge?', '1', '2014-10-30 02:49:21', '1.22.82.216', 0),
(336, 4, 2, NULL, NULL, NULL, 'Argentina recovered democracy 30 years ago, and the citizens still dont understand how the system works. After years of political parties monopolizing the country agenda, people don''t believe that the system will resolve their problems......', '1', '2014-10-30 02:51:04', '1.22.82.216', 0),
(338, 4, 5, NULL, NULL, NULL, 'Right now there are no spaces besides the political parties to participate in the serious problems of the country, and people become socially irresponsible and dont realize they are part of the solution and can create a better future.', '1', '2014-10-30 02:53:48', '1.22.82.216', 0),
(339, 4, 2, NULL, NULL, NULL, 'How will this project solve this problem?n', '1', '2014-10-30 02:54:58', '1.22.82.216', 0),
(340, 4, 4, NULL, NULL, NULL, 'Engagement of the civil society in the country''s issues is key to create a future and build more democracy,therefore we will offer a platform where people can get involved and engage with different topics. The project will be execute with many small actions going around the country''s issues, involving specially youngsters from high schools and universities.', '1', '2014-10-30 02:55:56', '1.22.82.216', 0),
(341, 4, 3, NULL, NULL, NULL, 'Potential Long Term ImpactnThe long term effects are difficult to project, everything can happen basically. We expect to create more social committed society and reach around 300 students yearly and 4000 people with our actions.', '1', '2014-10-30 03:05:35', '1.22.82.216', 0);

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(255) DEFAULT NULL,
  `fips` varchar(255) DEFAULT NULL,
  `iso2` varchar(255) DEFAULT NULL,
  `iso3` varchar(255) DEFAULT NULL,
  `ison` varchar(255) DEFAULT NULL,
  `internet` varchar(255) DEFAULT NULL,
  `capital` varchar(255) DEFAULT NULL,
  `map_ref` varchar(255) DEFAULT NULL,
  `singular` varchar(255) DEFAULT NULL,
  `plural` varchar(255) DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `currency_code` varchar(255) DEFAULT NULL,
  `population` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `comment` text,
  `active` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=219 ;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`country_id`, `country_name`, `fips`, `iso2`, `iso3`, `ison`, `internet`, `capital`, `map_ref`, `singular`, `plural`, `currency`, `currency_code`, `population`, `title`, `comment`, `active`) VALUES
(1, 'USA', '', '', '', '', '', '', '', '', '', '', '$', '', '', '', '1'),
(2, 'India', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '1'),
(3, 'Japan', '', '', '', '', '', '', '', '', '', '', '#', '', '', '', '0'),
(4, 'Qatar', '', '', '', '', '', 'doha', '', '', '', 'Riyal', '', '20000000', '', '', '0'),
(5, 'United Kingdom', '', '', '', '', '', 'London', '', '', '', '', '', '30000000', '', '', '0'),
(6, 'Russia', '', '', '', '', '', '', '', '', '', '', '', '50000000', '', '', '0'),
(7, 'Fiji', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(8, 'Afghanistan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '1'),
(9, 'Albania', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '1'),
(10, 'Algeria', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '1'),
(11, 'Andorra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '1'),
(12, 'Angola', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '1'),
(13, 'Antigua and Barbuda', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '1'),
(14, 'Argentina', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '1'),
(15, 'Armenia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '1'),
(18, 'Australia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '1'),
(19, 'Austria', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '1'),
(20, 'Azerbaijan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '1'),
(21, 'Bahamas', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(22, 'Bahrain', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(23, 'Bangladesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(24, 'Barbados', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(26, 'Belarus', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(27, 'Belgium', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '1'),
(28, 'Belize', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(29, 'Benin', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(30, 'Bhutan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(31, 'Bolivia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(32, 'Bosnia and Herzegovina', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(33, 'Botswana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(34, 'Brazil', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '1'),
(35, 'Brunei ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(36, 'Bulgaria', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(37, 'Burkina Faso', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(38, 'Burma', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(39, 'Burundi', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(40, 'Cambodia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(41, 'Cameroon', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(42, 'Canada', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(43, 'Cape Verde', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(44, 'Central African Republic', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(45, 'Chad', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(46, 'Chile', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(47, 'China', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(48, 'Colombia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(49, 'Comoros', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(50, 'Congo ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(51, 'Costa Rica', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(52, 'Croatia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(53, 'Cuba', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(54, 'Cyprus', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(55, 'Czech Republic', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(56, 'Denmark', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(57, 'Djibouti', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(58, 'Dominica', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(59, 'Dominican Republic', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(60, 'Ecuador', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(61, 'Egypt', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(62, 'El Salvador', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(63, 'Equatorial Guinea', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(64, 'Eritrea', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(65, 'Estonia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(66, 'Ethiopia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(67, 'Finland', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(68, 'France', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(69, 'Gabon', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(70, 'Gambia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(71, 'Georgia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(72, 'Germany', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(73, 'Ghana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(74, 'Grenada', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(75, 'Guatemala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(76, 'Guinea', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(77, 'Guinea-Bissau', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(78, 'Guyana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(79, 'Haiti', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(81, 'Holy See', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(82, 'Honduras', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(83, 'Hong Kong', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(84, 'Hungary', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(85, 'Iceland', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(86, 'Indonesia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(87, 'Iran', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(88, 'Iraq', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(89, 'Ireland', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(90, 'Italy', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(91, 'Jamaica', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(92, 'Jordan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(93, 'Kazakhstan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(94, 'Kenya', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(95, 'Kiribati', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(96, 'Korea', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(97, 'Kosovo', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(98, 'Kuwait', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(99, 'Kyrgyzstan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(100, 'Laos', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(101, 'Latvia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(102, 'Lebanon', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(103, 'Lesotho', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(104, 'Liberia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(105, 'Libya', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(106, 'Liechtenstein', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(107, 'Lithuania', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(108, 'Luxembourg', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(109, 'Macau', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(110, 'Macedonia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(111, 'Madagascar', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(112, 'Malawi', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '1'),
(113, 'Malaysia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(114, 'Maldives', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(115, 'Mali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(116, 'Malta', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(117, 'Uruguay', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(118, 'Marshall Islands', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(119, 'Mauritania', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(120, 'Mauritius', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(121, 'Mexico', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(122, 'Micronesia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(123, 'Moldova', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(124, 'Monaco', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(125, 'Mongolia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(126, 'Montenegro', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(127, 'Morocco', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(128, 'Mozambique', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(129, 'Namibia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(130, 'Nauru', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(131, 'Nepal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(132, 'Netherlands', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(133, 'Netherlands Antilles', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(134, 'New Zealand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(135, 'Nicaragua', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(136, 'Niger', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(137, 'Nigeria', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(138, 'North Korea', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(139, 'Norway', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(140, 'Oman', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(141, 'Pakistan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(142, 'Palau', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(143, 'Palestinian Territories', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(144, 'Panama', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(145, 'Papua New Guinea', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(146, 'Paraguay', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(147, 'Peru', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(148, 'Philippines', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(149, 'Poland', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(152, 'Romania', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(154, 'Rwanda', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(155, 'Saint Kitts and Nevis', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(156, 'Saint Lucia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(157, 'Saint Vincent and the Grenadines', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(158, 'Samoa ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(159, 'San Marino', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(160, 'Sao Tome and Principe', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(161, 'Saudi Arabia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(162, 'Senegal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(163, 'Serbia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(164, 'Seychelles', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(167, 'Sierra Leone', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(169, 'Slovakia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(170, 'Slovenia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(171, 'Solomon Islands', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(172, 'Somalia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(173, 'South Africa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(174, 'South Korea', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(175, 'South Sudan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(176, 'Spain ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(177, 'Sri Lanka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(178, 'Sudan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(179, 'Suriname', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(180, 'Swaziland ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(181, 'Sweden', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(182, 'Switzerland', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(183, 'Syria', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(184, 'Taiwan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(185, 'Tajikistan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(186, 'Tanzania', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(187, 'Thailand ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(188, 'Timor-Leste', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(189, 'Togo', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(190, 'Tonga', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(191, 'Trinidad and Tobago', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(193, 'Tunisia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(194, 'Turkey', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(195, 'Turkmenistan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(196, 'Tuvalu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(197, 'Uganda', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(198, 'Ukraine', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(199, 'United Arab Emirates', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(200, 'Uzbekistan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(201, 'Vanuatu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(202, 'Venezuela', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(203, 'Vietnam', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(204, 'Yemen', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(205, 'Zambia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(206, 'Zimbabwe', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(207, 'singapore', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(210, 'puerto rico', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0'),
(211, 'Portugal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(216, 'teatsts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(218, 'Alphabet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1');

-- --------------------------------------------------------

--
-- Table structure for table `cronjob`
--

CREATE TABLE IF NOT EXISTS `cronjob` (
  `cronjob_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `cronjob` varchar(255) DEFAULT NULL,
  `date_run` datetime NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`cronjob_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `crons`
--

CREATE TABLE IF NOT EXISTS `crons` (
  `crons_id` int(11) NOT NULL AUTO_INCREMENT,
  `cron_function` varchar(255) DEFAULT NULL,
  `cron_title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`crons_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `crons`
--

INSERT INTO `crons` (`crons_id`, `cron_function`, `cron_title`) VALUES
(1, 'set_auto_ending', 'Preapprove the Ended Project'),
(2, 'affiliate_update_earn_status', 'Affiliate Update Earn Status');

-- --------------------------------------------------------

--
-- Table structure for table `currency_code`
--

CREATE TABLE IF NOT EXISTS `currency_code` (
  `currency_code_id` int(50) NOT NULL AUTO_INCREMENT,
  `currency_name` varchar(50) DEFAULT NULL,
  `currency_code` varchar(50) DEFAULT NULL,
  `currency_symbol` varchar(50) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`currency_code_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `currency_code`
--

INSERT INTO `currency_code` (`currency_code_id`, `currency_name`, `currency_code`, `currency_symbol`, `active`) VALUES
(24, 'Canadian Dollar', 'CAD', 'CAD', 1),
(3, 'Czech Koruna', 'CZK', 'kc', 0),
(4, 'Danish Krone', 'DKK', 'kr', 0),
(5, 'Euro', 'EUR', '&euro;', 0),
(6, 'Hong Kong Dollar', 'HKD', '$$$', 1),
(7, 'Hungarian Forint', 'HUF', 'Ft', 1),
(8, 'Israeli New Sheqel', 'ILS', ' &#8362;', 1),
(10, 'Mexican Peso', 'MXN', '$', 1),
(11, 'Norwegian Krone', 'NOK', 'kr', 1),
(12, 'New Zealand Dollar', 'NZD', '$', 1),
(13, 'Polish Zloty', 'PLN', 'zt', 1),
(14, 'Pound Sterling', 'GBP', '&pound;', 1),
(15, 'Singapore Dollar', 'SGD', '$', 1),
(16, 'Swedish Krona', 'SEK', 'kr', 1),
(17, 'Swiss Franc', 'CHF', 'CHF', 1),
(18, 'U.S. Dollar', 'USD', '$', 1),
(19, 'testib', 'f', 'f', 0),
(23, 'Test', 'Test', 'itssecret', 0),
(27, 'Japanese Yen', 'JPY', '¥', 1);

-- --------------------------------------------------------

--
-- Table structure for table `custom_link`
--

CREATE TABLE IF NOT EXISTS `custom_link` (
  `custom_link_id` int(100) NOT NULL AUTO_INCREMENT,
  `project_id` int(100) NOT NULL,
  `clink` text COLLATE utf8_unicode_ci NOT NULL,
  `lititle` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`custom_link_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `email_setting`
--

CREATE TABLE IF NOT EXISTS `email_setting` (
  `email_setting_id` int(10) NOT NULL AUTO_INCREMENT,
  `mailer` varchar(50) DEFAULT NULL,
  `sendmail_path` varchar(255) DEFAULT NULL,
  `smtp_port` varchar(50) DEFAULT NULL,
  `smtp_host` varchar(255) DEFAULT NULL,
  `smtp_email` varchar(255) DEFAULT NULL,
  `smtp_password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`email_setting_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `email_setting`
--

INSERT INTO `email_setting` (`email_setting_id`, `mailer`, `sendmail_path`, `smtp_port`, `smtp_host`, `smtp_email`, `smtp_password`) VALUES
(1, 'mail', '/usr/sbin/sendmail', '25', 'mail.groupfund.me', 'smtp@groupfund.me', 'smtp2123');

-- --------------------------------------------------------

--
-- Table structure for table `email_template`
--

CREATE TABLE IF NOT EXISTS `email_template` (
  `email_template_id` int(11) NOT NULL AUTO_INCREMENT,
  `task` varchar(255) DEFAULT NULL,
  `from_address` varchar(255) DEFAULT NULL,
  `reply_address` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `message` text,
  PRIMARY KEY (`email_template_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=60 ;

--
-- Dumping data for table `email_template`
--

INSERT INTO `email_template` (`email_template_id`, `task`, `from_address`, `reply_address`, `subject`, `message`) VALUES
(1, 'Welcome Email', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Welcome to {site_name}', '<p><strong><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2">{user_name}</span></strong>,{break}{break} <strong>{user_name}</strong> welcome to {site_name}.{break}{break} Congratulations!!! Your account has been activated, Now start exploring and enjoying our uninterrupted services. We wish you all success while being with us. {break} <span style="color:rgb(153, 153, 153); font-family:arial,helvetica,sans-serif; font-size:12px">Sincerely,</span>&nbsp;{break}<strong>{site_name} Team</strong></p>\n'),
(2, 'New User Join', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Sign up successfully on {site_name}', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2">{user_name}</span></strong>, {break}{break} Greetings from {site_name} Team, We welcome you to our portal on your successful sign-up with us. {break}{break} Your Email Address is {email} and Password is {password}. (Keep it confidential).{break} To activate your account and to start enjoying our valuable services, please click on the link given below:{break} {login_link}. {break}{break} <span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<span style="color: #333333;font-weight: bold;">{site_name} Team. </span></p>\n'),
(3, 'Forgot Password', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Forgot Password Request, {site_name}', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2">{user_name}</span></strong>, {break}Your request has been submitted to {site_name}.{break} Your Email Address {email} and Password is {password}.{break} Now you can take login from {login_link}. {break} <span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span>.</p>\n'),
(4, 'Admin User Active', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Your Account Activated, {site_name}', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2">{user_name}</span></strong>,{break}Your account has been activated by administrator.{break}Your Email Address {email} and Password is {password}.{break}Now you can take login from {login_link}.{break}&nbsp;<span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span></p>\n'),
(5, 'Admin User Deactivate', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Your Account Deactivated, {site_name}', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2">{user_name}</span></strong>,{break}{break}Your account has been deactivated by Administrator.{break}<span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span></p>\n'),
(6, 'Admin User Delete', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Your Account Delete, {site_name}', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2">{user_name}</span></strong>,{break}{break}Your account has been deleted by Administrator.{break}{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span></p>\n'),
(7, 'Contact Us', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'GroupFund :', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2">Administrator</span></strong>,{break}You have new inquiry by {name},{email}.{break} {message}.{break} <strong style="color: #333333;font-weight: bold;">Sincerely,</strong>{break}<strong style="color: #333333;font-weight: bold;">&nbsp;System Administrator.</strong></p>\n'),
(8, 'New Project Successful Alert', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'New Project Successfully submitted', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2">{user_name}</span></strong>,{break} You have successfully submitted New Project {project_name}.{break} You can see the project page from the following link : {break}{project_page_link}. {break}{break} <span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}{site_name}<span style="font-weight: bold;"> Team.</span></p>\n'),
(9, 'Admin Project Activate Alert', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Project activated by administrator, {site_name}', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello </span> <span style="color:#3bb0d2">{user_name}</span></strong>,{break}Project {project_name} has been approved by administrator.You can see the project page from the following link : {project_page_link}.{break}Please configure your paypal email address otherwise no one can give you a donation on this project.{break}<span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span></p>\n'),
(10, 'Admin Project Inctivate Alert', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Project deactivated by administrator, {site_name}', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2">{user_name}</span></strong>,{break}Project {project_name} has been declined by administrator.{break}<span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span></p>\n'),
(11, 'Admin Project Delete Alert', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Project deleted by administrator, {site_name}', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2">{user_name}</span></strong>,{break}Project {project_name} has been deleted by administrator.{break}&nbsp;<span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span></p>\n'),
(12, 'New Comment Admin Alert', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'New Comment posted on project, {site_name}', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2"> Administrator</span></strong>,{break}New comment posted by {comment_user_name} on Project {project_name}.{break}Comment : {comment}{break}Comment Profile Link : {comment_user_profile_link}{break}<span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span></p>\n'),
(13, 'New Comment Owner Alert', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'New Comment posted on your project, {site_name}', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2">{user_name}</span></strong>, {break}New comment posted by {comment_user_name} on Project {project_name}. {break} Comment : {comment} {break} Comment Profile Link : {comment_user_profile_link} {break} <span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span></p>\n'),
(14, 'New Comment Poster Alert', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'New Comment posted on project, {site_name}', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello </span> <span style="color:#3bb0d2">{user_name}</span></strong>, New comment posted by {comment_user_name} on Project {project_name}. {break} Comment : {comment} {break} Comment Profile Link : {comment_user_profile_link} {break} &nbsp;<span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span></p>\n'),
(15, 'New Fund Admin Notification', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'New Fund Added on project,GroupFund', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2"> Administrator</span></strong>,{break}New Fund({donote_amount}) added on the {project_name} {project_page_link} by {donor_name} {donor_profile_link}.{break}&nbsp;<span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span></p>\n'),
(16, 'New Fund Owner Notification', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'New Fund Added on project, {site_name}', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello </span> <span style="color:#3bb0d2">{user_name}</span></strong>,{break}New Fund({donote_amount}) added on the {project_name} {project_page_link} by {donor_name} {donor_profile_link}.{break}<b>Comment:</b>{comment}{break}<b>Shipping Detail:</b>{shipping}{break}{break}<span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span></p>\n'),
(17, 'New Fund Donor Notification', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'New Fund Added on project, {site_name}', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello </span> <span style="color:#3bb0d2">{donor_name}</span></strong>,{break}New Fund <b><i>({donote_amount})</i></b> added by you on the <b><i>{project_name}</i></b> {project_page_link}.{break}<span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span></p>\n'),
(18, 'New Project Draft Successful Alert', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Project saved in draft successfully', '   \n                                       	<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2"> {user_name}</span></strong>, {break}You have successfully Saved your project {project_name}, not submitted.{break} You can view your project page using the following link : {project_page_link}. <span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span> {break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span></p>\n                              \n                                       '),
(19, 'New Project Post Admin Alert', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'New Project Post on {site_name}', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello </span> <span style="color:#3bb0d2"> Administrator</span></strong>,{break}New Project {project_name} {project_page_link} is posted&nbsp;by User {user_name} {user_profile_link}{break}{break}&nbsp;<span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span></p>\n'),
(20, 'Project Finish Admin Alert', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Project Finish Alert', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello </span> <span style="color:#3bb0d2"> Administrator</span></strong>, {break}{break} Project : {project_name}{project_page_link}. {break}{break} Summary : {project_summary} {break}{break} Creator : {user_name}{user_profile_link} {break}{break} Goal : {project_goal}.{break}{break} Raise : {project_total_raise}.{break}{break} Backers : {break} {backer_list}{break}{break} <span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span></p>\n'),
(21, 'Project Finish Owner Alert', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Project Finish Alert', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2"> {user_name}</span></strong>, {break}{break} Project : {project_name}{project_page_link}. {break}{break} Summary : {project_summary} {break}{break} Creator : {user_name}{user_profile_link} {break}{break} Goal : {project_goal}.{break}{break} Raise : {project_total_raise}.{break}{break} Backers : {break} {backer_list}{break}{break} <span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span></p>\n'),
(22, 'Project Finish Donor Alert', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Project Finish Alert', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2"> {donor_name}</span></strong>, {break}{break} Project : {project_name}{project_page_link}. {break}{break} Summary : {project_summary} {break}{break} Creator : {user_name}{user_profile_link} {break}{break} Goal : {project_goal}.{break}{break} Raise : {project_total_raise}.{break}{break} Backers : {break} {own_backer}{break}{break} <span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span></p>\n'),
(23, 'Change Password', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Change Password Request, {site_name}', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2"> {user_name}</span></strong>,{break} Your request has been submitted to {site_name}.{break} Your Password is updated successfully. New login details are as below: {break} Email Address: {email} {break}Password: {password}.{break} {break} {break} <span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span></p>\n'),
(24, 'Donation Cancel User Notification', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Fund cancelled on Project', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2"> {user_name}</span></strong>,{break} Fund of ({donote_amount}) on the {project_name} {project_page_link} by {donor_name} {donor_profile_link} is cancelled. {break} <span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span></p>\n'),
(25, 'Donation Cancel Donor Notification', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Fund cancelled on Project', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello </span> <span style="color:#3bb0d2">{donor_name}</span></strong>,{break} Fund of ({donote_amount}) on the {project_name} {project_page_link} is cancelled. {break}&nbsp;<span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span></p>\n'),
(26, 'Donation Cancel Admin Notification', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Fund cancelled on Project', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2"> Administrator</span></strong>,{break} Fund of ({donote_amount}) on the {project_name} {project_page_link} by {donor_name} {donor_profile_link} is cancelled. {break}&nbsp;<span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<b>&nbsp;</b><span style="line-height: 1.6em;"><strong>System Administrator</strong>{break}</span><strong><span style="line-height: 1.6em;">Thank You.</span></strong></p>\n'),
(27, 'Project Cancelled Admin Notification', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Project cancelled on {site_name}', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2"> Administrator</span></strong>,{break} The {project_name} {project_page_link} is cancelled as its end date is reached and it is not successful. {break} <span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<strong><span style="line-height: 1.6em;">System Administrator{break}</span></strong><span style="line-height: 1.6em;"><strong>Thank You</strong>.</span></p>\n'),
(28, 'Project Cancelled User Notification', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Project cancelled on {site_name}', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2"> {user_name}</span></strong>,{break} The {project_name} {project_page_link} is cancelled as its end date is reached and it is not successful. {break} <span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}&nbsp;<strong style="color: #333333;">System Administrator.</strong></p>\n'),
(29, 'Project Successful Admin Notification', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Project successful on {site_name}', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2"> Administrator</span></strong>,{break} The {project_name} {project_page_link} is successful as its end date is reached and it has achieved the successful goal amount. {break} <span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span></p>\n'),
(30, 'Project Successful User Notification', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Project successful on {site_name}', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2"> {user_name}</span></strong>,{break} The {project_name} {project_page_link} is successful as its end date is reached and it has achieved the successful goal amount. {break} <span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<strong style="color:#333333">System Administrator</strong>.</p>\n'),
(31, 'Wallet Withdraw Request', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'New Wallet Withdraw Request', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2"> Administrator</span></strong>,{break} you have new withdraw request by {name}.{break} The details are as below : {break} {details}{break}{break} <span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<strong style="color: #333333;">System Administrator</strong>.</p>\n'),
(32, 'project new updates', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', '(project_name) project new updates', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2"> {user_name}</span></strong> {break} New updates from your donate project {break} updates:{project_update}{break} {break} <span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}​&nbsp;<strong style="color: #333333;">System Administrator.</strong></p>\n'),
(33, 'Email Invitation', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Join me on {site_name}', '<p><strong><span style="color:#39434d">Hi</span>,</strong>{break}<strong> <span style="color:#3bb0d2">{login_user_name}</span></strong> has been using {site_name}, a place to discover, donate and post or share campaigns, and wants you to join and {invitation_link}start funding{end_invitation_link}.{break}{break} {invitation_link}Accept Invite{end_invitation_link}{break}{break} {message}{break} &nbsp;<span style="color:rgb(153, 153, 153); font-family:arial,helvetica,sans-serif; font-size:12px">Sincerely,</span>&nbsp;{break}<strong>{site_name} Team.</strong></p>\n'),
(34, 'New Message(admin)', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'New Message', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2"> Administrator</span></strong>,New message from{message_user_name} on Project {project_name}.{break}Project Link : {project_link}{break}Content : {content} {break}Profile Link : {message_user_profile_link}{break}<span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span></p>\n'),
(35, 'New Message(user)', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'New Message', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2"> {user_name}</span></strong>,{break} New message from {message_user_name} on Project {project_name}. {break} Project Link : {project_link} {break} Content : {content} {break} Profile Link : {message_user_profile_link} {break} <span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span></p>\n'),
(36, 'Message user profile(Admin)', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'New Message', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello </span> <span style="color:#3bb0d2">{user_name}</span></strong>,{break} New message from {message_user_name} {break} Content : {content} {break} Profile Link : {message_user_profile_link} {break}&nbsp;<span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span></p>\n'),
(37, 'Message user profile(User)', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'New Message', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2"> {user_name}</span></strong>,{break} New message from {message_user_name} {break} Content : {content} {break} Profile Link : {message_user_profile_link} {break} <span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<span style="font-weight: bold;color: #333333">{site_name} Team.</span></p>\n'),
(38, 'Member Invitation', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'You are invite', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2"> {user_name}</span></strong>, {break} {project_owner_name} has invited You to be a team on project {url_project_title} and by clicking on this link you accept the invite and after signing up or logging In.. you become part of team on it and can edit some steps and help promote project{break} {break} Accept Your invitation by click here {login_link}. {break}{break} <span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span></p>\n'),
(39, 'Send Message To Team', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'New Comment for {user_name}', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span></strong> from<strong><span style="color:#3bb0d2;font-size: 15px;">{user_name}</strong> National Donation Service{break} {email} received a new comment. {user_name} said: &ldquo;{comment}&rdquo; To reply to their comment go to:{project_page_link} Cheers, The National Donation Service Team Reminder: Your email address is kept private on National Donation Service. Only administrators of campaigns you contribute to and users that you message directly have access to your email address.</p>\n'),
(54, 'Forgot Password Request', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Forgot Password Request, {site_name}', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello </span> <span style="color:#3bb0d2">{user_name}</span></strong>, {break}Your forgot password request has been submitted to {site_name}.{break} Click on this link to reset your Password : {login_link}. {break} <span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span></p>\n'),
(55, 'Email Verification', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Resend verification Request, {site_name}', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2"> {user_name}</span></strong>{break}, Your password has been submitted to {site_name}.{break} Click on this link to confirm your Password : {login_link}. {break} <span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span></p>\n'),
(56, 'Admin User Suspended', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Admin suspended user', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2"> {user_name}</span></strong>,{break}{email}.{break}Message : {message}.{break}{break} <span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<strong style="color: #333333;">System Administrator.</strong></p>\n'),
(57, 'User Follower', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Your follower', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello </span> <span style="color:#3bb0d2">{user_name}</span></strong>, {break}You are following by {follower_user_name}. {break} User Profile Link : {user_profile_link} {break}&nbsp;<span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span></p>\n'),
(58, 'Send message', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Send message', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2"> {user_name}</span></strong>, {break}New message posted by {message_user_name} on Date {dateadded}. {break}Subject: {subject} {break} Message: {message} {break} Message Profile Link : {message_user_profile_link} {break} <span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span></p>\n'),
(59, 'Update Profile', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Update Profile Request', '<p>Hello {user_name} {last_name} {break} You have successfully updated your profile.{break} Username : {user_name}{break} Lastname: {last_name}{break} Email: {email}{break} Password: {password} {break} Address: {address} {break} Zip&nbsp;code: {zip_code}{break} {break} System Administrator{break} Thank You.</p>\n');

-- --------------------------------------------------------

--
-- Table structure for table `facebook_setting`
--

CREATE TABLE IF NOT EXISTS `facebook_setting` (
  `facebook_setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `facebook_application_id` varchar(255) DEFAULT NULL,
  `facebook_login_enable` varchar(255) DEFAULT NULL,
  `facebook_access_token` varchar(500) DEFAULT NULL,
  `facebook_api_key` varchar(255) DEFAULT NULL,
  `facebook_user_id` varchar(255) DEFAULT NULL,
  `facebook_secret_key` varchar(255) DEFAULT NULL,
  `facebook_user_autopost` varchar(255) DEFAULT NULL,
  `facebook_wall_post` varchar(255) DEFAULT NULL,
  `facebook_url` text,
  `fb_img` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`facebook_setting_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `facebook_setting`
--

INSERT INTO `facebook_setting` (`facebook_setting_id`, `facebook_application_id`, `facebook_login_enable`, `facebook_access_token`, `facebook_api_key`, `facebook_user_id`, `facebook_secret_key`, `facebook_user_autopost`, `facebook_wall_post`, `facebook_url`, `fb_img`) VALUES
(1, '665394413522000', '1', 'BAAFcRSVO2ZAkBAGVZCxSxYVCZCOwhKOltilZCmlfZAwvagGGPN993WhIRBChlYGFUTwZBGSiyWfQnAveDRZBwp7y0iis9pxeKuembPqS9PQALZAqrcaYbu58hVYeGZB94kkkM3bvJPkjhZAbh5zXQ7IQMZAJjEKU8htTGEuyA12cyZApqKEoE3Q4sIJEUuaXX2XNjZCfKS9JJQ8dRYGOulpe6HUHWxSEoKfBWk7wZD', '665394413522000', '100004037024764', 'f5339e3b48d391178ed00254d8664ade', '0', '0', 'http://www.facebook.com/pages/fundraisingscript/187170521330689', '100004037024764.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE IF NOT EXISTS `faq` (
  `faq_id` int(11) NOT NULL AUTO_INCREMENT,
  `faq_category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `question` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `answer` text CHARACTER SET utf8,
  `faq_order` int(10) NOT NULL,
  `faq_home` int(10) NOT NULL DEFAULT '0',
  `active` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`faq_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=43 ;

--
-- Dumping data for table `faq`
--

INSERT INTO `faq` (`faq_id`, `faq_category_id`, `language_id`, `question`, `answer`, `faq_order`, `faq_home`, `active`, `date_added`) VALUES
(27, 12, 1, 'Testing purpose', '<p>forMost contributions are non-tax-deductible. However, some campaigns may choose to offer tax deductions on donations only if they are set up as a non-profit and only receive funds into their not-profit-profit PayPal account.</p>\n', 1, 0, '1', '2014-07-02 06:04:21'),
(26, 12, 1, 'How much does Indiegogoclone cost?', '<p>Joining Indiegogoclone is free however, there is a standard platform fee levied on any funds raised through the platform, which is 3.5% of funds raised if you meet your goal or 6.5% if you do not meet your goal.</p>\n', 2, 0, '1', '2014-07-02 06:04:50'),
(25, 12, 1, 'Do you recommend connecting with Facebook or Twitter?', '<p>Yes, we do! It&#39;s a simple way for anyone to log in to Indiegogoclone.</p>\n\n<p>&nbsp;</p>\n\n<p>If you&#39;re a campaigner, it&#39;s a great way to let backers know a little bit more about you. It&#39;s an easy way to show backers you&#39;re a real person. You may also edit the privacy settings of your Facebook or Twitter account to control what your guests see.</p>\n', 3, 0, '1', '2014-07-02 06:05:32'),
(24, 12, 1, 'Does Indiegogoclone ascertain a campaign or owner’s claim?', '<p>Indiegogoclone does not ascertain a campaign&rsquo;s or owner&rsquo;s claims. All claims and responsibilities of each project are its owner&rsquo;s. People decide a project&rsquo;s legitimacy or worthiness and decide whether they want to make a donation or not</p>\n', 5, 0, '1', '2014-07-02 06:07:54'),
(29, 12, 1, 'What is indiegogo clone?', '<p>Crowd funding site</p>\n', 3, 0, '1', '2014-07-02 06:06:33'),
(23, 12, 1, 'What is Groupfund?', '<p>Groupfund&nbsp;is a fundraising service platform that provides people who want to raise money an avenue to create, launch and broadcast their campaigns. tubestart is also used by people to find inspiring campaigns from across the world that they can align with.</p>\n', 4, 0, '1', '2014-07-02 06:07:26'),
(30, 12, 1, 'How FAQ works', '\n                                                <p>Here is the way FAQ WORKS</p>\n\n<p> </p>\n\n<p>The Brabham BT19 is a Formula One racing car designed by Ron Tauranac for the British Brabham team. The BT19 competed in the 1966 and 1967 Formula One World Championships and was used by Australian driver Jack Brabham to win his third World Championship in 1966. The BT19, which Brabham referred to as his "Old Nail", was the first car bearing its driver''s name to win a World Championship race. The car was initially conceived in 1965 for a 1.5-litre (92-cubic inch) Coventry Climax engine, but never raced in this form. For the 1966 season the Fédération Internationale de l''Automobile doubled the limit on engine capacity to 3 litres (183 cu in). Australian company Repco developed a new V8 engine for Brabham''s use in 1966, but a disagreement between Brabham and Tauranac over the latter''s role in the racing team left no time to develop a new car to handle it. Instead, the existing BT19 chassis was modified for the job. Only one BT19 was built. It was bought by Repco in 2004 and put on display in the National Sports Museum in Melbourne, Australia, in 2008. It is often demonstrated at motorsport events. (Full article...)</p>\n\n<p> </p>\n\n                                       ', 4, 0, '1', '2014-07-02 06:07:40'),
(34, 12, 2, 'Combien ne Indiegogoclone coût?', '<p>Rejoindre Indiegogoclone est libre cependant, il ya une taxe de plate-forme standard per&ccedil;u sur les fonds recueillis gr&acirc;ce &agrave; la plate-forme, qui est de 3,5% des fonds lev&eacute;s si vous atteindre votre objectif ou 6,5% si vous ne r&eacute;pondez pas &agrave; votre objectif.</p>\n', 2, 0, '1', '2014-07-02 06:14:39'),
(35, 12, 2, 'Recommandez-vous connectant avec Facebook ou Twitter?', '<p>Oui, nous le faisons! C&#39;est une fa&ccedil;on simple pour quiconque de se connecter &agrave; Indiegogoclone.&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>Si vous &ecirc;tes un militant, c&#39;est une excellente fa&ccedil;on de laisser les bailleurs savent un peu plus sur vous. C&#39;est un moyen facile de montrer les bailleurs que vous &ecirc;tes une personne r&eacute;elle. Vous pouvez &eacute;galement modifier les param&egrave;tres de votre compte Facebook ou Twitter confidentialit&eacute; de contr&ocirc;ler ce que vos clients voient.</p>\n', 3, 0, '1', '2014-07-02 06:16:18'),
(36, 12, 2, 'Quelle est INDIEGOGO clone?', '<p>Foule place de financement</p>\n', 3, 0, '1', '2014-07-02 06:17:50'),
(38, 12, 2, 'Quelle est Groupfund?', '<p>Groupfund est une plate-forme de services de collecte de fonds qui offre aux personnes qui veulent amasser des fonds pour cr&eacute;er une voie, le lancement et la diffusion de leurs campagnes. tubestart est &eacute;galement utilis&eacute; par les gens &agrave; trouver des campagnes inspirantes de partout dans le monde qu&#39;ils peuvent s&#39;aligner.</p>\n', 4, 0, '1', '2014-07-02 06:21:38'),
(39, 12, 2, 'Comment FAQ œuvres', '<p>Voici la fa&ccedil;on FAQ TRAVAUX&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>La Brabham BT19 est une voiture de Formule Un de course con&ccedil;u par Ron Tauranac pour l&#39;&eacute;quipe britannique Brabham. Le BT19 particip&eacute; &agrave; la 1966 et 1967 championnat de Formule One World et a &eacute;t&eacute; utilis&eacute; par le pilote Australien Jack Brabham pour gagner son troisi&egrave;me championnat du monde en 1966. L&#39;BT19, qui Brabham appel&eacute; son &quot;vieux clou&quot;, a &eacute;t&eacute; la premi&egrave;re voiture portant son conducteur nommer &agrave; remporter une course du Championnat du Monde. La voiture a &eacute;t&eacute; initialement con&ccedil;u en 1965 pour un 1,5 litre (92 pouces-cube) moteur Coventry Climax, mais n&#39;a jamais couru sous cette forme. Pour la saison 1966, la F&eacute;d&eacute;ration Internationale de l&#39;Automobile a doubl&eacute; la limite de la capacit&eacute; du moteur de 3 litres (183 cu in). Soci&eacute;t&eacute; australienne Repco a d&eacute;velopp&eacute; un nouveau moteur V8 pour l&#39;utilisation de Brabham en 1966, mais un d&eacute;saccord entre Brabham et Tauranac sur le r&ocirc;le de l&#39;&eacute;quipe de course de celui-ci laissa pas le temps de d&eacute;velopper une nouvelle voiture pour y faire face. Au lieu de cela, le ch&acirc;ssis BT19 existant a &eacute;t&eacute; modifi&eacute; pour le travail. Un seul BT19 a &eacute;t&eacute; construit. Il a &eacute;t&eacute; achet&eacute; par Repco en 2004 et mis en exposition au Mus&eacute;e National du Sport &agrave; Melbourne, en Australie, en 2008. Elle est souvent d&eacute;montr&eacute; lors d&#39;&eacute;v&eacute;nements de sport automobile. (Article complet ...)</p>\n', 4, 0, '1', '2014-07-02 06:56:48'),
(40, 12, 2, 'Ne Indiegogoclone vérifier une campagne ou la demande du propriétaire?', '<p>Indiegogoclone does not ascertain a campaign&rsquo;s or owner&rsquo;s claims. All claims and responsibilities of each project are its owner&rsquo;s. People decide a project&rsquo;s legitimacy or worthiness and decide whether they want to make a donation or not</p>\n', 5, 0, '1', '2014-07-02 06:58:22');

-- --------------------------------------------------------

--
-- Table structure for table `faq_category`
--

CREATE TABLE IF NOT EXISTS `faq_category` (
  `faq_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `faq_category_name` varchar(255) DEFAULT NULL,
  `faq_category_url_name` varchar(255) DEFAULT NULL,
  `faq_category_order` int(10) NOT NULL,
  `faq_category_home` int(10) NOT NULL DEFAULT '0',
  `active` varchar(255) DEFAULT NULL,
  `language_id` int(11) NOT NULL,
  PRIMARY KEY (`faq_category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `faq_category`
--

INSERT INTO `faq_category` (`faq_category_id`, `parent_id`, `faq_category_name`, `faq_category_url_name`, `faq_category_order`, `faq_category_home`, `active`, `language_id`) VALUES
(3, 0, 'FundraisingScript Basics', 'fundraisingscript-basics', 1, 1, '1', 0),
(4, 0, 'Creating a Project', 'creating-a-project', 2, 1, '1', 0),
(5, 0, 'Backing a Project', 'backing-a-project', 3, 1, '1', 0),
(6, 3, 'HOW IT WORKS', 'how-it-works', 1, 0, '1', 0),
(7, 3, 'ACCOUNT SETTINGS', 'account-settings', 2, 0, '1', 0),
(8, 3, 'SITE BASICS', 'site-basics', 3, 0, '1', 0),
(9, 0, 'tst', 'tst', 0, 1, '1', 0),
(10, 0, 'tst', 'tst1', 0, 0, '1', 0),
(11, 10, 'indiegogoclone_faq', 'indiegogoclone_faq', 56, 0, '1', 0),
(12, 10, 'Indiegogoclone_faq_question', 'Indiegogoclone_faq_question', 3, 0, '1', 0),
(13, 12, 'testing purpose faq category111', 'testing-purpose-faq-category111', 1, 0, '1', 0);

-- --------------------------------------------------------

--
-- Table structure for table `featured_sponsors`
--

CREATE TABLE IF NOT EXISTS `featured_sponsors` (
  `featured_sponsors_id` int(11) NOT NULL AUTO_INCREMENT,
  `featured_sponsors_title` varchar(255) NOT NULL,
  `featured_sponsors_image` varchar(255) NOT NULL,
  `active` varchar(255) NOT NULL,
  PRIMARY KEY (`featured_sponsors_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `featured_sponsors`
--

INSERT INTO `featured_sponsors` (`featured_sponsors_id`, `featured_sponsors_title`, `featured_sponsors_image`, `active`) VALUES
(9, 'wetwe', 'feature_43191.png', '1');

-- --------------------------------------------------------

--
-- Table structure for table `file_gallery`
--

CREATE TABLE IF NOT EXISTS `file_gallery` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_path` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_desc` text COLLATE utf8_unicode_ci,
  `project_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=17 ;

--
-- Dumping data for table `file_gallery`
--

INSERT INTO `file_gallery` (`id`, `file_name`, `file_path`, `file_desc`, `project_id`) VALUES
(1, 'latest technology', 'kog-tech-chem-1a-sample.pdf', '0', 5),
(2, 'Political parties, campaigning & donations', '10-817-trade-union-political-funds-guide.pdf', '0', 4),
(3, 'community', 'community.pdf', '0', 3),
(4, 'MOTOR VEHICLE ACCIDENT ', 'mcl-Act-198-of-1965.pdf', '0', 2),
(5, 'FINANCING POLITICAL PARTIES', '1883_ghcddpolicyguidelines.pdf', '0', 1),
(6, 'school charity', 'mcl-Act-198-of-19651.pdf', '0', 6),
(7, 'funding', '1883_ghcddpolicyguidelines1.pdf', '0', 8),
(8, 'distribution arrangement', '1883_ghcddpolicyguidelines2.pdf', '0', 9),
(9, 'Mastered', '1883_ghcddpolicyguidelines3.pdf', '0', 10),
(10, 'latest technology', '1883_ghcddpolicyguidelines4.pdf', '0', 11),
(11, '3d technology', '1883_ghcddpolicyguidelines5.pdf', '0', 13),
(12, 'funding ...', 'mcl-Act-198-of-19652.pdf', '0', 14),
(13, '3d technology', 'mcl-Act-198-of-19653.pdf', '0', 15),
(14, 'funding ...', 'mcl-Act-198-of-19654.pdf', '0', 19),
(15, 'spots', '1883_ghcddpolicyguidelines6.pdf', '0', 20),
(16, '3d effects', '3D-Printing-Technology.pdf', '0', 21);

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE IF NOT EXISTS `gallery` (
  `gallery_id` int(50) NOT NULL AUTO_INCREMENT,
  `gallery_name` varchar(255) DEFAULT NULL,
  `gallery_image` varchar(255) DEFAULT NULL,
  `active` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`gallery_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `gateways_details`
--

CREATE TABLE IF NOT EXISTS `gateways_details` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `payment_gateway_id` int(50) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `value` varchar(200) DEFAULT NULL,
  `label` varchar(200) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=48 ;

--
-- Dumping data for table `gateways_details`
--

INSERT INTO `gateways_details` (`id`, `payment_gateway_id`, `name`, `value`, `label`, `description`) VALUES
(4, 1, 'site_status', 'sandbox', 'Site Status', 'For Test Account : sandbox\n\nFor Live Account : live'),
(6, 1, 'paypal_email', 'jayshree.rockersinfo@gmail.com', 'Paypal Email', 'Type your Paypal buisness Email Account.'),
(13, 3, 'site_status', 'sandbox', 'Site Status', 'For Test Account : sandbox , For Live Account : live'),
(14, 3, 'variable_fees', '5', 'Variable Fees', 'Type your  Amazon Variable Fees.'),
(15, 3, 'fixed_fees', '5', 'Fixed Fees', 'Type Your Amazon Fixed Fees.'),
(22, 3, 'amazon_email', 'rakesh007_patel@yahoo.co.in', 'Amazon Email', 'Type Your Amazon Email Id'),
(27, 4, 'x_login', '9R6J2pbY', 'x_login', 'The merchant unique API Login ID'),
(28, 4, 'x_tran_key', '7sbQz792J8YNfR5T', 'x_tran_key', 'The merchant unique Transaction Key'),
(29, 4, 'x_type', 'AUTH_CAPTURE', 'x_type', 'The type of credit card transaction\n'),
(30, 4, 'x_method', 'CC', 'x_method', 'The payment method'),
(31, 4, 'x_description', 'Sample Transaction', 'x_description', 'The transaction description'),
(34, 3, 'aws_access_key_id', 'AKIAJHRGJTWJ6TZAYFRA', 'Amazon Access Key Id', 'Type Your Amazon aws_access_key_id.'),
(35, 3, 'aws_secret_access_key', 'mGEiblOo7xnZYOPlWHnmoo3/p5MRhicQBFm4CZ/X', 'Amazon Secret Access Key', 'Type your aws_secret_access_key.'),
(37, 1, 'paypal_username', 'jayshree.rockersinfo_api1.gmail.com', 'paypal_username', 'Type Your paypal Username'),
(38, 1, 'paypal_password', '1321252087', 'paypal_password', 'Type Your Paypal Password.'),
(39, 1, 'paypal_signature', 'AAQNP4IUOwIbkYjIuH2o4oHIZiVzAXXPY8ZxlXELGuNXh541QLWnI56m', 'paypal_signature', 'Type Your Paypal Signature'),
(40, 4, 'site_status', 'sandbox', 'Site Status', 'Enter Site Status'),
(41, 1, 'gateway_fees', '4', 'Gateway Fees', 'its in percentage.its transsactin fees taken by paypal,generally its 4%'),
(42, 4, 'gateway_fees', '4', 'Gateway Fees', 'its in percentage.its transsactin fees taken by paypal,generally its 4%'),
(43, 5, 'client_id', '98605', 'Client ID', 'Client ID ,You can get from wepay account '),
(44, 5, 'client_secret', 'ba1fc09f2e', 'Client Secret', 'Client Secret ,You can get from wepay account '),
(45, 5, 'access_token', 'STAGE_af5e4fe1c4db92a83d7ffb31b3c6c40b2a45544d911d813c85ba1d5af5882e27', 'Access Token', 'Access Token ,You can get from wepay account'),
(46, 5, 'account_id', '2111334680', 'Account Id', 'Account Id ,You can get from wepay account'),
(47, 5, 'site_status', 'stage', 'Site Status', 'Site Status ,stage => for test ,live=>live ');

-- --------------------------------------------------------

--
-- Table structure for table `gateways_detailsss`
--

CREATE TABLE IF NOT EXISTS `gateways_detailsss` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `payment_gateway_id` int(50) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `value` varchar(200) DEFAULT NULL,
  `label` varchar(200) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=41 ;

--
-- Dumping data for table `gateways_detailsss`
--

INSERT INTO `gateways_detailsss` (`id`, `payment_gateway_id`, `name`, `value`, `label`, `description`) VALUES
(13, 3, 'site_status', 'sandbox', 'Site Status', 'For Test Account : sandbox , For Live Account : live'),
(14, 3, 'variable_fees', '5', 'Variable Fees', 'Type your  Amazon Variable Fees.'),
(15, 3, 'fixed_fees', '5', 'Fixed Fees', 'Type Your Amazon Fixed Fees.'),
(22, 3, 'amazon_email', 'rakesh007_patel@yahoo.co.in', 'Amazon Email', 'Type Your Amazon Email Id'),
(27, 4, 'x_login', '75sqQ96qHEP8', 'x_login', 'The merchant unique API Login ID'),
(28, 4, 'x_tran_key', '7r83Sb4HUd58Tz5p', 'x_tran_key', 'The merchant unique Transaction Key'),
(29, 4, 'x_type', 'AUTH_CAPTURE', 'x_type', 'The type of credit card transaction\n'),
(30, 4, 'x_method', 'CC', 'x_method', 'The payment method'),
(31, 4, 'x_description', 'Sample Transaction', 'x_description', 'The transaction description'),
(34, 3, 'aws_access_key_id', 'AKIAJHRGJTWJ6TZAYFRA', 'Amazon Access Key Id', 'Type Your Amazon aws_access_key_id.'),
(35, 3, 'aws_secret_access_key', 'mGEiblOo7xnZYOPlWHnmoo3/p5MRhicQBFm4CZ/X', 'Amazon Secret Access Key', 'Type your aws_secret_access_key.'),
(40, 4, 'site_status', 'sandbox', 'Site Status', 'Enter Site Status');

-- --------------------------------------------------------

--
-- Table structure for table `google_plus_setting`
--

CREATE TABLE IF NOT EXISTS `google_plus_setting` (
  `google_plus_setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `google_plus_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `google_plus_enable` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`google_plus_setting_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `google_plus_setting`
--

INSERT INTO `google_plus_setting` (`google_plus_setting_id`, `google_plus_link`, `google_plus_enable`) VALUES
(1, 'https://www.plus.google.com', '1');

-- --------------------------------------------------------

--
-- Table structure for table `google_setting`
--

CREATE TABLE IF NOT EXISTS `google_setting` (
  `google_setting_id` int(50) NOT NULL AUTO_INCREMENT,
  `consumer_key` varchar(255) DEFAULT NULL,
  `consumer_secret` varchar(255) DEFAULT NULL,
  `google_enable` int(50) NOT NULL DEFAULT '1',
  `google_url` text NOT NULL,
  PRIMARY KEY (`google_setting_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `google_setting`
--

INSERT INTO `google_setting` (`google_setting_id`, `consumer_key`, `consumer_secret`, `google_enable`, `google_url`) VALUES
(1, '6730636684-c79pe7i4rv5guifck3p16nkitq83p0d6.apps.googleusercontent.com', 'VTUjNxg7wawX518t6jnZiGni', 1, 'https://www.google.co.in/');

-- --------------------------------------------------------

--
-- Table structure for table `guidelines`
--

CREATE TABLE IF NOT EXISTS `guidelines` (
  `guidelines_id` int(10) NOT NULL AUTO_INCREMENT,
  `guidelines_title` varchar(255) DEFAULT NULL,
  `guidelines_content` longtext,
  `guidelines_meta_title` text,
  `guidelines_meta_keyword` text,
  `guidelines_meta_description` text,
  PRIMARY KEY (`guidelines_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `guidelines`
--

INSERT INTO `guidelines` (`guidelines_id`, `guidelines_title`, `guidelines_content`, `guidelines_meta_title`, `guidelines_meta_keyword`, `guidelines_meta_description`) VALUES
(1, 'Guidelines', '<p><strong>Project Guidelines</strong></p>\r\n<ol start=KSYDOU1KSYDOU>\r\n<li>FundraisingScript is a funding platform focused on a broad spectrum of creative projects. The guidelines below articulate our mission and focus. Please note that any project that violates these guidelines will be declined or removed. Please contact us if you have any questions.</li>\r\n<li><strong>Projects. Projects. Projects.</strong>&nbsp;FundraisingScript is for the funding of projects – albums, films, specific works – that have clearly defined goals and expectations.</li>\r\n<li><strong>Projects with a creative purpose.</strong>&nbsp;FundraisingScript can be used to fund projects from the creative fields of Art, Comics, Dance, Design, Fashion, Film, Food, Games, Music, Photography, Publishing, Technology, and Theater. We currently only support projects from these categories.</li>\r\n<li><strong>No charity or cause funding.</strong>&nbsp;Examples of prohibited use include raising money for the Red Cross, funding an awareness campaign, funding a scholarship, or donating a portion of funds raised on FundraisingScript to a charity or cause.</li>\r\n<li><strong>No KSYDOUfund my lifeKSYDOU projects.</strong>&nbsp;Examples include projects to pay tuition or bills, go on vacation, or buy a new camera.</li>\r\n<li><strong>Rewards, not financial incentives.</strong>&nbsp;The FundraisingScript economy is based on the offering of rewards – copies of the work, limited editions, fun experiences. Offering financial incentives, such as ownership, financial returns (for example, a share of profits), or repayment (loans) is prohibited.</li>\r\n</ol>\r\n<div  noshade=KSYDOUnoshadeKSYDOU width=KSYDOU533KSYDOU \r\n<p><strong>Community Guidelines</strong></p>\r\n<ol start=KSYDOU1KSYDOU>\r\n<li>We rely on respectful interactions to ensure that FundraisingScript is a friendly place. Please follow the rules below.</li>\r\n<li><strong>Spread the word but donKSYSINGt spam.</strong>&nbsp;Spam includes sending unsolicited @ messages to people on Twitter. This makes everyone on FundraisingScript look bad. DonKSYSINGt do it.</li>\r\n<li><strong>DonKSYSINGt promote a project on other projectsKSYSING pages.</strong>&nbsp;Your comments will be deleted and your account may be suspended.</li>\r\n<li><strong>Be courteous and respectful.</strong>&nbsp;DonKSYSINGt harass or abuse other members.</li>\r\n<li><strong>DonKSYSINGt post obscene, hateful, or objectionable content.</strong>&nbsp;If you do we will remove it and suspend you.</li>\r\n<li><strong>DonKSYSINGt post copyrighted content without permission.</strong>&nbsp;Only post content that you have the rights to.</li>\r\n<li><strong>If you donKSYSINGt like a project, donKSYSINGt back it.</strong>&nbsp;No need to be a jerk.</li>\r\n<li>Actions that violate these rules or our&nbsp;<a href=KSYDOUhttp://spicyfund.com/fund_demo/home/content/terms-and-conditions/12KSYDOU>Terms of Use</a>&nbsp;may lead to an account being suspended or deleted. WeKSYSINGd prefer not to do that, so be cool, okay? Okay.</li>\r\n</ol>\r\n  ', 'Guidelines-FundraisingScript', 'Guidelines-FundraisingScript', 'Guidelines-FundraisingScript');

-- --------------------------------------------------------

--
-- Table structure for table `home_page`
--

CREATE TABLE IF NOT EXISTS `home_page` (
  `home_id` int(11) NOT NULL AUTO_INCREMENT,
  `home_title` varchar(255) DEFAULT NULL,
  `home_description` text,
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`home_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `home_page`
--

INSERT INTO `home_page` (`home_id`, `home_title`, `home_description`, `active`) VALUES
(1, 'Why Us?', '<p style="font-size: 11px;"><a style="color: rgb(0, 0, 0); font-weight: bold; text-decoration: none;" href="http://www.rockersinfo.com">Rockers Technologies</a> has already created the solution for fundraising. Anyone intends to raise funds for any cause can buy this solution from us to see rocketing change in incoming funds. One can directly start own website with this solution. More than 2 years of experience in this segment and pool of experienced and skillful developers help us to bring changes in the fundraising script in case client needs some updates. We understand that each client has different objectives so we offer customized solution. You tell us your objectives, and we will come up with modified script that suits your requirements.aaaa????????????</p>\n<p style="font-size: 18px; font-weight: bold;">We will make a fundraising website for you</p>\n<p style="font-size: 11px;">We are here to give wings to your fundraising ideas. We?ll make fundraising website for you. We already have ready to use clone of Crowd Funding website like kickstart ,gofundme , firstgiving and indiegogo and we will promise you to provide dedicated support and industry standard quality.</p>', 1);

-- --------------------------------------------------------

--
-- Table structure for table `idea`
--

CREATE TABLE IF NOT EXISTS `idea` (
  `idea_id` int(50) NOT NULL AUTO_INCREMENT,
  `idea_name` varchar(255) DEFAULT NULL,
  `idea_image` varchar(255) DEFAULT NULL,
  `idea_description` text,
  `active` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`idea_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `idea`
--

INSERT INTO `idea` (`idea_id`, `idea_name`, `idea_image`, `idea_description`, `active`) VALUES
(2, 'Medical Bills', '1309861110_kcmdrkonqi.jpg', 'Raise money online to help friends cover medical bills\nand expenses in the event of an accident or illness.', '1'),
(3, 'Weddings And Honeymoons', '2.jpg', 'Raise money online for your dream wedding, perfect\nhoneymoon, cash registry or bridal shower', '1'),
(4, 'Special Occasions', '1309861094_Gift_Box_(Gold).jpg', 'Raise money online for group gift purchases, birthdays,\nanniversaries, bachelor parties and celebrations!', '1');

-- --------------------------------------------------------

--
-- Table structure for table `image_setting`
--

CREATE TABLE IF NOT EXISTS `image_setting` (
  `image_setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `p_small_width` int(11) NOT NULL,
  `p_small_height` int(11) NOT NULL,
  `u_s_width` int(11) NOT NULL,
  `u_s_height` int(11) NOT NULL,
  `u_b_width` int(11) NOT NULL,
  `u_b_height` int(11) NOT NULL,
  `u_m_width` int(255) NOT NULL,
  `u_m_height` int(255) NOT NULL,
  `p_medium_width` int(11) NOT NULL,
  `p_medium_height` int(11) NOT NULL,
  `p_ratio` int(11) NOT NULL,
  `u_ratio` int(11) NOT NULL,
  `g_ratio` int(11) NOT NULL,
  `p_thumb_width` varchar(255) NOT NULL,
  `p_thumb_height` varchar(255) NOT NULL,
  `p_large_width` int(5) NOT NULL,
  `p_large_height` int(5) NOT NULL,
  PRIMARY KEY (`image_setting_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `image_setting`
--

INSERT INTO `image_setting` (`image_setting_id`, `p_small_width`, `p_small_height`, `u_s_width`, `u_s_height`, `u_b_width`, `u_b_height`, `u_m_width`, `u_m_height`, `p_medium_width`, `p_medium_height`, `p_ratio`, `u_ratio`, `g_ratio`, `p_thumb_width`, `p_thumb_height`, `p_large_width`, `p_large_height`) VALUES
(1, 124, 64, 50, 50, 150, 150, 70, 70, 1200, 500, 0, 0, 0, '214', '168', 1000, 400);

-- --------------------------------------------------------

--
-- Table structure for table `invite_members`
--

CREATE TABLE IF NOT EXISTS `invite_members` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `project_id` varchar(50) DEFAULT '0',
  `user_id` int(10) DEFAULT '0',
  `admin` tinyint(2) DEFAULT '0',
  `email` varchar(100) DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  `invite_user_id` int(50) NOT NULL,
  `member_role` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=240 ;

--
-- Dumping data for table `invite_members`
--

INSERT INTO `invite_members` (`id`, `project_id`, `user_id`, `admin`, `email`, `code`, `status`, `invite_user_id`, `member_role`) VALUES
(22, '38', 3, 1, 'ankit.rockersinfo@gmail.com', '261093647', 0, 0, ''),
(26, '37', 2, 1, 'ankit.rockersinfo@gmail.com', '', 1, 36, ''),
(27, '39', 3, 1, 'ankit.rockersinfo@gmail.com', '', 1, 36, ''),
(29, '49', 2, 1, 'Ankit.rockersinfo@gmail.com', '379107516', 0, 0, ''),
(34, '37', 2, 1, 'mihir.rockersinfo@gmail.com', '', 1, 3, ''),
(35, '49', 2, 1, 'mihir.rockersinfo@gmail.com', '', 1, 3, ''),
(36, '67', 3, 1, 'mihir.test.rockersinfo@gmail.com', '', 1, 2, ''),
(37, '67', 3, 1, 'asdfsd@fds.dff', '419410809', 0, 0, ''),
(41, '89', 3, 1, 'hardik.rockersinfo@gmail.com', '', 1, 8, ''),
(54, '33', 3, 1, 'test051@gmai.com', '213401478', 0, 0, ''),
(57, '89', 3, 1, 'ankit.rockersinfo@gmail.com', '', 1, 36, ''),
(58, '109', 3, 1, 'hardik.rockersinfo@gmail.com', '', 1, 8, ''),
(60, '157', 6, 0, 'chintan.rockersinfo@gmail.com', '335424481', 0, 0, ''),
(65, '117', 3, 1, 'hardik.rockersinfo@gmail.com', '874570960', 0, 0, ''),
(74, '117', 3, 1, 'ankit.rockersinfo@gmail.com', '71293795', 0, 0, ''),
(77, '176', 6, 1, 'ankit.rockersinfo@gmail.com', '380652289', 0, 0, ''),
(82, '177', 6, 1, 'rahul.rockersinfo@gmail.com', '39228958', 0, 0, ''),
(83, '177', 6, 1, 'kaumil.test01.rockersinfo@gmail.com', '', 1, 112, ''),
(85, '184', 119, 1, 'a@g.com', '199431611', 0, 0, ''),
(86, '190', 120, 0, 'qa.rockersinfo@gmail.com', '228091021', 0, 0, ''),
(88, '189', 6, 0, 'kaumil.test00.rockersinfo@gmail.com', '555711000', 0, 0, ''),
(89, '190', 120, 1, 'chintan.test.rockersinfo@gmail.com', '', 1, 98, 'Helper test'),
(91, '189', 6, 1, 'chintan.rockersinfo@gmail.com', '', 1, 120, 'Photo'),
(92, '183', 6, 1, 'chintan.rockersinfo@gmail.com', '', 1, 120, ''),
(94, '193', 6, 1, 'kaumil.test00.rockersinfo@gmail.com', '', 1, 121, ''),
(95, '201', 124, 0, 'chintan.qa.rockersinfo@gmail.com', '830435642', 0, 0, ''),
(97, '214', 127, 0, 'pradyumna.rockersinfo@gmail.com', '', 1, 126, ''),
(98, '194', 3, 0, 'rahul.rockersinfo@gmail.com', '389244351', 0, 0, ''),
(107, '220', 127, 0, 'chintan.test02.rockersinfo@gmail.com', '', 1, 154, ''),
(108, '288', 127, 1, 'akash.rockersinfo@gmail.com', '951964209', 0, 0, ''),
(109, '285', 184, 1, 'jayshree.rockersinfo@gmail.com', '648849596', 0, 0, ''),
(111, '306', 127, 0, 'jayshree.test.rockersinfo@gmail.com', '937762658', 0, 0, ''),
(113, '309', 216, 1, 'jayshree.rockersinfo@gmail.com', '', 1, 127, 'sub admin'),
(115, '311', 216, 1, 'jayshree.rockersinfo@gmail.com', '', 1, 127, ''),
(116, '317', 127, 1, 'ankit.rockersinfo@gmail.com', '64573690', 0, 0, ''),
(118, '322', 127, 1, 'jayshree.test.rockersinfo@gmail.com', '347904634', 0, 0, ''),
(119, '311', 216, 1, 'jayshree.rockersinfo@gmail.com', '', 1, 127, 'sub admin'),
(120, '330', 127, 1, 'jayshreepatel04@yahoo.com', '722791036', 0, 0, ''),
(133, '336', 127, 1, 'jayshree.test01.rockersinfo@gmail.com', '', 1, 232, 'Sub admin'),
(134, '337', 127, 0, 'jayshree.test01.rockersinfo@gmail.com', '', 1, 232, ''),
(135, '338', 216, 0, 'jayshree.rockersinfo@gmail.com', '', 1, 127, ''),
(136, '359', 127, 1, 'jayshreepatel04@yahoo.com', '426888836', 0, 0, ''),
(137, '387', 216, 0, 'rahul.rockersinfo@gmail.com', '463761419', 0, 0, ''),
(138, '388', 127, 1, 'jayshreepatel04@yahoo.com', '501459529', 0, 0, ''),
(139, '360', 216, 0, 'rahul@gmail.com', '113960581', 0, 0, ''),
(140, '370', 219, 1, 'jayshree.rockersinfo@gmail.com', '', 1, 127, ''),
(141, '394', 127, 1, 'Jayshreepatel04@yahoo.com', '147041408', 0, 0, ''),
(144, '415', 82, 1, 'jayshree.rockersinfo@gmail.com', '', 1, 127, ''),
(145, '414', 127, 1, 'jayshree.test.rockersinfo@gmail.com', '873006612', 0, 0, ''),
(146, '412', 127, 0, 'jayshreepatel04@yahoo.com', '', 1, 216, ''),
(147, '412', 127, 0, 'jayshree.test.rockersinfo@gmail.com', '', 1, 237, ''),
(149, '417', 127, 0, 'jayshreepatel04@yahoo.com', '', 1, 216, ''),
(150, '418', 216, 0, 'jayshree.rockersinfo@gmail.com', '', 1, 127, ''),
(160, '27', 8, 0, 'ritu.rockersinfo@gmail.com', '', 1, 7, ''),
(165, '3', 1, 1, 'jayshree.test01.rockersinfo@gmail.com', '', 1, 6, ''),
(167, '3', 1, 1, 'jayshree.test.rockersinfo@gmail.com', '', 1, 2, 'quality analyst'),
(168, '12', 1, 1, 'priya.rockersinfo@gmail.com', '841875683', 0, 0, ''),
(174, '16', 1, 1, 'jayshree.test@hotmail.com', '498794453', 0, 0, ''),
(175, '17', 1, 1, 'jayshree.test.rockersinfo@gmail.com', '28494521', 0, 0, ''),
(176, '19', 1, 1, 'rakesh007_patel@yahoo.co.in', '', 1, 18, ''),
(178, '22', 1, 1, 'jayshree.test.rockersinfo@gmail.com', '', 1, 2, ''),
(190, '28', 1, 1, 'jayshree.test.rockersinfo@gmail.com', '', 1, 2, 'Admin1'),
(191, '28', 1, 0, 'jayshree.test01.rockersinfo@gmail.com', '', 1, 6, 'Admin2'),
(198, '28', 1, 0, 'rockersinfo@gmail.com', '656031568', 0, 0, ''),
(200, '28', 1, 0, 'rockersinfo123@gmail.com', '463173929', 0, 0, ''),
(202, '28', 1, 0, 'tstt@gmail.com', '14759158', 0, 0, ''),
(206, '31', 1, 1, 'jayshree.test.rockersinfo@gmail.com', '', 1, 2, ''),
(221, '89', 10, 0, 'rockersinfo1@gmail.com', '225995091', 0, 0, ''),
(223, '89', 10, 0, 'rockersinfo@gmail.com', '995010664', 0, 0, ''),
(231, '91', 1, 0, 'vadgama.rockersinfo@gmail.com', '', 1, 36, ''),
(237, '92', 1, 1, 'jayshree.test.rockersinfo@gmail.com', '735655938', 0, 0, ''),
(238, '122', 1, 0, 'bhavana.rockersinfo@gmail.com', '969423783', 0, 0, ''),
(239, '159', 1, 1, 'jayshree.test.rockersinfo@gmail.com', '223017433', 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `invite_request`
--

CREATE TABLE IF NOT EXISTS `invite_request` (
  `request_id` int(100) NOT NULL AUTO_INCREMENT,
  `invite_code` text,
  `invite_email` varchar(255) DEFAULT NULL,
  `invite_date` datetime NOT NULL,
  `invite_ip` varchar(255) DEFAULT NULL,
  `invite_by` int(100) NOT NULL,
  PRIMARY KEY (`request_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=66 ;

--
-- Dumping data for table `invite_request`
--

INSERT INTO `invite_request` (`request_id`, `invite_code`, `invite_email`, `invite_date`, `invite_ip`, `invite_by`) VALUES
(1, 'PPeNAJb8XdB4', 'ankit.rockersinfo@gmail.com', '2013-08-31 15:32:56', '1.22.81.192', 23),
(2, 'e5SgMNMkYYb7', 'mihir.rockersinfo@gmail.com', '2013-09-02 17:02:41', '1.22.82.189', 91),
(3, 'CsW3LQPuJ83u', 'ankit.rockersinfo@gmail.com', '2013-09-10 10:15:15', '1.22.83.211', 3),
(4, 'JKD4ALyy9Wkz', 'rahul.rockersinfo@gmail.com', '2013-09-10 14:35:54', '210.89.56.198', 2),
(5, 'JKD4ALyy9Wkz', 'mihir.test5.rockersinfo@gmail.com', '2013-09-10 14:36:59', '210.89.56.198', 2),
(6, 'JKD4ALyy9Wkz', 'mihir.test2.rockersinfo@gmail.com', '2013-09-10 14:37:02', '210.89.56.198', 2),
(7, 'JKD4ALyy9Wkz', 'mihir.test4.rockersinfo@gmail.com', '2013-09-10 14:37:04', '210.89.56.198', 2),
(8, 'JKD4ALyy9Wkz', 'mihir.test3.rockersinfo@gmail.com', '2013-09-10 14:37:06', '210.89.56.198', 2),
(9, 'JKD4ALyy9Wkz', 'abcd@gmail.com', '2013-09-10 14:39:48', '210.89.56.198', 2),
(10, 'JKD4ALyy9Wkz', 'abcd1@gmail.com', '2013-09-10 14:39:51', '210.89.56.198', 2),
(11, 'JKD4ALyy9Wkz', 'abcd2@gmail.com', '2013-09-10 14:39:54', '210.89.56.198', 2),
(12, 'JKD4ALyy9Wkz', 'abcd3@gmail.com', '2013-09-10 14:39:56', '210.89.56.198', 2),
(13, 'CsW3LQPuJ83u', 'mihir.test1.rockersinfo@gmail.com', '2013-09-10 15:45:30', '1.22.83.211', 3),
(14, 'BYwgBc9AjNR2', 'kaumil.rockersinfo@gmail.com', '2013-09-10 17:48:00', '1.22.83.211', 6),
(15, 'BYwgBc9AjNR2', 'vijay.rockersinfo@gmail.com', '2013-09-10 17:48:03', '1.22.83.211', 6),
(16, 'BYwgBc9AjNR2', 'viral.rockersinfo@gmail.com', '2013-09-10 17:48:05', '1.22.83.211', 6),
(17, 'BYwgBc9AjNR2', 'hardik.rockersinfo@gmail.com', '2013-09-10 17:48:08', '1.22.83.211', 6),
(18, 'BYwgBc9AjNR2', 'kaumil.rockersinfo@gmail.com', '2013-09-10 17:56:54', '1.22.83.211', 6),
(21, 'CsW3LQPuJ83u', 'kaumil.test01.rockersinfo@gmail.com', '2014-01-07 02:37:59', '1.22.83.67', 3),
(22, 'nCAkCicadS8G', 'paddy.testrockersinfo@gmail.com', '2014-02-06 02:34:52', '1.22.82.138', 126),
(23, 'p2sRsRsMtvSJ', 'margee.rockersinfo@gmail.com', '2014-05-24 06:42:51', '1.22.80.128', 216),
(24, 'p2sRsRsMtvSJ', 'grimika.rockersinfo@gmail.com', '2014-05-24 06:43:51', '1.22.80.128', 216),
(25, 'p2sRsRsMtvSJ', 'grimika.rockersinfo@gmail.com', '2014-05-24 06:43:55', '1.22.80.128', 216),
(26, 'p2sRsRsMtvSJ', 'grimika.rockersinfo@gmail.com', '2014-05-24 06:44:03', '1.22.80.128', 216),
(27, 'p2sRsRsMtvSJ', 'grimika.rockersinfo@gmail.com', '2014-05-24 06:44:12', '1.22.80.128', 216),
(28, 'p2sRsRsMtvSJ', 'grimika.rockersinfo@gmail.com', '2014-05-24 06:44:17', '1.22.80.128', 216),
(29, 'p2sRsRsMtvSJ', 'anita.rockersinfo@gmail.com', '2014-05-24 06:45:25', '1.22.80.128', 216),
(30, 'p2sRsRsMtvSJ', 'rekha.rockersinfo@gmail.com', '2014-05-24 06:46:12', '1.22.80.128', 216),
(31, 'p2sRsRsMtvSJ', 'dhru.rockersinfo@gmail.com', '2014-05-24 06:46:29', '1.22.80.128', 216),
(32, 'p2sRsRsMtvSJ', 'jayshree.rockersinfo@gmail.com', '2014-05-24 06:47:53', '1.22.80.128', 216),
(33, 'p2sRsRsMtvSJ', 'grimika.rockersinfo@gmail.com', '2014-05-24 07:10:07', '1.22.80.128', 216),
(34, 'p2sRsRsMtvSJ', 'jayshree.test02.rockersinfo@gmail.com', '2014-05-24 07:13:17', '1.22.80.128', 216),
(35, 'mc9Bfq2phGU3', 'jayshreepatel04@yahoo.com', '2014-05-28 00:47:53', '1.22.81.145', 127),
(36, 'mc9Bfq2phGU3', 'mehul21.rockersinfo@gmail.com', '2014-05-28 00:53:54', '1.22.81.145', 127),
(37, 'p2sRsRsMtvSJ', 'rockersinfo@gmail.com', '2014-05-28 09:05:26', '103.254.202.29', 216),
(38, 'p2sRsRsMtvSJ', 'alpesh.rockersinfo@gmail.com', '2014-05-28 09:23:06', '1.22.81.145', 216),
(39, 'mc9Bfq2phGU3', 'phogat@gmail.com', '2014-05-29 06:12:42', '1.22.81.138', 127),
(40, 'mc9Bfq2phGU3', 'akash.rockersinfo@gmail.com', '2014-05-30 15:43:54', '14.96.165.72', 127),
(41, 'mc9Bfq2phGU3', 'jayshree.test.rockersinfo@gmail.com', '2014-05-30 15:46:16', '14.96.165.72', 127),
(42, 'mc9Bfq2phGU3', 'jayshree.test02.rockersinfo@gmail.com', '2014-05-30 16:56:07', '14.96.165.72', 127),
(43, 'mc9Bfq2phGU3', 'jayshree.test02.rockersinfo@gmail.com', '2014-05-30 16:57:54', '14.96.165.72', 127),
(44, 'mc9Bfq2phGU3', 'jayshree.test02.rockersinfo@gmail.com', '2014-05-30 17:02:33', '14.96.165.72', 127),
(45, 'mc9Bfq2phGU3', 'jayshree.test01.rockersinfo@gmail.com', '2014-06-02 04:18:19', '1.22.80.139', 127),
(46, 'mc9Bfq2phGU3', 'margee.test.rockersinfo@gmail.com', '2014-06-02 04:18:21', '1.22.80.139', 127),
(47, 'mc9Bfq2phGU3', 'nirali.rockersinfo@gmail.com', '2014-06-02 04:20:48', '1.22.80.139', 127),
(48, 'mc9Bfq2phGU3', 'margee.test01.rockersinfo@gmail.com', '2014-06-02 04:23:20', '1.22.80.139', 127),
(49, 'mc9Bfq2phGU3', 'hr.rockersinfo@gmail.com', '2014-06-02 10:17:56', '103.254.202.195', 127),
(50, 'mc9Bfq2phGU3', 'hr.rockersinfo@gmail.com', '2014-06-02 10:29:12', '103.254.202.195', 127),
(51, 'mc9Bfq2phGU3', 'rekha.rockersinfo@gmail.com', '2014-06-02 10:29:31', '103.254.202.195', 127),
(52, 'mc9Bfq2phGU3', 'anita.rockersinfo@gmail.com', '2014-06-02 10:29:45', '103.254.202.195', 127),
(53, 'MG4yXNBKdj3Q', 'jayshree.test@hotmail.com', '2014-07-22 06:05:52', '1.22.81.234', 1),
(54, 'MG4yXNBKdj3Q', 'jayshree022@gmail.com', '2014-07-22 06:08:14', '1.22.81.234', 1),
(55, 'MG4yXNBKdj3Q', 'jayshree022@gmail.com', '2014-07-29 01:37:07', '1.22.81.160', 1),
(56, 'MG4yXNBKdj3Q', 'rashmi.rockersinfo@gmail.com', '2014-07-29 01:37:35', '1.22.81.160', 1),
(57, 'MG4yXNBKdj3Q', 'alpesh.rockersinfo@gmail.com', '2014-07-29 01:37:44', '1.22.81.160', 1),
(58, '8MWJUeNxqRQK', 'ankit.rockersinfo@gmail.com', '2014-07-29 05:06:40', '1.22.83.14', 2),
(59, 'cKyve22vdqnD', 'chintan.test.rockersinfo@gmail.com', '2014-07-30 02:12:40', '1.22.80.216', 6),
(60, 'cKyve22vdqnD', 'chintan.test.rockersinfo@gmail.com', '2014-07-30 03:23:33', '1.22.80.216', 6),
(61, 'cKyve22vdqnD', 'chintan.test.rockersinfo@gmail.com', '2014-07-30 03:58:24', '1.22.80.216', 6),
(62, 'cKyve22vdqnD', 'chintan.test05.rockersinfo@gmail.com', '2014-07-30 04:17:46', '1.22.80.216', 6),
(63, '8MWJUeNxqRQK', 'priya.rockersinfo@gmail.com', '2014-07-30 05:38:12', '103.254.202.42', 2),
(64, 'Vvv2dn5PTEux', 'jayshree022@gmail.com', '2014-09-11 04:52:47', '1.22.80.220', 1),
(65, 'Vvv2dn5PTEux', 'ritu.rockersinfo@gmail.com', '2014-09-15 04:06:29', '210.89.56.198', 1);

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE IF NOT EXISTS `language` (
  `language_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_name` varchar(255) DEFAULT NULL,
  `iso2` varchar(255) DEFAULT NULL,
  `iso3` varchar(255) DEFAULT NULL,
  `active` varchar(255) DEFAULT NULL,
  `language_folder` varchar(250) NOT NULL,
  `country_flag` varchar(250) NOT NULL,
  `direction` varchar(50) NOT NULL,
  `default` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`language_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`language_id`, `language_name`, `iso2`, `iso3`, `active`, `language_folder`, `country_flag`, `direction`, `default`) VALUES
(1, 'English', 'en', '', '1', 'english', '', 'ltr', 1),
(2, 'French', 'fr', 'fr_FR', '1', 'french', '', 'ltr', 0);

-- --------------------------------------------------------

--
-- Table structure for table `learn_more`
--

CREATE TABLE IF NOT EXISTS `learn_more` (
  `pages_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL,
  `pages_title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `sub_title` varchar(255) NOT NULL,
  `icon_image` varchar(255) NOT NULL,
  `description` text CHARACTER SET utf8,
  `slug` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `active` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `meta_keyword` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `meta_description` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `footer_bar` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `header_bar` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `left_side` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `right_side` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `external_link` text CHARACTER SET utf8,
  `learn_category` varchar(255) NOT NULL,
  PRIMARY KEY (`pages_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=63 ;

--
-- Dumping data for table `learn_more`
--

INSERT INTO `learn_more` (`pages_id`, `parent_id`, `language_id`, `pages_title`, `sub_title`, `icon_image`, `description`, `slug`, `active`, `meta_keyword`, `meta_description`, `footer_bar`, `header_bar`, `left_side`, `right_side`, `external_link`, `learn_category`) VALUES
(47, 0, 0, 'Test Page', 'Test Sub Page ', '', '<p>qtge8gqeiqio gqegoqg oeqoeg oqgeoqg oeqeg oqeoqgo egqoegoq g</p>\n', 'fbnfd ryre reyer', '1', '', '', '0', '0', '0', '0', '', '3'),
(19, 0, 0, 'Get Fired Up', 'Get Fired Up', '28924icon.png', '                                                Surf our site to see thousands of inspiring, new and exiciting campaigns.Very unlicky that you won’t get fired up by the  type of campaigns running on there. Collaborate with the thousands of others who use Gorackup to find what inspires them.                                                                                                                                        ', 'How TubeStart Works?', '1', '', '', '0', '0', '0', '0', '', 'how_works'),
(18, 0, 0, 'Gain an All-Time Global Acknowledgment', 'Gain an All-Time Global Acknowledgment', '18040icon.jpg', '<p>tubestart is built with the most effective social-sharing tools that get the word out faster, once your campaign is launched, use those tools to spread the word on your social or personal networks. Get people to &ldquo;like&rdquo; and repost your campaign as this will amplify the success of your campaign&rsquo;s publicity thereby enabling you hit your target goal faster.</p>\n', 'Why TubeStart?', '1', 'rp', 'abcd', '0', '0', '0', '0', '', 'why_nds'),
(17, 0, 1, 'Raise the Funds You Need', 'Raise the Funds You Need', '63394icon.png', '<p>We all should have the means of raising funds whenever we need to. Thanks to Gorackup, now we all do. Our platform is now being used globally to raise funds for whatever reasons. Join in now, it is free.</p>\n', 'Why TubeStart?', '1', 'rp', 'defg', '0', '0', '0', '0', '0', '16'),
(62, 0, 1, 'Pages Title', 'Sub Title', '.22040icon', '<p>Description</p>\n', 'Slug', '0', 'Meta Keyword', 'Meta Description', '0', '0', '0', '0', '0', '16'),
(60, 0, 2, 'Pages Title', 'Sub Title', '.52188icon', '<p>Jayshree</p>\n', 'Slug', '1', 'Meta Keyword', 'Meta Description', '0', '0', '0', '0', '0', '13'),
(44, 0, 0, 'Any one can help corporate', 'Backers Also Help Spread The World', '', '<p><span style="color:rgb(0, 0, 0); font-family:arial,liberation sans,dejavu sans,sans-serif; font-size:14px">I want to remove the search bar and footer (showing how many rows there are visible) that is added to the table by default. I just want to use this plugin for sorting, basically. Can this be done?</span></p>\n', 'about_us', '1', 'fees-test', 'fees-test', '0', '0', '0', '0', '', '3'),
(46, 0, 1, 'Register Your Indiegogo Campaign as a Nonprofit', 'Register Your Indiegogo Campaign as a Nonprofit', '', '<p>&nbsp;If you or the organization you are raising funds for has 501(c)(3) exemption status, you can designate your campaign as a 501(c)(3). When you create your campaign, you will be asked &quot;Which person or organization will receive funds from your campaign?&quot; Select &quot;A nonprofit institution registered as a 501(c)(3) in the United States.&quot; Next, on the &quot;Get Funded&quot; tab, enter the EIN (Employer Identification Number) for your organization. Your campaign is now ready to receive funds. Your 25% discount on Indiegogo platform fees will be automatically applied. All funds will be sent to your organization through FirstGiving.</p>\n\n<p>&nbsp;</p>\n', 'organization ', '1', 'If you or the organization you are raising funds for has 501(c)(3) exemption status, you can designate your campaign as a 501(c)(3). When you create your campaign, you will be asked ', 'If you or the organization you are raising funds for has 501(c)(3) exemption status, you can designate your campaign as a 501(c)(3). When you create your campaign, you will be asked ', '0', '0', '0', '0', '0', '6'),
(54, 0, 2, 'Enregistrer votre campagne Indiegogo comme un but non lucratif', 'Enregistrer votre campagne Indiegogo comme un but non lucratif', '', '<p>Si vous ou l&#39;organisation que vous soulevez des fonds pour a 501 (c) (3) le statut d&#39;exemption, vous pouvez d&eacute;signer votre campagne comme un 501 (c) (3). Lorsque vous cr&eacute;ez votre campagne, vous serez invit&eacute; &quot;Quelle personne ou organisation recevront des fonds de votre campagne?&quot; S&eacute;lectionnez &laquo;Une institution &agrave; but non lucratif enregistr&eacute;e en tant que 501 (c) (3) aux Etats-Unis.&quot; Ensuite, dans l&#39;onglet &quot;Get financ&eacute;&quot;, entrez le num&eacute;ro EIN (Employer Identification Number) pour votre organisation. Votre campagne est maintenant pr&ecirc;t &agrave; recevoir des fonds. Votre 25% de r&eacute;duction sur les frais de plate-forme IndieGoGo sera automatiquement appliqu&eacute;e. Tous les fonds seront envoy&eacute;s &agrave; votre organisation gr&acirc;ce &agrave; Firstgiving.</p>\n', 'organization_french', '1', 'Si vous ou l''organisation que vous soulevez des fonds pour a 501 (c) (3) le statut d''exemption, vous pouvez désigner votre campagne comme un 501 (c) (3). Lorsque vous créez votre campagne, vous serez invité', 'Si vous ou l''organisation que vous soulevez des fonds pour a 501 (c) (3) le statut d''exemption, vous pouvez désigner votre campagne comme un 501 (c) (3). Lorsque vous créez votre campagne, vous serez invité', '0', '0', '0', '0', '0', '6'),
(52, 0, 1, 'Jayshree', 'Jayshree', '.31081icon', '<p>Description</p>\n', 'groupfund', '1', '', '', '0', '0', '0', '0', '0', '13'),
(55, 0, 2, 'Tout le monde peut aider l''entreprise', 'Backers aident également répartis dans le monde', '', '<p>Je veux enlever la barre de recherche et pied de page (montrant combien d&#39;enregistrements sont visibles) qui est ajout&eacute; &agrave; la table par d&eacute;faut. Je veux juste utiliser ce plugin pour le tri, essentiellement. Est-ce possible?</p>\n', 'about_us_french', '1', 'frais essai', 'frais essai', '0', '0', '0', '0', '0', '16'),
(56, 0, 2, 'Élastiques et inélastiques options Type de financement', 'Élastiques et inélastiques options Type de financement', '', '<p>Indiegogoclone a deux types d&#39;options de financement &agrave; choisir: options de type &eacute;lastique et in&eacute;lastique.&nbsp;</p>\n\n<p>&Eacute;lastique / Flexible Type de financement: Si s&eacute;lectionn&eacute;, militants r&eacute;cup&egrave;re tous les fonds si leur objectif de financement n&#39;est pas atteint.&nbsp;</p>\n\n<p>In&eacute;lastique Financement Type / Fixe: Si s&eacute;lectionn&eacute;, militants ne recives les fonds si les objectifs de financement sont remplies et les fonds seront rembours&eacute;s aux cotisants respectifs.</p>\n', 'Get Going_french', '1', 'Si vous ou l''organisation que vous soulevez des fonds pour a 501 (c) (3) le statut d''exemption, vous pouvez désigner votre campagne comme un 501 (c) (3). Lorsque vous créez votre campagne, vous serez invité', 'If you or the organization you are raising funds for has 501(c)(3) exemption status, you can designate your campaign as a 501(c)(3). When you create your campaign, you will be asked ', '0', '0', '0', '0', '0', '13'),
(57, 0, 1, 'Social-partage Tools - outils convaincants pour votre campagne.', 'Social-partage Tools - outils convaincants pour votre campagne.', '', '<p>Lancer une campagne, Partager sur les principaux r&eacute;seaux sociaux, envoyer des e-mails directs, de suivre et regarder votre campagne prendre son envol, gardez vos bailleurs de mises &agrave; jour et bien plus encore.</p>\n', 'Feature_french', '1', '', '', '0', '0', '0', '0', '0', '15'),
(58, 0, 2, 'Gagner un All-Time mondial Remerciements', 'Gagner un All-Time mondial Remerciements', '', '<p>tubestart est construit avec des outils de partage social-les plus efficaces qui obtiennent le mot rapidement, une fois que votre campagne est lanc&eacute;e, utiliser ces outils pour passer le mot sur ​​vos r&eacute;seaux sociaux ou personnels. Amener les gens &agrave; &quot;similaires&quot; et reposter votre campagne car cela va amplifier le succ&egrave;s de la publicit&eacute; de votre campagne vous permettant ainsi frappez votre objectif cible plus rapidement.</p>\n', 'Why TubeStart_french', '1', '', '', '0', '0', '0', '0', '0', '2');

-- --------------------------------------------------------

--
-- Table structure for table `learn_more_category`
--

CREATE TABLE IF NOT EXISTS `learn_more_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `category_name` varchar(255) DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `footer` varchar(255) DEFAULT NULL,
  `right_side` varchar(255) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `learn_more_category`
--

INSERT INTO `learn_more_category` (`category_id`, `language_id`, `category_name`, `active`, `footer`, `right_side`) VALUES
(6, 1, 'Nonprofits', 1, 'yes', '0'),
(2, 1, 'How Indiegogoclone Works', 1, 'yes', '0'),
(3, 1, 'Why Indiegogoclone', 1, 'yes', '0'),
(9, 1, 'New Right Menu', 1, '0', 'yes'),
(10, 1, 'Get GO', 1, '0', '0'),
(12, 2, 'Obtenez GO', 1, '0', '0'),
(13, 2, 'Nouveau menu de droite', 1, '0', 'yes'),
(14, 2, 'Organismes sans but lucratif', 1, 'yes', '0'),
(15, 1, 'Get Going', 1, 'yes', 'yes'),
(16, 2, 'Pourquoi Indiegogoclone', 1, 'yes', '0'),
(17, 1, 'Comment Indiegogoclone Travaux', 0, 'yes', '0');

-- --------------------------------------------------------

--
-- Table structure for table `linkdin_setting`
--

CREATE TABLE IF NOT EXISTS `linkdin_setting` (
  `linkdin_setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `linkdin_enable` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `linkedin_access` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `linkedin_secret` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `linkdin_url` text CHARACTER SET utf8,
  PRIMARY KEY (`linkdin_setting_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `linkdin_setting`
--

INSERT INTO `linkdin_setting` (`linkdin_setting_id`, `linkdin_enable`, `linkedin_access`, `linkedin_secret`, `linkdin_url`) VALUES
(1, '1', '75qaggben5158t', 'skUcMGUho0Vi0Hce', 'https://in.linkedin.com/');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE IF NOT EXISTS `member` (
  `member_id` int(100) NOT NULL AUTO_INCREMENT,
  `user_id` int(100) NOT NULL,
  `project_id` int(100) NOT NULL,
  `member_email` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `message_conversation`
--

CREATE TABLE IF NOT EXISTS `message_conversation` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `is_read` int(11) NOT NULL DEFAULT '0' COMMENT '0-unread,1-read',
  `message_subject` text,
  `message_content` text,
  `project_id` int(11) NOT NULL,
  `date_added` date NOT NULL,
  `reply_message_id` int(11) NOT NULL,
  PRIMARY KEY (`message_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `message_conversation`
--

INSERT INTO `message_conversation` (`message_id`, `sender_id`, `receiver_id`, `is_read`, `message_subject`, `message_content`, `project_id`, `date_added`, `reply_message_id`) VALUES
(1, 3, 2, 0, 'tet', 'dfczxdcdszfd  dhdhgdb   g gfb bg ', 0, '2014-10-29', 0),
(2, 2, 3, 0, 'hjgh', 'ghhgjghjghjghj', 0, '2014-10-29', 0);

-- --------------------------------------------------------

--
-- Table structure for table `message_setting`
--

CREATE TABLE IF NOT EXISTS `message_setting` (
  `message_setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `email_admin_on_new_message` int(11) NOT NULL DEFAULT '1' COMMENT '0-no,1-yes',
  `email_user_on_new_message` int(11) NOT NULL DEFAULT '1' COMMENT '0-no,1-yes',
  `default_message_subject` varchar(255) DEFAULT NULL,
  `message_enable` int(11) NOT NULL DEFAULT '1' COMMENT '0-no,1-yes',
  PRIMARY KEY (`message_setting_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `message_setting`
--

INSERT INTO `message_setting` (`message_setting_id`, `email_admin_on_new_message`, `email_user_on_new_message`, `default_message_subject`, `message_enable`) VALUES
(1, 1, 1, 'New Message', 0);

-- --------------------------------------------------------

--
-- Table structure for table `meta_setting`
--

CREATE TABLE IF NOT EXISTS `meta_setting` (
  `meta_setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `meta_keyword` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`meta_setting_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `meta_setting`
--

INSERT INTO `meta_setting` (`meta_setting_id`, `title`, `meta_keyword`, `meta_description`) VALUES
(1, 'Fundraising script', 'fundraising, fundraising script, fundraising scripts, fundraising script for sale, fundraising website clone, script like fundraising, site like Crowd funding , Crowd funding  ideas, Crowd funding script, scripts fundraising, unique fund', 'One of the BEST ways to raise money online and Get your online donation website FREE! Invite family and friends to donate to any of your fundraising ideas.');

-- --------------------------------------------------------

--
-- Table structure for table `newsletter_job`
--

CREATE TABLE IF NOT EXISTS `newsletter_job` (
  `job_id` int(100) NOT NULL AUTO_INCREMENT,
  `newsletter_id` int(100) NOT NULL,
  `send_total` int(11) DEFAULT '0',
  `job_date` date DEFAULT NULL,
  `job_start_date` date NOT NULL,
  `newsletter_type` varchar(255) DEFAULT NULL,
  `temp_date` date DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `draft` varchar(255) NOT NULL,
  `sent` varchar(255) NOT NULL,
  PRIMARY KEY (`job_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `newsletter_job`
--

INSERT INTO `newsletter_job` (`job_id`, `newsletter_id`, `send_total`, `job_date`, `job_start_date`, `newsletter_type`, `temp_date`, `start_date`, `end_date`, `draft`, `sent`) VALUES
(30, 17, 0, NULL, '2014-10-04', '', NULL, NULL, NULL, '1', ''),
(29, 7, 0, NULL, '1969-12-31', 'duration', NULL, NULL, NULL, '1', ''),
(27, 6, 0, NULL, '2014-09-11', '', NULL, NULL, NULL, '1', ''),
(28, 7, 0, NULL, '2014-09-30', '', NULL, NULL, NULL, '1', ''),
(26, 6, 0, NULL, '2014-09-11', '', NULL, NULL, NULL, '1', ''),
(25, 7, 0, NULL, '2014-07-30', 'daily', NULL, NULL, NULL, '1', ''),
(11, 7, 0, NULL, '2014-08-02', 'daily', NULL, NULL, NULL, '1', ''),
(12, 17, 0, NULL, '2014-08-06', '', NULL, NULL, NULL, '1', ''),
(14, 7, 0, NULL, '2014-08-11', '', NULL, NULL, NULL, '1', ''),
(20, 8, 0, NULL, '2014-08-22', 'daily', NULL, NULL, NULL, '1', ''),
(23, 7, 0, NULL, '2014-08-28', '', NULL, NULL, NULL, '1', '');

-- --------------------------------------------------------

--
-- Table structure for table `newsletter_jobs`
--

CREATE TABLE IF NOT EXISTS `newsletter_jobs` (
  `job_id` int(100) NOT NULL AUTO_INCREMENT,
  `newsletter_id` int(100) NOT NULL,
  `send_total` int(100) NOT NULL,
  `job_date` date NOT NULL,
  `job_start_date` date NOT NULL,
  PRIMARY KEY (`job_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `newsletter_jobs`
--

INSERT INTO `newsletter_jobs` (`job_id`, `newsletter_id`, `send_total`, `job_date`, `job_start_date`) VALUES
(3, 8, 0, '2014-01-07', '2014-01-07');

-- --------------------------------------------------------

--
-- Table structure for table `newsletter_report`
--

CREATE TABLE IF NOT EXISTS `newsletter_report` (
  `report_id` int(100) NOT NULL AUTO_INCREMENT,
  `newsletter_user_id` int(100) NOT NULL,
  `job_id` int(100) NOT NULL,
  `is_fail` int(20) NOT NULL DEFAULT '0',
  `is_open` int(20) NOT NULL DEFAULT '0',
  `send_date` datetime NOT NULL,
  `type` varchar(255) NOT NULL,
  `shedule_id` int(11) NOT NULL,
  PRIMARY KEY (`report_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

-- --------------------------------------------------------

--
-- Table structure for table `newsletter_setting`
--

CREATE TABLE IF NOT EXISTS `newsletter_setting` (
  `newsletter_setting_id` int(10) NOT NULL AUTO_INCREMENT,
  `newsletter_from_name` varchar(255) DEFAULT NULL,
  `newsletter_from_address` varchar(255) DEFAULT NULL,
  `newsletter_reply_name` varchar(255) DEFAULT NULL,
  `newsletter_reply_address` varchar(255) DEFAULT NULL,
  `new_subscribe_email` varchar(255) DEFAULT NULL,
  `unsubscribe_email` varchar(255) DEFAULT NULL,
  `new_subscribe_to` varchar(255) DEFAULT NULL,
  `selected_newsletter_id` int(20) NOT NULL,
  `number_of_email_send` int(20) NOT NULL,
  `break_between_email` int(20) NOT NULL,
  `mailer` varchar(50) DEFAULT NULL,
  `sendmail_path` varchar(255) DEFAULT NULL,
  `smtp_port` int(20) DEFAULT NULL,
  `smtp_host` varchar(255) DEFAULT NULL,
  `smtp_email` varchar(255) DEFAULT NULL,
  `smtp_password` varchar(255) DEFAULT NULL,
  `break_type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`newsletter_setting_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `newsletter_setting`
--

INSERT INTO `newsletter_setting` (`newsletter_setting_id`, `newsletter_from_name`, `newsletter_from_address`, `newsletter_reply_name`, `newsletter_reply_address`, `new_subscribe_email`, `unsubscribe_email`, `new_subscribe_to`, `selected_newsletter_id`, `number_of_email_send`, `break_between_email`, `mailer`, `sendmail_path`, `smtp_port`, `smtp_host`, `smtp_email`, `smtp_password`, `break_type`) VALUES
(1, 'Fundraising Script', 'jayshree.rockersinfo@gmail.com', 'Fundraising Script', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'all', 27, 30, 15, 'mail', '/usr/sbin/sendmail', 25, 'mail.groupfund.me', 'smtp@groupfund.me', 'smtp2123', 'hours');

-- --------------------------------------------------------

--
-- Table structure for table `newsletter_subscribe`
--

CREATE TABLE IF NOT EXISTS `newsletter_subscribe` (
  `subscribe_id` int(100) NOT NULL AUTO_INCREMENT,
  `newsletter_user_id` int(100) NOT NULL,
  `newsletter_id` int(100) NOT NULL,
  `subscribe_date` date NOT NULL,
  PRIMARY KEY (`subscribe_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=177 ;

--
-- Dumping data for table `newsletter_subscribe`
--

INSERT INTO `newsletter_subscribe` (`subscribe_id`, `newsletter_user_id`, `newsletter_id`, `subscribe_date`) VALUES
(176, 555, 17, '2014-10-04'),
(175, 554, 28, '2014-10-02');

-- --------------------------------------------------------

--
-- Table structure for table `newsletter_template`
--

CREATE TABLE IF NOT EXISTS `newsletter_template` (
  `newsletter_id` int(100) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) DEFAULT NULL,
  `template_content` longtext,
  `attach_file` varchar(255) DEFAULT NULL,
  `allow_subscribe_link` int(10) NOT NULL DEFAULT '0',
  `allow_unsubscribe_link` int(10) NOT NULL DEFAULT '0',
  `project_id` int(100) NOT NULL,
  `newsletter_create_date` date NOT NULL,
  PRIMARY KEY (`newsletter_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `newsletter_template`
--

INSERT INTO `newsletter_template` (`newsletter_id`, `subject`, `template_content`, `attach_file`, `allow_subscribe_link`, `allow_unsubscribe_link`, `project_id`, `newsletter_create_date`) VALUES
(21, 'Newsletter Testing', '<p>http://groupfund.me/index.php/admin/newsletter/send_testing_newsetter</p>\n', '', 0, 1, 0, '2014-08-21'),
(27, 'mk', '<p>;.&#39;;.</p>\n', '', 0, 0, 0, '2014-10-02'),
(28, 'mkmk', '<p>jih</p>\n', '', 0, 0, 0, '2014-10-02'),
(6, 'New Inquiry from Fundraisingscript', '<table cellpadding=\\"3\\" cellspacing=\\"3\\" width=\\"700\\" style=\\"border:1px solid #000\\">\\r\\n<tbody><tr>\\r\\n<td colspan=\\"2\\" style=\\"text-align:left;vertical-align:top\\"><a href=\\"http://hireiphone.com/indigo/projects/car1/277\\" style=\\"font-size:18px;font-weight:bold;color:#114a75;text-transform:capitalize\\">Car</a></td>\\r\\n</tr>\\r\\n<tr>\\r\\n<td style=\\"text-align:left;vertical-align:top\\">\\r\\n         <a href=\\"http://hireiphone.com/indigo/projects/car1/277\\" target=\\"_blank\\"><img class=\\"p_img\\" src=\\"http://hireiphone.com/indigo/upload/thumb/no_img.jpg\\" width=\\"190\\" height=\\"150\\" title=\\"Car\\"></a>\\r\\n</td>\\r\\n<td style=\\"text-align:left;padding:10px;vertical-align:top\\">\\r\\n<table cellpadding=\\"2\\" cellspacing=\\"2\\" style=\\"border:1px solid #000\\">\\r\\n<tbody><tr>\\r\\n<td style=\\"font-size:16px;font-weight:bold;color:#114a75;text-align:center;vertical-align:top\\">Goal</td>\\r\\n<td style=\\"font-size:16px;font-weight:bold;color:#114a75;text-align:center;vertical-align:top\\">Raised</td>\\r\\n<td style=\\"font-size:16px;font-weight:bold;color:#114a75;text-align:center;vertical-align:top\\">Days Left</td>\\r\\n</tr>\\r\\n<tr>\\r\\n<td style=\\"font-size:12px;font-weight:bold;text-align:center;vertical-align:top\\">\\r\\n$2,000.00\\r\\n</td>\\r\\n<td style=\\"font-size:12px;font-weight:bold;text-align:center;vertical-align:top\\">\\r\\n0 RAISED\\r\\n</td>\\r\\n<td style=\\"font-size:12px;font-weight:bold;text-align:center;vertical-align:top\\">\\r\\n19 DAYS LEFT\\r\\n</td>\\r\\n</tr>\\r\\n<tr><td colspan=\\"3\\" height=\\"18\\">&nbsp;</td></tr>\\r\\n<tr>\\r\\n<td style=\\"text-align:left;vertical-align:top\\" colspan=\\"3\\">\\r\\n<span style=\\"font-size:16px;font-weight:bold;color:#114a75\\">Summary: </span> HKJHKSD\\r\\n</td>\\r\\n</tr>\\r\\n</tbody></table>\\r\\n</td>\\r\\n</tr>\\r\\n<tr>\\r\\n<td style=\\"text-align:left;padding:10px;vertical-align:top\\" colspan=\\"2\\"></td>\\r\\n</tr>\\r\\n</tbody></table>  ', '64261newsletter.jpg', 1, 1, 0, '2013-04-12'),
(7, 'New image for our office', 'Hello This &nbsp;newsletter for testing purpose created.&nbsp;Hello This &nbsp;newsletter for testing purpose created.&nbsp;Hello This &nbsp;newsletter for testing purpose created.&nbsp;Hello This &nbsp;newsletter for testing purpose created.&nbsp;Hello This &nbsp;newsletter for testing purpose created.&nbsp;Hello This &nbsp;newsletter for testing purpose created.&nbsp;Hello This &nbsp;newsletter for testing purpose created.&nbsp;Hello This &nbsp;newsletter for testing purpose created.&nbsp;Hello This &nbsp;newsletter for testing purpose created.  ', '', 0, 0, 0, '2013-04-12'),
(8, 'New Promotional', 'Hello this is promotional scheme,', '96652newsletter.jpg', 1, 1, 118, '2014-01-06'),
(17, 'Newsletter', '<p>News letter</p>\n', '.62696newsletter', 0, 0, 0, '2014-08-07');

-- --------------------------------------------------------

--
-- Table structure for table `newsletter_user`
--

CREATE TABLE IF NOT EXISTS `newsletter_user` (
  `newsletter_user_id` int(100) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `user_date` datetime NOT NULL,
  `user_ip` varchar(255) DEFAULT NULL,
  `is_subscribe` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0=''not_subscriber'',1=''subscriber''',
  PRIMARY KEY (`newsletter_user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=556 ;

--
-- Dumping data for table `newsletter_user`
--

INSERT INTO `newsletter_user` (`newsletter_user_id`, `user_name`, `email`, `user_date`, `user_ip`, `is_subscribe`) VALUES
(554, NULL, 'jayshree.rockersinfo@gmail.com', '2014-09-11 07:27:44', '1.22.80.220', 2),
(555, '', 'jayshree.rockersinfo@gmail.com', '2014-10-04 03:57:12', '27.109.7.130', 1);

-- --------------------------------------------------------

--
-- Table structure for table `outside_link`
--

CREATE TABLE IF NOT EXISTS `outside_link` (
  `link_id` int(100) NOT NULL AUTO_INCREMENT,
  `project_id` int(100) NOT NULL,
  `facebook` text COLLATE utf8_unicode_ci NOT NULL,
  `twitter` text COLLATE utf8_unicode_ci NOT NULL,
  `youtube` text COLLATE utf8_unicode_ci NOT NULL,
  `imdb` text COLLATE utf8_unicode_ci NOT NULL,
  `website` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`link_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `outside_link`
--

INSERT INTO `outside_link` (`link_id`, `project_id`, `facebook`, `twitter`, `youtube`, `imdb`, `website`) VALUES
(1, 5, '', '', '', '', ''),
(2, 4, '', '', '', '', ''),
(3, 3, '', '', '', '', ''),
(4, 1, '', '', '', '', ''),
(5, 8, '', '', '', '', ''),
(6, 9, '', '', '', '', ''),
(7, 14, '', '', '', '', ''),
(8, 21, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `pages_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL,
  `pages_title` varchar(255) DEFAULT NULL,
  `description` text,
  `slug` varchar(255) DEFAULT NULL,
  `active` varchar(255) DEFAULT NULL,
  `meta_keyword` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `footer_bar` varchar(20) DEFAULT NULL,
  `header_bar` varchar(20) DEFAULT NULL,
  `left_side` varchar(20) DEFAULT NULL,
  `right_side` varchar(20) DEFAULT NULL,
  `external_link` text,
  PRIMARY KEY (`pages_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`pages_id`, `parent_id`, `language_id`, `pages_title`, `description`, `slug`, `active`, `meta_keyword`, `meta_description`, `footer_bar`, `header_bar`, `left_side`, `right_side`, `external_link`) VALUES
(18, 0, 1, 'About Us', '<p>Our goal is to help find a financial solution for the specific needs of the student, and we believe that every student deserves an equal access to education and life luck, they provide the student with the tools and knowledge needed to succeed in life and have more opportunities for their future.&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>We believe that education is the foundation of a great nation, helping a student, you are not only giving them a better opportunity for life, you build a new generation, more humane and sensitive, help them get their tuition, pay their college loan, books and living expenses.&nbsp;</p>\n\n<p>We know for a fact That there are Thousands of educational givers and thesis-have same Concerns and desire of equality of Opportunities, thats why we put all our efforts to connect with the thesis givers students in need, So THAT givers can get to know the student Needs, dreams and aspirations.</p>\n', 'about-us', '1', 'Fund Me Online, online fundraising, donation website, donations online, accept donations', 'One of the BEST ways to raise money online. Get your online donation website FREE! Invite family & friends to donate online to any of your online fundraising ideas.', 'yes', '0', '0', '0', ''),
(19, 0, 1, 'Terms of Service', '<h4>Indiegogoclone<br />\nTerms of Use (&ldquo;Agreement&rdquo;)<br />\nThis Agreement was created on September 10, 2013.&nbsp;<br />\nWelcome to Indiegogoclone, the website and online service of Indiegogoclone. By accessing or using our Website, services and Indiegogoclone, Content provided through or in connection with the services provided by our Website (collectively, &quot;Service&quot;), you, as a User or Member, signify that you have read, understood, and agree to be bound by these Terms of Use (&quot;Agreement&quot;), the Privacy Policy posted at Indiegogoclone along with all other operating rules, policies and procedures and additional terms and conditions published on our Website (which are incorporated by reference), and you are at least 13 years old. Capitalized terms are defined in this Agreement.</h4>\n\n<h4>We reserve the right to amend this Agreement at any time and without notice. If we do this, we will post the amended Agreement on this page and indicate at the top of the page the date the Agreement was last revised. Your continued use of the Service after any such changes constitutes your acceptance of the new Agreement. If you do not agree to any of this Agreement or any changes to this Agreement, do not use or access (or continue to access) the Service or discontinue immediately any use of the Service.<br />\nWho We Are<br />\nIndiegogoclone is a global donation-based crowdfunding platform for agriculture, empowering farmers and ranchers to obtain non-conventional funding for conventional needs. Since launching in September 2013, Indiegogoclone has enabled Project Owners to launch Projects from around the world. Indiegogoclone is changing the way people raise funds for agriculture - creative, entrepreneurial or cause-related projects.&nbsp;<br />\nWe are an open platform. You understand and acknowledge that you may be exposed to Member Content that may be inaccurate, offensive, indecent, or objectionable. Member Content should not be viewed as being an endorsement or opinion shared by Indiegogoclone. You agree that Indiegogoclone is not liable for any damages or losses resulting from such Member Content.<br />\nUsers of Our Service and Members</h4>\n', 'Terms-of-service', '1', 'Fundraising script Terms and Conditions', 'Fundraising script Terms and Conditions', 'yes', '0', '0', '0', ''),
(20, 0, 1, 'Privacy Policy', '<h4>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h4>\n\n<p>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n\n<h4>The standard chunk of Lorem Ipsum used since the 1500s is reproduced</h4>\n\n<p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from &quot;de Finibus Bonorum et Malorum&quot; by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>\n', 'privacy', '1', 'Fundraising script Privacy Policy', 'Fundraising script Privacy Policy', 'yes', '0', '0', '0', ''),
(21, 0, 1, 'Career', '<p>Lorem Ipsum is simply dummy text of the printing industry and composition.&nbsp;</p>\n\n<p>Lorem Ipsum has been the standard dummy text of the industry since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularized in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&nbsp;</p>\n\n<p>The piece standard Lorem Ipsum used since the 1500s is reproduced true&nbsp;</p>\n\n<p>The standard chunk of Lorem Ipsum used since the 1500s is Reproduced below for Those interested. Sections 1.10.32 and 1.10.33 from &quot;Finibus Bonorum and of Malorum&quot; by Cicero are aussi Reproduced In Their exact original form, Accompanied by English versions from the 1914 translation by H. Rackham.</p>\n', 'career', '1', 'Groupfund.me career', 'Groupfund.me offer best benefits to employee', 'yes', '0', '0', 'yes', ''),
(23, 0, 1, 'Help', '<h4>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h4>\n\n<p>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n', 'help', '1', '', '', 'yes', 'yes', '0', '0', ''),
(24, 0, 1, 'Fees', '<h4>Funding Options.Lesser Fees.</h4>\n\n<p>To create and start a campaign on Indiegogoclone is totally free. All you have to do is register and start a campaign. Choosing from the funding options we have, you may go with the Elastic Funding which lets you keep all of the funds raised from your campaign, even if you don&rsquo;t hit your target. Or, you may choose to go with the inelastic Funding, which refunds the donations back to your Backers should you don&rsquo;t hit your target. Whichever way, you pay nothing till you start raising funds.</p>\n\n<h4>Elastic Fund (Flexible)</h4>\n\n<div class="fees_main">\n<div class="redbg_left">You Hit Your Target</div>\n\n<div class="fees_right">{hit_target_flexible}%</div>\n\n<div class="clear">&nbsp;</div>\n</div>\n\n<div class="fees_main">\n<div class="redbg_left">You Don&acute;t Hit Your Target</div>\n\n<div class="fees_right">\n<p><strong>{hit_nottarget_flexible}% but you get to keep what you earned.</strong></p>\n\n<p>This encourages people to set reasonable goals and promote their campaigns.</p>\n</div>\n\n<div class="clear">&nbsp;</div>\n</div>\n\n<div class="fees_main">\n<div class="redbg_left">Third Party Fee</div>\n\n<div class="fees_right">\n<p>Paypal Charges 2.9% on all transactions. Please check their <a href="http://paypal.com" target="_blank">website</a> for details.</p>\n</div>\n\n<div class="clear">&nbsp;</div>\n</div>\n\n<h4>Inelastic Funding (Fixed)</h4>\n\n<div class="fees_main">\n<div class="redbg_left">You Hit Your Target</div>\n\n<div class="fees_right">\n<p>{hit_target_fixed}%</p>\n</div>\n\n<div class="clear">&nbsp;</div>\n</div>\n\n<div class="fees_main">\n<div class="redbg_left">You Don&acute;t Hit Your Target</div>\n\n<div class="fees_right">\n<p><strong>0% and your contributions get refunded.</strong></p>\n\n<p>Third-party fees do not apply for refunded contributions.</p>\n</div>\n\n<div class="clear">&nbsp;</div>\n</div>\n\n<div class="fees_main">\n<div class="redbg_left">Third Party Fee</div>\n\n<div class="fees_right">\n<p>Paypal Charges 2.9% on all transactions. Please check their <a href="http://paypal.com" target="_blank">website</a> for details.</p>\n</div>\n\n<div class="clear">&nbsp;</div>\n</div>\n', 'fees', '1', 'fees-test', 'fees-test', 'yes', '0', '0', '0', ''),
(25, 0, 1, 'SideBar Test', '<p>sdfsdf</p>\n', 'sdf', '1', 'asd', 'asd', 'yes', '0', '0', 'yes', ''),
(27, 0, 1, 'Get Help', '<h2>We Want to Make You Happy</h2>\n\n<p><strong>We want your experience on website to be a good one.</strong>&nbsp;We&#39;re here to help you fund your campaign or contribute to one that sparks your interest. We&#39;ll support you in the process, so don&#39;t hesitate to contact us or visit our Help Center for answers to your questions.</p>\n\n<p>Search our&nbsp;<a href="javascript://" style="color: rgb(255, 0, 81); text-decoration: none;">Help Center</a>&nbsp;for information, advice and ideas. Ask us a question or tell us how we&#39;re doing. Your happiness is important to us.</p>\n', 'crowdfunding-campaign-basics', '1', 'crowdfunding', 'crowdfunding', 'yes', '0', '0', 'yes', ''),
(28, 0, 2, 'à propos de nous', '<p>Notre objectif est d&#39;aider &agrave; trouver une solution financi&egrave;re pour les besoins sp&eacute;cifiques de l&#39;&eacute;l&egrave;ve, et nous croyons que chaque &eacute;l&egrave;ve m&eacute;rite un acc&egrave;s &eacute;gal &agrave; l&#39;&eacute;ducation et &agrave; la vie de chance, ils fournir &agrave; l&#39;&eacute;tudiant les outils et les connaissances n&eacute;cessaires pour r&eacute;ussir dans la vie et avoir plus possibilit&eacute;s pour leur avenir.&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>Nous croyons que l&#39;&eacute;ducation est le fondement d&#39;une grande nation, d&#39;aider un &eacute;tudiant, vous ne sont pas seulement de leur donner une meilleure chance pour la vie, vous construisez une nouvelle g&eacute;n&eacute;ration, plus humaine et plus sensible, les aidons &agrave; leurs frais de scolarit&eacute;, payer leur pr&ecirc;t d&#39;&eacute;tudes coll&eacute;giales, des livres et des frais de subsistance.&nbsp;</p>\n\n<p>Nous savons pour un fait qu&#39;il ya des milliers de donneurs d&#39;enseignement et th&egrave;se ont les m&ecirc;mes pr&eacute;occupations et le d&eacute;sir de l&#39;&eacute;galit&eacute; des chances, c&#39;est pourquoi nous mettons tous nos efforts &agrave; se connecter avec les dispensateurs de th&egrave;se des &eacute;tudiants dans le besoin, de sorte que donneurs peuvent conna&icirc;tre le besoins des &eacute;tudiants, des r&ecirc;ves et aspirations.</p>\n', 'about_us_french', '1', 'Financer moi en ligne, collecte de fonds en ligne, site de dons, des dons en ligne, accepter des dons', 'Une des meilleures façons de recueillir des fonds en ligne. Obtenez votre site de dons en ligne GRATUIT! Invitez la famille et les amis de faire un don en ligne à toutes vos idées de collecte de fonds en ligne.', 'yes', NULL, NULL, '0', ''),
(29, 0, 2, 'carrière', '<p>Lorem Ipsum est simplement faux texte de l&#39;industrie de l&#39;imprimerie et de la composition.&nbsp;</p>\n\n<p>Lorem Ipsum est le faux texte standard de l&#39;industrie depuis les ann&eacute;es 1500, quand une imprimante inconnu a pris une cuisine de type et il brouill&eacute;s pour faire un livre sp&eacute;cimen type. Il a non seulement surv&eacute;cu &agrave; cinq si&egrave;cles, mais aussi le saut dans la composition &eacute;lectronique, reste essentiellement inchang&eacute;. Il a &eacute;t&eacute; popularis&eacute; dans les ann&eacute;es 1960 avec la sortie des feuilles Letraset contenant des passages Lorem Ipsum, et plus r&eacute;cemment avec le logiciel de PAO comme PageMaker Aldus y compris les versions de Lorem Ipsum.&nbsp;</p>\n\n<p>La norme de pi&egrave;ce Lorem Ipsum utilis&eacute; depuis les ann&eacute;es 1500 est reproduit vrai&nbsp;</p>\n\n<p>Le morceau norme de Lorem Ipsum utilis&eacute; depuis les ann&eacute;es 1500 est reproduit ci-dessous personnes int&eacute;ress&eacute;es. Les articles 1.10.32 et 1.10.33 de &quot;Finibus Bonorum et de Malorum&quot; par Cic&eacute;ron sont also reproduits dans leur forme originale exacte, accompagn&eacute; par les versions anglaises de la traduction 1914 par H. Rackham.</p>\n', 'career_french', '1', 'Groupfund.me carrière', 'Groupfund.me offrir les meilleurs avantages aux employés', 'yes', NULL, NULL, 'yes', ''),
(30, 0, 2, 'honoraires', '<p>Frais de financement Options.Lesser.&nbsp;</p>\n\n<p>Pour cr&eacute;er et lancer une campagne sur Indiegogoclone est totalement gratuit. Tout ce que vous avez &agrave; faire est de vous inscrire et de commencer une campagne. Choisissant parmi les options de financement que nous avons, vous pouvez aller avec le financement &eacute;lastique qui vous permet de garder tous les fonds recueillis &agrave; partir de votre campagne, m&ecirc;me si vous ne touchez pas votre cible. Ou, vous pouvez choisir d&#39;aller avec le financement in&eacute;lastique, qui rembourse les dons retourner &agrave; Backers devraient vous n&#39;avez pas atteint votre cible. Quelle que soit la fa&ccedil;on, vous ne payez rien jusqu&#39;&agrave; ce que vous commencez &agrave; collecter des fonds.&nbsp;</p>\n\n<p>Fonds &eacute;lastique (flexible)&nbsp;</p>\n\n<p>Vous frappez votre cible&nbsp;<br />\n{%} hit_target_flexible&nbsp;</p>\n\n<p>Vous ne touchez pas votre cible&nbsp;<br />\n{} hit_nottarget_flexible% mais vous arrivez &agrave; garder ce que vous avez gagn&eacute;.&nbsp;</p>\n\n<p>Cela encourage les gens &agrave; se fixer des objectifs raisonnables et de promouvoir leurs campagnes.&nbsp;</p>\n\n<p><br />\nFrais de tiers&nbsp;<br />\nPaypal Frais de 2,9% sur toutes les transactions. S&#39;il vous pla&icirc;t consulter leur site web pour plus de d&eacute;tails.&nbsp;</p>\n\n<p><br />\nIn&eacute;lastique financement (fixe)&nbsp;</p>\n\n<p>Vous frappez votre cible&nbsp;<br />\n{%} hit_target_fixed&nbsp;</p>\n\n<p><br />\nVous ne touchez pas votre cible&nbsp;<br />\n0% et vos contributions obtenir le remboursement.&nbsp;</p>\n\n<p>Les frais de tiers ne s&#39;appliquent pas aux cotisations rembours&eacute;es.&nbsp;</p>\n\n<p><br />\nFrais de tiers&nbsp;<br />\nPaypal Frais de 2,9% sur toutes les transactions. S&#39;il vous pla&icirc;t consulter leur site web pour plus de d&eacute;tails.</p>\n', 'fees_french', '1', 'frais essai', 'frais essai', 'yes', NULL, NULL, '0', ''),
(31, 0, 2, 'obtenir de l''aide', '<p>Nous voulons vous rendre heureux&nbsp;</p>\n\n<p>Nous voulons que votre exp&eacute;rience sur le site pour &ecirc;tre une bonne chose. Nous sommes l&agrave; pour vous aider &agrave; financer votre campagne ou contribuer &agrave; celui qui aura &eacute;veill&eacute; votre curiosit&eacute;. Nous vous accompagnons dans le processus, alors n&#39;h&eacute;sitez pas &agrave; nous contacter ou &agrave; visiter notre Centre d&#39;aide pour obtenir des r&eacute;ponses &agrave; vos questions.&nbsp;</p>\n\n<p>Rechercher notre Centre d&#39;aide pour obtenir des informations, des conseils et des id&eacute;es. Nous poser une question ou nous dire comment nous faisons. Votre bonheur est importante pour nous.</p>\n', 'crowdfunding-campaign-basics_french', '1', 'crowdfunding', 'crowdfunding', 'yes', NULL, NULL, 'yes', ''),
(32, 0, 2, 'aider', '<p>Lorem Ipsum est simplement faux texte de l&#39;industrie de l&#39;imprimerie et de la composition.&nbsp;</p>\n\n<p>Lorem Ipsum est le faux texte standard de l&#39;industrie depuis les ann&eacute;es 1500, quand une imprimante inconnu a pris une cuisine de type et il brouill&eacute;s pour faire un livre sp&eacute;cimen type. Il a non seulement surv&eacute;cu &agrave; cinq si&egrave;cles, mais aussi le saut dans la composition &eacute;lectronique, reste essentiellement inchang&eacute;. Il a &eacute;t&eacute; popularis&eacute; dans les ann&eacute;es 1960 avec la sortie des feuilles Letraset contenant des passages Lorem Ipsum, et plus r&eacute;cemment avec le logiciel de PAO comme PageMaker Aldus y compris les versions de Lorem Ipsum.</p>\n', 'help_french', '1', '', '', 'yes', NULL, NULL, '0', ''),
(33, 0, 1, 'Appuyez sur', '<p>Lorem Ipsum est simplement faux texte de l&#39;industrie de l&#39;imprimerie et de la composition.&nbsp;</p>\n\n<p>Lorem Ipsum est le faux texte standard de l&#39;industrie depuis les ann&eacute;es 1500, quand une imprimante inconnu a pris une cuisine de type et il brouill&eacute;s pour faire un livre sp&eacute;cimen type. Il a non seulement surv&eacute;cu &agrave; cinq si&egrave;cles, mais aussi le saut dans la composition &eacute;lectronique, reste essentiellement inchang&eacute;. Il a &eacute;t&eacute; popularis&eacute; dans les ann&eacute;es 1960 avec la sortie des feuilles Letraset contenant des passages Lorem Ipsum, et plus r&eacute;cemment avec le logiciel de PAO comme PageMaker Aldus y compris les versions de Lorem Ipsum.&nbsp;</p>\n\n<p>Le morceau norme de Lorem Ipsum utilis&eacute; depuis les ann&eacute;es 1500 est reproduit&nbsp;</p>\n\n<p>Le morceau norme de Lorem Ipsum utilis&eacute; depuis les ann&eacute;es 1500 est reproduit ci-dessous pour ceux qui sont int&eacute;ress&eacute;s. Les articles 1.10.32 et 1.10.33 de &quot;de Finibus Bonorum et Malorum&quot; par Cic&eacute;ron sont &eacute;galement reproduites dans leur forme originale exacte, accompagn&eacute; par les versions anglaises de la traduction 1914 par H. Rackham.</p>\n', 'press-french', '1', '', '', 'yes', NULL, NULL, '0', ''),
(34, 0, 1, 'Politique de confidentialité', '<p><font><font>Lorem Ipsum is simply dummy text of the printing industry and composition.&nbsp;</font></font></p>\n\n<p><font><font>Lorem Ipsum has been the standard dummy text of the industry since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </font><font>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. </font><font>It was popularized in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&nbsp;</font></font></p>\n\n<p><font><font>The piece standard Lorem Ipsum used since the 1500s is reproduced&nbsp;</font></font></p>\n\n<p><font><font>The piece standard Lorem Ipsum used since the 1500s is reproduced below for those interested. </font><font>Sections 1.10.32 and 1.10.33 from &quot;Finibus Bonorum and of Malorum&quot; by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</font></font></p>\n', 'privacy-french', '1', 'Script de collecte de fonds Politique de confidentialité', 'Script de collecte de fonds Politique de confidentialité', 'yes', NULL, NULL, '0', ''),
(36, 0, 2, 'Conditions d''utilisation', '<p>Indiegogoclone&nbsp;<br />\nConditions d&#39;utilisation (&laquo;Accord&raquo;)&nbsp;<br />\nCet accord a &eacute;t&eacute; cr&eacute;&eacute; le 10 Septembre 2013.&nbsp;<br />\nBienvenue &agrave; Indiegogoclone, le site et le service en ligne de Indiegogoclone. En acc&eacute;dant ou en utilisant notre site Web, services et Indiegogoclone, contenu fournis par ou en relation avec les services fournis par notre site Web (collectivement, &laquo;Service&raquo;), vous, en tant qu&#39;utilisateur ou membres, indiquez que vous avez lu, compris et accept&eacute; d&#39;&ecirc;tre li&eacute; par ces conditions d&#39;utilisation (&quot;Accord&quot;), la politique de confidentialit&eacute; affich&eacute;e &agrave; Indiegogoclone avec toutes les autres r&egrave;gles de fonctionnement, les politiques et les proc&eacute;dures et les modalit&eacute;s et conditions suppl&eacute;mentaires publi&eacute;es sur notre site Web (qui sont incorpor&eacute;s par r&eacute;f&eacute;rence), et vous &ecirc;tes &acirc;g&eacute; d&#39;au moins 13 ans. Les termes sont d&eacute;finis dans le pr&eacute;sent Accord.&nbsp;</p>\n\n<p>Nous nous r&eacute;servons le droit de modifier le pr&eacute;sent Accord &agrave; tout moment et sans pr&eacute;avis. Si nous faisons cela, nous afficherons l&#39;Accord modifi&eacute; sur cette page et indiquer en haut de la page la date de l&#39;accord a &eacute;t&eacute; r&eacute;vis&eacute;e pour la derni&egrave;re. Votre utilisation continue du Service apr&egrave;s de telles modifications constitue votre acceptation de la nouvelle convention. Si vous n&#39;acceptez pas toutes du pr&eacute;sent Accord ou de toute modification au pr&eacute;sent Accord, ne pas utiliser ou d&#39;acc&egrave;s (ou de continuer &agrave; l&#39;acc&egrave;s) Service ou cesser imm&eacute;diatement toute utilisation du Service.&nbsp;<br />\nQui sommes-nous?&nbsp;<br />\nIndiegogoclone est une plate-forme de crowdfunding mondiale fond&eacute;e don-pour l&#39;agriculture, l&#39;autonomisation des agriculteurs et des &eacute;leveurs pour obtenir un financement non conventionnel pour les besoins classiques. Depuis le lancement en Septembre 2013, a permis Indiegogoclone projet Propri&eacute;taires de lancer des projets dans le monde entier. Indiegogoclone est en train de changer la fa&ccedil;on dont les gens amassent des fonds pour l&#39;agriculture - cr&eacute;atives, entrepreneuriales ou projets li&eacute;s &agrave; causer.&nbsp;<br />\nNous sommes une plate-forme ouverte. Vous comprenez et reconnaissez que vous pouvez &ecirc;tre expos&eacute; &agrave; un Contenu de membre qui peuvent &ecirc;tre inexacts, offensant, ind&eacute;cent ou choquant. Contenu de membres ne doit pas &ecirc;tre consid&eacute;r&eacute;e comme &eacute;tant une approbation ou une opinion partag&eacute;e par Indiegogoclone. Vous acceptez que Indiegogoclone n&#39;est pas responsable des dommages ou pertes r&eacute;sultant de ce Contenu.&nbsp;<br />\nLes utilisateurs de notre service et des membres</p>\n', 'Terms_of_service_french', '1', 'Script de collecte de fonds Termes et Conditions', 'Script de collecte de fonds Termes et Conditions', 'yes', NULL, NULL, '0', ''),
(37, 0, 1, 'Test page', '<p>Test pageTest pageTest pageTest pageTest pageTest pageTest pageTest pageTest pageTest pageTest pageTest pageTest pageTest pageTest pageTest pageTest pageTest pageTest pageTest pageTest pageTest pageTest page</p>\n', 'page', '1', '', '', 'yes', NULL, NULL, 'yes', ''),
(38, 0, 1, 'Test page', '<p>sfadfdsfaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaasdfasdfasdfasfasdfasdfasfddfasdfasfasdfasfasdfasdfaaaaa</p>\n', 'page', '1', '', '', 'yes', NULL, NULL, 'yes', ''),
(39, 0, 1, 'Test page', '<p>ggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg</p>\n\n<h4>Funding Options.Lesser Fees.</h4>\n\n<p>To create and start a campaign on Indiegogoclone is totally free. All you have to do is register and start a campaign. Choosing from the funding options we have, you may go with the Elastic Funding which lets you keep all of the funds raised from your campaign, even if you don&rsquo;t hit your target. Or, you may choose to go with the inelastic Funding, which refunds the donations back to your Backers should you don&rsquo;t hit your target. Whichever way, you pay nothing till you start raising funds.</p>\n\n<h4>Funding Options.Lesser Fees.</h4>\n\n<p>To create and start a campaign on Indiegogoclone is totally free. All you have to do is register and start a campaign. Choosing from the funding options we have, you may go with the Elastic Funding which lets you keep all of the funds raised from your campaign, even if you don&rsquo;t hit your target. Or, you may choose to go with the inelastic Funding, which refunds the donations back to your Backers should you don&rsquo;t hit your target. Whichever way, you pay nothing till you start raising funds.</p>\n\n<h4>Funding Options.Lesser Fees.</h4>\n\n<p>To create and start a campaign on Indiegogoclone is totally free. All you have to do is register and start a campaign. Choosing from the funding options we have, you may go with the Elastic Funding which lets you keep all of the funds raised from your campaign, even if you don&rsquo;t hit your target. Or, you may choose to go with the inelastic Funding, which refunds the donations back to your Backers should you don&rsquo;t hit your target. Whichever way, you pay nothing till you start raising funds.</p>\n\n<h4>Funding Options.Lesser Fees.</h4>\n\n<p>To create and start a campaign on Indiegogoclone is totally free. All you have to do is register and start a campaign. Choosing from the funding options we have, you may go with the Elastic Funding which lets you keep all of the funds raised from your campaign, even if you don&rsquo;t hit your target. Or, you may choose to go with the inelastic Funding, which refunds the donations back to your Backers should you don&rsquo;t hit your target. Whichever way, you pay nothing till you start raising funds.</p>\n\n<h4>Funding Options.Lesser Fees.</h4>\n\n<p>To create and start a campaign on Indiegogoclone is totally free. All you have to do is register and start a campaign. Choosing from the funding options we have, you may go with the Elastic Funding which lets you keep all of the funds raised from your campaign, even if you don&rsquo;t hit your target. Or, you may choose to go with the inelastic Funding, which refunds the donations back to your Backers should you don&rsquo;t hit your target. Whichever way, you pay nothing till you start raising funds.</p>\n\n<h4>Funding Options.Lesser Fees.</h4>\n\n<p>To create and start a campaign on Indiegogoclone is totally free. All you have to do is register and start a campaign. Choosing from the funding options we have, you may go with the Elastic Funding which lets you keep all of the funds raised from your campaign, even if you don&rsquo;t hit your target. Or, you may choose to go with the inelastic Funding, which refunds the donations back to your Backers should you don&rsquo;t hit your target. Whichever way, you pay nothing till you start raising funds.</p>\n\n<h4>&nbsp;</h4>\n', 'page', '1', '', '', '0', NULL, NULL, '0', '');

-- --------------------------------------------------------

--
-- Table structure for table `payments_gateways`
--

CREATE TABLE IF NOT EXISTS `payments_gateways` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `status` varchar(200) DEFAULT NULL,
  `image` text,
  `function_name` varchar(200) DEFAULT NULL,
  `suapport_masspayment` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `payments_gateways`
--

INSERT INTO `payments_gateways` (`id`, `name`, `status`, `image`, `function_name`, `suapport_masspayment`) VALUES
(1, 'Paypal', 'Active', 'fancybox-y.png', 'paypal', 'no'),
(4, 'Authorize.net(AIM)', 'Inactive', 'images.jpeg', 'auth_net_aim', 'no'),
(5, 'Wepay', 'Inactive', 'images.jpeg', 'wepay', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `payments_gateways_back`
--

CREATE TABLE IF NOT EXISTS `payments_gateways_back` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `status` varchar(200) DEFAULT NULL,
  `image` text,
  `function_name` varchar(200) DEFAULT NULL,
  `suapport_masspayment` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `payments_gateways_back`
--

INSERT INTO `payments_gateways_back` (`id`, `name`, `status`, `image`, `function_name`, `suapport_masspayment`) VALUES
(3, 'Amazon Payment', 'Inactive', 'fancy_shadow_nw.png', 'amazon_payment', 'no'),
(4, 'Authorize.net(AIM)', 'Inactive', 'images.jpeg', 'auth_net_aim', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `paypal`
--

CREATE TABLE IF NOT EXISTS `paypal` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `site_status` varchar(25) DEFAULT NULL,
  `application_id` varchar(255) DEFAULT NULL,
  `paypal_email` varchar(250) DEFAULT NULL,
  `paypal_username` varchar(150) DEFAULT NULL,
  `paypal_password` varchar(255) DEFAULT NULL,
  `paypal_signature` varchar(255) DEFAULT NULL,
  `preapproval` int(2) NOT NULL COMMENT 'instant = 0,preapprove =1',
  `fees_taken_from` varchar(50) DEFAULT NULL,
  `transaction_fees` double(10,2) NOT NULL,
  `gateway_status` int(2) NOT NULL,
  `donate_limit` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `paypal`
--

INSERT INTO `paypal` (`id`, `site_status`, `application_id`, `paypal_email`, `paypal_username`, `paypal_password`, `paypal_signature`, `preapproval`, `fees_taken_from`, `transaction_fees`, `gateway_status`, `donate_limit`) VALUES
(1, 'sandbox', 'APP-80W284485P519543T', 'mihir.test.rockersinfo@gmail.com', 'mihir.test.rockersinfo_api1.gmail.com', '1377498678', 'AFcWxV21C7fd0v3bYYYRCpSSRl31AWGT4755TL-t5vWTRKimjZgwA-3y', 1, 'PRIMARYRECEIVER', 5.00, 1, '2000.00');

-- --------------------------------------------------------

--
-- Table structure for table `paypal_credit_card`
--

CREATE TABLE IF NOT EXISTS `paypal_credit_card` (
  `paypal_credit_card_id` int(11) NOT NULL AUTO_INCREMENT,
  `credit_card_version` varchar(50) DEFAULT NULL,
  `credit_card_proxy_port` int(50) NOT NULL,
  `credit_card_proxy_host` varchar(100) DEFAULT NULL,
  `credit_card_use_proxy` int(20) NOT NULL DEFAULT '0' COMMENT '0=no,1=yes',
  `credit_card_subject` varchar(255) DEFAULT NULL,
  `credit_card_preapproval` int(20) NOT NULL DEFAULT '0' COMMENT '0=instant,1=preapprove',
  `credit_card_api_signature` varchar(255) DEFAULT NULL,
  `credit_card_username` varchar(255) DEFAULT NULL,
  `credit_card_password` varchar(255) DEFAULT NULL,
  `credit_card_site_status` int(20) NOT NULL COMMENT '0=sandbox,1=live',
  `credit_card_gateway_status` int(20) NOT NULL COMMENT '0=inactive,1=active',
  PRIMARY KEY (`paypal_credit_card_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `paypal_credit_card`
--

INSERT INTO `paypal_credit_card` (`paypal_credit_card_id`, `credit_card_version`, `credit_card_proxy_port`, `credit_card_proxy_host`, `credit_card_use_proxy`, `credit_card_subject`, `credit_card_preapproval`, `credit_card_api_signature`, `credit_card_username`, `credit_card_password`, `credit_card_site_status`, `credit_card_gateway_status`) VALUES
(1, '76.0', 808, '127.0.0.1', 0, '', 0, 'Abg0gYcQyxQvnf2HDJkKtA-p6pqhA1k-KTYE0Gcy1diujFio4io5Vqjf', 'platfo_1255077030_biz_api1.gmail.com', '1255077037', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `paypal_response_request`
--

CREATE TABLE IF NOT EXISTS `paypal_response_request` (
  `paypal_response_request_id` int(11) NOT NULL AUTO_INCREMENT,
  `paypal_request` text CHARACTER SET utf8 NOT NULL,
  `paypal_response` text CHARACTER SET utf8 NOT NULL,
  `project_id` int(11) NOT NULL,
  `donar_id` int(11) NOT NULL,
  `transaction_status` varchar(255) CHARACTER SET utf8 NOT NULL,
  `insert_date` datetime NOT NULL,
  PRIMARY KEY (`paypal_response_request_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1019 ;

--
-- Dumping data for table `paypal_response_request`
--

INSERT INTO `paypal_response_request` (`paypal_response_request_id`, `paypal_request`, `paypal_response`, `project_id`, `donar_id`, `transaction_status`, `insert_date`) VALUES
(1014, 'cancelUrl=http%3A%2F%2Fwww.groupfund.me%2Findex.php%2Fhome&returnUrl=http%3A%2F%2Fwww.groupfund.me%2Findex.php%2Fadaptive_paypal%2Fpreapprovereceipt&currencyCode=USD&startingDate=2014-10-30&endingDate=2015-02-14&maxNumberOfPayments=1&maxTotalAmountOfAllPayments=300.00&requestEnvelope.errorLanguage=en_US&memo=Project+%3A+Political+parties%2C+campaigning+%26amp%3B+donations%284%29%2C+Amount+%3A+300.00%2C+Project+Owner+%3A+ketan+patel%2C+On+Site+%3A+http%3A%2F%2Fwww.groupfund.me%2Findex.php', 'Request From payment case 1 503::responseEnvelope.timestamp=2014-10-30T01:01:28.554-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=e88dafc164cd7,responseEnvelope.build=13414382,preapprovalKey=PA-91C99300PS596063J,', 4, 5, 'SUCCESS', '2014-10-30 03:01:28'),
(1015, 'preapprovalKey=PA-91C99300PS596063J&requestEnvelope.errorLanguage=en_US', 'Response From preapprovereceipt 854::responseEnvelope.timestamp=2014-10-30T01:02:53.289-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=e258160a0b207,responseEnvelope.build=13414382,approved=true,cancelUrl=http://www.groupfund.me/index.php/home,curPayments=0,curPaymentsAmount=0.00,curPeriodAttempts=0,currencyCode=USD,dateOfMonth=0,dayOfWeek=NO_DAY_SPECIFIED,endingDate=2015-02-14T23:59:28.000-08:00,maxNumberOfPayments=1,maxTotalAmountOfAllPayments=300.00,paymentPeriod=NO_PERIOD_SPECIFIED,pinType=NOT_REQUIRED,returnUrl=http://www.groupfund.me/index.php/adaptive_paypal/preapprovereceipt,senderEmail=anita.rockersinfo@gmail.com,memo=Project : Political parties, campaigning &amp; donations(4), Amount : 300.00, Project Owner : ketan patel, On Site : http://www.groupfund.me/index.php,startingDate=2014-10-30T00:00:28.000-07:00,status=ACTIVE,displayMaxTotalAmount=false,sender.accountId=LXU9FUADSCKUJ,', 4, 5, 'SUCCESS', '2014-10-30 03:02:53'),
(999, 'cancelUrl=http%3A%2F%2Fwww.groupfund.me%2Findex.php%2Fhome&returnUrl=http%3A%2F%2Fwww.groupfund.me%2Findex.php%2Fadaptive_paypal%2Fpreapprovereceipt&currencyCode=USD&startingDate=2014-10-29&endingDate=2015-01-21&maxNumberOfPayments=1&maxTotalAmountOfAllPayments=100.00&requestEnvelope.errorLanguage=en_US&memo=Project+%3A+Olive+Chapel+Elementary+receives+%2410%2C000+donation+for+technology%285%29%2C+Amount+%3A+100.00%2C+Project+Owner+%3A+ketan+patel%2C+On+Site+%3A+http%3A%2F%2Fwww.groupfund.me%2Findex.php', 'Request From payment case 1 503::responseEnvelope.timestamp=2014-10-29T20:58:07.766-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=be8f2bcf7d484,responseEnvelope.build=13414382,preapprovalKey=PA-4H5107423D623741E,', 5, 5, 'SUCCESS', '2014-10-29 22:58:07'),
(1000, 'cancelUrl=http%3A%2F%2Fwww.groupfund.me%2Findex.php%2Fhome&returnUrl=http%3A%2F%2Fwww.groupfund.me%2Findex.php%2Fadaptive_paypal%2Fpreapprovereceipt&currencyCode=USD&startingDate=2014-10-29&endingDate=2015-01-21&maxNumberOfPayments=1&maxTotalAmountOfAllPayments=100.00&requestEnvelope.errorLanguage=en_US&memo=Project+%3A+Olive+Chapel+Elementary+receives+%2410%2C000+donation+for+technology%285%29%2C+Amount+%3A+100.00%2C+Project+Owner+%3A+ketan+patel%2C+On+Site+%3A+http%3A%2F%2Fwww.groupfund.me%2Findex.php', 'Request From payment case 1 503::responseEnvelope.timestamp=2014-10-29T21:54:54.240-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=6accc76bc0669,responseEnvelope.build=13414382,preapprovalKey=PA-36C35634KH771500C,', 5, 5, 'SUCCESS', '2014-10-29 23:54:54'),
(1001, 'preapprovalKey=PA-36C35634KH771500C&requestEnvelope.errorLanguage=en_US', 'Response From preapprovereceipt 854::responseEnvelope.timestamp=2014-10-29T21:56:45.600-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=c4f880d9dad4a,responseEnvelope.build=13414382,approved=true,cancelUrl=http://www.groupfund.me/index.php/home,curPayments=0,curPaymentsAmount=0.00,curPeriodAttempts=0,currencyCode=USD,dateOfMonth=0,dayOfWeek=NO_DAY_SPECIFIED,endingDate=2015-01-21T23:59:54.000-08:00,maxNumberOfPayments=1,maxTotalAmountOfAllPayments=100.00,paymentPeriod=NO_PERIOD_SPECIFIED,pinType=NOT_REQUIRED,returnUrl=http://www.groupfund.me/index.php/adaptive_paypal/preapprovereceipt,senderEmail=anita.rockersinfo@gmail.com,memo=Project : Olive Chapel Elementary receives $10,000 donation for technology(5), Amount : 100.00, Project Owner : ketan patel, On Site : http://www.groupfund.me/index.php,startingDate=2014-10-29T00:00:54.000-07:00,status=ACTIVE,displayMaxTotalAmount=false,sender.accountId=LXU9FUADSCKUJ,', 5, 5, 'SUCCESS', '2014-10-29 23:56:45'),
(992, 'actionType=PAY&cancelUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fcron%2Fcron_preapprove&returnUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fcron%2Fcron_preapprove&ipnNotificationUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fcron%2Fcron_preapprove&currencyCode=USD&clientDetails.deviceId=mydevice&clientDetails.ipAddress=113.193.174.213&clientDetails.applicationId=APP-80W284485P519543T&requestEnvelope.errorLanguage=en_US&memo=Project+%3A+Funders+test%28146%29%2C+Amount+%3A+40.00%2CProject+Creator+Pay+Fees%28%25%29+%3A+11.00%2C+Create+By+%3A+Jayshree+Patel%2C+On+Site+%3A+http%3A%2F%2Fgroupfund.me%2F&feesPayer=PRIMARYRECEIVER&receiverList.receiver%5B0%5D.email=ankit.rockersinfo%40gmail.com&receiverList.receiver%5B0%5D.amount=40.00&receiverList.receiver%5B0%5D.primary%5B0%5D=true&receiverList.receiver%5B1%5D.email=mihir.test.rockersinfo%40gmail.com&receiverList.receiver%5B1%5D.amount=4.40&receiverList.receiver%5B1%5D.primary%5B1%5D=false&preapprovalKey=PA-4TA73880DC695914C&senderEmail=anita.rockersinfo%40gmail.com', 'Response From cron_preapprove 320::responseEnvelope.timestamp=2014-09-30T01:22:51.751-07:00,responseEnvelope.ack=Failure,responseEnvelope.correlationId=0257ec82d7acc,responseEnvelope.build=12595496,error(0).errorId=579024,error(0).domain=PLATFORM,error(0).subdomain=Application,error(0).severity=Error,error(0).category=Application,error(0).message=The preapproval key can''t be used before the start date or after the end date,', 146, 0, 'FAILURE', '2014-09-30 03:22:51'),
(993, 'actionType=PAY&cancelUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fcron%2Fcron_preapprove&returnUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fcron%2Fcron_preapprove&ipnNotificationUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fcron%2Fcron_preapprove&currencyCode=USD&clientDetails.deviceId=mydevice&clientDetails.ipAddress=113.193.174.213&clientDetails.applicationId=APP-80W284485P519543T&requestEnvelope.errorLanguage=en_US&memo=Project+%3A+Funders+test%28146%29%2C+Amount+%3A+100.00%2CProject+Creator+Pay+Fees%28%25%29+%3A+11.00%2C+Create+By+%3A+Jayshree+Patel%2C+On+Site+%3A+http%3A%2F%2Fgroupfund.me%2F&feesPayer=PRIMARYRECEIVER&receiverList.receiver%5B0%5D.email=ankit.rockersinfo%40gmail.com&receiverList.receiver%5B0%5D.amount=100.00&receiverList.receiver%5B0%5D.primary%5B0%5D=true&receiverList.receiver%5B1%5D.email=mihir.test.rockersinfo%40gmail.com&receiverList.receiver%5B1%5D.amount=11.00&receiverList.receiver%5B1%5D.primary%5B1%5D=false&preapprovalKey=PA-81A81619R9857240E&senderEmail=anita.rockersinfo%40gmail.com', 'Response From cron_preapprove 320::responseEnvelope.timestamp=2014-09-30T01:22:54.204-07:00,responseEnvelope.ack=Failure,responseEnvelope.correlationId=5290f5b7dfd77,responseEnvelope.build=12595496,error(0).errorId=579024,error(0).domain=PLATFORM,error(0).subdomain=Application,error(0).severity=Error,error(0).category=Application,error(0).message=The preapproval key can''t be used before the start date or after the end date,', 146, 0, 'FAILURE', '2014-09-30 03:22:54'),
(974, 'cancelUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fhome&returnUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fadaptive_paypal%2Fpreapprovereceipt&currencyCode=USD&startingDate=2014-09-15&endingDate=2014-09-18&maxNumberOfPayments=1&maxTotalAmountOfAllPayments=100.00&requestEnvelope.errorLanguage=en_US&memo=Project+%3A+Funders+test%28146%29%2C+Amount+%3A+100.00%2C+Project+Owner+%3A+Jayshree+Patel%2C+On+Site+%3A+http%3A%2F%2Fgroupfund.me%2Findex.php', 'Request From payment case 1 503::responseEnvelope.timestamp=2014-09-15T23:41:26.444-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=ed852d806c929,responseEnvelope.build=12595496,preapprovalKey=PA-37F32489D2646243K,', 146, 91, 'SUCCESS', '2014-09-16 01:41:26'),
(973, 'cancelUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fhome&returnUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fadaptive_paypal%2Fpreapprovereceipt&currencyCode=USD&startingDate=2014-09-13&endingDate=2014-09-13&maxNumberOfPayments=1&maxTotalAmountOfAllPayments=550.00&requestEnvelope.errorLanguage=en_US&memo=Project+%3A+Project+ending+soon+days+test%28133%29%2C+Amount+%3A+550.00%2C+Project+Owner+%3A+Jayshree+Patel%2C+On+Site+%3A+http%3A%2F%2Fgroupfund.me%2Findex.php', 'Request From payment case 1 503::responseEnvelope.timestamp=2014-09-13T03:26:23.645-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=a649f99f01306,responseEnvelope.build=12595496,preapprovalKey=PA-1J77930789098570N,', 133, 91, 'SUCCESS', '2014-09-13 05:26:23'),
(972, 'actionType=PAY&cancelUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fcron%2Fcron_preapprove&returnUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fcron%2Fcron_preapprove&ipnNotificationUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fcron%2Fcron_preapprove&currencyCode=USD&clientDetails.deviceId=mydevice&clientDetails.ipAddress=1.22.82.247&clientDetails.applicationId=APP-80W284485P519543T&requestEnvelope.errorLanguage=en_US&memo=Project+%3A+Ending+soon+test%28137%29%2C+Amount+%3A+900.00%2CProject+Creator+Pay+Fees%28%25%29+%3A+5.00%2C+Create+By+%3A+ankit+patel%2C+On+Site+%3A+http%3A%2F%2Fgroupfund.me%2F&feesPayer=PRIMARYRECEIVER&receiverList.receiver%5B0%5D.email=ankit.rockersinfo%40gmail.com&receiverList.receiver%5B0%5D.amount=900.00&receiverList.receiver%5B0%5D.primary%5B0%5D=true&receiverList.receiver%5B1%5D.email=mihir.test.rockersinfo%40gmail.com&receiverList.receiver%5B1%5D.amount=45.00&receiverList.receiver%5B1%5D.primary%5B1%5D=false&preapprovalKey=PA-36322316G53600941&senderEmail=anita.rockersinfo%40gmail.com', 'Response from Cron preapprival_cron 709::responseEnvelope.timestamp=2014-09-12T05:34:54.689-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=10489cb32826b,responseEnvelope.build=12595496,payKey=AP-6S006780R81814833,paymentExecStatus=COMPLETED,paymentInfoList.paymentInfo(0).transactionId=9ES490026Y067540A,paymentInfoList.paymentInfo(0).transactionStatus=COMPLETED,paymentInfoList.paymentInfo(0).receiver.amount=900.00,paymentInfoList.paymentInfo(0).receiver.email=ankit.rockersinfo@gmail.com,paymentInfoList.paymentInfo(0).receiver.primary=true,paymentInfoList.paymentInfo(0).receiver.accountId=FC6QJ72EBYT6G,paymentInfoList.paymentInfo(0).pendingRefund=false,paymentInfoList.paymentInfo(0).senderTransactionId=2U921864L4973981V,paymentInfoList.paymentInfo(0).senderTransactionStatus=COMPLETED,paymentInfoList.paymentInfo(1).transactionId=2A389334D4232041J,paymentInfoList.paymentInfo(1).transactionStatus=COMPLETED,paymentInfoList.paymentInfo(1).receiver.amount=45.00,paymentInfoList.paymentInfo(1).receiver.email=mihir.test.rockersinfo@gmail.com,paymentInfoList.paymentInfo(1).receiver.primary=false,paymentInfoList.paymentInfo(1).receiver.accountId=7GZC89QQLL4EE,paymentInfoList.paymentInfo(1).pendingRefund=false,paymentInfoList.paymentInfo(1).senderTransactionId=3PE0598843446834Y,paymentInfoList.paymentInfo(1).senderTransactionStatus=COMPLETED,sender.accountId=LXU9FUADSCKUJ,', 137, 0, 'SUCCESS', '2014-09-12 07:34:54'),
(970, 'preapprovalKey=PA-36322316G53600941&requestEnvelope.errorLanguage=en_US', 'Response From preapprovereceipt 854::responseEnvelope.timestamp=2014-09-12T05:32:15.736-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=cd1c942c0d9d9,responseEnvelope.build=12595496,approved=true,cancelUrl=http://groupfund.me/index.php/home,curPayments=0,curPaymentsAmount=0.00,curPeriodAttempts=0,currencyCode=USD,dateOfMonth=0,dayOfWeek=NO_DAY_SPECIFIED,endingDate=2014-09-22T23:59:26.000-07:00,maxNumberOfPayments=1,maxTotalAmountOfAllPayments=900.00,paymentPeriod=NO_PERIOD_SPECIFIED,pinType=NOT_REQUIRED,returnUrl=http://groupfund.me/index.php/adaptive_paypal/preapprovereceipt,senderEmail=anita.rockersinfo@gmail.com,memo=Project : Ending soon test(137), Amount : 900.00, Project Owner : Jayshree Patel, On Site : http://groupfund.me/index.php,startingDate=2014-09-12T00:00:26.000-07:00,status=ACTIVE,displayMaxTotalAmount=false,sender.accountId=LXU9FUADSCKUJ,', 137, 6, 'SUCCESS', '2014-09-12 07:32:15'),
(971, 'actionType=PAY&cancelUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fcron%2Fcron_preapprove&returnUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fcron%2Fcron_preapprove&ipnNotificationUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fcron%2Fcron_preapprove&currencyCode=USD&clientDetails.deviceId=mydevice&clientDetails.ipAddress=1.22.82.247&clientDetails.applicationId=APP-80W284485P519543T&requestEnvelope.errorLanguage=en_US&memo=Project+%3A+Ending+soon+test%28137%29%2C+Amount+%3A+100.00%2CProject+Creator+Pay+Fees%28%25%29+%3A+5.00%2C+Create+By+%3A+ankit+patel%2C+On+Site+%3A+http%3A%2F%2Fgroupfund.me%2F&feesPayer=PRIMARYRECEIVER&receiverList.receiver%5B0%5D.email=ankit.rockersinfo%40gmail.com&receiverList.receiver%5B0%5D.amount=100.00&receiverList.receiver%5B0%5D.primary%5B0%5D=true&receiverList.receiver%5B1%5D.email=mihir.test.rockersinfo%40gmail.com&receiverList.receiver%5B1%5D.amount=5.00&receiverList.receiver%5B1%5D.primary%5B1%5D=false&preapprovalKey=PA-3FE22526H88019024&senderEmail=anita.rockersinfo%40gmail.com', 'Response from Cron preapprival_cron 709::responseEnvelope.timestamp=2014-09-12T05:34:48.170-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=122959752482e,responseEnvelope.build=12595496,payKey=AP-01P49490856048054,paymentExecStatus=COMPLETED,paymentInfoList.paymentInfo(0).transactionId=2ST96774PE934761G,paymentInfoList.paymentInfo(0).transactionStatus=COMPLETED,paymentInfoList.paymentInfo(0).receiver.amount=100.00,paymentInfoList.paymentInfo(0).receiver.email=ankit.rockersinfo@gmail.com,paymentInfoList.paymentInfo(0).receiver.primary=true,paymentInfoList.paymentInfo(0).receiver.accountId=FC6QJ72EBYT6G,paymentInfoList.paymentInfo(0).pendingRefund=false,paymentInfoList.paymentInfo(0).senderTransactionId=4E11102695655060N,paymentInfoList.paymentInfo(0).senderTransactionStatus=COMPLETED,paymentInfoList.paymentInfo(1).transactionId=056737748L112672V,paymentInfoList.paymentInfo(1).transactionStatus=COMPLETED,paymentInfoList.paymentInfo(1).receiver.amount=5.00,paymentInfoList.paymentInfo(1).receiver.email=mihir.test.rockersinfo@gmail.com,paymentInfoList.paymentInfo(1).receiver.primary=false,paymentInfoList.paymentInfo(1).receiver.accountId=7GZC89QQLL4EE,paymentInfoList.paymentInfo(1).pendingRefund=false,paymentInfoList.paymentInfo(1).senderTransactionId=5NV773022U506100W,paymentInfoList.paymentInfo(1).senderTransactionStatus=COMPLETED,sender.accountId=LXU9FUADSCKUJ,', 137, 0, 'SUCCESS', '2014-09-12 07:34:48'),
(966, 'actionType=PAY&cancelUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fcron%2Fcron_preapprove&returnUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fcron%2Fcron_preapprove&ipnNotificationUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fcron%2Fcron_preapprove&currencyCode=USD&clientDetails.deviceId=mydevice&clientDetails.ipAddress=1.22.82.247&clientDetails.applicationId=APP-80W284485P519543T&requestEnvelope.errorLanguage=en_US&memo=Project+%3A+Test+paypal+condition%28141%29%2C+Amount+%3A+100.00%2CProject+Creator+Pay+Fees%28%25%29+%3A+11.00%2C+Create+By+%3A+Jayshree+Patel%2C+On+Site+%3A+http%3A%2F%2Fgroupfund.me%2F&feesPayer=PRIMARYRECEIVER&receiverList.receiver%5B0%5D.email=ankit.rockersinfo%40gmail.com&receiverList.receiver%5B0%5D.amount=100.00&receiverList.receiver%5B0%5D.primary%5B0%5D=true&receiverList.receiver%5B1%5D.email=mihir.test.rockersinfo%40gmail.com&receiverList.receiver%5B1%5D.amount=11.00&receiverList.receiver%5B1%5D.primary%5B1%5D=false&preapprovalKey=PA-77H73992CS6270827&senderEmail=anita.rockersinfo%40gmail.com', 'Response From cron_preapprove 320::responseEnvelope.timestamp=2014-09-11T22:51:02.836-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=a6e087d7255b1,responseEnvelope.build=12595496,payKey=AP-5JB67685UA930273R,paymentExecStatus=COMPLETED,paymentInfoList.paymentInfo(0).transactionId=7HH500101L134763S,paymentInfoList.paymentInfo(0).transactionStatus=COMPLETED,paymentInfoList.paymentInfo(0).receiver.amount=100.00,paymentInfoList.paymentInfo(0).receiver.email=ankit.rockersinfo@gmail.com,paymentInfoList.paymentInfo(0).receiver.primary=true,paymentInfoList.paymentInfo(0).receiver.accountId=FC6QJ72EBYT6G,paymentInfoList.paymentInfo(0).pendingRefund=false,paymentInfoList.paymentInfo(0).senderTransactionId=48Y606172X871583H,paymentInfoList.paymentInfo(0).senderTransactionStatus=COMPLETED,paymentInfoList.paymentInfo(1).transactionId=5LP77155RK436583M,paymentInfoList.paymentInfo(1).transactionStatus=COMPLETED,paymentInfoList.paymentInfo(1).receiver.amount=11.00,paymentInfoList.paymentInfo(1).receiver.email=mihir.test.rockersinfo@gmail.com,paymentInfoList.paymentInfo(1).receiver.primary=false,paymentInfoList.paymentInfo(1).receiver.accountId=7GZC89QQLL4EE,paymentInfoList.paymentInfo(1).pendingRefund=false,paymentInfoList.paymentInfo(1).senderTransactionId=9EM24042CT559212L,paymentInfoList.paymentInfo(1).senderTransactionStatus=COMPLETED,sender.accountId=LXU9FUADSCKUJ,', 141, 0, 'SUCCESS', '2014-09-12 00:51:02'),
(967, 'cancelUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fhome&returnUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fadaptive_paypal%2Fpreapprovereceipt&currencyCode=USD&startingDate=2014-09-12&endingDate=2014-09-22&maxNumberOfPayments=1&maxTotalAmountOfAllPayments=100.00&requestEnvelope.errorLanguage=en_US&memo=Project+%3A+Ending+soon+test%28137%29%2C+Amount+%3A+100.00%2C+Project+Owner+%3A+Jayshree+Patel%2C+On+Site+%3A+http%3A%2F%2Fgroupfund.me%2Findex.php', 'Request From payment case 1 503::responseEnvelope.timestamp=2014-09-12T05:18:14.069-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=3cb8570e5fdb3,responseEnvelope.build=12595496,preapprovalKey=PA-3FE22526H88019024,', 137, 6, 'SUCCESS', '2014-09-12 07:18:14'),
(968, 'preapprovalKey=PA-3FE22526H88019024&requestEnvelope.errorLanguage=en_US', 'Response From preapprovereceipt 854::responseEnvelope.timestamp=2014-09-12T05:19:14.963-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=6222cda54cfd3,responseEnvelope.build=12595496,approved=true,cancelUrl=http://groupfund.me/index.php/home,curPayments=0,curPaymentsAmount=0.00,curPeriodAttempts=0,currencyCode=USD,dateOfMonth=0,dayOfWeek=NO_DAY_SPECIFIED,endingDate=2014-09-22T23:59:13.000-07:00,maxNumberOfPayments=1,maxTotalAmountOfAllPayments=100.00,paymentPeriod=NO_PERIOD_SPECIFIED,pinType=NOT_REQUIRED,returnUrl=http://groupfund.me/index.php/adaptive_paypal/preapprovereceipt,senderEmail=anita.rockersinfo@gmail.com,memo=Project : Ending soon test(137), Amount : 100.00, Project Owner : Jayshree Patel, On Site : http://groupfund.me/index.php,startingDate=2014-09-12T00:00:13.000-07:00,status=ACTIVE,displayMaxTotalAmount=false,sender.accountId=LXU9FUADSCKUJ,', 137, 6, 'SUCCESS', '2014-09-12 07:19:15'),
(969, 'cancelUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fhome&returnUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fadaptive_paypal%2Fpreapprovereceipt&currencyCode=USD&startingDate=2014-09-12&endingDate=2014-09-22&maxNumberOfPayments=1&maxTotalAmountOfAllPayments=900.00&requestEnvelope.errorLanguage=en_US&memo=Project+%3A+Ending+soon+test%28137%29%2C+Amount+%3A+900.00%2C+Project+Owner+%3A+Jayshree+Patel%2C+On+Site+%3A+http%3A%2F%2Fgroupfund.me%2Findex.php', 'Request From payment case 1 503::responseEnvelope.timestamp=2014-09-12T05:30:26.965-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=191dee7fe52a5,responseEnvelope.build=12595496,preapprovalKey=PA-36322316G53600941,', 137, 6, 'SUCCESS', '2014-09-12 07:30:26'),
(964, 'preapprovalKey=PA-77H73992CS6270827&requestEnvelope.errorLanguage=en_US', 'Response From preapprovereceipt 854::responseEnvelope.timestamp=2014-09-11T22:37:45.948-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=97f7909867330,responseEnvelope.build=12595496,approved=true,cancelUrl=http://groupfund.me/index.php/home,curPayments=0,curPaymentsAmount=0.00,curPeriodAttempts=0,currencyCode=USD,dateOfMonth=0,dayOfWeek=NO_DAY_SPECIFIED,endingDate=2014-09-14T23:59:33.000-07:00,maxNumberOfPayments=1,maxTotalAmountOfAllPayments=100.00,paymentPeriod=NO_PERIOD_SPECIFIED,pinType=NOT_REQUIRED,returnUrl=http://groupfund.me/index.php/adaptive_paypal/preapprovereceipt,senderEmail=anita.rockersinfo@gmail.com,memo=Project : Test paypal condition(141), Amount : 100.00, Project Owner : Jayshree Patel, On Site : http://groupfund.me/index.php,startingDate=2014-09-11T00:00:33.000-07:00,status=ACTIVE,displayMaxTotalAmount=false,sender.accountId=LXU9FUADSCKUJ,', 141, 6, 'SUCCESS', '2014-09-12 00:37:46'),
(965, 'actionType=PAY&cancelUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fcron%2Fcron_preapprove&returnUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fcron%2Fcron_preapprove&ipnNotificationUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fcron%2Fcron_preapprove&currencyCode=USD&clientDetails.deviceId=mydevice&clientDetails.ipAddress=1.22.82.247&clientDetails.applicationId=APP-80W284485P519543T&requestEnvelope.errorLanguage=en_US&memo=Project+%3A+Test+paypal+condition%28141%29%2C+Amount+%3A+700.00%2CProject+Creator+Pay+Fees%28%25%29+%3A+11.00%2C+Create+By+%3A+Jayshree+Patel%2C+On+Site+%3A+http%3A%2F%2Fgroupfund.me%2F&feesPayer=PRIMARYRECEIVER&receiverList.receiver%5B0%5D.email=ankit.rockersinfo%40gmail.com&receiverList.receiver%5B0%5D.amount=700.00&receiverList.receiver%5B0%5D.primary%5B0%5D=true&receiverList.receiver%5B1%5D.email=mihir.test.rockersinfo%40gmail.com&receiverList.receiver%5B1%5D.amount=77.00&receiverList.receiver%5B1%5D.primary%5B1%5D=false&preapprovalKey=PA-9LM508186S810083T&senderEmail=anita.rockersinfo%40gmail.com', 'Response From cron_preapprove 320::responseEnvelope.timestamp=2014-09-11T22:50:57.593-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=351fceb82875a,responseEnvelope.build=12595496,payKey=AP-0CB26979H8980635N,paymentExecStatus=COMPLETED,paymentInfoList.paymentInfo(0).transactionId=7N865633677513447,paymentInfoList.paymentInfo(0).transactionStatus=COMPLETED,paymentInfoList.paymentInfo(0).receiver.amount=700.00,paymentInfoList.paymentInfo(0).receiver.email=ankit.rockersinfo@gmail.com,paymentInfoList.paymentInfo(0).receiver.primary=true,paymentInfoList.paymentInfo(0).receiver.accountId=FC6QJ72EBYT6G,paymentInfoList.paymentInfo(0).pendingRefund=false,paymentInfoList.paymentInfo(0).senderTransactionId=3PP84442NR268115F,paymentInfoList.paymentInfo(0).senderTransactionStatus=COMPLETED,paymentInfoList.paymentInfo(1).transactionId=00X32853EX9163803,paymentInfoList.paymentInfo(1).transactionStatus=COMPLETED,paymentInfoList.paymentInfo(1).receiver.amount=77.00,paymentInfoList.paymentInfo(1).receiver.email=mihir.test.rockersinfo@gmail.com,paymentInfoList.paymentInfo(1).receiver.primary=false,paymentInfoList.paymentInfo(1).receiver.accountId=7GZC89QQLL4EE,paymentInfoList.paymentInfo(1).pendingRefund=false,paymentInfoList.paymentInfo(1).senderTransactionId=4HG46087HC157004W,paymentInfoList.paymentInfo(1).senderTransactionStatus=COMPLETED,sender.accountId=LXU9FUADSCKUJ,', 141, 0, 'SUCCESS', '2014-09-12 00:50:57'),
(962, 'preapprovalKey=PA-9LM508186S810083T&requestEnvelope.errorLanguage=en_US', 'Response From preapprovereceipt 854::responseEnvelope.timestamp=2014-09-11T22:35:49.891-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=0d2fecf64072c,responseEnvelope.build=12595496,approved=true,cancelUrl=http://groupfund.me/index.php/home,curPayments=0,curPaymentsAmount=0.00,curPeriodAttempts=0,currencyCode=USD,dateOfMonth=0,dayOfWeek=NO_DAY_SPECIFIED,endingDate=2014-09-14T23:59:49.000-07:00,maxNumberOfPayments=1,maxTotalAmountOfAllPayments=700.00,paymentPeriod=NO_PERIOD_SPECIFIED,pinType=NOT_REQUIRED,returnUrl=http://groupfund.me/index.php/adaptive_paypal/preapprovereceipt,senderEmail=anita.rockersinfo@gmail.com,memo=Project : Test paypal condition(141), Amount : 700.00, Project Owner : Jayshree Patel, On Site : http://groupfund.me/index.php,startingDate=2014-09-11T00:00:49.000-07:00,status=ACTIVE,displayMaxTotalAmount=false,sender.accountId=LXU9FUADSCKUJ,', 141, 6, 'SUCCESS', '2014-09-12 00:35:49'),
(963, 'cancelUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fhome&returnUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fadaptive_paypal%2Fpreapprovereceipt&currencyCode=USD&startingDate=2014-09-11&endingDate=2014-09-14&maxNumberOfPayments=1&maxTotalAmountOfAllPayments=100.00&requestEnvelope.errorLanguage=en_US&memo=Project+%3A+Test+paypal+condition%28141%29%2C+Amount+%3A+100.00%2C+Project+Owner+%3A+Jayshree+Patel%2C+On+Site+%3A+http%3A%2F%2Fgroupfund.me%2Findex.php', 'Request From payment case 1 503::responseEnvelope.timestamp=2014-09-11T22:36:33.643-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=5d70ef3149e4a,responseEnvelope.build=12595496,preapprovalKey=PA-77H73992CS6270827,', 141, 6, 'SUCCESS', '2014-09-12 00:36:33'),
(960, 'preapprovalKey=PA-9LM508186S810083T&requestEnvelope.errorLanguage=en_US', 'Response From preapprovereceipt 854::responseEnvelope.timestamp=2014-09-11T22:35:31.168-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=e7720e2d3c5da,responseEnvelope.build=12595496,approved=true,cancelUrl=http://groupfund.me/index.php/home,curPayments=0,curPaymentsAmount=0.00,curPeriodAttempts=0,currencyCode=USD,dateOfMonth=0,dayOfWeek=NO_DAY_SPECIFIED,endingDate=2014-09-14T23:59:49.000-07:00,maxNumberOfPayments=1,maxTotalAmountOfAllPayments=700.00,paymentPeriod=NO_PERIOD_SPECIFIED,pinType=NOT_REQUIRED,returnUrl=http://groupfund.me/index.php/adaptive_paypal/preapprovereceipt,senderEmail=anita.rockersinfo@gmail.com,memo=Project : Test paypal condition(141), Amount : 700.00, Project Owner : Jayshree Patel, On Site : http://groupfund.me/index.php,startingDate=2014-09-11T00:00:49.000-07:00,status=ACTIVE,displayMaxTotalAmount=false,sender.accountId=LXU9FUADSCKUJ,', 141, 6, 'SUCCESS', '2014-09-12 00:35:31'),
(961, 'preapprovalKey=PA-9LM508186S810083T&requestEnvelope.errorLanguage=en_US', 'Response From preapprovereceipt 854::responseEnvelope.timestamp=2014-09-11T22:35:36.041-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=68b57a9b448f2,responseEnvelope.build=12595496,approved=true,cancelUrl=http://groupfund.me/index.php/home,curPayments=0,curPaymentsAmount=0.00,curPeriodAttempts=0,currencyCode=USD,dateOfMonth=0,dayOfWeek=NO_DAY_SPECIFIED,endingDate=2014-09-14T23:59:49.000-07:00,maxNumberOfPayments=1,maxTotalAmountOfAllPayments=700.00,paymentPeriod=NO_PERIOD_SPECIFIED,pinType=NOT_REQUIRED,returnUrl=http://groupfund.me/index.php/adaptive_paypal/preapprovereceipt,senderEmail=anita.rockersinfo@gmail.com,memo=Project : Test paypal condition(141), Amount : 700.00, Project Owner : Jayshree Patel, On Site : http://groupfund.me/index.php,startingDate=2014-09-11T00:00:49.000-07:00,status=ACTIVE,displayMaxTotalAmount=false,sender.accountId=LXU9FUADSCKUJ,', 141, 6, 'SUCCESS', '2014-09-12 00:35:36'),
(959, 'cancelUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fhome&returnUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fadaptive_paypal%2Fpreapprovereceipt&currencyCode=USD&startingDate=2014-09-11&endingDate=2014-09-14&maxNumberOfPayments=1&maxTotalAmountOfAllPayments=700.00&requestEnvelope.errorLanguage=en_US&memo=Project+%3A+Test+paypal+condition%28141%29%2C+Amount+%3A+700.00%2C+Project+Owner+%3A+Jayshree+Patel%2C+On+Site+%3A+http%3A%2F%2Fgroupfund.me%2Findex.php', 'Request From payment case 1 503::responseEnvelope.timestamp=2014-09-11T22:34:49.468-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=bbc7e3de327ca,responseEnvelope.build=12595496,preapprovalKey=PA-9LM508186S810083T,', 141, 6, 'SUCCESS', '2014-09-12 00:34:49'),
(988, 'cancelUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fhome&returnUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fadaptive_paypal%2Fpreapprovereceipt&currencyCode=USD&startingDate=2014-09-16&endingDate=2014-09-18&maxNumberOfPayments=1&maxTotalAmountOfAllPayments=189.00&requestEnvelope.errorLanguage=en_US&memo=Project+%3A+Funders+test%28146%29%2C+Amount+%3A+189.00%2C+Project+Owner+%3A+Jayshree+Patel%2C+On+Site+%3A+http%3A%2F%2Fgroupfund.me%2Findex.php', 'Request From payment case 1 503::responseEnvelope.timestamp=2014-09-16T04:11:02.712-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=1e7ab062f54fe,responseEnvelope.build=12595496,preapprovalKey=PA-0WN999028J378840C,', 146, 91, 'SUCCESS', '2014-09-16 06:11:02'),
(989, 'preapprovalKey=PA-0WN999028J378840C&requestEnvelope.errorLanguage=en_US', 'Response From preapprovereceipt 854::responseEnvelope.timestamp=2014-09-16T04:11:47.524-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=bfc6ef38eee40,responseEnvelope.build=12595496,approved=true,cancelUrl=http://groupfund.me/index.php/home,curPayments=0,curPaymentsAmount=0.00,curPeriodAttempts=0,currencyCode=USD,dateOfMonth=0,dayOfWeek=NO_DAY_SPECIFIED,endingDate=2014-09-18T23:59:02.000-07:00,maxNumberOfPayments=1,maxTotalAmountOfAllPayments=189.00,paymentPeriod=NO_PERIOD_SPECIFIED,pinType=NOT_REQUIRED,returnUrl=http://groupfund.me/index.php/adaptive_paypal/preapprovereceipt,senderEmail=anita.rockersinfo@gmail.com,memo=Project : Funders test(146), Amount : 189.00, Project Owner : Jayshree Patel, On Site : http://groupfund.me/index.php,startingDate=2014-09-16T00:00:02.000-07:00,status=ACTIVE,displayMaxTotalAmount=false,sender.accountId=LXU9FUADSCKUJ,', 146, 91, 'SUCCESS', '2014-09-16 06:11:47'),
(987, 'preapprovalKey=PA-8A7536675J954563H&requestEnvelope.errorLanguage=en_US', 'Response From preapprovereceipt 854::responseEnvelope.timestamp=2014-09-16T04:09:44.815-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=60abc04e08939,responseEnvelope.build=12595496,approved=true,cancelUrl=http://groupfund.me/index.php/home,curPayments=0,curPaymentsAmount=0.00,curPeriodAttempts=0,currencyCode=USD,dateOfMonth=0,dayOfWeek=NO_DAY_SPECIFIED,endingDate=2014-09-18T23:59:50.000-07:00,maxNumberOfPayments=1,maxTotalAmountOfAllPayments=500.00,paymentPeriod=NO_PERIOD_SPECIFIED,pinType=NOT_REQUIRED,returnUrl=http://groupfund.me/index.php/adaptive_paypal/preapprovereceipt,senderEmail=anita.rockersinfo@gmail.com,memo=Project : Funders test(146), Amount : 500.00, Project Owner : Jayshree Patel, On Site : http://groupfund.me/index.php,startingDate=2014-09-16T00:00:50.000-07:00,status=ACTIVE,displayMaxTotalAmount=false,sender.accountId=LXU9FUADSCKUJ,', 146, 91, 'SUCCESS', '2014-09-16 06:09:44'),
(986, 'cancelUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fhome&returnUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fadaptive_paypal%2Fpreapprovereceipt&currencyCode=USD&startingDate=2014-09-16&endingDate=2014-09-18&maxNumberOfPayments=1&maxTotalAmountOfAllPayments=500.00&requestEnvelope.errorLanguage=en_US&memo=Project+%3A+Funders+test%28146%29%2C+Amount+%3A+500.00%2C+Project+Owner+%3A+Jayshree+Patel%2C+On+Site+%3A+http%3A%2F%2Fgroupfund.me%2Findex.php', 'Request From payment case 1 503::responseEnvelope.timestamp=2014-09-16T04:08:50.268-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=5bb56e171377e,responseEnvelope.build=12595496,preapprovalKey=PA-8A7536675J954563H,', 146, 91, 'SUCCESS', '2014-09-16 06:08:50'),
(1018, 'preapprovalKey=PA-5W847791LT332392V&requestEnvelope.errorLanguage=en_US', 'Response From preapprovereceipt 854::responseEnvelope.timestamp=2014-10-30T01:07:01.910-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=22723f2c35c55,responseEnvelope.build=13414382,approved=true,cancelUrl=http://www.groupfund.me/index.php/home,curPayments=0,curPaymentsAmount=0.00,curPeriodAttempts=0,currencyCode=USD,dateOfMonth=0,dayOfWeek=NO_DAY_SPECIFIED,endingDate=2015-02-14T23:59:05.000-08:00,maxNumberOfPayments=1,maxTotalAmountOfAllPayments=300.00,paymentPeriod=NO_PERIOD_SPECIFIED,pinType=NOT_REQUIRED,returnUrl=http://www.groupfund.me/index.php/adaptive_paypal/preapprovereceipt,senderEmail=anita.rockersinfo@gmail.com,memo=Project : Political parties, campaigning &amp; donations(4), Amount : 300.00, Project Owner : ketan patel, On Site : http://www.groupfund.me/index.php,startingDate=2014-10-30T00:00:05.000-07:00,status=ACTIVE,displayMaxTotalAmount=false,sender.accountId=LXU9FUADSCKUJ,', 4, 3, 'SUCCESS', '2014-10-30 03:07:01'),
(984, 'cancelUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fhome&returnUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fadaptive_paypal%2Fpreapprovereceipt&currencyCode=USD&startingDate=2014-09-16&endingDate=2014-09-18&maxNumberOfPayments=1&maxTotalAmountOfAllPayments=50.00&requestEnvelope.errorLanguage=en_US&memo=Project+%3A+Funders+test%28146%29%2C+Amount+%3A+50.00%2C+Project+Owner+%3A+Jayshree+Patel%2C+On+Site+%3A+http%3A%2F%2Fgroupfund.me%2Findex.php', 'Request From payment case 1 503::responseEnvelope.timestamp=2014-09-16T04:07:08.021-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=e8c1aef12a70e,responseEnvelope.build=12595496,preapprovalKey=PA-65N41843HY283393V,', 146, 91, 'SUCCESS', '2014-09-16 06:07:08'),
(985, 'preapprovalKey=PA-65N41843HY283393V&requestEnvelope.errorLanguage=en_US', 'Response From preapprovereceipt 854::responseEnvelope.timestamp=2014-09-16T04:07:56.390-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=5c51065a22cf3,responseEnvelope.build=12595496,approved=true,cancelUrl=http://groupfund.me/index.php/home,curPayments=0,curPaymentsAmount=0.00,curPeriodAttempts=0,currencyCode=USD,dateOfMonth=0,dayOfWeek=NO_DAY_SPECIFIED,endingDate=2014-09-18T23:59:08.000-07:00,maxNumberOfPayments=1,maxTotalAmountOfAllPayments=50.00,paymentPeriod=NO_PERIOD_SPECIFIED,pinType=NOT_REQUIRED,returnUrl=http://groupfund.me/index.php/adaptive_paypal/preapprovereceipt,senderEmail=anita.rockersinfo@gmail.com,memo=Project : Funders test(146), Amount : 50.00, Project Owner : Jayshree Patel, On Site : http://groupfund.me/index.php,startingDate=2014-09-16T00:00:08.000-07:00,status=ACTIVE,displayMaxTotalAmount=false,sender.accountId=LXU9FUADSCKUJ,', 146, 91, 'SUCCESS', '2014-09-16 06:07:56'),
(981, 'preapprovalKey=PA-4TA73880DC695914C&requestEnvelope.errorLanguage=en_US', 'Response From preapprovereceipt 854::responseEnvelope.timestamp=2014-09-15T23:45:20.162-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=86a9c9b12691d,responseEnvelope.build=12595496,approved=true,cancelUrl=http://groupfund.me/index.php/home,curPayments=0,curPaymentsAmount=0.00,curPeriodAttempts=0,currencyCode=USD,dateOfMonth=0,dayOfWeek=NO_DAY_SPECIFIED,endingDate=2014-09-18T23:59:28.000-07:00,maxNumberOfPayments=1,maxTotalAmountOfAllPayments=40.00,paymentPeriod=NO_PERIOD_SPECIFIED,pinType=NOT_REQUIRED,returnUrl=http://groupfund.me/index.php/adaptive_paypal/preapprovereceipt,senderEmail=anita.rockersinfo@gmail.com,memo=Project : Funders test(146), Amount : 40.00, Project Owner : Jayshree Patel, On Site : http://groupfund.me/index.php,startingDate=2014-09-15T00:00:28.000-07:00,status=ACTIVE,displayMaxTotalAmount=false,sender.accountId=LXU9FUADSCKUJ,', 146, 91, 'SUCCESS', '2014-09-16 01:45:20'),
(982, 'cancelUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fhome&returnUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fadaptive_paypal%2Fpreapprovereceipt&currencyCode=USD&startingDate=2014-09-16&endingDate=2014-09-18&maxNumberOfPayments=1&maxTotalAmountOfAllPayments=100.00&requestEnvelope.errorLanguage=en_US&memo=Project+%3A+Funders+test%28146%29%2C+Amount+%3A+100.00%2C+Project+Owner+%3A+Jayshree+Patel%2C+On+Site+%3A+http%3A%2F%2Fgroupfund.me%2Findex.php', 'Request From payment case 1 503::responseEnvelope.timestamp=2014-09-16T03:53:31.592-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=6c3e8715f506f,responseEnvelope.build=12595496,preapprovalKey=PA-81A81619R9857240E,', 146, 91, 'SUCCESS', '2014-09-16 05:53:31'),
(983, 'preapprovalKey=PA-81A81619R9857240E&requestEnvelope.errorLanguage=en_US', 'Response From preapprovereceipt 854::responseEnvelope.timestamp=2014-09-16T03:54:38.938-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=087f894ee4bcc,responseEnvelope.build=12595496,approved=true,cancelUrl=http://groupfund.me/index.php/home,curPayments=0,curPaymentsAmount=0.00,curPeriodAttempts=0,currencyCode=USD,dateOfMonth=0,dayOfWeek=NO_DAY_SPECIFIED,endingDate=2014-09-18T23:59:31.000-07:00,maxNumberOfPayments=1,maxTotalAmountOfAllPayments=100.00,paymentPeriod=NO_PERIOD_SPECIFIED,pinType=NOT_REQUIRED,returnUrl=http://groupfund.me/index.php/adaptive_paypal/preapprovereceipt,senderEmail=anita.rockersinfo@gmail.com,memo=Project : Funders test(146), Amount : 100.00, Project Owner : Jayshree Patel, On Site : http://groupfund.me/index.php,startingDate=2014-09-16T00:00:31.000-07:00,status=ACTIVE,displayMaxTotalAmount=false,sender.accountId=LXU9FUADSCKUJ,', 146, 91, 'SUCCESS', '2014-09-16 05:54:39'),
(980, 'preapprovalKey=PA-4TA73880DC695914C&requestEnvelope.errorLanguage=en_US', 'Response From preapprovereceipt 854::responseEnvelope.timestamp=2014-09-15T23:44:30.980-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=7924e4b33a8d7,responseEnvelope.build=12595496,approved=false,cancelUrl=http://groupfund.me/index.php/home,curPayments=0,curPaymentsAmount=0.00,curPeriodAttempts=0,currencyCode=USD,dateOfMonth=0,dayOfWeek=NO_DAY_SPECIFIED,endingDate=2014-09-18T23:59:28.000-07:00,maxNumberOfPayments=1,maxTotalAmountOfAllPayments=40.00,paymentPeriod=NO_PERIOD_SPECIFIED,pinType=NOT_REQUIRED,returnUrl=http://groupfund.me/index.php/adaptive_paypal/preapprovereceipt,memo=Project : Funders test(146), Amount : 40.00, Project Owner : Jayshree Patel, On Site : http://groupfund.me/index.php,startingDate=2014-09-15T00:00:28.000-07:00,status=ACTIVE,displayMaxTotalAmount=false,', 146, 91, 'SUCCESS', '2014-09-16 01:44:30'),
(979, 'cancelUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fhome&returnUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fadaptive_paypal%2Fpreapprovereceipt&currencyCode=USD&startingDate=2014-09-15&endingDate=2014-09-18&maxNumberOfPayments=1&maxTotalAmountOfAllPayments=40.00&requestEnvelope.errorLanguage=en_US&memo=Project+%3A+Funders+test%28146%29%2C+Amount+%3A+40.00%2C+Project+Owner+%3A+Jayshree+Patel%2C+On+Site+%3A+http%3A%2F%2Fgroupfund.me%2Findex.php', 'Request From payment case 1 503::responseEnvelope.timestamp=2014-09-15T23:44:28.864-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=ef14167f38641,responseEnvelope.build=12595496,preapprovalKey=PA-4TA73880DC695914C,', 146, 91, 'SUCCESS', '2014-09-16 01:44:28'),
(956, 'actionType=PAY&cancelUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fcron%2Fcron_preapprove&returnUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fcron%2Fcron_preapprove&ipnNotificationUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fcron%2Fcron_preapprove&currencyCode=USD&clientDetails.deviceId=mydevice&clientDetails.ipAddress=1.22.81.225&clientDetails.applicationId=APP-80W284485P519543T&requestEnvelope.errorLanguage=en_US&memo=Project+%3A+Active+3%28111%29%2C+Amount+%3A+1000.00%2CProject+Creator+Pay+Fees%28%25%29+%3A+11.00%2C+Create+By+%3A+Jayshree+Patel%2C+On+Site+%3A+http%3A%2F%2Fgroupfund.me%2F&feesPayer=PRIMARYRECEIVER&receiverList.receiver%5B0%5D.email=ankit.rockersinfo%40gmail.com&receiverList.receiver%5B0%5D.amount=1000.00&receiverList.receiver%5B0%5D.primary%5B0%5D=true&receiverList.receiver%5B1%5D.email=mihir.test.rockersinfo%40gmail.com&receiverList.receiver%5B1%5D.amount=110.00&receiverList.receiver%5B1%5D.primary%5B1%5D=false&preapprovalKey=PA-2XV925049B696980X&senderEmail=anita.rockersinfo%40gmail.com', 'Response From cron_preapprove 320::responseEnvelope.timestamp=2014-09-02T02:51:29.493-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=7a2edba96c6a0,responseEnvelope.build=12595496,payKey=AP-01T72970PJ111225Y,paymentExecStatus=COMPLETED,paymentInfoList.paymentInfo(0).transactionId=98F15282H7289760E,paymentInfoList.paymentInfo(0).transactionStatus=COMPLETED,paymentInfoList.paymentInfo(0).receiver.amount=1000.00,paymentInfoList.paymentInfo(0).receiver.email=ankit.rockersinfo@gmail.com,paymentInfoList.paymentInfo(0).receiver.primary=true,paymentInfoList.paymentInfo(0).receiver.accountId=FC6QJ72EBYT6G,paymentInfoList.paymentInfo(0).pendingRefund=false,paymentInfoList.paymentInfo(0).senderTransactionId=5L307009SR434814W,paymentInfoList.paymentInfo(0).senderTransactionStatus=COMPLETED,paymentInfoList.paymentInfo(1).transactionId=8X253593JR3910708,paymentInfoList.paymentInfo(1).transactionStatus=COMPLETED,paymentInfoList.paymentInfo(1).receiver.amount=110.00,paymentInfoList.paymentInfo(1).receiver.email=mihir.test.rockersinfo@gmail.com,paymentInfoList.paymentInfo(1).receiver.primary=false,paymentInfoList.paymentInfo(1).receiver.accountId=7GZC89QQLL4EE,paymentInfoList.paymentInfo(1).pendingRefund=false,paymentInfoList.paymentInfo(1).senderTransactionId=6BB23900BD050822B,paymentInfoList.paymentInfo(1).senderTransactionStatus=COMPLETED,sender.accountId=LXU9FUADSCKUJ,', 111, 0, 'SUCCESS', '2014-09-02 05:51:29'),
(977, 'preapprovalKey=PA-9FA628668U781291T&requestEnvelope.errorLanguage=en_US', 'Response From preapprovereceipt 854::responseEnvelope.timestamp=2014-09-15T23:43:18.967-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=0f4d70bd58bcf,responseEnvelope.build=12595496,approved=true,cancelUrl=http://groupfund.me/index.php/home,curPayments=0,curPaymentsAmount=0.00,curPeriodAttempts=0,currencyCode=USD,dateOfMonth=0,dayOfWeek=NO_DAY_SPECIFIED,endingDate=2014-09-18T23:59:37.000-07:00,maxNumberOfPayments=1,maxTotalAmountOfAllPayments=100.00,paymentPeriod=NO_PERIOD_SPECIFIED,pinType=NOT_REQUIRED,returnUrl=http://groupfund.me/index.php/adaptive_paypal/preapprovereceipt,senderEmail=anita.rockersinfo@gmail.com,memo=Project : Funders test(146), Amount : 100.00, Project Owner : Jayshree Patel, On Site : http://groupfund.me/index.php,startingDate=2014-09-15T00:00:37.000-07:00,status=ACTIVE,displayMaxTotalAmount=false,sender.accountId=LXU9FUADSCKUJ,', 146, 91, 'SUCCESS', '2014-09-16 01:43:19'),
(978, 'cancelUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fhome&returnUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fadaptive_paypal%2Fpreapprovereceipt&currencyCode=USD&startingDate=2014-09-15&endingDate=2014-09-18&maxNumberOfPayments=1&maxTotalAmountOfAllPayments=50.00&requestEnvelope.errorLanguage=en_US&memo=Project+%3A+Funders+test%28146%29%2C+Amount+%3A+50.00%2C+Project+Owner+%3A+Jayshree+Patel%2C+On+Site+%3A+http%3A%2F%2Fgroupfund.me%2Findex.php', 'Request From payment case 1 503::responseEnvelope.timestamp=2014-09-15T23:43:45.246-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=b5af4d674ec71,responseEnvelope.build=12595496,preapprovalKey=PA-6RK03079FF563144R,', 146, 91, 'SUCCESS', '2014-09-16 01:43:45'),
(1016, 'cancelUrl=http%3A%2F%2Fwww.groupfund.me%2Findex.php%2Fhome&returnUrl=http%3A%2F%2Fwww.groupfund.me%2Findex.php%2Fadaptive_paypal%2Fpreapprovereceipt&currencyCode=USD&startingDate=2014-10-30&endingDate=2015-02-14&maxNumberOfPayments=1&maxTotalAmountOfAllPayments=300.00&requestEnvelope.errorLanguage=en_US&memo=Project+%3A+Political+parties%2C+campaigning+%26amp%3B+donations%284%29%2C+Amount+%3A+300.00%2C+Project+Owner+%3A+ketan+patel%2C+On+Site+%3A+http%3A%2F%2Fwww.groupfund.me%2Findex.php', 'Request From payment case 1 503::responseEnvelope.timestamp=2014-10-30T01:06:05.390-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=2bb7e9cd3a1f6,responseEnvelope.build=13414382,preapprovalKey=PA-5W847791LT332392V,', 4, 3, 'SUCCESS', '2014-10-30 03:06:05'),
(1017, 'preapprovalKey=PA-5W847791LT332392V&requestEnvelope.errorLanguage=en_US', 'Response From preapprovereceipt 854::responseEnvelope.timestamp=2014-10-30T01:06:52.593-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=043ac54a37814,responseEnvelope.build=13414382,approved=true,cancelUrl=http://www.groupfund.me/index.php/home,curPayments=0,curPaymentsAmount=0.00,curPeriodAttempts=0,currencyCode=USD,dateOfMonth=0,dayOfWeek=NO_DAY_SPECIFIED,endingDate=2015-02-14T23:59:05.000-08:00,maxNumberOfPayments=1,maxTotalAmountOfAllPayments=300.00,paymentPeriod=NO_PERIOD_SPECIFIED,pinType=NOT_REQUIRED,returnUrl=http://www.groupfund.me/index.php/adaptive_paypal/preapprovereceipt,senderEmail=anita.rockersinfo@gmail.com,memo=Project : Political parties, campaigning &amp; donations(4), Amount : 300.00, Project Owner : ketan patel, On Site : http://www.groupfund.me/index.php,startingDate=2014-10-30T00:00:05.000-07:00,status=ACTIVE,displayMaxTotalAmount=false,sender.accountId=LXU9FUADSCKUJ,', 4, 3, 'SUCCESS', '2014-10-30 03:06:52'),
(1013, 'preapprovalKey=PA-7VB17899B8811374C&requestEnvelope.errorLanguage=en_US', 'Response From preapprovereceipt 854::responseEnvelope.timestamp=2014-10-30T00:58:53.157-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=fea56ea24d7f3,responseEnvelope.build=13414382,approved=true,cancelUrl=http://www.groupfund.me/index.php/home,curPayments=0,curPaymentsAmount=0.00,curPeriodAttempts=0,currencyCode=USD,dateOfMonth=0,dayOfWeek=NO_DAY_SPECIFIED,endingDate=2015-02-14T23:59:00.000-08:00,maxNumberOfPayments=1,maxTotalAmountOfAllPayments=200.00,paymentPeriod=NO_PERIOD_SPECIFIED,pinType=NOT_REQUIRED,returnUrl=http://www.groupfund.me/index.php/adaptive_paypal/preapprovereceipt,senderEmail=anita.rockersinfo@gmail.com,memo=Project : Political parties, campaigning &amp; donations(4), Amount : 200.00, Project Owner : ketan patel, On Site : http://www.groupfund.me/index.php,startingDate=2014-10-30T00:00:00.000-07:00,status=ACTIVE,displayMaxTotalAmount=false,sender.accountId=LXU9FUADSCKUJ,', 4, 4, 'SUCCESS', '2014-10-30 02:58:53'),
(1012, 'cancelUrl=http%3A%2F%2Fwww.groupfund.me%2Findex.php%2Fhome&returnUrl=http%3A%2F%2Fwww.groupfund.me%2Findex.php%2Fadaptive_paypal%2Fpreapprovereceipt&currencyCode=USD&startingDate=2014-10-30&endingDate=2015-02-14&maxNumberOfPayments=1&maxTotalAmountOfAllPayments=200.00&requestEnvelope.errorLanguage=en_US&memo=Project+%3A+Political+parties%2C+campaigning+%26amp%3B+donations%284%29%2C+Amount+%3A+200.00%2C+Project+Owner+%3A+ketan+patel%2C+On+Site+%3A+http%3A%2F%2Fwww.groupfund.me%2Findex.php', 'Request From payment case 1 503::responseEnvelope.timestamp=2014-10-30T00:58:00.316-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=d4c66615b3e45,responseEnvelope.build=13414382,preapprovalKey=PA-7VB17899B8811374C,', 4, 4, 'SUCCESS', '2014-10-30 02:58:00'),
(1011, 'preapprovalKey=PA-3XA61060RH852425D&requestEnvelope.errorLanguage=en_US', 'Response From preapprovereceipt 854::responseEnvelope.timestamp=2014-10-29T23:27:28.908-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=cc6b98ef898f3,responseEnvelope.build=13414382,approved=true,cancelUrl=http://www.groupfund.me/index.php/home,curPayments=0,curPaymentsAmount=0.00,curPeriodAttempts=0,currencyCode=USD,dateOfMonth=0,dayOfWeek=NO_DAY_SPECIFIED,endingDate=2015-04-23T23:59:32.000-07:00,maxNumberOfPayments=1,maxTotalAmountOfAllPayments=100.00,paymentPeriod=NO_PERIOD_SPECIFIED,pinType=NOT_REQUIRED,returnUrl=http://www.groupfund.me/index.php/adaptive_paypal/preapprovereceipt,senderEmail=anita.rockersinfo@gmail.com,memo=Project : Donation-accepting organizations and projects(6), Amount : 100.00, Project Owner : ketan patel, On Site : http://www.groupfund.me/index.php,startingDate=2014-10-29T00:00:32.000-07:00,status=ACTIVE,displayMaxTotalAmount=false,sender.accountId=LXU9FUADSCKUJ,', 6, 4, 'SUCCESS', '2014-10-30 01:27:28'),
(1010, 'cancelUrl=http%3A%2F%2Fwww.groupfund.me%2Findex.php%2Fhome&returnUrl=http%3A%2F%2Fwww.groupfund.me%2Findex.php%2Fadaptive_paypal%2Fpreapprovereceipt&currencyCode=USD&startingDate=2014-10-29&endingDate=2015-04-23&maxNumberOfPayments=1&maxTotalAmountOfAllPayments=100.00&requestEnvelope.errorLanguage=en_US&memo=Project+%3A+Donation-accepting+organizations+and+projects%286%29%2C+Amount+%3A+100.00%2C+Project+Owner+%3A+ketan+patel%2C+On+Site+%3A+http%3A%2F%2Fwww.groupfund.me%2Findex.php', 'Request From payment case 1 503::responseEnvelope.timestamp=2014-10-29T23:26:32.893-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=b8dff107fc8a5,responseEnvelope.build=13414382,preapprovalKey=PA-3XA61060RH852425D,', 6, 4, 'SUCCESS', '2014-10-30 01:26:32'),
(1007, 'preapprovalKey=PA-7CV610943J7803626&requestEnvelope.errorLanguage=en_US', 'Response From preapprovereceipt 854::responseEnvelope.timestamp=2014-10-29T23:13:11.759-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=9d921eeebfcf7,responseEnvelope.build=13414382,approved=true,cancelUrl=http://www.groupfund.me/index.php/home,curPayments=0,curPaymentsAmount=0.00,curPeriodAttempts=0,currencyCode=USD,dateOfMonth=0,dayOfWeek=NO_DAY_SPECIFIED,endingDate=2015-04-23T23:59:57.000-07:00,maxNumberOfPayments=1,maxTotalAmountOfAllPayments=200.00,paymentPeriod=NO_PERIOD_SPECIFIED,pinType=NOT_REQUIRED,returnUrl=http://www.groupfund.me/index.php/adaptive_paypal/preapprovereceipt,senderEmail=anita.rockersinfo@gmail.com,memo=Project : Donation-accepting organizations and projects(6), Amount : 200.00, Project Owner : ketan patel, On Site : http://www.groupfund.me/index.php,startingDate=2014-10-29T00:00:57.000-07:00,status=ACTIVE,displayMaxTotalAmount=false,sender.accountId=LXU9FUADSCKUJ,', 6, 3, 'SUCCESS', '2014-10-30 01:13:11');
INSERT INTO `paypal_response_request` (`paypal_response_request_id`, `paypal_request`, `paypal_response`, `project_id`, `donar_id`, `transaction_status`, `insert_date`) VALUES
(1008, 'cancelUrl=http%3A%2F%2Fwww.groupfund.me%2Findex.php%2Fhome&returnUrl=http%3A%2F%2Fwww.groupfund.me%2Findex.php%2Fadaptive_paypal%2Fpreapprovereceipt&currencyCode=USD&startingDate=2014-10-29&endingDate=2015-04-23&maxNumberOfPayments=1&maxTotalAmountOfAllPayments=100.00&requestEnvelope.errorLanguage=en_US&memo=Project+%3A+Donation-accepting+organizations+and+projects%286%29%2C+Amount+%3A+100.00%2C+Project+Owner+%3A+ketan+patel%2C+On+Site+%3A+http%3A%2F%2Fwww.groupfund.me%2Findex.php', 'Request From payment case 1 503::responseEnvelope.timestamp=2014-10-29T23:19:11.319-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=2694e760179ba,responseEnvelope.build=13414382,preapprovalKey=PA-2FR456594M738980A,', 6, 5, 'SUCCESS', '2014-10-30 01:19:11'),
(1009, 'preapprovalKey=PA-2FR456594M738980A&requestEnvelope.errorLanguage=en_US', 'Response From preapprovereceipt 854::responseEnvelope.timestamp=2014-10-29T23:19:55.125-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=dc52da1b1ecf4,responseEnvelope.build=13414382,approved=true,cancelUrl=http://www.groupfund.me/index.php/home,curPayments=0,curPaymentsAmount=0.00,curPeriodAttempts=0,currencyCode=USD,dateOfMonth=0,dayOfWeek=NO_DAY_SPECIFIED,endingDate=2015-04-23T23:59:11.000-07:00,maxNumberOfPayments=1,maxTotalAmountOfAllPayments=100.00,paymentPeriod=NO_PERIOD_SPECIFIED,pinType=NOT_REQUIRED,returnUrl=http://www.groupfund.me/index.php/adaptive_paypal/preapprovereceipt,senderEmail=anita.rockersinfo@gmail.com,memo=Project : Donation-accepting organizations and projects(6), Amount : 100.00, Project Owner : ketan patel, On Site : http://www.groupfund.me/index.php,startingDate=2014-10-29T00:00:11.000-07:00,status=ACTIVE,displayMaxTotalAmount=false,sender.accountId=LXU9FUADSCKUJ,', 6, 5, 'SUCCESS', '2014-10-30 01:19:55'),
(1004, 'cancelUrl=http%3A%2F%2Fwww.groupfund.me%2Findex.php%2Fhome&returnUrl=http%3A%2F%2Fwww.groupfund.me%2Findex.php%2Fadaptive_paypal%2Fpreapprovereceipt&currencyCode=USD&startingDate=2014-10-29&endingDate=2015-01-21&maxNumberOfPayments=1&maxTotalAmountOfAllPayments=300.00&requestEnvelope.errorLanguage=en_US&memo=Project+%3A+Olive+Chapel+Elementary+receives+%2410%2C000+donation+for+technology%285%29%2C+Amount+%3A+300.00%2C+Project+Owner+%3A+ketan+patel%2C+On+Site+%3A+http%3A%2F%2Fwww.groupfund.me%2Findex.php', 'Request From payment case 1 503::responseEnvelope.timestamp=2014-10-29T22:32:44.535-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=818c697bea1a0,responseEnvelope.build=13414382,preapprovalKey=PA-2GA061505U689250E,', 5, 3, 'SUCCESS', '2014-10-30 00:32:44'),
(1005, 'preapprovalKey=PA-2GA061505U689250E&requestEnvelope.errorLanguage=en_US', 'Response From preapprovereceipt 854::responseEnvelope.timestamp=2014-10-29T22:33:49.770-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=10009edbf8513,responseEnvelope.build=13414382,approved=true,cancelUrl=http://www.groupfund.me/index.php/home,curPayments=0,curPaymentsAmount=0.00,curPeriodAttempts=0,currencyCode=USD,dateOfMonth=0,dayOfWeek=NO_DAY_SPECIFIED,endingDate=2015-01-21T23:59:44.000-08:00,maxNumberOfPayments=1,maxTotalAmountOfAllPayments=300.00,paymentPeriod=NO_PERIOD_SPECIFIED,pinType=NOT_REQUIRED,returnUrl=http://www.groupfund.me/index.php/adaptive_paypal/preapprovereceipt,senderEmail=anita.rockersinfo@gmail.com,memo=Project : Olive Chapel Elementary receives $10,000 donation for technology(5), Amount : 300.00, Project Owner : ketan patel, On Site : http://www.groupfund.me/index.php,startingDate=2014-10-29T00:00:44.000-07:00,status=ACTIVE,displayMaxTotalAmount=false,sender.accountId=LXU9FUADSCKUJ,', 5, 3, 'SUCCESS', '2014-10-30 00:33:49'),
(1006, 'cancelUrl=http%3A%2F%2Fwww.groupfund.me%2Findex.php%2Fhome&returnUrl=http%3A%2F%2Fwww.groupfund.me%2Findex.php%2Fadaptive_paypal%2Fpreapprovereceipt&currencyCode=USD&startingDate=2014-10-29&endingDate=2015-04-23&maxNumberOfPayments=1&maxTotalAmountOfAllPayments=200.00&requestEnvelope.errorLanguage=en_US&memo=Project+%3A+Donation-accepting+organizations+and+projects%286%29%2C+Amount+%3A+200.00%2C+Project+Owner+%3A+ketan+patel%2C+On+Site+%3A+http%3A%2F%2Fwww.groupfund.me%2Findex.php', 'Request From payment case 1 503::responseEnvelope.timestamp=2014-10-29T23:11:57.081-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=c3a2d301aa29c,responseEnvelope.build=13414382,preapprovalKey=PA-7CV610943J7803626,', 6, 3, 'SUCCESS', '2014-10-30 01:11:57'),
(1002, 'cancelUrl=http%3A%2F%2Fwww.groupfund.me%2Findex.php%2Fhome&returnUrl=http%3A%2F%2Fwww.groupfund.me%2Findex.php%2Fadaptive_paypal%2Fpreapprovereceipt&currencyCode=USD&startingDate=2014-10-29&endingDate=2015-01-21&maxNumberOfPayments=1&maxTotalAmountOfAllPayments=300.00&requestEnvelope.errorLanguage=en_US&memo=Project+%3A+Olive+Chapel+Elementary+receives+%2410%2C000+donation+for+technology%285%29%2C+Amount+%3A+300.00%2C+Project+Owner+%3A+ketan+patel%2C+On+Site+%3A+http%3A%2F%2Fwww.groupfund.me%2Findex.php', 'Request From payment case 1 503::responseEnvelope.timestamp=2014-10-29T22:23:02.964-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=661bb92f5eda6,responseEnvelope.build=13414382,preapprovalKey=PA-7T982700FU313480A,', 5, 4, 'SUCCESS', '2014-10-30 00:23:02'),
(1003, 'preapprovalKey=PA-7T982700FU313480A&requestEnvelope.errorLanguage=en_US', 'Response From preapprovereceipt 854::responseEnvelope.timestamp=2014-10-29T22:25:18.913-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=f36c54e47c476,responseEnvelope.build=13414382,approved=true,cancelUrl=http://www.groupfund.me/index.php/home,curPayments=0,curPaymentsAmount=0.00,curPeriodAttempts=0,currencyCode=USD,dateOfMonth=0,dayOfWeek=NO_DAY_SPECIFIED,endingDate=2015-01-21T23:59:02.000-08:00,maxNumberOfPayments=1,maxTotalAmountOfAllPayments=300.00,paymentPeriod=NO_PERIOD_SPECIFIED,pinType=NOT_REQUIRED,returnUrl=http://www.groupfund.me/index.php/adaptive_paypal/preapprovereceipt,senderEmail=anita.rockersinfo@gmail.com,memo=Project : Olive Chapel Elementary receives $10,000 donation for technology(5), Amount : 300.00, Project Owner : ketan patel, On Site : http://www.groupfund.me/index.php,startingDate=2014-10-29T00:00:02.000-07:00,status=ACTIVE,displayMaxTotalAmount=false,sender.accountId=LXU9FUADSCKUJ,', 5, 4, 'SUCCESS', '2014-10-30 00:25:18'),
(953, 'cancelUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fhome&returnUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fadaptive_paypal%2Fpreapprovereceipt&currencyCode=USD&startingDate=2014-09-02&endingDate=2014-09-04&maxNumberOfPayments=1&maxTotalAmountOfAllPayments=1000.00&requestEnvelope.errorLanguage=en_US&memo=Project+%3A+Active+3%28111%29%2C+Amount+%3A+1000.00%2C+Project+Owner+%3A+Jayshree+Patel%2C+On+Site+%3A+http%3A%2F%2Fgroupfund.me%2Findex.php', 'Request From payment case 1 503::responseEnvelope.timestamp=2014-09-02T02:36:45.305-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=ee76c418956c7,responseEnvelope.build=12595496,preapprovalKey=PA-3C7942390B265693L,', 111, 91, 'SUCCESS', '2014-09-02 05:36:45'),
(954, 'cancelUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fhome&returnUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fadaptive_paypal%2Fpreapprovereceipt&currencyCode=USD&startingDate=2014-09-02&endingDate=2014-09-04&maxNumberOfPayments=1&maxTotalAmountOfAllPayments=1000.00&requestEnvelope.errorLanguage=en_US&memo=Project+%3A+Active+3%28111%29%2C+Amount+%3A+1000.00%2C+Project+Owner+%3A+Jayshree+Patel%2C+On+Site+%3A+http%3A%2F%2Fgroupfund.me%2Findex.php', 'Request From payment case 1 503::responseEnvelope.timestamp=2014-09-02T02:44:43.266-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=ddddcedf0f662,responseEnvelope.build=12595496,preapprovalKey=PA-2XV925049B696980X,', 111, 91, 'SUCCESS', '2014-09-02 05:44:43'),
(955, 'preapprovalKey=PA-2XV925049B696980X&requestEnvelope.errorLanguage=en_US', 'Response From preapprovereceipt 854::responseEnvelope.timestamp=2014-09-02T02:46:20.769-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=bc777bd824379,responseEnvelope.build=12595496,approved=true,cancelUrl=http://groupfund.me/index.php/home,curPayments=0,curPaymentsAmount=0.00,curPeriodAttempts=0,currencyCode=USD,dateOfMonth=0,dayOfWeek=NO_DAY_SPECIFIED,endingDate=2014-09-04T23:59:43.000-07:00,maxNumberOfPayments=1,maxTotalAmountOfAllPayments=1000.00,paymentPeriod=NO_PERIOD_SPECIFIED,pinType=NOT_REQUIRED,returnUrl=http://groupfund.me/index.php/adaptive_paypal/preapprovereceipt,senderEmail=anita.rockersinfo@gmail.com,memo=Project : Active 3(111), Amount : 1000.00, Project Owner : Jayshree Patel, On Site : http://groupfund.me/index.php,startingDate=2014-09-02T00:00:43.000-07:00,status=ACTIVE,displayMaxTotalAmount=false,sender.accountId=LXU9FUADSCKUJ,', 111, 91, 'SUCCESS', '2014-09-02 05:46:20'),
(997, 'cancelUrl=http%3A%2F%2Fwww.groupfund.me%2Findex.php%2Fhome&returnUrl=http%3A%2F%2Fwww.groupfund.me%2Findex.php%2Fadaptive_paypal%2Fpreapprovereceipt&currencyCode=USD&startingDate=2014-10-02&endingDate=2014-10-11&maxNumberOfPayments=1&maxTotalAmountOfAllPayments=600.00&requestEnvelope.errorLanguage=en_US&memo=Project+%3A+Member+test%28159%29%2C+Amount+%3A+600.00%2C+Project+Owner+%3A+Jayshree+Patel%2C+On+Site+%3A+http%3A%2F%2Fwww.groupfund.me%2Findex.php', 'Request From payment case 1 503::responseEnvelope.timestamp=2014-10-02T23:45:26.582-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=90529762126cf,responseEnvelope.build=12382931,preapprovalKey=PA-26560116E57815913,', 159, 0, 'SUCCESS', '2014-10-03 01:45:26'),
(998, 'cancelUrl=http%3A%2F%2Fwww.groupfund.me%2Findex.php%2Fhome&returnUrl=http%3A%2F%2Fwww.groupfund.me%2Findex.php%2Fadaptive_paypal%2Fpreapprovereceipt&currencyCode=USD&startingDate=2014-10-29&endingDate=2015-01-21&maxNumberOfPayments=1&maxTotalAmountOfAllPayments=100.00&requestEnvelope.errorLanguage=en_US&memo=Project+%3A+Olive+Chapel+Elementary+receives+%2410%2C000+donation+for+technology%285%29%2C+Amount+%3A+100.00%2C+Project+Owner+%3A+ketan+patel%2C+On+Site+%3A+http%3A%2F%2Fwww.groupfund.me%2Findex.php', 'Request From payment case 1 503::responseEnvelope.timestamp=2014-10-29T20:54:40.306-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=8beff59233203,responseEnvelope.build=13414382,preapprovalKey=PA-4AU42472SJ042810Y,', 5, 5, 'SUCCESS', '2014-10-29 22:54:40'),
(994, 'actionType=PAY&cancelUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fcron%2Fcron_preapprove&returnUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fcron%2Fcron_preapprove&ipnNotificationUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fcron%2Fcron_preapprove&currencyCode=USD&clientDetails.deviceId=mydevice&clientDetails.ipAddress=113.193.174.213&clientDetails.applicationId=APP-80W284485P519543T&requestEnvelope.errorLanguage=en_US&memo=Project+%3A+Funders+test%28146%29%2C+Amount+%3A+50.00%2CProject+Creator+Pay+Fees%28%25%29+%3A+11.00%2C+Create+By+%3A+Jayshree+Patel%2C+On+Site+%3A+http%3A%2F%2Fgroupfund.me%2F&feesPayer=PRIMARYRECEIVER&receiverList.receiver%5B0%5D.email=ankit.rockersinfo%40gmail.com&receiverList.receiver%5B0%5D.amount=50.00&receiverList.receiver%5B0%5D.primary%5B0%5D=true&receiverList.receiver%5B1%5D.email=mihir.test.rockersinfo%40gmail.com&receiverList.receiver%5B1%5D.amount=5.50&receiverList.receiver%5B1%5D.primary%5B1%5D=false&preapprovalKey=PA-65N41843HY283393V&senderEmail=anita.rockersinfo%40gmail.com', 'Response From cron_preapprove 320::responseEnvelope.timestamp=2014-09-30T01:22:54.886-07:00,responseEnvelope.ack=Failure,responseEnvelope.correlationId=b20e6ce2d6e8f,responseEnvelope.build=12595496,error(0).errorId=579024,error(0).domain=PLATFORM,error(0).subdomain=Application,error(0).severity=Error,error(0).category=Application,error(0).message=The preapproval key can''t be used before the start date or after the end date,', 146, 0, 'FAILURE', '2014-09-30 03:22:54'),
(995, 'actionType=PAY&cancelUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fcron%2Fcron_preapprove&returnUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fcron%2Fcron_preapprove&ipnNotificationUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fcron%2Fcron_preapprove&currencyCode=USD&clientDetails.deviceId=mydevice&clientDetails.ipAddress=113.193.174.213&clientDetails.applicationId=APP-80W284485P519543T&requestEnvelope.errorLanguage=en_US&memo=Project+%3A+Funders+test%28146%29%2C+Amount+%3A+500.00%2CProject+Creator+Pay+Fees%28%25%29+%3A+11.00%2C+Create+By+%3A+Jayshree+Patel%2C+On+Site+%3A+http%3A%2F%2Fgroupfund.me%2F&feesPayer=PRIMARYRECEIVER&receiverList.receiver%5B0%5D.email=ankit.rockersinfo%40gmail.com&receiverList.receiver%5B0%5D.amount=500.00&receiverList.receiver%5B0%5D.primary%5B0%5D=true&receiverList.receiver%5B1%5D.email=mihir.test.rockersinfo%40gmail.com&receiverList.receiver%5B1%5D.amount=55.00&receiverList.receiver%5B1%5D.primary%5B1%5D=false&preapprovalKey=PA-8A7536675J954563H&senderEmail=anita.rockersinfo%40gmail.com', 'Response From cron_preapprove 320::responseEnvelope.timestamp=2014-09-30T01:22:55.744-07:00,responseEnvelope.ack=Failure,responseEnvelope.correlationId=938216acd2a71,responseEnvelope.build=12595496,error(0).errorId=579024,error(0).domain=PLATFORM,error(0).subdomain=Application,error(0).severity=Error,error(0).category=Application,error(0).message=The preapproval key can''t be used before the start date or after the end date,', 146, 0, 'FAILURE', '2014-09-30 03:22:55'),
(996, 'actionType=PAY&cancelUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fcron%2Fcron_preapprove&returnUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fcron%2Fcron_preapprove&ipnNotificationUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fcron%2Fcron_preapprove&currencyCode=USD&clientDetails.deviceId=mydevice&clientDetails.ipAddress=113.193.174.213&clientDetails.applicationId=APP-80W284485P519543T&requestEnvelope.errorLanguage=en_US&memo=Project+%3A+Funders+test%28146%29%2C+Amount+%3A+189.00%2CProject+Creator+Pay+Fees%28%25%29+%3A+11.00%2C+Create+By+%3A+Jayshree+Patel%2C+On+Site+%3A+http%3A%2F%2Fgroupfund.me%2F&feesPayer=PRIMARYRECEIVER&receiverList.receiver%5B0%5D.email=ankit.rockersinfo%40gmail.com&receiverList.receiver%5B0%5D.amount=189.00&receiverList.receiver%5B0%5D.primary%5B0%5D=true&receiverList.receiver%5B1%5D.email=mihir.test.rockersinfo%40gmail.com&receiverList.receiver%5B1%5D.amount=20.79&receiverList.receiver%5B1%5D.primary%5B1%5D=false&preapprovalKey=PA-0WN999028J378840C&senderEmail=anita.rockersinfo%40gmail.com', 'Response From cron_preapprove 320::responseEnvelope.timestamp=2014-09-30T01:22:56.439-07:00,responseEnvelope.ack=Failure,responseEnvelope.correlationId=f0a7aa85dea31,responseEnvelope.build=12595496,error(0).errorId=579024,error(0).domain=PLATFORM,error(0).subdomain=Application,error(0).severity=Error,error(0).category=Application,error(0).message=The preapproval key can''t be used before the start date or after the end date,', 146, 0, 'FAILURE', '2014-09-30 03:22:56'),
(990, 'actionType=PAY&cancelUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fcron%2Fcron_preapprove&returnUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fcron%2Fcron_preapprove&ipnNotificationUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fcron%2Fcron_preapprove&currencyCode=USD&clientDetails.deviceId=mydevice&clientDetails.ipAddress=113.193.174.213&clientDetails.applicationId=APP-80W284485P519543T&requestEnvelope.errorLanguage=en_US&memo=Project+%3A+Funders+test%28146%29%2C+Amount+%3A+100.00%2CProject+Creator+Pay+Fees%28%25%29+%3A+11.00%2C+Create+By+%3A+Jayshree+Patel%2C+On+Site+%3A+http%3A%2F%2Fgroupfund.me%2F&feesPayer=PRIMARYRECEIVER&receiverList.receiver%5B0%5D.email=ankit.rockersinfo%40gmail.com&receiverList.receiver%5B0%5D.amount=100.00&receiverList.receiver%5B0%5D.primary%5B0%5D=true&receiverList.receiver%5B1%5D.email=mihir.test.rockersinfo%40gmail.com&receiverList.receiver%5B1%5D.amount=11.00&receiverList.receiver%5B1%5D.primary%5B1%5D=false&preapprovalKey=PA-37F32489D2646243K&senderEmail=anita.rockersinfo%40gmail.com', 'Response From cron_preapprove 320::responseEnvelope.timestamp=2014-09-30T01:22:47.075-07:00,responseEnvelope.ack=Failure,responseEnvelope.correlationId=79d762f6dc194,responseEnvelope.build=12595496,error(0).errorId=579024,error(0).domain=PLATFORM,error(0).subdomain=Application,error(0).severity=Error,error(0).category=Application,error(0).message=The preapproval key can''t be used before the start date or after the end date,', 146, 0, 'FAILURE', '2014-09-30 03:22:47'),
(991, 'actionType=PAY&cancelUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fcron%2Fcron_preapprove&returnUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fcron%2Fcron_preapprove&ipnNotificationUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fcron%2Fcron_preapprove&currencyCode=USD&clientDetails.deviceId=mydevice&clientDetails.ipAddress=113.193.174.213&clientDetails.applicationId=APP-80W284485P519543T&requestEnvelope.errorLanguage=en_US&memo=Project+%3A+Funders+test%28146%29%2C+Amount+%3A+100.00%2CProject+Creator+Pay+Fees%28%25%29+%3A+11.00%2C+Create+By+%3A+Jayshree+Patel%2C+On+Site+%3A+http%3A%2F%2Fgroupfund.me%2F&feesPayer=PRIMARYRECEIVER&receiverList.receiver%5B0%5D.email=ankit.rockersinfo%40gmail.com&receiverList.receiver%5B0%5D.amount=100.00&receiverList.receiver%5B0%5D.primary%5B0%5D=true&receiverList.receiver%5B1%5D.email=mihir.test.rockersinfo%40gmail.com&receiverList.receiver%5B1%5D.amount=11.00&receiverList.receiver%5B1%5D.primary%5B1%5D=false&preapprovalKey=PA-9FA628668U781291T&senderEmail=anita.rockersinfo%40gmail.com', 'Response From cron_preapprove 320::responseEnvelope.timestamp=2014-09-30T01:22:49.632-07:00,responseEnvelope.ack=Failure,responseEnvelope.correlationId=ca3fba05dcf3e,responseEnvelope.build=12595496,error(0).errorId=579024,error(0).domain=PLATFORM,error(0).subdomain=Application,error(0).severity=Error,error(0).category=Application,error(0).message=The preapproval key can''t be used before the start date or after the end date,', 146, 0, 'FAILURE', '2014-09-30 03:22:49'),
(976, 'cancelUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fhome&returnUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fadaptive_paypal%2Fpreapprovereceipt&currencyCode=USD&startingDate=2014-09-15&endingDate=2014-09-18&maxNumberOfPayments=1&maxTotalAmountOfAllPayments=100.00&requestEnvelope.errorLanguage=en_US&memo=Project+%3A+Funders+test%28146%29%2C+Amount+%3A+100.00%2C+Project+Owner+%3A+Jayshree+Patel%2C+On+Site+%3A+http%3A%2F%2Fgroupfund.me%2Findex.php', 'Request From payment case 1 503::responseEnvelope.timestamp=2014-09-15T23:42:37.935-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=663ed41b5e358,responseEnvelope.build=12595496,preapprovalKey=PA-9FA628668U781291T,', 146, 91, 'SUCCESS', '2014-09-16 01:42:37'),
(957, 'cancelUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fhome&returnUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fadaptive_paypal%2Fpreapprovereceipt&currencyCode=USD&startingDate=2014-09-10&endingDate=2014-09-19&maxNumberOfPayments=1&maxTotalAmountOfAllPayments=1000.00&requestEnvelope.errorLanguage=en_US&memo=Project+%3A+Stripe+success%28125%29%2C+Amount+%3A+1000.00%2C+Project+Owner+%3A+Jayshree+Patel%2C+On+Site+%3A+http%3A%2F%2Fgroupfund.me%2Findex.php', 'Request From payment case 1 503::responseEnvelope.timestamp=2014-09-10T23:49:04.773-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=b6e1399fc9ab2,responseEnvelope.build=12595496,preapprovalKey=PA-30837428F1123401T,', 125, 0, 'SUCCESS', '2014-09-11 09:49:04'),
(958, 'cancelUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fhome&returnUrl=http%3A%2F%2Fgroupfund.me%2Findex.php%2Fadaptive_paypal%2Fpreapprovereceipt&currencyCode=USD&startingDate=2014-09-10&endingDate=2014-09-19&maxNumberOfPayments=1&maxTotalAmountOfAllPayments=1000.00&requestEnvelope.errorLanguage=en_US&memo=Project+%3A+Stripe+success%28125%29%2C+Amount+%3A+1000.00%2C+Project+Owner+%3A+Jayshree+Patel%2C+On+Site+%3A+http%3A%2F%2Fgroupfund.me%2Findex.php', 'Request From payment case 1 503::responseEnvelope.timestamp=2014-09-10T23:55:55.298-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=12c0fe57b4bd4,responseEnvelope.build=12595496,preapprovalKey=PA-06B256785E6542054,', 125, 0, 'SUCCESS', '2014-09-11 09:55:55'),
(975, 'preapprovalKey=PA-37F32489D2646243K&requestEnvelope.errorLanguage=en_US', 'Response From preapprovereceipt 854::responseEnvelope.timestamp=2014-09-15T23:42:26.935-07:00,responseEnvelope.ack=Success,responseEnvelope.correlationId=1c1b4eb05960f,responseEnvelope.build=12595496,approved=true,cancelUrl=http://groupfund.me/index.php/home,curPayments=0,curPaymentsAmount=0.00,curPeriodAttempts=0,currencyCode=USD,dateOfMonth=0,dayOfWeek=NO_DAY_SPECIFIED,endingDate=2014-09-18T23:59:26.000-07:00,maxNumberOfPayments=1,maxTotalAmountOfAllPayments=100.00,paymentPeriod=NO_PERIOD_SPECIFIED,pinType=NOT_REQUIRED,returnUrl=http://groupfund.me/index.php/adaptive_paypal/preapprovereceipt,senderEmail=anita.rockersinfo@gmail.com,memo=Project : Funders test(146), Amount : 100.00, Project Owner : Jayshree Patel, On Site : http://groupfund.me/index.php,startingDate=2014-09-15T00:00:26.000-07:00,status=ACTIVE,displayMaxTotalAmount=false,sender.accountId=LXU9FUADSCKUJ,', 146, 91, 'SUCCESS', '2014-09-16 01:42:26');

-- --------------------------------------------------------

--
-- Table structure for table `perk`
--

CREATE TABLE IF NOT EXISTS `perk` (
  `perk_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `perk_title` varchar(255) DEFAULT NULL,
  `perk_description` text,
  `perk_amount` varchar(255) DEFAULT NULL,
  `perk_total` varchar(255) DEFAULT NULL,
  `perk_get` varchar(255) DEFAULT NULL,
  `coupon_id` int(100) DEFAULT NULL,
  `coupon_status` varchar(20) DEFAULT NULL,
  `estdate` datetime NOT NULL,
  `shipping_status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`perk_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `perk`
--

INSERT INTO `perk` (`perk_id`, `project_id`, `perk_title`, `perk_description`, `perk_amount`, `perk_total`, `perk_get`, `coupon_id`, `coupon_status`, `estdate`, `shipping_status`) VALUES
(5, 2, ' Vehicle Acciden', 'The Motor Vehicle Accident Claims Fund (MVACF) is considered to be the "payer of last resort" as it provides compensation to people injured in automobile accidents when no automobile insurance exists to respond to the claim.', '50', '100', NULL, NULL, NULL, '1969-12-31 04:46:21', '0'),
(6, 1, 'state funding ', 'Manifesto at 2015 general election expected to include pledge to ''take the big money out of politics'' - but it has yet to reach a consensus with Tories and Lib ', '50', '100', NULL, NULL, NULL, '2014-10-31 04:50:13', '0'),
(7, 5, 'Education', 'Education in its general sense is a form of learning in which the knowledge, skills, and habits of a group of people are transferred from one generation to the next through teaching, training, or researc', '10', '100', NULL, NULL, NULL, '2014-10-30 04:27:05', '0'),
(8, 4, 'political parties and other campaigner', 'We produce guidance for those we regulate to help them comply with the rules on political funding and spending. If you are a political campaigner please go to our guidance pages for more information on the rules.', '50', '100', NULL, NULL, NULL, '2014-10-31 05:24:02', '0'),
(9, 6, 'donation for children', 'The organization is now urging other groups to take an interest in the education of the children and contribute other items which will ease the cost of securing an education.', '100', '100', NULL, NULL, NULL, '2014-11-07 23:32:01', '0'),
(10, 8, 'sponcer', 'Our community investment professionals follow a rigorous process when evaluating proposals to ensure funds are invested with the utmost care and responsibility. The process includes an assessment of your charity’s financial statements as well as its fiscal management and governance practices', '100', '100', NULL, NULL, NULL, '2014-11-30 00:56:50', '0'),
(12, 10, 'Donation for children', 'This project is open for funding for EVERYWHERE AND EVERYONE in the world! We will ship anywhere and if we meet our video Stretch Goals there will be absolutely no region coding, etc. and PAL versions of the DVD/Blu-Ray will be made.', '2000', '100', NULL, NULL, NULL, '2015-04-18 01:58:57', '0'),
(13, 11, 'concept designer', ' I worked for several years as a concept artist for films and games before taking the leap and starting my own studio with fellow artists Jan Ditlev, Christopher Rabenhorst, and Jonas Springborg.', '100', '100', NULL, NULL, NULL, '2015-02-14 03:01:53', '0'),
(14, 13, '3d screen', 'Easel isn''t the only software that works with Carvey.  You can also use your favorite CAD, CAM, and machine control software to have Carvey carve out complex 3D shapes.', '2000', '100', NULL, NULL, NULL, '2015-04-18 01:55:22', '0'),
(15, 14, 'Moveable parts', 'We have made folding and unfolding super easy by decorating all moveable parts with pictograms, that will take you on a guided tour of how to open and securely locking your REST. ', '2000', '100', NULL, NULL, NULL, '2014-12-20 01:54:10', '0'),
(17, 15, 'Headphone', 'The devices we carry are capable of flawless playback, but the media we run through these devices is subpar.  Sound design for the average consumer revolves around 2D audio on cheap headphones with basic bass and treble controls. Accessibility appears to overrule quality.', '1700', '100', NULL, NULL, NULL, '2015-02-14 01:52:26', '0'),
(18, 19, '3d screen', 'A slim profile bumper case that provides amazing drop protection. Protection that does NOT compromise on the original design', '1500', '100', NULL, NULL, NULL, '2015-04-18 01:51:51', '0'),
(19, 20, 'gifts ', 'The newly created United States Olympic and Paralympic Foundation hopes to significantly increase fundraising, focusing on wealthy donors. "There''s a strong sense that we can be raising a lot more in major gifts than we have been raising," USOC CEO Scott Blackmun said Monday after Jon Denney, a top fundraising executive with Stanford University and former All-America swimmer, was named foundation president.', '1000', '100', NULL, NULL, NULL, '2015-02-14 01:51:20', '0'),
(20, 21, 'Charities ', '3D printing is a form of additive manufacturing technology where a three dimensional object is created by laying down successive layers of material. 3D printers are generally faster, more affordable and easier to use than other additive manufacturing technologies.', '1500', '100', NULL, NULL, NULL, '2014-12-28 01:51:09', '0'),
(21, 23, 'Pledge for perk', 'Pledge for perk', '100', '12', NULL, NULL, NULL, '0000-00-00 00:00:00', '0');

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE IF NOT EXISTS `project` (
  `project_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `user_role` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `project_title` varchar(255) DEFAULT NULL,
  `url_project_title` varchar(255) DEFAULT NULL,
  `project_summary` text,
  `project_address` varchar(255) DEFAULT NULL,
  `project_city` varchar(255) DEFAULT NULL,
  `project_state` varchar(255) DEFAULT NULL,
  `project_country` varchar(255) DEFAULT NULL,
  `project_lat` varchar(255) DEFAULT NULL,
  `project_long` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `description` text,
  `display_page` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `video_type` varchar(10) DEFAULT NULL,
  `video` text,
  `custom_video` text,
  `video_image` varchar(255) DEFAULT NULL,
  `amount` int(10) DEFAULT NULL,
  `amount_get` decimal(10,2) DEFAULT NULL,
  `end_date` datetime NOT NULL,
  `allow_overflow` varchar(255) DEFAULT NULL,
  `host_ip` varchar(255) DEFAULT NULL,
  `total_rate` varchar(255) DEFAULT NULL,
  `vote` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `active` varchar(255) DEFAULT NULL,
  `active_cnt` int(10) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `total_days` int(50) NOT NULL,
  `is_featured` int(10) NOT NULL DEFAULT '0',
  `project_draft` int(10) NOT NULL DEFAULT '0',
  `end_send` int(10) NOT NULL DEFAULT '0',
  `p_google_code` varchar(255) DEFAULT NULL,
  `payment_type` varchar(255) NOT NULL,
  `fundby` varchar(50) DEFAULT NULL,
  `funding_type` varchar(225) DEFAULT NULL,
  `person_type` varchar(255) NOT NULL,
  `deadline` varchar(255) NOT NULL,
  `specif` varchar(50) NOT NULL,
  `pitch_text` text NOT NULL,
  `pitch_media` varchar(255) NOT NULL,
  `pitch_image` varchar(255) NOT NULL,
  `pitch_video` varchar(255) NOT NULL,
  `paypal_email` varchar(255) NOT NULL,
  `paypal_first_name` varchar(255) NOT NULL,
  `paypal_last_name` varchar(255) NOT NULL,
  `promotion_code` varchar(255) NOT NULL,
  `view_counter` varchar(255) NOT NULL,
  `personal_first_name` varchar(255) NOT NULL,
  `personal_last_name` varchar(255) NOT NULL,
  `personal_country` varchar(255) NOT NULL,
  `personal_street` varchar(255) NOT NULL,
  `personal_dob` date NOT NULL,
  `personal_phone_number` varchar(255) NOT NULL,
  `personal_city` varchar(255) NOT NULL,
  `personal_zipcode` varchar(255) NOT NULL,
  `project_currency_symbol` varchar(80) DEFAULT NULL,
  `project_currency_code` varchar(80) DEFAULT NULL,
  `bank_account_country` varchar(65) NOT NULL,
  `routing_number` varchar(65) NOT NULL,
  `account_number` varchar(65) NOT NULL,
  `bank_token_id` varchar(65) NOT NULL,
  `bank_name` varchar(65) NOT NULL,
  `recipient_id` varchar(50) NOT NULL,
  PRIMARY KEY (`project_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`project_id`, `user_id`, `user_role`, `category_id`, `currency_id`, `project_title`, `url_project_title`, `project_summary`, `project_address`, `project_city`, `project_state`, `project_country`, `project_lat`, `project_long`, `color`, `description`, `display_page`, `image`, `video_type`, `video`, `custom_video`, `video_image`, `amount`, `amount_get`, `end_date`, `allow_overflow`, `host_ip`, `total_rate`, `vote`, `status`, `active`, `active_cnt`, `date_added`, `total_days`, `is_featured`, `project_draft`, `end_send`, `p_google_code`, `payment_type`, `fundby`, `funding_type`, `person_type`, `deadline`, `specif`, `pitch_text`, `pitch_media`, `pitch_image`, `pitch_video`, `paypal_email`, `paypal_first_name`, `paypal_last_name`, `promotion_code`, `view_counter`, `personal_first_name`, `personal_last_name`, `personal_country`, `personal_street`, `personal_dob`, `personal_phone_number`, `personal_city`, `personal_zipcode`, `project_currency_symbol`, `project_currency_code`, `bank_account_country`, `routing_number`, `account_number`, `bank_token_id`, `bank_name`, `recipient_id`) VALUES
(1, 2, 'admin', 31, 18, 'state funding of political parties', 'state-funding-of-political-parties', 'Manifesto at 2015 general election expected to include pledge to ''take the big money out of politics'' - but it has yet to reach a consensus with Tories and Lib ', '', 'boston', NULL, '1', NULL, NULL, NULL, NULL, NULL, 'project_51310.jpeg', NULL, 'https://www.youtube.com/watch?v=7OQMomEgliw', NULL, NULL, 999, NULL, '2015-05-31 00:39:51', NULL, '1.22.83.171', NULL, NULL, '1', '2', 1, '2014-10-22 00:39:51', 2, 0, 0, 0, 'UA-0000-10', 'wallet', '', 'Flexible', '', 'specificdate', '2014-10-22', '<div class="widget storyContent article widget-editable viziwyg-section-1024 inpage-widget-8939454 articleContent" style="outline: none; font-size: 1.3em; margin-bottom: 6px; font-family: Georgia, ''Times New Roman'', serif; line-height: normal; margin-top: -12px !important;">\n<p>Labour is threatening to impose a &pound;5,000 cap on donations to political parties and hand them more taxpayers&#39; money without reaching agreement with the Conservatives and the Liberal Democrats.</p>\n</div>\n\n<div class="widget storyContent article widget-editable viziwyg-section-1024 inpage-widget-6138699 articleContent" style="outline: none; font-size: 1.3em; margin-bottom: 6px; font-family: Georgia, ''Times New Roman'', serif; line-height: normal; margin-top: -12px !important;">\n<div class="body " style="outline: none; line-height: 1.4;">\n<p>The Labour manifesto at the 2015 general election is expected to include a pledge to &ldquo;take the big money out of politics&rdquo;. Until now, politicians have accepted that reform of party funding would require agreement between the three main parties. But for the first time, senior Labour figures are suggesting that Ed Miliband might introduce legislation without first reaching consensus if the party wins the election.</p>\n\n<p>Although an incoming Labour Government would try to reach a deal with the Tories and Lib Dems, bringing in millions of pounds more state funding without cross-party agreement&nbsp; would be controversial. It would be attacked by the Tories as a way of compensating Labour for its reduced income from the trade unions. On Wednesday, the GMB union announced it is cutting its affiliation fees to Labour from &pound;1.2m to &pound;150,000 a year over Mr Miliband&#39;s plans to reform the party-union link.</p>\n\n<p>John Denham, the former Cabinet minister who was also Mr Miliband&#39;s parliamentary private secretary, told&nbsp;<em>The Independent</em>&nbsp;the two Coalition parties are &ldquo;dangerously close&rdquo; to breaching the tradition about needing consensus by including the unions in the Government&#39;s Lobbying Bill, which will restrict their spending on campaigns, without consulting the independent Electoral Commission. He said this &ldquo;red line&rdquo; would be &ldquo;100 per cent&rdquo; crossed if the Tories carried out threats to legislate to force union members to &ldquo;opt in&rdquo; to paying affiliation fees to Labour. Mr Miliband wants to make this change as part of his internal Labour reforms but argues that it does not need to be done by law. Mr Denham said: &ldquo;Given the precedent that has been set [on lobbying] and particularly if there are any further moves to legislate on the Labour-union relationship, there would be no obstacle to a Labour government simply legislating after the next election. Absolutely none.&rdquo;</p>\n\n<p>Mr Denham, who stressed he was speaking personally and not for Mr Miliband, led Labour&#39;s negotiating team in talks with the Tories and Lib Dems on party funding which collapsed in July. He said: &ldquo;Labour should campaign very clearly over the next two years on taking the big money out of politics and get a mandate for that, perhaps leaving the question of whether we still need to get consensus open at this stage. Everyone agrees that [consensus] would be better. There needs to be an attempt to establish a public consensus about what is needed.&rdquo;</p>\n\n<p>The Labour MP said the proposed &pound;5,000 donations cap should be &ldquo;decoupled&rdquo; from state funding to boost the prospects of a cross-party agreement, saying trying to deal with both issues&nbsp; at once had proved a huge obstacle this year. &ldquo;I think you have to say we will&nbsp; deal with the big donations first and then separately see if you can make a public case for additional [state] funding,&rdquo; he said.</p>\n\n<p>Although Labour has taken no decisions on taxpayer support, its plan could be modelled on&nbsp; a blueprint by the independent&nbsp; Committee on Standards on Public Life, which proposed&nbsp; parties be recompensed for a donations cap with &pound;23m a year of public money.</p>\n\n<p>Labour and the Lib Dems blamed each other for the breakdown of this year&#39;s negotiations. But they both support a low ceiling on individual donations and could reach agreement if they form a coalition in a hung parliament. The Tories, who receive more large donations, back a higher &pound;50,000 cap.</p>\n\n<p>Miliband allies argue that his current battle with the unions gives him a strong platform to call for changes to &ldquo;take the big money out of politics&rdquo; in 2015. His ideas include lower spending by parties as well as some extra taxpayer support.</p>\n\n<p>Today the Labour leader held an hour-long meeting with Paul Kenny, general secretary of the GMB, who said afterwards&nbsp; it was &ldquo;cordial.&rdquo; He added: &ldquo;Different points were exchanged and we have gone away to think about things. The scale of the issues on the table are probably only just beginning to be realised by all parties.&rdquo;</p>\n\n<p>A senior Labour source described the talks as &ldquo;constructive and businesslike&rdquo;, adding: &ldquo;Ed made clear he wants to press ahead with reforms. He is determined to create a modern, open and transparent relationship with affiliates.&rdquo;</p>\n\n<p>But one union official said: &ldquo;The vision seems to be: ditch the affiliation but don&#39;t ditch the unions altogether. Our role would be one of placard carriers and cheque writers.&rdquo;</p>\n</div>\n</div>\n', 'image', 'project_93478.png', '', 'ankit.rockersinfo@gmail.com', 'ankit', 'patel', '', '73', '', '', '', '', '1969-12-31', '', '', '', '$', 'USD', '', '', '', '', '', ''),
(2, 2, 'admin', 11, 18, 'Motor Vehicle Accident Claims Fund ', 'motor-vehicle-accident-claims-fund', 'The Motor Vehicle Accident Claims Fund (MVACF) is considered to be the "payer of last resort" as it provides compensation to people injured in automobile accide.', '', 'boston', NULL, '1', NULL, NULL, NULL, NULL, NULL, 'project_56200.jpeg', NULL, 'https://www.youtube.com/watch?v=_DcG8jIb2sw', NULL, NULL, 500, NULL, '2015-04-30 00:39:51', NULL, '1.22.83.176', NULL, NULL, '1', '2', 1, '2014-10-22 00:39:51', 2, 0, 0, 0, 'UA-0000-00', 'wallet', '', 'Flexible', '', 'specificdate', '2014-10-22', '<p><strong>Accident Fund Insurance Company of America</strong>&nbsp;is a&nbsp;<a href="http://en.wikipedia.org/wiki/Workers%27_compensation" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="Workers'' compensation">workers&#39; compensation</a>&nbsp;<a href="http://en.wikipedia.org/wiki/Insurance" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="Insurance">insurance</a>&nbsp;company headquartered in<a href="http://en.wikipedia.org/wiki/Lansing,_Michigan" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="Lansing, Michigan">Lansing, Michigan</a>&nbsp;licensed to offer their services in 49 states plus the&nbsp;<a class="mw-redirect" href="http://en.wikipedia.org/wiki/District_of_Columbia" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="District of Columbia">District of Columbia</a>. The company is an operating unit of Accident Fund Holdings, one of the largest writer of workers compensation insurance in the United States according to A.M. Best.The company is currently led by Michael K. Britt, president.</p>\n', 'image', 'project_95326.jpeg', '', 'ankit.rockersinfo@gmail.com', 'ankit', 'patel', '', '23', '', '', '', '', '1969-12-31', '', '', '', '$', 'USD', '', '', '', '', '', ''),
(4, 2, 'admin', 31, 18, 'Political parties, campaigning &amp; donations', 'political-parties-campaigning--donations', 'This section provides an overview of the different rules that control the financial activities of political parties, candidates and other campaigners at major e', '', 'boston', NULL, '1', NULL, NULL, NULL, NULL, NULL, 'project_11966.jpeg', NULL, 'https://www.youtube.com/watch?v=C5BeN0LNvak', NULL, NULL, 888, '800.00', '2015-02-14 00:39:51', NULL, '1.22.83.176', NULL, NULL, '1', '2', 1, '2014-10-22 00:39:51', 2, 0, 0, 0, 'UA-0000-11', 'wallet', '', 'Flexible', '', 'specificdate', '2014-10-22', '<p>We publish details of political parties&rsquo; and other campaigners&rsquo; income and spending on our online database, which include:</p>\n\n<ul>\n	<li>political parties&rsquo; annual accounts</li>\n	<li>donations and loans to political parties and other campaigners</li>\n	<li>campaign spending by political parties and other campaigners</li>\n	<li>a list of registered political parties and other campaigners</li>\n</ul>\n', 'image', 'project_2123.jpeg', '', 'ankit.rockersinfo@gmail.com', 'ankit', 'patel', '', '27', '', '', '', '', '1969-12-31', '', '', '', '$', 'USD', '', '', '', '', '', ''),
(5, 2, 'admin', 47, 18, 'Olive Chapel Elementary receives $10,000 donation for technology', 'olive-chapel-elementary-receives-10000-donation-for-technology', 'Employees of Prime Mortgage Lending of Apex surprised the Olive Chapel Elementary School community at its Artapalooza community event .', '', 'boston', NULL, '1', NULL, NULL, NULL, NULL, NULL, 'project_63938.jpeg', NULL, 'https://www.youtube.com/watch?v=_DcG8jIb2sw', NULL, NULL, 600, '700.00', '2015-01-21 00:39:51', NULL, '1.22.83.176', NULL, NULL, '1', '2', 1, '2014-10-22 00:39:51', 2, 1, 0, 0, 'UA-0000-10', 'wallet', '', 'Flexible', '', 'specificdate', '2014-10-22', '<p>We provide a specialized-niche exchange process that is proven to work. Donation offers and initial requests are monitored and nonprofit participants are screened while we take into consideration options for pick-up, shipping, and tax-exempt status.</p>\n\n<p>We serve as a beneficial asset to many communities and can be especially helpful for isolated communities within or near large metropolitan centers. We are here to cordially, competently and personally assist you with building bridges toward philanthropy while providing advice about technology recycling and reuse for education.</p>\n', 'image', 'project_3787.jpeg', '', 'ankit.rockersinfo@gmail.com', 'ankit', 'patel', '', '82', '', '', '', '', '1969-12-31', '', '', '', '$', 'USD', '', '', '', '', '', ''),
(6, 2, 'admin', 47, 18, 'Donation-accepting organizations and projects', 'donationaccepting-organizations-and-projects', 'Here is a list of organizations that accept bitcoin donations. ... BTC is getting more and more popular at open projects due to it''s own open', '', 'boston', NULL, '1', NULL, NULL, NULL, NULL, NULL, 'project_86547.jpeg', NULL, 'https://www.youtube.com/watch?v=hbIgG4Q-HSg', NULL, NULL, 500, '400.00', '2015-04-23 00:39:51', NULL, '1.22.83.171', NULL, NULL, '1', '2', 1, '2014-10-22 00:39:51', 40, 0, 0, 0, 'UA-0000-012', 'wallet', '', 'Flexible', '', 'specificdate', '2014-11-30', '<p>School children in the sprawling Kibera slum are a happy lot after they were outfitted with new school uniforms courtesy of a local non-governmental organization. One hundred and ninety students at Kibera primary school were outfitted with new uniforms to take them through the year as the first school term begins. The non-governmental organization, Sauti kenya had sought to ascertain the needs of school children in the area in order to give a helping hand to those from disadvantaged families. The organization is now urging other groups to take an interest in the education of the children and contribute other items which will ease the cost of securing an education.</p>\n', 'image', 'project_77.jpeg', '', 'ankit.rockersinfo@gmail.com', 'ankit', 'patel', '', '44', '', '', '', '', '1969-12-31', '', '', '', '$', 'USD', '', '', '', '', '', ''),
(7, 4, '', 33, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'project_99277.jpeg', NULL, NULL, NULL, NULL, 1000, NULL, '2015-02-28 08:12:00', NULL, '27.109.7.130', NULL, NULL, '1', '0', 0, '2014-10-21 08:12:00', 0, 0, 0, 0, NULL, '', NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', '', 'CAD', 'CAD', '', '', '', '', '', ''),
(8, 2, 'admin', 23, 18, 'Sponsor and donate', 'sponsor-and-donate', 'Take a minute to help change someone''s life forever. Make a donation and make a differenc', '', 'boston', NULL, '1', NULL, NULL, NULL, NULL, NULL, 'project_21112.jpeg', NULL, 'https://www.youtube.com/watch?v=yMXeGL1lyxs', NULL, NULL, 900, NULL, '2025-03-21 00:00:00', NULL, '1.22.83.171', NULL, NULL, '1', '1', 0, '2014-10-22 00:37:24', 3693, 0, 0, 0, 'UA-0000-012', 'wallet', '', 'Flexible', '', 'specificdate', '2024-11-30', '<div class="bText">\n<p>The words <em><strong>donation</strong></em> and <em><strong>sponsorship</strong></em> are often used in the same context to describe funds that have been received as gifts. Donations and sponsorships are, however, two completely different concepts in the world of fundraising.</p>\n\n<p><strong>Donations</strong></p>\n\n<p>Donations are made by an individual/company/foundation/trust, etc for specific development projects at tertiary institutions or NGOs. When approaching companies for donations, these funds are usually obtained from the Corporate Social Investment (CSI) departments. Company shareholders decide&nbsp;on a certain percentage of company profit to be allocated to corporate social investment annually.</p>\n\n<p>If there are no conditions to the donations, they are <em>bona fida</em> donations and donors are entitled to tax rebate certificates according to Section 18A of the Income Tax Act.</p>\n\n<p><strong>Sponsorships</strong></p>\n\n<p>Sponsorships are obtained from companies&#39; marketing departments and are regarded as marketing budget expenditure &mdash; therefore sponsorships are not <em>bona fida</em> gifts and are not eligible for tax rebate certificates. &nbsp;</p>\n\n<p>Sponsors regard their contribution as a commercial transaction and expect return on investment by means of marketing opportunities, e.g. the company&#39;s logo on course material, folders, sport bags and clothes; banners at the venue of for example a conference, course, event, etc.</p>\n\n<p>Key selling points for an event or project are: audience size and geographical scope; the prestige of the offering to the audience; opportunity for client entertainment; sole sponsorship; brand exposure opportunitues and value of the exposure.</p>\n</div>\n\n<p>&nbsp;</p>\n', 'image', 'project_74726.jpeg', '', 'ankit.rockersinfo@gmail.com', 'ankit', 'patel', '', '8', '', '', '', '', '1969-12-31', '', '', '', '$', 'USD', '', '', '', '', '', ''),
(10, 5, 'admin', 11, 18, 'VIDEO GAMES LIVE', 'video-games-live', 'Picture the energy and excitement of a rock concert mixed with the power and emotion of a symphony orchestra combined together by the technology, interactivity,', '', 'boston', NULL, '1', NULL, NULL, NULL, NULL, NULL, 'project_56252.jpeg', NULL, 'https://www.youtube.com/watch?v=75HjFi4_r10', NULL, NULL, 4100, NULL, '2014-10-31 00:00:00', NULL, '1.22.82.216', NULL, NULL, '1', '1', 0, '2014-10-30 01:55:39', 2, 0, 0, 0, 'UA-0000-15', 'wallet', '', 'Flexible', '', 'specificdate', '2014-10-31', '<p>The&nbsp; campaign will help us raise the funds to create and produce our fourth album which we have titled &quot;Level 4&quot;. This will be a studio recording (not a live recording).&nbsp; Our hope is to be able to obtain funding for our stretch goals so we can produce a full length live concert motion picture as well.&nbsp; Over the past decade Video Games Live has helped to legitimize video game music and has shown the world the creative, cultural and artistic side of gaming. Although VGL has obtained a lot of accolades and success, the music industry and record companies are VERY different and no music or record company is willing to invest the budget needed to accomplish our goals and put out a top quality product that we and the entire game industry can be proud of. They don&#39;t believe in the culturally artistic significance of video game music and they don&#39;t believe that people are interested in listening after the game is turned off.&nbsp; And that&#39;s where YOU come in!</p>\n', 'image', 'project_85561.jpeg', '', 'ankit.rockersinfo@gmail.com', 'ankit', 'patel', '', '4', '', '', '', '', '1969-12-31', '', '', '', '$', 'USD', '', '', '', '', '', ''),
(11, 5, 'admin', 45, 18, 'world is seeing and supporting', 'world-is-seeing-and-supporting', 'We are over the hills with gratitude and joy! The world is seeing and supporting Fall of GODS coming to life! Thanks everyone!!! So here is a new stretchgoal.', '', 'boston', NULL, '1', NULL, NULL, NULL, NULL, NULL, 'project_53412.jpeg', NULL, 'https://www.youtube.com/watch?v=UqB0HfmduSY&list=PLcvwepZEsuCs9LOExngzQwNKVDu8U54nh', NULL, NULL, 700, NULL, '2016-03-19 06:05:17', NULL, '1.22.83.171', NULL, NULL, '1', '2', 1, '2014-10-27 06:05:17', 509, 0, 0, 0, 'UA-0000-011', 'wallet', '', 'Flexible', '', 'specificdate', '2016-03-13', '<p>Fall of Gods is an illustrated book inspired by Norse mythology, following Vali, a warrior haunted by his past, but who has found peace in the arms of a woman. For years he has lived far from society, tending to his farm and trying to forget the battles he fought&hellip;and the crimes he committed. But one day his love disappears, and he must set out to find her. He will once again have to face the creatures of Jotunheim and the powerful Aesir. Suddenly, the man he thought he had buried deep down inside has risen to the surface once more...and he comes seeking vengeance.</p>\n\n<p>Fall of Gods is all about love for Norse heritage. It&rsquo;s made with a desire to reinvent the mythology and come up with a new and exiting story that captures the essence of the brutal world within the old north. It unfolds in a fantastic universe inspired by Scandinavian nature, with an epic scale and inherent drama. The book will be anchored by detailed illustrations like the ones you see on this page.</p>\n', 'image', 'project_10786.png', '', 'ankit.rockersinfo@gmail.com', 'ankit', 'patel', '', '16', '', '', '', '', '1969-12-31', '', '', '', '$', 'USD', '', '', '', '', '', ''),
(13, 5, 'admin', 32, 18, 'The 3D carving machine ', 'the-3d-carving-machine', 'Carvey is a new, remarkably easy to use, tabletop 3D carving machine for making ideas into real objects. Make your mark.', '', 'boston', NULL, '1', NULL, NULL, NULL, NULL, NULL, 'project_85158.jpeg', NULL, 'https://www.youtube.com/watch?v=yZlnZwDcPjY', NULL, NULL, 4500, NULL, '2014-10-31 00:00:00', NULL, '1.22.82.216', NULL, NULL, '1', '1', 0, '2014-10-30 01:54:45', 2, 0, 0, 0, 'UA-0000-011', 'wallet', '', 'Flexible', '', 'specificdate', '2014-10-31', '<h1>What is Carvey?&nbsp;</h1>\n\n<p>Carvey is a 3D carving machine that allows you to make quality objects out of a variety of materials including wood, metal and plastic. Using Carvey and our free design software, you can make your idea a reality in three easy steps. First, design your product using our software, next choose your material, then click carve. Carvey will cut your design, giving you the power of creation right from your tabletop. Carvey is the next step in 3D manufacturing.</p>\n\n<h1><strong>Step 1: Create your design</strong></h1>\n\n<p>Carvey comes with Easel, our free design software that lets you design in 2D and view in 3D, instantly. What you see on the screen is what you get from the machine.&nbsp;</p>\n\n<p>Easel runs in a web browser and controls Carvey. With it you can carve your own design, a design you&rsquo;ve imported, or a file from our library of projects that other Easel users have created and shared.&nbsp;</p>\n\n<p>Easel isn&#39;t the only software that works with Carvey. &nbsp;You can also use your favorite CAD, CAM, and machine control software to have Carvey carve out complex 3D shapes.</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n', 'image', 'project_48539.jpeg', '', 'ankit.rockersinfo@gmail.com', 'ankit', 'patel', '', '1', '', '', '', '', '1969-12-31', '', '', '', '$', 'USD', '', '', '', '', '', ''),
(14, 5, 'admin', 33, 18, 'World''s most compact foldable seat', 'worlds-most-compact-foldable-seat', 'Never before has something this complex been so simple! A foldable seat, that fits in your pocket, and it looks good. Rest on the go!', '', 'new yourk', NULL, '1', NULL, NULL, NULL, NULL, NULL, 'project_64309.jpeg', NULL, 'https://www.youtube.com/watch?v=svuwuwq3qA0', NULL, NULL, 5000, NULL, '2016-02-14 00:00:00', NULL, '1.22.83.171', NULL, NULL, '1', '1', 0, '2014-10-22 03:30:36', 473, 0, 0, 0, 'UA-0000-011', 'wallet', '', 'Flexible', '', 'specificdate', '2016-02-14', '<p>REST comes in 5 colors! Drop us a line if your favorite is not among these and we will do our best to include it in the next batch.For the first time ergonomic seating is truly portable, inconspicuous enough to use in public and exceptionally functional. REST lets you enjoy all the benefits of portable resting without being a burden to bring along.</p>\n\n<p><strong>Imagine</strong> if you could pop out a foldable seat when your back and knees starts aching, watching your favorite sports or concert</p>\n\n<p><strong>Imagine </strong>not having to carry a clumsy chair with you when going hunting, fishing or bird watching</p>\n\n<p><strong>Remember </strong>standing in 2+ hour passport lines after 8 hours of flight or waiting for delayed transportation&nbsp; &nbsp; &nbsp;</p>\n\n<p><strong>NOW, IMAGINE:&nbsp; </strong>Having a seat in your pocket that will help you stay focused and active, keeping you in eyeheight with your standing friends, while being inconspicuous enough for nobody to notice!</p>\n', 'image', 'project_25872.jpeg', '', 'ankit.rockersinfo@gmail.com', 'ankit', 'patel', '', '1', '', '', '', '', '1969-12-31', '', '', '', '$', 'USD', '', '', '', '', '', ''),
(15, 5, 'admin', 45, 18, ' Wireless Audio Headphones', 'wireless-audio-headphones', 'Bluetooth headphones with a twist - listen to your favorite music AND capture 3D Audio. Hear and capture your world in a whole new way.', '', 'California', NULL, '1', NULL, NULL, NULL, NULL, NULL, 'project_25146.jpeg', NULL, 'https://www.youtube.com/watch?v=DXAE-lqZdLc&list=PLGJuJjmiKcjrjEGbjoHcPVZNiG8-jnq1j', NULL, NULL, 4000, NULL, '2015-10-18 00:00:00', NULL, '1.22.83.171', NULL, NULL, '1', '1', 0, '2014-10-22 03:55:24', 354, 0, 0, 0, 'UA-0000-16', 'wallet', '', 'Flexible', '', 'specificdate', '2015-10-18', '<h1>We live in a 3D sounding world, so why are we capturing it in 2D?</h1>\n\n<p>Most audio, via mobile devices and digital cameras, is captured in what Hooke refers to as 2D. It is often unintelligible and does not capture the feeling of &ldquo;being there&rdquo;. If sound occurs all around us in 3D, we should be capturing it that way. Most headphones, however, are not designed to record audio at all.</p>\n\n<div class="template oembed">&nbsp;</div>\n\n<p><em>Binaural&nbsp;</em><strong>-</strong>&nbsp; a method of recording sound that uses two microphones, arranged with the intent to create a 3-D stereo sound sensation for the listener of actually being in the room with the performers or instruments.<br />\n<br />\nHooke combines the discovery of binaural recording and exceptional playback within one stylish unit.<br />\n<br />\nThe logic behind Hooke is simple: why not put the microphones by your ear instead of in your hand? Our hands do not hear, our ears do.</p>\n', 'image', 'project_41467.jpeg', '', 'ankit.rockersinfo@gmail.com', 'ankit', 'patel', '', '1', '', '', '', '', '1969-12-31', '', '', '', '$', 'USD', '', '', '', '', '', ''),
(19, 5, 'admin', 47, 18, 'Slim impact Bumper for iPhone5', 'slim-impact-bumper-for-iphone5', 'A slim profile bumper case that provides amazing drop protection. Protection that does NOT compromise on the original design', '', 'Minnesota', NULL, '1', NULL, NULL, NULL, NULL, NULL, 'project_48202.jpeg', NULL, 'https://www.youtube.com/watch?v=HxE54_ysNS0', NULL, NULL, 3000, NULL, '2016-03-27 00:00:00', NULL, '1.22.83.171', NULL, NULL, '1', '1', 0, '2014-10-22 04:45:32', 515, 0, 0, 0, 'UA-0000-25', 'wallet', '', 'Flexible', '', 'specificdate', '2016-03-27', '<div class="short_blurb py2 pl1 pr1">\n<p>A slim profile bumper case that provides amazing drop protection. Protection that does NOT compromise on the original design</p>\n</div>\n\n<h1><strong>Product features:</strong></h1>\n\n<ul>\n	<li>Slim profile bumper is only 2.5mm thick</li>\n	<li>Allows for the device to be seen and not wrapped up in a bulky case, the way Steve intended</li>\n	<li>Super-lightweight (weighs 12 grams, less than half an OUNCE!)</li>\n	<li>Ultra-high impact absorbing properties, with no visible damage to the device following drop tests of over 20 feet or 7 meters</li>\n	<li>Hard and soft construction with a matte texture for ease of grip</li>\n	<li>Available in attractive fashion colors including pink, purple, black, white, orange and teal&nbsp;</li>\n	<li>Compatible with third-party headphones (including Beats by Dr. Dre) and lighting cable</li>\n	<li>Design of bumper allows for easy access to all ports and buttons</li>\n	<li>Powered by Egg Drop Technology&nbsp;</li>\n	<li>Available for the <strong>iPhone 5/5S/5C</strong>, the <strong>iPhone 6</strong> and the <strong>iPhone 6 Plus</strong>.</li>\n</ul>\n', 'image', 'project_390.jpeg', '', 'ankit.rockersinfo@gmail.com', 'ankit', 'patel', '', '1', '', '', '', '', '1969-12-31', '', '', '', '$', 'USD', '', '', '', '', '', ''),
(20, 5, 'admin', 23, 18, 'Funding for elite athletes', 'funding-for-elite-athletes', 'The U.S. Olympic team has been successful athletically, winning the overall medal counts in the 2012 London Summer Games with 104 and in the 2010 Vancouver Wint', '', 'Kelly Whiteside', NULL, '1', NULL, NULL, NULL, NULL, NULL, 'project_55712.jpeg', NULL, 'https://www.youtube.com/watch?v=DJAOEATUosc', NULL, NULL, 2000, NULL, '2017-03-12 00:00:00', NULL, '1.22.83.171', NULL, NULL, '1', '1', 0, '2014-10-22 05:09:37', 865, 0, 0, 0, 'UA-0000-25', 'wallet', '', 'Flexible', '', 'specificdate', '2017-03-12', '<p>The U.S. Olympic team has been successful athletically, winning the overall medal counts in the 2012 London Summer Games with 104 and in the 2010 Vancouver Winter Games with 37. And the U.S. Olympic Committee has been successful financially, reporting a $91 million revenue surplus in 2012, according to federal tax filings.</p>\n\n<p>So why do many aspiring Olympians struggle financially?</p>\n\n<p>The USOC receives no government funding, and a down economy hasn&#39;t helped. Some national governing bodies have struggled financially or organizationally. And there are the hard, cold facts &mdash; win and be rewarded. The USOC provides funds to national governing bodies (NGBs) based on performance. As a result, the rich get richer. Last year, the USOC provided grants of more than $4 million to both USA Track &amp; Field and USA Swimming, which produced the largest medal hauls in the London Olympics. The U.S. Ski &amp; Snowboard Association &mdash; which claimed 21 medals in Vancouver in 2010 &mdash; was the only other NGB to top $4 million in grants.</p>\n\n<p>USA Gymnastics ($2.7 million), U.S. Speedskating ($2.7 million) and USA Shooting ($2.4 million) were the only other sports to receive more than $2 million in grants.</p>\n\n<p>After the funding allocations are made, individual NGBs distribute the money based on performance and potential. Some NGBs, especially those with a lineup of corporate sponsors backing them, are able to provide athletes with more resources. Others, such as U.S. Speedskating, provide ice time, coaches, trainers and uniforms but no cash support beyond the USOC money it receives and distributes to athletes.</p>\n\n<p>&quot;The bottom line is everyone has to understand there&#39;s a clear path, and they are result- and performance-based,&quot; U.S. Speedskating President Mike Plant said. &quot;It&#39;s a little bit of a crystal ball. It&#39;s based on the results of last year and who looks like they have a shot at achieving Olympic success. That&#39;s what this has to be about, and unfortunately that&#39;s not easy for some individuals to really kind of deal with.&quot;</p>\n\n<p>Over the past four-year cycle, from 2009 to 2012, 84% of USOC expenditures, nearly $568 million, went to U.S. athletes and NGBs through direct support and what it calls programming (high-performance support programs, training centers), according to a report released by the USOC in April.</p>\n\n<p>The newly created United States Olympic and Paralympic Foundation hopes to significantly increase fundraising, focusing on wealthy donors. &quot;There&#39;s a strong sense that we can be raising a lot more in major gifts than we have been raising,&quot; USOC CEO Scott Blackmun said Monday after Jon Denney, a top fundraising executive with Stanford University and former All-America swimmer, was named foundation president.</p>\n\n<p>Supported mainly by revenue from television rights and corporate sponsorships, the USOC funds 1,500 winter and summer athletes, Blackmun said, but the goal is to support an additional 1,000, funding every member of a national team.</p>\n', 'image', 'project_41863.jpeg', '', 'ankit.rockersinfo@gmail.com', 'ankit', 'patel', '', '1', '', '', '', '', '1969-12-31', '', '', '', '$', 'USD', '', '', '', '', '', ''),
(21, 5, 'admin', 47, 18, '3D Printer', '3d-printer', 'Why TAZ 2 is the Best Desktop 3D Printer for You:nnTAZ offers the largest build area of any assembled desktop 3D printer. ', '', 'boston', NULL, '1', NULL, NULL, NULL, NULL, NULL, 'project_39680.jpeg', NULL, 'https://googleads.g.doubleclick.net/aclk?sa=L&ai=CTNwd5sRNVNW9GYelugSal4LQDtWMju8FtZXhmNQB2dkeEAIg4_uTAygCUMe-8vP6_____wFg5ZLlg6gOyAEBqQLfKi0WaI5RPqgDAaoEjAFP0JsZ_j7qrLKW4LnVmrvR08qyilKuTtOTgSE279yWmtwDaLjqSDHKLHU_O26oP78vIKrJfFkKyzUdzN9WO59ktqvQ8dYAA6Rpvt67qVf5GeJN1ajz_KAruIBpKL3IdgPBMVw0ZYIKKhkaRpOBqq0NlBgzNF2-YR5T7hWEbaXKnjLN0bhFltn68kqn0KAGGsgG_crogtQBgAed-fI1&num=2&sig=AOD64_3nzBEcwwLbzqkA9VAw8gxoARFohg&ctype=21&video_id=7ZdPdBRAPQk&client=ca-pub-6219811747049371&adurl=http://www.youtube.com/channel/UCfaZTOrxkbQl5ZxURIQDXiA%3Fv%3D7ZdPdBRAPQk', NULL, NULL, 2100, NULL, '2015-12-20 00:00:00', NULL, '1.22.80.241', NULL, NULL, '1', '1', 0, '2014-10-26 22:55:35', 417, 0, 0, 0, 'UA-0000-00', 'wallet', '', 'Flexible', '', 'specificdate', '2015-12-20', '<p>Built upon the philosophy of freedom, Aleph Objects, Inc. is transforming the 3D printer industry. Based in Loveland, Colorado, we founded the LulzBot brand on a dedication to the free and open exchange of information and innovation, and in doing so, have made a name for ourselves in the growing maker community. In fact, LulzBot 3D printers are the only 3D printers to carry the Free Software Foundation&#39;s Respects Your Freedom certification. Our TAZ printer is our fifth generation printer and is representative of the best and brightest ideas sourced from a global community of passionate innovators.</p>\n\n<p>Why TAZ 2 is the Best Desktop 3D Printer for You:</p>\n\n<p>TAZ offers the largest build area of any assembled desktop 3D printer. Print your large part in one complete piece or multitask by printing many smaller prints simultaneously.<br />\nNew to TAZ 2 print without being attached to a computer. TAZ 2 comes with a LCD display with a SD card reader for untethered printing.&nbsp;<br />\nYou have the flexibility to use the optimal material for your project by choosing from the broadest selection of printing material.<br />\nMinimize your ongoing printing cost with free software upgrades, free upgraded designs and the ability to do self repair.<br />\nCreate detailed, higher quality prints with 75 micron print resolution.<br />\nFinish your prints faster and more reliably with excellent hot-end control and 200mm/s print speed.</p>\n', 'image', 'project_925.jpeg', '', 'ankit.rockersinfo@gmail.com', 'ankit', 'patel', '', '3', '', '', '', '', '1969-12-31', '', '', '', '$', 'USD', '', '', '', '', '', ''),
(23, 3, '', 33, 18, 'Technology and telecom', 'technology-and-telecom', 'Technology and telecom', '', 'Baroda', NULL, '2', NULL, NULL, NULL, NULL, NULL, 'project_72935.jpeg', NULL, 'http://youtu.be/EmNWWZCW80Q', NULL, NULL, 900, NULL, '2014-10-30 00:00:00', NULL, '27.109.7.130', NULL, NULL, '1', '1', 0, '2014-10-27 05:58:24', 2, 0, 0, 0, '', 'wallet', '', 'Fixed', '', 'specificdate', '2014-10-30', '<p>Test wallet and paypal</p>\n', 'video', '', 'http://i1.ytimg.com/vi/EmNWWZCW80Q/hqdefault.jpg', 'jayshreepatel04@yahoo.com', 'Jayshree', 'Patel', '', '7', '', '', '', '', '1969-12-31', '', '', '', '$', 'USD', '', '', '', '', '', ''),
(28, 3, '', 11, 18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1000, NULL, '2014-10-31 00:58:56', NULL, '1.22.82.216', NULL, NULL, '1', '0', 0, '2014-10-30 00:58:56', 0, 0, 0, 0, NULL, '', NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', '', '$', 'USD', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `project_category`
--

CREATE TABLE IF NOT EXISTS `project_category` (
  `project_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) DEFAULT NULL,
  `project_category_name` varchar(255) DEFAULT NULL,
  `url_category_title` varchar(255) NOT NULL,
  `active` varchar(255) DEFAULT NULL,
  `parent_project_category_id` int(100) NOT NULL DEFAULT '0',
  PRIMARY KEY (`project_category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=48 ;

--
-- Dumping data for table `project_category`
--

INSERT INTO `project_category` (`project_category_id`, `language_id`, `project_category_name`, `url_category_title`, `active`, `parent_project_category_id`) VALUES
(11, 1, 'Accidents & Personal Crisis', 'accidents-personal-crisis', '1', 0),
(23, 1, 'Sports', 'sports', '1', 0),
(24, 1, 'Animals', 'animals', '1', 0),
(31, 1, 'Politics', 'politics1', '1', 0),
(32, 1, 'Creativity', 'creativity1', '1', 0),
(33, 1, 'Nature', 'nature1', '1', 0),
(34, 1, 'Nature sub', 'nature-sub1', '1', 33),
(38, 1, 'Other & Miscellaneous', 'other--miscellaneous1', '1', 32),
(39, 2, 'sportif', 'sports_french', '0', 0),
(45, 1, 'Community', 'community2', '1', 0),
(47, 1, 'Technology', 'technology1', '1', 0);

-- --------------------------------------------------------

--
-- Table structure for table `project_favourite`
--

CREATE TABLE IF NOT EXISTS `project_favourite` (
  `favourite_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_added` date NOT NULL,
  PRIMARY KEY (`favourite_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `project_flag`
--

CREATE TABLE IF NOT EXISTS `project_flag` (
  `project_flag_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_flag_name` varchar(255) DEFAULT NULL,
  `active` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`project_flag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `project_follower`
--

CREATE TABLE IF NOT EXISTS `project_follower` (
  `project_follow_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `project_follow_user_id` int(11) NOT NULL,
  `project_follow_date` datetime NOT NULL,
  PRIMARY KEY (`project_follow_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `project_follower`
--

INSERT INTO `project_follower` (`project_follow_id`, `project_id`, `project_follow_user_id`, `project_follow_date`) VALUES
(2, 5, 5, '2014-10-29 03:37:03'),
(3, 5, 3, '2014-10-30 00:07:33'),
(4, 5, 4, '2014-10-30 00:28:24'),
(5, 6, 3, '2014-10-30 01:14:05'),
(6, 6, 5, '2014-10-30 01:22:12'),
(7, 6, 4, '2014-10-30 01:29:05'),
(8, 4, 4, '2014-10-30 02:59:30'),
(9, 4, 5, '2014-10-30 03:04:05'),
(10, 4, 3, '2014-10-30 03:05:41');

-- --------------------------------------------------------

--
-- Table structure for table `project_gallery`
--

CREATE TABLE IF NOT EXISTS `project_gallery` (
  `project_gallery_id` int(100) NOT NULL AUTO_INCREMENT,
  `project_id` int(100) NOT NULL,
  `project_image` varchar(255) DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `image_name` varchar(255) NOT NULL,
  `image_desc` text NOT NULL,
  PRIMARY KEY (`project_gallery_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `project_gallery`
--

INSERT INTO `project_gallery` (`project_gallery_id`, `project_id`, `project_image`, `date_added`, `image_name`, `image_desc`) VALUES
(1, 5, 'project_85384.jpeg', '2014-10-21 05:20:26', 'Technology Invented', 'The technology itself is based on the exposition almost all science museums have (when you stick your hand in and lift sticks to make a figure). But to use that existing technology to create something like this is genius.'),
(2, 4, 'project_991.jpeg', '2014-10-21 05:28:19', 'political fund', 'If you nationally fund parties it would be more democratic, and give all elections campaigns a fair footing. '),
(4, 1, 'project_93555.jpeg', '2014-10-30 01:02:52', 'Fund', 'The idea of community may simply come down to supporting and interacting positively with other individuals who share a vested interest.'),
(5, 5, 'project_44786.jpeg', '2014-10-21 22:52:43', 'Technology Invented', 'At the MIT Media Lab, the Tangible Media Group believes the future of computing is tactile. Unveiled today, the inFORM is MIT''s new scrying pool for imagining the interfaces of tomorrow. Almost like a table of living clay, the inFORM is a surface that three-dimensionally changes shape, allowing users to not only interact with digital content in meatspace, but even hold hands with a person hundreds of miles away. And that''s only the beginning.'),
(6, 2, 'project_35738.png', '2014-10-21 23:05:12', 'Accident Claims Fund ', 'The Motor Vehicle Accident Claims Fund (MVACF) is considered to be the "payer of last resort" as it provides compensation to people injured in automobile accidents when no automobile insurance exists to respond to the claim.'),
(7, 6, 'project_94966.jpeg', '2014-10-21 23:36:56', 'education studying', 'If you are looking to make donations for a good cause, donations to rural children''s education in india, donations for poor children in need,'),
(8, 8, 'project_67019.png', '2014-10-22 00:41:44', 'investment professionals', 'Our community investment professionals follow a rigorous process when evaluating proposals to ensure funds are invested with the utmost care and responsibility. The process includes an assessment of your charity’s financial statements as well as its fiscal management and governance practices. We require our charitable partners to clearly demonstrate the social impact of our contribution. Our funding evaluation process may also include a review by regional and/or national committees.'),
(10, 10, 'project_96929.jpeg', '2014-10-22 01:22:16', 'Technology Invented', 'ach music segment in Video Games Live is personally arranged and orchestrated by the original composers and also uses input from the actual game designers & producers and game developers & publishers.'),
(11, 11, 'project_51713.jpeg', '2014-10-22 03:04:57', 'Transplantation', ' This animated video explains the transplant waiting list, how someone becomes a donor, the process of matching organs, and signing up to share the gift of life.'),
(12, 13, 'project_80170.jpeg', '2014-10-22 03:19:17', 'real products', 'Connect Carvey to your computer via USB, click “Carve”, and Carvey does the rest.  Sit back and watch as Carvey cuts or engraves your design. Carvey enables you to create real objects, with the same level of quality as what you see on store shelves such'),
(13, 14, 'project_64631.png', '0000-00-00 00:00:00', '', ''),
(14, 15, 'project_21643.jpeg', '2014-10-22 04:30:19', 'Technology Invented', 'The devices we carry are capable of flawless playback, but the media we run through these devices is subpar.'),
(15, 19, 'project_94633.jpeg', '2014-10-22 04:57:16', 'investment professionals', 'The Crash Guard bumper features a minimal design approach. Our goal is to cover up as little of your device as possible while adding maximum protection'),
(16, 20, 'project_55338.jpeg', '2014-10-22 05:17:45', 'real products', ' A coding kit that makes it easy for anyone to learn programming, create games, program robots, control hard'),
(17, 21, 'project_97840.jpeg', '2014-10-26 23:21:50', 'technology ', '3D printing is a form of additive manufacturing technology where a three dimensional object is created by laying down successive layers of material. 3D printers are generally faster, more affordable and easier to use than other additive manufacturing technologies.');

-- --------------------------------------------------------

--
-- Table structure for table `project_setting`
--

CREATE TABLE IF NOT EXISTS `project_setting` (
  `project_setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `enable_paypal` varchar(255) DEFAULT NULL,
  `payment_flow` varchar(255) DEFAULT NULL,
  `pay_fee` varchar(255) DEFAULT NULL,
  `project_listing_fee` varchar(255) DEFAULT NULL,
  `project_listing_fee_type` varchar(255) DEFAULT NULL,
  `commission` varchar(255) DEFAULT NULL,
  `project_short_desc_length` varchar(255) DEFAULT NULL,
  `site_stat_front` varchar(255) DEFAULT NULL,
  `min_project_amount` varchar(255) DEFAULT NULL,
  `max_project_amount` varchar(255) DEFAULT NULL,
  `project_user` varchar(255) DEFAULT NULL,
  `edit_amount` varchar(255) DEFAULT NULL,
  `edit_end_date` date NOT NULL,
  `approve_project` varchar(255) DEFAULT NULL,
  `cancel_project` varchar(255) DEFAULT NULL,
  `default_pledge` varchar(255) DEFAULT NULL,
  `enable_fixed_pledge` varchar(255) DEFAULT NULL,
  `enable_owner_fixed_amount` varchar(255) DEFAULT NULL,
  `enable_multiple_type` varchar(255) DEFAULT NULL,
  `enable_owner_multiple_amount` varchar(255) DEFAULT NULL,
  `enable_suggested_type` varchar(255) DEFAULT NULL,
  `enable_owner_suggested_amount` varchar(255) DEFAULT NULL,
  `enable_min_amount` varchar(255) DEFAULT NULL,
  `enable_owner_min_amount` varchar(255) DEFAULT NULL,
  `allow_guest_backers_list` varchar(255) DEFAULT NULL,
  `allow_guest_backers_amount` varchar(255) DEFAULT NULL,
  `allow_backers_amount` varchar(255) DEFAULT NULL,
  `allow_cancel_pledge_backers` varchar(255) DEFAULT NULL,
  `min_days_pledge_cancel` varchar(255) DEFAULT NULL,
  `allow_cancel_pledge_owner` varchar(255) DEFAULT NULL,
  `allow_owner_fund_own` varchar(255) DEFAULT NULL,
  `allow_owner_fund_other` varchar(255) DEFAULT NULL,
  `enable_project_reward` varchar(255) DEFAULT NULL,
  `reward_detail_optional` varchar(255) DEFAULT NULL,
  `allow_owner_edit_reward` varchar(255) DEFAULT NULL,
  `small_project_max_days` varchar(255) DEFAULT NULL,
  `small_project_max_amount` varchar(255) DEFAULT NULL,
  `funded_percentage` varchar(255) DEFAULT NULL,
  `no_of_category` varchar(255) DEFAULT NULL,
  `enable_overfund` varchar(255) DEFAULT NULL,
  `owner_set_overfund` varchar(255) DEFAULT NULL,
  `enable_project_follower` varchar(255) DEFAULT NULL,
  `enable_project_comment` varchar(255) DEFAULT NULL,
  `enable_update_comment` varchar(255) DEFAULT NULL,
  `enable_project_rating` varchar(255) DEFAULT NULL,
  `logged_user_login` varchar(255) DEFAULT NULL,
  `enable_project_flag` varchar(255) DEFAULT NULL,
  `auto_suspend_project` varchar(255) DEFAULT NULL,
  `auto_fund_capture` varchar(255) DEFAULT NULL,
  `auto_suspend_comment` varchar(255) DEFAULT NULL,
  `auto_suspend_update` varchar(255) DEFAULT NULL,
  `auto_suspend` varchar(255) DEFAULT NULL,
  `auto_suspend_message` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`project_setting_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `project_setting`
--

INSERT INTO `project_setting` (`project_setting_id`, `enable_paypal`, `payment_flow`, `pay_fee`, `project_listing_fee`, `project_listing_fee_type`, `commission`, `project_short_desc_length`, `site_stat_front`, `min_project_amount`, `max_project_amount`, `project_user`, `edit_amount`, `edit_end_date`, `approve_project`, `cancel_project`, `default_pledge`, `enable_fixed_pledge`, `enable_owner_fixed_amount`, `enable_multiple_type`, `enable_owner_multiple_amount`, `enable_suggested_type`, `enable_owner_suggested_amount`, `enable_min_amount`, `enable_owner_min_amount`, `allow_guest_backers_list`, `allow_guest_backers_amount`, `allow_backers_amount`, `allow_cancel_pledge_backers`, `min_days_pledge_cancel`, `allow_cancel_pledge_owner`, `allow_owner_fund_own`, `allow_owner_fund_other`, `enable_project_reward`, `reward_detail_optional`, `allow_owner_edit_reward`, `small_project_max_days`, `small_project_max_amount`, `funded_percentage`, `no_of_category`, `enable_overfund`, `owner_set_overfund`, `enable_project_follower`, `enable_project_comment`, `enable_update_comment`, `enable_project_rating`, `logged_user_login`, `enable_project_flag`, `auto_suspend_project`, `auto_fund_capture`, `auto_suspend_comment`, `auto_suspend_update`, `auto_suspend`, `auto_suspend_message`) VALUES
(1, '0', '0', '5', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0000-00-00', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `project_status`
--

CREATE TABLE IF NOT EXISTS `project_status` (
  `project_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_status_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`project_status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `project_status`
--

INSERT INTO `project_status` (`project_status_id`, `project_status_name`) VALUES
(1, 'Started'),
(2, 'Finished'),
(3, 'Paused'),
(4, '50% finished'),
(5, 'Stopped');

-- --------------------------------------------------------

--
-- Table structure for table `rights`
--

CREATE TABLE IF NOT EXISTS `rights` (
  `rights_id` int(100) NOT NULL AUTO_INCREMENT,
  `rights_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`rights_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=63 ;

--
-- Dumping data for table `rights`
--

INSERT INTO `rights` (`rights_id`, `rights_name`) VALUES
(1, 'list_admin'),
(2, 'admin_login'),
(3, 'list_user'),
(4, 'user_login'),
(5, 'list_project_category'),
(6, 'list_project'),
(7, 'list_idea'),
(8, 'list_gallery'),
(9, 'list_amazon'),
(10, 'list_paypal'),
(11, 'list_normal_paypal'),
(12, 'list_transaction'),
(13, 'home_page'),
(14, 'list_pages'),
(15, 'list_country'),
(16, 'list_state'),
(17, 'list_city'),
(18, 'add_site_setting'),
(19, 'add_meta_setting'),
(20, 'add_facebook_setting'),
(21, 'add_twitter_setting'),
(22, 'add_email_setting'),
(23, 'add_email_template'),
(24, 'assign_rights'),
(25, 'add_spam_setting'),
(26, 'spam_report'),
(27, 'spamer'),
(28, 'list_newsletter'),
(29, 'list_newsletter_user'),
(30, 'newsletter_setting'),
(31, 'newsletter_job'),
(34, 'list_faq_category'),
(35, 'list_faq'),
(36, 'list_school'),
(37, 'guidelines'),
(38, 'add_wallet_setting'),
(39, 'list_payment_gateway'),
(40, 'list_gateway_detail'),
(41, 'list_wallet_review'),
(42, 'list_wallet_withdraw'),
(48, 'project_report'),
(47, 'add_image_setting'),
(49, 'transaction_report'),
(50, 'add_filter_setting'),
(51, 'list_cronjob'),
(52, 'set_fund'),
(53, 'add_yahoo_setting'),
(54, 'add_google_setting'),
(55, 'affiliates'),
(56, 'add_message_setting'),
(57, 'list_message'),
(58, 'list_credit_card'),
(59, 'list_stripe'),
(60, 'add_youtube_setting'),
(61, 'add_google_plus_setting'),
(62, 'add_linkdin_setting');

-- --------------------------------------------------------

--
-- Table structure for table `rights_assign`
--

CREATE TABLE IF NOT EXISTS `rights_assign` (
  `assign_id` int(100) NOT NULL AUTO_INCREMENT,
  `admin_id` int(100) NOT NULL,
  `rights_id` int(100) NOT NULL,
  `rights_set` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`assign_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=610 ;

--
-- Dumping data for table `rights_assign`
--

INSERT INTO `rights_assign` (`assign_id`, `admin_id`, `rights_id`, `rights_set`) VALUES
(1, 1, 1, 1),
(2, 1, 2, 1),
(3, 1, 3, 1),
(4, 1, 4, 1),
(5, 1, 5, 1),
(6, 1, 6, 1),
(7, 1, 7, 1),
(8, 1, 8, 1),
(9, 1, 9, 1),
(10, 1, 10, 1),
(11, 1, 11, 1),
(12, 1, 12, 1),
(13, 1, 13, 1),
(14, 1, 14, 1),
(15, 1, 15, 1),
(16, 1, 16, 1),
(17, 1, 17, 1),
(18, 1, 18, 1),
(19, 1, 19, 1),
(20, 1, 20, 1),
(21, 1, 21, 1),
(22, 1, 22, 1),
(23, 1, 23, 1),
(24, 1, 24, 1),
(25, 1, 25, 1),
(26, 1, 26, 1),
(27, 1, 27, 1),
(28, 1, 28, 1),
(29, 1, 29, 1),
(30, 1, 30, 1),
(31, 1, 31, 1),
(33, 1, 34, 1),
(34, 1, 35, 1),
(35, 1, 36, 1),
(36, 1, 37, 1),
(37, 1, 38, 1),
(38, 1, 39, 1),
(39, 1, 40, 1),
(40, 1, 41, 1),
(41, 1, 42, 1),
(42, 1, 47, 1),
(43, 1, 48, 1),
(44, 1, 49, 1),
(45, 1, 50, 1),
(46, 1, 51, 1),
(47, 1, 52, 1),
(48, 1, 53, 1),
(49, 1, 54, 1),
(50, 1, 55, 1),
(51, 1, 56, 1),
(52, 1, 57, 1),
(53, 1, 58, 1),
(553, 27, 62, 1),
(552, 27, 61, 1),
(551, 27, 60, 1),
(550, 27, 59, 1),
(549, 27, 58, 1),
(548, 27, 57, 1),
(547, 27, 56, 1),
(546, 27, 55, 1),
(545, 27, 54, 1),
(544, 27, 53, 1),
(543, 27, 52, 1),
(542, 27, 51, 1),
(541, 27, 50, 1),
(540, 27, 49, 1),
(539, 27, 48, 1),
(538, 27, 47, 1),
(537, 27, 42, 1),
(536, 27, 41, 1),
(535, 27, 40, 1),
(534, 27, 39, 1),
(533, 27, 38, 1),
(532, 27, 37, 1),
(531, 27, 36, 1),
(530, 27, 35, 1),
(529, 27, 34, 1),
(528, 27, 31, 1),
(527, 27, 30, 1),
(526, 27, 29, 1),
(525, 27, 28, 1),
(524, 27, 27, 1),
(523, 27, 26, 1),
(522, 27, 25, 1),
(521, 27, 24, 1),
(520, 27, 23, 1),
(519, 27, 22, 1),
(518, 27, 21, 1),
(517, 27, 20, 1),
(516, 27, 19, 1),
(515, 27, 18, 1),
(514, 27, 17, 1),
(513, 27, 16, 1),
(512, 27, 15, 1),
(511, 27, 14, 1),
(510, 27, 13, 1),
(509, 27, 12, 1),
(508, 27, 11, 1),
(507, 27, 10, 1),
(506, 27, 9, 1),
(217, 8, 62, 1),
(216, 8, 61, 1),
(215, 8, 60, 1),
(214, 8, 59, 1),
(106, 8, 1, 1),
(107, 8, 2, 1),
(108, 8, 3, 1),
(109, 8, 4, 1),
(110, 8, 5, 1),
(111, 8, 6, 1),
(112, 8, 7, 1),
(113, 8, 8, 1),
(114, 8, 9, 1),
(115, 8, 10, 1),
(116, 8, 11, 1),
(117, 8, 12, 1),
(118, 8, 13, 1),
(119, 8, 14, 1),
(120, 8, 15, 1),
(121, 8, 16, 1),
(122, 8, 17, 1),
(123, 8, 18, 1),
(124, 8, 19, 1),
(125, 8, 20, 1),
(126, 8, 21, 1),
(127, 8, 22, 1),
(128, 8, 23, 1),
(129, 8, 24, 1),
(130, 8, 25, 1),
(131, 8, 26, 1),
(132, 8, 27, 1),
(133, 8, 28, 1),
(134, 8, 29, 1),
(135, 8, 30, 1),
(136, 8, 31, 1),
(137, 8, 34, 1),
(138, 8, 35, 1),
(139, 8, 36, 1),
(140, 8, 37, 1),
(141, 8, 38, 1),
(142, 8, 39, 1),
(143, 8, 40, 1),
(144, 8, 41, 1),
(145, 8, 42, 1),
(146, 8, 47, 1),
(147, 8, 48, 1),
(148, 8, 49, 1),
(149, 8, 50, 1),
(150, 8, 51, 1),
(151, 8, 52, 1),
(152, 8, 53, 1),
(153, 8, 54, 1),
(154, 8, 55, 1),
(155, 8, 56, 1),
(156, 8, 57, 1),
(157, 8, 58, 1),
(505, 27, 8, 1),
(504, 27, 7, 1),
(503, 27, 6, 1),
(502, 27, 5, 1),
(501, 27, 4, 1),
(500, 27, 3, 1),
(499, 27, 2, 1),
(498, 27, 1, 1),
(210, 1, 59, 1),
(211, 1, 60, 1),
(212, 1, 61, 1),
(213, 1, 62, 1);

-- --------------------------------------------------------

--
-- Table structure for table `school`
--

CREATE TABLE IF NOT EXISTS `school` (
  `school_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `school_url_title` varchar(255) DEFAULT NULL,
  `description` text,
  `active` varchar(20) DEFAULT NULL,
  `school_order` int(10) NOT NULL,
  PRIMARY KEY (`school_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `school`
--

INSERT INTO `school` (`school_id`, `title`, `school_url_title`, `description`, `active`, `school_order`) VALUES
(1, 'Defining Your Project', 'defining-your-project', '<p>Whether it&rsquo;s a book, a film, or a piece of hardware, the one trait that every FundraisingScript campaign shares is that it is a&nbsp;<em>project.</em>&nbsp;Defining what your FundraisingScript project&nbsp;<em>is</em>&nbsp;is the first step for every creator.</p>\n<p>What are you raising funds to do? Having a focused and well-defined project with a clear beginning and end is vital. For example: recording a new album is a finite project &mdash; the project finishes when the band releases the album &mdash; but launching a music career is not. There is no end, just an ongoing effort. FundraisingScript is open only to finite projects.</p>\n<p>With a precisely defined goal, expectations are transparent for both the creator and potential backers. Backers can judge how realistic the project&rsquo;s goals are, as well as the project creator&rsquo;s ability to complete them. And for creators, the practice of defining a project&rsquo;s goal establishes the scope of the endeavor, often an important step in the creative process.</p>\n<p>FundraisingScript thrives on these open exchanges and clear explanations of goals. Make sure your project does this!</p>\n<p><strong>SENDING US YOUR IDEA</strong></p>\n<p>Before launching a project, we ask people to share their idea with us to make sure it fits our&nbsp;<a href=KSYDOUhttp://spicyfund.com/fund_demo/help/guidelinesKSYDOU>guidelines</a>&nbsp;and makes good use of the platform. If youKSYSINGre unsure if your project is a good fit for FundraisingScript (or if FundraisingScript is a good fit for your project), weKSYSINGd encourage you to peruse&nbsp;&nbsp;<a href=KSYDOUhttp://spicyfund.com/fund_demo/project/successfulKSYDOU>successful</a>&nbsp;projects in your projectKSYSINGs category. When youKSYSINGre ready, send us your proposal. WeKSYSINGd love to hear about your project!</p>', '1', 1),
(2, 'Creating Rewards', 'creating-rewards', '<p>Rewards are what backers receive in exchange for pledging to a project. The importance of creative, tangible, and fairly priced rewards cannot be overstated. Projects whose rewards are overpriced or uninspired struggle to find support.</p>\n<p><strong>DECIDING WHAT TO OFFER</strong></p>\n<p>Every project&rsquo;s primary rewards should be things made by the project itself. If the project is to record a new album, then rewards should include a copy of the CD when it&rsquo;s finished. Rewards ensure that backers will benefit from a project just as much as its creator (i.e., they get cool stuff that they helped make possible!).</p>\n<p>There are four common reward types that we see on FundraisingScript:</p>\n<ul>\n<li><em>Copies of the thing:</em>&nbsp;the album, the DVD, a print from the show. These items should be priced what they would cost in a retail environment.</li>\n<li><em>Creative collaborations:</em>&nbsp;a backer appears as a hero in the comic, everyone gets painted into the mural, two backers do the handclaps for track 3.</li>\n<li><em>Creative experiences:</em>&nbsp;a visit to the set, a phone call from the author, dinner with the cast, a concert in your backyard.</li>\n<li><em>Creative mementos:</em>&nbsp;Polaroids sent from location, thanks in the credits, meaningful tokens that tell a story.</li>\n</ul>\n<p><strong>DECIDING HOW TO PRICE</strong></p>\n<p>FundraisingScript isn&rsquo;t charity: we champion exchanges that are a mix of commerce and patronage, and the numbers bear this out. To date the most popular pledge amount is $25 and the average pledge is around $70. Small amounts are where it&rsquo;s at: projects<em>without</em>&nbsp;a reward less than $20 succeed 35% of the time, while projects&nbsp;<em>with</em>&nbsp;a reward less than $20 succeed 54% of the time.</p>\n<p>So what works? Offering something of value. Actual value considers more than just sticker price. If it&rsquo;s a limited edition or a one-of-a-kind experience, there&rsquo;s a lot of flexibility based on your audience. But if it&rsquo;s a manufactured good, then it&rsquo;s a good idea to stay reasonably close to its real-world cost.</p>\n<p>There is no magic bullet, and we encourage every project to be as creative and true to itself as possible. Put yourself in your backers&rsquo; shoes: would&nbsp;<em>you</em>&nbsp;drop the cash on your rewards? The answer to that question will tell you a lot about your project&rsquo;s potential.</p>', '1', 2),
(3, 'Setting Your Goal', 'setting-your-goal', '<p>FundraisingScript operates on an all-or-nothing funding model where projects must be fully funded or no money changes hands. Projects must set a funding goal and a length of time to reach it. There&rsquo;s no magic formula to determining the right goal or duration. Every project is different, but there are a few things to keep in mind.</p>\n<p><strong>RESEARCHING YOUR BUDGET</strong></p>\n<p>How much money do you need? Are you raising the full budget or a portion of it? Have you factored in the cost of producing rewards and delivering them to backers? Avoid later headaches by doing your research, and be as transparent as you can. Backers will appreciate it.</p>\n<p><strong>CONSIDERING YOUR NETWORKS</strong></p>\n<p>FundraisingScript is not a magical source of money. Funding comes from a variety of sources &mdash; your audience, your friends and family, your broader social networks, and, if your project does well, strangers from around the web. It&rsquo;s up to you to build that momentum for your project.</p>\n<p><strong>CHOOSING YOUR GOAL</strong></p>\n<p>Once you&rsquo;ve researched your budget and considered your reach, you&rsquo;re ready to set your funding goal. Because funding is all-or-nothing, you can always raise more than your goal but never less. Figure out how much money you need to complete the project as promised (while considering how much funding you think you can generate), and select an amount close to that.</p>\n<p><strong>SETTING YOUR PROJECT DEADLINE</strong></p>\n<p>Projects can last anywhere from one to 60 days, however a longer project duration is not necessarily better. Statistically, projects lasting 30 days or less have our highest success rates. A FundraisingScript project takes a lot of work to run, and shorter projects set a tone of confidence and help motivate your backers to join the party. Longer durations incite less urgency, encourage procrastination, and tend to fizzle out.</p>', '1', 3),
(4, 'Building Your Project', 'building-your-project', '<p>As you build your project, take your time! The average successfully funded creator spends nearly two weeks tweaking their project before launching. A thoughtful and methodical approach can pay off.</p>\n<p><strong>TITLING YOUR PROJECT</strong></p>\n<p>Your FundraisingScript project title should be simple, specific, and memorable, and it should include the title of the creative project youKSYSINGre raising funds for. Imagine your title as a distinct identity that will set it apart (KSYDOUMake my new album&rdquo; isn&rsquo;t as helpful or searchable as &ldquo;The K-Stars record their debut EP,&nbsp;<em>All Or Nothing</em>&nbsp;KSYDOU). Avoid words like KSYDOUhelp,KSYDOU KSYDOUsupport,KSYDOU or KSYDOUfund.KSYDOU They imply that youKSYSINGre asking someone to do you a favor rather than offering an experience they&rsquo;re going to love.</p>\n<p><strong>PICKING YOUR PROJECT IMAGE</strong></p>\n<p>Your project image is how you will be represented on FundraisingScript and the rest of the web. Pick something that accurately reflects your project and that looks nice, too!</p>\n<p>&nbsp;</p>\n<p><strong>WRITING YOUR SHORT DESCRIPTION</strong></p>\n<p>Your short description appears in your project&rsquo;s widget, and it&rsquo;s the best place to quickly communicate to your audience what your project is about. Stay focused and be clear on what your project hopes to accomplish. If you had to describe your project in one tweet, how would you do it?</p>\n<p><strong>WRITING YOUR BIO</strong></p>\n<p>Your bio is a great opportunity to share more about you. Why are you the one to take on this project? What prior work can you share via links? This is key to earning your backers&rsquo; trust.</p>', '1', 4),
(5, 'Promoting Your Project', 'promoting-your-project', '<p>An exceptional project can lead to outpourings of support from all corners of the web, but for most projects, support comes from within their own networks and their networks&rsquo; networks. If you want people to back your project you have to tell them about it. More than once! And in a variety of ways! Here&rsquo;s how:</p>\n<p><strong>SMART OUTREACH</strong></p>\n<p>A nice, personal message is the most effective way to let someone know about your project. Send an email to your close friends and family so they can be first to pledge, then use your personal blog, your Facebook page, and your Twitter account to tune in everyone who&rsquo;s paying attention. Don&rsquo;t overwhelm with e-blasts and group messages, but be sure to remind your networks about your projects a few times throughout the course of its duration. Take the time to contact people individually. It makes a big difference.</p>\n<p><strong>MEETING UP</strong></p>\n<p>Don&rsquo;t be afraid to take your FundraisingScript project out into the real world. Nothing connects people to an idea like seeing the twinkle in your eye when you talk about it. Host pledge parties, print posters or flyers to distribute around your community, and organize meetups to educate people about your endeavor. Be creative!</p>\n<p><strong>STOPPING THE PRESSES</strong></p>\n<p>Contact your local newspaper, TV, and radio stations and tell them about your project. Seek out like-minded blogs and online media outlets to request coverage. Writers are always looking for stories to write about, and the media has a big soft spot for DIY success stories.</p>\n<p><strong>KEEPING IT REAL</strong></p>\n<p>Whatever channel you use to tell your project&rsquo;s story, don&rsquo;t spam. This includes posting your link on other FundraisingScript project pages, @messaging people to beg for money on Twitter, link-bombing on Facebook, and generally nagging people you don&rsquo;t already know. Over-posting can alienate your friends and fans, and it makes every other FundraisingScript project look bad too. Don&rsquo;t do it!</p>', '1', 5),
(6, 'Project Updates', 'project-updates', '<p>Project updates serve as your project&rsquo;s blog. They&rsquo;re a great way to share your progress, post media, and thank your backers. Posting a project update automatically sends an email to all your backers with that update. You can choose to make each update public for everyone to see, or reserve it for just your backers to view.</p>\n<p><strong>BUILDING MOMENTUM</strong></p>\n<p>While your project is live and the clock ticking, keep your backers informed and inspired to help you spread the word. Instead of posting a link to your project and asking for pledges every day, treat your project like a story that is unfolding and update everyone on its progress. &ldquo;Pics from last night&rsquo;s show!&rdquo; or &ldquo;We found a printer for our book!&rdquo; with a link to your project is engaging and fun for everybody to follow along with.</p>\n<p><strong>SHARING THE PROCESS</strong></p>\n<p>Once your project is successfully funded, don&rsquo;t forget about all the people that helped make it possible. Let backers and spectators watch your project come to life by sharing the decisions you make with them, explaining how it feels as your goal becomes a reality, and even asking them for feedback. Keeping backers informed and engaged is an essential part of FundraisingScript.</p>\n<p><strong>CELEBRATING SUCCESS</strong></p>\n<p>Sharing reviews, press, and photos from your project out in the world &mdash; whether it&rsquo;s opening night of your play or your book on someone&rsquo;s bookshelf &mdash; is great for everyone involved. The story of your project doesn&rsquo;t end after it gets shipped out. You still have a captivated audience that&rsquo;s cheering for you. Communicating with them can be one of the most rewarding parts of the process.</p>', '1', 6),
(8, 'testinng purpose111', 'testinng-purpose111', 'define???????/??//  ', '1', 20);

-- --------------------------------------------------------

--
-- Table structure for table `shipping_detail`
--

CREATE TABLE IF NOT EXISTS `shipping_detail` (
  `shipping_detail_id` int(50) NOT NULL AUTO_INCREMENT,
  `shipping_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `shipping_country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `shipping_add` text COLLATE utf8_unicode_ci NOT NULL,
  `shipping_city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `shipping_state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `shipping_zipcode` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `transaction_id` int(50) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`shipping_detail_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `site_setting`
--

CREATE TABLE IF NOT EXISTS `site_setting` (
  `site_setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `site_name` varchar(255) DEFAULT NULL,
  `site_version` varchar(255) DEFAULT NULL,
  `site_language` varchar(255) DEFAULT NULL,
  `currency_symbol` varchar(255) DEFAULT NULL,
  `currency_code` varchar(255) DEFAULT NULL,
  `date_format` varchar(255) DEFAULT NULL,
  `time_format` varchar(255) DEFAULT NULL,
  `date_time_format` varchar(255) DEFAULT NULL,
  `date_format_tooltip` varchar(255) DEFAULT NULL,
  `time_format_tooltip` varchar(255) DEFAULT NULL,
  `date_time_format_tooltip` varchar(255) DEFAULT NULL,
  `date_time_highlight_tooltip` varchar(255) DEFAULT NULL,
  `site_tracker` varchar(255) DEFAULT NULL,
  `robots` varchar(255) DEFAULT NULL,
  `how_it_works_video` varchar(255) DEFAULT NULL,
  `normal_paypal` int(10) NOT NULL DEFAULT '1',
  `paypal_status` varchar(255) DEFAULT NULL,
  `paypal_email` varchar(255) DEFAULT NULL,
  `paypal_url` varchar(255) DEFAULT NULL,
  `paypal_API_UserName` varchar(255) DEFAULT NULL,
  `paypal_API_Password` varchar(255) DEFAULT NULL,
  `paypal_API_Signature` varchar(255) DEFAULT NULL,
  `donation_amount` varchar(50) DEFAULT NULL,
  `maximum_donation_amount` double(10,2) NOT NULL DEFAULT '2000.00',
  `auto_target_achive` int(10) NOT NULL DEFAULT '0',
  `ending_soon` int(20) NOT NULL DEFAULT '7',
  `popular` double(10,2) NOT NULL DEFAULT '60.00',
  `most_funded` int(20) NOT NULL,
  `successful` double(10,2) NOT NULL DEFAULT '90.00',
  `small_project` int(20) NOT NULL DEFAULT '1000',
  `site_logo` varchar(255) DEFAULT NULL,
  `site_logo_hover` varchar(255) DEFAULT NULL,
  `fund_ideas` varchar(255) DEFAULT NULL,
  `project_min_days` int(5) NOT NULL,
  `project_max_days` int(5) NOT NULL,
  `time_zone` varchar(255) DEFAULT NULL,
  `mobile_site` int(1) NOT NULL DEFAULT '1',
  `currency_symbol_side` varchar(50) DEFAULT NULL,
  `decimal_points` int(5) NOT NULL,
  `enable_funding_option` tinyint(1) NOT NULL,
  `flexible_fees` decimal(10,2) NOT NULL,
  `suc_flexible_fees` decimal(10,2) NOT NULL,
  `fixed_fees` decimal(10,2) NOT NULL,
  `instant_fees` double(10,2) NOT NULL DEFAULT '0.00',
  `minimum_goal` decimal(10,2) NOT NULL,
  `maximum_goal` double(10,2) NOT NULL DEFAULT '2000.00',
  `minimum_reward_amount` double(10,2) NOT NULL DEFAULT '10.00',
  `maximum_reward_amount` double(10,2) NOT NULL DEFAULT '200.00',
  `maximum_project_per_year` int(25) NOT NULL,
  `maximum_donate_per_project` int(25) NOT NULL,
  `enable_facebook_stream` tinyint(1) NOT NULL,
  `enable_twitter_stream` tinyint(1) NOT NULL,
  `captcha_public_key` varchar(300) NOT NULL,
  `captcha_private_key` varchar(300) NOT NULL,
  `num_days_bydate` int(5) NOT NULL,
  `address_limit` int(3) NOT NULL,
  `forget_time_limit` int(11) NOT NULL,
  `perk_enable` varchar(255) NOT NULL,
  `personal_information_enable` varchar(255) NOT NULL,
  `payment_gateway` int(1) NOT NULL DEFAULT '0',
  `demomode` tinyint(1) NOT NULL DEFAULT '0',
  `version_license` varchar(250) NOT NULL,
  `version_url` varchar(150) NOT NULL,
  `version_pcode` varchar(70) NOT NULL,
  `ending_days` varchar(255) NOT NULL,
  `signup_captcha` int(11) NOT NULL,
  `contact_us_captcha` int(11) NOT NULL,
  PRIMARY KEY (`site_setting_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `site_setting`
--

INSERT INTO `site_setting` (`site_setting_id`, `site_name`, `site_version`, `site_language`, `currency_symbol`, `currency_code`, `date_format`, `time_format`, `date_time_format`, `date_format_tooltip`, `time_format_tooltip`, `date_time_format_tooltip`, `date_time_highlight_tooltip`, `site_tracker`, `robots`, `how_it_works_video`, `normal_paypal`, `paypal_status`, `paypal_email`, `paypal_url`, `paypal_API_UserName`, `paypal_API_Password`, `paypal_API_Signature`, `donation_amount`, `maximum_donation_amount`, `auto_target_achive`, `ending_soon`, `popular`, `most_funded`, `successful`, `small_project`, `site_logo`, `site_logo_hover`, `fund_ideas`, `project_min_days`, `project_max_days`, `time_zone`, `mobile_site`, `currency_symbol_side`, `decimal_points`, `enable_funding_option`, `flexible_fees`, `suc_flexible_fees`, `fixed_fees`, `instant_fees`, `minimum_goal`, `maximum_goal`, `minimum_reward_amount`, `maximum_reward_amount`, `maximum_project_per_year`, `maximum_donate_per_project`, `enable_facebook_stream`, `enable_twitter_stream`, `captcha_public_key`, `captcha_private_key`, `num_days_bydate`, `address_limit`, `forget_time_limit`, `perk_enable`, `personal_information_enable`, `payment_gateway`, `demomode`, `version_license`, `version_url`, `version_pcode`, `ending_days`, `signup_captcha`, `contact_us_captcha`) VALUES
(2, 'Groupfund', '2.0.0.20', '1', '$', 'USD', 'm/d/Y', 'HH:MM:SS', '', '', '', '', '', 'UA-23165973-1', '', '', 0, 'sandbox', 'jigar_1313046627_biz@gmail.com', 'https://www.sandbox.paypal.com/cgi-bin/webscri', 'jigar_1313046627_biz_api1.gmail.com', '1313046663', 'AMuOxt1FpmAKBEJ6exYeVH0TefE8AHDjH6WasXwi3PskdUAUPWS2au44', '10', 2000.00, 1, 30, 100.00, 1, 100.00, 1000, '98086logo_10911.png', '45770logo_12099.png', '1', 1, 3650, 'America/Rainy_River', 0, 'left', 2, 2, '10.00', '5.00', '11.00', 0.00, '100.00', 5000.00, 10.00, 2000.00, 1000, 0, 1, 1, '6LeiAdcSAAAAAJSei9mOKAcXV0dXEDFQhp-2xb8L', '6LeiAdcSAAAAAJKCCtd6_iiXpeGYG_10TMqcVrpQ', 0, 3, 24, '1', '0', 1, 0, 'amhio%9ynr8glhv58!rb', 'http://airtaskerclone.com/versions/api/example/process/', 'FUND001', '10', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `spam_control`
--

CREATE TABLE IF NOT EXISTS `spam_control` (
  `spam_control_id` int(10) NOT NULL AUTO_INCREMENT,
  `spam_report_total` int(30) NOT NULL,
  `spam_report_expire` int(30) NOT NULL,
  `total_register` int(30) NOT NULL,
  `register_expire` int(30) NOT NULL,
  `total_comment` int(30) NOT NULL,
  `comment_expire` int(30) NOT NULL,
  `total_contact` int(30) NOT NULL,
  `contact_expire` int(30) NOT NULL,
  PRIMARY KEY (`spam_control_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `spam_control`
--

INSERT INTO `spam_control` (`spam_control_id`, `spam_report_total`, `spam_report_expire`, `total_register`, `register_expire`, `total_comment`, `comment_expire`, `total_contact`, `contact_expire`) VALUES
(1, 1, 1, 1, 2, 1, 1, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `spam_inquiry`
--

CREATE TABLE IF NOT EXISTS `spam_inquiry` (
  `inquire_id` int(100) NOT NULL AUTO_INCREMENT,
  `inquire_spam_ip` varchar(255) DEFAULT NULL,
  `inquire_date` date NOT NULL,
  PRIMARY KEY (`inquire_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `spam_ip`
--

CREATE TABLE IF NOT EXISTS `spam_ip` (
  `spam_id` int(100) NOT NULL AUTO_INCREMENT,
  `spam_ip` varchar(255) DEFAULT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `permenant_spam` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`spam_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1200 ;

--
-- Dumping data for table `spam_ip`
--

INSERT INTO `spam_ip` (`spam_id`, `spam_ip`, `start_date`, `end_date`, `permenant_spam`) VALUES
(1197, '113.193.174.138', '2014-09-30', '2014-10-02', 0),
(1198, '210.89.56.198', '2014-09-30', '2014-10-02', 0),
(1185, '1.22.81.133', '2014-09-30', '2014-10-02', 0),
(1186, '1.22.81.190', '2014-09-30', '2014-10-02', 0),
(1187, '1.22.81.220', '2014-09-30', '2014-10-02', 0),
(1188, '1.22.82.242', '2014-09-30', '2014-10-02', 0),
(1189, '1.22.83.115', '2014-09-30', '2014-10-02', 0),
(1190, '1.22.83.179', '2014-09-30', '2014-10-02', 0),
(1191, '1.22.83.4', '2014-09-30', '2014-10-02', 0),
(1192, '1.22.83.66', '2014-09-30', '2014-10-02', 0),
(1193, '1.22.83.90', '2014-09-30', '2014-10-02', 0),
(1194, '103.254.201.122', '2014-09-30', '2014-10-02', 0),
(1195, '103.254.202.133', '2014-09-30', '2014-10-02', 0),
(1196, '103.254.202.74', '2014-09-30', '2014-10-02', 0),
(1174, '1.22.83.179', '2014-09-30', '2014-10-02', 0),
(1175, '1.22.83.4', '2014-09-30', '2014-10-02', 0),
(1176, '1.22.83.66', '2014-09-30', '2014-10-02', 0),
(1177, '1.22.83.90', '2014-09-30', '2014-10-02', 0),
(1178, '103.254.201.122', '2014-09-30', '2014-10-02', 0),
(1179, '103.254.202.133', '2014-09-30', '2014-10-02', 0),
(1180, '103.254.202.74', '2014-09-30', '2014-10-02', 0),
(1181, '113.193.174.138', '2014-09-30', '2014-10-02', 0),
(1182, '210.89.56.198', '2014-09-30', '2014-10-02', 0),
(1183, '1.22.80.116', '2014-09-30', '2014-10-02', 0),
(1184, '1.22.80.159', '2014-09-30', '2014-10-02', 0),
(1167, '1.22.80.116', '2014-09-30', '2014-10-02', 0),
(1168, '1.22.80.159', '2014-09-30', '2014-10-02', 0),
(1169, '1.22.81.133', '2014-09-30', '2014-10-02', 0),
(1170, '1.22.81.190', '2014-09-30', '2014-10-02', 0),
(1171, '1.22.81.220', '2014-09-30', '2014-10-02', 0),
(1172, '1.22.82.242', '2014-09-30', '2014-10-02', 0),
(1173, '1.22.83.115', '2014-09-30', '2014-10-02', 0);

-- --------------------------------------------------------

--
-- Table structure for table `spam_report_ip`
--

CREATE TABLE IF NOT EXISTS `spam_report_ip` (
  `spam_report_id` int(100) NOT NULL AUTO_INCREMENT,
  `spam_ip` varchar(255) DEFAULT NULL,
  `spam_user_id` int(100) NOT NULL,
  `reported_user_id` int(100) NOT NULL,
  PRIMARY KEY (`spam_report_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=47 ;

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE IF NOT EXISTS `state` (
  `state_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `state_name` varchar(255) DEFAULT NULL,
  `active` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`state_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=156 ;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`state_id`, `country_id`, `state_name`, `active`) VALUES
(11, 2, 'Gujarat', '1'),
(14, 117, 'Artigas', '1'),
(15, 117, 'Canelones', '1'),
(16, 117, 'Cerro Largo', '1'),
(17, 117, 'Colonia', '1'),
(18, 117, 'Durazno', '1'),
(19, 117, 'Flores', '1'),
(21, 117, 'Florida', '1'),
(22, 117, 'Lavalleja', '1'),
(23, 117, 'Maldonado', '1'),
(24, 117, 'Montevideo', '1'),
(25, 117, 'Paysandu', '1'),
(26, 117, 'Rivera', '1'),
(27, 117, 'Rocha', '1'),
(28, 117, 'Salto', '1'),
(29, 18, 'Canberra', '1'),
(30, 19, 'Vienna', '1'),
(31, 21, 'Nassau', '1'),
(32, 34, 'Brasilia', '1'),
(33, 42, 'Ottawa', '1'),
(34, 44, 'Bangui', '1'),
(36, 47, 'Beijing', '1'),
(37, 47, 'Shaanxi', '1'),
(38, 47, 'Shanghai', '1'),
(39, 51, 'San Jose', '1'),
(40, 54, 'Nicosia', '1'),
(41, 56, 'denmark', '1'),
(46, 7, 'Suva', '1'),
(47, 68, 'Paris', '1'),
(48, 68, 'Gers', '1'),
(49, 68, 'Jura', '1'),
(50, 68, 'Savoie', '1'),
(51, 71, 'Atlanta', '1'),
(52, 72, 'Berlin', '1'),
(53, 73, 'Accra', '1'),
(54, 79, 'Port-au-Prince', '1'),
(55, 83, 'Victoria', '1'),
(56, 83, 'Fragrance Harbour ', '1'),
(57, 2, 'Delhi', '1'),
(58, 90, 'Rome', '1'),
(59, 3, 'Tokyo', '1'),
(60, 3, 'Yokohama', '1'),
(61, 3, 'Osaka', '1'),
(62, 3, 'Nagoya', '1'),
(63, 3, 'Sapporo', '1'),
(64, 3, 'Kobe', '1'),
(65, 3, 'Kyoto', '1'),
(66, 3, 'Fukuoka', '1'),
(67, 3, 'Kawasaki', '1'),
(68, 3, 'Saitama', '1'),
(69, 3, 'Hiroshima', '1'),
(70, 3, 'Sendai', '1'),
(71, 3, 'Kitakyushu ', '1'),
(72, 94, 'Nairobi', '1'),
(73, 102, 'Beirut', '1'),
(74, 105, 'Tripoli ', '1'),
(75, 113, 'Kuala Lumpur', '1'),
(76, 121, 'mexico city', '1'),
(77, 124, 'Monte Carlo', '1'),
(78, 127, 'Rabat', '1'),
(79, 132, 'Amsterdam', '1'),
(80, 134, 'Wellington', '1'),
(81, 139, 'Oslo', '1'),
(82, 137, 'Abuja', '1'),
(83, 144, 'Panama City', '1'),
(84, 147, 'Lima', '1'),
(85, 150, 'Lisbon', '1'),
(86, 149, 'Warsaw', '1'),
(87, 6, 'Moscow', '1'),
(88, 207, 'singapore', '1'),
(89, 1, 'Alabama', '1'),
(90, 1, 'Alaska', '1'),
(91, 1, 'AriZona', '1'),
(92, 1, 'California', '1'),
(93, 1, 'Arlamsa', '1'),
(94, 1, 'colorado', '1'),
(95, 1, 'DC', '1'),
(96, 1, 'Delaware', '1'),
(97, 1, 'Florida', '1'),
(98, 1, 'Georgia', '1'),
(99, 1, 'Hawaii', '1'),
(100, 1, 'Idaho', '1'),
(101, 1, 'Illinois', '1'),
(102, 1, 'Indiana', '1'),
(103, 1, 'Iowa', '1'),
(104, 1, 'Kansas', '1'),
(105, 1, 'Kentucky', '1'),
(106, 1, 'Louisiana', '1'),
(107, 1, 'Maine', '1'),
(108, 1, 'Maryland', '1'),
(109, 1, 'Massachusetts', '1'),
(110, 1, 'Michigan', '1'),
(111, 1, 'Minnesota', '1'),
(112, 1, 'Mississippi', '1'),
(113, 1, 'Missouri', '1'),
(114, 1, 'montana', '1'),
(115, 1, 'Nebraska', '1'),
(116, 1, 'Nevada', '1'),
(117, 1, 'New Hampshire', '1'),
(118, 1, 'New Mexico', '1'),
(119, 1, 'New York', '1'),
(120, 1, 'New Jersey', '1'),
(121, 1, 'North Carolina', '1'),
(122, 1, 'North Dakota', '1'),
(123, 1, 'Ohio', '1'),
(124, 1, 'Oklahoma', '1'),
(125, 1, 'Oregon', '1'),
(126, 1, 'Pennsylvania', '1'),
(127, 1, 'Rhode Island', '1'),
(128, 1, 'South Carolina', '1'),
(129, 1, 'South Dakota', '1'),
(130, 1, 'Tennessee', '1'),
(131, 1, 'Texas', '1'),
(132, 1, 'Utah', '1'),
(133, 1, 'Vermont', '1'),
(134, 1, 'Virginia', '1'),
(135, 1, 'Washington', '1'),
(136, 1, 'West Virginia', '1'),
(137, 1, 'Wisconsin', '1'),
(138, 1, 'Wyoming', '1'),
(139, 173, 'Pretoria', '1'),
(140, 176, 'La Rioja', '1'),
(141, 176, 'Madrid', '1'),
(142, 176, 'Murcia', '1'),
(143, 176, 'Balearic Islands', '1'),
(144, 176, 'Aragon', '1'),
(145, 176, 'Catalonia', '1'),
(146, 176, 'Castilla-La Mancha', '1'),
(147, 176, 'Galicia', '1'),
(148, 181, 'Stockholm', '1'),
(149, 180, 'Bern', '1'),
(150, 194, 'Ankara Province', '1'),
(151, 5, 'London', '1'),
(154, 208, 'vado dara', '0'),
(155, 2, 'Bihar', '1');

-- --------------------------------------------------------

--
-- Table structure for table `stripe_setting`
--

CREATE TABLE IF NOT EXISTS `stripe_setting` (
  `id` int(11) NOT NULL,
  `stripe_key_test_public` varchar(255) NOT NULL,
  `stripe_key_test_secret` varchar(255) NOT NULL,
  `stripe_key_live_public` varchar(255) NOT NULL,
  `stripe_key_live_secret` varchar(255) NOT NULL,
  `stripe_enable` tinyint(4) NOT NULL,
  `stripe_test_mode` tinyint(1) NOT NULL,
  `fees_taken_from` varchar(255) NOT NULL,
  `preapproval` tinyint(11) NOT NULL,
  `gateway_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stripe_setting`
--

INSERT INTO `stripe_setting` (`id`, `stripe_key_test_public`, `stripe_key_test_secret`, `stripe_key_live_public`, `stripe_key_live_secret`, `stripe_enable`, `stripe_test_mode`, `fees_taken_from`, `preapproval`, `gateway_status`) VALUES
(1, 'pk_test_4H67yWebnwtGD9AYIAXs1Atm', 'sk_test_W8iEZILRhXdsS7uRylrmTqHI', 'sk_live_4XviMnnSvKfdduxBFpflUTV0', 'pk_live_Jwx7wi756bK4ydKfjthULWWf', 0, 0, '0', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tag_interest`
--

CREATE TABLE IF NOT EXISTS `tag_interest` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `status` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tag_interest`
--

INSERT INTO `tag_interest` (`id`, `name`, `status`) VALUES
(1, 'footblall', 1),
(2, 'new asdfasdf', 1),
(3, 'wallyball', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tag_skill`
--

CREATE TABLE IF NOT EXISTS `tag_skill` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tag_skill`
--

INSERT INTO `tag_skill` (`id`, `name`, `status`) VALUES
(1, 'programming 1', 1),
(2, 'asdad newfsdf', 1),
(3, 'php', 0);

-- --------------------------------------------------------

--
-- Table structure for table `temp`
--

CREATE TABLE IF NOT EXISTS `temp` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `detail` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `temp_adaptive`
--

CREATE TABLE IF NOT EXISTS `temp_adaptive` (
  `temp_id` int(100) NOT NULL AUTO_INCREMENT,
  `user_id` int(100) NOT NULL,
  `project_id` int(100) NOT NULL,
  `perk_id` int(100) NOT NULL DEFAULT '0',
  `total_amount` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `pay_fee` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `host_ip` varchar(255) DEFAULT NULL,
  `transaction_date_time` datetime NOT NULL,
  `paypal_paykey` varchar(255) DEFAULT NULL,
  `paypal_adaptive_status` varchar(50) DEFAULT NULL,
  `temp_anonymous` tinyint(1) NOT NULL,
  `facebook` int(1) NOT NULL,
  `twitter` int(1) NOT NULL,
  `payment_done` int(1) NOT NULL,
  `keep_update` tinyint(1) NOT NULL,
  `recipient` varchar(255) NOT NULL,
  `gift` tinyint(1) NOT NULL,
  PRIMARY KEY (`temp_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=54 ;

-- --------------------------------------------------------

--
-- Table structure for table `temp_preapprove`
--

CREATE TABLE IF NOT EXISTS `temp_preapprove` (
  `temp_pre_id` int(100) NOT NULL AUTO_INCREMENT,
  `preapprovalKey` varchar(255) DEFAULT NULL,
  `user_id` int(100) NOT NULL,
  `project_id` int(50) NOT NULL,
  `perk_id` int(50) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `comment` text,
  `host_ip` varchar(100) DEFAULT NULL,
  `transaction_date_time` datetime NOT NULL,
  `temp_anonymous` tinyint(1) NOT NULL,
  `facebook` int(1) NOT NULL,
  `twitter` int(1) NOT NULL,
  `payment_done` int(1) NOT NULL,
  `keep_update` tinyint(1) NOT NULL,
  `recipient` varchar(255) NOT NULL,
  `gift` tinyint(1) NOT NULL,
  `donor_email` varchar(40) DEFAULT NULL,
  `total_amount` decimal(10,2) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `shipping` text,
  PRIMARY KEY (`temp_pre_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `temp_preapprove`
--

INSERT INTO `temp_preapprove` (`temp_pre_id`, `preapprovalKey`, `user_id`, `project_id`, `perk_id`, `name`, `comment`, `host_ip`, `transaction_date_time`, `temp_anonymous`, `facebook`, `twitter`, `payment_done`, `keep_update`, `recipient`, `gift`, `donor_email`, `total_amount`, `amount`, `shipping`) VALUES
(1, 'PA-4AU42472SJ042810Y', 5, 5, 0, 'jaimin', 'Donate Technology for Education .........', '1.22.82.216', '2014-10-29 22:54:40', 1, 0, 0, 0, 0, '', 0, 'jaimin.rockersinfo@gmail.com', '100.00', '100.00', ''),
(2, 'PA-4H5107423D623741E', 5, 5, 0, 'jaimin', 'Donate Technology for Education ........', '1.22.82.216', '2014-10-29 22:58:07', 1, 0, 0, 0, 0, '', 0, 'jaimin.rockersinfo@gmail.com', '100.00', '100.00', ''),
(3, 'PA-36C35634KH771500C', 5, 5, 0, 'jaimin', '', '1.22.82.216', '2014-10-29 23:54:54', 1, 0, 0, 1, 0, '', 0, 'jaimin.rockersinfo@gmail.com', '100.00', '100.00', ''),
(4, 'PA-7T982700FU313480A', 4, 5, 0, 'Priya', 'Chapel Elementary receives ..', '1.22.82.216', '2014-10-30 00:23:03', 1, 0, 0, 1, 0, '', 0, 'priya.rockersinfo@gmail.com', '300.00', '300.00', ''),
(5, 'PA-2GA061505U689250E', 3, 5, 0, 'Jayshree', 'fund raising  support and personalizing your contribution..', '1.22.82.216', '2014-10-30 00:32:44', 1, 0, 0, 1, 0, '', 0, 'jayshree.rockersinfo@gmail.com', '300.00', '300.00', ''),
(6, 'PA-7CV610943J7803626', 3, 6, 0, 'Jayshree', '', '1.22.82.216', '2014-10-30 01:11:57', 1, 0, 0, 1, 0, '', 0, 'jayshree.rockersinfo@gmail.com', '200.00', '200.00', ''),
(7, 'PA-2FR456594M738980A', 5, 6, 0, 'jaimin', '', '1.22.82.216', '2014-10-30 01:19:11', 1, 0, 0, 1, 0, '', 0, 'jaimin.rockersinfo@gmail.com', '100.00', '100.00', ''),
(8, 'PA-3XA61060RH852425D', 4, 6, 0, 'Priya', 'Community-Based Education Programs donation...', '1.22.82.216', '2014-10-30 01:26:32', 1, 0, 0, 1, 0, '', 0, 'priya.rockersinfo@gmail.com', '100.00', '100.00', ''),
(9, 'PA-7VB17899B8811374C', 4, 4, 0, 'Priya', 'fundrasing your support and personalizing your contribution..', '1.22.82.216', '2014-10-30 02:58:00', 1, 0, 0, 1, 0, '', 0, 'priya.rockersinfo@gmail.com', '200.00', '200.00', ''),
(10, 'PA-91C99300PS596063J', 5, 4, 0, 'jaimin', 'donation great for voicing your support and personalizing your contribution..', '1.22.82.216', '2014-10-30 03:01:28', 1, 0, 0, 1, 0, '', 0, 'jaimin.rockersinfo@gmail.com', '300.00', '300.00', ''),
(11, 'PA-5W847791LT332392V', 3, 4, 0, 'Jayshree', '', '1.22.82.216', '2014-10-30 03:06:05', 1, 0, 0, 1, 0, '', 0, 'jayshree.rockersinfo@gmail.com', '300.00', '300.00', '');

-- --------------------------------------------------------

--
-- Table structure for table `themes`
--

CREATE TABLE IF NOT EXISTS `themes` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `color_orange` varchar(10) NOT NULL,
  `color_darkorange` varchar(10) NOT NULL,
  `color_grey` varchar(10) NOT NULL,
  `color_black` varchar(10) NOT NULL,
  `color_white` varchar(10) NOT NULL,
  `color_lightgrey` varchar(10) NOT NULL,
  `color_blue` varchar(10) NOT NULL,
  `color_darkblue` varchar(10) NOT NULL,
  `color_ccc` varchar(10) NOT NULL,
  `style_font1` varchar(255) NOT NULL,
  `style_font2` varchar(255) NOT NULL,
  `default_value` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `themes`
--

INSERT INTO `themes` (`id`, `color_orange`, `color_darkorange`, `color_grey`, `color_black`, `color_white`, `color_lightgrey`, `color_blue`, `color_darkblue`, `color_ccc`, `style_font1`, `style_font2`, `default_value`) VALUES
(1, '#8d5630', '#e56712', '#4c4c4c', '#323232', '#ffffff', '#efefef', '#3bb0d2', '#32869c', '#cccccc', 'Arial, Helvetica, sans-serif', 'Arial, Helvetica, sans-serif', 0),
(2, '#f37621', '#e56712', '#4c4c4c', '#323232', '#ffffff', '#efefef', '#3bb0d2', '#32869c', '#cccccc', 'Arial, Helvetica, sans-serif', 'Arial, Helvetica, sans-serif', 1);

-- --------------------------------------------------------

--
-- Table structure for table `timezone`
--

CREATE TABLE IF NOT EXISTS `timezone` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=579 ;

--
-- Dumping data for table `timezone`
--

INSERT INTO `timezone` (`id`, `name`) VALUES
(1, 'Africa/Abidjan'),
(2, 'Africa/Asmera'),
(3, 'Africa/Blantyre'),
(4, 'Africa/Ceuta'),
(5, 'Africa/Douala'),
(6, 'Africa/Johannesburg'),
(7, 'Africa/Kinshasa'),
(8, 'Africa/Lubumbashi'),
(9, 'Africa/Mbabane'),
(10, 'Africa/Niamey'),
(11, 'Africa/Timbuktu'),
(12, 'Africa/Accra'),
(13, 'Africa/Bamako'),
(14, 'Africa/Brazzaville'),
(15, 'Africa/Conakry'),
(16, 'Africa/El_Aaiun'),
(17, 'Africa/Juba'),
(18, 'Africa/Lagos'),
(19, 'Africa/Lusaka'),
(20, 'Africa/Mogadishu'),
(21, 'Africa/Nouakchott'),
(22, 'Africa/Tripoli'),
(23, 'Africa/Addis_Ababa'),
(24, 'Africa/Bangui'),
(25, 'Africa/Bujumbura'),
(26, 'Africa/Dakar'),
(27, 'Africa/Freetown'),
(28, 'Africa/Kampala'),
(29, 'Africa/Libreville'),
(30, 'Africa/Malabo'),
(31, 'Africa/Monrovia'),
(32, 'Africa/Ouagadougou'),
(33, 'Africa/Tunis'),
(34, 'Africa/Algiers'),
(35, 'Africa/Banjul'),
(36, 'Africa/Cairo'),
(37, 'Africa/Dar_es_Salaam'),
(38, 'Africa/Gaborone'),
(39, 'Africa/Khartoum'),
(40, 'Africa/Lome'),
(41, 'Africa/Maputo'),
(42, 'Africa/Nairobi'),
(43, 'Africa/Porto-Novo'),
(44, 'Africa/Windhoek'),
(45, 'Africa/Asmara'),
(46, 'Africa/Bissau'),
(47, 'Africa/Casablanca'),
(48, 'Africa/Djibouti'),
(49, 'Africa/Harare'),
(50, 'Africa/Kigali'),
(51, 'Africa/Luanda'),
(52, 'Africa/Maseru'),
(53, 'Africa/Ndjamena'),
(54, 'Africa/Sao_Tome'),
(55, 'America/Adak'),
(56, 'America/Argentina/Buenos_Aires'),
(57, 'America/Argentina/La_Rioja'),
(58, 'America/Argentina/San_Luis'),
(59, 'America/Atikokan'),
(60, 'America/Belem'),
(61, 'America/Boise'),
(62, 'America/Caracas'),
(63, 'America/Chihuahua'),
(64, 'America/Cuiaba'),
(65, 'America/Denver'),
(66, 'America/El_Salvador'),
(67, 'America/Godthab'),
(68, 'America/Guatemala'),
(69, 'America/Hermosillo'),
(70, 'America/Indiana/Tell_City'),
(71, 'America/Inuvik'),
(72, 'America/Kentucky/Louisville'),
(73, 'America/Lima'),
(74, 'America/Managua'),
(75, 'America/Mazatlan'),
(76, 'America/Mexico_City'),
(77, 'America/Montreal'),
(78, 'America/Nome'),
(79, 'America/Ojinaga'),
(80, 'America/Port-au-Prince'),
(81, 'America/Rainy_River'),
(82, 'America/Rio_Branco'),
(83, 'America/Santo_Domingo'),
(84, 'America/St_Barthelemy'),
(85, 'America/St_Vincent'),
(86, 'America/Tijuana'),
(87, 'America/Whitehorse'),
(88, 'America/Anchorage'),
(89, 'America/Argentina/Catamarca'),
(90, 'America/Argentina/Mendoza'),
(91, 'America/Argentina/Tucuman'),
(92, 'America/Atka'),
(93, 'America/Belize'),
(94, 'America/Buenos_Aires'),
(95, 'America/Catamarca'),
(96, 'America/Coral_Harbour'),
(97, 'America/Curacao'),
(98, 'America/Detroit'),
(99, 'America/Ensenada'),
(100, 'America/Goose_Bay'),
(101, 'America/Guayaquil'),
(102, 'America/Indiana/Indianapolis'),
(103, 'America/Indiana/Vevay'),
(104, 'America/Iqaluit'),
(105, 'America/Kentucky/Monticello'),
(106, 'America/Los_Angeles'),
(107, 'America/Manaus'),
(108, 'America/Mendoza'),
(109, 'America/Miquelon'),
(110, 'America/Montserrat'),
(111, 'America/Noronha'),
(112, 'America/Panama'),
(113, 'America/Port_of_Spain'),
(114, 'America/Rankin_Inlet'),
(115, 'America/Rosario'),
(116, 'America/Sao_Paulo'),
(117, 'America/St_Johns'),
(118, 'America/Swift_Current'),
(119, 'America/Toronto'),
(120, 'America/Winnipeg'),
(121, 'America/Anguilla'),
(122, 'America/Argentina/ComodRivadavia'),
(123, 'America/Argentina/Rio_Gallegos'),
(124, 'America/Argentina/Ushuaia'),
(125, 'America/Bahia'),
(126, 'America/Blanc-Sablon'),
(127, 'America/Cambridge_Bay'),
(128, 'America/Cayenne'),
(129, 'America/Cordoba'),
(130, 'America/Danmarkshavn'),
(131, 'America/Dominica'),
(132, 'America/Fort_Wayne'),
(133, 'America/Grand_Turk'),
(134, 'America/Guyana'),
(135, 'America/Indiana/Knox'),
(136, 'America/Indiana/Vincennes'),
(137, 'America/Jamaica'),
(138, 'America/Knox_IN'),
(139, 'America/Louisville'),
(140, 'America/Marigot'),
(141, 'America/Menominee'),
(142, 'America/Moncton'),
(143, 'America/Nassau'),
(144, 'America/North_Dakota/Beulah'),
(145, 'America/Pangnirtung'),
(146, 'America/Porto_Acre'),
(147, 'America/Recife'),
(148, 'America/Santa_Isabel'),
(149, 'America/Scoresbysund'),
(150, 'America/St_Kitts'),
(151, 'America/Tegucigalpa'),
(152, 'America/Tortola'),
(153, 'America/Yakutat'),
(154, 'America/Antigua'),
(155, 'America/Argentina/Cordoba'),
(156, 'America/Argentina/Salta'),
(157, 'America/Aruba'),
(158, 'America/Bahia_Banderas'),
(159, 'America/Boa_Vista'),
(160, 'America/Campo_Grande'),
(161, 'America/Cayman'),
(162, 'America/Costa_Rica'),
(163, 'America/Dawson'),
(164, 'America/Edmonton'),
(165, 'America/Fortaleza'),
(166, 'America/Grenada'),
(167, 'America/Halifax'),
(168, 'America/Indiana/Marengo'),
(169, 'America/Indiana/Winamac'),
(170, 'America/Jujuy'),
(171, 'America/Kralendijk'),
(172, 'America/Lower_Princes'),
(173, 'America/Martinique'),
(174, 'America/Merida'),
(175, 'America/Monterrey'),
(176, 'America/New_York'),
(177, 'America/North_Dakota/Center'),
(178, 'America/Paramaribo'),
(179, 'America/Porto_Velho'),
(180, 'America/Regina'),
(181, 'America/Santarem'),
(182, 'America/Shiprock'),
(183, 'America/St_Lucia'),
(184, 'America/Thule'),
(185, 'America/Vancouver'),
(186, 'America/Yellowknife'),
(187, 'America/Araguaina'),
(188, 'America/Argentina/Jujuy'),
(189, 'America/Argentina/San_Juan'),
(190, 'America/Asuncion'),
(191, 'America/Barbados'),
(192, 'America/Bogota'),
(193, 'America/Cancun'),
(194, 'America/Chicago'),
(195, 'America/Creston'),
(196, 'America/Dawson_Creek'),
(197, 'America/Eirunepe'),
(198, 'America/Glace_Bay'),
(199, 'America/Guadeloupe'),
(200, 'America/Havana'),
(201, 'America/Indiana/Petersburg'),
(202, 'America/Indianapolis'),
(203, 'America/Juneau'),
(204, 'America/La_Paz'),
(205, 'America/Maceio'),
(206, 'America/Matamoros'),
(207, 'America/Metlakatla'),
(208, 'America/Montevideo'),
(209, 'America/Nipigon'),
(210, 'America/North_Dakota/New_Salem'),
(211, 'America/Phoenix'),
(212, 'America/Puerto_Rico'),
(213, 'America/Resolute'),
(214, 'America/Santiago'),
(215, 'America/Sitka'),
(216, 'America/St_Thomas'),
(217, 'America/Thunder_Bay'),
(218, 'America/Virgin'),
(219, 'Antarctica/Casey'),
(220, 'Antarctica/McMurdo'),
(221, 'Antarctica/Vostok'),
(222, 'Antarctica/Davis'),
(223, 'Antarctica/Palmer'),
(224, 'Antarctica/DumontDUrville'),
(225, 'Antarctica/Rothera'),
(226, 'Antarctica/Macquarie'),
(227, 'Antarctica/South_Pole'),
(228, 'Antarctica/Mawson'),
(229, 'Antarctica/Syowa'),
(230, 'Arctic/Longyearbyen'),
(231, 'Asia/Aden'),
(232, 'Asia/Aqtobe'),
(233, 'Asia/Baku'),
(234, 'Asia/Calcutta'),
(235, 'Asia/Dacca'),
(236, 'Asia/Dushanbe'),
(237, 'Asia/Hong_Kong'),
(238, 'Asia/Jayapura'),
(239, 'Asia/Kashgar'),
(240, 'Asia/Krasnoyarsk'),
(241, 'Asia/Macau'),
(242, 'Asia/Nicosia'),
(243, 'Asia/Phnom_Penh'),
(244, 'Asia/Rangoon'),
(245, 'Asia/Seoul'),
(246, 'Asia/Tbilisi'),
(247, 'Asia/Tokyo'),
(248, 'Asia/Ust-Nera'),
(249, 'Asia/Yerevan'),
(250, 'Asia/Almaty'),
(251, 'Asia/Ashgabat'),
(252, 'Asia/Bangkok'),
(253, 'Asia/Choibalsan'),
(254, 'Asia/Damascus'),
(255, 'Asia/Gaza'),
(256, 'Asia/Hovd'),
(257, 'Asia/Jerusalem'),
(258, 'Asia/Kathmandu'),
(259, 'Asia/Kuala_Lumpur'),
(260, 'Asia/Magadan'),
(261, 'Asia/Novokuznetsk'),
(262, 'Asia/Pontianak'),
(263, 'Asia/Riyadh'),
(264, 'Asia/Shanghai'),
(265, 'Asia/Tehran'),
(266, 'Asia/Ujung_Pandang'),
(267, 'Asia/Vientiane'),
(268, 'Asia/Amman'),
(269, 'Asia/Ashkhabad'),
(270, 'Asia/Beirut'),
(271, 'Asia/Chongqing'),
(272, 'Asia/Dhaka'),
(273, 'Asia/Harbin'),
(274, 'Asia/Irkutsk'),
(275, 'Asia/Kabul'),
(276, 'Asia/Katmandu'),
(277, 'Asia/Kuching'),
(278, 'Asia/Makassar'),
(279, 'Asia/Novosibirsk'),
(280, 'Asia/Pyongyang'),
(281, 'Asia/Saigon'),
(282, 'Asia/Singapore'),
(283, 'Asia/Tel_Aviv'),
(284, 'Asia/Ulaanbaatar'),
(285, 'Asia/Vladivostok'),
(286, 'Asia/Anadyr'),
(287, 'Asia/Baghdad'),
(288, 'Asia/Bishkek'),
(289, 'Asia/Chungking'),
(290, 'Asia/Dili'),
(291, 'Asia/Hebron'),
(292, 'Asia/Istanbul'),
(293, 'Asia/Kamchatka'),
(294, 'Asia/Khandyga'),
(295, 'Asia/Kuwait'),
(296, 'Asia/Manila'),
(297, 'Asia/Omsk'),
(298, 'Asia/Qatar'),
(299, 'Asia/Sakhalin'),
(300, 'Asia/Taipei'),
(301, 'Asia/Thimbu'),
(302, 'Asia/Ulan_Bator'),
(303, 'Asia/Yakutsk'),
(304, 'Asia/Aqtau'),
(305, 'Asia/Bahrain'),
(306, 'Asia/Brunei'),
(307, 'Asia/Colombo'),
(308, 'Asia/Dubai'),
(309, 'Asia/Ho_Chi_Minh'),
(310, 'Asia/Jakarta'),
(311, 'Asia/Karachi'),
(312, 'Asia/Kolkata'),
(313, 'Asia/Macao'),
(314, 'Asia/Muscat'),
(315, 'Asia/Oral'),
(316, 'Asia/Qyzylorda'),
(317, 'Asia/Samarkand'),
(318, 'Asia/Tashkent'),
(319, 'Asia/Thimphu'),
(320, 'Asia/Urumqi'),
(321, 'Asia/Yekaterinburg'),
(322, 'Atlantic/Azores'),
(323, 'Atlantic/Faroe'),
(324, 'Atlantic/St_Helena'),
(325, 'Atlantic/Bermuda'),
(326, 'Atlantic/Jan_Mayen'),
(327, 'Atlantic/Stanley'),
(328, 'Atlantic/Canary'),
(329, 'Atlantic/Madeira'),
(330, 'Atlantic/Cape_Verde'),
(331, 'Atlantic/Reykjavik'),
(332, 'Atlantic/Faeroe'),
(333, 'Atlantic/South_Georgia'),
(334, 'Australia/ACT'),
(335, 'Australia/Currie'),
(336, 'Australia/Lindeman'),
(337, 'Australia/Perth'),
(338, 'Australia/Victoria'),
(339, 'Australia/Adelaide'),
(340, 'Australia/Darwin'),
(341, 'Australia/Lord_Howe'),
(342, 'Australia/Queensland'),
(343, 'Australia/West'),
(344, 'Australia/Brisbane'),
(345, 'Australia/Eucla'),
(346, 'Australia/Melbourne'),
(347, 'Australia/South'),
(348, 'Australia/Yancowinna'),
(349, 'Australia/Broken_Hill'),
(350, 'Australia/Hobart'),
(351, 'Australia/North'),
(352, 'Australia/Sydney'),
(353, 'Australia/Canberra'),
(354, 'Australia/LHI'),
(355, 'Australia/NSW'),
(356, 'Australia/Tasmania'),
(357, 'Europe/Amsterdam'),
(358, 'Europe/Berlin'),
(359, 'Europe/Busingen'),
(360, 'Europe/Guernsey'),
(361, 'Europe/Kaliningrad'),
(362, 'Europe/Luxembourg'),
(363, 'Europe/Monaco'),
(364, 'Europe/Podgorica'),
(365, 'Europe/San_Marino'),
(366, 'Europe/Stockholm'),
(367, 'Europe/Vaduz'),
(368, 'Europe/Warsaw'),
(369, 'Europe/Andorra'),
(370, 'Europe/Bratislava'),
(371, 'Europe/Chisinau'),
(372, 'Europe/Helsinki'),
(373, 'Europe/Kiev'),
(374, 'Europe/Madrid'),
(375, 'Europe/Moscow'),
(376, 'Europe/Prague'),
(377, 'Europe/Sarajevo'),
(378, 'Europe/Tallinn'),
(379, 'Europe/Vatican'),
(380, 'Europe/Zagreb'),
(381, 'Europe/Athens'),
(382, 'Europe/Brussels'),
(383, 'Europe/Copenhagen'),
(384, 'Europe/Isle_of_Man'),
(385, 'Europe/Lisbon'),
(386, 'Europe/Malta'),
(387, 'Europe/Nicosia'),
(388, 'Europe/Riga'),
(389, 'Europe/Simferopol'),
(390, 'Europe/Tirane'),
(391, 'Europe/Vienna'),
(392, 'Europe/Zaporozhye'),
(393, 'Europe/Belfast'),
(394, 'Europe/Bucharest'),
(395, 'Europe/Dublin'),
(396, 'Europe/Istanbul'),
(397, 'Europe/Ljubljana'),
(398, 'Europe/Mariehamn'),
(399, 'Europe/Oslo'),
(400, 'Europe/Rome'),
(401, 'Europe/Skopje'),
(402, 'Europe/Tiraspol'),
(403, 'Europe/Vilnius'),
(404, 'Europe/Zurich'),
(405, 'Europe/Belgrade'),
(406, 'Europe/Budapest'),
(407, 'Europe/Gibraltar'),
(408, 'Europe/Jersey'),
(409, 'Europe/London'),
(410, 'Europe/Minsk'),
(411, 'Europe/Paris'),
(412, 'Europe/Samara'),
(413, 'Europe/Sofia'),
(414, 'Europe/Uzhgorod'),
(415, 'Europe/Volgograd'),
(416, 'Indian/Antananarivo'),
(417, 'Indian/Kerguelen'),
(418, 'Indian/Reunion'),
(419, 'Indian/Chagos'),
(420, 'Indian/Mahe'),
(421, 'Indian/Christmas'),
(422, 'Indian/Maldives'),
(423, 'Indian/Cocos'),
(424, 'Indian/Mauritius'),
(425, 'Indian/Comoro'),
(426, 'Indian/Mayotte'),
(427, 'Pacific/Apia'),
(428, 'Pacific/Efate'),
(429, 'Pacific/Galapagos'),
(430, 'Pacific/Johnston'),
(431, 'Pacific/Marquesas'),
(432, 'Pacific/Noumea'),
(433, 'Pacific/Ponape'),
(434, 'Pacific/Tahiti'),
(435, 'Pacific/Wallis'),
(436, 'Pacific/Auckland'),
(437, 'Pacific/Enderbury'),
(438, 'Pacific/Gambier'),
(439, 'Pacific/Kiritimati'),
(440, 'Pacific/Midway'),
(441, 'Pacific/Pago_Pago'),
(442, 'Pacific/Port_Moresby'),
(443, 'Pacific/Tarawa'),
(444, 'Pacific/Yap'),
(445, 'Pacific/Chatham'),
(446, 'Pacific/Fakaofo'),
(447, 'Pacific/Guadalcanal'),
(448, 'Pacific/Kosrae'),
(449, 'Pacific/Nauru'),
(450, 'Pacific/Palau'),
(451, 'Pacific/Rarotonga'),
(452, 'Pacific/Tongatapu'),
(453, 'Pacific/Chuuk'),
(454, 'Pacific/Fiji'),
(455, 'Pacific/Guam'),
(456, 'Pacific/Kwajalein'),
(457, 'Pacific/Niue'),
(458, 'Pacific/Pitcairn'),
(459, 'Pacific/Saipan'),
(460, 'Pacific/Truk'),
(461, 'Pacific/Easter'),
(462, 'Pacific/Funafuti'),
(463, 'Pacific/Honolulu'),
(464, 'Pacific/Majuro'),
(465, 'Pacific/Norfolk'),
(466, 'Pacific/Pohnpei'),
(467, 'Pacific/Samoa'),
(468, 'Pacific/Wake'),
(469, 'Brazil/Acre'),
(470, 'Canada/Central'),
(471, 'Canada/Pacific'),
(472, 'Chile/EasterIsland'),
(473, 'Eire'),
(474, 'Etc/GMT+1'),
(475, 'Etc/GMT+3'),
(476, 'Etc/GMT+8'),
(477, 'Etc/GMT-11'),
(478, 'Etc/GMT-3'),
(479, 'Etc/GMT-8'),
(480, 'Etc/Universal'),
(481, 'GB-Eire'),
(482, 'Greenwich'),
(483, 'Israel'),
(484, 'MET'),
(485, 'MST7MDT'),
(486, 'Portugal'),
(487, 'Singapore'),
(488, 'US/Aleutian'),
(489, 'US/Hawaii'),
(490, 'US/Pacific-New'),
(491, 'Brazil/DeNoronha'),
(492, 'Canada/East-Saskatchewan'),
(493, 'Canada/Saskatchewan'),
(494, 'CST6CDT'),
(495, 'EST'),
(496, 'Etc/GMT+10'),
(497, 'Etc/GMT+4'),
(498, 'Etc/GMT+9'),
(499, 'Etc/GMT-12'),
(500, 'Etc/GMT-4'),
(501, 'Etc/GMT-9'),
(502, 'Etc/UTC'),
(503, 'GMT'),
(504, 'Hongkong'),
(505, 'Jamaica'),
(506, 'Mexico/BajaNorte'),
(507, 'Navajo'),
(508, 'PRC'),
(509, 'Turkey'),
(510, 'US/Arizona'),
(511, 'US/Indiana-Starke'),
(512, 'US/Samoa'),
(513, 'Brazil/East'),
(514, 'Canada/Eastern'),
(515, 'Canada/Yukon'),
(516, 'Cuba'),
(517, 'EST5EDT'),
(518, 'Etc/GMT+11'),
(519, 'Etc/GMT+5'),
(520, 'Etc/GMT-0'),
(521, 'Etc/GMT-13'),
(522, 'Etc/GMT-5'),
(523, 'Etc/GMT0'),
(524, 'Etc/Zulu'),
(525, 'GMT+0'),
(526, 'HST'),
(527, 'Japan'),
(528, 'Mexico/BajaSur'),
(529, 'NZ'),
(530, 'PST8PDT'),
(531, 'UCT'),
(532, 'US/Central'),
(533, 'US/Michigan'),
(534, 'UTC'),
(535, 'Brazil/West'),
(536, 'Canada/Mountain'),
(537, 'CET'),
(538, 'EET'),
(539, 'Etc/GMT'),
(540, 'Etc/GMT+12'),
(541, 'Etc/GMT+6'),
(542, 'Etc/GMT-1'),
(543, 'Etc/GMT-14'),
(544, 'Etc/GMT-6'),
(545, 'Etc/Greenwich'),
(546, 'Factory'),
(547, 'GMT-0'),
(548, 'Iceland'),
(549, 'Kwajalein'),
(550, 'Mexico/General'),
(551, 'NZ-CHAT'),
(552, 'ROC'),
(553, 'Universal'),
(554, 'US/East-Indiana'),
(555, 'US/Mountain'),
(556, 'W-SU'),
(557, 'Canada/Atlantic'),
(558, 'Canada/Newfoundland'),
(559, 'Chile/Continental'),
(560, 'Egypt'),
(561, 'Etc/GMT+0'),
(562, 'Etc/GMT+2'),
(563, 'Etc/GMT+7'),
(564, 'Etc/GMT-10'),
(565, 'Etc/GMT-2'),
(566, 'Etc/GMT-7'),
(567, 'Etc/UCT'),
(568, 'GB'),
(569, 'GMT0'),
(570, 'Iran'),
(571, 'Libya'),
(572, 'MST'),
(573, 'Poland'),
(574, 'ROK'),
(575, 'US/Alaska'),
(576, 'US/Eastern'),
(577, 'US/Pacific'),
(578, 'WET');

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE IF NOT EXISTS `transaction` (
  `transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(100) NOT NULL,
  `project_id` int(11) NOT NULL,
  `perk_id` int(11) NOT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `listing_fee` varchar(255) DEFAULT NULL,
  `pay_fee` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `host_ip` varchar(255) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `paypal_email` varchar(255) DEFAULT NULL,
  `transaction_date_time` datetime NOT NULL,
  `amazon_transaction_id` text,
  `paypal_paykey` varchar(255) DEFAULT NULL,
  `paypal_adaptive_status` varchar(50) DEFAULT NULL,
  `preapproval_key` varchar(255) DEFAULT NULL,
  `preapproval_pay_key` varchar(255) DEFAULT NULL,
  `preapproval_status` varchar(100) DEFAULT NULL,
  `preapproval_total_amount` varchar(100) DEFAULT NULL,
  `paypal_transaction_id` varchar(255) DEFAULT NULL,
  `wallet_transaction_id` varchar(255) DEFAULT NULL,
  `wallet_payment_status` tinyint(1) NOT NULL,
  `trans_anonymous` tinyint(1) NOT NULL,
  `credit_card_transaction_id` varchar(255) DEFAULT NULL,
  `credit_card_capture_transaction_id` varchar(255) DEFAULT NULL,
  `credit_card_payment_status` int(20) NOT NULL DEFAULT '0' COMMENT '0=not done,1=captured',
  `keep_update` tinyint(1) NOT NULL,
  `recipient` varchar(255) NOT NULL,
  `gift` tinyint(1) NOT NULL,
  `wepay_preapproval_id` int(255) NOT NULL,
  `wepay_account_id` int(255) NOT NULL,
  `wepay_frequency` int(255) NOT NULL,
  `wepay_start_time` int(255) NOT NULL,
  `wepay_end_time` int(255) NOT NULL,
  `wepay_amount` double(10,2) NOT NULL,
  `wepay_checkout_id` int(255) NOT NULL,
  `wepay_get_frequency` int(255) NOT NULL,
  `shipping` text,
  `customer_payment_key` varchar(255) NOT NULL,
  `stripe_token` varchar(255) NOT NULL,
  `perk_delivered` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`transaction_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `transaction`
--

INSERT INTO `transaction` (`transaction_id`, `user_id`, `project_id`, `perk_id`, `amount`, `listing_fee`, `pay_fee`, `name`, `email`, `host_ip`, `comment`, `paypal_email`, `transaction_date_time`, `amazon_transaction_id`, `paypal_paykey`, `paypal_adaptive_status`, `preapproval_key`, `preapproval_pay_key`, `preapproval_status`, `preapproval_total_amount`, `paypal_transaction_id`, `wallet_transaction_id`, `wallet_payment_status`, `trans_anonymous`, `credit_card_transaction_id`, `credit_card_capture_transaction_id`, `credit_card_payment_status`, `keep_update`, `recipient`, `gift`, `wepay_preapproval_id`, `wepay_account_id`, `wepay_frequency`, `wepay_start_time`, `wepay_end_time`, `wepay_amount`, `wepay_checkout_id`, `wepay_get_frequency`, `shipping`, `customer_payment_key`, `stripe_token`, `perk_delivered`) VALUES
(1, 5, 5, 0, '100.00', '0', '0.00', 'jaimin', 'jaimin.rockersinfo@gmail.com', '1.22.82.216', '', 'anita.rockersinfo@gmail.com', '2014-10-29 23:56:45', NULL, NULL, NULL, 'PA-36C35634KH771500C', NULL, 'ON_HOLD', '100.00', NULL, NULL, 0, 1, NULL, NULL, 0, 0, '', 0, 0, 0, 0, 0, 0, 0.00, 0, 0, '', '', '', 0),
(2, 4, 5, 0, '300.00', '0', '0.00', 'Priya', 'priya.rockersinfo@gmail.com', '1.22.82.216', 'Chapel Elementary receives ..', 'anita.rockersinfo@gmail.com', '2014-10-30 00:25:18', NULL, NULL, NULL, 'PA-7T982700FU313480A', NULL, 'ON_HOLD', '300.00', NULL, NULL, 0, 1, NULL, NULL, 0, 0, '', 0, 0, 0, 0, 0, 0, 0.00, 0, 0, '', '', '', 0),
(3, 3, 5, 0, '300.00', '0', '0.00', 'Jayshree', 'jayshree.rockersinfo@gmail.com', '1.22.82.216', 'fund raising  support and personalizing your contribution..', 'anita.rockersinfo@gmail.com', '2014-10-30 00:33:49', NULL, NULL, NULL, 'PA-2GA061505U689250E', NULL, 'ON_HOLD', '300.00', NULL, NULL, 0, 1, NULL, NULL, 0, 0, '', 0, 0, 0, 0, 0, 0, 0.00, 0, 0, '', '', '', 0),
(4, 3, 6, 0, '200.00', '0', '0.00', 'Jayshree', 'jayshree.rockersinfo@gmail.com', '1.22.82.216', '', 'anita.rockersinfo@gmail.com', '2014-10-30 01:13:11', NULL, NULL, NULL, 'PA-7CV610943J7803626', NULL, 'ON_HOLD', '200.00', NULL, NULL, 0, 1, NULL, NULL, 0, 0, '', 0, 0, 0, 0, 0, 0, 0.00, 0, 0, '', '', '', 0),
(5, 5, 6, 0, '100.00', '0', '0.00', 'jaimin', 'jaimin.rockersinfo@gmail.com', '1.22.82.216', '', 'anita.rockersinfo@gmail.com', '2014-10-30 01:19:55', NULL, NULL, NULL, 'PA-2FR456594M738980A', NULL, 'ON_HOLD', '100.00', NULL, NULL, 0, 1, NULL, NULL, 0, 0, '', 0, 0, 0, 0, 0, 0, 0.00, 0, 0, '', '', '', 0),
(6, 4, 6, 0, '100.00', '0', '0.00', 'Priya', 'priya.rockersinfo@gmail.com', '1.22.82.216', 'Community-Based Education Programs donation...', 'anita.rockersinfo@gmail.com', '2014-10-30 01:27:28', NULL, NULL, NULL, 'PA-3XA61060RH852425D', NULL, 'ON_HOLD', '100.00', NULL, NULL, 0, 1, NULL, NULL, 0, 0, '', 0, 0, 0, 0, 0, 0, 0.00, 0, 0, '', '', '', 0),
(7, 4, 4, 0, '200.00', '0', '0.00', 'Priya', 'priya.rockersinfo@gmail.com', '1.22.82.216', 'fundrasing your support and personalizing your contribution..', 'anita.rockersinfo@gmail.com', '2014-10-30 02:58:53', NULL, NULL, NULL, 'PA-7VB17899B8811374C', NULL, 'ON_HOLD', '200.00', NULL, NULL, 0, 1, NULL, NULL, 0, 0, '', 0, 0, 0, 0, 0, 0, 0.00, 0, 0, '', '', '', 0),
(8, 5, 4, 0, '300.00', '0', '0.00', 'jaimin', 'jaimin.rockersinfo@gmail.com', '1.22.82.216', 'donation great for voicing your support and personalizing your contribution..', 'anita.rockersinfo@gmail.com', '2014-10-30 03:02:53', NULL, NULL, NULL, 'PA-91C99300PS596063J', NULL, 'ON_HOLD', '300.00', NULL, NULL, 0, 1, NULL, NULL, 0, 0, '', 0, 0, 0, 0, 0, 0, 0.00, 0, 0, '', '', '', 0),
(9, 3, 4, 0, '300.00', '0', '0.00', 'Jayshree', 'jayshree.rockersinfo@gmail.com', '1.22.82.216', '', 'anita.rockersinfo@gmail.com', '2014-10-30 03:06:52', NULL, NULL, NULL, 'PA-5W847791LT332392V', NULL, 'ON_HOLD', '300.00', NULL, NULL, 0, 1, NULL, NULL, 0, 0, '', 0, 0, 0, 0, 0, 0, 0.00, 0, 0, '', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `transaction_type`
--

CREATE TABLE IF NOT EXISTS `transaction_type` (
  `transaction_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_type_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`transaction_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `transaction_type`
--

INSERT INTO `transaction_type` (`transaction_type_id`, `transaction_type_name`) VALUES
(1, 'cash'),
(2, 'check'),
(3, 'credit card');

-- --------------------------------------------------------

--
-- Table structure for table `translation`
--

CREATE TABLE IF NOT EXISTS `translation` (
  `translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`translation_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `translation`
--

INSERT INTO `translation` (`translation_id`, `language`) VALUES
(2, 'English'),
(9, 'French');

-- --------------------------------------------------------

--
-- Table structure for table `translation_key`
--

CREATE TABLE IF NOT EXISTS `translation_key` (
  `key_id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`key_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `translation_text`
--

CREATE TABLE IF NOT EXISTS `translation_text` (
  `text_id` int(11) NOT NULL AUTO_INCREMENT,
  `translation_id` int(11) NOT NULL,
  `key_id` int(11) NOT NULL,
  `text` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`text_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `twitter_setting`
--

CREATE TABLE IF NOT EXISTS `twitter_setting` (
  `twitter_setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `twitter_enable` varchar(255) DEFAULT NULL,
  `twitter_user_name` varchar(255) DEFAULT NULL,
  `consumer_key` varchar(255) DEFAULT NULL,
  `consumer_secret` varchar(255) DEFAULT NULL,
  `tw_oauth_token` varchar(500) DEFAULT NULL,
  `tw_oauth_token_secret` varchar(500) DEFAULT NULL,
  `autopost_site` varchar(255) DEFAULT NULL,
  `autopost_user` varchar(255) DEFAULT NULL,
  `twitter_url` text,
  `twiter_img` varchar(255) DEFAULT NULL,
  `tw_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`twitter_setting_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `twitter_setting`
--

INSERT INTO `twitter_setting` (`twitter_setting_id`, `twitter_enable`, `twitter_user_name`, `consumer_key`, `consumer_secret`, `tw_oauth_token`, `tw_oauth_token_secret`, `autopost_site`, `autopost_user`, `twitter_url`, `twiter_img`, `tw_id`) VALUES
(1, '0', '0', 'HrmnWsL1i0N6t52mMOWw', 'VYm7Otv5fD2qUM32QF8kRDebr1vj7u7QU7p7V9lfdVI', '495080696864460800', 'Chintan2Rockers', '0', '0', 'https://twitter.com/Chintan2Rockers', 'anita_rockers.jpg', '624506841');

-- --------------------------------------------------------

--
-- Table structure for table `updates`
--

CREATE TABLE IF NOT EXISTS `updates` (
  `update_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `updates` text,
  `image` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`update_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=239 ;

--
-- Dumping data for table `updates`
--

INSERT INTO `updates` (`update_id`, `project_id`, `updates`, `image`, `status`, `date_added`) VALUES
(1, 12, '<p>sdfs fs fs fs fs&nbsp;</p>\n', NULL, '0', '2013-09-07 11:11:19'),
(2, 12, '<p>sd fsd fs fs fs s</p>\n', NULL, '0', '2013-09-07 11:11:36'),
(23, 25, '<p>Hello Wait For More Update.</p>\n\n<p>I respect your</p>\n', NULL, '0', '2013-09-10 17:30:30'),
(24, 18, '<p>tetetet</p>\n', NULL, '0', '2013-09-11 17:22:19'),
(26, 30, '<p>hi how are you.... we are giving to update</p>\n', NULL, '0', '2013-09-14 17:34:10'),
(27, 33, '<p>Hrello</p>\n', NULL, '0', '2013-09-21 15:59:05'),
(33, 109, '<p>wsrwwww r39yr09 30ry3 r903y9r 390yr903 9ry390 9390r 093h9rh90 3wqhr 03hr 939rh9qw3 rwqh&nbsp;</p>\n', NULL, '0', '2013-12-25 05:14:47'),
(34, 109, '<p><span style="color:rgb(76, 76, 76); font-family:arial,helvetica,sans-serif; font-size:12px">Save tigers save tigers save tigers save tigers save tigers save tigers save tigers save tigers save tigers save&nbsp;</span></p>\n', NULL, '0', '2013-12-25 05:14:59'),
(35, 109, '<p><span style="color:rgb(76, 76, 76); font-family:arial,helvetica,sans-serif; font-size:12px">Save tigers save tigers save tigers save tigers save tigers save tigers save tigers save tigers save tigers save</span></p>\n', NULL, '0', '2013-12-25 05:15:23'),
(36, 109, '<p>Save tigers save tigers save tigers save tigers save tigers save tigers save tigers save tigers save tigers save</span></p>\n', NULL, '0', '2013-12-25 05:15:48'),
(37, 119, '<p>Save tigers save tigers save tigers save tigers save tigers save tigers save tigers save tigers save tigers save tigers save tigers save tigers - See more at: http://www.hireiphone.com/indiegogoclone/projects/save-tigers1/109#.UrqHodIW2kp</p>\n', NULL, '0', '2013-12-25 05:25:06'),
(41, 95, '<h4>Review all Updates to your Project below.</h4>\n', NULL, '0', '2013-12-31 23:54:52'),
(42, 95, '<h4>Review all Updates to your Project below.</h4>\n', NULL, '0', '2013-12-31 23:55:02'),
(49, 34, '<p>Create steps create steps create steps create steps create steps create steps create steps create steps create steps create steps</p>\n\n<p><span style="background-color:rgb(239, 239, 239); color:rgb(76, 76, 76); font-family:arial,helvetica,sans-serif; font-size:12px"><a href="http://www.hireiphone.com/indiegogoclone/category/accidents-personal-crisis" style="text-decoration: none; outline: none; color: rgb(59, 176, 210);">Accidents &amp; Personal Crisis</a>&nbsp;- Vadodara , India</span><span style="background-color:rgb(239, 239, 239); color:rgb(76, 76, 76); font-family:arial,helvetica,sans-serif; font-size:12px">&nbsp;- See more at: http://www.hireiphone.com/indiegogoclone/projects/create-steps1#.UsUW8WQW3ZY</span></p>\n', NULL, '0', '2014-01-02 03:44:27'),
(51, 177, 'fsdfsdfsdf', 'no_img.jpg', '0', '2014-01-10 11:14:22'),
(52, 154, '<p>fsfsfsf wfwfw wrwrw r wrw wrwrww rwrw</p>\n', NULL, '0', '2014-03-20 05:31:36'),
(53, 222, '<h2 style="margin-left:120px">SCIENCE MATTERS: SUPPORTING STEM RESEARCH BY URBAN YOUTH</h2>\n\n<p><span style="font-size:16px">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</span>&nbsp;<br />\n<br />\nDonec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.&nbsp;<br />\n<br />\nAenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi.&nbsp;<br />\n<br />\nNam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.&nbsp;</p>\n', NULL, '0', '2014-03-22 01:42:40'),
(54, 222, '<h2 style="margin-left:120px">SCIENCE MATTERS: SUPPORTING STEM RESEARCH BY URBAN YOUTH</h2>\n\n<p><span style="font-size:16px">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</span>&nbsp;<br />\n<br />\nDonec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.&nbsp;<br />\n<br />\nAenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi.&nbsp;<br />\n<br />\nNam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.&nbsp;</p>\n', NULL, '0', '2014-03-22 01:42:44'),
(55, 222, '<h2 style="margin-left:120px">SCIENCE MATTERS: SUPPORTING STEM RESEARCH BY URBAN YOUTH</h2>\n\n<p><span style="font-size:16px">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</span>&nbsp;<br />\n<br />\nDonec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.&nbsp;<br />\n<br />\nAenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi.&nbsp;<br />\n<br />\nNam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.&nbsp;</p>\n', NULL, '0', '2014-03-22 01:42:45'),
(56, 222, '<h2 style="margin-left:120px">SCIENCE MATTERS: SUPPORTING STEM RESEARCH BY URBAN YOUTH</h2>\n\n<p><span style="font-size:16px">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</span>&nbsp;<br />\n<br />\nDonec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.&nbsp;<br />\n<br />\nAenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi.&nbsp;<br />\n<br />\nNam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.&nbsp;</p>\n', NULL, '0', '2014-03-22 01:42:46'),
(57, 222, '<h2 style="margin-left:120px">SCIENCE MATTERS: SUPPORTING STEM RESEARCH BY URBAN YOUTH</h2>\n\n<p><span style="font-size:16px">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</span>&nbsp;<br />\n<br />\nDonec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.&nbsp;<br />\n<br />\nAenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi.&nbsp;<br />\n<br />\nNam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.&nbsp;</p>\n', NULL, '0', '2014-03-22 01:42:47'),
(58, 222, '<h2 style="margin-left:120px">SCIENCE MATTERS: SUPPORTING STEM RESEARCH BY URBAN YOUTH</h2>\n\n<p><span style="font-size:16px">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</span>&nbsp;<br />\n<br />\nDonec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.&nbsp;<br />\n<br />\nAenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi.&nbsp;<br />\n<br />\nNam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.&nbsp;</p>\n', NULL, '0', '2014-03-22 01:42:47'),
(59, 222, '<h2 style="margin-left:120px">SCIENCE MATTERS: SUPPORTING STEM RESEARCH BY URBAN YOUTH</h2>\n\n<p><span style="font-size:16px">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</span>&nbsp;<br />\n<br />\nDonec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.&nbsp;<br />\n<br />\nAenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi.&nbsp;<br />\n<br />\nNam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.&nbsp;</p>\n', NULL, '0', '2014-03-22 01:42:48'),
(60, 222, '<h2 style="margin-left:120px">SCIENCE MATTERS: SUPPORTING STEM RESEARCH BY URBAN YOUTH</h2>\n\n<p><span style="font-size:16px">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</span>&nbsp;<br />\n<br />\nDonec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.&nbsp;<br />\n<br />\nAenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi.&nbsp;<br />\n<br />\nNam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.&nbsp;</p>\n', NULL, '0', '2014-03-22 01:42:48'),
(61, 222, '<h2 style="margin-left:120px">SCIENCE MATTERS: SUPPORTING STEM RESEARCH BY URBAN YOUTH</h2>\n\n<p><span style="font-size:16px">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</span>&nbsp;<br />\n<br />\nDonec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.&nbsp;<br />\n<br />\nAenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi.&nbsp;<br />\n<br />\nNam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.&nbsp;</p>\n', NULL, '0', '2014-03-22 01:42:49'),
(71, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:51:06'),
(74, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:51:07'),
(75, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:51:07'),
(76, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:51:07'),
(77, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:51:08'),
(78, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:51:08'),
(79, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:51:08'),
(80, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:51:10'),
(81, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:51:10'),
(82, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:51:10'),
(83, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:51:10'),
(84, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:51:10'),
(85, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:51:11'),
(86, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:51:11'),
(87, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:51:11'),
(88, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:51:11'),
(89, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:51:11'),
(90, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:51:12'),
(91, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:51:12'),
(92, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:51:12');
INSERT INTO `updates` (`update_id`, `project_id`, `updates`, `image`, `status`, `date_added`) VALUES
(94, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:51:13'),
(95, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:51:13'),
(96, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:51:14'),
(97, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:51:14'),
(98, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:51:14'),
(99, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:51:14'),
(100, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:51:15'),
(101, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:51:15'),
(102, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:51:16'),
(103, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:51:17'),
(104, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:51:59'),
(105, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:52:00'),
(106, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:52:01'),
(107, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:52:01'),
(108, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:52:01'),
(109, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:52:02'),
(110, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:52:02'),
(111, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:52:02'),
(112, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:52:02'),
(113, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:52:03'),
(114, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:52:03'),
(115, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:52:03'),
(116, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:52:04'),
(117, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:52:05'),
(118, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:52:05'),
(119, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:52:05'),
(120, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:52:05'),
(121, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:52:05'),
(122, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:52:06'),
(123, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:52:06'),
(124, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:52:06'),
(125, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:52:06'),
(126, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:52:06'),
(127, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:52:07');
INSERT INTO `updates` (`update_id`, `project_id`, `updates`, `image`, `status`, `date_added`) VALUES
(128, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:52:07'),
(129, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:52:07'),
(130, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:52:07'),
(131, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:52:08'),
(132, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:52:08'),
(133, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:52:08'),
(134, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:52:08'),
(135, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:52:08'),
(136, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:52:09'),
(137, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:52:09'),
(138, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:52:09'),
(139, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:52:09'),
(140, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:52:10'),
(141, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:52:10'),
(142, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:52:10'),
(143, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:52:10'),
(144, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:52:10'),
(145, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:52:10'),
(146, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:52:11'),
(147, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:52:11'),
(148, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n\n<div class="story-feature narrow" style="color: rgb(80, 80, 80); font-family: Arial, Helmet, Freesans, sans-serif; line-height: 16px; position: relative; margin: 0px -160px 16px 16px; width: 144px; float: right; display: inline; overflow: hidden; clear: right;"><a class="hidden" href="http://www.bbc.com/news/uk-england-derbyshire-26680076#story_continues_2" style="color: rgb(74, 113, 148); text-decoration: none; font-weight: bold; position: absolute; top: -5000px; left: -5000px;">Continue reading the main story</a>\n\n<h2>&ldquo;Start Quote</h2>\n\n<blockquote>\n<p>We&#39;ve been very surprised how many people have used it and not just people from the village, we&#39;ve had people from Mayfield and Ashbourne&rdquo;</p>\n</blockquote>\nPeter FoxInventor of Speedy Shop</div>\n\n<p>&quot;Instead of having a machine which is a certain design and therefore things have to fit in it... we&#39;ve designed the machine around the product,&quot; he said.</p>\n', NULL, '0', '2014-05-09 02:52:11'),
(149, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n', NULL, '0', '2014-05-09 02:52:21'),
(150, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n', NULL, '0', '2014-05-09 02:52:22'),
(151, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n', NULL, '0', '2014-05-09 02:52:23'),
(152, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n', NULL, '0', '2014-05-09 02:52:23'),
(153, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n', NULL, '0', '2014-05-09 02:52:24'),
(154, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n', NULL, '0', '2014-05-09 02:52:24'),
(155, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n', NULL, '0', '2014-05-09 02:52:24'),
(156, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n', NULL, '0', '2014-05-09 02:52:25'),
(157, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n', NULL, '0', '2014-05-09 02:52:25'),
(158, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n', NULL, '0', '2014-05-09 02:52:25'),
(159, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n', NULL, '0', '2014-05-09 02:52:26'),
(160, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n', NULL, '0', '2014-05-09 02:52:27'),
(161, 285, '<p>The Speedy Shop, in a pub car park in Clifton, near Ashbourne, sells more than 80 products.</p>\n\n<p>Peter Fox, who has submitted a patent application for his invention, said it was &quot;not just a business&quot;, but an idea that could serve the community.</p>\n\n<p>The automatic village shop sends an email when stock is running low.</p>\n\n<p>Mr Fox, 50, said the design was &quot;completely different&quot; to a standard vending machine.</p>\n', NULL, '0', '2014-05-09 02:52:27'),
(163, 285, '<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n', NULL, '0', '2014-05-09 03:11:13'),
(167, 288, '<p style="text-align:start">Now that the warmer weather is here, anti-smoking groups are calling on the Quebec government to ban smoking&nbsp;on&nbsp;terrasses.</p>\n\n<p style="text-align:start">&nbsp;</p>\n\n<p style="text-align:start">&quot;The majority of&nbsp;Quebecers&nbsp;are non-smokers and they would like to be able to enjoy a good drink and being outdoors without being exposed to smoke,&rdquo; said&nbsp;Fran&ccedil;ois&nbsp;Damphousse, director of the Quebec Non-Smokers&#39; Rights Association.</p>\n\n<p style="text-align:start">&nbsp;</p>\n\n<p style="text-align:start">Damphousse&nbsp;says studies show that smoking &mdash;&nbsp;even outside &mdash; affects air quality.</p>\n\n<p style="text-align:start">&nbsp;</p>\n\n<p style="text-align:start">&ldquo;When you are in close proximity to a smoker, you are exposed to all the carcinogens that are found in environmental tobacco smoke, and this is especially important for the workers. They are working in the bars and restaurants for long hours.&rdquo;</p>\n\n<p style="text-align:start">&nbsp;</p>\n\n<p style="text-align:start">Four other provinces and one territory &mdash;&nbsp;Alberta, Nova&nbsp;Scotia, Prince Edward Island, Newfoundland and Labrador and the Yukon &mdash;&nbsp;have already banned smoking on&nbsp;terrasses and&nbsp;Damphousse&nbsp;said he thinks it&rsquo;s time for Quebec to catch up.</p>\n', NULL, '0', '2014-05-09 03:13:09'),
(168, 288, '<p style="text-align:start">Now that the warmer weather is here, anti-smoking groups are calling on the Quebec government to ban smoking&nbsp;on&nbsp;terrasses.</p>\n\n<p style="text-align:start">&nbsp;</p>\n\n<p style="text-align:start">&quot;The majority of&nbsp;Quebecers&nbsp;are non-smokers and they would like to be able to enjoy a good drink and being outdoors without being exposed to smoke,&rdquo; said&nbsp;Fran&ccedil;ois&nbsp;Damphousse, director of the Quebec Non-Smokers&#39; Rights Association.</p>\n\n<p style="text-align:start">&nbsp;</p>\n\n<p style="text-align:start">Damphousse&nbsp;says studies show that smoking &mdash;&nbsp;even outside &mdash; affects air quality.</p>\n\n<p style="text-align:start">&nbsp;</p>\n\n<p style="text-align:start">&ldquo;When you are in close proximity to a smoker, you are exposed to all the carcinogens that are found in environmental tobacco smoke, and this is especially important for the workers. They are working in the bars and restaurants for long hours.&rdquo;</p>\n\n<p style="text-align:start">&nbsp;</p>\n\n<p style="text-align:start">Four other provinces and one territory &mdash;&nbsp;Alberta, Nova&nbsp;Scotia, Prince Edward Island, Newfoundland and Labrador and the Yukon &mdash;&nbsp;have already banned smoking on&nbsp;terrasses and&nbsp;Damphousse&nbsp;said he thinks it&rsquo;s time for Quebec to catch up.</p>\n', NULL, '0', '2014-05-09 03:13:11'),
(169, 288, '<p style="text-align:start">Now that the warmer weather is here, anti-smoking groups are calling on the Quebec government to ban smoking&nbsp;on&nbsp;terrasses.</p>\n\n<p style="text-align:start">&nbsp;</p>\n\n<p style="text-align:start">&quot;The majority of&nbsp;Quebecers&nbsp;are non-smokers and they would like to be able to enjoy a good drink and being outdoors without being exposed to smoke,&rdquo; said&nbsp;Fran&ccedil;ois&nbsp;Damphousse, director of the Quebec Non-Smokers&#39; Rights Association.</p>\n\n<p style="text-align:start">&nbsp;</p>\n\n<p style="text-align:start">Damphousse&nbsp;says studies show that smoking &mdash;&nbsp;even outside &mdash; affects air quality.</p>\n\n<p style="text-align:start">&nbsp;</p>\n\n<p style="text-align:start">&ldquo;When you are in close proximity to a smoker, you are exposed to all the carcinogens that are found in environmental tobacco smoke, and this is especially important for the workers. They are working in the bars and restaurants for long hours.&rdquo;</p>\n\n<p style="text-align:start">&nbsp;</p>\n\n<p style="text-align:start">Four other provinces and one territory &mdash;&nbsp;Alberta, Nova&nbsp;Scotia, Prince Edward Island, Newfoundland and Labrador and the Yukon &mdash;&nbsp;have already banned smoking on&nbsp;terrasses and&nbsp;Damphousse&nbsp;said he thinks it&rsquo;s time for Quebec to catch up.</p>\n', NULL, '0', '2014-05-09 03:13:26'),
(174, 293, '<p>Popular instructors and lesson times book up fast. We recommend calling as soon as your plans are made.&nbsp; Private lessons are offered in all ability levels for both ski and snowboard.</p>\n\n<p>Private Lessons may have up to five people of your choosing. Please note: for safety reasons, the pace of lesson will be geared towards the slowest person(s). For this reason we do not recommend combining groups with young children (under 6) with adults or big kids.</p>\n', NULL, '0', '2014-05-16 07:40:05'),
(175, 309, '<p><strong>Nutty Butter</strong><span style="color:rgb(17, 17, 17); font-family:georgia,times new roman,times,serif">&nbsp;is the result of an overactive culinary imagination and obsession with&nbsp;&nbsp;nut butter.</span></p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n', NULL, '0', '2014-05-26 03:04:52'),
(177, 311, '<p>testing active sattus</p>\n', NULL, '0', '2014-05-29 04:13:45'),
(178, 311, '<p>Updates for testing purpose</p>\n', NULL, '0', '2014-05-30 16:44:25'),
(179, 339, '<p>testing calcle buttn issue</p>\n', NULL, '0', '2014-06-03 02:33:36'),
(180, 339, '<p>dasdasd adsaf</p>\n', NULL, '0', '2014-06-03 02:46:10'),
(181, 359, '<p>Updates</p>\n', NULL, '0', '2014-06-03 03:55:46'),
(182, 378, '<p>Updates</p>\n\n<p>&nbsp;</p>\n', NULL, '0', '2014-06-06 02:19:52'),
(185, 415, '<p>Addd updates for Testing purpose only.</p>\n', NULL, '0', '2014-06-13 08:30:57');
INSERT INTO `updates` (`update_id`, `project_id`, `updates`, `image`, `status`, `date_added`) VALUES
(186, 16, '<p><a href="http://en.wikipedia.org/wiki/Project_Tiger" style="text-decoration: none; color: rgb(11, 0, 128); font-family: sans-serif; font-size: 14px; line-height: 22.399999618530273px; background-image: none; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;" title="Project Tiger">Project Tiger</a><span style="color:rgb(37, 37, 37); font-family:sans-serif; font-size:14px">, started in 1972, is a major effort to conserve the&nbsp;</span><a href="http://en.wikipedia.org/wiki/Tiger" style="text-decoration: none; color: rgb(11, 0, 128); font-family: sans-serif; font-size: 14px; line-height: 22.399999618530273px; background-image: none; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;" title="Tiger">tiger</a><span style="color:rgb(37, 37, 37); font-family:sans-serif; font-size:14px">&nbsp;and its habitats in&nbsp;</span><a href="http://en.wikipedia.org/wiki/India" style="text-decoration: none; color: rgb(11, 0, 128); font-family: sans-serif; font-size: 14px; line-height: 22.399999618530273px; background-image: none; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;" title="India">India</a><span style="color:rgb(37, 37, 37); font-family:sans-serif; font-size:14px">.</span><span style="color:rgb(37, 37, 37); font-family:sans-serif; font-size:14px">&nbsp;At the turn of the 20th century, one estimate of the tiger&nbsp;</span><a href="http://en.wikipedia.org/wiki/Population" style="text-decoration: none; color: rgb(11, 0, 128); font-family: sans-serif; font-size: 14px; line-height: 22.399999618530273px; background-image: none; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;" title="Population">population</a><span style="color:rgb(37, 37, 37); font-family:sans-serif; font-size:14px">&nbsp;in India placed the figure at 40,000, yet an Indian tiger census conducted in 1972 revealed the existence of only 1827 tigers. Various pressures in the latter part of the 20th century led to the progressive&nbsp;</span><strong><a href="http://en.wikipedia.org/wiki/Decline" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="Decline">decline</a>&nbsp;of&nbsp;<a href="http://en.wikipedia.org/wiki/Wilderness" style="text-decoration: none; color: rgb(11, 0, 128); background: none;" title="Wilderness">wilderness</a></strong><span style="color:rgb(37, 37, 37); font-family:sans-serif; font-size:14px">&nbsp;resulting in the disturbance of viable tiger&nbsp;</span><a class="mw-redirect" href="http://en.wikipedia.org/wiki/Habitats" style="text-decoration: none; color: rgb(11, 0, 128); font-family: sans-serif; font-size: 14px; line-height: 22.399999618530273px; background-image: none; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;" title="Habitats">habitats</a><span style="color:rgb(37, 37, 37); font-family:sans-serif; font-size:14px">. At the&nbsp;</span><a class="mw-redirect" href="http://en.wikipedia.org/wiki/IUCN" style="text-decoration: none; color: rgb(11, 0, 128); font-family: sans-serif; font-size: 14px; line-height: 22.399999618530273px; background-image: none; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;" title="IUCN">International Union for Conservation of Nature and Natural Resources</a><span style="color:rgb(37, 37, 37); font-family:sans-serif; font-size:14px">&nbsp;(IUCN) General Assembly meeting in&nbsp;</span><a href="http://en.wikipedia.org/wiki/Delhi" style="text-decoration: none; color: rgb(11, 0, 128); font-family: sans-serif; font-size: 14px; line-height: 22.399999618530273px; background-image: none; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;" title="Delhi">Delhi</a><span style="color:rgb(37, 37, 37); font-family:sans-serif; font-size:14px">&nbsp;in 1969, serious concern was voiced about the&nbsp;</span><a class="extiw" href="http://en.wiktionary.org/wiki/threat" style="text-decoration: none; color: rgb(102, 51, 102); font-family: sans-serif; font-size: 14px; line-height: 22.399999618530273px; background-image: none; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;" title="wiktionary:threat">threat</a><span style="color:rgb(37, 37, 37); font-family:sans-serif; font-size:14px">&nbsp;to several&nbsp;</span><a href="http://en.wikipedia.org/wiki/Species" style="text-decoration: none; color: rgb(11, 0, 128); font-family: sans-serif; font-size: 14px; line-height: 22.399999618530273px; background-image: none; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;" title="Species">species</a><span style="color:rgb(37, 37, 37); font-family:sans-serif; font-size:14px">&nbsp;of&nbsp;</span><a href="http://en.wikipedia.org/wiki/Wildlife" style="text-decoration: none; color: rgb(11, 0, 128); font-family: sans-serif; font-size: 14px; line-height: 22.399999618530273px; background-image: none; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;" title="Wildlife">wildlife</a><span style="color:rgb(37, 37, 37); font-family:sans-serif; font-size:14px">, and the shrinkage of wilderness in India from poaching. In 1970, a national ban on&nbsp;</span><strong>tiger hunting</strong><span style="color:rgb(37, 37, 37); font-family:sans-serif; font-size:14px">&nbsp;was imposed, and in 1972 the&nbsp;</span><a class="mw-redirect" href="http://en.wikipedia.org/wiki/Wildlife_Protection_Act" style="text-decoration: none; color: rgb(11, 0, 128); font-family: sans-serif; font-size: 14px; line-height: 22.399999618530273px; background-image: none; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;" title="Wildlife Protection Act">Wildlife Protection Act</a><span style="color:rgb(37, 37, 37); font-family:sans-serif; font-size:14px">&nbsp;came into force. The framework was then set to formulate a project for tiger conservation with an</span><a href="http://en.wikipedia.org/wiki/Ecology" style="text-decoration: none; color: rgb(11, 0, 128); font-family: sans-serif; font-size: 14px; line-height: 22.399999618530273px; background-image: none; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;" title="Ecology">ecological</a><span style="color:rgb(37, 37, 37); font-family:sans-serif; font-size:14px">&nbsp;approach.</span></p>\n', NULL, '0', '2014-06-26 07:42:31'),
(188, 35, '<p>&nbsp; &nbsp;&nbsp;</p>\n', NULL, '0', '2014-07-18 07:36:39'),
(189, 35, '<p>&nbsp; &nbsp; &nbsp;</p>\n', NULL, '0', '2014-07-18 07:38:25'),
(190, 36, '<p>Trees are important, valuable and necessary for the existence of any living creature on earth. They are essential to life as we know it and are the ground troops on an environmental frontline.</p>\n\n<p>Oxygen production, cleaning the soil, controlling noise pollution, slowing down storm water runoff, acting as carbon sinks, cleaning the air, providing shade and coolness, being windbreaks and fighting soil erosion can be considered as the first few reasons why trees are priceless.</p>\n\n<p>In the State of Maharashtra, the Maharashtra (Urban Areas) Protection and Preservation of Trees Act, 1975 makes a better provision for trees in the urban areas by regulating felling of trees and providing for planting of adequate number of new trees in those areas.</p>\n\n<p>This act states that no can fell any tree or cause any tree to be felled, whether of ownership or otherwise, in an urban area without seeking the permission of the Tree Authority.</p>\n', NULL, '0', '2014-07-19 03:04:28'),
(191, 58, '<p>testing updatees with fench</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n', NULL, '0', '2014-07-23 02:19:57'),
(221, 55, '<p>Updates1</p>\n', NULL, '0', '2014-08-18 07:29:47'),
(222, 55, '<p>Updates2</p>\n\n<p>&nbsp;</p>\n', NULL, '0', '2014-08-18 07:30:23'),
(223, 55, '<p>Updates3</p>\n', NULL, '0', '2014-08-18 07:30:07'),
(224, 55, '<p>Updates4</p>\n', NULL, '0', '2014-08-18 07:30:15'),
(225, 55, '<p>Updates5</p>\n', NULL, '0', '2014-08-18 07:30:43'),
(226, 55, '<p>Updates6</p>\n', NULL, '0', '2014-08-18 07:30:52'),
(227, 55, '<p>Updates7</p>\n', NULL, '0', '2014-08-18 07:31:03'),
(228, 55, '<p>Updates8</p>\n', NULL, '0', '2014-08-18 07:31:13'),
(229, 55, '<p>Updates9</p>\n', NULL, '0', '2014-08-18 07:31:24'),
(230, 55, '<p>Updates10</p>\n', NULL, '0', '2014-08-18 07:31:34'),
(231, 55, '<p>Updates11</p>\n', NULL, '0', '2014-08-18 07:31:43'),
(232, 55, '<p>Updates12</p>\n', NULL, '0', '2014-08-18 07:31:52'),
(233, 59, '<p><span style="color:rgb(0, 0, 0); font-family:arial,sans-serif; font-size:12px">Aquaponics is a combination of aquaculture (fish farming) and hydroponics (soilless plant farming). It utilizes fish wastewater as a resource by circulating it through hydroponic grow beds where plants take up its nutrients. This differs from conventional closed-system aquaculture where fish wastewater is treated using various types of bio-filtration and then either returned to the fish-rearing tanks or discarded. In aquaponics, a symbiotic relationship is formed between fish and plants. The fish provide most of the plants&rsquo; required nutrients and the plants clean the water for the fish.</span></p>\n', NULL, '0', '2014-08-18 09:15:29'),
(234, 60, '<p><span style="background-color:rgb(247, 247, 247); color:rgb(68, 68, 68); font-family:helvetica neue,helvetica,arial,sans-serif; font-size:14px">Brain research that helps create effective and inspiring leaders, by balancing two opposing ways of thinking: empathy and task focus.</span></p>\n', NULL, '0', '2014-08-18 09:16:15'),
(235, 5, '<p><span style="background-color:rgb(255, 255, 255); color:rgb(37, 37, 37); font-family:sans-serif; font-size:14px">Here is a list of organizations that accept bitcoin donations. Only notable donation-accepting sites should be added here. Additions must include a reputable 3rd-party source for verification. Do not donate to organizations and projects that you cannot verify are legitimate............</span></p>\n', NULL, '0', '2014-10-29 22:47:18'),
(236, 6, '<p>This excludes announcing&nbsp;<em>newly</em>&nbsp;developed services, which naturally don&#39;t have &#39;notability&#39;.</p>\n\n<p>A solution could be to create another page, or to divide this page into one section with notability and one without. The general problem with this term is, who decides, who has notability and who not?</p>\n\n<p>My use-case is announcing</p>\n', NULL, '0', '2014-10-30 00:48:25'),
(237, 4, '<p><span style="color:rgb(67, 52, 11); font-family:arial,helvetica,liberation sans,freesans,sans-serif; font-size:15px">The project promotes civic participation and democracy in Argentina (democracy was re-established in 1983 after decades of dictatorships). The idea is to create a platform where young people can get involved in the life of the country from a non-political-party angle.The concept will give information and tools of civic participation, public actions, foster social commitment and make democracy stronger, accepting diverse opinions and voices to create a better future for all.</span></p>\n', NULL, '0', '2014-10-30 02:48:29'),
(238, 2, '<p><span style="color:rgb(67, 52, 11); font-family:arial,helvetica,liberation sans,freesans,sans-serif; font-size:15px">Your gift will support the donation of </span>Motor Vehicle Accident Claims Fund<span style="color:rgb(67, 52, 11); font-family:arial,helvetica,liberation sans,freesans,sans-serif; font-size:15px">&nbsp;and supporting services to improve the quality of education in some developing countries.</span></p>\n', NULL, '0', '2014-10-30 03:15:14');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(1000) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `gender` varchar(50) DEFAULT NULL,
  `is_admin` int(10) NOT NULL DEFAULT '0',
  `fb_uid` varchar(100) DEFAULT NULL,
  `tw_id` varchar(255) DEFAULT NULL,
  `tw_screen_name` varchar(255) DEFAULT NULL,
  `tw_oauth_token` text,
  `tw_oauth_token_secret` text,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `zip_code` varchar(255) DEFAULT NULL,
  `paypal_email` varchar(255) DEFAULT NULL,
  `paypal_verified` varchar(10) DEFAULT NULL,
  `signup_ip` varchar(255) DEFAULT NULL,
  `active` varchar(255) DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `amazon_token_id` varchar(255) DEFAULT NULL,
  `refund_token_id` varchar(255) DEFAULT NULL,
  `user_about` text,
  `user_website` varchar(255) DEFAULT NULL,
  `user_occupation` varchar(255) DEFAULT NULL,
  `user_interest` text,
  `user_skill` text,
  `facebook_url` varchar(255) DEFAULT NULL,
  `twitter_url` varchar(255) DEFAULT NULL,
  `linkedln_url` varchar(255) DEFAULT NULL,
  `googleplus_url` varchar(255) DEFAULT NULL,
  `bandcamp_url` varchar(255) DEFAULT NULL,
  `youtube_url` varchar(255) DEFAULT NULL,
  `myspace_url` varchar(255) DEFAULT NULL,
  `confirm_key` varchar(25) DEFAULT NULL,
  `unique_code` varchar(255) DEFAULT NULL,
  `reference_user_id` int(100) DEFAULT NULL,
  `enable_facebook_stream` tinyint(1) NOT NULL,
  `enable_twitter_stream` tinyint(1) NOT NULL,
  `image_no` varchar(50) DEFAULT NULL,
  `fb_access_token` varchar(200) DEFAULT NULL,
  `forgot_unique_code` varchar(225) NOT NULL,
  `request_date` datetime NOT NULL,
  `facebook_wall_post` int(1) NOT NULL,
  `autopost_site` int(1) NOT NULL,
  `admin_active` tinyint(1) NOT NULL,
  `suspend_reason` text NOT NULL,
  `linkdin_id` varchar(200) DEFAULT NULL,
  `subscribe_id` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=''no newsletter'',1=''registered'',2=''subscriber''',
  `bank_account` varchar(255) NOT NULL,
  `bank_account_country` varchar(255) NOT NULL,
  `routing_number` int(11) NOT NULL,
  `account_number` int(11) NOT NULL,
  `bank_token_id` varchar(255) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `profile_slug` varchar(100) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_name`, `last_name`, `email`, `password`, `image`, `gender`, `is_admin`, `fb_uid`, `tw_id`, `tw_screen_name`, `tw_oauth_token`, `tw_oauth_token_secret`, `address`, `city`, `state`, `country`, `zip_code`, `paypal_email`, `paypal_verified`, `signup_ip`, `active`, `date_added`, `amazon_token_id`, `refund_token_id`, `user_about`, `user_website`, `user_occupation`, `user_interest`, `user_skill`, `facebook_url`, `twitter_url`, `linkedln_url`, `googleplus_url`, `bandcamp_url`, `youtube_url`, `myspace_url`, `confirm_key`, `unique_code`, `reference_user_id`, `enable_facebook_stream`, `enable_twitter_stream`, `image_no`, `fb_access_token`, `forgot_unique_code`, `request_date`, `facebook_wall_post`, `autopost_site`, `admin_active`, `suspend_reason`, `linkdin_id`, `subscribe_id`, `bank_account`, `bank_account_country`, `routing_number`, `account_number`, `bank_token_id`, `bank_name`, `profile_slug`) VALUES
(1, 'John', 'Maxwel', 'john123@gmail.com', 'e2abe954707f234620dac7d55adc1c66', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1.22.83.150', NULL, '2014-10-19 23:29:51', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2dOqN6Ei5l9kQy15y4Bt', 'X7gNC8b5BJZu', 0, 0, 0, NULL, NULL, '', '0000-00-00 00:00:00', 0, 0, 0, '', NULL, 0, '', '', 0, 0, '', '', 'john-maxwell'),
(2, 'ketan', 'patel', 'ketan.rockersinfo@gmail.com', '25d55ad283aa400af464c76d713c07ad', '100007825816726.jpg', NULL, 0, '100007825816726', '', '', '', '', 'Vadodara, Gujarat, India', NULL, NULL, NULL, '390017', NULL, NULL, '1.22.83.150', '1', '2014-10-19 23:50:40', NULL, NULL, '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'SFyNuUWPZZo8', 0, 0, 0, NULL, 'CAAJdLDWnTFABAK8yeWJwWOWiSKyy89ZBeR156QlmaZByys7L0h9ODa0NatzP7CnvvCJfvV6s4LwVPqXWIhu0eZAKNwbzuKahNkHtnsxD3cZC0nDGHiQvrXcXTb8ZC7As6GXdZCbPscMWZBxj1yuTnQC5brPFEbRk66aYZCWwFN22btffzVLHdvN1NvSrd1BPode0qZC', '', '0000-00-00 00:00:00', 0, 0, 0, '', NULL, 0, '', '', 0, 0, '', '', 'ketan-patel123'),
(3, 'Jayshree', 'Patel', 'jayshree.rockersinfo@gmail.com', '25d55ad283aa400af464c76d713c07ad', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1.22.81.148', '1', '2014-10-20 03:06:29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'E4EJHQnE7XNj', 0, 0, 0, NULL, NULL, '', '0000-00-00 00:00:00', 0, 0, 0, '', NULL, 0, '', '', 0, 0, '', '', 'jayshree-patel'),
(4, 'Priya', 'Patwardhan', 'priya.rockersinfo@gmail.com', '25d55ad283aa400af464c76d713c07ad', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '27.109.7.130', '1', '2014-10-21 05:13:12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'RiVcjVsMS88b', 0, 0, 0, NULL, NULL, '', '0000-00-00 00:00:00', 0, 0, 0, '', NULL, 0, '', '', 0, 0, '', '', 'priya-patwardhan'),
(5, 'jaimin', 'parmar', 'jaimin.rockersinfo@gmail.com', '25d55ad283aa400af464c76d713c07ad', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1.22.83.171', '1', '2014-10-22 01:12:44', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'Tadwzn8XvbMu', 0, 0, 0, NULL, NULL, '', '0000-00-00 00:00:00', 0, 0, 0, '', NULL, 0, '', '', 0, 0, '', '', 'jamin-parmar'),
(6, 'Tambe', '', 'stanleynkongho@gmail.com', 'b40154210b05e83bccc1529d93df525c', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '41.203.64.131', '1', '2014-10-25 08:20:56', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'SqUEoKFEmkMX', 0, 0, 0, NULL, NULL, '', '0000-00-00 00:00:00', 0, 0, 0, '', NULL, 0, '', '', 0, 0, '', '', 'tambe'),
(7, 'Jayu', 'Patel', 'jayshree.test.rockersinfo@gmail.com', '25d55ad283aa400af464c76d713c07ad', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '27.109.7.130', '1', '2014-10-27 06:12:41', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'YqN3gPvSbwvL', 0, 0, 0, NULL, NULL, '', '0000-00-00 00:00:00', 0, 0, 0, '', NULL, 0, '', '', 0, 0, '', '', 'jayu-patel'),
(8, 'Viral', 'Vadgama', 'vadgama.rockersinfo@gmail.com', 'sha256:1000:Y7IqisjeXoVD6PhzqJbCH92JwwEmD/uM:oaCrwJ77lGUZONLkLvWalD8vnmyoVAGz', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '127.0.0.1', '1', '2014-11-26 06:41:50', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'cJgBN7tEGsg5', 0, 0, 0, NULL, NULL, '', '0000-00-00 00:00:00', 0, 0, 0, '', NULL, 0, '', '', 0, 0, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `user_card_info`
--

CREATE TABLE IF NOT EXISTS `user_card_info` (
  `user_card_id` int(100) NOT NULL AUTO_INCREMENT,
  `user_id` int(100) NOT NULL,
  `card_first_name` varchar(255) DEFAULT NULL,
  `card_last_name` varchar(255) DEFAULT NULL,
  `card_number` varchar(255) DEFAULT NULL,
  `card_type` varchar(100) DEFAULT NULL,
  `card_expiration_month` int(30) NOT NULL,
  `card_expiration_year` int(50) NOT NULL,
  `card_address` text,
  `card_city` varchar(255) DEFAULT NULL,
  `card_state` varchar(255) DEFAULT NULL,
  `card_zipcode` varchar(255) DEFAULT NULL,
  `card_transaction_id` varchar(255) DEFAULT NULL,
  `card_verify_status` int(30) NOT NULL DEFAULT '0',
  `card_date` datetime NOT NULL,
  `card_ip` varchar(255) DEFAULT NULL,
  `card_paypal_email` varchar(255) DEFAULT NULL,
  `card_paypal_verify` int(20) NOT NULL DEFAULT '0' COMMENT '0=not,1=yes',
  PRIMARY KEY (`user_card_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_follow`
--

CREATE TABLE IF NOT EXISTS `user_follow` (
  `follower_id` int(11) NOT NULL AUTO_INCREMENT,
  `follow_user_id` int(11) NOT NULL,
  `follow_by_user_id` int(11) NOT NULL,
  `user_follow_date` datetime NOT NULL,
  PRIMARY KEY (`follower_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_login`
--

CREATE TABLE IF NOT EXISTS `user_login` (
  `login_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `login_date_time` datetime NOT NULL,
  `login_ip` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`login_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=90 ;

--
-- Dumping data for table `user_login`
--

INSERT INTO `user_login` (`login_id`, `user_id`, `login_date_time`, `login_ip`) VALUES
(1, 1, '2014-10-19 23:33:59', '1.22.83.150'),
(2, 1, '2014-10-19 23:47:59', '1.22.83.150'),
(3, 2, '2014-10-19 23:50:40', '1.22.83.150'),
(4, 1, '2014-10-19 23:57:57', '1.22.83.150'),
(5, 1, '2014-10-20 00:01:15', '1.22.83.150'),
(6, 2, '2014-10-20 00:07:41', '1.22.83.150'),
(7, 2, '2014-10-20 00:58:01', '1.22.83.150'),
(8, 2, '2014-10-20 00:59:25', '1.22.83.150'),
(9, 2, '2014-10-20 01:03:55', '1.22.83.150'),
(10, 2, '2014-10-20 02:39:43', '1.22.81.148'),
(11, 3, '2014-10-20 03:13:34', '1.22.81.148'),
(12, 2, '2014-10-20 03:14:02', '1.22.81.148'),
(13, 2, '2014-10-20 03:34:36', '1.22.81.148'),
(14, 2, '2014-10-20 04:27:03', '1.22.81.148'),
(15, 2, '2014-10-20 22:45:06', '1.22.83.176'),
(16, 2, '2014-10-21 00:18:09', '1.22.83.176'),
(17, 2, '2014-10-21 00:31:33', '27.109.7.130'),
(18, 2, '2014-10-21 00:49:15', '1.22.83.176'),
(19, 2, '2014-10-21 02:41:31', '1.22.83.176'),
(20, 2, '2014-10-21 03:24:26', '1.22.83.176'),
(21, 2, '2014-10-21 04:56:50', '27.109.7.130'),
(22, 2, '2014-10-21 05:07:14', '1.22.83.176'),
(23, 2, '2014-10-21 05:07:15', '27.109.7.130'),
(24, 2, '2014-10-21 05:09:10', '27.109.7.130'),
(25, 2, '2014-10-21 05:10:42', '27.109.7.130'),
(26, 4, '2014-10-21 05:19:21', '27.109.7.130'),
(27, 2, '2014-10-21 08:10:45', '27.109.7.130'),
(28, 4, '2014-10-21 08:11:43', '27.109.7.130'),
(29, 2, '2014-10-21 22:49:29', '1.22.83.171'),
(30, 2, '2014-10-21 23:01:09', '27.109.7.130'),
(31, 2, '2014-10-21 23:01:32', '27.109.7.130'),
(32, 2, '2014-10-21 23:23:54', '27.109.7.130'),
(33, 2, '2014-10-22 00:41:09', '27.109.7.130'),
(34, 5, '2014-10-22 01:13:08', '1.22.83.171'),
(35, 5, '2014-10-22 01:15:53', '1.22.83.171'),
(36, 2, '2014-10-22 02:29:38', '27.109.7.130'),
(37, 5, '2014-10-22 02:44:04', '1.22.83.171'),
(38, 3, '2014-10-22 03:02:07', '27.109.7.130'),
(39, 2, '2014-10-22 03:40:00', '27.109.7.130'),
(40, 2, '2014-10-22 03:40:44', '27.109.7.130'),
(41, 2, '2014-10-22 03:46:27', '1.22.83.171'),
(42, 2, '2014-10-26 22:36:47', '1.22.80.241'),
(43, 5, '2014-10-26 22:47:13', '1.22.80.241'),
(44, 2, '2014-10-27 02:05:20', '27.109.7.130'),
(45, 2, '2014-10-27 04:09:10', '1.22.80.241'),
(46, 3, '2014-10-27 05:14:08', '27.109.7.130'),
(47, 7, '2014-10-27 06:17:26', '27.109.7.130'),
(48, 6, '2014-10-28 07:11:57', '41.203.64.130'),
(49, 2, '2014-10-29 00:59:13', '27.109.7.130'),
(50, 3, '2014-10-29 01:01:19', '27.109.7.130'),
(51, 2, '2014-10-29 01:34:39', '1.22.80.198'),
(52, 3, '2014-10-29 01:57:53', '27.109.7.130'),
(53, 3, '2014-10-29 02:11:52', '27.109.7.130'),
(54, 3, '2014-10-29 02:12:25', '27.109.7.130'),
(55, 5, '2014-10-29 02:47:30', '1.22.80.198'),
(56, 2, '2014-10-29 03:10:30', '1.22.80.198'),
(57, 3, '2014-10-29 03:39:05', '27.109.7.130'),
(58, 3, '2014-10-29 04:58:43', '1.22.80.198'),
(59, 2, '2014-10-29 05:00:10', '1.22.80.198'),
(60, 2, '2014-10-29 22:01:24', '1.22.82.216'),
(61, 5, '2014-10-29 22:03:41', '1.22.82.216'),
(62, 2, '2014-10-29 22:23:57', '1.22.82.216'),
(63, 3, '2014-10-30 00:04:20', '1.22.82.216'),
(64, 5, '2014-10-30 00:06:23', '1.22.82.216'),
(65, 4, '2014-10-30 00:18:36', '1.22.82.216'),
(66, 2, '2014-10-30 00:21:30', '1.22.82.216'),
(67, 3, '2014-10-30 00:53:44', '1.22.82.216'),
(68, 5, '2014-10-30 01:13:00', '1.22.82.216'),
(69, 5, '2014-10-30 01:15:12', '1.22.82.216'),
(70, 4, '2014-10-30 01:23:42', '1.22.82.216'),
(71, 2, '2014-10-30 01:36:27', '1.22.82.216'),
(72, 4, '2014-10-30 02:47:34', '1.22.82.216'),
(73, 2, '2014-10-30 02:48:02', '1.22.82.216'),
(74, 5, '2014-10-30 02:52:02', '1.22.82.216'),
(75, 3, '2014-10-30 03:03:24', '1.22.82.216'),
(76, 2, '2014-10-31 06:42:49', '127.0.0.1'),
(77, 2, '2014-10-31 07:49:18', '127.0.0.1'),
(78, 2, '2014-11-03 02:03:06', '127.0.0.1'),
(79, 2, '2014-11-03 02:03:37', '127.0.0.1'),
(80, 2, '2014-11-03 02:15:55', '127.0.0.1'),
(81, 2, '2014-11-03 02:16:25', '127.0.0.1'),
(82, 2, '2014-11-03 02:19:01', '127.0.0.1'),
(83, 2, '2014-11-03 02:19:13', '127.0.0.1'),
(84, 2, '2014-11-03 02:19:37', '127.0.0.1'),
(85, 2, '2014-11-03 02:22:23', '127.0.0.1'),
(86, 3, '2014-11-14 03:33:26', '127.0.0.1'),
(87, 3, '2014-11-26 03:59:47', '127.0.0.1'),
(88, 8, '2014-11-26 06:58:46', '127.0.0.1'),
(89, 3, '2014-12-11 23:04:53', '127.0.0.1');

-- --------------------------------------------------------

--
-- Table structure for table `user_notification`
--

CREATE TABLE IF NOT EXISTS `user_notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `user_alert` tinyint(1) NOT NULL,
  `add_fund` tinyint(1) NOT NULL,
  `project_alert` tinyint(1) NOT NULL,
  `comment_alert` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `user_notification`
--

INSERT INTO `user_notification` (`id`, `user_id`, `user_alert`, `add_fund`, `project_alert`, `comment_alert`) VALUES
(1, 1, 1, 1, 1, 1),
(2, 2, 1, 1, 1, 1),
(3, 3, 1, 1, 1, 1),
(4, 4, 1, 1, 1, 1),
(5, 5, 1, 1, 1, 1),
(6, 6, 1, 1, 1, 1),
(7, 7, 1, 1, 1, 1),
(8, 8, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_setting`
--

CREATE TABLE IF NOT EXISTS `user_setting` (
  `user_setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `login_with` varchar(255) DEFAULT NULL,
  `admin_activation` varchar(255) DEFAULT NULL,
  `email_varification` varchar(255) DEFAULT NULL,
  `auto_login` varchar(255) DEFAULT NULL,
  `notification_mail` varchar(255) DEFAULT NULL,
  `welcome_mail` varchar(255) DEFAULT NULL,
  `password_change_logout` varchar(255) DEFAULT NULL,
  `enable_openid` varchar(255) DEFAULT NULL,
  `user_switch_language` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_setting_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user_setting`
--

INSERT INTO `user_setting` (`user_setting_id`, `login_with`, `admin_activation`, `email_varification`, `auto_login`, `notification_mail`, `welcome_mail`, `password_change_logout`, `enable_openid`, `user_switch_language`) VALUES
(1, 'b', 'b', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `version_update`
--

CREATE TABLE IF NOT EXISTS `version_update` (
  `ver_id` int(11) NOT NULL AUTO_INCREMENT,
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `license` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `product_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`ver_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=41 ;

--
-- Dumping data for table `version_update`
--

INSERT INTO `version_update` (`ver_id`, `version`, `description`, `license`, `product_name`, `date`) VALUES
(3, '5.0.3', 'Occasionally, we may release small updates in the form of a patch to one\n or several source file. These source files are regular zip files that \ncontain updated files.<br><br>There are two methods for updating - \nthe easiest is the one-click update. If it doesn''t work, or you just \nprefer to be more hands-on, you can follow the manual update process. <br><br>If\n you are working with a source code ,Make a backup of everything in your\n site, including the database. This is extremely important so that you \ncan roll back to a running site no matter what happens during migration.<br><br>Although\n we usually produce upgrades in order to improve a functionality, there \nare risks involved—including the possibility that the upgrade will \nworsen the website.  ', 'kiwjw4tyn3nkcf!4zcrf', 'FUND001', '2014-02-27 00:00:00'),
(4, '5.0.4', 'ASDASDFGFSDGDDDG ASDFGFSDGDDDG ASDFGFSDGDDDG ASDFGFSDGDDDG     FGFSDGDDDGASDFGFSDGDDDG  ', 'ecmxfkpon9iefode58d4', 'FUND001', '2014-02-26 00:00:00'),
(5, '5.0.4', 'Occasionally, we may release small updates in the form of a patch to one\n or several source file. These source files are regular zip files that \ncontain updated files.<br><br>There are two methods for updating - \nthe easiest is the one-click update. If it doesn''t work, or you just \nprefer to be more hands-on, you can follow the manual update process. <br><br>If\n you are working with a source code ,Make a backup of everything in your\n site, including the database. This is extremely important so that you \ncan roll back to a running site no matter what happens during migration.<br><br>Although\n we usually produce upgrades in order to improve a functionality, there \nare risks involved—including the possibility that the upgrade will \nworsen the website.  ', 'i2n0t5nehgylyed!ewmc', 'FUND001', '2014-02-25 00:00:00'),
(6, '5.0.5', 'Occasionally, we may release small updates in the form of a patch to one or several source file. These source files are regular zip files that contain updated files.<br><br>There are two methods for updating - the easiest is the one-click update. If it doesn''t work, or you just prefer to be more hands-on, you can follow the manual update process. <br><br>If you are working with a source code ,Make a backup of everything in your site, including the database. This is extremely important so that you can roll back to a running site no matter what happens during migration.<br><br>Although we usually produce upgrades in order to improve a functionality, there are risks involved—including the possibility that the upgrade will worsen the website.  ', 'phttm9vquw!yfg05!344', 'FUND001', '2014-02-19 00:00:00'),
(7, '5.0.6', 'Occasionally, we may release small updates in the form of a patch to one or several source file. These source files are regular zip files that contain updated files.<br><br>There are two methods for updating - the easiest is the one-click update. If it doesn''t work, or you just prefer to be more hands-on, you can follow the manual update process. <br><br>If you are working with a source code ,Make a backup of everything in your site, including the database. This is extremely important so that you can roll back to a running site no matter what happens during migration.<br><br>Although we usually produce upgrades in order to improve a functionality, there are risks involved—including the possibility that the upgrade will worsen the website.  ', 'sfv4u02v@cjfd%2cxbip', 'FUND001', '2014-02-18 00:00:00'),
(8, '5.0.7', 'Occasionally, we may release small updates in the form of a patch to one or several source file. These source files are regular zip files that contain updated files.<br><br>There are two methods for updating - the easiest is the one-click update. If it doesn''t work, or you just prefer to be more hands-on, you can follow the manual update process. <br><br>If you are working with a source code ,Make a backup of everything in your site, including the database. This is extremely important so that you can roll back to a running site no matter what happens during migration.<br><br>Although we usually produce upgrades in order to improve a functionality, there are risks involved—including the possibility that the upgrade will worsen the website.  ', 'e8cua2@88b1xhbkjr8s4', 'FUND001', '2014-02-24 00:00:00'),
(9, '5.0.8', 'Occasionally, we may release small updates in the form of a patch to one or several source file. These source files are regular zip files that contain updated files.<br><br>There are two methods for updating - the easiest is the one-click update. If it doesn''t work, or you just prefer to be more hands-on, you can follow the manual update process. <br><br>If you are working with a source code ,Make a backup of everything in your site, including the database. This is extremely important so that you can roll back to a running site no matter what happens during migration.<br><br>Although we usually produce upgrades in order to improve a functionality, there are risks involved—including the possibility that the upgrade will worsen the website.  ', 'u@kr!dkm6jsa53pmjsxa', 'FUND001', '2014-02-25 00:00:00'),
(10, '5.1.0', '<div>ticket no.</div><div><a rel="nofollow noreferrer" href="https://www.google.com/url?q=https%3A%2F%2Ffundraising.zendesk.com%2Ftickets%2F890%3Fcol%3D25895847%26desc%3D1%26order%3Dcreated%26page%3D1&sa=D&sntz=1&usg=AFQjCNGFx3TzST_u-gIP73XLGpGTDuZGEg" dir="ltr" target="_blank">https://fundraising.zendesk.<wbr>com/tickets/890?col=25895847&<wbr>desc=1&order=created&page=1</a><br>\n</div><div><br></div><div>1.other feature tab is working i<a href="http://groupfund.me" target="_blank"></a>. <br>\n</div><div><strong><br></strong></div><div><a rel="nofollow noreferrer" href="https://www.google.com/url?q=https%3A%2F%2Ffundraising.zendesk.com%2Ftickets%2F889%3Fcol%3D25895847%26desc%3D1%26order%3Dcreated%26page%3D1&sa=D&sntz=1&usg=AFQjCNE2ik5cqH8NSOwrKiTu4545qaNVGQ" dir="ltr" target="_blank">https://fundraising.zendesk.<wbr>com/tickets/889?col=25895847&<wbr>desc=1&order=created&page=1</a><br>\n</div><div><br></div><div>2. In admin , user list should be allow to search by  username , email address , ip address - <strong></strong></div>\n<div><br></div><div><a rel="nofollow noreferrer" href="https://www.google.com/url?q=https%3A%2F%2Ffundraising.zendesk.com%2Ftickets%2F888%3Fcol%3D25895847%26desc%3D1%26order%3Dcreated%26page%3D1&sa=D&sntz=1&usg=AFQjCNEnoigg0BPiUbxaiYc4G6etqgx-IQ" dir="ltr" target="_blank">https://fundraising.zendesk.<wbr>com/tickets/888?col=25895847&<wbr>desc=1&order=created&page=1</a><br>\n</div><div><br></div>3. forgot password id working in indigogo.<br><br><div><br></div><div>4. admin - email template - correct.</div><div>5. term and condition in sign up page - correct.</div>\n<div>6. in email template - help center is link in help link by admin (paragraph can be change from editor.)</div><div><br></div>    ', 'k9ydy!r%v49wu4146qc%', 'FUND001', '2014-02-26 00:00:00'),
(11, '5.1.1', '<div>ticket no.</div><div><a rel="nofollow noreferrer" href="https://www.google.com/url?q=https%3A%2F%2Ffundraising.zendesk.com%2Ftickets%2F890%3Fcol%3D25895847%26desc%3D1%26order%3Dcreated%26page%3D1&sa=D&sntz=1&usg=AFQjCNGFx3TzST_u-gIP73XLGpGTDuZGEg" dir="ltr" target="_blank">https://fundraising.zendesk.<wbr>com/tickets/890?col=25895847&<wbr>desc=1&order=created&page=1</a><br>\n</div><div><br></div><div>1.other feature tab is working i. <br>\n</div><div><strong><br></strong></div><div><a rel="nofollow noreferrer" href="https://www.google.com/url?q=https%3A%2F%2Ffundraising.zendesk.com%2Ftickets%2F889%3Fcol%3D25895847%26desc%3D1%26order%3Dcreated%26page%3D1&sa=D&sntz=1&usg=AFQjCNE2ik5cqH8NSOwrKiTu4545qaNVGQ" dir="ltr" target="_blank">https://fundraising.zendesk.<wbr>com/tickets/889?col=25895847&<wbr>desc=1&order=created&page=1</a><br>\n</div><div><br></div><div>2. In admin , user list should be allow to search by  username , email address , ip address - <strong></strong></div>\n<div><br></div><div><a rel="nofollow noreferrer" href="https://www.google.com/url?q=https%3A%2F%2Ffundraising.zendesk.com%2Ftickets%2F888%3Fcol%3D25895847%26desc%3D1%26order%3Dcreated%26page%3D1&sa=D&sntz=1&usg=AFQjCNEnoigg0BPiUbxaiYc4G6etqgx-IQ" dir="ltr" target="_blank">https://fundraising.zendesk.<wbr>com/tickets/888?col=25895847&<wbr>desc=1&order=created&page=1</a><br>\n</div><div><br></div>3. forgot password id working in indigogo.<br><br><div><br></div><div>4. admin - email template - correct.</div><div>5. term and condition in sign up page - correct.</div>\n<div>6. in email template - help center is link in help link by admin (paragraph can be change from editor.)</div><div><br></div>  ', '7mltg5idngk9yff7tse1', 'FUND001', '2014-02-27 00:00:00'),
(12, '5.1.2', '<div>ticket no.</div><div><a rel="nofollow noreferrer" href="https://www.google.com/url?q=https%3A%2F%2Ffundraising.zendesk.com%2Ftickets%2F890%3Fcol%3D25895847%26desc%3D1%26order%3Dcreated%26page%3D1&sa=D&sntz=1&usg=AFQjCNGFx3TzST_u-gIP73XLGpGTDuZGEg" dir="ltr" target="_blank">https://fundraising.zendesk.<wbr>com/tickets/890?col=25895847&<wbr>desc=1&order=created&page=1</a><br></div><div><br></div><div>1.other feature tab is working i. <br></div><div><strong><br></strong></div><div><a rel="nofollow noreferrer" href="https://www.google.com/url?q=https%3A%2F%2Ffundraising.zendesk.com%2Ftickets%2F889%3Fcol%3D25895847%26desc%3D1%26order%3Dcreated%26page%3D1&sa=D&sntz=1&usg=AFQjCNE2ik5cqH8NSOwrKiTu4545qaNVGQ" dir="ltr" target="_blank">https://fundraising.zendesk.<wbr>com/tickets/889?col=25895847&<wbr>desc=1&order=created&page=1</a><br></div><div><br></div><div>2. In admin , user list should be allow to search by  username , email address , ip address - <strong></strong></div><div><br></div><div><a rel="nofollow noreferrer" href="https://www.google.com/url?q=https%3A%2F%2Ffundraising.zendesk.com%2Ftickets%2F888%3Fcol%3D25895847%26desc%3D1%26order%3Dcreated%26page%3D1&sa=D&sntz=1&usg=AFQjCNEnoigg0BPiUbxaiYc4G6etqgx-IQ" dir="ltr" target="_blank">https://fundraising.zendesk.<wbr>com/tickets/888?col=25895847&<wbr>desc=1&order=created&page=1</a><br></div><div><br></div>3. forgot password id working in indigogo.<br><br><div><br></div><div>4. admin - email template - correct.</div><div>5. term and condition in sign up page - correct.</div><div>6. in email template - help center is link in help link by admin (paragraph can be change from editor.)</div>  <div><br></div>  ', 'pjkxza7ghibb5ycr1zj2', 'FUND001', '2014-03-02 00:00:00'),
(13, '5.1.3', '<div>ticket no.</div><div><a rel="nofollow noreferrer" href="https://www.google.com/url?q=https%3A%2F%2Ffundraising.zendesk.com%2Ftickets%2F890%3Fcol%3D25895847%26desc%3D1%26order%3Dcreated%26page%3D1&sa=D&sntz=1&usg=AFQjCNGFx3TzST_u-gIP73XLGpGTDuZGEg" dir="ltr" target="_blank">https://fundraising.zendesk.<wbr>com/tickets/890?col=25895847&<wbr>desc=1&order=created&page=1</a><br></div><div><br></div><div>1.other feature tab is working i. <br></div><div><strong><br></strong></div><div><a rel="nofollow noreferrer" href="https://www.google.com/url?q=https%3A%2F%2Ffundraising.zendesk.com%2Ftickets%2F889%3Fcol%3D25895847%26desc%3D1%26order%3Dcreated%26page%3D1&sa=D&sntz=1&usg=AFQjCNE2ik5cqH8NSOwrKiTu4545qaNVGQ" dir="ltr" target="_blank">https://fundraising.zendesk.<wbr>com/tickets/889?col=25895847&<wbr>desc=1&order=created&page=1</a><br></div><div><br></div><div>2. In admin , user list should be allow to search by  username , email address , ip address - <strong></strong></div><div><br></div><div><a rel="nofollow noreferrer" href="https://www.google.com/url?q=https%3A%2F%2Ffundraising.zendesk.com%2Ftickets%2F888%3Fcol%3D25895847%26desc%3D1%26order%3Dcreated%26page%3D1&sa=D&sntz=1&usg=AFQjCNEnoigg0BPiUbxaiYc4G6etqgx-IQ" dir="ltr" target="_blank">https://fundraising.zendesk.<wbr>com/tickets/888?col=25895847&<wbr>desc=1&order=created&page=1</a><br></div><div><br></div>3. forgot password id working in indigogo.<br><br><div><br></div><div>4. admin - email template - correct.</div><div>5. term and condition in sign up page - correct.</div><div>6. in email template - help center is link in help link by admin (paragraph can be change from editor.)</div>  <div><br></div>  ', 'nqocox82p6elx6@2b83t', 'FUND001', '2014-03-03 00:00:00'),
(14, '5.1.4', '<div><div><strong>-bug ticket is attached.</strong></div><div><strong>-in version update (admin) - search is working and pagination also.</strong></div><div><strong>--Done – cronjob delete and listing</strong></div><p><strong> – done active/inactive category is working in frontside<br></strong></p><p><strong>-Done  grid have shorting functionality  - arrow is display in column</strong></p>  <br></div>  ', '2y4dmjm1!pgct06xu2p@', 'FUND001', '2014-03-03 00:00:00'),
(18, '5.1.7', 'Occasionally, we may release small updates in the form of a patch to one or several source file. These source files are regular zip files that contain updated files.<br><br>There are two methods for updating - the easiest is the one-click update. If it doesn''t work, or you just prefer to be more hands-on, you can follow the manual update process. <br><br>If you are working with a source code ,Make a backup of everything in your site, including the database. This is extremely important so that you can roll back to a running site no matter what happens during migration.<br><br>Although we usually produce upgrades in order to improve a functionality, there are risks involved—including the possibility that the upgrade will worsen the website.  ', 'vct75tuk08xx2g835b3e', 'FUND001', '2014-03-14 00:00:00'),
(17, '5.1.5', '<div><strong>-bug ticket is attached.</strong></div><div><strong>-in version update (admin) - search is working and pagination also.</strong></div><div><strong>--Done – cronjob delete and listing</strong></div><p><strong> – done active/inactive category is working in frontside<br></strong></p><p><strong>-Done  grid have shorting functionality  - arrow is display in column</strong></p>  <div><strong><br></strong></div>  ', 'os23yj444smx!h2iniaa', 'FUND001', '2014-03-03 00:00:00'),
(19, '5.1.8', 'Occasionally, we may release small updates in the form of a patch to one or several source file. These source files are regular zip files that contain updated files.<br><br>There are two methods for updating - the easiest is the one-click update. If it doesn''t work, or you just prefer to be more hands-on, you can follow the manual update process. <br><br>If you are working with a source code ,Make a backup of everything in your site, including the database. This is extremely important so that you can roll back to a running site no matter what happens during migration.<br><br>Although we usually produce upgrades in order to improve a functionality, there are risks involved—including the possibility that the upgrade will worsen the website.  ', 'i%6d17j68o0k@%djbe@%', 'FUND001', '2014-03-14 00:00:00'),
(20, '5.1.9', 'Occasionally, we may release small updates in the form of a patch to one or several source file. These source files are regular zip files that contain updated files.<br><br>There are two methods for updating - the easiest is the one-click update. If it doesn''t work, or you just prefer to be more hands-on, you can follow the manual update process. <br><br>If you are working with a source code ,Make a backup of everything in your site, including the database. This is extremely important so that you can roll back to a running site no matter what happens during migration.<br><br>Although we usually produce upgrades in order to improve a functionality, there are risks involved—including the possibility that the upgrade will worsen the website.  ', 't740e9xwsnw5@9gqv5vs', 'FUND001', '2014-03-15 00:00:00'),
(21, '5.1.10', 'Occasionally, we may release small updates in the form of a patch to one or several source file. These source files are regular zip files that contain updated files.<br><br>There are two methods for updating - the easiest is the one-click update. If it doesn''t work, or you just prefer to be more hands-on, you can follow the manual update process. <br><br>If you are working with a source code ,Make a backup of everything in your site, including the database. This is extremely important so that you can roll back to a running site no matter what happens during migration.<br><br>Although we usually produce upgrades in order to improve a functionality, there are risks involved—including the possibility that the upgrade will worsen the website.  ', 'znf3o3g5hvq0nq@a%3!j', 'FUND001', '2014-03-21 00:00:00'),
(22, '5.2.2', 'Occasionally, we may release small updates in the form of a patch to one or several source file. These source files are regular zip files that contain updated files.<br><br>There are two methods for updating - the easiest is the one-click update. If it doesn''t work, or you just prefer to be more hands-on, you can follow the manual update process. <br><br>If you are working with a source code ,Make a backup of everything in your site, including the database. This is extremely important so that you can roll back to a running site no matter what happens during migration.<br><br>Although we usually produce upgrades in order to improve a functionality, there are risks involved—including the possibility that the upgrade will worsen the website.  ', 'uzqiahqda@fx1gzxlsiz', 'FUND001', '2014-04-01 00:00:00'),
(23, '5.2.1', 'Occasionally, we may release small updates in the form of a patch to one or several source file. These source files are regular zip files that contain updated files.<br><br>There are two methods for updating - the easiest is the one-click update. If it doesn''t work, or you just prefer to be more hands-on, you can follow the manual update process. <br><br>If you are working with a source code ,Make a backup of everything in your site, including the database. This is extremely important so that you can roll back to a running site no matter what happens during migration.<br><br>Although we usually produce upgrades in order to improve a functionality, there are risks involved—including the possibility that the upgrade will worsen the website.  ', 'fels380zo445e366a1b7', 'FUND001', '2014-04-01 00:00:00'),
(24, '5.2.3', 'Occasionally, we may release small updates in the form of a patch to one or several source file. These source files are regular zip files that contain updated files.<br><br>There are two methods for updating - the easiest is the one-click update. If it doesn''t work, or you just prefer to be more hands-on, you can follow the manual update process. <br><br>If you are working with a source code ,Make a backup of everything in your site, including the database. This is extremely important so that you can roll back to a running site no matter what happens during migration.<br><br>Although we usually produce upgrades in order to improve a functionality, there are risks involved—including the possibility that the upgrade will worsen the website.  ', 'e6nxx8ddnvrma85pp%5s', 'FUND001', '2014-04-05 00:00:00'),
(25, '1.0.0.2', 'Admin Logo change on mouse hover effect.<br> Dashboard URL  ', 'qk%09r3y%e0ionhzn6lk', 'FUND001', '2014-08-27 00:00:00'),
(26, '1.0.0.3', 'Admin Logo change on mouse hover effect.<br> Dashboard URL  ', 'nf61yf5bnesujo2lte@h', 'FUND001', '2014-08-27 00:00:00'),
(27, '1.0.0.6', '/home/rocku3/Desktop/Fundrasing 29/admin_css<br>/home/rocku3/Desktop/Fundrasing 29/application<br>/home/rocku3/Desktop/Fundrasing 29/css<br>/home/rocku3/Desktop/Fundrasing 29/images  ', 'c@@58yb11vsd1%!aktku', 'FUND001', '2014-08-27 00:00:00'),
(28, '1.0.0.7', 'Dashboard Listing in admin side<br>Admin side logo hover effect as like on front side  ', '8onzhjttjz3htr0a6ajk', 'FUND001', '2014-08-27 00:00:00'),
(29, '1.0.0.8', 'Dashboard Listing on admin side<br>Logo Hover effect in admin side as front side  ', '345wx@03vznidws1z7ox', 'FUND001', '2014-08-27 00:00:00'),
(30, '1.0.0.10', 'Fundraising contact us page  ', 'd0l0wi8@oytlekgecx28', 'FUND001', '2014-08-27 00:00:00'),
(31, '2.0.0.11', 'From this update you can find home controller update   ', '9xew!7xhqrfzg48sabgg', 'FUND001', '2014-09-12 00:00:00'),
(32, '2.0.0.12', 'From this version you can find following update :<br><br>1) Help document for each of the social site settings and payment gatway from their setting page in admin panel by click on help icon on left side corner after page title<br>2) When admin update information from the site setting in admin panel then default no image will be reproduce based on the updated site logo.  ', '4kx17cyr09z!xk1u@04!', 'FUND001', '2014-09-12 00:00:00'),
(33, '2.0.0.13', 'From this version you can find following updates :<br><br>1) Link In texts are change according to its functional operation. (#1016)<br>2) If current logged in user do not have it profile picture then, it should show default image in place of broken image shown. (#1015)<br>3) In admin user listing password column has been removed now. (#1012)<br>4) On sign up step2 label of textarea and buttons are changed now. (#1009)<br>5) Contact us page now show in one page, means its scroll has been removed now. (#994)  ', 'smwyqfnu3vo986iui07w', 'FUND001', '2014-09-13 00:00:00'),
(34, '2.0.0.14', 'You can find following updates from this version :<br><br>1) Change site name in place of indiegogo at create step 1<br>2) Taggin tool (1010)<br>3) Admin should able to see pending project (1023)<br>4) Admin auto logout when front side logout<br>5) Stripe fixed failure     ', '!q6tws52mu3g%zfdul2q', 'FUND001', '2014-09-15 00:00:00'),
(35, '2.0.0.15', 'Help document link for all social sign up page has been changed to global link for all the fundraising script   ', 'cfhr3mas!ve4p3j8ka9g', 'FUND001', '2014-09-18 00:00:00'),
(36, '2.0.0.16', 'From this update user can not get error for site setting page update. User will get new default image with uploaded logo   ', 'd%a9e6tp%38hlbfru6eh', 'FUND001', '2014-09-18 00:00:00'),
(37, '2.0.0.17', 'New Updates on Help Doc icons   ', '3xzcbmztgrwruhqxrgan', 'FUND001', '2014-09-23 00:00:00'),
(38, '2.0.0.18', 'New Updates with Help Doc icon   ', 'uy3bxggzyp1v25ulhm9j', 'FUND001', '2014-09-23 00:00:00'),
(39, '2.0.0.19', 'New Updates with Help Doc icon    ', '23qqfehuwxty9x5kmxym', 'FUND001', '2014-09-23 00:00:00'),
(40, '2.0.0.20', 'Help icons update with tooltips   ', 'amhio%9ynr8glhv58!rb', 'FUND001', '2014-09-23 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `video_gallery`
--

CREATE TABLE IF NOT EXISTS `video_gallery` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `image` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `media_video_desc` text COLLATE utf8_unicode_ci,
  `media_video_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `media_video_title` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `project_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=36 ;

--
-- Dumping data for table `video_gallery`
--

INSERT INTO `video_gallery` (`id`, `image`, `media_video_desc`, `media_video_name`, `media_video_title`, `project_id`) VALUES
(1, 'http://i1.ytimg.com/vi/lvtfD_rJ2hE/hqdefault.jpg', ' At the MIT Media Lab, the Tangible Media Group believes the future of computing is tactile. Unveiled today, the inFORM is MIT''s new scrying pool for imagining the interfaces of tomorrow. Almost like a table of living clay, the inFORM is a surface that three-dimensionally changes shape, allowing users to not only interact with digital content in meatspace, but even hold hands with a person hundreds of miles away. And that''s only the beginning.', 'Amazing Technology Invented By MIT - Tangible Media ', 'http://www.youtube.com/watch?v=lvtfD_rJ2hE', 5),
(2, 'http://i1.ytimg.com/vi/lvtfD_rJ2hE/hqdefault.jpg', ' At the MIT Media Lab, the Tangible Media Group believes the future of computing is tactile. Unveiled today, the inFORM is MIT''s new scrying pool for imagining the interfaces of tomorrow. Almost like a table of living clay, the inFORM is a surface that three-dimensionally changes shape, allowing users to not only interact with digital content in meatspace, but even hold hands with a person hundreds of miles away. And that''s only the beginning.', 'Amazing Technology Invented By MIT - Tangible Media ', 'http://www.youtube.com/watch?v=lvtfD_rJ2hE', 5),
(3, 'http://i1.ytimg.com/vi/RmaGTclE6Kw/hqdefault.jpg', ' How should political parties be funded.... by donations as now, or general taxation (which means you fund a party that is so morally and politically bankrupt - like Labour', 'political parties', 'http://www.youtube.com/watch?v=RmaGTclE6Kw', 4),
(4, 'http://i1.ytimg.com/vi/RmaGTclE6Kw/hqdefault.jpg', ' How should political parties be funded.... by donations as now, or general taxation (which means you fund a party that is so morally and politically bankrupt - like Labour', 'political parties', 'http://www.youtube.com/watch?v=RmaGTclE6Kw', 4),
(11, 'http://i1.ytimg.com/vi/QdboHXncX1M/hqdefault.jpg', ' We buy junk vehicles for quick cash! We give you an honest quote over the phone and we will stick to that price if your vehicle description was accurate. We provide FREE TOWING for any cars we buy. We service the Edmonton area and also all surrounding areas and communities. Give us a call today for free junk car removal service.\n', 'donation for accident', 'https://www.youtube.com/watch?v=QdboHXncX1M', 2),
(10, 'http://i1.ytimg.com/vi/QdboHXncX1M/hqdefault.jpg', ' We buy junk vehicles for quick cash! We give you an honest quote over the phone and we will stick to that price if your vehicle description was accurate. We provide FREE TOWING for any cars we buy. We service the Edmonton area and also all surrounding areas and communities. Give us a call today for free junk car removal service.\n', 'donation for accident', 'https://www.youtube.com/watch?v=QdboHXncX1M', 2),
(7, 'http://i1.ytimg.com/vi/rQ3qzm0c7FA/hqdefault.jpg', ' The first Community Season 6 trailer, which premiered at the show’s Comic-Con panel earlier this year. In typical Community style, it’s cheeky, over the top, and chock full of pop culture references.', 'Amazing Technology Invented By MIT - Tangible Media', 'http://www.youtube.com/watch?v=rQ3qzm0c7FA', 3),
(8, 'http://i1.ytimg.com/vi/7OQMomEgliw/hqdefault.jpg', 'The idea of community may simply come down to supporting and interacting positively with other individuals who share a vested interest.', 'Political ', 'http://www.youtube.com/watch?v=7OQMomEgliw', 1),
(12, 'http://i1.ytimg.com/vi/9e8RPILkIbU/hqdefault.jpg', ' Isha Vidhya is a public school charity, ngo, non-profit organization aims to transform the fate of poor rural children by providing affordable, high quality primary school education to villages across South India''s Tamil Nadu state. Over the coming years, Isha Vidhya will set up 206 schools designed specifically to create confident, English-speaking, computer literate children who are on par with students from India''s best urban schools.', 'donation Campaig', 'https://www.youtube.com/watch?v=9e8RPILkIbU', 6),
(13, 'http://i1.ytimg.com/vi/9e8RPILkIbU/hqdefault.jpg', ' Isha Vidhya is a public school charity, ngo, non-profit organization aims to transform the fate of poor rural children by providing affordable, high quality primary school education to villages across South India''s Tamil Nadu state. Over the coming years, Isha Vidhya will set up 206 schools designed specifically to create confident, English-speaking, computer literate children who are on par with students from India''s best urban schools.', 'donation Campaig', 'https://www.youtube.com/watch?v=9e8RPILkIbU', 6),
(14, 'http://i1.ytimg.com/vi/yMXeGL1lyxs/hqdefault.jpg', 'instrument donation and sponsorship drive being led by Music Basti, to support our project "Re-Sound", working with partner NGOs in Delhi, across Mehrauli, Okhla, Kapashera, Papankalan and Mahipalpur with over 100 children and youth.  ', 'Donate Instruments. ', 'https://www.youtube.com/watch?v=yMXeGL1lyxs', 8),
(16, 'http://i1.ytimg.com/vi/75HjFi4_r10/hqdefault.jpg', ' We couldn’t have filmed GameLoading without the support of our original Kickstarter backers and now we’re turning to Kickstarter again to raise funds to put the finishing touches on the film. All original Kickstarter backers who back us a second time get a special Double Backer reward. ', 'successful ', 'https://www.youtube.com/watch?v=75HjFi4_r10', 9),
(17, 'http://i1.ytimg.com/vi/75HjFi4_r10/hqdefault.jpg', ' We couldn’t have filmed GameLoading without the support of our original Kickstarter backers and now we’re turning to Kickstarter again to raise funds to put the finishing touches on the film. All original Kickstarter backers who back us a second time get a special Double Backer reward. ', 'successful ', 'https://www.youtube.com/watch?v=75HjFi4_r10', 9),
(18, 'http://i1.ytimg.com/vi/pmZV36XgXaI/hqdefault.jpg', ' ach music segment in Video Games Live is personally arranged and orchestrated by the original composers and also uses input from the actual game designers & producers and game developers & publishers.', ' parties', 'https://www.youtube.com/watch?v=pmZV36XgXaI', 10),
(19, 'http://i1.ytimg.com/vi/pmZV36XgXaI/hqdefault.jpg', ' ach music segment in Video Games Live is personally arranged and orchestrated by the original composers and also uses input from the actual game designers & producers and game developers & publishers.', ' parties', 'https://www.youtube.com/watch?v=pmZV36XgXaI', 10),
(20, 'http://i1.ytimg.com/vi/HuKx2a5HkIM/hqdefault.jpg', ' This animated video explains the transplant waiting list, how someone becomes a donor, the process of matching organs, and signing up to share the gift of life.', 'Donation and Transplantation', 'https://www.youtube.com/watch?v=HuKx2a5HkIM', 11),
(21, 'http://i1.ytimg.com/vi/HuKx2a5HkIM/hqdefault.jpg', ' This animated video explains the transplant waiting list, how someone becomes a donor, the process of matching organs, and signing up to share the gift of life.', 'Donation and Transplantation', 'https://www.youtube.com/watch?v=HuKx2a5HkIM', 11),
(22, 'http://i1.ytimg.com/vi/Af0gk_kiGac/hqdefault.jpg', ' Connect Carvey to your computer via USB, click “Carve”, and Carvey does the rest.  Sit back and watch as Carvey cuts or engraves your design. Carvey enables you to create real objects, with the same level of quality', 'technology', 'https://www.youtube.com/watch?v=Af0gk_kiGac&list=PLcvwepZEsuCsGLekHb9B6z_y-MS9MyxhM', 13),
(23, 'http://i1.ytimg.com/vi/Af0gk_kiGac/hqdefault.jpg', ' Connect Carvey to your computer via USB, click “Carve”, and Carvey does the rest.  Sit back and watch as Carvey cuts or engraves your design. Carvey enables you to create real objects, with the same level of quality', 'technology', 'https://www.youtube.com/watch?v=Af0gk_kiGac&list=PLcvwepZEsuCsGLekHb9B6z_y-MS9MyxhM', 13),
(24, 'http://i1.ytimg.com/vi/2JyUYkvM6cA/hqdefault.jpg', ' NSS Blood Donation Camp is an initiative by NSS BITS-Pilani, Pilani Campus, which in collaboration with Indian Red Cross Society, New Delhi will be organizing its 33rd Blood Donation Camp on 1-2 Febraury,2014.', 'successful', 'https://www.youtube.com/watch?v=2JyUYkvM6cA', 14),
(25, 'http://i1.ytimg.com/vi/2JyUYkvM6cA/hqdefault.jpg', ' NSS Blood Donation Camp is an initiative by NSS BITS-Pilani, Pilani Campus, which in collaboration with Indian Red Cross Society, New Delhi will be organizing its 33rd Blood Donation Camp on 1-2 Febraury,2014.', 'successful', 'https://www.youtube.com/watch?v=2JyUYkvM6cA', 14),
(26, 'http://i1.ytimg.com/vi/b7Z7oGRvDE0/hqdefault.jpg', 'When just one component of your blood is collected for transfusion, the collection process is known as "pheresis," or "automation." This video describes the automation process involved in Double Red Cell donation and Platelet Pheresis.', 'Blood Donation ', 'https://www.youtube.com/watch?v=b7Z7oGRvDE0&list=PLxQ6RaReV9ZVH8wvBBku4wmP63VFBEjpG', 15),
(27, 'http://i1.ytimg.com/vi/b7Z7oGRvDE0/hqdefault.jpg', 'When just one component of your blood is collected for transfusion, the collection process is known as "pheresis," or "automation." This video describes the automation process involved in Double Red Cell donation and Platelet Pheresis.', 'Blood Donation ', 'https://www.youtube.com/watch?v=b7Z7oGRvDE0&list=PLxQ6RaReV9ZVH8wvBBku4wmP63VFBEjpG', 15),
(28, 'http://i1.ytimg.com/vi/p_FTdg9IeD4/hqdefault.jpg', ' There are a lot of cases out there for the iPhone, however the cases that offer protection are often very bulky and heavy. Our new product Rhino Shield Crash Guard is about to change that. Rhino Shield Crash Guard is unique, as it offers extreme protection without any bulk', 'successful', 'https://www.youtube.com/watch?v=p_FTdg9IeD4', 19),
(29, 'http://i1.ytimg.com/vi/p_FTdg9IeD4/hqdefault.jpg', ' There are a lot of cases out there for the iPhone, however the cases that offer protection are often very bulky and heavy. Our new product Rhino Shield Crash Guard is about to change that. Rhino Shield Crash Guard is unique, as it offers extreme protection without any bulk', 'successful', 'https://www.youtube.com/watch?v=p_FTdg9IeD4', 19),
(30, 'http://i1.ytimg.com/vi/p_FTdg9IeD4/hqdefault.jpg', ' There are a lot of cases out there for the iPhone, however the cases that offer protection are often very bulky and heavy. Our new product Rhino Shield Crash Guard is about to change that. Rhino Shield Crash Guard is unique, as it offers extreme protection without any bulk', 'successful', 'https://www.youtube.com/watch?v=p_FTdg9IeD4', 19),
(31, 'http://i1.ytimg.com/vi/IoVKnK2EmxA/hqdefault.jpg', ' Supported mainly by revenue from television rights and corporate sponsorships, the USOC funds 1,500 winter and summer athletes, Blackmun said, but the goal is to support an additional 1,000, funding every member of a national team.', 'sports', 'https://www.youtube.com/watch?v=IoVKnK2EmxA', 20),
(32, 'http://i1.ytimg.com/vi/IoVKnK2EmxA/hqdefault.jpg', ' Supported mainly by revenue from television rights and corporate sponsorships, the USOC funds 1,500 winter and summer athletes, Blackmun said, but the goal is to support an additional 1,000, funding every member of a national team.', 'sports', 'https://www.youtube.com/watch?v=IoVKnK2EmxA', 20),
(33, 'http://i1.ytimg.com/vi/IoVKnK2EmxA/hqdefault.jpg', ' Supported mainly by revenue from television rights and corporate sponsorships, the USOC funds 1,500 winter and summer athletes, Blackmun said, but the goal is to support an additional 1,000, funding every member of a national team.', 'sports', 'https://www.youtube.com/watch?v=IoVKnK2EmxA', 20),
(34, 'http://i1.ytimg.com/vi/8aghzpO_UZE/hqdefault.jpg', ' 3D printers offer product developers the ability to print parts and assemblies made of several materials with different mechanical and physical properties in a single build process. Advanced 3D printing technologies yield models that can serve as product prototypes.', ' product developers ', 'https://www.youtube.com/watch?v=8aghzpO_UZE', 21),
(35, 'http://i1.ytimg.com/vi/8aghzpO_UZE/hqdefault.jpg', ' 3D printers offer product developers the ability to print parts and assemblies made of several materials with different mechanical and physical properties in a single build process. Advanced 3D printing technologies yield models that can serve as product prototypes.', ' product developers ', 'https://www.youtube.com/watch?v=8aghzpO_UZE', 21);

-- --------------------------------------------------------

--
-- Table structure for table `wallet`
--

CREATE TABLE IF NOT EXISTS `wallet` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `debit` varchar(200) DEFAULT NULL,
  `credit` varchar(200) DEFAULT NULL,
  `user_id` int(50) NOT NULL,
  `status` varchar(100) DEFAULT NULL,
  `detail` text,
  `admin_status` varchar(100) DEFAULT NULL,
  `wallet_date` datetime NOT NULL,
  `wallet_transaction_id` varchar(255) DEFAULT NULL,
  `wallet_payee_email` varchar(255) DEFAULT NULL,
  `wallet_ip` varchar(255) DEFAULT NULL,
  `gateway_id` int(10) NOT NULL,
  `project_id` int(100) NOT NULL,
  `donate_status` tinyint(1) NOT NULL,
  `reason` longtext,
  `wallet_anonymous` tinyint(1) NOT NULL,
  `donor_id` int(10) DEFAULT NULL,
  `total_donation` decimal(10,2) NOT NULL DEFAULT '0.00',
  `admin_fees` decimal(10,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `wallet_bank`
--

CREATE TABLE IF NOT EXISTS `wallet_bank` (
  `bank_id` int(11) NOT NULL AUTO_INCREMENT,
  `withdraw_id` int(11) NOT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_branch` varchar(255) DEFAULT NULL,
  `bank_ifsc_code` varchar(255) DEFAULT NULL,
  `bank_address` varchar(255) DEFAULT NULL,
  `bank_city` varchar(255) DEFAULT NULL,
  `bank_state` varchar(255) DEFAULT NULL,
  `bank_country` varchar(255) DEFAULT NULL,
  `bank_zipcode` varchar(255) DEFAULT NULL,
  `bank_unique_id` varchar(255) DEFAULT NULL,
  `bank_account_holder_name` varchar(255) DEFAULT NULL,
  `bank_account_number` varchar(255) DEFAULT NULL,
  `bank_withdraw_type` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`bank_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `wallet_setting`
--

CREATE TABLE IF NOT EXISTS `wallet_setting` (
  `wallet_id` int(10) NOT NULL AUTO_INCREMENT,
  `wallet_add_fees` double(10,2) NOT NULL,
  `wallet_enable` int(10) NOT NULL,
  `wallet_donation_fees` double(10,2) NOT NULL,
  `wallet_minimum_amount` double(10,2) NOT NULL,
  `wallet_payment_type` tinyint(1) NOT NULL COMMENT 'instant = 0 ,preapprove =1',
  `direct_donation_option` int(1) NOT NULL,
  `add_whole_amount` int(1) NOT NULL,
  `add_remain_amount` int(1) NOT NULL,
  PRIMARY KEY (`wallet_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `wallet_setting`
--

INSERT INTO `wallet_setting` (`wallet_id`, `wallet_add_fees`, `wallet_enable`, `wallet_donation_fees`, `wallet_minimum_amount`, `wallet_payment_type`, `direct_donation_option`, `add_whole_amount`, `add_remain_amount`) VALUES
(1, 0.00, 0, 5.00, 15.00, 0, 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wallet_withdraw`
--

CREATE TABLE IF NOT EXISTS `wallet_withdraw` (
  `withdraw_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `withdraw_date` datetime NOT NULL,
  `withdraw_ip` varchar(255) DEFAULT NULL,
  `withdraw_method` varchar(50) DEFAULT NULL,
  `withdraw_amount` double(10,2) NOT NULL,
  `withdraw_status` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`withdraw_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `wallet_withdraw_gateway`
--

CREATE TABLE IF NOT EXISTS `wallet_withdraw_gateway` (
  `gateway_withdraw_id` int(100) NOT NULL AUTO_INCREMENT,
  `withdraw_id` int(100) NOT NULL,
  `gateway_name` varchar(255) DEFAULT NULL,
  `gateway_account` varchar(255) DEFAULT NULL,
  `gateway_city` varchar(255) DEFAULT NULL,
  `gateway_state` varchar(255) DEFAULT NULL,
  `gateway_country` varchar(255) DEFAULT NULL,
  `gateway_zip` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`gateway_withdraw_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `wepay`
--

CREATE TABLE IF NOT EXISTS `wepay` (
  `wepay_id` int(10) NOT NULL AUTO_INCREMENT,
  `wepay_email` varchar(255) NOT NULL,
  `wepay_client_id` int(200) NOT NULL,
  `wepay_client_secret` varchar(255) NOT NULL,
  `wepay_clinet_access_token` varchar(255) NOT NULL,
  `wepay_status` int(10) NOT NULL DEFAULT '0',
  `wepay_gateway_status` int(10) NOT NULL DEFAULT '0',
  `wepay_transaction_fees` double(10,2) NOT NULL,
  `wepay_account_id` int(100) NOT NULL,
  PRIMARY KEY (`wepay_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `wepay`
--

INSERT INTO `wepay` (`wepay_id`, `wepay_email`, `wepay_client_id`, `wepay_client_secret`, `wepay_clinet_access_token`, `wepay_status`, `wepay_gateway_status`, `wepay_transaction_fees`, `wepay_account_id`) VALUES
(1, 'rockersinfo@gmail.com', 90089, '47f12b65c8', 'ef10b6d7b88934ad3cec97c549ab6e040d20f098409eda943d290e4edc9f4895', 0, 1, 1.40, 19064);

-- --------------------------------------------------------

--
-- Table structure for table `word_detecter_setting`
--

CREATE TABLE IF NOT EXISTS `word_detecter_setting` (
  `word_detecter_setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `enable_word_detecter` varchar(255) DEFAULT NULL,
  `words` text,
  PRIMARY KEY (`word_detecter_setting_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `word_detecter_setting`
--

INSERT INTO `word_detecter_setting` (`word_detecter_setting_id`, `enable_word_detecter`, `words`) VALUES
(1, 'yes', 'bomb');

-- --------------------------------------------------------

--
-- Table structure for table `yahoo_setting`
--

CREATE TABLE IF NOT EXISTS `yahoo_setting` (
  `yahoo_setting_id` int(50) NOT NULL AUTO_INCREMENT,
  `consumer_key` varchar(255) DEFAULT NULL,
  `consumer_secret` varchar(255) DEFAULT NULL,
  `yahoo_enable` int(50) NOT NULL DEFAULT '1',
  PRIMARY KEY (`yahoo_setting_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `yahoo_setting`
--

INSERT INTO `yahoo_setting` (`yahoo_setting_id`, `consumer_key`, `consumer_secret`, `yahoo_enable`) VALUES
(1, 'dj0yJmk9eVZYM01idG1TV3hPJmQ9WVdrOVJ6a3dVWGxNTTJVbWNHbzlOekV4TnpnNU9EWXkmcz1jb25zdW1lcnNlY3JldCZ4PTQx', '411354646419bbb5324081045a66459710285f30', 0);

-- --------------------------------------------------------

--
-- Table structure for table `youtube_setting`
--

CREATE TABLE IF NOT EXISTS `youtube_setting` (
  `youtube_setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `youtube_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `youtube_enable` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`youtube_setting_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `youtube_setting`
--

INSERT INTO `youtube_setting` (`youtube_setting_id`, `youtube_link`, `youtube_enable`) VALUES
(1, 'https://www.youtube.com', '1');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
