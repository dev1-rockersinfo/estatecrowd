<?php
$taxonomy_setting = taxonomy_setting();
//instead of project or campaign replace this name
$project = $taxonomy_setting['project_name'];
$funds = $taxonomy_setting['funds'];
$comments = $taxonomy_setting['comments'];
$comment = $taxonomy_setting['comments'];
$follower = $taxonomy_setting['follower'];
$project_name = $taxonomy_setting['project_name'];
$site_setting = site_setting();
$site_name = $site_setting['site_name'];
$funds_plural = $taxonomy_setting['funds_plural'];
$investor = $taxonomy_setting['investor'];

$lang["Home"] = "Home";
//Header 
define("HOME", "Home"); //rightmenu(7)
define("ABOUT", "About : ");
define("PROJECTS", ''.$project.'s');
define("BLOG", "Blog");
define("RAISED", "Raised");
define("LOG_IN", "Log in");
define("SIGN_UP", "Sign Up");
define("SIGN_OUT", "Sign Out");
define("BROWSE", "Browse");
define("YOUTUBE_CREATORS", "YouTube Creators");
define("CREATE", "Create");
define("CREATE_A_CAMPAIGN", 'Create a '.$project.'');
define("NEW_COMPAIGN", 'New '.$project.'');
define("COMPAIGN", $project);
define("WHAT_IS", "What is");
define("LEARN", "Learn");
define("DASHBOARD", "Dashboard");
define("MY_PROJECTS", 'My '.$project.'s');
define("START_YOUR_PROJECT", 'Start your '.$project.'');
define("TYPE_AND_ENTER", "Type and enter.");
define("HI", "Hi");
define("TO_YOUR_ACCOUNT", "To your account");
define("JOIN_WITH_US", "Join with us");
//Footer
define("ABOUT_US", "About Us"); //rightmenu (8)
define("TERMS_AND_CONDITIONS", "Terms and Conditions"); //rightmenu(9)
define("PRIVACY_POLICY", "Privacy Policy");
define("CAREERS", "Careers");
define("PRESS", "Press");
define("WHY", "Why");
define("FEATURES", "Features"); //RIGHTMENU (14)
define("FEES", "Fees"); //RIGHTMENU (15)
define("FAQ", "FAQ"); //RIGHTMENU(16)
define("HOW_TUBESTART_WORKS", "How ".$site_name." works"); //(17)
define("HOW", "How");
define("WORKS", "Works");
define("HELP", "Help");
define("CONTACT_US", "Contact Us");
define("ALL_RIGHT_RESERVED", " All Rights Reserved");
define("WEEKLY_NEWSLETTER", "Weekly newsletter");
define("LETS_SOCIALIZE", "Let's socialize");
define("GET_AWESOME_NEWS", "Get awesome news delivered to your inbox each week");
define("FACEBOOK", "Facebook");
define("TWITTER", "Twitter");
define("GOOGLE_PLUS", "Google Plus");
define("LINKEDIN", "Linkedin");
define("YOUTUBE", "Youtube");
define("ENTER_YOUR_EMAIL", "Enter your e-mail"); //10
define("ENTER_VALID_EMAIL", "Enter valid email"); //10,13,30
define("COPYRIGHT", "� 2013"); //152
define("ROCKERS_TECHNOLOGIES", "ROCKERS TECHNOLOGIES"); //158
define("DESIGNED_BY", "Designed By"); //158
//content
define("DAYS_LEFT", "Days Left");
define("DAYS_TO_GO", "Days To Go");
define("HOURS_LEFT", "Hours Left");
define("MINUTES_LEFT", "Minutes Left");
define("SECONDS_LEFT", "Seconds Left");
define("LATEST_PROJECT", 'Latest '.$project.'s'); //131
define("VIEW_CAMPAIGN", 'View '.$project.''); //104
//Search
define("SEARCH_BY", "Search By");
define("SEARCH", "Search");
define("PLEASE_ENTER_SEARCH_KEYWORD", "Please enter search keyword");
define("ALL_PROJECTS", 'All '.$project.'s');
define("FEATURED", "Featured");
define("ROUNDING_OFF", "Rounding Off");
define("POPULAR", "Popular");
define("LATEST_COMPAIGNS", 'Latest '.$project.'s');
define("NEAR_ME", "Near Me");
define("SUCCESS_STORY", "Success Stories");
define("MOST_FUNDED", "Most Funded");
define("ADVANCED_SEARCH", "Advanced Search");
define("KEYWORDS", "Keywords");
define("TITLE", "Title");
define("CATEGORY", "Category");
define("CITY", "City");
define("COUNTRY", "Country");
define("GOAL_SIZE", "Goal Size");
define("STATUS", "Status");
define("GOAL_TYPE", "Goal Type");
define("CELEBRATIONS_AND_SPECIAL_EVENTS", 'Celebrations & special events test education '.$funds.'');
define("CATEGORIES", "Categories");
define("SELECT_CATEGORY", "Select Category");
define("SELECT_COUNTRY", "Select Country");
define('SIGNUP', 'Sign Up');
define('NEW_TO', 'New To ');
define('NAME', 'Full Name');
define('EMAIL_ADDRESS', 'Email');
define('RETYPE_EMAIL', 'Retype Email');
define('PASSWORD', 'Password');
define('RETYPE_PASSWORD', 'Retype Password');
define('DISCOVER_NEW_PROJECTS_WITH_WEEKLY_NEWSLETTER_PLEASE_ACCEPT_TERMS_CONDITION_TO_ENJOY_YOU_AGREE_TO_OUR', 'By clicking Sign Up for %s, you agree to our ');
define('NEW_PROJECTS_WITH_WEEKLY_NEWSLETTER', "I'd like the weekly %s newsletter.");
define('TERMS_OF_USE', 'Terms Of Use');
define('LOGIN', 'Log In');
define('SUCCESS', 'Success');
define('WE_WILL_NEVER_POST_ANYTHING_WITHOUT_YOUR_PERMISSION', 'We will never post anything without your permission.');
define('EXISTS_ACCOUNT_ASSOCIATE_WITHMAIL', 'There is an existing account associated with this email.');
define('YOU_HAVE_SIGNED_UP_SUCCESSFULLY', 'You have signed up successfully. Your account details sent to your email address.');
define('PLEASE_LOGIN_TO_CONTINUE', 'Please Login to continue');
define('LOG_IN_TO_YOUR_ACCOUNT', 'Log In to your account');
define('DONT_HAVE_AN_ACCOUNT', "Don't have an account?");
define('EMAIL_LOGIN', 'Email Log In');
define('LOGIN_WITH_AN_EMAIL_ADDRESS', 'Log In With An Email Address');
define('LOGIN_WITH_A_SOCIAL_NETWORK', 'Log In with a social network');
define('LOGIN_WITH_FACEBOOK_TWITTER_OR_LINKEDIN', 'Log In with Facebook, Twitter or LinkedIn');
define('SIGNUP_WITH_A_SOCIAL_NETWORK', 'Sign up with a social network');
define('QUICKLY_CREATE_AN_ACCOUNT', 'Quickly create an account');
define('SIGNUP_MANUALLY', 'Sign up manually');
define('CREATE_AN_ACCOUNG_USING_EMAIL', 'Create an account using email');
define('REMEMBER_ME', 'Remember Me');
define('I_FORGOT_MY_PASSWORD', 'Forgot Password?');
define('SOCIAL_LOG_IN', 'Social log In');
define('EMAIL_ADDRESS_OR_PASSWORD_ARE_INVALID', 'Email Address or Password is invalid.');
define('YOUR_EMAIL_ADDRESS_IS_SUSPENDED', 'Your email address is suspended by admin.');
define('YOUR_ACCOUNT_IS_NOT_ACTIVEPLEASE_CHECK_YOUR_INBOX_AND_CLICK_ON_ACTIVATION_LINK', 'Your account is not active. Please check your inbox and click on activation link.');
define('YOU_ARE_SIGNED_OUT_SUCCESSFULLY', 'You have signed out successfully.');
define('RESEND_VARIFICATION_LINK', 'Resend verification link.');
define('SEND_VARIFICATION', 'Send Verification');
define('CLICK_HERE', 'Click Here');
define('RESEND', 'Resend');
define('PLEASE_ENTER_EMAIL_TO_SEND_VARIFICATION_LINK', 'Please enter email to send verification link');
define('PLEASE_ENTER_EMAIL_ADDRESS_FOR_VARIFICATION_LINK', 'Please enter email address for verification link');
define('EMAIL_ADDRESS_NOT_FOUND', 'Email Address Not Found.');
define('YOUR_ACCOUNT_IS_ALREADY_ACTIVE', 'Your account is already active.');
define('FORGET_PASSWORD', 'Forgot your password?');
define('TELL_US_THE_EMAIL_YOU_USED_TO_SIGNUP_WELL_GET_YOU_LOGGED_IN', "Tell us the email you used to sign up and we'll get you logged in.");
define('RESET_MY_PASSWORD', 'Reset');
define('CREATE_A', 'Create a');
define('ACCOUNT', 'Account');
define('ALREADY_HAVE_AN_ACCOUNT', 'Already have an account?');
define('MAIL_SENT_SUCCESSFULLY', 'Mail sent succesfully');
define('BACK_TO_LOGIN', 'Back To Log In');
define('PLEASE_ENTER_EMAIL', 'Please Enter Email');
define('SUBMIT', 'Submit'); //login (179)
define('RESET_PASSWORD', 'Reset Password');
define('RESET', 'Reset');
define('YOUR_ACCOUNT_ACTIVATED_SUCCESSFULLY', 'Congratulations! Your account is activated successfully. Now you can login with your login details.');
define('ERROR_OCCURED_IN_ACTIVATING_ACCOUNT', 'Error in activate process of your account. Please contact administrator.');
define('YOUR_DONATION_MADE_SUCCESSFULLY', 'Your donation has been made successfully');
define('YOUR_DONATION_FAILED', 'Your donation process failed, Try again.');
define('MESSAGE_SEND_SUCCESSFULLY', 'Message sent successfully.');
define('YOUR_REQUEST_EXPIRED', 'Your link is expired.');
define('QUICK_LINKS', 'Quick Links');
define('SOCIAL_SIGN_UP', 'Social sign up');
define('NEWSLETTER', 'Newsletter');
define('LOGIN1', 'Log In');
define('FILL_YOUR_INFO', 'Fill your information');
define('FIRST_NAME', 'First Name');
define('UPLOAD_PHOTO', 'Upload Photo');
define('CHANGE_PHOTO', 'Change Photo');
define('LAST_NAME', 'Last Name');
define('SKIP', 'Skip');
define('SAVE_AND_CONTINUE', 'Save and Continue');
define('SAVE_AND_FINISH', 'Save And Finish');
define('ABOUT_YOURSELF', 'About Yourself');
define('INTREST', 'Interest');
define('SKILL', 'Skill');
define('OCCUPATION', 'Occupation');
define('VIEW_COMPAIGN', 'View Compaign');
define('PLEASE_SEND_EMAIL_FOR_VARIFICATION_LINK', 'Please send email for verification link.');
define('IPBAND_CANNOTREG_NOW_SIGNIN', 'Your IP has been band due to spam. You can not login now.');
define('IPBAND_CANNOTREG_NOW', 'Your IP has been band due to spam. You can not register now.');
define('IP_BAND_CONTACT_WEB_MASTER', 'Your IP address  has been band. Please contact to admin.');


/*** contact form ******/
define('IPBAND_CANNOT_INQUIRY', 'Your IP has been Band due to Spam. You can not Post Inquire.');
define('INQUIRY_MESSAGE_SENT_SUCCESSFULLY', 'Your inquiry sent successfully.');
define('SEND', 'SEND');
define('BANDCAMP', 'Bandcamp');
define('GROUP_FUND_CREATORS', 'Group Fund Creators');
define('GROUP_FUND', 'Group Fund');
define('NO_RESULT_FOUND', 'No results found. Use the options in the left-hand sidebar to refine your search.');
define('INVITE_MSGS', '%s would like to add you to %s team on %s ,Login or Sign up to join the team');
define('NEW_THIS_WEEK', 'New This Week');
define('YOUR_ACCOUNT_INACTIVE_ADMIN', 'Your account is inactive by admin.');
define('INACTIVE_ACCOUNT_AFTER_SIGNUP', 'Your Account is not Active. Please check your inbox and click on activation link.');
define('RESEND_VERIFICATION_LINK', 'Resend Verification link');
define('RESET_PASSWORD_FAILED', 'Send reset password request failed. Please try again.');
define('YOUR_ACCOUNT_ALREADY_ACTIVE', 'Your account is already activated.');
define('RESEND_VERIFICATION_FAILED', 'Resend verificatioin Process failed.');
define('WRONG_FACEDBOOK_SETTINGS', 'There are some error occur with facebook API, please verify your facebook setting.');
define('TWITTER_CONNECTION_ERROR', 'Could not connect to Twitter. Refresh the page or try again later.');
define('WRONG_TWITTER_SETTINGS', 'There are some error occur with twiiter API, please verify your twitter setting.');
define('WRONG_LINKEDIN_SETTINGS', 'There are some error occur with linkdin API, please verify your linkdin setting.');
define('SOCIAL_DETAILS_UPDATED_SUCCESSFULLY', 'Social details updated successfully.');
define('OLD_PASSWORD_DOESNT_MATCH', 'The old password you entered does not match with your old password.');
//header view 
define('YOUR_ACCOUNT_ACTIVATED_SUCCESSFULYY_LOGIN', "Congratulations! Your account is activated successfully. Now you can login with your login details.");
define('YOUR_ACCOUNT_ACTIVATED_ALREADY_LOGIN', "Your account is already activated. You can login with your login details.");
define('CONFIRM_ACCOUNT_ERROR', "Your have some error to confirm account.");
define('SIGN_UP_SUCCESSFULLY_DETAILS_SENT', "You have signed up successfully. Your account details sent to your email address.");
define('WELCOME_LOGIN_SUCCESSFULL', "Welcome %s, You have logged in successfully.");
define('PASSWORD_RESET_SUCCESSFULLY_LOGIN', "Your password reset successfully. Now you can take login.");
define('EMAIL_VERIFICATION_LINK_RESEND', "Email verification link has been resent to your email successfully.");
define('DRAFT_PROJECT_MESSAGE_HOME', 'This '.$project.' is only a Draft. It cannot be publish '.$project.'.');
define('DONATION_DONE_SUCCESSFULLY_HOME', "Your donation done successfully.");
define('DONATION_FAILED_HOME', "Your donation is failed.");
define('MESSAGE_SENT_SUCCESSFULLY_HOME', "Your message sent successfully.");
define('EMAIL_ALREADY_SUBSCRIBED_WITH_SITE', "Your Email is already subscribe with site.");
define('EMAIL_SUBSCRIBED_WITH_SITE', "Your email subscribe successfully.");
define('YOUR_ACCOUNT_IS_ALREADY_ACTIVATED', "Your account is already activated.");
//content views
define('YOUR_EMAIL', 'Your Email');
define('LINK_YOUR_PROFILE_OR_PROJECT', 'Link to your '.$project.' or profile');
define('RESET_PASSWORD_LINK_SENT_SUCCESSFULLY_VIEW', 'Reset password link sent to your email successfully.');
define('CONFIRM_PASSWORD', 'Confirm Password');
define('SELECT_QUESTION_TYPE', 'Select Question');
//validation social signup steps
define('EMAIL_IS_REQUIRED','Email is required.');
define('EMAIL_VALID_EMAIL','Enter valid Email.');
define('PASSWORD_IS_REQUIRED','Password is required.');
define('ENTER_ATLEAST_EIGHT_CHARACTER','Enter atleast 8 character.');
define('ENTER_ATLEAST_FIVE_CHARACTER','Enter atleast 5 character.');
define('YOU_CAN_NOT_ENTER_MORE_THAN_TWE_CHARACTER','You can not enter more than 20 characters.');
define('FIRST_NAME_LAST_NAME_ARE_REQUIRED','First name and Last name are required.');
define('USERNAME_CANNOT_BE_MORE_THAN_CHAR','User name can not be more than 39 character.');
define('ENTER_FIRSTNAME_LASTNAME_ONLY','Enter first name and last name only.');
define('ENTER_FIRSTNAME_LASTNAME','Enter first name and last name.');
define('RETYPE_EMAIL_IS_REQUIRED','Retype Email is required.');
define('EMAIL_DOES_NOT_MATCH','Email does not match.');
define('RETYPE_PASSWORD_IS_REQUIRED','Confirm Password is required.');
define('PASSWORD_DOES_NOT_MATCH','Password does not match.');
define('INVITE_CODE_IS_REQUIRED','Invite code is required.');
define('PLEASE_ACCEPT_TERMS_OF_USE','Please Accept Terms Of Use.');
define('THE_EMAIL_ALREADY_EXISTS','This Email already exists');

//validation social signup steps
define('ENTER_FIRST_NAME','Enter first name.');
define('ENTER_FIRST_NAME_ONLY','Enter first name only.');
define('FIRST_NAME_CANNOT_BE_MORE_THAN_THIRTY_CHAR','First name can not be more than 39 character.');
define('FIRST_NAME_CANNOT_BE_LESS_THAN_THREE_CHAR','First name can not be less then 3 character.');
define('ENTER_LAST_NAME','Enter last name.');
define('ENTER_LAST_NAME_ONLY','Enter last name only.');
define('LAST_NAME_CANNOT_BE_MORE_THAN_THIRTY_CHAR','Last name can not be more than 39 character.');
define('LAST_NAME_CANNOT_BE_LESS_THAN_THREE_CHAR','Last name can not be less then 3 character.');
define('ZIPCODE_IS_REQUIRED','Postcode is required.');
define('ENTER_VALID_ZIPCODE','Enter valid Postcode.');
define('ENTER_VALID_URL','Enter valid url');
// 404 page
define('SORRY','Sorry');
define('WE_CANT_FIND_THAT_PAGE','we can`t find that page.');
define('RETURN_TO_HOME_PAGE','RETURN TO HOME PAGE');
define('PLEASE_BE_SURE_TO_DOUBLE_CHECK_YOUR_SPELLING_TOMAKE_SURE_YOU','Please be sure to double check your spelling to make sure you correctly entered the address, or you can simply head to the home page by clicking the button below.');
// main dashboard
define('YOUR_PROJECTS','Your '.$project.'s');
define('PROJECT_FOLLOWERS',''.$project.' Followers');
define('PROJECT_FOLLOWING',''.$project.' Following');
define('YOUR_TOTAL_PROJECTS','Your Total '.$project.'');
define('TOTAL_PROJECTS','Total '.$project.'s');
define('YOUR_DONATION','Your Donation');
define('YOUR_RUNNING_DONATION','Running Investor Process');
define('RUNNING','Running');
define('DONATION_RECEIVED','Donation Received');
define('GENERATE_CODE','Generate Code');
define('THERE_ARE_NO_INVITATION_CODE_HISTORY','There are no invitation code history.');
define('USER_FOLLOWERS_FOLLOWINGS','User Followers / Followings');
define('NO_PROJECTS_UNDER_YOUR_BELT_YET','No '.$project.'s under your belt yet,');
define('BUT_YOU_HAVE_GREAT_IDEAS_ANDSO_DO_THE_PEOPLE_YOU_KNOW','but you have great ideas and so do the people you know. You could start a '.$project.' today or team up with friends to make your ideas come to life!');
// dashboard sidebar
define('MAIN_NAVIGATION','Main Navigation');
define('TRANSACTION_HISTORY','Transaction History');
define('SOCIAL_NETWORK_DETAILS','Social Network Details');
define('MY_PROFILE','My Profile');
define('INBOX','Inbox');
define('INVITE_FRIENDS','Invite Friends');
define('ACTIVITY','Activity');
define('IMAGE_VERIFICATION_WRONG','Image Verification Wrong');
define('ENTER_VERIFICATION_CODE','Enter verification code');
define('PLEASE_ENTER_QUESTION_DETAIL','Please enter question details');
define('PLEASE_ENTER_VALID_EMAIL_ADDRESS','Please enter valid Email address');
define('PLEASE_SELECT_QUESTION_TYPE','Please select question type');
//transaction
define('SEARCH_BY_PROJECT','Search By '.$project.'');
define('SEARCH_BY_EMAIL','Search By Email');
define('SEARCH_BY_STATUS','Search By Status');
define('GO','Go');
define('SR_NO','Sr. NO');
define('CR_DR','Cr/Dr');
define('PREAPPROVAL_KEY','Pre-approval Key');
define('SERVICE_FEES','Service Fees');
define('EXPORT_FILE','Export file');
define('THERE_ARENO_TRANSACTION_HISTORY','There are no transaction history.');
define('MOST_FUNDED_PROJECT','Most Funded '.$project.'');
define('WEBSITE_URL','Website url');
define('BANDCAMP_URL','Bandcamp url');
define('ENTER_VALID_FACEBOOK_URL','Enter valid facebook url');
define('ENTER_VALID_TWITTER_URL','Enter valid twitter url');
define('ENTER_VALID_LINKEDIN_URL','Enter valid linkedin url');
define('ENTER_VALID_YOUTUBE_URL','Enter valid youtube url');
define('ENTER_VALID_BASECAMP_URL','Enter valid basecamp url');
define('ENTER_VALID_MYSPACE_URL','Enter valid myspace url');
define('ENTER_VALID_GOOGLEPLUS_URL','Enter valid google+ url');
define('COMMENT_POSTED_SUCCESSFULLY','Comment posted successfully');
define('THE_FILETYPE_YOU_ARE_TRYING_TO_YPLOAD_ISNOT_ALLOWED','The filetype you are trying to upload is not allowed.');
define('SOCIAL_DETAIL_UPDATED_SUCCESSFULLY','Social details updated sucessfully');
define('SEARCH_BY_TITLE','search by title');
define('CONNECT_WITH_FACEBOOK','Connect with facebook');
define('CONNECT_WITH_TWIITER','Connect with Twitter');
define('CONNECT_WITH_LINKEDIN','Connect with Linkedin');
define('WITH_FACEBOOK','Continue with Facebook');
define('WITH_TWIITER','Continue with Twitter');
define('WITH_LINKEDIN','Continue with Linkedin');
define('WE_COULDN_FIND_ANY_OF_YOUR_GMAIL_CONTACT_BECAUSE_YOU_HAVE_CONNECTED_GMAIL','We couldn´t find any of your Gmail contacts because you haven´t connected Gmail. Click the button to temporarily connect Gmail');
define('WE_WILL_NEVER_SEND_OUT_ANY_EMAIL_MESSAGE_WITHOUT_ASKING','We will never send out any email messages without asking');
define('FIND_FIND_FROM_GMAIL','Find find from gmail');
define('HOURS','hours');
define('MINUTES','minutes');
define('MONTHS','months');
define('YEARS','years');
define('BROWSE_IMAGE','Browse Image');
define('WE_COULDNOT_FIND_ANY_OF_YOUR_FRIEND_FROM_FACEBOOK_BECAUSE_YOU_HAVE_CONNECTED_FACEBOOK',"We couldn't find any of your friends from Facebook because you haven't connected Facebook. Click the button below to connect");
define('INVITE_YOUR_FRIENDS','Invite your friends');
define('INVITE','INVITE');
define('FEATURED_PROJECTS','Featured '.$project.'s');
define('ROUNDING_OFF_PROJECTS','Rounding Off '.$project.'s');
define('POPULAR_PROJECTS','Popular '.$project.'s');
define('SUCCESS_STORY_PROJECTS','Success Story '.$project.'s');
define('WHAT',"What's");
define('START_NOW',"Start Now");
define('PROJECT_LIMIT','You can not create more then %s '.$project.' per year');
define('NO_PROJECTS_AVAILABLE','No '.$project.'s available');
define('HOW_IT_WORKS',"HOW IT WORKS");
define('OUR_MODEL_TECHNOLOGY','Our model, technology, and team of in-house experts make us the most trusted platform in the crowdfunding industry.<br />
 We leverage all the tools in our power to amplify '.$project.'s, so you can raise maximum awareness');
define('CREATE_COMPAIGN','Create '.$project.'');
define('LAUNCH_COMPAIGN','Launch '.$project.'');
define('PROMOTE_TO_NETWORK',"Promote to Network");
define('COLLECT_YOUR_FUNDS',"Collect Your Funds");
define('START_A_CAMPAIGN','Start a '.$project.'');
define('SO_YOU_HAVE_GOT_AN_IDEA',"So you've got an idea? Draft your appealing '".$project."' & submit.");
define('ALLOW_US_SOME_TIME','Allow us some time to review/moderate your '.$project.' or let us provide feedback.');
define('TELL_YOUR_CROWD_ABOUT','Tell your crowd about '.$project.' and share via social networking channels.');
define('ENCOURAGE_YOUR_CROWD_WITH',"Encourage your crowd with rewards, hit your target & collect your funds.");
define('VIEW_PROJECT', 'View '.$project.'');
define('COMPLETE_SUCCESS', 'Complete (success)');
define('ALL_PROJECT', 'All '.$project.'s');
define('DRAFT_PROJECT', 'Draft '.$project.'s');
define('PENDING_PROJECT', 'Pending '.$project.'s');
define('ACTIVE_PROJECT', 'Active '.$project.'s');
define('SUCCESSFUL_PROJECT', 'Successful '.$project.'s');
define('CONSOLE_PROJECT', 'Console '.$project.'s');
define('FAILURE_PROJECT', 'Failure '.$project.'s');
define('DECLINE_PROJECT', 'Decline '.$project.'s');
define('CAMPAIGN_DASHBOARD', ''.$project.' Dashboard');
define('EXPLORE', 'Explore');
define('WE_HELP_MAKE_ANY_IDEA_A_REALITY_THROUGH_CROWDFUNDING', 'We help make any idea a reality through crowdfunding. <br /> Discover and support these fundraising '.$project.'s today!');
define('YOUR_CAMPAIGN', 'Your '.$project.'');
define('HAS_BEEN_SUBMITTED_SUCCESSFULLY', 'has been submitted successfully.');
define('WHAT_NEXT', 'What Next?');
define('TEAM_WILL_REVIEW_DETAIL_OF_YOUR_CAMPAIGN_AND_YOU_WILL_BE_NOTIFIED', 'team will review details of your '.$project.' and you will be notified via email regarding your '.$project.' status.');
define('GO_TO_DASHBOARD', 'Go to dashboard');
define('THE_BROWSER_YOU_ARE_USING_INTERNET', 'The browser you are using, Internet Explorer 6, is no longer supported by this site. This means that some features will not work for you.');
define('SO_PLEASE_TAKE_A_MOMENT_TO_CLICK_ONE_OF_THE_LINK_BELOW_TO_UPGRADE', 'So please take a moment to click one of the links below to upgrade to a more recent browser!');
define('GOOGLE_CHROME', 'Google Chrome');
define('MOZILLA_FIREFOX', 'Mozilla Firefox');
define('APPLE_SAFARI', 'Apple Safari');
define('COMPLETED_DONATIONS','Completed Investments');
define('PENDING_DONATIONS','Pending Investments');
define('RECEIVED','Received');
define('CANCELLED','Cancelled');
define('PIPELINE','Pipeline');
define('SITE_NAME','Site Name');
define('LANGUAGE','Language');
define('CHANGE_LANGUAGE','Change Language');
define('EMAIL_OR_PASSWORD_IS_WRONG_PLEASE_TRY_AGAIN', 'Email or Password is wrong,please try again...!!!');
define('YOU_CAN_NOT_DELETE_THIS_PROJECT', 'You can not delete this project.');
define('MORE_FILTERS', 'More filters');
define('THIS_EMAIL_IS_ADDRESS', 'This email is address');
define('ALREADY_EXIST_IN_OUR_SYSTEM_NDO_YOU_WANT_TO_MERGE_YOUR_TWITTER_ACCOUNT_WITH_THIS', ' already exist in our system,\nDo you want to merge your twitter account with this ');
define('ACCOUNT_', 'account ');
define('YOUR_ACCOUND_HAS_BEEN_MERGE_SUCCESSFULLY', 'Your account has been merge successfully.');
define('IT_MIGHT_BE_SOME_PROBLEM_FOR_MERGE_YOUR_ACCOUNT_PLEASE_TRY_AGAIN_LATER', 'It might be some problem for merge your account, Please try again later.');
define('MY_COMMENT','My Comment');
define('HIDDEN','Hidden');
define('INACTIVE_HIDDEN','Inactive-Hidden');
define('INACTIVE_VISIBLE','Inactive-Visible');
define('OK','Ok');
define("HOUR_LEFT", "Hour Left");
define("NO_TIME_LEFT", "No time left");
define('TEXT_YEAR', 'year');
define('TEXT_MONTH', 'month');
define('TEXT_DAY', 'day');
define('TEXT_HOUR', 'hour');
define('TEXT_MINUTE', 'minute');
define('TEXT_YEARS', 'years');
define('TEXT_MONTHS', 'months');
define('TEXT_DAYS', 'days');
define('TEXT_HOURS', 'hours');
define('FUNDED', 'Funded');
define('ALREADY_EXIST_IN_OUR_SYSTEM_NDO_YOU_WANT_TO_MERGE_YOUR_SOCIAL_ACCOUNT_WITH_THIS', 'already exist in our system,\nDo you want to merge your social account with this ');

/********----------------------Account_lang.php----------------------------******/
define('MEMBER_SINCE', 'Member Since');
define('JOINED', 'Joined');
define('LAST_LOGIN', 'Last Login');
define('MIN_AGO', 'min ago');
define('HR_TEXT', 'hr');
define('MIN_TEXT', 'min');
define('SEC_AGO', 'sec ago');
define('SEC', 'sec');
define('DAYS', 'Days');
define('DAY_LEFT', 'Day left');
define('TEXT_AGO', 'ago');
define('MY_ACCOUNT', 'My Account');
define('EDIT_PROFILE_SETTINGS', 'Edit Profile & Settings');
define('EDIT_PROFILE', 'Edit Profile');
define('EMAIL_NOTIFICATION_SETTING', 'Email Notification Setting');
define('USER_ALERT', 'User Alert');
define('ADD_FUND', 'Add Fund');
define('PROJECT_ALERT', ''.$project_name.' Alert');
define('COMMENT_ALERT', 'Comment Alert');
define('PROJECT', ''.$project_name.'');
define('PAYEE_EMAIL', 'Payee Email');
define('MY_DONATION', 'My '.$funds_plural);
define('NOTIFICATION', 'Notification');
define('MY_COMMENTS', 'My Comments');
define('NO_COMMENT_ADDED', 'There is no comment added');
define('CHANGE_PASSWORD', 'Change Password');
define('OLD_PASSWORD', 'Old Password');
define('NEW_PASSWORD', 'New Password');
define('CON_PASSWORD', 'Confirm Password');
define('INCOMING_FUND', 'Incoming Donation');
define('END_DATE', 'End Date');
define('THERE_ARE_NO_INCOMING_DONATION', 'There are no incoming '.$funds_plural.'.');
define('THERE_ARE_NO_DONATION', 'There are no '.$funds_plural.'.');
define('SAVE_CHANGES', 'Save Changes');
// account page validation
define('THIS_FIELD_CANNOT_BE_LEFT_BLANK','This field can not be left blank.');
define('ENTER_ZIP_CODE_NOT_LESS_THAN_FIVE_CHAR','Enter Zip Code not less than 5 character.');
define('ENTER_ZIP_CODE_NOT_MORE_THAN_SEVEN_CHAR','Enter Zip Code not more than 7 character.');
define('OLD_PASSWORD_IS_REQUIRED','Old password is required.');
define('CONFIRM_PASSWORD_IS_REQUIRED','Confirm Password is required.');
define('PAYPAL_REQUEST_AND_RESPONSE', 'Paypal Request and Response');
//define('ARE_YOU_SURE_YOU_WANT_TO_DELETE_SELECTED_RECORD', 'Are ypu sure?you want to delete selected record(s).');
define('DONER', 'Doner');
define('PAYPAL_REQUEST', 'Paypal Request');
define('ENTER_PROFILE_SLUG', 'Enter Profile Slug');
define('SPACE_ID_NOT_REQUIRED', 'Space is not required.');
define('PROFILE_SLUG_ALREADY_EXIST', 'Profile slug is already exist. Please try another');
define('PROFILE_SLUG', 'Profile Slug');
// notification text
define('COMPAIGNS_YOU_BACK', ''.$project_name.'s you back');
define('NEW_PLEDGES', 'New pledges');
define('NEW_COMMENTS', 'New comments');
define('NEW_FOLLOW', 'New follow');
define('NEW_UPDATES', 'New updates');
define('COMPAIGNS_YOU_FOLLOW', ''.$project_name.'s you follow');
define('SOCIAL_NOTIFICATIONS', 'Social notifications');
define('NEW_FOLLOWERS', 'New followers');
define('COMMENT_REPLY', 'Comment reply');
define('COMPAIGNS_YOU_CREATED', ''.$project_name.'s you created');
define('SAVE_SETTINGS', 'Save Settings');

define('PROFILE_SLUG_IS_REQUIRED', 'Profil Slug id  required');

define('SOCIAL_DETAILS', 'Social details');
define('TOTAL_DONATION', 'Total Investment');
define('ACTIVE_DONATION', 'Active Investment');
define('SUCCESSFUL_DONATION', 'Successful Investment');
define('UNSUCCESSFUL_DONATION', 'Unsuccessful Investment');
define('THERE_ARENO_INVESTOR_PROCESS', 'There are no running investor process.');

define('OCCUPATION_CANNOT_MORE_THAN_39_CHARS', 'Occupation cannot be more than 39 characters.');
define('INVALID_GOOGLE_PLUS_LINK', 'Invalid Goole Plus Link');
define('MUST_CONTAIL_VALID_URL', 'The %s field must contain a valid URL.');
define('BY', 'by');
define('CLOSE', 'Close');
define('UPDATE_DELETE_CONFIRMATION', 'Are you sure,You want to Delete this Update?');

//==============inbox_details============

define('SENT_ON','sent on');
define('THERE_ARE_NO_MESSAGE_IN_INBOX','There are no message in inbox.'); 
define('PLEASE_ADD_CONVERSATION_TEXT','Please add conversation text'); 
define('PLEASE_ENTER_TEXT','Please Enter Text'); 

//===================index.php===============
define('CONVERSATION_WITH','Conversation with'); 
define('BACK_TO_INBOX','back to indox');
define('REPLY','Reply');
define('TEAM','Team');


/**** profile_lang.php***/
define('FOLLOWING','Following');
define('FOLLOW','Follow');
define('UNFOLLOW','Unfollow');
define('LOCATION','Location');
define('CONTACT_ME','Contact me');
define('ALSO_FIND_ME_ON','Also find me on');
define('MY_SPACE','My Space');
define('CREATED_PROJECTS','Created '.$project.'');
define('CONTRIBUTE_NOW','Contribute Now');
define('NO_PROJECTS_FOUND','No '.$project.'s found');
define('CONTRIBUTIONS','Contributions');
define('NO_CONTRIBUTIONS','No Contributions');
define('SEE_MORE','See more');
define('COMMENTS',$comment);
define('AT','At');
define('ON','On');
define('COMMENTED_ON','Commented on');
define('NO_COMMENTS_FOUND','No '.$comment.' found');
define('MY_FOLLOWERS','My Followers');
define('NO_FOLLOWERS_FOUND','No '.$follower.'s found');
define('MY_FOLLOWINGS','My Followings');
define('NO_FOLLOWINGS_FOUND','No followings found');
define('ABOUT_ME','About me');
define('NA','N/A');
define('SKILLS','Skills');
define('INTEREST','Interest');
define('CONTRIBUTE_AMOUNT','Contribute Amount');
define('TOTAL_RAISED_FOR_OWN_PROJECTS','Total Raised for own '.$project.'s');
define('TOTAL_DONATED_TO_OTHER_PROJECTS','Total donated to other '.$project.'s');
define('PROFILE','Profile');
define('CONTRIBUTED','Contributed');
define('SHOW_MORE_CONTRIBUTIONS','Show more contributions');
define('THIS_USER_HAS_NOT_ENTERED','This user has not entered any details about themselves yet.');
define('THIS_USER_HAS_NOT_ADDED_ANY_SKILLS_YET','This user has not added any skills yet.');
define('THIS_USER_HAS_NOT_ADDED_ANY_INTEREST_YET','This user has not entered any interest yet.');
define('RECIPIENT_MAIL_INVALID','Recipient email is invalid');
define('INVITATION_HAS_BEEN_SENT_TO','Invitation Has been sent to ');

/*Invite_lang.php*/
define("GMAIL", "Gmail");

define("MULTIPLE_EMAIL_ADDRESS_SHOULD_BE_SEPERATED_WITH_COMMA", "Multiple email addresses should be separated with comma.");
define('YOU_ALREADY_SENT_INVITATION_TO_THIS_EMAIL', 'You already sent Invitation to this email.');
define('YOU_CAN_NOT_INVITE_YOUR_SELF', 'You can not invite your self.');
define('RECIPIENT_MESSAGE_IS_REQUIRED', 'Recipient message is required.');
define('INVITATION_IS_FAILED', 'Invitation is failed.');
define('SHOW_SELECTED_FRIENDS', 'Show selected friends');
define('WE_COULDN_T_FIND_ANY_OF_YOUR_FRIENDS_FROM_FACEBOOK_BECAUSE_YOU_HAVEN_T_CONNECTED_FACEBOOK_AND_REKAUDO_CLICK_THE_BUTTON_BELOW_TO_CONNECT', 'We couldn´t find any of your friends from Facebook because you haven´t connected Facebook. Click the button below to connect.');
define('INVITE_YOUR_FRIENDS_TO', 'Invite Your Friends to ');
define('WE_COULDNT_FIND_ANY_OF_YOUR_GMAIL_CONTACTS_BECAUSE_YOU_HAVENT_CONNECTED_GMAIL_AND_REKAUDO_CLICK_THE_BUTTON_TO_TEMPORARILY_CONNECT_GMAIL_AND_REKAUDO', 'We couldn´t find any of your Gmail contacts because you haven´t connected Gmail. Click the button to temporarily connect Gmail');
define('WE_WILL_NEVER_SEND_OUT_ANY_EMAIL_MESSAGES_WITHOUT_ASKING', ' We will never send out any email messages without asking.');
define('FRIENDS_ON', 'Friends on');
define('YAHOO_MAIL', 'Yahoo mail');
define('INVITE_SENT', 'Invite sent');
define('SELECT_ALL', 'Select All');
define('DESELECT_ALL', 'Deselect All');
define('INVITE_ALL', 'Invite All');
define('WE_COULDNT_FIND_ANY_OF_YOUR_YAHOO_CONTACTS_BECAUSE_YOU_HAVENT_CONNECTED_YAHOO_AND_REKAUDO_CLICK_THE_BUTTON_TO_TEMPORARILY_CONNECT_YAHOO', 'We couldn´t find any of your Yahoo contacts because you haven´t connected Yahoo. Click the button to temporarily connect Yahoo  ');
define('YOUR_AFFILIATE_REQUEST_IS_IN_PENDING', 'Your affiliate request is in pending.');
define('SORRY_AN_ERROR_OCCURES_PLEASE_TRY_AGAIN', 'Sorry an error occures please try again..!');
define('YOUR_INVITATION_SENT_SUCCESSFULLY', 'Your Invitation sent successfully');
define('SEND_ALL', 'Send All');
define('FACEBOOK_WILL_FETCH', 'Facebook Will fetch only those friends who are using application (%s)');
 

/* content_lang.php*/ 
define('HAVE_DIFFERENT_QUESTION', 'Have a different question?');
define('YOUR_NAME', 'Your Name');
define('CONTACT_US_EMAIL', 'Your Email');
define('CONTACT_US_NAME', 'Your Name');
define('CONTACT_US_MESSAGE', 'Details');
define('PLEASE_ENTER_NAME', 'Please enter your Name');
define('PLEASE_ENTER_VALID_EMAIL', 'Please enter valid Email Id');
define('PLEASE_ENTER_MESSAGE', 'Please enter question details');
define('PLEASE_ENTER_QUESTIONS', 'Please enter your question');
define('PLEASE_ENTER_QUESTIONS_ABOUT', 'Please enter your question type');
define('CONTACT_US_QUESTIONS', 'Your Question');
define('CONTACT_US_LINK', 'Link to your project or profile');
define('CONTACT_US_ABOUT', 'Your Question is about');
define('SELECT_TYPE', 'Select Type');
define('FUNDRAISING_ACCOUNT_LOGIN', 'Fundraising Account/Login');
define('FUNDRAISING_PROJECT', 'Fundraising '.$project.' Idea');
define('INTERNATIONAL', 'International');
define('OTHER', 'Other');
define('QUESTION', 'Question : ');
define('DETAILS', 'Details ');
define('LINKS', 'Links : ');
define('YOUR_QUESTION', 'Your Question');
define('THE_GUIDE_TO_A_SUCCESSFUL_CAMPAIGN', 'The Guide To a Successful Campaign');
define('STILL_WANT_TO_LEARN_MORE', 'Still Want to Learn More?');
define('GET_GOING', 'Get Going!');
define('FULLY_FUNDED', 'Fully Funded');
define('SUCCESS_STORYS', 'Success Story');
define('HELP_CENTER', 'Help Center');
define('KNOWLEDGE_BASE', 'Knowledge Base');
define('ARTICLES', 'Articles');
define('RESPONSE_WITHIN_24_HOURS', 'Response within 24 hours');
define('SUBMIT_A_REQUEST', 'Submit a request');
define('WE_ARE_HERE_TO_HELP_TO_GET_START_PLEASE_SELECT_THE_TYPE_OF_ISSUE', "We are here to help! To get started, please select the type of issue you'd like to contact us about");
define('PLEASE_USE_A_FEW_WORD_TO_SUMMARIZE_YOU_QUESION', "Please use a few words to summarize your question. You may see links to helpful articles appear as you type");
define('PLEASE_ENTER_ADDITIONAL_DETAIL_OF_YOUR_REQUEST', 'Please enter additional details of your request. To help us get you an answer as quickly as possible, please consider including as many specific details as possible. For example, if your question is about a particular '.$project.' on');
define('PLEASE_INCLUDE_THE_NAME_OF_THE_CAMPAING_AND_A_LINK', 'please include the name of the '.$project.' and a link.');
define('FIRST_AND_LAST_NAME', 'First and Last Name');
define('YOUR_EMAIL_ADDRESS', 'Your email address');
define('NO_LEARN_MORE_CATEGORY_AVAILABLE', 'No learn more category available');
define('RELATED_ARTICLES', 'Related articles');
define("SECURITY_CHECK", "Security Check");
define("JOIN_NOW", "Join Now");


/*equity_lang.php*/
define('THIS_COMPANY_MAY_BE_INTERESTED','This '.$project.' is open for both accredited & no-accredited '.$investor.'s.');
define('THIS_COMPANY_ONLY_INTERESTED','This '.$project.' is open for only accredited '.$investor.'s.');
define('RAISE_DETAILS','Raise Details');
define('FUNDING_GOAL','Funding Goal');
define('CURRENT_RESERVATIONS','Current Reservations');
define('MINIMUM_RESERVATIONS','Minimum Reservation');
define('INTEREST_PER_YEAR','Interest (% per year)');
define('TERM_LENGTH_MONTHS','Term Length (Months)');

define('RAISED_OF','Raised of');
define('COMPLETE_SUCCESSB','Complete (success)');
define('OPEN_DATE','Open Date');
define('VIEW_ALL_FOLLOWERS','View All Followers');
define('COPY_THE_CODE_BELOW_AND_PASTE','Copy the code below and paste it into your website or blog.');
define('EMBED_THIS_CARD_IN_YOUR_WEBSITE','Embed this card in your website or blog');
define('CAMPAIGN_SHORT_LINK','Campaign short link');
define('CONNECT_WITH_COMPANY','Connect with Company');
define('HIGHLIGHTS','Highlights');
define('TOP_INVESTORS','Top Investors');

//Request Access
define('MISSING_MINIMUM_PROFILE_ITEMS','Missing Minimum Profile Items');
define('BEFORE_YOU_CAN_REQUEST_ACCESS_OR_MAKE_COMMITMENT','Before you can request access or make commitment to a deal you mast have the following profile item completed:');
define('USER_PROFILE_IMAGE','User Profile Image');
define('CURRENT_LOCATION','Current Location');
define('PLEASE_CLICK_ON_THE_COMPLETE_PROFILE_BUTTON','Please click on the Complete Profile button to complete the required profile items. After you successfully complete your profile, you will be redirected back to this page and request will be processed automatically.');
define('REQUEST_ACCESS','Request Access');
define('YOU_MUST_BE_AN_ACCREDITED_INVESTOR_AND_REQUEST','This section is only viewable to those who are granted full or partial access to the deal room.');
define('YOUR_REQUEST_TO_ACCESS_UPDATES_SECTION_HAS_BEEN_SUBMITTED','Your request to access updates section has been submitted successfully.');

define('YOUR_REQUEST_TO_ACCESS_COMMENTS_SECTION_HAS_BEEN_SUBMITTED','Your request to access '.$comments.' section has been submitted successfully.');

define('YOUR_REQUEST_TO_ACCESS_FUNDERS_SECTION_HAS_BEEN_SUBMITTED','Your request to access funders section has been submitted successfully.');
define('YOUR_REQUEST_TO_ACCESS_DOCS_MEDIA_SECTION_HAS_BEEN_SUBMITTED','Your request to access docs & media section has been submitted successfully.');
define('PLEASE_WAIT_UNTILL_YOUR_ACCESS_REQUEST_IS_APPROVED','Please wait until your access request is approved.');
define('REQUEST_PENDING','Request Pending');
define('ACCESS_REQUEST','Access Request');
define('PERMISSION','Permission');
define('ACCREDIATION_STATUS','Accreditation Status');
define('SECTION','Section');

define('NOT_ACCREDITED', 'Not Accredited');
define('SET_PERMISSIONS', 'Set Permissions');
define('DOCS_MEDIA','Docs & Media');
define('I_AM_INTRESTED','I am Interested');
define('INVEST_NOW','Invest Now');
define('INVESTMENT_IN_PROCESS','Investment In Process');
define('VERIFY_INVESTOR_STATUS','Verify Investor Status');
define('YOU_REQUEST_IS_STILL_PENDING','Your request to become '.$investor.' is pending, please wait until your request is approved.');
define('YOU_REQUEST_IS_REJECTED_BY_OWNER','You request is rejected by owner');
define('NO_PERMISSION','No Permission');
define('ACCEPT','Accept');
define('DENY','Deny');
define('ARE_YOU_SURE_YOU_WANT_TO_ALLOW','Are you sure you want to allow %s to invest on your '.$project.' ?');

define('POST_A_COMMENT','Post a Comment');
define('POST_COMMENT','Post Comment');
define('DOCUMENTS','Documents');
define('THIS_DOCUMENTS_IS_ONLY_VIEWABLE_TO_THOSE','This document is only viewable to those who are granted full access');
define('MAKE_YOUR_PROJECT_LIVE_TO_SHARE_ON_SOCIAL','Make your '.$project.' live to share on social media websites.');

//=============project ==================///// 
define('DECLINE', 'Decline');
define('EDIT_PROJECT', 'Edit '.$project);
define('DELETE_PROJECT', 'Delete '.$project);
define('PROJECT_CLOSED','Closed');

define('SHARE', 'Share'); 
define('OVERVIEW', 'Overview');
define('DONATIONS', 'Investments');
define('RAISED_TOWARDS', 'Raised towards');
define('UPDATES', 'Updates');
define('WIDGETS', 'Widgets');
define('ENDED', 'Ended on');
define('VIEW_PROJECT_PAGE', 'View '.$project.' Page');
define('ACTION', 'Action');
define('FOLLOWERS', 'Followers');
define('VIDEO_GALLERY','Video Gallery');
define('ADD_VIDEO','+ Add Video');
define('IMAGE_GALLERY','Image Gallery');
define('ADD_IMAGE','+ Add Image');
define('EDIT', 'Edit');
define('THANKS_FOR_JOINING_THE_TEAM','Thanks for Joining the Team!');
define('CONGRATES_YOU_ARE_NOW_PART_OF_THE','Congrats! You`re now part of the');
define('TEAM_ON','team on');
define('IS_CAMPAIGNING_FOR','is campaigning for');
define('AND_HAS_ADDED_YOU_AS_A_MEMBER_ONTHE_TEAM','and has added you as a member on the team. Now that you`re on board, help get the ball rolling and start letting people know about this great '.$project .'!');
define('EMBED', 'Embed');
define('PERKS', 'Perks');
define('THERE_IS_NO_UPDATES_FOR_THIS_PROJECT', 'There are no updates for this '.$project);
define('THERE_IS_NO_COMMENT_FOR_THIS_PROJECT', 'There are no '.$comments.' for this '.$project);
define('THERE_IS_NO_FUNDERS_FOR_THIS_PROJECT', 'There are no funders for this '.$project);
define('KEEP_PRIVATE', 'Keep private');
define('COMMENT', 'Comment');
define('SHOW_COMMENT_AFTER_APPROVED', 'Comment added successfully. It will be seen after company owner approval.');
define('PROJECT_GOAL', 'Project Goal');
define('FUNDERS', 'Funders');
define('DATE', 'Date');
define('PROJECT_STATUS','Project Status');
define('TOTAL_RECEIVE_PAYMENT', 'Total Received Payment');
define('REMAIN_DAYS', 'Remaining Days');
define('DONOR', 'Donor');
define('AMOUNT_DOLOR', 'Amount');
define('AMOUNT_FEES', 'Fees');
define('NO_DONATION', 'No Investments');
define('PERK', 'Perk');
define('NEXT', 'Next');
define('DELETE', 'Delete');
define('ADD_NEW', 'Add New');
define('SHARE_YOUR_PROJECT', 'Share Your Project');
define('SHARE_PROJECT_ON_TWITTER', 'Share '.$project.' on Twitter');
define('SHARE_ON_FURL', 'Share '.$project.' on furl');
define('FURL', 'furl');
define('SHARE_ON_STUMBLEUPON', 'Share '.$project.' on StumbleUpon');
define('STUMBLEUPON', 'stumbleupon');
define('DELICIOUS', 'delicious');
define('SHARE_ON_DELICIOUS', 'Share '.$project.' on Delicious');
define('SHARE_ON_DIGG', 'Share '.$project.' on Digg');
define('SHARE_ON_TUMBLR', 'Share '.$project.' on Tumblr');
define('THUBMLR', ' Tumblr ');
define('SHARE_ON_REDDIT', 'Share '.$project.' on Reddit');
define('REDDIT', 'reddit');
define('SHARE_ON_MIXX', 'Share '.$project.' on Mixx');
define('MIXX', 'Mixx');
define('SHARE_ON_TECHNORATI', 'Share '.$project.' on Technorati');
define('TECHNORATI', 'Technorati');
define('SHARE_ON_YAHOO_BUZZ', 'Share '.$project.' on Yahoo! Buzz');
define('YAHOO_BUZZ', 'Yahoo! Buzz');
define('DESIGNFLOAT', 'Share '.$project.' on DesignFloat');
define('SHARE_ON_BLINKLIST', 'Share '.$project.' on BlinkList');
define('BLINKLIST', 'BlinkList');
define('SHARE_ON_MYSPACE', 'Share '.$project.' on Myspace');
define('MYSPACE', 'Myspace');
define('SHARE_WITH_FARK', 'Share '.$project.' with Fark');
define('FARK', 'Fark');
define('SUBSCRIBE_TO_RSS', 'Subscribe to RSS');
define('RSS', 'RSS');
define('SHARE_ON_GOOGLE_PLUS', 'Share '.$project.' on Google+');
define('SAVE_THIS_REASON_TO_USE_AGAIN', 'Save this reason to use again');
define('ENTER_REASON_WHY_YOU_WANT_DENY_USER', ' Enter reason why you want to deny user request');
define('THE_PAGE_YOU_ARE_LOOKING_FOR_IS_CURRENTLY','The page you are looking for is currently in "DRAFT" mode, hidden from public or an invalid link.
Please contact campaign owner for further information if you think this is an error.');
define('THE_PAGE_YOU_ARE_LOOKING_FOR_IS_CURRENTLY_IN_DRAFT_MODE_OR_HIDDEN','The page you are looking for is currently in "DRAFT" mode or hidden from public. Please contact campaign owner if you feel this is an error!!! ');
define('PROJECT_DEAL_TYPE',''.$project.' Deal Type');
define('CURRENT_ROUND','Current Round');
define('RECORD_DELETED_SUCCESSFULLY', 'Record deleted successfully.');
define('RECORD_ADDED_SUCCESSFULLY', 'Record added successfully.');
define('RECORD_APPROVED_SUCCESSFULLY', 'Record approved successfully.');
define('RECORD_DECLINED_SUCCESSFULLY', 'Record declined successfully');
define('RECORD_REPLIED_SUCCESSFULLY', 'Record replied successfully.');
define('RECORD_SPAM_SUCCESSFULLY', 'Record spam successfully.');
define('SHOW_MORE_FUNDERS','Show more funders');
define('COMPLETE','Complete');
define('REQUEST','Request');
define('REQUESTED_FOR','Requested for');
define('POSTED_AN_ANNOUNCEMENT', 'Posted an announcement');
define('SPAM', 'Spam');
define('REPORT_SPAM', 'Report Spam');
define('APPROVED_BTN', 'APPROVED');
define('FLEXIBLE_FUNDING_CAMPAIGN', 'Flexible Funding '.$project.'');
define('FIXED_FUNDING_CAMPAIGN', 'Fixed Funding '.$project.'');
define('FLEXIBLE_FUNDING', 'Flexible Funding');
define('FIXED_FUNDING', 'Fixed Funding');
define('THIS_CAMPAIGN_WILL_ENDED_AND_WILL_RECEIVE_ALL_FUNDS_RAISED_UPTO', 'This '.$project.' will ended and will receive all funds raised upto');
define('THIS_CAMPAIGN_HAS_ENDED_AND_WILL_RECEIVE_ALL_FUNDS_RAISED_UPTO', 'This '.$project.' will receive all funds raised even if it does not reach its goal.');
define('THIS_CAMPAIGN_HAS_ENDED_ON', 'This '.$project.' has ended on ');
define('THIS_CAMPAIGN_STARTED_ON', 'This '.$project.' started on ');
define('AND_WILL_CLOSE', ' and will close on ');
define('AND_CLOSED', ' and closed on ');
define('SAID', ' Said ');
define('POST_REPLY', 'Post Reply');
define('NO_DOCUMENT_HAS_BEEN_ADDED_YET', 'No document has been added yet.');
define('NO_IMAGE_HAS_BEEN_ADDED_YET', 'No image has been added yet.');
define('NO_VIDEO_HAS_BEEN_ADDED_YET', 'No Video has been added yet.');
define('NO_PERK_HAS_BEEN_PURCHASED_YET', 'No perk has been purchased yet.');
define('SHOW_MORE_COMMENTS', 'Show more comments');
define('ARE_YOU_SURE_YOU_WANT_TO_APPROVE_THIS_COMMENT', 'Are you sure,You want to approve this comment?');
define('ARE_YOU_SURE_YOU_WANT_TO_DECLINE_THIS_COMMENT', 'Are you sure,You want to decline this comment?');
define('ARE_YOU_SURE_YOU_WANT_TO_SPAM_THIS_COMMENT', 'Are you sure,You want to spam this comment?');
define('ARE_YOU_SURE_YOU_WANT_TO_DELETE_THIS_COMMENT', 'Are you sure ,You want to delete this equity?');
define('MAKE_YOUR_PROJECT_LIVE_TO_VIEW_AND_COPY_WIDGET_CODE', 'Make your '.$project.' live to view and copy widget code.');
define('THIS_PROJECT_IS_INACTIVE_AND_NO_LONGER', 'This '.$project.' is inactive and no longer accepting '.$funds.' due to');
define('THIS_REASON', 'this reason');
define('REASON_INACTIVE_THIS', 'Reason inactive this '.$project.' is not provided by '.$site_name.' Team');
define('THIS_PROJECT_IS_HIDDEN_AND_NO_LONGER', 'This '.$project.' is hidden and no longer accepting '.$funds.' due to');
define('UPDATES_IS_REQURED', 'Updates is required');
define('DENY_ACCESS_REQUEST', 'Deny Access Request');
define('DENY_INTEREST_REQUEST', 'Deny Interest Request');
define('COMPAIGN_CLOSED',''.$project.' Closed');
define('PROJECT_RETURN_CALCULATOR','Projected Return Calculator');
define('PRINCIPAL','Principal');
define('GROSS_ANUAL_RETURN','Gross Annual Return');
define('TERM','Term');
define('TOTAL_RETURN','Total Return');
define('PROPERTY_SUMMARY','Property Summary');
define('RETURN_OVERVIEW','Return Overview');
define('ANUAL_SERVICING_FEE','Annual servicing fee');
define('NET_TO_INVESTORS','Net to investors');
define('VIEW_SPONCER_PROFILE_PROFILE','View sponsor\'s full profile');
define('SHARE_ON_FACEBOOK', 'Share '.$project.' on Facebook');


/*start_equity_lang.php*/

define("DRAFT_YOUR_CAMPAIGN", 'Draft Your '.$project.'');
define("ENHANCE_YOUR_CAMPAIGN", 'Enhance Your '.$project.'');
define("PREFUNDING_DETAILS", "Pre Funding Details");
define("ENHANCE_AND_MAKE_YOUR_COMPAIGN", 'Enhance and make your '.$project.' to make it appealing and convincing to  increase your success ratio.');

define("FUNDRAISING_DETAILS", "Fundraising Detail");
define("DEAL_TYPE", "Deal Type");
define("DEAL_HIGHLIGHTS", "Deal Highlights");
define("ELEVATOR_PITCH", "Elevator Pitch");
define("CHOOSE_THE_FOUR_MOST_BUZZ_WORTHY", "Choose the 4 most buzz-worthy hightlights about your current round that will impress investors. This will be featured near the top of your deal.");
define("ADD_COVER_PHOTO", "Add Cover Photo");
define("VIDEO_URLC", "Video URL");
define("INVESTORS", "Investors");
define("DEAL_DOCUMENTS", "Deal Documents");
define("PREVIOUS_FUNDING", "Previous Funding");


define("I_HAVE_READ_AND_UNDERSTAND", " I have read and understand Crowdfunding's project guidelines.");
define("COMPANY_PROFILE", "Company Profile");
define("COMPANY_BASIC", "Company Basic");
define("COMPANY_NAME", "Company Name");
define("EQUITY_CROWDFUNDING_URL", "".$project." Crowdfunding URL");
define("WEBSITE_URLC", "Website URL");
define("YEAR_FOUNDED", "Year Founded");
define("HEADQUATER_CITY", "Headquarters City");
define("HEADQUATER_STATE", "Headquarters State");
define("HEADQUATER_COUNTRY", "Headquarters Country");
define("COMPANY_CATEGORY", "Company Category");
define("COMPANY_INDUSTRY", "Company Industry");
define("SELECT_A_CATEGORY", "Select a Category...");
define("SELECT_INDUSTRIES", "Select Industries...");
define("ASSETS_UNDER_MANAGEMENT", "Assets Under Management");
define("SQUARE_FOOTAGE_DEVELOPED", "Square Footage Developed");
define("SQUARE_FEET", ",000 Square feet");
define("COMPANY_ASSETS", "Company Assets");
define("UPLOAD_YOUR_LOGO", "Upload your logo to your profile.");
define("COMPANY_OVERVIEW", "Company Overview");
define("COMPANY_CONNECT", "Company Connect");
define("CONNECT_YOUR_SOCIAL_NETWORKS", "Connect your social networks and website. These educate and demonstrate your credibility.");
define("EMAIL_CONTACT", "email Contact");
define("COMPANY_EMAIL", "Company email");
define("SOCIAL", "Social");
define("ENTER_THE_URLS_FROM_YOUR", "Enter the URLs from your social profiles");
define("LINKED_IN", "Linked In");
define("LINKED_IN_URL", "Linked In URL");
define("FACEBOOK_URLC", "Facebook URL");
define("TWITTER_URLC", "Twitter URL");
define("TEAM_MEMBER_BIO", "Team Member Bio");
define("MEMBER_PHOTO", "Member Photo");
define("ROLE_TITLE", "Role / Title");
define("VISIBLE_ON_PROFILE", "Visible on Profile?");
define("MAKE_ADMIN", "Make Admin?");
define("TEAM_MEMBERS_CONTENT", "Add your team members and advisors so that people know who is involved. Each stakeholder represents a powerful and personal way to demonstrate the strength of your company.");
define("MEMBER_ROLE", "Member Role");
define("ACCESS_LEVEL", "Access Level");
define("VISIBLE_PROFILE", "Visible on Profile");
define("TEAM_MEMBER_TYPE", "Team Member Type");
define("ADD_TEAM_MEMBER", "+ Add Team Member");
define("ADD_COMPANY", "Add Company");
define("ALL_DETAIL_OF_YOUR_DEAL", "All the details of your deal will be covered in this section. This is where you will define the terms of your deal and provide key documents.");
define("MINIMUM_RAISE", "Minimum Raise");
define("MAXIMUM_RAISE", "Maximum Raise");
define("COMPANY_NAME_ALREADY_EXISTS", "Company Name already exists");
define("PLEASE_CLICK_ON_COMPANY_INDUSTRY", "Please Click on Company Industry to Delete");
define("YOU_HAVE_ALREADY_APPLIED_THAT", "You have already applied that industry");
define("PLEASE_SELECT_COMPANY_INDUSTRY", "Please Select Company Industry");
define("YOU_MAY_ONLY_SELECT_UP_TO", "You may only select up to %s industries");
define("PNG_JPG_OR_GIF_RECOMENDED_SIZE", "PNG, JPG or GIF <br/><br/><b>Recommended size:</b><br/>170 x 170 pixels <br> Max Size: 2MB");
define("URL_ALREADY_EXISTS", "url already exists");
define("TERMS_OF_SERVICE", "Terms of Service");
define("FUNDRAISING_GOAL", "Fundraising Goal");
define("CLOSING_DATE_THIS_CAN_BE_EXTENDED", "Closing Date");
define("CLOSING_DATE", "Closing Date");
define("IS_YOUR_COMPANY_CURRENTLY_FUNDRAISING", "Is your company currently fundraising?");
define("AMOUNT_RAISED_IN_CURRENT_ROUND", "Amount Raised in Current Round");
define("DATE_ROUND_OPENED", "Date Round Opened");
define("SELECT_THE_DEAL_TYPE_THAT_FITS_YOUR_CURRENT_ROUND", "Select the deal type that fits your current round.");
define("EQUITY","".$project."");
define("PRICE_EQUITY_IN_YOUR_COMPANY", "Priced equity in your company");
define("CONVERTIBLE_NOTE", "Convertible Note");
define("DEBT_THAT_CAN_CONVERT_TO_EQUITY", "Debt that can convert to equity");
define("DEBT", "Debt");
define("BORROW_FROM_INVESTORS_AT_SET_INTEREST_TERMS", "Borrow from investors at set interest terms");

define("REVENU_SHARE", "Revenue Share");
define("SELL_A_PORTION_OF_YOUR_FUTURE_REVENUES_TO_YOUR_INVESTORS", "Sell a portion of your future revenues to your investors");

define("AVAILABLE_SHARES", "Available Shares");
define("WHAT_PERCENTAGE_OF_THE_TOTAL_COMPANY", "What percentage of the total company's equity is available?");
define("COMPANY_VALUATION", "Company Valuation");
define("EQUITY_AVAILABLE", "".$project." Available");
define("PRICE_PER_SHARE", "Price Per Share");
define("TERM_LENGTH", "Term Length");
define("VALUATION_CAP", "Valuation Cap");
define("CONVERSATION_DISCOUNT", "Conversion Discount");
define("WARRANT_COVERAGE", "Warrant Coverage");
define("RETURNT", "Return Type");
define("SELECT_RETURN", "Select Return");
define("GROSS_REVENUES", "Gross Revenues");
define("GROSS_PROFITS", "Gross Profits");
define("RETURN_PERCENTAGE", "Return Percentage");
define("MAXIMUM_RETURN", "Maximum Return");
define("INVESTMENT_AMOUNT", "Invesment Amount");
define("PAYMENT_FREQUENCY", "Payment Frequency");
define("SELECT_AYMENT_FREQUENCY", "Select Payment Frequency");
define("BIOMONTHLY", "Bimonthly");
define("QUARTERLY", "Quarterly");

define("PAYMENT_START_DATE", "Payback Start Date");
define("SELECT_PAYMENT_DATE_TYPE", "Select Payment Date Type");
define("EXACT_DATE", "Exact Date");
define("NUMBER_OF_MONTHS_FROM", "Number of Months From Finance Close");

define("NEXT_TO_STEP_2", "Next to Step 2");
define("UPDATE_TEAM_MEMBER", "Update Team Member");
define("COMPANY_CURRENTLY_FUNDRAISING", "Company currently fundraising");

define("SELECT_TEAM_MEMBER_TYPE", "Select Team Member Type");
define("RECOMMENDED_SIZE_800_450", "PNG, JPG or GIF <br/><br/><b>Recommended size:</b><br/>800 x 450 pixels");
define("OPTIONAL", "(Optional)");
define("PLUS_ADD_PERK", "+ Add Perk");
define("NEXT_TO_STEP_3", "Next to Step 3");
define("MAIN_VIDEO_OR_IMAGE_TO_BE_DISPLAYED", 'Main video or image to be displayed at the top of your '.$project.' page.');
define("YOUTUBE_OR_VIMEO", "Youtube or Vimeo");
define("PLEASE_CLICK_ON_TEAM_MEMBER_TO_DELETE", "Please Click on Team Member to Delete");
define("PLEASE_CLICK_ON_TEAM_MEMBER_TO_EDIT", "Please Click on Team Member to Edit");
define("PLEASE_CLICK_ON_PERK_TO_DELETE", "Please Click on Perk to Delete");
define("PLEASE_CLICK_ON_PERK_TO_EDIT", "Please Click on perk to Edit");
define("INVALID_DATA", "Invalid data");
define("UPDATE_PERK", "Update perk");
define("ENTER_AND_CONFIRM_YOUR_COMPANY", "Enter and confirm your company's bank account information to get funded in the end if your '".$project." is fully funded.");
define("BANK_ACCOUNT_DETAIL", "Bank Account Detail");
define("NAMEOF_BANK_AS_IT_APPEARS", "Name of bank as it appears on your paper checks.");
define("ACCOUNT_TYPE", "Account Type");
define("SELECT_ACCOUNT_TYPE", "Select Account Type");
define("ACCOUNT_NUMBERC", "Account Number");
define("EXAMPLE_ACCOUNT_NUMBER", "<strong>Example:</strong> 0001 23456 789");
define("CONFIRM_ACCOUNT_NUMBER", "Confirm Account No.");
define("CONFIRM_YOUR_ACCOUNT_NUMBER_BY_TYPING_AGAIN", "Confirm your account number by typing it again.");
define("ROUTING_NO", "Routing No.");
define("NINE_DIGIT_NUMBER_ON_THE_BOTTOM_LEFT", "9-digit number on the bottom left of your paper checks.<br>(<strong>Example:</strong> 123456789)");
define("FINISH", "Finish");
define("AN_EXECUTIVE_SUMMARY_AND_TERM_SHEET_ARE_REQUIRED", "An executive summary and term sheet are required. You can add any additional documents and mark them as Confidential to control investor access.");
define("EXECUTIVE_SUMMARY", "Executive Summary");
define("TERM_SHEET", "Term Sheet");
define("ADD_CUSTOM_DOCUMENT", "+ Add Custom Document");
define("NEXT_STEP_FOUR", "Next to Step 4");
define("ADD_ADDITIONAL_DOCS_OR_FILES", "Add additional docs or files that would be relevant to investors.");
///added by rakesh
define('PRE_FUNDING_DETAILS','Pre Funding Details');
define('ADD_YOUR_PRE_FUDING','Add your pre funding and current investors details. Manage your documents from same section.');
define('EQUITY_PREVIOUS_FUNDING','Previous Funding');
define('ADD_FUNDING_FROM_PREV','Add funding from previously closed rounds. Do not include funding within your current round.');
//define('FUNDING_SOURCE','Funding Source');
define('SELECT_FUNDING_SOURCE','Select Funding Source');
define('EQUITY_FUNDING_TYPE','Funding Type');
define('SELECT_EQUITY_FUNDING_TYPE','Select Funding Type');
define('FUNDING_AMOUNT','Funding Amount');
define('FUNDING_DATE','Funding Date');
define('ADD_PREV_FUNDING_SOURCE','+ Add Previous Funding Source');
define('UPDATE_PREV_FUNDING_SOURCE','Update Previous Funding Source');
define('PLEASE_CLICK_ON_PREVIUOS_FUNDING_TO_DELETE','Please Click on Previous Funding  to Delete');
define('PLEASE_CLICK_ON_PREVIUOS_FUNDING_TO_EDIT','Please Click on Previous Funding to Edit');
define('PLEASE_CLICK_ON_INVESTOR_TO_DELETE','Please Click on INVESTOR  to Delete');
define('PLEASE_CLICK_ON_INVESTOR_TO_EDIT','Please Click on INVESTORto Edit');
define('RAISE_SOURCE', 'Raise Source');
//define('INVESTORS', 'Investors');
define('SELECT_INVESTOR_TYPE', 'Select Investor Type');
define('INVESTOR_TYPE', 'Investor Type');
define('INVESTOR_PHOTO', 'Investor Photo');
define('INVESTOR_BIO', 'Investor Bio');

define('ADD_INVESTORS','+ Add Investor');
define('UPDATE_INVESTORS','Update Investor');


define('UPDATE_VIDEO','Update Video');
define('URL','URL');
define('ADD_MORE_VIDEO','Add more videos related to your '.$project.'. These will show up in the \'Docs & Media\' tab of your '.$project.' page.');
define('PLEASE_CLICK_ON_VIDEO_TO_EDIT','Please Click on Video to Edit');
define('PLEASE_CLICK_ON_VIDEO_TO_DELETE','Please Click on Video to Delete');

define('UPDATE_IMAGE','Update Image');
define('ADD_MORE_IMAGE','Add more images related to your '.$project.'. These will show up in the \'Docs & Media\' tab of your '.$project.' page.');
define('PLEASE_CLICK_ON_IMAGE_TO_EDIT','Please Click on IMAGE to Edit');
define('PLEASE_CLICK_ON_IMAGE_TO_DELETE','Please Click on IMAGE to Delete');
define('PLEASE_CLICK_ON_DEAL_DOCUMENT_TO_DELETE','Please Click on Deal Document to Delete');
define('PLEASE_CLICK_ON_DEAL_DOCUMENT_TO_EDIT','Please Click on Deal Document to Edit');
define('CONFEDENTIAL','Confidential');
define('ALL_ACCESS','All Access');
define('PRIVATEC','Private');
define('PUBLICC','Public');
define('PLEASE_UPLOAD_DOC_PPT_ZIP_PDF','Please upload a .doc, .ods, .pdf, .zip ,.ppt ,.xls ,.odt and .rtf file');
define('SORRY_THIS_FILE_IS_TOO_LARGE_PLEASE_SELECT_A_FILE_THAT_IS_LESS_THAN_TWO_MB', 'Sorry, this file is too large.  Please select a file that is less than 2 MB');
define('MINIMUM_RAISE_SHOULD_NOT_BE_LESS_THAN', 'Minimum Raise should not be less than');
define('MAXIMUM_RAISE_SHOULD_NOT_BE_GREATER_THAN', 'Maximum Raise should not be greater than');

define('NUMBER_OF_MONTHS','Number of Months');
define('PRICE_PER_SHARE_SHOULD_BE_BETWEEN', 'Price Per Share should be between');
define('EQUITY_AVAILABLE_SHOULD_BE_BETWEEN', ''.$project.' Available should be between');
define('COMPANY_VALUATION_SHOULD_BE_BETWEEN', 'Company Valuation should be between');

define('INTEREST_SHOULD_BE_BETWEEN', 'Interest should be between');
define('TERM_LENGTH_SHOULD_BE_BETWEEN', 'Term Length should be between');
define('VALUATION_CAP_SHOULD_BE_BETWEEN', 'Valuation Cap should be between');
define('CONVERSATION_SHOULD_BE_BETWEEN', 'Conversion Discount should be between');
define('WARRANT_COVERAGE_SHOULD_BE_BETWEEN', 'Warrant Coverage should be between');

define('RETURN_PERCENTAGE_SHOULD_BE_BETWEEN', 'Return Percentage should be between');
define('MAXIMUM_RETURN_SHOULD_BE_BETWEEN', 'Maximum Return should be between');
define('YOU_HAVE_ALREADY_SENT_INVITATION', 'You have already sent invitation to this email address');
define('PLEASE_SELECT_THE_ROLE_FOR_THIS_MEMBER', 'Please select the role for this member');

////******************* new added**************//
define("PENDING", "Pending");
define("SUCCESSFUL", "Successful");
define("CONSOLE_SUCCESS", "Console success");
define("FAILURE", "Unsuccessful");
define("GOAL", "Goal");
define("TEAM_MEMBERS", "Team Members");
define("GET_FUNDED", "Get funded");
define('GOAL_SHOUID_BE_BETWEEN', 'Goal should be between');
define('TO', 'to');
define("YOUR_PERKS", "Your Perks");
define("AMOUNT", "Amount");
define("DESCRIPTION", "Description");
define('LINK', 'Link');
define("WEBSITE", "Website");
define('CHANGE_IMAGE', 'Change Image');
define("EMAIL", "Email");
define("SEND_INVITES", "Send Invites");
define("PERSONAL_INFORMATION", "Personal Information");
define("ROUTING_NUMBER", "Routing Number");
define("ADDRESS", "Address");
define("GOOGLE_ANALYTICS_CODE", "Google Analytics Code ");
define("FUNDING_TYPE", "Funding Type");
define('YOUTUBE_LINK', 'Youtube Link');
define('PROJECT_TITLE', 'Project Title');
define("CURRENCY", "Currency");
define("ZIPCODE", "Postcode");
define("COVER_PHOTO", "Cover Photo");
define("THE_COMPANY_ASSETS_FIEID_IS_REQUIRED", "The Company Assets Field is required.");
define("THE_COMPANY_COVER_FIEID_IS_REQUIRED", "The Company Cover Photo Field is required.");
define("SHOW_ME_WHATS_LEFT_TO_DO", "Show me what left to do");
define("SAVE", "Save");
define('NOT_COMPLETED', 'Not completed');
define('GO_LIVE_CHECKLIST', 'Go live checklist');
define('HERE_IS_WHAT_IS_LEFT_TO_DO_BEFORE_YOU_GO_LIVE', 'Here is what is left to do before you go live.');
define('SELECT_COMPANY', 'Select Company');
define('ADD_NEW_COMPANY', 'Add New Company');
define('PLEASE_SELECT_A_COMPANY_OR_ADD_NEW_COMPANY', 'Please Select a Company or Add New Company');
define('PERK_AMOUNT', 'Perk Amount');
define('IMAGE_UPLOADED_SUCCESSFULLY', 'Image uploaded successfully');
define('DELETE_COMMENT_CONFIRMATION','Are you sure,You want to delete this comment?');
define('RECORD_UPDATED_SUCCESSFULLY', 'Record updated successfully');
define('DELETE_PROJECT_CONFIRMATION','Are you sure ,You want to delete this '.$project.'.?');
define('IMAGE_MUST_BE_GREATER_THAN','Image Must be greater than  800px X 450px');
define('PERK_AMOUNT_SHOULD_BETWEEN', 'Perk amount should between %s and %s for each perk.');
define("RECORD_UPDATED_SUCCESS", "Record updated successfully.");
define('INVALID_VIDEO_URL', 'Invalid video URL');
define('COMPLETE_MY_COMPAIGN', 'Complete My '.$project.'');

define("KEEP_THE_FUNDS_YOU_RAISE_EVEN_IF_YOU_DONT_REACH_YOUR_GOAL", "Keep the funds you raise, even if you don’t reach your goal.");
define("GOOD_IF_YOUR_PROJECT_CAN_USE_WHATEVER_AMOUNT_OF_FUNDS_YOU_CAN_RAISE", "Good if your ".$project." can use whatever amount of funds you can raise.");
define("YOU_CAN_RECEIVE_FUNDS_VIA_DIRECT_CREDIT_CARD_OR_PAYPAL", "You can receive funds via direct credit card or PayPal");
define('WE_CHARGE_FEE_IF_YOU_REACH_YOUR_GOAL_AND_FEE_IF_YOU_DONT', "We charge a %s%% fee if you reach your goal, and a %s%% fee if you don't.");
define("CONTRIBUTIONS_ARE_REFUNDED_IF_YOU_DONT_MEET_YOUR_GOAL", " Contributions are refunded if you don't meet your goal.");
define("GOOD_IF_YOUR_PROJECT_CANT_HAPPEN_UNLESS_YOU_REACH_YOUR_GOAL", " Good if your ".$project." can't happen unless you reach your goal.");
define("YOU_RECEIVE_FUNDS_VIA_PAYPAL_ONLY", "  You receive funds via PayPal only");
define("YOUR_MAXIMUM_COMPAIGN_LENGTH_IS_DAYS", "  Your maximum ".$project." length is %s days");
define("WE_CHARGE_FEE_IF_YOU_REACH_YOUR_GOAL_AND_FEE_IF_YOU_DONT_BUT_YOU_GET_NO_MONEY", "We charge a %s%% fee if you reach your goal, and no fee if you don't, but you get no money");
define("INELASTIC_FUNDING", "Inelastic Funding");
define('INELASTIC_FUNDING_FIXED', 'Inelastic Funding (Fixed)');
define('ELASTIC_FUNDING_FLEXIBLE', 'Elastic Funding (Flexible)');
define("DONATIONS_ARE_REFUNDED_IF_TARGET_IS_NOT_MET", " ".$funds_plural." are refunded if target is not met.");
define("THIS_IS_GOOD_IF_YOUR_PROJECT_CANT_COMMENCE_UNLESS_YOUR_TARGET_IS_MET", "This is good if your project can’t commence unless your target is met.");
define('COMPLETED', 'Completed');
define('PLEASE_UPLOAD_A_JPG_PNG_GIF_FILE', 'Please upload a .jpg, .png, .gif file');
define('DOCUMENT', 'Document');
define('CUSTOMIZE_YOUR_EQUITY_URL', 'Customize your '.$project.' Crowdfunding URL. You may include letters, numbers, dashes (-), or underscores (_) only.');
define('PLEASE_SELECT_ONE_DEAL_TYPE', 'Please select one Deal Type');
define('MAXIMUM_RAISE_AMOUNT_MUST_NOT_BE_GREATER_THAN_YOUR_GOAL', 'Maximum raise amount must not be greater than your goal amount.');
define('MIMIMUM_RAISE_AMOUNT_MUST_NOT_BE_GREATER_THAN_YOUR_MAXIMUM_AMOUNT', 'Minimum raise amount must not be greater than your Maximum raise amount.');
define('PLEASE_ENTER_A_DOCUMENT', 'Please enter a document name');
define('PLEASE_SELECT_A_DOCUMENT_TYPE', 'Please select a document type');
define('YOU_CAN_NOT_INVITE_YOURSELF', 'You can not invite yourself');
define('SORRY_THIS_FILE_IS_TOO_LARGE_PLEASE_SELECT_A_JPG_FILE_THAT_IS_LESS_THAN_TWO_MB_OR_TRY_RESIZING_IT_USING_A_PHOTO_EDITOR', 'Sorry, this file is too large.  Please select a .jpg file that is less than 2 MB or try resizing it using a photo editor.');
define('YEAR_FOUNDED_CANNOT_BE', 'Year founded cannot be future year');
define("GO_LIVE_AND_GET_FUNDED", "Go live and get funded!");
define("YOUR_CAMPAIGN_IS", "Your campaign is");
define("NOT_READY", "Not Ready");
define("READY", "Ready");
define("TO_GO_LIVE_QUITE_YET", "To go live quite yet.");
define("ALLOWED_INVESTOR", "Who can invest on your offering?");
define("ONLY_ACCREDIATED", "Only Accredited Investors");
define("BOTH_ACCREDIATED", "Both (Both Accredited and Non-Accredited investors can invest)");

// project detail section 
define("PROJECT_DETAIL", "Project Detail");
define("PROJECT_DETAIL_BOTTOM_TEXT", "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.");
//define("PROJECT_NAME", "Project Name");
define("PROJECT_DESCRIPTION", "Project Description");
define("PROJECT_TYPE", "Project Type");
define("PROJECT_SUB_TYPE", "Project Sub-Type");
define("STREET", "Street");
//define("CITY", "City");
//define("STATE", "State");
//define("COUNTRY", "Country");
define("PROPERTY_SIZE", "Property Size");
define("LOT_SIZE", "Lot Size");
define("YEAR_BUILT", "Year Built");

//investment summary and market summary
define("MARKET_SUMMARY", "Market Summary");
define("HIGHLIGHT_1", "Highlight");
define("HIGHLIGHT_2", "Highlights #2");
define("HIGHLIGHT_3", "Highlights #3");
define("HIGHLIGHT_4", "Highlights #4");
define("INVESTMENT_SUMMARY", "Investment Summary");
define("SUMMARY", "Summary");
define("MARKET_SUMMARY_TEXT", "Market summary text");
define("INVESTMENT_SUMMARY_TEXT", "Investment summary text");


define("UPLOAD_YOUR_COVER_PHOTO", "Upload your cover photo.");
define("COMPANY_URL", "Company URL");
define("FEATURED_PROJECT", "Featured ".$project."");
define("PROJECT_SUMMARY", "".$project." Summary");
define("ADD_FEATURED_PROJECT", "+ Add Featured ".$project."");
define("UPDATE_FEATURED_PROJECT", "Update Featured ".$project."");
define("JOIN_INVESTOR_NETWORK", "Join Network");
define("YOU_ARE_A_MEMBER", "You Are a Member");
define("LEAVE_NETWORK", "Leave Network");

define("INVESTOR_NETWORK", "Investor Network");
define("YOU_MUST_BE_A_MEMBER_OF_THE_INVESTOR", "You must be a member of the investor network to view the other investors");
define("ACCREDITED_INVESTORS", "Accredited Investors");
define("ACCREDITED_AND_NON_ACCREDITED_INVESTORS", "Accredited & Non-Accredited Investors");
define("OPEN_TO", "Open To");

define("PROJECTED_RETURN_CALCULATOR", "Projected Return Calculator");
define("ABOUT_THE_SPONSER", "About the Sponsor");
define("PREVOIUS_FUNDING", "Previous Funding");
define('RECOMMENDED_FILE_FORMAT_SHOULD_BE_IMG_DOC', 'Recommended file format should be .doc, .ods, .pdf, .zip, .ppt, .xls, .odt and .rtf .');

define('SORRY_THIS_FILE_IS_TOO_LARGE_PLEASE_SELECT_A_JPG_FILE_THAT_IS_LESS_THAN_MB_OR_TRY_RESIZING_IT_USING_A_PHOTO_EDITOR', 'Sorry, this file is too large.  Please select a .jpg file that is less than %s MB or try resizing it using a photo editor.');
define('SORRY_THIS_FILE_IS_TOO_LARGE_PLEASE_SELECT_A_FILE_THAT_IS_LESS_THAN_MB', 'Sorry, this file is too large.  Please select a file that is less than %s MB');

define("PNG_JPG_OR_GIF_RECOMENDED_SIZE_FROM_ADMIN", "PNG, JPG or GIF <br/><br/><b>Recommended size:</b><br/>170 x 170 pixels <br> Max Size: %s MB");
define("ADD_HIGHLIGHT", "+ ADD HIGHLIGHT");
define("REQUIRED_FIELD", "Required Field");

/*social_networkig_lang.php*/
define('MANAGE_YOUR_SOCIAL_NETWORK_DETAILS', 'Manage your Social Network Details');
define('FACEBOOK_URL', 'Facebook URL');
define('TWITTER_URL', 'Twitter URL');
define('LINKEDIN_URL', 'Linkedln URL');
define('GOOGLE_PLUS_URL', 'Google Plus URL');
define('BASECAMP_URL', 'BaseCamp URL');
define('YOUTUBE_URL', 'Youtube URL');
define('MYSPACE_URL', 'MySpace URL');
define('INVALID_FACEBOOK_LINK','Invalid facebook Link');
define('INVALID_TWITTER_LINK','Invalid twitter Link');
define('INVALID_LINKEDIN_LINK','Invalid linkedin Link');
define('INVALID_BANDCAMP_LINK','Invalid bandcamp Link');
define('INVALID_YOUTUBE_LINK','Invalid youtube Link');
define('INVALID_MYSPACE_LINK','Invalid myspace Link');
define('INVALID_IMDB_LINK','Invalid imdb Link');

/*message_lang.php*/
define('SEND_MESSAGE','Send message');
define('SUBJECT','Subject');
define('MESSAGE_LIST','Message list');
define('SENDER','Sender');
define('RECEIVER','Receiver');
define('MESSAGE_CONVERSATION','Message Conversation');
define('VIEW_CONVERSATION','View Message');
define('CONVERSATION_MESSAGE','Conversation Messages');
define('CONVERSATION_OF_MESSAGE','Conversation Of Message');
define('CONVERSATION_DATE','Conversation Date');
define('SEND_A_MESSAGE_TO','Send a message to');
define('YOUR_MESSAGE_HAS_BEEN_SUCCESSFULLY','your message has been successfully sent');
define('SUBJECT_IS_REQUIRED','Subject is required');
define('MESSAGE_IS_REQUIRED','Message is required');
define('SEND_A_REPLY','Reply');
define('CONVERSATION','Conversation');

/*cron_lang.php*/
define('CRON_YOUR_PAYMENT_PROCESS_VIOLATED_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU','Hello %s ,<br/><br/>Your payment process is violated %s <br/><br/><br/>Please check your settings. Try again.<br/><br/><br/>Thank You.');

define('CRON_HELLO_ADMINISTRATOR_APIERROR7_PAYMENT_PROCESS_VIOLATED_DUE_TO_ERRORMESSAGE_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU','Hello Administrator,<br/><br/> APIError(7). Payment process is violated due to %s <br/><br/>Donar Name : %s <br/><br/>Donar Email : %s <br/><br/>Payee Email : ".$donar_email." <br/><br/>Please check your settings. Try again.<br/><br/><br/>Thank You.');

define('CRON_HELLO_ADMINISTRATOR_APIERROR8_PAYMENT_PROCESS_VIOLATED_DUE_TO_ERROR_MESSAGE_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU','Hello Administrator,<br/><br/> APIError(8). Payment process is violated due to %s <br/><br/>Donar Name : %s <br/><br/>Donar Email : %s <br/><br/>Payee Email : %s <br/><br/>Please check your settings. Try again.<br/><br/><br/>Thank You.');

define('CRON_HELLO_YOUR_PAYMENT_PROCESS_VIOLATED_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU','Hello %s,<br/><br/>Your payment process is violated. <br/><br/><br/>Please check your settings. Try again.<br/><br/><br/>Thank You.');

define('CRON_HELLO_ADMINISTRATOR_CATCHEXCEPTION8_PAYMENT_PROCESS_VIOLATED_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU','Hello Administrator,<br/><br/> catchException(8). Payment process is violated.<br/><br/>Donar Name : %s <br/><br/>Donar Email : %s <br/><br/>Payee Email : %s <br/><br/>Please check your settings. Try again.<br/><br/><br/>Thank You.');

define('CRON_HELLO_PAYMENT_PROCESS_NOT_COMPLETED_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU','Hello %s ,<br/><br/>Your payment process is not completed.<br/><br/><br/>Please check your settings. Try again.<br/><br/><br/>Thank You.');

define('CRON_HELLO_ADMINISTRATOR_APIERROR10_PAYMENT_PROCESS_NOT_COMPLETED_DUE_TO_CASE_IS_NOT_MATCH_WITH_PAYMENTEXECSTATUS_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANK_YOU','Hello Administrator,<br/><br/> APIError(10). Payment process is not completed due to case is not match with paymentExecStatus. <br/><br/>Donar Name : %s <br/><br/>Donar Email : %s <br/><br/>Payee Email : %s <br/><br/>Please check your settings. Try again.<br/><br/><br/>Thank You.');


/*invest_lang.php*/
define('DONATION_AMOUNT','Donation Amount');
define('ADD_SHIPPING_ADDRESS','Add shipping address');
define('FORM_FUND','Frm_fund');
define('DONATION','Donation');
define('STEP','Step');
define('PICK_YOUR_REWARD','Pick your reward');
define('PICK_A_PERK','Pick a perk');
define('NO_PERK_JUST_A_CONTRIBUTION','No Perk, just a investment!');
define('ENTER_YOUR_TOTAL_CONTRIBUTION_AMOUNT','Enter your total contribution amount.');
define('YOUR_INFORMATION','Your Information');
define('CONFIRM_EMAIL','Confirm E-mail');
define('I_WOULD_LIKE_TO_RECEIVE_WEEKLY_NEWSLETTER','I would like to receive weekly newsletter.');
define('SHIPPING_INFORMATION','Shipping information');
define('PERSONALIZATION','Personalization');
define('SELECT_HOW_YOU_WOULD_LIKE_YOUR_CONTRIBUTION_TO_BE_DESPLAYED_ON_THE_PUBLIC_CAMPAIN_PAGE','Select how you would like your contribution to be displayed on the public '.$project.' page.');
define('GIFT','Gift');
define('I_WOULD_LIKE_TO_MAKE_THIS_CONTRIBUTION_FOR_SOMEONE_ELSE','I would like to make this contribution for someone else.');
define('RECIPIENT','Recipient');
define('PRIVACY','Privacy');
define('VISIBLE_SHOW_YOUR_OR_THEIR_NAME_AND_AMOUNT','Visible: Show your (or their) name and amount');
define('IDENTITY_ONLY_SHOW_YOUR_OR_THEIR_NAME_BUT_HIDE_THE_AMOUNT','Identity-Only: Show your (or their) name, but hide the amount');
define('ANONYMOUS','Anonymous');
define('ADD_A_PUBLIC_COMMENT_GREAT_FOR_VOICING_YOUR_SUPPORT_AND_PERSONALYSING_YOUR_CONTRIBUTION','Add a public '.$comment.' - great for voicing your support and personalizing your contribution.');
define('PUBLIC_COMMENT','Public Comment');
define('I_WOULD_LIKE_TO_RECEIVE_EMAILS_WITH_UPDATES_FROM_THE_CAMPAIGN_OWNER','I would like to receive emails with updates from the '.$project.' owner.');
define('PAYMENT_METHOD','Payment Method');
define('NO_GATEWAY_AVAILABLE','No Gateway Available');
define('CONTRIBUTE_VIA_PAYPAL','Contribute via PayPal');
define('PAYMENT','Payment');
define('SHIPPING_ADDRESS','Shipping Address');

define('DONATION_MIN_AMOUNT','Donation amount should be greater than %s.');

define('DONATION_MAX_AMOUNT','Donation amount should be less than %s.');

define('INVALID_DONATION_AMOUNT','Invalid donation amount.');

define('EMAIL_SHOULD_NOT_BE_BLANK','Email should not be blank.');

define('INVALID_EMAIL_FORMAT','Invalid email format.');

define('EMAILS_NOT_SAME','Email and Confirm Email should be same.');

define('ENTER_SHIPPING_ADDRESS','Enter Shipping Address.');

define('ENTER_RECIPIENT','Enter Recipient');

define('SELECT_ONE_GATEWAY', 'Please select at least one gateway.');

define('CONTRIBUTE','Contribute');
define('PAYMENT_KEY','Payment Key');
define('TRANSACTION_DETAIL','Transaction Detail');

define('CT_DT','Ct/Dt');
define('SHIPPING','Shipping');
define('WITHDRAWAL_DETAILS','Withdrawal Details');

define('VIEW_DETAIL','View Details');
define('YOUR_DONATION_AMOUNT_IS','Your Donation Amount is');
define('YOU_SELECTED','You selected');
define('ESTIMATED_DELIVERY','Estimated delivery');
define('TOTAL_CONTRIBUTION','Total Contribution');

define('JUST_FOR_THE_CAMPAIGNER_TO_SEND','After clicking this button, you’ll be redirected to payment gateway site where you can pay.');
define('AFTER_CLICKING_THIS_BUTTON','After clicking this button, you’ll be redirected to payment gateway site where you can pay.');
define('CONTRIBUTE_VIA_STRIPE','Contribute via Stripe');
define('EXPIRY_MONTH','Expiry Month');
define('EXPIRY_YEAR','Expiry Year');
define('CVC_NUMBER','CVC Number');
define('MONTH','Month');
define('YEAR','Year');
define('MY_FUNDIND','My funding');
define('INCOMING_DONATION','Incoming donation');

//new added
define('ADD_PERK_INVEST','Add Perk');
define('CHANGE_PERK','Change Perk');
define('CLAIMED', 'Claimed');

/// investor admin steps
define('INVESTER_WILL_DOWNLOAD_CONTRACT_DOCUMENTS', ' will download contract documents');
define('FROM_HERE', 'from here');
define('TO_SIGN_AND_SEND_BACK', 'to sign and send back');
define('DOWNLOAD_PAYMENT_RECEIPT_FROM_HERE', 'Download payment receipt from here');
define('CONFIRM_INVESTMENT_AMOUNT_YOU_HAVE_RECEIVED', 'Confirm '.$funds.' amount you have received by bank wire or check');
define('THIS_STEP_IS_PENDING', 'This step is pending');
define('INVESTER_WILL_UPLOAD_SIGNED_COPY_OF_CONTRACT', ' will upload signed copy of contract documents and you will be able to download signed document from this step');
define('DOWNLOAD_SIGNED_CONTRACT_DOCUMENT_FROM_HERE', 'Download signed contract document from here');
define('CONFIRM_DOCUMENTS_ARE_SIGNED_WELL_AND_UPTO_DATE', 'Confirm signed contract documents are signed well and upto date');
define('CLICK_COMPLETE_BUTTON_TO_MARK_THIS_STEP_AS_COMPLETED_BELOW', 'Click "Approve" button to mark this step as completed below');
define('THIS_STEP_IS_CONFIRMED_AND_COMPLETED', 'This step is confirmed & completed');
define('INVESTER_WILL_GO_TO_BANK_TO_WIRE_TRANSFER', '%s will go to bank to wire transfer or electronic bank transfer the total '.$funds.' amount of %s with following bank account information.');
define('YOU_WILL_GO_TO_BANK_TO_WIRE_TRANSFER', 'Please make '.$funds.' of %s in following bank account and provide acknowledgement of same in next step.4.');
define('INVESTOR_WILL_UPLOAD_PAYMENT_RECEIPT_SNAP', 'Investor will upload payment receipt snap or tracking details for the payment he/she made');
define('UPON_COMPLETION_OF_THIS_STEP_WILL_BE_CONFIRMED', 'Upon completion of this step, %s will be confirmed as investor and will be listed under "Investors" tab on '.$project.' page. As well as '.$funds.' amount will be added automatically added in raised '.$funds.' amount of %s');
define('NOTES_FROM_INVESTOR', 'Now as %s is legal investor in this campaign, its time to send share certificate to the investor by post email or email, please prepare share certificates and write note below on how %s will receive share certificates');
define('NOW_AS_INVESTOR_IS_LEGAL_INVESTOR_IN_THIS_CAMPAIGN', 'Now as %s is legal investor in this '.$project.', its time to send share certificate to the investor by post email or email, please prepare share certificates and write note below on how %s will receive share certificates');
define('RECEIPT_CONFIRMED', 'Receipt Confirmed');
define('PAYMENT_INFORMATION', 'Payment Information');
define('ONCE_ALL_IS_VERIFY_AND_DONE_WILL_APPROVE_AND_CONFIRM', 'Once all is verify and done %s team will approve and confirm this step');
define('ONCE_THIS_STEP_IS_APPROVED_AND_CONFIRMED', 'Once this step is approved and confirmed please proceed to the step 3. Payment');
define('TEAM_WILL_DOWNLOAD_AND_VERIFY_YOUR_SIGNED', '%s team will download and verify your signed contract you uploaded from here');
define('YOUR_NOTE', 'Your Note');
define('CLICK_HERE_TO_VIEW_PAYMENT_REC', ' Click here to view your payment receipt:');
define('YOU_WILL_SANNOUNCE_SHARE_RECIPT', 'you will confirm and mark this step to announce he/she has received share certificates and other legal documents.');
define('RECEIVE_SHARES_DOCUMENTS', 'Receive Shares/Documents');
define('INVESTOR_WILL_CONFIRM_SHARE_DOCUMENTS', '%s  will confirm and mark this step to announce he/she has received share certificates and other legal documents.');
define('NOTE','Note');
define('APPROVED','Approved');
define('INV_ENTER_YOUR_BANK_AND_CHECK_INFORMATION','Enter/Edit your bank and check information to receive %s from investor.');
define('REJECT_COMMENT','Reject Comment');
define('REJECT_REASON','Enter Reason for Rejection.');
define('SENDMESSAGE_TO_SITE_TEAM','Send Message to %s Team');
define('SENDMESSAGE_TO_INVESTOR_NAME','Send message to %s');
define('PROCESS','Process');

define('YOUR_PAYMENT_ACKNOWLEDGEMENT_INFORMATION_HAS_BEEN_SUBMITTED_SUCCESSFULLY', 'Your payment acknowledgement information has been submitted successfully. ');
define('TEAM_WILL_REVIEW_AND_CONFIRM_YOUR_PAYMENT_SHORTLY', ' team will review and confirm your payment shortly.');
define('STEP_2_HAS_BEEN_CONFIRMED_AND_COMPLETED_PROCEED_TO_THE_STEP_3_PAYMENT', 'Step 2. has been confirmed and completed. Proceed to the Step 3. Payment');
define('STATUS_HAS_BEEN_REJECTED_SUCCESSFULLY', 'Status has been Rejected successfully');
define('YOURSHARE_DOCUMENT_TRANSMISSION_INFORMATION_SUBMITTED_AND_SENT', 'Your share/document transmission information submitted and sent');
define('STEP_4_HAS_BEEN_CONFIRMED_AND_COMPLETED_PROCEED_TO_THE_STEP_5_SHARES_DOCUMENTS', 'Step 4. has been confirmed and completed. Proceed to the Step 5. Shares/Documents');	
define('SHARE_DOCUMENT_DELIVERY_HAS_BEEN_CONFIRMED_AND_MARKED_AS_DELIVERED_SUCCESSFULLY', 'Share/Document delivery has been confirmed and marked as delivered successfully.');
define('CLICK_HERE_TO_VIEW_YOUR_PAYMENT_RECEIPT', 'Click here to view your payment receipt');
define('YOUR_INVESTMENT_DONE_SUCCESSFULLY','Your '.$funds.' done successfully.');

/*follower_lang.php*/
define('FOLLOWING_PROJECTS','Following '.$project.'');
define('THERE_ARE_NO_FOLLOWING_PROJECT','There are no following '.$project.'.');

/*donation_lang.php*/

define('MY_FUNDING', 'My Funding');

/*admin_equity_lang.php*/

define('CONTRACT_DOCUMENT','Contract Document');
define('CONTRACT_MANAGE','Contract Manage');
define('OFFLINE','Offline');
define('EQUITY_NAME',''.$project_name.' Name');
define('HISTORY','History');
define('DOCUMENT_NAME','Document Name');
define('CURRENT_STATUS','Current Status');
define('DOWNLOAD_ATTACH_FILE','Download Attach File');

define('PROCESSING_CONTRACT','Processing Contract');
define('DOWNLOADED_CONTRACT','Downloaded Contract');
define('UPLOADED_SIGNED_CONTRACT','Uploaded Signed Contract');
define('SIGNED_CONTRACT_APPROVE','Signed Contract Approve');
define('PAYMENT_PROCESS','Payment Process');
define('PAYMENT_RECIEPT_UPLOADED','Payment Reciept Uploaded');
define('PAYMENT_CONFIRMED','Payment Confirmed');
define('TRACKING_SHIPMENT','Tracking Shipment');

define('SHIPMENT','Shipment');
define('NO_RECORDS_AVAILABLE','No Records available');
define('YOUR_MESSAGE_HAS_BEEN_SUCCESSFULLY_SENT','Your message has been successfully sent.');
define('ACKNOWLEDGE_NOTE','Acknowledge Note');

define('COMPANY_CATEGORY_LIST','Company Category List');
define('COMPANY_CATEGORY_ALREADY_EXIST','Company Category Alredy Exist.');
define('COMPANY_CATEGORY_NAME','Company Categoty Name');
define('INSERT','insert');
define('UPDATE_MSG','update');
define('ADD_COMPANY_CATEGORY','Add Company Category');
define('THEME','Theme');
define('EDIT_COMPANY_CATEGORY','Edit Company Category');
define('FUNDING_SOURCE','Funding Source');
define('DOCUMENT_TYPE','Document Type');
define('COMPANY_INDUSTRY_LIST','Company Industry List');
define('ADD_COMPANY_INDUSTRY','Add Company Industry');
define('DELETE_INDUSTRY_CONFIRMATION','Are you sure,You want to delete this industry?');
define('EDIT_COMPANY_INDUSTRY','Edit Company Industry');
define('COMPANY_INDUSTRY_ALREADY_EXIST','Company Industry Name Already Exist.');
define('FUNDING_SOURCE_LIST','Funding Source List');
define('DELETE_FUNDING_SOURCE_CONFIRMATION','Are you sure,You want to delete this funding source?');
define('ADD_FUNDING_SOURCE','Add Funding Source');
define('EDIT_FUNDING_SOURCE','Edit Funding Source');
define('FUNDING_SOURCE_ALREADY_EXIST','Funding Source Alredy Exist.');
define('DELETE_FUNDING_TYPE_CONFIRMATION','Are you sure,You want to delete this funding type?');
define('FUNDING_TYPE_LIST','Funding Type List');
define('ADD_FUNDING_TYPE','Add Funding Type');
define('EDIT_FUNDING_TYPE','Edit Funding Type');
define('FUNDING_TYPE_ALREADY_EXIST','Funding Type Alredy Exist.');
define('DELETE_DOCUMENT_TYPE_CONFORMATION','Are you sure,You want to delete this document type?');
define('ADD_DOCUMENT_TYPE','Add Document Type');
define('EDIT_DOCUMENT_TYPE','Edit Document Type');
define('DOCUMENT_TYPE_LIST','Document Type List');
define('DOCUMENT_TYPE_ALREADY_EXIST','Document Type Alredy Exist.');
define('DELETE_ACCOUNT_TYPE_CONFORMATION','Are you sure,You want to delete this account type?');
define('ACCOUNT_TYPE_LIST','Account Type List');
define('ADD_ACCOUNT_TYPE','Add Account Type');
define('EDIT_ACCOUNT_TYPE','Edit Account Type');
define('ACCOUNT_TYPE_ALREADY_EXIST','Account Type Alredy Exist.');
define('DELETE_TEAM_MEMBER_CONFIRMATION','Are you sure,You want to delete this team member type?');
define('TEAM_MEMBER_TYPE_LIST','Team Member Type List');
define('ADD_TEAM_MEMBER_TYPE','Add Team Member Type');
define('EDIT_TEAM_MEMBER_TYPE','Edit Team Member Type');
define('TEAM_MEMBER_TYPE_ALREADY_EXIST','Team Member Type Alredy Exist.');

//front equity

define('FILE_UPLOAD_SUCCESS','Your contract copy has been submitted successfully.');
define('PLEASE_UPLOAD_ZIP_OR_PDF_FILE','Please upload zip or pdf file');
define('SORRY_THIS_FILE_IS_TOO_LARGE','Sorry this file is too large.');
define('CONGRATULATIONS','Congratulations!');
define('YOU_ARE_ALMOST_THERE','You are almost there');
define('WHAT_NEXT_PLEASE_FOLLW_THE_STEP_BELOW','What Next? Please follw the step below.');
define('SIGN_CONTRACT',''.$funds.' Contract');
define('YOU_MAY_HAVE_RECEIVED_AN_EMAIL_WITH_CONTRACT_YOU_NEED_TO_SIGN_AND_SEND_BACK_OR','You may have received an email with contract you need to sign and send back or ');
define('DOWNLOAD_FROM_HERE','download from here');
define('THIS_STEP_IS_CONFIRMED_COMPLETED','This step is confirmed & completed');
define('UPLOAD_CONTRACT','Upload Contract');
define('PLEASE_UPLOAD_SIGNED_CONTRACT_FROM_HERE_AND_WAIT_UNTIL_EQUITY_CROWDFUNDING_TEAM_APPROVES_AND_MARK_IT_AS_CONFIRMED_UPLOAD_CONTRACT_HERE','Please upload signed contract from here and wait until '.$site_name.' team approves and mark it as confirmed. Upload contract here');
define('PLEASE_DO_NOT_PROCEED_TO_STEP_UNTIL_STEP_IS_CONFIRMED','Please do not proceed to Step.3 until Step.2 is confirmed.');
define('USE_FOLLOWING_BANK_INFORMATION_TO_MAKE_PAYMENT','Use following bank information to make payment.​');
define('OR_USE_FOLLOWING_DETAILS_TO_WRITE_US_CHECK','OR Use following details to write us Check');
define('ACKNOWLEDGE','Acknowledge');
define('UPLOAD_PAYMENT_RECEIPT_SNAP_OR_TRACKING_DETAILS_FOR_THE_PAYMENT_YOU_MADE_UPLOAD_RECEIPT','Upload payment receipt snap or tracking details for the payment you made. Upload receipt');
define('CONFIRMED','Confirmed');
define('PAYMENT_IS_APPROVED_AND_CONFIRMED_BY_EQUITY_CROWDFUNDING_TEAM','Payment is approved and confirmed by '.$site_name.' team.');
define('TRACK_YOUR_SHIPMENT','Track your shipment');
define('WRITE_A_NOTE','Write a note');
define('EQUITY_LIST',''.$project_name.' List');

define('DEAL_TYPE_SETTING_LIST','Deal Type Setting List');
define('DEAL_TYPE_NAME_ALREADY_EXIST','Deal Type Name Already Exist.');
define('DEAL_TYPE_SETTING','Deal Type Setting');
define('EDIT_DEAL_TYPE_SETTING','Edit Deal Type Setting');
define('MIN_PRICE_PER_SHARE','Min Price Per Share');
define('MAX_PRICE_PER_SHARE','Max Price Per Share');
define('MIN_EQUITY_AVAILABLE','Min '.$project_name.' Available');
define('MAX_EQUITY_AVAILABLE','Max '.$project_name.' Available');
define('MIN_COMPANY_VALUATION','Min Company Valuation');
define('MAX_COMPANY_VALUATION','Max Company Valuation');
define('DEAL_TYPE_NAME','Deal Type Name');
define('DEAL_TYPE_DESCRIPTION','Deal Type Description');
define('DEAL_TYPE_ICON','Deal Type Icon');
define('PLEASE_SELECT_IMAGE_MORE_THAN_1300','Please select image more than 1300 * 600');
define('MIN_CONVERTIBLE_INTEREST','Min Convertible Interest');
define('MAX_CONVERTIBLE_INTEREST','Max Convertible Interest');
define('MIN_VALUATION_CAP','Min Valuation Cap');
define('MAX_VALUATION_CAP','Max Valuation Cap');
define('MIN_WARRANT_COVERAGE','Min Warrant Coverage');
define('MAX_WARRANT_COVERAGE','Max Warrant Coverage');
define('MIN_CONVERTIBLE_TERM_LENGHT','Min Convertible Term Length');
define('MAX_CONVERTIBLE_TERM_LENGHT','Max Convertible Term Length');
define('MIN_CONVERSATION_DISCOUNT','Min Conversation Discount');
define('MAX_CONVERSATION_DISCOUNT','Max Conversation Discount');
define('MIN_DEBT_INTEREST','Min Debt Interest');
define('MAX_DEBT_INTEREST','Max Debt Interest');
define('MIN_DEBT_TERM_LENGTH','Min Debt Term Length');
define('MAX_DEBT_TERM_LENGTH','Max Debt Term Length');
define('MIN_MAXIMUM_RETURN','Min Maximum Return');
define('MAX_MAXIMUM_RETURN','Max Maximum Return');
define('MIN_RETURN_PERCENTAGE','Min Return Percentage');
define('MAX_RETURN_PERCENTAGE','Max Return Percentage');

define('INVESTOR_TYPE_LIST','Investor Type List');
define('ADD_INVESTOR_TYPE','Add Investor Type');
define('EDIT_INVESTOR_TYPE','Edit Investor Type');
define('INVESTOR_TYPE_NAME','Investor Type Name');
define('INVESTOR_TYPE_NAME_ALREADY_EXIST','Investor Type Name Already Exist.');
define('DELETE_INVESTOR_TYPE_CONFIRMATION','Are you sure,You want to delete this Investor type?');

define('INVESTMENT_STEPS_FOR_ADMIN','Investment Steps for Admin');
define('NO_ACTION_REQUIRED_IN_THIS_STEP_FOR_YOU','No action required in this step for you.');
define('ACKNOWLEDGEMENT','Acknowledgement');
define('CONFIRMATION','Confirmation');
define('RECEIPT_CONFIRMATION','Receipt Confirmation');

define('URL_COMPANY_CATETORY','URL Company Category');
define('LANGUAGE_ID','Language ID');
define('ENGLISH','English');
define('IMAGE_UPLOAD','Image upload');

define('EQUITY_PERK',''.$project_name.' Perk');
define('PERK_TITLE','Perk Title');
define('TOTAL_CLAIM','Total Claim');
define('EQUITY_UPDATES',''.$project_name.' Updates');
define('NO_UPDATES','No Updates');
define('EQUITY_DONATION',''.$project_name.' Donation');
define('EQUITY_COMMENT',''.$project_name.' Comment');
define('NO_COMMENTS','No Comment');
define('EQUITY_REPORT',''.$project_name.' Report');
define('PROGRESS','Progress');
define('INVESTOR','Investor');
define('COMPANY','Company');
define('CREATE_BY','Created by');
define('EQUITY_OVERVIEW',''.$project_name.' Overview');
define('INVESTMENT','Investment');
define('GALLERY','Gallery');
define('DOWNLOAD_CONTRACT_DOCUMENT','Download Contract Document');
define('CONTRACT_DOCUMENT_SUCCESSFULLY_UPLOADED','Contract Document successfully Uploaded');
define('UPLOAD_CONTRACT_DOCUMENT','Upload Contract Document');
define('AS_YOU_HAVE_TO_DECIDED_TO_DECLINE','As you have decided to decline this %s, please provide reason why you declined this %s, this will better guide %s to help improve his %s and resubmit for review.');
define('SAVE_THIS_REASON_TO_USE_ON_OTHER','Save this reason to use on other');
define('REASON_TO_DECLINE_THIS','Reason to decline this');
define('CANCEL','Cancel');
define('WRITE_REASON_WHY_YOU_DEACTIVATED_THIS','Write reason why you deactivated this');
define('HIDDEN_MODE','Hidden Mode');
define('AS_YOU_HAVE_TO_DECIDED_TO_INACTIVE','As you have decided to inactive this %s, please choose following options before you do it.');
define('SELECT_THIS_OPTION_IF_YOU_WANT_TO_HIDE','Select this option if you want to hide %s completely from website., no users will be able to view this %s.');
define('SELECT_THIS_OPTION_IF_YOU_WANT_TO_SHOW','Select this option if you want to show %s with inactive mode, all users will be able to view this %s but will not be able to %s, %s, %s, follow.');
define('INACTIVE_MODE','Inactive Mode');
define('PLEASE_NOTE_IF_ONCE_IS_INACTIVATED','NOTE: Please note, if once %s is inactivated all %s');
define('YOU_CAN_HOLD_ALL_UNTILL_YOU_RESOLVE_YOUR_DOUBTS_AND_REASONS','You can hold all %s until you resolve your doubts and reasons to deactivate this %s with %s and activate again.');
define('IF_MANAGED_MANUALLY_BY_TEAM','If managed manually by %s team');
define('YOU_CAN_REFUND_ALL_FUND','You can refund all %s manually to the %s in through wire transfer or check if %s are being processed manually by <site-name> team.');
define('FEEDBACK','Feedback');
define('IMAGE','Image');
define('PERK_AMMOUNT','Perk Amount');
define('IP','Ip');
define('ARE_YOU_SURE_TO_DELETE_IMAGE', 'Are you sure to delete Image?');
define('MAKE_THIS_FEATURED', 'Make this Featured');
define('MAKE_THIS_UNFEATURED', 'Make this Unfeatured');
define('CANT_BE_FEATURED', 'Can`t be Featured');
define('CANT_DELETE', 'Can`t Delete');
define('INACTIVE_THIS', 'Inactive This');
define('EQUITY_SETTING', ''.$project_name.' Setting');
define('ACCREDENTIAL_REQUIRED', 'Accreditation Required?');


define('BY_PROJECT', 'By '.$project_name.'');
define('BY_OWNER_NAME', 'By Owner Name');
define('BY_PAR_FUNDED', 'By % Funded');
define('MAX_FUNDED', 'Max Funded');
define('MIN_FUNDED', 'Min Funded');
define('CLEAR_ALL_FILTERS', 'Clear All Filters');
define('INVESTMENT_STATUS', 'Investment Status');
define('DOCUMENT_REJECTED', 'Document Rejected');
define('TRACKING_SHIPMENT_CONFIRMED', 'Tracking Shipment Confirmed');

define('CASH_FLOW_STATUS', 'Cash Flow Status');
define('NONE','None');
define('CASH_FLOW','Cash Flowing to Investors');
define('ARE_YOU_SURE_YOU_WANT_TO_UPDATE_CASHFLOW_RECORD', 'Are you sure, You want to Update Cash Flow Status?');
// you can not remove this line because it is used for admin can select cash flowing type
define ("CASH_FLOW_ARRAY", serialize (array (NONE,CASH_FLOW)));
define('HAS_BEEN_UPDATED_SUCCESSFULLY', 'has been Updated successfully.');
define('INVESTOR_TYPE_DESCRIPTION','Investor Type Description');
define('INVESTOR_OPTION_LINK','Investor Option Link');	
define('INVESTMENT_TAX_RELIEF','Investor Tax Relief');
define('ADD_INVESTOR_TAX_RELIEF','Add Investor Tax Relief');
define('EDIT_INVESTOR_TAX_RELIEF','Edit Investor Tax Relief');
define('INVESTMENT_TAX_RELIEF_TYPE','Investor Tax Relief Type');
define('MAIN_PHASE', 'Main Phase');
define('SUB_PHASE', 'Sub Phase');


/*admin_user_lang.php*/
define('FORGOT_PASSWORD', 'Forgot Password');
define('FUNDRAISING_SCRIPT_BY', 'Fundraising Script By');
define('ROCKERSINFO_ALL_RIGHTS_RESERVED', 'rockersinfo.com All rights reserved');
define('ENTER_YOUR_EMAIL_ADDRESS_BELOW_TO_RESET_YOUR_PASSWORD', 'Enter your e-mail address below to reset your password.');
define('ERROR_PAGES', 'Error Page');
define('ERROR_PAGE', 'Error Page');
define('404', '404');
define('PAGE_NOT_FOUND', 'Page Not Found');
define('WE_ARE_SORRY_THE_PAGE_YOU_WERE_LOOKING_FOR_DOEST_EXIST_ANYMORE', 'We are sorry, the page you were looking for does not exist anymore.');
define('ADD_USER', 'Add User');
define('FACEBOOK_PROFILE_URL', 'Facebook Profile URL');
define('TWITTER_PROFILE_URL', 'Twitter Profile URL');
define('LINKEEDIN_PROFILE_URL', 'Linkedln Profile URL');
define('GOOGLE_PLUS_PROFILE_URL', 'Google Plus Profile URL');
define('BANDCAMP_PROFILE_URL', 'BandCamp Profile URL');
define('YOUTUBE_PROFILE_URL', 'Youtube Profile URL');
define('MYSPACE_PROFILE_URL', 'MySpace Profile URL');
define('INVALID_FACEBOOK_PROFILE_URL','Invalid Facebook Profile URL');
define('INVALID_TWITTER_PROFILE_URL','Invalid Twitter Profile URL');
define('INVALID_LINKEEDIN_PROFILE_URL','Invalid Linkedln Profile URL');
define('INVALID_BANDCAMP_PROFILE_URL','Invalid BandCamp Profile URL');
define('INVALID_YOUTUBE_PROFILE_URL','Invalid Youtube Profile URL');
define('INVALID_MYSPACE_PROFILE_URL','Invalid MySpace Profile URL');
define('INVALID_GOOGLE_PLUS_PROFILE_URL','Invalid Google Plus Profile URL');

define('INACTIVE', 'Inactive');
define('ADMIN_TYPE', 'Admin Type');
define('ACTIVE', 'Active');
define('SUSPEND', 'Suspend');
define('ADD_SUSPEND_REASON', 'Add suspend reason');
define('UPDATE', 'Update');
define('GRAPHICAL_REPORT', 'Graphical Report');
define('USER_LIST', 'User List');
define('USER_LOGIN', 'User Login');
define('ADMIN_LOGIN', 'Admin Login');
define('PROJECT_CATEGORIES', 'Project Categories');
define('PAYMENT_MODULE', 'Payment Module');
define('PAYPAL_ADAPTIVE_GATEWAY', 'Paypal Adaptive Gateway');
define('WALLET_GATEWAY', 'Wallet Gateway');
define('TRANSACTION', 'Transaction');
define('CONTENT_PAGES', 'Content Pages');
define('PAGES', 'Pages');
define('LEARN_MORE', 'Learn more');
define('LEARN_MORE_CATEGORIES', 'Learn more categories');
define('GLOBALIZATION', 'Globalization');
define('COUNTRIES', 'Countries');
define('OTHER_FEATURE', 'Other Feature');
define('CRON_JOBS', 'Cron Jobs');
define('MESSAGES', 'Messages');
define('STATS', 'Status');
define('COMMON_SETTING', 'Common Setting');
define('COMMISION_SETTING', 'Commision Setting');
define('AFFILIATE_REQUEST', 'Affiliate Request');
define('WITHDRAW_FUND_REQUEST', 'Withdraw Fund Request');
define('EMAIL_TEMPLATE', 'Email Template');
define('SETTING', 'Setting');
define('SITE', 'Site');
define('META', 'Meta');
define('GOOGLE', 'Google');
define('YAHOO', 'Yahoo');
define('IMAGE_SIZE', 'Image Size');
define('FILTER', 'Filter');
define('SPAM_SETTING', 'Spam Setting');
define('SPAM_REPORT', 'Spam Report');
define('SPAMER', 'Spamer');
define('NEWLETTERS', 'Newsletters');
define('NEWLETTERS_USERS', 'Newsletter Users');
define('NEWLETTERS_JOBS', 'Newsletter Job');
define('NEWLETTERS_SETTING', 'Newsletter Setting');
define('REPORT', 'Report');
define('PROJECT_REPORT', 'Project Report');
define('GENERAL_INFORMATION', 'General Information');
define('NEW_USERS', 'New Users');
define('PENDING_PROJECTS', 'Pending Campaigns');
define('RUNNING_PROJECTS', 'Running Campaigns');
define('SUCCESSFUL_PROJECTS', 'Successful Campaigns');
define('FAILURE_PROJECTS', 'Failure Campaigns');
define('LAST', 'Last');
define('USERNAME', 'Username');
define('POSTED_DATE', 'Posted Date');
define('NO_RECORD_FOUND', 'No record found');
define('NEW_RUNNING_PROJECTS', 'New Running Campaigns');
define('AMOUNT_RAISED', 'Amount Raised');
define('COMPLETED_PROJECTS', 'Completed Campaigns');
define('DONATE_AMT', 'Donate Amt');
define('EARNING', 'Earning');
define('REMAIN_FUND', 'Remain Fund');
define('FAILED_PROJECTS', 'Failed Campaigns');
define('NEW_DONORS', 'New Donors');
define('IP_ADDRESS', 'IP Address');
define('FUNDS', 'Funds');
define('REQUEST_DATE', 'Request Date');
define('NEW_AFFILIATE_WITHDRAW_REQUEST', 'New Affiliate Withdraw Requests');
define('REQUEST_IP', 'Request IP');
define('ROCKERSINFO_ALL_RIGHT_RESERVED', 'rockersinfo.com All rights reserved');
define('YOU_HAVE_1_NEW_NOTIFICATION', 'You have %s new notification(s).');
define('YOU_DONT_HAVE_ANY_NEW_NOTIFICATION', 'You do not have any new notification.');
define('ADMIN', 'Admin');
define('CRONJOB', 'Cronjob time');
define('LAST_LOGIN_IP', 'Last Login Ip');
define('LOG_OUT', 'Log Out');
define('STEP_1_OF_4', 'Step 1 of 4');
define('STEP_1', 'Step 1');
define('STEP_2', 'Step 2');
define('STEP_3', 'Step 3');
define('FINAL_STEP', 'Final Step');
define('FILL_UP_STEP_1', 'Fill up step 1');
define('FILL_UP_STEP_2', 'Fill up step 2');
define('FILL_UP_STEP_3', 'Fill up step 3');
define('OCCASIONALLY_WE_MAY_RELEASE_SMALL_UPDATE_IN_THE_FORM_OF_A_PATCH_TO_ONE_OR_SEVERAL_SOURCE_FILE', 'Occasionally, we may release small updates in the form of a patch to one or several source file. These source files are regular zip files that contain updated files.');
define('BACK', 'Back');
define('CONTINUE1', 'Continue');
define('NEW_RECORD_HAS_BEEN_ADDED_SUCCESSFULLY', 'New Record has been added successfully.');
define('RECORD_HAS_BEEN_UPDATED_SUCCESSFULLY', 'Record has been updated successfully.');
define('STATE', 'State');
define('SIGN_UP_IP', 'Sign up IP');
define('INACTIVE_USERS', 'Inactive Users');
define('AFFILIATE_REQUEST_USERS', 'Affiliate Request Users');
define('WHY_DO_YOU_WANT_AFFILIATE', 'Why do you want affiliate?');
define('PROMOTIANAL_METHOD', 'Promotional Method');
define('RECORD_HAS_BEEN_DELETED_SUCCESSFULLY', 'Record has been deleted successfully.');
define('USERS', 'Users');
define('SIGNUP_IP_ADDRESS', 'Signup IP Address');
define('LAST_10_PENDING_PROJECTS', 'Last 10 Pending Campaigns');
define('LAST_5_NEWRUNNING_PROJECTS', 'Last 5 New Running Campaigns');
define('LAST_5_COMPLETED_PROJECTS', 'Last 5 Completed Campaigns');
define('LAST_5_FAILED_PROJECTS', 'Last 5 Failed Campaigns');
define('LAST_5_NEW_PROJECTS', 'Last 10 New Donors');
define('NEW_AFFILIATE_WITHDRAW_REQUESTS', '10 New Affiliate Withdraw Requests');
define('LAST_10_NEW_USERS', 'Last 10 New Users');
define('LAST_5_INACTIVE_USERS', 'Last 5 Inactive Users');
define('LAST_5_AFFILIATE_USERS', 'Last 5 Affiliate Request Users');
define('REGISTERED_ON', 'Registered On');
define('FUNDRAISINGSCRIPT_COM', 'Fundraisingscript.com');
define('ERRORS', 'Error');
define('SOURCE_FILE_NAME', 'Source File Name');
define('DESTINATION_FILE_NAME', 'Destination File Name');
define('NO_UPDATE_FOUND', 'No Update Found');
define('DO_YOU_WANT_TO_UPDATE_YOUR_BACK_UP_AGAIN_THEN_CLICK_HERE', 'Do you want to update your back up again then click here.');
define('ADAPTIVE_PAYPAL_SETTING', 'Adaptive Paypal Setting');
define('SITE_STATUS', 'Site Status');
define('TRANSACTION_TYPE', 'Transaction Type');
define('SAND_BOX', 'Sand box');
define('LIVE', 'Live');
define('INSTANT', 'Instant');
define('PREAPPROVAL', 'Preapproval');
define('PAYPAL_APPLICATION_ID', 'Paypal Application Id');
define('PAYPAL_API_USERNAME', 'Paypal API Username');
define('PAYPAL_API_PASSWORD', 'Paypal API Password');
define('PAYPAL_API_SIGNATURE', 'Paypal API Signature');
define('PAYPAL_EMAIL_ID', 'Paypal Email Id');
define('PAYPAL_PAYPAL_FIRST_NAME', 'User Paypal First Name');
define('PAYPAL_PAYPAL_LAST_NAME', 'User Paypal Last Name');
define('PAYPAL_FEES_TAKEN_FROM', 'Paypal Fees taken from');
define('PROJECT_OWNER', 'Project Owner');
define('FROM_ADDRESS', 'From Address');
define('REPLY_ADDRESS', 'Reply Address');
define('NOTE_IT_WILL_UPDATE_ALL_THE_TEMPLATE_EMAIL_ADDRESS_IF_YOU_TO_CHANGE_ANY_PARTICULAR_EMAIL_ADDRESS_THEN_YOU_CAN_CHANGE_IT_GOING_IN_THAT_GOING_IN_THAT_EMAIL_TEMPLATE', 'Note: It will update all the template email address. If you want to change any particular email address then you can change it going in that email template.');
define('SELECT_PROJECT_FOR_TESTING_DONATION', 'Select '.$project.' for testing donation.');
define('SELECT_PROJECT_FOR_TEST', 'Select '.$project.' for test.');
define('ENTER_DONATE_AMOUNT_OR_SELECT_PERK', 'Enter donate amount Or select perk.');
define('PROJECT_OWNER_HAVE_NOT_VERIFY_PAYPAL_ACCOUNT_PLEASE_ENTER_PAYPAL_ACCOUNT_DETAIL_BELOW_AND_VERIFY', 'Project Owner have not verify Paypal Account. Please enter Paypal Account detail below and Verify.');
define('USER_PAYPAL_AC_EMAIL', 'User Paypal A/C Email');
define('OR', 'OR');
define('SELECT_DONAR_USER_FOR_MAKING_DONATION', 'Select donar user for making donation.');
define('SELECT_DONAR_USER', 'Select Donar User');
define('NOTES', 'NOTE');
define('BEFORE_PROCEED_TO_NEXT_STEP_PLEASE_LOGIN_TO_PAYPAL_LOGIN_TO_PAYPAL_ACCOUNT_OTHERWISE_YOU_NEED_COME_BACK_AGAIN_AND_DO_ALL_THE_PROCESS', 'Before proceed to next step, Please login to Paypal Account Otherwise you need come back again and do all the process once again.');
define('FOR_SANDBOX_TEST', 'For Sandbox Test');
define('DEVELOPER_PAYPAL_COM', 'developer.paypal.com');
define('FOR_LIVE_TEST', 'For Live Test');
define('COUNTRY_NAME', 'Country Name');
define('MESSAGE', 'Message');
define('RECORDS_HAS_BEEN_UPDATED_SUCCESSFULLY', 'Record(s) has been updated successfully.');
define('VIEW', 'View');
define('CURRENCY_NAME', 'Currency Name');
define('CURRENCY_CODE', 'Currency Code');
define('CURRENCY_SYMBOL', 'Currency Symbol');
define('CRONJOBS', 'Cronjob');
define('SELETE_CRON_JOB', 'Selete Cron job');
define('RUN', 'Run');
define('LAST_RUN_TIME', 'Last Run Time');
define('NO_CURRENCIES_FOUND', 'No currencies found.');
define('PAYPAL_ADAPTIVE_PAYMENT_GATEWAY', 'Paypal Adaptive Payment Gateway');
define('SANDBOX', 'Sandbox');
define('IF_ACTIVE_THEN_YOU_HAVE_TO_SET_CRON_JOB_FOR_EVERY_MINUTE_WITH_THIS_URL', 'If active then you have to set cron job for every 5 minutes  with this URL.');
define('YOU_HAVE_TO_INACTIVATE_THE_CURRENTLY_ACTIVE_PAYMENT_GATEWAY_TO_ACTIVATE_THIS_PAYEMNT', 'You have to inactivate the currently active Payment Gateway to activate this Payment Gateway.');
define('PAYMENT_GATEWAY_ACTIVATED_SUCCESSFULLY', 'Payment Gateway activated successfully.');
define('PAYMENT_GATEWAY_INACTIVATED_SUCCESSFULLY', 'Payment Gateway inactivated successfully.');
define('TEST_YOUR_PAYPAL_SETTING', 'Test your Paypal setting.');
define('GATEWAY_STATUS', 'Gateway Status');
define('OPTIONS', 'Options');
define('TRANSACTIONS', 'Transactions');
define('ADD_ADMINISTRATOR', 'Add Administrator');
define('ADMINISTRATOR', 'Administrator');
define('SUPER_ADMINISTRATOR', 'Super Administrator');
define('ASSIGN_RIGHTS', 'Assign Rights');
define('USERS_LOGIN', 'Users Login');
define('RIGHT_NAME', 'Right Name');
define('CANNOT_DELETE_ALL_THE_ADMIN', 'Cannot delete all the Admin. Atleast one should be active.');
define('RECORD_HAS_BEEN_DELETED_SUCCESSFULLY_FROM_ADMIN_LOGIN', 'Record has been deleted successfully from admin login.');
define('RIGHT_HAS_BEEN_UPDATED_SUCCESSFULLY', 'Rights has been updated successfully.');
define('ADD_ADMIN', 'Add Admin');
define('ADMINISTRATOR_MANAGED_TABLE', 'Administrator Managed Table');
define('RIGHTS', 'Rights');
define('LOGIN_DETAIL_HAS_BEEN_DELETED_SUCCESSFULLY', 'Login details has been deleted successfully.');
define('LOGIN_IP_ADDRESS', 'Login IP Address');
define('LOGIN_DATE', 'Login Date');
define('LOGIN_TIME', 'Login Time');
/*define('REPORT', 'Report');*/
define('INVALID_USERNAME_OR_PASSWORD', 'Invalid Username or Password');
define('YOU_HAVE_LOGGED_OUT_SUCCESSFULLY', 'You have logged out successfully');
define('PASSWORD_IS_SEND_TO_YOUR_EMAIL_ADDRESS', 'Password is send to your email address');
define('USER_EMAIL_ADDRESS_IS_NOT_FOUND', 'User Email address is not found');
define('USER_RECORD_IS_NOT_FOUND', 'User record is not found');

define('SUPER_ADMIN', 'Super Admin');
define('PROJECT_CATEGORIESS', 'Project categories');
define('LEARN_MOREE', 'Learn more');
define('LEARN_MORE_CATEGORIESS', 'Learn more categories');
define('DISPLAY', 'Display');
define('RECORD_PER_PAGE', 'Records Per Page');
define('NOTHING_FOUND_SORRY', 'Nothing found - sorry');
define('SHOWING', 'Showing');
define('RECORD', 'Records');
define('CHANGE_YOUR_PASSWORD', 'Change Your Password');
define('LAST_UPDATE_VERSION', 'Last Update Version');
define('UPDATED_ON', 'Updated on');
define('VERSION', 'Version');
define('SEE_ALL_UPDATES', 'See all Updates');
define('RIGHT', 'Right');
define('THERE_IS_AN_EXISTING_ACCOUNT_ASSOCIATED_WITH_THIS_EMAIL_ID', 'There is an existing account associated with this Email ID');
define('THERE_IS_AN_EXISTING_ACCOUNT_ASSOCIATED_WITH_THIS_USERNAME', 'There is an existing account associated with this Username');
define('YOUR_PASSWORD_HAS_BEEN_UPDATED_SUCCESSFULLY', 'Your Password has been updated successfuly');
define('SIGNATURE', 'Signature');
define('SELECT_PROJECT', 'Select Project');
define('DONATE_AMOUNT', 'Donate Amount');
define('COUNTRY_NAME_IS_ALREADY_EXIST', 'Country name is already exist');
define('MAILER_TYPE', 'Mailer Type');
define('SENDMAIL_PATH', 'Sendmail Path');
define('SENDMAIL_MAIL', 'Sender Mail');
define('RECEIVER_MAIL', 'Receiver Mail');
define('TEST_MESSAGE_SUCCESSFULLY_SENT', 'Test Message Successfully Sent');

define('CREADIT_CARD_VERSION', 'Credit Card Version');

define('PAYPAL_STATUS', 'Paypal Status');
define('COMMISSION_FEE', 'Commission Fee');
define('ACCESS_KEY_ID', 'Access Key Id');
define('SECRET_ACCESS_KEY', 'Secret Access Key');
define('TRANSACTION_TYPE_NAME', 'Transaction Type Name');
define('UPDATE_VERSION', 'Update Version');
define('DELETE_ADMINISTRATOR_CONFIRMATION ', 'Are you sure,Do you want to delete this record ?');
define('TAXONOMY', 'Taxonomy');
define('TAXONOMY_SETTING', 'Taxonomy setting');
define('PROJECT_NAME', 'Project name');
define('PROJECT_URL', 'Project URL');
define('PROJECT_FUNER', 'Project funder');
define('FOLLOWER', 'Follower');
define('USER', 'User');
define('NO', 'No');
define('EDIT_USER', 'Edit User');
define('SORRY_YOUR_PREAPPROVAL_PAYMENT_PROCESS_IS_VIOLATED_DUE_TO_FOLLOWING_ERROR', 'Sorry..!  Your Preapproval payment process is violated due to following error');
define('SORRY_YOUR_PAYMENT_PROCESS_IS_VIOLATED_DUE_TO_ERROR_IN_PAYRECIEPT_METHOD_PLEASE_CHECK_YOUR_SETTING_AND_TRY_ONCE_AGAIN', 'Sorry...! Your Payment process is violated due to error in PayReciept Method. Please check your setting and try once again.');
define('PREAPPROVAL PROCESS IS VIOLATED DUE TO PREAPPROVAL KEY ALREADY EXISTS IN THE SYSTEM. THIS HAPPEN DUE TO 2 REASON', 'Sorry...! Your  Preapproval Process is violated due to Preapproval Key already exists in the system. This happen due to 2 reason.<br/><br/>1). Due to lowe internet connectivity.<br/>2). Paypal Send response twice for same transaction. Check Manually Project donation amount and New Donor Entry.');
define('SORRY_YOUR_PREAPPROVAL_PROCESS_IS_VIOLATED_DUE_TO_ERROR_OCCURRED_IN_PAYMENTDETAILS_METHOD_PLEASE_CHECK_YOUR_SETTING_AND_TRY_ONCE_AGAIN', 'Sorry...! Your  Preapproval Process is violated due to Error occurred in PaymentDetails method. Please check your setting and try once again');
define('SORRY_YOUR_INSTANT_PAYMENT_PROCESS_FAILED_DUE_TO_ERROR', 'Sorry...! Your  Instant payment process failed due to error');
define('SORRY_YOUR_INSTANT_PAYMENT_PROCESS_FAILED_DUE_TO_ERROR_OCCURRED_IN_PAYMENTDETAILS_METHOD_PLEASE_CHECK_YOUR_SETTING_AND_TRY_ONCE_AGAIN', 'Sorry...! Your Instant Payment process failed due to Error occurred in PaymentDetails method. Please check your setting and try once again');
define('SORRY_YOUR_INSTANT_PAYMENT_PROCESS_IS_VIOLATED_DUE_TO_PAYPAL_AP_PAYPAL_NEW_DONOR_ENTRY', 'Sorry...! Your  Instant Payment Process is violated due to Paypal AP Key already exists in the system. This happen due to 2 reason.<br/><br/>1). Due to lowe internet connectivity.<br/>2). Paypal Send response twice for same transaction. Check Manually Project donation amount and New Donor Entry.');
define('SORRY_YOUR_PREAPPROVAL_PAYMENT_PROCESS_IS_VIOLATED_DUE_TO_ERROR_IN_PAYMENTDETAILS_METHOD_PLEASE_CHECK_YOURSETTING_AND_TRY_ONCE_AGAIN', 'Sorry..!  Your Preapproval payment process is violated due to error in PaymentDetails method. Please check your setting and try once again.');
define('SORRY_YOUR_PAYMENT_PROCESS_IS_VIOLATED_DUE_TO_ERROR_IN_PAYMENTEXECSTATUS_PLEASE_CHECK_YOUR_SETTING_AND_TRY_ONCE_AGAIN.', 'Sorry...! Your Payment process is violated due to error in paymentExecStatus. Please check your setting and try once again.');
define('PLEASE_CHECK_YOUR_SETTINGS_AND_TRY_ONCE_AGAIN', 'Please check your settings and Try once again');
define('YOUR_PAYMENT_PROCESS_IS_SUCCESSED_ON_PROJECT', 'Your Payment Process is successed on'.$project);
define('BELOW_IS_PAYPAL_RESPONSE', 'Below is Paypal Response');
define('ARE_YOU_SURE_YOU_WANT_TO_ACTIVE_SELECTED_COUNTRY', 'Are you sure, you want to active selected country?');
define('ARE_YOU_SURE_YOU_WANT_TO_INACTIVE_SELECTED_COUNTRY', 'Are you sure, you want to inactive selected country?');

define('RUNNING_INVESTMENT','Running Investment');
define('CREATED_DATE','Created Date');
define('INVESTOR_ACTION','Investor Action');
define('OWNER_ACTION','Owner Action');

/*admin_sapm_lang.php*/
define('TOTAL_SPAM_REPORT_ALLOW', 'Total Spam Report Allow');
define('REPORT_SPAMER_EXPIRE', 'Report Spammer Expire[In Days]');
define('TOTAL_REGISTRATION_ALLOW_FROM_SAME_IP', 'Total Registration Allow From Same IP');
define('REGISTRATION_SPMAER_EXPIRE', 'Registration Spammer Expire[In Days]');
define('TOTAL_COMMENT_ALLOW_FROM_SAME_IP_IN_ONE_WAY', 'Total Comment Allow From Same IP In One Day');
define('COMMENT_SPAMER_EXPIRE', 'Comment Spammer Expire[In Days]');
define('TOTAL_INQUIRY_ALLOW_SAME_IP_IN_ONE_WAY', 'Total Inquiry Allow From Same IP In One Day');
define('INQUIRY_SPAMER_EXPIRE', 'Inquiry Spammer Expire[In Days]');
define('ADD_SPAMMER', 'Add Spammer');
define('SPAMMER', 'Spammer');
define('SPAM_IP', 'Spam IP');
define('MAKE_PERMENANT', 'Make Permanent');
define('SPAM_REPORT_HAS_BEEN_DELETED_SUCCESSFULLY', 'Spam Report has been deleted successfully.');
define('REPORT_MAKE_SPAM_HAS_BEEN_SUCCESSFULLY', 'Report make spam has been successfully.');
define('MAKE_SPAMER', 'Make Spammer');
define('TOTAL_REPORT', 'Total Report');
define('SPAM_BY', 'Spam By');
define('REPORT_BY', 'Report By');
define('PERMENANT', 'Permanent');
define('NO_RECORD_AVAILABLE', 'No record availble');
define('REPORTED_EXPIRE', 'Reported Expire');
define('TOTAL_REGISTRATION', 'Total Registration');
define('REGISTRATION_SPAM_EXPIRE', 'Registration Spam Expire');
define('TOTAL_COMMNET', 'Total Comment');
define('COMMENT_SPAM_EXPIRE', 'Comment Spam Expire');
define('TOTAL_INQUIRY', 'Total Inquiry');
define('INQUIRY_SPAM_EXPIRE', 'Inquiry Spam Expire');
define('ARE_YOU_SURE_YOU_WANT_TO_MAKE_PERMENANT_SPAMER_SELECTED_REPORT', 'Are you sure, you want to make Permenant spamer selected report(s)?');

/*admin_setting_lang.php*/
define('EMAIL_SETTING_UPDATED_SUCCESSFULLY', 'Email settings updated successfully.');
define('FACEBOOK_SETTING', 'Facebook Setting');
define('FACEBOOK_SETTING_UPDATED_SUCCESSFULLY', 'Facebook settings updated successfully.');
define('FACEBOOK_SETTING_YOU_HAVE_ENTERED_ARE_TRUE', 'Facebook settings you have entered are true.');
define('FACEBOOK_PROFILE_FULL_URL', 'Facebook Profile Full URL');
define('FACEBOOK_APPLICATION_ID', 'Facebook Application ID');
define('FACEBOOK_LOGIN_ENABLE', 'Facebook Login Enable');
define('FACEBOOK_APPLICATION_API_KEY', 'Facebook Application API Key');
define('FACEBOOK_APPLICATION_SECRET_KEY', 'Facebook Application Secret Key');
define('ALLOW_POST_ON_FACEBOOK_WALL', 'Allow Post on Facebook Wall');
define('FACEBOOK_ACCESS_TOKEN', 'Facebook Access Token');
define('FACEBOOK_USER_ID', 'Facebook User ID');
define('FACEBOOK_IMAGE', 'Facebook Image');
define('WANT_TO_CHANGE_FACEBOOK_DETAILS', 'Want to Change Facebook Details');
define('CONNECT_TO_FACEBOOK', 'Connect to Facebook');
define('FILTER_SETTING', 'Filter Setting');
define('ROUNDING_OFF_DAYS', 'Rounding Off(Days)');
define('POPULAR_PER', 'Popular(%)');
define('MOST_FUNDED_PER', 'Most Funded(%)');
define('SUCCESS_STORIES_PER', 'Success Stories(%)');
define('FILTER_SETTING_UPDATED_SUCCESSFULLY', 'Filter settings updated successfully.');
define('GOOGLE_SETTING', 'Google setting');
define('GOOGLE_SETTING_UPDATED_SUCCESSFULLY', 'Google settings updated successfully.');
define('GOOGLE_ENABLED', 'Google Enabled');
define('CONSUMER_KEYS', 'Consumer Key');
define('CONSUMER_SECRETS', 'Consumer Secret');
define('IMAGE_SETTING', 'Image setting');
define('PROJECT_THEUMBNAIL_WIDTH', '' . $project . ' Thumbnail Width');
define('PROJECT_THEUMBNAIL_HEIGHT', '' . $project . ' Thumbnail Height');
define('PROJECT_THEUMBNAIL_ASPECT_RATIO', '' . $project . ' Thumbnail Aspect Ratio');
define('TRUE_LANG', 'TRUE');
define('FALSE_LANG', 'FALSE');
define('PROJECT_SMALL_WIDTH', '' . $project . ' Small Width');
define('PROJECT_SMALL_HEIGHT', '' . $project . ' Small Height');
define('PROJECT_MEDIUM_WIDTH', '' . $project . ' Medium Width');
define('PROJECT_MEDIUM_HEIGHT', '' . $project . ' Medium Height');
define('PROJECT_LARGE_WIDTH', '' . $project . ' Large Width');
define('PROJECT_LARGE_HEIGHT', '' . $project . ' Large Height');
define('USER_SMAIL_WIDTH', 'User Small Width');
define('USER_SMAIL_HEIGHT', 'User Small Height');
define('USER_MEDIUM_WIDTH', 'User Medium Width');
define('USER_MEDIUM_HEIGHT', 'User Medium Height');
define('USER_BIG_WIDTH', 'User Big Width');
define('USER_BIG_HEIGHT', 'User Big Height');
define('USER_THUMBAIL_ASPECT_RATIO', 'User Thumbnail Aspect Ratio');
define('IMAGE_SIZE_SETTING_UPDATED_SUCCESSFULLY', 'Image size settings updated successfully.');
define('PROJECT_GALLERY_THUMBNAIL_ASPECT_RATIO', '' . $project . ' Gallery Thumbnail Aspect Ratio');
define('LINKEDIN_SETTING', 'Linkedin setting');
define('LINKEDIN_SETTING_UPDATED_SUCCESSFULLY', 'Linkedin settings updated successfully.');
define('LINKEDIN_PROFILE_FULL_URL', 'LinkdIn Profile Full URL');
define('LINKEDIN_ENABLED', 'Linkedin Enabled');
define('LINKEDIN_ACCESS', 'LinkdIn API Key');
define('LINKEDIN_SECRET', 'LinkdIn Secret Key');
define('MESSAGE_SETTING', 'Message Setting');
define('MESSAGE_SETTING_UPDATED_SUCCESSFULLY', 'Message settings updated successfully.');
define('SEND_EMAIL_TO_ADMIN_ON_NEW_MESSAGE', 'Send email to admin on new message.');
define('ENABLE', 'Enable');
define('DISABLE', 'Disable');
define('DEFAULT_MESSAGE_SUBJECT', 'Default message subject');
define('MESSAGE_MODULE', 'Message Module');
define('META_SETTING', 'Meta setting');
define('META_SETTING_UPDATED_SUCCESSFULLY', 'Meta settings updated successfully.');
define('SITE_SETTING', 'Site setting');
define('SITE_SETTING_UPDATED_SUCCESSFULLY', 'Site settings updated successfully.');
define('SITE_VERSION', 'Site Version');
define('CAPTCHA_SETTING', 'Captcha Settings');
define('CONTACT_US_PAGE', 'Contact us page');
define('SIGN_UP_PAGE', 'Sign up page');
define('CREATE_A_RECAPTCHA_KEY_FOR_YOUR_SITE_FROM_HERE_AND_SET_THE_KEY_VALUES_BELOW', 'Create a reCAPTCHA key for your site from here and set the key values below.');
define('PRIVATE_KEY', 'Private Key');
define('PUBLIC_KEY', 'Public Key');
define('LOGO_SETTING', 'Logo Setting');
define('SITE_LOGO', 'Site Logo');
define('SITE_LOGO_HOVER', 'Site Logo Hover');
define('FAVICON_IMAGE', 'Favicon Image');

define('LANGUAGE_AND_DATE_TIME_SETTING', 'Language & Date Time Setting');
define('SITE_LANGUAGE', 'Site Language');
define('DATE_FORMAT', 'Date Format');
define('TIME_FORMAT', 'Time Format');
define('SITE_TIME_ZONE', 'Site Time Zone');
define('FEED_SETTINGS', 'Feed Settings');
define('ENABLE_FACEBOOK_STREAM', 'Enable Facebook Stream');
define('ENABLE_TWITTER_STREAM', 'Enable Twitter Stream');
define('PROJECT_LIMITS', '' . $project . ' Limits');
define('MAXIMUM_NO_PROJECT_PER_YEAR_FOR_ONE_USER', 'Maximum No. of ' . $project . ' per Year (for one User)');
define('MAXIMUM_NO_DONATION_PER_PROJECT', 'Maximum No. of Donations per ' . $project . ' (for one User)');
define('PROJECT_GOAL_AMOUNT_SETTING', '' . $project . ' Goal Amount Setting');
define('MINIMUM_PROJECT_GOAL_AMOUNT', 'Minimum ' . $project . ' Goal Amount');
define('MAXIMUM_PROJECT_GOAL_AMOUNT', 'Maximum ' . $project . ' Goal Amount');
define('PROJECT_TARGET_DAYS_SETTING', '' . $project . ' Target Days Setting');
define('MINIMUM_DAYS_FOR_PROJECTS', 'Minimum days for ' . $project . '');
define('MAXIMUM_DAYS_FOR_PROJECTS', 'Maximum days for ' . $project . '');
define('DONATION_AMOUNT_SETTING', 'Donation Amount Setting');
define('MINIMUM_DONATION_AMOUNT', 'Minimum Donation Amount');
define('MAXIMUM_DONATION_AMOUNT', 'Maximum Donation Amount');
define('PAYPAL_RESTRICTION_FOR_MAXIMUM_DONATION_AMOUNT', '(NOTE : Paypal restriction for Maximum Donation amount is $2000 USD. Set limit as your currency conversation rate.)');
define('REWARD_AMOUNT_SETTING', 'Reward(Perk) Amount Setting');
define('PERK_ENABLE_DISABLE', 'Perk Enable/Disable');
define('MINIMUM_PERK_AMOUNT', 'Minimum Reward(Perk) Amount');
define('MAXIMUM_PERK_AMOUNT', 'Maximum Reward(Perk) Amount');
define('DONATION_TYPE_SETTING', 'Donation Type Setting');
define('PAYMENT_GATEWAY', 'Payment Gateway');
define('PAYMENT_ADAPTIVE', 'Paypal Adaptive');
define('WALLET', 'Wallet');
define('PROJECT_ACHIVE_GOAL_AUTO_PREAPPROVAL', '' . $project . ' Achieve Goal Auto Preapproval');
define('NOTE_NO_MEAN_PREAPPROVED_DOANTION_AMOUNT_TRANSFER_ON_PROJECT_END_DATE', '(NOTE : NO means preapproved donation amount transfer on ' . $project . ' end date.Yes means preapproved donation amount transfer when ' . $project . ' achieved its target goal amount before end date.)');
define('FUNDING_DONATION_TYPE_FOR_PROJECTS', 'Funding/Donation Type For ' . $project . '');
define('FIXED', 'Fixed');
define('FLEXIBLE', 'Flexible');
define('NOTE_WORK_ONLY_FOR_PREAPPROVAL_IS_ENABLE', '(NOTE : Works only for Preapproval is Enable. By default Funding/Donation Type of the ' . $project . ' is fixed.)');
define('FIXED_PROJECT_COMMISION', 'Fixed ' . $project . ' Commission (%)');
define('FLEXIBLE_SUCCESSFUL_PROJECT_COMMISION', 'Flexible Successful ' . $project . ' Commission (%)');
define('FLEXIBLE_UNSUCCESSFUL_PROJECT_COMMISION', 'Flexible Unsuccessful ' . $project . ' Commission (%)');
define('PERSONAL_INFORMATION_SETTING', 'Personal Information Setting');
define('ENABLE_PERSONAL_INFORMATION', 'Enable Personal Information');
define('TWITTER_SETTING', 'Twitter setting');
define('TWITTER_SETTING_UPDATED_SUCCESSFULLY', 'Twitter settings updated successfully.');
define('TWITTER_SETTING_YOU_HAVE_ENTERED_ARE_TRUE', 'Twitter settings you have entered are true.');
define('TWITTER_PROFILE_FULL_URL', 'Twitter Profile Full URL');
define('TWITTER_ENABLE', 'Twitter enabled');
define('ALLOW_POST_ON_TWITTER', 'Allow Post on Twitter');
define('ACCESS_TOKEN_SECRET', 'Access token secret');
define('ACCESS_TOKEN', 'Access token');
define('TWIITER_IMAGE', 'Twitter Image');
define('WANT_TO_CHANGE_TWITTER_DETAILS', 'Want to Change Twitter Details');
define('CONNECT_TO_TWITTER', 'Connect to Twitter');
define('ADD_TWITTER_DETAIL', 'Add Twitter Details');
define('YAHOO_SETTING', 'Yahoo setting');
define('YAHOO_SETTING_UPDATED_SUCCESSFULLY', 'Yahoo settings updated successfully.');
define('YAHOO_ENABLED', 'Yahoo Enabled');
define('YOUTUBE_SETTING', 'Youtube setting');
define('YOUTUBE_PROFILE_FULL_URL', 'Youtube Profile full url');
define('YOUTUBE_ENABLED', 'Youtube Enable');
define('YOUTUBE_SETTING_UPDATED_SUCCESSFULLY', 'Youtube settings updated successfully.');
define('GOOGLE_PLUS_SETTING', 'Google plus Enable');
define('GOOGLE_PLUS_SETTING_UPDATED_SUCCESSFULLY', 'Google plus settings updated successfully.');
define('GOOGLE_PROFILE_FULL_URL', 'Google plus profile full url');
define('GOOGLE_PLUS_ENABLED', 'Google plus enable');
define('PROJECT_ENDING_SOON_SETTING', '' . $project . ' ending soon setting');
define('PROJECT_ENDING_SOON_DAYS', '' . $project . ' ending soon days');
define('HOME_SLIDER', 'Select what you want to display');
define('DYNAMIC_SLIDER', 'Static Banners');
define('MANAGE_SLIDER', 'Manage Sliders');
define('ADD_SLIDER', 'Add Slider');
define('BRIEF', 'Brief');
define('SLIDER_IMAGE', 'Slider Image');
define('BUTTON_TITLE', 'Button Title');
define('BUTTON_LINK', 'Button Link');
define('EDIT_SLIDER', 'Edit Slider');
define('BANNER', 'Banner');
define('TO_GO_MANAGE_IMAGE', 'to go and manage image banners in slider.');
define('TO_GO_MANAGE_FEATURED', 'to go and manage featured ' . $project . '.');
define('TO_GO_MANAGE_ORDER_OF_IMAGE', 'to go and manage order of image banners in slider.');
define('TO_GO_MANAGE_ORDER_OF_FEATURED', 'to go and manage order of featured ' . $project . ' in slider.');

define('SLIDER_SETTING', 'Slider Sorting');
define('HOME_BANNER_SLIDER', 'Home Banner Slider');
define('BANNER_SLIDER', 'Banner Slider');
define('SOCIAL_SETTING', 'Social Setting');
define('OTHER_SETTING', 'Other Setting');
define('DROPDOWN_MANAGE', 'Manage Dropdown');
define('TAXONOMY_SETTING_UPDATED_SUCCESSFULLY', 'Taxonomy settings updated successfully.');
define('SINGULAR', 'Singular');
define('PLURAL', 'Plural');
define('PAST_PARTICIPATE', 'Past Participate');
define('GERUND', 'Gerund');
define('PROJECT_NAME_SINGULAR', 'Project Name Singular');
define('PROJECT_NAME_PLURAL', 'Project Name Plural');
define('PROJECT_OWNER_SINGULAR', 'Project Owner Singular');
define('PROJECT_OWNER_PLURAL', 'Project Owner Plural');
define('FUNDS_SINGULAR', 'Funds Singular');
define('FUNDS_PLURAL', 'Funds Plural');
define('FUNDS_PAST_PARTICIPATE', 'Funds Past Participate');
define('FUNDS_GERUND', 'Funds Gerund');



define('COMMENTS_SINGULAR', 'Comments Singular');
define('COMMENTS_PLURAL', 'Comments Plural');
define('COMMENTS_PAST_PARTICIPATE', 'Comments Past Participate');
define('COMMENTS_GERUND', 'Comments Gerund');


define('UPDATES_SINGULAR', 'Updates Singular');
define('UPDATES_PLURAL', 'Updates Plural');
define('UPDATES_PAST_PARTICIPATE', 'Updates Past Participate');
define('UPDATES_GERUND', 'Updates Gerund');


define('FOLLOWER_SINGULAR', 'Followers Singular');
define('FOLLOWER_PLURAL', 'Followers Plural');
define('FOLLOWER_PAST_PARTICIPATE', 'Followers Past Participate');
define('FOLLOWER_GERUND', 'Followers Gerund');
define('PREVIOUS', 'Previous');

define('PROJECT_UPDATES', '' . $project . ' Updates');

define('PROJECT_PERK', '' . $project . ' Perk');
define('CLAIMED_PERK', 'Claimed Perk');
define('NO_PERK', ' No Perk');
define('PROJECT_DONATION', '' . $project . ' Donation');
define('PROJECT_COMMENT', '' . $project . ' Comments');
define('PROJECT_LIST', '' . $project . ' List');
define('ADD_TAGS', 'Add Tags');
define('PROJECT_TAGS', '' . $project . ' Tags');
define('ADD_PROJECT_TAGS', 'Add ' . $project . ' Tags');
define('TAGS', 'Tags');
define('TAGS_LIST', 'Tags List');
define('INTEREST_TAGS_LIST', 'Interest Tags List');
define('SKILL_TAGS_LIST', 'Skill Tags List');
define('USER_NAME', 'User Name');
define('TRANSACTIONO_REPORT', 'Transactions Report');
define('EARNED', 'Earned');
define('TRANSACTION_ID', 'Transaction ID');
define('ADD_PROJECT_CATEGORY', 'Add ' . $project . ' Category');
define('PROJECT_CATEGORY', '' . $project . ' Category');
define('PARENT_CATEGORY', 'Parent Category');
define('MAIN_CATEGORY', 'Main category');
define('PROJECT_CATEGORY_NAME', '' . $project . ' Category Name');
define('TAGS_NAME', 'Interest Name');
define('GOAL_AMOUNT', 'Goal Amount');
define('RAISED_AMOUNT', 'Raised Amount');
define('VIEW_COMMENTS', 'View Comments');
define('HAS_BEEN_DELETED_SUCCESSFULLY', 'has been deleted successfully.');
define('YOU_CAN_NOT_DELETE_ACTIVE_PROJECT', 'You can not delete active ' . $project . '.');
define('HAS_BEEN_ACTIVATED_SUCCESSFULLY', 'has been activated successfully.');
define('HAS_BEEN_APPROVED_SUCCESSFULLY', 'has been approved successfully.');
define('YOU_CAN_NOT_ACTIVE_EXPIRED_OR_ALREADY_ACTIVE', 'You can not active expired or already active, successful or failure ' . $project . '.');

define('YOU_CAN_NOT_INACTIVE_EXPIRED_OR_ALREADY_ACTIVE', 'You can not inactive expired or already inactive, successful or failure ' . $project . '.');

define('HAS_BEEN_INACTIVATED_SUCCESSFULLY', 'has been inactivated successfully.');
define('HAS_BEEN_DECLIENED_SUCCESSFULLY', 'has been declined successfully.');
define('CANNOT_BE_DECLIENED', 'cannot be decline');
define('HAS_BEEN_FEATURED_SUCCESSFULLY', 'has been featured successfully.');
define('HAS_BEEN_ACTIVATED_AND_SET_FEATURED_SUCCESSFULLY', 'has been activated and set featured successfully.');
define('CANNOT_BE_SET_TO_FEATURED_AS_IT_IS_EXPIRED_SUCCESSFUL_FAILURE', 'cannot be set to featured as it is expired, successful or failure');
define('HAS_BEED_REMOVE_FROM_FEATURED_SUCCESSFULLY', 'has been remove from featured successfully');
define('NEW', 'New');
define('NEWS', 'New');
define('HAS_BEEDN_ADDED_SUCCESSFULLY', 'has been added successfully');
define('HAS_BEEDN_UPDATED_SUCCESSFULLY', 'has been updated successfully');
define('NEW_RECORDS_HAS_BEEN_ADDED_SUCCESSFULLY', 'New Record(s) has been added successfully.');
define('DECLIENED', 'Decline');
define('NOT_FEATURED', 'Not Featured');
define('ADD_CATEGORIES', 'Add Categories');
define('CATEGORIES_TYPE', 'Category Type');
define('ADD_AMOUNT', 'Add amount');
define('WHERE_TO_DISPLAY_CURRENCY_SYMBOL', 'Where to display Currency Symbol?');
define('DECIMAL_POINTS_AFTER_AMOUNT', 'Decimal Points After Amount');
define('ARE_YOU_SURE_YOU_WANT_TO_DELETE_SELECTED_PROJECT', 'Are you sure, You want to delete selected ' . $project . '(s)?');
define('ARE_YOU_SURE_YOU_WANT_TO_ACTIVE_SELECTED_PROJECT', 'Are you sure, You want to active selected ' . $project . '(s)?');
define('ARE_YOU_SURE_YOU_WANT_TO_APPROVE_SELECTED_PROJECT', 'Are you sure, You want to approve selected ' . $project . '(s)?');
define('ARE_YOU_SURE_YOU_WANT_TO_DECLINE_SELECTED_PROJECT', 'Are you sure, You want to declined selected ' . $project . '(s)?');
define('ARE_YOU_SURE_YOU_WANT_TO_FETAUTE_SELECTED_PROJECT', 'Are you sure, You want to Featured selected ' . $project . '(s)?');
define('ARE_YOU_SURE_YOU_WANT_TO_REMOVE_FEATURE_SELECTED_PROJECT', 'Are You sure, you want to Remove Featured selected ' . $project . '(s)?');

define('ARE_YOU_SURE_YOU_WANT_TO_DELETE_SELECTED_RECORD', 'Are you sure, You want to delete selected record(s)?');
define('ARE_YOU_SURE_YOU_WANT_TO_ACTIVE_SELECTED_RECORD', 'Are you sure, You want to active selected record(s)?');
define('ARE_YOU_SURE_YOU_WANT_TO_INACTIVE_SELECTED_RECORD', 'Are you sure, You want to inactive selected record(s)?');
define('ARE_YOU_SURE_YOU_WANT_TO_SPAMMER_SELECTED_RECORD', 'Are you sure, You want to spamer selected record(s)?');
define('ARE_YOU_SURE_YOU_WANT_TO_REMOVE_ACTIVE_SELECTED_CURRENCIES', 'Are you sure, You want to active selected currencies?');
define('ARE_YOU_SURE_YOU_WANT_TO_REMOVE_INACTIVE_SELECTED_CURRENCIES', 'Are you sure, You want to inactive selected currencies?');
define('AMOUNT_SETTING_UPDATED_SUCCESSFULLY', 'Amount settings updated successfully.');


define('PROJECT_SMALL_IMAGE_HEIGHT', '' . $project . ' Small Image Height');
define('PROJECT_MEDIUM_IMAGE_WIDTH', '' . $project . ' Medium Image Width');
define('PROJECT_MEDIUM_IMAGE_HEIGHT', '' . $project . ' Medium Image Height');
define('PROJECT_LARGE_IMAGE_WIDTH', '' . $project . ' Large Image Width');
define('PROJECT_LARGE_IMAGE_HEIGHT', '' . $project . ' Large Image Height');
define('PROJECT_SMALL_IMAGE_WIDTH', 'User Small Image Width');
define('USER_SMALL_IMAGE_HEIGHT', 'User Small Image Height');
define('USER_BIG_IMAGE_WIDTH', 'User Big Image Width');
define('USER_BIG_IMAGE_HEIGHT', 'User Big Image Height');
define('USER_MEDIUM_WIDTH_IMAGE_WIDTH', 'User Medium Image Width');
define('USER_MEDIUM_WIDTH_IMAGE_HEIGHT', 'User Medium Image Height');
define('PROJECT_THUMB_IMAGE_WIDTH', '' . $project . ' Thumb Image Width');
define('PROJECT_THUMB_IMAGE_HEIGHT', '' . $project . ' Thumb Image Height');

define('PROJECT_THUMBNAIL_WIDTH_SHOULD_BE_GREATE_THAN_ZERO', '' . $project . ' Thumbnail Width should be greator than Zero');
define('PROJECT_THUMBNAIL_HEIGHT_SHOULD_BE_GREATE_THAN_ZERO', '' . $project . ' Thumbnail Height should be greator than Zero');

define('PROJECT_SMALL_WIDTH_SHOULD_BE_GREATER_THAN_ZERO', '' . $project . ' Small Width should be greator than Zero');
define('PROJECT_SMALL_HEIGHT_SHOULD_BE_GREATER_THAN_ZERO', '' . $project . ' Small Height should be greator than Zero');
define('PROJECT_MEDIUM_WIDTH_SHOULD_BE_GREATER_THAN_ZERO', '' . $project . ' Medium Width should be greator than Zero');
define('PROJECT_MEDIUM_HEIGHT_SHOULD_BE_GREATER_THAN_ZERO', '' . $project . ' Medium Height should be greator than Zero');
define('PROJECT_LARGE_WIDTH_SHOULD_BE_GREATER_THAN_ZERO', '' . $project . ' Large Width should be greator than Zero');
define('PROJECT_LARGE_HEIGHT_SHOULD_BE_GREATER_THAN_ZERO', '' . $project . ' Large Height should be greator than Zero');
define('USER_SMALL_WIDTH_SHOULD_BE_GREATER_THAN_ZERO', 'User Small Width should be greator than Zero');
define('USER_SMALL_HEIGHT_SHOULD_BE_GREATER_THAN_ZERO', 'User Small Height should be greator than Zero');
define('USER_MEDIUM_WIDTH_SHOULD_BE_GREATER_THAN_ZERO', 'User Medium Width should be greator than Zero');
define('USER_MEDIUM_HEIGHT_SHOULD_BE_GREATER_THAN_ZERO', 'User Medium Height should be greator than Zero');
define('PROJECT_BIG_WIDTH_SHOULD_BE_GREATER_THAN_ZERO', '' . $project . ' Big  Width should be greator than Zero');
define('PROJECT_BIG_HEIGHT_SHOULD_BE_GREATER_THAN_ZERO', '' . $project . ' Big  Height should be greator than Zero');

define('NOT_AVAILBLE', 'Not Available');
define('PAGE_TITLE', 'Page Title');
define('FUND_GOAL', 'Fund Goal');
define('PROJECT_ADDRESS', '' . $project . ' Address');
define('PERK_TOTAL', 'Perk Total');
define('REPLY_HAS_BEEN_ADDED_SUCCESSFULLY', 'Reply has been added successfully');
define('REASON', 'Reason');
define('PLEASE_ENTER_AMOUNT_GREATER_THAN_ZERO', 'Please enter amount greator than zero');
define('DEDUCT_FUND', 'Deduct Fund');
define('AMOUNT_SHOULD_BE_GREATER_THAN_ZERO', 'Amount should be greator than zero');
define('PREAPPROVAL_ENABLE', 'Preapproval Enable');

define('PROJECT_ENTER_MAXIMUM_PROJECT_GOAL_AMOUNT_GREATER_THAN_MINIMUM_PROJECT_GOAL_AMOUNT', 'Please enter Maximum ' . $project . ' Goal Amount greater than Minimum ' . $project . ' Goal Amount');
define('PROJECT_ENTER_MAXIMUM_DAYS_FOR_PROJECT_GREATER_THAN_MINIMUM_DAYS_FOR_PROJECT', 'Please enter Maximum days for ' . $project . ' greater than Minimum days for ' . $project . '');

define('PLEASE_ENTER_MAXIMUM_DONATION_AMOUNT_GREATER_THAN_MINIMUM_DONATION_AMOUNT', 'Please enter Maximum Donation Amount greater than Minimum Donation Amount');

define('PLEASE_ENTER_MAXIMUM_MAXIMUM_REWARD_AMOUNT_GREATER_THAN_MINIMUM_REWARD_AMOUNT', 'Please enter Maximum Reward(Perk) Amount greater than Minimum Reward(Perk) Amount');

define('PLEASE_ENTER_MINIMUM_REWARD_AMOUNT_GREATER_THAN_DONATION_AMOUNT', 'Please enter minimum reward amount greater than donation amount');
define('PLEASE_ENTER_MAXIMUM_REWARD_AMOUNT_LESS_THAN_DONATION_AMOUNT', 'Please enter maximum reward amount less than donation amount');
define('ENDING_SOON', 'Ending Soon');

define('VERSIONS', 'Versions');

define('ADD', 'Add');

define('BANK_NAME', 'Bank Name');
define('SAMPLE_FILE', 'Sample File');
define('ACCREDENTIAL_DOCUMENT', 'Accreditation Document');
define('CONTRACT_COPY_UPLOAD_BY', 'Who will manage contract with investors?');
define('OTHER_SETTING_UPDATED_SUCCESSFULLY', 'Other settings updated successfully.');
define('FOR_ACCREDENTIAL_DOCUMENT', 'For Accreditation Document');
define('SUCCESS_THEMES_HAS_BEEN_UPDATED_SUCCESSFULLY', 'Success!Themes has been updated successfully.');
define('YOUR_VERSION_UPDATED_SUCCESSFULLY', 'Your Version updated successfully.');

define('YOUR_BACKUP_VERSION_UPDATED_SUCCESSFULLY', 'Your Backup Version updated successfully...');
define('THERE_IS_SOME_PROBLEM_WITH_UPDATING_YOUR_BACK_UP_VERSION', 'There is some problem with updating your back up version.');
define('IF_SMTP_USER_IS_GMAIL_THEN', 'if smtp user is gmail then ssl://smtp.googlemail.com');
define('SEND_EMAIL_TO_USER_ON_NEW_MESSAGE', 'Send email to user on new message');
define('IMAGE_DIMENSIONS', ' Image Dimensions');
define('CLEAR_CACHE', 'Clear Cache');
define('TWITTER_USERANME', 'Twitter Useranme');
define('THEMES', 'Themes');
define('COLOR_ORANGE', 'Color orange');
define('COLOR_DARKORANGE', 'Color darkorange');
define('COLOR_GREY', 'Color grey');
define('COLOR_BLACK', 'Color black');
define('COLOR_WHITE', 'Color white');
define('COLOR_LIGHTGREY', 'Color lightgrey');
define('COLOR_BLUE', 'Color blue');
define('COLOR_DARKBLUE', 'Color darkblue');
define('COLOR_CCC', 'Color ccc');
define('FONT_STYLE', 'Font style');
define('ADMIN_FEES', 'Admin Fees');
define('DONORS_PAYPAL_EMAIL', 'Donor`s paypal email');
define('ARE_YOU_SURE_YOU_WANT_TO_CONFIRM_SELECTED_RECORD', 'Are you sure, you want to confirm selected record(s)?');
define('AVAILABLE_BALANCE', 'Available Balance');
define('BANK_DETA', 'Bank Data');
define("WHO_SELECT_ACCREDIATION", "Who Select Accrediation");
define("PROJECT_OWNER_ADMIN", "" . $project . " Owner");
define("ALLPROJECTS", "All " . $project . "s");
define("OWNER", "Owner");
define("CONTRACT_COPY_REQUIRED", "Contract Copy Required");
define('BEFORE_YOU_CAN_APPROVE_THIS_PROJECT', 'Before you approve this ' . $project . ' uploading contract is required, please follow %s to upload contract.');
define('THIS_LINK', 'this link');
define('ACCREDENTIAL_SETTING','Accreditation Setting');
define('CONTRACT_SETTING','Contract Setting');
define('ACCREDIATION_SELECT_MANAGE_TITLE','How investor accreditation will be managed?');
define('ACCREDIATION_SELECT_ALLPROJECT','Keep accreditation required for all investors before investing.');
define('ACCREDIATION_SELECT_PROJECT_OWNER_ADMIN','Let ' . $project . ' owners decide who can invest on their ' . $project . 's');

define('CONTRACT_COPY_UPLOAD_BY_OWNER','' . $project . ' owner');
define('CONTRACT_COPY_UPLOAD_BY_ADMIN','Site Admin(you or your admin staff)');
define('PROJECT_TITLE_GOES_HERE','project-title-goes-here');
define('IMAGE_UPLOAD_LIMIT', 'Image upload limit');
define('UPLOAD_LIMIT_SHOULD_BE_GREATER_THAN_ZERO', 'Upload limit should be greater than zero');


/*admin_newsletter_lang.php*/

define('UPLOAD_CSV', 'Upload CSV');
define('DOWNLOAD_SAMPLE_CSV_FILE', 'Download Sample CSV file');
define('ADD_NEWSLETTER', 'Add Newsletter');
define('ADD_NEWSLETTER_JOB', 'Add Newsletter Job');
define('NEWSLETTER_SETTING', 'Newsletter Setting');
define('ATTACH_FILE', 'Attach File');
define('VIEW_ATTACHMENT', 'View Attachment');
define('ALLOW_UNSUBCRIBE_LINK', 'Allow Unsubscribe Link');
define('YES', 'Yes');
define('NOS', 'No');
define('SUBSCRIBE_TO', 'Subscribe To');
define('NONES', 'None');
define('ALL', 'All');
define('SELECT_NEWSLETTER', 'Select Newsletter');
define('SELECT_USER', 'Select User');
define('SELECT', 'Select');
define('SUBSCRIBER', 'Subscriber');
define('REGISTERED', 'Registered');
define('BOTH', 'Both');
define('SUBSCRIBER_USER_LIST', 'Subscriber User List');
define('REGISTER_USER_LIST', 'Register User List');
define('ALL_USER', 'All users');
define('RECURSIVE', 'Recursive');
define('NEWSLETTER_TYPE', 'Newsletter Type');
define('DAILY', 'Daily');
define('WEEKLY', 'Weekly');
define('MONTHLY', 'Monthly');
define('DURATION', 'Duration');
define('SELECT_A_DAY_OF_WEEK', 'Select a day of week');
define('SELECT_A_DAY', 'Select a day');
define('SELECT_START_DATE', 'Select Start Date');
define('SELECT_END_DATE', 'Select End Date');
define('JOB_START_DATE', 'Job Start Date');
define('JOB_CREATE_DATE', 'Job Create Date');
define('TOTAL_SUBSCRIBER', 'Total Subscriber');
define('TOTAL_SEND', 'Total Send');
define('TOTAL_OPEN', 'Total Open');
define('TOTAL_FAIL', 'Total Fail');
define('NEWSLETTERS', 'Newsletter');
define('TOTAL_USER', 'Total User');
define('NEWSLETTER_JOB', 'Newsletters Job');
define('REFRES', 'Refresh');
define('DRAFT', 'Draft');
define('SENT', 'Sent');
define('START_DATE', 'Start Date');
define('STATISTICS', 'Statistics');
define('SENDS', 'Send');
define('OPEN', 'Open');
define('CREATE_DATE', 'Create Date');
define('NO_RECORDS_FOUND', 'No Records found.');
define('IMPORTANT_NOTE_PLEASE_SET_THE_CRON_JOB_ON_YOUR_SERVER_WITH_URL', 'Important Note : Please set the cron job on your server with URL.');
define('EXPORT_CSV', 'Export CSV');
define('IMPORT_CSV', 'Import CSV');
define('FROM_NAME', 'From Name');
define('FROM_EMAIL_ADDRESS', 'From Email Address');
define('REPLY_NAME', 'Reply Name');
define('REPLY_EMAIL_ADDRESS', 'Reply Email Address');
define('NEW_SUBSCRIBE_EMAIL', 'New Subscribe Email');
define('UNSUBSCRIBE_EMAIL', 'Unsubscriber Email');
define('USER_DEFAULT_NEWSLETTER', 'User Default Newsletter');
define('SELECTED', 'Selected');
define('NO_TEMPLATE', 'No Template');
define('NUMBER_OF_EMAIL_SEND', 'Number Of Email Send');
define('BREAK_BETWEEN_NO_EMAIL_SEND', 'Break Between No. Email Send');
define('BREAK_TYPE', 'Break Type');
define('MAILER', 'Mailer');
define('SEND_MAIL_PATH', 'Send Mail Path');
define('IF_MAILER_IS_SENDMAIL', '(If Mailer is sendmail)');
define('SMTP_PORT', 'SMTP Port');
define('SMTP_HOST', 'SMTP Host');
define('SMTP_EMAIL', 'SMTP Email');
define('SMTP_PASSWORD', 'SMTP Password');
define('SEND_TEST_MAIL', 'Send Test Mail');
define('PHP_MAIL', 'PHP Mail');
define('SMTP', 'SMTP');
define('SENDMAIL', 'Sendmail');
define('EMAIL_SETTING', 'Email Setting');
define('SENDER_EMAIL', 'Sender Email');
define('RECEIVER_EMAIL', 'Receiver Email');
define('SENT_MESSAGE_ON_SENDING_A_TEST_MAIL', 'Sent message on sending a test mail');

define('WEEKLY_DAY', 'Weekly Day');
define('MONTHLY_DAY', 'Monthly Day');
define('END_DATE_MUST_BE_GREATER_THAN_START_DATE', 'End date must be greater then start date');
define('FAIL', 'Fail');
define('CONTENT', 'Content');
define('UPLOAD_CSV_ONLY', 'Upload csv only');
define('NAMES','Name');


/*admin_graph_lang.php*/
define('GRAPHICAL_REPORT_OF_PROJECT','Graphical Report of '.$project.'');
define('WEEKLY_AVERAGE_PROJECTS_LINE','Weekly Average '.$project.'s [Line]');
define('WEEKLY_AVERAGE_PROJECTS_BAR','Weekly Average '.$project.'s [Bar]');
define('MONTHLY_AVERAGE_PROJECTS_LINE','Monthly Average '.$project.'s [Line]');
define('MONTHLY_AVERAGE_PROJECTS_BAR','Monthly Average '.$project.'s [Bar]');
define('YEARLY_AVERAGE_PROJECTS_LINE','Yearly Average '.$project.'s [Line]');
define('YEARLY_AVERAGE_PROJECTS_BAR','Yearly Average '.$project.'s [Bar]');
define('TOTAL_PROJECTS_PIE_CHART','Total '.$project.'s Pie Chart');
define('TRANSACTION_PROJECTS_PIE_CHART','Transactions '.$project.' Pie Chart');
define('GRAPHICAL_REPORT_OF_USER_REGISTRATION','Graphical Report of User Registration');
define('WEEKLY_AVERAGE_REGISTRATION_LINE','Weekly Average Registration [Line]');
define('WEEKLY_AVERAGE_REGISTRATION_BAR','Weekly Average Registration [Bar]');
define('MONTHLY_AVERAGE_REGISTRATION_LINE','Monthly Average Registration [Line]');
define('MONTHLY_AVERAGE_REGISTRATION_BAR','Monthly Average Registration [Bar]');
define('YEARLY_AVERAGE_REGISTRATION_LINE','Yearly Average Registration [Line]');
define('YEARLY_AVERAGE_REGISTRATION_BAR','Yearly Average Registration [Bar]');

define('WITH_STACKING', 'With stacking');
define('WITHOUT_STACKING', 'Without stacking');
define('BARS', 'Bars');
define('LINES', 'Lines');
define('LINES_WITH_STEPS', 'Lines with steps');
define('REGISTRATION', 'Registration');


/*admin_email_lang.php*/

define('MASTER_EMAIL_TEMPLATE','Master Email Template');
define('SELECT_EMAIL_TEMPLATE','Select Email Template');
define('EMAIL_FOR_TASK','Email For Task');

/*admin_contant_lang.php*/


define('ORDER','Order');
define('ANSWER','Answer');
define('ADD_PAGES','Add pages');
define('CATEGORY_NAME','Category Name');
define('DISPLAY_PLACE','Display Place');
define('FOOTER','Footer');
define('PAGES_SIDEBAR','Pages Sidebar');
define('LEARN_CATEGORY','Learn Category');
define('PAGES_TITLE','Pages Title');
define('SUB_TITLE','Sub Title');
define('ICON','Icon');
define('SELECT_FILE','Select file');
define('CHANGE','Change');
define('REMOVE','Remove');
define('SLUG','Slug');
define('META_KEYWORD','Meta Keyword');
define('META_DESCRIPTION','Meta Description');
define('SIDEBAR','Sidebar');
define('RECORDS_HAS_BEEN_DELETED_SUCCESSFULLY','Record[s] has been deleted successfully.');
define('RECORD_HAS_BEEN_ACTIVATED_SUCCESSFULLY','Record[s] has been activated successfully.');
define('RECORD_HAS_BEEN_INACTIVATED_SUCCESSFULLY','Record[s] has been inactivated successfully.');
define('RECORD_HAS_BEEN_SHOW_ON_HELP_PAGE_SUCCESSFULLY','Record[s] has been show on help page successfully.');
define('RECORD_HAS_BEEN_REMOVE_ON_HELP_PAGE_SUCCESSFULLY','Record[s]) has been remove on help page successfully.');
define('YOU_CAN_NOT_DEACTIVATED_ALL_CURRENCIES','You can not deactivate all currencies. There should be one active.');
define('RECORDS_HAS_BEEN_MAKE_PERMENANT_SUCCESSFULLY','Record has been make Permenant successfully.');

define('LEARN_MORE_CATEGORY','Learn More Category');
define('LEARN_CATEGORY_NAME','Learn Category Name');
define('LEARN_MORE_PAGES','Learn More Pages');
define('MORE','More');
//==============Payment Gateway===========================
define('WALLET_SETTING','Wallet Setting');
define('WALLET_ADD_TIME_FEES','Wallet Add Time Fees[%]');
define('WALLET_ADD_MIN_AMOUNT','Wallet Add Time Minimum Amount');
define('WALLET_WITHDRAW_FEES','Wallet Withdraw[Donate] Time Fees[%] ');
define('DIRECT_DONATE_OPTION','Direct donate option');
define('ADD_WHOLE_DONATION_AMOUNT','Add whole donation amount');
define('ADD_REMAINING_DONATION_AMOUNT','Add remaining donation amount');
define('WALLET_REVIEW','Wallet Review');
define('REVIEW','Review');
define('CONFIRM','Confirm');
define('AMOUNT_ADDED','Amount Added');
define('GATEWAY','Gateway');
define('WALLET_WITHDRAW','Wallet Withdraw');
define('CURRENT_AMOUNT','Current Amount');
define('AMOUNT_TO_PAY','Amount To Pay');
define('AMAZON_GATEWAY','Amazon Gateway');
define('VARIABLE_FEES','Variable Fees');
define('FIXED_FEES','Fixed Fees');
define('SUPPORT_MASSPAYMENT','SUPPORT MASSPAYMENT');
define('Support_Masspayment','Support Masspayment');
define('FUNCTION_NAME','Function Name');
define('Name','Name');
define('Value','Value');
define('Label','Label');
define('Description','Description');
define('Action','Action');
define('Image','Image');
define('LANGUAGE_IS_ALREADY_EXIST','Language is already exist');
define('ALLOW_ONLY_IMAGE_FILE_TO_UPLOAD','Allow only Image File to Upload');
define('MESSAGE_SUBJECT','Message subject');
define('FUNCTION_NAMES','Function Name');
define('SUAPPORT_MASSPAYMENT','Suapport Masspayment');
define('PAGES_DESCRIPTION','Page description');
define('EXTERNAL_URL','External url');
define('FAQ_CATEGORY','Faq category');
define('QUESTIONS','Question');
define('ACTIONS','Action');
//all confirm message
define('DELETE_USER_CONFIRMATION','Are you sure,You want to delete user?');
define('DELETE_SELECT_RECORD_CONFIRMATION','Please select a record');
define('DELETE_CATEGORY_CONFIRMATION','Are you sure,You want to delete this category?');
define('DELETE_PAGE_CONFIRMATION','Are you sure,You want to delete this page?');
define('DELETE_COUNTRY_CONFIRMATION','Are you sure,You want to delete country?');
define('DELETE_CURRENCY_CONFIRMATION','Are you sure,You want to delete currency?');
define('DELETE_GATEWAY_DETAIL_CONFIRMATION','Are you sure,You want to delete Gateways detail?');
define('DELETE_ADMINISTRATOR_CONFIRMATION','Are you sure,You want to delete Administrator?');
define('DELETE_GATEWAY_PAYMENT_CONFIRMATION','Are you sure,You want to delete Payment Gateways?');
define('ARE_YOU_SURE_YOU_WANT_TO_DELETE_SELECTED_LEARN_CATEGORIES', 'Are you sure, you want to delete selected learn categories?');
define('ARE_YOU_SURE_YOU_WANT_TO_ACTIVE_SELECTED_LEARN_CATEGORIES', 'Are you sure, you want to active selected learn category?');
define('ARE_YOU_SURE_YOU_WANT_TO_INACTIVE_SELECTED_LEARN_CATEGORIES', 'Are you sure, you want to inactive selected learn category?');
define('ARE_YOU_SURE_YOU_WANT_TO_DELETE_SELECTED_MESSAGE', 'Are you sure, you want to delete selected message(s)?');
define('TYPE_A_MESSAGE_HERE', 'Type a message here...');
define('RECORDS_UPDATED_BY_THIS_CRONJOB', 'Records updated by this cronjob');
define('NO_RECORDS_UPDATED_BY_THIS_CRONJOB', 'No Records updated by this cronjob');
define('PAYMENT_GATEWAYS', 'Payment Gateways');
define('WITHDRAW_REQUEST', 'Withdraw Request');
define('TEST_UPDATE', 'Test & Update');
define('GATEWAYS_DETAILS', 'Gateways Details');

//CATEGORY
define('ADD_CATEGORY', 'Add Category');
define('EDIT_CATEGORY', 'Edit Category');
define('PARENT', 'Parent');
define('CHILD', 'Child');
define('TYPE', 'Type');
define('CATEGORY_LIST', 'Category list');

//Phase
define('ADD_PHASE', 'Add Phase');
define('EDIT_PHASE', 'Edit Phase');
define('PHASE_LIST', 'Phase list');
define('DELETE_PHASE', 'Are you sure, you want to delete this phase?');
define('PHASE_NAME', 'Phase Name');
define('PHASE', 'Phases');
define('LOADING', 'Loading');
define('EQUITY_CATEGORIES', ''.$project_name.' Categories');
define('PROJECT_PHASE', ''.$project_name.' Phases');

define('MAIN', 'Main');
define('RELATED', 'Related');
define('CATEGORY_DESCRIPTION', 'Category Description');


/*admin_accredation_lang.php*/

define('ACCREDITATION_USERS', 'Accreditation Users');
define('ACCREDITATION_USER', 'Accreditation User');
define('ACCREDITATION_USERS_DETAIL', 'Accreditation Users Details');
define('APPROV', 'Approve');

define('ACCREDITATION_DETAILS', 'Accreditation Details');
define('INCOME', 'Income');

define('VERIFICATION_OPTIONS', 'Verification Options');
define('DOWNLOAD', 'Download');

define('REJECT', 'Reject');
define('REJECTED', 'Rejected');
define('YOU_CAN_APPROVE', 'You can approve');
define('YOU_CAN_REJECT', 'You can reject');

/*activity_lang.php*/

define('REVIEW_ALL_ACTIVITY','Review all Activity');

define('FOLLOW_BY','Follow by');

define('MY_COMMENT_TEXT','My '.$comments.' text');

define('COMMENT_ON_PROJECT',''.$comments.' on  '.$project);

define('WELCOME','Welcome');

define('FOLLOW_PROJECT_TEXT','Follow '.$project.' text');

define('PROJECT_FOLLOW_BY_ME',''.$project.' follow by me');

define('MY_POST_PROJECT','My post '.$project);

define('FOLLOWER_POST_PROJECT',$follower.' post '.$project);

define('FOLLOWER_PROJECT_UPDATES',''.$follower.' '.$project.' updates');

define('THERE_ARE_NO_ACTIVITIES','There are no Activities.');

define('MY_FOLLOWERS_FOLLOW','My '.$follower.' follow');

define('FOLLOWING_POST_PROJECT','Following post '.$project);

define('FOLLOWING_DONATION','Following Donation');

define('FOLLOWING_COMMENTS','Following '.$comments);

define('FOLLOWING_UPDATES','Following updates');

define('FOLLOWING_GALLERY','Following gallery');

define('FOLLOWING_PROJECT','Following Project');

define('ADD_GALLERY_IN_PROJECT','Add gallery in '.$project);

define('FOLLOWER_PROJECT_DONATION','Follower '.$project.' donation');

define('DONATION_ON_PROJECT','Donation on '.$project);
///added for only activity messages
define('YOU_HAVE_STARTED_FOLLOWING','You have started %s %s');
define('USER_HAVE_STARTED_FOLLOWING_YOU','%s has started %s you.');
define('USER_HAVE_STARTED_FOLLOWING_USER','%s has started %s %s');
define('YOU_HAVE_STARTED_UNFOLLOWING','You have started %s %s');
define('USER_HAVE_STARTED_UNFOLLOWING_YOU','%s has started %s you.');
define('USER_HAVE_STARTED_UNFOLLOWING_USER','%s has started %s %s');
define('USER_JOINED_SITE','%s joined %s');
define('YOU_UPDTAED_ACCOUNT_INFORMATION','You have updated your account information');
define('USER_UPDTAED_ACCOUNT_INFORMATION','%s has updated account information');
define('YOU_APPLIED_TO_BECOME_ACCREDITED','You applied to become accredited %s');
define('USER_APPLIED_TO_BECOME_ACCREDITED','%s applied to become %s %s');


define('USER_HAVE_STARTED_FOLLOWING_YOUR_CAMPAIGNS','%s has started %s you created %s');
define('USER_HAVE_STARTED_FOLLOWING_USER_ON_CAMPAIGNS','%s has started %s %s you already %s');
define('USER_HAVE_STARTED_UNFOLLOWING_YOUR_CAMPAIGNS','%s has started %s you created %s');
define('USER_HAVE_STARTED_UNFOLLOWING_USER_ON_CAMPAIGNS','%s has started %s %s you already %s');

define('USER_HAVE_STARTED_FUNDING_USER_ON_CAMPAIGNS','%s has started %s %s you already %s');
define('USER_HAVE_STARTED_COMMNENT_USER_ON_CAMPAIGNS','%s has started %s %s you already %s');
define('USER_HAVE_STARTED_TEAMMEMBER_USER_ON_CAMPAIGNS','%s has started %s %s you already a team member on.');
define('USER_HAVE_STARTED_FOLLOWING_ADMIN','%s has started %s %s');


define('USER_HAVE_STARTED_FUNDING_USER_ON_CAMPAIGNS_UNFOLLOW','%s has started %s %s you already %s');
define('USER_HAVE_STARTED_COMMNENT_USER_ON_CAMPAIGNS_UNFOLLOW','%s has started %s %s you already %s');
define('USER_HAVE_STARTED_TEAMMEMBER_USER_ON_CAMPAIGNS_UNFOLLOW','%s has started %s %s you already a team member on.');
define('USER_HAVE_STARTED_FOLLOWING_ADMIN_UNFOLLOW','%s has started %s %s');
//updates
define('YOU_HAVE_STARTED_UPDATES','You have posted a new %s on %s');
define('USER_HAVE_STARTED_UPDATES_FOR_FOLLOWING_USER','%s has posted new %s on %s you already %s');
define('USER_HAVE_STARTED_UPDATED_FOR_DONOR_USER','%s has posted new %s on %s you already %s');
define('USER_HAVE_STARTED_UPDATES_FOR_COMMENTER_USER','%s has posted new %s on %s you already %s');
define('USER_HAVE_STARTED_UPDATES_FOR_TEAMMEMBER_USER','%s has posted new %s on %s you already a team member on');
define('USER_HAVE_STARTED_UPDATE_ADMIN','%s has posted new %s on %s');
//funded
define('YOU_HAVE_STARTED_FUND','You have %s %s with %s');
define('USER_HAVE_STARTED_FUND_PROJECT_OWNER','%s you already created is %s with %s by %s');
define('USER_HAVE_STARTED_FUND_FOR_FOLLOWING_USER','%s you already %s is %s with %s by %s');
define('USER_HAVE_STARTED_FUND_FOR_DONOR_USER','%s you already %s is %s with %s by %s');
define('USER_HAVE_STARTED_FUND_FOR_COMMENTER_USER','%s you already %s is %s with %s by %s');
define('USER_HAVE_STARTED_FUND_FOR_TEAMMEMBER_USER','%s is %s with %s by %s you already a team member on');
define('USER_HAVE_STARTED_FUND_ADMIN','%s is %s with %s by %s');

//commented
define('YOU_HAVE_STARTED_COMMENTED','You have %s on %s');
define('USER_HAVE_STARTED_COMMENTED_PROJECT_OWNER','%s %s on %s you already created');
define('USER_HAVE_STARTED_COMMENTED_FOR_FOLLOWING_USER','%s %s on %s you already %s');
define('USER_HAVE_STARTED_COMMENTED_FOR_DONOR_USER','%s %s on %s you already %s');
define('USER_HAVE_STARTED_COMMENTED_FOR_COMMENTER_USER','%s %s on%s you already %s');
define('USER_HAVE_STARTED_COMMENTED_FOR_TEAMMEMBER_USER','%s %s on %s you  are already a team member on');
define('USER_HAVE_STARTED_COMMENTED_ADMIN','%s  %s  on %s ');
//draft_project and submit
define('YOU_CREATED_PROJECT_DRAFT','You have created a new %s %s');
define('YOU_SUBMITED_PROJECT','You have submitted a new %s %s');
define('YOU_SUBMITED_PROJECT_ADMIN','%s submitted a new %s %s for review');
//admin project approve
define('ADMIN_PROJECT_APPROVED_OWNER','Your %s %s is approved and published by %s team');
define('ADMIN_PROJECT_APPROVED_ADMIN','You have approved and published %s %s');
//admin project declined
define('ADMIN_PROJECT_DECLINED_OWNER','Your %s %s is declined by %s ');
define('ADMIN_PROJECT_DECLINED_ADMIN','You have declined %s %s');
//admin project deleted
define('ADMIN_PROJECT_DELETED_OWNER','Your %s %s is deleted by %s team');
define('ADMIN_PROJECT_DELETED_ADMIN','You have deleted %s %s');

//admin project active
define('ADMIN_HAVE_ACTIVE_PROJECT','You have activated and published %s %s ');
define('ADMIN_HAVE_ACTIVE_PROJECT_OWNER','Your %s %s is activated and published by %s team');
define('ADMIN_HAVE_ACTIVE_PROJECT_FOR_FOLLOWING_USER','%s %s you already %s is activated and published by %s team');
define('ADMIN_HAVE_ACTIVE_PROJECT_FOR_DONOR_USER','%s %s you already %s is activated and published by %s team');
define('ADMIN_HAVE_ACTIVE_PROJECT_FOR_COMMENTER_USER','%s %s you already %s is activated and published by %s team');
define('ADMIN_HAVE_ACTIVE_PROJECT_FOR_TEAMMEMBER_USER','%s %s you already added as a team member is activated and published by %s team');
//admin project inactive
define('ADMIN_HAVE_INACTIVE_PROJECT','You have inactivated and unpublished %s %s');
define('ADMIN_HAVE_INACTIVE_PROJECT_OWNER','Your %s %s is inactivated and unpublished by %s team');
define('ADMIN_HAVE_INACTIVE_PROJECT_FOR_FOLLOWING_USER','%s %s you already %s is inactivated and unpublished by %s team');
define('ADMIN_HAVE_INACTIVE_PROJECT_FOR_DONOR_USER','%s %s you already %s is inactivated and unpublished by %s team');
define('ADMIN_HAVE_INACTIVE_PROJECT_FOR_COMMENTER_USER','%s %s you already %s is inactivated and unpublished by %s team');
define('ADMIN_HAVE_INACTIVE_PROJECT_FOR_TEAMMEMBER_USER','%s %s you already added as a team member is inactivated and unpublished by %s team');
//project feedback by onwer
define('PROJECT_FEEDBACK_OWNER_SUMBIITED_BY_OWENER','You have submitted %s %s for %s');
define('PROJECT_FEEDBACK_ADMIN_SUMBIITED_BY_OWENER','%s has submitted %s %s for %s');

//project feedback by admin
define('PROJECT_FEEDBACK_OWNER_SUMBIITED_BY_ADMIN','%s team has submitted %s %s for %s');
define('PROJECT_FEEDBACK_ADMIN_SUMBIITED_BY_ADMIN','You have submitted %s %s for %s');

//team member added
define('TEAM_MEMBER_ADDED_OWNER','You have added team member on %s');
define('TEAM_MEMBER_ADDED_ADMIN','%s has added a team member on %s');
define('TEAM_MEMBER_ADDED_USER','%s has added a team member on %s');

//inrest request
define('INTREST_REQUEST_OWNER','You have received %s to become %s on %s from %s');
define('INTREST_REQUEST_USER','You have expressed your interest to become %s on %s');
//access request
define('ACCESS_REQUEST_OWNER','You have received %s to access %s on %s from %s');
define('ACCESS_REQUEST_USER','You have sent your access request for %s to access %s');
//upload contract document
define('UPLOAD_CONTRACT_DOCUMENT_USER','You have %s %s process  contract copy on %s');
define('UPLOAD_CONTRACT_DOCUMENT_ADMIN','%s has %s %s  contract copy on %s');
//approve contract document
define('APPROVED_CONTRACT_DOCUMENT_USER','Your %s contract copy has been %s on %s by %s team');
define('APPROVED_CONTRACT_DOCUMENT_ADMIN','You have %s %s contract copy from %s on %s');
//declined contract document
define('DECLINED_CONTRACT_DOCUMENT_USER','Your %s contract copy has been %s on %s by %s team');
define('DECLINED_CONTRACT_DOCUMENT_ADMIN','You have %s %s contract copy from %s on %s');
//acknowledgement contract document
define('ACKNOWLEDGEMENT_CONTRACT_DOCUMENT_USER','You have %s %s  acknowledgement information on %s');
define('ACKNOWLEDGEMENT_DOCUMENT_ADMIN','%s has %s %s  acknowledgement information on %s');
//approve acknowledgement contract document
define('APPROVE_ACKNOWLEDGEMENT_CONTRACT_DOCUMENT_USER','Your %s  acknowledgement has been %s on %s by %s team');
define('APPROVE_ACKNOWLEDGEMENT_DOCUMENT_ADMIN','You have %s %s acknowledgement from %s on %s');
//declined acknowledgement contract document
define('DECLINED_ACKNOWLEDGEMENT_CONTRACT_DOCUMENT_USER','Your %s  acknowledgement has been %s on %s by %s team');
define('DECLINED_ACKNOWLEDGEMENT_DOCUMENT_ADMIN','You have %s %s acknowledgement from %s on %s');
//submitted tracking
define('SUBMITTED_TRACKING_CONTRACT_DOCUMENT_USER','%s team has %s share certificate/docuemnt transmission information on %s');
define('SUBMITTED_TRACKING_DOCUMENT_ADMIN','You have %s share certificate/document transmission information on %s for %s');
//confirmed submitted tracking
define('CONFIRMED_SUBMITTED_TRACKING_CONTRACT_DOCUMENT_USER','You have %s delivery of  share certificate/document  on %s');
define('CONFIRMED_SUBMITTED_TRACKING_DOCUMENT_ADMIN','%s has %s delivery of  share certificate/document  on %s');

// project success
define('ADMIN_HAVE_SUCCESS_PROJECT','%s %s created by %s ended successfully');
define('ADMIN_HAVE_SUCCESS_PROJECT_OWNER','Your %s %s ended successfully ');
define('ADMIN_HAVE_SUCCESS_PROJECT_FOR_FOLLOWING_USER','%s %s you %s ended successfully');
define('ADMIN_HAVE_SUCCESS_PROJECT_FOR_DONOR_USER','%s %s you %s ended successfully');
define('ADMIN_HAVE_SUCCESS_PROJECT_FOR_COMMENTER_USER','%s %s you %s ended successfully');
define('ADMIN_HAVE_SUCCESS_PROJECT_FOR_TEAMMEMBER_USER','%s %s you are team member on ended successfully');
// project unsuccess
define('ADMIN_HAVE_UNSUCCESS_PROJECT','%s %s created by %s ended unsuccessfully');
define('ADMIN_HAVE_UNSUCCESS_PROJECT_OWNER','Your %s %s ended unsuccessfully ');
define('ADMIN_HAVE_UNSUCCESS_PROJECT_FOR_FOLLOWING_USER','%s %s you %s ended unsuccessfully');
define('ADMIN_HAVE_UNSUCCESS_PROJECT_FOR_DONOR_USER','%s %s you %s ended unsuccessfully');
define('ADMIN_HAVE_UNSUCCESS_PROJECT_FOR_COMMENTER_USER','%s %s you %s ended unsuccessfully');
define('ADMIN_HAVE_UNSUCCESS_PROJECT_FOR_TEAMMEMBER_USER','%s %s you are team member on ended unsuccessfully');
define('UPLOADED','Uploaded');
define('APPROVED_AND_CONFIRMED','Approved and Confirmed');
define('ACTIVITIES','Activities');



 define('FUNDRISE_USES_THIS_INFORMATION_TO_DISTRIBUTE_YOUR_DIVIDENDS_AND_TAX_DOCUMENTS_IF_YOU_BELIEVE_YOU_HAVE_ENTERED_YOUR_DATE_OF_BIRTH_OR_SOCIAL_SECURITY_NUMBER_INCORRECTLY_PLEASE', 'Fundrise uses this information to distribute your dividends and tax documents. If you believe you have entered your date of birth or Social Security number incorrectly, please');

define('LEGAL_FIRST_NAME', 'Legal First Name');

define('LEGAL_LAST_NAME', 'Legal Last Name');

define('THIS_MUST_BE_YOUR_PRINCIPAL_RESIDENCE', 'This must be your principal residence.');

define('PHONE_NUMBER', 'Phone Number');

define('BANK_DETAIL', 'Bank Details');

define('CHECKING', 'Checking');

define('SAVINGS', 'Savings');

define('NAME_OF_BANK_AS_IT_APPEARS_ON_YOUR_PAPER_CHECKS', 'Name of bank as it appears on your paper checks.');

define('EXAMPLE', 'Example');

define('CONFIRM_ACCOUNT_NO', 'Confirm Account No');

define('CONFIRM_YOUR_ACCOUNT_NUMBER_BY_TYPING_IT_AGAIN', 'Confirm your account number by typing it again.');

define('DIGIT_NUMBER_ON_THE_BOTTOM_LEFT_OF_YOUR_PAPER_CHECKS', '9-digit number on the bottom left of your paper checks.');

define('PERSONAL_INFORMATION_ADDED_SUCCESSFULLY', 'Personal information added successfully..!');

define('ENTER_ADDRESS', 'Enter Address');

define('CITY_IS_REQUIRED', 'City is required.');

define('STATE_IS_REQUIRED', 'State is required.');

define('ENTER_PHONE', 'Enter Phone number.');

define('ENTER_ACCOUNT_NUMBER', 'Enter Account Number.');

define('ENTER_BANK_NAME', 'Enter Bank Name.');

define('ACCOUNT_NUMBER_CANNOT_BE_MORE_THAN_40', 'Account Number cannot be more than 40 character.');

define('ACCOUNT_NUMBER_CANNOT_BE_LESS_THAN_4', 'Account Number cannot be less than 4 character.');

define('ENTER_ACCOUNT_NUMBER_CON', 'Enter Confirm Account Number.');

define('ENTER_ACCOUNT_NUMBER_CAN_NOT_MATCH', 'The Confirm Account No. does not match with Account Number.');

define('INVESTMENT_ACCOUNT', 'Investment Account');

define('FULL_NAME', 'Full Name');


define('ENTER_ROUTING_NUMBER', 'Enter Routing number.');

define('ACCREDITATION_STATUS', 'Accreditation Status');

define('ACCREDITED', 'Accredited');

define('EXTERNAL_BANK_ACCOUNTS', 'External Bank Accounts');

define('PRIMARY', 'Primary');

define('PLEASE_UPLOAD_DOC_OR_PDF_FILE', 'Please upload doc or pdf file');

define('DUE_TO_EXISTING_SECURITIES_REGULATIONS_THE_MAJORITY_OF_CROWDFUNDING_OFFERINGS_ARE_CURRENTLY_ONLY_AVAILABLE_TO_ACCREDITED_INVESTORS_PLEASE_INDICATE_YOUR_ACCREDITATION_STATUS_BELOW','Due to existing securities regulations, the majority of Crowdfunding offerings are currently only available to accredited investors. Please indicate your accreditation status below.');

define('I_AM_AN_ACCREDITED_INVESTOR', ' I am an accredited investor.');

define( 'I_AM' , 'I am');

define('NOT', 'NOT');

define('AN_ACCREDITED_INVESTOR', 'an accredited investor.');

define('ACCREDITATION_TYPE', 'Accreditation Type');

define('PLEASE_CHECK_ALL_OF_THE_FOLLOWING_THAT_APPLY', 'Please check all of the following that apply:');

define('NET_WORTH', 'Net Worth');

define('I_HAVE_INDIVIDUAL_NET_WORTH_OR_JOINT_NET_WORTH_WITH_MY_SPOUSE_THAT_EXCEEDS_1_MILLION_EXCLUDING_THE_VALUE_OF_MY_PRIMARY_RESIDENCE', 'I have individual net worth, or joint net worth with my spouse, that exceeds $1 million (excluding the value of my primary residence).');

define('INDIVIDUAL_INCOME', 'Individual Income');

define('I_HAVE_INDIVIDUAL_INCOME_EXCEEDING', ' I have individual income exceeding');

define('IN_EACH_OF_THE_PAST_TWO_YEARS_AND_EXPECT_TO_REACH_THE_SAME_THIS_YEAR', ' in each of the past two years and expect to reach the same this year');

define('JOINT_INCOME', 'Joint Income');

define('I_HAVE_COMBINED_INCOME_WITH_MY_SPOUSE_EXCEEDING', 'I have combined income with my spouse exceeding');

define('BUSINESS', 'Business');

define('IN_ASSETS_AND_OR_ALL_OF_THE_EQUITY_OWNERS_ARE_ACCREDITED', 'in assets and/or all of the equity owners are accredited');

define('I_INVEST_ON_BEHALF_OF_A_BUSINESS_OR_INVESTMENT_COMPANY_WITH_MORE_THAN', 'I invest on behalf of a business or investment company with more than');

define('VERIFICATION', 'Verification');

define('DUE_TO_EXISTING_SECURITIES_REGULATIONS_CROWDFUNDING_IS_REQUIRED_TO_DETERMINE_THAT_INVESTORS_ARE_QUALIFIED_TO_PARTICIPATE_IN_THE_INVESTMENTS_OFFERED_ON_THE_WEBSITE_PLEASE_HELP_US_DETERMINE_YOUR_ACCREDITATION_STATUS_BY_CHOOSING_ONE_OF_THE_FOLLOWING_OPTIONS', 'Due to existing securities regulations, Crowdfunding is required to determine that investors are qualified to participate in the investments offered on the website. Please help us determine your accreditation status by choosing one of the following options');

define('OPTION_A', 'Option A');

define('PHONE_CALL', 'Phone Call');

define('HAVE_A_PHONE_CALL_WITH_A_MEMBER_OF_THE_CROWDFUNDING_INVESTMENTS_TEAM', 'Have a phone call with a member of the Crowdfunding investments team');

define('REQUEST_A_PHONE_CALL', 'Request A Phone Call');

define('OPTION_B', 'Option B');

define('ACCREDITATION_LETTER', 'Accreditation Letter');

define('HAVE_YOUR_PERSONAL_BROKER_CERTIFIED_PUBLIC_ACCOUNTANT_INVESTMENT_ADVISOR_OR_LICENSED_ATTORNEY_COMPLETE_OUR_STANDARD_ACCREDITATION_LETTER', 'Have your personal broker, certified public accountant, investment advisor or licensed attorney complete our standard accreditation letter');

define('UPLOAD_A_COMPLETED_LETTER','Upload A Completed Letter');

define('DOWNLOAD_BLANK_LETTER', 'Download Blank Letter');
	
define('ACCREDITATION_ADDED_SUCCESSFULLY', 'Accreditation information added Successfully');

define('YOU_HAVE_ALREADY_APPLIED_FOR_ACCREDITATION', 'You have already applied for accreditation');

define('FIRST_FILL_PERSONAL_INFORMATION', 'First fill your Personal Information please.');


define('APPROVE', 'Approve');
define('ACCOUNT_NUMBER', 'Account Number');
define('ZIP_CODE', 'Zipcode');
define("DOC_OR_PDF", "DOC, PDF, ZIP, JPG, PNG, GIF ");
define('PLEASE_UPLOAD_DOC_IMAGE_ZIP_PDF','Please upload a .doc, .ods, .pdf, .zip, .odt, .rtf, .jpg, .png and .gif file');
define('SLIDER_IMAGE_REQUIRED', 'Slider Image is required.');
define('SELECT_IMAGE_MORE_THAN_1300_600', 'Please select image more than 1300 * 600.');
define('THERE_IS_SOME_PROBLEM_SITE_TRY_AFTER_SOMETIME', 'There is some problem with site. Try after sometime');
?>