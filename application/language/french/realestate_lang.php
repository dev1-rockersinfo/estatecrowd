<?php
$taxonomy_setting = taxonomy_setting();
//instead of project or campaign replace this name
$project = $taxonomy_setting['project_name'];
$funds = $taxonomy_setting['funds'];
$comments = $taxonomy_setting['comments'];
$comment = $taxonomy_setting['comments'];
$follower = $taxonomy_setting['follower'];
$project_name = $taxonomy_setting['project_name'];
$site_setting = site_setting();
$site_name = $site_setting['site_name'];
$funds_plural = $taxonomy_setting['funds_plural'];
$investor = $taxonomy_setting['investor'];

define("HOME", "Accueil"); //rightmenu(7)
define("ABOUT", "Sur:");
define("PROJECTS",  $project);
define("BLOG", "Blog");
define("RAISED", "Relevé");
define("LOG_IN", "S'identifier");
define("SIGN_UP", "Signer");
define("SIGN_OUT", "Se Déconnecter");
define("BROWSE", "Feuilleter");
define("YOUTUBE_CREATORS", "YouTube Créateurs");
define("CREATE", "Créer");
define("CREATE_A_CAMPAIGN","Créer un ".$project);
define("NEW_COMPAIGN","New ". $project);
define("COMPAIGN", $project);
define("WHAT_IS", "Qu'est-ce que");
define("LEARN", "Apprendre");
define("DASHBOARD", "Tableau de bord");
define("MY_PROJECTS","Mon ".$project." Est");
define("START_YOUR_PROJECT","Commencez votre ".$project);
define("TYPE_AND_ENTER", "Tapez et entrez.");
define("HI", "Salut");
define("TO_YOUR_ACCOUNT", "Pour votre compte");
define("JOIN_WITH_US", "Joignez-vous à nous");
//Footer
define("ABOUT_US", "A Propos De Nous"); //rightmenu (8)
define("TERMS_AND_CONDITIONS", "Termes et conditions"); //rightmenu(9)
define("PRIVACY_POLICY", "Politique de confidentialité");
define("CAREERS", "Carrières");
define("PRESS", "Presse");
define("WHY", "Pourquoi");
define("FEATURES", "Caractéristiques"); //RIGHTMENU (14)
define("FEES", "Honoraires"); //RIGHTMENU (15)
define("FAQ", "FAQ"); //RIGHTMENU(16)
define("HOW_TUBESTART_WORKS", "Comment œuvres tubestart"); //(17)
define("HOW", "Comment");
define("WORKS", "Œuvres");
define("HELP", "Aidez-Moi");
define("CONTACT_US", "Contactez Nous");
define("ALL_RIGHT_RESERVED", " Tous Droits Réservés");
define("WEEKLY_NEWSLETTER", "Lettre d'information hebdomadaire");
define("LETS_SOCIALIZE", "Disons socialiser");
define("GET_AWESOME_NEWS", "Recevoir nouvelles génial livré à votre boîte de réception chaque semaine");
define("FACEBOOK", "Facebook");
define("TWITTER", "Twitter");
define("GOOGLE_PLUS", "Google Plus");
define("LINKEDIN", "Linkedin");
define("YOUTUBE", "Youtube");
//a
define("ENTER_YOUR_EMAIL", "Entrez votre email"); //10
define("ENTER_VALID_EMAIL", "Entrez e-mail valide"); //10,13,30
define("COPYRIGHT", "2013"); //152
define("ROCKERS_TECHNOLOGIES", "ROCKERS TECHNOLOGIES"); //158
define("DESIGNED_BY", "Conçu Par"); //158
//content
define("DAYS_LEFT", "Jours Restants");
define("DAYS_TO_GO", "Days To Aller");
define("HOURS_LEFT", "Heures Qui Reste");
define("MINUTES_LEFT", "Minutes Gauche");
define("SECONDS_LEFT", "Secondes");
//a
define("LATEST_PROJECT","Dernières ".$project." De"); //131
define("VIEW_CAMPAIGN","Vue ". $project); //104
//Recherche
define("SEARCH_BY", "Rechercher Par");
define("SEARCH", "Recherche");
define("PLEASE_ENTER_SEARCH_KEYWORD", "S'il vous plaît entrer recherche par mot clé");
define("ALL_PROJECTS","All ". $project." Est");
define("FEATURED", "Sélection");
define("ROUNDING_OFF", "Arrondir");
define("POPULAR", "Populaire");
define("LATEST_COMPAIGNS","Dernières ". $project." De");
define("NEAR_ME", "Proche De Moi");
define("SUCCESS_STORY", "Réussites");
define("MOST_FUNDED", "La plupart subventionnées");
define("ADVANCED_SEARCH", "Recherche Avancée");
define("KEYWORDS", "Mots-clés");
define("TITLE", "Titre");
define("CATEGORY", "Catégorie");
define("CITY", "Ville");
define("COUNTRY", "Pays");
define("GOAL_SIZE", "Objectif Taille");
define("STATUS", "Statut");
define("GOAL_TYPE", "Type de But");
define("CELEBRATIONS_AND_SPECIAL_EVENTS","Célébrations et événements spéciaux tester l'éducation ".$funds);
define("CATEGORIES", "Catégories");
define("SELECT_CATEGORY", "Choisir Une Catégorie");
define("SELECT_COUNTRY", "Choisissez Le Pays");
define('SIGNUP',"Signer");
define('NEW_TO',"Pour New");
define('NAME',"Nom Complet");
define('EMAIL_ADDRESS',"Adresse e-mail");
define('RETYPE_EMAIL',"Confirmer votre Email");
define('PASSWORD',"Mot de passe");
define('RETYPE_PASSWORD',"Retaper Le Mot De Passe");
//define('DISCOVER_NEW_PROJECTS_WITH_WEEKLY_NEWSLETTER_PLEASE_ACCEPT_TERMS_CONDITION_TO_ENJOY_YOU_AGREE_TO_OUR',"Discover new projects with weekly Bulletin Please accept terms & condition to enjoy %s, you agree to our");
define('DISCOVER_NEW_PROJECTS_WITH_WEEKLY_NEWSLETTER_PLEASE_ACCEPT_TERMS_CONDITION_TO_ENJOY_YOU_AGREE_TO_OUR',"En cliquant sur Inscription %s, vous acceptez nos ");
define('NEW_PROJECTS_WITH_WEEKLY_NEWSLETTER', "Je voudrais le bulletin de l'hebdomadaire%.");
define('TERMS_OF_USE',"Conditions D'Utilisation");
define('LOGIN',"S'identifier");
define('SUCCESS',"Succès");
define('WE_WILL_NEVER_POST_ANYTHING_WITHOUT_YOUR_PERMISSION',"Nous ne serons jamais poster quoi que ce soit sans votre permission.");
define('EXISTS_ACCOUNT_ASSOCIATE_WITHMAIL',"Il est déjà un compte associé à ce courriel.");
define('YOU_HAVE_SIGNED_UP_SUCCESSFULLY',"Vous avez signé avec succès. Vos détails de compte envoyés à votre adresse e-mail.");
define('PLEASE_LOGIN_TO_CONTINUE',"S'il vous plaît Connectez-vous pour poursuivre");
define('LOG_IN_TO_YOUR_ACCOUNT',"Connectez-vous à votre compte");
define('DONT_HAVE_AN_ACCOUNT', "Vous n'avez pas de compte?");
define('EMAIL_LOGIN',"Email Connexion");
define('LOGIN_WITH_AN_EMAIL_ADDRESS',"Connectez-vous avec une adresse e-mail");
define('LOGIN_WITH_A_SOCIAL_NETWORK',"Connexion avec un réseau social");
define('LOGIN_WITH_FACEBOOK_TWITTER_OR_LINKEDIN',"S'identifier avec Facebook, Twitter or LinkedIn");
define('SIGNUP_WITH_A_SOCIAL_NETWORK',"Inscrivez-vous à un réseau social");
define('QUICKLY_CREATE_AN_ACCOUNT',"Créez rapidement un compte");
define('SIGNUP_MANUALLY',"Inscrivez-vous manuellement");
define('CREATE_AN_ACCOUNG_USING_EMAIL',"Créez un compte en utilisant email");
define('REMEMBER_ME',"Souviens-Toi De Moi");
define('I_FORGOT_MY_PASSWORD',"J'ai oublié mon mot de passe.");
define('SOCIAL_LOG_IN',"Journal Social En");
define('EMAIL_ADDRESS_OR_PASSWORD_ARE_INVALID',"Adresse e-mail ou mot de passe est invalide.");
define('YOUR_EMAIL_ADDRESS_IS_SUSPENDED',"Votre adresse e-mail est suspendu par admin.");
define('YOUR_ACCOUNT_IS_NOT_ACTIVEPLEASE_CHECK_YOUR_INBOX_AND_CLICK_ON_ACTIVATION_LINK',"Votre compte est inactif. S'il vous plaît vérifier votre boîte de réception et cliquez sur le lien d'activation.");
define('YOU_ARE_SIGNED_OUT_SUCCESSFULLY',"Vous avez signé avec succès.");
define('REENVOYER_VARIFICATION_LINK',"Renvoyer le lien de vérification.");
define('ENVOYER_VARIFICATION',"Envoyer la vérification");
define('CLICK_HERE',"Cliquez Ici");
define('REENVOYER',"Renvoyer");
define('PLEASE_ENTER_EMAIL_TO_ENVOYER_VARIFICATION_LINK',"S'il vous plaît entrer un courriel à envoyer le lien de vérification");
define('PLEASE_ENTER_EMAIL_ADDRESS_FOR_VARIFICATION_LINK',"S'il vous plaît entrez l'adresse e-mail pour le lien de vérification");
define('EMAIL_ADDRESS_NOT_FOUND',"Adresse e-mail introuvable.");
define('YOUR_ACCOUNT_IS_ALREADY_ACTIVE',"Votre compte est déjà actif.");
define('FORGET_PASSWORD',"Mot de passe oublié?");
define('TELL_US_THE_EMAIL_YOU_USED_TO_SIGNUP_WELL_GET_YOU_LOGGED_IN', "Donnez-nous l'e-mail que vous avez utilisé pour vous inscrire, et nous allons vous connecté.");
define('RESET_MY_PASSWORD',"Réinitialiser Mon Mot De Passe");
define('CREATE_A',"Créer un");
define('ACCOUNT',"Compte");
define('ALREADY_HAVE_AN_ACCOUNT',"Vous avez déjà un compte?");
define('MAIL_SENT_SUCCESSFULLY',"Courrier envoyé avec succès");
define('BACK_TO_LOGIN',"Retour Pour se connecter");
define('PLEASE_ENTER_EMAIL',"S'il vous plaît Entrez Email");
define('SUBMIT',"Soumettre"); //login (179)
define('RESET_PASSWORD',"Réinitialiser Le Mot De Passe");
define('RESET',"Réinitialiser");
define('YOUR_ACCOUNT_ACTIVATED_SUCCESSFULLY',"Félicitations! Votre compte est activé avec succès. Maintenant, vous pouvez vous connecter avec vos informations de connexion.");
define('ERROR_OCCURED_IN_ACTIVATING_ACCOUNT',"Erreur dans le processus activation de votre compte. S'il vous plaît contacter l'administrateur.");
define('YOUR_DONATION_MADE_SUCCESSFULLY',"Votre don a été fait avec succès");
define('YOUR_DONATION_FAILED',"Your donation process failed, Try again.");
define('MESSAGE_ENVOYER_SUCCESSFULLY',"Message envoyé avec succès.");
define('YOUR_REQUEST_EXPIRED',"Votre lien est périmé.");
define('QUICK_LINKS',"Liens Rapides");
define('SOCIAL_SIGN_UP',"Signe social jusqu'à");
define('NEWSLETTER',"Bulletin");
define('LOGIN1',"S'identifier");
define('FILL_YOUR_INFO',"Remplissez vos informations");
define('FIRST_NAME',"Prénom");
define('UPLOAD_PHOTO',"Télécharger Une Photo");
define('CHANGE_PHOTO',"Changer La Photo");
define('LAST_NAME',"Nom De Famille");
define('SKIP',"Sauter");
define('SAVE_AND_CONTINUE',"Sauvegarder et continuer");
define('SAVE_AND_FINISH',"Sauvegarder et terminer");
define('ABOUT_YOURSELF',"À Propos De Toi");
define('INTREST',"Intérêt");
define('SKILL',"Compétence");
define('OCCUPATION',"Occupation");
define('VIEW_COMPAIGN',"Voir Compaign");
define('PLEASE_ENVOYER_EMAIL_FOR_VARIFICATION_LINK',"S'il vous plaît envoyez un courriel pour le lien de vérification.");
define('IPBAND_CANNOTREG_NOW_SIGNIN',"Votre adresse IP a été la bande à cause du spam. Vous ne pouvez pas vous connecter maintenant.");
define('IPBAND_CANNOTREG_NOW',"Votre adresse IP a été la bande à cause du spam. Vous ne pouvez pas vous inscrire maintenant.");
define('IP_BAND_CONTACT_WEB_MASTER',"Votre adresse IP a été la bande. S'il vous plaît contacter à l'admin.");
/*** contact form ******/



define('IPBAND_CANNOT_INQUIRY',"Votre adresse IP a été Band en raison de spam. Vous ne pouvez pas poster Inquire.");
define('INQUIRY_MESSAGE_SENT_SUCCESSFULLY',"Votre demande envoyé avec succès.");
define('ENVOYER',"ENVOYER");
define('BANDCAMP',"Bandcamp");
define('GROUP_FUND_CREATORS',"Groupe Fonds Créateurs");
define('GROUP_FUND',"Fonds du Groupe");
define('NO_RESULT_FOUND',"Aucun résultat trouvé. Utilisez les options dans la barre latérale de gauche pour affiner votre recherche.");
/******* */
define('INVITE_MSGS',"%s would like to add you to %s team on %s ,Login or Sign up to join the team");
define('NEW_THIS_WEEK',"Nouveautés de la semaine");
define('YOUR_ACCOUNT_INACTIVE_ADMIN',"Votre compte est inactif par admin.");
define('INACTIVE_ACCOUNT_AFTER_SIGNUP',"Votre compte est pas active. S'il vous plaît vérifier votre boîte de réception et cliquez sur le lien d'activation.");
define('RESEND_VERIFICATION_LINK',"Vérification lien Recevoir à nouveau");
define('RESET_PASSWORD_FAILED',"Envoyer une demande de réinitialisation de mot échoué. S'il vous plaît essayer à nouveau.");
define('YOUR_ACCOUNT_ALREADY_ACTIVE',"Votre compte est déjà activé.");
define('RESEND_VERIFICATION_FAILED',"Renvoyer verificatioin processus a échoué.");
define('WRONG_FACEDBOOK_SETTINGS',"There are some error occur with facebook API, please verify your facebook setting.");
define('TWITTER_CONNECTION_ERROR',"Impossible de se connecter à Twitter. Rafraîchissez la page ou réessayez plus tard.");
define('WRONG_TWITTER_SETTINGS',"There are some error occur with twiiter API, please verify your twitter setting.");
define('WRONG_LINKEDIN_SETTINGS',"There are some error occur with linkdin API, please verify your linkdin setting.");
define('SOCIAL_DETAILS_UPDATED_SUCCESSFULLY',"Détails sociaux mis à jour avec succès.");
define('OLD_PASSWORD_DOESNT_MATCH',"L'ancien mot de passe que vous avez saisi ne correspond pas avec votre ancien mot de passe.");
//header view 
define('YOUR_ACCOUNT_ACTIVATED_SUCCESSFULYY_LOGIN', "Félicitations! Votre compte est activé avec succès. Maintenant, vous pouvez vous connecter avec vos informations de connexion.");
define('YOUR_ACCOUNT_ACTIVATED_ALREADY_LOGIN', "Votre compte est déjà activé. Vous pouvez vous connecter avec vos informations de connexion.");
define('CONFIRM_ACCOUNT_ERROR', "Votre ont une erreur de confirmer compte.");
define('SIGN_UP_SUCCESSFULLY_DETAILS_SENT', "Vous avez signé avec succès. Vos détails de compte envoyés à votre adresse e-mail.");
define('WELCOME_LOGIN_SUCCESSFULL', "Bienvenue %s, vous êtes connecté avec succès.");
define('PASSWORD_RESET_SUCCESSFULLY_LOGIN', "Votre mot de passe correctement remis à zéro. Maintenant, vous pouvez prendre la connexion.");
define('EMAIL_VERIFICATION_LINK_RESEND', "lien de vérification de courriel a été renvoyé à votre adresse email avec succès.");
define('DRAFT_PROJECT_MESSAGE_HOME',"Cette ".$project."  est seulement un projet. Il ne peut pas être publier ". $project);
define('DONATION_DONE_SUCCESSFULLY_HOME', "Votre don fait avec succès.");
define('DONATION_FAILED_HOME', "Votre don est échoué.");
define('MESSAGE_SENT_SUCCESSFULLY_HOME', "Votre message envoyé avec succès.");
define('EMAIL_ALREADY_SUBSCRIBED_WITH_SITE', "Votre Email est déjà abonnez-vous avec le site.");
define('EMAIL_SUBSCRIBED_WITH_SITE', "Votre courriel Abonnez-succès.");
define('YOUR_ACCOUNT_IS_ALREADY_ACTIVATED', "Votre compte est déjà activé.");
//content views

define('YOUR_EMAIL',"Ton E-Mail");

define('LINK_YOUR_PROFILE_OR_PROJECT',"Lien vers votre".$project." ou le profil");
define('RESET_PASSWORD_LINK_SENT_SUCCESSFULLY_VIEW',"Lien Mot de passe Réinitialiser envoyé à votre adresse email avec succès.");
define('CONFIRM_PASSWORD',"Confirmez Le Mot De Passe");
define('SELECT_QUESTION_TYPE',"Sélectionnez question");


//validation social signup steps
define('EMAIL_IS_REQUIRED',"Email est requis.");
define('EMAIL_VALID_EMAIL',"Entrez valide Email.");
define('PASSWORD_IS_REQUIRED',"Le mot de passe est demandé.");
define('ENTER_ATLEAST_EIGHT_CHARACTER',"Entrez atleast 8 caractères.");
define('ENTER_ATLEAST_FIVE_CHARACTER',"Entrez atleast 5 caractère.");
define('YOU_CAN_NOT_ENTER_MORE_THAN_TWE_CHARACTER',"Vous ne pouvez pas entrer plus de 20 caractères.");
define('FIRST_NAME_LAST_NAME_ARE_REQUIRED',"Prénom et nom sont nécessaires.");
define('USERNAME_CANNOT_BE_MORE_THAN_CHAR',"Nom d'utilisateur ne peut pas être plus de 39 caractères.");
define('ENTER_FIRSTNAME_LASTNAME_ONLY',"Entrez le prénom et nom de famille seulement.");
define('ENTER_FIRSTNAME_LASTNAME',"Entrez le prénom et le nom.");
define('RETYPE_EMAIL_IS_REQUIRED',"Confirmer votre Email est requis.");
define('EMAIL_DOES_NOT_MATCH',"Email ne correspond pas.");
define('RETYPE_PASSWORD_IS_REQUIRED',"Confirmer mot de passe est nécessaire.");
define('PASSWORD_DOES_NOT_MATCH',"Le mot de passe ne correspond pas.");
define('INVITE_CODE_IS_REQUIRED',"Inviter code est nécessaire.");
define('PLEASE_ACCEPT_TERMS_OF_USE',"S'il vous plaît Accepter Conditions d'Utilisation.");
define('THE_EMAIL_ALREADY_EXISTS',"Ce courriel existe déjà");

//validation social signup steps


define('ENTER_FIRST_NAME',"Entrez le prénom.");
define('ENTER_FIRST_NAME_ONLY',"Entrez le prénom seulement.");
define('FIRST_NAME_CANNOT_BE_MORE_THAN_THIRTY_CHAR',"Prénom ne peut pas être plus de 39 caractères.");
define('FIRST_NAME_CANNOT_BE_LESS_THAN_THREE_CHAR',"Prénom ne peut pas être inférieure à 3 caractères.");
define('ENTER_LAST_NAME',"Entrez le nom.");
define('ENTER_LAST_NAME_ONLY',"Entrez le nom seulement.");
define('LAST_NAME_CANNOT_BE_MORE_THAN_THIRTY_CHAR',"Dernier nom ne peut pas être plus de 39 caractères.");
define('LAST_NAME_CANNOT_BE_LESS_THAN_THREE_CHAR',"Dernier nom ne peut pas être inférieure à 3 caractères.");
define('ZIPCODE_IS_REQUIRED',"Code postal est nécessaire.");
define('ENTER_VALID_ZIPCODE',"Entrez le code postal valide.");
define('ENTER_VALID_URL',"Entrez URL valide");

// 404 page
define('SORRY',"Désolé");
define('WE_CANT_FIND_THAT_PAGE',"Nous ne peux pas trouver cette page.");
define('RETURN_TO_HOME_PAGE',"Retour à la page d'accueil");


define('PLEASE_BE_SURE_TO_DOUBLE_CHECK_YOUR_SPELLING_TOMAKE_SURE_YOU',"S'il vous plaît assurez-vous de vérifier votre orthographe vous assurer que vous avez entré correctement l'adresse, ou vous pouvez simplement la tête de la page d'accueil en cliquant sur le bouton ci-dessous.");
// main dashboard
define('YOUR_PROJECTS',"Votre ". $project." Est");
define('PROJECT_FOLLOWERS',$project." Abonnements");
define('PROJECT_FOLLOWING', $project."  Suivant");
define('YOUR_TOTAL_PROJECTS',"Votre total ". $project);
define('TOTAL_PROJECTS',"Total ". $project." De");
define('YOUR_DONATION',"Votre don");
define('YOUR_RUNNING_DONATION',"Courir processus de l'investisseur");
define('RUNNING',"Course");
define('DONATION_RECEIVED',"Les dons reçus");
define('GENERATE_CODE',"Générer le code");
define('THERE_ARE_NO_INVITATION_CODE_HISTORY',"Il n'y a pas d'histoire de code d'invitation.");
define('USER_FOLLOWERS_FOLLOWINGS',"Abonnés de l'utilisateur / Suiveurs");
define('NO_PROJECTS_UNDER_YOUR_BELT_YET',"Aucune ". $project." Est sous votre ceinture encore,");
define('BUT_YOU_HAVE_GREAT_IDEAS_ANDSO_DO_THE_PEOPLE_YOU_KNOW',"mais vous avez de bonnes idées et ainsi faites les personnes que vous connaissez. Vous pourriez commencer un ".$project." aujourd'hui ou faites équipe avec des amis pour faire vos idées prennent vie!");
// dashboard sidebar

define('MAIN_NAVIGATION',"Navigation principale");
define('TRANSACTION_HISTORY',"Historique des transactions");
define('SOCIAL_NETWORK_DETAILS',"Réseau social Détails");
define('MY_PROFILE',"Mon Profil");
define('INBOX',"Boîte de réception");
define('INVITE_FRIENDS',"Inviter Des Amis");
define('ACTIVITY',"Activité");
define('IMAGE_VERIFICATION_WRONG',"Mauvaise image de vérification");
define('ENTER_VERIFICATION_CODE',"Entrez le code de vérification");
define('PLEASE_ENTER_QUESTION_DETAIL',"S'il vous plaît entrer les détails d'interrogation");
define('PLEASE_ENTER_VALID_EMAIL_ADDRESS',"S'il vous plaît entrez l'adresse e-mail valide");

define('PLEASE_SELECT_QUESTION_TYPE',"S'il vous plaît sélectionner le type de question");

//transaction
define('SEARCH_BY_PROJECT',"Recherche par ". $project);

define('SEARCH_BY_EMAIL',"Recherche par Email");

define('SEARCH_BY_STATUS',"Recherche par statut");

define('GO',"Aller");

define('SR_NO',"Sr. NO");

define('CR_DR',"Cr / Dr");

define('SERVICE_FEES',"Frais de service");

define('PREAPPROVAL_KEY',"Clé pré-approbation");

define('EXPORT_FILE',"fichier d'exportation");

define('THERE_ARENO_TRANSACTION_HISTORY',"Il n'y a pas d'histoire de transaction.");


define('MOST_FUNDED_PROJECT',"La plupart Financé ". $project);
define('WEBSITE_URL',"URL de site web");
define('BANDCAMP_URL',"Bandcamp url");
define('ENTER_VALID_FACEBOOK_URL',"Entrez facebook URL valide");
define('ENTER_VALID_TWITTER_URL',"Entrez twitter URL valide");
define('ENTER_VALID_LINKEDIN_URL',"Entrez linkedin URL valide");
define('ENTER_VALID_YOUTUBE_URL',"Entrez url youtube valide");
define('ENTER_VALID_BASECAMP_URL',"Entrez basecamp URL valide");
define('ENTER_VALID_MYSPACE_URL',"Entrez myspace URL valide");
define('ENTER_VALID_GOOGLEPLUS_URL',"Entrez google + URL valide");

define('COMMENT_POSTED_SUCCESSFULLY',"Commentaire posté avec succès");
define('THE_FILETYPE_YOU_ARE_TRYING_TO_YPLOAD_ISNOT_ALLOWED',"Le type de fichier que vous essayez de télécharger est pas autorisé.");
define('SOCIAL_DETAIL_UPDATED_SUCCESSFULLY',"Détails sociaux mis à jour avec succès");
define('SEARCH_BY_TITLE',"Recherche par titre");
define('CONNECT_WITH_FACEBOOK',"Connecter avec Facebook");
define('CONNECT_WITH_TWIITER',"Connectez-vous avec Twitter");
define('CONNECT_WITH_LINKEDIN',"Connectez-vous avec Linkedin");
define('WITH_FACEBOOK',"avec Facebook");
define('WITH_TWIITER',"avec Twitter");
define('WITH_LINKEDIN',"avec Linkedin");
define('WE_COULDN_FIND_ANY_OF_YOUR_GMAIL_CONTACT_BECAUSE_YOU_HAVE_CONNECTED_GMAIL',"Nous ne pouvais pas trouver un de vos contacts Gmail parce que vous connaître minutieusement connecté Gmail. Cliquez sur le bouton pour connecter temporairement Gmail");

define('WE_WILL_NEVER_SEND_OUT_ANY_EMAIL_MESSAGE_WITHOUT_ASKING',"Nous ne serons jamais envoyer des messages électroniques sans demander");
define('FIND_FIND_FROM_GMAIL',"Trouver trouver à partir de Gmail");

define('HOURS',"heures");
define('MINUTES',"minutes");
define('MONTHS',"mois");
define('YEARS',"années");
define('BROWSE_IMAGE',"Navigation Image");

define('WE_COULDNOT_FIND_ANY_OF_YOUR_FRIEND_FROM_FACEBOOK_BECAUSE_YOU_HAVE_CONNECTED_FACEBOOK',"Nous ne pouvions pas trouver un de vos amis de Facebook parce que vous ne l'avez pas connecté Facebook. Cliquez sur le bouton ci-dessous pour vous connecter");

define('INVITE_YOUR_FRIENDS',"Invite tes amis");
define('INVITE',"INVITE");
define('FEATURED_PROJECTS',"Vedette ". $project." De");
define('ROUNDING_OFF_PROJECTS',"L'arrondissement ".$project." De");
define('POPULAR_PROJECTS',"Populaire ".$project." De");
define('SUCCESS_STORY_PROJECTS',"Success Story ".$project." De");
define('WHAT',"Qu'est-ce");
define('START_NOW',"Commence Maintenant");
define('PROJECT_LIMIT',"Vous ne pouvez pas créer plus de% s ". $project." par an");
define('NO_PROJECTS_AVAILABLE',"Aucune ". $project." Est disponible");

define('HOW_IT_WORKS',"COMMENT ÇA MARCHE");

define('OUR_MODEL_TECHNOLOGY',"Notre modèle, la technologie, et de l'équipe d'experts à l'interne nous rendre la plateforme plus grand dans l'industrie de crowdfunding. <br />
  Nous tirons profit de tous les outils en notre pouvoir pour amplifier les ".$project.", afin que vous puissiez sensibiliser maximale");
define('CREATE_COMPAIGN',"Créer ".$project);
define('LAUNCH_COMPAIGN',"Lancement  ".$project);
define('PROMOTE_TO_NETWORK',"Promouvoir vers le réseau");
define('COLLECT_YOUR_FUNDS',"Recueillir vos fonds");
define('START_A_CAMPAIGN',"Lancer un ". $project);
define('SO_YOU_HAVE_GOT_AN_IDEA',"Donc, vous avez une idée? Projet de votre attrayant ".$project." Et soumettez.");
define('ALLOW_US_SOME_TIME',"Laissez-nous le temps d'examiner / modérer votre ". $project." ou laissez-nous apporter des commentaires.");
define('TELL_YOUR_CROWD_ABOUT',"Dites à votre foule à propos de ". $project." et de partager via des canaux de réseautage social.");
define('ENCOURAGE_YOUR_CROWD_WITH',"Encourage your crowd with rewards, hit your target & collect your funds.");
define('VIEW_PROJECT',"View ". $project);
define('COMPLETE_SUCCESS',"Complets (succèss)");
define('ALL_PROJECT',"All ". $project." Est");
define('DRAFT_PROJECT',"Projet ".$project." De");
define('PENDING_PROJECT',"En attendant ". $project." De");
define('ACTIVE_PROJECT',"Active". $project." De");
define('SUCCESSFUL_PROJECT',"Succès ". $project." De");
define('CONSOLE_PROJECT',"Console ". $project." De");
define('FAILURE_PROJECT',"Défaut". $project."De");
define('DECLINE_PROJECT',"Déclin ". $project." De");
define('CAMPAIGN_DASHBOARD', $project." Tableau de bord");
define('EXPLORE',"Explorer");
define('WE_HELP_MAKE_ANY_IDEA_A_REALITY_THROUGH_CROWDFUNDING',"Nous aidons faire une idée une réalité grâce crowdfunding. <br /> Découvrez et soutenir ces activités de financement ". $project." aujourd'hui!");
define('YOUR_CAMPAIGN',"Votre ". $project);
define('HAS_BEEN_SUBMITTED_SUCCESSFULLY',"a été soumis avec succès.");
define('WHAT_NEXT',"Que faire ensuite?");
define('TEAM_WILL_REVIEW_DETAIL_OF_YOUR_CAMPAIGN_AND_YOU_WILL_BE_NOTIFIED',"équipe examinera les détails de votre ". $project." et vous serez averti par e-mail concernant votre". $project." état.");
define('GO_TO_DASHBOARD',"Aller au tableau de bord");
define('THE_BROWSER_YOU_ARE_USING_INTERNET',"The browser you are using, Internet Explorer 6, is no longer supported by this site. This means that some features will not work for you.");
define('SO_PLEASE_TAKE_A_MOMENT_TO_CLICK_ONE_OF_THE_LINK_BELOW_TO_UPGRADE',"Donc, s'il vous plaît prendre un moment pour cliquer sur un des liens ci-dessous pour passer à un navigateur plus récent!");
define('GOOGLE_CHROME',"Google Chrome");
define('MOZILLA_FIREFOX',"Mozilla Firefox");
define('APPLE_SAFARI',"Apple Safari");

define('COMPLETED_DONATIONS',"investissements Terminé");
define('PENDING_DONATIONS',"Dans l'attente de investissements");
define('RECEIVED',"Reçu");
define('CANCELLED',"Annulé");
define('PIPELINE',"Pipeline");

define('SITE_NAME',"Nom du site");
define('LANGUAGE',"Langue");
define('CHANGE_LANGUAGE','Changer de langue');
define('EMAIL_OR_PASSWORD_IS_WRONG_PLEASE_TRY_AGAIN',"Email ou mot de passe est faux, s`il vous plaît essayer à nouveau...!!!");

define('YOU_CAN_NOT_DELETE_THIS_PROJECT',"Vous ne pouvez pas supprimer ce projet.");
define('MORE_FILTERS',"plus de filtres");
define('THIS_EMAIL_IS_ADDRESS',"Cette adresse email est");
define('ALREADY_EXIST_IN_OUR_SYSTEM_NDO_YOU_WANT_TO_MERGE_YOUR_TWITTER_ACCOUNT_WITH_THIS'," Il existe déjà dans notre système, \ nVoulez-vous de fusionner votre compte Twitter avec ce ");
define('ACCOUNT_',"compte ");
define('YOUR_ACCOUND_HAS_BEEN_MERGE_SUCCESSFULLY',"Votre compte a été fusionné avec succès.");
define('IT_MIGHT_BE_SOME_PROBLEM_FOR_MERGE_YOUR_ACCOUNT_PLEASE_TRY_AGAIN_LATER',"Il pourrait être un problème pour fusionner votre compte, S`il vous plaît réessayer plus tard.");
define('MY_COMMENT',"Mon commentaire");
define('HIDDEN',"caché");
define('INACTIVE_HIDDEN',"Inactif - Caché");
define('INACTIVE_VISIBLE',"Inactif - Visible");
define('OK',"Ok");
define("HOUR_LEFT", "heure gauche");
define("NO_TIME_LEFT", "Pas de temps à gauche");
define('TEXT_YEAR',"an");
define('TEXT_MONTH',"mois");
define('TEXT_DAY',"jour");
define('TEXT_HOUR',"heure");
define('TEXT_MINUTE',"minute");


define('TEXT_YEARS',"années");
define('TEXT_MONTHS',"mois");
define('TEXT_DAYS',"journées");
define('TEXT_HOURS',"heures");
define('FUNDED',"funded");
define('ALREADY_EXIST_IN_OUR_SYSTEM_NDO_YOU_WANT_TO_MERGE_YOUR_SOCIAL_ACCOUNT_WITH_THIS',"Il existe déjà dans notre système, \nVoulez-vous de fusionner votre compte sociale avec ce");

define('COMPANY_URL',"URL de la société");
define('UPLOAD_YOUR_COVER_PHOTO',"Téléchargez votre photo de couverture");
define('FEATURED_PROJECT',"développement vedette");
define('PROJECT_SUMMARY',"Résumé de développement");
define('ADD_FEATURED_PROJECT',"+ Ajouter développement en vedette");
define('RESEND_VARIFICATION_LINK', 'Renvoyer le lien de vérification.');
define('SEND_VARIFICATION', 'Envoyer la vérification');
define('RESEND', 'Renvoyez');


/*account_lang.php*/

define('MEMBER_SINCE',"membre Depuis");
define('JOINED',"relié");
define('LAST_LOGIN',"dernière Connexion");
define('MIN_AGO',"Il ya min");
define('HR_TEXT',"heure");
define('MIN_TEXT',"min");
define('SEC_AGO',"s il ya");
define('SEC',"seconde");
define('DAYS',"jours");
define('DAY_LEFT',"jour gauche");
define('TEXT_AGO',"depuis");
define('MY_ACCOUNT',"Mon Compte");
define('EDIT_PROFILE_SETTINGS',"Modifier le profil et paramètres");
define('EDIT_PROFILE',"Modifier le profil");
define('EMAIL_NOTIFICATION_SETTING',"Email Paramètres notification");
define('USER_ALERT',"Alerte de l'utilisateur");
define('ADD_FUND',"Ajouter Fonds");
define('PROJECT_ALERT',"Project Alert");
define('COMMENT_ALERT',"commentaire alerte");
define('PROJECT', $project);
define('PAYEE_EMAIL',"bénéficiaire Email");
define('MY_DONATION',"Mon investissement");
define('NOTIFICATION',"notification");
define('MY_COMMENTS',"Mes Commentaires");
define('NO_COMMENT_ADDED',"Il n'y a pas '". $comment."ajoutée");
define('CHANGE_PASSWORD',"Changer Le Mot De Passe");
define('OLD_PASSWORD',"Ancien Mot De Passe");
define('NEW_PASSWORD',"Nouveau Mot De Passe");
define('CON_PASSWORD',"Confirmez Le Mot De Passe");
define('INCOMING_FUND',"Donation entrant");
define('END_DATE',"date De Fin");
define('THERE_ARE_NO_INCOMING_DONATION',"Il n'y a pas de investissements entrants.");
define('THERE_ARE_NO_DONATION',"Il n'y a pas de investissement.");
define('SAVE_CHANGES',"Sauvegarder Les Changements");
// account page validation
define('THIS_FIELD_CANNOT_BE_LEFT_BLANK',"Ce champ ne peut pas être laissée en blanc.");
define('ENTER_ZIP_CODE_NOT_LESS_THAN_FIVE_CHAR',"Entrez le code de pas moins de 5 caractère postal.");
define('ENTER_ZIP_CODE_NOT_MORE_THAN_SEVEN_CHAR',"Code postal Code non plus de 7 caractères.");
define('OLD_PASSWORD_IS_REQUIRED',"Ancien mot de passe est nécessaire.");
define('CONFIRM_PASSWORD_IS_REQUIRED',"Confirmer mot de passe est nécessaire.");
define('PAYPAL_REQUEST_AND_RESPONSE',"Paypal demande et la réponse");
//define('ARE_YOU_SURE_YOU_WANT_TO_DELETE_SELECTED_RECORD',"Êtes-vous ypu? Vous voulez supprimer l'enregistrement sélectionné (s).");
define('DONER',"Doner");
define('PAYPAL_REQUEST',"Paypal Demande");
define('ENTER_PROFILE_SLUG',"Entrez profil Slug");
define('SPACE_ID_NOT_REQUIRED',"L`espace est pas nécessaire");
define('PROFILE_SLUG_ALREADY_EXIST',"Profil slug est déjà exister . S`il vous plaît essayer une autre");
define('PROFILE_SLUG',"Profil Slug");
// notification text
define('COMPAIGNS_YOU_BACK',"Vous Campagnes de retour");
define('NEW_PLEDGES',"Les nouvelles promesses");
define('NEW_COMMENTS',"nouveaux commentaires");
define('NEW_FOLLOW',"nouvelle suivi");
define('NEW_UPDATES',"nouvelles mises à jour");
define('COMPAIGNS_YOU_FOLLOW',"Campagnes vous suivez");
define('SOCIAL_NOTIFICATIONS',"notifications sociales");
define('NEW_FOLLOWERS',"Nouveaux adeptes");
define('COMMENT_REPLY',"commentaire réponse");
define('COMPAIGNS_YOU_CREATED',"Campagnes vous avez créés");
define('THIS_SLUG_IS_ALREADY_EXIST_PLEASE_ENTER_NEW_SLUG',"Cette limace est déjà exister. S`il vous plaît entrer un nouveau slug");
define('COMMENT_DELETED_SUCCESSFULLY',"Commentaire supprimé avec succès,");
define('AMOUNT_MUST_BE_GREATER_THAN_ZERO',"Le montant doit être supérieur à zéro");
define('AMOUNT_FIELD_CANNOT_BE_NEGATIVE_AND_STRING',"Champ Montant peut pas être négatif et de la ficelle.");
define('SOCIAL_DETAILS',"détails sociaux");
//=RM 96
define('TOTAL_DONATION',"Total Investissement");
define('ACTIVE_DONATION',"Actif Investissement");
define('SUCCESSFUL_DONATION',"Réussi Investissement");
define('UNSUCCESSFUL_DONATION',"Infructueux Investissement");
define('THERE_ARENO_INVESTOR_PROCESS',"Il ne courent processus d'investisseur.");
define('OCCUPATION_CANNOT_MORE_THAN_39_CHARS', 'Profession ne peut être plus de 39 caractères.');
define('INVALID_GOOGLE_PLUS_LINK', 'Google valide Link Plus');
define('MUST_CONTAIL_VALID_URL', 'la %s champ doit contenir une URL valide.');
define('BY', 'par');
define('CLOSE', 'Près');
define('UPDATE_DELETE_CONFIRMATION', 'Etes-vous sûr, vous voulez supprimer cette mise à jour?');

/*inbox_lang.php*/
//==============inbox_details============
define('SENT_ON',"envoyé sur");
define('THERE_ARE_NO_MESSAGE_IN_INBOX',"Il n'y a pas de message dans la boîte de réception."); 
define('PLEASE_ADD_CONVERSATION_TEXT',"S'il vous plaît ajouter du texte de la conversation"); 
define('PLEASE_ENTER_TEXT',"S'il vous plaît Entrez texte"); 
//===================index.php===============
define('CONVERSATION_WITH',"Conversation avec"); 
define('BACK_TO_INBOX',"retour à Indox");
define('REPLY',"répondre");
define('TEAM','Équipe');

/*profile_lang.php*/
define('FOLLOWING',"suivant");

define('FOLLOW',"suivre");

define('UNFOLLOW',"Se désabonner");

define('LOCATION',"emplacement");

define('CONTACT_ME',"Contactez moi");

define('ALSO_FIND_ME_ON',"Retrouvez également sur moi");

define('MY_SPACE',"Mon Espace");
define('CREATED_PROJECTS',"créé ".$project."");

define('CONTRIBUTE_NOW',"contribuer maintenant");
define('NO_PROJECTS_FOUND',"non ".$project."s trouvé");

define('CONTRIBUTIONS',"contributions");

define('NO_CONTRIBUTIONS',"Aucun Contributions");

define('SEE_MORE',"voir plus");

define('COMMENTS',"commentaire");

define('AT',"à");

define('ON',"sur");

define('COMMENTED_ON',"commenté sur");

define('NO_COMMENTS_FOUND',"Aucune ". $comment." trouvé");

define('MY_FOLLOWERS',"Mes suiveurs");

define('NO_FOLLOWERS_FOUND',"Aucune ". $follower." A trouvé");

define('MY_FOLLOWINGS',"Mes Suiveurs");

define('NO_FOLLOWINGS_FOUND',"Aucun suivants trouvés");

define('ABOUT_ME',"A propos de moi");

define('NA',"N / A");

define('SKILLS',"compétences");

define('INTEREST',"intérêt");

define('CONTRIBUTE_AMOUNT',"contribuer Montant");

define('TOTAL_RAISED_FOR_OWN_PROJECTS',"Total recueilli pour son propre ".$project);

define('TOTAL_DONATED_TO_OTHER_PROJECTS',"Total des dons à d'autres ".$project);


define('PROFILE',"profil");

define('CONTRIBUTED',"Proposé");

define('SHOW_MORE_CONTRIBUTIONS',"Montrer plus de contributions");

define('THIS_USER_HAS_NOT_ENTERED',"Cet utilisateur n`a pas encore entré de détails sur eux-mêmes .");

define('THIS_USER_HAS_NOT_ADDED_ANY_SKILLS_YET',"Cet utilisateur n`a pas encore ajouté de compétences .");

define('THIS_USER_HAS_NOT_ADDED_ANY_INTEREST_YET',"Cet utilisateur n`a pas encore entré d` intérêt.");

define('RECIPIENT_MAIL_INVALID','Email du destinataire est invalide');

define('INVITATION_HAS_BEEN_SENT_TO','Invitation a été envoyée à ');

/*invite_lang.php*/

define("GMAIL", "Gmail");
define("MULTIPLE_EMAIL_ADDRESS_SHOULD_BE_SEPERATED_WITH_COMMA", "De multiples adresses email doivent être séparées par des virgules");
define('YOU_ALREADY_SENT_INVITATION_TO_THIS_EMAIL',"Vous avez déjà envoyé Invitation à cet e-mail.");
define('YOU_CAN_NOT_INVITE_YOUR_SELF',"Vous ne pouvez pas vous inviter.");
define('RECIPIENT_MESSAGE_IS_REQUIRED',"Un message du bénéficiaire est requise.");
define('INVITATION_IS_FAILED',"Invitation a échoué.");
define('SHOW_SELECTED_FRIENDS',"Afficher les options sélectionnées amis");
define('WE_COULDN_T_FIND_ANY_OF_YOUR_FRIENDS_FROM_FACEBOOK_BECAUSE_YOU_HAVEN_T_CONNECTED_FACEBOOK_AND_REKAUDO_CLICK_THE_BUTTON_BELOW_TO_CONNECT',"Nous ne pouvais pas trouver un de vos amis de Facebook parce que vous connaître minutieusement connecté Facebook . Cliquez sur le bouton ci-dessous pour vous connecter.");
define('INVITE_YOUR_FRIENDS_TO',"Invitez vos amis à ");
define('WE_COULDNT_FIND_ANY_OF_YOUR_GMAIL_CONTACTS_BECAUSE_YOU_HAVENT_CONNECTED_GMAIL_AND_REKAUDO_CLICK_THE_BUTTON_TO_TEMPORARILY_CONNECT_GMAIL_AND_REKAUDO',"Nous ne pouvais pas trouver un de vos contacts Gmail parce que vous connaître minutieusement connecté Gmail . Cliquez sur le bouton pour connecter temporairement Gmail");
define('WE_WILL_NEVER_SEND_OUT_ANY_EMAIL_MESSAGES_WITHOUT_ASKING',"Nous ne serons jamais envoyer des messages électroniques sans demander.");
define('FRIENDS_ON',"amis sur");
define('YAHOO_MAIL',"yahoo Mail");
define('INVITE_SENT',"Inviter envoyé");
define('SELECT_ALL',"Tout Sélectionner");
define('DESELECT_ALL',"Deselect All");
define('INVITE_ALL',"Tout Déselectionner");
define('WE_COULDNT_FIND_ANY_OF_YOUR_YAHOO_CONTACTS_BECAUSE_YOU_HAVENT_CONNECTED_YAHOO_AND_REKAUDO_CLICK_THE_BUTTON_TO_TEMPORARILY_CONNECT_YAHOO',"Nous ne pouvais pas trouver un de vos contacts Yahoo parce que vous connaître minutieusement connecté Yahoo. Cliquez sur le bouton pour connecter temporairement Yahoo ");
define('YOUR_AFFILIATE_REQUEST_IS_IN_PENDING',"Votre demande d`affiliation est en attente.");
define('SORRY_AN_ERROR_OCCURES_PLEASE_TRY_AGAIN',"Désolé, une erreur a lieu s`il vous plaît essayer à nouveau..!");
define('YOUR_INVITATION_SENT_SUCCESSFULLY',"Votre Invitation envoyée avec succès");
define('SEND_ALL',"Envoyer Toutes");
define('FACEBOOK_WILL_FETCH',"Facebook va chercher seulement ces amis qui utilisent l`application (%s)");

/*content_lang.php*/

define('HAVE_DIFFERENT_QUESTION', "Vous avez une question différente?");
define('YOUR_NAME', "Votre Nom");
define('CONTACT_US_EMAIL', "Ton E-Mail");
define('CONTACT_US_NAME', "Votre Nom");
define('CONTACT_US_MESSAGE', "Détails");
define('PLEASE_ENTER_NAME', "S'il vous plaît entrez votre Nom");
define('PLEASE_ENTER_VALID_EMAIL', "S'il vous plaît entrer valide Email Id");
define('PLEASE_ENTER_MESSAGE', "S'il vous plaît entrer les détails d'interrogation");
define('PLEASE_ENTER_QUESTIONS', "S'il vous plaît entrez votre question");
define('PLEASE_ENTER_QUESTIONS_ABOUT', "S'il vous plaît entrez votre type de question");
define('CONTACT_US_QUESTIONS', "Votre Question");
define('CONTACT_US_LINK', "Lien vers votre projet ou votre profil");
define('CONTACT_US_ABOUT', "Votre question est à propos");
define('SELECT_TYPE', "Sélectionner le type");
define('FUNDRAISING_ACCOUNT_LOGIN', "Compte de collecte de fonds / Connexion");
define('FUNDRAISING_PROJECT', "Collecte de fonds ". $project."  Idée");
define('INTERNATIONAL', "International");
define('OTHER', "Autre");
define('QUESTION', "Question:");
define('DETAILS', "Détails");
define('LINKS', "Liens:");
define('YOUR_QUESTION', "Votre Question");
define('THE_GUIDE_TO_A_SUCCESSFUL_CAMPAIGN', "Le Guide d'une campagne réussie");
define('STILL_WANT_TO_LEARN_MORE', "Encore voulez en savoir plus?");
define('GET_GOING', "Get Going!");
define('FULLY_FUNDED', "Entièrement financé");
define('SUCCESS_STORYS', "Une Histoire À Succès");
define('HELP_CENTER', "Centre D'Aide");
define('KNOWLEDGE_BASE', "Base de connaissances");
define('ARTICLES', "Articles");
define('RESPONSE_WITHIN_24_HOURS', "Réponse dans les 24 heures");
define('SUBMIT_A_REQUEST', "Soumettre une requête");
define('WE_ARE_HERE_TO_HELP_TO_GET_START_PLEASE_SELECT_THE_TYPE_OF_ISSUE', "Nous sommes ici pour aider! Pour commencer, s'il vous plaît sélectionnez le type de question que vous souhaitez nous contacter à propos");
define('PLEASE_USE_A_FEW_WORD_TO_SUMMARIZE_YOU_QUESION', "S'il vous plaît utiliser quelques mots pour résumer votre question.");
define('PLEASE_ENTER_ADDITIONAL_DETAIL_OF_YOUR_REQUEST', "S'il vous plaît entrer des détails supplémentaires sur votre demande. Pour nous aider à vous fournir une réponse aussi rapidement que possible, s'il vous plaît envisager d'inclure autant de détails spécifiques que possible. Par exemple, si votre question est sur une situation particulière ".$project." sur");
define('PLEASE_INCLUDE_THE_NAME_OF_THE_CAMPAING_AND_A_LINK', "s'il vous plaît inclure le nom de l'".$project." et un lien.");
define('FIRST_AND_LAST_NAME', "Prénom et nom");
define('YOUR_EMAIL_ADDRESS', "Votre adresse e-mail");
define('NO_LEARN_MORE_CATEGORY_AVAILABLE', "Aucune catégorie en savoir plus disponible");
define('RELATED_ARTICLES', "Articles en relation");
define("SECURITY_CHECK", "Contrôle de sécurité");
define("JOIN_NOW", "Joindre Maintenant"); 

/*equity_lang.php*/
define('THIS_COMPANY_MAY_BE_INTERESTED',"Cette ".$project." est ouverte à la fois accrédité et non-accrédité ".$investor."s.");
define('THIS_COMPANY_ONLY_INTERESTED',"Cette ".$project." est ouverte pour les ".$investor."s accrédités seulement.");
define('RAISE_DETAILS',"Levez Détails");
define('FUNDING_GOAL',"Objectif de financement");
define('CURRENT_RESERVATIONS',"Réservations en cours");
define('MINIMUM_RESERVATIONS',"Réservation minimum");
define('INTEREST_PER_YEAR',"Intérêt (% par ouir)");
define('TERM_LENGTH_MONTHS',"Longueur terme (Moiss)");

define('RAISED_OF',"Arboré de");
define('COMPLETE_SUCCESSB',"Complets (succèss)");
define('OPEN_DATE',"Date d'ouverture");
define('VIEW_ALL_FOLLOWERS',"Voir tous les Abonnés");
define('COPY_THE_CODE_BELOW_AND_PASTE',"Copiez le code ci-dessous et collez-le dans votre site web ou votre blog.");
define('EMBED_THIS_CARD_IN_YOUR_WEBSITE',"Intégrer cette carte dans votre site ou blog");
define('CAMPAIGN_SHORT_LINK',"Campagne Lien court");
define('CONNECT_WITH_COMPANY',"Connectez-vous avec Société");
define('HIGHLIGHTS',"Points Forts");
define('TOP_INVESTORS',"Top investisseurs");

//Demande D'Accès
define('MISSING_MINIMUM_PROFILE_ITEMS',"Manquant Profil Minimum Articles");
define('BEFORE_YOU_CAN_REQUEST_ACCESS_OR_MAKE_COMMITMENT',"Avant que vous pouvez demander l'accès ou faire engagement à un accord mât vous ont le profil suivant l'article terminé:");
define('USER_PROFILE_IMAGE',"Profil image");
define('CURRENT_LOCATION',"Localisation Actuelle");
define('PLEASE_CLICK_ON_THE_COMPLETE_PROFILE_BUTTON',"Please click on the Complet Profile button to complete the required profile items. After you successfully complete your profile, you will be redirected back to this page and request will be processed automatically.");
define('REQUEST_ACCESS',"Demande D'Accès");
define('YOU_MUST_BE_AN_ACCREDITED_INVESTOR_AND_REQUEST',"Cette section est visible seulement à ceux qui se voient accorder l'accès total ou partiel à la salle d'affaire.");
define('YOUR_REQUEST_TO_ACCESS_UPDATES_SECTION_HAS_BEEN_SUBMITTED',"Votre demande pour accéder à la section des mises à jour a été soumis avec succès.");

define('YOUR_REQUEST_TO_ACCESS_COMMENTS_SECTION_HAS_BEEN_SUBMITTED',"Votre demande d'accès ".$comments." section a été soumis avec succès.");

define('YOUR_REQUEST_TO_ACCESS_FUNDERS_SECTION_HAS_BEEN_SUBMITTED',"Votre demande à la section des bailleurs de fonds a accès été soumis avec succès.");
define('YOUR_REQUEST_TO_ACCESS_DOCS_MEDIA_SECTION_HAS_BEEN_SUBMITTED',"Votre demande à la section accès docs et des médias a été soumis avec succès.");
define('PLEASE_WAIT_UNTILL_YOUR_ACCESS_REQUEST_IS_APPROVED',"S'il vous plaît attendre jusqu'à ce que votre demande d'accès est approuvé.");
define('REQUEST_PENDING',"Demande En Attente");
define('ACCESS_REQUEST',"Demande d'accès");
define('PERMISSION',"Autorisation");
define('ACCREDIATION_STATUS',"Statut d'accréditation");
define('SECTION',"Section");

define('NOT_ACCREDITED',"Non Accredited");
define('SET_PERMISSIONS',"Définir les autorisations");
define('DOCS_MEDIA',"Docs & Media");
define('I_AM_INTRESTED',"Je suis Intrested");
define('INVEST_NOW',"Investir maintenant");
define('INVESTMENT_IN_PROCESS',"Investissement In Process");
define('VERIFY_INVESTOR_STATUS',"Vérifier le statut d'investisseur");
define('YOU_REQUEST_IS_STILL_PENDING',"Vous demande est toujours en attente");
define('YOU_REQUEST_IS_REJECTED_BY_OWNER',"Vous demande est rejetée par le propriétaire");
define('NO_PERMISSION',"Aucune Autorisation");
define('ACCEPT',"Accepter");
define('DENY',"Nier");
define('ARE_YOU_SURE_YOU_WANT_TO_ALLOW',"Etes-vous sûr que vous voulez autoriser% s à investir sur votre ".$project." ?");

define('POST_A_COMMENT',"Poster un commentaire");
define('POST_COMMENT',"Poster Un Commentaireaire");
define('DOCUMENTS',"Documents");
define('THIS_DOCUMENTS_IS_ONLY_VIEWABLE_TO_THOSE',"Ce document est visible seulement à ceux qui se voient accorder un accès complet");
define('MAKE_YOUR_PROJECT_LIVE_TO_SHARE_ON_SOCIAL',"Faites de ".$project." vivre pour partager sur les sites de médias sociaux.");

//=============project ==================///// 
define('DECLINE',"Déclin");
define('EDIT_PROJECT',"Modifier ".$project);
define('DELETE_PROJECT',"Supprimer ". $project);
define('PROJECT_CLOSED',"Fermé");

define('SHARE',"Partager"); 
define('OVERVIEW',"Vue d'ensemble");
define('DONATIONS',"Investissement");
define('RAISED_TOWARDS',"Levé vers");
define('UPDATES',"Mises à jour");
define('WIDGETS',"Widgets");
define('ENDED',"Limitée sur");
define('VIEW_PROJECT_PAGE',"Voir la page de ".$project);
define('ACTION',"Action");
define('FOLLOWERS',"Abonnements");
define('VIDEO_GALLERY',"Galerie Vidéo");
define('ADD_VIDEO',"+ Ajouter vidéo");
define('IMAGE_GALLERY',"Galerie d'images");
define('ADD_IMAGE',"+ Ajouter une image");
define('EDIT',"Éditer");
define('THANKS_FOR_JOINING_THE_TEAM',"Merci pour rejoindre l'équipe!");
define('CONGRATES_YOU_ARE_NOW_PART_OF_THE',"Félicitations! You`re maintenant partie de la");
define('TEAM_ON',"équipe");
define('IS_CAMPAIGNING_FOR',"fait campagne pour");
define('AND_HAS_ADDED_YOU_AS_A_MEMBER_ONTHE_TEAM',"and has added you as a member on the team. Now that you`re on board, help get the ball rolling and start letting people know about this great ".$project ."!");
define('EMBED',"Intégrer");
define('PERKS',"Perks");
define('THERE_IS_NO_UPDATES_FOR_THIS_PROJECT',"Il n'y a pas de mises à jour pour cette ". $project);
define('THERE_IS_NO_COMMENT_FOR_THIS_PROJECT',"Il n'y a pas ". $comments." pour cette ". $project);
define('THERE_IS_NO_FUNDERS_FOR_THIS_PROJECT',"Il n'y a pas les bailleurs de fonds pour ce ". $project);
define('KEEP_PRIVATE',"Gardez privée");
define('COMMENT',"Commentaire");
define('SHOW_COMMENT_AFTER_APPROUVÉ',"Commentaire ajouté avec succès. On verra après propriétaire de la société approbation.");
define('PROJECT_GOAL',"Objectif du projet");
define('FUNDERS',"Les bailleurs de fonds");
define('DATE',"Date");
define('PROJECT_STATUS',"État du projet");
define('TOTAL_RECEIVE_PAYMENT',"Total Paiement reçu");
define('REMAIN_DAYS',"Jours restants");
define('DONOR',"Donateur");
define('AMOUNT_DOLOR',"Quantité");
define('AMOUNT_FEES',"Honoraires");
define('NO_DONATION',"Aucun investissements");
define('PERK',"Perk");
define('NEXT',"Suivant");
define('DELETE',"Effacer");
define('ADD_NEW',"Ajouter un nouveau");
define('SHARE_YOUR_PROJECT',"Partager votre $project");

define('SHARE_PROJECT_ON_TWITTER',"Partager $project sur Twitter");
define('SHARE_ON_FURL',"Partager $project sur Furl");
define('FURL',"ferler");
define('SHARE_ON_STUMBLEUPON',"Partager $project sur StumbleUpon");
define('STUMBLEUPON',"StumbleUpon");
define('DELICIOUS',"délicieux");
define('SHARE_ON_DELICIOUS',"Partager $project sur Delicious");
define('SHARE_ON_DIGG',"Partager $project sur Digg");
define('SHARE_ON_TUMBLR',"Partager $project sur Tumblr");
define('THUBMLR'," Tumblr");
define('SHARE_ON_REDDIT',"Partager $project sur Reddit");
define('REDDIT',"reddit");
define('SHARE_ON_MIXX',"Partager $project sur Mixx");
define('MIXX',"Mixx");
define('SHARE_ON_TECHNORATI',"Partager $project sur Technorati");
define('TECHNORATI',"Technorati");
define('SHARE_ON_YAHOO_BUZZ',"Partager $project sur Yahoo! Buzz");
define('YAHOO_BUZZ',"Yahoo! Buzz");
define('DESIGNFLOAT',"Partager $project sur DesignFloat");
define('SHARE_ON_BLINKLIST',"Partager $project sur BlinkList");
define('BLINKLIST',"BlinkList");
define('SHARE_ON_MYSPACE',"Partager $project sur Myspace");
define('MYSPACE',"Mon Espace");
define('SHARE_WITH_FARK',"Partager $project sur Fark");
define('FARK',"Fark");
define('SUBSCRIBE_TO_RSS',"Abonnez-vous au flux RSS");
define('RSS',"RSS");
define('SHARE_ON_GOOGLE_PLUS',"Partager $project sur Google+");
define('SAVE_THIS_REASON_TO_USE_AGAIN',"Enregistrer cette raison d'utiliser à nouveau");
define('ENTER_REASON_WHY_YOU_WANT_DENY_USER'," Entrez raison pour laquelle vous souhaitez refuser la demande de l'utilisateur");


define('THE_PAGE_YOU_ARE_LOOKING_FOR_IS_CURRENTLY',"La page que vous recherchez est actuellement en mode DRAFT, caché public ou un lien invalide.
S'il vous plaît contacter le propriétaire de la campagne pour plus d'informations si vous pensez que cela est une erreur.");

define('THE_PAGE_YOU_ARE_LOOKING_FOR_IS_CURRENTLY_IN_DRAFT_MODE_OR_HIDDEN',"La page que vous recherchez est actuellement en mode «PROJET» ou caché public. S'il vous plaît contacter le propriétaire de la campagne si vous sentez que cela est une erreur !!!");
define('PROJECT_DEAL_TYPE',$project." Type d'aubaine");
define('CURRENT_ROUND',"Cycle actuel");
define('RECORD_DELETED_SUCCESSFULLY',"Notice supprimée avec succès.");
define('RECORD_ADDED_SUCCESSFULLY',"Enregistrez ajouté avec succès.");
define('RECORD_APPROUVÉ_SUCCESSFULLY',"Enregistrez approuvé succès.");
define('RECORD_DECLINED_SUCCESSFULLY',"Enregistrez décliné avec succès");
define('RECORD_REPLIED_SUCCESSFULLY',"Enregistrez répondu avec succès.");
define('RECORD_SPAM_SUCCESSFULLY',"Spam d'enregistrement avec succès.");
define('SHOW_MORE_FUNDERS',"Afficher plus les bailleurs de fonds");
define('COMPLETE',"Complet");
define('REQUEST',"Demande");
define('REQUESTED_FOR',"demandée pour");
define('POSTED_AN_ANNOUNCEMENT',"Publié une annonce");
define('SPAM',"Spam");
define('REPORT_SPAM',"Signaler Comme Spam");
define('APPROUVÉ_BTN',"APPROUVÉ");

define('FLEXIBLE_FUNDING_CAMPAIGN', $project." Financement flexible");
define('FIXED_FUNDING_CAMPAIGN',"Financement fixe ". $project);
define('FLEXIBLE_FUNDING',"Financement flexible");
define('FIXED_FUNDING',"Financement fixe");
define('THIS_CAMPAIGN_WILL_ENDED_AND_WILL_RECEIVE_ALL_FUNDS_RAISED_UPTO',"Cette ". $project." sera terminé et recevra tous les fonds recueillis jusqu'à");
define('THIS_CAMPAIGN_HAS_ENDED_AND_WILL_RECEIVE_ALL_FUNDS_RAISED_UPTO',"Cette ". $project." recevra tous les fonds recueillis, même si elle ne parvient pas à son objectif.");
define('THIS_CAMPAIGN_HAS_ENDED_ON',"Cette ". $project." a terminé sur");
define('THIS_CAMPAIGN_STARTED_ON',"Cette ".$project."  commencé à");
define('AND_WILL_CLOSE'," et se terminera le");
define('AND_CLOSED'," et fermé sur");
define('SAID'," Dit");
define('POST_REPLY',"Répondre");
define('NO_DOCUMENT_HAS_BEEN_ADDED_YET',"Aucun document n`a été ajouté.");
define('NO_IMAGE_HAS_BEEN_ADDED_YET',"Aucune image n`a encore été ajoutée.");
define('NO_VIDEO_HAS_BEEN_ADDED_YET',"Aucune vidéo n`a encore été ajoutée.");
define('NO_PERK_HAS_BEEN_PURCHASED_YET',"Aucun avantage n`a encore été acheté.");
define('SHOW_MORE_COMMENTS',"Afficher plus de commentaires");
define('ARE_YOU_SURE_YOU_WANT_TO_APPROVE_THIS_COMMENT',"Êtes-vous sûr, vous souhaitez approuver ce commentaire?");
define('ARE_YOU_SURE_YOU_WANT_TO_DECLINE_THIS_COMMENT',"Êtes-vous sûr, vous voulez diminuer ce commentaire?");
define('ARE_YOU_SURE_YOU_WANT_TO_SPAM_THIS_COMMENT',"Êtes-vous sûr, vous voulez spammer ce commentaire?");
define('ARE_YOU_SURE_YOU_WANT_TO_DELETE_THIS_COMMENT',"Êtes-vous sûr, vous voulez supprimer cette équité?");
define('MAKE_YOUR_PROJECT_LIVE_TO_VIEW_AND_COPY_WIDGET_CODE',"Faire vivre votre ". $project." pour voir et copier le code widget,.");
define('UPDATES_IS_REQURED',"Mises à jour est nécessaire");
define('DENY_ACCESS_REQUEST',"Refuser l`accès Demande");
define('DENY_INTEREST_REQUEST',"Refuser la demande d`intérêt");
define('THIS_PROJECT_IS_INACTIVE_AND_NO_LONGER',"cette ".$project." est inactif et ne plus accepter ".$funds." en raison de");
define('THIS_REASON',"cette raison,");
define('REASON_INACTIVE_THIS',"Raison inactif Cette ".$project." ne sont pas fournis par ".$site_name." équipe");
define('THIS_PROJECT_IS_HIDDEN_AND_NO_LONGER',"cette ".$project." est caché et d`accepter plus ".$funds." en raison de");

define('COMPAIGN_CLOSED',"".$project." Fermé");

//define('HIGHLIGHTS',"Highlights");
define('PROJECT_RETURN_CALCULATOR',"Calculatrice de rendement prévu");
define('PRINCIPAL',"Principal");
define('GROSS_ANUAL_RETURN',"Retour annuel brut");
define('TERM',"Terme");
define('TOTAL_RETURN',"Total Return");
define('PROPERTY_SUMMARY',"Sommaire de la propriété");
define('RETURN_OVERVIEW',"Retour Vue d'ensemble");
define('ANUAL_SERVICING_FEE',"Frais d'entretien annuel");
define('NET_TO_INVESTORS',"Net pour les investisseurs");
define('VIEW_SPONCER_PROFILE_PROFILE',"Voir le profil complet de commanditaire");
define('SHARE_ON_FACEBOOK', 'Partager '.$project.' sur Facebook');

/*start_eequity_lang,php*/

define("DRAFT_YOUR_CAMPAIGN","Votre Projet ".$project);
define("ENHANCE_YOUR_CAMPAIGN","Améliorer votre ".$project);
define("PREFUNDING_DETAILS", "Pré Details Bailleurs de fonds");
define("ENHANCE_AND_MAKE_YOUR_COMPAIGN","Renforcer et rendre votre ".$project." pour le rendre attrayant et convaincant pour augmenter votre taux de réussite.");

define("FUNDRAISING_DETAILS", "Collecte de fonds Détail");
define("DEAL_TYPE", "Type d'aubaine");
define("DEAL_HIGHLIGHTS", "Faits saillants Deal");
define("ELEVATOR_PITCH", "Elevaàr Pitch");
define("CHOOSE_THE_FOUR_MOST_BUZZ_WORTHY", "Choisissez les 4 hightlights plus de buzz-dignes sujet de votre cycle actuel qui va impressionner les investisseurs. Ce sera en vedette dans le haut de votre transaction.");
define("ADD_COVER_PHOTO", "Ajouter Phoà de couverture");
define("VIDEO_URLC", "URL de la vidéo");
define("INVESTORS", "Investisseurs");
define("DEAL_DOCUMENTS", "Documents Deal");
define("PREVIOUS_FUNDING", "Financement précédente");


define("I_HAVE_READ_AND_UNDERSTAND", " Je l'ai lu et compris les lignes directrices du projet de crowdfunding.");
define("COMPANY_PROFILE", "Profil de l'entreprise");
define("COMPANY_BASIC", "Société de base");
define("COMPANY_NAME", "Nom de l'entreprise");
define("EQUITY_CROWDFUNDING_URL", "Équité crowdfunding URL");
define("WEBSITE_URLC", "URL de site web");
define("YEAR_FOUNDED", "Année de fondation");
define("HEADQUATER_CITY", "Siège de Ville");
define("HEADQUATER_STATE", "Etat de siège");
define("HEADQUATER_COUNTRY", "Siège Pays");
define("COMPANY_CATEGORY", "Société Catégorie");
define("COMPANY_INDUSTRY", "Secteur d'activité");
define("SELECT_A_CATEGORY", "Choisir une catégorie ...");
define("SELECT_INDUSTRIES", "Sélectionnez Industries ...");
define("ASSETS_UNDER_MANAGEMENT", "Les actifs sous gestion");
define("SQUARE_FOOTAGE_DEVELOPED", "Place Footage Développé");
define("SQUARE_FEET", ",000 pieds carrés");
define("COMPANY_ASSETS", "Actifs de la société");
define("UPLOAD_YOUR_LOGO", "Téléchargez votre logo à votre profil.");
define("COMPANY_OVERVIEW", "Présentation De L'Entreprise");
define("COMPANY_CONNECT", "Société Connect");
define("CONNECT_YOUR_SOCIAL_NETWORKS", "Connectez vos réseaux sociaux et le site Web. Ces éduquer et démontrer votre crédibilité.");
define("EMAIL_CONTACT", "courriel Contactez");
define("COMPANY_EMAIL", "Société email");
define("SOCIAL", "Social");
define("ENTER_THE_URLS_FROM_YOUR", "Entrez les URL de vos profils sociaux");
define("LINKED_IN", "LinkedIn");
define("LINKED_IN_URL", "LinkedIn In URL");
define("FACEBOOK_URLC", "Facebook URL");
define("TWITTER_URLC", "Twitter URL");
define("TEAM_MEMBER_BIO", "Équipe Bio membres");
define("MEMBER_PHOTO", "Phoàs membres");
define("ROLE_TITLE", "Rôle / titre");
define("VISIBLE_ON_PROFILE", "Visible sur le profil");
define("MAKE_ADMIN", "Assurez Administrateur?");
define("TEAM_MEMBERS_CONTENT", "Ajouter des membres et conseillers de votre équipe afin que les gens savent qui est impliqué. Chaque partie prenante représente un moyen puissant et personnelle pour démontrer la force de votre entreprise.");
define("MEMBER_ROLE", "Membre Rôle");
define("ACCESS_LEVEL", "Niveau d'accès");
define("VISIBLE_PROFILE", "Visible sur le profil");
define("TEAM_MEMBER_TYPE", "Type de membre de l'équipe");
define("ADD_TEAM_MEMBER", "+ Ajouter Membre de l'équipe");
define("ADD_COMPANY", "Ajouter entreprise");
define("ALL_DETAIL_OF_YOUR_DEAL", "Tous les détails de votre transaction seront couverts dans cette section. Ceci est où vous allez définir les termes de votre affaire et fournir des documents clés.");
define("MINIMUM_RAISE", "Levez minimum");
define("MAXIMUM_RAISE", "Relance maximum");
define("COMPANY_NAME_ALREADY_EXISTS", "Nom de l'entreprise existe déjà");
define("PLEASE_CLICK_ON_COMPANY_INDUSTRY", "S'il vous plaît Cliquez sur Domaine de la société pour supprimer");
define("YOU_HAVE_ALREADY_APPLIED_THAT", "Vous avez déjà appliqué cette industrie");
define("PLEASE_SELECT_COMPANY_INDUSTRY", "S'il vous plaît Sélectionnez Domaine de la société");
define("YOU_MAY_ONLY_SELECT_UP_TO", "Vous ne pouvez sélectionner jusqu'à industries% de");
define("PNG_JPG_OR_GIF_RECOMENDED_SIZE", "PNG, JPG or GIF <br/><br/><b>Recommended size:</b><br/>170 x 170 pixels <br> Max Size: 2MB");
define("URL_ALREADY_EXISTS", "l'URL existe déjà");
define("TERMS_OF_SERVICE", "Conditions d'utilisation");
define("FUNDRAISING_GOAL", "Objectif de financement");
define("CLOSING_DATE_THIS_CAN_BE_EXTENDED", "Date de clôture");
define("CLOSING_DATE", "Date de clôture");
define("IS_YOUR_COMPANY_CURRENTLY_FUNDRAISING", "Votre entreprise est actuellement la collecte de fonds?");
define("AMOUNT_RAISED_IN_CURRENT_ROUND", "Montant Élevé dans ronde actuelle");
define("DATE_ROUND_OPENED", "Date Tour Ouvert");
define("SELECT_THE_DEAL_TYPE_THAT_FITS_YOUR_CURRENT_ROUND", "Sélectionnez le type de contrat qui correspond à votre cycle actuel.");
define("EQUITY", "Équité");
define("PRICE_EQUITY_IN_YOUR_COMPANY", "L'équité prix dans votre entreprise");
define("CONVERTIBLE_NOTE", "Remarque Convertible");
define("DEBT_THAT_CAN_CONVERT_TO_EQUITY", "La dette qui peut convertir à l'équité");
define("DEBT", "Dette");
define("BORROW_FROM_INVESTORS_AT_SET_INTEREST_TERMS", "Emprunter des investisseurs au set termes d'intérêt");

define("REVENU_SHARE", "Revenue Share");
define("SELL_A_PORTION_OF_YOUR_FUTURE_REVENUES_TO_YOUR_INVESTORS", "Vente d'une partie de vos revenus futurs à vos investisseurs");

define("AVAILABLE_SHARES", "Actions disponibles");
define("WHAT_PERCENTAGE_OF_THE_TOTAL_COMPANY", "Quel est le pourcentage des capitaux propres de l'entreprise àtale est disponible?");
define("COMPANY_VALUATION", "Evaluation d'entreprise");
define("EQUITY_AVAILABLE", "Equity Disponible");
define("PRICE_PER_SHARE", "Prix ​​par action");
define("TERM_LENGTH", "Longueur terme");
define("VALUATION_CAP", "Cap d'évaluation");
define("CONVERSATION_DISCOUNT", "Remise de conversion");
define("WARRANT_COVERAGE", "Couverture Warrant");
define("RETURNT", "Type de reàur");
define("SELECT_RETURN", "Sélectionnez Reàur");
define("GROSS_REVENUES", "Revenus bruts");
define("GROSS_PROFITS", "Bénéfices Bruts");
define("RETURN_PERCENTAGE", "Pourcentage de reàur");
define("MAXIMUM_RETURN", "Rendement maximal");
define("INVESTMENT_AMOUNT", "Montant Invesment");
define("PAYMENT_FREQUENCY", "Fréquence des paiements");
define("SELECT_AYMENT_FREQUENCY", "Sélectionnez Fréquence des paiements");
define("BIOMONTHLY", "Bimensuel");
define("QUARTERLY", "Trimestriel");

define("PAYMENT_START_DATE", "Payback Date de début");
define("SELECT_PAYMENT_DATE_TYPE", "Sélectionnez date de paiement de type");
define("EXACT_DATE", "Date Exacte");
define("NUMBER_OF_MONTHS_FROM", "Nombre de mois, à partir Finance Fermer");

define("NEXT_TO_STEP_2", "Suivant à l'étape 2");
define("UPDATE_TEAM_MEMBER", "Mise à jour membre de l'équipe");
define("COMPANY_CURRENTLY_FUNDRAISING", "Société actuellement la collecte de fonds");

define("SELECT_TEAM_MEMBER_TYPE", "Sélectionnez Type de membre de l'équipe");
define("RECOMMENDED_SIZE_800_450", "PNG, JPG or GIF <br/><br/><b>taille recommandée:</b><br/>800 x 450 pixels");
define("OPTIONAL", "(Optionnel)");
define("PLUS_ADD_PERK", "+ Ajouter Perk");
define("NEXT_TO_STEP_3", "Suivant à l'étape 3");
define("MAIN_VIDEO_OR_IMAGE_TO_BE_DISPLAYED","Vidéo principale ou de l'image à afficher en haut de votre ".$project."page.");
define("YOUTUBE_OR_VIMEO", "Youtube ou Vimeo");
define("PLEASE_CLICK_ON_TEAM_MEMBER_TO_DELETE", "S'il vous plaît Cliquez sur Membre de l'équipe pour supprimer");
define("PLEASE_CLICK_ON_TEAM_MEMBER_TO_EDIT", "S'il vous plaît Cliquez sur Membre de l'équipe à Modifier");
define("PLEASE_CLICK_ON_PERK_TO_DELETE", "S'il vous plaît Cliquez sur Perk pour supprimer");
define("PLEASE_CLICK_ON_PERK_TO_EDIT", "S'il vous plaît Cliquez sur Modifier avantage à");
define("INVALID_DATA", "Données invalides");
define("UPDATE_PERK", "Mise à jour perk");
define("ENTER_AND_CONFIRM_YOUR_COMPANY", "Entrez et confirmez les informations de compte bancaire de votre entreprise à obtenir du financement à la fin si votre ". $project." est entièrement financé.");
define("BANK_ACCOUNT_DETAIL", "Détail du compte bancaire");
define("NAMEOF_BANK_AS_IT_APPEARS", "Nom de la banque tel qu'il apparaît sur vos chèques papier.");
define("ACCOUNT_TYPE", "Type de compte");
define("SELECT_ACCOUNT_TYPE", "Sélectionner le type de compte");
define("ACCOUNT_NUMBERC", "Numéro de compte");
define("EXAMPLE_ACCOUNT_NUMBER", "<Strong> Exemple: </ strong> 0001 23456 789");
define("CONFIRM_ACCOUNT_NUMBER", "Confirmez N ° de compte");
define("CONFIRM_YOUR_ACCOUNT_NUMBER_BY_TYPING_AGAIN", "Confirmez votre numéro de compte en le retapant.");
define("ROUTING_NO", "Routage No.");
define("NINE_DIGIT_NUMBER_ON_THE_BOTTOM_LEFT", "Numéro à 9 chiffres en bas à gauche de vos chèques papier <br> (<strong> Exemple:. </ Strong> 123456789)");
define("FINISH", "Finition");
define("AN_EXECUTIVE_SUMMARY_AND_TERM_SHEET_ARE_REQUIRED", "Un résumé et la feuille terme sont nécessaires. Vous pouvez ajouter des documents supplémentaires et les marquer comme confidentielle pour contrôler l'accès des investisseurs.");
define("EXECUTIVE_SUMMARY", "Résumé");
define("TERM_SHEET", "Term Sheet");
define("ADD_CUSTOM_DOCUMENT", "+ Ajouter un document personnalisé");
define("NEXT_STEP_FOUR", "Suivant à l'étape 4");
define("ADD_ADDITIONAL_DOCS_OR_FILES", "Ajouter docs ou des fichiers supplémentaires qui seraient pertinents pour les investisseurs.");
///added by rakesh
define('PRE_FUNDING_DETAILS',"Pré Details Bailleurs de fonds");
define('ADD_YOUR_PRE_FUDING',"Ajoutez vos détails de financement et les investisseurs actuels pré. Gérer vos documents de même section.");
define('EQUITY_PREVIOUS_FUNDING',"Financement précédente");
define('ADD_FUNDING_FROM_PREV',"Ajouter un financement de àurs auparavant fermés. Ne pas inclure le financement au sein de votre cycle actuel.");
//define('FUNDING_SOURCE',"Source De Financement");
define('SELECT_FUNDING_SOURCE',"Sélectionnez Source de financement");
define('EQUITY_FUNDING_TYPE',"Type de financement");
define('SELECT_EQUITY_FUNDING_TYPE',"Sélectionner le type de financement");
define('FUNDING_AMOUNT',"Montant du financement");
define('FUNDING_DATE',"Date de financement");
define('ADD_PREV_FUNDING_SOURCE',"+ Ajouter financement précédente Source");
define('UPDATE_PREV_FUNDING_SOURCE',"Mettre à jour financement précédente Source");
define('PLEASE_CLICK_ON_PREVIUOS_FUNDING_TO_DELETE',"S'il vous plaît Cliquez sur le financement précédent pour supprimer");
define('PLEASE_CLICK_ON_PREVIUOS_FUNDING_TO_EDIT',"S'il vous plaît Cliquez sur le financement Avant Modifier");
define('PLEASE_CLICK_ON_INVESTOR_TO_DELETE',"S'il vous plaît Cliquez sur pour supprimer INVESTISSEUR");
define('PLEASE_CLICK_ON_INVESTOR_TO_EDIT',"S'il vous plaît Cliquez sur Modifier INVESTORà");
define('RAISE_SOURCE',"Levez Source");
//define('INVESTORS',"Investisseurs");
define('SELECT_INVESTOR_TYPE',"Sélectionner le type de l'investisseur");
define('INVESTOR_TYPE',"Type d'investisseur");
define('INVESTOR_PHOTO',"Investisseurs phoà");
define('INVESTOR_BIO',"Bio Invesàr");

define('ADD_INVESTORS',"+ Ajouter investisseurs");
define('UPDATE_INVESTORS',"Mise à jour de l'investisseur");


define('UPDATE_VIDEO',"Mise à jour vidéo");
define('URL',"URL");
define('ADD_MORE_VIDEO',"Ajouter plus de vidéos liés à votre ". $project."  Ceux-ci apparaîtront dans l'onglet \"Docs & Media de votre ". $project." page.");
define('PLEASE_CLICK_ON_VIDEO_TO_EDIT',"S'il vous plaît Cliquez sur la vidéo pour modifier");
define('PLEASE_CLICK_ON_VIDEO_TO_DELETE',"S'il vous plaît Cliquez sur la vidéo pour supprimer");

define('UPDATE_IMAGE',"Mise à jour de l'image");
define('ADD_MORE_IMAGE',"Ajouter plus d'images liées à votre ".$project."  Ceux-ci apparaîtront dans l'onglet \"Docs & Media de votre ". $project." page.");
define('PLEASE_CLICK_ON_IMAGE_TO_EDIT',"S'il vous plaît cliquer sur l'image pour modifier");
define('PLEASE_CLICK_ON_IMAGE_TO_DELETE',"S'il vous plaît cliquer sur l'image pour supprimer");
define('PLEASE_CLICK_ON_DEAL_DOCUMENT_TO_DELETE',"S'il vous plaît Cliquez sur Traiter document à supprimer");
define('PLEASE_CLICK_ON_DEAL_DOCUMENT_TO_EDIT',"S'il vous plaît Cliquez sur Modifier document donne à");
define('CONFEDENTIAL',"Confidentiel");
define('ALL_ACCESS',"Tout Accès");
define('PRIVATEC',"Privé");
define('PUBLICC',"Public");
define('PLEASE_UPLOAD_DOC_PPT_ZIP_PDF',"S'il vous plaît télécharger un fichier .doc, .ods, .pdf, .zip, .ppt, .xls, .odt");
define('SORRY_THIS_FILE_IS_TOO_LARGE_PLEASE_SELECT_A_FILE_THAT_IS_LESS_THAN_TWO_MB',"Désolé, ce fichier est ào grande. S'il vous plaît sélectionner un fichier qui est inférieure à 2 Mo");
define('MINIMUM_RAISE_SHOULD_NOT_BE_LESS_THAN',"Raise minimum ne doit pas être inférieure à");
define('MAXIMUM_RAISE_SHOULD_NOT_BE_GREATER_THAN',"Raise maximale ne doit pas être supérieure à");

define('NUMBER_OF_MONTHS',"Nombre de mois");
define('PRICE_PER_SHARE_SHOULD_BE_BETWEEN',"Prix ​​par action devrait se situer entre");
define('EQUITY_AVAILABLE_SHOULD_BE_BETWEEN',"Disponible équité doit être comprise entre");
define('COMPANY_VALUATION_SHOULD_BE_BETWEEN',"Evaluation d'entreprise devrait se situer entre");

define('INTEREST_SHOULD_BE_BETWEEN',"L'intérêt devrait se situer entre");
define('TERM_LENGTH_SHOULD_BE_BETWEEN',"Longueur de durée doit être comprise entre");
define('VALUATION_CAP_SHOULD_BE_BETWEEN',"Cap d'évaluation doit être comprise entre");
define('CONVERSATION_SHOULD_BE_BETWEEN',"Remise de conversion doit être comprise entre");
define('WARRANT_COVERAGE_SHOULD_BE_BETWEEN',"Couverture adjudant doit être comprise entre");

define('RETURN_PERCENTAGE_SHOULD_BE_BETWEEN',"Reàur Pourcentage devrait se situer entre");
define('MAXIMUM_RETURN_SHOULD_BE_BETWEEN',"Rendement maximal doit être comprise entre");
define('YOU_HAVE_ALREADY_SENT_INVITATION',"Vous avez déjà envoyé l'invitation à cette adresse e-mail");
define('PLEASE_SELECT_THE_ROLE_FOR_THIS_MEMBER',"S'il vous plaît sélectionnez le rôle pour ce membre");

////******************* new added**************//
define("PENDING", "En attendant");
define("SUCCESSFUL", "Réussi");
define("CONSOLE_SUCCESS", "Le succès de la console");
define("FAILURE", "Échec");
define("GOAL", "But");
define("TEAM_MEMBERS", "Membres De L'Équipe");
define("GET_FUNDED", "Obtenez financés");
define('GOAL_SHOUID_BE_BETWEEN',"Objectif devrait être entre");
define('TO',"à");
define("YOUR_PERKS", "Vos Perks");
define("AMOUNT", "Quantité");
define("DESCRIPTION", "Description");
define('LINK',"Lien");
define("WEBSITE", "Site Internet");
define('CHANGE_IMAGE',"Changer l'image");
define("EMAIL", "Email");
define("SEND_INVITES", "Envoyer des invitations");
define("PERSONAL_INFORMATION", "Information Personnelle");
define("ROUTING_NUMBER", "Nombre de routage");
define("ADDRESS", "Adresse");
define("GOOGLE_ANALYTICS_CODE", "Google Code Analytics");
define("FUNDING_TYPE", "Type de financement");
define('YOUTUBE_LINK',"Lien Youtube");
define('PROJECT_TITLE',"Titre Du Projet");
define("CURRENCY", "Devise");
define("ZIPCODE", "Code Postal");
define("COVER_PHOTO", "Photo De Couverture");
define("THE_COMPANY_ASSETS_FIEID_IS_REQUIRED", "The Field actifs de la société est nécessaire.");
define("THE_COMPANY_COVER_FIEID_IS_REQUIRED", "La photo Compagnie de campagne couverture est n?essaire.");
define("SHOW_ME_WHATS_LEFT_TO_DO", "Montrez-moi ce qui reste à faire");
define("SAVE", "Sauvegarder");
define('NOT_COMPLETED',"Pas achevé");
define('GO_LIVE_CHECKLIST',"Aller liste de contrôle direct");
define('HERE_IS_WHAT_IS_LEFT_TO_DO_BEFORE_YOU_GO_LIVE',"Voici ce qui reste à faire avant que vous allez vivre.");
define('SELECT_COMPANY',"Sélectionnez Société");
define('ADD_NEW_COMPANY',"Ajouter une nouvelle société");
define('PLEASE_SELECT_A_COMPANY_OR_ADD_NEW_COMPANY',"S'il vous plaît sélectionner une entreprise ou Ajouter une nouvelle société");
define('PERK_AMOUNT',"Montant Perk");
define('IMAGE_UPLOADED_SUCCESSFULLY',"Image téléchargée avec succès");
define('DELETE_COMMENT_CONFIRMATION',"Are you sure,You want to delete this comment?");
define('RECORD_UPDATED_SUCCESSFULLY',"Enregistrez correctement mis à jour.");
define('DELETE_PROJECT_CONFIRMATION',"Are you sure ,You want to delete this ".$project.".?");
define('IMAGE_MUST_BE_GREATER_THAN',"L'image doit être supérieure à 800px X 450px");
define('PERK_AMOUNT_SHOULD_BETWEEN',"Perk montant devrait entre% s et% s pour chaque avantage.");
define("RECORD_UPDATED_SUCCESS", "Enregistrez correctement mis à jour.");
define('INVALID_VIDEO_URL',"Invalid URL de la vidéo");
define('COMPLETE_MY_COMPAIGN',"Compléter ma ". $project);

define("KEEP_THE_FUNDS_YOU_RAISE_EVEN_IF_YOU_DONT_REACH_YOUR_GOAL", "Gardez les fonds que vous soulevez, même si vous ne parvenez pas à votre objectif.");
define("GOOD_IF_YOUR_PROJECT_CAN_USE_WHATEVER_AMOUNT_OF_FUNDS_YOU_CAN_RAISE", "Bon si votre ". $project." peut utiliser tout montant de fonds que vous pouvez soulever.");
define("YOU_CAN_RECEIVE_FUNDS_VIA_DIRECT_CREDIT_CARD_OR_PAYPAL", "Vous pouvez recevoir des fonds par carte de crédit direct ou PayPal");
define('WE_CHARGE_FEE_IF_YOU_REACH_YOUR_GOAL_AND_FEE_IF_YOU_DONT', "Nous facturons des frais de %s%% si vous atteignez votre objectif, et des frais de %s%% si vous ne le faites pas.");
define("CONTRIBUTIONS_ARE_REFUNDED_IF_YOU_DONT_MEET_YOUR_GOAL", " Les cotisations sont remboursées si vous ne répondez pas à votre objectif.");
define("GOOD_IF_YOUR_PROJECT_CANT_HAPPEN_UNLESS_YOU_REACH_YOUR_GOAL", " Bon si votre ".$project." ne peut se produire que si vous atteignez votre objectif.");
define("YOU_RECEIVE_FUNDS_VIA_PAYPAL_ONLY", "  Vous recevez des fonds via PayPal uniquement");
define("YOUR_MAXIMUM_COMPAIGN_LENGTH_IS_DAYS", "  Votre maximale ". $project." longueur est de% s jours");
define("WE_CHARGE_FEE_IF_YOU_REACH_YOUR_GOAL_AND_FEE_IF_YOU_DONT_BUT_YOU_GET_NO_MONEY", "Nous facturons des frais de %s%% si vous atteignez votre objectif, et sans frais si vous ne le faites pas, mais vous avez pas d'argent");
define("INELASTIC_FUNDING", "Financement inélastique");
define('INELASTIC_FUNDING_FIXED',"Financement inélastique (Fixed)");
define('ELASTIC_FUNDING_FLEXIBLE',"Financement élastique (flexible)");
define("DONATIONS_ARE_REFUNDED_IF_TARGET_IS_NOT_MET",  $funds_plural." sont remboursés si la cible est pas remplie.");
define("THIS_IS_GOOD_IF_YOUR_PROJECT_CANT_COMMENCE_UNLESS_YOUR_TARGET_IS_MET", "Ce qui est bon si votre projet ne peut pas commencer à moins que votre objectif est atteint.");
define('COMPLETED',"Terminé");
define('PLEASE_UPLOAD_A_JPG_PNG_GIF_FILE',"S'il vous plaît télécharger un fichier .jpg, .png, .gif");
define('DOCUMENT',"Document");
define('CUSTOMIZE_YOUR_EQUITY_URL',"Personnalisez votre $project crowdfunding URL. Vous pouvez inclure lettres, des chiffres, des tirets (-), ou de soulignement (_) seulement.");
define('PLEASE_SELECT_ONE_DEAL_TYPE',"S'il vous plaît choisir un type Deal");
define('MAXIMUM_RAISE_AMOUNT_MUST_NOT_BE_GREATER_THAN_YOUR_GOAL',"Montant de la relance maximale ne doit pas être supérieure à montant de votre objectif.");
define('MIMIMUM_RAISE_AMOUNT_MUST_NOT_BE_GREATER_THAN_YOUR_MAXIMUM_AMOUNT',"Montant de la relance minimum ne doit pas être supérieur à votre montant de la relance maximale.");
define('PLEASE_ENTER_A_DOCUMENT',"S'il vous plaît entrez un nom de document");
define('PLEASE_SELECT_A_DOCUMENT_TYPE',"S'il vous plaît sélectionner un type de document");
define('YEAR_FOUNDED_CANNOT_BE',"Année de fondation ne peut pas être l`année future");
define("GO_LIVE_AND_GET_FUNDED", "Aller en direct et obtenir du financement!");
define("YOUR_CAMPAIGN_IS", "Votre campagne est");
define("NOT_READY", "Not prêt");
define("READY", "prêt");
define("TO_GO_LIVE_QUITE_YET", "Pour aller vivre àut à fait encore.");

define("ALLOWED_INVESTOR", "Qui peut investir sur votre offre ?");
define("ONLY_ACCREDIATED", "Seuls Accrediated investisseurs");
define("BOTH_ACCREDIATED", "Les deux investisseurs ( deux Accrediated et non - Accrediated peuvent investir . )");

// project detail section 
define("PROJECT_DETAIL", "projet Détail");
define("PROJECT_DETAIL_BOTTOM_TEXT", "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.");
//define("PROJECT_NAME", "Project Name");
define("PROJECT_DESCRIPTION", "Description du projet");
define("PROJECT_TYPE", "Type de projet");
define("PROJECT_SUB_TYPE", "Sous-projet");
define("STREET", "rue");
//define("CITY", "City");
//define("STATE", "State");
//define("COUNTRY", "Country");
define("PROPERTY_SIZE", "Surface");
define("LOT_SIZE", "La taille du lot");
define("YEAR_BUILT", "Année de construction");

//investment summary and market summary
define("MARKET_SUMMARY", "Sommaire du marché");
define("HIGHLIGHT_1", "Points forts #1");
define("HIGHLIGHT_2", "Points forts #2");
define("HIGHLIGHT_3", "Points forts #3");
define("HIGHLIGHT_4", "Points forts #4");
define("INVESTMENT_SUMMARY", "Résumé d'investissement");
define("SUMMARY", "Récapitulatif");
define("MARKET_SUMMARY_TEXT","Texte sommaire du marché");
define("INVESTMENT_SUMMARY_TEXT", "Texte de synthèse d'investissement");

define("UPDATE_FEATURED_PROJECT", "Mise à jour en vedette ".$project."");
define("JOIN_INVESTOR_NETWORK", "Rejoignez réseau");
define("YOU_ARE_A_MEMBER", "Vous êtes membre");
define("LEAVE_NETWORK", "Laissez réseau");
define("INVESTOR_NETWORK", "Réseau des investisseurs");
define("YOU_MUST_BE_A_MEMBER_OF_THE_INVESTOR", "Vous devez être membre du réseau des investisseurs de voir les autres investisseurs");


define("ACCREDITED_INVESTORS", "investisseurs accrédités");
define("ACCREDITED_AND_NON_ACCREDITED_INVESTORS", "Les investisseurs accrédités et non accrédités");
define("OPEN_TO", "Ouvert à");

define("PROJECTED_RETURN_CALCULATOR", "Calculatrice de rendement prévu");
define("ABOUT_THE_SPONSER", "A propos du commanditaire");
define("PREVOIUS_FUNDING", "financement précédente");
define('RECOMMENDED_FILE_FORMAT_SHOULD_BE_IMG_DOC',"Format de fichier recommandées devraient être .doc, .ods, .pdf, .zip, .ppt, .xls, .rtf et .odt.");

define('SORRY_THIS_FILE_IS_TOO_LARGE_PLEASE_SELECT_A_JPG_FILE_THAT_IS_LESS_THAN_MB_OR_TRY_RESIZING_IT_USING_A_PHOTO_EDITOR',"Désolé, ce fichier est trop volumineux.  S'il vous plaît sélectionner un fichier .jpeg est inférieure à %s MB ou essayez de redimensionner l'aide d'un éditeur de photo.");
define('SORRY_THIS_FILE_IS_TOO_LARGE_PLEASE_SELECT_A_FILE_THAT_IS_LESS_THAN_MB',"Désolé, ce fichier est trop volumineux.  S'il vous plaît sélectionner un fichier qui est inférieure à than %s MB");
define("PNG_JPG_OR_GIF_RECOMENDED_SIZE_FROM_ADMIN", "PNG, JPG or GIF <br/><br/><b>taille recommandée:</b><br/>170 x 170 pixels <br> max Taille: %s MB");
define("ADD_HIGHLIGHT", "+ AJOUTER FAITS SAILLANTS");
define("REQUIRED_FIELD", "Champ requis");


/*activity_lang.php*/
define('REVIEW_ALL_ACTIVITY',"Passez en revue toutes les activités");

define('FOLLOW_BY',"Suivez par");

define('MY_COMMENT_TEXT',"Mon commentaire de texte");

define('COMMENT_ON_PROJECT',"Commentaire sur le projet");


define('WELCOME',"accueil");

define('FOLLOW_PROJECT_TEXT',"Suivez texte du projet");

define('PROJECT_FOLLOW_BY_ME',"Suivi du projet par moi");

define('MY_POST_PROJECT',"Mon projet de poste");

define('FOLLOWER_POST_PROJECT',"Suiveur après projet");

define('FOLLOWER_PROJECT_UPDATES',"Mises à jour du projet de Follower");

define('THERE_ARE_NO_ACTIVITIES',"Il n'y a pas les activités.");

define('MY_FOLLOWERS_FOLLOW',"Mes disciples suivent");

define('FOLLOWING_POST_PROJECT',"Après après projet");

define('FOLLOWING_DONATION',"Après le don");

define('FOLLOWING_COMMENTS',"Suite aux commentaires");

define('FOLLOWING_UPDATES',"mises à jour suivantes");

define('FOLLOWING_GALLERY',"galerie suivante");

define('FOLLOWING_PROJECT',"Suite du projet");

define('ADD_GALLERY_IN_PROJECT',"Ajouter la galerie en projet");

define('FOLLOWER_PROJECT_DONATION',"Don du projet de Follower");

define('DONATION_ON_PROJECT',"Don sur le projet");
///added for only activity messages
define('YOU_HAVE_STARTED_FOLLOWING',"Vous avez commencé %s %s");
define('USER_HAVE_STARTED_FOLLOWING_YOU',"%s a commencé %s tu.");
define('USER_HAVE_STARTED_FOLLOWING_USER',"%s a commencé %s %s");
define('YOU_HAVE_STARTED_UNFOLLOWING',"Vous avez commencé %s %s");
define('USER_HAVE_STARTED_UNFOLLOWING_YOU',"%s a commencé %s tu.");
define('USER_HAVE_STARTED_UNFOLLOWING_USER',"%s un startedd startehas %s %s");
define('USER_JOINED_SITE',"%s rejoint %s");
define('YOU_UPDTAED_ACCOUNT_INFORMATION',"Vous avez mis à jour vos informations de compte");
define('USER_UPDTAED_ACCOUNT_INFORMATION',"%s a mis à jour les informations de compte");
define('YOU_APPLIED_TO_BECOME_ACCREDITED',"Vous avez demandé à devenir accrédité %s");
define('USER_APPLIED_TO_BECOME_ACCREDITED',"%s demandé à devenir %s %s");


define('USER_HAVE_STARTED_FOLLOWING_YOUR_CAMPAIGNS',"%s a commencé %s votre projet %s");
define('USER_HAVE_STARTED_FOLLOWING_USER_ON_CAMPAIGNS',"%s a commencé %s %s that you are already %s");
define('USER_HAVE_STARTED_UNFOLLOWING_YOUR_CAMPAIGNS',"%s a commencé %s votre projet %s");
define('USER_HAVE_STARTED_UNFOLLOWING_USER_ON_CAMPAIGNS',"%s a commencé %s %s que vous êtes déjà %s");

define('USER_HAVE_STARTED_FUNDING_USER_ON_CAMPAIGNS',"%s a commencé %s %s que vous êtes déjà %s");
define('USER_HAVE_STARTED_COMMNENT_USER_ON_CAMPAIGNS',"%s a commencé%s %s que vous êtes déjà %s");
define('USER_HAVE_STARTED_TEAMMEMBER_USER_ON_CAMPAIGNS',"%s a commencé %s %s que vous êtes déjà un membre de l'équipe de");
define('USER_HAVE_STARTED_FOLLOWING_ADMIN',"%s a commencé %s %s");


define('USER_HAVE_STARTED_FUNDING_USER_ON_CAMPAIGNS_UNFOLLOW',"%s a commencé %s %s que vous êtes déjà %s");
define('USER_HAVE_STARTED_COMMNENT_USER_ON_CAMPAIGNS_UNFOLLOW',"%s a commencé %s %s que vous êtes déjà %s");
define('USER_HAVE_STARTED_TEAMMEMBER_USER_ON_CAMPAIGNS_UNFOLLOW',"%s a commencé %s %s que vous êtes déjà un membre de l'équipe de");
define('USER_HAVE_STARTED_FOLLOWING_ADMIN_UNFOLLOW',"%s a commencé %s %s");
//updates
define('YOU_HAVE_STARTED_UPDATES',"Vous avez posté un nouveau %s sur %s");
define('USER_HAVE_STARTED_UPDATES_FOR_FOLLOWING_USER',"%s a posté nouvelle %s sur %s que vous êtes déjà %s");
define('USER_HAVE_STARTED_UPDATED_FOR_DONOR_USER',"%s a posté nouvelle %s sur %s que vous êtes déjà %s");
define('USER_HAVE_STARTED_UPDATES_FOR_COMMENTER_USER',"%s a posté nouvelle %s sur %s que vous êtes déjà %s");
define('USER_HAVE_STARTED_UPDATES_FOR_TEAMMEMBER_USER',"%s a posté nouvelle %s sur %s que vous êtes déjà un membre de l'équipe de'");
define('USER_HAVE_STARTED_UPDATE_ADMIN',"%s a posté nouvelle %s sur %s");
//funded
define('YOU_HAVE_STARTED_FUND',"Tu as %s %s avec %s");
define('USER_HAVE_STARTED_FUND_PROJECT_OWNER',"%s que vous avez déjà créé est %s avec %s par %s");
define('USER_HAVE_STARTED_FUND_FOR_FOLLOWING_USER',"%s que vous êtes déjà %s est %s avec %s par %s");
define('USER_HAVE_STARTED_FUND_FOR_DONOR_USER',"%s que vous êtes déjà %s est %s avec %s par %s");
define('USER_HAVE_STARTED_FUND_FOR_COMMENTER_USER',"%s que vous êtes déjà %s est %s avec %s par %s");
define('USER_HAVE_STARTED_FUND_FOR_TEAMMEMBER_USER',"%s est %s avec %s par %s que vous êtes déjà un membre de l'équipe de'");
define('USER_HAVE_STARTED_FUND_ADMIN',"%s est %s avec %s de %s");

//commented
define('YOU_HAVE_STARTED_COMMENTED',"Tu as %s sur %s");
define('USER_HAVE_STARTED_COMMENTED_PROJECT_OWNER',"%s %s sur %s votre projet");
define('USER_HAVE_STARTED_COMMENTED_FOR_FOLLOWING_USER',"%s %s sur %s que vous êtes déjà %s");
define('USER_HAVE_STARTED_COMMENTED_FOR_DONOR_USER',"%s %s sur %s que vous êtes déjà %s");
define('USER_HAVE_STARTED_COMMENTED_FOR_COMMENTER_USER',"%s %s sur %s que vous êtes déjà %s");
define('USER_HAVE_STARTED_COMMENTED_FOR_TEAMMEMBER_USER',"%s %s sur %s que vous êtes déjà un membre de l'équipe de'");
define('USER_HAVE_STARTED_COMMENTED_ADMIN',"%s  %s  sur %s ");
//draft_project and submit
define('YOU_CREATED_PROJECT_DRAFT',"Vous avez créé un nouveau %s %s");
define('YOU_SUBMITED_PROJECT',"Vous avez soumis une nouvelle %s %s");
define('YOU_SUBMITED_PROJECT_ADMIN',"%s a soumis une nouvelle %s %s pour évaluation");
//admin project approve
define('ADMIN_PROJECT_APPROVED_OWNER',"votre %s %s est approuvée et publiée par %s équipe");
define('ADMIN_PROJECT_APPROVED_ADMIN',"Vous avez approuvé et publié %s %s");
//admin project declined
define('ADMIN_PROJECT_DECLINED_OWNER',"votre %s %s a été refusée par %s ");
define('ADMIN_PROJECT_DECLINED_ADMIN',"Vous avez refusé %s %s");
//admin project deleted
define('ADMIN_PROJECT_DELETED_OWNER',"votre %s %s a été supprimée par %s équipe");
define('ADMIN_PROJECT_DELETED_ADMIN',"Vous avez supprimé %s %s");

//admin project active
define('ADMIN_HAVE_ACTIVE_PROJECT',"Vous avez activé et publié %s %s ");
define('ADMIN_HAVE_ACTIVE_PROJECT_OWNER',"votre %s %s a été activé et publié par %s équipe");
define('ADMIN_HAVE_ACTIVE_PROJECT_FOR_FOLLOWING_USER',"%s %s que vous êtes déjà %s a été activé et publié par %s équipe");
define('ADMIN_HAVE_ACTIVE_PROJECT_FOR_DONOR_USER',"%s %s que vous avez déjà %s a été activé et publié par %s équipe");
define('ADMIN_HAVE_ACTIVE_PROJECT_FOR_COMMENTER_USER',"%s %s que vous avez déjà %s a été activé et publié par %s équipe");
define('ADMIN_HAVE_ACTIVE_PROJECT_FOR_TEAMMEMBER_USER',"%s %s que vous avez déjà été ajouté en tant que membre de l'équipe a été activé et publié par' %s équipe");
//admin project inactive
define('ADMIN_HAVE_INACTIVE_PROJECT',"Vous avez désactivé et inédits %s %s");
define('ADMIN_HAVE_INACTIVE_PROJECT_OWNER',"votre %s %sa été désactivé et inédit de %s équipe");
define('ADMIN_HAVE_INACTIVE_PROJECT_FOR_FOLLOWING_USER',"%s %s que vous êtes déjà %s a été désactivé et inédit de %s équipe");
define('ADMIN_HAVE_INACTIVE_PROJECT_FOR_DONOR_USER',"%s %s que vous avez déjà %s a été désactivé et inédit de %s équipe");
define('ADMIN_HAVE_INACTIVE_PROJECT_FOR_COMMENTER_USER',"%s %s que vous avez déjà %sa été désactivé et inédit de %s équipe");
define('ADMIN_HAVE_INACTIVE_PROJECT_FOR_TEAMMEMBER_USER',"%s %s que vous avez déjà été ajouté en tant que membre de l'équipe a été désactivé et inédit de' %s équipe");
//project feedback by onwer
define('PROJECT_FEEDBACK_OWNER_SUMBIITED_BY_OWENER',"Vous avez présenté un nouveau message pour  %s %s %s");
define('PROJECT_FEEDBACK_ADMIN_SUMBIITED_BY_OWENER',"%sa soumis un nouveau message pour  %s %s %s");

//project feedback by admin
define('PROJECT_FEEDBACK_OWNER_SUMBIITED_BY_ADMIN',"%s Équipe a présenté %s %s pour %s");
define('PROJECT_FEEDBACK_ADMIN_SUMBIITED_BY_ADMIN',"Vous avez soumis %s %s pour %s");

//team member added
define('TEAM_MEMBER_ADDED_OWNER',"Vous avez ajouté membre de l'équipe sur' %s");
define('TEAM_MEMBER_ADDED_ADMIN',"%s a ajouté un membre de l'équipe sur' %s");
define('TEAM_MEMBER_ADDED_USER',"%s a ajouté un membre de l'équipe sur' %s");

//inrest request
define('INTREST_REQUEST_OWNER',"Vous avez reçu %s devenir %s sur %s de %s");
define('INTREST_REQUEST_USER',"Vous avez exprimé intérêt à devenir un %s sur %s");
//access request
define('ACCESS_REQUEST_OWNER',"Vous avez reçu %s accéder %s sur %s de %s");
define('ACCESS_REQUEST_USER',"Vous avez envoyé votre demande d'accès pour' %s accéder %s");
//upload contract document
define('UPLOAD_CONTRACT_DOCUMENT_USER',"Tu as %s % scontrat de processus copier sur %s");
define('UPLOAD_CONTRACT_DOCUMENT_ADMIN',"%s a %s %s copie du contrat sur %s");
//approve contract document
define('APPROVED_CONTRACT_DOCUMENT_USER',"Your %s copie du contrat a été %s sur %s par %s équipe");
define('APPROVED_CONTRACT_DOCUMENT_ADMIN',"Tu as %s % scopie du contrat de %s sur %s");
//declined contract document
define('DECLINED_CONTRACT_DOCUMENT_USER',"votre %s copie du contrat a été %s sur %s par %s équipe");
define('DECLINED_CONTRACT_DOCUMENT_ADMIN',"Tu as %s %scopie du contrat de %s sur %s");
//acknowledgement contract document
define('ACKNOWLEDGEMENT_CONTRACT_DOCUMENT_USER',"Tu as %s %sreconnaissance des informations sur %s");
define('ACKNOWLEDGEMENT_DOCUMENT_ADMIN',"%s a %s %sreconnaissance des informations sur %s");
//approve acknowledgement contract document
define('APPROVE_ACKNOWLEDGEMENT_CONTRACT_DOCUMENT_USER',"votre %saccusé de réception a été %s sur %s par %s équipe");
define('APPROVE_ACKNOWLEDGEMENT_DOCUMENT_ADMIN',"Tu as %s %s accusé de réception à partir de %s sur %s");
//declined acknowledgement contract document
define('DECLINED_ACKNOWLEDGEMENT_CONTRACT_DOCUMENT_USER',"votre %s accusé de réception a été %s sur %s par %s équipe");
define('DECLINED_ACKNOWLEDGEMENT_DOCUMENT_ADMIN',"Tu as %s %s accusé de réception à partir de%s sur %s");
//submitted tracking
define('SUBMITTED_TRACKING_CONTRACT_DOCUMENT_USER',"%s équipe a %s Partager certificat d'information / de transmission de documents sur' %s");
define('SUBMITTED_TRACKING_DOCUMENT_ADMIN',"Tu as %s Partager certificat d'information / de transmission de documents' sur %s pour %s");
//confirmed submitted tracking
define('CONFIRMED_SUBMITTED_TRACKING_CONTRACT_DOCUMENT_USER',"Tu as %s la livraison de la part certificat / document sur %s");
define('CONFIRMED_SUBMITTED_TRACKING_DOCUMENT_ADMIN',"%s a %sla livraison de la part certificat / document sur %s");

// project success
define('ADMIN_HAVE_SUCCESS_PROJECT',"%s %s créé par %s terminé avec succès!");
define('ADMIN_HAVE_SUCCESS_PROJECT_OWNER',"votre %s %s terminé avec succès!");
define('ADMIN_HAVE_SUCCESS_PROJECT_FOR_FOLLOWING_USER',"%s %s tu es %s terminé avec succès!");
define('ADMIN_HAVE_SUCCESS_PROJECT_FOR_DONOR_USER',"%s %s votre as %s terminé avec succès!");
define('ADMIN_HAVE_SUCCESS_PROJECT_FOR_COMMENTER_USER',"%s %s tu as %s terminé avec succès!");
define('ADMIN_HAVE_SUCCESS_PROJECT_FOR_TEAMMEMBER_USER',"%s %s vous êtes un membre de l'équipe sur hasterminé Avec succès !'");
// project unsuccess
define('ADMIN_HAVE_UNSUCCESS_PROJECT',"%s %s créé par %s terminé sans succès!");
define('ADMIN_HAVE_UNSUCCESS_PROJECT_OWNER',"votre %s %s terminé sans succès!");
define('ADMIN_HAVE_UNSUCCESS_PROJECT_FOR_FOLLOWING_USER',"%s %s tu es %s terminé sans succès!");
define('ADMIN_HAVE_UNSUCCESS_PROJECT_FOR_DONOR_USER',"%s %s tu as %s terminé sans succès!");
define('ADMIN_HAVE_UNSUCCESS_PROJECT_FOR_COMMENTER_USER',"%s %s tu as %s terminé sans succès!");
define('ADMIN_HAVE_UNSUCCESS_PROJECT_FOR_TEAMMEMBER_USER',"%s %s vous êtes un membre de l'équipe a terminé le succès!'");
define('UPLOADED',"Uploaded");
define('APPROVED_AND_CONFIRMED',"Approuvé et confirmé");
define('ACTIVITIES',"activités");

/*social_networking_lang.php*/
define('MANAGE_YOUR_SOCIAL_NETWORK_DETAILS',"Gérez votre réseau social Détails");
define('FACEBOOK_URL',"Facebook URL");
define('TWITTER_URL',"Twitter URL");
define('LINKEDIN_URL',"Linkedln URL");
define('GOOGLE_PLUS_URL',"Google Plus URL");
define('BASECAMP_URL',"BaseCamp URL");
define('YOUTUBE_URL',"Youtube URL");
define('MYSPACE_URL',"MySpace URL");


define('INVALID_FACEBOOK_LINK',"Invalid facebook Lien");
define('INVALID_TWITTER_LINK',"Invalid twitter Lien");
define('INVALID_LINKEDIN_LINK',"Invalid linkedin Lien");
define('INVALID_BANDCAMP_LINK',"Invalid bandcamp Lien");
define('INVALID_YOUTUBE_LINK',"Invalid YouTube Lien");
define('INVALID_MYSPACE_LINK',"Invalid myspace lien");
define('INVALID_IMDB_LINK',"Invalid imdb Lien");

/*accreditation_lang.php*/

 define('FUNDRISE_USES_THIS_INFORMATION_TO_DISTRIBUTE_YOUR_DIVIDENDS_AND_TAX_DOCUMENTS_IF_YOU_BELIEVE_YOU_HAVE_ENTERED_YOUR_DATE_OF_BIRTH_OR_SOCIAL_SECURITY_NUMBER_INCORRECTLY_PLEASE',"Fundrise uses this information to distribute your dividends and tax documents. If you believe you have entered your date of birth or Social Security number incorrectly, please");

define('LEGAL_FIRST_NAME',"Juridique Prénom");

define('LEGAL_LAST_NAME',"Juridique Nom");

define('THIS_MUST_BE_YOUR_PRINCIPAL_RESIDENCE',"Ce doit être votre résidence principale.");

define('PHONE_NUMBER',"numéro de téléphone");

define('BANK_DETAIL',"Coordonnées Bancaires");

define('CHECKING',"Vérification");

define('SAVINGS',"Épargnes");

define('NAME_OF_BANK_AS_IT_APPEARS_ON_YOUR_PAPER_CHECKS',"Nom de la banque tel qu'il apparaît sur vos chèques papier.");

define('EXAMPLE',"Exemple");

define('CONFIRM_ACCOUNT_NO',"Confirmez compte Non");

define('CONFIRM_YOUR_ACCOUNT_NUMBER_BY_TYPING_IT_AGAIN',"Confirmez votre numéro de compte en le retapant.");

define('DIGIT_NUMBER_ON_THE_BOTTOM_LEFT_OF_YOUR_PAPER_CHECKS',"Numéro à 9 chiffres en bas à gauche de vos chèques papier.");

define('PERSONAL_INFORMATION_ADDED_SUCCESSFULLY',"Les renseignements personnels ajouté avec succès ..!");

define('ENTER_ADDRESS',"Saisissez l'adresse");

define('CITY_IS_REQUIRED',"Ville est nécessaire.");

define('STATE_IS_REQUIRED',"État est nécessaire.");

define('ENTER_PHONE',"Entrez le numéro de téléphone.");

define('ENTER_ACCOUNT_NUMBER',"Entrez le numéro de compte.");

define('ENTER_BANK_NAME',"Entrez le nom de la Banque.");

define('ACCOUNT_NUMBER_CANPAS_BE_MORE_THAN_40',"Numéro de compte ne peut pas être plus de 40 caractères.");

define('ACCOUNT_NUMBER_CANPAS_BE_LESS_THAN_4',"Numéro de compte ne peut pas être inférieure à 4 caractères.");

define('ENTER_ACCOUNT_NUMBER_CON',"Entrer Confirmez numéro de compte.");

define('ENTER_ACCOUNT_NUMBER_CAN_PAS_MATCH',"Le n ° de compte Confirmer ne correspond pas avec le Numéro de compte.");

define('INVESTMENT_ACCOUNT',"Compte de placement");

define('FULL_NAME',"Nom Complet");

define('ENTER_ROUTING_NUMBER',"Entrez le numéro de routage.");

define('ACCREDITATION_STATUS',"Statut d'accréditation");

define('ACCREDITED',"Accrédité");

define('EXTERNAL_BANK_ACCOUNTS',"Comptes bancaires externes");

define('PRIMARY',"Primaire");

define('PLEASE_UPLOAD_DOC_OR_PDF_FILE',"S'il vous plaît télécharger doc ou pdf fichier");

define('DUE_TO_EXISTING_SECURITIES_REGULATIONS_THE_MAJORITY_OF_CROWDFUNDING_OFFERINGS_ARE_CURRENTLY_ONLY_AVAILABLE_TO_ACCREDITED_INVESTORS_PLEASE_INDICATE_YOUR_ACCREDITATION_STATUS_BELOW',"Due to existing securities regulations, the majority of Crowdfunding offerings are currently only available to accredited investors. Please indicate your accreditation status below.");

define('I_AM_AN_ACCREDITED_INVESTOR'," Je suis un investisseur accrédité.");

define( 'I_AM' ,"Je suis");

define('NOT',"PAS");

define('AN_ACCREDITED_INVESTOR',"un investisseur accrédité.");

define('ACCREDITATION_TYPE',"Type d'agrément");

define('PLEASE_CHECK_ALL_OF_THE_FOLLOWING_THAT_APPLY',"S'il vous plaît vérifier tous les éléments suivants applicables:");

define('NET_WORTH',"Net Worth");

define('I_HAVE_INDIVIDUAL_NET_WORTH_OR_JOINT_NET_WORTH_WITH_MY_SPOUSE_THAT_EXCEEDS_1_MILLION_EXCLUDING_THE_VALUE_OF_MY_PRIMARY_RESIDENCE',"I have individual net worth, or joint net worth with my spouse, that exceeds $1 million (excluding the value of my primary residence).");

define('INDIVIDUAL_INCOME',"Le revenu des particuliers");

define('I_HAVE_INDIVIDUAL_INCOME_EXCEEDING'," Je dois revenu individuel excédant");

define('IN_EACH_OF_THE_PAST_TWO_YEARS_AND_EXPECT_TO_REACH_THE_SAME_THIS_YEAR'," dans chacune des deux dernières années et attendre pour atteindre le même cette année");

define('JOINT_INCOME',"Revenu mixte");

define('I_HAVE_COMBINED_INCOME_WITH_MY_SPOUSE_EXCEEDING',"Je suis revenu combiné avec mon conjoint excédant");

define('BUSINESS',"Entreprise");

define('IN_ASSETS_AND_OR_ALL_OF_THE_EQUITY_OWNERS_ARE_ACCREDITED',"d'actifs et / ou tous les propriétaires d'actions sont accrédités");

define('I_INVEST_ON_BEHALF_OF_A_BUSINESS_OR_INVESTMENT_COMPANY_WITH_MORE_THAN',"Je investis au nom d'une société commerciale ou d'investissement avec plus de");

define('VERIFICATION',"Vérification");

define('DUE_TO_EXISTING_SECURITIES_REGULATIONS_CROWDFUNDING_IS_REQUIRED_TO_DETERMINE_THAT_INVESTORS_ARE_QUALIFIED_TO_PARTICIPATE_IN_THE_INVESTMENTS_OFFERED_ON_THE_WEBSITE_PLEASE_HELP_US_DETERMINE_YOUR_ACCREDITATION_STATUS_BY_CHOOSING_ONE_OF_THE_FOLLOWING_OPTIONS',"Due to existing securities regulations, Crowdfunding is required to determine that investors are qualified to participate in the investments offered on the website. Please help us determine your accreditation status by choosing one of the following options");

define('OPTION_A',"Option A");

define('PHONE_CALL',"Appel Téléphonique");

define('HAVE_A_PHONE_CALL_WITH_A_MEMBER_OF_THE_CROWDFUNDING_INVESTMENTS_TEAM',"Avoir une conversation téléphonique avec un membre de l'équipe des investissements crowdfunding");

define('REQUEST_A_PHONE_CALL',"Demander un appel téléphonique");

define('OPTION_B',"Option B");

define('ACCREDITATION_LETTER',"Lettre d'accréditation");

define('HAVE_YOUR_PERSONAL_BROKER_CERTIFIED_PUBLIC_ACCOUNTANT_INVESTMENT_ADVISOR_OR_LICENSED_ATTORNEY_COMPLETE_OUR_STANDARD_ACCREDITATION_LETTER',"Have your personal broker, certified public accountant, investment advisor or licensed attorney complete our standard accreditation letter");

define('UPLOAD_A_COMPLETED_LETTER',"Ajouter une lettre Terminé");

define('DOWNLOAD_BLANK_LETTER',"Télécharger Lettre Blank");
	
define('ACCREDITATION_ADDED_SUCCESSFULLY',"informations d'accréditation ajouté avec succès");

define('YOU_HAVE_ALREADY_APPLIED_FOR_ACCREDITATION',"Vous avez déjà demandé une accréditation");

define('FIRST_FILL_PERSONAL_INFORMATION',"Remplissez d'abord vos renseignements personnels s'il vous plaît.");


define('APPROVE',"Approuver");
define('ACCOUNT_NUMBER',"Numéro de compte");
define('ZIP_CODE',"Code Postal");
define("DOC_OR_PDF", "DOC, PDF, ZIP, JPG, PNG, GIF ");
define('PLEASE_UPLOAD_DOC_IMAGE_ZIP_PDF',"S'il vous plaît télécharger un fichier .doc  .ods,, .pdf , .zip , .odt , .rtf , .jpg , .png et .gif");

/*message_lang.php*/
define('SEND_MESSAGE',"envoyer le message");
define('SUBJECT',"sujet");
define('MESSAGE_LIST',"Liste des messages");
define('SENDER',"expéditeur");
define('RECEIVER',"récepteur");
define('MESSAGE_CONVERSATION',"message conversation");
define('VIEW_CONVERSATION',"Voir le message");
define('CONVERSATION_MESSAGE',"conversation Messages");
define('CONVERSATION_OF_MESSAGE',"conversation de message");
define('CONVERSATION_DATE',"conversation date");
define('SEND_A_MESSAGE_TO',"Envoyer un message à");
define('YOUR_MESSAGE_HAS_BEEN_SUCCESSFULLY',"Votre message a été envoyé avec succès");
define('SUBJECT_IS_REQUIRED',"Objet est nécessaire");
define('MESSAGE_IS_REQUIRED',"Message est requis");
define('SEND_A_REPLY',"répondre");
define('CONVERSATION',"conversation");

/*invest_lang.php*/
define('DONATION_AMOUNT',"Montant Du Don");
define('ADD_SHIPPING_ADDRESS',"Ajouter adresse de livraison");
define('FORM_FUND',"Frm_fund");
define('DONATION',"Don");
define('STEP',"Étape");
define('PICK_YOUR_REWARD',"Choisissez votre récompense");
define('PICK_A_PERK',"Choisissez un avantage");
define('NO_PERK_JUST_A_CONTRIBUTION',"No Perk, just a investment!");
define('ENTER_YOUR_TOTAL_CONTRIBUTION_AMOUNT',"Entrez le montant total de la contribution.");
define('YOUR_INFORMATION',"Vos Informations");
define('CONFIRM_EMAIL',"Confirmez votre e-mail");
define('I_WOULD_LIKE_TO_RECEIVE_WEEKLY_NEWSLETTER',"Je voudrais recevoir la newsletter hebdomadaire.");
define('SHIPPING_INFORMATION',"Information d'expédition");
define('PERSONALIZATION',"Personnalisation");
define('SELECT_HOW_YOU_WOULD_LIKE_YOUR_CONTRIBUTION_TO_BE_DESPLAYED_ON_THE_PUBLIC_CAMPAIN_PAGE',"Sélectionnez comment vous souhaitez que votre contribution soit affiché sur le public ".$project ." page.");
define('GIFT',"Cadeau");
define('I_WOULD_LIKE_TO_MAKE_THIS_CONTRIBUTION_FOR_SOMEONE_ELSE',"Je tiens à apporter cette contribution pour quelqu'un d'autre.");
define('RECIPIENT',"Destinataire");
define('PRIVACY',"Confidentialité");
define('VISIBLE_SHOW_YOUR_OR_THEIR_NAME_AND_AMOUNT',"Visible: Montrez votre (ou their) name and amount");
define('IDENTITY_ONLY_SHOW_YOUR_OR_THEIR_NAME_BUT_HIDE_THE_AMOUNT',"Identité seule: Montrez votre (ou their) name, but hide the amount");
define('ANONYMOUS',"Anonyme");
define('ADD_A_PUBLIC_COMMENT_GREAT_FOR_VOICING_YOUR_SUPPORT_AND_PERSONALYSING_YOUR_CONTRIBUTION',"Ajouter un ". $comment." Public - Idéal pour exprimer votre soutien et de personnaliser votre contribution.");
define('PUBLIC_COMMENT',"Commentaire publique");
define('I_WOULD_LIKE_TO_RECEIVE_EMAILS_WITH_UPDATES_FROM_THE_CAMPAIGN_OWNER',"Je souhaite recevoir des emails avec des mises à jour de la ".$project." propriétaire.");
define('PAYMENT_METHOD',"Mode de paiement");
define('NO_GATEWAY_AVAILABLE',"Aucune passerelle Disponible");
define('CONTRIBUTE_VIA_PAYPAL',"Contribuer via PayPal");
define('PAYMENT',"Paiement");
define('SHIPPING_ADDRESS',"Adresse De Livraison");

define('DONATION_MIN_AMOUNT',"Montant du don doit être supérieure à% s.");

define('DONATION_MAX_AMOUNT',"Montant du don doit être inférieure à% s.");

define('INVALID_DONATION_AMOUNT',"Montant du don non valide.");

define('EMAIL_SHOULD_NOT_BE_BLANK',"Email ne doit pas être vide.");

define('INVALID_EMAIL_FORMAT',"Format e-mail valide.");

define('EMAILS_NOT_SAME',"Email Confirmer l'email et devraient être les mêmes.");

define('ENTER_SHIPPING_ADDRESS',"Saisissez l'adresse d'expédition.");

define('ENTER_RECIPIENT',"Entrez bénéficiaire");

define('SELECT_ONE_GATEWAY',"S'il vous plaît sélectionner au moins une passerelle.");

define('CONTRIBUTE',"Contribuer");
define('PAYMENT_KEY',"Clé de paiement");
define('TRANSACTION_DETAIL',"Détail de transaction");

define('CT_DT',"Ct / Dt");
define('SHIPPING',"Livraison");
define('WITHDRAWAL_DETAILS',"Retrait Détails");

define('VIEW_DETAIL',"Voir Les Détails");
define('YOUR_DONATION_AMOUNT_IS',"Votre Montant du don est");
define('YOU_SELECTED',"Vous avez sélectionné");
define('ESTIMATED_DELIVERY',"Délai de livraison estimé");
define('TOTAL_CONTRIBUTION',"Contribution totale");

define('JUST_FOR_THE_CAMPAIGNER_TO_SEND',"After clicking this button, you’ll be redirected to payment gateway site where you can pay.");
define('AFTER_CLICKING_THIS_BUTTON',"After clicking this button, you’ll be redirected to payment gateway site where you can pay.");
define('CONTRIBUTE_VIA_STRIPE',"Contribuer via Stripe");
define('EXPIRY_MONTH',"Mois d'expiration");
define('EXPIRY_YEAR',"Année d'expiration");
define('CVC_NUMBER',"Nombre CVC");
define('MONTH',"Mois");
define('YEAR',"Année");
define('MY_FUNDIND',"Mon financement");
define('INCOMING_DONATION',"Don entrant");

//new added
define('ADD_PERK_INVEST',"Ajouter Perk");
define('CHANGE_PERK',"Changer Perk");
define('CLAIMED',"Revendiqué");

/// investor admin steps
define('INVESTER_WILL_DOWNLOAD_CONTRACT_DOCUMENTS'," Le téléchargement des documents contractuels");
define('FROM_HERE',"d'ici");
define('TO_SIGN_AND_SEND_BACK',"à signer et à renvoyer");
define('DOWNLOAD_PAYMENT_RECEIPT_FROM_HERE',"Télécharger le reçu de paiement à partir d'ici");
define('CONFIRM_INVESTMENT_AMOUNT_YOU_HAVE_RECEIVED',"Confirmer ".$funds." montant que vous avez reçu par virement bancaire ou par chèque");
define('THIS_STEP_IS_PENDING',"Cette étape est en suspens");
define('INVESTER_WILL_UPLOAD_SIGNED_COPY_OF_CONTRACT'," téléchargera copie signée des documents contractuels et vous serez en mesure de télécharger le document signé de cette étape");
define('DOWNLOAD_SIGNED_CONTRACT_DOCUMENT_FROM_HERE',"Télécharger document signé de contrat d'ici");
define('CONFIRM_DOCUMENTS_ARE_SIGNED_WELL_AND_UPTO_DATE',"Confirmez documents contractuels signés sont bien et jusqu'à ce jour signé");
define('CLICK_COMPLETE_BUTTON_TO_MARK_THIS_STEP_AS_COMPLETED_BELOW',"Cliquez sur \"Approuver\" bouton pour marquer cette étape comme terminée ci-dessous");
define('THIS_STEP_IS_CONFIRMED_AND_COMPLETED',"Cette étape est confirmée et complétée");
define('INVESTER_WILL_GO_TO_BANK_TO_WIRE_TRANSFER',"% S ira à la banque au virement bancaire ou par virement bancaire électronique, le total ". $funds." montant de% s avec les informations suivantes de compte bancaire.");
define('YOU_WILL_GO_TO_BANK_TO_WIRE_TRANSFER',"S'il vous plaît faire ". $funds."  de% s dans le compte bancaire suivant et fournissent accusé de même dans le prochain l'étape 4 ne.");
define('INVESTOR_WILL_UPLOAD_PAYMENT_RECEIPT_SNAP',"Investisseur télécharger réception de paiement instantanés ou détails de repérage pour le paiement, il / elle a fait");
define('UPON_COMPLETION_OF_THIS_STEP_WILL_BE_CONFIRMED', "À l'issue de cette étape, %s sera confirmé comme investisseur et sera répertorié sous l'onglet Investisseurs à la page de ".$project.". Ainsi que le montant de ".$funds." sera ajouté automatiquement ajouté dans le montant de ".$funds." relevée de %s");
define('NOTES_FROM_INVESTOR', "Maintenant que %s est investisseur juridique dans cette campagne, il est temps d'envoyer des certificats d'actions à l'investisseur par courrier électronique ou e-mail, s'il vous plaît préparer les certificats d'actions et d'écrire note ci-dessous sur la façon dont Vicky Patel recevra les certificats d'actions");
define('NOW_AS_INVESTOR_IS_LEGAL_INVESTOR_IN_THIS_CAMPAIGN', "Maintenant que %s est investisseur juridique dans ce ".$project.", il est temps d'envoyer des certificats d'actions à l'investisseur par courrier électronique ou e-mail, s'il vous plaît préparer les certificats d'actions et d'écrire note ci-dessous sur la façon dont %s recevra les certificats d'actions");

define('RECEIPT_CONFIRMED',"Reçu Confirmé");
define('PAYMENT_INFORMATION',"Information De Paiement");
define('ONCE_ALL_IS_VERIFY_AND_DONE_WILL_APPROVE_AND_CONFIRM',"Une fois que tout est de vérifier et de faire l'équipe% de va approuver et confirmer cette étape");
define('ONCE_THIS_STEP_IS_APPROVED_AND_CONFIRMED',"Une fois que cette étape est approuvé et confirmé s'il vous plaît passez à l'étape 3. Paiement");
define('TEAM_WILL_DOWNLOAD_AND_VERIFY_YOUR_SIGNED',"L'équipe% de va télécharger et vérifier votre contrat signé vous avez téléchargé à partir d'ici");
define('YOUR_NOTE',"Votre note");
define('CLICK_HERE_TO_VIEW_PAYMENT_REC'," Cliquez ici pour voir votre reçu de paiement:");
define('YOU_WILL_SANNOUNCE_SHARE_RECIPT',"vous confirmer et marquer cette étape pour annoncer des certificats d'actions, il / elle a reçues et d'autres documents juridiques.");
define('RECEIVE_SHARES_DOCUMENTS',"Recevoir des actions / Documents");
define('INVESTOR_WILL_CONFIRM_SHARE_DOCUMENTS',"% S confirmer et marquer cette étape pour annoncer des certificats d'actions, il / elle a reçues et d'autres documents juridiques.");
define('NOTE',"Note");
define('APPROVED',"Approuvé");
define('INV_ENTER_YOUR_BANK_AND_CHECK_INFORMATION',"Entrer / Modifier votre banque et vérifier les informations à recevoir% s de l'investisseur.");
define('REJECT_COMMENT',"Rejeter Commentaire");
define('REJECT_REASON',"Entrez raison du rejet.");
define('SENDMESSAGE_TO_SITE_TEAM',"Envoyer un message à l'équipe% de");
define('SENDMESSAGE_TO_INVESTOR_NAME',"Envoyer un message à% s");
define('PROCESS',"Processus");

define('YOUR_PAYMENT_ACKNOWLEDGEMENT_INFORMATION_HAS_BEEN_SUBMITTED_SUCCESSFULLY',"Vos informations d`acquittement de paiement a été soumis avec succès. ");
define('TEAM_WILL_REVIEW_AND_CONFIRM_YOUR_PAYMENT_SHORTLY'," équipe examiner et confirmer votre paiement sous peu.");
define('STEP_2_HAS_BEEN_CONFIRMED_AND_COMPLETED_PROCEED_TO_THE_STEP_3_PAYMENT',"Étape 2. a été confirmé et complété. Passez à l`étape 3. Paiement");
define('STATUS_HAS_BEEN_REJECTED_SUCCESSFULLY',"Statut a été rejetée avec succès");
define('YOURSHARE_DOCUMENT_TRANSMISSION_INFORMATION_SUBMITTED_AND_SENT',"Vos informations de transmission share / document présenté et envoyé");
define('STEP_4_HAS_BEEN_CONFIRMED_AND_COMPLETED_PROCEED_TO_THE_STEP_5_SHARES_DOCUMENTS',"Étape 4. a été confirmé et complété. Passez à l`étape 5. Actions / Documents");	
define('SHARE_DOCUMENT_DELIVERY_HAS_BEEN_CONFIRMED_AND_MARKED_AS_DELIVERED_SUCCESSFULLY',"Livraison Share / Document a été confirmée et marqué comme livré avec succès.");
define('CLICK_HERE_TO_VIEW_YOUR_PAYMENT_RECEIPT',"Cliquez ici pour voir votre reçu de paiement");
define('YOUR_INVESTMENT_DONE_SUCCESSFULLY',"Votre ".$funds." fait avec succès.");

/*follower_lang.php*/
define('FOLLOWING_PROJECTS',"Après ".$project);
	
define('THERE_ARE_NO_FOLLOWING_PROJECT',"Il n'y a pas de ".$project." Suivant.");

/**cron_lang.php*/
define('CRON_YOUR_PAYMENT_PROCESS_VIOLATED_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU',"Hello %s ,<br/><br/>Your payment process is violated %s <br/><br/><br/>Please check your settings. Try again.<br/><br/><br/>Thank You.");

define('CRON_HELLO_ADMINISTRATOR_APIERROR7_PAYMENT_PROCESS_VIOLATED_DUE_TO_ERRORMESSAGE_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU',"Hello Administrator,<br/><br/> APIError(7). Payment process is violated due to %s <br/><br/>Donar Name : %s <br/><br/>Donar Email : %s <br/><br/>Payee Email : %s <br/><br/>Please check your settings. Try again.<br/><br/><br/>Thank You.");

define('CRON_HELLO_ADMINISTRATOR_APIERROR8_PAYMENT_PROCESS_VIOLATED_DUE_TO_ERROR_MESSAGE_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU',"Hello Administrator,<br/><br/> APIError(8). Payment process is violated due to %s <br/><br/>Donar Name : %s <br/><br/>Donar Email : %s <br/><br/>Payee Email : %s <br/><br/>Please check your settings. Try again.<br/><br/><br/>Thank You.");

define('CRON_HELLO_YOUR_PAYMENT_PROCESS_VIOLATED_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU',"Hello %s,<br/><br/>Your payment process is violated. <br/><br/><br/>Please check your settings. Try again.<br/><br/><br/>Thank You.");

define('CRON_HELLO_ADMINISTRATOR_CATCHEXCEPTION8_PAYMENT_PROCESS_VIOLATED_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU',"Hello Administrator,<br/><br/> catchException(8). Payment process is violated.<br/><br/>Donar Name : %s <br/><br/>Donar Email : %s <br/><br/>Payee Email : %s <br/><br/>Please check your settings. Try again.<br/><br/><br/>Thank You.");

define('CRON_HELLO_PAYMENT_PROCESS_NOT_COMPLETED_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU',"Hello %s ,<br/><br/>Your payment process is not completed.<br/><br/><br/>Please check your settings. Try again.<br/><br/><br/>Thank You.");

define('CRON_HELLO_ADMINISTRATOR_APIERROR10_PAYMENT_PROCESS_NOT_COMPLETED_DUE_TO_CASE_IS_NOT_MATCH_WITH_PAYMENTEXECSTATUS_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANK_YOU',"Hello Administrator,<br/><br/> APIError(10). Payment process is not completed due to case is not match with paymentExecStatus. <br/><br/>Donar Name : %s <br/><br/>Donar Email : %s <br/><br/>Payee Email : %s <br/><br/>Please check your settings. Try again.<br/><br/><br/>Thank You.");

/*admin_newsletter_lang.php*/

define('UPLOAD_CSV',"Télécharge CSV");
define('DOWNLOAD_SAMPLE_CSV_FILE',"Télécharger l'exemple de fichier CSV");
define('ADD_NEWSLETTER',"Ajouter Bulletin");
define('ADD_NEWSLETTER_JOB',"Ajouter Bulletin Travail");
define('NEWSLETTER_SETTING',"Bulletin Cadre");
define('ATTACH_FILE',"Pièce Jointe");
define('VIEW_ATTACHMENT',"Afficher la pièce jointe");
define('ALLOW_UNSUBCRIBE_LINK',"Autoriser désabonner Lien");
define('YES',"Oui");
define('NOS',"Non");
define('SUBSCRIBE_TO',"S'abonner");
define('NONES',"Aucun");
define('ALL',"Tous");
define('SELECT_NEWSLETTER',"Choisir Bulletin");
define('SELECT_USER',"Sélectionnez Utilisateur");
define('SELECT',"Sélectionner");
define('SUBSCRIBER',"Abonné");
define('REGISTERED',"Inscrit");
define('BOTH',"Les deux");
define('SUBSCRIBER_USER_LIST',"Abonné Liste des utilisateurs");
define('REGISTER_USER_LIST',"Créer Liste de l'utilisateur");
define('ALL_USER',"Tous les utilisateurs");
define('RECURSIVE',"Récursif");
define('NEWSLETTER_TYPE',"La lettre type");
define('DAILY',"Tous les jours");
define('WEEKLY',"Hebdomadaire");
define('MONTHLY',"Mensuel");
define('DURATION',"Durée");
define('SELECT_A_DAY_OF_WEEK',"Sélectionnez un jour de la semaine");
define('SELECT_A_DAY',"Sélectionnez un jour");
define('SELECT_START_DATE',"Sélectionnez Date de début");
define('SELECT_END_DATE',"Sélectionnez Date de fin");
define('JOB_START_DATE',"Date de début d'emploi");
define('JOB_CREATE_DATE',"Job date de création");
define('TOTAL_SUBSCRIBER',"Abonné total");
define('TOTAL_SEND',"Envoyer total");
define('TOTAL_OPEN',"Ouvrir total");
define('TOTAL_FAIL',"Total Échouer");
define('NEWSLETTERS',"Bulletin");
define('TOTAL_USER',"Totale utilisateur");
define('NEWSLETTER_JOB',"Bulletins d'emploi");
define('REFRES',"Rafraîchir");
define('DRAFT',"Avant-projet");
define('SENT',"Envoyé");
define('START_DATE',"Date De Début");
define('STATISTICS',"Statistiques");
define('SENDS',"Envoyer");
define('OPEN',"Ouvert");
define('CREATE_DATE',"Créer Un Rendez-Vous");
define('NO_RECORDS_FOUND',"Aucun enregistrement trouvé.");
define('IMPORTANT_NOTE_PLEASE_SET_THE_CRON_JOB_ON_YOUR_SERVER_WITH_URL',"Remarque importante: S'il vous plaît définir la tâche cron sur votre serveur avec l'URL.");
define('EXPORT_CSV',"Exporter CSV");
define('IMPORT_CSV',"Importer CSV");
define('FROM_NAME',"De Nom");
define('FROM_EMAIL_ADDRESS',"De Adresse courriel");
define('REPLY_NAME',"Répondre Nom");
define('REPLY_EMAIL_ADDRESS',"Répondre Adresse Email");
define('NEW_SUBSCRIBE_EMAIL',"New abonner Email");
define('UNSUBSCRIBE_EMAIL',"Unsubscriber Email");
define('USER_DEFAULT_NEWSLETTER',"Par défaut utilisation Lettre");
define('SELECTED',"Sélectionné");
define('NO_TEMPLATE',"Pas de modèle");
define('NUMBER_OF_EMAIL_SEND',"Nombre de Email Envoyer");
define('BREAK_BETWEEN_NO_EMAIL_SEND',"Rupture entre No. Email Envoyer");
define('BREAK_TYPE',"Type de Pause");
define('MAILER',"Mailer");
define('SEND_MAIL_PATH',"Envoyer chemin de courrier");
define('IF_MAILER_IS_SENDMAIL',"(Si Mailer est sendmail)");
define('SMTP_PORT',"Port SMTP");
define('SMTP_HOST',"Hôte SMTP");
define('SMTP_EMAIL',"E-mail SMTP");
define('SMTP_PASSWORD',"Mot de passe SMTP");
define('SEND_TEST_MAIL',"Send Test Mail");
define('PHP_MAIL',"PHP Courrier");
define('SMTP',"SMTP");
define('SENDMAIL',"Envoyer Un Mail");
define('EMAIL_SETTING',"Email Cadre");
define('SENDER_EMAIL',"Email Sender");
define('RECEIVER_EMAIL',"Récepteur Email");
define('SENT_MESSAGE_ON_SENDING_A_TEST_MAIL',"Envoyé un message sur l'envoi d'un e-mail de test");

define('WEEKLY_DAY',"Jour de la semaine");
define('MONTHLY_DAY',"Jour mensuel");
define('END_DATE_MUST_BE_GREATER_THAN_START_DATE',"date de fin doit être supérieur à la date de début");
define('FAIL',"Échouer");
define('CONTENT',"Contenu");
define('UPLOAD_CSV_ONLY',"Ajouter csv seulement");
define('NAMES',"nom");
/*admin_user_home_lang.php*/
define('FORGOT_PASSWORD',"Mot De Passe Oublié");
define('FUNDRAISING_SCRIPT_BY',"Collecte de fonds par Script");
define('ROCKERSINFO_ALL_RIGHTS_RESERVED',"rockersinfo.com Tous droits réservés");
define('ENTER_YOUR_EMAIL_ADDRESS_BELOW_TO_RESET_YOUR_PASSWORD',"Entrez votre adresse e-mail ci-dessous pour réinitialiser votre mot de passe.");
define('ERROU_PAGES',"Erreur Page");
define('ERROU_PAGE',"Erreur Page");
define('404',"404");
define('PAGE_NOT_FOUND',"Page Nonn Trouvée");
define('WE_ARE_SOURY_THE_PAGE_YOU_WERE_LOOKING_FOU_DOEST_EXIST_ANYMOUE',"We are sorry, the page you were looking for does not exist anymore.");
define('ADD_USER',"Ajouter un utilisateur");
define('FACEBOOK_PROFILE_URL',"Profil Facebook URL");
define('TWITTER_PROFILE_URL',"Twitter Profil URL");
define('LINKEEDIN_PROFILE_URL',"Linkedln profil URL");
define('GOOGLE_PLUS_PROFILE_URL',"Google Plus Profil URL");
define('BANDCAMP_PROFILE_URL',"BandCamp profil URL");
define('YOUTUBE_PROFILE_URL',"Youtube URL du profil");
define('MYSPACE_PROFILE_URL',"MySpace Profil URL");
define('INVALID_FACEBOOK_PROFILE_URL',"Facebook valide URL du profil");
define('INVALID_TWITTER_PROFILE_URL',"Twitter valide URL du profil");
define('INVALID_LINKEEDIN_PROFILE_URL',"Linkedln non valide URL du profil");
define('INVALID_BANDCAMP_PROFILE_URL',"BandCamp non valide URL du profil");
define('INVALID_YOUTUBE_PROFILE_URL',"Youtube URL valide profil");
define('INVALID_MYSPACE_PROFILE_URL',"MySpace non valide URL du profil");
define('INVALID_GOOGLE_PLUS_PROFILE_URL',"Google Plus valide URL du profil");

define('INACTIVE',"Inactif");
define('ADMIN_TYPE',"Type de Administrateur");
define('ACTIVE',"Actif");
define('SUSPEND',"Suspendre");
define('ADD_SUSPEND_REASON',"Ajouter suspendre raison");
define('UPDATE',"Mise À Jour");
define('GRAPHICAL_REPOUT',"Rapport graphique");
define('USER_LIST',"Liste de l'utilisateur");
define('USER_LOGIN',"Utilisateur En Ligne");
define('ADMIN_LOGIN',"Administrateur Login");
define('PROJECT_CATEGOUIES',"Catégories de projets");
define('PAYMENT_MODULE',"Module de paiement");
define('PAYPAL_ADAPTIVE_GATEWAY',"Paypal Adaptive passerelle");
define('WALLET_GATEWAY',"Wallet passerelle");
define('TRANSACTION',"Transaction");
define('CONTENT_PAGES',"Pages de contenu");
define('PAGES',"Pages");
define('LEARN_MOUE',"Apprendre plus");
define('LEARN_MOUE_CATEGOUIES',"En savoir plus catégories");
define('GLOBALIZATION',"Mondialisation");
define('COUNTRIES',"Pays");
define('OTHER_FEATURE',"Autre caractéristique");
define('CRON_JOBS',"Cron Jobs");
define('MESSAGES',"Messages");
define('STATS',"Statut");
define('COMMON_SETTING',"Param.Commun");
define('COMMISION_SETTING',"Commision Cadre");
define('AFFILIATE_REQUEST',"Demande d'affiliation");
define('WITHDRAW_FUND_REQUEST',"Demande de retrait Fonds");
define('EMAIL_TEMPLATE',"Modèle de courrier électronique");
define('SETTING',"Réglage");
define('SITE',"Site");
define('META',"Meta");
define('GOOGLE',"Google");
define('YAHOO',"Yahoo");
define('IMAGE_SIZE',"Taille de l'image");
define('FILTER',"Filtre");
define('SPAM_SETTING',"Spam Cadre");
define('SPAM_REPOUT',"Spam Rapport");
define('SPAMER',"Spamer");
define('NEWLETTERS',"Bulletins");
define('NEWLETTERS_USERS',"Bulletin Utilisateurs");
define('NEWLETTERS_JOBS',"Bulletin d'emploi");
define('NEWLETTERS_SETTING',"Bulletin Cadre");
define('REPOUT',"Rapport");
define('PROJECT_REPOUT',"Rapport de projet");
define('GENERAL_INFOUMATION',"Informations Générales");
define('NEW_USERS',"Nonuveaux Utilisateurs");
define('PENDING_PROJECTS',"Dans l'attente de Projets");
define('RUNNING_PROJECTS',"Projets en cours");
define('SUCCESSFUL_PROJECTS',"Projets réussis");
define('FAILURE_PROJECTS',"Projets d'échec");
define('LAST',"Dernière");
define('USERNAME',"Nonm d'utilisateur");
define('POSTED_DATE',"Date de publication");
define('NO_RECOUD_FOUND',"Aucun Enregistrement Trouvé");
define('NEW_RUNNING_PROJECTS',"Nonuveaux projets de course");
define('AMOUNT_RAISED',"Somme amassée");
define('COMPLETED_PROJECTS',"Projets achevés");
define('DONATE_AMT',"Donner Amt");
define('EARNING',"Revenus");
define('REMAIN_FUND',"Restez Fonds");
define('FAILED_PROJECTS',"Projets qui ont échoué");
define('NEW_DONOUS',"Nonuveaux donateurs");
define('IP_ADDRESS',"Adresse IP");
define('FUNDS',"Fonds");
define('REQUEST_DATE',"Date De La Demande");
define('NEW_AFFILIATE_WITHDRAW_REQUEST',"Nonuveau partenaire demandes de retrait");
define('REQUEST_IP',"Demande IP");
define('ROCKERSINFO_ALL_RIGHT_RESERVED',"rockersinfo.com Tous droits réservés");
define('YOU_HAVE_1_NEW_NOTIFICATION',"Vous avez %s nouvelle notification(s).");
define('YOU_DONT_HAVE_ANY_NEW_NOTIFICATION',"Vous ne disposez pas de nouvelle notification.");
define('ADMIN',"Administrateur");
define('CRONJOB',"Temps Cronjob");
define('LAST_LOGIN_IP',"Dernière connexion Ip");
define('LOG_OUT',"Se Déconnecter");
define('STEP_1_OF_4',"Étape 1 sur 4");
define('STEP_1',"Étape 1");
define('STEP_2',"Étape 2");
define('STEP_3',"Etape 3");
define('FINAL_STEP',"Étape finale");
define('FILL_UP_STEP_1',"Remplir l'étape 1");
define('FILL_UP_STEP_2',"Remplir l'étape 2");
define('FILL_UP_STEP_3',"Remplir l'étape 3");
define('OCCASIONALLY_WE_MAY_RELEASE_SMALL_UPDATE_IN_THE_FORM_OF_A_PATCH_TO_ONE_OR_SEVERAL_SOURCE_FILE',"Occasionally, we may release small updates in the form of a patch to one or several source file. These source files are regular zip files that contain updated files.");
define('BACK',"Retour");
define('CONTINUE1',"Continuer");
define('NEW_RECORD_HAS_BEEN_ADDED_SUCCESSFULLY',"Nonuveau record a été ajouté avec succès.");
define('RECORD_HAS_BEEN_UPDATED_SUCCESSFULLY',"Enregistrement a été mis à jour avec succès.");
define('STATE',"État");
define('SIGN_UP_IP',"Inscrivez-IP");
define('INACTIVE_USERS',"Les utilisateurs inactifs");
define('AFFILIATE_REQUEST_USERS',"Affilié Demande Utilisateurs");
define('WHY_DO_YOU_WANT_AFFILIATE',"Pourquoi voulez-vous affilié?");
define('PROMOTIANAL_METHOD',"Méthode promotionnelle");
define('RECORD_HAS_BEEN_DELETED_SUCCESSFULLY',"Enregistrement a été supprimé avec succès.");
define('USERS',"Utilisateurs");
define('SIGNUP_IP_ADDRESS',"Adresse IP inscrire");
define('LAST_10_PENDING_PROJECTS',"10 derniers projets en attente");
define('LAST_5_NEWRUNNING_PROJECTS',"Derniers 5 nouveaux projets de course");
define('LAST_5_COMPLETED_PROJECTS',"5 dernières Projets achevés");
define('LAST_5_FAILED_PROJECTS',"5 derniers projets ont échoué");
define('LAST_5_NEW_PROJECTS',"Dernière 10 nouveaux donateurs");
define('NEW_AFFILIATE_WITHDRAW_REQUESTS',"10 Nonuveau partenaire demandes de retrait");
define('LAST_10_NEW_USERS',"Les 10 derniers nouveaux utilisateurs");
define('LAST_5_INACTIVE_USERS',"5 Dernière Utilisateurs inactifs");
define('LAST_5_AFFILIATE_USERS',"5 dernières affiliation Demande Utilisateurs");
define('REGISTERED_ON',"Enregistré Sur");
define('FUNDRAISINGSCRIPT_COM',"Fundraisingscript.com");
define('ERRORS',"Erreur");
define('SOURCE_FILE_NAME',"Source Nonm du fichier");
define('DESTINATION_FILE_NAME',"Destination File Name");
define('NO_UPDATE_FOUND',"Nonn Mise à jour Trouvé");
define('DO_YOU_WANT_TO_UPDATE_YOUR_BACK_UP_AGAIN_THEN_CLICK_HERE',"Voulez-vous mettre à jour votre remonter puis cliquez ici.");
define('ADAPTIVE_PAYPAL_SETTING',"Adaptive Cadre Paypal");
define('SITE_STATUS',"Statut du site");
define('TRANSACTION_TYPE',"Type de transaction");
define('SAND_BOX',"Bac à sable");
define('LIVE',"Vivre");
define('INSTANT',"Instantané");
define('PREAPPROVAL',"Préautorisation");
define('PAYPAL_APPLICATION_ID',"Paypal application Id");
define('PAYPAL_API_USERNAME',"Paypal API Nonm d'utilisateur");
define('PAYPAL_API_PASSWORD',"Paypal API Mot de passe");
define('PAYPAL_API_SIGNATURE',"Paypal API Signature");
define('PAYPAL_EMAIL_ID',"Paypal Email Id");
define('PAYPAL_PAYPAL_FIRST_NAME',"Utilisateur Paypal Prénom");
define('PAYPAL_PAYPAL_LAST_NAME',"Utilisateur Paypal Nonm");
define('PAYPAL_FEES_TAKEN_FROM',"Frais paypal prises de");
define('PROJECT_OWNER',"Projet Propriétaire");
define('FROM_ADDRESS',"De Adresse");
define('REPLY_ADDRESS',"Adresse Répondre");
define('NOTE_IT_WILL_UPDATE_ALL_THE_TEMPLATE_EMAIL_ADDRESS_IF_YOU_TO_CHANGE_ANY_PARTICULAR_EMAIL_ADDRESS_THEN_YOU_CAN_CHANGE_IT_GOING_IN_THAT_GOING_IN_THAT_EMAIL_TEMPLATE',"Remarque: Il mettra à jour toutes les adresses modèle de courriel. Si vous souhaitez modifier l'adresse e-mail particulier, alors vous pouvez changer ce que ça va dans ce modèle de courriel.");
define('SELECT_PROJECT_FOR_TESTING_DONATION',"Sélectionnez ". $project." pour les tests don.");
define('SELECT_PROJECT_FOR_TEST',"Sélectionnez ". $project." pour le test.");
define('ENTER_DONATE_AMOUNT_OU_SELECT_PERK',"Entrez don quantité Ou sélectionnez avantage.");
define('PROJECT_OWNER_HAVE_NOT_VERIFY_PAYPAL_ACCOUNT_PLEASE_ENTER_PAYPAL_ACCOUNT_DETAIL_BELOW_AND_VERIFY',"Projet propriétaire ont pas vérifier compte Paypal. S'il vous plaît entrez compte Paypal détail ci-dessous et à la vérification.");
define('USER_PAYPAL_AC_EMAIL',"Utilisateur Paypal A / C Email");
define('OU',"OU");
define('SELECT_DONAR_USER_FOR_MAKING_DONATION',"Sélectionnez l'utilisateur donar pour faire don.");
define('SELECT_DONAR_USER',"Sélectionnez Donar utilisateur");
define('NOTES',"NOTE");
define('BEFORE_PROCEED_TO_NEXT_STEP_PLEASE_LOGIN_TO_PAYPAL_LOGIN_TO_PAYPAL_ACCOUNT_OTHERWISE_YOU_NEED_COME_BACK_AGAIN_AND_DO_ALL_THE_PROCESS',"Before proceed to next step, Please login to Paypal Account Otherwise you need come back again and do all the process once again.");
define('FOR_SANDBOX_TEST',"Pour Tas de sable Test");
define('DEVELOPER_PAYPAL_COM',"developer.paypal.com");
define('FOR_LIVE_TEST',"Pour l'essai en direct");
define('COUNTRY_NAME',"Nonm Du Pays");
define('MESSAGE',"Message");
define('RECORDS_HAS_BEEN_UPDATED_SUCCESSFULLY',"Record (s) has been updated successfully.");
define('VIEW',"Vue");
define('CURRENCY_NAME',"Nonm de la devise");
define('CURRENCY_CODE',"Code De Devise");
define('CURRENCY_SYMBOL',"Symbole monétaire");
define('CRONJOBS',"Cronjob");
define('SELETE_CRON_JOB',"Emploi Selete Cron");
define('RUN',"Course");
define('LAST_RUN_TIME',"Dernière Run Time");
define('NO_CURRENCIES_FOUND',"Pas de monnaies trouvées.");
define('PAYPAL_ADAPTIVE_PAYMENT_GATEWAY',"Paypal Adaptive passerelle de paiement");
define('SANDBOX',"Tas de sable");
define('IF_ACTIVE_THEN_YOU_HAVE_TO_SET_CRON_JOB_FOR_EVERY_MINUTE_WITH_THIS_URL',"Si active, alors vous devez régler cron pour toutes les 5 minutes avec cette URL.");
define('YOU_HAVE_TO_INACTIVATE_THE_CURRENTLY_ACTIVE_PAYMENT_GATEWAY_TO_ACTIVATE_THIS_PAYEMNT',"Vous avez pour inactiver la passerelle de paiement actuellement actif pour activer cette passerelle de paiement.");
define('PAYMENT_GATEWAY_ACTIVATED_SUCCESSFULLY',"Payment Gateway activé avec succès.");
define('PAYMENT_GATEWAY_INACTIVATED_SUCCESSFULLY',"Payment Gateway inactivé avec succès.");
define('TEST_YOUR_PAYPAL_SETTING',"Testez votre réglage Paypal.");
define('GATEWAY_STATUS',"État de la passerelle");
define('OPTIONS',"Options");
define('TRANSACTIONS',"Transactions");
define('ADD_ADMINISTRATOR',"Ajouter un administrateur");
define('ADMINISTRATOR',"Administrateur");
define('SUPER_ADMINISTRATOR',"Super administrateur");
define('ASSIGN_RIGHTS',"Attribuer des droits");
define('USERS_LOGIN',"Utilisateurs Connexion");
define('RIGHT_NAME',"Nonm droit");
define('CANNOT_DELETE_ALL_THE_ADMIN',"Vous ne pouvez pas supprimer tous Admin. Atleast devrait être actif.");
define('RECORD_HAS_BEEN_DELETED_SUCCESSFULLY_FROM_ADMIN_LOGIN',"Enregistrement a été supprimé avec succès de login admin.");
define('RIGHT_HAS_BEEN_UPDATED_SUCCESSFULLY',"L'homme a été mis à jour avec succès.");
define('ADD_ADMIN',"Ajouter admin");
define('ADMINISTRATOR_MANAGED_TABLE',"Tableau administrateur Managed");
define('RIGHTS',"Droits");
define('LOGIN_DETAIL_HAS_BEEN_DELETED_SUCCESSFULLY',"Les informations de connexion a été supprimé avec succès.");
define('LOGIN_IP_ADDRESS',"Identifiez-vous Adresse IP");
define('LOGIN_DATE',"Date de connexion");
define('LOGIN_TIME',"Durée de connexion");
/*define('REPORT',"Rapport");*/
define('INVALID_USERNAME_OR_PASSWORD',"Nonm d'utilisateur ou mot de passe invalide");
define('YOU_HAVE_LOGGED_OUT_SUCCESSFULLY',"Vous avez enregistré avec succès");
define('PASSWORD_IS_SEND_TO_YOUR_EMAIL_ADDRESS',"Mot de passe est envoyé à votre adresse e-mail");
define('USER_EMAIL_ADDRESS_IS_NOT_FOUND',"Utilisateur Adresse Email est introuvable");
define('USER_RECORD_IS_NOT_FOUND',"enregistrement de l'utilisateur est introuvable");

define('SUPER_ADMIN',"Super administrateur");
define('PROJECT_CATEGORIESS',"Les catégories de projets");
define('LEARN_MOREE',"Apprendre plus");
define('LEARN_MORE_CATEGORIESS',"En savoir plus catégories");
define('DISPLAY',"Exposition");
define('RECORD_PER_PAGE',"Rapports par Page");
define('NOTHING_FOUND_SORRY',"Pour l'instant - désolé");
define('SHOWING',"Projection");
define('RECORD',"Archives");
define('CHANGE_YOUR_PASSWORD',"Changez Votre Mot De Passe");
define('LAST_UPDATE_VERSION',"Dernière mise à jour Version");
define('UPDATED_ON',"Mis à jour le");
define('VERSION',"Version");
define('SEE_ALL_UPDATES',"Voir l'ensemble des mises à jour");
define('RIGHT',"Droit");
define('THERE_IS_AN_EXISTING_ACCOUNT_ASSOCIATED_WITH_THIS_EMAIL_ID',"Il est déjà un compte associé à cette Email ID");
define('THERE_IS_AN_EXISTING_ACCOUNT_ASSOCIATED_WITH_THIS_USERNAME',"Il est déjà un compte associé à ce nom d'utilisateur");
define('YOUR_PASSWORD_HAS_BEEN_UPDATED_SUCCESSFULLY',"Votre mot de passe a été mis à jour avec succès");
define('SIGNATURE',"Signature");
define('SELECT_PROJECT',"Sélectionnez Projet");
define('DONATE_AMOUNT',"Faire un don Montant");
define('COUNTRY_NAME_IS_ALREADY_EXIST',"Le nom du pays est existe déjà");
define('MAILER_TYPE',"Mailer type");
define('SENDMAIL_PATH',"Sendmail Chemin");
define('SENDMAIL_MAIL',"Expéditeur courrier");
define('RECEIVER_MAIL',"Récepteur Courrier");
define('TEST_MESSAGE_SUCCESSFULLY_SENT',"Test Message envoyé avec succès");

define('CREADIT_CARD_VERSION',"Version de carte de crédit");

define('PAYPAL_STATUS',"Paypal Statut");
define('COMMISSION_FEE',"Frais De Commission");
define('ACCESS_KEY_ID',"Access Key Id");
define('SECRET_ACCESS_KEY',"Clé d'accès secrète");
define('TRANSACTION_TYPE_NAME',"Type de transaction Nonm");
define('UPDATE_VERSION',"Update Version");
define('DELETE_ADMINISTRATOR_CONFIRMATION ',"Are you sure,Do you want to delete this record ?");
define('TAXONOMY',"Taxonomie");
define('TAXONOMY_SETTING',"réglage de la taxonomie");
define('PROJECT_NAME',"Nonm du projet");
define('PROJECT_URL',"URL de projet");
define('PROJECT_FUNER',"bailleur de fonds du projet");
define('FOLLOWER',"Disciple");
define('USER',"Utilisateur");
define('NO',"Non");
define('EDIT_USER',"Modifier l'utilisateur");
define('GENERAL_INFORMATION',"Informations Générales");
 define('NO_RECORD_FOUND',"Aucun Enregistrement Trouvé");
define('GRAPHICAL_REPORT',"Rapport graphique");
define('SPAM_REPORT',"Spam Report");

define('SORRY_YOUR_PREAPPROVAL_PAYMENT_PROCESS_IS_VIOLATED_DUE_TO_FOLLOWING_ERROR',"désolé..!  Votre processus de paiement pré approuvé est violé en raison de l`erreur suivante");
define('SORRY_YOUR_PAYMENT_PROCESS_IS_VIOLATED_DUE_TO_ERROR_IN_PAYRECIEPT_METHOD_PLEASE_CHECK_YOUR_SETTING_AND_TRY_ONCE_AGAIN',"désolé...! Votre processus de paiement est violé en raison d`une erreur dans PayReciept Méthode. S`il vous plaît vérifier vos paramètres et essayez à nouveau.");
define('PREAPPROVAL PROCESS IS VIOLATED DUE TO PREAPPROVAL KEY ALREADY EXISTS IN THE SYSTEM. THIS HAPPEN DUE TO 2 REASON',"désolé...! Votre processus d`approbation préalable est violé en raison de préqualification clé existe déjà dans le système. Cela arrive en raison de 2 raison.<br/><br/>1). En raison de la faible connectivité Internet.<br/>2). Paypal Envoyer réponse deux fois pour une même transaction. Vérifiez le montant du don manuellement projet et nouvelle entrée des donateurs.");
define('SORRY_YOUR_PREAPPROVAL_PROCESS_IS_VIOLATED_DUE_TO_ERROR_OCCURRED_IN_PAYMENTDETAILS_METHOD_PLEASE_CHECK_YOUR_SETTING_AND_TRY_ONCE_AGAIN',"désolé...! Votre processus d`approbation préalable est violé en raison de l`erreur a eu lieu dans les détails de paiement méthode. S`il vous plaît vérifier vos paramètres et essayez à nouveau");
define('SORRY_YOUR_INSTANT_PAYMENT_PROCESS_FAILED_DUE_TO_ERROR',"désolé...! Votre processus de paiement instantanée a échoué en raison d`une erreur");
define('SORRY_YOUR_INSTANT_PAYMENT_PROCESS_FAILED_DUE_TO_ERROR_OCCURRED_IN_PAYMENTDETAILS_METHOD_PLEASE_CHECK_YOUR_SETTING_AND_TRY_ONCE_AGAIN',"désolé...! Votre processus instantanée de paiement échoué en raison d`erreur est survenue dans les détails de paiement méthode. S`il vous plaît vérifier vos paramètres et essayez à nouveau");
define('SORRY_YOUR_INSTANT_PAYMENT_PROCESS_IS_VIOLATED_DUE_TO_PAYPAL_AP_PAYPAL_NEW_DONOR_ENTRY',"désolé...! Votre Processus instantanée de paiement est violé en raison de Paypal clé API existe déjà dans le système. Cela arrive en raison de la raison 2.<br/><br/>1).En raison de la faible connectivité Internet.<br/>2). Paypal Envoyer réponse deux fois pour une même transaction. Vérifiez le montant du don manuellement projet et nouvelle entrée des donateurs.");
define('SORRY_YOUR_PREAPPROVAL_PAYMENT_PROCESS_IS_VIOLATED_DUE_TO_ERROR_IN_PAYMENTDETAILS_METHOD_PLEASE_CHECK_YOURSETTING_AND_TRY_ONCE_AGAIN',"désolé..!  Votre processus de paiement pré approuvé est violé en raison d`une erreur dans les détails de paiement méthode. S`il vous plaît vérifier vos paramètres et essayez à nouveau.");
define('SORRY_YOUR_PAYMENT_PROCESS_IS_VIOLATED_DUE_TO_ERROR_IN_PAYMENTEXECSTATUS_PLEASE_CHECK_YOUR_SETTING_AND_TRY_ONCE_AGAIN',"désolé...! Votre processus de paiement est violé en raison d`une erreur dans le paiement Exec Statut. S`il vous plaît vérifier vos paramètres et essayez à nouveau.");
define('PLEASE_CHECK_YOUR_SETTINGS_AND_TRY_ONCE_AGAIN',"S`il vous plaît vérifier vos paramètres et essayez à nouveau");
define('YOUR_PAYMENT_PROCESS_IS_SUCCESSED_ON_PROJECT',"Votre processus de paiement est le succès dans le".$project);
define('BELOW_IS_PAYPAL_RESPONSE',"Réponse ci-dessous est Paypal");
define('ARE_YOU_SURE_YOU_WANT_TO_ACTIVE_SELECTED_COUNTRY',"Êtes-vous sûr, vous voulez pays actif sélectionné?");
define('ARE_YOU_SURE_YOU_WANT_TO_INACTIVE_SELECTED_COUNTRY',"Êtes-vous sûr, vous voulez inactif pays sélectionné?");

define('RUNNING_INVESTMENT',"investissement courir");
define('CREATED_DATE',"date de création");
define('INVESTOR_ACTION',"IAction de l`investisseur");
define('OWNER_ACTION',"Propriétaire d'action");

/*admin_setting_lang.php*/

define('EMAIL_SETTING_UPDATED_SUCCESSFULLY',"Paramètres de messagerie correctement mis à jour.");

define('FACEBOOK_SETTING',"Cadre Facebook");

define('FACEBOOK_SETTING_UPDATED_SUCCESSFULLY',"Paramètres Facebook mis à jour avec succès.");

define('FACEBOOK_SETTING_YOU_HAVE_ENTERED_ARE_VRAI',"Facebook réglages que vous avez saisis sont vraies.");

define('FACEBOOK_PROFILE_FULL_URL',"Profil Facebook URL complet");

define('FACEBOOK_APPLICATION_ID',"Application Facebook ID");

define('FACEBOOK_LOGIN_ENABLE',"Facebook Connexion Activer");

define('FACEBOOK_APPLICATION_API_KEY',"Application Facebook clé API");

define('FACEBOOK_APPLICATION_SECRET_KEY',"Application Facebook Secret Key");
define('ALLOW_POST_ON_FACEBOOK_WALL',"Autoriser Poster sur Facebook mur");
define('FACEBOOK_ACCESS_TOKEN',"Facebook jeton d'accès");
define('FACEBOOK_USER_ID',"Facebook ID de l'utilisateur");
define('FACEBOOK_IMAGE',"Facebook Image");
define('WANT_TO_CHANGE_FACEBOOK_DETAILS',"Vous voulez changer Facebook Détails");
define('CONNECT_TO_FACEBOOK',"Connectez-vous à Facebook");
define('FILTER_SETTING',"Configuration du filtre");
define('ROUNDING_OFF_DAYS',"Arrondir (Jours)");
define('POPULAR_PER',"Populaire (%)");
define('MOST_FUNDED_PER',"La plupart Financé (%)");
define('SUCCESS_STORIES_PER',"Histoires de réussite (%)");
define('FILTER_SETTING_UPDATED_SUCCESSFULLY',"Les paramètres de filtrage mis à jour avec succès.");
define('GOOGLE_SETTING',"Paramètre Google");
define('GOOGLE_SETTING_UPDATED_SUCCESSFULLY',"Google paramètres mis à jour avec succès.");
define('GOOGLE_ENABLED',"Google Permettred");
define('CONSUMER_KEYS',"Key à la consommation");
define('CONSUMER_SECRETS',"Consumer secret");
define('IMAGE_SETTING',"réglage de l'image");
define('PROJECT_THEUMBNAIL_WIDTH', $project." Miniature Largeur");
define('PROJECT_THEUMBNAIL_HEIGHT', $project." Miniature Hauteur");
define('PROJECT_THEUMBNAIL_ASPECT_RATIO', $project." Aspect Ratio Miniature");
define('TRUE',"VRAI");
define('FALSE',"FAUX");
define('PROJECT_SMALL_WIDTH', $project." Petit Largeur");
define('PROJECT_SMALL_HEIGHT', $project." Petite Taille");
define('PROJECT_MEDIUM_WIDTH', $project." Moyen Largeur");
define('PROJECT_MEDIUM_HEIGHT', $project." Taille Moyenne");
define('PROJECT_LARGE_WIDTH', $project." Grande Largeur");
define('PROJECT_LARGE_HEIGHT', $project." Grande Hauteur");
define('USER_SMAIL_WIDTH',"Utilisateur Petit Largeur");
define('USER_SMAIL_HEIGHT',"Utilisateur Petite Taille");
define('USER_MEDIUM_WIDTH',"L'utilisateur moyen Largeur");
define('USER_MEDIUM_HEIGHT',"Utilisateur hauteur moyenne");
define('USER_BIG_WIDTH',"Utilisateur Big Largeur");
define('USER_BIG_HEIGHT',"Utilisateur Big Hauteur");
define('USER_THUMBAIL_ASPECT_RATIO',"Utilisateur Miniature Aspect Ratio");
define('IMAGE_SIZE_SETTING_UPDATED_SUCCESSFULLY',"réglages de taille d'image mis à jour avec succès.");
define('PROJECT_GALLERY_THUMBNAIL_ASPECT_RATIO', $project." Galerie Thumbnail Aspect Ratio");
define('LINKEDIN_SETTING',"Paramètre Linkedin");
define('LINKEDIN_SETTING_UPDATED_SUCCESSFULLY',"Linkedin paramètres mis à jour avec succès.");
define('LINKEDIN_PROFILE_FULL_URL',"Linkdin URL du profil complet");
define('LINKEDIN_ENABLED',"Linkedin Permettred");
define('LINKEDIN_ACCESS',"Linkdin clé API");
define('LINKEDIN_SECRET',"Linkdin Secret Key");
define('MESSAGE_SETTING',"Cadre message");
define('MESSAGE_SETTING_UPDATED_SUCCESSFULLY',"Paramètres de messages mis à jour avec succès.");
define('SEND_EMAIL_TO_ADMIN_ON_NEW_MESSAGE',"Envoyer un email à admin sur un nouveau message.");
define('ENABLE',"Permettre");
define('DISABLE',"Désactiver");
define('DEFAULT_MESSAGE_SUBJECT',"Par défaut l'objet du message");
define('MESSAGE_MODULE',"Module de message");
define('META_SETTING',"Paramètre Meta");
define('META_SETTING_UPDATED_SUCCESSFULLY',"Meta paramètres mis à jour avec succès.");
define('SITE_SETTING',"paramètre du site");

define('SITE_SETTING_UPDATED_SUCCESSFULLY',"Les paramètres du site mis à jour avec succès.");
define('SITE_VERSION',"Site Version");

define('CAPTCHA_SETTING',"Paramètres Captcha");

define('CONTACT_US_PAGE',"Contact us");

define('SIGN_UP_PAGE',"Inscrivez-vous la page");

define('CREATE_A_RECAPTCHA_KEY_FOR_YOUR_SITE_FROM_HERE_AND_SET_THE_KEY_VALUES_BELOW',"Créez une clé reCAPTCHA pour votre site à partir d'ici et définir les valeurs clés ci-dessous.");
define('PRIVATE_KEY',"Clé privée");
define('PUBLIC_KEY',"À clé publique");
define('LOGO_SETTING',"Logo Cadre");
define('SITE_LOGO',"Site Logo");
define('SITE_LOGO_HOVER',"Site Logo Hover");
define('FAVICON_IMAGE', 'Favicon Image');
define('LANGUAGE_AND_DATE_TIME_SETTING',"Langue et de la date Réglage de l'heure");
define('SITE_LANGUAGE',"Langue du site");
define('DATE_FORMAT',"Format De Date");
define('TIME_FORMAT',"Format De L'Heure");
define('SITE_TIME_ZONE',"Durée du site Zone");
define('FEED_SETTINGS',"Réglages alimentation");
define('ENABLE_FACEBOOK_STREAM',"Activer Facebook flux");
define('ENABLE_TWITTER_STREAM',"Activer Twitter Flux");
define('PROJECT_LIMITS', $project." Limites");
define('MAXIMUM_NO_PROJECT_PER_YEAR_FOR_ONE_USER',"Nombre maximum de ". $project." par an (pour une utilisationr)");
define('MAXIMUM_NO_DONATION_PER_PROJECT',"Nombre maximum de dons par ". $project."  (Pour une utilisationr)");
define('PROJECT_GOAL_AMOUNT_SETTING', $project." Objectif Montant Cadre");
define('MINIMUM_PROJECT_GOAL_AMOUNT', $project." Minimum Objectif Montant");

define('MAXIMUM_PROJECT_GOAL_AMOUNT', $project." Maximum Objectif Montant");

define('PROJECT_TARGET_DAYS_SETTING', $project." Jours cibles Cadre");

define('MINIMUM_DAYS_FOR_PROJECTS',"Nombre de jours minimum pour ".$project);

define('MAXIMUM_DAYS_FOR_PROJECTS',"Jours maximum pour ".$project);

define('DONATION_AMOUNT_SETTING',"Montant du don Cadre");

define('MINIMUM_DONATION_AMOUNT',"Montant du don minimum");

define('MAXIMUM_DONATION_AMOUNT',"Montant maximal des dons");

define('PAYPAL_RESTRICTION_FOR_MAXIMUM_DONATION_AMOUNT',"(REMARQUE: restriction Paypal pour quantité don maximum est de 2000 $ USD Définir limite que votre taux de conversion de devises..)");

define('REWARD_AMOUNT_SETTING',"Récompense (Perk) Amount Setting");

define('PERK_ENABLE_DISABLE',"Perk Activer / Désactiver");

define('MINIMUM_PERK_AMOUNT',"Récompense minimum (Perk) Amount");

define('MAXIMUM_PERK_AMOUNT',"Récompense Maximum (Perk) Amount");
define('DONATION_TYPE_SETTING',"Type de don Cadre");
define('PAYMENT_GATEWAY',"passerelle de paiement");
define('PAYMENT_ADAPTIVE',"Paypal Adaptive");
define('WALLET',"Portefeuille");
define('PROJECT_ACHIVE_GOAL_AUTO_PREAPPROVAL', $project." Atteindre l'objectif Auto Préautorisation");
define('NOTE_NO_MEAN_PREAPPROVED_DOANTION_AMOUNT_TRANSFER_ON_PROJECT_END_DATE',"(REMARQUE: NO signifie transfert de montant du don préapprouvés sur date.Yes finaux moyens de transfert de montant du don préapprouvés quand ".$project." A atteint son montant objectif cible avant la date de fin ".$project.")");
define('FUNDING_DONATION_TYPE_FOR_PROJECTS',"Type de financement du / de don pour ". $project);
define('FIXED',"Fixé");
define('FLEXIBLE',"Flexible");
define('NOTE_WORK_ONLY_FOR_PREAPPROVAL_IS_ENABLE',"(REMARQUE:".$project." ne fonctionne que pour Préautorisation est Activer Par défaut Financement / Donation Type de l'est fixe.)");
define('FIXED_PROJECT_COMMISION',"Fixe ". $project." Commission (%)");
define('FLEXIBLE_SUCCESSFUL_PROJECT_COMMISION',"Flexible ". $project." Réussie Commission (%)");
define('FLEXIBLE_UNSUCCESSFUL_PROJECT_COMMISION',"Flexible ". $project." Échec Commission (%)");
define('PERSONAL_INFORMATION_SETTING',"Réglage des renseignements personnels");
define('ENABLE_PERSONAL_INFORMATION',"Activer renseignements personnels");
define('TWITTER_SETTING',"Paramètre Twitter");
define('TWITTER_SETTING_UPDATED_SUCCESSFULLY',"Twitter paramètres mis à jour avec succès.");
define('TWITTER_SETTING_YOU_HAVE_ENTERED_ARE_TRUE',"Twitter paramètres que vous avez saisis sont vraies.");
define('TWITTER_PROFILE_FULL_URL',"Twitter URL du profil complet");
define('TWITTER_ENABLE',"Twitter activé");
define('ALLOW_POST_ON_TWITTER',"Autoriser Poster sur Twitter");
define('ACCESS_TOKEN_SECRET',"Accès secret jeton");
define('ACCESS_TOKEN',"Jeton d'accès");
define('TWIITER_IMAGE',"Twitter image");
define('WANT_TO_CHANGE_TWITTER_DETAILS',"Vous voulez changer Twitter Détails");
define('CONNECT_TO_TWITTER',"Connectez-vous à Twitter");
define('ADD_TWITTER_DETAIL',"Ajouter Twitter Détails");
define('YAHOO_SETTING',"Paramètre Yahoo");
define('YAHOO_SETTING_UPDATED_SUCCESSFULLY',"Yahoo paramètres mis à jour avec succès.");
define('YAHOO_ENABLED',"Yahoo Enabled");
define('YOUTUBE_SETTING',"Paramètre Youtube");
define('YOUTUBE_PROFILE_FULL_URL',"Profil Youtube URL complète");
define('YOUTUBE_ENABLED',"Youtube Activer");
define('YOUTUBE_SETTING_UPDATED_SUCCESSFULLY',"Youtube paramètres mis à jour avec succès.");
define('GOOGLE_PLUS_SETTING',"Google plus Activer");
define('GOOGLE_PLUS_SETTING_UPDATED_SUCCESSFULLY',"Google, plus les paramètres mis à jour avec succès.");
define('GOOGLE_PROFILE_FULL_URL',"Google plus url profil complet");
define('GOOGLE_PLUS_ENABLED',"Google, plus de permettre");
define('PROJECT_ENDING_SOON_SETTING',$project." se terminant bientôt mise");
define('PROJECT_ENDING_SOON_DAYS',$project." finissant bientôt jours");

define('HOME_SLIDER',"Sélectionnez ce que vous souhaitez afficher");

define('DYNAMIC_SLIDER',"Bannières statiques");

define('MANAGE_SLIDER',"Gérer Sliders");

define('ADD_SLIDER',"Ajouter une glissière");

define('BRIEF',"Bref");

define('SLIDER_IMAGE',"Curseur image");

define('BUTTON_TITLE',"Bouton Titre");

define('BUTTON_LINK',"Bouton Lien");

define('EDIT_SLIDER',"Modifier Curseur");

define('BANNER',"Bannière");

define('TO_GO_MANAGE_IMAGE',"d'aller et de gérer des bannières d'images en coulisse.");

define('TO_GO_MANAGE_FEATURED',"d'aller et de gérer sélectionnée ".$project.".");

define('TO_GO_MANAGE_ORDER_OF_IMAGE',"d'aller et de gérer afin de bannières d'images en coulisse.");

define('TO_GO_MANAGE_ORDER_OF_FEATURED',"d'aller et de gérer afin de vedette ".$project." à curseur.");


define('SLIDER_SETTING',"Curseur tri");

define('HOME_BANNER_SLIDER',"Accueil Bannière Curseur");

define('BANNER_SLIDER',"Bannière Curseur");

define('SOCIAL_SETTING',"Cadre social");

define('OTHER_SETTING',"Autre Cadre");

define('DROPDOWN_MANAGE',"Gérer Dropdown");

define('TAXONOMY_SETTING_UPDATED_SUCCESSFULLY',"les paramètres de la taxonomie actualisée avec succès.");

define('SINGULAR',"Singulier");

define('PLURAL',"Pluriel");

define('PAST_PARTICIPATE',"Passé Participer");

define('GERUND',"Gérondif");

define('PROJECT_NAME_SINGULAR',"Nom du projet Singulier");

define('PROJECT_NAME_PLURAL',"Nom du projet Pluriel");

define('PROJECT_OWNER_SINGULAR',"Projet Singular Propriétaire");

define('PROJECT_OWNER_PLURAL',"Projet Plural Propriétaire");

define('FUNDS_SINGULAR',"Fonds Singular");

define('FUNDS_PLURAL',"Fonds Pluriel");

define('FUNDS_PAST_PARTICIPATE',"Fonds passées Participer");

define('FUNDS_GERUND',"Fonds Gerund");




define('COMMENTS_SINGULAR',"Commentaires Singular");

define('COMMENTS_PLURAL',"Commentaires Pluriel");

define('COMMENTS_PAST_PARTICIPATE',"Commentaires passées Participer");

define('COMMENTS_GERUND',"Commentaires Gerund");



define('UPDATES_SINGULAR',"Mises à jour Singulier");

define('UPDATES_PLURAL',"Mises à jour Pluriel");

define('UPDATES_PAST_PARTICIPATE',"Mises à jour passées Participer");

define('UPDATES_GERUND',"Mises à jour Gerund");



define('FOLLOWER_SINGULAR',"Abonnements Singular");

define('FOLLOWER_PLURAL',"Abonnements Pluriel");

define('FOLLOWER_PAST_PARTICIPATE',"Abonnements passées Participer");

define('FOLLOWER_GERUND',"Abonnements Gerund");

define('PREVIOUS',"Précédent");


define('PROJECT_UPDATES',  $project." Mises à jour");


define('PROJECT_PERK',  $project." Perk");

define('CLAIMED_PERK',"Perk Déclaré");

define('NO_PERK'," Aucune Perk");

define('PROJECT_DONATION', $project." Don");

define('PROJECT_COMMENT', $project." Commentaires");

define('PROJECT_LIST',  $project."  Liste");

define('ADD_TAGS',"Ajouter Des Balises");

define('PROJECT_TAGS',  $project." Mots clés");

define('ADD_PROJECT_TAGS',"Ajouter ". $project." Mots clés");

define('TAGS',"Mots clés");

define('TAGS_LIST',"Liste Tags");

define('INTEREST_TAGS_LIST',"Liste intérêt Mots-clés");

define('SKILL_TAGS_LIST',"Compétences Liste Tags");

define('USER_NAME',"Nom d'utilisateur");

define('TRANSACTIONO_REPORT',"Transactions Rapport");

define('EARNED',"Earned");

define('TRANSACTION_ID',"Identifiant de transaction");

define('ADD_PROJECT_CATEGORY',"Ajouter ". $project." Catégorie");

define('PROJECT_CATEGORY',  $project." Catégorie");

define('PARENT_CATEGORY',"Catégorie parente");

define('MAIN_CATEGORY',"Catégorie principale");

define('PROJECT_CATEGORY_NAME', $project." Nom De Catégorie");

define('TAGS_NAME',"Nom d'intérêt");

define('GOAL_AMOUNT',"Objectif Montant");

define('RAISED_AMOUNT',"Somme amassée");

define('VIEW_COMMENTS',"Voir Commentaires");

define('HAS_BEEN_DELETED_SUCCESSFULLY',"a été supprimé avec succès.");

define('YOU_CAN_NOT_DELETE_ACTIVE_PROJECT',"Vous ne pouvez pas supprimer actif ".$project);

define('HAS_BEEN_ACTIVATED_SUCCESSFULLY',"a été activé avec succès.");

define('HAS_BEEN_APPROVED_SUCCESSFULLY',"a été approuvé avec succès.");

define('YOU_CAN_NOT_ACTIVE_EXPIRED_OR_ALREADY_ACTIVE',"Vous ne pouvez pas actif expiré ou déjà actif , le succès ou l`échec ".$project.".");

define('YOU_CAN_NOT_INACTIVE_EXPIRED_OR_ALREADY_ACTIVE',"Vous ne pouvez pas inactive expiré ou déjà inactive , le succès ou l`échec ".$project.".");


define('HAS_BEEN_INACTIVATED_SUCCESSFULLY',"a été inactivé avec succès.");

define('HAS_BEEN_DECLIENED_SUCCESSFULLY',"a été décliné avec succès.");

define('CANNOT_BE_DECLIENED',"ne peut pas être diminuer");

define('HAS_BEEN_FEATURED_SUCCESSFULLY',"a été présenté avec succès.");

define('HAS_BEEN_ACTIVATED_AND_SET_FEATURED_SUCCESSFULLY',"a été activé et présenté ensemble avec succès.");

define('CANNOT_BE_SET_TO_FEATURED_AS_IT_IS_EXPIRED_SUCCESSFUL_FAILURE',"cannot be set to featured as it is expired, successful or failure");

define('HAS_BEED_REMOVE_FROM_FEATURED_SUCCESSFULLY',"a été présenté avec succès à partir de supprimer");

define('NEW',"Nouveau");

define('NEWS',"Nouveau");

define('HAS_BEEDN_ADDED_SUCCESSFULLY',"a été ajouté avec succès");

define('HAS_BEEDN_UPDATED_SUCCESSFULLY',"a été mis à jour avec succès");

define('NEW_RECORDS_HAS_BEEN_ADDED_SUCCESSFULLY',"Nouvel Enregistrement (s) has been added successfully.");

define('DECLIENED',"Déclin");

define('NOT_FEATURED',"Non vedette");

define('ADD_CATEGORIES',"Ajouter des catégories");

define('CATEGORIES_TYPE',"Catégorie type");

define('ADD_AMOUNT',"Ajouter la quantité");

define('WHERE_TO_DISPLAY_CURRENCY_SYMBOL',"Où afficher Symbole monétaire?");

define('DECIMAL_POINTS_AFTER_AMOUNT',"Décimales après Montant"); 

define('ARE_YOU_SURE_YOU_WANT_TO_DELETE_SELECTED_PROJECT',"Are you sure, You want to delete selected ".$project."(s)?");

define('ARE_YOU_SURE_YOU_WANT_TO_ACTIVE_SELECTED_PROJECT',"Are you sure, You want to active selected ".$project."(s)?");

define('ARE_YOU_SURE_YOU_WANT_TO_APPROVE_SELECTED_PROJECT',"Are you sure, You want to approve selected ".$project."(s)?");

define('ARE_YOU_SURE_YOU_WANT_TO_DECLINE_SELECTED_PROJECT',"Are you sure, You want to declined selected ".$project."(s)?");

define('ARE_YOU_SURE_YOU_WANT_TO_FETAUTE_SELECTED_PROJECT',"Are you sure, You want to Featured selected ".$project."(s)?");

define('ARE_YOU_SURE_YOU_WANT_TO_REMOVE_FEATURE_SELECTED_PROJECT',"Are You sure, you want to Remove Featured selected ".$project."(s)?");


define('ARE_YOU_SURE_YOU_WANT_TO_DELETE_SELECTED_RECORD',"Are you sure, You want to delete selected record(s)?");

define('ARE_YOU_SURE_YOU_WANT_TO_ACTIVE_SELECTED_RECORD',"Are you sure, You want to active selected record(s)?");

define('ARE_YOU_SURE_YOU_WANT_TO_INACTIVE_SELECTED_RECORD',"Are you sure, You want to inactive selected record(s)?");

define('ARE_YOU_SURE_YOU_WANT_TO_SPAMMER_SELECTED_RECORD',"Are you sure, You want to spamer selected record(s)?");

define('ARE_YOU_SURE_YOU_WANT_TO_REMOVE_ACTIVE_SELECTED_CURRENCIES',"Are you sure, You want to active selected currencies?");

define('ARE_YOU_SURE_YOU_WANT_TO_REMOVE_INACTIVE_SELECTED_CURRENCIES',"Are you sure, You want to inactive selected currencies?");

define('AMOUNT_SETTING_UPDATED_SUCCESSFULLY',"Montant paramètres mis à jour.");



define('PROJECT_SMALL_IMAGE_HEIGHT',  $project." Petit Hauteur de l'image");

define('PROJECT_MEDIUM_IMAGE_WIDTH', $project." Moyen Largeur de l'image");

define('PROJECT_MEDIUM_IMAGE_HEIGHT', $project." Image en hauteur moyenne");

define('PROJECT_LARGE_IMAGE_WIDTH', $project." Grande Largeur de l'image");

define('PROJECT_LARGE_IMAGE_HEIGHT', $project." Grande Hauteur de l'image");

define('PROJECT_SMALL_IMAGE_WIDTH',"Utilisateur Petit Largeur de l'image");

define('USER_SMALL_IMAGE_HEIGHT',"Utilisateur Petit Hauteur de l'image");

define('USER_BIG_IMAGE_WIDTH',"Utilisateur Big Largeur de l'image");

define('USER_BIG_IMAGE_HEIGHT',"Big utilisateur Hauteur de l'image");

define('USER_MEDIUM_WIDTH_IMAGE_WIDTH',"L'utilisateur moyen Largeur de l'image");

define('USER_MEDIUM_WIDTH_IMAGE_HEIGHT',"Utilisateur Moyen Hauteur de l'image");

define('PROJECT_THUMB_IMAGE_WIDTH',  $project." Thumb Largeur de l'image");

define('PROJECT_THUMB_IMAGE_HEIGHT', $project." Thumb Hauteur de l'image");


define('PROJECT_THUMBNAIL_WIDTH_SHOULD_BE_GREATE_THAN_ZERO', $project." Miniature Largeur devrait être greator à zéro");

define('PROJECT_THUMBNAIL_HEIGHT_SHOULD_BE_GREATE_THAN_ZERO', $project." Hauteur vignette doit être greator à zéro");


define('PROJECT_SMALL_WIDTH_SHOULD_BE_GREATER_THAN_ZERO', $project." Petit Largeur devrait être greator à zéro");

define('PROJECT_SMALL_HEIGHT_SHOULD_BE_GREATER_THAN_ZERO', $project." Petit Hauteur devrait être greator à zéro");

define('PROJECT_MEDIUM_WIDTH_SHOULD_BE_GREATER_THAN_ZERO', $project." Largeur moyenne devrait être greator à zéro");

define('PROJECT_MEDIUM_HEIGHT_SHOULD_BE_GREATER_THAN_ZERO', $project." Hauteur moyenne devrait être greator à zéro");

define('PROJECT_LARGE_WIDTH_SHOULD_BE_GREATER_THAN_ZERO', $project." Grande Largeur doit être greator à zéro");

define('PROJECT_LARGE_HEIGHT_SHOULD_BE_GREATER_THAN_ZERO', $project." Grande Hauteur devrait être greator à zéro");

define('USER_SMALL_WIDTH_SHOULD_BE_GREATER_THAN_ZERO',"Utilisateur faible largeur devrait être greator à zéro");

define('USER_SMALL_HEIGHT_SHOULD_BE_GREATER_THAN_ZERO',"Utilisateur faible hauteur devrait être greator à zéro");

define('USER_MEDIUM_WIDTH_SHOULD_BE_GREATER_THAN_ZERO',"Utilisateur largeur moyenne devrait être greator à zéro");

define('USER_MEDIUM_HEIGHT_SHOULD_BE_GREATER_THAN_ZERO',"moyenne de l'utilisateur Hauteur devrait être greator à zéro");

define('PROJECT_BIG_WIDTH_SHOULD_BE_GREATER_THAN_ZERO',  $project." Big Largeur devrait être greator à zéro");

define('PROJECT_BIG_HEIGHT_SHOULD_BE_GREATER_THAN_ZERO',  $project." Big Hauteur devrait être greator à zéro");


define('NOT_AVAILBLE',"Indisponible");

define('PAGE_TITLE',"Titre De La Page");

define('FUND_GOAL',"Objectif du fonds");

define('PROJECT_ADDRESS',  $project." Adresse");

define('PERK_TOTAL',"Perk total");

define('REPLY_HAS_BEEN_ADDED_SUCCESSFULLY',"Répondre a été ajouté avec succès");

define('REASON',"Raison");

define('PLEASE_ENTER_AMOUNT_GREATER_THAN_ZERO',"S'il vous plaît entrez le montant greator à zéro");

define('DEDUCT_FUND',"Fonds Déduire");

define('AMOUNT_SHOULD_BE_GREATER_THAN_ZERO',"Montant devrait être greator à zéro");

define('PREAPPROVAL_ENABLE',"Préautorisation Activer");


define('PROJECT_ENTER_MAXIMUM_PROJECT_GOAL_AMOUNT_GREATER_THAN_MINIMUM_PROJECT_GOAL_AMOUNT',"S'il vous plaît entrer ".$project." Maximum Objectif Montant minimum supérieur à ".$project." Objectif Montant");

define('PROJECT_ENTER_MAXIMUM_DAYS_FOR_PROJECT_GREATER_THAN_MINIMUM_DAYS_FOR_PROJECT',"S'il vous plaît entrer jours maximum pour ".$project." plus de jours minimum pour ".$project);


define('PLEASE_ENTER_MAXIMUM_DONATION_AMOUNT_GREATER_THAN_MINIMUM_DONATION_AMOUNT',"S'il vous plaît entrer Montant maximal du don supérieur à minimum Montant du don");


define('PLEASE_ENTER_MAXIMUM_MAXIMUM_REWARD_AMOUNT_GREATER_THAN_MINIMUM_REWARD_AMOUNT',"S'il vous plaît entrer récompense maximale (Perk) Amount greater than Minimum Reward(Perk) Amount");


define('PLEASE_ENTER_MINIMUM_REWARD_AMOUNT_GREATER_THAN_DONATION_AMOUNT',"S'il vous plaît entrez le montant minimum de récompense plus grande que le montant du don");

define('PLEASE_ENTER_MAXIMUM_REWARD_AMOUNT_LESS_THAN_DONATION_AMOUNT',"S'il vous plaît entrez le montant de la récompense maximale inférieure montant du don");

define('ENDING_SOON',"Ending Soon");


define('VERSIONS',"Versions");


define('ADD',"Ajouter");


define('BANK_NAME',"Nom De Banque");

define('SAMPLE_FILE',"Exemple de fichier");

define('ACCREDENTIAL_DOCUMENT',"Document de Accredential");

define('CONTRACT_COPY_UPLOAD_BY',"Qui va gérer contrat avec des investisseurs?");

define('OTHER_SETTING_UPDATED_SUCCESSFULLY',"Autres paramètres mis à jour avec succès.");

define('FOR_ACCREDENTIAL_DOCUMENT',"Pour document Accredential");
define('SUCCESS_THEMES_HAS_BEEN_UPDATED_SUCCESSFULLY',"Succès!Thèmes a été mis à jour avec succès.");
define('YOUR_VERSION_UPDATED_SUCCESSFULLY',"Votre version mise à jour avec succès.");

define('YOUR_BACKUP_VERSION_UPDATED_SUCCESSFULLY',"Votre Backup Version mise à jour avec succès...");
define('THERE_IS_SOME_PROBLEM_WITH_UPDATING_YOUR_BACK_UP_VERSION',"Il ya un problème avec la mise à jour de votre version de sauvegarde.");
define('IF_SMTP_USER_IS_GMAIL_THEN',"si l`utilisateur smtp est gmail puis ssl://smtp.googlemail.com");
define('SEND_EMAIL_TO_USER_ON_NEW_MESSAGE',"Envoyer un email à l`utilisateur sur un nouveau message");
define('IMAGE_DIMENSIONS'," Dimensions de l`image");
define('CLEAR_CACHE',"Vider Le Cache");
define('TWITTER_USERANME',"Nom D`Utilisateur Twitter");
define('THEMES',"thèmes");
define('COLOR_ORANGE',"Couleur orange");
define('COLOR_DARKORANGE',"Couleur orange foncé");
define('COLOR_GREY',"Couleur gris");
define('COLOR_BLACK',"la couleur noire");
define('COLOR_WHITE',"Couleur blanc");
define('COLOR_LIGHTGREY',"Couleur gris clair");
define('COLOR_BLUE',"Couleur bleu");
define('COLOR_DARKBLUE',"Couleur bleu foncé");
define('COLOR_CCC',"Couleur ccc");
define('FONT_STYLE',"le style de police");
define('ADMIN_FEES',"Frais de dossier");
define('DONORS_PAYPAL_EMAIL',"Les bailleurs de fonds email paypal");
define('ARE_YOU_SURE_YOU_WANT_TO_CONFIRM_SELECTED_RECORD',"Êtes-vous sûr, vous voulez confirmer dossiers sélectionnés?");
define('AVAILABLE_BALANCE',"Solde disponible");
define('BANK_DETA',"Banque de données");

define("WHO_SELECT_ACCREDIATION", "Qui Sélectionnez Accréditation");
define("PROJECT_OWNER_ADMIN", "".$project." Propriétaire");
define("ALLPROJECTS", "tous ".$project."s");
define("OWNER", "Propriétaire");
define('BEFORE_YOU_CAN_APPROVE_THIS_PROJECT',"Avant de vous approuvez cette ".$project." contrat d` ajout est nécessaire , s`il vous plaît suivre %s télécharger contrat.");
define('THIS_LINK',"ce lien");
define('ACCREDENTIAL_SETTING',"Cadre Accredential");
define('CONTRACT_SETTING',"Cadre de contrat");

define('ACCREDIATION_SELECT_MANAGE_TITLE',"Comment investisseur accréditation sera géré?");
define('ACCREDIATION_SELECT_ALLPROJECT',"Gardez accréditation requise pour tous les investisseurs avant d'investir.");
define('ACCREDIATION_SELECT_PROJECT_OWNER_ADMIN',"Que les propriétaires de la ". $project ." de décider qui peut investir sur leurs ".$project."s");

define('CONTRACT_COPY_UPLOAD_BY_OWNER',"Propriétaire de " . $project ."");
define('CONTRACT_COPY_UPLOAD_BY_ADMIN',"Administrateur du site (vous ou votre personnel administratif)");
define('PROJECT_TITLE_GOES_HERE',"projet-titre-va-ici");
define('IMAGE_UPLOAD_LIMIT', "Limite d'upload d'image");
define('UPLOAD_LIMIT_SHOULD_BE_GREATER_THAN_ZERO', 'Ajouter limite devrait être supérieure à zéro');

define("CONTRACT_COPY_REQUIRED", "Contrat copie exigée");

/*admin_strip_lang.php*/
define('CUSTOMER_KEY',"Customer Key");
define('STRIPE_PAYMENT_GATEWAY',"Passerelle de paiement Stripe");
define('STRIPE_KEY_TEST_PUBLIC',"Stripe Key Public Test");
define('STRIPE_KEY_TEST_SECRET',"Stripe Key test secret");
define('STRIPE_KEY_LIVE_PUBLIC',"Stripe clé publique en direct");
define('STRIPE_KEY_LIVE_SECRET',"Stripe clé secrète en direct");
define('STRIPE_FEE_TAKEN_FROM',"Honoraires Stripe prises de");
define('STRIPE',"Stripe");

/*admin_spam_lang.php*/
define('TOTAL_SPAM_REPORT_ALLOW',"Rapport total Spam Laisser");
define('REPORT_SPAMER_EXPIRE',"Rapport spammer expirer [En Days]");
define('TOTAL_REGISTRATION_ALLOW_FROM_SAME_IP',"Total Inscription Laisser De la même IP");
define('REGISTRATION_SPMAER_EXPIRE',"spammer d'enregistrement expirer [En Days]");
define('TOTAL_COMMENT_ALLOW_FROM_SAME_IP_IN_ONE_WAY',"total Commentaireaire Laisser De même IP Dans One Day");
define('COMMENT_SPAMER_EXPIRE',"Commentaire spammer expirer [En Days]");
define('TOTAL_INQUIRY_ALLOW_SAME_IP_IN_ONE_WAY',"Demande total Permettre De même IP Dans One Day");
define('INQUIRY_SPAMER_EXPIRE',"Demande spammer expirer [En Days]");
define('ADD_SPAMMER',"Ajouter spammer");
define('SPAMMER',"spammer");
define('SPAM_IP',"Spam IP");
define('MAKE_PERMENANT',"Rendre permanent");
define('SPAM_REPORT_HAS_BEEN_DELETED_SUCCESSFULLY',"Spam Report a été supprimé avec succès.");
define('REPORT_MAKE_SPAM_HAS_BEEN_SUCCESSFULLY',"Rapport faire le spam a été avec succès.");
define('MAKE_SPAMER',"Assurez Spammer");
define('TOTAL_REPORT',"Rapport total");
define('SPAM_BY',"Spam Par");
define('REPORT_BY',"Par rapport");
define('PERMENANT',"permanent");
define('NO_RECORD_AVAILABLE',"Aucun enregistrement availble");
define('REPORTED_EXPIRE',"rapporté expirer");
define('TOTAL_REGISTRATION',"total Inscription");
define('REGISTRATION_SPAM_EXPIRE',"Spam d'enregistrement Expirez");
define('TOTAL_COMMNET',"total Commentaire");
define('COMMENT_SPAM_EXPIRE',"Commentaire Spam Expirez");
define('TOTAL_INQUIRY',"Commission d'enquête totale");
define('INQUIRY_SPAM_EXPIRE',"Demande Spam Expirez");
define('ARE_YOU_SURE_YOU_WANT_TO_MAKE_PERMENANT_SPAMER_SELECTED_REPORT',"Êtes-vous sûr, vous voulez faire des rapports spammeur bas hors sélectionnés?");


/*admin_graph_lang.php*/
define('GRAPHICAL_REPORT_OF_PROJECT',"Rapport graphique de".$project);
define('WEEKLY_AVERAGE_PROJECTS_LINE',"[Ligne] de la semaine ". $project);
define('WEEKLY_AVERAGE_PROJECTS_BAR',"[Bar] de la semaine ".$project);
define('MONTHLY_AVERAGE_PROJECTS_LINE',"[Ligne] du mensuel ". $project);
define('MONTHLY_AVERAGE_PROJECTS_BAR',"[Bar] Mensuel ". $project);
define('YEARLY_AVERAGE_PROJECTS_LINE',"[Ligne] de chaque année ". $project);
define('YEARLY_AVERAGE_PROJECTS_BAR',"[Bar] de chaque année ". $project);
define('TOTAL_PROJECTS_PIE_CHART',"Pie Chart de Total ".$project);
define('TRANSACTION_PROJECTS_PIE_CHART',"De". $project."Transactions Pie Chart");
define('GRAPHICAL_REPORT_OF_USER_REGISTRATION',"Rapport graphique de l' enregistrement de l' utilisateur");
define('WEEKLY_AVERAGE_REGISTRATION_LINE',"Inscription hebdomadaire moyenne [Ligne]");
define('WEEKLY_AVERAGE_REGISTRATION_BAR',"Inscription hebdomadaire moyenne [bar]");
define('MONTHLY_AVERAGE_REGISTRATION_LINE',"Inscription mensuel moyen [Ligne]");
define('MONTHLY_AVERAGE_REGISTRATION_BAR',"Inscription mensuel moyen [bar]");
define('YEARLY_AVERAGE_REGISTRATION_LINE',"Inscription annuelle moyenne [Ligne]");
define('YEARLY_AVERAGE_REGISTRATION_BAR',"Inscription annuelle moyenne [bar]");
define('WITH_STACKING',"avec empilage");
define('WITHOUT_STACKING',"sans empilage");
define('BARS',"Bars");
define('LINES',"lignes");
define('LINES_WITH_STEPS',"Lignes avec étapes");
define('REGISTRATION', 'Enregistrement');
/*admin_emai_lang.php*/

define('MASTER_EMAIL_TEMPLATE',"Maître Email Template");
define('SELECT_EMAIL_TEMPLATE',"Sélectionnez Modèle de courrier électronique");
define('EMAIL_FOR_TASK',"Email pour la tâche");

/*admin_acreditation_lang.php*/

define('ACCREDITATION_USERS',"Utilisateurs d 'accréditation");
define('ACCREDITATION_USER',"Accréditation utilisateur");
define('ACCREDITATION_USERS_DETAIL',"Utilisateurs d 'accréditation Détails");
define('APPROV',"approuver");

define('ACCREDITATION_DETAILS',"détails Accréditation");
define('INCOME',"revenu");

define('VERIFICATION_OPTIONS',"Options de vérification");
define('DOWNLOAD',"télécharger");

define('REJECT',"rejeter");
define('REJECTED',"rejeté");
define('YOU_CAN_APPROVE', 'Vous pouvez approuver');
define('YOU_CAN_REJECT', 'Vous pouvez refuser');
/*admin_content_lang.php*/

define('ORDER',"Commande");
define('ANSWER',"Répondre");
define('ADD_PAGES',"Ajouter pages");
define('CATEGORY_NAME',"Nom De Catégorie");
define('DISPLAY_PLACE',"Affichage Lieu");
define('FOOTER',"Pied de page");
define('PAGES_SIDEBAR',"Pages Sidebar");
define('LEARN_CATEGORY',"Apprenez Catégorie");
define('PAGES_TITLE',"Pages Titre");
define('SUB_TITLE',"Sous le titre");
define('ICON',"Icône");
define('SELECT_FILE',"Choisir le dossier");
define('CHANGE',"Changement");
define('REMOVE',"Supprimer");
define('SLUG',"Limace");
define('META_KEYWORD',"Meta mots-clés");
define('META_DESCRIPTION',"Meta Description");
define('SIDEBAR',"Sidebar");
define('RECORDS_HAS_BEEN_DELETED_SUCCESSFULLY',"Enregistrez [s] a été supprimé avec succès.");
define('RECORD_HAS_BEEN_ACTIVATED_SUCCESSFULLY',"Enregistrez [s] a été activé avec succès.");
define('RECORD_HAS_BEEN_INACTIVATED_SUCCESSFULLY',"Enregistrez [s] a été inactivé avec succès.");
define('RECORD_HAS_BEEN_SHOW_ON_HELP_PAGE_SUCCESSFULLY',"Enregistrez [s] a été exposition sur la page d'aide avec succès.");
define('RECORD_HAS_BEEN_REMOVE_ON_HELP_PAGE_SUCCESSFULLY',"Enregistrez [s]) has been remove on help page successfully.");
define('YOU_CAN_NOT_DEACTIVATED_ALL_CURRENCIES',"Vous ne pouvez pas désactiver toutes les monnaies. Il devrait y avoir un actif.");
define('RECORDS_HAS_BEEN_MAKE_PERMENANT_SUCCESSFULLY',"Enregistrement a été fait avec succès bas hors.");

define('LEARN_MORE_CATEGORY',"En savoir plus Catégorie");
define('LEARN_CATEGORY_NAME',"Apprenez Nom de la catégorie");
define('LEARN_MORE_PAGES',"En savoir plus Pages");
define('MORE',"Plus");
//==============Payment Passerelle===========================
define('WALLET_SETTING',"Wallet Cadre");
define('WALLET_ADD_TIME_FEES',"Wallet Ajouter Temps frais [%]");
define('WALLET_ADD_MIN_AMOUNT',"Wallet Ajouter Temps Montant minimum");
define('WALLET_WITHDRAW_FEES',"Wallet retirer [don] Temps frais [%]");
define('DIRECT_DONATE_OPTION',"L'option Direct don");
define('ADD_WHOLE_DONATION_AMOUNT',"Ajouter montant du don entier");
define('ADD_REMAINING_DONATION_AMOUNT',"Ajouter le reste du montant du don");
define('WALLET_REVIEW',"Wallet critique");
define('REVIEW',"Examen");
define('CONFIRM',"Confirmer");
define('AMOUNT_ADDED',"Montant Ajouté");
define('GATEWAY',"Passerelle");
define('WALLET_WITHDRAW',"Wallet Retrait");
define('CURRENT_AMOUNT',"Montant actuel");
define('AMOUNT_TO_PAY',"Montant à payer");
define('AMAZON_GATEWAY',"Amazon passerelle");
define('VARIABLE_FEES',"Droits variables");
define('FIXED_FEES',"Frais fixes");
define('SUPPORT_MASSPAYMENT',"SOUTIEN MASSPAYMENT");
define('Support_Masspayment',"Soutien Masspayment");
define('FUNCTION_NAME',"Nom de la fonction");
define('Nom',"Nom");
define('Valeur',"Valeur");
define('Étiquette',"Étiquette");
define('Description',"Description");
define('Action',"Action");
define('Image',"Image");
define('LANGUAGE_IS_ALREADY_EXIST',"La langue est existe déjà");
define('ALLOW_ONLY_IMAGE_FILE_TO_UPLOAD',"Autoriser uniquement fichier image à télécharger");
define('MESSAGE_SUBJECT',"Objet du message");
define('FUNCTION_NAMES',"Nom de la fonction");
define('SUAPPORT_MASSPAYMENT',"Suapport Masspayment");
define('PAGES_DESCRIPTION',"Description de la page");
define('EXTERNAL_URL',"URL externe");
define('FAQ_CATEGORY',"catégorie Faq");
define('QUESTIONS',"Question");
define('ACTIONS',"Action");
//all confirm message
define('DELETE_USER_CONFIRMATION',"Etes-vous sûr, vous voulez supprimer l'utilisateur?");
define('DELETE_SELECT_RECORD_CONFIRMATION',"S'il vous plaît sélectionner un enregistrement");
define('DELETE_CATEGORY_CONFIRMATION',"Etes-vous sûr, vous voulez supprimer cette catégorie?");
define('DELETE_PAGE_CONFIRMATION',"Etes-vous sûr, vous voulez supprimer cette page?");
define('DELETE_COUNTRY_CONFIRMATION',"Etes-vous sûr, vous voulez supprimer pays?");
define('DELETE_CURRENCY_CONFIRMATION',"Etes-vous sûr, vous voulez supprimer monnaie?");
define('DELETE_GATEWAY_DETAIL_CONFIRMATION',"Etes-vous sûr, vous voulez supprimer les passerelles détail?");
define('DELETE_ADMINISTRATOR_CONFIRMATION',"Etes-vous sûr, vous voulez supprimer administrateur?");
define('DELETE_GATEWAY_PAYMENT_CONFIRMATION',"Etes-vous sûr, vous voulez supprimer la passerelle de paiements?");
define('ARE_YOU_SURE_YOU_WANT_TO_DELETE_SELECTED_LEARN_CATEGORIES',"Êtes-vous sûr, vous voulez supprimer choisi apprendre catégories?");
define('ARE_YOU_SURE_YOU_WANT_TO_ACTIVE_SELECTED_LEARN_CATEGORIES',"Êtes-vous sûr, vous voulez apprendre actif choisi catégorie?");
define('ARE_YOU_SURE_YOU_WANT_TO_INACTIVE_SELECTED_LEARN_CATEGORIES',"Êtes-vous sûr, vous voulez apprendre catégorie inactive sélectionnée?");
define('ARE_YOU_SURE_YOU_WANT_TO_DELETE_SELECTED_MESSAGE',"Êtes-vous sûr, vous voulez supprimer les messages sélectionnés?");
define('TYPE_A_MESSAGE_HERE',"Tapez un message ici...");
define('RECORDS_UPDATED_BY_THIS_CRONJOB',"Dossiers mises à jour par cette tâche cron");
define('NO_RECORDS_UPDATED_BY_THIS_CRONJOB',"Aucune information mises à jour par cette tâche cron");
define('PAYMENT_GATEWAYS',"Passerelles de paiement");
define('WITHDRAW_REQUEST',"Demande de retrait");
define('TEST_UPDATE',"Test & Mise à jour");
define('GATEWAYS_DETAILS',"passerelles Détails");


//CATEGORY
define('ADD_CATEGORY',"Ajouter Catégorie");
define('EDIT_CATEGORY',"Modifier une catégorie");
define('PARENT',"Parent");
define('CHILD',"Enfant");
define('TYPE',"Type");
define('CATEGORY_LIST',"Liste Catégorie");

//Phase
define('ADD_PHASE',"Ajouter phase");
define('EDIT_PHASE',"phase Modifier");
define('PHASE_LIST',"Liste Phase");
define('DELETE_PHASE',"Etes-vous sûr, vous voulez supprimer cette phase?");
define('PHASE_NAME',"phase Nom");
define('PHASE',"phases");
define('LOADING',"Chargement");
define('EQUITY_CATEGORIES', ''.$project_name.' Kategorien');
define('PROJECT_PHASE', 'Phasen '.$project_name);

define('MAIN',"Principal");
define('RELATED',"en relation");
define('CATEGORY_DESCRIPTION',"catégorie Description");

/*admin_equity_lang.php*/

define('CONTRACT_DOCUMENT',"Document de contrat");
define('CONTRACT_MANAGE',"Gérer contrat");
define('OFFLINE',"Hors ligne");
define('EQUITY_NAME',"Nom de l'équité");
define('HISTORY',"Histoire");
define('DOCUMENT_NAME',"Nom du document");
define('CURRENT_STATUS',"Statut Actuel");
define('DOWNLOAD_ATTACH_FILE',"Télécharger Joindre un fichier");

define('PROCESSING_CONTRACT',"Traitement contrat");
define('DOWNLOADED_CONTRACT',"Contrat téléchargé");
define('UPLOADED_SIGNED_CONTRACT',"Téléchargée Signature du contrat");
define('SIGNED_CONTRACT_APPROVE',"Contrat signé Approuver");
define('PAYMENT_PROCESS',"Processus de paiement");
define('PAYMENT_RECIEPT_UPLOADED',"Paiement Reciept Téléchargée");
define('PAYMENT_CONFIRMED',"Paiement Accepté");
define('TRACKING_SHIPMENT',"Suivi de l'expédition");

define('SHIPMENT',"Expédition");
define('NO_RECORDS_AVAILABLE',"Aucune information disponible");
define('YOUR_MESSAGE_HAS_BEEN_SUCCESSFULLY_SENT',"Votre message a été envoyé avec succès.");
define('ACKNOWLEDGE_NOTE',"Reconnaître Remarque");

define('COMPANY_CATEGORY_LIST',"Catégorie Société Liste");
define('COMPANY_CATEGORY_ALREADY_EXIST',"Société Catégorie alredy exister.");
define('COMPANY_CATEGORY_NAME',"Nom de la société Categoty");
define('INSERT',"insérer");
define('UPDATE_MSG',"mise à jour");
define('ADD_COMPANY_CATEGORY',"Ajouter Société Catégorie");
define('THEME',"Thème");
define('EDIT_COMPANY_CATEGORY',"Modifier Société Catégorie");
define('FUNDING_SOURCE',"Source De Financement");
define('DOCUMENT_TYPE',"Type De Document");
define('COMPANY_INDUSTRY_LIST',"Société Liste Industrie");
define('ADD_COMPANY_INDUSTRY',"Ajouter Domaine de la société");
define('DELETE_INDUSTRY_CONFIRMATION',"Are you sure,You want to delete this industry?");
define('EDIT_COMPANY_INDUSTRY',"Modifier Domaine de la société");
define('COMPANY_INDUSTRY_ALREADY_EXIST',"Société Industrie Nom déjà exister.");
define('FUNDING_SOURCE_LIST',"Financement Liste de Source");
define('DELETE_FUNDING_SOURCE_CONFIRMATION',"Are you sure,You want to delete this funding source?");
define('ADD_FUNDING_SOURCE',"Ajouter Source de financement");
define('EDIT_FUNDING_SOURCE',"Modifier la source de financement");
define('FUNDING_SOURCE_ALREADY_EXIST',"Source de financement alredy exister.");
define('DELETE_FUNDING_TYPE_CONFIRMATION',"Are you sure,You want to delete this funding type?");
define('FUNDING_TYPE_LIST',"Financement Liste");
define('ADD_FUNDING_TYPE',"Ajouter Type de financement");
define('EDIT_FUNDING_TYPE',"Modifier le type de financement");
define('FUNDING_TYPE_ALREADY_EXIST',"Type de financement alredy exister.");
define('DELETE_DOCUMENT_TYPE_CONFORMATION',"Are you sure,You want to delete this document type?");
define('ADD_DOCUMENT_TYPE',"Ajouter Type de document");
define('EDIT_DOCUMENT_TYPE',"Modifier le type de document");
define('DOCUMENT_TYPE_LIST',"Type de document Liste");
define('DOCUMENT_TYPE_ALREADY_EXIST',"Type de document alredy exister.");
define('DELETE_ACCOUNT_TYPE_CONFORMATION',"Are you sure,You want to delete this account type?");
define('ACCOUNT_TYPE_LIST',"Type de compte Liste");
define('ADD_ACCOUNT_TYPE',"Ajouter Type de compte");
define('EDIT_ACCOUNT_TYPE',"Modifier le type de compte");
define('ACCOUNT_TYPE_ALREADY_EXIST',"Type de compte alredy exister.");
define('DELETE_TEAM_MEMBER_CONFIRMATION',"Are you sure,You want to delete this team member type?");
define('TEAM_MEMBER_TYPE_LIST',"Team Type de Liste des membres");
define('ADD_TEAM_MEMBER_TYPE',"Ajouter Type de membre de l'équipe");
define('EDIT_TEAM_MEMBER_TYPE',"Modifier le type Membre de l'équipe");
define('TEAM_MEMBER_TYPE_ALREADY_EXIST',"Team Type membres alredy exister.");

//front equity

define('FILE_UPLOAD_SUCCESS',"Votre copie du contrat a été soumis avec succès.");
define('PLEASE_UPLOAD_ZIP_OR_PDF_FILE',"S'il vous plaît télécharger le fichier pdf ou zip");
define('SORRY_THIS_FILE_IS_TOO_LARGE',"Désolé, ce fichier est trop volumineux.");
define('CONGRATULATIONS',"Félicitations!");
define('YOU_ARE_ALMOST_THERE',"Vous y êtes presque");
define('WHAT_NEXT_PLEASE_FOLLW_THE_STEP_BELOW',"Que faire ensuite? S'il vous plaît follw l'étape ci-dessous.");
define('SIGN_CONTRACT',$funds."» Contrat");
define('YOU_MAY_HAVE_RECEIVED_AN_EMAIL_WITH_CONTRACT_YOU_NEED_TO_SIGN_AND_SEND_BACK_OR',"Vous avez reçu un e-mail avec un contrat que vous devez signer et renvoyer ou");
define('DOWNLOAD_FROM_HERE',"télécharger ici");
define('THIS_STEP_IS_CONFIRMED_COMPLETED',"Cette étape est confirmée et complétée");
define('UPLOAD_CONTRACT',"Ajouter contrat");
define('PLEASE_UPLOAD_SIGNED_CONTRACT_FROM_HERE_AND_WAIT_UNTIL_EQUITY_CROWDFUNDING_TEAM_APPROVES_AND_MARK_IT_AS_CONFIRMED_UPLOAD_CONTRACT_HERE',"S'il vous plaît télécharger contrat signé d'ici et d'attendre de l'équipe approuve l'équité crowdfunding et la marquer comme confirmé. Télécharge contrat ici");
define('PLEASE_DO_NOT_PROCEED_TO_STEP_UNTIL_STEP_IS_CONFIRMED',"S'il vous plaît ne pas procéder à Step.3 jusqu'à Step.2 est confirmée.");
define('USE_FOLLOWING_BANK_INFORMATION_TO_MAKE_PAYMENT',"Utiliser les informations de la banque ci-dessous pour effectuer le paiement.");
define('OR_USE_FOLLOWING_DETAILS_TO_WRITE_US_CHECK',"OU Utilisez les détails suivants pour écrire nous Check");
define('ACKNOWLEDGE',"Reconnaître");
define('UPLOAD_PAYMENT_RECEIPT_SNAP_OR_TRACKING_DETAILS_FOR_THE_PAYMENT_YOU_MADE_UPLOAD_RECEIPT',"Télécharge paiement réception mousqueton ou détails de suivi pour le paiement que vous avez fait. Ajouter réception");
define('CONFIRMED',"Confirmé");
define('PAYMENT_IS_APPROVED_AND_CONFIRMED_BY_EQUITY_CROWDFUNDING_TEAM',"Le paiement est approuvé et confirmé par l'équipe ".$site_name."");
define('TRACK_YOUR_SHIPMENT',"Suivez votre envoi");
define('WRITE_A_NOTE',"Donnez une note");
define('EQUITY_LIST',"Liste d'actions");

define('DEAL_TYPE_SETTING_LIST',"Traiter Liste de réglage");
define('DEAL_TYPE_NAME_ALREADY_EXIST',"Traiter Type Nom déjà exister.");
define('DEAL_TYPE_SETTING',"Type d'aubaine Cadre");
define('EDIT_DEAL_TYPE_SETTING',"Modifier le type Deal Cadre");
define('MIN_PRICE_PER_SHARE',"Min Prix par action");
define('MAX_PRICE_PER_SHARE',"Max Prix par action");
define('MIN_EQUITY_AVAILABLE',"Equity Min Disponible");
define('MAX_EQUITY_AVAILABLE',"Max Equity Disponible");
define('MIN_COMPANY_VALUATION',"Min Evaluation d'entreprise");
define('MAX_COMPANY_VALUATION',"Max Evaluation d'entreprise");
define('DEAL_TYPE_NAME',"Type d'aubaine Nom");
define('DEAL_TYPE_DESCRIPTION',"Type d'aubaine description");
define('DEAL_TYPE_ICON',"Type d'aubaine Icône");
define('PLEASE_SELECT_IMAGE_MORE_THAN_1300',"S'il vous plaît sélectionnez l'image plus de 1300 * 600");
define('MIN_CONVERTIBLE_INTEREST',"Min Convertible intérêt");
define('MAX_CONVERTIBLE_INTEREST',"Max Convertible intérêt");
define('MIN_VALUATION_CAP',"Cap d'évaluation Min");
define('MAX_VALUATION_CAP',"Cap d'évaluation Max");
define('MIN_WARRANT_COVERAGE',"Couverture adjudant Min");
define('MAX_WARRANT_COVERAGE',"Couverture Max Warrant");
define('MIN_CONVERTIBLE_TERM_LENGHT',"Longueur Min transformable");
define('MAX_CONVERTIBLE_TERM_LENGHT',"Longueur max transformable");
define('MIN_CONVERSATION_DISCOUNT',"Conversation Remise min");
define('MAX_CONVERSATION_DISCOUNT',"Conversation Discount Max");
define('MIN_DEBT_INTEREST',"Min Intérêt sur la dette");
define('MAX_DEBT_INTEREST',"Max Intérêt sur la dette");
define('MIN_DEBT_TERM_LENGTH',"Longueur min Durée de la dette");
define('MAX_DEBT_TERM_LENGTH',"Longueur max Durée de la dette");
define('MIN_MAXIMUM_RETURN',"Min rendement maximal");
define('MAX_MAXIMUM_RETURN',"Max rendement maximal");
define('MIN_RETURN_PERCENTAGE',"Min retour Pourcentage");
define('MAX_RETURN_PERCENTAGE',"Max Retour Pourcentage");

define('INVESTOR_TYPE_LIST',"Liste type d'investisseur");
define('ADD_INVESTOR_TYPE',"Ajouter le type d'investisseur");
define('EDIT_INVESTOR_TYPE',"Modifier le type d'investisseur");
define('INVESTOR_TYPE_NAME',"Nom type d'investisseur");
define('INVESTOR_TYPE_NAME_ALREADY_EXIST',"Type d'investisseur Nom déjà exister.");
define('DELETE_INVESTOR_TYPE_CONFIRMATION',"Are you sure,You want to delete this Investisseur type?");

define('INVESTMENT_STEPS_FOR_ADMIN',"Étapes d'investissement pour Admin");
define('NO_ACTION_REQUIRED_IN_THIS_STEP_FOR_YOU',"Aucune action requise dans cette étape pour vous.");
define('ACKNOWLEDGEMENT',"Remerciements");
define('CONFIRMATION',"Confirmation");
define('RECEIPT_CONFIRMATION',"Accusé De Réception");

define('URL_COMPANY_CATETORY',"URL Société Catégorie");
define('LANGUAGE_ID',"Langue ID");
define('ENGLISH',"Anglais");
define('IMAGE_UPLOAD',"Image upload");

define('EQUITY_PERK',"Perk Equity");
define('PERK_TITLE',"Titre Perk");
define('TOTAL_CLAIM',"Recours Total");
define('EQUITY_UPDATES',"Mises à jour Equity");
define('NO_UPDATES',"Pas de mises à jour");
define('EQUITY_DONATION',"Donation d'actions");
define('EQUITY_COMMENT',"Equity Commentaire");
define('NO_COMMENTS',"Aucun Commentaire");
define('EQUITY_REPORT',"Rapport sur l'équité");
define('PROGRESS',"Progrès");
define('INVESTOR',"Investisseur");
define('COMPANY',"Entreprise");
define('CREATE_BY',"Créé par");
define('EQUITY_OVERVIEW',"Equity Présentation");
define('INVESTMENT',"Investissement");
define('GALLERY',"Galerie");
define('DOWNLOAD_CONTRACT_DOCUMENT',"Télécharger le document contrat");
define('CONTRACT_DOCUMENT_SUCCESSFULLY_UPLOADED',"Document de contrat Téléchargé succès");
define('UPLOAD_CONTRACT_DOCUMENT',"Télécharger un document contrat");
define('AS_YOU_HAVE_TO_DECIDED_TO_DECLINE',"As you have decided to decline this %s, please provide reason why you declined this %s, this will better guide %s to help improve his %s and resubmit for review.");
define('SAVE_THIS_REASON_TO_USE_ON_OTHER',"Enregistrer cette raison d'utiliser sur d'autres");
define('REASON_TO_DECLINE_THIS',"Raison de refuser cette");
define('CANCEL',"Annuler");
define('WRITE_REASON_WHY_YOU_DEACTIVATED_THIS',"Ecrire raison pour laquelle vous avez désactivé cette");
define('HIDDEN_MODE',"Hidden Mode");
define('AS_YOU_HAVE_TO_DECIDED_TO_INACTIVE',"Comme vous avez décidé de inactif ce %s, s'il vous plaît choisir les options suivantes avant de le faire.");
define('SELECT_THIS_OPTION_IF_YOU_WANT_TO_HIDE',"Sélectionnez cette option si vous voulez cacher %s complètement de site Web., Pas les utilisateurs seront en mesure de voir ce %s.");
define('SELECT_THIS_OPTION_IF_YOU_WANT_TO_SHOW',"Sélectionnez cette option si vous voulez montrer le %s en mode inactif, tous les utilisateurs seront en mesure de voir ce %s mais ne sera pas en mesure de mettre à jour, %s, %s, suivre.");
define('INACTIVE_MODE',"Mode inactif");
define('PLEASE_NOTE_IF_ONCE_IS_INACTIVATED',"NOTE: S'il vous plaît noter, si une fois le %s est inactivé tout %s");
define('YOU_CAN_HOLD_ALL_UNTILL_YOU_RESOLVE_YOUR_DOUBTS_AND_REASONS',"Vous pouvez tenir tous les% s jusqu'à ce que vous résoudre vos doutes et les raisons pour désactiver avec% s de ce% et d'activer à nouveau.");
define('IF_MANAGED_MANUALLY_BY_TEAM',"Si géré manuellement par l'équipe% de");
define('YOU_CAN_REFUND_ALL_FUND',"Vous pouvez rembourser la totalité% s manuellement à l'% s par virement bancaire ou par chèque si% s sont en cours de traitement manuellement par <nom-site> équipe.");
define('FEEDBACK',"Réaction");
define('IMAGE',"Image");
define('PERK_AMMOUNT',"Montant Perk");
define('IP',"Ip");
define('ARE_YOU_SURE_TO_DELETE_IMAGE',"Êtes-vous sûr de vouloir supprimer l`image?");
define('MAKE_THIS_FEATURED',"Faire de cette vedette");
define('MAKE_THIS_UNFEATURED',"Faire de cette Unfeatured");
define('CANT_BE_FEATURED',"Ne peut pas être en vedette");
define('CANT_DELETE',"Impossible de supprimer");
define('INACTIVE_THIS',"inactive Cette");
define('EQUITY_SETTING',"Cadre d`actions");
define('ACCREDENTIAL_REQUIRED',"Accredential requis?");


define('BY_PROJECT',"par ".$project_name."");
define('BY_OWNER_NAME',"Par nom du propriétaire");
define('BY_PAR_FUNDED',"Par % subventionnées");
define('MAX_FUNDED',"Max subventionnées");
define('MIN_FUNDED',"Min subventionnées");
define('CLEAR_ALL_FILTERS',"Effacer tous les filtres");

define('INVESTMENT_STATUS',"Statut d`investissement");
define('DOCUMENT_REJECTED',"rejeté document");
define('TRACKING_SHIPMENT_CONFIRMED',"Suivi de l`expédition Confirmé");

define('CASH_FLOW_STATUS',"Flux de trésorerie Etat");
define('NONE',"aucun");
define('CASH_FLOW',"Argent qui coule aux investisseurs");
define('ARE_YOU_SURE_YOU_WANT_TO_UPDATE_CASHFLOW_RECORD',"Etes-vous sûr , vous souhaitez mettre à jour les flux de trésorerie Etat ?");
// you can not remove this line because it is used for admin can select cash flowing type
define ("CASH_FLOW_ARRAY", serialize (array (NONE,CASH_FLOW)));
define('HAS_BEEN_UPDATED_SUCCESSFULLY',"a été mis à jour avec succès.");
define('INVESTOR_TYPE_DESCRIPTION',"Type d'investisseur description");
define('INVESTOR_OPTION_LINK',"Investisseurs Option Lien");	
define('INVESTMENT_TAX_RELIEF',"Investor Tax Relief");
define('ADD_INVESTOR_TAX_RELIEF',"Ajouter Investor Tax Relief");
define('EDIT_INVESTOR_TAX_RELIEF',"dit Investor Tax Relief");
define('INVESTMENT_TAX_RELIEF_TYPE',"Type d'investisseur allégement fiscal");

define('MAIN_PHASE', 'Main Phase');
define('SUB_PHASE', 'Sous phase');
define('SLIDER_IMAGE_REQUIRED', 'Curseur image est nécessaire.');
define('SELECT_IMAGE_MORE_THAN_1300_600', "S'il vous plaît sélectionnez l'image plus de 1300 * 600.");
