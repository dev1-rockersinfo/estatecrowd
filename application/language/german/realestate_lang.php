<?php
$taxonomy_setting = taxonomy_setting();
//instead of project or campaign replace this name
$project = $taxonomy_setting['project_name'];
$funds = $taxonomy_setting['funds'];
$comments = $taxonomy_setting['comments'];
$comment = $taxonomy_setting['comments'];
$follower = $taxonomy_setting['follower'];
$project_name = $taxonomy_setting['project_name'];
$site_setting = site_setting();
$site_name = $site_setting['site_name'];
$funds_plural = $taxonomy_setting['funds_plural'];
$investor = $taxonomy_setting['investor'];

$lang["Home"] = "Home";
//Header 

define ("HOME", "Home");
define ("ABOUT", "Über:");
define ("PROJECTS", $project."s");
define ("BLOG", "Blog");
define ("RAISED", "Volkszählung");
define ("LOG_IN", "Login");
define ("SIGN_UP", "Registrieren");
define ("SIGN_OUT", "Abmelden");
define ("BROWSE", "Durchsuchen");
define ("YOUTUBE_CREATORS", "YouTube Creators");
define ("CREATE", "Erstellen");


define("CREATE_A_CAMPAIGN", "Ein kreieren".$project);
define("NEW_COMPAIGN", "Neu ".$project);
define("COMPAIGN", $project);
define("WHAT_IS", "Was ist");
define("LEARN", "Lernen");
define("DASHBOARD", "Armaturenbrett");
define("MY_PROJECTS", "Meine ".$project."s");
define("START_YOUR_PROJECT", "beginnen Sie Ihren ".$project);

define ("TYPE_AND_ENTER", "Typ und geben.");
define ("HI", "Hallo");
define ("TO_YOUR_ACCOUNT", "Um Ihr Konto");
define ("JOIN_WITH_US", "Join mit uns");


//Footer
define ("ABOUT_US", "Über uns"); // rightmenu (8)
define ("TERMS_AND_CONDITIONS", "Allgemeine Geschäftsbedingungen"); // rightmenu (9)
define ("PRIVACY_POLICY", "Datenschutz");
define ("CAREERS", "Karriere");
define ("PRESS", "Presse");
define ("WHY", "Why");
define ("FEATURES", "Eigenschaften"); // RIGHTMENU (14)
define ("FEES", "Gebühren"); // RIGHTMENU (15)
define ("FAQ", "Hilfe"); // RIGHTMENU (16)
define ("HOW_TUBESTART_WORKS", "Wie funktioniert ".$site_name.""); // (17)


define ("HOW", "Wie");
define ("WORKS", "Werke");
define ("HELP", "Hilfe");
define ("CONTACT_US", "Kontaktieren Sie uns");
define ("ALL_RIGHT_RESERVED", "Alle Rechte vorbehalten");
define ("WEEKLY_NEWSLETTER", "Weekly Newsletter");
define ("LETS_SOCIALIZE", "Lass uns zu sozialisieren");
define ("GET_AWESOME_NEWS", "Get Awesome Nachrichten in Ihren Posteingang geliefert! Jede Woche");
define ("FACEBOOK", "Facebook");
define ("TWITTER", "Twitter");
define ("GOOGLE_PLUS", "Google Plus");
define ("LINKEDIN", "Linkedin");
define ("YOUTUBE", "Youtube");

//a

define ("ENTER_YOUR_EMAIL", "Geben Sie Ihre E-Mail"); // 10
define ("ENTER_VALID_EMAIL", "Geben Sie gültige E-Mail"); // 10,13,30
define ("COPYRIGHT", "2013"); // 152
define ("ROCKERS_TECHNOLOGIES", "ROCKERS TECHNOLOGIES"); // 158
define ("DESIGNED_BY", "Designed By"); // 158


//content
define ("DAYS_LEFT", "Tage Left");
define ("DAYS_TO_GO", "Days To Go");
define ("HOURS_LEFT", "Hours Left");
define ("MINUTES_LEFT", "Minutes Left");
define ("SECONDS_LEFT", "Sekunden nach links");
//a
define("LATEST_PROJECT", "Neueste ".$project."s"); //131
define("VIEW_CAMPAIGN", "Aussicht ".$project.""); //104


//Search

define ("SEARCH_BY", "Suche nach");
define ("SEARCH", "Suchen");
define ("PLEASE_ENTER_SEARCH_KEYWORD", "Bitte geben Sie Suchbegriff");


define("ALL_PROJECTS", "Alle ".$project."s");
define("FEATURED", "Hervorgehoben");
define("ROUNDING_OFF", "Abrundung");
define("POPULAR", "Beliebt");
define("LATEST_COMPAIGNS", "Neueste ".$project."s");


define ("NEAR_ME", "Near Me");
define ("SUCCESS_STORY", "Erfolgsgeschichten");
define ("MOST_FUNDED", "Die meisten funded");
define ("ADVANCED_SEARCH", "Erweiterte Suche");
define ("KEYWORDS", "Keywords");
define ("TITLE", "Titel");
define ("CATEGORY", "Kategorie");
define ("CITY", "City");
define ("COUNTRY", "Land");
define ("GOAL_SIZE", "Goal Size");
define ("STATUS", "Status");
define ("GOAL_TYPE", "Zieltyp");

define("CELEBRATIONS_AND_SPECIAL_EVENTS", "Feste & besondere Anlässe Test Bildung ".$funds);
define ("CATEGORIES", "Kategorien");
define ("SELECT_CATEGORY", "Kategorie wählen");
define ("SELECT_COUNTRY", "Land auswählen");
define ("SIGNUP", "Registrieren");
define ("NEW_TO", "New To");
define ("NAME", "Full Name");
define ("EMAIL_ADDRESS", "E-Mail-Adresse");
define ("RETYPE_EMAIL", "E-Mail erneut eingeben");
define ("PASSWORD", "Password");
define ("RETYPE_PASSWORD", "Passwort erneut eingeben");


//define("DISCOVER_NEW_PROJECTS_WITH_WEEKLY_NEWSLETTER_PLEASE_ACCEPT_TERMS_CONDITION_TO_ENJOY_YOU_AGREE_TO_OUR","Discover new projects with weekly Newsletter Please accept terms & condition to enjoy %s, you agree to our");

define("DISCOVER_NEW_PROJECTS_WITH_WEEKLY_NEWSLETTER_PLEASE_ACCEPT_TERMS_CONDITION_TO_ENJOY_YOU_AGREE_TO_OUR", "Mit einem Klick Anmeldung für% s, erklären Sie sich mit unseren");
define ("NEW_PROJECTS_WITH_WEEKLY_NEWSLETTER", "Ich würde die wöchentliche% s Newsletter gefallen.");
define ("TERMS_OF_USE", "Nutzungsbedingungen");
define ("LOGIN", "Login");
define ("SUCCESS", "Erfolg");
define ("WE_WILL_NEVER_POST_ANYTHING_WITHOUT_YOUR_PERMISSION", "Wir werden nie etwas zu schreiben, ohne Ihre Erlaubnis.");
define ("EXISTS_ACCOUNT_ASSOCIATE_WITHMAIL", "Es ist eine bestehende E-Mail-Konto mit diesem verbunden ist.");
define ("YOU_HAVE_SIGNED_UP_SUCCESSFULLY", "Sie haben erfolgreich unterzeichnet Ihre Kontodaten, um Ihre E-Mail-Adresse gesendet..");
define ("PLEASE_LOGIN_TO_CONTINUE", "Bitte Loggen Sie sich ein, um fortzufahren");
define ("LOG_IN_TO_YOUR_ACCOUNT", "Anmelden bei Ihrem Konto");
define ("DONT_HAVE_AN_ACCOUNT", "Sie haben noch kein Konto?");
define ("EMAIL_LOGIN", "E-Mail Login");



define ("LOGIN_WITH_AN_EMAIL_ADDRESS", "Melden Sie sich mit einer Email-Adresse");
define ("LOGIN_WITH_A_SOCIAL_NETWORK", "Melden Sie sich mit einem sozialen Netzwerk");
define ("LOGIN_WITH_FACEBOOK_TWITTER_OR_LINKEDIN", "Anmelden mit Facebook, Twitter oder LinkedIn");
define ("SIGNUP_WITH_A_SOCIAL_NETWORK", "Melden Sie sich bei einem sozialen Netzwerk");
define ("QUICKLY_CREATE_AN_ACCOUNT", "Erstellen Sie schnell ein Konto");
define ("SIGNUP_MANUALLY", "Registrieren von Hand");
define ("CREATE_AN_ACCOUNG_USING_EMAIL", "Erstellen Sie ein Konto mit E-Mail");
define ("REMEMBER_ME", "Remember Me");
define ("I_FORGOT_MY_PASSWORD", "Ich habe mein Passwort vergessen.");
define ("SOCIAL_LOG_IN", "Social Log In");
define ("EMAIL_ADDRESS_OR_PASSWORD_ARE_INVALID", "E-Mail-Adresse oder Kennwort ist ungültig.");
define ("YOUR_EMAIL_ADDRESS_IS_SUSPENDED", "Ihre E-Mail-Adresse ist von admin aufgehängt.");
define ("YOUR_ACCOUNT_IS_NOT_ACTIVEPLEASE_CHECK_YOUR_INBOX_AND_CLICK_ON_ACTIVATION_LINK", "Ihr Konto nicht aktiv ist Überprüfen Sie Ihren Posteingang und klicken Sie auf Aktivierungslink..");
define ("YOU_ARE_SIGNED_OUT_SUCCESSFULLY", "Sie haben erfolgreich abgeschlossen.");
define ("RESEND_VARIFICATION_LINK", "Erneut senden Bestätigungslink.");
define ("SEND_VARIFICATION", "Bestätigung senden");
define ("CLICK_HERE", "Klicken Sie hier");
define ("RESEND","Erneut senden");
define ("PLEASE_ENTER_EMAIL_TO_SEND_VARIFICATION_LINK", "Bitte geben Sie den Bestätigungslink per E-Mail senden");
define ("PLEASE_ENTER_EMAIL_ADDRESS_FOR_VARIFICATION_LINK", "Bitte geben Sie E-Mail-Adresse für Bestätigungslink");
define ("EMAIL_ADDRESS_NOT_FOUND", "E-Mail-Adresse nicht gefunden.");
define ("YOUR_ACCOUNT_IS_ALREADY_ACTIVE", "Ihr Konto bereits aktiv ist.");
define ("FORGET_PASSWORD", "Passwort vergessen?");
define ("TELL_US_THE_EMAIL_YOU_USED_TO_SIGNUP_WELL_GET_YOU_LOGGED_IN", "Bitte geben Sie uns die E-Mail die Sie sich anmelden verwendet, und wir bringen Sie eingeloggt.");
define ("RESET_MY_PASSWORD", "Reset Password");
define ("CREATE_A", "Create a");
define ("ACCOUNT", "Konto");


define ("ALREADY_HAVE_AN_ACCOUNT", "haben bereits ein Konto?");
define ("MAIL_SENT_SUCCESSFULLY", "Mail gesendet erfolgreich");
define ("BACK_TO_LOGIN", "Back To Anmelden");
define ("PLEASE_ENTER_EMAIL", "Bitte geben Sie E-Mail");
define ("SUBMIT", "Einreichen"); // login (179)
define ("RESET_PASSWORD", "Passwort zurücksetzen");
define ("RESET", "Reset");



define ("YOUR_ACCOUNT_ACTIVATED_SUCCESSFULLY", "Herzlichen Glückwunsch Ihr Konto erfolgreich aktiviert Jetzt können Sie mit Ihren Login-Daten anmelden!..");
define ("ERROR_OCCURED_IN_ACTIVATING_ACCOUNT", "Fehler in activate Prozess Ihres Kontos Bitte kontaktieren Sie Administrator..");
define ("YOUR_DONATION_MADE_SUCCESSFULLY", "Ihre Spende hat erfolgreich gemacht");
define ("YOUR_DONATION_FAILED", "Ihre Spende Prozess gescheitert, Versuchen Sie es erneut.");
define ("MESSAGE_SEND_SUCCESSFULLY", "Nachricht erfolgreich gesendet.");
define ("YOUR_REQUEST_EXPIRED", "Ihre Verbindung ist abgelaufen.");



define ("QUICK_LINKS", "Quick Links");
define ("SOCIAL_SIGN_UP", "Social sign up");
define ("NEWSLETTER", "Newsletter");
define ("LOGIN1", "Login");
define ("FILL_YOUR_INFO", "Füllen Sie Ihre Daten");
define ("FIRST_NAME", "Vorname");
define ("UPLOAD_PHOTO", "Foto hochladen");
define ("CHANGE_PHOTO", "Change Photo");
define ("LAST_NAME", "Nachname");
define ("SKIP", "Skip");
define ("SAVE_AND_CONTINUE", "Speichern und fortfahren");
define ("SAVE_AND_FINISH", "Speichern und Beenden");
define ("ABOUT_YOURSELF", "Über mich");
define ("INTREST", "Interest");
define ("SKILL", "Skill");
define ("OCCUPATION", "Beruf");
define ("VIEW_COMPAIGN", "Ansicht Kampagne mit");


/*** contact form ******/

define ("PLEASE_SEND_EMAIL_FOR_VARIFICATION_LINK", "Bitte senden Sie E-Mail für Bestätigungslink.");
define ("IPBAND_CANNOTREG_NOW_SIGNIN", "Ihre IP wurde wegen Spam gewesen Band Du kannst jetzt nicht anmelden..");
define ("IPBAND_CANNOTREG_NOW", "Ihre IP wurde wegen Spam gewesen Band Du kannst jetzt nicht zu registrieren..");
define ("IP_BAND_CONTACT_WEB_MASTER", "Ihre IP-Adresse ist Band bitte dem Server-Betreiber zu kontaktieren..");


define ("IPBAND_CANNOT_INQUIRY", "Ihre IP wurde Band aufgrund von Spam kann nicht Beitrag zu erkundigen..");
define ("INQUIRY_MESSAGE_SENT_SUCCESSFULLY", "Ihre Anfrage erfolgreich gesendet.");
define ("SEND", "SEND");
define ("BANDCAMP", "Bandcamp");
define ("GROUP_FUND_CREATORS", "Gruppe Fund Creators");
define ("GROUP_FUND", "Gruppe Fonds");
define ("NO_RESULT_FOUND", "Keine Ergebnisse gefunden Verwenden Sie die Optionen in der linken Seitenleiste, um Ihre Suche zu verfeinern..");


/******* */

define ("INVITE_MSGS", "% s möchte Ihnen auf% s zu% s Team hinzuzufügen, Anmelden oder Registrieren, dem Team beizutreten");
define ("NEW_THIS_WEEK", "diese Woche");
define ("YOUR_ACCOUNT_INACTIVE_ADMIN", "Ihr Konto ist von admin inaktiv.");
define ("INACTIVE_ACCOUNT_AFTER_SIGNUP", "Ihr Konto nicht aktiv ist Überprüfen Sie Ihren Posteingang und klicken Sie auf Aktivierungslink..");
define ("RESEND_VERIFICATION_LINK", "Erneut senden Bestätigungslink");
define ("RESET_PASSWORD_FAILED", "Send Reset Passwortabfrage fehlgeschlagen Versuchen Sie es erneut..");
define ("YOUR_ACCOUNT_ALREADY_ACTIVE", "Ihr Konto wurde bereits aktiviert.");
define ("RESEND_VERIFICATION_FAILED", "Erneut senden verificatioin Prozess gescheitert.");


define ("WRONG_FACEDBOOK_SETTINGS", "Es gibt mit Facebook-API ein Fehler auftritt, überprüfen Sie bitte Ihre Facebook-Einstellung.");
define ("TWITTER_CONNECTION_ERROR", "Konnte nicht mit Twitter verbinden Aktualisieren Sie die Seite oder versuchen Sie es später erneut..");
define ("WRONG_TWITTER_SETTINGS", "Es gibt mit Twiiter API ein Fehler auftritt, überprüfen Sie bitte Ihre Twitter-Einstellung.");
define ("WRONG_LINKEDIN_SETTINGS", "Es gibt mit Linkdin API ein Fehler auftritt, überprüfen Sie bitte Ihre Linkdin Einstellung.");
define ("SOCIAL_DETAILS_UPDATED_SUCCESSFULLY", "Social Details erfolgreich aktualisiert.");
define ("OLD_PASSWORD_DOESNT_MATCH", "Der alte eingegebene Passwort nicht mit Ihrem alten Kennwort überein.");

//header view 

define ("YOUR_ACCOUNT_ACTIVATED_SUCCESSFULYY_LOGIN", "Herzlichen Glückwunsch Ihr Konto erfolgreich aktiviert Jetzt können Sie mit Ihren Login-Daten anmelden!..");
define ("YOUR_ACCOUNT_ACTIVATED_ALREADY_LOGIN", ". Ihr Konto ist bereits aktiviert Sie können mit Ihren Login-Daten anmelden.");
define ("CONFIRM_ACCOUNT_ERROR", "Ihr habe einige Fehler zu berücksichtigen, zu bestätigen.");
define ("SIGN_UP_SUCCESSFULLY_DETAILS_SENT", "Sie haben erfolgreich unterzeichnet Ihre Kontodaten, um Ihre E-Mail-Adresse gesendet..");


define("WELCOME_LOGIN_SUCCESSFULL", "Herzlich willkommen %s, Sie haben sich erfolgreich angemeldet.");
define ("PASSWORD_RESET_SUCCESSFULLY_LOGIN", "Ihr Passwort erfolgreich zurückgesetzt Jetzt können Sie Login zu nehmen..");
define ("EMAIL_VERIFICATION_LINK_RESEND", "E-Mail-Bestätigungslink wurde an Ihre E-Mail erfolgreich übel wurde.");


define("DRAFT_PROJECT_MESSAGE_HOME", "Dies ".$project." ist nur ein Entwurf. Es kann nicht zu veröffentlichen ".$project.".");


define ("DONATION_DONE_SUCCESSFULLY_HOME", "Ihre Spende erfolgreich getan.");
define ("DONATION_FAILED_HOME", "Ihre Spende ist fehlgeschlagen.");
define ("MESSAGE_SENT_SUCCESSFULLY_HOME", "Ihre Nachricht erfolgreich gesendet.");
define ("EMAIL_ALREADY_SUBSCRIBED_WITH_SITE", "Ihre E-Mail ist bereits mit Seite zu abonnieren.");
define ("EMAIL_SUBSCRIBED_WITH_SITE", "Ihre E-Mail erfolgreich zu abonnieren.");
define ("YOUR_ACCOUNT_IS_ALREADY_ACTIVATED", "Ihr Konto wurde bereits aktiviert.");


//content views

define("YOUR_EMAIL", "Deine E-Mail");
define("LINK_YOUR_PROFILE_OR_PROJECT", "Link zu Ihrer ".$project." oder Profil");
define ("RESET_PASSWORD_LINK_SENT_SUCCESSFULLY_VIEW", "Passwort zurücksetzen Link zu Ihrer E-Mail erfolgreich gesendet.");
define ("CONFIRM_PASSWORD", "Confirm Password");
define ("SELECT_QUESTION_TYPE", "Wählen Question");


//validation social signup steps
define ("EMAIL_IS_REQUIRED", "E-Mail ist nicht erforderlich.");
define ("EMAIL_VALID_EMAIL", "Geben Sie gültige E-Mail.");
define ("PASSWORD_IS_REQUIRED", "Kennwort erforderlich ist.");
define ("ENTER_ATLEAST_EIGHT_CHARACTER", "Geben Sie atleast 8-Zeichen.");
define ("ENTER_ATLEAST_FIVE_CHARACTER", "Geben Sie atleast 5 Charakter.");
define ("YOU_CAN_NOT_ENTER_MORE_THAN_TWE_CHARACTER", "Sie können nicht mehr als 20 Zeichen.");
define ("FIRST_NAME_LAST_NAME_ARE_REQUIRED", "Vorname und Nachname erforderlich ist.");


define ("USERNAME_CANNOT_BE_MORE_THAN_CHAR", "Benutzername darf nicht mehr als 39 Zeichen sein.");
define ("ENTER_FIRSTNAME_LASTNAME_ONLY", "Geben Sie den Vornamen und erst Nachnamen ein.");
define ("ENTER_FIRSTNAME_LASTNAME", "Geben Sie den Vornamen und Nachnamen ein.");
define ("RETYPE_EMAIL_IS_REQUIRED", "E-Mail erneut eingeben ist nicht erforderlich.");
define ("EMAIL_DOES_NOT_MATCH", "E-Mail nicht übereinstimmt.");
define ("RETYPE_PASSWORD_IS_REQUIRED", "Kennwort bestätigen erforderlich.");
define ("PASSWORD_DOES_NOT_MATCH", "Password stimmt nicht.");
define ("INVITE_CODE_IS_REQUIRED", "Lade-Codes notwendig ist.");
define ("PLEASE_ACCEPT_TERMS_OF_USE", "Bitte akzeptieren Sie Nutzungsbedingungen.");
define ("THE_EMAIL_ALREADY_EXISTS", "Diese E-Mail ist bereits vorhanden");

//validation social signup steps


define ("ENTER_FIRST_NAME", "Geben Sie den Vornamen.");
define ("ENTER_FIRST_NAME_ONLY", "Geben Sie den Vornamen nur.");
define ("FIRST_NAME_CANNOT_BE_MORE_THAN_THIRTY_CHAR", "Vorname darf nicht mehr als 39 Zeichen sein.");
define ("FIRST_NAME_CANNOT_BE_LESS_THAN_THREE_CHAR", "Vorname nicht weniger als 3 Zeichen sein.");
define ("ENTER_LAST_NAME", "Geben Sie den Nachnamen.");
define ("ENTER_LAST_NAME_ONLY", "Geben Sie den Nachnamen nur.");
define ("LAST_NAME_CANNOT_BE_MORE_THAN_THIRTY_CHAR", "Nachname kann nicht mehr als 39 Zeichen sein.");
define ("LAST_NAME_CANNOT_BE_LESS_THAN_THREE_CHAR", "Nachname kann nicht weniger als 3 Zeichen sein.");
define ("ZIPCODE_IS_REQUIRED", "Postleitzahl ist erforderlich.");
define ("ENTER_VALID_ZIPCODE", "Geben Sie gültige Postleitzahl.");
define ("ENTER_VALID_URL", "Geben Sie gültige URL");

// 404 page
define ("SORRY", "Sorry");
define ("WE_CANT_FIND_THAT_PAGE", "wir` t die Seite zu finden. ");
define ("RETURN_TO_HOME_PAGE", "Zurück zur Homepage");
define ("PLEASE_BE_SURE_TO_DOUBLE_CHECK_YOUR_SPELLING_TOMAKE_SURE_YOU", "Bitte achten Sie darauf, überprüfen Sie die Schreibweise, um sicherzustellen, dass Sie die Adresse korrekt eingegeben haben, oder Sie können einfach den Kopf auf die Homepage, indem Sie auf die Schaltfläche unten.");


// main dashboard
define("YOUR_PROJECTS","Ihre ".$project."s");
define("PROJECT_FOLLOWERS","".$project." Verfolger");
define("PROJECT_FOLLOWING","".$project." folgenden");
define("YOUR_TOTAL_PROJECTS","Ihr Gesamt ".$project."");
define("TOTAL_PROJECTS","Gesamt ".$project."s");


define ("YOUR_DONATION", "Ihre Spende");
define ("YOUR_RUNNING_DONATION", "Running Investor Process");
define ("RUNNING", "Running");
define ("DONATION_RECEIVED", "Spenden empfangen");
define ("GENERATE_CODE", "Code generieren");
define ("THERE_ARE_NO_INVITATION_CODE_HISTORY", "Es gibt kein Einladungscode Geschichte.");
define ("USER_FOLLOWERS_FOLLOWINGS", "User-Verfolger / Followings");


define("NO_PROJECTS_UNDER_YOUR_BELT_YET","Nein ".$project."s unter dem Gürtel noch,");
define("BUT_YOU_HAVE_GREAT_IDEAS_ANDSO_DO_THE_PEOPLE_YOU_KNOW","aber Sie haben tolle Ideen und so auch die Menschen, die Sie kennen. Sie könnten beginnen ein ".$project." heute oder ein Team mit Freunden, um Ihre Ideen zum Leben!");
// dashboard sidebar


define ("MAIN_NAVIGATION", "Hauptnavigation");
define ("TRANSACTION_HISTORY", "Transaction History");
define ("SOCIAL_NETWORK_DETAILS", "Social Network Details");
define ("MY_PROFILE", "Mein Profil");
define ("INBOX", "Posteingang");
define ("INVITE_FRIENDS", "Freunde einladen");
define ("ACTIVITY", "Aktivität");
define ("IMAGE_VERIFICATION_WRONG", "Bild Verifikation Wrong");
define ("ENTER_VERIFICATION_CODE", "Sicherheitscode eingeben");
define ("PLEASE_ENTER_QUESTION_DETAIL", "Bitte geben Sie Frage Details");
define ("PLEASE_ENTER_VALID_EMAIL_ADDRESS", "Bitte geben Sie eine gültige E-Mail-Adresse");


define("PLEASE_SELECT_QUESTION_TYPE","Bitte wählen Sie Fragentyp");

//transaction
define("SEARCH_BY_PROJECT","Suche nach ".$project."");

define ("SEARCH_BY_EMAIL", "Suche nach E-Mail");
define ("SEARCH_BY_STATUS", "Suche nach Status");
define ("GO", "Go");
define ("SR_NO", "SB Nr.");
define ("CR_DR", "Cr / Dr");
define ("SERVICE_FEES", "Servicegebühren");
define ("PREAPPROVAL_KEY", "Pre-Zulassung Key");
define ("EXPORT_FILE", "Export-Datei");
define ("THERE_ARENO_TRANSACTION_HISTORY", "Es gibt keine Transaktion Geschichte.");


define("MOST_FUNDED_PROJECT","Die meisten Gefördert ".$project."");

define ("WEBSITE_URL", "Website-URL");
define ("BANDCAMP_URL", "Bandcamp url");
define ("ENTER_VALID_FACEBOOK_URL", "Geben Sie gültige face url");
define ("ENTER_VALID_TWITTER_URL", "Geben Sie gültige twitter url");
define ("ENTER_VALID_LINKEDIN_URL", "Geben Sie gültige LinkedIn url");
define ("ENTER_VALID_YOUTUBE_URL", "Geben Sie gültige youtube url");
define ("ENTER_VALID_BASECAMP_URL", "Geben Sie gültige Basislager url");
define ("ENTER_VALID_MYSPACE_URL", "Geben Sie gültige myspace url");
define ("ENTER_VALID_GOOGLEPLUS_URL", "Geben Sie gültige google + URL");

define ("COMMENT_POSTED_SUCCESSFULLY", "Kommentar veröffentlicht erfolgreich");
define ("THE_FILETYPE_YOU_ARE_TRYING_TO_YPLOAD_ISNOT_ALLOWED", "Der Dateityp Sie hochzuladen versuchen, ist nicht erlaubt.");
define ("SOCIAL_DETAIL_UPDATED_SUCCESSFULLY", "Social Details erfolgreich aktualisiert");
define ("SEARCH_BY_TITLE", "Suche nach Titel");
define ("CONNECT_WITH_FACEBOOK", "Verbinden mit Facebook");
define ("CONNECT_WITH_TWIITER", "Verbinden mit Twitter");
define ("CONNECT_WITH_LINKEDIN", "Verbinden mit Linkedin");
define ("WITH_FACEBOOK", "Mit Facebook");
define ("WITH_TWIITER", "Mit Twitter");
define ("WITH_LINKEDIN", "Mit Linkedin");

define("WE_COULDN_FIND_ANY_OF_YOUR_GMAIL_CONTACT_BECAUSE_YOU_HAVE_CONNECTED_GMAIL","Wir konnten zu finden alle Ihre Google Mail-Kontakte, weil Sie Google Mail verbunden haben. Klicken Sie auf die Taste, um vorübergehend eine Verbindung Gmail");

define ("WE_WILL_NEVER_SEND_OUT_ANY_EMAIL_MESSAGE_WITHOUT_ASKING", "Wir werden nie senden keine E-Mails, ohne zu fragen");
define ("FIND_FIND_FROM_GMAIL", "Finden Sie finden vom gmail");


define ("HOURS", "Stunden");
define ("MINUTES", "Minuten");
define ("MONTHS", "Monat");
define ("YEARS", "Jahre");
define ("BROWSE_IMAGE", "Durchsuchen Bild");

define("WE_COULDNOT_FIND_ANY_OF_YOUR_FRIEND_FROM_FACEBOOK_BECAUSE_YOU_HAVE_CONNECTED_FACEBOOK","Wir konnten keine Ihrer Freunde von Facebook, weil Sie nicht angeschlossen haben Facebook. Klicken Sie auf die Schaltfläche unten, um eine Verbindung");
define ("INVITE_YOUR_FRIENDS", "Freunde einladen");
define ("INVITE", "INVITE");


/********************************************************/

define("FEATURED_PROJECTS","Hervorgehobenk                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ".$project."s");
define("ROUNDING_OFF_PROJECTS","Abrundung ".$project."s");
define("POPULAR_PROJECTS","Beliebt ".$project."s");
define("SUCCESS_STORY_PROJECTS","Erfolgsgeschichte ".$project."s");
define("WHAT","Was");
define("START_NOW","Jetzt anfangen");
define("PROJECT_LIMIT","Sie können nicht mehr zu schaffen, dann %s ".$project." pro Jahr");
define("NO_PROJECTS_AVAILABLE","Nein ".$project."s verfügbar");

define("HOW_IT_WORKS","WIE FUNKTIONIERT ES");

define("OUR_MODEL_TECHNOLOGY","Unser Modell, Technologie und Team von Experten im Haus machen uns die größte Plattform in der Crowdfunding-Industrie.<br />
 Wir nutzen alle Tools in unserer Macht zu verstärken ".$project."s, so können Sie maximal sensibilisieren");
define("CREATE_COMPAIGN","Erstellen ".$project."");
define("LAUNCH_COMPAIGN","Starten ".$project."");
define("PROMOTE_TO_NETWORK","Werbung zum Netzwerk");
define("COLLECT_YOUR_FUNDS","Sammeln Sie Ihre Funds");
define("START_A_CAMPAIGN","Starte ein ".$project."");
define("SO_YOU_HAVE_GOT_AN_IDEA","Damit Sie eine Vorstellung haben? Zeichnen Sie Ihre Berufung ".$project." & einreichen.");
define("ALLOW_US_SOME_TIME","Lassen Sie uns einige Zeit, um zu überprüfen / moderieren Ihre ".$project." oder lassen Sie uns ein Feedback.");
define("TELL_YOUR_CROWD_ABOUT","Lassen Sie Ihre Zuschauer zu ".$project." und über Social-Networking-Kanäle teilen.");
define("ENCOURAGE_YOUR_CROWD_WITH","Ermutigen Sie Ihr Publikum mit Belohnungen, treffen Sie Ihre Ziel & sammeln Ihr Geld.");
define("VIEW_PROJECT", "Aussicht ".$project."");
define("COMPLETE_SUCCESS", "Complete (Erfolg)");
define("ALL_PROJECT", "Alle ".$project."s");
define("DRAFT_PROJECT", "Entwurf ".$project."s");
define("PENDING_PROJECT", "In Erwartung ".$project."s");
define("ACTIVE_PROJECT", "Aktiv ".$project."s");
define("SUCCESSFUL_PROJECT", "Erfolgreich ".$project."s");
define("CONSOLE_PROJECT", "Konsole ".$project."s");
define("FAILURE_PROJECT", "Ausfall ".$project."s");
define("DECLINE_PROJECT", "Ablehnen ".$project."s");
define("CAMPAIGN_DASHBOARD", "".$project." Armaturenbrett");


define("EXPLORE", "Erkunden");
define("WE_HELP_MAKE_ANY_IDEA_A_REALITY_THROUGH_CROWDFUNDING", "Wir helfen, eine Idee, eine Wirklichkeit durch Crowdfunding. <br /> Entdecken und unterstützen diese Fundraising- ".$project."s heute!");
define("YOUR_CAMPAIGN", "Ihre ".$project."");
define("HAS_BEEN_SUBMITTED_SUCCESSFULLY", "wurde erfolgreich gesendet.");
define("WHAT_NEXT", "Was als nächstes?");
define("TEAM_WILL_REVIEW_DETAIL_OF_YOUR_CAMPAIGN_AND_YOU_WILL_BE_NOTIFIED", "Mannschaft Details überprüfen Sie Ihre ".$project." und Sie werden per E-Mail über benachrichtigt Ihren ".$project." Status.");
define("GO_TO_DASHBOARD", "Gehen Sie zu Dashboard");
define("THE_BROWSER_YOU_ARE_USING_INTERNET", "Der Browser Sie verwenden, Internet Explorer 6 wird nicht mehr von dieser Website unterstützt. Das bedeutet, dass einige Funktionen nicht für Sie arbeiten.");
define("SO_PLEASE_TAKE_A_MOMENT_TO_CLICK_ONE_OF_THE_LINK_BELOW_TO_UPGRADE", "Also bitte einen Moment Zeit nehmen, um eine der unten aufgeführten Links klicken, um auf eine neuere Browser-Upgrade!");


define("GOOGLE_CHROME", "Google Chrome");
define("MOZILLA_FIREFOX", "Mozilla Firefox");
define("APPLE_SAFARI", "Apple Safari");


define ("COMPLETED_DONATIONS", "Abgeschlossen Investments");
define ("PENDING_DONATIONS", "Ausstehend Investments");
define ("RECEIVED", "Empfangene");
define ("CANCELLED", "Abgebrochen");
define ("PIPELINE", "Pipeline");


define ("SITE_NAME", "Standortname");
define ("LANGUAGE", "Sprache");
define('CHANGE_LANGUAGE','Sprache �ndern');

define("EMAIL_OR_PASSWORD_IS_WRONG_PLEASE_TRY_AGAIN", "E-Mail oder Passwort falsch ist, versuchen Sie es erneut ... !!!");


define("YOU_CAN_NOT_DELETE_THIS_PROJECT", "Sie können dieses Projekt nicht löschen.");
define ("MORE_FILTERS", "Zusätzliche Filter");
define ("THIS_EMAIL_IS_ADDRESS", "Diese E-Mail-Adresse ist");

define("ALREADY_EXIST_IN_OUR_SYSTEM_NDO_YOU_WANT_TO_MERGE_YOUR_TWITTER_ACCOUNT_WITH_THIS", " gibt es bereits in unserem System, \n Möchten Sie Ihre Twitter-Konto mit diesem zusammenführen möchten ");

define ("ACCOUNT_", "Konto");
define ("YOUR_ACCOUND_HAS_BEEN_MERGE_SUCCESSFULLY", "Ihr Konto wurde erfolgreich zusammenzuführen.");
define ("IT_MIGHT_BE_SOME_PROBLEM_FOR_MERGE_YOUR_ACCOUNT_PLEASE_TRY_AGAIN_LATER", "Es könnte ein Problem sein, für Ihr Konto zusammenführen, Bitte versuchen Sie es später erneut.");
define ("MY_COMMENT", "Mein Kommentar");
define ("HIDDEN", "Versteckt");
define ("INACTIVE_HIDDEN", "Inaktiv-Verborgen");
define ("INACTIVE_VISIBLE", "Inaktiv-Visible");


define ("OK", "Ok");
define ("HOUR_LEFT", "Stunden Left");
define ("NO_TIME_LEFT", "Keine Zeit mehr");
define ("TEXT_YEAR", "Jahr");
define ("TEXT_MONTH", "Monat");
define ("TEXT_DAY", "Tag");
define ("TEXT_HOUR", "Stunde");
define ("TEXT_MINUTE", "Minute");

define ("TEXT_YEARS", "Jahre");
define ("TEXT_MONTHS", "Monat");
define ("TEXT_DAYS", "Tag");
define ("TEXT_HOURS", "Stunden");


define("FUNDED", "kapitalgedeckten");
define("ALREADY_EXIST_IN_OUR_SYSTEM_NDO_YOU_WANT_TO_MERGE_YOUR_SOCIAL_ACCOUNT_WITH_THIS", "gibt es bereits in unserem System, \n Möchten Sie Ihre Social-Konto mit diesem zusammenführen möchten ");

/*account_lang.php*/
define('MEMBER_SINCE', 'Mitglied seit');
define('JOINED', 'Beigetreten');
define('LAST_LOGIN', 'Letzte Anmeldung');
define('MIN_AGO', 'min vor');
define('HR_TEXT', 'hr');
define('MIN_TEXT', 'min');
define('SEC_AGO', 'sec ago');
define('SEC', 'Sekunde');
define('DAYS', 'Tage');
define('DAY_LEFT', 'Tag verlassen');
define('TEXT_AGO', 'vor');
define('MY_ACCOUNT', 'Mein Konto');
define('EDIT_PROFILE_SETTINGS', 'Profil & Einstellungen bearbeiten');
define('EDIT_PROFILE', 'Profil bearbeiten');
define('EMAIL_NOTIFICATION_SETTING', 'Email Notification Setting');
define('USER_ALERT', 'Benutzerbenachrichtigungs');
define('ADD_FUND', 'In Fonds');
define('PROJECT_ALERT', ''.$project_name.' Aufmerksam');
define('COMMENT_ALERT', 'Kommentar Alarm');
define('PROJECT', ''.$project_name.'');
define('PAYEE_EMAIL', 'Zahlungsempfänger E-Mail');
define('MY_DONATION', 'Meine '.$funds_plural);
define('NOTIFICATION', 'Mitteilung');
define('MY_COMMENTS', 'Meine Kommentare');
define('NO_COMMENT_ADDED', 'Es gibt keinen Kommentar hinzugefügt');
define('CHANGE_PASSWORD', 'Kennwort ändern');
define('OLD_PASSWORD', 'Altes Passwort');
define('NEW_PASSWORD', 'Neues Kennwort');
define('CON_PASSWORD', 'Bestätige das Passwort');
define('INCOMING_FUND', 'eingehende Spenden');
define('END_DATE', 'Enddatum');
define('THERE_ARE_NO_INCOMING_DONATION', 'Es gibt keine eingehende '.$funds_plural.'.');
define('THERE_ARE_NO_DONATION', 'Es gibt keine '.$funds_plural.'.');
define('SAVE_CHANGES', 'Änderungen speichern');
// account page validation

define('THIS_FIELD_CANNOT_BE_LEFT_BLANK','Dieses Feld darf nicht leer bleiben.');

define('ENTER_ZIP_CODE_NOT_LESS_THAN_FIVE_CHAR','PLZ eingeben nicht weniger als 5 Zeichen.');

define('ENTER_ZIP_CODE_NOT_MORE_THAN_SEVEN_CHAR','PLZ eingeben nicht mehr als 7 Zeichen.');

define('OLD_PASSWORD_IS_REQUIRED','Altes Passwort erforderlich.');

define('CONFIRM_PASSWORD_IS_REQUIRED','Kennwort bestätigen erforderlich.');
define('PAYPAL_REQUEST_AND_RESPONSE', 'Paypal Anfrage und Antwort');
//define('ARE_YOU_SURE_YOU_WANT_TO_DELETE_SELECTED_RECORD', 'Are ypu sure?you want to delete selected record(s).');
define('DONER', 'Doner');
define('PAYPAL_REQUEST', 'Paypal anfordern');

define('ENTER_PROFILE_SLUG', 'Geben Profil Slug');
define('SPACE_ID_NOT_REQUIRED', 'Raum nicht erforderlich ist.');
define('PROFILE_SLUG_ALREADY_EXIST', 'Profil Slug ist bereits vorhanden . Bitte versuchen Sie einen anderen');
define('PROFILE_SLUG', 'Profil Slug');
// notification text
define('COMPAIGNS_YOU_BACK', ''.$project_name.'s Sie zurück');
define('NEW_PLEDGES', 'neue Versprechen');
define('NEW_COMMENTS', 'neue Kommentare');
define('NEW_FOLLOW', 'folgen NEW');
define('NEW_UPDATES', 'neue Updates');
define('COMPAIGNS_YOU_FOLLOW', ''.$project_name.' s dir folgen');
define('SOCIAL_NOTIFICATIONS', 'Social Benachrichtigungen');
define('NEW_FOLLOWERS', 'neue Anhänger');
define('COMMENT_REPLY', 'Kommentar antworten');
define('COMPAIGNS_YOU_CREATED', ''.$project_name.' s Sie erstellt');
define('SAVE_SETTINGS', 'Einstellungen speichern');

define('PROFILE_SLUG_IS_REQUIRED', 'Profil Slug -ID erforderlich');

define('SOCIAL_DETAILS', 'Sozial Details');
define('TOTAL_DONATION', 'Gesamtinvestition');
define('ACTIVE_DONATION', 'aktive Anlage');
define('SUCCESSFUL_DONATION', 'erfolgreiche Investition');
define('UNSUCCESSFUL_DONATION', 'Erfolglose Investitions');
define('THERE_ARENO_INVESTOR_PROCESS', 'Es gibt kein fließendes Investorenprozess.');

define('OCCUPATION_CANNOT_MORE_THAN_39_CHARS', 'Beruf nicht mehr als 39 Zeichen lang sein.');
define('INVALID_GOOGLE_PLUS_LINK', 'Ungültige Google Plus Link-');
define('MUST_CONTAIL_VALID_URL', 'la %s champ doit contenir une URL valide.');
define('BY', 'durch');
define('CLOSE', 'Nah dran');
define('UPDATE_DELETE_CONFIRMATION', 'Sind Sie sicher, Sie wollen diesen Update-Delete?');

/*accreditation_lang.php*/
 define('FUNDRISE_USES_THIS_INFORMATION_TO_DISTRIBUTE_YOUR_DIVIDENDS_AND_TAX_DOCUMENTS_IF_YOU_BELIEVE_YOU_HAVE_ENTERED_YOUR_DATE_OF_BIRTH_OR_SOCIAL_SECURITY_NUMBER_INCORRECTLY_PLEASE', 'Fundrise uses this information to distribute your dividends and tax documents. If you believe you have entered your date of birth or Social Security number incorrectly, please');

define('LEGAL_FIRST_NAME', 'legaler Vorname');

define('LEGAL_LAST_NAME', 'Legal Nachname');

define('THIS_MUST_BE_YOUR_PRINCIPAL_RESIDENCE', 'Dies muss Hauptwohnsitz sein.');

define('PHONE_NUMBER', 'Telefonnummer');

define('BANK_DETAIL', 'Bankdaten');

define('CHECKING', 'Überprüfung');

define('SAVINGS', 'Ersparnisse');

define('NAME_OF_BANK_AS_IT_APPEARS_ON_YOUR_PAPER_CHECKS', 'Name der Bank, wie es auf dem Papier überprüft wird.');

define('EXAMPLE', 'Beispiel');

define('CONFIRM_ACCOUNT_NO', 'Bestätigen Konto Nr');

define('CONFIRM_YOUR_ACCOUNT_NUMBER_BY_TYPING_IT_AGAIN', 'Bestätigen Sie Ihre Kontonummer durch erneute Eingabe.');

define('DIGIT_NUMBER_ON_THE_BOTTOM_LEFT_OF_YOUR_PAPER_CHECKS', '9-stellige Nummer auf der unten links auf Ihrem Papier-Schecks.');

define('PERSONAL_INFORMATION_ADDED_SUCCESSFULLY', 'Persönliche Daten erfolgreich hinzugefügt ..!');

define('ENTER_ADDRESS', 'Adresse eingeben');

define('CITY_IS_REQUIRED', 'Stadt ist erforderlich.');

define('STATE_IS_REQUIRED', 'Staat erforderlich ist.');

define('ENTER_PHONE', 'Geben Sie Ihre Telefonnummer ein.');

define('ENTER_ACCOUNT_NUMBER', 'Geben Sie Kontonummer.');

define('ENTER_BANK_NAME', 'Geben Sie Name der Bank.');

define('ACCOUNT_NUMBER_CANNICHT_BE_MORE_THAN_40', 'Kontonummer kann nicht mehr als 40 Zeichen sein.');

define('ACCOUNT_NUMBER_CANNICHT_BE_LESS_THAN_4', 'Kontonummer kann nicht weniger als 4 Zeichen sein.');

define('ENTER_ACCOUNT_NUMBER_CON', 'ENTER bestätigen Kontonummer.');

define('ENTER_ACCOUNT_NUMBER_CAN_NICHT_MATCH', 'Das Konto bestätigen No. nicht mit Kontonummer entsprechen.');

define('INVESTMENT_ACCOUNT', 'Investitions Konto');

define('FULL_NAME', 'Vollständiger Name');


define('ENTER_ROUTING_NUMBER', 'Geben Sie Routing-Nummer.');

define('ACCREDITATION_STATUS', 'Akkreditierungsstatus');

define('ACCREDITED', 'Akkreditiert');

define('EXTERNAL_BANK_ACCOUNTS', 'Externen Bankkonten');

define('PRIMARY', 'Primär');

define('PLEASE_UPLOAD_DOC_OR_PDF_FILE', 'Bitte laden Sie doc oder pdf-Datei');

define('DUE_TO_EXISTING_SECURITIES_REGULATIONS_THE_MAJORITY_OF_CROWDFUNDING_OFFERINGS_ARE_CURRENTLY_ONLY_AVAILABLE_TO_ACCREDITED_INVESTORS_PLEASE_INDICATE_YOUR_ACCREDITATION_STATUS_BELOW','Due to existing securities regulations, the majority of Crowdfunding offerings are currently only available to accredited investors. Please indicate your accreditation status below.');

define('I_AM_AN_ACCREDITED_INVESTOR', ' Ich bin ein zugelassener Anleger.');

define( 'I_AM' , 'ich bin');

define('NOT', 'NICHT');

define('AN_ACCREDITED_INVESTOR', 'ein akkreditierter Investor.');

define('ACCREDITATION_TYPE', 'Akkreditierungstyp');

define('PLEASE_CHECK_ALL_OF_THE_FOLLOWING_THAT_APPLY', 'Bitte überprüfen Sie alle der folgenden, die gelten:');

define('NET_WORTH', 'Net Worth');

define('I_HAVE_INDIVIDUAL_NET_WORTH_OR_JOINT_NET_WORTH_WITH_MY_SPOUSE_THAT_EXCEEDS_1_MILLION_EXCLUDING_THE_VALUE_OF_MY_PRIMARY_RESIDENCE', 'I have individual net worth, or joint net worth with my spouse, that exceeds $1 million (excluding the value of my primary residence).');

define('INDIVIDUAL_INCOME', 'Individual Income');

define('I_HAVE_INDIVIDUAL_INCOME_EXCEEDING', ' Ich habe individuellen Einkommen über');

define('IN_EACH_OF_THE_PAST_TWO_YEARS_AND_EXPECT_TO_REACH_THE_SAME_THIS_YEAR', ' in jedem der letzten zwei Jahre und erwarten, dass das gleiche in diesem Jahr erreichen');

define('JOINT_INCOME', 'Gemeinsame Income');

define('I_HAVE_COMBINED_INCOME_WITH_MY_SPOUSE_EXCEEDING', 'Ich habe mit meiner Frau Einkommens mitgeführt');

define('BUSINESS', 'Geschäft');

define('IN_ASSETS_AND_OR_ALL_OF_THE_EQUITY_OWNERS_ARE_ACCREDITED', 'des Vermögens und / oder die Gesamtheit der Anteilseigner akkreditiert sind');

define('I_INVEST_ON_BEHALF_OF_A_BUSINESS_OR_INVESTMENT_COMPANY_WITH_MORE_THAN', 'Ich investiere im Namen eines Unternehmens oder einer Investmentgesellschaft mit mehr als');

define('VERIFICATION', 'Überprüfung');

define('DUE_TO_EXISTING_SECURITIES_REGULATIONS_CROWDFUNDING_IS_REQUIRED_TO_DETERMINE_THAT_INVESTORS_ARE_QUALIFIED_TO_PARTICIPATE_IN_THE_INVESTMENTS_OFFERED_ON_THE_WEBSITE_PLEASE_HELP_US_DETERMINE_YOUR_ACCREDITATION_STATUS_BY_CHOOSING_ONE_OF_THE_FOLLOWING_OPTIONS', 'Due to existing securities regulations, Crowdfunding is required to determine that investors are qualified to participate in the investments offered on the website. Please help us determine your accreditation status by choosing one of the following options');

define('OPTION_A', 'Option A');

define('PHONE_CALL', 'Anruf');

define('HAVE_A_PHONE_CALL_WITH_A_MEMBER_OF_THE_CROWDFUNDING_INVESTMENTS_TEAM', 'Haben Sie einen Anruf mit einem Mitglied der Crowdfunding-Investments-Team');

define('REQUEST_A_PHONE_CALL', 'Request A Phone Call');

define('OPTION_B', 'Option B');

define('ACCREDITATION_LETTER', 'Akkreditierungsschreiben');

define('HAVE_YOUR_PERSONAL_BROKER_CERTIFIED_PUBLIC_ACCOUNTANT_INVESTMENT_ADVISOR_OR_LICENSED_ATTORNEY_COMPLETE_OUR_STANDARD_ACCREDITATION_LETTER', 'Have your personal broker, certified public accountant, investment advisor or licensed attorney complete our standard accreditation letter');

define('UPLOAD_A_COMPLETED_LETTER','Laden Sie ein ausgefülltes Schreiben');

define('DOWNLOAD_BLANK_LETTER', 'Laden Sie leere Brief');
	
define('ACCREDITATION_ADDED_SUCCESSFULLY', 'Akkreditierungsinformationen erfolgreich hinzugefügt');

define('YOU_HAVE_ALREADY_APPLIED_FOR_ACCREDITATION', 'Sie haben schon für Akkreditierung angewendet');

define('FIRST_FILL_PERSONAL_INFORMATION', 'Zuerst füllen Sie Ihre persönlichen Informationen wenden Sie sich bitte.');


define('APPROVE', 'Genehmigen');
define('ACCOUNT_NUMBER', 'Accountnummer');
define('ZIP_CODE', 'PLZ');
define("DOC_OR_PDF", "DOC, PDF, ZIP, JPG, PNG, GIF ");
define('PLEASE_UPLOAD_DOC_IMAGE_ZIP_PDF','Bitte laden Sie eine .doc, .ods , .pdf, .zip, .odt , RTF , JPG, PNG und GIF-Datei');

/*activity_lang.php*/

define('REVIEW_ALL_ACTIVITY','Bewertung Alle Aktivitäten');

define('FOLLOW_BY','Folgen Sie durch');

define('MY_COMMENT_TEXT','Mein '.$comments.' Text');

define('COMMENT_ON_PROJECT',''.$comments.' on '. $project);

define('WELCOME','Herzlich willkommen');

define('FOLLOW_PROJECT_TEXT','Folgen '.$project.' Text');

define('PROJECT_FOLLOW_BY_ME',''.$project.' folgen Sie von mir');

define('MY_POST_PROJECT','Mein Post '.$project);

define('FOLLOWER_POST_PROJECT',$follower.' post '. $project);

define('FOLLOWER_PROJECT_UPDATES',''.$follower.' '.$project.' Aktuelles');

define('THERE_ARE_NO_ACTIVITIES','Es sind keine Aktivitäten.');

define('MY_FOLLOWERS_FOLLOW','Mein '. $follower.' Folgen');

define('FOLLOWING_POST_PROJECT','Nach der Post '. $project);

define('FOLLOWING_DONATION','Nach Donation');

define('FOLLOWING_COMMENTS','Nach '. $comments);

define('FOLLOWING_UPDATES','Nach Updates');

define('FOLLOWING_GALLERY','Nach Galerie');

define('FOLLOWING_PROJECT','Nach Projekt');

define('ADD_GALLERY_IN_PROJECT','In Galerie in '. $project);

define('FOLLOWER_PROJECT_DONATION','Follower '. $project.' Spende');

define('DONATION_ON_PROJECT','Spende an '. $project);
///added for only activity messages
define('YOU_HAVE_STARTED_FOLLOWING','Sie haben begonnen, %s  %s');
define('USER_HAVE_STARTED_FOLLOWING_YOU',' %s hat  %s Sie begonnen haben.');
define('USER_HAVE_STARTED_FOLLOWING_USER',' %s gestartet wurde  %s  %s');
define('YOU_HAVE_STARTED_UNFOLLOWING','Sie haben begonnen,  %s  %s');
define('USER_HAVE_STARTED_UNFOLLOWING_YOU',' %s hat  %s Sie begonnen haben.');
define('USER_HAVE_STARTED_UNFOLLOWING_USER',' %s gestartet wurde  %s  %s');
define('USER_JOINED_SITE',' %s beigetreten %s');
define('YOU_UPDTAED_ACCOUNT_INFORMATION','Sie haben Ihre Kontoinformationen aktualisiert');
define('USER_UPDTAED_ACCOUNT_INFORMATION',' %s hat aktualisierte Kontoinformationen');
define('YOU_APPLIED_TO_BECOME_ACCREDITED','Sie angewendet akkreditierten %s werden');
define('USER_APPLIED_TO_BECOME_ACCREDITED',' %s angewendet zu werden  %s  %s');


define('USER_HAVE_STARTED_FOLLOWING_YOUR_CAMPAIGNS',' %s gestartet wurde  %s Sie erstellt %s');
define('USER_HAVE_STARTED_FOLLOWING_USER_ON_CAMPAIGNS',' %s gestartet wurde  %s  %s Sie bereits %s');
define('USER_HAVE_STARTED_UNFOLLOWING_YOUR_CAMPAIGNS',' %s gestartet wurde %s Sie erstellt %s');
define('USER_HAVE_STARTED_UNFOLLOWING_USER_ON_CAMPAIGNS',' %s gestartet wurde  %s  %s Sie bereits %s');

define('USER_HAVE_STARTED_FUNDING_USER_ON_CAMPAIGNS',' %s gestartet wurde  %s  %s Sie bereits %s');
define('USER_HAVE_STARTED_COMMNENT_USER_ON_CAMPAIGNS',' %s gestartet wurde  %s  %s Sie bereits %s');
define('USER_HAVE_STARTED_TEAMMEMBER_USER_ON_CAMPAIGNS','  %s Sie bereits ein Teammitglied am gestartet.');
define('USER_HAVE_STARTED_FOLLOWING_ADMIN',' %s gestartet wurde  %s  %s');


define('USER_HAVE_STARTED_FUNDING_USER_ON_CAMPAIGNS_UNFOLLOW',' %s gestartet wurde  %s  %s Sie bereits %s');
define('USER_HAVE_STARTED_COMMNENT_USER_ON_CAMPAIGNS_UNFOLLOW',' %s gestartet wurde  %s  %s Sie bereits %s');
define('USER_HAVE_STARTED_TEAMMEMBER_USER_ON_CAMPAIGNS_UNFOLLOW','  %s Sie bereits ein Teammitglied am gestartet.');
define('USER_HAVE_STARTED_FOLLOWING_ADMIN_UNFOLLOW',' %s gestartet wurde  %s  %s');
//updates
define('YOU_HAVE_STARTED_UPDATES','Sie haben eine neue  %s auf  %s geschrieben');
define('USER_HAVE_STARTED_UPDATES_FOR_FOLLOWING_USER',' %s hat neue  %s auf  %s Sie bereits  %s geschrieben');
define('USER_HAVE_STARTED_UPDATED_FOR_DONOR_USER',' %s hat neue  %s auf %s Sie bereits %s geschrieben');
define('USER_HAVE_STARTED_UPDATES_FOR_COMMENTER_USER',' %s hat neue  %s auf  %s Sie bereits %s geschrieben');
define('USER_HAVE_STARTED_UPDATES_FOR_TEAMMEMBER_USER',' %s hat neue %s auf  %s Sie bereits ein Team-Mitglied über die Entsendung');
define('USER_HAVE_STARTED_UPDATE_ADMIN',' %s hat neue %s auf  %s geschrieben');
//funded
define('YOU_HAVE_STARTED_FUND','Sie haben  %s  %s mit %s');
define('USER_HAVE_STARTED_FUND_PROJECT_OWNER',' %s Sie bereits erstellt ist  %s mit  %s von  %s');
define('USER_HAVE_STARTED_FUND_FOR_FOLLOWING_USER',' %s Sie bereits %s ist %s mit %s von %s');
define('USER_HAVE_STARTED_FUND_FOR_DONOR_USER',' %s Sie bereits %s ist %s mit %s von %s');
define('USER_HAVE_STARTED_FUND_FOR_COMMENTER_USER',' %s Sie bereits %s ist %s mit %s von %s');
define('USER_HAVE_STARTED_FUND_FOR_TEAMMEMBER_USER',' %s ist %s mit %s von %s Sie bereits ein Teammitglied auf');
define('USER_HAVE_STARTED_FUND_ADMIN',' %s ist %s mit %s von %s');

//commented
define('YOU_HAVE_STARTED_COMMENTED','Sie haben %s auf %s');
define('USER_HAVE_STARTED_COMMENTED_PROJECT_OWNER','  %s  %s auf %s Sie bereits erstellt');
define('USER_HAVE_STARTED_COMMENTED_FOR_FOLLOWING_USER','  %s  %s auf %s Sie bereits %s');
define('USER_HAVE_STARTED_COMMENTED_FOR_DONOR_USER','  %s  %s auf %s Sie bereits %s');
define('USER_HAVE_STARTED_COMMENTED_FOR_COMMENTER_USER','  %s  %s auf %s Sie bereits %s');
define('USER_HAVE_STARTED_COMMENTED_FOR_TEAMMEMBER_USER','  %s  %s auf %s Sie bereits ein Teammitglied auf sind');
define('USER_HAVE_STARTED_COMMENTED_ADMIN','  %s  %s auf %s');
//draft_project and submit
define('YOU_CREATED_PROJECT_DRAFT','Sie haben eine neue  %s  %s');
define('YOU_SUBMITED_PROJECT','Sie haben angegeben, eine neue  %s  %s');
define('YOU_SUBMITED_PROJECT_ADMIN',' %s hat ein neues  %s  %s für die Überprüfung');
//admin project approve
define('ADMIN_PROJECT_APPROVED_OWNER','Ihr  %s  %s wird genehmigt und von %s-Team veröffentlicht');
define('ADMIN_PROJECT_APPROVED_ADMIN','Sie haben genehmigten und veröffentlichten  %s  %s');
//admin project declined
define('ADMIN_PROJECT_DECLINED_OWNER','Ihr  %s  %s ist von %s zurückgegangen');
define('ADMIN_PROJECT_DECLINED_ADMIN','Sie haben abgelehnt  %s  %s');
//admin project deleted
define('ADMIN_PROJECT_DELETED_OWNER','Ihr  %s  %s ist von %s Team gelöscht');
define('ADMIN_PROJECT_DELETED_ADMIN','Sie haben gelöscht  %s  %s');

//admin project active
define('ADMIN_HAVE_ACTIVE_PROJECT','Sie haben aktiviert und veröffentlichte  %s  %s');
define('ADMIN_HAVE_ACTIVE_PROJECT_OWNER','Ihr  %s  %s aktiviert und durch %s-Team veröffentlicht');
define('ADMIN_HAVE_ACTIVE_PROJECT_FOR_FOLLOWING_USER','  %s  %s Sie bereits %s aktiviert und durch %s-Team veröffentlicht');
define('ADMIN_HAVE_ACTIVE_PROJECT_FOR_DONOR_USER','  %s  %s Sie bereits %s aktiviert und durch %s-Team veröffentlicht');
define('ADMIN_HAVE_ACTIVE_PROJECT_FOR_COMMENTER_USER','  %s  %s Sie bereits %s aktiviert und durch %s-Team veröffentlicht');
define('ADMIN_HAVE_ACTIVE_PROJECT_FOR_TEAMMEMBER_USER','  %s  %s Sie bereits aufgenommen, wie ein Teammitglied aktiviert und durch %s-Team veröffentlicht');
//admin project inactive
define('ADMIN_HAVE_INACTIVE_PROJECT','Sie haben inaktiviert und unveröffentlichten  %s  %s');
define('ADMIN_HAVE_INACTIVE_PROJECT_OWNER','Ihr  %s  %s ist von %s Team inaktiviert und unveröffentlichten');
define('ADMIN_HAVE_INACTIVE_PROJECT_FOR_FOLLOWING_USER','  %s  %s Sie bereits %s ist von %s Team inaktiviert und unveröffentlichten');
define('ADMIN_HAVE_INACTIVE_PROJECT_FOR_DONOR_USER','  %s  %s Sie bereits %s ist von %s Team inaktiviert und unveröffentlichten');
define('ADMIN_HAVE_INACTIVE_PROJECT_FOR_COMMENTER_USER','  %s  %s Sie bereits %s ist von %s Team inaktiviert und unveröffentlichten');
define('ADMIN_HAVE_INACTIVE_PROJECT_FOR_TEAMMEMBER_USER','  %s  %s Sie bereits aufgenommen, wie ein Team-Mitglied inaktiviert und unveröffentlichten durch %s Team');
//project feedback by onwer
define('PROJECT_FEEDBACK_OWNER_SUMBIITED_BY_OWENER','Sie haben nach %s eingereicht  %s  %s');
define('PROJECT_FEEDBACK_ADMIN_SUMBIITED_BY_OWENER','  %s  %s %s für %s eingereicht');

//project feedback by admin
define('PROJECT_FEEDBACK_OWNER_SUMBIITED_BY_ADMIN','Mannschaft %s hat %s eingereicht  %s  %s');
define('PROJECT_FEEDBACK_ADMIN_SUMBIITED_BY_ADMIN','Sie haben nach %s eingereicht  %s  %s');

//team member added
define('TEAM_MEMBER_ADDED_OWNER','Sie haben Team-Mitglied auf %s hinzugefügt');
define('TEAM_MEMBER_ADDED_ADMIN',' %s hat ein Team-Mitglied auf %s hinzugefügt');
define('TEAM_MEMBER_ADDED_USER',' %s hat ein Team-Mitglied auf %s hinzugefügt');

//inrest request
define('INTREST_REQUEST_OWNER','Sie haben %s auf %s von %s empfangen werden, um %s zu werden');
define('INTREST_REQUEST_USER','Sie haben Ihr Interesse am %s ausgedrückt in %s zu werden');
//access request
define('ACCESS_REQUEST_OWNER','Sie haben %s auf %s von %s zugreifen %s empfangen');
define('ACCESS_REQUEST_USER','Sie haben Ihr Zugriffsanforderung für %s geschickt, um Zugriff auf %s');
//upload contract document
define('UPLOAD_CONTRACT_DOCUMENT_USER','Sie haben  %s  %s Prozess Vertragskopie auf %s');
define('UPLOAD_CONTRACT_DOCUMENT_ADMIN',' %s hat  %s  %s Vertragskopie auf %s');
//approve contract document
define('APPROVED_CONTRACT_DOCUMENT_USER','Ihr %s Vertragskopie hat %s auf %s von %s Team');
define('APPROVED_CONTRACT_DOCUMENT_ADMIN','Sie haben  %s  %s Vertrag Kopie von %s auf %s');
//declined contract document
define('DECLINED_CONTRACT_DOCUMENT_USER','Ihr %s Vertragskopie hat %s auf %s von %s Team');
define('DECLINED_CONTRACT_DOCUMENT_ADMIN','Sie haben  %s  %s Vertrag Kopie von %s auf %s');
//acknowledgement contract document
define('ACKNOWLEDGEMENT_CONTRACT_DOCUMENT_USER','Sie haben  %s  %s Bestätigungsinformation auf %s');
define('ACKNOWLEDGEMENT_DOCUMENT_ADMIN',' %s hat  %s  %s Bestätigungsinformation auf %s');
//approve acknowledgement contract document
define('APPROVE_ACKNOWLEDGEMENT_CONTRACT_DOCUMENT_USER','Ihr %s Anerkennung war %s auf %s von %s Team');
define('APPROVE_ACKNOWLEDGEMENT_DOCUMENT_ADMIN','Sie haben  %s  %s Bestätigung von %s auf %s');
//declined acknowledgement contract document
define('DECLINED_ACKNOWLEDGEMENT_CONTRACT_DOCUMENT_USER','Ihr %s Anerkennung war %s auf %s von %s Team');
define('DECLINED_ACKNOWLEDGEMENT_DOCUMENT_ADMIN','Sie haben  %s  %s Bestätigung von %s auf %s');
//submitted tracking
define('SUBMITTED_TRACKING_CONTRACT_DOCUMENT_USER','Mannschaft %s hat %s Anteilschein / docuemnt Übertragung Informationen über %s');
define('SUBMITTED_TRACKING_DOCUMENT_ADMIN','Sie haben %s Anteilschein / Dokumentenübertragung Informationen über %s für %s');
//confirmed submitted tracking
define('CONFIRMED_SUBMITTED_TRACKING_CONTRACT_DOCUMENT_USER','Sie haben %s Lieferung von Anteilsschein / Dokument auf %s');
define('CONFIRMED_SUBMITTED_TRACKING_DOCUMENT_ADMIN',' %s hat %s Lieferung von Anteilsschein / Dokument auf %s');

// project success
define('ADMIN_HAVE_SUCCESS_PROJECT','  %s  %s erstellt von %s erfolgreich beendet');
define('ADMIN_HAVE_SUCCESS_PROJECT_OWNER','Ihr  %s  %s erfolgreich beendet');
define('ADMIN_HAVE_SUCCESS_PROJECT_FOR_FOLLOWING_USER','  %s  %s Sie %s erfolgreich beendet');
define('ADMIN_HAVE_SUCCESS_PROJECT_FOR_DONOR_USER','  %s  %s Sie %s erfolgreich beendet');
define('ADMIN_HAVE_SUCCESS_PROJECT_FOR_COMMENTER_USER','  %s  %s Sie %s erfolgreich beendet');
define('ADMIN_HAVE_SUCCESS_PROJECT_FOR_TEAMMEMBER_USER','  %s  %s Sie Team-Mitglied sind erfolgreich beendet');
// project unsuccess
define('ADMIN_HAVE_UNSUCCESS_PROJECT','  %s  %s erstellt von %s endete erfolglos');
define('ADMIN_HAVE_UNSUCCESS_PROJECT_OWNER','Ihr  %s  %s endete erfolglos');
define('ADMIN_HAVE_UNSUCCESS_PROJECT_FOR_FOLLOWING_USER','  %s  %s %s Sie endete erfolglos');
define('ADMIN_HAVE_UNSUCCESS_PROJECT_FOR_DONOR_USER','  %s  %s %s Sie endete erfolglos');
define('ADMIN_HAVE_UNSUCCESS_PROJECT_FOR_COMMENTER_USER','  %s  %s %s Sie endete erfolglos');
define('ADMIN_HAVE_UNSUCCESS_PROJECT_FOR_TEAMMEMBER_USER','  %s  %s Sie Team-Mitglied sind erfolglos beendet');
define('UPLOADED','Hochgeladen');
define('APPROVED_AND_CONFIRMED','Gebilligt und bestätigt');
define('ACTIVITIES','Aktivitäten');


/*content_lang.php*/
define("HAVE_DIFFERENT_QUESTION", "Haben Sie eine andere Frage?");
define("YOUR_NAME", "Ihr Name");
define("CONTACT_US_EMAIL", "Deine E-Mail");
define("CONTACT_US_NAME", "Ihr Name");
define("CONTACT_US_MESSAGE", "Einzelheiten");
define("PLEASE_ENTER_NAME", "Bitte geben Sie Ihren Namen ein");
define("PLEASE_ENTER_VALID_EMAIL", "Bitte geben Sie eine gültige E-Mail-Id");
define("PLEASE_ENTER_MESSAGE", "Bitte geben Sie Frage Details");
define("PLEASE_ENTER_QUESTIONS", "Bitte geben Sie Ihre Frage");
define("PLEASE_ENTER_QUESTIONS_ABOUT", "Bitte geben Sie Ihre Frage Typ");
define("CONTACT_US_QUESTIONS", "Ihre Frage");
define("CONTACT_US_LINK", "Link zu Ihrem Projekt oder Profil");
define("CONTACT_US_ABOUT", "Ihre Frage bezieht sich auf");
define("SELECT_TYPE", "Wählen Sie Art");
define("FUNDRAISING_ACCOUNT_LOGIN", "Fundraising Registrierung / Login");
define("FUNDRAISING_PROJECT", "Fundraising ". $project." Idee");
define("INTERNATIONAL", "Internationale");
define("OTHER", "Andere");
define("QUESTION", "Frage :");
define("DETAILS", "Einzelheiten");
define("LINKS", "Links:");
define("YOUR_QUESTION", "Ihre Frage");
define("THE_GUIDE_TO_A_SUCCESSFUL_CAMPAIGN", "Der Leitfaden für eine erfolgreiche Kampagne");
define("STILL_WANT_TO_LEARN_MORE", "Noch Möchten Sie mehr erfahren?");
define("GET_GOING", "Loslegen!");
define("FULLY_FUNDED", "Voll finanziert");
define("SUCCESS_STORYS", "Erfolgsgeschichte");
define("HELP_CENTER", "Hilfezentrum");
define("KNOWLEDGE_BASE", "Wissensbasis");
define("ARTICLES", "Articles");
define("RESPONSE_WITHIN_24_HOURS", "Antwort innerhalb 24 Stunden");
define("SUBMIT_A_REQUEST", "Einen Antrag stellen");
define("WE_ARE_HERE_TO_HELP_TO_GET_START_PLEASE_SELECT_THE_TYPE_OF_ISSUE", "Wir sind hier um zu helfen! Zu erhalten begonnen, wählen Sie bitte die Art von Problem Sie möchten, kontaktieren Sie uns über");
define("PLEASE_USE_A_FEW_WORD_TO_SUMMARIZE_YOU_QUESION", "Bitte verwenden Sie ein paar Worte zu Ihrer Frage zusammenfassen. Sie sehen unter Umständen Links zu hilfreichen Artikel erscheinen während der Eingabe");
define("PLEASE_ENTER_ADDITIONAL_DETAIL_OF_YOUR_REQUEST", "Bitte geben Sie weitere Details zu Ihrer Anfrage. Um uns zu helfen, wie schnell Sie eine Antwort wie möglich, beachten Sie bitte auch so viele spezifische Informationen wie möglich. Zum Beispiel, wenn Ihre Frage ist über ein spezielles ".$project." am");
define("PLEASE_INCLUDE_THE_NAME_OF_THE_CAMPAING_AND_A_LINK", "bitte geben Sie den Namen des ".$project." und einen Link.");
define("FIRST_AND_LAST_NAME", "Vor-und Nachname");
define("YOUR_EMAIL_ADDRESS", "deine Emailadresse");
define("NO_LEARN_MORE_CATEGORY_AVAILABLE", "Kein erfahren Sie mehr Kategorie verfügbar");
define("RELATED_ARTICLES", "In Verbindung stehende Artikel");
define("SECURITY_CHECK", "Sicherheitskontrolle");
define("JOIN_NOW", "Jetzt beitreten"); 

/*cron_lang.php*/
define('CRON_YOUR_PAYMENT_PROCESS_VIOLATED_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU','Hallo %s ,<br/><br/>Ihr Zahlungsprozess verletzt %s <br/><br/><br/>Bitte überprüfen Sie Ihre Einstellungen. Versuch es noch einmal.<br/><br/><br/>Danke.');

define('CRON_HELLO_ADMINISTRATOR_APIERROR7_PAYMENT_PROCESS_VIOLATED_DUE_TO_ERRORMESSAGE_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU','Hallo Administrator,<br/><br/> APIError(7). Zahlungsvorgang aufgrund verletzt %s <br/><br/>Donar Namen: %s <br/><br/>Donar Email : %s <br/><br/>Zahlungsempfänger E-Mail : ".$donar_email." <br/><br/>Bitte überprüfen Sie Ihre Einstellungen. Versuch es noch einmal.<br/><br/><br/>Danke.');

define('CRON_HELLO_ADMINISTRATOR_APIERROR8_PAYMENT_PROCESS_VIOLATED_DUE_TO_ERROR_MESSAGE_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU','Hallo Administrator,<br/><br/> APIError(8). Zahlungsvorgang aufgrund verletzt %s <br/><br/>Donar Namen : %s <br/><br/>Donar Email : %s <br/><br/>Zahlungsempfänger E-Mail : %s <br/><br/>Bitte überprüfen Sie Ihre Einstellungen. Versuch es noch einmal.<br/><br/><br/>Danke.');

define('CRON_HELLO_YOUR_PAYMENT_PROCESS_VIOLATED_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU','Hallo %s,<br/><br/>Ihr Zahlungsprozess verletzt. <br/><br/><br/>Bitte überprüfen Sie Ihre Einstellungen. Versuch es noch einmal.<br/><br/><br/>Danke.');

define('CRON_HELLO_ADMINISTRATOR_CATCHEXCEPTION8_PAYMENT_PROCESS_VIOLATED_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU','Hallo Administrator,<br/><br/> catchException(8). Zahlungsprozess verletzt.<br/><br/>Donar Namen : %s <br/><br/>Donar Email : %s <br/><br/>Zahlungsempfänger E-Mail : %s <br/><br/>Bitte überprüfen Sie Ihre Einstellungen. Versuch es noch einmal.<br/><br/><br/>Danke.');

define('CRON_HELLO_PAYMENT_PROCESS_NOT_COMPLETED_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU','Hello %s ,<br/><br/>Ihr Zahlungsprozess nicht abgeschlossen ist.<br/><br/><br/>Bitte überprüfen Sie Ihre Einstellungen. Versuch es noch einmal.<br/><br/><br/>Danke.');

define('CRON_HELLO_ADMINISTRATOR_APIERROR10_PAYMENT_PROCESS_NOT_COMPLETED_DUE_TO_CASE_IS_NOT_MATCH_WITH_PAYMENTEXECSTATUS_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANK_YOU','Hallo Administrator,<br/><br/> APIError(10). Zahlungsvorgang nicht abgeschlossen ist aufgrund der Fall ist nicht mit paymentExecStatus entsprechen. <br/><br/>Donar Namen : %s <br/><br/>Donar Email : %s <br/><br/>Zahlungsempfänger E-Mail : %s <br/><br/>Bitte überprüfen Sie Ihre Einstellungen. Versuch es noch einmal.<br/><br/><br/>Danke.');

/*equity_lang.php*/
define("THIS_COMPANY_MAY_BE_INTERESTED","Dieses ".$project." ist sowohl für akkreditierte & offene no-akkreditiert ".$investor." s.");
define("THIS_COMPANY_ONLY_INTERESTED","Dieses ".$project." ist nur für akkreditierte offen ".$investor." s.");
define("RAISE_DETAILS","Raise-Details");
define("FUNDING_GOAL","Finanzierung Goal");
define("CURRENT_RESERVATIONS","Aktuelle Reservierungen");
define("MINIMUM_RESERVATIONS","Minimum Reservierung");
define("INTEREST_PER_YEAR","Interest (% per year)");
define("TERM_LENGTH_MONTHS","Tigen Länge (Monats)");

define("RAISED_OF","Erhabener");
define("COMPLETE_SUCCESSB","Komplett (Erfolgs)");
define("OPEN_DATE","Öffnung Datum");
define("VIEW_ALL_FOLLOWERS","Alle Verfolger");
define("COPY_THE_CODE_BELOW_AND_PASTE","Kopieren Sie den nachfolgenden Code und fügen Sie ihn auf Ihrer Website oder Blog.");
define("EMBED_THIS_CARD_IN_YOUR_WEBSITE","Integrieren Sie diese Karte in Ihrer Website oder Blog");
define("CAMPAIGN_SHORT_LINK","Kampagne Kurzlink");
define("CONNECT_WITH_COMPANY","Verbinden mit Unternehmen");
define("HIGHLIGHTS","Höhepunkte");
define("TOP_INVESTORS","Top Investors");

//Anfrage Zugang
define("MISSING_MINIMUM_PROFILE_ITEMS","Fehlende Mindest Profil Gegenstände");
define("BEFORE_YOU_CAN_REQUEST_ACCESS_OR_MAKE_COMMITMENT","Bevor Sie Zugang anfordern oder machen Engagement für eine Sache Sie Mast habe folgendes Profil Punkt beendet:");
define("USER_PROFILE_IMAGE","Benutzerprofil Bild");
define("CURRENT_LOCATION","Aktueller Standort");
define("PLEASE_CLICK_ON_THE_COMPLETE_PROFILE_BUTTON","Please click on the Komplett Profile button to complete the required profile items. After you successfully complete your profile, you will be redirected back to this page and request will be processed automatically.");
define("REQUEST_ACCESS","Anfrage Zugang");
define("YOU_MUST_BE_AN_ACCREDITED_INVESTOR_AND_REQUEST","Dieser Abschnitt ist nur sichtbar für diejenigen, die vollständige oder teilweise den Zugang zum Datumnraum gewährt wird.");
define("YOUR_REQUEST_TO_ACCESS_UPDATES_SECTION_HAS_BEEN_SUBMITTED","Ihre Anfrage ist dem Kapitel Aktuelles zugreifen wurde erfolgreich gesendet.");

define("YOUR_REQUEST_TO_ACCESS_COMMENTS_SECTION_HAS_BEEN_SUBMITTED","Ihre Anfrage zum Zugriff ".$comments." Abschnitt wurde erfolgreich gesendet.");

define("YOUR_REQUEST_TO_ACCESS_FUNDERS_SECTION_HAS_BEEN_SUBMITTED","Ihre Anfrage den Zugriff Geldgeber Abschnitt wurde erfolgreich gesendet.");
define("YOUR_REQUEST_TO_ACCESS_DOCS_MEDIA_SECTION_HAS_BEEN_SUBMITTED","Ihre Anfrage den Zugriff docs & Media Sektion wurde erfolgreich gesendet.");
define("PLEASE_WAIT_UNTILL_YOUR_ACCESS_REQUEST_IS_GENEHMIGT","Bitte warten Sie, bis Ihr Zugriffsanforderung genehmigt.");
define("REQUEST_PENDING","Fordern Pending");
define("ACCESS_REQUEST","Zugang anfordern");
define("PERMISSION","Erlaubnis");
define("ACCREDIATION_STATUS","Akkreditierungsstatus");
define("SECTION","Abschnitt");

define("NOT_ACCREDITED", "Nicht akkreditiert");
define("SET_PERMISSIONS", "Festlegen von Berechtigungen");
define("DOCS_MEDIA","Text & Medien");
define("I_AM_INTRESTED","Ich bin interessiert");
define("INVEST_NOW","Jetzt investieren");
define("INVESTMENT_IN_PROCESS","Investitionen in Prozess");
define("VERIFY_INVESTOR_STATUS","Stellen Sie sicher, Investor-Status");
define("YOU_REQUEST_IS_STILL_PENDING","Your request to become ".$investor." is pending, please wait until your request is approved.");
define("YOU_REQUEST_IS_REJECTED_BY_OWNER","Sie verlangen, wird vom Eigentümer abgelehnt");
define("NO_PERMISSION","Keine Erlaubnis");
define("ACCEPT","Akzeptieren");
define("DENY","Leugnen");
define("ARE_YOU_SURE_YOU_WANT_TO_ALLOW","Sind Sie sicher, damit% s auf Ihrem investieren wollen ".$project." ?");

define("POST_A_COMMENT","Einen Kommentar posten");
define("POST_COMMENT","Kommentar hinzufügen");
define("DOCUMENTS","Unterlagen");
define("THIS_DOCUMENTS_IS_ONLY_VIEWABLE_TO_THOSE","Dieses Dokument ist nur sichtbar für diejenigen, die vollen Zugriff gewährt werden,");
define("MAKE_YOUR_PROJECT_LIVE_TO_SHARE_ON_SOCIAL","Machen Sie Ihre ".$project." leben, um auf Social Media Websites zu teilen.");

//=============project ==================///// 
define("DECLINE", "Ablehnen");
define("EDIT_PROJECT", "Bearbeiten ".$project);
define("DELETE_PROJECT", "Löschen ".$project);
define("PROJECT_CLOSED","Abgeschlossen");

define("SHARE", "Aktie"); 
define("OVERVIEW", "Überblick");
define("DONATIONS", "Investments");
define("RAISED_TOWARDS", "Richtung angehoben");
define("UPDATES", "Aktuelles");
define("WIDGETS", "Widgets");
define("ENDED", "Endete am");
define("VIEW_PROJECT_PAGE", "Ausblick ".$project." Seite");
define("ACTION", "Aktion");
define("FOLLOWERS", "Verfolger");
define("VIDEO_GALLERY","Video-Galerie");
define("ADD_VIDEO","+ Video hinzufügen");
define("IMAGE_GALLERY","Bildergalerie");
define("ADD_IMAGE","+ Bild");
define("EDIT", "Bearbeiten");
define("THANKS_FOR_JOINING_THE_TEAM","Vielen Dank für die zum Team!");
define("CONGRATES_YOU_ARE_NOW_PART_OF_THE","Congrats! Sie `re nun Teil der");
define("TEAM_ON","Team auf");
define("IS_CAMPAIGNING_FOR","setzt sich für");
define("AND_HAS_ADDED_YOU_AS_A_MEMBER_ONTHE_TEAM","and has added you as a member on the team. Now that you`re on board, help get the ball rolling and start letting people know about this great ".$project ."!");
define("EMBED", "Einbetten");
define("PERKS", "Vorteils");
define("THERE_IS_NO_UPDATES_FOR_THIS_PROJECT", "Es sind keine Aktualisierungen für diese ". $project);
define("THERE_IS_NO_COMMENT_FOR_THIS_PROJECT", "Es gibt keine ".$comments.". für diese ".$project);
define("THERE_IS_NO_FUNDERS_FOR_THIS_PROJECT", "Es sind keine Geldgeber für diese ".$project);
define("KEEP_PRIVATE", "Privat halten");
define("COMMENT", "Kommentar");
define("SHOW_COMMENT_AFTER_GENEHMIGT", "Kommentar hinzugefügt erfolgreich. Es wird nach der Firmeninhaber die Genehmigung zu sehen.");
define("PROJECT_GOAL", "Projektziel");
define("FUNDERS", "Geldgeber");
define("DATE", "Datum");
define("PROJECT_STATUS","Projekt-Status");
define("TOTAL_RECEIVE_PAYMENT", "Insgesamt empfangene Zahlung");
define("REMAIN_DAYS", "Verbleibende Tage");
define("DONOR", "Spender");
define("AMOUNT_DOLOR", "Menge");
define("AMOUNT_FEES", "Gebühren");
define("NO_DONATION", "Keine Investitionen");
define("PERK", "Vorteil");
define("NEXT", "Nächster");
define("DELETE", "Löschen");
define("ADD_NEW", "Neue hinzufügen");
define("SHARE_YOUR_PROJECT", "Teilen Sie Ihr $project");

define("SHARE_PROJECT_ON_TWITTER", "Teilen $project auf Twitter");
define("SHARE_ON_FURL", "Teilen $project auf Furl");
define("FURL", "aufrollen");
define("SHARE_ON_STUMBLEUPON", "Teilen $project auf StumbleUpon");
define("STUMBLEUPON", "Teilen $project auf StumbleUpon");
define("DELICIOUS", "köstlich");
define("SHARE_ON_DELICIOUS", "Teilen $project auf Delicious");
define("SHARE_ON_DIGG", "Teilen $project auf Digg");
define("SHARE_ON_TUMBLR", "Teilen $project auf Tumblr");
define("THUBMLR", " Tumblr");
define("SHARE_ON_REDDIT", "Teilen $project auf Reddit");
define("REDDIT", "reddit");
define("SHARE_ON_MIXX", "Teilen $project auf Mixx");
define("MIXX", "Mixx");
define("SHARE_ON_TECHNORATI", "Teilen $project auf Technorati");
define("TECHNORATI", "Technorati");
define("SHARE_ON_YAHOO_BUZZ", "Teilen $project auf Yahoo! Buzz");
define("YAHOO_BUZZ", "Yahoo! Buzz");
define("DESIGNFLOAT", "Teilen $project auf Designfloat");
define("SHARE_ON_BLINKLIST", "Teilen $project auf Blinklist");
define("BLINKLIST", "Blinklist");
define("SHARE_ON_MYSPACE", "Teilen $project auf Myspace");
define("MYSPACE", "Mein Platz");
define("SHARE_WITH_FARK", "Teilen $project auf Fark");
define("FARK", "Fark");
define("SUBSCRIBE_TO_RSS", "RSS-");
define("RSS", "RSS");
define("SHARE_ON_GOOGLE_PLUS", "Teilen $project auf Google+");
define("SAVE_THIS_REASON_TO_USE_AGAIN", "Speichern Sie diese Grund, wieder verwenden");
define("ENTER_REASON_WHY_YOU_WANT_DENY_USER", " Geben Grund, warum Sie auf Benutzeranforderung verweigern möchten");


define("THE_PAGE_YOU_ARE_LOOKING_FOR_IS_CURRENTLY","The page you are looking for is currently in 'DRAFT' mode, hidden from public or an invalid link.
Please contact campaign owner for further information if you think this is an error.");

define("THE_PAGE_YOU_ARE_LOOKING_FOR_IS_CURRENTLY_IN_DRAFT_MODE_OR_HIDDEN","Die Seite, die Sie suchen, ist derzeit im Modus 'Entwurf' oder von Öffentlichkeit verborgen. Bitte kontaktieren Sie Besitzer Kampagne, wenn Sie denken, dass dies ein Fehler !!!");
define("PROJECT_DEAL_TYPE",$project."Geschäftsart");
define("CURRENT_ROUND","Aktuelle Runde");
define("RECORD_DELETED_SUCCESSFULLY", "Nehmen Sie erfolgreich gelöscht.");
define("RECORD_ADDED_SUCCESSFULLY", "Nehmen Sie erfolgreich hinzugefügt.");
define("RECORD_GENEHMIGT_SUCCESSFULLY", "Nehmen Sie erfolgreich genehmigt.");
define("RECORD_DECLINED_SUCCESSFULLY", "Nehmen Sie erfolgreich zurückgegangen");
define("RECORD_REPLIED_SUCCESSFULLY", "Nehmen Sie antwortete erfolgreich.");
define("RECORD_SPAM_SUCCESSFULLY", "Nehmen Sie Spam erfolgreich.");
define("SHOW_MORE_FUNDERS","Weitere Geldgeber anzeigen");
define("COMPLETE","Komplett");
define("REQUEST","Anfordern");
define("REQUESTED_FOR","Für Erwünschte");
define("POSTED_AN_ANNOUNCEMENT", "Verwendet eine Ansage");
define("SPAM", "Spam");
define("REPORT_SPAM", "Spam melden");
define("GENEHMIGT_BTN", "GENEHMIGT");

define("FLEXIBLE_FUNDING_CAMPAIGN", "Flexible Finanzierung ". $project);
define("FIXED_FUNDING_CAMPAIGN", "Fester Funding ". $project);
define("FLEXIBLE_FUNDING", "Flexible Finanzierung");
define("FIXED_FUNDING", "Feste Finanzierung");
define("THIS_CAMPAIGN_WILL_ENDED_AND_WILL_RECEIVE_ALL_FUNDS_RAISED_UPTO", "Dieses ".$project." wird beendet, und alle bis zu sammelte Spenden empfangen");
define("THIS_CAMPAIGN_HAS_ENDED_AND_WILL_RECEIVE_ALL_FUNDS_RAISED_UPTO", "Dieses ".$project." erhalten alle Gelder, auch wenn es nicht sein Ziel zu erreichen.");
define("THIS_CAMPAIGN_HAS_ENDED_ON", "Dieses ".$project." hat endete");
define("THIS_CAMPAIGN_STARTED_ON", "Dieses ".$project." Beitrag vom");
define("AND_WILL_CLOSE", " und schließt sich an");
define("AND_CLOSED", " und schloss am");
define("SAID", " Sagte,");
define("POST_REPLY", "Briefantwort");

define("NO_DOCUMENT_HAS_BEEN_ADDED_YET", "Kein Dokument wurde hinzugefügt.");
define("NO_IMAGE_HAS_BEEN_ADDED_YET", "Kein Bild wurde bereits hinzugefügt.");
define("NO_VIDEO_HAS_BEEN_ADDED_YET", "Kein Video wurde noch nicht aufgenommen.");
define("NO_PERK_HAS_BEEN_PURCHASED_YET", "Kein Vorteil wurde noch gekauft.");
define("SHOW_MORE_COMMENTS", "Weitere Kommentare anzeigen");
define("ARE_YOU_SURE_YOU_WANT_TO_APPROVE_THIS_COMMENT", "Are you sure,You want to approve this comment?");
define("ARE_YOU_SURE_YOU_WANT_TO_DECLINE_THIS_COMMENT", "Are you sure,You want to decline this comment?");
define("ARE_YOU_SURE_YOU_WANT_TO_SPAM_THIS_COMMENT", "Are you sure,You want to spam this comment?");
define("ARE_YOU_SURE_YOU_WANT_TO_DELETE_THIS_COMMENT", "Are you sure ,You want to delete this equity?");
define("MAKE_YOUR_PROJECT_LIVE_TO_VIEW_AND_COPY_WIDGET_CODE", "Machen Sie Ihre ".$project." leben, um zu betrachten und kopieren Widget-Code.");
define("THIS_PROJECT_IS_INACTIVE_AND_NO_LONGER", "Dieses ".$project." inaktiv ist und nicht mehr zu akzeptieren ".$funds." wegen");
define("THIS_REASON", "dieser Grund");
define("REASON_INACTIVE_THIS", "Grund inaktiv dieses ".$project." nicht durch bereitgestellt ".$site_name." Mannschaft");
define("THIS_PROJECT_IS_HIDDEN_AND_NO_LONGER", "Dieses ".$project." versteckt ist und nicht mehr zu akzeptieren ".$funds." wegen");
define("UPDATES_IS_REQURED", "Updates erforderlich");
define("DENY_ACCESS_REQUEST", "Zugriff verweigern anfordern");
define("DENY_INTEREST_REQUEST", "Deny Interest anfordern");

define("COMPAIGN_CLOSED", $project."Geschlossen");

//define("HIGHLIGHTS","Höhepunkte");
define("PROJECT_RETURN_CALCULATOR","Projizierte Return Calculator");
define("PRINCIPAL","Haupt-");
define("GROSS_ANUAL_RETURN","Gross Annual Return");
define("TERM","Begriff");
define("TOTAL_RETURN","Total Return");
define("PROPERTY_SUMMARY","Eigenschaft Zusammenfassung");
//define("MARKET_SUMMARY","Marktübersicht");
define("RETURN_OVERVIEW","Zurück Übersicht");
define("ANUAL_SERVICING_FEE","Jahresvergütung");
define("NET_TO_INVESTORS","Net für Investoren");
define("VIEW_SPONCER_PROFILE_PROFILE","Das vollständige Profil von Sponsoren");
//define("PREVIOUS_FUNDING","Zurück Finanzierung");
define('SHARE_ON_FACEBOOK', "Teilen $project auf Facebook");


/*followers_lang.php*/
define('FOLLOWING_PROJECTS','folgenden '.$project.'');
	
define('THERE_ARE_NO_FOLLOWING_PROJECT','Es gibt kein folgenden '.$project.'.');

//==============inbox_details============

define('SENT_ON','gesendet am');
define('THERE_ARE_NO_MESSAGE_IN_INBOX','Es gibt keine Meldung im Posteingang.'); 
define('PLEASE_ADD_CONVERSATION_TEXT','Bitte fügen Gespräch Text'); 
define('PLEASE_ENTER_TEXT','Bitte Text eingeben'); 

//===================index.php===============
define('CONVERSATION_WITH','Gespräch mit'); 
define('BACK_TO_INBOX','zurück zu Indox');
define('REPLY','Antworten');
define('TEAM','Mannschaft');

/*invest_lang.php*/
define('DONATION_AMOUNT','Spendenbetrag');
define('ADD_SHIPPING_ADDRESS','In Lieferadresse');
define('FORM_FUND','Frm_fund');
define('DONATION','Spende');
define('STEP','Schritt');
define('PICK_YOUR_REWARD','Wählen Sie Ihre Belohnung');
define('PICK_A_PERK','Wählen Sie einen Perk');
define('NO_PERK_JUST_A_CONTRIBUTION','No Perk, just a investment!');
define('ENTER_YOUR_TOTAL_CONTRIBUTION_AMOUNT','Geben Sie Ihre Gesamtbeitrag Betrag.');
define('YOUR_INFORMATION','Ihre Information');
define('CONFIRM_EMAIL','Bestätigungs-E-Mail');
define('I_WOULD_LIKE_TO_RECEIVE_WEEKLY_NEWSLETTER','Ich möchte wöchentliche Newsletter.');
define('SHIPPING_INFORMATION','Versandinformationen');
define('PERSONALIZATION','Personalisierung');
define('SELECT_HOW_YOU_WOULD_LIKE_YOUR_CONTRIBUTION_TO_BE_DESPLAYED_ON_THE_PUBLIC_CAMPAIN_PAGE','Wählen Sie, wie Sie möchten, dass Ihr Beitrag auf dem öffentlichen angezeigt werden '.$project.' Seite.');
define('GIFT','Geschenk');
define('I_WOULD_LIKE_TO_MAKE_THIS_CONTRIBUTION_FOR_SOMEONE_ELSE','Ich möchte diesen Beitrag für jemand anderes zu machen.');
define('RECIPIENT','Empfänger');
define('PRIVACY','Privatsphäre');
define('VISIBLE_SHOW_YOUR_OR_THEIR_NAME_AND_AMOUNT','Sichtbar: Zeigen Sie Ihre (oder their) name and amount');
define('IDENTITY_ONLY_SHOW_YOUR_OR_THEIR_NAME_BUT_HIDE_THE_AMOUNT','Identity-Only: Zeigen Sie Ihre (oder their) name, but hide the amount');
define('ANONYMOUS','Anonym');
define('ADD_A_PUBLIC_COMMENT_GREAT_FOR_VOICING_YOUR_SUPPORT_AND_PERSONALYSING_YOUR_CONTRIBUTION','Fügen Sie einen öffentlichen '. $comment.' - Ideal für Intonation Ihre Unterstützung und die Personalisierung Ihrer Spende.');
define('PUBLIC_COMMENT','Öffentliche Kommentar');
define('I_WOULD_LIKE_TO_RECEIVE_EMAILS_WITH_UPDATES_FROM_THE_CAMPAIGN_OWNER','Ich möchte E-Mails mit Updates aus dem Empfangs '.$project.' Inhaber.');
define('PAYMENT_METHOD','Bezahlverfahren');
define('NO_GATEWAY_AVAILABLE','No Gateway verfügbar');
define('CONTRIBUTE_VIA_PAYPAL','Mitmachen via PayPal');
define('PAYMENT','Zahlung');
define('SHIPPING_ADDRESS','Lieferanschrift');

define('DONATION_MIN_AMOUNT','Spendenbetrag sollte größer als% s sein.');

define('DONATION_MAX_AMOUNT','Spendenbetrag sollte kleiner als% s sein.');

define('INVALID_DONATION_AMOUNT','Ungültige Spendenbetrag.');

define('EMAIL_SHOULD_NOT_BE_BLANK','E-Mail sollte nicht leer sein.');

define('INVALID_EMAIL_FORMAT','Ungültiges E-Mail-Format.');

define('EMAILS_NOT_SAME','E-Mail und E-Mail bestätigen sollte gleich sein.');

define('ENTER_SHIPPING_ADDRESS','Geben Sie die Adresse versenden.');

define('ENTER_RECIPIENT','Geben Sie Empfänger');

define('SELECT_ONE_GATEWAY', 'Bitte wählen Sie mindestens ein Gateway.');

define('CONTRIBUTE','Beitragen');
define('PAYMENT_KEY','Payment Key');
define('TRANSACTION_DETAIL','Transaction Detail');

define('CT_DT','Ct / dt');
define('SHIPPING','Versand');
define('WITHDRAWAL_DETAILS','Widerrufsrecht-Details');

define('VIEW_DETAIL','Details anzeigen');
define('YOUR_DONATION_AMOUNT_IS','Ihre Spende Betrag ist');
define('YOU_SELECTED','Sie ausgewählt');
define('ESTIMATED_DELIVERY','voraussichtliche Lieferung');
define('TOTAL_CONTRIBUTION','Gesamtbeitrag');

define('JUST_FOR_THE_CAMPAIGNER_TO_SEND','After clicking this button, you’ll be redirected to payment gateway site where you can pay.');
define('AFTER_CLICKING_THIS_BUTTON','After clicking this button, you’ll be redirected to payment gateway site where you can pay.');
define('CONTRIBUTE_VIA_STRIPE','Mitmachen via Stripe');
define('EXPIRY_MONTH','Verfallmonats');
define('EXPIRY_YEAR','Ablauf Jahr');
define('CVC_NUMBER','CVC Nummer');
define('MONTH','Monat');
define('YEAR','Jahr');
define('MY_FUNDIND','Mein Finanzierung');
define('INCOMING_DONATION','Eingehende Spenden');

//new added
define('ADD_PERK_INVEST','In Perk');
define('CHANGE_PERK','Ändern Perk');
define('CLAIMED', 'Beansprucht');

/// investor admin steps
define('INVESTER_WILL_DOWNLOAD_CONTRACT_DOCUMENTS', ' werden Vertragsunterlagen downloaden');
define('FROM_HERE', 'von hier');
define('TO_SIGN_AND_SEND_BACK', 'zu unterschreiben und zurückschicken');
define('DOWNLOAD_PAYMENT_RECEIPT_FROM_HERE', 'Laden Sie Empfang der Zahlung von hier');
define('CONFIRM_INVESTMENT_AMOUNT_YOU_HAVE_RECEIVED', 'Bestätigen '.$funds.' Betrag, den Sie per Banküberweisung oder Scheck erhalten haben,');
define('THIS_STEP_IS_PENDING', 'Dieser Schritt fehlt noch');
define('INVESTER_WILL_UPLOAD_SIGNED_COPY_OF_CONTRACT', ' wird unterschriebene Kopie des Vertragsunterlagen hochladen und sie in der Lage, unterschriebene Dokument aus diesem Schritt downloaden');
define('DOWNLOAD_SIGNED_CONTRACT_DOCUMENT_FROM_HERE', 'Download von signierten Ausschreibungsunterlagen von hier');
define('CONFIRM_DOCUMENTS_ARE_SIGNED_WELL_AND_UPTO_DATE', 'Bestätigen unterzeichneten Vertragsunterlagen sind gut und bis zu Datum unterzeichnet');
define('CLICK_COMPLETE_BUTTON_TO_MARK_THIS_STEP_AS_COMPLETED_BELOW', 'Klicken Sie auf Bestätigen, um diesen Schritt zu markieren, wie unten abgeschlossen');
define('THIS_STEP_IS_CONFIRMED_AND_COMPLETED', 'Dieser Schritt bestätigt und abgeschlossen');
define('INVESTER_WILL_GO_TO_BANK_TO_WIRE_TRANSFER', '%s gehen, um zur Überweisung oder elektronische Überweisung der Gesamtbank. '.$funds.' Menge %s mit folgenden Kontoinformationen.');
define('YOU_WILL_GO_TO_BANK_TO_WIRE_TRANSFER', 'Bitte stellen '.$funds.' von %s in folgende Bankverbindung und bieten Anerkennung der gleichen im nächsten Step.4.');
define('INVESTOR_WILL_UPLOAD_PAYMENT_RECEIPT_SNAP', 'Anleger Zahlungsbeleg Schnapp- oder Tracking-Informationen für die Zahlung er Upload / machte sie');
define('UPON_COMPLETION_OF_THIS_STEP_WILL_BE_CONFIRMED', 'Nach Beendigung dieses Schrittes, %s wird als Investor bestätigt und wird auf unter "Investors" tab aufgeführt werden '.$project.' Seite. Ebenso gut wie '.$funds.' Menge zugegeben werden, wird automatisch eingefügt in angehoben '.$funds.' %s');
define('NOTES_FROM_INVESTOR', 'jetzt als %s ist , seine Zeit, Aktien -Zertifikat dem Anleger per Post E-Mail oder E-Mail zu schicken, bitte Aktienzertifikate vorbereiten und schreiben beachten Sie unten, wie in dieser Kampagne Rechts Investor %s werden Aktienzertifikate erhalten');
define('NOW_AS_INVESTOR_IS_LEGAL_INVESTOR_IN_THIS_CAMPAIGN', 'jetzt als %s is in dieser ist legal Investor '.$project.', seine Zeit Anteilschein an den Anleger per Post E-Mail oder E-Mail , bitte bereiten Anteilscheine zu senden und zu beachten unten schreiben , wie man %s werden Aktienzertifikate erhalten');
define('RECEIPT_CONFIRMED', 'Empfang bestätigt');
define('PAYMENT_INFORMATION', 'Zahlungsinformationen');
define('ONCE_ALL_IS_VERIFY_AND_DONE_WILL_APPROVE_AND_CONFIRM', 'Sobald alles Akkreditierung und getan% s Team zu genehmigen und bestätigen Sie diesen Schritt');
define('ONCE_THIS_STEP_IS_APPROVED_AND_CONFIRMED', 'Sobald dieser Schritt gebilligt und bestätigt bitte zu Schritt 3. Zahlung gehen');
define('TEAM_WILL_DOWNLOAD_AND_VERIFY_YOUR_SIGNED', '%s Team downloaden und überprüfen Sie Ihren unterschriebenen Vertrag man von hier hochgeladen');
define('YOUR_NOTE', 'Ihre Anmerkung');
define('CLICK_HERE_TO_VIEW_PAYMENT_REC', ' Klicken Sie hier, um Ihren Zahlungsbeleg anzuzeigen:');
define('YOU_WILL_SANNOUNCE_SHARE_RECIPT', 'Sie bestätigen, und markieren Sie diesen Schritt, um er / sie erhalten hat, Aktienzertifikate und andere juristische Dokumente bekannt zu geben.');
define('RECEIVE_SHARES_DOCUMENTS', 'Erhalten Aktien / Dokumente');
define('INVESTOR_WILL_CONFIRM_SHARE_DOCUMENTS', '%s wird bestätigen, und markieren Sie diesen Schritt, um er / sie erhalten hat, Aktienzertifikate und andere juristische Dokumente bekannt zu geben.');
define('NOTE','Notiz');
define('APPROVED','Genehmigt');
define('INV_ENTER_YOUR_BANK_AND_CHECK_INFORMATION','Geben Sie / Bearbeiten Sie Ihre Bank und überprüfen Sie Informationen, um% s vom Anleger zu erhalten.');
define('REJECT_COMMENT','Ablehnen Kommentar');
define('REJECT_REASON','Geben Sie Ablehnungsgrund.');
define('SENDMESSAGE_TO_SITE_TEAM','Nachricht senden zu% s-Team');
define('SENDMESSAGE_TO_INVESTOR_NAME','Nachricht an% s');
define('PROCESS','Verarbeiten');

define('YOUR_PAYMENT_ACKNOWLEDGEMENT_INFORMATION_HAS_BEEN_SUBMITTED_SUCCESSFULLY', 'Ihre Zahlungsbestätigungsinformation wurde erfolgreich gesendet.');
define('TEAM_WILL_REVIEW_AND_CONFIRM_YOUR_PAYMENT_SHORTLY', ' Team überprüfen und bestätigen Ihre Zahlung in Kürze.');
define('STEP_2_HAS_BEEN_CONFIRMED_AND_COMPLETED_PROCEED_TO_THE_STEP_3_PAYMENT', 'Schritt 2 wurde bestätigt und ergänzt. Fahren Sie mit Schritt 3. Zahlung');
define('STATUS_HAS_BEEN_REJECTED_SUCCESSFULLY', 'Status erfolgreich abgelehnt');
define('YOURSHARE_DOCUMENT_TRANSMISSION_INFORMATION_SUBMITTED_AND_SENT', 'Vorgelegt und schickte Ihrem share / Dokument Übertragungsinformationen');
define('STEP_4_HAS_BEEN_CONFIRMED_AND_COMPLETED_PROCEED_TO_THE_STEP_5_SHARES_DOCUMENTS', 'Schritt 4 wurde bestätigt und ergänzt. Fahren Sie mit Schritt 5. Aktien / Dokumente');	
define('SHARE_DOCUMENT_DELIVERY_HAS_BEEN_CONFIRMED_AND_MARKED_AS_DELIVERED_SUCCESSFULLY', 'Share / Dokumentlieferung bestätigt wurde und markiert als erfolgreich übermittelt.');
define('CLICK_HERE_TO_VIEW_YOUR_PAYMENT_RECEIPT', 'Klicken Sie hier, um Ihren Zahlungsbeleg anzuzeigen');
define('YOUR_INVESTMENT_DONE_SUCCESSFULLY','Ihr '.$funds.' erfolgreich durchgeführt.');


/*invites_lang.php*/
define("GMAIL", "Gmail");

define("MULTIPLE_EMAIL_ADDRESS_SHOULD_BE_SEPERATED_WITH_COMMA", "Mehrere E-Mail-Adressen sollten mit Komma getrennt werden.");
define('YOU_ALREADY_SENT_INVITATION_TO_THIS_EMAIL', 'Sie already sent Einladung an diese E-Mail.');
define('YOU_CAN_NOT_INVITE_YOUR_SELF', 'Sie können lädst du nicht deine Selbst.');
define('RECIPIENT_MESSAGE_IS_REQUIRED', 'Empfänger Meldung ist erforderlich.');
define('INVITATION_IS_FAILED', 'Einladung fehlgeschlagen.');
define('SHOW_SELECTED_FRIENDS', 'Freunde Zeige');
define('WE_COULDN_T_FIND_ANY_OF_YOUR_FRIENDS_FROM_FACEBOOK_BECAUSE_YOU_HAVEN_T_CONNECTED_FACEBOOK_AND_REKAUDO_CLICK_THE_BUTTON_BELOW_TO_CONNECT', 'Wir konnten alle Ihre Freunde aus Facebook zu finden, weil Sie angeschlossen Facebook haben. Klicken Sie unten auf die Schaltfläche, um zu verbinden.');
define('INVITE_YOUR_FRIENDS_TO', 'Laden Sie Ihre Freunde');
define('WE_COULDNT_FIND_ANY_OF_YOUR_GMAIL_CONTACTS_BECAUSE_YOU_HAVENT_CONNECTED_GMAIL_AND_REKAUDO_CLICK_THE_BUTTON_TO_TEMPORARILY_CONNECT_GMAIL_AND_REKAUDO', 'Wir konnten zu finden alle Ihre Google Mail-Kontakte, weil Sie Google Mail verbunden haben. Klicken Sie auf die Taste, um vorübergehend eine Verbindung Gmail');
define('WE_WILL_NEVER_SEND_OUT_ANY_EMAIL_MESSAGES_WITHOUT_ASKING', '  Wir werden nie senden keine E-Mails, ohne zu fragen.');
define('FRIENDS_ON', 'Freunde auf');
define('YAHOO_MAIL', 'Yahoo Mail');
define('INVITE_SENT', 'Einladung gesendet');
define('SELECT_ALL', 'Alle auswählen');
define('DESELECT_ALL', 'Alle abwählen');
define('INVITE_ALL', 'Alle einladen');
define('WE_COULDNT_FIND_ANY_OF_YOUR_YAHOO_CONTACTS_BECAUSE_YOU_HAVENT_CONNECTED_YAHOO_AND_REKAUDO_CLICK_THE_BUTTON_TO_TEMPORARILY_CONNECT_YAHOO', 'Wir konnten zu finden alle Ihre Yahoo-Kontakte, weil Sie verbunden Yahoo haben. Klicken Sie auf die Taste, um vorübergehend eine Verbindung Yahoo');
define('YOUR_AFFILIATE_REQUEST_IS_IN_PENDING', 'Ihre Affiliate-Anfrage ist anhängig.');
define('SORRY_AN_ERROR_OCCURES_PLEASE_TRY_AGAIN', 'Es tut uns leid Apparates ein Fehler auftritt versuchen Sie es erneut ..!');
define('YOUR_INVITATION_SENT_SUCCESSFULLY', 'Ihre Einladung erfolgreich gesendet');
define('SEND_ALL', 'Sende alle');
define('FACEBOOK_WILL_FETCH', 'Facebook wird nur die Freunde, die mit Anwendung sind zu holen (%s)');
 
 /*message_lang.php*/
define('SEND_MESSAGE','Nachricht senden');
define('SUBJECT','Gegenstand');
define('MESSAGE_LIST','Nachrichtenliste');
define('SENDER','Absender');
define('RECEIVER','Empfänger');
define('MESSAGE_CONVERSATION','Nachricht Gespräch');
define('VIEW_CONVERSATION','Nachricht anzeigen');
define('CONVERSATION_MESSAGE','Unterhaltung Nachrichten');
define('CONVERSATION_OF_MESSAGE','Gespräch Of Message');
define('CONVERSATION_DATE','Gespräch Datum');
define('SEND_A_MESSAGE_TO','Schicke eine Nachricht an');
define('YOUR_MESSAGE_HAS_BEEN_SUCCESSFULLY','Deine Nachricht wurde erfolgreich verschickt');
define('SUBJECT_IS_REQUIRED','Betreff wird benötigt');
define('MESSAGE_IS_REQUIRED','Nachricht benötigt');
define('SEND_A_REPLY','Antworten');
define('CONVERSATION','Gespräch');

/*Profile_lang.php*/
define("FOLLOWING","folgenden");

define("FOLLOW","Folgen");

define("UNFOLLOW","Entfolgen");

define("LOCATION","Ort");

define("CONTACT_ME","Kontaktiere mich");

define("ALSO_FIND_ME_ON","Finden Sie mich auch auf");

define("MY_SPACE","Mein Platz");

define("CREATED_PROJECTS","Erstellt ".$project);

define("CONTRIBUTE_NOW","Jetzt Mitmachen");

define("NO_PROJECTS_FOUND","Nein ".$project."s gefunden");

define("CONTRIBUTIONS","Beiträge");

define("NO_CONTRIBUTIONS","keine Beiträge");

define("SEE_MORE","mehr sehen");

define("COMMENTS",$comment);

define("AT","Beim");

define("ON","Am");

define("COMMENTED_ON","kommentiert");

define("NO_COMMENTS_FOUND","Nein ".$comment." gefunden");

define("MY_FOLLOWERS","Meine Follower");

define("NO_FOLLOWERS_FOUND","Nein ".$follower."s gefunden");

define("MY_FOLLOWINGS","Meine Followings");

define("NO_FOLLOWINGS_FOUND","Keine Anhängerschaft gefunden");

define("ABOUT_ME","Über mich");

define("NA","N / A");

define("SKILLS","Fähigkeiten");

define("INTEREST","Interessieren");

define("CONTRIBUTE_AMOUNT","Mitmachen Betrag");

define("TOTAL_RAISED_FOR_OWN_PROJECTS","Summe für eigene Raised ".$project."s");

define("TOTAL_DONATED_TO_OTHER_PROJECTS","Insgesamt gespendet zu anderen ".$project."s");


define("PROFILE","Profil");

define("CONTRIBUTED","beigetragen");

define("SHOW_MORE_CONTRIBUTIONS","Weitere Beiträge anzeigen");

define("THIS_USER_HAS_NOT_ENTERED","Dieser User hat noch keine Informationen über sich eingetragen.");

define("THIS_USER_HAS_NOT_ADDED_ANY_SKILLS_YET","Dieser User hat keine Fähigkeiten hinzugefügt.");

define("THIS_USER_HAS_NOT_ADDED_ANY_INTEREST_YET","Dieser Benutzer hat kein Interesse eingetragen.");

define('RECIPIENT_MAIL_INVALID','Empfänger E-Mail ist ungültig');
 
define('INVITATION_HAS_BEEN_SENT_TO','Einladung wurde gesendet ');


/*social_networking_lang.php*/
define('MANAGE_YOUR_SOCIAL_NETWORK_DETAILS', 'Verwalten Sie Ihre Social Network-Details');
define('FACEBOOK_URL', 'Facebook-URL');
define('TWITTER_URL', 'Twitter URL');
define('LINKEDIN_URL', 'Linkedln URL');
define('GOOGLE_PLUS_URL', 'Google Plus URL');
define('BASECAMP_URL', 'Basecamp URL');
define('YOUTUBE_URL', 'Youtube-URL');
define('MYSPACE_URL', 'MySpace-URL');


define('INVALID_FACEBOOK_LINK','Ungültige facebook Link-');
define('INVALID_TWITTER_LINK','Ungültige Twitter Link-');
define('INVALID_LINKEDIN_LINK','Ungültige LinkedIn Link-');
define('INVALID_BANDCAMP_LINK','Ungültige Bandcamp Link-');
define('INVALID_YOUTUBE_LINK','Ungültige YouTube Link-');
define('INVALID_MYSPACE_LINK','Ungültige myspace Link-');
define('INVALID_IMDB_LINK','Ungültige imdb Link-');


/*start_equity_lang.php*/

define("DRAFT_YOUR_CAMPAIGN", "Zeichnen Sie Ihre ".$project);
define("ENHANCE_YOUR_CAMPAIGN", "verbessern Sie Ihre ".$project);
define("PREFUNDING_DETAILS", "Pre Förderung Details");
define("ENHANCE_AND_MAKE_YOUR_COMPAIGN", "Verbessern Sie und machen Sie Ihre ".$project." es attraktiv und überzeugend zu Ihrem Erfolg-Verhältnis zu erhöhen, um.");

define("FUNDRAISING_DETAILS", "Fundraising-Detail");
define("DEAL_TYPE", "Geschäftsart");
define("DEAL_HIGHLIGHTS", "Deal Höhepunkte");
define("ELEVATOR_PITCH", "Elevator Pitch");
define("CHOOSE_THE_FOUR_MOST_BUZZ_WORTHY", "Wählen Sie die 4 die meisten Buzz-würdig hightlights über Ihre aktuellen Runde, die Investoren beeindrucken werden. Dies wird in der Nähe der Spitze der viel zu sehen sein.");
define("ADD_COVER_PHOTO", "Hinzufügen Cover Photo");
define("VIDEO_URLC", "Video-URL");
define("INVESTORS", "Investoren");
define("DEAL_DOCUMENTS", "Deal Dokumente");
define("PREVIOUS_FUNDING", "Zurück Finanzierung");


define("I_HAVE_READ_AND_UNDERSTAND", " Ich habe gelesen und Projektrichtlinien Crowdfunding zu verstehen.");
define("COMPANY_PROFILE", "Firmenprofil");
define("COMPANY_BASIC", "Unternehmensgrund");
define("COMPANY_NAME", "Name der Firma");
define("EQUITY_CROWDFUNDING_URL", $project." Crowdfunding-URL");
define("WEBSITE_URLC", "Webadresse");
define("YEAR_FOUNDED", "Gründungsjahr");
define("HEADQUATER_CITY", "Zentrale Stadt");
define("HEADQUATER_STATE", "Headquarters State");
define("HEADQUATER_COUNTRY", "Headquarters Land");
define("COMPANY_CATEGORY", "Unternehmenskategorie");
define("COMPANY_INDUSTRY", "Firma Branche");
define("SELECT_A_CATEGORY", "Wählen Sie eine Kategorie ...");
define("SELECT_INDUSTRIES", "Wählen Industries ...");
define("ASSETS_UNDER_MANAGEMENT", "Assets Under Management");
define("SQUARE_FOOTAGE_DEVELOPED", "Flächenangaben Entwickelt");
define("SQUARE_FEET", ", 000 square feet");
define("COMPANY_ASSETS", "Unternehmens Assets");
define("UPLOAD_YOUR_LOGO", "in Ihrem Profil Laden Sie Ihr Logo.");
define("COMPANY_OVERVIEW", "Selbstdarstellung");

define("COMPANY_CONNECT", "Firma Connect");
define("CONNECT_YOUR_SOCIAL_NETWORKS", "Schließen Sie Ihren sozialen Netzwerken und Website Diese erziehen und zeigen Sie Ihre Glaubwürdigkeit..");
define("EMAIL_CONTACT", "E-Mail-Kontakt");
define("COMPANY_EMAIL", "Firma E-Mail");
define("sozial", "Social");
define("ENTER_THE_URLS_FROM_YOUR", "Geben Sie die URLs Ihrer Social Profile");
define("LINKED_IN", "Linked In");
define("LINKED_IN_URL", "Linked In URL");
define("FACEBOOK_URLC", "Facebook URL");
define("TWITTER_URLC", "Twitter-URL");
define("TEAM_MEMBER_BIO", "Team Member Bio");
define("MEMBER_PHOTO", "Mitglied Photo");
define("ROLE_TITLE", "Rolle / Titel");
define("VISIBLE_ON_PROFILE", "im Profil angezeigt?");
define("MAKE_ADMIN", "Make Admin?");
define("TEAM_MEMBERS_CONTENT", "Fügen Sie Ihren Teammitgliedern und Beratern, damit die Menschen wissen, wer beteiligt ist jeder Akteur stellt eine leistungsfähige und persönliche Art und Weise, um die Stärke Ihres Unternehmens zu demonstrieren..");
define("MEMBER_ROLE", "Mitglied Rolle");


define("ACCESS_LEVEL", "Zugriffsebene");
define("VISIBLE_PROFILE", "im Profil angezeigt");
define("TEAM_MEMBER_TYPE", "Team Member");
define("ADD_TEAM_MEMBER", "+ hinzufügen Team Member");
define("ADD_COMPANY", "Firma hinzufügen");
define("ALL_DETAIL_OF_YOUR_DEAL", "Alle Details Ihrer Transaktion wird in diesem Abschnitt behandelt werden Dies ist, wo Sie die Bedingungen Ihrer Deal definieren und wichtige Dokumente..");
define("MINIMUM_RAISE", "Minimum Raise");
define("MAXIMUM_RAISE", "Maximale Raise");
define("COMPANY_NAME_ALREADY_EXISTS", "Unternehmensname ist bereits vorhanden");
define("PLEASE_CLICK_ON_COMPANY_INDUSTRY", "Bitte klicken Sie auf Firma Branche zu löschen");
define("YOU_HAVE_ALREADY_APPLIED_THAT", "haben Sie bereits, dass die Industrie angewendet");
define("PLEASE_SELECT_COMPANY_INDUSTRY", "Bitte wählen Firma Branche");
define("YOU_MAY_ONLY_SELECT_UP_TO", "Sie können nur wählen Sie bis zu% s Industrie");
define("PNG_JPG_OR_GIF_RECOMENDED_SIZE", "PNG, JPG oder GIF <br/> <b> Empfohlene Größe: </ b> <br/> 170 x 170 Pixel <br> Max Größe: 2 MB");
define("URL_ALREADY_EXISTS", "url ist bereits vorhanden");


define("TERMS_OF_SERVICE", "Nutzungsbedingungen");
define("FUNDRAISING_GOAL", "Fundraising-Ziel");
define("CLOSING_DATE_THIS_CAN_BE_EXTENDED", "Abschlussdatum");
define("CLOSING_DATE", "Abschlussdatum");
define("IS_YOUR_COMPANY_CURRENTLY_FUNDRAISING", "Ist Ihr Unternehmen derzeit Fundraising?");
define("AMOUNT_RAISED_IN_CURRENT_ROUND", "Betrag in aktuellen Runde Volkszählung");
define("DATE_ROUND_OPENED", "Datum Runde eröffnete");
define("SELECT_THE_DEAL_TYPE_THAT_FITS_YOUR_CURRENT_ROUND", "Wählen Sie das viel Typ, die Ihre aktuellen Runde passt.");
define("EQUITY", $project);



define("PRICE_EQUITY_IN_YOUR_COMPANY", "Preis von Eigenkapital in Ihrem Unternehmen");
define("CONVERTIBLE_NOTE", "Wandelanleihe");
define("DEBT_THAT_CAN_CONVERT_TO_EQUITY", "Schulden, die zum Eigenkapital umwandeln kann");
define("DEBT", "Debt");
define("BORROW_FROM_INVESTORS_AT_SET_INTEREST_TERMS", "Ausleihen von Investoren zu festen Zinskonditionen");
define("REVENU_SHARE", "Revenue Share");
define("SELL_A_PORTION_OF_YOUR_FUTURE_REVENUES_TO_YOUR_INVESTORS", "Verkaufen Sie einen Teil Ihrer künftigen Einnahmen, um Ihre Investoren");
define("AVAILABLE_SHARES", "Verfügbare Aktien");
define("WHAT_PERCENTAGE_OF_THE_TOTAL_COMPANY", "Wie viel Prozent des Eigenkapitals der gesamten Gesellschaft ist verfügbar?");
define("COMPANY_VALUATION", "Unternehmensbewertung");

define("EQUITY_AVAILABLE", $project ."Verfügbar.");


define("PRICE_PER_SHARE", "Preis per Share");
define("TERM_LENGTH", "Lauflänge");
define("VALUATION_CAP", "Bewertungs Cap");
define("CONVERSATION_DISCOUNT", "Conversion Discount");
define("WARRANT_COVERAGE", "Warrant Coverage");
define("RETURNT", "Rückgabetyp");
define("SELECT_RETURN", "Wählen Rückkehr");
define("GROSS_REVENUES", "Umsatzerlöse");
define("GROSS_PROFITS", "Bruttogewinn");
define("RETURN_PERCENTAGE", "Return Prozentsatz");
define("MAXIMUM_RETURN", "maximale Rendite");
define("INVESTMENT_AMOUNT", "Invesment Betrag");
define("PAYMENT_FREQUENCY", "Zahlung Frequenz");
define("SELECT_AYMENT_FREQUENCY", "Wählen Sie die Zahlung Frequenz");
define("BIOMONTHLY", "Alle zwei Monate");
define("QUARTERLY", "Quarterly");


define("PAYMENT_START_DATE", "Payback Startdatum");
define("SELECT_PAYMENT_DATE_TYPE", "Wählen Zahltag Typ");
define("EXACT_DATE", "Genaues Datum");
define("NUMBER_OF_MONTHS_FROM", "Anzahl der Monate ab Finance Close");

define("NEXT_TO_STEP_2", "Weiter zu Schritt 2");
define("UPDATE_TEAM_MEMBER", "Update Team Member");
define("COMPANY_CURRENTLY_FUNDRAISING", "Gesellschaft derzeit Fundraising");


define("SELECT_TEAM_MEMBER_TYPE", "wählen Team Member");
define("RECOMMENDED_SIZE_800_450", "PNG, JPG oder GIF <br/> <b> Empfohlene Größe: </ b> <br/> 800 x 450 Pixel");
define("OPTIONAL", "(optional)");
define("PLUS_ADD_PERK", "+ hinzufügen Perk");
define("NEXT_TO_STEP_3", "Weiter mit Schritt 3");


define("MAIN_VIDEO_OR_IMAGE_TO_BE_DISPLAYED", "Haupt Video oder Bild, um an der Spitze der angezeigt werden Ihre ".$project." Seite.");

define("YOUTUBE_OR_VIMEO", "Youtube oder Vimeo");
define("PLEASE_CLICK_ON_TEAM_MEMBER_TO_DELETE", "Bitte klicken Sie auf Team-Mitglied zu löschen");
define("PLEASE_CLICK_ON_TEAM_MEMBER_TO_EDIT", "Bitte klicken Sie auf Team-Mitglied zu Bearbeiten");
define("PLEASE_CLICK_ON_PERK_TO_DELETE", "Bitte klicken Sie auf Perk zu löschen");
define("PLEASE_CLICK_ON_PERK_TO_EDIT", "Bitte klicken Sie auf Vergünstigung zum Bearbeiten");
define("INVALID_DATA", "Ungültige Daten");

define("UPDATE_PERK", "Update Perk");
define("ENTER_AND_CONFIRM_YOUR_COMPANY", "eingeben und bestätigen Kontoinformationen Ihres Unternehmens, um am Ende, wenn Ihr finanziert bekommen ".$project." ist voll finanziert.");
define("BANK_ACCOUNT_DETAIL", "Bank Account Detail");
define("NAMEOF_BANK_AS_IT_APPEARS", "Name der Bank, wie es auf dem Papier überprüft wird.");
define("ACCOUNT_TYPE", "Account");
define("SELECT_ACCOUNT_TYPE", "Wählen Sie Konto");
define("ACCOUNT_NUMBERC", "Accountnummer");
define("EXAMPLE_ACCOUNT_NUMBER", "<strong> Beispiel: </ strong> 0001 23456 789");
define("CONFIRM_ACCOUNT_NUMBER", "Bestätigen Kontonummer");
define("CONFIRM_YOUR_ACCOUNT_NUMBER_BY_TYPING_AGAIN", "Bestätigen Sie Ihre Kontonummer durch erneute Eingabe.");
define("ROUTING_NO", "Routing Nein.");
define("NINE_DIGIT_NUMBER_ON_THE_BOTTOM_LEFT", "9-stellige Nummer auf der unten links auf Ihrem Papier-Schecks <br> (<strong> Beispiel: </ strong> 123456789).");
define("FINISH", "Beenden");

define("AN_EXECUTIVE_SUMMARY_AND_TERM_SHEET_ARE_REQUIRED", ". Eine Zusammenfassung und Term Sheet benötigt werden Es können keine zusätzlichen Dokumente hinzufügen und markieren Sie sie als vertraulich, den Zugang der Anleger zu steuern.");
define("EXECUTIVE_SUMMARY", "Zusammenfassung");
define("TERM_SHEET", "Term Sheet");
define("ADD_CUSTOM_DOCUMENT", "+ Fügen Sie benutzerdefinierte Dokument");
define("NEXT_STEP_FOUR", "Weiter mit Schritt 4");
define("ADD_ADDITIONAL_DOCS_OR_FILES", "Hinzufügen zusätzlicher Dokumente oder Dateien, die relevant sein Anleger würden.");


///added by rakesh
define("PRE_FUNDING_DETAILS", "Pre Förderung Details");
define("ADD_YOUR_PRE_FUDING", "Fügen Sie Ihre vorge Finanzierung und aktuelle Investoren Details Verwalten Sie Ihre Dokumente aus demselben Abschnitt..");
define("EQUITY_PREVIOUS_FUNDING", "Zurück Funding");
define("ADD_FUNDING_FROM_PREV", "Finanzierung ab zuvor geschlossenen Runden Enthalten Sie nicht die Finanzierung innerhalb Ihrer aktuellen Runde..");
//define("FUNDING_SOURCE","Funding Source");


define("SELECT_FUNDING_SOURCE", "Wählen Finanzierungsquelle");
define("EQUITY_FUNDING_TYPE", "Finanzierung Type");
define("SELECT_EQUITY_FUNDING_TYPE", "Wählen Sie Art der Fördermittel");
define("FUNDING_AMOUNT", "Förderbetrag ");
define("FUNDING_DATE", "Finanzierung Datum");
define("ADD_PREV_FUNDING_SOURCE", "+ hinzufügen Zurück Finanzierungsquelle");
define("UPDATE_PREV_FUNDING_SOURCE", "Aktualisieren Zurück Finanzierungsquelle");
define("PLEASE_CLICK_ON_PREVIUOS_FUNDING_TO_DELETE", "Bitte klicken Sie auf Zurück Finanzierung zu löschen");
define("PLEASE_CLICK_ON_PREVIUOS_FUNDING_TO_EDIT", "Bitte klicken Sie auf Zurück Finanzierung zu Bearbeiten");
define("PLEASE_CLICK_ON_INVESTOR_TO_DELETE", "Bitte klicken Sie auf Anleger löschen");
define("PLEASE_CLICK_ON_INVESTOR_TO_EDIT", "Bitte klicken Sie auf INVESTORto Edit");
define("RAISE_SOURCE", "Raise Source");

//define("INVESTORS", "Investors");
define("SELECT_INVESTOR_TYPE", "Select Investor Typ");
define("INVESTOR_TYPE", "Investor Typ");
define("INVESTOR_PHOTO", "Investor Photo");
define("INVESTOR_BIO", "Investor Bio");
define("ADD_INVESTORS", "+ Investor hinzufügen");
define("UPDATE_INVESTORS", "Update Investor");
define("UPDATE_VIDEO", "Update-Video");
define('NEW_RECORD_HAS_BEEN_ADDED_SUCCESSFULLY', 'Neuer Datensatz wurde erfolgreich hinzugefügt.');
define("URL", "URL");

define("ADD_MORE_VIDEO","Fügen Sie mehr Videos auf Ihrem bezogenen ".$project." Diese werden in der 'Docks & Medien' Register zeigen, bis Sie Ihre ".$project." Seite.");

define("PLEASE_CLICK_ON_VIDEO_TO_EDIT", "Bitte klicken Sie auf Video zu bearbeiten");
define("PLEASE_CLICK_ON_VIDEO_TO_DELETE", "Bitte klicken Sie auf Video zu löschen");
define("UPDATE_IMAGE", "Update-Bild");
define('RECORD_HAS_BEEN_DELETED_SUCCESSFULLY', 'Die Bilanz wurde erfolgreich gelöscht.');
define("ADD_MORE_IMAGE","Fügen Sie mehr Bilder im Zusammenhang mit Ihrer ".$project.". Diese werden in der 'Docks & Medien 'Register zeigen, bis Sie Ihre ".$project." Seite.");

define("PLEASE_CLICK_ON_IMAGE_TO_EDIT", "Bitte klicken Sie auf das Bild um es bearbeiten");
define("PLEASE_CLICK_ON_IMAGE_TO_DELETE", "Bitte klicken Sie auf das Bild zu löschen");
define("PLEASE_CLICK_ON_DEAL_DOCUMENT_TO_DELETE", "Bitte klicken Sie auf Deal Dokument zu löschen");
define("PLEASE_CLICK_ON_DEAL_DOCUMENT_TO_EDIT", "Bitte klicken Sie auf Deal-Dokument zu bearbeiten");
define("CONFEDENTIAL", "Vertraulich");
define("ALL_ACCESS", "All Access");
define("PRIVATEC", "Privat");
define("PUBLICC", "Public");

define("PLEASE_UPLOAD_DOC_PPT_ZIP_PDF", "Bitte laden Sie eine .doc, .ods , .pdf, .zip, .ppt, .xls, .odt und RTF-Datei");
define("SORRY_THIS_FILE_IS_TOO_LARGE_PLEASE_SELECT_A_FILE_THAT_IS_LESS_THAN_TWO_MB", "Leider ist diese Datei zu groß Bitte wählen Sie eine Datei, die weniger als 2 MB ist.");
define("MINIMUM_RAISE_SHOULD_NOT_BE_LESS_THAN", "Minimum Raise sollte nicht weniger als sein");
define("MAXIMUM_RAISE_SHOULD_NOT_BE_GREATER_THAN", "Maximum Raise sollte nicht größer als sein");
define("NUMBER_OF_MONTHS", "Anzahl der Monate");

define("PRICE_PER_SHARE_SHOULD_BE_BETWEEN", "Preis je Aktie soll zwischen");
define("EQUITY_AVAILABLE_SHOULD_BE_BETWEEN", "".$project." Verfügbar sollte zwischen");

define("COMPANY_VALUATION_SHOULD_BE_BETWEEN", "Unternehmensbewertung sollte zwischen");
define("INTEREST_SHOULD_BE_BETWEEN", "Das Interesse sollte zwischen");
define("TERM_LENGTH_SHOULD_BE_BETWEEN", "Lauflänge sollte zwischen");
define("VALUATION_CAP_SHOULD_BE_BETWEEN", "Bewertungs Cap sollte zwischen");
define("CONVERSATION_SHOULD_BE_BETWEEN", "Conversion Discount sollte zwischen");
define("WARRANT_COVERAGE_SHOULD_BE_BETWEEN", "Warrant Coverage sollte zwischen");
define("RETURN_PERCENTAGE_SHOULD_BE_BETWEEN", "Return Prozentsatz sollte zwischen");
define("MAXIMUM_RETURN_SHOULD_BE_BETWEEN", "maximale Rendite sollte zwischen");
define("YOU_HAVE_ALREADY_SENT_INVITATION", "Sie haben schon auf diese E-Mail-Adresse geschickt Einladung");
define("PLEASE_SELECT_THE_ROLE_FOR_THIS_MEMBER", "Bitte wählen Sie die Rolle für diesen Benutzer");

////******************* new added**************//


define("PENDING", "Ausstehend");
define("SUCCESSFUL", "erfolgreich");
define("CONSOLE_SUCCESS", "Console Erfolg");
define("FAILURE", "Nicht erfolgreich");
define("GOAL", "Ziel");
define("TEAM_MEMBERS", "Teammitglieder");
define("GET_FUNDED", "Lass finanziert");
define("GOAL_SHOUID_BE_BETWEEN", "Ziel sollte zwischen");
define("TO", "zu");
define("YOUR_PERKS", "Ihre Perks");
define("AMOUNT", "Betrag");
define("DESCRIPTION", "Beschreibung");
define("LINK", "Link");
define("WEBSITE", "Website");
define("CHANGE_IMAGE", "Bild ändern");
define("EMAIL", "E-Mail");
define("SEND_INVITES", "Einladungen senden");
define("PERSONAL_INFORMATION", "persönliche Daten");
define("ROUTING_NUMBER", "Routing Number");
define("ADDRESS", "Adresse");
define("GOOGLE_ANALYTICS_CODE", "Google Analytics-Code");
define("FUNDING_TYPE", "Finanzierung Type");
define("YOUTUBE_LINK", "Youtube-Link");
define("PROJECT_TITLE", "Projekttitel");
define("CURRENCY", "Währung");
define("ZIPCODE", "Postleitzahl");
define("COVER_PHOTO", "Cover Photo");
define("THE_COMPANY_ASSETS_FIEID_IS_REQUIRED", "Die Gesellschaft Vermögenswerte ist erforderlich.");
define("THE_COMPANY_COVER_FIEID_IS_REQUIRED", "Das Unternehmen Titelbild ist erforderlich.");
define("SHOW_ME_WHATS_LEFT_TO_DO", "Zeig mir, was noch zu tun");
define("SAVE", "Speichern");
define("NOT_COMPLETED", "Nicht abgeschlossen");
define("GO_LIVE_CHECKLIST", "Go Live-Checkliste");
define("HERE_IS_WHAT_IS_LEFT_TO_DO_BEFORE_YOU_GO_LIVE", "Hier ist, was ist noch zu tun, bevor Sie live gehen.");
define("SELECT_COMPANY", "Wählen Sie die Firma");
define("ADD_NEW_COMPANY", "Firmeneintrag");
define("PLEASE_SELECT_A_COMPANY_OR_ADD_NEW_COMPANY", "Bitte wählen Sie ein Unternehmen oder Firmeneintrag");
define("PERK_AMOUNT", "Perk Betrag");
define("IMAGE_UPLOADED_SUCCESSFULLY", "Bild erfolgreich hochgeladen");
define("DELETE_COMMENT_CONFIRMATION", "Sind Sie sicher, Sie wollen diesen Kommentar löschen?");
define("RECORD_UPDATED_SUCCESSFULLY", "Record erfolgreich aktualisiert");
define("DELETE_PROJECT_CONFIRMATION","Sind Sie sicher, Sie wollen diese löschen ".$project.".?");
define("IMAGE_MUST_BE_GREATER_THAN", "Bild muss größer als 800px X 450px sein");
define("PERK_AMOUNT_SHOULD_BETWEEN", "Perk Betrag sollte zwischen% s und% s für jeden Vorteil.");
define("RECORD_UPDATED_SUCCESS", "Record erfolgreich aktualisiert.");
define("INVALID_VIDEO_URL", "Ungültige Video-URL");
define("COMPLETE_MY_COMPAIGN", "Füllen Sie mein ".$project);
define("KEEP_THE_FUNDS_YOU_RAISE_EVEN_IF_YOU_DONT_REACH_YOUR_GOAL", "Halten Sie die Mittel, die Sie zu erhöhen, auch wenn Sie Ihr Ziel nicht erreichen.");
define("GOOD_IF_YOUR_PROJECT_CAN_USE_WHATEVER_AMOUNT_OF_FUNDS_YOU_CAN_RAISE", "Gut, wenn Ihr ".$project." können unabhängig Höhe der Mittel, die Sie erhöhen können.");

define("YOU_CAN_RECEIVE_FUNDS_VIA_DIRECT_CREDIT_CARD_OR_PAYPAL", "Sie können Geld per Direktkreditkarte oder PayPal erhalten");
define("WE_CHARGE_FEE_IF_YOU_REACH_YOUR_GOAL_AND_FEE_IF_YOU_DONT", "Wir berechnen %s%% Gebühr, wenn Sie Ihr Ziel erreichen, und eine %s%% Gebühr, wenn Sie dies nicht tun.");
define("CONTRIBUTIONS_ARE_REFUNDED_IF_YOU_DONT_MEET_YOUR_GOAL", " Beiträge werden zurückerstattet, wenn Sie Ihr Ziel nicht erfüllen.");
define("GOOD_IF_YOUR_PROJECT_CANT_HAPPEN_UNLESS_YOU_REACH_YOUR_GOAL", " Gut, wenn Ihr ".$project." kann nicht passieren, wenn Sie Ihr Ziel erreichen.");

define("YOU_RECEIVE_FUNDS_VIA_PAYPAL_ONLY", "Sie erhalten Geld per PayPal nur");
define("YOUR_MAXIMUM_COMPAIGN_LENGTH_IS_DAYS", "  Ihr Maximal ".$project." Länge %s Tage");
define("WE_CHARGE_FEE_IF_YOU_REACH_YOUR_GOAL_AND_FEE_IF_YOU_DONT_BUT_YOU_GET_NO_MONEY", "Wir berechnen %s%% Gebühr, wenn Sie Ihr Ziel, und keine Gebühr zu erreichen, wenn Sie nicht zu tun, aber Sie haben kein Geld bekommen");
define("INELASTIC_FUNDING", "Inelastische Funding");
define("INELASTIC_FUNDING_FIXED", "Inelastische Funding (Fixed)");
define("ELASTIC_FUNDING_FLEXIBLE", "Elastic Funding (Flexible)");
define("DONATIONS_ARE_REFUNDED_IF_TARGET_IS_NOT_MET", " ".$funds_plural." werden zurückerstattet, wenn das Ziel nicht erreicht wird.");
define("THIS_IS_GOOD_IF_YOUR_PROJECT_CANT_COMMENCE_UNLESS_YOUR_TARGET_IS_MET", "Das ist gut, wenn Ihr Projekt nicht beginnen kann, es sei denn Ihr Ziel erreicht.");
define("Abgeschlossen", "Abgeschlossen");
define("PLEASE_UPLOAD_A_JPG_PNG_GIF_FILE", "Bitte laden Sie eine JPG, PNG, GIF-Datei");
define("DOCUMENT", "Document");
define("CUSTOMIZE_YOUR_EQUITY_URL", "Passen Sie Ihre ".$project." Crowdfunding URL. Sie können Briefe enthalten, Nummern, Striche (-), oder Unterstriche (_) nur.");

define("PLEASE_SELECT_ONE_DEAL_TYPE", "Bitte wählen Sie eine Art Deal");
define("MAXIMUM_RAISE_AMOUNT_MUST_NOT_BE_GREATER_THAN_YOUR_GOAL", "Maximale Erhöhungsbetrag darf nicht größer als Ihr Ziel Betrag.");
define("MIMIMUM_RAISE_AMOUNT_MUST_NOT_BE_GREATER_THAN_YOUR_MAXIMUM_AMOUNT", "Mindesterhöhungsbetrag darf nicht größer sein als der maximale Erhöhungsbetrag sein.");
define("PLEASE_ENTER_A_DOCUMENT", "Bitte geben Sie einen Dokumentnamen");
define("PLEASE_SELECT_A_DOCUMENT_TYPE", "Bitte wählen Sie einen Dokumenttyp");
define("YOU_CAN_NOT_INVITE_YOURSELF", "können Sie nicht laden Sie sich");
define("SORRY_THIS_FILE_IS_TOO_LARGE_PLEASE_SELECT_A_JPG_FILE_THAT_IS_LESS_THAN_TWO_MB_OR_TRY_RESIZING_IT_USING_A_PHOTO_EDITOR", "Leider ist diese Datei zu groß Bitte wählen Sie eine JPG-Datei, die weniger als 2 MB ist, oder versuchen Sie es zu skalieren mit einer Bildbearbeitungssoftware..");
define("YEAR_FOUNDED_CANNOT_BE", "Gründungsjahr können zukünftige Jahr nicht");
define("GO_LIVE_AND_GET_FUNDED", "Gehen Sie leben und finanziert!");
define("YOUR_CAMPAIGN_IS", "Ihre Kampagne ist");
define("NOT_READY", "Nicht bereit");
define("READY", "Bereit");


define("TO_GO_LIVE_QUITE_YET", "Zu gehen leben noch nicht.");
define("ALLOWED_INVESTOR", "Wer kann auf Ihr Angebot zu investieren?");
define("ONLY_ACCREDIATED", "Nur Accredited Investors");
define("BOTH_ACCREDIATED", "Both (Beide Accredited und nicht akkreditierten Investoren investieren kann)");

// project detail section 

define("PROJECT_DETAIL", "Projekt-Detail");
define("PROJECT_DETAIL_BOTTOM_TEXT", "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.");
define("PROJECT_DESCRIPTION", "Projektbeschreibung");
define("PROJECT_TYPE", "Projekttyp");
define("PROJECT_SUB_TYPE", "Projekt-Sub-Type");
define("STREET", "Street");
define("PROPERTY_SIZE", "Fläche");
define("LOT_SIZE", "Grundst?cksgr??");
define("YEAR_BUILT", "Baujahr");

//investment summary and market summary

define("MARKET_SUMMARY", "Marktübersicht");
define("HIGHLIGHT_1", "Höhepunkt");
define("HIGHLIGHT_2", "Höhepunkt # 2");
define("HIGHLIGHT_3", "Höhepunkt # 3");
define("HIGHLIGHT_4", "Höhepunkt # 4");
define("INVESTMENT_SUMMARY", "Investment Summary");
define("SUMMARY", "Zusammenfassung");
define("MARKET_SUMMARY_TEXT", "Marktübersicht Text");
define("INVESTMENT_SUMMARY_TEXT", "Investment Summary Text");


define("UPLOAD_YOUR_COVER_PHOTO", "Ihre Cover-Foto hochladen.");
define("COMPANY_URL", "Unternehmens URL");

define("FEATURED_PROJECT", "Hervorgehoben ".$project."");
define("PROJECT_SUMMARY", $project." Zusammenfassung");
define("ADD_FEATURED_PROJECT", "+ Ausgewählte ".$project);
define("UPDATE_FEATURED_PROJECT", "Update Hervorgehoben ".$project);
define("JOIN_INVESTOR_NETWORK", "Join Network");
define("YOU_ARE_A_MEMBER", "Sie sind Mitglied");
define("LEAVE_NETWORK", "Lassen Network");

define("INVESTOR_NETWORK", "Investor Network");
define("YOU_MUST_BE_A_MEMBER_OF_THE_INVESTOR", "Du musst ein Mitglied des Investorennetzwerk um die anderen Anleger zu betrachten");
define("ACCREDITED_INVESTORS", "Accredited Investors");
define("ACCREDITED_AND_NON_ACCREDITED_INVESTORS", "Accredited & Non-akkreditierte Investoren");
define("OPEN_TO", "Open To");

define("PROJECTED_RETURN_CALCULATOR", "Projizierte Return Calculator");
define("ABOUT_THE_SPONSER", "Über den Sponsor");
define("PREVOIUS_FUNDING", "Zurück Funding");
define("RECOMMENDED_FILE_FORMAT_SHOULD_BE_IMG_DOC", "Empfohlene Dateiformat sollte .doc, .ods , .pdf, .zip, .ppt, .xls, .odt und .rtf sein.");

define("SORRY_THIS_FILE_IS_TOO_LARGE_PLEASE_SELECT_A_JPG_FILE_THAT_IS_LESS_THAN_MB_OR_TRY_RESIZING_IT_USING_A_PHOTO_EDITOR", "Leider ist diese Datei zu groß Bitte wählen Sie eine JPG-Datei, die kleiner als% s MB ist oder versuchen Sie es zu skalieren mit einer Bildbearbeitungssoftware..");
define("SORRY_THIS_FILE_IS_TOO_LARGE_PLEASE_SELECT_A_FILE_THAT_IS_LESS_THAN_MB", "Leider ist diese Datei zu groß Wählen Sie eine Datei, die kleiner als% s MB ist.");

define("SOCIAL", "Sozial");
define("PNG_JPG_OR_GIF_RECOMENDED_SIZE_FROM_ADMIN", "PNG, JPG oder GIF <br/> <b> Empfohlene Größe: </ b> <br/> 170 x 170 Pixel <br> Max Größe: %s MB");
define("ADD_HIGHLIGHT", "+ hinzufügen Highlight");
define("REQUIRED_FIELD", "Pflichtfeld");
define('COMPLETED', 'fertiggestellt');
/*upload_lang.php*/

define("upload_userfile_not_set", "Unfähig, einen Beitrag Variable namens userfile finden.");
define("upload_file_exceeds_limit", "Die hochgeladene Datei überschreitet die maximal zulässige Größe in der PHP-Konfigurationsdatei.");
define("upload_file_exceeds_form_limit", "Die hochgeladene Datei überschreitet die maximale Größe von dem Anmeldeformular erlaubt.");
define("upload_file_partial", "Die Datei wurde nur teilweise hochgeladen.");
define("upload_no_temp_directory", "Der temporäre Ordner fehlt.");
define("upload_unable_to_write_file", "Die Datei konnte nicht auf die Festplatte geschrieben werden.");
define("upload_stopped_by_extension", "Der Datei-Upload wurde durch Erweiterung gestoppt.");
define("upload_no_file_selected", "Sie haben keine Datei zum Hochladen auswählen.");
define("upload_invalid_filetype", "Der Dateityp Sie versuchen, zu laden ist nicht erlaubt.");
define("upload_invalid_filesize", "Die Datei, die Sie versuchen, zu laden ist größer als die zulässige Größe.");
define("upload_invalid_dimensions", "Das Bild, das Sie versuchen, zu laden exceedes die maximale Höhe oder Breite.");
define("upload_destination_error", "Ein Problem trat bei dem Versuch, um die hochgeladene Datei bis zum Zielort zu bewegen.");
define("upload_no_filepath", "Die Upload-Pfad nicht gültig zu sein.");
define("upload_no_file_types", "Sie haben keine erlaubte Dateitypen festgelegt.");
define("upload_bad_filename", "Der Dateiname Sie bereits vorgelegt auf dem Server vorhanden.");
define("upload_not_writable", "Die Upload-Zielordner erscheint nicht beschreibbar sein.");


/*admin_user_home_lang.php*/
define('FORGOT_PASSWORD', 'Passwort vergessen');
define('FUNDRAISING_SCRIPT_BY', 'Fundraising Script By');
define('ROCKERSINFO_ALL_RIGHTS_RESERVED', 'rockersinfo.com Alle Rechte vorbehalten');
define('ENTER_YOUR_EMAIL_ADDRESS_BELOW_TO_RESET_YOUR_PASSWODERD', 'Geben Sie Ihre E-Mail-Adresse unten, um Ihr Passwort zurückzusetzen.');
define('ERRODER_PAGES', 'Fehler Seite');
define('ERRODER_PAGE', 'Fehler Seite');
define('404', '404');
define('PAGE_NOT_FOUND', 'Seite nicht gefunden');
define('WE_ARE_SODERRY_THE_PAGE_YOU_WERE_LOOKING_FODER_DOEST_EXIST_ANYMODERE', 'We are sorry, the page you were looking for does not exist anymore.');
define('ADD_USER', 'Benutzer hinzufügen');
define('FACEBOOK_PROFILE_URL', 'Facebook Profil-URL');
define('TWITTER_PROFILE_URL', 'Twitter Profil-URL');
define('LINKEEDIN_PROFILE_URL', 'Linkedln Profil-URL');
define('GOOGLE_PLUS_PROFILE_URL', 'Google Plus Profil-URL');
define('BANDCAMP_PROFILE_URL', 'Bandcamp Profil-URL');
define('YOUTUBE_PROFILE_URL', 'Youtube Profil-URL');
define('MYSPACE_PROFILE_URL', 'MySpace Profil-URL');
define('INVALID_FACEBOOK_PROFILE_URL','Ungültige Facebook Profil-URL');
define('INVALID_TWITTER_PROFILE_URL','Ungültige Twitter Profil-URL');
define('INVALID_LINKEEDIN_PROFILE_URL','Ungültige Linkedln Profil-URL');
define('INVALID_BANDCAMP_PROFILE_URL','Ungültige Bandcamp Profil-URL');
define('INVALID_YOUTUBE_PROFILE_URL','Ungültige Youtube Profil-URL');
define('INVALID_MYSPACE_PROFILE_URL','Ungültige MySpace Profil-URL');
define('INVALID_GOOGLE_PLUS_PROFILE_URL','Ungültige Google Plus Profil-URL');

define('INACTIVE', 'Inaktiv');
define('ADMIN_TYPE', 'Admin Type');
define('ACTIVE', 'Aktiv');
define('SUSPEND', 'Aussetzen');
define('ADD_SUSPEND_REASON', 'In auszusetzen Grund');
define('UPDATE', 'Aktualisieren');
define('GRAPHICAL_REPORT', 'Grafische Bericht');
define('USER_LIST', 'Benutzerliste');
define('USER_LOGIN', 'Benutzer-Anmeldung');
define('ADMIN_LOGIN', 'Admin Login');
define('PROJECT_CATEGODERIES', 'Projektkategorien');
define('PAYMENT_MODULE', 'Payment Module');
define('PAYPAL_ADAPTIVE_GATEWAY', 'Paypal Adaptive-Gateway');
define('WALLET_GATEWAY', 'Wallet-Gateway');
define('TRANSACTION', 'Transaktion');
define('CONTENT_PAGES', 'Content-Seiten');
define('PAGES', 'Seiten');
define('LEARN_MODERE', 'Mehr erfahren');
define('LEARN_MODERE_CATEGODERIES', 'Weitere Kategorien');
define('GLOBALIZATION', 'Globalisierung');
define('COUNTRIES', 'Länder');
define('OTHER_FEATURE', 'Andere Eigenschaft');
define('CRON_JOBS', 'Cron Jobs');
define('MESSAGES', 'Nachrichten');
define('STATS', 'Status');
define('COMMON_SETTING', 'Allgemeine Einstellung');
define('COMMISION_SETTING', 'Provision Einstellung');
define('AFFILIATE_REQUEST', 'Affiliate-Antrag');
define('WITHDRAW_FUND_REQUEST', 'Zurückzuziehen Fund anfordern');
define('EMAIL_TEMPLATE', 'E-Mail-Vorlage');
define('SETTING', 'Rahmen');
define('SITE', 'Website');
define('META', 'Meta');
define('GOOGLE', 'Google');
define('YAHOO', 'Yahoo');
define('IMAGE_SIZE', 'Bildgröße');
define('FILTER', 'Filter');
define('SPAM_SETTING', 'Spam-Einstellung');
define('SPAM_REPODERT', 'Spam melden');
define('SPAMER', 'Spamer');
define('NEWLETTERS', 'Newsletters');
define('NEWLETTERS_USERS', 'Newsletter Benutzer');
define('NEWLETTERS_JOBS', 'Newsletter Stellenangebote');
define('NEWLETTERS_SETTING', 'Newsletter Setting');
define('REPODERT', 'Bericht');
define('PROJECT_REPODERT', 'Projektbericht');
define('GENERAL_INFODERMATION', 'Allgemeine Information');
define('NEW_USERS', 'Neue Nutzer');
define('PENDING_PROJECTS', 'Anhängigen Projekte');
define('RUNNING_PROJECTS', 'Laufende Projekte');
define('SUCCESSFUL_PROJECTS', 'Erfolgreiche Projekte');
define('FAILURE_PROJECTS', 'Failure Projekte');
define('LAST', 'Letzte');
define('USERNAME', 'Benutzername');
define('POSTED_DATE', 'Eingestellt seit');
define('NO_RECORD_FOUND', 'Kein Eintrag gefunden');
define('NEW_RUNNING_PROJECTS', 'New laufende Projekte');
define('AMOUNT_RAISED', 'Betrag angehoben');
define('COMPLETED_PROJECTS', 'Abgeschlossene Projekte');
define('DONATE_AMT', 'Spenden Amt');
define('EARNING', 'Earning');
define('REMAIN_FUND', 'Bleiben Fund');
define('FAILED_PROJECTS', 'Gescheiterte Projekte');
define('NEW_DONODERS', 'Neue Geber');
define('IP_ADDRESS', 'IP Adresse');
define('FUNDS', 'Mittel');
define('REQUEST_DATE', 'Anforderungsdatum');
define('NEW_AFFILIATE_WITHDRAW_REQUEST', 'Neue Partner zurückziehen Anfragen');
define('REQUEST_IP', 'IP anfordern');
define('ROCKERSINFO_ALL_RIGHT_RESERVED', 'rockersinfo.com Alle Rechte vorbehalten');
define('YOU_HAVE_1_NEW_NOTIFICATION', 'Sie haben% s neue Meldung (s).');
define('YOU_DONT_HAVE_ANY_NEW_NOTIFICATION', 'Sie haben noch keine neue Benachrichtigung.');
define('ADMIN', 'Admin');
define('CRONJOB', 'Cronjob Zeit');
define('LAST_LOGIN_IP', 'Letzte Anmeldung Ip');
define('LOG_OUT', 'Abmelden');
define('STEP_1_OF_4', 'Schritt 1 von 4');
define('STEP_1', 'Schritt 1');
define('STEP_2', 'Schritt 2');
define('STEP_3', 'Schritt 3');
define('FINAL_STEP', 'Letzter Schritt');
define('FILL_UP_STEP_1', 'Füllen Sie Schritt 1');
define('FILL_UP_STEP_2', 'Füllen Sie Schritt 2');
define('FILL_UP_STEP_3', 'Füllen Sie Schritt 3');
define('OCCASIONALLY_WE_MAY_RELEASE_SMALL_UPDATE_IN_THE_FODERM_OF_A_PATCH_TO_ONE_ODER_SEVERAL_SOURCE_FILE', 'Occasionally, we may release small updates in the form of a patch to one or several source file. These source files are regular zip files that contain updated files.');
define('BACK', 'Zurück');
define('CONTINUE1', 'Fortsetzen');
define('NEW_RECODERD_HAS_BEEN_ADDED_SUCCESSFULLY', 'Neuer Datensatz wurde erfolgreich hinzugefügt.');
define('RECORD_HAS_BEEN_UPDATED_SUCCESSFULLY', 'Die Bilanz wurde erfolgreich aktualisiert.');
define('STATE', 'Bundesland');
define('SIGN_UP_IP', 'Registrieren IP');
define('INACTIVE_USERS', 'Inaktive Benutzer');
define('AFFILIATE_REQUEST_USERS', 'Affiliate-Antrag Benutzer');
define('WHY_DO_YOU_WANT_AFFILIATE', 'Warum wollen Sie Affiliate?');
define('PROMOTIANAL_METHOD', 'Werbe-Methode');
define('RECODERD_HAS_BEEN_DELETED_SUCCESSFULLY', 'Die Bilanz wurde erfolgreich gelöscht.');
define('USERS', 'Benutzer');
define('SIGNUP_IP_ADDRESS', 'Anmelden IP-Adresse');
define('LAST_10_PENDING_PROJECTS', 'Die letzten 10 anstehenden Projekte');
define('LAST_5_NEWRUNNING_PROJECTS', 'Letzte 5 New laufende Projekte');
define('LAST_5_COMPLETED_PROJECTS', 'Letzte 5 Abgeschlossene Projekte');
define('LAST_5_FAILED_PROJECTS', 'Letzte 5 Gescheiterte Projekte');
define('LAST_5_NEW_PROJECTS', 'Die letzten 10 Neue Geber');
define('NEW_AFFILIATE_WITHDRAW_REQUESTS', '10 Neue Partner zurückziehen Anfragen');
define('LAST_10_NEW_USERS', 'Die letzten 10 Neue Nutzer');
define('LAST_5_INACTIVE_USERS', 'Letzte 5 Inaktive Benutzer');
define('LAST_5_AFFILIATE_USERS', 'Letzte 5 Affiliate-Antrag Benutzer');
define('REGISTERED_ON', 'Registrierte On');
define('FUNDRAISINGSCRIPT_COM', 'Fundraisingscript.com');
define('ERRODERS', 'Fehler');
define('SOURCE_FILE_NAME', 'Name der Quelldatei');
define('DESTINATION_FILE_NAME', 'Name der Zieldatei');
define('NO_UPDATE_FOUND', 'Keine Aktualisierung gefunden');
define('DO_YOU_WANT_TO_UPDATE_YOUR_BACK_UP_AGAIN_THEN_CLICK_HERE', 'Möchten Sie Ihre wieder zu aktualisieren, bis dann klicken Sie hier.');
define('ADAPTIVE_PAYPAL_SETTING', 'Adaptive Paypal Einstellung');
define('SITE_STATUS', 'Site Status');
define('TRANSACTION_TYPE', 'Art der Transaktion');
define('SAND_BOX', 'Sandkasten');
define('LIVE', 'Leben');
define('INSTANT', 'Augenblick');
define('PREAPPROVAL', 'Preapproval');
define('PAYPAL_APPLICATION_ID', 'Paypal Anwendungs ​​Id');
define('PAYPAL_API_USERNAME', 'Paypal API Benutzername');
define('PAYPAL_API_PASSWODERD', 'Paypal API Passwort');
define('PAYPAL_API_SIGNATURE', 'Paypal API Stempel, Unterschrift');
define('PAYPAL_EMAIL_ID', 'Paypal Email Id');
define('PAYPAL_PAYPAL_FIRST_NAME', 'Benutzer Paypal Vorname');
define('PAYPAL_PAYPAL_LAST_NAME', 'Benutzer Paypal Nachname');
define('PAYPAL_FEES_TAKEN_FROM', 'PayPal-Gebühren entnommen');
define('PROJECT_OWNER', 'Projekteigentümer');
define('FROM_ADDRESS', 'Von Adresse');
define('REPLY_ADDRESS', 'Antwortadresse');
define('NOTE_IT_WILL_UPDATE_ALL_THE_TEMPLATE_EMAIL_ADDRESS_IF_YOU_TO_CHANGE_ANY_PARTICULAR_EMAIL_ADDRESS_THEN_YOU_CAN_CHANGE_IT_GOING_IN_THAT_GOING_IN_THAT_EMAIL_TEMPLATE', 'Hinweis: Es wird die ganze Vorlage E-Mail Adresse zu aktualisieren. Wenn Sie eine bestimmte E-Mail-Adresse ändern möchten, dann können Sie ändern, es wird in dieser E-Mail-Vorlage.');
define('SELECT_PROJECT_FODER_TESTING_DONATION', 'Wählen Sie '.$project.' zum Testen Spende.');
define('SELECT_PROJECT_FODER_TEST', 'Wählen Sie '.$project.' für die Prüfung.');
define('ENTER_DONATE_AMOUNT_ODER_SELECT_PERK', 'Geben Sie spenden Betrag oder wählen Perk.');
define('PROJECT_OWNER_HAVE_NOT_VERIFY_PAYPAL_ACCOUNT_PLEASE_ENTER_PAYPAL_ACCOUNT_DETAIL_BELOW_AND_VERIFY', 'Projekt Besitzer haben nicht überprüfen PayPal-Konto. Bitte geben PayPal-Konto weiter unten und Überprüfen.');
define('USER_PAYPAL_AC_EMAIL', 'Benutzer Paypal A / C-E-Mail');
define('OR', 'ODER');
define('SELECT_DONAR_USER_FOR_MAKING_DONATION', 'Wählen donar Benutzer für die Herstellung Spende.');
define('SELECT_DONAR_USER', 'Wählen Donar Benutzer');
define('NOTES', 'NOTIZ');
define('BEFORE_PROCEED_TO_NEXT_STEP_PLEASE_LOGIN_TO_PAYPAL_LOGIN_TO_PAYPAL_ACCOUNT_OTHERWISE_YOU_NEED_COME_BACK_AGAIN_AND_DO_ALL_THE_PROCESS', 'Before proceed to next step, Please login to Paypal Account Otherwise you need come back again and do all the process once again.');
define('FOR_SANDBOX_TEST', 'Für Sandkasten-Test');
define('DEVELOPER_PAYPAL_COM', 'developer.paypal.com');
define('FOR_LIVE_TEST', 'Für Live-Test');
define('COUNTRY_NAME', 'Ländername');
define('MESSAGE', 'Nachricht');
define('RECORDS_HAS_BEEN_UPDATED_SUCCESSFULLY', 'Aufzeichnung(s) has been updated successfully.');
define('VIEW', 'Aussicht');
define('CURRENCY_NAME', 'Währungsbezeichnung');
define('CURRENCY_CODE', 'Währungscode');
define('CURRENCY_SYMBOL', 'Währungszeichen');
define('CRONJOBS', 'Cronjob');
define('SELETE_CRON_JOB', 'Selete Cron Job');
define('RUN', 'Lauf');
define('LAST_RUN_TIME', 'Last Run Time');
define('NO_CURRENCIES_FOUND', 'Keine Währungen gefunden.');
define('PAYPAL_ADAPTIVE_PAYMENT_GATEWAY', 'Paypal Adaptive Payment Gateway');
define('SANDBOX', 'Sandkasten');
define('IF_ACTIVE_THEN_YOU_HAVE_TO_SET_CRON_JOB_FOR_EVERY_MINUTE_WITH_THIS_URL', 'Wenn aktiv, dann müssen Sie Cron-Job für alle 5 Minuten mit diesem URL gesetzt.');
define('YOU_HAVE_TO_INACTIVATE_THE_CURRENTLY_ACTIVE_PAYMENT_GATEWAY_TO_ACTIVATE_THIS_PAYEMNT', 'Sie müssen die derzeit aktive Payment Gateway, um dieses Payment Gateway aktivieren inaktivieren.');
define('PAYMENT_GATEWAY_ACTIVATED_SUCCESSFULLY', 'Payment Gateway erfolgreich aktiviert.');
define('PAYMENT_GATEWAY_INACTIVATED_SUCCESSFULLY', 'Payment Gateway inaktiviert erfolgreich.');
define('TEST_YOUR_PAYPAL_SETTING', 'Testen Sie Ihr Paypal-Einstellung.');
define('GATEWAY_STATUS', 'Gateway-Status');
define('OPTIONS', 'Optionen');
define('TRANSACTIONS', 'Geschäfte');
define('ADD_ADMINISTRATOR', 'Administrator hinzufügen');
define('ADMINISTRATOR', 'Administrator');
define('SUPER_ADMINISTRATOR', 'Super-Administrator');
define('ASSIGN_RIGHTS', 'Assign Rechte');
define('USERS_LOGIN', 'Benutzer Einloggen');
define('RIGHT_NAME', 'Richtigen Namen');
define('CANNOT_DELETE_ALL_THE_ADMIN', 'Alle Admin kann nicht gelöscht werden. Atleast eine aktiv sein soll.');
define('RECORD_HAS_BEEN_DELETED_SUCCESSFULLY_FROM_ADMIN_LOGIN', 'Die Bilanz hat sich erfolgreich vom Admin-Login gelöscht.');
define('RIGHT_HAS_BEEN_UPDATED_SUCCESSFULLY', 'Rechte wurde erfolgreich aktualisiert.');
define('ADD_ADMIN', 'In Admin');
define('ADMINISTRATOR_MANAGED_TABLE', 'Administrator Managed Tabelle');
define('RIGHTS', 'Rechte');
define('LOGIN_DETAIL_HAS_BEEN_DELETED_SUCCESSFULLY', 'Login-Daten erfolgreich gelöscht wurde.');
define('LOGIN_IP_ADDRESS', 'Login-IP-Adresse');
define('LOGIN_DATE', 'Login Datum');
define('LOGIN_TIME', 'Login Zeit');
/*define('REPORT', 'Bericht');*/
define('INVALID_USERNAME_OR_PASSWORD', 'Ungültiger Benutzername oder Passwort');
define('YOU_HAVE_LOGGED_OUT_SUCCESSFULLY', 'Sie haben sich erfolgreich angemeldet');
define('PASSWORD_IS_SEND_TO_YOUR_EMAIL_ADDRESS', 'Passwort wird an Ihre E-Mail-Adresse senden');
define('USER_EMAIL_ADDRESS_IS_NOT_FOUND', 'Benutzer E-Mail-Adresse ist nicht gefunden');
define('USER_RECORD_IS_NOT_FOUND', 'Benutzerdatensatz nicht gefunden');

define('SUPER_ADMIN', 'Super-Admin');
define('PROJECT_CATEGORIESS', 'Projektkategorien');
define('LEARN_MOREE', 'Mehr erfahren');
define('LEARN_MORE_CATEGORIESS', 'Weitere Kategorien');
define('DISPLAY', 'Anzeigen');
define('RECORD_PER_PAGE', 'Datensätze pro Seite');
define('NOTHING_FOUND_SORRY', 'Im Moment gibt - sorry');
define('SHOWING', 'Zeige');
define('RECORD', 'Aufzeichnungen');
define('CHANGE_YOUR_PASSWORD', 'Ändern Sie Ihr Passwort');
define('LAST_UPDATE_VERSION', 'Letztes Update Version');
define('UPDATED_ON', 'Aktualisiert am');
define('VERSION', 'Version');
define('SEE_ALL_UPDATES', 'Alle Updates');
define('RIGHT', 'Recht');
define('THERE_IS_AN_EXISTING_ACCOUNT_ASSOCIATED_WITH_THIS_EMAIL_ID', 'Es gibt einen bestehenden Account mit dieser E-Mail-ID zugeordnet');
define('THERE_IS_AN_EXISTING_ACCOUNT_ASSOCIATED_WITH_THIS_USERNAME', 'Es gibt ein bestehendes Konto mit diesem Benutzernamen zugeordnet');
define('YOUR_PASSWORD_HAS_BEEN_UPDATED_SUCCESSFULLY', 'Ihr Passwort wurde successfuly aktualisiert');
define('SIGNATURE', 'Stempel, Unterschrift');
define('SELECT_PROJECT', 'Wählen Sie Projekt');
define('DONATE_AMOUNT', 'Spenden Betrag');
define('COUNTRY_NAME_IS_ALREADY_EXIST', 'Land Name ist bereits vorhanden');
define('MAILER_TYPE', 'Mailer Typ');
define('SENDMAIL_PATH', 'Sendmail Pfad');
define('SENDMAIL_MAIL', 'Absender E-Mail');
define('RECEIVER_MAIL', 'Empfänger E-Mail');
define('TEST_MESSAGE_SUCCESSFULLY_SENT', 'Testnachricht erfolgreich gesendet');

define('CREADIT_CARD_VERSION', 'Credit Card Version');

define('PAYPAL_STATUS', 'Paypal-Status');
define('COMMISSION_FEE', 'Kommission Fee');
define('ACCESS_KEY_ID', 'Access Key Id');
define('SECRET_ACCESS_KEY', 'Secret Access Key');
define('TRANSACTION_TYPE_NAME', 'Art der Transaktion Bezeichnung');
define('UPDATE_VERSION', 'Update Version');
define('DELETE_ADMINISTRATOR_CONFIRMATION ', 'Are you sure,Do you want to delete this record ?');
define('TAXONOMY', 'Taxonomy');
define('TAXONOMY_SETTING', 'Taxonomie-Einstellung');
define('PROJECT_NAME', 'Projektname');
define('PROJECT_URL', 'Projekt-URL');
define('PROJECT_FUNER', 'Projekt Geldgeber');
define('FOLLOWER', 'Anhänger');
define('USER', 'Benutzer');
define('NO', 'Nein');
define('EDIT_USER', 'Benutzer bearbeiten');

define('GENERAL_INFORMATION', 'Informations Générales');
define('SPAM_REPORT', 'Spam Report');

define('SORRY_YOUR_PREAPPROVAL_PAYMENT_PROCESS_IS_VIOLATED_DUE_TO_FOLLOWING_ERROR', 'Es tut uns leid..! Ihre Preapproval Zahlungsvorgang wird durch folgende Fehler verletzt');
define('SORRY_YOUR_PAYMENT_PROCESS_IS_VIOLATED_DUE_TO_ERROR_IN_PAYRECIEPT_METHOD_PLEASE_CHECK_YOUR_SETTING_AND_TRY_ONCE_AGAIN', 'Es tut uns leid...! Ihre Zahlungsvorgang aufgrund eines Fehlers in PayReciept Verfahren verletzt. Bitte überprüfen Sie Ihre Einstellungen, und versuchen Sie noch einmal.');
define('PREAPPROVAL PROCESS IS VIOLATED DUE TO PREAPPROVAL KEY ALREADY EXISTS IN THE SYSTEM. THIS HAPPEN DUE TO 2 REASON', 'Es tut uns leid...! Ihre Preapproval Prozess ist aufgrund Preapproval Key im System bereits vorhanden ist verletzt. Dies geschieht auf Grund 2 Grund. <br/>1). Due to lowe internet connectivity.<br/>2). Paypal Send response twice for same transaction. Check Manually Project donation amount and New Donor Entry.');
define('SORRY_YOUR_PREAPPROVAL_PROCESS_IS_VIOLATED_DUE_TO_ERROR_OCCURRED_IN_PAYMENTDETAILS_METHOD_PLEASE_CHECK_YOUR_SETTING_AND_TRY_ONCE_AGAIN', 'Es tut uns leid...! Ihre Preapproval Prozess verletzt wird aufgrund Fehler in PaymentDetails Methode. Bitte überprüfen Sie Ihre Einstellungen, und versuchen Sie erneut');
define('SORRY_YOUR_INSTANT_PAYMENT_PROCESS_FAILED_DUE_TO_ERROR', 'Es tut uns leid...! Auf einem Fehler Ihres Instant Zahlungsprozess gescheitert');
define('SORRY_YOUR_INSTANT_PAYMENT_PROCESS_FAILED_DUE_TO_ERROR_OCCURRED_IN_PAYMENTDETAILS_METHOD_PLEASE_CHECK_YOUR_SETTING_AND_TRY_ONCE_AGAIN', 'Es tut uns leid...! Ihre sofortige Zahlungsprozess scheiterte an Fehler in PaymentDetails Methode. Bitte überprüfen Sie Ihre Einstellungen, und versuchen Sie erneut');
define('SORRY_YOUR_INSTANT_PAYMENT_PROCESS_IS_VIOLATED_DUE_TO_PAYPAL_AP_PAYPAL_NEW_DONOR_ENTRY', 'Es tut uns leid...! Ihres Instant Zahlungsprozess wird durch Paypal AP Key im System bereits vorhanden ist verletzt. Dies geschieht auf Grund 2 Grund. <br/>1). Due to lowe internet connectivity.<br/>2). Paypal Send response twice for same transaction. Check Manually Project donation amount and New Donor Entry.');
define('SORRY_YOUR_PREAPPROVAL_PAYMENT_PROCESS_IS_VIOLATED_DUE_TO_ERROR_IN_PAYMENTDETAILS_METHOD_PLEASE_CHECK_YOURSETTING_AND_TRY_ONCE_AGAIN', 'Es tut uns leid..! Ihre Preapproval Bezahlvorgang aufgrund eines Fehlers in PaymentDetails Verfahren verletzt. Bitte überprüfen Sie Ihre Einstellungen, und versuchen Sie noch einmal.');
define('SORRY_YOUR_PAYMENT_PROCESS_IS_VIOLATED_DUE_TO_ERROR_IN_PAYMENTEXECSTATUS_PLEASE_CHECK_YOUR_SETTING_AND_TRY_ONCE_AGAIN.', 'Es tut uns leid...! Ihre Zahlungsvorgang aufgrund eines Fehlers in paymentExecStatus verletzt. Bitte überprüfen Sie Ihre Einstellungen, und versuchen Sie noch einmal.');
define('PLEASE_CHECK_YOUR_SETTINGS_AND_TRY_ONCE_AGAIN', 'Bitte überprüfen Sie Ihre Einstellungen und versuchen Sie erneut');
define('YOUR_PAYMENT_PROCESS_IS_SUCCESSED_ON_PROJECT', 'Ihr Zahlungsprozess auf '.$project.' successedt');
define('BELOW_IS_PAYPAL_RESPONSE', 'Unten ist Paypal Antwort');
define('ARE_YOU_SURE_YOU_WANT_TO_ACTIVE_SELECTED_COUNTRY', 'Are you sure, you want to active selected country?');
define('ARE_YOU_SURE_YOU_WANT_TO_INACTIVE_SELECTED_COUNTRY', 'Are you sure, you want to inactive selected country?');

define('RUNNING_INVESTMENT','Laufende Investitions');
define('CREATED_DATE','Erstellungs-Datum');
define('INVESTOR_ACTION','Investor Aktion');
define('OWNER_ACTION','Besitzer Aktion');

/*admin_contant_lang.php*/


define('ORDER','Auftrag');
define('ANSWER','Antworten');
define('ADD_PAGES','In Seiten');
define('CATEGORY_NAME','Name der Kategorie');
define('DISPLAY_PLACE','Display-Platz');
define('FOOTER','Fußzeile');
define('PAGES_SIDEBAR','Seiten Sidebar');
define('LEARN_CATEGORY','Erfahren Sie Kategorie');
define('PAGES_TITLE','Seiten Titel');
define('SUB_TITLE','Untertitel');
define('ICON','Symbol');
define('SELECT_FILE','Datei aussuchen');
define('CHANGE','Veränderung');
define('REMOVE','Entfernen');
define('SLUG','Slug');
define('META_KEYWORD','Meta-Schlüsselwörter');
define('META_DESCRIPTION','Meta Beschreibung');
define('SIDEBAR','Sidebar');
define('RECORDS_HAS_BEEN_DELETED_SUCCESSFULLY','Nehmen Sie [s] wurde erfolgreich gelöscht.');
define('RECORD_HAS_BEEN_ACTIVATED_SUCCESSFULLY','Nehmen Sie [s] wurde erfolgreich aktiviert.');
define('RECORD_HAS_BEEN_INACTIVATED_SUCCESSFULLY','Nehmen Sie [s] wurde erfolgreich inaktiviert wurde.');
define('RECORD_HAS_BEEN_SHOW_ON_HELP_PAGE_SUCCESSFULLY','Nehmen Sie [s] wurde show on Hilfeseite erfolgreich.');
define('RECORD_HAS_BEEN_REMOVE_ON_HELP_PAGE_SUCCESSFULLY','Aufzeichnungen]) has been remove on help page successfully.');
define('YOU_CAN_NOT_DEACTIVATED_ALL_CURRENCIES','Sie können alle Währungen, die nicht zu deaktivieren. Es sollte ein aktiver sein.');
define('RECORDS_HAS_BEEN_MAKE_PERMENANT_SUCCESSFULLY','Die Bilanz hat sich machen permenant erfolgreich.');

define('LEARN_MORE_CATEGORY','Erfahren Sie mehr Kategorie');
define('LEARN_CATEGORY_NAME','Erfahren Kategorie Name');
define('LEARN_MORE_PAGES','Erfahren Sie mehr Seiten');
define('MORE','Mehr');
//==============Payment Tor===========================
define('WALLET_SETTING','Wallet Einstellung');
define('WALLET_ADD_TIME_FEES','Wallet hinzufügen Zeit Gebühren [%]');
define('WALLET_ADD_MIN_AMOUNT','Wallet hinzufügen Zeit Mindestbetrag');
define('WALLET_WITHDRAW_FEES','Wallet zurückziehen [Donate] Zeit Gebühren [%]');
define('DIRECT_DONATE_OPTION','Direkt spenden Option');
define('ADD_WHOLE_DONATION_AMOUNT','In gesamten Spendenbetrag');
define('ADD_REMAINING_DONATION_AMOUNT','Die restlichen Spendenbetrag');
define('WALLET_REVIEW','Wallet Bewertung');
define('REVIEW','Rezension');
define('CONFIRM','Bestätigen');
define('AMOUNT_ADDED','Menge zugesetzt');
define('GATEWAY','Tor');
define('WALLET_WITHDRAW','Wallet zurückziehen');
define('CURRENT_AMOUNT','Aktuelle Höhe');
define('AMOUNT_TO_PAY','Zu zahlender Betrag');
define('AMAZON_GATEWAY','Amazon-Gateway');
define('VARIABLE_FEES','Variable Gebühren');
define('FIXED_FEES','Feste Gebühren');
define('SUPPORT_MASSPAYMENT','SUPPORT MASSPAYMENT');
define('Support_Masspayment','Support Masspayment');
define('FUNCTION_NAME','Funktionsname');
define('Name','Name');
define('Wert','Wert');
define('Etikette','Etikette');
define('Beschreibung','Beschreibung');
define('Aktion','Aktion');
define('Image','Image');
define('LANGUAGE_IS_ALREADY_EXIST','Die Sprache ist bereits vorhanden');
define('ALLOW_ONLY_IMAGE_FILE_TO_UPLOAD','Erlauben Sie nur Bild eine Datei zum hochladen');
define('MESSAGE_SUBJECT','Betreff der Nachricht');
define('FUNCTION_NAMES','Funktionsname');
define('SUAPPORT_MASSPAYMENT','Suapport Masspayment');
define('PAGES_DESCRIPTION','Seitenbeschreibung');
define('EXTERNAL_URL','Externe URL');
define('FAQ_CATEGORY','Faq Kategorie');
define('QUESTIONS','Frage');
define('ACTIONS','Aktion');
//all confirm message
define('DELETE_USER_CONFIRMATION','Sind Sie sicher, Sie möchten Benutzer zu löschen?');
define('DELETE_SELECT_RECORD_CONFIRMATION','Bitte wählen Sie einen Datensatz');
define('DELETE_CATEGORY_CONFIRMATION','Sind Sie sicher, Sie wollen diese Kategorie löschen?');
define('DELETE_PAGE_CONFIRMATION','Sind Sie sicher, Sie möchten diese Seite zu löschen?');
define('DELETE_COUNTRY_CONFIRMATION','Sind Sie sicher, Sie wollen Land zu löschen?');
define('DELETE_CURRENCY_CONFIRMATION','Sind Sie sicher, Sie wollen Währung zu löschen?');
define('DELETE_GATEWAY_DETAIL_CONFIRMATION','Sind Sie sicher, Sie wollen Gateways Detail zu löschen?');
define('DELETE_ADMINISTRATOR_CONFIRMATION','Sind Sie sicher, Sie wollen Administrator löschen?');
define('DELETE_GATEWAY_PAYMENT_CONFIRMATION','Sind Sie sicher, Sie wollen Zahlungs-Gateways zu löschen?');
define('ARE_YOU_SURE_YOU_WANT_TO_DELETE_SELECTED_LEARN_CATEGORIES', 'Sind Sie sicher, dass Sie ausgewählt löschen wollen lernen Kategorien?');
define('ARE_YOU_SURE_YOU_WANT_TO_ACTIVE_SELECTED_LEARN_CATEGORIES', 'Sind Sie sicher, Sie aktiv ausgewählt wollen Kategorie lerneny?');
define('ARE_YOU_SURE_YOU_WANT_TO_INACTIVE_SELECTED_LEARN_CATEGORIES', 'Sind Sie sicher, Sie wollen Kategorie ausgewählt, um inaktive lernen?');
define('ARE_YOU_SURE_YOU_WANT_TO_DELETE_SELECTED_MESSAGE', 'Sind Sie sicher, Sie wollen ausgewählte Mitteilungen zu löschen?');
define('TYPE_A_MESSAGE_HERE', 'Geben Sie eine Nachricht eingeben ...');
define('RECORDS_UPDATED_BY_THIS_CRONJOB', 'Rekorde dieses Cronjob aktualisiert');
define('NO_RECORDS_UPDATED_BY_THIS_CRONJOB', 'Keine Aufzeichnung dieses Cronjob aktualisiert');
define('PAYMENT_GATEWAYS', 'Zahlungs-Gateways');
define('WITHDRAW_REQUEST', 'Zurückzuziehen anfordern');
define('TEST_UPDATE', 'Test & aktualisieren');
define('GATEWAYS_DETAILS', 'Gateways Einzelheiten');

//CATEGORY
define('ADD_CATEGORY', 'Kategorie hinzufügen');
define('EDIT_CATEGORY', 'Kategorie bearbeiten');
define('PARENT', 'Elternteil');
define('CHILD', 'Kind');
define('TYPE', 'Art');
define('CATEGORY_LIST', 'Kategorie-Liste');

//Phase
define('ADD_PHASE', 'In Phase');
define('EDIT_PHASE', 'Bearbeiten Phase');
define('PHASE_LIST', 'Phase Liste');
define('DELETE_PHASE', 'Sind Sie sicher, diese Phase löschen möchten?');
define('PHASE_NAME', 'Phase-Name');
define('PHASE', 'Phasen');
define('LOADING', 'Laden');
define('EQUITY_CATEGORIES', ''.$project_name.' Kategorien');
define('PROJECT_PHASE', ''.$project_name.' Phasen');

define('MAIN', 'Main');
define('RELATED', 'Related');
define('CATEGORY_DESCRIPTION', 'Kategorie Beschreibung');

/*admin_accreditation_lang.php*/
define('ACCREDITATION_USERS', 'Akkreditierungs Benutzer');
define('ACCREDITATION_USER', 'Akkreditierungsbenutzer');
define('ACCREDITATION_USERS_DETAIL', 'Akkreditierungs Benutzer-Details');
define('APPROV', 'Genehmigen');

define('ACCREDITATION_DETAILS', 'Accreditation-Details');
define('INCOME', 'Einkommen');

define('VERIFICATION_OPTIONS', 'Verification Optionen');
define('DOWNLOAD', 'Download');

define('REJECT', 'Ablehnen');
define('REJECTED', 'abgelehnt');
define('YOU_CAN_APPROVE', 'Sie können genehmigen');
define('YOU_CAN_REJECT', 'Sie können zurückweisen');


/*admin_email_lang.php*/
define('MASTER_EMAIL_TEMPLATE','Master-E-Mail-Vorlage');
define('SELECT_EMAIL_TEMPLATE','Wählen Sie die E-Mail-Vorlage');
define('EMAIL_FOR_TASK','E-Mail Aufgaben');

/*admin_equity_lang.php*/

define('CONTRACT_DOCUMENT','Ausschreibungsunterlagen');
define('CONTRACT_MANAGE','Contract verwalten');
define('OFFLINE','Offline');
define('EQUITY_NAME',''.$project_name.' Name');
define('HISTORY','Geschichte');
define('DOCUMENT_NAME','Dokument Name');
define('CURRENT_STATUS','Aktueller Status');
define('DOWNLOAD_ATTACH_FILE','Herunterladen Datei anhängen');

define('PROCESSING_CONTRACT','Verarbeitungsvertrag');
define('DOWNLOADED_CONTRACT','Heruntergeladen Contract');
define('UPLOADED_SIGNED_CONTRACT','Hochgeladene unterzeichnet Vertrag');
define('SIGNED_CONTRACT_APPROVE','Unterzeichnet Vertrag genehmigen');
define('PAYMENT_PROCESS','Zahlungsprozess');
define('PAYMENT_RECIEPT_UPLOADED','Payment Reciept Hochgeladene');
define('PAYMENT_CONFIRMED','Bezahlung bestätigt');
define('TRACKING_SHIPMENT','Tracking Versand');

define('SHIPMENT','Sendung');
define('NO_RECORDS_AVAILABLE','Kein Eintrag vorhanden');
define('YOUR_MESSAGE_HAS_BEEN_SUCCESSFULLY_SENT','Deine Nachricht wurde erfolgreich versendet.');
define('ACKNOWLEDGE_NOTE','Quittieren Hinweis');

define('COMPANY_CATEGORY_LIST','Unternehmen Kategorieliste');
define('COMPANY_CATEGORY_ALREADY_EXIST','Genre alredy existieren.');
define('COMPANY_CATEGORY_NAME','Unternehmen Categoty Namen');
define('INSERT','einfügen');
define('UPDATE_MSG','aktualisieren');
define('ADD_COMPANY_CATEGORY','In Unternehmenskategorie');
define('THEME','Thema');
define('EDIT_COMPANY_CATEGORY','Bearbeiten Firma Kategorie');
define('FUNDING_SOURCE','Finanzierungsquelle');
define('DOCUMENT_TYPE','Art des Dokuments');
define('COMPANY_INDUSTRY_LIST','Firma Branche Liste');
define('ADD_COMPANY_INDUSTRY','In Unternehmen Branche');
define('DELETE_INDUSTRY_CONFIRMATION','Sind Sie sicher , Sie wollen diesen Industrie löschen?');
define('EDIT_COMPANY_INDUSTRY','Bearbeiten Firma Branche');
define('COMPANY_INDUSTRY_ALREADY_EXIST','Firma Branche Name bereits existiert.');
define('FUNDING_SOURCE_LIST','Funding Source List');
define('DELETE_FUNDING_SOURCE_CONFIRMATION','Sind Sie sicher , Sie wollen diesen Finanzierungsquelle löschen?');
define('ADD_FUNDING_SOURCE','In Finanzierungsquelle');
define('EDIT_FUNDING_SOURCE','Bearbeiten Finanzierungsquelle');
define('FUNDING_SOURCE_ALREADY_EXIST','Finanzierungsquelle alredy existieren.');
define('DELETE_FUNDING_TYPE_CONFIRMATION','Sind Sie sicher , Sie wollen diesen Fördertyp löschen?');
define('FUNDING_TYPE_LIST','Art der Fördermittel Liste');
define('ADD_FUNDING_TYPE','In Art der Fördermittel');
define('EDIT_FUNDING_TYPE','Bearbeiten Art der Fördermittel');
define('FUNDING_TYPE_ALREADY_EXIST','Art der Fördermittel alredy existieren.');
define('DELETE_DOCUMENT_TYPE_CONFORMATION','Sind Sie sicher , Sie wollen diesen Dokumenttyp löschen?');
define('ADD_DOCUMENT_TYPE','In Dokumenttyp');
define('EDIT_DOCUMENT_TYPE','Bearbeiten Dokumenttyp');
define('DOCUMENT_TYPE_LIST','Dokumenttyp Liste');
define('DOCUMENT_TYPE_ALREADY_EXIST','Dokumenttyp alredy existieren.');
define('DELETE_ACCOUNT_TYPE_CONFORMATION','Sind Sie sicher , Sie wollen diesen Kontotyp löschen?');
define('ACCOUNT_TYPE_LIST','Account-Liste');
define('ADD_ACCOUNT_TYPE','In Account');
define('EDIT_ACCOUNT_TYPE','Bearbeiten Account');
define('ACCOUNT_TYPE_ALREADY_EXIST','Account alredy existieren.');
define('DELETE_TEAM_MEMBER_CONFIRMATION','Sind Sie sicher , Sie wollen diesen Teammitglied Typ löschen?');
define('TEAM_MEMBER_TYPE_LIST','Team Member Typliste');
define('ADD_TEAM_MEMBER_TYPE','In Team Member Type');
define('EDIT_TEAM_MEMBER_TYPE','Bearbeiten Team Member Type');
define('TEAM_MEMBER_TYPE_ALREADY_EXIST','Team Member Type alredy existieren.');

//front equity

define('FILE_UPLOAD_SUCCESS','Ihr Vertrag Kopie wurde erfolgreich gesendet.');
define('PLEASE_UPLOAD_ZIP_OR_PDF_FILE','Bitte laden Sie zip oder PDF-Datei');
define('SORRY_THIS_FILE_IS_TOO_LARGE','Es tut uns leid diese Datei ist zu groß.');
define('CONGRATULATIONS','Herzliche Glückwünsche!');
define('YOU_ARE_ALMOST_THERE','Sie sind fast da');
define('WHAT_NEXT_PLEASE_FOLLW_THE_STEP_BELOW','Was als nächstes? Bitte follw den Schritt unten.');
define('SIGN_CONTRACT',''.$funds.' Auftrag');
define('YOU_MAY_HAVE_RECEIVED_AN_EMAIL_WITH_CONTRACT_YOU_NEED_TO_SIGN_AND_SEND_BACK_OR','Sie können eine E-Mail mit Vertrag müssen Sie unterschreiben und zurückschicken oder erhalten haben,');
define('DOWNLOAD_FROM_HERE','Download von hier');
define('THIS_STEP_IS_CONFIRMED_COMPLETED','Dieser Schritt bestätigt und abgeschlossen');
define('UPLOAD_CONTRACT','Hochladen Contract');
define('PLEASE_UPLOAD_SIGNED_CONTRACT_FROM_HERE_AND_WAIT_UNTIL_EQUITY_CROWDFUNDING_TEAM_APPROVES_AND_MARK_IT_AS_CONFIRMED_UPLOAD_CONTRACT_HERE','Bitte laden Sie unterzeichneten Vertrag von hier und warten Sie, bis '.$site_name.' genehmigt und markieren Sie ihn als bestätigt. Vertrags hochladen');
define('PLEASE_DO_NOT_PROCEED_TO_STEP_UNTIL_STEP_IS_CONFIRMED','Bitte nicht gehen Sie zu Schritt.3 bis Schritt.2 bestätigt.');
define('USE_FOLLOWING_BANK_INFORMATION_TO_MAKE_PAYMENT','Verwenden Sie folgende Bankinformationen, um Zahlung zu leisten.');
define('OR_USE_FOLLOWING_DETAILS_TO_WRITE_US_CHECK','ODER Verwenden Sie folgende Angaben, schreiben Sie uns Überprüfen');
define('ACKNOWLEDGE','Bestätigen');
define('UPLOAD_PAYMENT_RECEIPT_SNAP_OR_TRACKING_DETAILS_FOR_THE_PAYMENT_YOU_MADE_UPLOAD_RECEIPT','Laden Sie Empfang der Zahlung Schnapp- oder Tracking-Informationen für die Zahlung, die Sie gemacht. Empfang hochladen');
define('CONFIRMED','Bestätigt');
define('PAYMENT_IS_APPROVED_AND_CONFIRMED_BY_EQUITY_CROWDFUNDING_TEAM','Zahlung genehmigt und vom Eigenkapital Crowdfunding-Team bestätigt.');
define('TRACK_YOUR_SHIPMENT','Sendungsverfolgung');
define('WRITE_A_NOTE','Schreib eine Notitz');
define('EQUITY_LIST',''.$project_name.' Liste');

define('DEAL_TYPE_SETTING_LIST','Deal Typ Einstellung Liste');
define('DEAL_TYPE_NAME_ALREADY_EXIST','Deal Typ Name bereits vorhanden ist.');
define('DEAL_TYPE_SETTING','Deal Typ Einstellung');
define('EDIT_DEAL_TYPE_SETTING','Bearbeiten Deal Typ Einstellung');
define('MIN_PRICE_PER_SHARE','Mindestpreis je Aktie');
define('MAX_PRICE_PER_SHARE','Max Preis pro Aktie');
define('MIN_EQUITY_AVAILABLE','Min '.$project_name.' Verfügbar');
define('MAX_EQUITY_AVAILABLE','Max '.$project_name.' Verfügbar');
define('MIN_COMPANY_VALUATION','Min Unternehmensbewertung');
define('MAX_COMPANY_VALUATION','Max Unternehmensbewertung');
define('DEAL_TYPE_NAME','Deal Typ Name');
define('DEAL_TYPE_DESCRIPTION','Deal Typ Beschreibung');
define('DEAL_TYPE_ICON','Geschäftsart Icon');
define('PLEASE_SELECT_IMAGE_MORE_THAN_1300','Bitte wählen Sie die Bild über 1300 * 600');
define('MIN_CONVERTIBLE_INTEREST','Min Convertible Zins');
define('MAX_CONVERTIBLE_INTEREST','Max Convertible Zins');
define('MIN_VALUATION_CAP','Min Bewertungs Cap');
define('MAX_VALUATION_CAP','Max Bewertungs Cap');
define('MIN_WARRANT_COVERAGE','Min Warrant Coverage');
define('MAX_WARRANT_COVERAGE','Max Warrant Coverage');
define('MIN_CONVERTIBLE_TERM_LENGHT','Min Convertible Lauflänge');
define('MAX_CONVERTIBLE_TERM_LENGHT','Max Convertible Lauflänge');
define('MIN_CONVERSATION_DISCOUNT','Min Conversation Discount');
define('MAX_CONVERSATION_DISCOUNT','Max Conversation Discount');
define('MIN_DEBT_INTEREST','Min Schuldzinsen');
define('MAX_DEBT_INTEREST','Max Schuldzinsen');
define('MIN_DEBT_TERM_LENGTH','Min Debt Lauflänge');
define('MAX_DEBT_TERM_LENGTH','Max Debt Lauflänge');
define('MIN_MAXIMUM_RETURN','Min Max Return');
define('MAX_MAXIMUM_RETURN','Max Max Return');
define('MIN_RETURN_PERCENTAGE','Min Return Prozent');
define('MAX_RETURN_PERCENTAGE','Max Return Prozent');

define('INVESTOR_TYPE_LIST','Investor Typliste');
define('ADD_INVESTOR_TYPE','In Investor Typ');
define('EDIT_INVESTOR_TYPE','Bearbeiten Art der Anleger');
define('INVESTOR_TYPE_NAME','Investor Typ Name');
define('INVESTOR_TYPE_NAME_ALREADY_EXIST','Investor Typ Name bereits vorhanden ist.');
define('DELETE_INVESTOR_TYPE_CONFIRMATION','Are you sure,You want to delete this Investor type?');

define('INVESTMENT_STEPS_FOR_ADMIN','Investitions Schritte für Admin');
define('NO_ACTION_REQUIRED_IN_THIS_STEP_FOR_YOU','Keine Aktion in dieser Schritt für Sie erforderlich ist.');
define('ACKNOWLEDGEMENT','Anerkennung');
define('CONFIRMATION','Bestätigung');
define('RECEIPT_CONFIRMATION','Empfangsbestätigung');

define('URL_COMPANY_CATETORY','URL Firma Kategorie');
define('LANGUAGE_ID','Sprache ID');
define('ENGLISH','Englisch');
define('IMAGE_UPLOAD','Hochladen von Bildern');

define('EQUITY_PERK',''.$project_name.' Vorteil');
define('PERK_TITLE','Perk Titel');
define('TOTAL_CLAIM','Gesamtforderung');
define('EQUITY_UPDATES',''.$project_name.' Aktuelles');
define('NO_UPDATES','Keine Updates');
define('EQUITY_DONATION',''.$project_name.' Spende');
define('EQUITY_COMMENT',''.$project_name.' Kommentar');
define('NO_COMMENTS','Kein Kommentar');
define('EQUITY_REPORT',''.$project_name.' Bericht');
define('PROGRESS','Progress');
define('INVESTOR','Investor');
define('COMPANY','Gesellschaft');
define('CREATE_BY','Erstellt von');
define('EQUITY_OVERVIEW',''.$project_name.' Überblick');
define('INVESTMENT','Investition');
define('GALLERY','Galerie');
define('DOWNLOAD_CONTRACT_DOCUMENT','Herunterladen der Ausschreibungsunterlagen');
define('CONTRACT_DOCUMENT_SUCCESSFULLY_UPLOADED','Ausschreibungsunterlagen erfolgreich hochgeladen');
define('UPLOAD_CONTRACT_DOCUMENT','Hochladen der Ausschreibungsunterlagen');
define('AS_YOU_HAVE_TO_DECIDED_TO_DECLINE','As you have decided to decline this %s, please provide reason why you declined this %s, this will better guide %s to help improve his %s and resubmit for review.');
define('SAVE_THIS_REASON_TO_USE_ON_OTHER','Speichern Sie diese Grund, sich auf andere zu verwenden');
define('REASON_TO_DECLINE_THIS','Grund für diesen Rückgang');
define('CANCEL','Absagen');
define('WRITE_REASON_WHY_YOU_DEACTIVATED_THIS','Write Grund, warum Sie dies deaktiviert');
define('HIDDEN_MODE','Versteckten Modus');
define('AS_YOU_HAVE_TO_DECIDED_TO_INACTIVE','Als Sie sich entschieden haben, dieses %s zu inaktiv, wählen Sie bitte folgende Optionen, bevor Sie es tun.');
define('SELECT_THIS_OPTION_IF_YOU_WANT_TO_HIDE','Wählen Sie diese Option, wenn Sie %s vollständig von der Website ausblenden möchten., Werden keine Benutzer in der Lage, dieses %s zu sehen.');
define('SELECT_THIS_OPTION_IF_YOU_WANT_TO_SHOW','Wählen Sie diese Option, wenn Sie %s mit inaktiven Modus angezeigt werden soll, werden alle Benutzer in der Lage, dieser %s zu sehen, aber nicht in der Lage zu %s, %s, %s, zu folgen.');
define('INACTIVE_MODE','Inaktiven Modus');
define('PLEASE_NOTE_IF_ONCE_IS_INACTIVATED','HINWEIS: Bitte beachten Sie, wenn einmal %s inaktiviert alle %s');
define('YOU_CAN_HOLD_ALL_UNTILL_YOU_RESOLVE_YOUR_DOUBTS_AND_REASONS','Sie können halten Sie alle %s, bis Sie Ihre Zweifel und Gründen beschließen, dies %s mit %s zu deaktivieren und wieder zu aktivieren.');
define('IF_MANAGED_MANUALLY_BY_TEAM','Wenn manuell durch %s Team verwaltet');
define('YOU_CAN_REFUND_ALL_FUND','Sie können durch Banküberweisung zu erstatten alle %s manuell auf die %s in oder überprüfen Sie, ob %s werden manuell durch <Website-Name> Team bearbeitet.');
define('FEEDBACK','Feedback');
define('IMAGE','Image');
define('PERK_AMMOUNT','Perk Betrag');
define('IP','Ip');
define('ARE_YOU_SURE_TO_DELETE_IMAGE', 'Sind Sie sicher, die dem Bild löschen?');
define('MAKE_THIS_FEATURED', 'Machen Sie dieses Feature');
define('MAKE_THIS_UNFEATURED', 'Machen Sie diese Unfeatured');
define('CANT_BE_FEATURED', '`T Hervorgehoben werden');
define('CANT_DELETE', '`T Löschen');
define('INACTIVE_THIS', 'Inaktive Diese');
define('EQUITY_SETTING', ''.$project_name.' Rahmen');
define('ACCREDENTIAL_REQUIRED', 'Akkreditierungs Erforderlich?');


define('BY_PROJECT', 'Durch '.$project_name.'');
define('BY_OWNER_NAME', 'Mit dem Besitzer Name');
define('BY_PAR_FUNDED', 'Durch% finanziert');
define('MAX_FUNDED', 'Max Gefördert');
define('MIN_FUNDED', 'Min Gefördert');
define('CLEAR_ALL_FILTERS', 'Alle Filter zurücksetzen');
define('INVESTMENT_STATUS', 'Investment-Status');
define('DOCUMENT_REJECTED', 'Document Abgelehnt');
define('TRACKING_SHIPMENT_CONFIRMED', 'Verfolgung Sendungs ​​Bestätigte');

define('CASH_FLOW_STATUS', 'Cash Flow-Status');
define('NONE','Keine');
define('CASH_FLOW','Geld fließt an die Inhaber');
define('ARE_YOU_SURE_YOU_WANT_TO_UPDATE_CASHFLOW_RECORD', 'Sind Sie sicher , Sie wollen Cash Flow Status-Update ?');
// you can not remove this line because it is used for admin can select cash flowing type
define ("CASH_FLOW_ARRAY", serialize (array (NONE,CASH_FLOW)));
define('HAS_BEEN_UPDATED_SUCCESSFULLY', 'hat erfolgreich aktualisiert wurde.');
define('INVESTOR_TYPE_DESCRIPTION','Investor Typ Beschreibung');
define('INVESTOR_OPTION_LINK','Investor Option Link-');	
define('INVESTMENT_TAX_RELIEF','Investor Tax Relief');
define('ADD_INVESTOR_TAX_RELIEF','In Investor Tax Relief');
define('EDIT_INVESTOR_TAX_RELIEF','Bearbeiten Investor Tax Relief');
define('INVESTMENT_TAX_RELIEF_TYPE','Investor Tax Relief Typ');

define('MAIN_PHASE', 'Main Phase');
define('SUB_PHASE', 'Sub Phase');


/*admin_graph_lang.php*/
define('GRAPHICAL_REPORT_OF_PROJECT','Grafische Bericht '.$project.'');
define('WEEKLY_AVERAGE_PROJECTS_LINE','Wochendurchschnitt '.$project.' Die [Leitung]');
define('WEEKLY_AVERAGE_PROJECTS_BAR','Wochendurchschnitt '.$project.' Die [bar]');
define('MONTHLY_AVERAGE_PROJECTS_LINE','Monatsdurchschnitt '.$project.' Die [Leitung]');
define('MONTHLY_AVERAGE_PROJECTS_BAR','Monatsdurchschnitt '.$project.' Die [bar]');
define('YEARLY_AVERAGE_PROJECTS_LINE','Jahresdurchschnitt '.$project.' Die [Leitung]');
define('YEARLY_AVERAGE_PROJECTS_BAR','Jahresdurchschnitt '.$project.' Die [bar]');
define('TOTAL_PROJECTS_PIE_CHART','Total '.$project.' Die Kreisdiagramm');
define('TRANSACTION_PROJECTS_PIE_CHART','Transaktionen '.$project.'Kreisdiagramm');
define('GRAPHICAL_REPORT_OF_USER_REGISTRATION','Grafischen Bericht der Benutzerregistrierung');
define('WEEKLY_AVERAGE_REGISTRATION_LINE','Weekly Average Registration [Leitung]');
define('WEEKLY_AVERAGE_REGISTRATION_BAR','Weekly Average Registration [bar]');
define('MONTHLY_AVERAGE_REGISTRATION_LINE','Monatliche Durchschnitts Registration [Leitung]');
define('MONTHLY_AVERAGE_REGISTRATION_BAR','Monatliche Durchschnitts Registration [bar]');
define('YEARLY_AVERAGE_REGISTRATION_LINE','Jahresmittel Registration [Leitung]');
define('YEARLY_AVERAGE_REGISTRATION_BAR','Jahresmittel Registration [bar]');

define('WITH_STACKING', 'mit Stapel');
define('WITHOUT_STACKING', 'ohne Stapel');
define('BARS', 'Riegel');
define('LINES', 'Linien');
define('LINES_WITH_STEPS', 'Linien mit Schritten');
define('REGISTRATION', 'Registrierung');


/*admin_newsletter_lang.php*/

define('UPLOAD_CSV', 'Laden Sie CSV');
define('DOWNLOAD_SAMPLE_CSV_FILE', 'Download Beispiel CSV-Datei');
define('ADD_NEWSLETTER', 'In Rundschreiben');
define('ADD_NEWSLETTER_JOB', 'In Rundschreiben Job');
define('NEWSLETTER_SETTING', 'Rundschreiben Setting');
define('ATTACH_FILE', 'Datei anhängen');
define('VIEW_ATTACHMENT', 'Anlagenansicht');
define('ALLOW_UNSUBCRIBE_LINK', 'Erlauben Abmeldelink');
define('YES', 'Ja');
define('NOS', 'Nein');
define('SUBSCRIBE_TO', 'Abonnieren');
define('NONES', 'Keine');
define('ALL', 'Alle');
define('SELECT_NEWSLETTER', 'Wählen Sie die Rundschreiben');
define('SELECT_USER', 'Wählen Sie Benutzer');
define('SELECT', 'Auswählen');
define('SUBSCRIBER', 'Teilnehmer');
define('REGISTERED', 'Angemeldet');
define('BOTH', 'Beide');
define('SUBSCRIBER_USER_LIST', 'Subscriber User List');
define('REGISTER_USER_LIST', 'Registrieren User-Liste');
define('ALL_USER', 'Alle Nutzer');
define('RECURSIVE', 'Rekursive');
define('NEWSLETTER_TYPE', 'Rundschreiben Type');
define('DAILY', 'Täglich');
define('WEEKLY', 'Wöchentlich');
define('MONTHLY', 'Monatlich');
define('DURATION', 'Dauer');
define('SELECT_A_DAY_OF_WEEK', 'Wählen Sie einen Wochentag');
define('SELECT_A_DAY', 'Wählen Sie einen Tag');
define('SELECT_START_DATE', 'Wählen Sie Startdatum');
define('SELECT_END_DATE', 'Wählen Sie Enddatum');
define('JOB_START_DATE', 'Jobstartdatum');
define('JOB_CREATE_DATE', 'Job Erstellungsdatum');
define('TOTAL_SUBSCRIBER', 'Insgesamt Subscriber');
define('TOTAL_SEND', 'Insgesamt senden');
define('TOTAL_OPEN', 'Total Öffnen');
define('TOTAL_FAIL', 'Gesamtausfall');
define('NEWSLETTERS', 'Rundschreiben');
define('TOTAL_USER', 'Insgesamt Erfahrungs');
define('NEWSLETTER_JOB', 'Newsletter Stellenangebote');
define('REFRES', 'Aktualisieren');
define('DRAFT', 'Entwurf');
define('SENT', 'Sent');
define('START_DATE', 'Anfangsdatum');
define('STATISTICS', 'Statistiken');
define('SENDS', 'Senden');
define('OPEN', 'Öffnen');
define('CREATE_DATE', 'Erstellungsdatum');
define('NO_RECORDS_FOUND', 'Keine Aufzeichnungen gefunden.');
define('IMPORTANT_NOTE_PLEASE_SET_THE_CRON_JOB_ON_YOUR_SERVER_WITH_URL', 'Wichtiger Hinweis: Bitte stellen Sie den cron-Job auf dem Server mit der URL.');
define('EXPORT_CSV', 'Export CSV');
define('IMPORT_CSV', 'CSV-Import');
define('FROM_NAME', 'Von Namen');
define('FROM_EMAIL_ADDRESS', 'Von E-Mail Adresse');
define('REPLY_NAME', 'Antworten Name');
define('REPLY_EMAIL_ADDRESS', 'Antwort E-Mail Adresse');
define('NEW_SUBSCRIBE_EMAIL', 'Neue E-Mail abonnieren');
define('UNSUBSCRIBE_EMAIL', 'Unsubscriber Email');
define('USER_DEFAULT_NEWSLETTER', 'User Default Newsletter');
define('SELECTED', 'Ausgewählt');
define('NO_TEMPLATE', 'Kein Template');
define('NUMBER_OF_EMAIL_SEND', 'Anzahl E-Mail senden');
define('BREAK_BETWEEN_NO_EMAIL_SEND', 'Pause zwischen No. E-Mail senden');
define('BREAK_TYPE', 'Break Typ');
define('MAILER', 'Mailer');
define('SEND_MAIL_PATH', 'Mail-Pfad');
define('IF_MAILER_IS_SENDMAIL', '(Wenn Mailer ist sendmail)');
define('SMTP_PORT', 'SMTP-Port');
define('SMTP_HOST', 'SMTP-Host');
define('SMTP_EMAIL', 'SMTP-E-Mail');
define('SMTP_PASSWORD', 'SMTP-Passwort');
define('SEND_TEST_MAIL', 'Send Test Mail');
define('PHP_MAIL', 'PHP-Mail');
define('SMTP', 'SMTP');
define('SENDMAIL', 'E-Mails senden');
define('EMAIL_SETTING', 'E-Mail-Einstellung');
define('SENDER_EMAIL', 'Absender E-Mail');
define('RECEIVER_EMAIL', 'Empfänger E-Mail');
define('SENT_MESSAGE_ON_SENDING_A_TEST_MAIL', 'Gesendete Nachricht auf das Senden einer Test-Mail');

define('WEEKLY_DAY', 'Wöchentliche Day');
define('MONTHLY_DAY', 'Monatliche Day');
define('END_DATE_MUST_BE_GREATER_THAN_START_DATE', 'Enddatum muss größer als Anfangsdatum sein');
define('FAIL', 'Scheitern');
define('CONTENT', 'Inhalt');
define('UPLOAD_CSV_ONLY', 'Hochladen csv nur');
define('NAMES','Name');


/*admin_setting_lang.php*/
define('EMAIL_SETTING_UPDATED_SUCCESSFULLY', 'E-Mail-Einstellungen erfolgreich aktualisiert.');
define('FACEBOOK_SETTING', 'Facebook-Einstellung');
define('FACEBOOK_SETTING_UPDATED_SUCCESSFULLY', 'Facebook-Einstellungen erfolgreich aktualisiert.');
define('FACEBOOK_SETTING_YOU_HAVE_ENTERED_ARE_WAHR', 'Facebook-Einstellungen, die Sie eingegeben haben, sind wahr.');
define('FACEBOOK_PROFILE_FULL_URL', 'Facebook-Profil Volle URL');
define('FACEBOOK_APPLICATION_ID', 'Facebook Anwendungs-ID');
define('FACEBOOK_LOGIN_ENABLE', 'Facebook Login Aktivieren');
define('FACEBOOK_APPLICATION_API_KEY', 'Facebook Application API Key');
define('FACEBOOK_APPLICATION_SECRET_KEY', 'Facebook-Anwendung Secret Key');
define('ALLOW_POST_ON_FACEBOOK_WALL', 'Erlauben Auf Facebook Wand');
define('FACEBOOK_ACCESS_TOKEN', 'Facebook Direkter Zugang Token');
define('FACEBOOK_USER_ID', 'Facebook User ID');
define('FACEBOOK_IMAGE', 'Facebook Image');
define('WANT_TO_CHANGE_FACEBOOK_DETAILS', 'Willst du Ändern Facebook-Details');
define('CONNECT_TO_FACEBOOK', 'Connect to Facebook');
define('FILTER_SETTING', 'Filtereinstellung');
define('ROUNDING_OFF_DAYS', 'Abrundung (Days)');
define('POPULAR_PER', 'Beliebt(%)');
define('MOST_FUNDED_PER', 'Die meisten Gefördert (%)');
define('SUCCESS_STORIES_PER', 'Erfolgsgeschichten(%)');
define('FILTER_SETTING_UPDATED_SUCCESSFULLY', 'Filter-Einstellungen erfolgreich aktualisiert.');
define('GOOGLE_SETTING', 'Google-Einstellung');
define('GOOGLE_SETTING_UPDATED_SUCCESSFULLY', 'Google-Einstellungen erfolgreich aktualisiert.');
define('GOOGLE_ENABLED', 'Google Aktiviert');
define('CONSUMER_KEYS', 'Consumer Key');
define('CONSUMER_SECRETS', 'Konsumentengeheimnis');
define('IMAGE_SETTING', 'Bildeinstellung');
define('PROJECT_THEUMBNAIL_WIDTH', ''.$project.' Thumbnail Breite');
define('PROJECT_THEUMBNAIL_HEIGHT', ''.$project.' Thumbnail Höhe');
define('PROJECT_THEUMBNAIL_ASPECT_RATIO', ''.$project.' Miniaturseitenverhältnis');
define('TRUE', 'WAHR');
define('FALSE', 'FALSCH');
define('PROJECT_SMALL_WIDTH', ''.$project.' Kleine Breite');
define('PROJECT_SMALL_HEIGHT', ''.$project.' Kleine Höhe');
define('PROJECT_MEDIUM_WIDTH', ''.$project.' Medium Breite');
define('PROJECT_MEDIUM_HEIGHT', ''.$project.'  Mittlere Größe');
define('PROJECT_LARGE_WIDTH', ''.$project.' Große Breite');
define('PROJECT_LARGE_HEIGHT', ''.$project.' Große Höhe');
define('USER_SMAIL_WIDTH', 'Benutzer Kleine Breite');
define('USER_SMAIL_HEIGHT', 'Benutzer Kleine Höhe');
define('USER_MEDIUM_WIDTH', 'Benutzer Mittlere Breite');
define('USER_MEDIUM_HEIGHT', 'Benutzermittelhoch');
define('USER_BIG_WIDTH', 'Benutzer Big Breite');
define('USER_BIG_HEIGHT', 'Benutzer Big Höhe');
define('USER_THUMBAIL_ASPECT_RATIO', 'Benutzerminiaturseitenverhältnis');
define('IMAGE_SIZE_SETTING_UPDATED_SUCCESSFULLY', 'Bildgröße-Einstellungen erfolgreich aktualisiert.');
define('PROJECT_GALLERY_THUMBNAIL_ASPECT_RATIO', ''.$project.' Gallery Miniaturseitenverhältnis');
define('LINKEDIN_SETTING', 'Linkedin Einstellung');
define('LINKEDIN_SETTING_UPDATED_SUCCESSFULLY', 'Linkedin Einstellungen erfolgreich aktualisiert.');
define('LINKEDIN_PROFILE_FULL_URL', 'Linkdin Profil Volle URL');
define('LINKEDIN_ENABLED', 'Linkedin Aktiviert');
define('LINKEDIN_ACCESS', 'LinkdIn API Key');
define('LINKEDIN_SECRET', 'LinkdIn Secret Key');
define('MESSAGE_SETTING', 'Nachricht Setting');
define('MESSAGE_SETTING_UPDATED_SUCCESSFULLY', 'Mitteilungseinstellungen erfolgreich aktualisiert.');
define('SEND_EMAIL_TO_ADMIN_ON_NEW_MESSAGE', 'E-Mail schicken, um auf neue Nachricht admin.');
define('ENABLE', 'Aktivieren');
define('DISABLE', 'Deaktivieren');
define('DEFAULT_MESSAGE_SUBJECT', 'Standardmeldung unterliegen');
define('MESSAGE_MODULE', 'Meldungsmodul');
define('META_SETTING', 'Meta-Einstellung');
define('META_SETTING_UPDATED_SUCCESSFULLY', 'Meta-Einstellungen erfolgreich aktualisiert.');
define('SITE_SETTING', 'Website-Einstellung');
define('SITE_SETTING_UPDATED_SUCCESSFULLY', 'Website-Einstellungen erfolgreich aktualisiert.');
define('SITE_VERSION', 'Site-Version');
define('CAPTCHA_SETTING', 'Captcha-Einstellungen');
define('CONTACT_US_PAGE', 'Kontaktieren Sie uns Seite');
define('SIGN_UP_PAGE', 'Registrieren Seite');
define('CREATE_A_RECAPTCHA_KEY_FOR_YOUR_SITE_FROM_HERE_AND_SET_THE_KEY_VALUES_BELOW', 'Erstellen Sie ein reCAPTCHA Schlüssel für Ihre Website von hier aus und legen Sie die Schlüsselwerte unten.');
define('PRIVATE_KEY', 'Private Key');
define('PUBLIC_KEY', 'Public Key');
define('LOGO_SETTING', 'Logo Setting');
define('SITE_LOGO', 'Site-Logo');
define('SITE_LOGO_HOVER', 'Site-Logo Hover');
define('FAVICON_IMAGE', 'Favicon Bild');
define('LANGUAGE_AND_DATE_TIME_SETTING', 'Sprache & Datum Zeiteinstellung');
define('SITE_LANGUAGE', 'Website-Sprache wählen');
define('DATE_FORMAT', 'Datumsformat');
define('TIME_FORMAT', 'Zeitformat');
define('SITE_TIME_ZONE', 'Website-Zeitzone');
define('FEED_SETTINGS', 'Feed-Einstellungen');
define('ENABLE_FACEBOOK_STREAM', 'Aktivieren Sie Facebook-Stream');
define('ENABLE_TWITTER_STREAM', 'Aktivieren Sie Twitter-Stream');
define('PROJECT_LIMITS', ''.$project.' Limits');
define('MAXIMUM_NO_PROJECT_PER_YEAR_FOR_ONE_USER', 'Maximale Anzahl '.$project.' Pro Jahr (für eine Nutzungr)');
define('MAXIMUM_NO_DONATION_PER_PROJECT', 'Maximale Anzahl der Spenden pro '.$project.' (Für eine Nutzungr)');
define('PROJECT_GOAL_AMOUNT_SETTING', ''.$project.' Tor Anzahl Setting');
define('MINIMUM_PROJECT_GOAL_AMOUNT', 'Minimum '.$project.'Tor Betrag');
define('MAXIMUM_PROJECT_GOAL_AMOUNT', 'Maximal '.$project.'Tor Betrag');
define('PROJECT_TARGET_DAYS_SETTING', ''.$project.' \"Zielreich Einstellung');
define('MINIMUM_DAYS_FOR_PROJECTS', 'Mindest Tage '.$project.'');
define('MAXIMUM_DAYS_FOR_PROJECTS', 'Maximale Tage '.$project.'');
define('DONATION_AMOUNT_SETTING', 'Spendenbetrag Einstellung');
define('MINIMUM_DONATION_AMOUNT', 'Mindestspende Betrag');
define('MAXIMUM_DONATION_AMOUNT', 'Maximale Spende Betrag');
define('PAYPAL_RESTRICTION_FOR_MAXIMUM_DONATION_AMOUNT', '(HINWEIS: Paypal Beschränkung für maximale Spendenbetrag ist $ 2000 USD Set Grenze als Ihre Währung Gespräch Rate..)');
define('REWARD_AMOUNT_SETTING', 'Belohnung (Prok) Amount Setting');
define('PERK_ENABLE_DISABLE', 'Perk aktivieren / deaktivieren');
define('MINIMUM_PERK_AMOUNT', 'Mindest Reward (Prok) Amount');
define('MAXIMUM_PERK_AMOUNT', 'Maximale Belohnung (Prok) Amount');
define('DONATION_TYPE_SETTING', 'Donation Typ Einstellung');
define('PAYMENT_GATEWAY', 'Zahlungs-Gateways');
define('PAYMENT_ADAPTIVE', 'Paypal Adaptive');
define('WALLET', 'Kurztasche');
define('PROJECT_ACHIVE_GOAL_AUTO_PREAPPROVAL', ''.$project.' \"Achieve Goal Auto Preapproval');
define('NOTE_NO_MEAN_PREAPPROVED_DOANTION_AMOUNT_TRANSFER_ON_PROJECT_END_DATE', '(HINWEIS: keineswegs preapproved Spendenbetrag Übertragung auf Ende date.Yes bedeutet preapproved Spendenbetrag Übertragung, wenn \'$ Projekt..\' Seinen Zielbetrag erreicht Ziel vor Enddatum \"$ Projekt.. \'.)');
define('FUNDING_DONATION_TYPE_FOR_PROJECTS', 'Finanzierung / Donation Typ Für '.$project.'');
define('FIXED', 'Fester');
define('FLEXIBLE', 'Flexibel');
define('NOTE_WORK_ONLY_FOR_PREAPPROVAL_IS_ENABLE', '(HINWEIS: '.$project.' Funktioniert nur für Preapproval ist aktivieren Standardmäßig Funding / Donation Art der festgelegt ist.)');
define('FIXED_PROJECT_COMMISION', 'Feste '.$project.' Kommission (%)');
define('FLEXIBLE_SUCCESSFUL_PROJECT_COMMISION', 'Flexible Erfolgreiche '.$project.' Kommission (%)');
define('FLEXIBLE_UNSUCCESSFUL_PROJECT_COMMISION', 'Flexible Erfolglose '.$project.' Kommission (%)');
define('PERSONAL_INFORMATION_SETTING', 'Persönliche Informationen einstellen');
define('ENABLE_PERSONAL_INFORMATION', 'Aktivieren Sie Persönliche Informationen');
define('TWITTER_SETTING', 'Twitter-Einstellung');
define('TWITTER_SETTING_UPDATED_SUCCESSFULLY', 'Twitter-Einstellungen erfolgreich aktualisiert.');
define('TWITTER_SETTING_YOU_HAVE_ENTERED_ARE_TRUE', 'Twitter Einstellungen, die Sie eingegeben haben, sind wahr.');
define('TWITTER_PROFILE_FULL_URL', 'Twitter-Profil Volle URL');
define('TWITTER_ENABLE', 'Twitter aktiviert');
define('ALLOW_POST_ON_TWITTER', 'Erlauben Auf Twitter veröffentlichen');
define('ACCESS_TOKEN_SECRET', 'Zugriffstoken Geheimnis');
define('ACCESS_TOKEN', 'Zugangstoken');
define('TWIITER_IMAGE', 'Twitter Bild');
define('WANT_TO_CHANGE_TWITTER_DETAILS', 'Willst du Ändern Twitter Details');
define('CONNECT_TO_TWITTER', 'Eine Verbindung mit Twitter');
define('ADD_TWITTER_DETAIL', 'In Twitter Details');
define('YAHOO_SETTING', 'Yahoo-Einstellung');
define('YAHOO_SETTING_UPDATED_SUCCESSFULLY', 'Yahoo-Einstellungen erfolgreich aktualisiert.');
define('YAHOO_ENABLED', 'Yahoo Aktiviert');
define('YOUTUBE_SETTING', 'Youtube-Einstellung');
define('YOUTUBE_PROFILE_FULL_URL', 'Youtube Profil vollständige URL');
define('YOUTUBE_ENABLED', 'Youtube aktivieren');
define('YOUTUBE_SETTING_UPDATED_SUCCESSFULLY', 'Youtube-Einstellungen erfolgreich aktualisiert.');
define('GOOGLE_PLUS_SETTING', 'Google Plus aktivieren');
define('GOOGLE_PLUS_SETTING_UPDATED_SUCCESSFULLY', 'Google Plus-Einstellungen erfolgreich aktualisiert.');
define('GOOGLE_PROFILE_FULL_URL', 'Google Plus Profil im url');
define('GOOGLE_PLUS_ENABLED', 'Google Plus aktivieren');
define('PROJECT_ENDING_SOON_SETTING', ''.$project.' Endet in Kürze einstellen');
define('PROJECT_ENDING_SOON_DAYS', ''.$project.' Bald endende Tagen');
define('HOME_SLIDER', 'Wählen Sie, was Sie anzeigen möchten');
define('DYNAMIC_SLIDER', 'Statische Banner');
define('MANAGE_SLIDER', 'Sliders verwalten');
define('ADD_SLIDER', 'In Slider');
define('BRIEF', 'Kurz');
define('SLIDER_IMAGE', 'Slider Bild');
define('BUTTON_TITLE', 'Button Titel');
define('BUTTON_LINK', 'Button Link');
define('EDIT_SLIDER', 'Bearbeiten Slider');
define('BANNER', 'Banner');
define('TO_GO_MANAGE_IMAGE', 'zu gehen und Image-Banner Verwaltung in Schieberegler.');
define('TO_GO_MANAGE_FEATURED', 'zu gehen und funktionsfähige Verwaltung '.$project.'.');
define('TO_GO_MANAGE_ORDER_OF_IMAGE', 'zu gehen und die Reihenfolge der Bild Banner Verwaltung in Schieberegler.');
define('TO_GO_MANAGE_ORDER_OF_FEATURED', 'zu gehen und die Reihenfolge der Feature-Verwaltung '.$project.'Im Schieberegler.');

define('SLIDER_SETTING', 'Slider Sortierung');
define('HOME_BANNER_SLIDER', 'Home Banner Slider');
define('BANNER_SLIDER', 'Banner Slider');
define('SOCIAL_SETTING', 'Social Setting');
define('OTHER_SETTING', 'Weitere Einstellungen');
define('DROPDOWN_MANAGE', 'Dropdown verwalten');
define('TAXONOMY_SETTING_UPDATED_SUCCESSFULLY', 'Taxonomie-Einstellungen erfolgreich aktualisiert.');
define('SINGULAR', 'Singular');
define('PLURAL', 'Plural');
define('PAST_PARTICIPATE', 'Historische Mitmachen');
define('GERUND', 'Gerundium');
define('PROJECT_NAME_SINGULAR', 'Projektname Singular');
define('PROJECT_NAME_PLURAL', 'Projektname Plural');
define('PROJECT_OWNER_SINGULAR', 'Projektbesitzer Singular');
define('PROJECT_OWNER_PLURAL', 'Projektbesitzer Plural');
define('FUNDS_SINGULAR', 'Funds Singular');
define('FUNDS_PLURAL', 'Funds Plural');
define('FUNDS_PAST_PARTICIPATE', 'Funds Historische Mitmachen');
define('FUNDS_GERUND', 'Funds Gerund');



define('COMMENTS_SINGULAR', 'Kommentare Singular');
define('COMMENTS_PLURAL', 'Kommentare Plural');
define('COMMENTS_PAST_PARTICIPATE', 'Kommentare Historische Mitmachen');
define('COMMENTS_GERUND', 'Kommentare Gerund');


define('UPDATES_SINGULAR', 'Aktuelles Singular');
define('UPDATES_PLURAL', 'Aktuelles Plural');
define('UPDATES_PAST_PARTICIPATE', 'Aktuelles Vergangenheit Mitmachen');
define('UPDATES_GERUND', 'Aktuelles Gerund');


define('FOLLOWER_SINGULAR', 'Verfolger Singular');
define('FOLLOWER_PLURAL', 'Verfolger Plural');
define('FOLLOWER_PAST_PARTICIPATE', 'Verfolger Historische Mitmachen');
define('FOLLOWER_GERUND', 'Verfolger Gerund');
define('PREVIOUS', 'Zurück');

define('PROJECT_UPDATES', ''.$project.' Updates');

define('PROJECT_PERK', ''.$project.' Vorteil');
define('CLAIMED_PERK', 'Behauptete Perk');
define('NO_PERK', ' Kein Perk');
define('PROJECT_DONATION', ''.$project.' Spende');
define('PROJECT_COMMENT', ''.$project.' Bemerkungen');
define('PROJECT_LIST', ''.$project.'  Liste');
define('ADD_TAGS', 'Schlagwörter hinzufügen');
define('PROJECT_TAGS', ''.$project.' Stichworte');
define('ADD_PROJECT_TAGS', 'Hinzufügen '.$project.' Stichworte');
define('TAGS', 'Schlagwörter');
define('TAGS_LIST', 'Schlagwörter Liste');
define('INTEREST_TAGS_LIST', 'Interesse Schlagwörter Liste');
define('SKILL_TAGS_LIST', 'Geschicklichkeit Schlagwörter Liste');
define('USER_NAME', 'Benutzername');
define('TRANSACTIONO_REPORT', 'Transaktionen melden');
define('EARNED', 'Errungenschaften');
define('TRANSACTION_ID', 'Transaktions-ID');
define('ADD_PROJECT_CATEGORY', 'Hinzufügen '.$project.' Kategorie');
define('PROJECT_CATEGORY', ''.$project.' Kategorie');
define('PARENT_CATEGORY', 'Eltern-Kategorie');
define('MAIN_CATEGORY', 'Hauptkategorie');
define('PROJECT_CATEGORY_NAME', ''.$project.' Name der Kategorie');
define('TAGS_NAME', 'Interesse Namen');
define('GOAL_AMOUNT', 'Ziel Betrag');
define('RAISED_AMOUNT', 'Raised Betrag');
define('VIEW_COMMENTS', 'View Comments');
define('HAS_BEEN_DELETED_SUCCESSFULLY', 'wurde erfolgreich gelöscht.');
define('YOU_CAN_NOT_DELETE_ACTIVE_PROJECT', 'Sie können nicht aktiv löschen '.$project.'');
define('HAS_BEEN_ACTIVATED_SUCCESSFULLY', 'wurde erfolgreich aktiviert.');
define('HAS_BEEN_APPROVED_SUCCESSFULLY', 'erfolgreich zugelassen.');
define('YOU_CAN_NOT_ACTIVE_EXPIRED_OR_ALREADY_ACTIVE', 'You can not active expired or already active, successful or failure ' . $project . '.');

define('YOU_CAN_NOT_INACTIVE_EXPIRED_OR_ALREADY_ACTIVE', 'You can not inactive expired or already inactive, successful or failure ' . $project . '.');

define('HAS_BEEN_INACTIVATED_SUCCESSFULLY', 'hat erfolgreich inaktiviert wurde.');
define('HAS_BEEN_DECLIENED_SUCCESSFULLY', 'hat erfolgreich abgelehnt worden.');
define('CANNOT_BE_DECLIENED', 'kann nicht zurückgehen werden,');
define('HAS_BEEN_FEATURED_SUCCESSFULLY', 'hat erfolgreich gekennzeichnet worden.');
define('HAS_BEEN_ACTIVATED_AND_SET_FEATURED_SUCCESSFULLY', 'aktiviert wurde und Set erfolgreich vorgestellt.');
define('CANNOT_BE_SET_TO_FEATURED_AS_IT_IS_EXPIRED_SUCCESSFUL_FAILURE', 'cannot be set to featured as it is expired, successful or failure');
define('HAS_BEED_REMOVE_FROM_FEATURED_SUCCESSFULLY', 'war von Erfolg gekennzeichnet entfernen');
define('NEW', 'Neu');
define('NEWS', 'Neu');
define('HAS_BEEDN_ADDED_SUCCESSFULLY', 'erfolgreich hinzugefügt wurde');
define('HAS_BEEDN_UPDATED_SUCCESSFULLY', 'wurde erfolgreich aktualisiert');
define('NEW_RECORDS_HAS_BEEN_ADDED_SUCCESSFULLY', 'Neuer Eintrag(s) has been added successfully.');
define('DECLIENED', 'Ablehnen');
define('NOT_FEATURED', 'Nicht Hervorgehoben');
define('ADD_CATEGORIES', 'In Kategorien');
define('CATEGORIES_TYPE', 'Kategorie Typ');
define('ADD_AMOUNT', 'Betrag hinzufügen');
define('WHERE_TO_DISPLAY_CURRENCY_SYMBOL', 'Wo Währungssymbol angezeigt werden?');
define('DECIMAL_POINTS_AFTER_AMOUNT', 'Dezimalpunkte nach Betrag');
define('ARE_YOU_SURE_YOU_WANT_TO_DELETE_SELECTED_PROJECT', 'Are you sure, You want to delete selected ' . $project . '(s)?');
define('ARE_YOU_SURE_YOU_WANT_TO_ACTIVE_SELECTED_PROJECT', 'Are you sure, You want to active selected ' . $project . '(s)?');
define('ARE_YOU_SURE_YOU_WANT_TO_APPROVE_SELECTED_PROJECT', 'Are you sure, You want to approve selected ' . $project . '(s)?');
define('ARE_YOU_SURE_YOU_WANT_TO_DECLINE_SELECTED_PROJECT', 'Are you sure, You want to declined selected ' . $project . '(s)?');
define('ARE_YOU_SURE_YOU_WANT_TO_FETAUTE_SELECTED_PROJECT', 'Are you sure, You want to Featured selected ' . $project . '(s)?');
define('ARE_YOU_SURE_YOU_WANT_TO_REMOVE_FEATURE_SELECTED_PROJECT', 'Are You sure, you want to Remove Featured selected ' . $project . '(s)?');

define('ARE_YOU_SURE_YOU_WANT_TO_DELETE_SELECTED_RECORD', 'Are you sure, You want to delete selected record(s)?');
define('ARE_YOU_SURE_YOU_WANT_TO_ACTIVE_SELECTED_RECORD', 'Are you sure, You want to active selected record(s)?');
define('ARE_YOU_SURE_YOU_WANT_TO_INACTIVE_SELECTED_RECORD', 'Are you sure, You want to inactive selected record(s)?');
define('ARE_YOU_SURE_YOU_WANT_TO_SPAMMER_SELECTED_RECORD', 'Are you sure, You want to spamer selected record(s)?');
define('ARE_YOU_SURE_YOU_WANT_TO_REMOVE_ACTIVE_SELECTED_CURRENCIES', 'Are you sure, You want to active selected currencies?');
define('ARE_YOU_SURE_YOU_WANT_TO_REMOVE_INACTIVE_SELECTED_CURRENCIES', 'Are you sure, You want to inactive selected currencies?');
define('AMOUNT_SETTING_UPDATED_SUCCESSFULLY', 'Menge Einstellungen erfolgreich aktualisiert.');


define('PROJECT_SMALL_IMAGE_HEIGHT', ''.$project.' Kleine Bildhöhe');
define('PROJECT_MEDIUM_IMAGE_WIDTH', ''.$project.' Medium Bildbreite');
define('PROJECT_MEDIUM_IMAGE_HEIGHT', ''.$project.' Medium Bildhöhe');
define('PROJECT_LARGE_IMAGE_WIDTH', ''.$project.' Große Bildbreite');
define('PROJECT_LARGE_IMAGE_HEIGHT', ''.$project.' Großes Bild Höhe');
define('PROJECT_SMALL_IMAGE_WIDTH', 'Benutzerkleinbildbreite');
define('USER_SMALL_IMAGE_HEIGHT', 'Benutzer Kleine Bildhöhe');
define('USER_BIG_IMAGE_WIDTH', 'Benutzer Big Bildbreite');
define('USER_BIG_IMAGE_HEIGHT', 'Benutzer Big Bildhöhe');
define('USER_MEDIUM_WIDTH_IMAGE_WIDTH', 'Benutzer Medium Bildbreite');
define('USER_MEDIUM_WIDTH_IMAGE_HEIGHT', 'Benutzer Medium Bildhöhe');
define('PROJECT_THUMB_IMAGE_WIDTH', ''.$project.' Thumb Bildbreite');
define('PROJECT_THUMB_IMAGE_HEIGHT', ''.$project.' Thumb Bildhöhe');

define('PROJECT_THUMBNAIL_WIDTH_SHOULD_BE_GREATE_THAN_ZERO', ''.$project.' Thumbnail Breite sollte greator als Null sein');
define('PROJECT_THUMBNAIL_HEIGHT_SHOULD_BE_GREATE_THAN_ZERO', ''.$project.' Thumbnail Größe sollte greator als Null sein');

define('PROJECT_SMALL_WIDTH_SHOULD_BE_GREATER_THAN_ZERO', ''.$project.' Kleine Breite sollte greator als Null sein');
define('PROJECT_SMALL_HEIGHT_SHOULD_BE_GREATER_THAN_ZERO', ''.$project.' Kleine Höhe sollte greator als Null sein');
define('PROJECT_MEDIUM_WIDTH_SHOULD_BE_GREATER_THAN_ZERO', ''.$project.' Medium Breite sollte greator als Null sein');
define('PROJECT_MEDIUM_HEIGHT_SHOULD_BE_GREATER_THAN_ZERO', ''.$project.' Mittelhoch sollte greator als Null sein');
define('PROJECT_LARGE_WIDTH_SHOULD_BE_GREATER_THAN_ZERO', ''.$project.' Große Breite sollte greator als Null sein');
define('PROJECT_LARGE_HEIGHT_SHOULD_BE_GREATER_THAN_ZERO', ''.$project.' Große Höhe sollte greator als Null sein');
define('USER_SMALL_WIDTH_SHOULD_BE_GREATER_THAN_ZERO', 'Benutzer Kleine Breite sollte greator als Null sein');
define('USER_SMALL_HEIGHT_SHOULD_BE_GREATER_THAN_ZERO', 'Benutzer Kleine Höhe sollte greator als Null sein');
define('USER_MEDIUM_WIDTH_SHOULD_BE_GREATER_THAN_ZERO', 'Benutzer Mittlere Breite sollte greator als Null sein');
define('USER_MEDIUM_HEIGHT_SHOULD_BE_GREATER_THAN_ZERO', 'Benutzermittelhoch sollte greator als Null sein');
define('PROJECT_BIG_WIDTH_SHOULD_BE_GREATER_THAN_ZERO', ''.$project.' Big Breite sollte greator als Null sein');
define('PROJECT_BIG_HEIGHT_SHOULD_BE_GREATER_THAN_ZERO', ''.$project.' Big Höhe sollte greator als Null sein');

define('NOT_AVAILBLE', 'Nicht verfügbar');
define('PAGE_TITLE', 'Seitentitel');
define('FUND_GOAL', 'Fund Goal');
define('PROJECT_ADDRESS', ''.$project.' Adresse');
define('PERK_TOTAL', 'Perk Gesamt');
define('REPLY_HAS_BEEN_ADDED_SUCCESSFULLY', 'Antwort wurde erfolgreich hinzugefügt');
define('REASON', 'Grund');
define('PLEASE_ENTER_AMOUNT_GREATER_THAN_ZERO', 'Bitte geben Sie Menge greator als Null');
define('DEDUCT_FUND', 'Abzüglich Fund');
define('AMOUNT_SHOULD_BE_GREATER_THAN_ZERO', 'Betrag sollte greator als Null sein');
define('PREAPPROVAL_ENABLE', 'Preapproval aktivieren');

define('PROJECT_ENTER_MAXIMUM_PROJECT_GOAL_AMOUNT_GREATER_THAN_MINIMUM_PROJECT_GOAL_AMOUNT', 'Bitte geben Sie Maximum '.$project.'Tor Betrag größer als Minimum '.$project.' Tor Betrag');
define('PROJECT_ENTER_MAXIMUM_DAYS_FOR_PROJECT_GREATER_THAN_MINIMUM_DAYS_FOR_PROJECT', 'Bitte geben Sie Maximale Tage '.$project.' Größer als Minimum Tage '.$project.'');

define('PLEASE_ENTER_MAXIMUM_DONATION_AMOUNT_GREATER_THAN_MINIMUM_DONATION_AMOUNT', 'Bitte geben Sie Maximale Spende Betrag größer ist als Mindestspende Betrag');

define('PLEASE_ENTER_MAXIMUM_MAXIMUM_REWARD_AMOUNT_GREATER_THAN_MINIMUM_REWARD_AMOUNT', 'Bitte geben Sie Maximale Belohnung (Prok) Amount greater than Minimum Reward(Perk) Amount');

define('PLEASE_ENTER_MINIMUM_REWARD_AMOUNT_GREATER_THAN_DONATION_AMOUNT', 'Bitte geben Sie mindestens Bonusbetrag größer als Spendenbetrag');
define('PLEASE_ENTER_MAXIMUM_REWARD_AMOUNT_LESS_THAN_DONATION_AMOUNT', 'Bitte geben Sie maximale Belohnung Menge von weniger als Spendenbetrag');
define('ENDING_SOON', 'Bald endend');

define('VERSIONS', 'Versionen');

define('ADD', 'Hinzufügen');

define('BANK_NAME', 'Bank Name');
define('SAMPLE_FILE', 'Beispieldatei');
define('ACCREDENTIAL_DOCUMENT', 'Akkreditierungsdokument');
define('CONTRACT_COPY_UPLOAD_BY', 'Wer wird Vertrag mit Investoren zu verwalten?');
define('OTHER_SETTING_UPDATED_SUCCESSFULLY', 'Weitere Einstellungen erfolgreich aktualisiert.');
define('FOR_ACCREDENTIAL_DOCUMENT', 'Für Akkreditierungsdokument');
define('SUCCESS_THEMES_HAS_BEEN_UPDATED_SUCCESSFULLY', 'Erfolg! Themen wurde erfolgreich aktualisiert.');
define('YOUR_VERSION_UPDATED_SUCCESSFULLY', 'Ihre Version erfolgreich aktualisiert.');
define('YOUR_BACKUP_VERSION_UPDATED_SUCCESSFULLY', 'Ihre Backup-Version erfolgreich aktualisiert ...');
define('THERE_IS_SOME_PROBLEM_WITH_UPDATING_YOUR_BACK_UP_VERSION', 'Es gibt ein Problem mit der Aktualisierung Ihrer Backup-Version.');
define('IF_SMTP_USER_IS_GMAIL_THEN', 'wenn der SMTP-Benutzer ist gmail dann ssl: //smtp.googlemail.com');
define('SEND_EMAIL_TO_USER_ON_NEW_MESSAGE', 'E-Mail an Benutzer auf neue Nachrichten');
define('IMAGE_DIMENSIONS', ' Bildabmessungen');
define('CLEAR_CACHE', 'Cache leeren');
define('TWITTER_USERANME', 'Twitter Useranme');
define('THEMES', 'Themes');
define('COLOR_ORANGE', 'Farbe orange');
define('COLOR_DARKORANGE', 'Farbe darkorange');
define('COLOR_GREY', 'Farbe grau');
define('COLOR_BLACK', 'Farbe schwarz');
define('COLOR_WHITE', 'Farbe weiß');
define('COLOR_LIGHTGREY', 'Farbe hellgrau');
define('COLOR_BLUE', 'Farbe blau');
define('COLOR_DARKBLUE', 'Farbe dunkelblau');
define('COLOR_CCC', 'Farbe ccc');
define('FONT_STYLE', 'Schriftstil');
define('ADMIN_FEES', 'Admin Gebühren');
define('DONORS_PAYPAL_EMAIL', 'Donor`s paypal E-Mail');
define('ARE_YOU_SURE_YOU_WANT_TO_CONFIRM_SELECTED_RECORD', 'Are you sure, you want to confirm selected record(s)?');
define('AVAILABLE_BALANCE', 'Verfügbares Guthaben');
define('BANK_DETA', 'Bankdaten');
define("WHO_SELECT_ACCREDIATION", "Wer Wählen Accrediation");
define("PROJECT_OWNER_ADMIN", "".$project." Inhaber");
define("ALLPROJECTS", "All ".$project." S");
define("OWNER", "Inhaber");
define("CONTRACT_COPY_REQUIRED", "Contract Copy Erforderlich");
define('BEFORE_YOU_CAN_APPROVE_THIS_PROJECT', 'Before you approve this ' . $project . ' uploading contract is required, please follow %s to upload contract.');
define('THIS_LINK', 'dieser Link');
define('ACCREDENTIAL_SETTING','Akkreditierungs Einstellung');
define('CONTRACT_SETTING','Contract Einstellung');
define('ACCREDIATION_SELECT_MANAGE_TITLE','Wie Investor Akkreditierung verwaltet werden?');
define('ACCREDIATION_SELECT_ALLPROJECT','Halten Sie die Akkreditierung für alle Anleger erforderlich ist, bevor Sie investieren.');
define('ACCREDIATION_SELECT_PROJECT_OWNER_ADMIN','Lassen '.$project.' Besitzer entscheiden, wer auf ihre investieren können\". $ Projekt. s');
define('CONTRACT_COPY_UPLOAD_BY_OWNER',''.$project.' Inhaber');
define('CONTRACT_COPY_UPLOAD_BY_ADMIN','Site Admin (Sie oder Ihr Administrator staff)');
define('PROJECT_TITLE_GOES_HERE','Projekt-Titel-goes-hier');
define('IMAGE_UPLOAD_LIMIT', 'Bild Upload-Limit');
define('UPLOAD_LIMIT_SHOULD_BE_GREATER_THAN_ZERO', 'Grenze Upload sollte grßer als Null sein');

/*admin_spam_lang.php*/
define('TOTAL_SPAM_REPORT_ALLOW', 'Insgesamt Spam melden zulassen');
define('REPORT_SPAMER_EXPIRE', 'Bericht Spammer Expire [in Tagen]');
define('TOTAL_REGISTRATION_ALLOW_FROM_SAME_IP', 'Gesamt Registrierung zulassen Von Gleiche IP');
define('REGISTRATION_SPMAER_EXPIRE', 'Registrierung Spammer Expire [in Tagen]');
define('TOTAL_COMMENT_ALLOW_FROM_SAME_IP_IN_ONE_WAY', 'Gesamt Kommentar zulassen Von Gleiche IP In One Day');
define('COMMENT_SPAMER_EXPIRE', 'Kommentar Spammer Expire [in Tagen]');
define('TOTAL_INQUIRY_ALLOW_SAME_IP_IN_ONE_WAY', 'Insgesamt Anfrage zulassen Von Gleiche IP In One Day');
define('INQUIRY_SPAMER_EXPIRE', 'Anfrage Spammer Expire [in Tagen]');
define('ADD_SPAMMER', 'In Spammer');
define('SPAMMER', 'Spammer');
define('SPAM_IP', 'Spam-IP');
define('MAKE_PERMENANT', 'Permanent Make');
define('SPAM_REPORT_HAS_BEEN_DELETED_SUCCESSFULLY', 'Spam Bericht wurde erfolgreich gelöscht.');
define('REPORT_MAKE_SPAM_HAS_BEEN_SUCCESSFULLY', 'Bericht zu machen Spam erfolgreich gewesen.');
define('MAKE_SPAMER', 'machen Spammer');
define('TOTAL_REPORT', 'Gesamtbericht');
define('SPAM_BY', 'Spam');
define('REPORT_BY', 'Bericht von');
define('PERMENANT', 'Permanent');
define('NO_RECORD_AVAILABLE', 'Keine Rekord availble');
define('REPORTED_EXPIRE', 'berichtet Expire');
define('TOTAL_REGISTRATION', 'Gesamt Registrierung');
define('REGISTRATION_SPAM_EXPIRE', 'Registrierung Spam Expire');
define('TOTAL_COMMNET', 'Gesamt Kommentar');
define('COMMENT_SPAM_EXPIRE', 'Kommentar-Spam Expire');
define('TOTAL_INQUIRY', 'insgesamt Anfrage');
define('INQUIRY_SPAM_EXPIRE', 'Anfrage Spam Expire');
define('ARE_YOU_SURE_YOU_WANT_TO_MAKE_PERMENANT_SPAMER_SELECTED_REPORT', 'Are you sure, you want to make Permenant spamer selected report(s)?');
define('SLIDER_IMAGE_REQUIRED', 'Slider Bild erforderlich ist.');
define('SELECT_IMAGE_MORE_THAN_1300_600', 'Bitte wählen Sie die Bild über 1300 * 600.');
