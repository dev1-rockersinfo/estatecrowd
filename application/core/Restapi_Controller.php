<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

// require_once (APPPATH . 'helpers/language_helper' . EXT);
// require_once (APPPATH . 'helpers/custom_helper' . EXT);
require_once (APPPATH . 'helpers/setting_helper' . EXT);


class Restapi_Controller extends ROCKERS_Controller {

    public $log_id = '';
    public $user_id = 0;
    public $data_response_type = "json";
    public $language_code = 'en';
    public $language_id=1;

    public function __construct() {

        parent::__construct();
        $this->user_id = 0;
        $this->data_response_type = 'json';

        
        
        $api_token = "";
        $site_settings = site_setting();

        $time_zone = $site_settings['time_zone'];
        if ($time_zone != '') {
            date_default_timezone_set($time_zone);
        } else {
            date_default_timezone_set('America/New_York');
        }

    }
    
    function checkNullArray($data=array()){
        
        if(!empty($data)){
            foreach ($data as $key => $value) {
                
                if(is_array($value)){
                    $data[$key]=$this->checkNullArray($value);
                } else {
                    if (is_null($value)) {
                         $data[$key] = "";
                    }
                }
            }
        }
        
        return $data;
    }

    function getRequestHeaders() {
        $headers = array();
        foreach ($_SERVER as $key => $value) {
            if (substr($key, 0, 5) <> 'HTTP_') {
                continue;
            }
            $header = str_replace(' ', '-', ucwords(str_replace('_', ' ', strtolower(substr($key, 5)))));
            $headers[$header] = $value;
        }
        return $headers;
    }

    function getallheaders() {
        $headers = '';
        foreach ($_SERVER as $name => $value) {
            if (substr($name, 0, 5) == 'HTTP_') {
                $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }
        return $headers;
    }

    
    function createToken($user_id=0) {

        $username = SecurePostData($this->input->post('username'));
        $password = SecurePostData($this->input->post('password'));
        $client_id = SecurePostData($this->input->post('client_id'));
        $client_secret = SecurePostData($this->input->post('client_secret'));

        if(is_null($username)){
            $success = false;
            $msg = "Username missing.";
            $code = 1001;
            $response_data['success'] = $success;
            $response_data['message'] = (string) $msg;
            $response_data['error_code'] = $code;
            $this->convert_response_data($response_data);
        }

        if(is_null($password)){
            $success = false;
            $msg = "Password missing.";
            $code = 1002;
            $response_data['success'] = $success;
            $response_data['message'] = (string) $msg;
            $response_data['error_code'] = $code;
            $this->convert_response_data($response_data);
        }

        if(is_null($client_id)){
            $success = false;
            $msg = "Client id missing.";
            $code = 1003;
            $response_data['success'] = $success;
            $response_data['message'] = (string) $msg;
            $response_data['error_code'] = $code;
            $this->convert_response_data($response_data);
        }
        if(is_null($client_secret)){
            $success = false;
            $msg = "Client secret missing.";
            $code = 1004;
            $response_data['success'] = $success;
            $response_data['message'] = (string) $msg;
            $response_data['error_code'] = $code;
            $this->convert_response_data($response_data);
        }


        $sfdata = $this->db->select('*')->from('salesforce_user')->get()->row_array();

        if($username!=$sfdata['username']){
            $success = false;
            $msg = "Invalid Username";
            $code = 1005;
            $response_data['success'] = $success;
            $response_data['message'] = (string) $msg;
            $response_data['error_code'] = $code;
            $this->convert_response_data($response_data);
        }

        if($password!=$sfdata['password']){
            $success = false;
            $msg = "Invalid Password.";
            $code = 1006;
            $response_data['success'] = $success;
            $response_data['message'] = (string) $msg;
            $response_data['error_code'] = $code;
            $this->convert_response_data($response_data);
        }

        if($client_id!=$sfdata['client_id']){
            $success = false;
            $msg = "invalid Client id.";
            $code = 1007;
            $response_data['success'] = $success;
            $response_data['message'] = (string) $msg;
            $response_data['error_code'] = $code;
            $this->convert_response_data($response_data);
        }
        if($client_secret!=$sfdata['client_secret']){
            $success = false;
            $msg = "Invalid Client secret.";
            $code = 1008;
            $response_data['success'] = $success;
            $response_data['message'] = (string) $msg;
            $response_data['error_code'] = $code;
            $this->convert_response_data($response_data);
        }


        $sfdata = $this->db->select('*')->from('webservice_authentication')->order_by('id','DESC')->limit(1)->get()->row_array();
        if($sfdata['exp_time']>time()){
            return $sfdata['user_salt'];
        }

        //*** To set token
        $token = $this->setToken($user_id);

        if (isset($token) && !empty($token)){
            
            $insert_user_auth = array(
                'sfid' => $sfdata['sfid'],
                'user_salt' => $token['salt'],
                'ip_address' => $_SERVER['REMOTE_ADDR'],
                'server_detail' => serialize($_SERVER),
                'created_time' => $token['iat'],
                'updated_time' => $token['iat'],
                'exp_time' => $token['iex'],
                'is_active' => 1,
                'is_delete' => 0
            );
            $user_auth_data = $this->db->insert('webservice_authentication', $insert_user_auth);

            return $token['salt'];
        } else {
            return false;
        }
        return true;
    }

    /**
     * setToken()
     * To setup the token.
     * @param type $user_id
     * @param type $plane_id
     * @return type 
     */
    function setToken($user_id = 0) {


        //*** Load JWT Library ***//
        $this->load->library('JWT',$this->data_response_type);

        //*** Generate a session salt.
        $salt = "";
        $salt = md5(uniqid(rand(), true));

        $IAT = time();
        $tomorrow = strtotime('+1 day', strtotime(date('Y-m-d H:i:s')));

        //*** Generate the token informations
        $servers = unserialize(API_SERVER);
        
        
         $server_detail = unserialize(API_SERVER);   // To fetch server detail from config file.
            //*** To check where the request come from the correct server or not??
            
        $domain_name=get_domain_name($_SERVER['SERVER_NAME']);
        
        $issuer_name=$servers[0];
		
		//echo $domain_name; 
		//print_r($server_detail); 	
        
        if (!in_array($domain_name, $server_detail)) {
            
			
			if (!in_array($_SERVER['SERVER_ADDR'], $server_detail)) {
				$success = false;
				$msg = "Server detail not match. Please login and try again";
				$code = 2002;
				$response_data['success'] = $success;
				$response_data['message'] = (string) $msg;
				$response_data['error_code'] = $code;
				$this->convert_response_data($response_data);
			} else {
			
					$issuer_name=$_SERVER['SERVER_ADDR'];
			}
        } else {
            $issuer_name=$domain_name;
        }

        $generate_token = array(
            'iss' => $issuer_name,
            'iat' => $IAT,
            'iex' => $tomorrow,            
            'exp' => $IAT + (API_TOKEN_EXPIRATION * (60 * 60)),
            'user_id' => $user_id,
            'user_salt' => $salt,
            'lang' => $this->language_code,
        );

        $token = $this->jwt->encode($generate_token, API_TOKEN_KEY, API_TOKEN_ALGO);
        //$decoded_token = JWT::decode($token, API_TOKEN_KEY, API_TOKEN_ALGO);
        return array('token' => $token, 'salt' => $salt, 'iat' => $IAT,'iex'=>$tomorrow);
    }

    function tokenValidation($token = NULL) {

        //*** Load JWT Library ***//
        $this->load->library('JWT',$this->data_response_type);
        
        if (isset($token) && !empty($token)) {

            $sfid = "";
            $user_salt = "";
            $expiration_time = "";
            $iat = "";

            $server_detail = unserialize(API_SERVER);   // To fetch server detail from config file.
            //*** To check where the request come from the correct server or not??
            
            $domain_name=get_domain_name($_SERVER['SERVER_NAME']);           


            if (!in_array($domain_name, $server_detail)) {			
    			if (!in_array($_SERVER['SERVER_ADDR'], $server_detail)) {
    				$success = false;
    				$msg = "Server detail not match. Please login and try again";
    				$code = 2003;
    				$response_data['success'] = $success;
    				$response_data['message'] = (string) $msg;
    				$response_data['error_code'] = $code;
    				$this->convert_response_data($response_data);
    			} 
            }

            $sfdata = $this->db->select('*')->from('webservice_authentication')->where('user_salt',$token)->limit(1)->get()->row_array();

            if(is_null($sfdata)){
                $success = false;
                $msg = "Invalid Token.";
                $code = 2004;
                $response_data['success'] = $success;
                $response_data['message'] = (string) $msg;
                $response_data['error_code'] = $code;
                $this->convert_response_data($response_data);
            }

            if($sfdata['exp_time']<=time()){
                $success = false;
                $msg = "Token Expire.";
                $code = 2005;
                $response_data['success'] = $success;
                $response_data['message'] = (string) $msg;
                $response_data['error_code'] = $code;
                $this->convert_response_data($response_data);
            }

            return true;
            
        } else {
            $success = false;
            $msg = "Please login and try again";
            $code = 2001;
            $response_data['success'] = $success;
            $response_data['message'] = (string) $msg;
            $response_data['error_code'] = $code;
            $this->convert_response_data($response_data);
        }
    }

    function convert_response_data($dataresponse = array(), $datatype = "json") {
        $tmpdataresponse = $dataresponse;
        // pr($dataresponse); die;

        if ($this->data_response_type == "json") {
            $dataresponse = json_encode($dataresponse);

            if ($dataresponse == "") {
                $dataresponse = $this->_json_encode($tmpdataresponse);
            }
        }

        $api_status = -1;
        if ($dataresponse != "") {
            $api_status = 1;
        }


        //====api log update status======

        $info = '';
        // $this->log_updatecall($this->log_id, $api_status, $dataresponse, $info);

        //====api log update status======

        echo $dataresponse;
        die;
    }

// alternative json_encode
    function _json_encode($val) {
        if (is_string($val))
            return '"' . addslashes($val) . '"';
        if (is_numeric($val))
            return $val;
        if ($val === null)
            return 'null';
        if ($val === true)
            return 'true';
        if ($val === false)
            return 'false';

        $assoc = false;
        $i = 0;
        foreach ($val as $k => $v) {
            if ($k !== $i++) {
                $assoc = true;
                break;
            }
        }
        $res = array();
        foreach ($val as $k => $v) {
            $v = $this->_json_encode($v);
            if ($assoc) {
                $k = '"' . addslashes($k) . '"';
                $v = $k . ':' . $v;
            }
            $res[] = $v;
        }
        $res = implode(',', $res);
        return ($assoc) ? '{' . $res . '}' : '[' . $res . ']';
    }
    
   function encrypt_decrypt($action, $string) {
       $output = false;
       
        $encrypt_method = "AES-256-CBC";
        $secret_key = API_SECRET_KEY;
        $secret_iv = API_SECRET_IV;

        // hash
        $key = hash('sha256', $secret_key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        //$iv = $secret_iv;

        if ($action == 'encrypt') {

          $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);

          //$output = base64_encode($output);
        } else if ($action == 'decrypt') {

          //$output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
          $output = openssl_decrypt($string, $encrypt_method, $key, 0, $iv);
        }
        
        return $output; 
  }
  
  ///=============webservice image upload code====

    function saveImage($base64img) {

        //$base64img = str_replace('data:image/jpeg;base64,', '', $base64img);

        $p = strpos($base64img, ','); 
        if($p>=0){
            
            $base64img = substr($base64img, $p + 1);
        }
        
        $base64img = str_replace(' ', '+', $base64img);
        
        $data = base64_decode($base64img);

        $random_code = md5(mt_rand(1, 123123123));


        $file_name = 'kuser-' . $random_code . '.jpeg';
        $filePath = base_path().'upload/orig/' . $file_name;
        file_put_contents($filePath, $data);
        
               
        return $file_name;
    }

   function saveurlImage($image_url, $path_type='', $image_type='') {

        $data = file_get_contents($image_url);

        $random_code = md5(mt_rand(1, 123123123));


        $file_name = 'user-' . $random_code . '.jpg';
        $filePath = UPLOAD_TMP_DIR . $file_name;
        file_put_contents($filePath, $data);


        //bunch type
        $squareimg = Configure::read($image_type);

        $this->requestAction(
                array('plugin' => 'imageopt', 'controller' => 'imageoptapp', 'action' => 'imageProcessCompleted'), array("file_name" => $file_name, "filePath" => $filePath, "squareimg" => $squareimg, "path_type" => $path_type)
        );

        return $file_name;
    }
  

}

// END Restapi class
    /* End of file Restapi.php */
    /* Location: ./application/controller/api/Restapi.php */