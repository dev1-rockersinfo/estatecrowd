<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . 'helpers/language_helper' . EXT);
require_once(APPPATH . 'helpers/custom_helper' . EXT);
require_once(APPPATH . 'helpers/setting_helper' . EXT);

class ROCKERS_Controller extends MX_Controller
{
    public function __construct()
    {

        // get the CI superobject

        $CI = &get_instance();
        parent::__construct();
		$lang ='';
		    if(!empty($_GET['lang']))
			{
				// Turn en-gb into en
				$lang = substr($_GET['lang'], 0, 2);
				
				$_SESSION['lang_code'] = $lang;
			}


			// Lang has already been set and is stored in a session
			elseif( !empty($_SESSION['lang_code']) )
			{
				$lang = $_SESSION['lang_code'];
			}

			// Lang has is picked by a user.
			// Set it to a session variable so we are only checking one place most of the time
			elseif( !empty($_COOKIE['lang_code']) )
			{
				$lang = $_SESSION['lang_code'] = $_COOKIE['lang_code'];
			}
			
        /***** language *****/
        if (!empty($_GET['lang'])) {
            setting_deletecache('learn_more_category');
            setting_deletecache('learn_more_category_right_side');
            setting_deletecache('pages_menu');
            setting_deletecache('pages_right_menu');
            setting_deletecache('category_array');
            setting_deletecache('allcategory');
            setting_deletecache('parent');
            setting_deletecache('child');
            setting_deletecache('catname');
        }

        $supported_language = get_supported_lang();
        $default_language = get_current_language();
        $default_folder = $supported_language[$default_language];
        $site_settings = site_setting();


        $time_zone = $site_settings['time_zone'];
        if ($time_zone != '') {
            date_default_timezone_set($time_zone);
        } else {
            date_default_timezone_set('America/New_York');
        }


         if (empty($lang) or !in_array($lang, array_keys($supported_language))) {
            $lang = $default_language;
			$_SESSION['lang_code']=$lang;
        }

        // //////======check for $lang set====

        if (isset($lang)) {
            if ($lang != '') {
                $change_folder = $supported_language[$lang];
            }
        }

        // //////======check for language folder exists====

       if (file_exists(base_path() . 'system/language/' . $change_folder)) {
            if (file_exists(base_path() . 'application/language/' . $change_folder)) {
            } else {
                $change_folder = $default_folder;
                $_SESSION['lang_code'] = $default_language;
                $this->config->set_item('language', $change_folder);
            }
        } else {
            $change_folder = $default_folder;
            $_SESSION['lang_code'] = $default_language;
            $this->config->set_item('language', $change_folder);
        }

        $_SESSION['lang_folder'] = $change_folder;

        // ///===lock front side language=======

        $this->config->set_item('language', $change_folder);
        //echo $change_folder;die;

        // ///===load the language files=======

        $this->lang->load('realestate', $_SESSION['lang_folder']);
        
        // /***** language *****/
    }

    public static function &get_instance()
    {
        return self::$instance;
    }

    /*
        Function name :mailalerts()
        Parameter :$type ,comment ,project ,user ,mail_title
        Return : none
        Use : type -->its used for switch case to fire email for pariticular process
              comments -> data for comments process.
              project -> data for projects.
              mail_title->title of mail
              user -> data for users.
              we are firing normal email from here.

        */
    function mailalerts($type, $comment, $project, $user, $mail_title)
    {
       
        $site_setting = site_setting();
        $site_name = $site_setting['site_name'];

        $taxonomy_setting = taxonomy_setting();
        $project_name = $taxonomy_setting['project_name'];
        $comments_plural = $taxonomy_setting['comments_plural'];
        $comments_past = $taxonomy_setting['comments_past'];
        $taxo_comment = $taxonomy_setting['comments'];
        switch ($type) {
            case 'comment_alert':
                $break = "<br/>";
                $user_name = $comment['user_name'];
                $comment_user_name = $comment['comment_user_name'];
                $property_address = $comment['property_address'];
                $comment_data = $comment['comment'];
                $comment_name_anchor = $comment['comment_name_anchor'];
                $comment_profile_link = $comment['comment_profile_link'];
                $property_page_link = $comment['property_page_link'];
                $email = $comment['email'];
                $email_template = $this->db->query("select * from `email_template` where task='$mail_title'");
                $email_temp = $email_template->row();
                $email_message = $email_temp->message;
                $email_subject = $email_temp->subject;
                $email_subject = str_replace('{company_name}', $property_address, $email_subject);
                $email_subject = str_replace('{project_name}', $project_name, $email_subject);
                $email_subject = str_replace('{comment}', $taxo_comment, $email_subject);
                $email_address_from = $email_temp->from_address;
                $email_address_reply = $email_temp->reply_address;
                $email_message = str_replace('{user_name}', $user_name, $email_message);
                $email_message = str_replace('{comment}', $taxo_comment, $email_message);
                $email_message = str_replace('{project_name}', $project_name, $email_message);
                $email_message = str_replace('{comment_user_name}', $comment_name_anchor, $email_message);
                $email_message = str_replace('{comment_user_profile_link}', $comment_profile_link, $email_message);
                $email_message = str_replace('{company_name}', $property_address, $email_message);
                $email_message = str_replace('{comment_plural}', $comments_plural, $email_message);
                $email_message = str_replace('{comment_past}', $comments_past, $email_message);
                $email_message = str_replace('{comments}', $comment_data, $email_message);
                $email_message = str_replace('{site_name}', $site_name, $email_message);
                $email_message = str_replace('{equity_page_link}', $property_page_link, $email_message);
                $str = $email_message;

                email_send($email_address_from, $email_address_reply, $email, $email_subject, $str);
                break;

            case 'user_follower':
                $user_name = $user['user_name'];
                $follow_user_name = $user['follow_user_name'];
                $follower_user_link = $user['follower_user_link'];
                $followr_user_name_without_link = $user['followr_user_name_without_link'];
                $break = "<br/>";
                $email = $user['email'];
                $email_template = $this->db->query("select * from `email_template` where task='$mail_title'");
                $email_temp = $email_template->row();
                $email_message = $email_temp->message;
                $email_subject = $email_temp->subject;
                $email_subject = str_replace('{site_name}', $site_name, $email_subject);
                $email_subject = str_replace('{following_user_name}', $followr_user_name_without_link, $email_subject);
                $email_address_from = $email_temp->from_address;
                $email_address_reply = $email_temp->reply_address;
                $email_message = str_replace('{user_name}', $user_name, $email_message);
                $email_message = str_replace('{following_user_name}', $follow_user_name, $email_message);
                $email_message = str_replace('{user_profile_link}', $follower_user_link, $email_message);
                $email_message = str_replace('{site_name}', $site_name, $email_message);
                $str = $email_message;
               
                email_send($email_address_from, $email_address_reply, $email, $email_subject, $str);
                break;

            case 'user_message':
                $break = "<br/>";
                $user_name = $user['user_name'];
                $message_user_name = $user['message_user_name'];
                $dateadded = $user['dateadded'];
                $subject = $user['subject'];
                $message = $user['message'];
                $subject_subject = strip_tags($subject);
                $view_message_link = $user['view_message_link'];
                $email = $user['email'];
                $email_template = $this->db->query("select * from `email_template` where task='$mail_title'");
                $email_temp = $email_template->row();
                $email_message = $email_temp->message;
                $email_subject = $email_temp->subject;
                $email_subject = str_replace('{subject}', $subject_subject, $email_subject);
                $email_address_from = $email_temp->from_address;
                $email_address_reply = $email_temp->reply_address;
               
                $email_message = str_replace('{user_name}', $user_name, $email_message);
                $email_message = str_replace('{message_user_name}', $message_user_name, $email_message);
                $email_message = str_replace('{dateadded}', $dateadded, $email_message);
                $email_message = str_replace('{subject}', $subject, $email_message);
                $email_message = str_replace('{message}', $message, $email_message);
                $email_message = str_replace('{view_message_link}', $view_message_link, $email_message);
                $email_message = str_replace('{site_name}', $site_name, $email_message);
                $str = $email_message;
                email_send($email_address_from, $email_address_reply, $email, $email_subject, $str);
                break;


            case 'comment_admin_alert':
                $break = "<br/>";
                $user_name = $comment['user_name'];
                $comment_user_name = $comment['comment_user_name'];
                $property_address = $comment['property_address'];
                $comment_data = $comment['comment'];
                $comment_profile_link = $comment['comment_profile_link'];

                // $email=$comment['email'];

                $email_template = $this->db->query("select * from `email_template` where task='$mail_title'");
                $email_temp = $email_template->row();
                $email_message = $email_temp->message;
                $email_subject = $email_temp->subject;
                $email_subject = str_replace('{company_name}', $property_address, $email_subject);
                $email_subject = str_replace('{project_name}', $project_name, $email_subject);
                $email_subject = str_replace('{comment}', $taxo_comment, $email_subject);
                $email_address_from = $email_temp->from_address;
                $email_address_reply = $email_temp->reply_address;
                $email = $email_address_from;
                $email_message = str_replace('{break}', $break, $email_message);
                $email_message = str_replace('{comment_user_name}', $comment_user_name, $email_message);
                $email_message = str_replace('{company_name}', $property_address, $email_message);
                $email_message = str_replace('{project_name}', $project_name, $email_message);
                $email_message = str_replace('{comment}', $taxo_comment, $email_message);
                $email_message = str_replace('{comments}', $comment_data, $email_message);
                $email_message = str_replace('{comment_user_profile_link}', $comment_profile_link, $email_message);
                $email_message = str_replace('{site_name}', $site_name, $email_message);
                $str = $email_message;
                email_send($email_address_from, $email_address_reply, $email, $email_subject, $str);
                break;
            /*	case 'project_live':
            break;
            case 'project_active':
            break;
            case 'project_success':
            break;			*/
        }
    }

    /*
    Function name :mailalerts()
    Parameter :$type ,comment ,project ,user ,mail_title
    Return : none
    Use : type -->its used for switch case to fire email for pariticular process
          comments -> data for comments process.
          project -> data for projects.
          mail_title->title of mail
          user -> data for users.
          we are firing normal email from here.

    */
    function EquityMailAlerts($type, $comment, $equity, $user, $mail_title)
    {
        $site_setting = site_setting();
        $site_name = $site_setting['site_name'];
        $taxonomy_setting = taxonomy_setting();
        $project_name = $taxonomy_setting['project_name'];
        $project_url = $taxonomy_setting['project_url'];
        switch ($type) {


            case 'equity_draft':
                $break = "<br/>";
                $user_name = $equity['user_name'];
                $equity_name = $equity['equity_name'];
                $complete_my_campaign = $equity['complete_my_campaign'];
                $email = $equity['email'];
                $email_template = $this->db->query("select * from `email_template` where task='$mail_title'");
                $email_temp = $email_template->row();
                $email_message = $email_temp->message;
                $email_subject = $email_temp->subject;
                $email_subject = str_replace('{project_name}', $project_name, $email_subject);
                $email_subject = str_replace('{site_name}', $site_name, $email_subject);

                $email_address_from = $email_temp->from_address;
                $email_address_reply = $email_temp->reply_address;
                $email_message = str_replace('{user_name}', $user_name, $email_message);
                $email_message = str_replace('{project_name}', $project_name, $email_message);
                $email_message = str_replace('{complete_my_campaign}', $complete_my_campaign, $email_message);
                $email_message = str_replace('{site_name}', $site_name, $email_message);

                $str = $email_message;

                email_send($email_address_from, $email_address_reply, $email, $email_subject, $str);
                break;
            case 'equity_submit_for_admin_approval':

                $break = "<br/>";
                $user_name = $equity['user_name'];
                $equity_name = $equity['equity_name'];
                $equity_page_link = $equity['equity_page_link'];
                $user_profile_link = $equity['user_profile_link'];

                $equity_list_link = '<a href ="' . site_url('admin/equity/list_equity') . '">' . CLICK_HERE . '</a>';
                $equity_page_link_name = '<a href ="' . site_url($project_url.'/' . $equity_name) . '">' . $equity_name . '</a>';
                $company_category = $equity['company_category'];
                $company_industry = $equity['company_industry'];
                $campaign_duration_deadline = GetDaysLeft($equity['end_date']);
                $deal_type = $equity['deal_type_name'];
                $campaign_goal = $equity['equity_currency_symbol'] . ' ' . $equity['goal'];
                $equity_detail_admin_link = '<a href ="' . site_url('admin/campaign/campaign_detail/' . $equity['property_url']) . '">' . MORE . '</a>';
                $equity_user_messasing_link_in_admin = '<a href ="' . site_url('admin/message/view_message/' . $equity['equity_id'] . '/' . $equity['user_id']) . '">' . MESSAGE . '</a>';


                $email = $equity['email'];
                $email_template = $this->db->query("select * from `email_template` where task='$mail_title'");
                $email_temp = $email_template->row();
                $email_message = $email_temp->message;
                $email_subject = $email_temp->subject;
                $email_subject = str_replace('{project_name}', $project_name, $email_subject);
                $email_subject = str_replace('{equity_owner_name}', $user_name, $email_subject);
                $email_address_from = $email_temp->from_address;
                $email_address_reply = $email_temp->reply_address;
                $email_message = str_replace('{break}', $break, $email_message);
                $email_message = str_replace('{company_name}', $equity_name, $email_message);
                $email_message = str_replace('{equity_owner_name}', $user_profile_link, $email_message);
                $email_message = str_replace('{equity_page_link}', $equity_page_link_name, $email_message);
                $email_message = str_replace('{project_name}', $project_name, $email_message);
                $email_message = str_replace('{site_name}', $site_name, $email_message);

                $email_message = str_replace('{equity_list_link}', $equity_list_link, $email_message);
                $email_message = str_replace('{company_category}', $company_category, $email_message);
                $email_message = str_replace('{company_industry}', $company_industry, $email_message);
                $email_message = str_replace('{campaign_duration_deadline}', $campaign_duration_deadline, $email_message);
                $email_message = str_replace('{deal_type}', $deal_type, $email_message);
                $email_message = str_replace('{campaign_goal}', $campaign_goal, $email_message);
                $email_message = str_replace('{equity_detail_admin_link}', $equity_detail_admin_link, $email_message);
                $email_message = str_replace('{equity_user_messasing_link_in_admin}', $equity_user_messasing_link_in_admin, $email_message);

                $str = $email_message;
                email_send($email_address_from, $email_address_reply, $email_address_from, $email_subject, $str);
                break;


            case 'equity_success_submit':
                $break = "<br/>";
                $user_name = $equity['user_name'];
                $equity_name = $equity['equity_name'];
                $company_name_link = $equity['company_name_link'];

                $email = $equity['email'];
                $email_template = $this->db->query("select * from `email_template` where task='$mail_title'");
                $email_temp = $email_template->row();
                $email_message = $email_temp->message;
                $email_subject = $email_temp->subject;
                $email_subject = str_replace('{project_name}', $project_name, $email_subject);
                $email_subject = str_replace('{site_name}', $site_name, $email_subject);
                $email_address_from = $email_temp->from_address;
                $email_address_reply = $email_temp->reply_address;
                $email_message = str_replace('{project_name}', $project_name, $email_message);
                $email_message = str_replace('{user_name}', $user_name, $email_message);
                $email_message = str_replace('{company_name}', $company_name_link, $email_message);
                $email_message = str_replace('{site_name}', $site_name, $email_message);
                $str = $email_message;

                email_send($email_address_from, $email_address_reply, $email, $email_subject, $str);
                break;
            /*	case 'project_live':
            break;
            case 'project_active':
            break;
            case 'project_success':
            break;			*/
        }
    }

    /*
        Function name :adminalerts()
        Parameter :$type ,comment ,project ,user ,mail_title
        Return : none
        Use : prj -->project data which will used for email
              trans -> transaction data which will used for email
              user_detail -> data for users.
              post_email ->if user not login then this email is used  for sending email to donor
              we are firing payment related email from here.

        */
    function adminalerts($prj, $trans, $user_detail, $post_email = '')
    {

        $site_setting = site_setting();
        $site_name = $site_setting['site_name'];
        $project_name = 'campaign';
        $project_url = 'properties';
        $funds = 'Investment';
        $funds_past = 'Invested';
        $funds_plural = 'Investments';
        $property_address = $prj['property_address'];
        $property_url = $prj['property_url'];
        $campaign_id = $prj['campaign_id'];
        $property_id = $prj['property_id'];
        $username = $prj['user_name'];
        $project_owner_email = $prj['email'];

        $project_page_link = site_url($project_url.'/' . $property_url);
        $donote_amount = set_currency($trans->amount);
        $donate_user_date = UserData($trans->user_id);
        $donor_profile_link = site_url('user/' . $donate_user_date[0]['profile_slug']);
        $donar_email = $user_detail['email'];
        $min_investment_amount = set_currency($prj['min_investment_amount']);
        $raised_amount = set_currency($prj['amount_get']);
        $amountdata = array('amount_get' => $prj['amount_get'], 'min_investment_amount' => $prj['min_investment_amount']);
        $percentage_amount = GetCampaignPercentage($amountdata);
        $percentage = floor($percentage_amount);
        $investment_close_date = $prj['investment_close_date'];
        $days_left = GetDaysLeft($investment_close_date);

        if ($this->session->userdata('user_id') != '') {
            $donor_name = $user_detail['user_name'] . ' ' . $user_detail['last_name'];
            $donor_name_anchor = '<a href="' . $donor_profile_link . '">' . $donor_name . '</a>';
        } else {
            $donar_email = $trans->donor_email;
            $donor_name = $donar_email;
            $donor_name_anchor = $donor_name;
        }

        if ($trans->comment != '') $donor_comment = $trans->comment;

        else $donor_comment = 'No Comment';
        // if ($trans->shipping != '') $donor_shipping = $trans->shipping;
        // else $donor_shipping = 'No Shipping';
        $project_title_anchor = '<a href="' . $project_page_link . '">' . $property_address . '</a>';

        // /////////============admin alert==========

        $email_template = $this->db->query("select * from `email_template` where task='New Fund Admin Notification'");
        $email_temp = $email_template->row();
        $email_message = $email_temp->message;
        $email_subject = $email_temp->subject;
        $email_subject = str_replace('{project_name}', $project_name, $email_subject);
        $email_subject = str_replace('{property_address}', $property_address, $email_subject);
        $email_subject = str_replace('{funds}', $funds, $email_subject);
        $email_address_from = $email_temp->from_address;
        $email_address_reply = $email_temp->reply_address;
        $email_to = $email_address_from;
        $email_message = str_replace('{break}', '<br/>', $email_message);
        $email_message = str_replace('{user_name}', $username, $email_message);
        $email_message = str_replace('{property_address}', $property_address, $email_message);
        $email_message = str_replace('{donor_name}', $donor_name_anchor, $email_message);
        $email_message = str_replace('{funds_past}', $funds_past, $email_message);
        $email_message = str_replace('{donate_amount}', $donote_amount, $email_message);
        $email_message = str_replace('{project_page_link}', $project_page_link, $email_message);
        $str = $email_message;
        email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);

        // /////=================owner alert===================
        $email_template = $this->db->query("select * from `email_template` where task='New Fund Owner Notification'");
        $email_temp = $email_template->row();
        $email_message = $email_temp->message;
        $email_subject = $email_temp->subject;
        $email_subject = str_replace('{project_name}', $project_name, $email_subject);
        $email_subject = str_replace('{property_address}', $property_address, $email_subject);
        $email_subject = str_replace('{funds}', $funds, $email_subject);
        $email_address_from = $email_temp->from_address;
        $email_address_reply = $email_temp->reply_address;
        $email_to = $project_owner_email;
        $email_message = str_replace('{break}', '<br/>', $email_message);
        $email_message = str_replace('{user_name}', $username, $email_message);
        $email_message = str_replace('{project_name}', $project_name, $email_message);
        $email_message = str_replace('{property_address}', $property_address, $email_message);
        $email_message = str_replace('{donor_name}', $donor_name_anchor, $email_message);
        $email_message = str_replace('{donate_amount}', $donote_amount, $email_message);
        $email_message = str_replace('{project_page_link}', $project_page_link, $email_message);
        $email_message = str_replace('{funds_plural}', $funds_plural, $email_message);
        $email_message = str_replace('{funds_past}', $funds_past, $email_message);
        $email_message = str_replace('{raised_amount}', $raised_amount, $email_message);
        $email_message = str_replace('{percentage}', $percentage, $email_message);
        $email_message = str_replace('{days_left}', $days_left, $email_message);
        $email_message = str_replace('{goal}', $min_investment_amount, $email_message);
        $email_message = str_replace('{site_name}', $site_name, $email_message);
        $str = $email_message;
        email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);

        // ///////////////==============donor alert================================


        $email_template = $this->db->query("select * from `email_template` where task='New Fund Donor Notification'");
        $email_temp = $email_template->row();
        $email_message = $email_temp->message;
        $email_subject = $email_temp->subject;
        $email_subject = str_replace('{property_address}', $property_address, $email_subject);
        $email_subject = str_replace('{funds}', $funds, $email_subject);
        $email_address_from = $email_temp->from_address;
        $email_address_reply = $email_temp->reply_address;
        $email_to = $donar_email;
        $email_message = str_replace('{break}', '<br/>', $email_message);
        $email_message = str_replace('{user_name}', $username, $email_message);
        $email_message = str_replace('{property_address}', $project_title_anchor, $email_message);
        $email_message = str_replace('{donor_name}', $donor_name_anchor, $email_message);
        $email_message = str_replace('{donate_amount}', $donote_amount, $email_message);
        $email_message = str_replace('{funds}', $funds, $email_message);
        $email_message = str_replace('{project_page_link}', $project_page_link, $email_message);
        $email_message = str_replace('{site_name}', $site_name, $email_message);
        $str = $email_message;
        email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);

        // ///////////////============== Project you back notification================================

        $project_you_back = $this->db->query("SELECT un.add_fund,un.user_id,u.email,u.user_name,u.last_name FROM `user_notification` un
					inner join user u on u.user_id=un.user_id
					inner join transaction tr on tr.user_id=u.user_id
					
					where  un.add_fund=1 and tr.campaign_id=" . $campaign_id . "
					group by u.user_id");

        $all_back_user = $project_you_back->result_array();

        if (is_array($all_back_user)) {
            foreach ($all_back_user as $back_user) {
                $add_fund = $back_user['add_fund'];
                $donated_user_name = $back_user['user_name'];
                $donated_last_name = $back_user['last_name'];
                $donated_email = $back_user['email'];

                $email_template = $this->db->query("select * from `email_template` where task='New fund added on your backed project'");
                $email_temp = $email_template->row();
                $email_message = $email_temp->message;
                $email_subject = $email_temp->subject;
                $email_subject = str_replace('{project_name}', $project_name, $email_subject);
                $email_subject = str_replace('{property_address}', $property_address, $email_subject);
                $email_subject = str_replace('{funds}', $funds, $email_subject);
                $email_subject = str_replace('{funds_past}', $funds_past, $email_subject);

                $email_address_from = $email_temp->from_address;
                $email_address_reply = $email_temp->reply_address;
                $email_to = $donated_email;
                $email_message = str_replace('{break}', '<br/>', $email_message);
                $email_message = str_replace('{user_name}', $donated_user_name, $email_message);
                $email_message = str_replace('{property_address}', $property_address, $email_message);
                $email_message = str_replace('{funds_plural}', $funds_plural, $email_message);
                $email_message = str_replace('{funds_past}', $funds_past, $email_message);
                $email_message = str_replace('{funds}', $funds, $email_message);
                $email_message = str_replace('{donor_name}', $donor_name_anchor, $email_message);
                $email_message = str_replace('{donate_amount}', $donote_amount, $email_message);
                $email_message = str_replace('{raised_amount}', $raised_amount, $email_message);
                $email_message = str_replace('{project_name}', $project_name, $email_message);
                $email_message = str_replace('{percentage}', $percentage, $email_message);
                $email_message = str_replace('{days_left}', $days_left, $email_message);
                $email_message = str_replace('{goal}', $min_investment_amount, $email_message);

                $email_message = str_replace('{project_page_link}', $project_page_link, $email_message);
                $email_message = str_replace('{site_name}', $site_name, $email_message);

                $str = $email_message;


                email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);

                # code...
            }
        }

        // ///////////////============== Project you follow notification================================


        $project_you_follow = $this->db->query("SELECT un.new_pledge_alert,un.user_id,u.email,u.user_name,u.last_name FROM `user_notification` un
						inner join user u on u.user_id=un.user_id
						inner join property_follower pf on pf.property_follow_user_id=u.user_id
						where  un.new_pledge_alert=1 and pf.property_id=" . $property_id . "
						group by u.user_id");

        $all_project_follow_user = $project_you_follow->result_array();

        if (is_array($all_project_follow_user)) {
            foreach ($all_project_follow_user as $follower_user) {

                $follower_user_name = $follower_user['user_name'];
                $follower_last_name = $follower_user['last_name'];
                $follower_email = $follower_user['email'];

                $email_template = $this->db->query("select * from `email_template` where task='New Pledge on campaign you followed'");
                $email_temp = $email_template->row();
                $email_message = $email_temp->message;
                $email_subject = $email_temp->subject;
                $email_subject = str_replace('{project_name}', $project_name, $email_subject);
                $email_subject = str_replace('{property_address}', $property_address, $email_subject);
                $email_address_from = $email_temp->from_address;
                $email_address_reply = $email_temp->reply_address;
                $email_to = $follower_email;
                $email_message = str_replace('{break}', '<br/>', $email_message);
                $email_message = str_replace('{user_name}', $follower_user_name, $email_message);
                $email_message = str_replace('{project_name}', $project_name, $email_message);
                $email_message = str_replace('{property_address}', $project_title_anchor, $email_message);
                $email_message = str_replace('{donor_name}', $donor_name_anchor, $email_message);
                $email_message = str_replace('{funds_plural}', $funds_plural, $email_message);
                $email_message = str_replace('{funds_past}', $funds_past, $email_message);
                $email_message = str_replace('{donate_amount}', $donote_amount, $email_message);
                $email_message = str_replace('{raised_amount}', $raised_amount, $email_message);
                $email_message = str_replace('{percentage}', $percentage, $email_message);
                $email_message = str_replace('{days_left}', $days_left, $email_message);
                $email_message = str_replace('{goal}', $min_investment_amount, $email_message);

                $email_message = str_replace('{project_page_link}', $project_page_link, $email_message);
                $email_message = str_replace('{site_name}', $site_name, $email_message);
                $str = $email_message;


                email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);

                # code...
            }
        }

        // ///////============SUCCESS Email ==============

    }

    //obsolate function, nor more use in script.
    function socail_share($prj, $login_user)
    {
        return 0;
    }
/*
    Function name :update_project_perk()
    Parameter :$project_id ,perk_id ,project_owner_amount 
    Return : none
    Use : Update the project table after donation process with perk_id and raised donation amount.
           
    */
    function update_project_perk($campaign_id = 0, $units = 0, $campaign_owner_amount = 0)
    {

        
        $don_prj = GetOnePropertyCampaign($campaign_id);
        if ($don_prj['amount_get'] != "")
        {
            $amt = $don_prj['amount_get'];

        }
        else
        {
            $amt = 0;
        }

          if ($don_prj['campaign_units_get'] != "")
        {
          
            $campaign_units_get = $don_prj['campaign_units_get'];

        }
        else
        {
            $campaign_units_get = 0;
        }

        $data_don = array(
            'amount_get' => $amt + $campaign_owner_amount,
            'campaign_units_get' =>$campaign_units_get + $units,
        );
        $this->db->where('campaign_id', $campaign_id);
        $this->db->update('campaign', $data_don);


        $property_url = '';
        if (isset($don_prj['property_url'])) $property_url = $don_prj['property_url'];
        property_deletecache($campaign_id, $property_url);
       }


    /*
    Function name :active_gateway()
    Parameter : gateway_id,joinarr,limit,order
    Return : array of all gateway detail
    Use : Fetch all gateway particular Criteria
    */
    function active_gateway($gateway_id = 0, $joinarr = array(
            'user'
        ), $limit = 10, $order = array(
            'id' => 'desc'
        ), $group_by = '')
    {
        $selectfields = 'payments_gateways.id,payments_gateways.name,payments_gateways.status,payments_gateways.image,payments_gateways.function_name';
        if (in_array('gateways_details', $joinarr)) {
            $selectfields .= ',gateways_details.name,gateways_details.value';
        }

        $this->db->select($selectfields);
        $this->db->from('payments_gateways');
        if (in_array('gateways_details', $joinarr)) {
            $this->db->join('gateways_details', 'gateways_details.payment_gateway_id=payments_gateways.id');
        }

        $this->db->where('payments_gateways.status', 'Active');
        if ($gateway_id > 0) $this->db->where('payments_gateways.id', $gateway_id);
        if (is_array($order)) {
            foreach ($order as $key => $val) {
                $this->db->order_by($key, $val);
            }
        }

        if ($group_by != '') {
            $this->db->group_by($group_by);
        }

        $query = $this->db->get();

        // echo "<br /><br /><br />";
        // echo $this->db->last_query();
        // die;
        // echo "<br /><br /><br />";

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

     /**
     * @return array of supported formats
     * document type restrictions.
     */
    protected function getSupportedFileTypes(){
        return $file_type_array = array(
            'zip'=>'application/zip', 
            'pdf'=>'application/pdf', 
            'ods'=>'application/vnd.oasis.opendocument.spreadsheet',
            'odp'=>'application/vnd.oasis.opendocument.presentation',
            'doc'=>'application/msword',  
            'rar'=>'application/x-rar',
            'tar.gz'=>'application/x-gzip',
            'xls'=>'application/vnd.ms-excel', 
            'ppt'=>'application/vnd.ms-powerpoint',
            'rtf'=>'text/rtf', 
            'odt'=>'application/vnd.oasis.opendocument.text', 
            'vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'vnd.openxmlformats-officedocument.wordprocessingml.document',
            'octet-stream', 
            'excel', 
            'msexcel', 
            'powerpoint');
    }

}

// END MY_Controller class

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */
