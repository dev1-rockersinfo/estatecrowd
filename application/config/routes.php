<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*

| -------------------------------------------------------------------------

| URI ROUTING

| -------------------------------------------------------------------------

| This file lets you re-map URI requests to specific controller functions.

|

| Typically there is a one-to-one relationship between a URL string

| and its corresponding controller class/method. The segments in a

| URL normally follow this pattern:

|

|	example.com/class/method/id/

|

| In some instances, however, you may want to remap this relationship

| so that a different class/function is called than the one

| corresponding to the URL.

|

| Please see the user guide for complete details:

|

|	http://codeigniter.com/user_guide/general/routing.html

|

| -------------------------------------------------------------------------

| RESERVED ROUTES

| -------------------------------------------------------------------------

|

| There area two reserved routes:

|

|	$route['default_controller'] = 'welcome';

|

| This route indicates which controller class should be loaded if the

| URI contains no data. In the above example, the "welcome" class

| would be loaded.

|

|	$route['404_override'] = 'errors/page_missing';

|

| This route will tell the Router what URI segments to use if those provided

| in the URL cannot be matched to a valid route.

|

*/

$filename="taxonomy.json";
$filepath=getcwd()."/application/cache/".$filename;
if(is_file($filepath))
{
	$myfile = fopen($filepath, "r") or die("Unable to open file!");
	$data= fread($myfile,filesize($filepath));	
	fclose($myfile);
	$result=json_decode($data, true);
	if(is_array($result))
	{
		$url_name=$result['project_url'];
		$route[$url_name.'/(.*)'] = "campaign/campaign_detail/$1";

		$company_url_name=$result['company_url'];//die;
		$route[$company_url_name.'/(.*)'] = "company/company_detail/$1";
	}
}
else
{
	include(APPPATH.'config/database.php');
	$active_group='default';
	if($db[$active_group]['pconnect'])
	{
		//$con=mysqli_connect($db[$active_group]['hostname'],$db[$active_group]['username'],$db[$active_group]['password']);
		$mysqli = new mysqli($db[$active_group]['hostname'],$db[$active_group]['username'],$db[$active_group]['password']);

		/* check connection */
		if ($mysqli->connect_errno) {
			printf("Connect failed: %s\n", $mysqli->connect_error);
			exit();
		}
		$mysqli->select_db($db[$active_group]['database']);
		/* Select queries return a resultset */
		if ($result = $mysqli->query("SELECT * FROM taxonomy_setting where language_id=1")) {
			
			$result_arr=$result->fetch_array();
			/* free result set */
			$result->close();
			if(is_array($result_arr))
			{
				$url_name=$result_arr['project_url'];//die;
				$route[$url_name.'/(.*)'] = "campaign/campaign_detail/$1";

				$company_url_name=$result_arr['company_url'];//die;
				$route[$company_url_name.'/(.*)'] = "company/company_detail/$1";
			}	
		}
		
		
	}
	
}	

$projectControllerName=$url_name;//only string,not other character will work
//$route['default_controller'] = "home";
$default_controller = "home";
$route['default_controller'] = $default_controller ;
//$controller_exceptions = array('admin','forums','profile','search','donation','projects','home','user','content');

$route['404_override'] = "home/error_page";

$route['admin'] = 'admin/home/index';

$route['invited/(.*)/(.*)']="home/invited/$1/$2";

$route['invited/(.*)']="home/invited/$1";

$route['invited']="home/signup";



$route['projects'] = "search/index";

$route['de/projects'] = "search/index";

$route['en/projects'] = "search/index";

$route['dashboard'] = "home/main_dashboard";
$route['dashboard/(.*)'] = "home/main_dashboard/$1";
$route['home/dashboard'] = "home/main_dashboard";
$route['home/dashboard/(.*)'] = "home/main_dashboard/$1";

$route['category/(.*)'] = "search/index/$1";

$route['de/category/(.*)'] = "search/index/$1";

$route['en/category/(.*)'] = "search/index/$1";



$route['projects/(.*)'] = "project/project_detail/$1";

$route['de/projects/(.*)/(.*)'] = "project/project_detail/$1/$2";

$route['en/projects/(.*)/(.*)'] = "project/project_detail/$1/$2";


$route['properties/(.*)'] = "property/property_detail/$1";

$route['de/properties/(.*)/(.*)'] = "property/property_detail/$1/$2";

$route['en/properties/(.*)/(.*)'] = "property/property_detail/$1/$2";


$route['crowds/(.*)'] = "crowd/crowd_detail/$1";

$route['de/crowds/(.*)/(.*)'] = "crowd/crowd_detail/$1/$2";

$route['en/crowds/(.*)/(.*)'] = "crowd/crowd_detail/$1/$2";



$route['offering/(.*)'] = "campaign/campaign_detail/$1";

$route['de/offering/(.*)/(.*)'] = "campaign/campaign_detail/$1/$2";

$route['en/offering/(.*)/(.*)'] = "campaign/campaign_detail/$1/$2";


$route['startproject/create-step1'] = "start_project/create_step1";

$route[$projectControllerName.'/(.*)/(.*)'] = "equity/$1/$2";
$route['start_'.$projectControllerName.'/(.*)'] = "start_equity/$1";
$route['start_'.$projectControllerName.'/(.*)/(.*)'] = "start_equity/$1/$2";
$route['start_'.$projectControllerName.'/(.*)/(.*)/(.*)'] = "start_equity/$1/$2/$3";
$route['follower/'.$projectControllerName.'_following'] = "follower/equity_following";

$route['reward'] = "search";

$route['de/reward'] = "search/index";

$route['en/reward'] = "search/index";





$route['reward/(.*)'] = "project/add_fund/$1";

$route['de/reward/(.*)'] = "project/add_fund/$1";

$route['en/reward/(.*)'] = "project/add_fund/$1";





$route['reward/(.*)/(.*)'] = "project/add_fund/$1/$2";

$route['de/reward/(.*)/(.*)'] = "project/add_fund/$1/$2";

$route['en/reward/(.*)/(.*)'] = "project/add_fund/$1/$2";









$route['member/(.*)'] = "profile/user_profile/$1";

$route['de/member/(.*)'] = "profile/user_profile/$1";

$route['en/member/(.*)'] = "profile/user_profile/$1";

$route['user/(.*)'] = "profile/user_profile/$1";


$route['ciunit'] = "ciunit_controller/index";
$route['ciunit/(:any)'] = "ciunit_controller/index/$1";



$route['^de/(.+)$'] = "$1";

$route['^en/(.+)$'] = "$1";


$route['contact_us'] = "content/contact_us";
$route['learn_more/(.*)'] = "content/learn_more/$1";
$route['faq'] = "content/faq";

//$route['([^/]+)/?'] = "content/pages/$1";
$route['content/(.*)'] = "content/pages/$1";

$route['^de$'] = $route['default_controller'];

$route['^en$'] = $route['default_controller'];

 //$route["^((?!\b".implode('\b|\b', $controller_exceptions)."\b).*)$"] = $default_controller.'/$1';

/* End of file routes.php */

/* Location: ./application/config/routes.php */

?>
