<?php
/*********************************************************************************
 * This the taskrabbitclone.com  by Rockers Technology. is paid software. It is released under the terms of 
 * the following BSD License.
 * 
 *  Rockers Technologies (Head Office)
 *    5038,Berthpage Dr
 *    suwanee, GA. Zip Code : 30024
    
 *    E-mail Address : nishu@rockersinfo.com
 * 
 * Copyright © 2012-2020 by Rockers Technology , INC a domestic profit corporation has been duly incorporated under
the laws of the state of georiga , USA. www.rockersinfo.com
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 
 * - Redistributions of source code must retain the above copyright notice, this 
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this 
 *   list of conditions and the following disclaimer in the documentation and/or 
 *   other materials provided with the distribution.

 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
class Cron extends ROCKERS_Controller {

	function Cron()
	{
		 parent::__construct();	
		$this->load->model('home_model');
		$this->load->model('project_model');
		$this->load->model('account_model');
		
	}
	
	
	//////////////===============check spam ip===========
	
	
	
	function check_spam_report()
	{
		
		$spam_control=$this->db->query("select * from spam_control");
		$control=$spam_control->row();
		
		$report_total=$control->spam_report_total;
		$total_register=$control->total_register;
		
		$report_expire=date('Y-m-d', strtotime('+'.$control->spam_report_expire.' days'));
		$register_expire=date('Y-m-d', strtotime('+'.$control->register_expire.' days'));
		
		
		$total_contact=$control->total_contact;
		$contact_expire=date('Y-m-d', strtotime('+'.$control->contact_expire.' days'));
		
		////////////////=======common check=================
		
		$get_report=$this->db->query("select spam_ip, count(*) as total from spam_report_ip group by spam_ip HAVING COUNT(*) >= ".$report_total);
		
		
		
		if($get_report->num_rows()>0)
		{
			$report=$get_report->result();
			
			foreach($report as $rs)
			{
				$insert_spam=$this->db->query("insert into spam_ip(`spam_ip`,`start_date`,`end_date`)values('".$rs->spam_ip."','".date('Y-m-d')."','".$report_expire."')");
												
				$delete_from_report=$this->db->query("delete from spam_report_ip where spam_ip='".$rs->spam_ip."'");
									
			}
		}
		
		
		////////////////=======common check=================
		
		
		////////////////=======Registration check=================
		
		
		$chk_register=$this->db->query("select signup_ip, count(*) as total from user group by signup_ip HAVING COUNT(*) >= ".$total_register);
		
		if($chk_register->num_rows()>0)
		{
			$register_report=$chk_register->result();
			
			foreach($register_report as $rs)
			{
				$insert_spam=$this->db->query("insert into spam_ip(`spam_ip`,`start_date`,`end_date`)values('".$rs->signup_ip."','".date('Y-m-d')."','".$register_expire."')");
													
			}
		}
		
		
		////////////////=======Registration check=================
		
		
		////////////////=======Inquire check=================
		
		
		$chk_inquiry=$this->db->query("select inquire_spam_ip, count(*) as total from spam_inquiry group by inquire_spam_ip HAVING COUNT(*) >= ".$total_contact);
		
		if($chk_inquiry->num_rows()>0)
		{
			$inquiry_report=$chk_inquiry->result();
			
			foreach($inquiry_report as $rs)
			{
				$insert_spam=$this->db->query("insert into spam_ip(`spam_ip`,`start_date`,`end_date`)values('".$rs->inquire_spam_ip."','".date('Y-m-d')."','".$contact_expire."')");
													
			}
		}
		
		
		////////////////=======Inquire check=================
		
		
		
			
	}
	
	
	//////////////===============make spam ip expire===========
	
	function allow_block_ip()
	{
		
		$check_expire=$this->db->query("select * from spam_ip where permenant_spam!='1' and end_date='".date('Y-m-d')."'");
		
		if($check_expire->num_rows()>0)
		{
			$expire=$check_expire->result();
			
			foreach($expire as $exp)
			{
				$delete_spam=$this->db->query("delete from spam_ip where spam_ip='".$exp->spam_ip."'");
			}			
		}	
	
	}
	
	
	
	//////////////===============check spam ip===========
	
	
	
	
	
function cron_preapprove()
{
		
		$site_setting = site_setting();
		$paypal= adaptive_paypal_detail();	
		if($site_setting['currency_code']!='') 
		{
		
			$currency_code=$site_setting['currency_code'];
		
		}
		else
		{
			$currency_code='USD';
		}
		$chk_auto_target_achive=$site_setting['auto_target_achive'];
		////////=============email=====================
		$this->load->library('email');
		$email_setting=$this->db->query("select * from `email_setting` where email_setting_id='1'");
		$email_set=$email_setting->row();

			///////====smtp====			
			if($email_set->mailer=='smtp')
			{
				$config['protocol']='smtp';  
				$config['smtp_host']=trim($email_set->smtp_host);  
				$config['smtp_port']=trim($email_set->smtp_port);  
				$config['smtp_timeout']='30';  
				$config['smtp_user']=trim($email_set->smtp_email);  
				$config['smtp_pass']=trim($email_set->smtp_password);  
						
			}			
			/////=====sendmail======			
			elseif(	$email_set->mailer=='sendmail')
			{	
				$config['protocol'] = 'sendmail';
				$config['mailpath'] = trim($email_set->sendmail_path);
				
			}			
			/////=====php mail default======			
			else
			{
			
			}			
			$config['wordwrap'] = TRUE;	
			$config['mailtype'] = 'html';
			$config['crlf'] = '\n\n';
			$config['newline'] = '\n\n';			
			$this->email->initialize($config);
			if($chk_auto_target_achive==1)
			{
				$sql="select * from project where (end_date<='".date("Y-m-d H:i:s")."' or  CAST(amount_get as decimal(10,2))>= CAST(amount as decimal(10,2))) and active='2'";
				$get_project=$this->db->query($sql);
			}
			else
			{
				$sql="select * from project where end_date<=now() and active='2'";
				$get_project=$this->db->query($sql);
			}	 
	echo "<br>222".$sql;
			if($get_project->num_rows()>0)
			{	
					echo "<br>224";	
						if($paypal->gateway_status=='1')
						{
								
								$this->adaptive_initailze($paypal);		
							//	$this->load->library('NVP_SampleConstants');		
							//	$NVP_SampleConstants = new NVP_SampleConstants();
						}		
								$email_template=$this->db->query("select * from `email_template` where task='New Fund Admin Notification'");
								$email_temp=$email_template->row();			
								$email_address_from=$email_temp->from_address;
								$email_address_reply=$email_temp->reply_address;
									$project=$get_project->result();
									foreach($project as $prjs)
									{
											if($paypal->gateway_status=='1')
											{	echo "<br>239";
												//fixed project tyoe 
												if($prjs->payment_type=='0')
												{echo "<br>244";
														//if achieved goal
														if($prjs->amount_get>=$prjs->amount)
														{
															$this->fixed_success($prjs,$currency_code);
															
														}
														else
														{
															$this->fixed_failure($prjs,$currency_code);
														}	
												
												} 
												else
												{echo "<br>258";
														////////=======FLXIBLE PROJECT	
														if($prjs->amount_get>=$prjs->amount)
														{
															echo "<br>264";
															$this->flexible_success($prjs,$currency_code);
															
														}
														else
														{echo "<br>269";
															$this->flexible_unsuccess($prjs,$currency_code);
														}
												
												}
											}
											else
											{
														echo "<br>271";
														if($prjs->amount_get>=$prjs->amount)
														{
															//for success project with achieve goal
															$this->db->query("update project set `active`='3' , `status`='5' where project_id='".$prjs->project_id."'");
															
														}
														else
														{
														//for unsuccess project with not achieve goal
															$this->db->query("update project set `active`='4' , `status`='5' where project_id='".$prjs->project_id."'");
														}
											
											}	
												
												
												
														//$this->db->query("update project set `active`='0' , `status`='5' where project_id='".$prjs->project_id."'");
											
									}    /////===foreach project
							
							
			}   ////=====get project
					
			
				


			$data = array(
						'user_id' => '0',
						'cronjob' => '',
						'date_run' =>date('Y-m-d H:i:s'),
						'status' => '1',
					);
			$this->db->insert('cronjob', $data);
			
			die;	
		
}
//fixed project achieved goal

function  fixed_success($prjs,$currency_code)
{
	
							echo "<br>321";
								$site_setting = site_setting();
								$paypal= adaptive_paypal_detail();
								//$this->adaptive_initailze($paypal);
								// all the PayPal Header parameters are set in lib/callerservice.php

								$this->load->library('NVP_SampleConstants');
								$NVP_SampleConstants = new NVP_SampleConstants();
								$feesPayer='PRIMARYRECEIVER'; 
								$actionType='PAY';
								
								$id=$prjs->project_id;					
								$get_project_user_detail=$this->project_model->GetAllProjects(0,$id);
								$prj = $get_project_user_detail[0];	
								if($prj['project_currency_code']!='') 
								{		
									$currency_code=$prj['project_currency_code'];			
								}
									
								$user_detail =$this->account_model->GetAllUsers($prj['user_id']);	
								$user_detail =	$user_detail[0];						
								$project_title=$prj['project_title'];
								$project_id=$prjs->project_id;									 
								if(isset($prj['paypal_email']) or $prj['paypal_email']!='')$project_owner_email=$prj['paypal_email'];
								else $project_owner_email=$user_detail['paypal_email'];
								$project_create_name=$user_detail['user_name'].' '.$user_detail['last_name'];				
								$sql=	"select * from transaction where project_id='".$prjs->project_id."' and preapproval_key!='' and (preapproval_status='FAIL' or preapproval_status='ON_HOLD')";		
								echo $sql;
								$get_preapproval=$this->db->query($sql);
								if($get_preapproval->num_rows()>0)
								{
										$transaction_detail=$get_preapproval->result();					
										foreach($transaction_detail as $transd)
										{
												try 
												{
														$donar_proj=$this->account_model->GetAllUsers($transd->user_id);	
														$donar_proj =	$donar_proj[0];
														$returnURL = site_url('cron/cron_preapprove/');     //////////=project detail page link will goes here
														$cancelURL = site_url('cron/cron_preapprove/');
														$ipnNotificationUrl=site_url('cron/cron_preapprove/');											
														$preapprovalKey=$transd->preapproval_key; /////////////////////===========get from the paypal database or take from user post=======
														$donar_email=$transd->email;
														$donar_comment=$transd->comment;
														$donar_name=$transd->name;	
														$perk_id=$transd->perk_id;	
														$last_transaction_id=$transd->transaction_id;										
														$donar_amount = $transd->preapproval_total_amount;  ////===user posted amount
														$orignial_amount=$donar_amount;
														$transaction_fees = $site_setting['fixed_fees'];
														$admin_amount= (($donar_amount * $transaction_fees)/100);   
														$project_owner_amount= $donar_amount;   						////=======assign full amount to the owner for fees payer						
														$admin_amount= str_replace(',','',number_format($admin_amount,2));
														$project_owner_amount=str_replace(',','',number_format($project_owner_amount,2));
														$senderEmail  =$donar_email;				///////////========*******DONAR/SENDER Pay Email Address====*******
														$payer_name='Project Creator';
														
														$request_array= array(
														Pay::$actionType => $actionType,
														Pay::$cancelUrl  => $cancelURL,
														Pay::$returnUrl=>   $returnURL,
														Pay::$ipnNotificationUrl=>   $ipnNotificationUrl,
														Pay::$currencyCode  =>$currency_code,   
														Pay::$clientDetails_deviceId  => DEVICE_ID,
														Pay::$clientDetails_ipAddress  => $_SERVER['REMOTE_ADDR'],
														Pay::$clientDetails_applicationId =>APPLICATION_ID,
														RequestEnvelope::$requestEnvelopeErrorLanguage => 'en_US',
														Pay::$memo =>  "Project : ".$project_title."(".$project_id."), Amount : ".$donar_amount .",".$payer_name." Pay Fees(%) : ".$transaction_fees.", Create By : ".$project_create_name.", On Site : ".base_url() ,				/////////=============comment send to all payment receiver ====												
														Pay::$feesPayer => $feesPayer              /////////////////set in the  pay from means our database adaptive setting  
														);
														
														$i=0;
														if($project_owner_amount>0)
														{
															$request_array[Pay::$receiverEmail[$i]] = $project_owner_email;
															$request_array[Pay::$receiverAmount[$i]] = $project_owner_amount;											
															$request_array[Pay::$primaryReceiver[$i]] = 'true';
															$i++;
														}	
														////////=set Admin  receiver============
															
														$admin_email=$paypal->paypal_email;	
														if($admin_amount>0)
														{									
															$request_array[Pay::$receiverEmail[$i]] = $admin_email;
															$request_array[Pay::$receiverAmount[$i]] = $admin_amount;											
															if($i>0)$request_array[Pay::$primaryReceiver[$i]] = 'false';
															else $request_array[Pay::$primaryReceiver[$i]] = 'true';
														}	
														
														if($preapprovalKey!= "")
														{
															$request_array[Pay::$preapprovalKey] = $preapprovalKey;
														}
														
														if($senderEmail!= "")
														{
															$request_array[Pay::$senderEmail]  = $senderEmail;
														}
														$nvpStr=http_build_query($request_array, '', '&');	
														$resArray=$NVP_SampleConstants->hash_call('AdaptivePayments/Pay',$nvpStr);										
														$ack = strtoupper($resArray['responseEnvelope.ack']);
																	
																	$strtemp='Response From cron_preapprove 320::';
																	foreach ($resArray as $key => $value) {
																			$strtemp.= $key."=".$value.",";
																		}
																		log_message('error',"CRON DATA:".$strtemp);
					
														if($ack!="SUCCESS")
														{
															$data= array('reshash' => 	$resArray);
															$this->session->set_userdata($data);
															$case ="";
															
															
															$payee_email=$donar_proj['email'];
															$paypal_msg1 = sprintf(CRON_YOUR_PAYMENT_PROCESS_VIOLATED_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU,$donar_proj['user_name'],$resArray['error(0).message']);
															$paypal_msg2 = sprintf(CRON_YOUR_PAYMENT_PROCESS_VIOLATED_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU,$donar_proj['user_name'],$resArray['error(0).message']);
															
															$paypal_msg3 = sprintf(CRON_HELLO_ADMINISTRATOR_APIERROR7_PAYMENT_PROCESS_VIOLATED_DUE_TO_ERRORMESSAGE_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU,$resArray['error(0).message'],$donar_proj['user_name'],$donar_proj['email'],$donar_email);
															paypal_failure_email( $donar_email,$donar_name,$payee_email,$project_id,$project_title,$paypal_msg1,$paypal_msg2,$paypal_msg3)	;
																													
														}
														else
														{
															$data2= array('payKey' =>	$resArray['payKey']);
															$this->session->set_userdata($data2);
															$payKey=$resArray['payKey'];
															$case ="";
																		
																		if(($resArray['paymentExecStatus'] == "COMPLETED" ))
																		{
																			$case ="1";
																		}
																		else if(($actionType== "PAY") && ($resArray['paymentExecStatus'] == "CREATED" ))
																		{
																			$case ="2";
																				
																		}
																		else if(($preapprovalKey!=null ) && ($actionType== "CREATE") && ($resArray['paymentExecStatus'] == "CREATED" ))
																		{
																			$case ="3";
																		}
																		else if(($actionType== "PAY_PRIMARY"))
																		{
																			$case ="4";
																				
																		}
																		else if(($actionType== "CREATE") && ($resArray['paymentExecStatus'] == "CREATED" ))
																		{
																			
																			$temp1=API_USERNAME;
																			$temp2=str_replace('_api1.','@',$temp1);
																			
																			if($temp2==$donar_email)
																			{
																				$case ="3";
																			}
																			else{
																				$case ="2";
																			}
																			
																		}															
																		else
																		{
																			$donar_proj=$this->account_model->GetAllUsers($transd->user_id);	
																			$donar_proj =	$donar_proj[0];
																			
																			$payee_email=$donar_proj['email'];
																			$paypal_msg1 = sprintf(CRON_YOUR_PAYMENT_PROCESS_VIOLATED_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU,$donar_proj['user_name'],$resArray['error(0).message']);
																			$paypal_msg2 = sprintf(CRON_YOUR_PAYMENT_PROCESS_VIOLATED_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU,$donar_proj['user_name'],$resArray['error(0).message']);
																			
																			$paypal_msg3 = sprintf(CRON_HELLO_ADMINISTRATOR_APIERROR7_PAYMENT_PROCESS_VIOLATED_DUE_TO_ERRORMESSAGE_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU,$resArray['error(0).message'],$donar_proj['user_name'],$donar_proj['email'],$donar_email);
																			paypal_failure_email( $donar_email,$donar_name,$payee_email,$project_id,$project_title,$paypal_msg1,$paypal_msg2,$paypal_msg3)	;
																		}
																	}
														
														
															}			
											catch(Exception $ex) 
											{
																			throw new Exception('Error occurred in PayReceipt method');
																			$donar_proj=$this->account_model->GetAllUsers($transd->user_id);	
																			$donar_proj =	$donar_proj[0];
																			
																			$payee_email=$donar_proj['email'];
																			$paypal_msg1 = sprintf(CRON_YOUR_PAYMENT_PROCESS_VIOLATED_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU,$donar_proj['user_name'],$resArray['error(0).message']);
																			$paypal_msg2 = sprintf(CRON_YOUR_PAYMENT_PROCESS_VIOLATED_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU,$donar_proj['user_name'],$resArray['error(0).message']);
																			
																			$paypal_msg3 = sprintf(CRON_HELLO_ADMINISTRATOR_APIERROR7_PAYMENT_PROCESS_VIOLATED_DUE_TO_ERRORMESSAGE_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU,$resArray['error(0).message'],$donar_proj['user_name'],$donar_proj['email'],$donar_email);
																			paypal_failure_email( $donar_email,$donar_name,$payee_email,$project_id,$project_title,$paypal_msg1,$paypal_msg2,$paypal_msg3)	;
											
											}				
														
										if($case=='1')
										{
												
												if(strtoupper($resArray['paymentExecStatus']) == "COMPLETED")
												{
													
													$update_status=$this->db->query("update transaction set `preapproval_status`='SUCCESS' , `preapproval_pay_key`='".$resArray['payKey']."' where `preapproval_key`='".$transd->preapproval_key."'");	
													//update project fir success
													$update_project_inactive=$this->db->query("update project set active=0 where project_id='".$transd->project_id."'");
													$donar_proj=$this->account_model->GetAllUsers($transd->user_id);	
													$donar_proj =	$donar_proj[0];
													
													/////////============SUCCESS Email ==============
												
													$payee_email=$transd->email;			
													$donar_email=$login_user['email'];	
													$donar_id=$transd->user_id;			
													$project_id=$prj['project_id'];			
													$project_owner_email=$prj['email'];		
														
													$username =$prj['user_name'];
													$project_name=$prj['project_title'];
													$project_page_link=site_url('projects/'.$prj['url_project_title'].'/'.$prj['project_id']);
													
													$donor_name=$login_user['user_name'];
													$donote_amount=$transd->preapproval_total_amount;			
													$donor_profile_link=site_url('member/'.$transd->user_id);				
														
													///////////============admin alert==========
														$this->adminalerts($prj,$transd,$donar_proj);
														
											
													
													
												}
																	
										}						
										else
										{
											
																			$donar_proj=$this->account_model->GetAllUsers($transd->user_id);	
																			$donar_proj =	$donar_proj[0];
																			
																			$payee_email=$donar_proj['email'];
																			$paypal_msg1 = sprintf(CRON_YOUR_PAYMENT_PROCESS_VIOLATED_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU,$donar_proj['user_name'],$resArray['error(0).message']);
																			$paypal_msg2 = sprintf(CRON_YOUR_PAYMENT_PROCESS_VIOLATED_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU,$donar_proj['user_name'],$resArray['error(0).message']);
																			
																			$paypal_msg3 = sprintf(CRON_HELLO_ADMINISTRATOR_APIERROR7_PAYMENT_PROCESS_VIOLATED_DUE_TO_ERRORMESSAGE_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU,$resArray['error(0).message'],$donar_proj['user_name'],$donar_proj['email'],$donar_email);
																			paypal_failure_email( $donar_email,$donar_name,$payee_email,$project_id,$project_title,$paypal_msg1,$paypal_msg2,$paypal_msg3)	;
											
										}
									
							/////////===========for payment redirect user to this link============= 		
								
							
										}  ///=====foreach preapproval
							
								 
								 
									}   ////====if found any key
								
								//for success project
								$this->db->query("update project set `active`='3' , `status`='5' where project_id='".$prjs->project_id."'");
							
							
							

}
//fixed project not achieved goal
function  fixed_failure($prjs,$currency_code)
{echo "<br>591";

								//for failure project
								$this->db->query("update project set `active`='5' , `status`='5' where project_id='".$prjs->project_id."'");
}	
//flexible project achieved goal

function  flexible_success($prjs,$currency_code)
{
		////////=======FLXIBLE PROJECT					
								
								//$prj=$prjs;
								echo "<br>600";
								$site_setting = site_setting();
								$paypal= adaptive_paypal_detail();
								//$this->adaptive_initailze($paypal);
								// all the PayPal Header parameters are set in lib/callerservice.php

								$this->load->library('NVP_SampleConstants');
								$NVP_SampleConstants = new NVP_SampleConstants();
								
								$feesPayer='PRIMARYRECEIVER'; 
								$actionType='PAY';
								$id=$prjs->project_id;				
								$get_project_user_detail=$this->project_model->GetAllProjects(0,$id);
								$prj = $get_project_user_detail[0];	
								if($prj['project_currency_code']!='') 
								{		
									$currency_code=$prj['project_currency_code'];			
								}							
								$user_detail =$this->account_model->GetAllUsers($prj['user_id']);	
								$user_detail =	$user_detail[0];				
								$project_title=$prj['project_title'];
								$project_id=$prjs->project_id;		
								
								if($prj['paypal_email']!='')
								{
								$project_owner_email=$prj['paypal_email'];
								$project_create_name=$prj['paypal_first_name'].' '.$prj['paypal_last_name'];								
								}else
								{
														 
								$project_owner_email=$user_detail['paypal_email'];
								$project_create_name=$user_detail['user_name'].' '.$user_detail['last_name'];
								
								}
								$sql="select * from transaction where project_id='".$prjs->project_id."' and preapproval_key!='' and (preapproval_status='FAIL' or preapproval_status='ON_HOLD')";
								echo "<br>628".$sql;
								$get_preapproval=$this->db->query($sql);
								if($get_preapproval->num_rows()>0)
								{						
										echo "<br>628";
										$transaction_detail=$get_preapproval->result();
										foreach($transaction_detail as $transd)
										{
											
											echo "<br>633";
											try 
											{
												echo "<br>639";
												$returnURL = site_url('cron/cron_preapprove/');     //////////=project detail page link will goes here
												$cancelURL = site_url('cron/cron_preapprove/');
												$ipnNotificationUrl=site_url('cron/cron_preapprove/');								
												$preapprovalKey=$transd->preapproval_key;   /////////////////////===========get from the paypal database or take from user post=======
												$donar_email=$transd->paypal_email;
												$donar_comment=$transd->comment;
												$donar_name=$transd->name;
												
												$donar_amount = $transd->preapproval_total_amount;  ////===user posted amount
												$orignial_amount=$donar_amount;
												$project_id=$transd->project_id;
												$perk_id=$transd->perk_id;
												$last_transaction_id=$transd->transaction_id;
												
												
												$transaction_fees=$paypal->transaction_fees;  ////= from the database
												if($prj['amount_get']< $prj['amount']){  $not_reach=1;   } else { $not_reach=0;  }
												if($not_reach==1){	
													$transaction_fees = $site_setting['flexible_fees'];
												} else{			
													$transaction_fees = $site_setting['suc_flexible_fees'];
												}
												
													
												$admin_amount_org= (($donar_amount * $transaction_fees)/100);   
												$project_owner_amount=  $transd->amount;    
												$admin_amount= number_format($admin_amount_org,2);
												$project_owner_amount=number_format($project_owner_amount,2);
												$senderEmail  =$donar_email;				///////////========*******DONAR/SENDER Pay Email Address====*******
												
												$payer_name='Project Creator';								
												$request_array= array(
												Pay::$actionType => $actionType, 
												Pay::$cancelUrl  => $cancelURL,
												Pay::$returnUrl=>   $returnURL,
												Pay::$ipnNotificationUrl=>   $ipnNotificationUrl,
												Pay::$currencyCode  =>$currency_code,  
												Pay::$clientDetails_deviceId  => DEVICE_ID,
												Pay::$clientDetails_ipAddress  => $_SERVER['REMOTE_ADDR'],
												Pay::$clientDetails_applicationId =>APPLICATION_ID,
												RequestEnvelope::$requestEnvelopeErrorLanguage => 'en_US',							
												Pay::$memo =>  "Project : ".$project_title."(".$project_id."), Amount : ".$donar_amount .",".$payer_name." Pay Fees(%) : ".$transaction_fees.", Create By : ".$project_create_name.", On Site : ".base_url() ,			/////////=============comment send to all payment receiver ====									
												Pay::$feesPayer => $feesPayer              /////////////////set in the  pay from means our database adaptive setting  
												);
											/////////================Set all receiver amount and email and  primary email
														$i=0;
														if($donar_amount>0)
														{
															$request_array[Pay::$receiverEmail[$i]] = $project_owner_email;
															$request_array[Pay::$receiverAmount[$i]] = $project_owner_amount;											
															$request_array[Pay::$primaryReceiver[$i]] = 'true';
															$i++;
														}	
														////////=set Admin  receiver============
															
														$admin_email=$paypal->paypal_email;	
														if($admin_amount_org>0)
														{									
															$request_array[Pay::$receiverEmail[$i]] = $admin_email;
															$request_array[Pay::$receiverAmount[$i]] = $admin_amount;											
															if($i>0)$request_array[Pay::$primaryReceiver[$i]] = 'false';
															else $request_array[Pay::$primaryReceiver[$i]] = 'true';
														}	
												if($preapprovalKey!= "")
												{
													$request_array[Pay::$preapprovalKey] = $preapprovalKey;
												}
												
																				
												///////////=========ABOVE SET======*******DONAR/SENDER Pay Email Address====*******
												if($senderEmail!= "")
												{
													$request_array[Pay::$senderEmail]  = $senderEmail;
												}
											
												$nvpStr=http_build_query($request_array, '', '&');									
												$resArray=$NVP_SampleConstants->hash_call('AdaptivePayments/Pay',$nvpStr);							
												$ack = strtoupper($resArray['responseEnvelope.ack']);
															
															$strtemp='Response from Cron preapprival_cron 709::';
															foreach ($resArray as $key => $value) {
																	$strtemp.= $key."=".$value.",";
																}
															log_message('error',"CRON DATA:".$strtemp);
															
															$data_insert=array('paypal_request'=>$nvpStr,
														   'paypal_response'=>$strtemp,'project_id'=>$project_id,
														   'donar_id'=>$donar_email,
														   'transaction_status'=>$ack,
														   'insert_date'=>date('Y-m-d H:i:s')
															  );													  

													 		$this->db->insert('paypal_response_request',$data_insert);
													 
													 
												if($ack!="SUCCESS")
												{
																$data= array(
																	'reshash' => 	$resArray,	 
																	);
																$this->session->set_userdata($data);
																$case ="";
																$donar_proj=$this->account_model->GetAllUsers($transd->user_id);	
																$donar_proj =	$donar_proj[0];
																				
																$payee_email=$donar_proj['email'];
																$paypal_msg1 = sprintf(CRON_YOUR_PAYMENT_PROCESS_VIOLATED_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU,$donar_proj['user_name'],$resArray['error(0).message']);
																$paypal_msg2 = sprintf(CRON_YOUR_PAYMENT_PROCESS_VIOLATED_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU,$donar_proj['user_name'],$resArray['error(0).message']);
																				
																$paypal_msg3 = sprintf(CRON_HELLO_ADMINISTRATOR_APIERROR7_PAYMENT_PROCESS_VIOLATED_DUE_TO_ERRORMESSAGE_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU,$resArray['error(0).message'],$donar_proj['user_name'],$donar_proj['email'],$donar_email);
																paypal_failure_email( $donar_email,$donar_name,$payee_email,$project_id,$project_title,$paypal_msg1,$paypal_msg2,$paypal_msg3)	;
																
												}
												else
												{		$data2= array('payKey' =>	$resArray['payKey']);
														$this->session->set_userdata($data2);
														$case ="";
																
																if(($resArray['paymentExecStatus'] == "COMPLETED" ))
																{
																	$case ="1";
																}
																else if(($actionType== "PAY") && ($resArray['paymentExecStatus'] == "CREATED" ))
																{
																	$case ="2";
																		
																}
																else if(($preapprovalKey!=null ) && ($actionType== "CREATE") && ($resArray['paymentExecStatus'] == "CREATED" ))
																{
																	$case ="3";
																}
																else if(($actionType== "PAY_PRIMARY"))
																{
																	$case ="4";
																		
																}
																else if(($actionType== "CREATE") && ($resArray['paymentExecStatus'] == "CREATED" ))
																{
																	
																	$temp1=API_USERNAME;
																	$temp2=str_replace('_api1.','@',$temp1);
																	
																	if($temp2==$donar_email)
																	{
																		$case ="3";
																	}
																	else{
																		$case ="2";
																	}
																	
																}
																
																else
																{
																	$donar_proj=$this->account_model->GetAllUsers($transd->user_id);	
																	$donar_proj =	$donar_proj[0];
																					
																	$payee_email=$donar_proj['email'];
																	$paypal_msg1 = sprintf(CRON_YOUR_PAYMENT_PROCESS_VIOLATED_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU,$donar_proj['user_name'],$resArray['error(0).message']);
																	$paypal_msg2 = sprintf(CRON_YOUR_PAYMENT_PROCESS_VIOLATED_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU,$donar_proj['user_name'],$resArray['error(0).message']);
																					
																	$paypal_msg3 = sprintf(CRON_HELLO_ADMINISTRATOR_APIERROR7_PAYMENT_PROCESS_VIOLATED_DUE_TO_ERRORMESSAGE_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU,$resArray['error(0).message'],$donar_proj['user_name'],$donar_proj['email'],$donar_email);
																	paypal_failure_email( $donar_email,$donar_name,$payee_email,$project_id,$project_title,$paypal_msg1,$paypal_msg2,$paypal_msg3)	;
																}
															}
												
												
													}
								
								
								catch(Exception $ex) 
								{
																echo "<br>798";
																throw new Exception('Error occurred in PayReceipt method');
									
																$donar_proj=$this->account_model->GetAllUsers($transd->user_id);	
																$donar_proj =	$donar_proj[0];
																				
																$payee_email=$donar_proj['email'];
																$paypal_msg1 = sprintf(CRON_YOUR_PAYMENT_PROCESS_VIOLATED_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU,$donar_proj['user_name'],$resArray['error(0).message']);
																$paypal_msg2 = sprintf(CRON_YOUR_PAYMENT_PROCESS_VIOLATED_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU,$donar_proj['user_name'],$resArray['error(0).message']);
																				
																$paypal_msg3 = sprintf(CRON_HELLO_ADMINISTRATOR_APIERROR7_PAYMENT_PROCESS_VIOLATED_DUE_TO_ERRORMESSAGE_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU,$resArray['error(0).message'],$donar_proj['user_name'],$donar_proj['email'],$donar_email);
																paypal_failure_email( $donar_email,$donar_name,$payee_email,$project_id,$project_title,$paypal_msg1,$paypal_msg2,$paypal_msg3)	;
											
								}
									/////////===========for payment redirect user to this link============= 
									if($case=='1')
									{
											
											if(strtoupper($resArray['paymentExecStatus']) == "COMPLETED")
											{
												
												echo "update transaction set `preapproval_status`='SUCCESS' , `preapproval_pay_key`='".$resArray['payKey']."' where `preapproval_key`='".$transd->preapproval_key."'";
												$update_status=$this->db->query("update transaction set `preapproval_status`='SUCCESS' , `preapproval_pay_key`='".$resArray['payKey']."' where `preapproval_key`='".$transd->preapproval_key."'");			$update_project_inactive=$this->db->query("update project set active=3 where project_id='".$transd->project_id."'");
												$donar_proj=$this->account_model->GetAllUsers($transd->user_id);	
												$donar_proj =	$donar_proj[0];
												
												/////////============SUCCESS Email ==============
													
										
														///////////============admin alert==========
															$this->adminalerts($prj,$transd,$donar_proj);
												
												
											}
										
									}
									
									else
									{
																$update_status=$this->db->query("update transaction set `preapproval_status`='FAIL' , `preapproval_pay_key`='' where `preapproval_key`='".$transd->preapproval_key."'");	
																$donar_proj=$this->account_model->GetAllUsers($transd->user_id);	
																$donar_proj =	$donar_proj[0];
																				
																$payee_email=$donar_proj['email'];
																$paypal_msg1 = sprintf(CRON_YOUR_PAYMENT_PROCESS_VIOLATED_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU,$donar_proj['user_name'],$resArray['error(0).message']);
																$paypal_msg2 = sprintf(CRON_YOUR_PAYMENT_PROCESS_VIOLATED_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU,$donar_proj['user_name'],$resArray['error(0).message']);
																				
																$paypal_msg3 = sprintf(CRON_HELLO_ADMINISTRATOR_APIERROR7_PAYMENT_PROCESS_VIOLATED_DUE_TO_ERRORMESSAGE_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU,$resArray['error(0).message'],$donar_proj['user_name'],$donar_proj['email'],$donar_email);
																paypal_failure_email( $donar_email,$donar_name,$payee_email,$project_id,$project_title,$paypal_msg1,$paypal_msg2,$paypal_msg3)	;
											
										
									}
									
									
									
									
									///redirect($payPalURL);
									
									
							/////////===========for payment redirect user to this link============= 		
								
							
										}  ///=====foreach preapproval
							
								 
								 
									}   ////====if found any key
								
								
								
								//for success project
								$this->db->query("update project set `active`='3' , `status`='5' where project_id='".$prjs->project_id."'");
							
								
								
								
								
}
//flexible project not achieved goal

function  flexible_unsuccess($prjs,$currency_code)
{

								
		////////=======FLXIBLE PROJECT	
		echo "<br>877";				
								$site_setting = site_setting();
								$paypal= adaptive_paypal_detail();
								//$this->adaptive_initailze($paypal);
								// all the PayPal Header parameters are set in lib/callerservice.php

								$this->load->library('NVP_SampleConstants');
								$NVP_SampleConstants = new NVP_SampleConstants();
								$feesPayer='PRIMARYRECEIVER'; 
								$actionType='PAY';
								$id=$prjs->project_id;				
								$get_project_user_detail=$this->project_model->GetAllProjects(0,$id);
								$prj = $get_project_user_detail[0];		
								if($prj['project_currency_code']!='') 
								{		
									$currency_code=$prj['project_currency_code'];			
								}						
								$user_detail =$this->account_model->GetAllUsers($prj['user_id']);	
								$user_detail =	$user_detail[0];				
								$project_title=$prj['project_title'];
								$project_id=$prjs->project_id;								 
								$project_owner_email=$user_detail['paypal_email'];
								$project_create_name=$user_detail['user_name'].' '.$user_detail['last_name'];
								$get_preapproval=$this->db->query("select * from transaction where project_id='".$prjs->project_id."' and preapproval_key!='' and  (preapproval_status='FAIL' or preapproval_status='ON_HOLD')");
								if($get_preapproval->num_rows()>0)
								{						
										$transaction_detail=$get_preapproval->result();
										foreach($transaction_detail as $transd)
										{
											try 
											{
												$returnURL = site_url('cron/cron_preapprove/');     //////////=project detail page link will goes here
												$cancelURL = site_url('cron/cron_preapprove/');
												$ipnNotificationUrl=site_url('cron/cron_preapprove/');								
												$preapprovalKey=$transd->preapproval_key;   /////////////////////===========get from the paypal database or take from user post=======
												$donar_email=$transd->paypal_email;
												$donar_comment=$transd->comment;
												$donar_name=$transd->name;
												
												$donar_amount = $transd->preapproval_total_amount;  ////===user posted amount
												$orignial_amount=$donar_amount;
												$project_id=$transd->project_id;
												$perk_id=$transd->perk_id;
												$last_transaction_id=$transd->transaction_id;
												
												$transaction_fees=$paypal->transaction_fees;  ////= from the database
												if($prj['amount_get'] < $prj['amount']){  $not_reach=1;   } else { $not_reach=0;  }
												if($not_reach==1){	
													$transaction_fees = $site_setting['flexible_fees'];
												} else{			
													$transaction_fees = $site_setting['suc_flexible_fees'];
												}	
												$admin_amount_org= (($donar_amount * $transaction_fees)/100);   
												$project_owner_amount=  $transd->amount;  
												$admin_amount= number_format($admin_amount_org,2);
												$project_owner_amount=number_format($project_owner_amount,2);
												$senderEmail  =$donar_email;				///////////========*******DONAR/SENDER Pay Email Address====*******
												
												$payer_name='Project Creator';								
												$request_array= array(
												Pay::$actionType => $actionType, 
												Pay::$cancelUrl  => $cancelURL,
												Pay::$returnUrl=>   $returnURL,
												Pay::$ipnNotificationUrl=>   $ipnNotificationUrl,
												Pay::$currencyCode  =>$currency_code,  
												Pay::$clientDetails_deviceId  => DEVICE_ID,
												Pay::$clientDetails_ipAddress  => $_SERVER['REMOTE_ADDR'],
												Pay::$clientDetails_applicationId =>APPLICATION_ID,
												RequestEnvelope::$requestEnvelopeErrorLanguage => 'en_US',							
												Pay::$memo =>  "Project : ".$project_title."(".$project_id."), Amount : ".$donar_amount .",".$payer_name." Pay Fees(%) : ".$transaction_fees.", Create By : ".$project_create_name.", On Site : ".base_url() ,			/////////=============comment send to all payment receiver ====									
												Pay::$feesPayer => $feesPayer              /////////////////set in the  pay from means our database adaptive setting  
												);
											/////////================Set all receiver amount and email and  primary email
												$i=0;
														if($donar_amount>0)
														{
															$request_array[Pay::$receiverEmail[$i]] = $project_owner_email;
															$request_array[Pay::$receiverAmount[$i]] = $project_owner_amount;											
															$request_array[Pay::$primaryReceiver[$i]] = 'true';
															$i++;
														}	
														////////=set Admin  receiver============
															
														$admin_email=$paypal->paypal_email;	
														if($admin_amount_org>0)
														{									
															$request_array[Pay::$receiverEmail[$i]] = $admin_email;
															$request_array[Pay::$receiverAmount[$i]] = $admin_amount;											
															if($i>0)$request_array[Pay::$primaryReceiver[$i]] = 'false';
															else $request_array[Pay::$primaryReceiver[$i]] = 'true';
														}	
												if($preapprovalKey!= "")
												{
													$request_array[Pay::$preapprovalKey] = $preapprovalKey;
												}
												
																				
												///////////=========ABOVE SET======*******DONAR/SENDER Pay Email Address====*******
												if($senderEmail!= "")
												{
													$request_array[Pay::$senderEmail]  = $senderEmail;
												}
											
												$nvpStr=http_build_query($request_array, '', '&');									
												$resArray=$NVP_SampleConstants->hash_call('AdaptivePayments/Pay',$nvpStr);							
												$ack = strtoupper($resArray['responseEnvelope.ack']);
															
															$strtemp='Response from Cron preapprival_cron 984::';
															foreach ($resArray as $key => $value) {
																	$strtemp.= $key."=".$value.",";
																}
															log_message('error',"CRON DATA:".$strtemp);
															
															$data_insert=array('paypal_request'=>$nvpStr,
														   'paypal_response'=>$strtemp,'project_id'=>$project_id,
														   'donar_id'=>$donar_email,
														   'transaction_status'=>$ack,
														   'insert_date'=>date('Y-m-d H:i:s')
															  );													  

													 		$this->db->insert('paypal_response_request',$data_insert);
															
															
												if($ack!="SUCCESS")
												{
																$data= array(
																	'reshash' => 	$resArray,	 
																	);
																$this->session->set_userdata($data);
																$case ="";
																$donar_proj=$this->account_model->GetAllUsers($transd->user_id);	
																$donar_proj =	$donar_proj[0];
																				
																$payee_email=$donar_proj['email'];
																$paypal_msg1 = sprintf(CRON_YOUR_PAYMENT_PROCESS_VIOLATED_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU,$donar_proj['user_name'],$resArray['error(0).message']);
																$paypal_msg2 = sprintf(CRON_YOUR_PAYMENT_PROCESS_VIOLATED_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU,$donar_proj['user_name'],$resArray['error(0).message']);
																				
																$paypal_msg3 = sprintf(CRON_HELLO_ADMINISTRATOR_APIERROR7_PAYMENT_PROCESS_VIOLATED_DUE_TO_ERRORMESSAGE_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU,$resArray['error(0).message'],$donar_proj['user_name'],$donar_proj['email'],$donar_email);
																paypal_failure_email( $donar_email,$donar_name,$payee_email,$project_id,$project_title,$paypal_msg1,$paypal_msg2,$paypal_msg3)	;
																
												}
												else
												{		$data2= array('payKey' =>	$resArray['payKey']);
														$this->session->set_userdata($data2);
														$case ="";
																
																if(($resArray['paymentExecStatus'] == "COMPLETED" ))
																{
																	$case ="1";
																}
																else if(($actionType== "PAY") && ($resArray['paymentExecStatus'] == "CREATED" ))
																{
																	$case ="2";
																		
																}
																else if(($preapprovalKey!=null ) && ($actionType== "CREATE") && ($resArray['paymentExecStatus'] == "CREATED" ))
																{
																	$case ="3";
																}
																else if(($actionType== "PAY_PRIMARY"))
																{
																	$case ="4";
																		
																}
																else if(($actionType== "CREATE") && ($resArray['paymentExecStatus'] == "CREATED" ))
																{
																	
																	$temp1=API_USERNAME;
																	$temp2=str_replace('_api1.','@',$temp1);
																	
																	if($temp2==$donar_email)
																	{
																		$case ="3";
																	}
																	else{
																		$case ="2";
																	}
																	
																}
																
																else
																{
																	$donar_proj=$this->account_model->GetAllUsers($transd->user_id);	
																	$donar_proj =	$donar_proj[0];
																					
																	$payee_email=$donar_proj['email'];
																	$paypal_msg1 = sprintf(CRON_YOUR_PAYMENT_PROCESS_VIOLATED_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU,$donar_proj['user_name'],$resArray['error(0).message']);
																	$paypal_msg2 = sprintf(CRON_YOUR_PAYMENT_PROCESS_VIOLATED_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU,$donar_proj['user_name'],$resArray['error(0).message']);
																					
																	$paypal_msg3 = sprintf(CRON_HELLO_ADMINISTRATOR_APIERROR7_PAYMENT_PROCESS_VIOLATED_DUE_TO_ERRORMESSAGE_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU,$resArray['error(0).message'],$donar_proj['user_name'],$donar_proj['email'],$donar_email);
																	paypal_failure_email( $donar_email,$donar_name,$payee_email,$project_id,$project_title,$paypal_msg1,$paypal_msg2,$paypal_msg3)	;
																}
															}
												
												
													}
								
								
								catch(Exception $ex) 
								{
																throw new Exception('Error occurred in PayReceipt method');
									
																$donar_proj=$this->account_model->GetAllUsers($transd->user_id);	
																$donar_proj =	$donar_proj[0];
																				
																$payee_email=$donar_proj['email'];
																$paypal_msg1 = sprintf(CRON_YOUR_PAYMENT_PROCESS_VIOLATED_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU,$donar_proj['user_name'],$resArray['error(0).message']);
																$paypal_msg2 = sprintf(CRON_YOUR_PAYMENT_PROCESS_VIOLATED_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU,$donar_proj['user_name'],$resArray['error(0).message']);
																				
																$paypal_msg3 = sprintf(CRON_HELLO_ADMINISTRATOR_APIERROR7_PAYMENT_PROCESS_VIOLATED_DUE_TO_ERRORMESSAGE_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU,$resArray['error(0).message'],$donar_proj['user_name'],$donar_proj['email'],$donar_email);
																paypal_failure_email( $donar_email,$donar_name,$payee_email,$project_id,$project_title,$paypal_msg1,$paypal_msg2,$paypal_msg3)	;
											
								}
									/////////===========for payment redirect user to this link============= 
									if($case=='1')
									{
											
											if(strtoupper($resArray['paymentExecStatus']) == "COMPLETED")
											{
												
												$update_status=$this->db->query("update transaction set `preapproval_status`='SUCCESS' , `preapproval_pay_key`='".$resArray['payKey']."' where `preapproval_key`='".$transd->preapproval_key."'");			$update_project_inactive=$this->db->query("update project set active=0 where project_id='".$transd->project_id."'");
												$donar_proj=$this->account_model->GetAllUsers($transd->user_id);	
												$donar_proj =	$donar_proj[0];
												
												/////////============SUCCESS Email ==============
														
										
														///////////============admin alert==========
															$this->adminalerts($prj,$transd,$donar_proj);
												
												
											}
										
									}
									
									else
									{
																$update_status=$this->db->query("update transaction set `preapproval_status`='FAIL' , `preapproval_pay_key`='' where `preapproval_key`='".$transd->preapproval_key."'");	
																$donar_proj=$this->account_model->GetAllUsers($transd->user_id);	
																$donar_proj =	$donar_proj[0];
																				
																$payee_email=$donar_proj['email'];
																$paypal_msg1 = sprintf(CRON_YOUR_PAYMENT_PROCESS_VIOLATED_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU,$donar_proj['user_name'],$resArray['error(0).message']);
																$paypal_msg2 = sprintf(CRON_YOUR_PAYMENT_PROCESS_VIOLATED_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU,$donar_proj['user_name'],$resArray['error(0).message']);
																				
																$paypal_msg3 = sprintf(CRON_HELLO_ADMINISTRATOR_APIERROR7_PAYMENT_PROCESS_VIOLATED_DUE_TO_ERRORMESSAGE_PLEASE_CHECK_YOUR_SETTINGS_TRY_AGAIN_THANKYOU,$resArray['error(0).message'],$donar_proj['user_name'],$donar_proj['email'],$donar_email);
																paypal_failure_email( $donar_email,$donar_name,$payee_email,$project_id,$project_title,$paypal_msg1,$paypal_msg2,$paypal_msg3)	;
											
										
									}
									
									
									
									
									///redirect($payPalURL);
									
									
							/////////===========for payment redirect user to this link============= 		
								
							
										}  ///=====foreach preapproval

							
								 
								 
									}   ////====if found any key
								
								
							
								//for unsuccess project
								$this->db->query("update project set `active`='4' , `status`='5' where project_id='".$prjs->project_id."'");
}
	
function cron_forgot_password(){
	$site_setting = site_setting();
		$query = $this->db->get_where('user',array('forgot_unique_code !='=>''));
		
		if($query->num_rows()>0)
		{
			
			$result = $query->result();
			
			foreach($result as $row){
				if(strtotime(date("Y-m-d H:i:s")) > strtotime(date("Y-m-d H:i:s",strtotime($row->request_date . ' + '.$site_setting['forget_time_limit'].' hours'))))
				{
				
					$this->db->query("update user set forgot_unique_code='' where forgot_unique_code='".$row->forgot_unique_code."'");
				}
			}	
			
		}
		
}
	


}
?>
