<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


define( "EWAY_CURL_ERROR_OFFSET", 1000 );
define( "EWAY_XML_ERROR_OFFSET", 2000 );
define( "EWAY_TRANSACTION_OK", 0 );
define( "EWAY_TRANSACTION_FAILED", 1 );
define( "EWAY_TRANSACTION_UNKNOWN", 2 );


//*** Parameters For Token Generation ***//
define('API_SERVER', serialize (array ("127.0.0.1","localhost","::1",'mydesichef.com')));
define('API_TOKEN_EXPIRATION', 240);  // In Hours
define('API_TOKEN_KEY', "abcd1234ass"); //==strong key also generate from online
define('API_TOKEN_ALGO', "HS256"); //==JWT encrypt method check in lib

define('API_SECRET_KEY','crwsf'); ///===When you change here you also need to change in mobile app
define('API_SECRET_IV','rotc.fscrw');  ///===When you change here you also need to change in mobile app

/* End of file constants.php */
/* Location: ./application/config/constants.php */