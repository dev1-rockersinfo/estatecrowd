<?php

/**
 * JSON Web Token implementation
 *
 * Minimum implementation used by Realtime auth, based on this spec:
 * http://self-issued.info/docs/draft-jones-json-web-token-01.html.
 *
 * @author Neuman Vong <neuman@twilio.com>
 */
/**
 *  Error Code For this page.
 *  0 Null
 *  1  Maximum stack depth exceeded
 *  2  Underflow or the modes mismatch
 *  3  Unexpected control character found
 *  4  Syntax error, malformed JSON
 *  5  Malformed UTF-8 characters, possibly incorrectly
 *  2011  Unknown JSON error
 *  2012  Empty algorithm
 *  2013  Signature verification failed
 *  2014  Algorithm not supported
 *  2015  Null result with non-null input
 *  2016  Null result with non-null input
 *  2017  Wrong number of segments
 *  2018  Invalid segment encoding
 *  2019  Invalid segment encoding
 */
class JWT
{
    
    public $response_format;
    
    public function __construct($response_type='') {
        
        if($response_type!=''){
            $this->response_format = $response_type;
        }
	
    }
    
    /**
     * @param string      $jwt    The JWT
     * @param string|null $key    The secret key
     * @param bool        $verify Don't skip verification process 
     *
     * @return object The JWT's payload as a PHP object
     */
    public function decode($jwt, $key = null, $verify = true)
    {
        $tks = explode('.', $jwt);
        if (count($tks) != 3) {
           //throw new UnexpectedValueException('Wrong number of segments');
           $success = false;
           $msg = "There is some problem please try again.";
           $code = 2017;
           $response_data['success'] = $success;
           $response_data['message'] = (string) $msg;
           $response_data['error_code'] = $code;
           $this->convert_response_data($response_data);
           
            
        }
        
        list($headb64, $payloadb64, $cryptob64) = $tks;
        if (null === ($header = $this->jsonDecode($this->urlsafeB64Decode($headb64)))
        ) { 
            //throw new UnexpectedValueException('Invalid segment encoding');
           $success = false;
           $msg = "There is some problem please try again.";
           $code = 2018;
           $response_data['success'] = $success;
           $response_data['message'] = (string) $msg;
           $response_data['error_code'] = $code;
           $this->convert_response_data($response_data);
           
        }
        if (null === $payload = $this->jsonDecode($this->urlsafeB64Decode($payloadb64))
        ) {
           //throw new UnexpectedValueException('Invalid segment encoding');
           $success = false;
           $msg = "There is some problem please try again.";
           $code = 2019;
           $response_data['success'] = $success;
           $response_data['message'] = (string) $msg;
           $response_data['error_code'] = $code;
           $this->convert_response_data($response_data);
           
        }
        $sig = $this->urlsafeB64Decode($cryptob64);
        if ($verify) {
            if (empty($header->alg)) {
               // throw new DomainException('Empty algorithm');
               // Empty algo.
               $success = false;
               $msg = "There is some problem please try again.";
               $code = 2012;
               $response_data['success'] = $success;
               $response_data['message'] = (string) $msg;
               $response_data['error_code'] = $code;
               $this->convert_response_data($response_data);
               
            }
            if ($sig != $this->sign("$headb64.$payloadb64", $key, $header->alg)) {
               // throw new UnexpectedValueException('Signature verification failed');
               // Signature verification failed.
               $success = false;
               $msg = "There is some problem please try again.";
               $code = 2013;
               $response_data['success'] = $success;
               $response_data['message'] = (string) $msg;
               $response_data['error_code'] = $code;
               $this->convert_response_data($response_data);
               
            }
        }
        return $payload;
    }

    /**
     * @param object|array $payload PHP object or array
     * @param string       $key     The secret key
     * @param string       $algo    The signing algorithm
     *
     * @return string A JWT
     */
    public function encode($payload, $key, $algo = 'HS256')
    {
        $header = array('typ' => 'JWT', 'alg' => $algo);

        $segments = array();
        $segments[] = $this->urlsafeB64Encode($this->jsonEncode($header));
        $segments[] = $this->urlsafeB64Encode($this->jsonEncode($payload));
        
        $signing_input = implode('.', $segments);
        
        $signature = $this->sign($signing_input, $key, $algo);
        
        $segments[] = $this->urlsafeB64Encode($signature);
        
        return implode('.', $segments);
    }

    /**
     * @param string $msg    The message to sign
     * @param string $key    The secret key
     * @param string $method The signing algorithm
     *
     * @return string An encrypted message
     */
    public function sign($msg, $key, $method = 'HS256')
    {
        $methods = array(
            'HS256' => 'sha256',
            'HS384' => 'sha384',
            'HS512' => 'sha512',
        );
        if (empty($methods[$method])) {
            //throw new DomainException('Algorithm not supported');
            $success = false;
            $msg = "There is some problem please try again.";
            $code = 2014;
            $response_data['success'] = $success;
            $response_data['message'] = (string) $msg;
            $response_data['error_code'] = $code;
            $this->convert_response_data($response_data);
           
        }
        return hash_hmac($methods[$method], $msg, $key, true);
    }

    /**
     * @param string $input JSON string
     *
     * @return object Object representation of JSON string
     */
    public function jsonDecode($input)
    {   
        if (version_compare(PHP_VERSION, '5.4.0', '>=') && !(defined('JSON_C_VERSION') && PHP_INT_SIZE > 4)) {
            /** In PHP >=5.4.0, json_decode() accepts an options parameter, that allows you
             * to specify that large ints (like Steam Transaction IDs) should be treated as
             * strings, rather than the PHP default behaviour of converting them to floats.
             */
            $obj = json_decode($input, false, 512, JSON_BIGINT_AS_STRING);
        } else {
            /** Not all servers will support that, however, so for older versions we must
             * manually detect large ints in the JSON string and quote them (thus converting
             *them to strings) before decoding, hence the preg_replace() call.
             */
            $max_int_length = strlen((string) PHP_INT_MAX) - 1;
            $json_without_bigints = preg_replace('/:\s*(-?\d{'.$max_int_length.',})/', ': "$1"', $input);
            $obj = json_decode($json_without_bigints);
        }
        
        //$obj = json_decode($input);
        if (function_exists('json_last_error') && $errno = json_last_error()) {
            $this->handleJsonError($errno);
        }
        else if ($obj === null && $input !== 'null') {
           //throw new DomainException('Null result with non-null input');
           $success = false;
           $msg = "There is some problem please try again.";
           $code = 2015;
           $response_data['success'] = $success;
           $response_data['message'] = (string) $msg;
           $response_data['error_code'] = $code;
           $this->convert_response_data($response_data);
           
        }
        return $obj;
        
    }

    /**
     * @param object|array $input A PHP object or array
     *
     * @return string JSON representation of the PHP object or array
     */
    public function jsonEncode($input)
    {
        $json = json_encode($input);
        if (function_exists('json_last_error') && $errno = json_last_error()) {
            $this->handleJsonError($errno);
        }
        else if ($json === 'null' && $input !== null) {
          //throw new DomainException('Null result with non-null input');
           $success = false;
           $msg = "There is some problem please try again.";
           $code = 2016;
           $response_data['success'] = $success;
           $response_data['message'] = (string) $msg;
           $response_data['error_code'] = $code;
           $this->convert_response_data($response_data);
           
        }
        return $json;
    }

    /**
     * @param string $input A base64 encoded string
     *
     * @return string A decoded string
     */
    public function urlsafeB64Decode($input)
    {
        $remainder = strlen($input) % 4;
        if ($remainder) {
            $padlen = 4 - $remainder;
            $input .= str_repeat('=', $padlen);
        }
        //echo "<pre>"; print_r(base64_decode(strtr($input, '-_', '+/')));  
        return base64_decode(strtr($input, '-_', '+/'));
    }

    /**
     * @param string $input Anything really
     *
     * @return string The base64 encode of what you passed in
     */
    public function urlsafeB64Encode($input)
    {
        return str_replace('=', '', strtr(base64_encode($input), '+/', '-_'));
    }

    /**
     * @param int $errno An error number from json_last_error()
     *
     * @return void
     */
    public function handleJsonError($errno)
    {
        $messages = array(
            JSON_ERROR_NONE             => null,
            JSON_ERROR_DEPTH            => 'Maximum stack depth exceeded',
            JSON_ERROR_STATE_MISMATCH   => 'Underflow or the modes mismatch',
            JSON_ERROR_CTRL_CHAR        => 'Unexpected control character found',
            JSON_ERROR_SYNTAX           => 'Syntax error, malformed JSON',
            JSON_ERROR_UTF8             => 'Malformed UTF-8 characters, possibly incorrectly' 
        );
        //echo "<pre>"; print_r($messages[$errno]); die;
        if(isset($messages[$errno])) {
          $success = false;
          $msg = "There is some problem please try again.";
          $code = $errno;
          $response_data['success'] = $success;
          $response_data['message'] = (string) $msg;
          $response_data['error_code'] = $code;
         
          $this->convert_response_data($response_data);
          
          
        } else {
          $success = false;
          $msg = 'There is some problem please try again.';
          $code = 2011;
          $response_data['success'] = $success;
          $response_data['message'] = (string) $msg;
          $response_data['error_code'] = $code;
          $this->convert_response_data($response_data);
          
        }
        /*throw new DomainException(
            isset($messages[$errno])
            ?  $messages[$errno]
            : 'Unknown JSON error: ' . $errno
        ); */
    }
    
    public function convert_response_data($dataresponse = array()) {
        $tmpdataresponse = $dataresponse;
        // pr($dataresponse); die;

        
        if ($this->response_format == "json") {
            $dataresponse = json_encode($dataresponse);

            if ($dataresponse == "") {
                $dataresponse = $this->_json_encode($tmpdataresponse);
            }
        }

        $api_status = -1;
        if ($dataresponse != "") {
            $api_status = 1;
        }


        //====api log update status======

        //$info = '';
        //$this->log_updatecall($this->log_id, $api_status, $dataresponse, $info);

        //====api log update status======

        echo $dataresponse;
        die;
    }

// alternative json_encode
    public static function _json_encode($val) {
        if (is_string($val))
            return '"' . addslashes($val) . '"';
        if (is_numeric($val))
            return $val;
        if ($val === null)
            return 'null';
        if ($val === true)
            return 'true';
        if ($val === false)
            return 'false';

        $assoc = false;
        $i = 0;
        foreach ($val as $k => $v) {
            if ($k !== $i++) {
                $assoc = true;
                break;
            }
        }
        $res = array();
        foreach ($val as $k => $v) {
            $v = $this->_json_encode($v);
            if ($assoc) {
                $k = '"' . addslashes($k) . '"';
                $v = $k . ':' . $v;
            }
            $res[] = $v;
        }
        $res = implode(',', $res);
        return ($assoc) ? '{' . $res . '}' : '[' . $res . ']';
    }

}

