<?php
	/**
	 * CodeIgniter Facebook Connect Graph API Library 
	 * 
	 * Author: Graham McCarthy (graham@hitsend.ca) HitSend inc. (http://hitsend.ca)
	 * 
	 * VERSION: 1.0 (2010-09-30)
	 * LICENSE: GNU GENERAL PUBLIC LICENSE - Version 2, June 1991
	 * 
	 **/
include(APPPATH.'libraries/facebook/autoload.php');
	
// added in v4.0.0

use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;
use Facebook\Entities\AccessToken;
use Facebook\HttpClients\FacebookCurlHttpClient;
use Facebook\HttpClients\FacebookHttpable;


	class Fb_connect {
		//declare private variables
		private $_obj;
		private $_api_key		= NULL;
		private $_secret_key	= NULL;
		
		//declare public variables
		public 	$user 			= NULL;
		public 	$user_id 		= FALSE;
		public 	$friends 		= NULL;
		
		public $fbLoginURL 	= "";
		public $fbaccesstoken 	= "";
		public $fbLogoutURL = "";
		
		public $fb 			= FALSE;
		public $fbSession	= FALSE;
		public $appkey		= 0;
		
		//constructor method.
		function __construct()
		{
			//Using the CodeIgniter object, rather than creating a copy of it
			$this->_obj =& get_instance();
			
			//loading the config paramters for facebook (where we stored our Facebook API and SECRET keys
			$this->_obj->load->config('facebook');
			//make sure the session library is initiated. may have already done this in another method.
			$this->_obj->load->library('session'); 
			
			$this->_api_key		= $this->_obj->config->item('facebook_api_key');
			$this->_secret_key	= $this->_obj->config->item('facebook_secret_key');

			$this->appkey = $this->_api_key; 
		
			//connect to facebook
			// init app with app id and secret
			FacebookSession::setDefaultApplication($this->_api_key,$this->_secret_key);
				
			// login helper with redirect_uri
				$redirect_url= site_url("home/facebook/");
				$helper = new FacebookRedirectLoginHelper($redirect_url );
//print_r($helper);
			try {
			  $session = $helper->getSessionFromRedirect();//var_dump($session);
			} catch( FacebookRequestException $ex ) {
			  // When Facebook returns an error
			  error_log($ex);var_dump($ex);
			 
			} catch( Exception $ex ) {
			  // When validation fails or other local issues
			  error_log($ex);
			  
			}
			//die;
			
			// If a valid fbSession is returned, try to get the user information contained within.
			if ($session) {//var_dump($this->fbSession);
			
					//store the return session from facebook
					$this->fbSession  =$session; 
					
					$request = new FacebookRequest( $session, 'GET', '/me' );
					  $response = $request->execute();
					  // get response
					  $graphObject = $response->getGraphObject();
					 
					$fbid = $graphObject->getProperty('id');  
					$name = $graphObject->getProperty('name'); 
					$arr=explode(" ", $name);            // To Get Facebook ID
					$first_name = $graphObject->getProperty('first_name');  // To Get Facebook Username
					$last_name = $graphObject->getProperty('last_name'); // To Get Facebook full name
					if(isset($arr[0]))	$first_name =$arr[0];
					if(isset($arr[1]))	$last_name =$arr[1];

					$request = new FacebookRequest( $session, 'GET', '/me', array(
											 'fields' => 'email'
										) );
					  $response = $request->execute();
					  // get response
					  $graphObject = $response->getGraphObject();


					$femail = $graphObject->getProperty('email');
					
					$this->fbSession  =$user_data['fbid'] = $fbid;           
					$user_data['first_name'] = $first_name;
					$user_data['last_name'] = $last_name;
					$user_data['email'] =  $femail;	

					$me = null;
				try {
					// graph api request for user data
					$this->user = $user_data;
					$this->user_id = $fbid;
					  $request = new FacebookRequest( $session, 'GET', '/me', array(
											 'fields' => 'picture',
											 'type' => 'large'
										) );
					  $response = $request->execute();
					  // get response
					  $graphObject = $response->getGraphObject();
					  $fbid = $graphObject->getProperty('picture');
					  $pic_url=$fbid->getProperty('url');
					if(isset($pic_url))$this->user["picture"]=$pic_url;
			    	//var_dump($me);die();
			    	
			    	
			  	} catch (FacebookApiException $e) {//var_dump($e);die();
			    	error_log($e);
			  	}
			}			
			
            $arra_scope=array('email','public_profile','user_friends');
            $this->fbLoginURL = $helper->getLoginUrl($arra_scope)    ;
			// login or logout url will be needed depending on current user state.
			//(if using the javascript api as well, you may not need these.)
			//var_dump($this->fbLoginURL);
			//	var_dump($this->fbLogoutURL);
			if ($session) {
				$this->fbLogoutURL =$helper->getLogoutUrl($session,site_url("home/logout"))    ;
			} 
			//var_dump($this->fbLoginURL);exit;			
		} //end Fb_connect() function
		
		function publish($user,$str=array())
		{
			/*	$this->fb = new Facebook(array(
						  'appId'  => $this->_api_key,
						  'secret' => $this->_secret_key,
						  'cookie' => true,
						));*/
				 //$user = $this->fb->getUser(); 
				 $publishStream =  $this->fb->api("/$user/feed", 'post',$str);
		}
		
		/*function myloginurl($function){
			$login =  $this->fb->getLoginUrl(
					array(
						'scope'         => 'public_profile,email,user_friends',
						'redirect_uri'  =>  site_url($function)
					)
			);
			return $login;
		}*/
		
		
		function mylogouturl($function){
	 			
			$logout =	$this->fbLogoutURL = $this->fb->getMyLogoutUrl($function);
			return $logout;	
  		}
	
	
	
		
	} // end class
