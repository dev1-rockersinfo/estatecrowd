<?php

/**
 * Created by PhpStorm.
 * User: rocku28
 * Date: 30/6/16
 * Time: 1:57 PM
 */
class Corelogiclib{

    private $access_token_url="https://access-api.corelogic.asia/";

    private $property_url="https://property-sandbox-api.corelogic.asia/";

    private $search_url="https://search-sandbox-api.corelogic.asia/";

    public function __construct(){

    }

    function create_access_token($params){
        $params = http_build_query( $params, NULL, '&' );
        return $this->send_request($this->access_token_url.'access/oauth/token?'.$params);
    }

    function search_peroperties_by_lat_long($token,$lat,$lon){

        $params_p = array(
            'lon'=>$lon,
            'lat'=>$lat,
            'access_token'=>$token
        );
        $params = http_build_query( $params_p, NULL, '&' );
        return $this->send_request($this->property_url.'bsg-au/v2/search/point?'.$params);
    }

    function get_properties_suggestion($token,$country_id,$limit){
        $params_p = array(
            'q'=>$country_id,
            'limit'=>$limit,
            'access_token'=>$token
        );
        $params = http_build_query( $params_p, NULL, '&' );
        return $this->send_request($this->property_url.'bsg-au/v1/suggest.json?'.$params);
    }

    function get_properties_by_postcode_id($pass_array,$postcode){
        $params = http_build_query( $pass_array, NULL, '&' );
        return $this->send_request($this->search_url.'search/au/property/postcode/'.$postcode.'?'.$params);
    }

    function get_property_from_locality($token,$locationType,$locationId){
        $params_p = array(
            'locationType'=>$locationType,
            'locationId'=>$locationId,
            'access_token'=>$token
        );
        $params = http_build_query( $params_p, NULL, '&' );
        return $this->send_request($this->property_url.'bsg-au/v1/search.json?'.$params);
    }

    function get_properties_data($token,$propertyId,$returnFields){
        $params_p = array(
            'propertyId'=>$propertyId,
            'returnFields'=>$returnFields,
            'access_token'=>$token
        );
       
        $params = http_build_query( $params_p, NULL, '&' );
        return $this->send_request($this->property_url.'bsg-au/v1/property/'.$propertyId.'.json?'.$params);
    }

    function send_request($url,$method=''){
        // create curl resource
        $ch = curl_init();

        // set url
        curl_setopt($ch, CURLOPT_URL, $url);


        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // $output contains the output string
        $output = curl_exec($ch);

        // close curl resource to free up system resources
        curl_close($ch);

        return $output;
    }
}