<?php

/**
 * Created by PhpStorm.
 * User: rocku28
 * Date: 30/6/16
 * Time: 1:57 PM
 */
class Salesforce_api{

  public function __construct(){
      //fetch settings from DB
      $CI =& get_instance();       
      $query = $CI->db->query("select * from salesforce_settings");
      $this->data = $query->row_array();
         
  }

  function get_token(){
    // check wether already generated or not
    // if($this->data['access_token']!=null){
    //     return $this->data;
    // }

    $url = 'https://cs6.salesforce.com/services/oauth2/token';
    $method = "POST";

    $postData = array(
      "grant_type" =>$this->data['grant_type'],
      "client_id" =>$this->data['client_id'],
      "client_secret" =>$this->data['client_secret'],
      "redirect_uri" =>$this->data['redirect_uri'],
      "username" =>$this->data['username'],
      "password" =>$this->data['password'],
    );

    $response = $this->send_request($url,$method,$postData); 

    $response = json_decode($response);

    $CI =& get_instance();

    if(isset($response->access_token) && $response->access_token!=null){
      $data_array = array(
        'access_token' => $response->access_token,
        'instance_url' => $response->instance_url,
        'id' => $response->id,
        'token_type' => $response->token_type,
        'issued_at' => $response->issued_at,
        'signature' => $response->signature,
      );

      $query = $CI->db->update("salesforce_settings",$data_array); //update token and give respose

      $query = $CI->db->query("select * from salesforce_settings");

      return $query->row_array();

    } else {
      $data_array = array(
        'access_token' => NULL,
        'instance_url' => NULL,
        'id' => NULL,
        'token_type' => NULL,
        'issued_at' => NULL,
        'signature' => NULL,
      );

      $query = $CI->db->update("salesforce_settings",$data_array); //null update token

      return false;
    }
  }


  // create new user in salesforce

  function create_new_user($userdata){
    $url = 'https://cs6.salesforce.com/services/apexrest/CoVestaREST/';
    $method = "POST";

    // if($this->data['access_token']!=null){
    //   $token = $this->data['access_token'];
    // } else {
      $this->data = $this->get_token();
      $token = $this->data['access_token'];
    // }

    $postData["operation"] = "Insert";
    $postData["userDetails"] = $userdata;

    $postData = json_encode($postData);
   
    $response = $this->send_request($url,$method,$postData,1);
    $response = json_decode($response);
    if(!is_null($response->salesforceUserId)){
      return $response->salesforceUserId;
    } else {
      return null;
    }
  }

  // update existing user in salesforce

  function update_existing_user($userdata){
    $url = 'https://cs6.salesforce.com/services/apexrest/CoVestaREST/';
    $method = "POST";

    // if($this->data['access_token']!=null){
    //   $token = $this->data['access_token'];
    // } else {
      $this->data = $this->get_token();
      $token = $this->data['access_token'];
    // }

    $postData["operation"] = "Update";
    $postData["userDetails"] = $userdata;

    $postData = json_encode($postData);

   
    $response = $this->send_request($url,$method,$postData,1);
    $response = json_decode($response);

    // print_r($response);
    // die;

    if(!is_null($response->salesforceUserId)){
      return $response->salesforceUserId;
    } else {
      return null;
    }
  }

  // Campaign Add call to Sales force

  function create_new_syndicate($campaigndetails){
    $url = 'https://cs6.salesforce.com/services/apexrest/CoVestaRESTSyndicate/';
    $method = "POST";

    // if($this->data['access_token']!=null){
    //   $token = $this->data['access_token'];
    // } else {
      $token_arr = $this->get_token();
      $token = $token_arr['access_token'];
    // }

    $postData["operation"] = "Insert";
    $postData["synDetails"] = $campaigndetails;

    $postData = json_encode($postData);
    $response = $this->send_request($url,$method,$postData,1);
    $response = json_decode($response);

    if(!is_null($response->salesforceSynId)){
      return $response->salesforceSynId;
    } else {
      return null;
    }
  }

   // Investment Add call to Sales force

  function create_new_investment($invdetails){
    $url = 'https://cs6.salesforce.com/services/apexrest/CoVestaRESTInvestment/';
    $method = "POST";

    // if($this->data['access_token']!=null){
    //   $token = $this->data['access_token'];
    // } else {
      $token_arr = $this->get_token();
      $token = $token_arr['access_token'];
    // }

    $postData["operation"] = "Insert";
    $postData["invDetails"] = $invdetails;

    $postData = json_encode($postData);
    $response = $this->send_request($url,$method,$postData,1);
    $response = json_decode($response);

    if(!is_null($response->salesforceInvId)){
      return $response->salesforceInvId;
    } else {
      return null;
    }
  }


  function send_request($url,$method='',$postData,$isheader=0){
    

    // create curl resource
    $ch = curl_init();

    // set url
    curl_setopt($ch, CURLOPT_URL, $url);

    // set header if require
    if($isheader==1){
      $header_array = array(
        'Content-Type: application/json',
        'authorization: '.$this->data['token_type'].' '.$this->data['access_token']
        );
      curl_setopt($ch, CURLOPT_HTTPHEADER, $header_array);
    }

    // curl_setopt($ch, CURLOPT_POST, count($postData));
    if($method=="POST"){
      curl_setopt($ch, CURLOPT_POSTFIELDS, $postData); 
    }


    //return the transfer as a string
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    // $output contains the output string
    $output = curl_exec($ch);

    // close curl resource to free up system resources
    curl_close($ch);

    return $output;
  }
}