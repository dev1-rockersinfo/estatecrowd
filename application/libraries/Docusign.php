<?php

/**
 * Created by PhpStorm.
 * User: rocku28
 * Date: 5/7/16
 * Time: 5:01 PM
 */
require_once APPPATH.'third_party/docusign-php-client/autoload.php';

class Docusign
{
    private $username;

    private $password;

    private $integrator_key;

    private $host;

    private $header_rest;

    public function __construct($params)
    {
        $this->username = $params['username'];
        $this->password = $params['password'];
        $this->integrator_key = $params['integrator_key'];
        $this->host = $params['host'];
        $this->header_rest = "<DocuSignCredentials><Username>" . $this->username . "</Username><Password>" . $this->password . "</Password><IntegratorKey>" . $this->integrator_key . "</IntegratorKey></DocuSignCredentials>";
    }

    function initialize(){
        $config = new DocuSign\eSign\Configuration();
        $config->setHost($this->host);
        $config->addDefaultHeader("X-DocuSign-Authentication",
            "{\"Username\":\"" . $this->username  . "\",
            \"Password\":\"" . $this->password . "\",
            \"IntegratorKey\":\"" . $this->integrator_key . "\"}");

        // instantiate a new docusign api client
        return $apiClient = new DocuSign\eSign\ApiClient($config);
    }
    
    function login($apiClient){

        // we will first make the Login() call which exists in the AuthenticationApi...
        $authenticationApi = new DocuSign\eSign\Api\AuthenticationApi($apiClient);

        // optional login parameters
        $options = new \DocuSign\eSign\Api\AuthenticationApi\LoginOptions();

        // call the login() API
        $loginInformation = $authenticationApi->login($options);
        return $loginInformation;
    }

    function send_envelope($apiClient,$recipientName,$recipientEmail,$documentFileName,$documentName,$accountId){
        // instantiate a new envelopeApi object
        $envelopeApi = new DocuSign\eSign\Api\EnvelopesApi($apiClient);

        // Add a document to the envelope
        $document = new DocuSign\eSign\Model\Document();
        $document->setDocumentBase64(base64_encode(file_get_contents($documentFileName)));
        $document->setName($documentName);
        $document->setDocumentId("1");

        // Create a |SignHere| tab somewhere on the document for the recipient to sign
        $signHere = new \DocuSign\eSign\Model\SignHere();
        $signHere->setXPosition("300");
        $signHere->setYPosition("280");
        $signHere->setDocumentId("1");
        $signHere->setPageNumber("2");
        $signHere->setRecipientId("1");

        // add the signature tab to the envelope's list of tabs
        $tabs = new DocuSign\eSign\Model\Tabs();
        $tabs->setSignHereTabs(array($signHere));

        // add a signer to the envelope
        $signer = new \DocuSign\eSign\Model\Signer();
        $signer->setEmail($recipientEmail);
        $signer->setName($recipientName);
        $signer->setRecipientId("1");
        $signer->setTabs($tabs);
        $signer->setClientUserId("1234");  // must set this to embed the recipient!

        // Add a recipient to sign the document
        $recipients = new DocuSign\eSign\Model\Recipients();
        $recipients->setSigners(array($signer));
        $envelop_definition = new DocuSign\eSign\Model\EnvelopeDefinition();
        $envelop_definition->setEmailSubject("[DocuSign PHP SDK] - Please sign this doc");

        // set envelope status to "sent" to immediately send the signature request
        $envelop_definition->setStatus("sent");
        $envelop_definition->setRecipients($recipients);
        $envelop_definition->setDocuments(array($document));

        // create and send the envelope! (aka signature request)
        return $envelop_summary = $envelopeApi->createEnvelope($accountId, $envelop_definition, null);
    }

    function sign_url($apiClient,$recipientName,$recipientEmail,$accountId,$envelopeId){

        // instantiate a new envelopeApi object
        $envelopeApi = new DocuSign\eSign\Api\EnvelopesApi($apiClient);
        // instantiate a RecipientViewRequest object
        $recipient_view_request = new \DocuSign\eSign\Model\RecipientViewRequest();
        // set where the recipient is re-directed once they are done signing
        $recipient_view_request->setReturnUrl(site_url('investment/signed/'.$accountId.'/'.$envelopeId));
        // configure the embedded signer
        $recipient_view_request->setUserName($recipientName);
        $recipient_view_request->setEmail($recipientEmail);
        // must reference the same clientUserId that was set for the recipient when they
        // were added to the envelope in step 2
        $recipient_view_request->setClientUserId("1234");
        // used to indicate on the certificate of completion how the user authenticated
        $recipient_view_request->setAuthenticationMethod("email");
        // generate the recipient view! (aka embedded signing URL)
        return $signingView = $envelopeApi->createRecipientView($accountId, $envelopeId, $recipient_view_request);
    }

    function list_documents_and_download($apiClient,$accountId,$envelopeId,$baseUrl)
    {
        $envelopeApi = new DocuSign\eSign\Api\EnvelopesApi($apiClient);
        $docsList = $envelopeApi->listDocuments($accountId, $envelopeId);
        $docCount = count($docsList->getEnvelopeDocuments());

        if (intval($docCount) > 0)
        {
            $document_name = array();
            foreach($docsList->getEnvelopeDocuments() as $document)
            {

                // echo $document.'<br>';
                $docs = json_decode($document);
                $docUri = $docs->uri;
                $name = $docs->name;

                $header = $this->header_rest;

                $curl = curl_init($baseUrl . $docUri );
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_BINARYTRANSFER, true);  
                curl_setopt($curl, CURLOPT_HTTPHEADER, array(                                                                          
                    "X-DocuSign-Authentication: $header" )                                                                       
                );
                
                $data = curl_exec($curl);
                $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                if ( $status != 200 ) {
                    echo "error calling webservice, status is:" . $status;
                    exit(-1);
                }
                $document_name[] = $name.'-'.$envelopeId.'.pdf';

                file_put_contents(base_path() . 'upload/doc/docusign/'.$name.'-'.$envelopeId.'.pdf',$data);
                curl_close($curl);
            }
            return $document_name;
        }
    }
}