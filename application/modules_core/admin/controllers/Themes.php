<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Themes extends ROCKERS_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('home_model');
    }

    function index($update_msg = '')

    {

        $posts['update_msg'] = '';
        if ($_POST) {

            //error_reporting(E_ALL);
            $posts = array();
            $posts["color_orange"] = $this->input->post('color_orange');
            $posts["color_darkorange"] = $this->input->post('color_darkorange');
            $posts["color_grey"] = $this->input->post('color_grey');
            $posts["color_black"] = $this->input->post('color_black');
            $posts["color_white"] = $this->input->post('color_white');
            $posts["color_lightgrey"] = $this->input->post('color_lightgrey');
            $posts["color_blue"] = $this->input->post('color_blue');
            $posts["color_darkblue"] = $this->input->post('color_darkblue');
            $posts["color_ccc"] = $this->input->post('color_ccc');
            $posts["style_font1"] = $this->input->post('style_font1');
            $posts["style_font2"] = $this->input->post('style_font2');
            //print_r($posts);
            // $response['posts'] = $posts;
            $base_path = $this->config->slash_item('base_path');
            $link1 = $base_path . 'css/';
            $fp = fopen($link1 . 'theme.json', 'w');
            fwrite($fp, json_encode($posts));
            fclose($fp);

            $data = file_get_contents($link1 . 'theme.json');
            $json = json_decode($data, true);

            $this->db->where('id', 1);
            $this->db->update('themes', $posts);
            $this->db->limit(1);

            /* $this->db->where('default_value', 1);
             $this->db->update('themes', $posts1);
             $this->db->limit(1);*/
            $posts['update_msg'] = 'Success!Themes has been updated successfully..';

            $default = false;

        } else {
            $query = $this->db->query("SELECT * FROM `themes` WHERE default_value = 0");
            $query->result_array();

            //fetch data from table...
            $posts = array();
            $posts["color_orange"] = '#f37621';
            $posts["color_darkorange"] = '#e56712';
            $posts["color_grey"] = '#4c4c4c';
            $posts["color_black"] = '#323232';
            $posts["color_white"] = '#ffffff';
            $posts["color_lightgrey"] = '#efefef';
            $posts["color_blue"] = '#3bb0d2';
            $posts["color_darkblue"] = '#32869c';
            $posts["color_ccc"] = '#cccccc';
            $posts["style_font1"] = $this->input->post('style_font1');
            $posts["style_font2"] = $this->input->post('style_font2');
        }
        //$data =$posts1;
        $data = $posts;
        $data['site_setting'] = site_setting();

        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Themes', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'themes/add_themes', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }

    function set_default($update_msg = '')
    {
        $posts['update_msg'] = '';
        $posts = array();
        $posts["color_orange"] = '#f37621';
        $posts["color_darkorange"] = '#e56712';
        $posts["color_grey"] = '#4c4c4c';
        $posts["color_black"] = '#323232';
        $posts["color_white"] = '#ffffff';
        $posts["color_lightgrey"] = '#efefef';
        $posts["color_blue"] = '#3bb0d2';
        $posts["color_darkblue"] = '#32869c';
        $posts["color_ccc"] = '#cccccc';
        $posts["style_font1"] = "Arial, Helvetica, sans-serif";
        $posts["style_font2"] = "Arial, Helvetica, sans-serif";

        $base_path = $this->config->slash_item('base_path');
        $link1 = $base_path . 'css/';
        $fp = fopen($link1 . 'theme.json', 'w');
        fwrite($fp, json_encode($posts));
        fclose($fp);

        $data = file_get_contents($link1 . 'theme.json');
        $json = json_decode($data, true);

        $this->db->where('default_value', 1);
        $this->db->update('themes', $posts);
        $this->db->limit(1);

        $posts['update_msg'] = SUCCESS_THEMES_HAS_BEEN_UPDATED_SUCCESSFULLY;

        $default = false;

        $data = $posts;
        $data['site_setting'] = site_setting();

        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Themes', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'themes/add_themes', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();

    }


}

?>
