<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Social_setting extends ROCKERS_Controller
{


    function __construct()

    {

        parent::__construct();


        $this->load->model('home_model');

        $this->load->library('fb_connect');

    }

    /*** facebook setting home page
     **/


    function index()

    {

        redirect('admin/social_setting/add_facebook_setting');

    }


    /*

     Function name :add_facebook_setting()

    Parameter : none.

    Return : none

    Use : this function is used to update facebook settings.

    */

    function add_facebook_setting($msg = '')


    {

        //echo "<br>msg:".$msg;
        if ($msg == 'true')

            $data['message'] = $msg;

        else {

            $data['message'] = '';

        }

        $check_rights = get_rights('add_facebook_setting');


        if ($check_rights == 0) {


            redirect('home/dashboard/no_rights');


        }


        $data['site_setting'] = site_setting();

        $this->load->library('form_validation');


        $this->form_validation->set_rules('facebook_application_id', FACEBOOK_APPLICATION_ID, 'required');


        if ($this->form_validation->run() == FALSE) {


            if (validation_errors()) {


                $data["error"] = validation_errors();


            } else {


                $data["error"] = "";


            }


            if ($this->input->post('facebook_setting_id')) {


                $data["facebook_setting_id"] = $this->input->post('facebook_setting_id');


                $data["facebook_application_id"] = $this->input->post('facebook_application_id');


                $data["facebook_login_enable"] = $this->input->post('facebook_login_enable');


                $data["facebook_access_token"] = $this->input->post('facebook_access_token');


                $data["facebook_api_key"] = $this->input->post('facebook_api_key');


                $data["facebook_user_id"] = $this->input->post('facebook_user_id');


                $data["facebook_secret_key"] = $this->input->post('facebook_secret_key');


                $data["facebook_user_autopost"] = $this->input->post('facebook_user_autopost');


                $data["facebook_wall_post"] = $this->input->post('facebook_wall_post');


                $data["facebook_url"] = $this->input->post('facebook_url');


                $data["fb_img"] = $this->input->post('fb_img');


            } else {


                $one_facebook_setting = facebook_setting();


                $data["facebook_setting_id"] = $one_facebook_setting->facebook_setting_id;


                $data["facebook_application_id"] = $one_facebook_setting->facebook_application_id;


                $data["facebook_login_enable"] = $one_facebook_setting->facebook_login_enable;


                $data["facebook_access_token"] = $one_facebook_setting->facebook_access_token;


                $data["facebook_api_key"] = $one_facebook_setting->facebook_api_key;


                $data["facebook_user_id"] = $one_facebook_setting->facebook_user_id;


                $data["facebook_secret_key"] = $one_facebook_setting->facebook_secret_key;


                $data["facebook_user_autopost"] = $one_facebook_setting->facebook_user_autopost;


                $data["facebook_wall_post"] = $one_facebook_setting->facebook_wall_post;


                $data["facebook_url"] = $one_facebook_setting->facebook_url;


                $data["fb_img"] = $one_facebook_setting->fb_img;

                if ($msg == 'true') {

                    $data['message'] = $msg;

                    $fb_uid = $this->fb_connect->fbSession;
                    //var_dump($fb_uid);
                    $fb_usr = $this->fb_connect->user;

                    //var_dump($fb_usr);

                    $fb_fbaccesstoken = $this->fb_connect->fbaccesstoken;
                    $data["facebook_access_token"] = $fb_fbaccesstoken;
                    $data["facebook_user_id"] = $fb_uid;
                }


            }


        } else {


            $data1 = array(

                'facebook_application_id' => $this->input->post('facebook_application_id'),

                'facebook_login_enable' => $this->input->post('facebook_login_enable'),

                /*'facebook_access_token' => $this->input->post('facebook_access_token'),*/

                'facebook_api_key' => $this->input->post('facebook_application_id'),

                /*'facebook_user_id' => $this->input->post('facebook_user_id'),*/

                'facebook_secret_key' => $this->input->post('facebook_secret_key'),

                'facebook_user_autopost' => $this->input->post('facebook_user_autopost'),

                'facebook_wall_post' => $this->input->post('facebook_wall_post'),

                'facebook_url' => $this->input->post('facebook_url'),

            );


            $this->home_model->table_update('facebook_setting_id', $this->input->post('facebook_setting_id'), 'facebook_setting', $data1);


            setting_deletecache('facebook_setting');
            setting_new_cache('facebook_setting', 'facebook_setting');


            $data["error"] = "success";


            $data["facebook_setting_id"] = $this->input->post('facebook_setting_id');


            $data["facebook_application_id"] = $this->input->post('facebook_application_id');


            $data["facebook_login_enable"] = $this->input->post('facebook_login_enable');


            $data["facebook_access_token"] = $this->input->post('facebook_access_token');


            $data["facebook_api_key"] = $this->input->post('facebook_api_key');


            $data["facebook_user_id"] = $this->input->post('facebook_user_id');


            $data["facebook_secret_key"] = $this->input->post('facebook_secret_key');


            $data["facebook_user_autopost"] = $this->input->post('facebook_user_autopost');


            $data["facebook_wall_post"] = $this->input->post('facebook_wall_post');


            $data["facebook_url"] = $this->input->post('facebook_url');


            $data["fb_img"] = $this->input->post('fb_img');


        }

        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');


        $this->template->write('title', 'Administrator', '', TRUE);

        $this->template->write_view('header', 'header', $data, TRUE);

        $this->template->write_view('main_content', 'setting/add_facebook_setting', $data, TRUE);

        $this->template->write_view('footer', 'footer', '', TRUE);

        $this->template->render();

    }


    /*

     Function name :add_twitter_setting()

    Parameter : none.

    Return : none

    Use : this function is used to update twitter settings.

    */

    function add_twitter_setting($msg = '')


    {

        if ($msg == 'true')

            $data['message'] = $msg;

        else {

            $data['message'] = '';

        }

        $check_rights = get_rights('add_twitter_setting');


        if ($check_rights == 0) {


            redirect('admin/home/dashboard/no_rights');


        }


        $data['site_setting'] = site_setting();


        $this->load->library('form_validation');


        $this->form_validation->set_rules('consumer_key', CONSUMER_KEYS, 'required');


        if ($this->form_validation->run() == FALSE) {


            if (validation_errors()) {


                $data["error"] = validation_errors();


            } else {


                $data["error"] = "";


            }


            if ($this->input->post('twitter_setting_id')) {


                $data["twitter_setting_id"] = $this->input->post('twitter_setting_id');


                $data["twitter_enable"] = $this->input->post('twitter_enable');


                $data["twitter_user_name"] = $this->input->post('twitter_user_name');


                $data["consumer_key"] = $this->input->post('consumer_key');


                $data["consumer_secret"] = $this->input->post('consumer_secret');


                $data["tw_oauth_token"] = $this->input->post('tw_oauth_token');


                $data["tw_oauth_token_secret"] = $this->input->post('tw_oauth_token_secret');


                $data["autopost_site"] = $this->input->post('autopost_site');


                $data["autopost_user"] = $this->input->post('autopost_user');


                $data["twitter_url"] = $this->input->post('twitter_url');


                $data["twiter_img"] = $this->input->post('twiter_img');


            } else {

                $one_twitter_setting = twitter_setting();


                $data["twitter_setting_id"] = $one_twitter_setting['twitter_setting_id'];


                $data["twitter_enable"] = $one_twitter_setting['twitter_enable'];


                $data["twitter_user_name"] = $one_twitter_setting['twitter_user_name'];


                $data["consumer_key"] = $one_twitter_setting['consumer_key'];


                $data["consumer_secret"] = $one_twitter_setting['consumer_secret'];


                $data["tw_oauth_token"] = $one_twitter_setting['tw_oauth_token'];


                $data["tw_oauth_token_secret"] = $one_twitter_setting['tw_oauth_token_secret'];


                $data["autopost_site"] = $one_twitter_setting['autopost_site'];


                $data["autopost_user"] = $one_twitter_setting['autopost_user'];


                $data["twitter_url"] = $one_twitter_setting['twitter_url'];


                $data["twiter_img"] = $one_twitter_setting['twiter_img'];


            }


        } else {


            $data1 = array(

                'twitter_enable' => $this->input->post('twitter_enable'),

                'twitter_user_name' => $this->input->post('twitter_user_name'),

                'consumer_key' => $this->input->post('consumer_key'),

                'consumer_secret' => $this->input->post('consumer_secret'),

                'tw_oauth_token' => $this->input->post('tw_oauth_token'),

                'tw_oauth_token_secret' => $this->input->post('tw_oauth_token_secret'),

                'autopost_site' => $this->input->post('autopost_site'),

                'autopost_user' => $this->input->post('autopost_user'),

                'twitter_url' => $this->input->post('twitter_url'),

            );


            $this->home_model->table_update('twitter_setting_id', $this->input->post('twitter_setting_id'), 'twitter_setting', $data1);


            //deletecache("twitter_setting");

            setting_deletecache('twitter_setting');
            setting_new_cache('twitter_setting', 'twitter_setting');

            $data["error"] = "success";


            $data["twitter_setting_id"] = $this->input->post('twitter_setting_id');


            $data["twitter_enable"] = $this->input->post('twitter_enable');


            $data["twitter_user_name"] = $this->input->post('twitter_user_name');


            $data["consumer_key"] = $this->input->post('consumer_key');


            $data["consumer_secret"] = $this->input->post('consumer_secret');


            $data["tw_oauth_token"] = $this->input->post('tw_oauth_token');


            $data["tw_oauth_token_secret"] = $this->input->post('tw_oauth_token_secret');


            $data["autopost_site"] = $this->input->post('autopost_site');


            $data["autopost_user"] = $this->input->post('autopost_user');


            $data["twitter_url"] = $this->input->post('twitter_url');


            $data["twiter_img"] = $this->input->post('twiter_img');


        }

        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');


        $this->template->write('title', 'Administrator', '', TRUE);

        $this->template->write_view('header', 'header', $data, TRUE);

        $this->template->write_view('main_content', 'setting/add_twitter_setting', $data, TRUE);

        $this->template->write_view('footer', 'footer', '', TRUE);

        $this->template->render();


    }


    /*

     Function name :add_google_setting()

    Parameter : none.

    Return : none

    Use : this function is used to update google settings.

    */

    function add_google_setting()


    {


        $data = array();


        $check_rights = get_rights('add_google_setting');


        if ($check_rights == 0) {


            redirect('admin/home/dashboard/no_rights');


        }

        $data['site_setting'] = site_setting();


        $this->load->library('form_validation');


        $this->form_validation->set_rules('consumer_key', CONSUMER_KEYS, 'required');


        $this->form_validation->set_rules('consumer_secret', CONSUMER_SECRETS, 'required');


        if ($this->form_validation->run() == FALSE) {


            if (validation_errors()) {


                $data["error"] = validation_errors();


            } else {


                $data["error"] = "";


            }


            if ($this->input->post('google_setting_id')) {


                $data["google_setting_id"] = $this->input->post('google_setting_id');


                $data["consumer_key"] = $this->input->post('consumer_key');


                $data["consumer_secret"] = $this->input->post('consumer_secret');


                $data["google_enable"] = $this->input->post('google_enable');


            } else {


                $one_google_setting = google_setting();


                $data["google_setting_id"] = $one_google_setting->google_setting_id;


                $data["consumer_key"] = $one_google_setting->consumer_key;


                $data["consumer_secret"] = $one_google_setting->consumer_secret;


                $data["google_enable"] = $one_google_setting->google_enable;

            }

        } else {


            $data1 = array(

                'consumer_key' => $this->input->post('consumer_key'),

                'consumer_secret' => $this->input->post('consumer_secret'),

                'google_enable' => $this->input->post('google_enable'),

            );

            $this->home_model->table_update('google_setting_id', $this->input->post('google_setting_id'), 'google_setting', $data1);


            setting_deletecache('google_setting');
            setting_new_cache('google_setting', 'google_setting');


            $data["error"] = "success";


            $data["google_setting_id"] = $this->input->post('google_setting_id');


            $data["consumer_key"] = $this->input->post('consumer_key');


            $data["consumer_secret"] = $this->input->post('consumer_secret');


            $data["google_enable"] = $this->input->post('google_enable');


        }


        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');

        $this->template->write('title', 'Administrator', '', TRUE);

        $this->template->write_view('header', 'header', $data, TRUE);

        $this->template->write_view('main_content', 'setting/add_google_setting', $data, TRUE);

        $this->template->write_view('footer', 'footer', '', TRUE);

        $this->template->render();


    }


    /*

     Function name :add_yahoo_setting()

    Parameter : none.

    Return : none

    Use : this function is used to update yahoo settings.

    */

    function add_yahoo_setting()


    {


        $data = array();


        $check_rights = get_rights('add_yahoo_setting');


        if ($check_rights == 0) {


            redirect('admin/home/dashboard/no_rights');


        }


        $data['site_setting'] = site_setting();


        $this->load->library('form_validation');


        $this->form_validation->set_rules('consumer_key', CONSUMER_KEYS, 'required');


        $this->form_validation->set_rules('consumer_secret', CONSUMER_SECRETS, 'required');


        if ($this->form_validation->run() == FALSE) {


            if (validation_errors()) {


                $data["error"] = validation_errors();


            } else {


                $data["error"] = "";


            }


            if ($this->input->post('yahoo_setting_id')) {

                $data["yahoo_setting_id"] = $this->input->post('yahoo_setting_id');


                $data["consumer_key"] = $this->input->post('consumer_key');


                $data["consumer_secret"] = $this->input->post('consumer_secret');


                $data["yahoo_enable"] = $this->input->post('yahoo_enable');

            } else {

                $one_yahoo_setting = yahoo_setting();


                $data["yahoo_setting_id"] = $one_yahoo_setting->yahoo_setting_id;


                $data["consumer_key"] = $one_yahoo_setting->consumer_key;


                $data["consumer_secret"] = $one_yahoo_setting->consumer_secret;


                $data["yahoo_enable"] = $one_yahoo_setting->yahoo_enable;


            }

        } else {


            $data1 = array(

                'consumer_key' => $this->input->post('consumer_key'),

                'consumer_secret' => $this->input->post('consumer_secret'),

                'yahoo_enable' => $this->input->post('yahoo_enable'),


            );

            $this->home_model->table_update('yahoo_setting_id', $this->input->post('yahoo_setting_id'), 'yahoo_setting', $data1);


            setting_deletecache('yahoo_setting');
            setting_new_cache('yahoo_setting', 'yahoo_setting');


            $data["error"] = "success";


            $data["yahoo_setting_id"] = $this->input->post('yahoo_setting_id');


            $data["consumer_key"] = $this->input->post('consumer_key');


            $data["consumer_secret"] = $this->input->post('consumer_secret');


            $data["yahoo_enable"] = $this->input->post('yahoo_enable');


        }


        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');


        $this->template->write('title', 'Administrator', '', TRUE);

        $this->template->write_view('header', 'header', $data, TRUE);

        $this->template->write_view('main_content', 'setting/add_yahoo_setting', $data, TRUE);

        $this->template->write_view('footer', 'footer', '', TRUE);

        $this->template->render();


    }


    /*

     Function name :add_linkdin_setting()

    Parameter : none.

    Return : none

    Use : this function is used to update Linkedin settings.

    */

    function add_linkdin_setting()

    {

        $check_rights = get_rights('add_linkdin_setting');


        if ($check_rights == 0) {

            redirect('admin/home/dashboard/no_rights');

        }

        $data['site_setting'] = site_setting();


        $this->load->library('form_validation');


        $this->form_validation->set_rules('linkdin_url', LINKEDIN_PROFILE_FULL_URL, 'required');

        $this->form_validation->set_rules('linkdin_access', LINKEDIN_ACCESS, 'required');

        $this->form_validation->set_rules('linkdin_secret', LINKEDIN_SECRET, 'required');


        if ($this->form_validation->run() == FALSE) {

            if (validation_errors()) {

                $data["error"] = validation_errors();

            } else {

                $data["error"] = "";

            }

            if ($this->input->post('linkdin_setting_id')) {

                $data["linkdin_setting_id"] = $this->input->post('linkdin_setting_id');


                $data["linkdin_enable"] = $this->input->post('linkdin_enable');


                $data["linkdin_access"] = $this->input->post('linkdin_access');


                $data["linkdin_secret"] = $this->input->post('linkdin_secret');


                $data["linkdin_url"] = $this->input->post('linkdin_url');

                //print_r($data);

            } else {


                $one_linkdin_setting = linkdin_setting();

                //print_r($one_linkdin_setting);

                $data["linkdin_setting_id"] = $one_linkdin_setting['linkdin_setting_id'];


                $data["linkdin_enable"] = $one_linkdin_setting['linkdin_enable'];


                $data["linkdin_access"] = $one_linkdin_setting['linkedin_access'];


                $data["linkdin_secret"] = $one_linkdin_setting['linkedin_secret'];


                $data["linkdin_url"] = $one_linkdin_setting['linkdin_url'];

            }


        } else {

            $data1 = array(

                'linkdin_enable' => $this->input->post('linkdin_enable'),

                'linkedin_access' => $this->input->post('linkdin_access'),

                'linkedin_secret' => $this->input->post('linkdin_secret'),

                'linkdin_url' => $this->input->post('linkdin_url'),

                /*'tw_oauth_token' => $this->input->post('tw_oauth_token'),

                'tw_oauth_token_secret' => $this->input->post('tw_oauth_token_secret'),*/

            );

            $this->home_model->table_update('linkdin_setting_id', $this->input->post('linkdin_setting_id'), 'linkdin_setting', $data1);

            //deletecache("linkdin_setting");
            setting_deletecache('linkdin_setting');
            setting_new_cache('linkdin_setting', 'linkdin_setting');
            $data["error"] = "success";


            $data["linkdin_setting_id"] = $this->input->post('linkdin_setting_id');


            $data["linkdin_enable"] = $this->input->post('linkdin_enable');


            $data["linkdin_access"] = $this->input->post('linkdin_access');


            $data["linkdin_secret"] = $this->input->post('linkdin_secret');


            $data["linkdin_url"] = $this->input->post('linkdin_url');

        }


        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');


        $this->template->write('title', 'Settings', '', TRUE);


        $this->template->write_view('header', 'header', $data, TRUE);


        $this->template->write_view('main_content', 'setting/add_linkdin_setting', $data, TRUE);


        $this->template->write_view('footer', 'footer', '', TRUE);


        $this->template->render();


    }


    function add_youtube_setting()

    {

        $check_rights = get_rights('add_youtube_setting');

        if ($check_rights == 0) {

            redirect('admin/home/dashboard/no_rights');

        }

        $data['site_setting'] = site_setting();

        $this->load->library('form_validation');

        $this->form_validation->set_rules('youtube_link', YOUTUBE_LINK, 'required');


        if ($this->form_validation->run() == FALSE) {

            if (validation_errors()) {

                $data["error"] = validation_errors();

            } else {

                $data["error"] = "";

            }

            if ($this->input->post('youtube_setting_id')) {

                $data["youtube_setting_id"] = $this->input->post('youtube_setting_id');

                $data["youtube_link"] = $this->input->post('youtube_link');

                $data["youtube_enable"] = $this->input->post('youtube_enable');

            } else {


                $one_youtube_setting = youtube_setting();

                $data["youtube_setting_id"] = $one_youtube_setting['youtube_setting_id'];

                $data["youtube_link"] = $one_youtube_setting['youtube_link'];

                $data["youtube_enable"] = $one_youtube_setting['youtube_enable'];

            }


        } else {

            $data1 = array(

                'youtube_link' => $this->input->post('youtube_link'),

                'youtube_enable' => $this->input->post('youtube_enable'),


            );

            $this->home_model->table_update('youtube_setting_id', $this->input->post('youtube_setting_id'), 'youtube_setting', $data1);

            //deletecache("linkdin_setting");
            setting_deletecache('youtube_setting');
            setting_new_cache('youtube_setting', 'youtube_setting');
            $data["error"] = "success";


            $data["youtube_setting_id"] = $this->input->post('youtube_setting_id');

            $data["youtube_link"] = $this->input->post('youtube_link');

            $data["youtube_enable"] = $this->input->post('youtube_enable');


        }


        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');

        $this->template->write('title', 'Settings', '', TRUE);

        $this->template->write_view('header', 'header', $data, TRUE);

        $this->template->write_view('main_content', 'setting/add_youtube_setting', $data, TRUE);

        $this->template->write_view('footer', 'footer', '', TRUE);

        $this->template->render();


    }


    function add_google_plus_setting()

    {

        $check_rights = get_rights('add_google_plus_setting');

        if ($check_rights == 0) {

            redirect('admin/home/dashboard/no_rights');

        }

        $data['site_setting'] = site_setting();

        $this->load->library('form_validation');

        $this->form_validation->set_rules('google_plus_link', GOOGLE_PROFILE_FULL_URL, 'required');


        if ($this->form_validation->run() == FALSE) {

            if (validation_errors()) {

                $data["error"] = validation_errors();

            } else {

                $data["error"] = "";

            }

            if ($this->input->post('google_plus_setting_id')) {

                $data["google_plus_setting_id"] = $this->input->post('google_plus_setting_id');

                $data["google_link"] = $this->input->post('google_link');

                $data["google_enable"] = $this->input->post('google_enable');

            } else {


                $google_plus_setting = google_plus_setting();

                $data["google_plus_setting_id"] = $google_plus_setting['google_plus_setting_id'];

                $data["google_plus_link"] = $google_plus_setting['google_plus_link'];

                $data["google_plus_enable"] = $google_plus_setting['google_plus_enable'];

            }


        } else {

            $data1 = array(

                'google_plus_link' => $this->input->post('google_plus_link'),

                'google_plus_enable' => $this->input->post('google_plus_enable'),


            );

            $this->home_model->table_update('google_plus_setting_id', $this->input->post('google_plus_setting_id'), 'google_plus_setting', $data1);

            setting_deletecache('google_plus_setting');
            setting_new_cache('google_plus_setting', 'google_plus_setting');

            $data["error"] = "success";


            $data["google_plus_setting_id"] = $this->input->post('google_plus_setting_id');

            $data["google_plus_link"] = $this->input->post('google_plus_link');

            $data["google_plus_enable"] = $this->input->post('google_plus_enable');


        }


        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');

        $this->template->write('title', 'Settings', '', TRUE);

        $this->template->write_view('header', 'header', $data, TRUE);

        $this->template->write_view('main_content', 'setting/add_google_plus_setting', $data, TRUE);

        $this->template->write_view('footer', 'footer', '', TRUE);

        $this->template->render();


    }

    function help_doc($doc_name)
    {

        $CI =& get_instance();

        $base_path = $CI->config->slash_item('base_path');

        $this->load->helper('download');

        if ($doc_name == "facebook") {
            $data = file_get_contents($base_path . "/upload/doc/apidocument/Facebook API.pdf"); // Read the file's contents
            $name = 'Facebook API.pdf';
        }
        if ($doc_name == "paypal") {
            $data = file_get_contents($base_path . "/upload/doc/apidocument/Adaptive PayPal Sandbox Configuration.pdf"); // Read the file's contents
            $name = 'Paypal API.pdf';
        }
        if ($doc_name == "google") {
            $data = file_get_contents($base_path . "/upload/doc/apidocument/Google API.pdf"); // Read the file's contents
            $name = 'Google API.pdf';
        }
        if ($doc_name == "linkedin") {
            $data = file_get_contents($base_path . "/upload/doc/apidocument/LinkedIn API.pdf"); // Read the file's contents
            $name = 'LinkedIn API.pdf';
        }
        if ($doc_name == "twitter") {
            $data = file_get_contents($base_path . "/upload/doc/apidocument/Twitter API.pdf"); // Read the file's contents
            $name = 'Twitter API.pdf';
        }

        force_download($name, $data);
    }


}


?>
