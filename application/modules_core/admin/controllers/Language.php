<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Language extends ROCKERS_Controller
{

    function __construct()
    {

        parent::__construct();
        $this->load->model('home_model');

    }

    function index()
    {
        redirect('admin/language/list_language/');
    }

    /*
    Function name :list_language()
    Parameter : $msg:message string of the last operation performed by admin.
    Return : none
    Use : to display list of language to admin.
    */

    function list_language($msg = '')
    {
        $data['msg'] = $this->session->flashdata('item');

        $check_rights = get_rights('list_language');

        if ($check_rights == 0) {
            //redirect('admin/home/dashboard/no_rights');
        }

        $data['result'] = get_table_data('language', array('language_name' => 'asc'));

        $data['site_setting'] = site_setting();
        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Language', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'language/list_language', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }

    /*
    Function name :add_province()
    Parameter : none.
    Return : none
    Use : to add new province entry into province table.
    */

    function add_language()
    {
        $check_rights = get_rights('list_language');

        if ($check_rights == 0) {
            //redirect('admin/home/dashboard/no_rights');
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('language_name', LANGUAGE_NAME, 'required|alpha_space');

        $this->form_validation->set_rules('language_folder', LANGUAGE_FOLDER, 'required|alpha_space');

        //$this->form_validation->set_rules('country_flag', 'Country Flag', 'required');


        $check_language = '';
        $data['success'] = '';
        $file_error = '';
        $country_flag = '';

        if ($_POST) {

            if ($this->languagename_check(SecurePostData($this->input->post('language_name'))) == 1) {

                $check_language = LANGUAGE_IS_ALREADY_EXIST;
            }

        }
        if ($_FILES) {
            if ($_FILES["country_flag"]["type"] != "image/jpeg" and $_FILES["country_flag"]["type"] != "image/pjpeg" and $_FILES["country_flag"]["type"] != "image/png" and $_FILES["country_flag"]["type"] != "image/x-png" and $_FILES["country_flag"]["type"] != "image/gif") {
                $file_error .= PLEASE_UPLOAD_A_JPG_PNG_GIF_FILE;
            } else if ($_FILES["country_flag"]["size"] > 2000000) {
                $file_error .= SORRY_THIS_FILE_IS_TOO;
            } else {
                $file_error = '';
            }
        }

        if ($this->form_validation->run() == FALSE || $check_language != '' || $file_error != '') {
            if (validation_errors() || $check_language != '' || $file_error != '') {
                $data["error"] = validation_errors() . $check_language . $file_error;

            } else {
                $data["error"] = "";
            }

            $data["language_id"] = $this->input->post('language_id');
            $data["language_name"] = $this->input->post('language_name');
            $data["active"] = $this->input->post('active');
            $data["language_folder"] = $this->input->post('language_folder');

        } else {

            if ($_FILES) {

                $data['userfile'] = move_uploaded_file($_FILES["country_flag"]["tmp_name"], 'upload/country_flag/' . $_FILES["country_flag"]["name"]);
                $country_flag = $_FILES["country_flag"]["name"];

            }
            $data = array(
                'language_name' => $this->input->post('language_name'),
                'active' => $this->input->post('active'),
                'language_folder' => $this->input->post('language_folder'),
                'country_flag' => $country_flag

            );


            $this->home_model->table_insert('language', $data);
            $msg = "insert";
            $this->session->set_flashdata('item', NEW_RECORD_HAS_BEEN_ADDED_SUCCESSFULLY);
            redirect('admin/language/list_language');

            setting_deletecache('supported_languages');
            setting_deletecache('default_language');

        }

        $data['site_setting'] = site_setting();

        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Language', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'language/add_language', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();

    }

    /*
    Function name :languagename_check()
    Parameter :languagename
    Return :1 or 0.
    Use : check language name into language table which already exist or not..
    */

    function languagename_check($language_name)
    {
        $language = array('language_name' => $language_name);
        $languagename = language_validate($language);

        if ($languagename != '') {
            return 1;
        } else {
            return 0;
        }

    }

    /*
    Function name :edit_language()
    Parameter : $id:id of the language admin wish to edit.
    Return : none
    Use : to edit the information of particular language.
    */

    /*function edit_language($id=0)
    {

        $check_rights=get_rights('list_language');

        if($check_rights==0)
        {
            redirect('admin/home/dashboard/no_rights');
        }

        $one_language = $this->province_model->get_one_tabledata('language',array('language_id'=>$id));
        $data["error"] = "";
        $file_error ='';
        $data["language_id"] = $id;
        $data["language_name"] = $one_language['language_name'];
        $data["active"] = $one_language['active'];
        $data["language_folder"] = $one_language['language_folder'];
        $data["country_flag"] = $one_language['country_flag'];

        if($_POST)
        {
            $file_error ='';
            $data["language_name"] = $this->input->post('language_name');
            $data["active"] = $this->input->post('active');
            $data["language_folder"] = $this->input->post('language_folder');
            $data['country_flag']=$_FILES["country_flag"]["name"];


            if($_FILES){
             if ($_FILES["country_flag"]["type"] != "image/jpeg" and $_FILES["country_flag"]["type"] != "image/pjpeg" and $_FILES["country_flag"]["type"] != "image/png" and $_FILES["country_flag"]["type"] != "image/x-png" and $_FILES["country_flag"]["type"] != "image/gif")
            {
                $file_error .= PLEASE_UPLOAD_A_JPG_PNG_GIF_FILE;
            } else if ($_FILES["country_flag"]["size"] > 2000000) {
                 $file_error .= SORRY_THIS_FILE_IS_TOO;
            }
            else{
                $file_error='';
            }

                $data['country_flag'] =	move_uploaded_file($_FILES["country_flag"]["tmp_name"],'upload/country_flag/'.$_FILES["country_flag"]["name"]);
                $data['country_flag']=$_FILES["country_flag"]["name"];

        }

            $this->province_model->table_update('language_id',$this->input->post('language_id'),'language',$data);
            $msg = "update";
            $this->session->set_flashdata('item', RECORD_HAS_BEEN_UPDATED_SUCCESSFULLY);
            redirect('admin/language/list_language');
        }





        $data['site_setting'] = site_setting();
        $theme='admin';

        $this->template->set_master_template($theme .'/template.php');
        $this->template->write('title', 'Language', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'language/edit_language', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }*/

    function edit_language($id = 0)
    {

        $check_rights = get_rights('list_language');

        if ($check_rights == 0) {
            //redirect('admin/home/dashboard/no_rights');
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('language_name', LANGUAGE_NAME, 'required|alpha_space');

        $this->form_validation->set_rules('language_folder', LANGUAGE_FOLDER, 'required|alpha_space');

        $one_language = $this->home_model->get_one_tabledata('language', array('language_id' => $id));
        //print_r($one_language);
        $data["error"] = "";
        $file_error = '';

        $data["language_id"] = $id;
        $data["language_name"] = $one_language['language_name'];
        $data["active"] = $one_language['active'];
        $data["language_folder"] = $one_language['language_folder'];
        $data["country_flag"] = $one_language['country_flag'];


        $check_language = '';
        $data['success'] = '';
        $file_error = '';
        $country_flag = $data["country_flag"];


        if ($_FILES) {

            if ($_FILES['country_flag']['name']) {

                if ($_FILES["country_flag"]["type"] != "image/jpeg" and $_FILES["country_flag"]["type"] != "image/pjpeg" and $_FILES["country_flag"]["type"] != "image/png" and $_FILES["country_flag"]["type"] != "image/x-png" and $_FILES["country_flag"]["type"] != "image/gif") {
                    $file_error .= PLEASE_UPLOAD_A_JPG_PNG_GIF_FILE;
                } else if ($_FILES["country_flag"]["size"] > 2000000) {
                    $file_error .= SORRY_THIS_FILE_IS_TOO;
                } else {
                    $file_error = '';
                }
            }
        }

        if ($this->form_validation->run() == FALSE || $check_language != '' || $file_error != '') {
            if (validation_errors() || $check_language != '' || $file_error != '') {
                $data["error"] = validation_errors() . $check_language . $file_error;

            } else {
                $data["error"] = "";
            }

            if ($_POST) {
                $data["language_id"] = $this->input->post('language_id');
                $data["language_name"] = $this->input->post('language_name');
                $data["active"] = $this->input->post('active');
                $data["language_folder"] = $this->input->post('language_folder');
            }
        } else {

            if ($_FILES) {

                if ($_FILES['country_flag']['name']) {
                    $data['userfile'] = move_uploaded_file($_FILES["country_flag"]["tmp_name"], 'upload/country_flag/' . $_FILES["country_flag"]["name"]);
                    $country_flag = $_FILES["country_flag"]["name"];
                }

            }
            $updatearray = array(
                'language_name' => $this->input->post('language_name'),
                'active' => $this->input->post('active'),
                'language_folder' => $this->input->post('language_folder'),
                'country_flag' => $country_flag

            );


            $this->home_model->table_update('language_id', $id, 'language', $updatearray);
            $msg = "update";
            setting_deletecache('supported_languages');
            setting_deletecache('default_language');
            $this->session->set_flashdata('item', RECORD_HAS_BEEN_UPDATED_SUCCESSFULLY);
            redirect('admin/language/list_language');


        }

        $data['site_setting'] = site_setting();

        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Language', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'language/edit_language', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }

    /*
    Function name :delete_language()
    Parameter : $id:id of the language admin wish to delete.
    Return : none
    Use : to delete particular language
    */

    function delete_language($id = 0)
    {
        $check_rights = get_rights('list_language');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        $this->db->delete('language', array('language_id' => $id));
        $msg = "delete";
        $this->session->set_flashdata('item', RECORD_HAS_BEEN_DELETED_SUCCESSFULLY);
        redirect('admin/language/list_language');
    }


}

?>
