<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Update extends ROCKERS_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('home_model');
        $this->load->library('CstFileDiff');
       
       
    }

    function index()
    {
        redirect('admin/update/list_update/');
    }
    
    function my_version($msg=''){
        
        
        $site_settting = site_setting();
        $data['site_setting'] = $site_settting;
        
        $data['msg'] = $msg;
        
        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Update', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'auto_update/process_update', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }
    
    
    function step1($msg = '')
    {
        $base_pth = base_path();
        $my_file = $base_pth . '/ver_update.xml';
        if (file_exists($my_file)) {
            $xml = simplexml_load_file($my_file);

            $array = (array)$xml;
            $data['array'] = $array;

        } else {
            exit('Failed to open file.');
        }
       
        $data['msg'] = $msg;        
        $this->load->view('auto_update/step1', $data);
        
    }

    function step2($msg = '')
    {
        $base_pth = base_path();
        $my_file = $base_pth . '/ver_update.xml';
        if (file_exists($my_file)) {
            $xml = simplexml_load_file($my_file);

            $array = (array)$xml;
            $data['array'] = $array;
        } else {
            exit('Failed to open file.');
        }        

        $data['msg'] = $msg;       
        $this->load->view('auto_update/step2', $data);       
    }

    function list_update($msg = '')
    {
       
        $allfiles = '';
        $site_settting = site_setting();
        
        $is_read = $this->session->userdata('is_read_fol');
        unset($is_read);
        
        if (!isset($is_read)) {
            
            $site_version = $site_settting['site_version'];
            $version_pcode = $site_settting['version_pcode'];
            $version_license = $site_settting['version_license'];
            $c_url = $site_settting['version_url'];
            
            $domain = $_SERVER['HTTP_HOST'];

            
            $folder_name_for_copy_zip = 'verr';
            $data = array('folder_name_for_copy_zip' => $folder_name_for_copy_zip, 'current_version' => $site_version, 'current_license' => $version_license);
            $this->session->set_userdata($data);
            
            
            
            $encoded = 'version_no=' . $site_version . '&';
            $encoded .= urlencode('version_license') . '=' . urlencode($version_license);
            $encoded .= '&' . urlencode('domain') . '=' . urlencode($domain);
            $encoded .= '&' . urlencode('type') . '=' . urlencode('list_version');
            $encoded .= '&' . urlencode('version_pcode') . '=' . urlencode(base64_encode($version_pcode));
           
            
            $ch = curl_init($c_url);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $encoded);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $output = curl_exec($ch);
           
            $xmlstr = $output;


            $output_version = new SimpleXMLElement($xmlstr);
            
            $array = (array)$output_version;
            
           //echo "<pre>"; print_r($array); die;
            
            $filepath = '';
            $version = '';
            $base_pth = base_path();
            
            
            
            if (isset($array['item']) && !empty($array['item'])) {
                
                
                $item_res=$array['item'];
                $obj = $item_res[0];
                
                
                
                if (isset($obj->successcode)) {
                    
                    if (isset($obj->filepath)) {
                        
                        $filepath = $obj->filepath;
                        $version = (string) $obj->version;
                        $license = (string) $obj->license; 
                        
                        $data = array('new_site_version' => $version, 'new_version_license' => $license);
                        $this->session->set_userdata($data);
                    }

                    if ($filepath != '') {

                        //$foler_path=$base_pth."ver/";
                        $folder_name_for_copy_zip = $this->session->userdata('folder_name_for_copy_zip');
                        $foler_path = $base_pth . $folder_name_for_copy_zip . "/";
                        if (!is_dir($foler_path)) {
                            mkdir($foler_path);
                            chmod($foler_path, 0777);
                        }
                        
                         

                        $path = $foler_path . "" . $version . ".zip";
                        $in = fopen($filepath, "rb");
                        $out = fopen($path, "wb");
                        while ($chunk = fread($in, 8192)) {
                            fwrite($out, $chunk, 8192);
                        }
                        fclose($in);
                        fclose($out);

                        $foler_path = $foler_path . $version . "/";
                        if (!is_dir($foler_path)) {
                            mkdir($foler_path);
                            chmod($foler_path, 0777);
                        }
                        unzip($path, $foler_path);

                       
                        
                        $extension1 = explode(".", $filepath);
                        $extension = $extension1[count($extension1) - 1];

                        $folder = explode("/", $filepath);
                        $folder_name = $folder[count($folder) - 1];
                        $folder_name = str_replace("." . $extension, "", $folder_name);
                        $dir = $foler_path . "/" . $folder_name;
                        $version_folder = "/" . $folder_name_for_copy_zip . "/" . $version;
                        if (is_dir($dir)) {
                            $version_folder = "/" . $folder_name_for_copy_zip . "/" . $version . "//" . $folder_name;
                        }
                        $is_read = $foler_path;
                        $new_site_version = $version;
                        $new_version_license = $license;

                        $data = array('version_folder' => $version_folder, 'is_read_fol' => $is_read);
                        $this->session->set_userdata($data);


                    }
                }


            } else {
                //maintain cookies for call request twise in day
                //	setcookie("fund_latest_version",$value, time()+3600*12);
            }

            curl_close($ch);

        }


        $is_read = $this->session->userdata('is_read_fol');

        if (isset($is_read)) $allfiles = $this->find_all_files(substr ($is_read,0,-1));
        
        
        
        //echo $_SESSION['is_read'];
        $data['site_setting'] = $site_settting;
        $data['allfiles'] = $allfiles;
        $data['msg'] = $msg;
        if ($msg == '') {
            $this->load->view('auto_update/ajax_list_update', $data);
        } else {
            $theme = 'admin';

            $this->template->set_master_template($theme . '/template.php');
            $this->template->write('title', 'Update', '', TRUE);
            $this->template->write_view('header', 'header', $data, TRUE);
            $this->template->write_view('main_content', 'auto_update/list_update', $data, TRUE);
            $this->template->write_view('footer', 'footer', '', TRUE);
            $this->template->render();
        }
    }

    function process_update()
    {

        try {
            error_reporting(0);
            $site_settting = site_setting();
            $is_read = $this->session->userdata('is_read_fol');
            
            $is_merge=0;
            
            if($_POST){
                if(isset($_POST['file_type_write'])){
                    $file_type_write=(int) $_POST['file_type_write'];
                    
                    if($file_type_write==2){
                        $is_merge=1;
                    }
                }
            }
            
           // echo $is_merge."==<br/><br/>";
           
           
          
            
            if (isset($is_read)) {
                $allfiles = $this->find_all_files($is_read);
                $files_to_zip = array();
                $base_pth = base_path();
                if (is_array($allfiles)) {
                    foreach ($allfiles as $key => $val) {
                        $dest = str_replace($this->session->userdata('version_folder'), "", $val);
                        $key = str_replace($base_pth . "/", "", $dest);
                        $key = str_replace($base_pth, "", $dest);
                        $source = $val;                       
                       
                        $files_to_zip[$key] = $dest;
                    }
                }

              
                 $site_version = $site_settting['site_version'];
                 $version_license = $site_settting['version_license'];
                
                
                
               
                $result = $this->create_zip($files_to_zip, $base_pth . $this->session->userdata('folder_name_for_copy_zip') . '/' . 'backup_' . $site_version . '.zip', true);
                
                
                
                if (is_array($allfiles)) {

                   
                    $c_url = $site_settting['version_url'];
                    $domain = $_SERVER['HTTP_HOST'];
                    $version_pcode = $site_settting['version_pcode'];
                   
                    $encoded = 'version_no=' . $site_version . '&';
                    $encoded .= urlencode('version_license') . '=' . urlencode($version_license);
                    $encoded .= '&' . urlencode('domain') . '=' . urlencode($domain);
                    $encoded .= '&' . urlencode('type') . '=' . urlencode('update_version');
                    $encoded .= '&' . urlencode('version_pcode') . '=' . urlencode(base64_encode($version_pcode));
                    
                  
                    
                    $ch = curl_init($c_url);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $encoded);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    $output = curl_exec($ch);
                    

                    foreach ($allfiles as $key => $val) {                        
                       
                        $dest = str_replace($this->session->userdata('version_folder'), "", $val);
                        $dest = str_replace("//", "/", $dest);
                       
                        $source = str_replace("//", "/", $val);
                        
                        
                        if (!file_exists($dest)) {
                            
                            $directory_path = dirname($dest);
                            if (!is_dir($directory_path)) { 
                                mkdir($directory_path);
                                chmod($directory_path, 0777); 
                            }   
                            
                            if (!copy($source, $dest)) {
                                //echo "<br/><br/>==== failed to copy NEW $source...<br/><br/>";
                             }
                           
                        } else {
                            if($is_merge==1){
                                                                
                                $merge_file_content= CstFileDiff::toMerge(CstFileDiff::compareFiles($source, $dest)); 
                                
                                $create_merge_file = fopen($dest, "w");
                               
                                fwrite($create_merge_file, $merge_file_content);
                                fclose($create_merge_file);
                                
                            } else {
                                // copy($source, $dest); === overwrite  files
                                if (!copy($source, $dest)) {
                                    //echo "<br/><br/>==== failed to copy $source...<br/><br/>";
                                }
                            }
                        }
                        
                        
                    }
                }
                
                
                $last_update_site_version = array('last_update_site_version' => $site_version,'last_update_site_license'=>$version_license);
                $this->session->set_userdata($last_update_site_version);
                

                $new_site_version = $this->session->userdata('new_site_version');
                $new_version_license = $this->session->userdata('new_version_license');
                if ($new_site_version != "") {

                    //$this->home_model->table_insert('version_update',$insert_array);
                    $base_pth = base_path();
                    $my_file = $base_pth . '/ver_update.xml';
                    if (file_exists($my_file)) {
                        $xml = simplexml_load_file($my_file);

                        $array = (array)$xml;
                        
                        $insert_array=array();
                        
                         if (isset($array['item']) && !empty($array['item'])) {
                
                            $item_res=$array['item'];

                            if(!isset($item_res->successcode)){

                                    $obj = $item_res[0];

                                    if (isset($obj->successcode)) { 


                                         $insert_array = array(
                                            "version" => (string)$obj->version,
                                            "description" => (string)$obj->description,
                                            "license" => (string)$obj->license,
                                            "product_name" => (string)$obj->product_name,
                                            "date" => date("Y-m-d")
                                        );

                                    }

                            } else {

                                $obj = $item_res; 

                                if (isset($obj->successcode)) {
                                    

                                     $insert_array = array(
                                            "version" => (string)$obj->version,
                                            "description" => (string)$obj->description,
                                            "license" => (string)$obj->license,
                                            "product_name" => (string)$obj->product_name,
                                            "date" => date("Y-m-d")
                                        );
                                    
                                } 
                            }


                        }
                        
                          
                            if(!empty($insert_array)){
                                
                                $xml_data_count	= count($xml->item);
                                for($i = 0; $i <=$xml_data_count;$i++){
                                    if((string) $xml->item[$i]->version == (string) $new_site_version)
                                    {
                                        unset($xml->item[$i]);
                                    }                                    
                                }
                                if(file_put_contents($base_pth . '/ver_update.xml', $xml->saveXML())){

                                
                                $this->home_model->table_insert('version_update', $insert_array);
                                $sql = "update site_setting set site_version='" . $new_site_version . "',version_license='" . $new_version_license . "'";
                                $this->db->query($sql);
                                deletecache("site_setting");
                                redirect('admin/update/list_update/yes');
                                
                            } else { 
                                redirect('admin/update/list_update/Unable to update version configuration file.');
                            }
                            }
                            //die;
                    } 
                     redirect('admin/update/list_update/Unable to find version update configuration file.');
                    
                }
                redirect('admin/update/list_update/Unable to update version. Please try once again.');
                //$data['allfiles'] =$allfiles;
            }
        } catch (Exception $e) {
            redirect('admin/update/list_update/' . $e->getMessage());
        }
        //die;
    }


    function backupfile()
    {
        try {

            $files_to_zip = array();
            $base_pth = base_path();
            $current_version = $this->session->userdata('last_update_site_version');
            $current_license = $this->session->userdata('last_update_site_license');
             
            $folder_name_for_copy_zip = $this->session->userdata('folder_name_for_copy_zip');
            $foler_path1 = $base_pth . $folder_name_for_copy_zip."/";
            
            
            $foler_path = $foler_path1 . "backup_".$current_version;
            
            $path = $foler_path1 . "backup_" . $current_version . ".zip";
            
           
            
            if (file_exists($path)) {
                
                
                
                if (!is_dir($foler_path)) {
                  
                    mkdir($foler_path);
                    chmod($foler_path, 0777);
                }
                unzip($path, $foler_path);
                
                
               

                $allfiles = $this->find_all_files($foler_path);
                $files_to_zip = array();
                $base_pth = base_path();
               
                 $version_folder = "/" . $folder_name_for_copy_zip . "/" . "backup_" . $current_version;
				
                foreach ($allfiles as $key => $val) {
                    $dest = str_replace($version_folder, "", $val);
                    $source = $val;
                    if (file_exists($source)) {
                        $directory_path = dirname($dest);
                        if (!is_dir($directory_path)) {
                            mkdir($directory_path);
                            chmod($directory_path, 0777);
                        }
			
						
                        copy($source, $dest);
                    }
                }
             
               
                if ($new_site_version != "") {
                    $sql = "update site_setting set site_version='" . $current_version . "',current_license='" . $current_license . "'";
                    $this->db->query($sql);
                    
                }
                redirect('admin/update/list_update/back');
            } else {
               
                redirect('admin/update/list_update/no_back');
            }    

        } catch (Exception $e) {
            redirect('admin/update/list_update/' . $e->getMessage());
        }

    }

    function find_all_files($dir)
    {
        if (is_dir($dir)) {
            $root = scandir($dir);
            foreach ($root as $value) {
                if ($value === '.' || $value === '..') {
                    continue;
                }
                if (is_file("$dir/$value")) {
                    $result[] = "$dir/$value";
                    continue;
                }
                foreach ($this->find_all_files("$dir/$value") as $value) {
                    $result[] = $value;
                }
            }
            return $result;
        }
        return array();
    }

    function create_zip($files = array(), $destination = '', $overwrite = false)
    {
        //if the zip file already exists and overwrite is false, return false
        if (file_exists($destination) && !$overwrite) {
            return false;
        }
        //vars
        $valid_files = array();
        //if files were passed in...
        if (is_array($files)) {
            //cycle through each file
            foreach ($files as $key => $file) {
                //make sure the file exists
                if (file_exists($file)) {
                    $valid_files[$key] = $file;
                }
            }
        }
        //if we have good files...
        if (count($valid_files)) {
            //create the archive
            $zip = new ZipArchive();
            if ($zip->open($destination, $overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
                return false;
            }
            //add the files
            foreach ($valid_files as $key => $file) {
                $zip->addFile($file, $key);
            }
            //debug
            //echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;

            //close the zip -- done!
            $zip->close();

            //check to make sure the file exists
            return file_exists($destination);
        } else {
            return false;
        }
    }

}

?>
