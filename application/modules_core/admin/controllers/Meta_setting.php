<?php

/*********************************************************************************
 * This the fundraisingscript.com  by Rockers Technology. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *  Rockers Technologies (Head Office)
 *    5038,Berthpage Dr
 *    suwanee, GA. Zip Code : 30024
 *    E-mail Address : nishu@rockersinfo.com
 *
 * Copyright � 2012-2020 by Rockers Technology , INC a domestic profit corporation has been duly incorporated under
 * the laws of the state of georiga , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
class Meta_setting extends ROCKERS_Controller
{
    function __construct()
    {
        parent::__construct();
        //$this->load->model('meta_setting_model');
        $this->load->model('home_model');

    }

    function index()
    {
        redirect('admin/meta_setting/add_meta_setting');
    }

    /*
     Function name :add_meta_setting()
    Parameter : none.
    Return : none
    Use : this function is used to update meta setting data in its table.
    */
    function add_meta_setting($lang_id=1)
    {

        $check_rights = get_rights('add_meta_setting');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        $data['site_setting'] = site_setting();
        $this->load->library('form_validation');

        $this->form_validation->set_rules('title', TITLE, 'required');

        if ($this->form_validation->run() == FALSE) {
            if (validation_errors()) {
                $data["error"] = validation_errors();
            } else {
                $data["error"] = "";
            }
            if ($this->input->post('meta_setting_id')) {
                $data["meta_setting_id"] = $this->input->post('meta_setting_id');
                $data["title"] = $this->input->post('title');
                $data["meta_keyword"] = $this->input->post('meta_keyword');
                $data["meta_description"] = $this->input->post('meta_description');
                $data["language_id"] = $this->input->post('language_id');
            } else {
                $one_meta_setting = meta_setting($lang_id);
                
                 if(!empty($one_meta_setting)){                 
                    $data["meta_setting_id"] = $one_meta_setting['meta_setting_id'];
                    $data["title"] = $one_meta_setting['title'];
                    $data["meta_keyword"] = $one_meta_setting['meta_keyword'];
                    $data["meta_description"] = $one_meta_setting['meta_description'];
                    $data["language_id"] = $one_meta_setting['language_id'];
                 } else {
                    $data["meta_setting_id"] = '';
                    $data["title"] = '';
                    $data["meta_keyword"] = '';
                    $data["meta_description"] = '';
                    $data["language_id"] = $lang_id;
                 }
            }

        } else {
            $data1 = array(
                'title' => $this->input->post('title'),
                'meta_keyword' => $this->input->post('meta_keyword'),
                'meta_description' => $this->input->post('meta_description'),
                'language_id' => $this->input->post('language_id'),
            );
            
            
            $check_exists=$this->home_model->get_one_tabledata('meta_setting',array('language_id' => $this->input->post('language_id')));
            
            if(empty($check_exists)) {            
                $this->home_model->table_insert('meta_setting',$data1);
            } else {            
                $this->home_model->table_update('meta_setting_id', $this->input->post('meta_setting_id'), 'meta_setting', $data1);
            }
            
            deletecache("meta_setting".$lang_id);
            
           
                
            //clear all cache
            $query_lang = $this->db->query("select * from meta_setting");
            if ($query_lang->num_rows() > 0)
            {
                    $query_lang_res= $query_lang->result_array();
                    foreach($query_lang_res as $lang)
                    {
                            $meta_lang_id=$lang['language_id'];
                            deletecache("meta_setting".$meta_lang_id);
                    }
            }



            $data["error"] = "success";

            $one_meta_setting = meta_setting($lang_id);
            $data["meta_setting_id"] = $one_meta_setting['meta_setting_id'];
            $data["title"] = $one_meta_setting['title'];
            $data["meta_keyword"] = $one_meta_setting['meta_keyword'];
            $data["meta_description"] = $one_meta_setting['meta_description'];
            $data["language_id"] = $one_meta_setting['language_id'];
            
        }
        
        $data['language'] = $this->home_model->get_language('language', array('active' => 1));
        
        
        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');


        $this->template->write('title', 'Administrator', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'setting/add_meta_setting', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }

}

/* end of file */