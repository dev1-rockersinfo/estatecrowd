<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Corelogic_api extends ROCKERS_Controller
{
    public function __construct(){
        parent::__construct();
        $this->load->model('corelogic_api_model','corel');
        $this->load->model('home_model');

    }

    function index(){

        $check_rights = get_rights('list_property');

        $data['msg'] = $this->session->flashdata('msg');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        $data['result'] = $this->corel->get_postcode();

        $data['site_setting'] = site_setting();

        $data['taxonomy_setting'] = taxonomy_setting();


        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Property List', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'corelogic_api/postcode_list', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();

    }

    function locality_list(){

        $check_rights = get_rights('list_property');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        $data['msg'] = $this->session->flashdata('msg');
        $search = array(
            'postcode_id'=>SecurePostData($this->input->get('postcode'))
        );

        $data['result'] = $this->corel->get_localities($search);



        $data['msg'] = '';

        $data['site_setting'] = site_setting();

        $data['taxonomy_setting'] = taxonomy_setting();

        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Property List', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'corelogic_api/locality_list', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();

    }

    function property_list(){

        $check_rights = get_rights('list_property');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        $data['result'] = $this->corel->get_api_properties();



        $data['msg'] = $this->session->flashdata('msg');

        $data['site_setting'] = site_setting();

        $data['taxonomy_setting'] = taxonomy_setting();

        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Property List', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'corelogic_api/property_list', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();

    }
}
?>