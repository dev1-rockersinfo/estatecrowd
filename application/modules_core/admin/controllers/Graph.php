<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Graph extends ROCKERS_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('home_model');
        $this->load->model('graph_model');
        $this->load->model('equity_model');    ///added by bhavana
    }

    function index()
    {
        //echo 'here';die;

        $data = array();

        $date = date('Y-m-d');

        $week_first_date = get_first_day_of_week($date);
        $week_last_date = get_last_day_of_week($date);

        $data['week_first_date'] = $week_first_date;
        $data['week_last_date'] = $week_last_date;

        $data['weekly_projects'] = $this->graph_model->get_weekly_projects($week_first_date, $week_last_date);

        $data['weekly_projects_success'] = $this->graph_model->get_weekly_projects_success($week_first_date, $week_last_date);
        $data['weekly_projects_active'] = $this->graph_model->get_weekly_projects_active($week_first_date, $week_last_date);
        $data['weekly_projects_new'] = $this->graph_model->get_weekly_projects_new($week_first_date, $week_last_date);


        //	$data['total_proj']=$this->home_model->GetAllProjects('','',0,'1,2,3,4,5',array('user','project_category'),0,'','','yes');
        $data['total_proj'] = $this->equity_model->GetAllEquities('', '', 0, '1,2,3,4,5,6', array('user'), $limit = 1000, array(
            'equity_id' => 'desc'
        ));

        //$data['total_pending_proj']=$this->home_model->GetAllProjects('','',0,'1',array('user'),0,'','','yes');
        $data['total_pending_proj'] = $this->equity_model->GetAllEquities('', '', 0, '1', array('user'), 0, '', '', 'yes');

        $data['total_active_proj'] = $this->equity_model->GetAllEquities('', '', 0, '2', array('user', 'project_category'), 0, '', '', 'yes');

        $data['total_success_proj'] = $this->equity_model->GetAllEquities('', '', 0, '3', array('user', 'project_category'), 0, '', '', 'yes');

        $data['total_console_success_proj'] = $this->equity_model->GetAllEquities('', '', 0, '4', array('user', 'project_category'), 0, '', '', 'yes');

        $data['total_failure_proj'] = $this->equity_model->GetAllEquities('', '', 0, '5', array('user', 'project_category'), 0, '', '', 'yes');

        $this->db->select('SUM(transaction.amount) as total_amount, SUM(transaction.pay_fee) as commision');

        $this->db->from('transaction');

        $this->db->join('campaign', 'transaction.campaign_id=campaign.campaign_id');

        $this->db->where('campaign.status in (2,3,4)');

        $query = $this->db->get();

        $data['transaction'] = $query->row_array();

        $month_first_date = date('Y-m-d', mktime(0, 0, 0, date('m'), 1, date('Y')));
        $month_last_date = date('Y-m-t', mktime(0, 0, 0, date('m'), 1, date('Y')));

        $month_first = 1;
        $month_last = 12;

        $data['month_first'] = $month_first;
        $data['month_last'] = $month_last;

        $data['monthly_projects'] = $this->graph_model->get_monthly_projects($month_first, $month_last);

        $data['monthly_projects_success'] = $this->graph_model->get_monthly_projects_success($week_first_date, $week_last_date);
        $data['monthly_projects_active'] = $this->graph_model->get_monthly_projects_active($week_first_date, $week_last_date);
        $data['monthly_projects_new'] = $this->graph_model->get_monthly_projects_new($week_first_date, $week_last_date);


        $year_back = strtotime('-2 years');
        $year_forward = strtotime('+2 years');

        $year_first = date('Y', $year_back);
        $year_last = date('Y', $year_forward);

        $data['year_first'] = $year_first;
        $data['year_last'] = $year_last;


        $data['yearly_projects'] = $this->graph_model->get_yearly_projects($year_first, $year_last);

        $data['yearly_projects_success'] = $this->graph_model->get_yearly_projects_success($year_first, $year_last);
        $data['yearly_projects_active'] = $this->graph_model->get_yearly_projects_active($year_first, $year_last);
        $data['yearly_projects_new'] = $this->graph_model->get_yearly_projects_new($year_first, $year_last);
        // echo '<pre>';
        // print_r($data);die;
        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Graphs', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'graphs/project_graph', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();

    }


    function user()
    {

        $data = array();

        $date = date('Y-m-d');

        $week_first_date = get_first_day_of_week($date);
        $week_last_date = get_last_day_of_week($date);

        $data['week_first_date'] = $week_first_date;
        $data['week_last_date'] = $week_last_date;

        $data['weekly_registration'] = $this->graph_model->get_weekly_registration($week_first_date, $week_last_date);
        $data['weekly_fb_registration'] = $this->graph_model->get_weekly_fb_registration($week_first_date, $week_last_date);
        $data['weekly_tw_registration'] = $this->graph_model->get_weekly_tw_registration($week_first_date, $week_last_date);


        $month_first_date = date('Y-m-d', mktime(0, 0, 0, date('m'), 1, date('Y')));
        $month_last_date = date('Y-m-t', mktime(0, 0, 0, date('m'), 1, date('Y')));

        $month_first = 1;
        $month_last = 12;

        $data['month_first'] = $month_first;
        $data['month_last'] = $month_last;

        $data['monthly_registration'] = $this->graph_model->get_monthly_registration($month_first, $month_last);
        $data['monthly_fb_registration'] = $this->graph_model->get_monthly_fb_registration($month_first, $month_last);
        $data['monthly_tw_registration'] = $this->graph_model->get_monthly_tw_registration($month_first, $month_last);


        $year_back = strtotime('-2 years');
        $year_forward = strtotime('+2 years');

        $year_first = date('Y', $year_back);
        $year_last = date('Y', $year_forward);

        $data['year_first'] = $year_first;
        $data['year_last'] = $year_last;

        $data['yearly_registration'] = $this->graph_model->get_yearly_registration($year_first, $year_last);
        $data['yearly_fb_registration'] = $this->graph_model->get_yearly_fb_registration($year_first, $year_last);
        $data['yearly_tw_registration'] = $this->graph_model->get_yearly_tw_registration($year_first, $year_last);

        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Graphs', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'graphs/user_graph', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }


}

?>
