<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Equity_company extends ROCKERS_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('equity_company_model');
        $this->load->model('home_model');


    }

    
    /*
     Function name :list_company_category()
    Parameter :$msg (message string)
    Return : none
    Use : to show the display admin company category page
    Description : this is used to show the admin company category page to company category into admin panel
    */

    function list_company_category($msg = '')
    {


        $check_rights = get_rights('manage_dropdown');


        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }
        $data['site_setting'] = site_setting();
        check_admin_authentication();


        $data['result'] = $this->equity_company_model->getall_records('company_category');
        $data['msg'] = $msg;

        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', COMPANY_CATEGORY_LIST, '', TRUE);
        $this->template->write_view('header', 'header', '', TRUE);
        $this->template->write_view('main_content', 'equity_company/list_company_category', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();

    }

    /*
     Function name :add_company_category()
    Parameter :$id (id)
    Return : none
    Use : to show the admin add company category and update company category page
    Description : this is used to inset/update company category  at admin site admin panel.
    */
    function add_company_category($id = '')
    {
        $check_rights = get_rights('manage_dropdown');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }
        $data = '';
        $data['site_setting'] = site_setting();
        $check_validate = '';

        check_admin_authentication();


        $data['query'] = $this->home_model->get_language('language', array('active' => 1));
        $company_category_name = trim($this->input->post('company_category_name'));
        $data['id'] = $id;

        if ($_POST) {
            if ($id == '') {
                $result = $this->equity_company_model->record_exist($tab_name = 'company_category', $parameter = 'company_category_name', $company_category_name);
                if ($result == 1) {
                    $check_validate = COMPANY_CATEGORY_ALREADY_EXIST;
                }
            } else {
                $result = $this->equity_company_model->record_exist1($tab_name = 'company_category', $parameter = 'company_category_name', $company_category_name, $id);
                if ($result == 1) {
                    $check_validate = COMPANY_CATEGORY_ALREADY_EXIST;
                }
            }
        }

        $image_error = '';
        if ($_FILES) {
            if ($_FILES['image_load']['tmp_name']) {
                $needheight = 400;
                $needwidth = 600;
                $fn = $_FILES['image_load']['tmp_name'];
                if ($fn) {
                    $size = getimagesize($fn);
                    $actualwidth = $size[0];
                    $actualheight = $size[0];
                }

                if ($_FILES["image_load"]["type"] != "image/jpeg" and $_FILES["image_load"]["type"] != "image/pjpeg" and $_FILES["image_load"]["type"] != "image/png" and $_FILES["image_load"]["type"] != "image/x-png" and $_FILES["image_load"]["type"] != "image/gif") {
                    $image_error = PLEASE_UPLOAD_A_JPG_PNG_GIF_FILE;
                } else if ($_FILES["image_load"]["size"] > 2000000) {
                    $image_error = SORRY_THIS_FILE_IS_TOO_LARGE_PLEASE_SELECT_A_JPG_FILE_THAT_IS_LESS_THAN_TWO_MB_OR_TRY_RESIZING_IT_USING_A_PHOTO_EDITOR;

                } else if ($needwidth > $actualwidth || $needheight > $actualheight) {
                    $image_error = 'Please select image more than 600 * 400';
                }
            }
        } else {
            // $image_load = $this->input->post('dynamic_image_image');
            $image_error = '';
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('company_category_name', COMPANY_CATEGORY, 'required');

        if ($this->form_validation->run() == FALSE || $check_validate != '' || $image_error != '') {
            if (validation_errors() || $check_validate != '' || $image_error != '') {
                $data['error'] = validation_errors() . $check_validate . $image_error;
            } else {
                $data['error'] = '';
            }

        } else {
            $upload_img = '';

            if ($_FILES['image_load']['type'] != '') {
                $upload_img = $_FILES['image_load']['type'];
                $rand = rand(0, 100000);
                $type_img = explode('/', $_FILES['image_load']['type']);
                $new_img = 'category_' . $rand . '.' . $type_img[1];
                $base_path = $this->config->slash_item('base_path');
                require_once $base_path . 'thumbgenerator/ThumbLib.inc.php';

                move_uploaded_file($_FILES['image_load']['tmp_name'], $base_path . "upload/orig/" . $new_img);

                $new_w = 350;
                $new_h = 220;
                $thumb = PhpThumbFactory::create($base_path . "upload/orig/" . $new_img);
                $thumb->adaptiveResize($new_w, $new_h);
                $cache_path = $base_path . "upload/category/" . $new_img;
                $thumb->save($cache_path);

                $new_w = 900;
                $new_h = 285;
                $thumb = PhpThumbFactory::create($base_path . "upload/orig/" . $new_img);
                $thumb->adaptiveResize($new_w, $new_h);
                $cache_path = $base_path . "upload/category_search/" . $new_img;
                $thumb->save($cache_path);

                unlink($base_path . "upload/orig/" . $new_img);
            }

            $url_company_category = '';
            $url_company_category = makeSlugs($this->input->post('company_category_name'));
            $chk_url_exists = $this->db->query("select MAX(url_company_category) as url_company_category_title from company_category where url_company_category like '" . $url_company_category . "%'");

            if ($chk_url_exists->num_rows() > 0) {
                $get_pr = $chk_url_exists->row();
                $strre = str_replace($url_company_category, '', $get_pr->url_company_category_title);

                if ($strre == '' || $strre == '0') {
                    $newcnt = '';
                } else {
                    $newcnt = (int)$strre + 1;
                }
                $url_company_category = $url_company_category . $newcnt;

            }


            if ($upload_img) {
                $array1 = array(
                    'company_category_name' => $this->input->post('company_category_name'),
                    'url_company_category' => $url_company_category,
                    'language_id' => $this->input->post('language_id'),
                    'company_category_desc' => $this->input->post('company_category_desc'),
                    'image' => $new_img,
                    'status' => $this->input->post('status')
                );
            } else {
                $array1 = array(
                    'company_category_name' => $this->input->post('company_category_name'),
                    'url_company_category' => $url_company_category,
                    'language_id' => $this->input->post('language_id'),
                    'company_category_desc' => $this->input->post('company_category_desc'),
                    'status' => $this->input->post('status')
                );
            }

            if ($id) {

                $msg = $this->equity_company_model->add_company_category($id, $array1);
                redirect('admin/equity_company/list_company_category/' . $msg);

            } else {

                $company_category_name = '';
                $id = '';
                $msg = $this->equity_company_model->add_company_category($id, $array1);
                redirect('admin/equity_company/list_company_category/' . $msg);
            }
        }

        if ($id) {
            $query = $this->db->query("SELECT * FROM company_category WHERE id='" . $id . "'");
            $result = $query->row_array();


            $data["company_category_name"] = $result['company_category_name'];
            $data["company_category_desc"] = $result['company_category_desc'];
             $data["language_id"] = $result['language_id'];
            $data["status"] = $result['status'];
            $data["id"] = $result['id'];
            $data["image"] = $result['image'];

        } else {
            $data["company_category_name"] = $this->input->post('company_category_name');
             $data["company_category_desc"] = $this->input->post('company_category_desc');
            $data["status"] = $this->input->post('status');
             $data["language_id"] = $this->input->post('language_id');
            $data["id"] = $this->input->post('id');
             $data["image"] = '';

        }

        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', COMPANY_CATEGORY, '', TRUE);
        $this->template->write_view('header', 'header', '', TRUE);
        $this->template->write_view('main_content', 'equity_company/add_company_category', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();


    }

    /*
     Function name :delete_company_category()
    Parameter :$id (id)
    Return : none
    Use : to show the admin site delete company category.
    Description : this is used to delete company category into admin panel
    */

    function delete_company_category($id = 0)
    {
        $check_rights = get_rights('manage_dropdown');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }
        $data['site_setting'] = site_setting();
        check_admin_authentication();


        $this->db->delete('company_category', array('id' => $id));

        redirect('admin/equity_company/list_company_category/delete');

    }

    /*
     Function name :list_company_industry()
    Parameter :$msg (message string)
    Return : none
    Use : to show the display admin company industry page
    Description : this is used to show the admin company nndustry page to company industry into admin panel
    */

    function list_company_industry($msg = '')
    {
        $check_rights = get_rights('manage_dropdown');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }
        $data['site_setting'] = site_setting();
        check_admin_authentication();


        $data['result'] = $this->equity_company_model->getall_records('company_industry');
        $data['msg'] = $msg;

        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', COMPANY_INDUSTRY_LIST, '', TRUE);
        $this->template->write_view('header', 'header', '', TRUE);
        $this->template->write_view('main_content', 'equity_company/list_company_industry', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();

    }

    /*
     Function name :add_company_industry()
    Parameter :$id (id)
    Return : none
    Use : to show the admin add company industry and update company industry page
    Description : this is used to inset/update company industry  at admin site admin panel.
    */
    function add_company_industry($id = '')
    {
        $check_rights = get_rights('manage_dropdown');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }
        $data = '';
        $check_validate = '';
        $data['site_setting'] = site_setting();
        check_admin_authentication();


        $company_industry_name = trim($this->input->post('company_industry_name'));
        $data['id'] = $id;

        if ($_POST) {
            if ($id == '') {
                $result = $this->equity_company_model->record_exist($tab_name = 'company_industry', $parameter = 'company_industry_name', $company_industry_name);
                if ($result == 1) {
                    $check_validate = COMPANY_INDUSTRY_ALREADY_EXIST;
                }
            } else {
                $result = $this->equity_company_model->record_exist1($tab_name = 'company_industry', $parameter = 'company_industry_name', $company_industry_name, $id);
                if ($result == 1) {
                    $check_validate = COMPANY_INDUSTRY_ALREADY_EXIST;
                }
            }
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('company_industry_name', COMPANY_INDUSTRY, 'required');

        if ($this->form_validation->run() == FALSE || $check_validate != '') {
            if (validation_errors() || $check_validate != '') {
                $data['error'] = validation_errors() . $check_validate;
            } else {
                $data['error'] = '';
            }

        } else {
            if ($id) {
                $msg = $this->equity_company_model->add_company_industry($id);

                redirect('admin/equity_company/list_company_industry/' . $msg);
            } else {
                $company_industry_name = '';
                $id = '';
                $msg = $this->equity_company_model->add_company_industry($id);
                redirect('admin/equity_company/list_company_industry/' . $msg);
            }
        }

        if ($id) {
            $query = $this->db->query("SELECT * FROM company_industry WHERE id='" . $id . "'");
            $result = $query->result();

            foreach ($result as $rs) {
                $data["company_industry_name"] = $rs->company_industry_name;
                $data["status"] = $rs->status;
                $data["id"] = $rs->id;
            }
        } else {
            $data["company_industry_name"] = $this->input->post('company_industry_name');
            $data["status"] = $this->input->post('status');
            $data["id"] = $this->input->post('id');
        }

        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', COMPANY_CATEGORY, '', TRUE);
        $this->template->write_view('header', 'header', '', TRUE);
        $this->template->write_view('main_content', 'equity_company/add_company_industry', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();


    }

    /*
     Function name :delete_company_industry()
    Parameter :$id (id)
    Return : none
    Use : to show the admin site delete company industry.
    Description : this is used to delete company industry into admin panel
    */

    function delete_company_industry($id = 0)
    {
        $check_rights = get_rights('manage_dropdown');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }
        check_admin_authentication();
        $data['site_setting'] = site_setting();

        $this->db->delete('company_industry', array('id' => $id));

        redirect('admin/equity_company/list_company_industry/delete');

    }


    /*
     Function name :list_funding_source()
    Parameter :$msg (message string)
    Return : none
    Use : to show the display admin funding source page
    Description : this is used to show the admin funding source page to funding source into admin panel
    */

    function list_funding_source($msg = '')
    {
        $check_rights = get_rights('manage_dropdown');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }
        $data['site_setting'] = site_setting();
        check_admin_authentication();


        $data['result'] = $this->equity_company_model->getall_records('funding_source');
        $data['msg'] = $msg;

        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', FUNDING_SOURCE, '', TRUE);
        $this->template->write_view('header', 'header', '', TRUE);
        $this->template->write_view('main_content', 'equity_company/list_funding_source', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();

    }

    /*
     Function name :add_funding_source()
    Parameter :$id (id)
    Return : none
    Use : to show the admin add funding source and update funding source page
    Description : this is used to inset/update funding source at admin site admin panel.
    */

    function add_funding_source($id = '')
    {
        $check_rights = get_rights('manage_dropdown');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }
        $data = '';
        $check_validate = '';
        $data['site_setting'] = site_setting();
        check_admin_authentication();


        $funding_source_name = trim($this->input->post('funding_source_name'));
        $data['id'] = $id;

        if ($_POST) {
            if ($id == '') {
                $result = $this->equity_company_model->record_exist($tab_name = 'funding_source', $parameter = 'funding_source_name', $funding_source_name);
                if ($result == 1) {
                    $check_validate = FUNDING_SOURCE_ALREADY_EXIST;
                }
            } else {
                $result = $this->equity_company_model->record_exist1($tab_name = 'funding_source', $parameter = 'funding_source_name', $funding_source_name, $id);
                if ($result == 1) {
                    $check_validate = FUNDING_SOURCE_ALREADY_EXIST;
                }
            }
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('funding_source_name', FUNDING_SOURCE, 'required');

        if ($this->form_validation->run() == FALSE || $check_validate != '') {
            if (validation_errors() || $check_validate != '') {
                $data['error'] = validation_errors() . $check_validate;
            } else {
                $data['error'] = '';
            }

        } else {
            if ($id) {
                $msg = $this->equity_company_model->add_funding_source($id);

                redirect('admin/equity_company/list_funding_source/' . $msg);
            } else {
                $company_industry_name = '';
                $id = '';
                $msg = $this->equity_company_model->add_funding_source($id);
                redirect('admin/equity_company/list_funding_source/' . $msg);
            }
        }

        if ($id) {
            $query = $this->db->query("SELECT * FROM funding_source WHERE id='" . $id . "'");
            $result = $query->result();

            foreach ($result as $rs) {
                $data["funding_source_name"] = $rs->funding_source_name;
                $data["status"] = $rs->status;
                $data["id"] = $rs->id;
            }
        } else {
            $data["funding_source_name"] = $this->input->post('funding_source_name');
            $data["status"] = $this->input->post('status');
            $data["id"] = $this->input->post('id');
        }

        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', FUNDING_SOURCE, '', TRUE);
        $this->template->write_view('header', 'header', '', TRUE);
        $this->template->write_view('main_content', 'equity_company/add_funding_source', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }

    /*
    Function name :delete_funding_source()
   Parameter :$id (id)
   Return : none
   Use : to show the admin site delete funding source.
   Description : this is used to delete funding source into admin panel
   */

    function delete_funding_source($id = 0)
    {
        $check_rights = get_rights('manage_dropdown');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }
        $data['site_setting'] = site_setting();
        check_admin_authentication();


        $this->db->delete('funding_source', array('id' => $id));

        redirect('admin/equity_company/list_funding_source/delete');

    }


    /*
    Function name :list_funding_type()
   Parameter :$msg (message string)
   Return : none
   Use : to show the display admin funding type page
   Description : this is used to show the admin funding type page to funding type into admin panel
   */

    function list_funding_type($msg = '')
    {
        $check_rights = get_rights('manage_dropdown');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }
        $data['site_setting'] = site_setting();
        check_admin_authentication();


        $data['result'] = $this->equity_company_model->getall_records('funding_type');
        $data['msg'] = $msg;

        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', FUNDING_TYPE, '', TRUE);
        $this->template->write_view('header', 'header', '', TRUE);
        $this->template->write_view('main_content', 'equity_company/list_funding_type', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();

    }

    /*
    Function name :add_funding_type()
   Parameter :$id (id)
   Return : none
   Use : to show the admin add funding type and update funding type page
   Description : this is used to inset/update funding type at admin site admin panel.
   */

    function add_funding_type($id = '')
    {
        $check_rights = get_rights('manage_dropdown');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }
        $data = '';
        $check_validate = '';
        $data['site_setting'] = site_setting();
        check_admin_authentication();


        $funding_type_name = trim($this->input->post('funding_type_name'));
        $data['id'] = $id;

        if ($_POST) {
            if ($id == '') {
                $result = $this->equity_company_model->record_exist($tab_name = 'funding_type', $parameter = 'funding_type_name', $funding_type_name);
                if ($result == 1) {
                    $check_validate = FUNDING_TYPE_ALREADY_EXIST;
                }
            } else {
                $result = $this->equity_company_model->record_exist1($tab_name = 'funding_type', $parameter = 'funding_type_name', $funding_type_name, $id);
                if ($result == 1) {
                    $check_validate = FUNDING_TYPE_ALREADY_EXIST;
                }
            }
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('funding_type_name', FUNDING_TYPE, 'required');

        if ($this->form_validation->run() == FALSE || $check_validate != '') {
            if (validation_errors() || $check_validate != '') {
                $data['error'] = validation_errors() . $check_validate;
            } else {
                $data['error'] = '';
            }

        } else {
            if ($id) {
                $msg = $this->equity_company_model->add_funding_type($id);

                redirect('admin/equity_company/list_funding_type/' . $msg);
            } else {
                $funding_type_name = '';
                $id = '';
                $msg = $this->equity_company_model->add_funding_type($id);
                redirect('admin/equity_company/list_funding_type/' . $msg);
            }
        }

        if ($id) {
            $query = $this->db->query("SELECT * FROM funding_type WHERE id='" . $id . "'");
            $result = $query->result();

            foreach ($result as $rs) {
                $data["funding_type_name"] = $rs->funding_type_name;
                $data["status"] = $rs->status;
                $data["id"] = $rs->id;
            }
        } else {
            $data["funding_type_name"] = $this->input->post('funding_type_name');
            $data["status"] = $this->input->post('status');
            $data["id"] = $this->input->post('id');
        }

        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', FUNDING_TYPE, '', TRUE);
        $this->template->write_view('header', 'header', '', TRUE);
        $this->template->write_view('main_content', 'equity_company/add_funding_type', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }


    /*
    Function name :delete_funding_type()
   Parameter :$id (id)
   Return : none
   Use : to show the admin site delete funding type.
   Description : this is used to delete funding type into admin panel
   */

    function delete_funding_type($id = 0)
    {
        $check_rights = get_rights('manage_dropdown');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }
        $data['site_setting'] = site_setting();
        check_admin_authentication();


        $this->db->delete('funding_type', array('id' => $id));

        redirect('admin/equity_company/list_funding_type/delete');

    }


    /*
    Function name :list_document_type()
    Parameter :$msg (message string)
    Return : none
    Use : to show the display admin document type page
    Description : this is used to show the admin document type page to document type into admin panel
    */

    function list_document_type($msg = '')
    {
        $check_rights = get_rights('manage_dropdown');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }
        $data['site_setting'] = site_setting();
        check_admin_authentication();


        $data['result'] = $this->equity_company_model->getall_records('document_type');
        $data['msg'] = $msg;

        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', DOCUMENT_TYPE, '', TRUE);
        $this->template->write_view('header', 'header', '', TRUE);
        $this->template->write_view('main_content', 'equity_company/list_document_type', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();

    }

    /*
    Function name :add_document_type()
   Parameter :$id (id)
   Return : none
   Use : to show the admin add document type and update document type page
   Description : this is used to inset/update document type at admin site admin panel.
   */

    function add_document_type($id = '')
    {
        $check_rights = get_rights('manage_dropdown');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }
        $data = '';
        $check_validate = '';
        $data['site_setting'] = site_setting();
        check_admin_authentication();


        $document_type_name = trim($this->input->post('document_type_name'));
        $data['id'] = $id;

        if ($_POST) {
            if ($id == '') {
                $result = $this->equity_company_model->record_exist($tab_name = 'document_type', $parameter = 'document_type_name', $document_type_name);
                if ($result == 1) {
                    $check_validate = DOCUMENT_TYPE_ALREADY_EXIST;
                }
            } else {
                $result = $this->equity_company_model->record_exist1($tab_name = 'document_type', $parameter = 'document_type_name', $document_type_name, $id);
                if ($result == 1) {
                    $check_validate = DOCUMENT_TYPE_ALREADY_EXIST;
                }
            }
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('document_type_name', DOCUMENT_TYPE, 'required');

        if ($this->form_validation->run() == FALSE || $check_validate != '') {
            if (validation_errors() || $check_validate != '') {
                $data['error'] = validation_errors() . $check_validate;
            } else {
                $data['error'] = '';
            }

        } else {
            if ($id) {
                $msg = $this->equity_company_model->add_document_type($id);

                redirect('admin/equity_company/list_document_type/' . $msg);
            } else {
                $document_type_name = '';
                $id = '';
                $msg = $this->equity_company_model->add_document_type($id);
                redirect('admin/equity_company/list_document_type/' . $msg);
            }
        }

        if ($id) {
            $query = $this->db->query("SELECT * FROM document_type WHERE id='" . $id . "'");
            $result = $query->result();

            foreach ($result as $rs) {
                $data["document_type_name"] = $rs->document_type_name;
                $data["status"] = $rs->status;
                $data["id"] = $rs->id;
            }
        } else {
            $data["document_type_name"] = $this->input->post('document_type_name');
            $data["status"] = $this->input->post('status');
            $data["id"] = $this->input->post('id');
        }

        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', DOCUMENT_TYPE, '', TRUE);
        $this->template->write_view('header', 'header', '', TRUE);
        $this->template->write_view('main_content', 'equity_company/add_document_type', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }

    /*
    Function name :delete_document_type()
   Parameter :$id (id)
   Return : none
   Use : to show the admin site delete document type.
   Description : this is used to delete document type into admin panel
   */

    function delete_document_type($id = 0)
    {

        $check_rights = get_rights('manage_dropdown');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }
        check_admin_authentication();


        $this->db->delete('document_type', array('id' => $id));

        redirect('admin/equity_company/list_document_type/delete');

    }


    /*
   Function name :list_account_type()
   Parameter :$msg (message string)
   Return : none
   Use : to show the display admin account type page
   Description : this is used to show the admin account type page to account type into admin panel
   */

    function list_account_type($msg = '')
    {
        $data['site_setting'] = site_setting();
        check_admin_authentication();

        $check_rights = get_rights('manage_dropdown');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }


        $data['result'] = $this->equity_company_model->getall_records('account_type');
        $data['msg'] = $msg;

        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', ACCOUNT_TYPE, '', TRUE);
        $this->template->write_view('header', 'header', '', TRUE);
        $this->template->write_view('main_content', 'equity_company/list_account_type', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();

    }

    /*
    Function name :add_account_type()
   Parameter :$id (id)
   Return : none
   Use : to show the admin add account type and update account type page
   Description : this is used to inset/update account type at admin site admin panel.
   */

    function add_account_type($id = '')
    {
        $check_rights = get_rights('manage_dropdown');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }
        $data = '';
        $check_validate = '';
        $data['site_setting'] = site_setting();
        check_admin_authentication();


        $account_type_name = trim($this->input->post('account_type_name'));
        $data['id'] = $id;

        if ($_POST) {
            if ($id == '') {
                $result = $this->equity_company_model->record_exist($tab_name = 'account_type', $parameter = 'account_type_name', $account_type_name);
                if ($result == 1) {
                    $check_validate = ACCOUNT_TYPE_ALREADY_EXIST;
                }
            } else {
                $result = $this->equity_company_model->record_exist1($tab_name = 'account_type', $parameter = 'account_type_name', $account_type_name, $id);
                if ($result == 1) {
                    $check_validate = ACCOUNT_TYPE_ALREADY_EXIST;
                }
            }
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('account_type_name', ACCOUNT_TYPE, 'required');

        if ($this->form_validation->run() == FALSE || $check_validate != '') {
            if (validation_errors() || $check_validate != '') {
                $data['error'] = validation_errors() . $check_validate;
            } else {
                $data['error'] = '';
            }

        } else {
            if ($id) {
                $msg = $this->equity_company_model->add_account_type($id);

                redirect('admin/equity_company/list_account_type/' . $msg);
            } else {
                $account_type_name = '';
                $id = '';
                $msg = $this->equity_company_model->add_account_type($id);
                redirect('admin/equity_company/list_account_type/' . $msg);
            }
        }

        if ($id) {
            $query = $this->db->query("SELECT * FROM account_type WHERE id='" . $id . "'");
            $result = $query->result();

            foreach ($result as $rs) {
                $data["account_type_name"] = $rs->account_type_name;
                $data["status"] = $rs->status;
                $data["id"] = $rs->id;
            }
        } else {
            $data["account_type_name"] = $this->input->post('account_type_name');
            $data["status"] = $this->input->post('status');
            $data["id"] = $this->input->post('id');
        }

        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', ACCOUNT_TYPE, '', TRUE);
        $this->template->write_view('header', 'header', '', TRUE);
        $this->template->write_view('main_content', 'equity_company/add_account_type', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }

    /*
    Function name :delete_account_type()
    Parameter :$id (id)
    Return : none
    Use : to show the admin site delete account type.
    Description : this is used to delete account type into admin panel
     */

    function delete_account_type($id = 0)
    {

        check_admin_authentication();

        $check_rights = get_rights('manage_dropdown');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        $this->db->delete('account_type', array('id' => $id));

        redirect('admin/equity_company/list_account_type/delete');

    }

    /*
    Function name :list_team_member_type()
    Parameter :$msg (message string)
    Return : none
    Use : to show the display admin team member type page
    Description : this is used to show the admin team member type page to team member type into admin panel
    */

    function list_team_member_type($msg = '')
    {
        $data['site_setting'] = site_setting();
        check_admin_authentication();

        $check_rights = get_rights('manage_dropdown');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        $data['result'] = $this->equity_company_model->getall_records('team_member_type');

        $data['msg'] = $msg;

        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', TEAM_MEMBER_TYPE, '', TRUE);
        $this->template->write_view('header', 'header', '', TRUE);
        $this->template->write_view('main_content', 'equity_company/list_team_member_type', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }

    /*
     Function name :add_team_member_type()
    Parameter :$id (id)
    Return : none
    Use : to show the admin add team member type and update team member type page
    Description : this is used to inset/update team member type at admin site admin panel.
    */

    function add_team_member_type($id = '')
    {
        $data['site_setting'] = site_setting();
        $check_validate = '';

        check_admin_authentication();

        $check_rights = get_rights('manage_dropdown');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        $team_member_type_name = trim($this->input->post('team_member_type_name'));
        if ($_POST) {
            if ($id == '') {
                $result = $this->equity_company_model->record_exist($tab_name = 'team_member_type', $parameter = 'team_member_type_name', $team_member_type_name);
                if ($result == 1) {
                    $check_validate = TEAM_MEMBER_TYPE_ALREADY_EXIST;
                }
            } else {
                $result = $this->equity_company_model->record_exist1($tab_name = 'team_member_type', $parameter = 'team_member_type_name', $team_member_type_name, $id);

                if ($result == 1) {
                    $check_validate = TEAM_MEMBER_TYPE_ALREADY_EXIST;
                }
            }
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('team_member_type_name', TEAM_MEMBER_TYPE, 'required');

        if ($this->form_validation->run() == False || $check_validate != '') {
            if (validation_errors() || $check_validate != '') {
                $data['error'] = validation_errors() . $check_validate;

            } else {
                $data['error'] = '';
            }
        } else {
            if ($id) {
                $msg = $this->equity_company_model->add_team_member_type($id);

                redirect('admin/equity_company/list_team_member_type/' . $msg);
            } else {
                $team_member_type_name = '';
                $id = '';
                $msg = $this->equity_company_model->add_team_member_type($id);
                redirect('admin/equity_company/list_team_member_type/' . $msg);
            }
        }
        if ($id) {
            $query = $this->db->query("SELECT * FROM team_member_type WHERE id='" . $id . "'");
            $result = $query->result();

            foreach ($result as $rs) {
                $data["team_member_type_name"] = $rs->team_member_type_name;
                $data["status"] = $rs->status;
                $data["id"] = $rs->id;
            }
        } else {
            $data["team_member_type_name"] = $this->input->post('team_member_type_name');
            $data["status"] = $this->input->post('status');
            $data["id"] = $this->input->post('id');
        }

        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', TEAM_MEMBER_TYPE, '', TRUE);
        $this->template->write_view('header', 'header', '', TRUE);
        $this->template->write_view('main_content', 'equity_company/add_team_member_type', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();

    }

    /*
    Function name :delete_team_member_type()
    Parameter :$id (id)
    Return : none
    Use : to show the admin site delete team member type.
    Description : this is used to delete team member type into admin panel
     */

    function delete_team_member_type($id = '')
    {

        check_admin_authentication();

        $check_rights = get_rights('manage_dropdown');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        $this->db->delete('team_member_type', array('id' => $id));
        redirect('admin/equity_company/list_team_member_type/delete');
    }




    //18th may,15 investor type

    /*
     Function name :list_investor_type()
    Parameter :$msg (message string)
    Return : none
    Use : to show the display admin company industry page
    Description : this is used to show the admin company nndustry page to company industry into admin panel
    */
    function list_investor_type($msg = '')
    {
        $data['site_setting'] = site_setting();
        check_admin_authentication();

        $check_rights = get_rights('manage_dropdown');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        $data['result'] = $this->equity_company_model->getall_records('investor_type');
        $data['msg'] = $msg;

        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', INVESTOR_TYPE, '', TRUE);
        $this->template->write_view('header', 'header', '', TRUE);
        $this->template->write_view('main_content', 'equity_company/list_investor_type', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }

    /*
    Function name :add_investor_type()
    Parameter :$id (id)
    Return : none
    Use : to show the admin add company industry and update company industry page
    Description : this is used to inset/update company industry  at admin site admin panel.
    */
    function add_investor_type($id = '')
    {
        $check_rights = get_rights('manage_dropdown');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }
        $data = '';
        $check_validate = '';
        $data['site_setting'] = site_setting();

        check_admin_authentication();


        $investor_type_name = trim($this->input->post('investor_type_name'));
        $data['id'] = $id;

        if ($_POST) {
            if ($id == '') {
                $result = $this->equity_company_model->record_exist($tab_name = 'investor_type', $parameter = 'investor_type_name', $investor_type_name);
                if ($result == 1) {
                    $check_validate = INVESTOR_TYPE_NAME_ALREADY_EXIST;
                }
            } else {
                $result = $this->equity_company_model->record_exist1($tab_name = 'investor_type', $parameter = 'investor_type_name', $investor_type_name, $id);
                if ($result == 1) {
                    $check_validate = INVESTOR_TYPE_NAME_ALREADY_EXIST;
                }
            }
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('investor_type_name', INVESTOR_TYPE_NAME, 'required');

        if ($this->form_validation->run() == FALSE || $check_validate != '') {
            if (validation_errors() || $check_validate != '') {
                $data['error'] = validation_errors() . $check_validate;
            } else {
                $data['error'] = '';
            }

        } else {

            $array1 = array(
                'investor_type_name' => $this->input->post('investor_type_name'),
                'status' => $this->input->post('status')
            );
            if ($id) {
                $msg = $this->equity_company_model->add_investor_type($id, $array1);

                redirect('admin/equity_company/list_investor_type/' . $msg);
            } else {
                $company_industry_name = '';
                $id = '';
                $msg = $this->equity_company_model->add_investor_type($id, $array1);
                redirect('admin/equity_company/list_investor_type/' . $msg);
            }
        }


        if ($id) {
            $query = $this->db->query("SELECT * FROM investor_type WHERE id='" . $id . "'");
            $result = $query->result();

            foreach ($result as $rs) {
                $data["investor_type_name"] = $rs->investor_type_name;
                $data["status"] = $rs->status;
                $data["id"] = $rs->id;
            }
        } else {
            $data["investor_type_name"] = $this->input->post('investor_type_name');
            $data["status"] = $this->input->post('status');
            $data["id"] = $this->input->post('id');
        }

        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', INVESTOR_TYPE, '', TRUE);
        $this->template->write_view('header', 'header', '', TRUE);
        $this->template->write_view('main_content', 'equity_company/add_investor_type', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }


    /*
    Function name :delete_investor_type()
    Parameter :$id (id)
    Return : none
    Use : to show the admin site delete Investor Type.
    Description : this is used to delete Investor Type into admin panel
    */
    function delete_investor_type($id = '')
    {

        check_admin_authentication();

        $check_rights = get_rights('manage_dropdown');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        $this->db->delete('investor_type', array('id' => $id));

        redirect('admin/equity_company/list_investor_type/delete');

    }

}

?>
