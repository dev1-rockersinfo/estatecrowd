<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Email_template extends ROCKERS_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('home_model');
    }

    function index()
    {
        redirect('admin/email_template/list_email_template/');
    }

    /*
     Function name :add_email_template()
    Parameter :$id=Email template id that admin wants to edit
    Return : none
    Use : Function used for editing or updating particular email template's content.
    */
    function add_email_template($id = 0)
    {

        $check_rights = get_rights('add_email_template');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        $data["error"] = "";
        $this->load->library('form_validation');
        $data["success"] = '';
        $this->form_validation->set_rules('from_address', FROM_ADDRESS, 'required|valid_email');
        $this->form_validation->set_rules('reply_address', REPLY_ADDRESS, 'required|valid_email');
        $this->form_validation->set_rules('subject', SUBJECT, 'required');
        $this->form_validation->set_rules('message', MESSAGE, 'required');
        if ($this->form_validation->run() == FALSE) {
            if (validation_errors()) {
                $data["error"] = validation_errors();
            } else {
                $data["error"] = "";
            }
            if ($this->input->post('email_template_id')) {
                $data["email_template_id"] = $this->input->post('email_template_id');
                $data["from_address"] = $this->input->post('from_address');
                $data["reply_address"] = $this->input->post('reply_address');
                $data["subject"] = $this->input->post('subject');
                $data["message"] = $this->input->post('message');
            } else {
                $one_email_template = $this->home_model->get_one_tabledata('email_template', array('email_template_id' => $id));
                $data["email_template_id"] = $one_email_template['email_template_id'];
                $data["from_address"] = $one_email_template['from_address'];
                $data["reply_address"] = $one_email_template['reply_address'];
                $data["subject"] = $one_email_template['subject'];
                $data["message"] = $one_email_template['message'];
            }

            $data['site_setting'] = site_setting();
            $theme = 'admin';

            $this->template->set_master_template($theme . '/template.php');
            $data["template"] = get_table_data('email_template', array('email_template_id' => 'asc'));
            $this->template->write('title', 'Email Templates', '', TRUE);
            $this->template->write_view('header', 'header', $data, TRUE);
            $this->template->write_view('main_content', 'email_template/add_email_template', $data, TRUE);
            $this->template->write_view('footer', 'footer', '', TRUE);
            $this->template->render();
        } else {
            $data1 = array(
                'from_address' => $this->input->post('from_address'),
                'reply_address' => $this->input->post('reply_address'),
                'subject' => $this->input->post('subject'),
                'message' => $this->input->post('message'),
            );
            $this->home_model->table_update('email_template_id', $this->input->post('email_template_id'), 'email_template', $data1);
            redirect('admin/email_template/list_email_template/update');
        }
    }

    /*
     Function name :list_email_template()
    Parameter :$msg=message string to notify admin about the operation he performed.
    Return : none
    Use : Function is used for listing all the email templates.
    */
    function list_email_template($msg = '')
    {

        $check_rights = get_rights('add_email_template');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        //$data['result'] = $this->pages_model->get_pages_result($offset, $limit);
        $data['result'] = get_table_data('email_template', array('email_template_id' => 'asc'));
        $data['msg'] = $msg;

        $data['site_setting'] = site_setting();

        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Pages', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'email_template/list_email_template', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }

    /*
     Function name :update_email_address()
    Parameter :none
    Return : none
    Use : Function is used to update from_address and reply_address
    of all the email templates.
    */
    function update_email_address()
    {

        $check_rights = get_rights('add_email_template');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }
        $data = array();
        $data["error"] = "";
        $data["from_address"] = "";
        $data["success"] = "";
        $data["reply_address"] = "";
        $this->load->library('form_validation');
        $this->form_validation->set_rules('from_address', FROM_ADDRESS, 'required|valid_email');
        $this->form_validation->set_rules('reply_address', REPLY_ADDRESS, 'required|valid_email');

        if ($_POST) {
            if ($this->form_validation->run() == FALSE) {
                if (validation_errors()) {
                    $data["error"] = validation_errors();
                } else {
                    $data["error"] = "";
                }
                $data["from_address"] = $this->input->post('from_address');
                $data["reply_address"] = $this->input->post('reply_address');

            } else {
                //$this->email_template_model->email_template_update();
                $data1 = array(
                    'from_address' => $this->input->post('from_address'),
                    'reply_address' => $this->input->post('reply_address'),
                );
                $this->db->update('email_template', $data1);
                redirect('admin/email_template/list_email_template/update');
            }
        }

        $data['site_setting'] = site_setting();

        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Email Templates', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'email_template/update_email_address', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }
}

?>
