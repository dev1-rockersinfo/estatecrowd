<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Activity extends ROCKERS_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('activity_model');
        //$this->load->model('user_model');
    }

    function index()
    {
        redirect('admin/activity/list_activity');

    }

    /*
     Function name :list_message()
    Parameter :$msg:message string to notify user about the operation he performed.
    Return : none
    Use : to get list of messages send by one user to another can be viewed by admin.
    */
    function list_activity($msg = '')
    {
        //$msg=unserialize(urldecode($msg));

        $check_rights = get_rights('list_activity');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        $data_read = array('read' => 1);
        $this->db->update('admin_activity', $data_read);

        //$data['result'] = $this->message_model->get_message_result($offset, $limit);

        $data['activities'] = $this->activity_model->admin_activities();


        $data['site_setting'] = site_setting();

        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Messages', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'activity/list_activity', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }


}

?>
