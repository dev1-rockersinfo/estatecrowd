<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Spam extends ROCKERS_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('spam_model');
        $this->load->model('home_model');
        $this->load->model('user_model');

    }

    function index()
    {
        redirect('spam/add_spam_setting');
    }


    function add_spammer()
    {

        $check_rights = get_rights('add_spam_setting');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('spam_ip', SPAM_IP, 'required|valid_ip');


        if ($this->form_validation->run() == FALSE) {
            if (validation_errors()) {
                $data["error"] = validation_errors();
            } else {
                $data["error"] = "";
            }

            $data["spam_ip"] = $this->input->post('spam_ip');

            $data["permenant_spam"] = $this->input->post('permenant_spam');


            $data['site_setting'] = site_setting();
            $theme = 'admin';

            $this->template->set_master_template($theme . '/template.php');
            $this->template->write('title', 'Add Spammer', '', TRUE);
            $this->template->write_view('header', 'header', $data, TRUE);
            $this->template->write_view('main_content', 'spam/add_spammer', $data, TRUE);
            $this->template->write_view('footer', 'footer', '', TRUE);
            $this->template->render();
        } else {


            $this->spam_model->add_spammer();
            setting_deletecache('spam_protectd');
            $offset = 0;
            $msg = 'success';
            redirect('admin/spam/spamer/' . $msg);
            //redirect('spam/spamer/0/success');


        }


    }


    function add_spam_setting()
    {
        $check_rights = get_rights('add_spam_setting');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        $this->load->library('form_validation');
        $data["success"] = "";
        $data["error"] = "";
        $this->form_validation->set_rules('spam_report_total', TOTAL_SPAM_REPORT_ALLOW, 'required|is_natural_no_zero');
        $this->form_validation->set_rules('spam_report_expire', REPORTED_EXPIRE, 'required|is_natural_no_zero');

        $this->form_validation->set_rules('total_register', TOTAL_REGISTRATION, 'required|is_natural_no_zero');
        $this->form_validation->set_rules('register_expire', REGISTRATION_SPAM_EXPIRE, 'required|is_natural_no_zero');

        $this->form_validation->set_rules('total_comment', TOTAL_COMMNET, 'required|is_natural_no_zero');
        $this->form_validation->set_rules('comment_expire', COMMENT_SPAM_EXPIRE, 'required|is_natural_no_zero');

        $this->form_validation->set_rules('total_contact', TOTAL_INQUIRY, 'required|is_natural_no_zero');
        $this->form_validation->set_rules('contact_expire', INQUIRY_SPAM_EXPIRE, 'required|is_natural_no_zero');

        if ($this->form_validation->run() == FALSE) {
            if (validation_errors()) {
                $data["error"] = validation_errors();
            } else {
                $data["error"] = "";
            }
            if ($this->input->post('spam_control_id')) {

                $spam_control = $this->spam_model->get_spam_control();

                $data["spam_control_id"] = $this->input->post('spam_control_id');

                $data["spam_report_total"] = $this->input->post('spam_report_total');
                $data["spam_report_expire"] = $this->input->post('spam_report_expire');

                $data["total_register"] = $this->input->post('total_register');
                $data["register_expire"] = $this->input->post('register_expire');

                $data["total_comment"] = $this->input->post('total_comment');
                $data["comment_expire"] = $this->input->post('comment_expire');

                $data["total_contact"] = $this->input->post('total_contact');
                $data["contact_expire"] = $this->input->post('contact_expire');

            } else {
                $spam_control = $this->spam_model->get_spam_control();

                $data["spam_control_id"] = $spam_control['spam_control_id'];

                $data["spam_report_total"] = $spam_control['spam_report_total'];
                $data["spam_report_expire"] = $spam_control['spam_report_expire'];

                $data["total_register"] = $spam_control['total_register'];
                $data["register_expire"] = $spam_control['register_expire'];

                $data["total_comment"] = $spam_control['total_comment'];
                $data["comment_expire"] = $spam_control['comment_expire'];

                $data["total_contact"] = $spam_control['total_contact'];
                $data["contact_expire"] = $spam_control['contact_expire'];


            }


            $data['site_setting'] = site_setting();
            $theme = 'admin';

            $this->template->set_master_template($theme . '/template.php');
            $this->template->write('title', 'Spam Settings', '', TRUE);
            $this->template->write_view('header', 'header', $data, TRUE);
            $this->template->write_view('main_content', 'spam/add_spam_setting', $data, TRUE);
            $this->template->write_view('footer', 'footer', '', TRUE);
            $this->template->render();
        } else {


            $this->spam_model->spam_control_update();


            $data["success"] = RECORD_HAS_BEEN_UPDATED_SUCCESSFULLY;

            $spam_control = $this->spam_model->get_spam_control();

            $data["spam_control_id"] = $spam_control['spam_control_id'];

            $data["spam_report_total"] = $spam_control['spam_report_total'];
            $data["spam_report_expire"] = $spam_control['spam_report_expire'];

            $data["total_register"] = $spam_control['total_register'];
            $data["register_expire"] = $spam_control['register_expire'];

            $data["total_comment"] = $spam_control['total_comment'];
            $data["comment_expire"] = $spam_control['comment_expire'];

            $data["total_contact"] = $spam_control['total_contact'];
            $data["contact_expire"] = $spam_control['contact_expire'];


            $data['site_setting'] = site_setting();

            $theme = 'admin';

            $this->template->set_master_template($theme . '/template.php');
            $this->template->write('title', 'Spam Settings', '', TRUE);
            $this->template->write_view('header', 'header', $data, TRUE);
            $this->template->write_view('main_content', 'spam/add_spam_setting', $data, TRUE);
            $this->template->write_view('footer', 'footer', '', TRUE);
            $this->template->render();
        }
    }


    function spam_report($msg = '')
    {
        $check_rights = get_rights('spam_report');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        /*	$this->load->library('pagination');

            //$limit=20;

            $config['uri_segment']='4';
            $config['base_url'] = site_url('spam/spam_report/'.$limit.'/');
            $config['total_rows'] = $this->spam_model->get_total_spam_report_count();
            $config['per_page'] = $limit;
            $this->pagination->initialize($config);
            $data['page_link'] = $this->pagination->create_links();
        */
        $data['result'] = $this->spam_model->get_spam_report_result();
        $data['msg'] = $msg;
        $data['option'] = '';
        $data['keyword'] = '';
        $data['search_type'] = 'normal';

        $data['site_setting'] = site_setting();
        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Spam Report', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'spam/list_spam_report', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();

    }

    /*
        function search_list_spam_report($limit=20,$option='',$keyword='',$offset=0,$msg='')
        {

            $check_rights=$this->home_model->get_rights('list_state');

            if(	$check_rights==0) {
                redirect('home/dashboard/no_rights');
            }



            $this->load->library('pagination');


            if($_POST)
            {
                $option=$this->input->post('option');
                $keyword=$this->input->post('keyword');
            }
            else
            {
                $option=$option;
                $keyword=$keyword;
            }

            $keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',trim($keyword)));

            $config['uri_segment']='6';
            $config['base_url'] = site_url('spam/search_list_spam_report/'.$limit.'/'.$option.'/'.$keyword.'/');
            $config['total_rows'] = $this->spam_model->get_total_spam_spamreport_count($option,$keyword);
            $config['per_page'] = $limit;
            $this->pagination->initialize($config);
            $data['page_link'] = $this->pagination->create_links();

            $data['result'] = $this->spam_model->get_spam_spamreport_result($option,$keyword,$offset, $limit);
            $data['msg'] = $msg;
            $data['offset'] = $offset;


            //$data['statelist']=$this->project_category_model->get_state();

            $data['site_setting'] = $this->home_model->select_site_setting();

            $data['limit']=$limit;
            $data['option']=$option;
            $data['keyword']=$keyword;
            $data['search_type']='search';

            $this->template->write('title', 'Search Spam', '', TRUE);
            $this->template->write_view('header', 'header', $data, TRUE);
            $this->template->write_view('main_content', 'list_spam_report', $data, TRUE);
            $this->template->write_view('footer', 'footer', '', TRUE);
            $this->template->render();
        }

    */
    function spam_report_action()
    {

        $check_rights = get_rights('spam_report');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }


        $offset = $this->input->post('offset');
        $action = $this->input->post('action');
        $spam_report_ip = $this->input->post('chk');


        if ($action == 'delete') {
            foreach ($spam_report_ip as $ip) {
                $this->spam_model->delete_spam_report($ip);
            }

            //redirect('spam/spam_report/'.$offset.'/delete');

            redirect('admin/spam/spam_report/delete');
        }

        if ($action == 'make_spam') {
            foreach ($spam_report_ip as $ip) {
                $this->spam_model->make_spam($ip);
            }

            //redirect('spam/spam_report/'.$offset.'/make_spam');
            redirect('admin/spam/spam_report/make_spam');
        }


    }


    function spamer($msg = '')
    {
        $check_rights = get_rights('spamer');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        /*	$this->load->library('pagination');

            $config['uri_segment']='4';
            $config['base_url'] = site_url('spam/spamer/'.$limit.'/');
            $config['total_rows'] = $this->spam_model->get_total_spamer_count();
            $config['per_page'] = $limit;
            $this->pagination->initialize($config);
            $data['page_link'] = $this->pagination->create_links();
        */
        $data['result'] = $this->spam_model->get_spamer_result();
        $data['msg'] = $msg;

        $data['option'] = '';
        $data['keyword'] = '';
        $data['search_type'] = 'normal';

        $data['site_setting'] = site_setting();
        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Spamer', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'spam/list_spamer', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();

    }

    /*
        function search_list_spam($limit=20,$option='',$keyword='',$offset=0,$msg='')
        {

            $check_rights=$this->home_model->get_rights('list_state');

            if(	$check_rights==0) {
                redirect('home/dashboard/no_rights');
            }



            $this->load->library('pagination');


            if($_POST)
            {
                $option=$this->input->post('option');
                $keyword=$this->input->post('keyword');
            }
            else
            {
                $option=$option;
                $keyword=$keyword;
            }

            $keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',trim($keyword)));

            $config['uri_segment']='6';
            $config['base_url'] = site_url('spam/search_list_spam/'.$limit.'/'.$option.'/'.$keyword.'/');
            $config['total_rows'] = $this->spam_model->get_total_search_spam_count($option,$keyword);
            $config['per_page'] = $limit;
            $this->pagination->initialize($config);
            $data['page_link'] = $this->pagination->create_links();

            $data['result'] = $this->spam_model->get_search_spam_result($option,$keyword,$offset, $limit);
            $data['msg'] = $msg;
            $data['offset'] = $offset;


            //$data['statelist']=$this->project_category_model->get_state();

            $data['site_setting'] = $this->home_model->select_site_setting();

            $data['limit']=$limit;
            $data['option']=$option;
            $data['keyword']=$keyword;
            $data['search_type']='search';

            $this->template->write('title', 'Search Spam', '', TRUE);
            $this->template->write_view('header', 'header', $data, TRUE);
            $this->template->write_view('main_content', 'list_spamer', $data, TRUE);
            $this->template->write_view('footer', 'footer', '', TRUE);
            $this->template->render();
        }
        */
    function spamer_action()
    {

        $check_rights = get_rights('spamer');
        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        $offset = $this->input->post('offset');
        $action = $this->input->post('action');
        $spam_ip = $this->input->post('chk');

        if ($action == 'delete') {
            foreach ($spam_ip as $ip) {
                $this->spam_model->delete_spamer($ip);
            }
            setting_deletecache('spam_protectd');
            redirect('admin/spam/spamer/delete');
        }

        if ($action == 'make_spam_permenant') {
            foreach ($spam_ip as $ip) {
                $this->spam_model->make_spam_permenant($ip);
            }
            setting_deletecache('spam_protectd');
            redirect('admin/spam/spamer/make_spam_permenant');
        }

    }


}

?>
