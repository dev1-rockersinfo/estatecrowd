<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Cronjob extends ROCKERS_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('cronjob_model');
        $this->load->model('home_model');

    }

    function index()
    {
        redirect('admin/cronjob/list_cronjob/');
    }

    /*
     Function name :list_cronjob()
    Parameter : $msg=message string.
    Return : none
    Use : to list the details of cronjob executed.
    */
    function list_cronjob($msg = '')
    {

        $check_rights = get_rights('list_cronjob');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }


        $data['result'] = $this->cronjob_model->get_cronjob_result();
        $data['msg'] = $msg;
        //$data['offset'] = $offset;
        //$data['limit']=$limit;
        $data['option'] = '';
        $data['keyword'] = '';
        $data['search_type'] = 'normal';
        $data['crons'] = $this->cronjob_model->get_cron_function();

        $data['site_setting'] = site_setting();

        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Cron Jobs', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'other/list_cronjob', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }

    /*
     Function name :action_cronjob()
    Parameter : none.
    Return : none;
    Use : Action admin wants to perform on user cronjob page like deleting cronjob records..
    */
    function action_cronjob()

    {

        $check_rights = get_rights('list_cronjob');

        if ($check_rights == 0) {

            redirect('admin/home/dashboard/no_rights');

        }

        $action = $this->input->post('action');

        $cronjob_id = $this->input->post('chk');

        if ($action == 'delete') {
            foreach ($cronjob_id as $id) {
                $this->db->query("delete from cronjob where cronjob_id='" . $id . "'");
            }
            redirect('admin/cronjob/list_cronjob/delete');
        }
    }


    /*
     Function name :run()
    Parameter : none.
    Return : none
    Use : To execute the option of cronjob selected by admin.
    type 1:Preapprove the Ended Project
    type 2:Affiliate Update Earn Status
    */
    function run()
    {
        // print_r($_POST); die;
        $check_rights = get_rights('list_cronjob');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        $function = $this->input->post('option');

        redirect('admin/cronjob/' . $function);

    }


    /*
     Function name :set_auto_ending()
    Parameter : none.
    Return : none
    Use : Set or update ended projects and update there status and refund funds to users, project owner and admin.
    */
    function set_auto_ending()
    {

        redirect('cron/cron_preapprove/yes');
    }

}

?>
