<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Email_setting extends ROCKERS_Controller
{

    function __construct()

    {

        parent::__construct();

        //$this->load->model('email_setting_model');

        $this->load->model('home_model');


    }


    function index()

    {

        redirect('admin/email_setting/add_email_setting/');

    }


    /*

     Function name :add_email_setting()

    Parameter : none.

    Return : none

    Use : this function is used to update email settings.

    */

    function add_email_setting()

    {


        $check_rights = get_rights('add_email_setting');


        if ($check_rights == 0) {

            redirect('admin/home/dashboard/no_rights');

        }


        $data['site_setting'] = site_setting();


        $this->load->library('form_validation');


        $this->form_validation->set_rules('mailer', MAILER_TYPE, 'required');


        if ($this->input->post('mailer') == 'sendmail') {

            $this->form_validation->set_rules('sendmail_path', SENDMAIL_PATH, 'required');

        }


        if ($this->input->post('mailer') == 'smtp') {

            $this->form_validation->set_rules('smtp_port', SMTP_PORT, 'required|is_natural_no_zero');

            $this->form_validation->set_rules('smtp_host', SMTP_HOST, 'required');

            $this->form_validation->set_rules('smtp_email', SMTP_EMAIL, 'required|valid_email');

            $this->form_validation->set_rules('smtp_password', SMTP_PASSWORD, 'required');

        }


        if ($this->form_validation->run() == FALSE) {

            if (validation_errors()) {

                $data["error"] = validation_errors();

            } else {

                $data["error"] = "";

            }

            if ($this->input->post('email_setting_id')) {

                $data["email_setting_id"] = $this->input->post('email_setting_id');

                $data["mailer"] = $this->input->post('mailer');

                $data["sendmail_path"] = $this->input->post('sendmail_path');

                $data["smtp_port"] = $this->input->post('smtp_port');

                $data["smtp_host"] = $this->input->post('smtp_host');

                $data["smtp_email"] = $this->input->post('smtp_email');

                $data["smtp_password"] = $this->input->post('smtp_password');


            } else {


                $email_setting = email_setting();


                $data["email_setting_id"] = $email_setting['email_setting_id'];

                $data["mailer"] = $email_setting['mailer'];

                $data["sendmail_path"] = $email_setting['sendmail_path'];

                $data["smtp_port"] = $email_setting['smtp_port'];

                $data["smtp_host"] = $email_setting['smtp_host'];

                $data["smtp_email"] = $email_setting['smtp_email'];

                $data["smtp_password"] = $email_setting['smtp_password'];


            }


        } else {

            $data1 = array(

                'mailer' => $this->input->post('mailer'),

                'sendmail_path' => $this->input->post('sendmail_path'),

                'smtp_port' => $this->input->post('smtp_port'),

                'smtp_host' => $this->input->post('smtp_host'),

                'smtp_email' => $this->input->post('smtp_email'),

                'smtp_password' => $this->input->post('smtp_password'),

            );

            $this->home_model->table_update('email_setting_id', $this->input->post('email_setting_id'), 'email_setting', $data1);


            $data["error"] = "success";

            $data["email_setting_id"] = $this->input->post('email_setting_id');


            $data["mailer"] = $this->input->post('mailer');

            $data["sendmail_path"] = $this->input->post('sendmail_path');

            $data["smtp_port"] = $this->input->post('smtp_port');

            $data["smtp_host"] = $this->input->post('smtp_host');

            $data["smtp_email"] = $this->input->post('smtp_email');

            $data["smtp_password"] = $this->input->post('smtp_password');

        }

        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');


        $this->template->write('title', 'Administrator', '', TRUE);

        $this->template->write_view('header', 'header', $data, TRUE);

        $this->template->write_view('main_content', 'setting/add_email_setting', $data, TRUE);

        $this->template->write_view('footer', 'footer', '', TRUE);

        $this->template->render();

    }



    /*Tesing Mail Functionality*/

    /*

     Function name :send_test_mail()

    Parameter : none.

    Return : none

    Use : this function is used to check email functionality from admin side..

    */

    function send_test_mail()

    {

        $check_rights = get_rights('add_email_setting');

        if ($check_rights == 0) {

            redirect('admin/home/dashboard/no_rights');

        }

        $this->load->library('form_validation');


        $this->form_validation->set_rules('sender_mail', SENDMAIL_MAIL, 'required|valid_email');

        $this->form_validation->set_rules('receiver_email', RECEIVER_MAIL, 'required|valid_email');

        $this->form_validation->set_rules('message_text', MESSAGE, 'required');


        if ($this->form_validation->run() == FALSE) {

            if (validation_errors()) {

                $data["error"] = validation_errors();

            } else {

                $data["error"] = "";

            }


            $data['site_setting'] = site_setting();

            $email_setting = email_setting();

            $data["email_setting_id"] = $email_setting['email_setting_id'];


            $theme = 'admin';

            $this->template->set_master_template($theme . '/template.php');


            $this->template->write('title', 'Administrator', '', TRUE);

            $this->template->write_view('header', 'header', $data, TRUE);

            $this->template->write_view('main_content', 'setting/send_testing_email', $data, TRUE);

            $this->template->write_view('footer', 'footer', '', TRUE);

            $this->template->render();

        } else {


            //$this->email_setting_model->email_setting_update();


            $email_setting = email_setting();


            $data["email_setting_id"] = $email_setting['email_setting_id'];

            $data["mailer"] = $email_setting['mailer'];

            $data["sendmail_path"] = $email_setting['sendmail_path'];

            $data["smtp_port"] = $email_setting['smtp_port'];

            $data["smtp_host"] = $email_setting['smtp_host'];

            $data["smtp_email"] = $email_setting['smtp_email'];

            $data["smtp_password"] = $email_setting['smtp_password'];


            /*Send Testing Mail*/

            $this->load->library('email');

            ///////====smtp====


            if ($email_setting['mailer'] == 'smtp') {

                $config['protocol'] = 'smtp';

                $config['smtp_host'] = trim($email_setting['smtp_host']);

                $config['smtp_port'] = trim($email_setting['smtp_port']);

                $config['smtp_timeout'] = '30';

                $config['smtp_user'] = trim($email_setting['smtp_email']);

                $config['smtp_pass'] = trim($email_setting['smtp_password']);

            } /////=====sendmail======

            elseif ($email_setting['mailer'] == 'sendmail') {

                $config['protocol'] = 'sendmail';

                $config['mailpath'] = trim($email_setting['sendmail_path']);

            } /////=====php mail default======

            else {


            }


            $config['wordwrap'] = TRUE;

            $config['mailtype'] = 'html';

            $config['crlf'] = '\n\n';

            $config['newline'] = '\n\n';


            $email_to = $this->input->post('receiver_email');

            $email_address_from = $this->input->post('sender_mail');

            $email_address_reply = $this->input->post('sender_mail');

            $email_subject = 'Testing Mail';

            $email_message = $this->input->post('message_text');


            $str = $email_message;


            $this->email->initialize($config);

            $this->email->from($email_address_from);

            $this->email->reply_to($email_address_reply);

            $this->email->to($email_to);

            $this->email->subject($email_subject);

            $this->email->message($str);


            if (!$this->email->send()) {

                $data["error"] = $this->email->print_debugger();

            } /*End Testing Mail*/

            else {

                $data["success"] = TEST_MESSAGE_SUCCESSFULLY_SENT;

            }

            $data["email_setting_id"] = $this->input->post('email_setting_id');

            $data['site_setting'] = site_setting();

            $theme = 'admin';

            $this->template->set_master_template($theme . '/template.php');


            $this->template->write('title', 'Administrator', '', TRUE);

            $this->template->write_view('header', 'header', $data, TRUE);

            $this->template->write_view('main_content', 'setting/send_testing_email', $data, TRUE);

            $this->template->write_view('footer', 'footer', '', TRUE);

            $this->template->render();

        }


    }


    /*End Testing Mail Functionality*/


}

?>
