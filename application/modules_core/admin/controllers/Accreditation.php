<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Accreditation extends ROCKERS_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('accreditation_model');
        $this->load->model('home_model');
    }


    function index()

    {
        redirect('admin/accreditation/accreditation_user_list');

    }

    /*
        Function name :accreditation_user_list()
        Parameter :$msg=message string to notify admin about the operation he performed.
        Return : none
        Use : Function used for listing accredited user list
    */


    public function accreditation_user_list($msg = '')
    {

        $admin_id = check_admin_authentication();

        $check_rights = get_rights('accreditation_user_list');
        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }


        $data['result'] = $this->accreditation_model->getTableData('accreditation', array(), array('accreditation_date' => 'desc'));

        $data['msg'] = $msg;

        $data['site_setting'] = site_setting();

        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');

        $this->template->write('title', 'Users', '', TRUE);

        $this->template->write_view('header', 'header', $data, TRUE);

        $this->template->write_view('main_content', 'accreditation/accreditation_user_list', $data, TRUE);

        $this->template->write_view('footer', 'footer', '', TRUE);

        $this->template->render();


    }

    /*
        Function name :accreditation_user_view()
        Parameter : id for particuler accredited user $msg=message string to notify admin about the operation he performed.
        Return : none
        Use : Function used for  accredited user Details and also approve and reject option
    */
    public function accreditation_user_view($id = '', $msg = '')
    {

        $admin_id = check_admin_authentication();

        $check_rights = get_rights('accreditation_user_list');
        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }


        $accredited = $this->accreditation_model->getTableData('accreditation', array('id' => $id));

        if (!$accredited) {
            redirect('admin/home/dashboard/no_rights');
        }

        $data['accredited_user'] = $accredited[0];

        $data['msg'] = $msg;

        $data['site_setting'] = site_setting();

        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');

        $this->template->write('title', 'Users', '', TRUE);

        $this->template->write_view('header', 'header', $data, TRUE);

        $this->template->write_view('main_content', 'accreditation/accreditation_user_view', $data, TRUE);

        $this->template->write_view('footer', 'footer', '', TRUE);

        $this->template->render();


    }

    /*
        Function name :approve()
        Parameter : id for particuler accredited user
        Return : none
        Use : Function used for  approve status change
    */

    public function approve($id = '')
    {

        $admin_id = check_admin_authentication();
        $check_rights = get_rights('accreditation_user_list');
        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        $accredited = $this->accreditation_model->getTableData('accreditation', array('id' => $id));
       
        $userdata = UserData($accredited[0]['user_id']);

        $username = $userdata[0]['user_name'] . ' ' . $userdata[0]['last_name'];
        $email = $userdata[0]['email'];
        $site_setting = site_setting();
        $site_name = $site_setting['site_name'];
        $taxonomy_setting = taxonomy_setting();

        $project_name_plural = $taxonomy_setting['project_name_plural'];
        $funds_gerund = $taxonomy_setting['funds_gerund'];

        $email_template = $this->db->query("select * from `email_template` where task='Approve Accrediation request'");
        $email_temp = $email_template->row();
        $email_message = $email_temp->message;
        $email_subject = $email_temp->subject;

        $email_address_from = $email_temp->from_address;
        $email_address_reply = $email_temp->reply_address;
        $email_to = $email;

        $email_message = str_replace('{break}', '<br/>', $email_message);
        $email_message = str_replace('{user_name}', $username, $email_message);
        $email_message = str_replace('{funds_gerund}', $funds_gerund, $email_message);
        $email_message = str_replace('{project_name_plural}', $project_name_plural, $email_message);
        $email_message = str_replace('{site_name}', $site_name, $email_message);

        $str = $email_message;
        email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);


        $this->accreditation_model->AddInsertUpdateTable('accreditation', 'id', $id, array('accreditation_status' => '1'));


        $user_data_update = array(
            'user_name' => $accredited[0]['first_name'],
            'last_name' =>$accredited[0]['last_name'],
            'address' => $accredited[0]['address'],
            'mobile_number' =>$accredited[0]['phone'],

            );

        $this->accreditation_model->AddInsertUpdateTable('user', 'user_id', $accredited[0]['user_id'], $user_data_update);





        redirect('admin/accreditation/accreditation_user_list/update');

    }

    /*
        Function name :reject()
        Parameter : id for particuler accredited user
        Return : none
         Use : Function used for  reject status change
     */
    public function reject($id = '')
    {
        $admin_id = check_admin_authentication();
        $check_rights = get_rights('accreditation_user_list');
        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        $accredited = $this->accreditation_model->getTableData('accreditation', array('id' => $id));


        $userdata = UserData($accredited[0]['user_id']);

        $username = $userdata[0]['user_name'] . ' ' . $userdata[0]['last_name'];
        $email = $userdata[0]['email'];
        $site_setting = site_setting();
        $site_name = $site_setting['site_name'];


        $email_template = $this->db->query("select * from `email_template` where task='Reject Accrediation request'");
        $email_temp = $email_template->row();
        $email_message = $email_temp->message;
        $email_subject = $email_temp->subject;

        $email_address_from = $email_temp->from_address;
        $email_address_reply = $email_temp->reply_address;
        $email_to = $email;

        $login_id = $this->session->userdata('admin_id');
        $admin_data = $this->db->query("select email from `admin` where admin_id=" . $login_id . "");
        $admin_detail = $admin_data->row();

        $admin_email = $admin_detail->email;

        $email_message = str_replace('{break}', '<br/>', $email_message);
        $email_message = str_replace('{user_name}', $username, $email_message);
        $email_message = str_replace('{admin_email}', $admin_email, $email_message);
        $email_message = str_replace('{site_name}', $site_name, $email_message);

        $str = $email_message;


        email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);

        $this->accreditation_model->AddInsertUpdateTable('accreditation', 'id', $id, array('accreditation_status' => '2'));

        redirect('admin/accreditation/accreditation_user_list/update');

    }


}
