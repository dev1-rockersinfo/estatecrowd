<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Equity_contract extends ROCKERS_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('home_model');
        $this->load->model('equity_model');
        $this->load->model('message_model');
    }


    /*
    Function name :equity_contract()
    Parameter : $msg
    Return : none
    Use : update the information.
    */

    function contract_update($msg = '')
    {
        check_admin_authentication();

        $data["error"] = "";
        $data['msg'] = '';
        $data['msg'] = $msg;

        if ($_POST) {

            $data_update = array(
                'detail' => $this->input->post('detail'),
                'update_date' => date('Y-m-d H:i:s')

            );

            $this->equity_model->equity_update('id', 1, 'equity_contract_document', $data_update);
            $msg = 'update';
            redirect('admin/equity_contract/contract_update/' . $msg);
        }

        $equity = $this->equity_model->get_equity_tabledata('equity_contract_document', array('id' => 1));
        $data['detail'] = $equity['detail'];

        $data['site_setting'] = site_setting();
        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', CONTRACT_DOCUMENT, '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'equity/equity_contract', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }

    /*
    Function name :contract_manage()
    Parameter : none
    Return : none
    Use : list the data.
    */

    function contract_manage($msg = '')
    {

        check_admin_authentication();

        $data['msg'] = '';
        $data['result'] = $this->equity_model->contract_manage_data();
        $data['msg'] = $msg;

        $data['site_setting'] = site_setting();
        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', CONTRACT_MANAGE, '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'equity/equity_manage', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();

    }

    /*
    Function name :detail()
    Parameter : $id
    Return : none
    Use : view the history data.
    */

    function detail($id = '', $process_id = '')
    {
        check_admin_authentication();
        $data['site_setting'] = site_setting();
        $data['result'] = $this->equity_model->get_history('equity_investment_process_history', array('invest_status_id' => $id, 'investment_process_id' => $process_id));

        $this->load->view('admin/equity/equity_history', $data);

    }

    /*
    Function name :approve_contract()
    Parameter : $id
    Return : none
    Use :update status and fetch the data
    */

    function approve_contract($id = '', $project_id = '', $user_id = '')
    {

        check_admin_authentication();

        $data['id'] = $id;
        $data['msg'] = '';
        $msg = '';
        $data['project_id'] = $project_id;
        $update_id = '';
        $data['user_id'] = $user_id;

        if ($_POST) {

            $project_id = $this->input->post('project_id');
            $status_id = $this->input->post('status_id');
            $user_id = $this->input->post('user_id');
            $shipment = $this->input->post('shipment');

            if ($status_id == 3) {

                $update_id = 4;

            }
            if ($status_id == 6) {

                $update_id = 7;
            }

            if ($shipment) {

                $data_update = array(
                    'invest_status_id' => $update_id,
                    'shipment' => $shipment,
                );

            } else {

                $data_update = array(
                    'invest_status_id' => $update_id
                );

            }

            $where = array(

                'invest_status_id' => $status_id,
                'equity_id' => $project_id,
            );

            $this->equity_model->equity_contract_update($where, 'equity_investment_process', $data_update);
            $msg = 'update';

            //admin send an email to user for approve


            $where_equity = array(
                'project_id' => $project_id
            );


            $equity_data = $this->equity_model->get_equity_tabledata('project', $where_equity);


            $where_user = array(
                'user_id' => $user_id
            );

            $user_data = $this->equity_model->get_equity_tabledata('user', $where_user);

            if ($status_id == 3) {

                $email_template = $this->db->query("select * from `email_template` where task='Investmemt Document Approved'");

            }
            if ($status_id == 6) {

                $email_template = $this->db->query("select * from `email_template` where task='Payment Reciept Approved'");
            }

            $email_temp = $email_template->row();

            $email_address_from = $email_temp->from_address;
            $email_address_reply = $email_temp->reply_address;
            $email_subject = $email_temp->subject;
            $email_message = $email_temp->message;


            $username = $user_data['user_name'];
            $email = $user_data['email'];
            $email_to = $email;
            $equity_name = $equity_data['project_title'];


            $email_subject = str_replace('{equity_name}', $equity_name, $email_subject);
            $email_subject = str_replace('{status}', 'Approve', $email_subject);

            $project_link = '<a href="' . site_url('investment/investor/' . $project_id) . '">click here</a>';

            $email_message = str_replace('{break}', '<br/>', $email_message);
            $email_message = str_replace('{user_name}', $username, $email_message);
            $email_message = str_replace('{equity_name}', $equity_name, $email_message);
            $email_message = str_replace('{click_here}', $project_link, $email_message);

            $str = $email_message;

            email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);

            //send message
            $login_id = $this->session->userdata('admin_id');

            $data_insert = array(
                'sender_id' => $login_id,
                'receiver_id' => $user_id,
                'is_read' => 0,
                'message_subject' => $email_subject,
                'message_content' => $str,
                'date_added' => date('Y-m-d H:i:s'),
                'type' => $login_id,
                'project_id' => $project_id
            );

            $message_insert = $this->message_model->insert_project_profile_message($data_insert);
            $message_setting = message_setting();
            $user_id = $user_id;
            $user_detail = UserData($user_id, array());
            $login_user_detail = UserData($login_id, array());
            $message_setting->message_enable;

            //$message_user_profile_link = site_url('member/' . $user_id);

            $user_name = $user_detail[0]['user_name'];
            $message_user_name = $login_user_detail[0]['user_name'];
            $content = $str;
            $user_not_own = $this->message_model->get_email_notification($user_detail[0]['user_id']);


            if (isset($user_not_own)) {
                if (isset($user_not_own->update_alert) && $user_not_own->update_alert == '1') {
                    $user['user_name'] = $user_detail[0]['user_name'] . ' ' . $user_detail[0]['last_name'];
                    $user['email'] = $user_detail[0]['email'];
                    $user['message_user_name'] = $login_user_detail[0]['user_name'] . ' ' . $login_user_detail[0]['last_name'];
                    $user['dateadded'] = date('Y-m-d');
                    $user['subject'] = $email_subject;
                    $user['message'] = $str;
                    //$user['message_user_profile_link'] = '<a href="' . site_url('member/' . $login_user_detail[0]['user_id']) . '">' . site_url('member/' . $login_user_detail[0]['user_id']) . '</a>';
                    $this->mailalerts('user_message', '', '', $user, 'Send message');
                    $msg = 'sent';
                    echo "<script>parent.window.location.href='" . site_url('admin/equity_contract/contract_manage/' . $msg) . "'</script>";
                }
            }
            echo "<script>parent.window.location.href='" . site_url('admin/equity_contract/contract_manage/' . $msg) . "'</script>";

        }

        if ($id) {

            $result = $this->equity_model->get_equity_tabledata('equity_investment_process', array('invest_status_id' => $id, 'equity_id' => $project_id));
            $data['document_name'] = $result['document_name'];
            $data['acknowledge_doc'] = $result['acknowledge_doc'];

        } else {

            $data['document_name'] = '';
            $data['acknowledge_doc'] = '';
        }


        $data['site_setting'] = site_setting();
        $this->load->view('admin/equity/equity_approve', $data);
    }

    /*
    Function name :download_doc()
    Parameter : $doc_name
    Return : none
    Use :download file
    */

    function download_doc($id = '', $doc_name = '')
    {

        check_admin_authentication();

        $this->load->helper('download');
        if ($id == 3) {

            $data = file_get_contents(base_url() . "upload/document/" . $doc_name); // Read the file's contents

        }
        if ($id == 6) {

            $data = file_get_contents(base_url() . "upload/acknowledge/" . $doc_name); // Read the file's contents
        }

        $name = $doc_name;
        force_download($name, $data);

    }

    /*
    Function name :send_message()
    Parameter : $user_id,$equity_id
    Return : none
    Use :send a message to user
    */

    function send_message($user_id = '', $equity_id = '')
    {

        check_admin_authentication();

        $data['errors'] = '';
        $msg = '';
        $data['user_id'] = $user_id;
        $data['equity_id'] = $equity_id;

        $login_id = $this->session->userdata('admin_id');

        $this->load->library('form_validation');
        $this->form_validation->set_rules('subject', SUBJECT, 'required');
        $this->form_validation->set_rules('comments', MESSAGES, 'required');

        if ($this->form_validation->run() == FALSE) {

            if (validation_errors()) {
                $data['errors'] = validation_errors();
            } else {
                $data['errors'] = '';
            }

        } else {

            $user_id = $this->input->post('user_id');
            $equity_id = $this->input->post('equity_id');

            $data_insert = array(
                'sender_id' => $login_id,
                'receiver_id' => $user_id,
                'is_read' => 0,
                'message_subject' => $this->input->post('subject'),
                'message_content' => $this->input->post('comments'),
                'date_added' => date('Y-m-d H:i:s'),
                'type' => $login_id,
                'project_id' => $equity_id
            );

            $message_insert = $this->message_model->insert_project_profile_message($data_insert);
            $message_setting = message_setting();
            $user_id = $user_id;
            $user_detail = UserData($user_id, array());
            $login_user_detail = UserData($login_id, array());
            $message_setting->message_enable;

            //$message_user_profile_link = site_url('member/' . $user_id);

            $user_name = $user_detail[0]['user_name'];
            $message_user_name = $login_user_detail[0]['user_name'];
            $content = $this->input->post('comments');
            $user_not_own = $this->message_model->get_email_notification($user_detail[0]['user_id']);


            if (isset($user_not_own)) {
                if (isset($user_not_own->update_alert) && $user_not_own->update_alert == '1') {
                    $user['user_name'] = $user_detail[0]['user_name'] . ' ' . $user_detail[0]['last_name'];
                    $user['email'] = $user_detail[0]['email'];
                    $user['message_user_name'] = $login_user_detail[0]['user_name'] . ' ' . $login_user_detail[0]['last_name'];
                    $user['dateadded'] = date('Y-m-d');
                    $user['subject'] = $this->input->post('subject');
                    $user['message'] = $this->input->post('comments');
                    //$user['message_user_profile_link'] = '<a href="' . site_url('member/' . $login_user_detail[0]['user_id']) . '">' . site_url('member/' . $login_user_detail[0]['user_id']) . '</a>';
                    $this->mailalerts('user_message', '', '', $user, 'Send message');
                    $msg = 'sent';
                    echo "<script>parent.window.location.href='" . site_url('admin/equity_contract/contract_manage/' . $msg) . "'</script>";
                }
            }


        }


        $data['site_setting'] = site_setting();
        $this->load->view('admin/equity/send_message', $data);
    }

    /*
    Function name :reject_status()
    Parameter : none
    Return : none
    Use :redirect list contract manage page
    */

    function reject_status($user_id = '', $project_id = '', $user_id = '')
    {

        $where_equity = array(
            'project_id' => $project_id
        );

        $equity_data = $this->equity_model->get_equity_tabledata('project', $where_equity);

        $where_user = array(
            'user_id' => $user_id
        );

        $user_data = $this->equity_model->get_equity_tabledata('user', $where_user);


        $email_template = $this->db->query("select * from `email_template` where task='Document Rejected'");
        $email_temp = $email_template->row();
        $email_address_from = $email_temp->from_address;
        $email_address_reply = $email_temp->reply_address;
        $email_subject = $email_temp->subject;
        $email_message = $email_temp->message;


        $username = $user_data['user_name'];
        $email = $user_data['email'];
        $email_to = $email;
        $equity_name = $equity_data['project_title'];

        $email_subject = str_replace('{equity_name}', $equity_name, $email_subject);
        $email_subject = str_replace('{status}', 'Reject', $email_subject);


        $project_link = '<a href="' . site_url('investment/investor/' . $project_id) . '">click here</a>';

        $email_message = str_replace('{break}', '<br/>', $email_message);
        $email_message = str_replace('{user_name}', $username, $email_message);
        $email_message = str_replace('{equity_name}', $equity_name, $email_message);
        $email_message = str_replace('{click_here}', $project_link, $email_message);

        $str = $email_message;
        email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);

        $login_id = $this->session->userdata('admin_id');

        $data_insert = array(
            'sender_id' => $login_id,
            'receiver_id' => $user_id,
            'is_read' => 0,
            'message_subject' => $email_subject,
            'message_content' => $str,
            'date_added' => date('Y-m-d H:i:s'),
            'type' => $login_id,
            'project_id' => $project_id
        );

        $message_insert = $this->message_model->insert_project_profile_message($data_insert);
        $message_setting = message_setting();
        $user_id = $user_id;
        $user_detail = UserData($user_id, array());
        $login_user_detail = UserData($login_id, array());
        $message_setting->message_enable;

        //$message_user_profile_link = site_url('member/' . $user_id);

        $user_name = $user_detail[0]['user_name'];
        $message_user_name = $login_user_detail[0]['user_name'];
        $content = $str;
        $user_not_own = $this->message_model->get_email_notification($user_detail[0]['user_id']);


        if (isset($user_not_own)) {
            if (isset($user_not_own->update_alert) && $user_not_own->update_alert == '1') {
                $user['user_name'] = $user_detail[0]['user_name'] . ' ' . $user_detail[0]['last_name'];
                $user['email'] = $user_detail[0]['email'];
                $user['message_user_name'] = $login_user_detail[0]['user_name'] . ' ' . $login_user_detail[0]['last_name'];
                $user['dateadded'] = date('Y-m-d');
                $user['subject'] = $email_subject;
                $user['message'] = $str;
                //$user['message_user_profile_link'] = '<a href="' . site_url('member/' . $login_user_detail[0]['user_id']) . '">' . site_url('member/' . $login_user_detail[0]['user_id']) . '</a>';
                $this->mailalerts('user_message', '', '', $user, 'Send message');
                $msg = 'sent';
                echo "<script>parent.window.location.href='" . site_url('admin/equity_contract/contract_manage/' . $msg) . "'</script>";
            }
        }

        echo "<script>parent.window.location.href='" . site_url('admin/equity_contract/contract_manage') . "'</script>";

    }

}

?>
