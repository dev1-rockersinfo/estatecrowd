<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class User extends ROCKERS_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        //$this->load->model('home_model');
    }


    function index()

    {

        redirect('admin/user/list_user');

    }

    /*
     Function name :add_user()
    Parameter : none.
    Return : none;
    Use : to add new user or update existing user from admin side.
    */
    function add_user()

    {

        $check_rights = get_rights('list_user');

        if ($check_rights == 0) {

            redirect('admin/home/dashboard/no_rights');

        }
        
        $email_error='';
        if($_POST){
            $username = $this->user_model->user_unique($this->input->post('email'));

            if ($username) {
                $email_error = EXISTS_ACCOUNT_ASSOCIATE_WITHMAIL; 
            
            } else {
               $email_error = '';
            }
        }
       
        $send_mail = 'no';
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', EMAIL, 'required|valid_email');

        $this->form_validation->set_rules('user_name', FIRST_NAME, 'required|alpha');

        $this->form_validation->set_rules('last_name', LAST_NAME, 'required|alpha');

        $this->form_validation->set_rules('address', ADDRESS, 'required');

        if (!$this->input->post('user_id')) {
            $this->form_validation->set_rules('password', PASSWORD, 'required');
        }
        $check_user = '';
        if ($_POST) {
            // $str = $this->input->post('email');
            // if ($this->input->post('user_id')) {
            //     // $query = $this->db->get_where('user', array('user_id' => $this->input->post('user_id')));
            //     // $res = $query->row_array();
            //     // $email = $res['email'];

            //      $query = $this->db->query("select email from user where email = '$str' and user_id != '" . $this->input->post('user_id') . "'");
            //     //$query = $this->db->query("select email from user where email = '$str' and email != '" . $email . "'");
            // } else {
            //     $query = $this->db->query("select email from user where email = '$str'");
            // }
           
            // if ($query->num_rows() > 0) {

            //     $ret = TRUE;
            // } else {
            //     $ret = FALSE;
            // }
           
            // if ($ret) {
            //     $check_user = EXISTS_ACCOUNT_ASSOCIATE_WITHMAIL . '<br />';
            // } else {
            //     $check_user = '';
            // }

        }


        $this->form_validation->set_rules('user_website', WEBSITE_URL, 'valid_url');


        $facebook_link = $this->input->post('facebook_url');
        $twitter_link = $this->input->post('twitter_url');
        $linkedln_url = $this->input->post('linkedln_url');
        $googleplus_url = $this->input->post('googleplus_url');
        $bandcamp_url = $this->input->post('bandcamp_url');
        $youtube_url = $this->input->post('youtube_url');
        $myspace_url = $this->input->post('myspace_url');

        $fb_error = '';
        $tw_error = '';
        $lin_error = '';
        $googleplus_error = '';
        $bandcamp_error = '';
        $youtube_error = '';
        $myspace_error = '';

        if ($facebook_link != '') {
            if (!substr_count($facebook_link, 'facebook.com')) {
                $fb_error = INVALID_FACEBOOK_PROFILE_URL . '<br/>';
            }
        }

        if ($twitter_link != '') {
            if (!substr_count($twitter_link, 'twitter.com')) {
                $tw_error = INVALID_TWITTER_PROFILE_URL . '<br/>';
            }
        }

        if ($linkedln_url != '') {
            if (!substr_count($linkedln_url, 'linkedin.com')) {
                $lin_error = INVALID_LINKEEDIN_PROFILE_URL . '<br/>';
            }
        }

        if ($googleplus_url != '') {
            if (!substr_count($googleplus_url, 'plus.google.com')) {
                $googleplus_error = INVALID_GOOGLE_PLUS_PROFILE_URL . '<br/>';

            }
        }

        if ($bandcamp_url != '') {
            if (!substr_count($bandcamp_url, 'basecamp.com')) {
                $bandcamp_error = INVALID_BANDCAMP_PROFILE_URL . '<br/>';

            }
        }

        if ($youtube_url != '') {
            if (!substr_count($youtube_url, 'youtube.com')) {
                $youtube_error = INVALID_YOUTUBE_PROFILE_URL . '<br/>';
            }
        }

        if ($myspace_url != '') {
            if (!substr_count($myspace_url, 'myspace.com')) {
                $myspace_error = INVALID_MYSPACE_PROFILE_URL . '<br/>';
            }
        }


        if ($this->form_validation->run() == FALSE || $check_user != '' || $fb_error != '' || $tw_error != '' || $lin_error != '' || $googleplus_error != '' || $bandcamp_error != '' || $youtube_error != '' || $myspace_error != '' || $email_error != '') {
            if (validation_errors() || $check_user != '' || $fb_error != '' || $tw_error != '' || $lin_error != '' || $googleplus_error != '' || $bandcamp_error != '' || $youtube_error != '' || $myspace_error != '' || $email_error != '') {

                $data["error"] = validation_errors() . $check_user . $fb_error . $tw_error . $lin_error . $googleplus_error . $bandcamp_error . $youtube_error . $myspace_error . $email_error;

            } else {

                $data["error"] = "";

            }

            $data["user_id"] = $this->input->post('user_id');

            $data["email"] = $this->input->post('email');

            $data["user_name"] = $this->input->post('user_name');

            $data["last_name"] = $this->input->post('last_name');

            $data["password"] = $this->input->post('password');

            $data["image"] = $this->input->post('image');

            $data["address"] = $this->input->post('address');

            $data["city"] = $this->input->post('city');

            $data["state"] = $this->input->post('state');

            $data["country"] = $this->input->post('country');

            $data["zip_code"] = $this->input->post('zip_code');

            $data["paypal_email"] = $this->input->post('paypal_email');

            $data["active"] = $this->input->post('active');

            $data['user_about'] = $this->input->post('user_about');

            $data['user_occupation'] = $this->input->post('user_occupation');

            $data['user_interest'] = $this->input->post('user_interest');

            $data['user_skill'] = $this->input->post('user_skill');

            $data['user_website'] = $this->input->post('user_website');

            $data['facebook_url'] = $this->input->post('facebook_url');

            $data['twitter_url'] = $this->input->post('twitter_url');

            $data['linkedln_url'] = $this->input->post('linkedln_url');

            $data['googleplus_url'] = $this->input->post('googleplus_url');

            $data['bandcamp_url'] = $this->input->post('bandcamp_url');

            $data['youtube_url'] = $this->input->post('youtube_url');

            $data['myspace_url'] = $this->input->post('myspace_url');

            $data['suspend_reason'] = $this->input->post('reason');

            $data['site_setting'] = site_setting();


            $theme = 'admin';

            $this->template->set_master_template($theme . '/template.php');

            $this->template->write('title', 'Users', '', TRUE);

            $this->template->write_view('header', 'header', $data, TRUE);

            $this->template->write_view('main_content', 'add_user', $data, TRUE);

            $this->template->write_view('footer', 'footer', '', TRUE);

            $this->template->render();

        } else {
            //$base_path = base_path();

            if ($this->input->post('user_id')) {
                $this->user_model->user_insert_update('update');

                /////////////============Activation email when admin activates a user===========
                if ($this->input->post('active') == 1) {
                    $result = UserData($this->input->post('user_id'));
                    $id = $this->input->post('user_id');
                    $confirm_key = $result[0]['confirm_key'];

                    redirect('home/confirm_account/' . $id . '/' . $confirm_key . '/admin');
                    $email_template = $this->db->query("select * from `email_template` where task='Admin User Active'");

                    $email_temp = $email_template->row();

                    $email_setting = $this->db->query("select * from `email_setting` where email_setting_id='1'");

                    $email_set = $email_setting->row();

                    $email_address_from = $email_temp->from_address;

                    $email_address_reply = $email_temp->reply_address;

                    $email_subject = $email_temp->subject;

                    $email_message = $email_temp->message;

                    $username = $this->input->post('user_name');

                    $password = $this->input->post('password');

                    $email = $this->input->post('email');

                    $login_link = base_url() . 'home/login';


                    $email_to = $this->input->post('email');


                    $email_message = str_replace('{break}', '<br/>', $email_message);

                    $email_message = str_replace('{user_name}', $username, $email_message);

                    $email_message = str_replace('{password}', $password, $email_message);

                    $email_message = str_replace('{email}', $email, $email_message);

                    $email_message = str_replace('{login_link}', $login_link, $email_message);

                    $str = $email_message;


                    email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);


                }

                //When admin deactivates a user, mail is send to the user about deactivation

                if ($this->input->post('active') == 0) {

                    $email_template = $this->db->query("select * from `email_template` where task='Admin User Deactivate'");

                    $email_temp = $email_template->row();

                    $email_setting = $this->db->query("select * from `email_setting` where email_setting_id='1'");

                    $email_set = $email_setting->row();

                    $email_address_from = $email_temp->from_address;

                    $email_address_reply = $email_temp->reply_address;

                    $email_subject = $email_temp->subject;

                    $email_message = $email_temp->message;

                    $username = $this->input->post('user_name');

                    $email = $this->input->post('email');

                    $login_link = base_url() . 'home/login';


                    $email_to = $this->input->post('email');


                    $email_message = str_replace('{break}', '<br/>', $email_message);

                    $email_message = str_replace('{user_name}', $username, $email_message);

                    $email_message = str_replace('{email}', $email, $email_message);

                    $str = $email_message;


                    email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);


                }


//When Admin suspends a user, records are updated and mail gets send to the user.
                if ($this->input->post('active') == 2) {
                    $site_setting = site_setting();
                    $email_template = $this->db->query("select * from `email_template` where task='Admin User Suspended'");

                    $email_temp = $email_template->row();

                    $email_setting = $this->db->query("select * from `email_setting` where email_setting_id='1'");

                    $email_set = $email_setting->row();

                    $email_address_from = $email_temp->from_address;

                    $email_address_reply = $email_temp->reply_address;

                    $email_subject = $email_temp->subject;

                    $email_message = $email_temp->message;

                    $username = $this->input->post('user_name');

                    $email = $this->input->post('email');

                    $login_link = base_url() . 'home/login';

                    $email_to = $this->input->post('email');

                    $suspend_message = $this->input->post('reason');
                    $email_address_admin = '<a href="' . $email_address_reply . '">' . $email_address_reply . '</a>';
                    $email_message = str_replace('{break}', '<br/>', $email_message);

                    $email_message = str_replace('{user_name}', $username, $email_message);

                    $email_message = str_replace('{email}', $email, $email_message);
                    $email_message = str_replace('{admin_email}', $email_address_admin, $email_message);
                    $email_message = str_replace('{site_name}', $site_setting['site_name'], $email_message);
                    $email_message = str_replace('{message}', $suspend_message, $email_message);

                    $str = $email_message;


                    email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);


                }

                /////////////============email===========

                $msg = "update";

            } else {

                $this->user_model->user_insert_update('insert');
                /////////////============email===========

                $email_setting = $this->db->query("select * from `email_setting` where email_setting_id='1'");

                $email_set = $email_setting->row();


                //////////=====welcome mail================


                $email_template = $this->db->query("select * from `email_template` where task='Welcome Email'");

                $email_temp = $email_template->row();

                $email_address_from = $email_temp->from_address;

                $email_address_reply = $email_temp->reply_address;

                $email_subject = $email_temp->subject;

                $email_message = $email_temp->message;

                $username = $this->input->post('user_name');

                $email = $this->input->post('email');

                $login_link = base_url() . 'home/login';

                $email_to = $this->input->post('email');

                $email_message = str_replace('{break}', '<br/>', $email_message);

                $email_message = str_replace('{user_name}', $username, $email_message);

                $email_message = str_replace('{email}', $email, $email_message);

                $str = $email_message;

                email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);

                /*	$this->email->from($email_address_from);

                    $this->email->reply_to($email_address_reply);

                    $this->email->to($email_to);

                    $this->email->subject($email_subject);

                    $this->email->message($str);

                    $this->email->send();

                    */
                /////////======welcome mail=======

                //////////=====New User join mail================

                $email_template = $this->db->query("select * from `email_template` where task='New User Join'");

                $email_temp = $email_template->row();

                $email_address_from = $email_temp->from_address;

                $email_address_reply = $email_temp->reply_address;

                $email_subject = $email_temp->subject;

                $email_message = $email_temp->message;

                $username = $this->input->post('user_name');

                $password = $this->input->post('password');

                $email = $this->input->post('email');

                $email_to = $this->input->post('email');


                $login_link = base_url() . 'home/login';

                $email_message = str_replace('{break}', '<br/>', $email_message);

                $email_message = str_replace('{user_name}', $username, $email_message);

                $email_message = str_replace('{password}', $password, $email_message);

                $email_message = str_replace('{email}', $email, $email_message);

                $email_message = str_replace('{login_link}', $login_link, $email_message);

                $str = $email_message;

                email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);

                /*	$this->email->from($email_address_from);

                    $this->email->reply_to($email_address_reply);

                    $this->email->to($email_to);

                    $this->email->subject($email_subject);

                    $this->email->message($str);

                    $this->email->send();

                    */

                /////////======New User join mail=======


                /////////////============email===========

                $msg = "insert";

            }

            $offset = $this->input->post('offset');

            redirect('admin/user/list_user/' . $msg);

        }

    }

    /*
     Function name :edit_user()
    Parameter : id=user_id of user whose information admin wants to edit.
    Return : none;
    Use : to edit information of user when admin wants to..
    */

    function edit_user($id = 0)

    {
        $check_rights = get_rights('list_user');

        if ($check_rights == 0) {

            redirect('admin/home/dashboard/no_rights');

        }


        $user_data = UserData($id);
        //echo '<pre>'; print_r($user_data); die;
        $one_user = $user_data[0];


        $data["error"] = "";

        $data["user_id"] = $id;

        $data["email"] = $one_user['email'];

        $data["user_name"] = $one_user['user_name'];

        $data["last_name"] = $one_user['last_name'];

        $data["password"] = $one_user['password'];

        $data["image"] = $one_user['image'];

        $data["address"] = $one_user['address'];


        $data["zip_code"] = $one_user['zip_code'];

        $data["paypal_email"] = $one_user['paypal_email'];

        $data["active"] = $one_user['active'];

        $data['user_about'] = $one_user['user_about'];

        $data['user_occupation'] = $one_user['user_occupation'];

        $data['user_interest'] = $one_user['user_interest'];

        $data['user_skill'] = $one_user['user_skill'];

        $data['user_website'] = $one_user['user_website'];

        $data['facebook_url'] = $one_user['facebook_url'];

        $data['twitter_url'] = $one_user['twitter_url'];

        $data['linkedln_url'] = $one_user['linkedln_url'];

        $data['googleplus_url'] = $one_user['googleplus_url'];

        $data['bandcamp_url'] = $one_user['bandcamp_url'];

        $data['youtube_url'] = $one_user['youtube_url'];

        $data['myspace_url'] = $one_user['myspace_url'];

        $data['suspend_reason'] = $one_user['suspend_reason'];

        $data['citylist'] = get_city();

        $data['countrylist'] = get_country();

        $data['statelist'] = get_state();

        $data['site_setting'] = site_setting();

        //$data["offset"] = $offset;

        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');

        $this->template->write('title', 'Users', '', TRUE);

        $this->template->write_view('header', 'header', $data, TRUE);

        $this->template->write_view('main_content', 'add_user', $data, TRUE);

        $this->template->write_view('footer', 'footer', '', TRUE);

        $this->template->render();

    }


    /*
     Function name :delete_user()
    Parameter : id=user_id of user whose account admin wants to delete.
    Return : none;
    Use : to delete user when admin wants to..
    */
    function delete_user($id = 0)

    {

        $check_rights = get_rights('list_user');

        if ($check_rights == 0) {

            redirect('admin/home/dashboard/no_rights');

        }
/////////////============email===========

        $user_detail = UserData($id);

        $user = $user_detail[0];

        $email_template = $this->db->query("select * from `email_template` where task='Admin User Delete'");

        $email_temp = $email_template->row();

        $email_setting = $this->db->query("select * from `email_setting` where email_setting_id='1'");

        $email_set = $email_setting->row();

        $email_address_from = $email_temp->from_address;

        $email_address_reply = $email_temp->reply_address;

        $email_subject = $email_temp->subject;

        $email_message = $email_temp->message;

        $username = $user['user_name'];

        $email_to = $user['email'];

        $email_message = str_replace('{break}', '<br/>', $email_message);

        $email_message = str_replace('{user_name}', $username, $email_message);

        $str = $email_message;


        email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);
/////////////============email===========	

        //log_message('Add User',"By Admin".$this->input->post('user_name'));

        //error_log("Add Admin User Gofundme", 1,"jigar.rockersinfo@gmail.com");

        $this->user_model->delete_user($id);

        //$this->db->delete('user',array('user_id'=>$id));

        redirect('admin/user/list_user/delete');

    }


    /*
     Function name :list_user()
    Parameter : $msg=message string=to notify admin about the activity or operation he/she performed..
    Return : none;
    Use : list all the users registered on his website..
    */
    function list_user($msg = '')

    {
        $check_rights = get_rights('list_user');
        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }


        //	$data['result'] = $this->user_model->get_user_result($offset, $limit);

        $data['result'] = get_table_data('user', array('user_id' => 'desc'));

        $data['msg'] = $msg;

        $data['option'] = '';


        $data['site_setting'] = site_setting();

        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');

        $this->template->write('title', 'Users', '', TRUE);

        $this->template->write_view('header', 'header', $data, TRUE);

        $this->template->write_view('main_content', 'list_user', $data, TRUE);

        $this->template->write_view('footer', 'footer', '', TRUE);

        $this->template->render();

    }


    /*
     Function name :user_login()
    Parameter : $msg=message string=notify admin about the activity or operation he/she performed..
    Return : none;
    Use : listing users login details.
    */
    function user_login($msg = '')

    {

        $check_rights = get_rights('user_login');

        if ($check_rights == 0) {

            redirect('admin/home/dashboard/no_rights');

        }

        $data['msg'] = $msg;

        $data['result'] = $this->user_model->get_userlogin_result();

        $data['site_setting'] = site_setting();

        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'User Logins', '', TRUE);

        $this->template->write_view('header', 'header', $data, TRUE);

        $this->template->write_view('main_content', 'user/list_user_login', $data, TRUE);

        $this->template->write_view('footer', 'footer', '', TRUE);

        $this->template->render();

    }

    /*
     Function name :action_login()
    Parameter : none.
    Return : none;
    Use : Action admin wants to perform on user login list page like deleting login records..
    */
    function action_login()

    {

        $check_rights = get_rights('user_login');

        if ($check_rights == 0) {

            redirect('admin/home/dashboard/no_rights');

        }

        $action = $this->input->post('action');

        $login_id = $this->input->post('chk');

        if ($action == 'delete') {
            foreach ($login_id as $id) {
                $this->db->query("delete from user_login where login_id='" . $id . "'");
            }
            redirect('admin/user/user_login/delete');
        }
    }

    /*
     Function name :username_check()
    Parameter : $username=entered email for registering or updating user.
    Return : returns true if email is unique else false with error message;
    Use : to check if entered email is unique or not..
    */
    function username_check($username)
    {
        $username = $this->user_model->user_unique($username);
        return $username;
    }
    /*
     *Functions not used in new admin theme=====Start=======
     *
     *function not used as we are using jquery data tables for searching
        function search_list_user($limit=20,$option='',$keyword='',$offset=0,$msg='')

        {



            $check_rights=get_rights('list_user');



            if(	$check_rights==0) {

                redirect('home/dashboard/no_rights');

            }







            $this->load->library('pagination');





            if($_POST)

            {

                $option=$this->input->post('option');

                $keyword=$this->input->post('keyword');

            }

            else

            {

                $option=$option;

                $keyword=$keyword;

            }



            $keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',trim($keyword)));



            $config['uri_segment']='5';

            $config['base_url'] = site_url('user/search_list_user/'.$limit.'/'.$option.'/'.$keyword.'/');

            $config['total_rows'] = $this->user_model->get_total_search_user_count($option,$keyword);

            $config['per_page'] = $limit;

            $this->pagination->initialize($config);

            $data['page_link'] = $this->pagination->create_links();



            $data['result'] = $this->user_model->get_search_user_result($option,$keyword,$offset, $limit);

            $data['msg'] = $msg;

            $data['offset'] = $offset;





            //$data['statelist']=$this->project_category_model->get_state();



            $data['site_setting'] = $this->home_model->select_site_setting();



            $data['limit']=$limit;

            $data['option']=$option;

            $data['keyword']=$keyword;

            $data['search_type']='search';



            $this->template->write('title', 'Search User List', '', TRUE);

            $this->template->write_view('header', 'header', $data, TRUE);

            $this->template->write_view('main_content', 'list_user', $data, TRUE);

            $this->template->write_view('footer', 'footer', '', TRUE);

            $this->template->render();

        }

     *
        function user_detail($id=0)

        {

            $this->load->library('form_validation');

            $this->form_validation->set_rules('from_email', 'From Email Address', 'valid_email');

            $this->form_validation->set_rules('subject', 'Subject', 'required');

            $this->form_validation->set_rules('message', 'Message', 'required');

            if($this->form_validation->run() == FALSE){

                if(validation_errors())

                {

                    $data["error"] = validation_errors();

                    $data["from_email"] = $this->input->post('from_email');

                    $data["subject"] = $this->input->post('subject');

                    $data["message"] = $this->input->post('message');

                }else{

                    $data["error"] = "";

                    $data["from_email"] = "";

                    $data["subject"] = "";

                    $data["message"] = "";

                }

            }else{

                $this->load->library('email');

                $this->email->from($this->input->post('from_email'));

                $this->email->to($this->input->post('email'));

                $this->email->subject($this->input->post('subject'));

                $this->email->message($this->input->post('message'));

                $this->email->send();

                $data["error"] = $this->email->print_debugger();

                $data["from_email"] = "";

                $data["subject"] = "";

                $data["message"] = "";

            }



            $data['site_setting'] = site_setting();



            $data['one_user'] = $this->user_model->get_one_user($id);

            $this->template->write('title', 'User Details', '', TRUE);

            $this->template->write_view('header', 'header', $data, TRUE);

            $this->template->write_view('main_content', 'user_detail', $data, TRUE);

            $this->template->write_view('footer', 'footer', '', TRUE);

            $this->template->render();



        }



        function getstate($countryid='')

        {



            $str=' <select tabindex="5" name="state" id="state" class="btn_input" style="text-transform:capitalize;" onblur="getcity(this.value)">';





                $query=$this->db->query("select * from state where active='1' and country_id='".$countryid."'");



                if($query->num_rows()>0)

                {

                    $state=$query->result();



                    foreach($state as $st)

                    {



                        $str .= "<option value='".$st->state_id."'>".$st->state_name."</option>";

                    }

                }

                else

                {

                    $str .= "<option value=''>No State</option>";

                }



            $str.='</select>';



            echo $str;

            die();





        }

        function getallcity()

        {

            $str2='<select tabindex="5" name="city" id="city" class="btn_input" style="text-transform:capitalize;">';



                $query=$this->db->query("select * from city where active='1'");



                if($query->num_rows()>0)

                {

                    $city=$query->result();



                    foreach($city as $ct)

                    {



                        $str2 .= "<option value=''>No City</option><option value='".$ct->city_id."'>".$ct->city_name."</option>";

                    }

                }





            $str2.='</select>';





            $str2.='</select>';



            echo $str2;

            die;



        }



        function getcity($stateid='')

        {



            $str='<select tabindex="5" name="city" id="city" class="btn_input" style="text-transform:capitalize;">';



                $query=$this->db->query("select * from city where active='1' and state_id='".$stateid."'");



                if($query->num_rows()>0)

                {

                    $city=$query->result();



                    foreach($city as $ct)

                    {



                        $str .= "<option value='".$ct->city_id."'>".$ct->city_name."</option>";

                    }

                }

                else

                {

                    $str .= "<option value=''>No City</option>";

                }



            $str.='</select>';



            echo $str;



            die();

        }

        *Functions not used in new admin theme=====End=======
     */


}

?>
