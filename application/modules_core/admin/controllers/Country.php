<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Country extends ROCKERS_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('home_model');

    }

    function index()
    {
        redirect('admin/country/list_country/');
    }

    /*
     Function name :add_country()
    Parameter : none.
    Return : none
    Use : to add new country entry into country table.
    */
    function add_country()
    {

        $check_rights = get_rights('list_country');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('country_name', COUNTRY_NAME, 'required|alpha_space');

        $check_country = '';

        if ($_POST) {

            if ($this->countryname_check(SecurePostData($this->input->post('country_name'))) == 1) {

                $check_country = COUNTRY_NAME_IS_ALREADY_EXIST;

            }

        }

        if ($this->form_validation->run() == FALSE || $check_country != '') {
            if (validation_errors() || $check_country != '') {
                $data["error"] = validation_errors() . $check_country;
            } else {
                $data["error"] = "";
            }
            $data["country_id"] = $this->input->post('country_id');
            $data["country_name"] = $this->input->post('country_name');

            $data["active"] = $this->input->post('active');

            $data['site_setting'] = site_setting();

            $theme = 'admin';

            $this->template->set_master_template($theme . '/template.php');
            $this->template->write('title', 'Countries', '', TRUE);
            $this->template->write_view('header', 'header', $data, TRUE);
            $this->template->write_view('main_content', 'country/add_country', $data, TRUE);
            $this->template->write_view('footer', 'footer', '', TRUE);
            $this->template->render();
        } else {
            $data = array(
                'country_name' => $this->input->post('country_name'),
                'active' => $this->input->post('active'),
            );


            if ($this->input->post('country_id')) {
                $this->home_model->table_update('country_id', $this->input->post('country_id'), 'country', $data);
                $msg = "update";
            } else {
                $this->home_model->table_insert('country', $data);
                $msg = "insert";
            }
            redirect('admin/country/list_country/' . $msg);
        }


    }

    /*
     Function name :edit_country()
    Parameter : $id:id of the country admin wish to edit.
    Return : none
    Use : to edit the information of particular country.
    */
    function edit_country($id = 0)
    {
        $check_rights = get_rights('list_country');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        $one_country = $this->home_model->get_one_tabledata('country', array('country_id' => $id));
        $data["error"] = "";
        $data["country_id"] = $id;
        $data["country_name"] = $one_country['country_name'];
        $data["active"] = $one_country['active'];

        $data['site_setting'] = site_setting();
        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Countries', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'country/add_country', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }

    /*
     Function name :delete_country()
    Parameter : $id:id of the country admin wish to delete.
    Return : none
    Use : to delete particular country
    */
    function delete_country($id = 0)
    {
        $check_rights = get_rights('list_country');
        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        $this->db->delete('country', array('country_id' => $id));
        redirect('admin/country/list_country/delete');
    }

    /*
     Function name :list_country()
    Parameter : $msg:message string of the last operation performed by admin.
    Return : none
    Use : to display list of countries to admin.
    */
    function list_country($msg = '')
    {

        $check_rights = get_rights('list_country');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        //$data['result'] = $this->country_model->get_country_result();
        $data['result'] = get_table_data('country', array('country_name' => 'asc'));
        $data['msg'] = $msg;

        $data['site_setting'] = site_setting();
        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Countries', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'country/list_country', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }

    /*
     Function name :action_country()
    Parameter : none
    Return : none
    Use : To activate or inactivate list of countries together.
    */
    function action_country()
    {
        if ($this->session->userdata('admin_id') == '') {
            redirect('admin/home/dashboard/no_rights');
        }


        $offset = $this->input->post('offset');
        $action = $this->input->post('action');
        $page_id = $this->input->post('chk');

        //print_r($_POST);die();
        switch ($action) {
            case 'active':
                $msg = 'active';
                foreach ($page_id as $id) {
                    $this->db->query("update country set active=1 where country_id='" . $id . "'");
                }
                break;
            case 'inactive':
                $msg = 'inactive';
                foreach ($page_id as $id) {
                    $this->db->query("update country set active=0 where country_id='" . $id . "'");
                }
                break;
        }
        redirect('admin/country/list_country/' . $msg);

    }

    function countryname_check($country_name)

    {

        $country = array('country_name' => $country_name);


        $countryname = country_validate($country);
        if($countryname){


                if($this->input->post('country_id')!=$countryname['country_id'])
                    {
                        return 1;

                    }
                  if ($countryname != '' && $this->input->post('country_id')=='')
                    {

                        return 1;

                    } else {

                    return 0;

                }
                }else{
                 return 0;
            }

    }


}

?>
