<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Message extends ROCKERS_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('message_model');
        //$this->load->model('user_model');
    }

    function index()
    {
        redirect('admin/message/list_message');

    }

    /*
     Function name :list_message()
    Parameter :$msg:message string to notify user about the operation he performed.
    Return : none
    Use : to get list of messages send by one user to another can be viewed by admin.
    */
    function list_message($msg = '')
    {
        //$msg=unserialize(urldecode($msg));

        $check_rights = get_rights('list_message');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        //$data['result'] = $this->message_model->get_message_result($offset, $limit);

        $data['result'] = $this->message_model->get_message_result();
        $data['msg'] = $msg;


        $data['site_setting'] = site_setting();
        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Messages', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'message/list_message', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }

    /*
     Function name :action_message()
    Parameter :none
    Return : none
    Use : to delete any conversation this function is used.
    */
    function action_message()
    {
        $check_rights = get_rights('list_message');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        $offset = $this->input->post('offset');
        $action = $this->input->post('action');
        $message_id = $this->input->post('chk');

        if ($action == 'delete') {
             foreach ($message_id as $id) {
                $message_detail = $this->db->query("select * from message_conversation where message_id='" . $id . "'");
                $message = $message_detail->row();
                $this->db->query("delete from message_conversation where message_id='" . $message->message_id . "' or reply_message_id = '".$message->message_id."'");
            }

            redirect('admin/message/list_adminmessage/delete');

        }

    }

    /*
     Function name :view_message()
    Parameter :$message_id:message id whose conversation admin wants to view
    Return : none
    Use : to view whole conversation between two persons or users on site.
    */

    function view_message($equity_id = '', $user_id = 0,$type='')
    {

        $check_rights = get_rights('list_message');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }
        
        $login_id = $this->session->userdata('admin_id');
         
        $data['error'] = "";

        $data['equity_id'] = $equity_id;        
        $equity_data = GetOneEquity($equity_id);
        
       
        
        $data['user_id'] = $user_id;
        $user_detail=UserData($user_id);
        
        if(!empty($equity_data) && !empty($user_detail)) {
            
            
            $data['user_detail']=$user_detail;
            $data['equity_data']=$equity_data;

           $message_type=2; //==for investor
            if($equity_data['user_id']==$user_id){
                $message_type=1; //=for feedback
            }            
            
            $data['message_type']=$message_type;

            $all_message = $this->message_model->get_unread_messsages($equity_id, $user_id, $message_type);

            if (is_array($all_message)) {

                foreach ($all_message as $message_unread) {
                    
                        $data_read = array('is_read' => 1);
                        $this->db->where('message_id', $message_unread->message_id);
                        $this->db->update('message_conversation', $data_read);
                    
                }
            }
            
            $data['message_thread']= $this->message_model->get_message_thread($equity_id, $user_id, $message_type);
            

            $data['site_setting'] = site_setting();


            $data['taxonomy_setting'] = taxonomy_setting();

            $theme = 'admin';

            $this->template->set_master_template($theme . '/template.php');
            $this->template->write('title', 'View Message ', '', TRUE);
            $this->template->write_view('header', 'header', $data, TRUE);
            $this->template->write_view('main_content', 'message/view_message', $data, TRUE);
            $this->template->write_view('footer', 'footer', '', TRUE);
            $this->template->render();
        
        } else {
            redirect('admin/home/dashboard');
        }

    }

    /*  code by shashi */

    function send_mail_project_profile($equity_id = '', $user_id = 0, $message_type = '')
    {

        $data['equity_id'] = $equity_id; 
        $equity_data = GetOneEquity($equity_id);
          
        
        $data['user_id'] = $user_id;
        $user_detail=UserData($user_id);
        
        if(!empty($equity_data) && !empty($user_detail)) {
        
            $equity_url = $equity_data['equity_url'];
            $company_name = $equity_data['company_name'];

            $taxonomy_setting = taxonomy_setting();
            $project_url = $taxonomy_setting['project_url'];
            $funds = $taxonomy_setting['funds'];

              $data['user_detail']=$user_detail;
            $data['equity_data']=$equity_data;

            $company_name_link = '<a href="' . site_url($project_url . '/' . $equity_url) . '">' . $company_name . '</a>';

            $message_type=2; //==for investor
            $subject = $company_name_link . ' ' . $funds . ' ' . PROCESS;
            if($equity_data['user_id']==$user_id){
                $message_type=1; //=for feedback
                 $subject = $company_name_link . ' ' . FEEDBACK;
            }  

            $this->load->library('form_validation');

            $this->form_validation->set_rules('comments', MESSAGES, 'required');
            if ($this->form_validation->run() == FALSE) {

               
                if (validation_errors()) {
                    $data['error_message'] = validation_errors();
                } else {
                    $data['error_message'] = '';
                }
                
                $data['site_setting'] = site_setting();
                $data['message_thread']= $this->message_model->get_message_thread($equity_id, $user_id, $message_type);
            

                $data['taxonomy_setting'] = taxonomy_setting();

                $theme = 'admin';

                $this->template->set_master_template($theme . '/template.php');
                $this->template->write('title', 'View Message ', '', TRUE);
                $this->template->write_view('header', 'header', $data, TRUE);
                $this->template->write_view('main_content', 'message/view_message', $data, TRUE);
                $this->template->write_view('footer', 'footer', '', TRUE);
                $this->template->render();

            } else {

                $login_id = $this->session->userdata('admin_id');

                $data['message'] = $this->message_model->get_message_detail($equity_id, $user_id, $message_type);
                $reply_message_id = 0;
                if (!empty($data['message'])) {
                    $reply_message_id = $data['message'][0]->message_id;
                } else {
                    project_activity_feedback('submit_by_admin', $login_id, $user_id, $equity_id);
                }
                
                $content = $this->input->post('comments');

               

                $data = array(
                    'sender_id' => 1,
                    'receiver_id' => $user_id,
                    'is_read' => 0,
                    'message_subject' => $subject,
                    'message_content' => $content,                   
                    'date_added' => date('Y-m-d H:i:s'),
                    'type' => $message_type,
                    'admin_replay' => 'admin',
                    'equity_id' => $equity_id,
                    'reply_message_id' => $reply_message_id,
                    'created_id' => $login_id,
                    'created' => date('Y-m-d H:i:s')
                );


                $message_insert = $this->message_model->insert_project_profile_message($data);
                
                if($reply_message_id > 0)
                {
                    $message_insert = $reply_message_id;
                }
                
                $message_setting = message_setting();
                $message_setting->message_enable;
               
                $user_detail = UserData($user_id, array());
                $user_name = $user_detail[0]['user_name'];
                
                $login_user_detail = UserData($login_id, array()); 
               // $message_user_name = $login_user_detail[0]['user_name'] . ' ' . $login_user_detail[0]['last_name'];
                  $getadmindata = adminsendmsg(1);
               
                $message_user_name = $getadmindata->username;

                $user['user_name'] = $user_detail[0]['user_name'] . ' ' . $user_detail[0]['last_name'];
                $user['email'] = $user_detail[0]['email'];
                $user['message_user_name'] = $message_user_name;
                $user['dateadded'] = date('Y-m-d');
                $user['subject'] = $subject;
                $user['message'] = $content;
                $user['view_message_link'] = '<a href="' . site_url('inbox/message_conversation/'.$message_insert) . '">' .CLICK_HERE . '</a>';
                $this->mailalerts('user_message', '', '', $user, 'Send message ');


                $this->session->set_flashdata('sucess_message', YOUR_MESSAGE_HAS_BEEN_SUCCESSFULLY);



                redirect('admin/message/view_message/' . $equity_id . '/' . $user_id);

            }
        } else {
            redirect('admin/home/dashboard');
        }
        
    }

    function list_adminmessage($msg = '')
    {
        //$msg=unserialize(urldecode($msg));

        $check_rights = get_rights('list_message');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        //$data['result'] = $this->message_model->get_message_result($offset, $limit);

        $data['result'] = $this->message_model->get_message_admin_result();
        $data['msg'] = $msg;


        $data['site_setting'] = site_setting();
        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Messages', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'message/list_adminmessage', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }


}

?>
