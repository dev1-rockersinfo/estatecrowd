<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Currency extends ROCKERS_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('home_model');

    }

    function index()
    {
        redirect('admin/currency/list_currency/');
    }

    /*
     Function name :add_currency()
    Parameter : none
    Return : none
    Use : To insert new record or update existing record of currency.
    */
    function add_currency()
    {

        $check_rights = get_rights('list_currency');

        if ($check_rights == 0) {
            redirect('home/dashboard/no_rights');
        }

        if ($this->session->userdata('admin_id') == '') {
            redirect('admin/home');
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('currency_name', CURRENCY_NAME, 'required|alpha_space');
        $this->form_validation->set_rules('currency_code', CURRENCY_CODE, 'required|alpha_space');
        //$this->form_validation->set_rules('currency_symbol', CURRENCY_SYMBOL, 'required|alpha_space');
        if ($this->form_validation->run() == FALSE) {
            if (validation_errors()) {
                $data["error"] = validation_errors();
            } else {
                $data["error"] = "";
            }
            $data["currency_code_id"] = $this->input->post('currency_code_id');
            $data["currency_name"] = $this->input->post('currency_name');
            $data["currency_code"] = $this->input->post('currency_code');
            $data["currency_symbol"] = $this->input->post('currency_symbol');

            $data["active"] = $this->input->post('active');

            $data['site_setting'] = site_setting();
            $theme = 'admin';

            $this->template->set_master_template($theme . '/template.php');

            $this->template->write('title', 'Currency', '', TRUE);
            $this->template->write_view('header', 'header', $data, TRUE);
            $this->template->write_view('main_content', 'other/add_currency', $data, TRUE);
            $this->template->write_view('footer', 'footer', '', TRUE);
            $this->template->render();
        } else {
            $data = array(
                'currency_name' => $this->input->post('currency_name'),
                'currency_code' => $this->input->post('currency_code'),
                'currency_symbol' => $this->input->post('currency_symbol'),
                'active' => $this->input->post('active'),
            );
            if ($this->input->post('currency_code_id')) {
                $this->home_model->table_update('currency_code_id', $this->input->post('currency_code_id'), 'currency_code', $data);
                $msg = "update";
            } else {
                $this->home_model->table_insert('currency_code', $data);
                $msg = "insert";
            }
            redirect('admin/currency/list_currency/' . $msg);
        }
    }

    /*
     Function name :edit_currency()
    Parameter : $id=currency id which you want to edit.
    Return : none
    Use : to edit the currency admin wants.
    */
    function edit_currency($id = 0)
    {
        $check_rights = get_rights('list_currency');

        if ($check_rights == 0) {
            redirect('home/dashboard/no_rights');
        }

        if ($this->session->userdata('admin_id') == '') {
            redirect('admin/home');
        }

        $one_currency = $this->home_model->get_one_tabledata('currency_code', array('currency_code_id' => $id));
        $data["error"] = "";
        $data["currency_code_id"] = $id;
        $data["currency_name"] = $one_currency['currency_name'];
        $data["currency_code"] = $one_currency['currency_code'];
        $data["currency_symbol"] = $one_currency['currency_symbol'];


        $data["active"] = $one_currency['active'];

        $data['site_setting'] = site_setting();
        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Currency', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'other/add_currency', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }

    /*
     Function name :delete_currency()
    Parameter : $id=currency id which you want to delete.
    Return : none
    Use : to delete the currency admin wants.
    */
    function delete_currency($id = 0)
    {
        $check_rights = get_rights('list_currency');

        if ($check_rights == 0) {
            redirect('home/dashboard/no_rights');
        }
        if ($this->session->userdata('admin_id') == '') {
            redirect('admin/home');
        }

        $this->db->delete('currency_code', array('currency_code_id' => $id));
        redirect('admin/currency/list_currency/delete');
    }

    /*
     Function name :list_currency()
    Parameter : $msg:message string of the last operation performed by admin.
    Return : none
    Use : to display list of currencies to admin.
    */
    function list_currency($msg = '')
    {

        $check_rights = get_rights('list_currency');

        if ($check_rights == 0) {
            redirect('home/dashboard/no_rights');
        }

        if ($this->session->userdata('admin_id') == '') {
            redirect('admin/home');
        }

        //$data['result'] = $this->currency_model->get_currency_result($offset, $limit);

        $data['result'] = get_table_data('currency_code', array('currency_name' => 'asc'));

        $data['msg'] = $msg;
        //$data['offset'] = $offset;
        //$data['limit']=$limit;
        $data['option'] = '';
        $data['keyword'] = '';
        $data['search_type'] = 'normal';

        $data['site_setting'] = site_setting();
        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Currency', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'other/list_currency', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }

    /*
     Function name :action_currency()
    Parameter : none
    Return : none
    Use : To activate or inactivate list of currencies together.
    */
    function action_currency()
    {

        $check_rights = get_rights('list_currency');

        if ($check_rights == 0) {
            redirect('home/dashboard/no_rights');
        }
        if ($this->session->userdata('admin_id') == '') {
            redirect('admin/home');
        }


        $offset = $this->input->post('offset');
        $action = $this->input->post('action');
        $page_id = $this->input->post('chk');
        $total_check = count($page_id);
        $total_currency = $this->check_currency_active();

        $left_currencies = $total_currency - $total_check;
        switch ($action) {
            case 'active':
                $msg = 'active';
                foreach ($page_id as $id) {
                    $this->db->query("update currency_code set active=1 where currency_code_id='" . $id . "'");
                }
                break;
            case 'inactive':
                $msg = 'inactive';
                if ($left_currencies <= 0) {
                    redirect('admin/currency/list_currency/cantproceed');
                } else {
                    foreach ($page_id as $id) {
                        $this->db->query("update currency_code set active=0 where currency_code_id='" . $id . "'");
                    }
                }
                break;
        }
        redirect('admin/currency/list_currency/' . $msg);
    }

    /*
     Function name :check_currency_active()
    Parameter : none
    Return : none
    Use : To check activate currencies so that admin cannot deactivate all the currencies.
    */
    function check_currency_active()
    {
        $query = $this->db->query("select * from currency_code where active=1");
        return $query->num_rows();
    }


}


?>
