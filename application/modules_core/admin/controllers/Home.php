<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Home extends ROCKERS_Controller
{

    function __construct()

    {

        parent::__construct();

        $this->load->model('home_model');

         $this->load->model('equity_model');

        $this->load->model('graph_model');

    }

    /*
     Function name :index()
    Parameter :$ln (message string)
    Return : none
    Use : to show the admin login page
    Description : this is used to show the admin login page to login into admin panel
    */

    function index($ln = '')

    {
        if ($this->session->userdata('admin_id') != '') {
            redirect('admin/home/dashboard');
        }

        $data['login'] = $ln;

        $data["error"] = "";

        $theme = 'admin';
        $this->template->set_master_template($theme . '/login_template.php');

        $this->template->write('title', 'Login', '', TRUE);

        $this->template->write_view('main_content', 'login', $data, TRUE);

        $this->template->render();
    }

    /*
     Function name :check_login()
    Parameter :none
    Return : none
    Use : to check admin authentication to login
    Description : to check admin authentication for valid username and password to login
    */


    function check_login()

    {

        $login = $this->home_model->is_login();

        if ($login == '1') {

            redirect("admin/home/dashboard");

        } else {

            redirect("admin/home/index/0");

        }

    }

    /*
     Function name :forgot_password()
    Parameter :none
    Return : none
    Use : to give forgot password request to admin
    Description : Email address of admin is entered to send email with password information
    */

    function forgot_password()

    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('email', EMAIL, 'required|valid_email');

        $data["login"] = "";

        if ($this->form_validation->run() == FALSE) {

            if (validation_errors()) {

                $data["error"] = validation_errors();

            } else {

                $data["error"] = "";

            }

            $theme = 'admin';

            $this->template->set_master_template($theme . '/login_template.php');

            $this->template->write('title', 'Forgot Password', '', TRUE);

            //$this->template->write_view('header', 'login_header', '', TRUE);

            $this->template->write_view('main_content', 'login', $data, TRUE);

            //$this->template->write_view('footer', 'footer', '', TRUE);

            $this->template->render();

        } else {
            $email = $this->input->post('email');
            $forgot_unique_code = randomNumber(20);
            $fdata = array(
                'forgot_unique_code' => $forgot_unique_code,
                'request_date' => date('Y-m-d H:i:s')
            );

            $this->db->where('email', $email);
            $this->db->update('admin', $fdata);

            $chk_mail = $this->home_model->forgot_email();
            if ($chk_mail == 0) {
                $data['error'] = 'email_not_found';

                $theme = 'admin';
                $this->template->set_master_template($theme . '/login_template.php');

                $this->template->write('title', 'Forgot Password', '', TRUE);

                //$this->template->write_view('header', 'login_header', '', TRUE);

                $this->template->write_view('main_content', 'login', $data, TRUE);

                //$this->template->write_view('footer', 'footer', '', TRUE);

                $this->template->render();
            } elseif ($chk_mail == 2) {

                $data['error'] = 'record_not_found';

                $theme = 'admin';
                $this->template->set_master_template($theme . '/login_template.php');

                $this->template->write('title', 'Forgot Password', '', TRUE);

                $this->template->write_view('main_content', 'login', $data, TRUE);

                $this->template->render();
            } else {

                $data['error'] = 'success';

                $theme = 'admin';
                $this->template->set_master_template($theme . '/login_template.php');

                $this->template->write('title', 'Forgot Password', '', TRUE);

                $this->template->write_view('main_content', 'login', $data, TRUE);

                $this->template->render();

            }

        }

    }


    function reset_password($forgot_unique_code = null)
    {  //print_r($_POST); die;
        $chk_forget_code = $this->home_model->checkForgetUniqueCode($forgot_unique_code);
        if ($chk_forget_code == 'expire') {
            redirect('admin/home/index/expire');
        }
        if ($chk_forget_code == 'not_sent') {
            redirect('admin/home/index/not_sent');
        }
        $user_id = $chk_forget_code;
        $this->load->library('form_validation');
        $this->form_validation->set_rules('password', PASSWORD, 'required|min_length[5]|max_length[20]|trim');
        $this->form_validation->set_rules('cpassword', RETYPE_PASSWORD, 'required|min_length[5]|max_length[20]|matches[password]|trim');
        $chk_user = 'false';
        $data['error'] = "";
        if ($this->form_validation->run() == FALSE || $chk_user == 'true') {

            if (validation_errors()) {
                $data['error'] = validation_errors();
            } else {
                $data['error'] = '';
            }
            if ($chk_user == 'true') {
                $spam_message = "<p>" . IPBAND_CANNOTREG_NOW . "</p>";
                $data['error'] = $spam_message;
            }
            if ($this->input->post('user_id')) {
                $data['user_id'] = SecurePostData($this->input->post('user_id'));
                $data['forgot_unique_code'] = SecurePostData($this->input->post('forget_unique_code'));
            } else {
                $data['user_id'] = $user_id;
                $data['forgot_unique_code'] = $forgot_unique_code;
            }
            $data['password'] = '';
            $data['cpassword'] = '';
        } else {
            $spass = array(
                'password' => SecurePostData(md5($this->input->post('password')))
            );
            $set = $this->home_model->setNewPassword($user_id, $spass);
            if ($set != '') {
                redirect('admin/home/index/reset_success');
            } else {
                redirect('admin/home/index/reset_fail');
            }
        }

        $data['error'] = '';
        $theme = 'admin';
        $this->template->set_master_template($theme . '/login_template.php');
        $this->template->write('title', 'Forgot Password', '', TRUE);
        $this->template->write_view('main_content', 'reset_password', $data, TRUE);
        $this->template->render();


    }

    /*
     Function name :dashboard()
    Parameter :none
    Return : none
    Use : To show dashboard of overall site to admin
    Description : details about users, projects and graphs is viewed onto this page by admin
    */

    function dashboard($msg = '')

    {

        //echo upload_url();
        /*	if($this->session->userdata('admin_id')=="")
            {
                redirect('admin/home');

            }
            */
        $assign_rights = get_rights('list_equity');

        if ($msg == 'project') {

            $assign_rights = get_rights('list_equity');

            if ($assign_rights == 0) {

                redirect('admin/home/dashboard/no_rights');

            }
        }

        /*	$this->load->library('pagination');

            $limit = '8';

            $config['base_url'] = site_url('admin/home/dashboard/'.$msg.'/'.$active.'/');

            $config['total_rows'] = $this->home_model->GetAllProjects('','',0,'1,2',array('user','project_category'),0,'','','yes');


            $config['per_page'] = $limit;

            $config['uri_segment'] = '5';

            $this->pagination->initialize($config);

            $data['page_link'] = $this->pagination->create_links();

            $data['latest_project'] = $this->home_model->GetAllProjects('','',0,'1,2',array('user','project_category'));

            $data['offset'] = $offset;

            */
        $data['msg'] = $msg;

        //$data['active']=$active;

        $data['site_setting'] = site_setting();


        //For fetching project results
        $data['pending_project'] = $this->home_model->GetAllCampaigns('', '', '1', array('user'));

        $data['completed_project'] = $this->home_model->GetAllCampaigns('', '', '3,4', array('user', 'transaction'), 5);

        $data['running_project'] = $this->home_model->GetAllCampaigns('', '', '2', array('user'), 5);

        $data['failure_project'] = $this->home_model->GetAllCampaigns('', '',  '5', array('user', 'transaction'), 5);


        //$data['customer'] = $this->home_model->get_customer();
        $data['latest_donors'] = $this->home_model->GetAllCampaigns('', '',  '', array('single_transaction'), 10, array('transaction_id' => 'desc'));
        //end donor n user list data fetch


        $data['total_transaction'] = $this->db->count_all('transaction');

        $data['total_project_posted'] = $this->home_model->GetAllCampaigns('', '', '2', array('user'), 0, '', '', 'yes');

        $data['total_completed_project'] = $this->home_model->get_total_completed_project();

        //data for cricles on dashboard
        $data['total_users'] = $this->home_model->get_all_users('', '', array(), array('user_id' => 'desc'), 'yes');

        $data['active_users'] = $this->home_model->get_all_users('1', '', array(), array('user_id' => 'desc'), 'yes');

        $data['inactive_users'] = $this->home_model->get_all_users('inactive', '', array(), array('user_id' => 'desc'), 'yes');

        $data['total_pending_proj'] = $this->home_model->GetAllCampaigns('', '','1', array('user',), 0, '', '', 'yes');

        $data['total_active_proj'] = $this->home_model->GetAllCampaigns('', '', '2', array('user',), 0, '', '', 'yes');

        $data['total_success_proj'] = $this->home_model->GetAllCampaigns('', '', '3,4', array('user',), 0, '', '', 'yes');

        $data['total_failure_proj'] = $this->home_model->GetAllCampaigns('', '',  '5', array('user',), 0, '', '', 'yes');

        $data['total_running_investment'] = $this->equity_model->InvestorRunningProcessHistory('yes');
        
       

        /*	$data['total_earned_month'] = $this->home_model->get_total_earned_month();

            $data['total_earned_week'] = $this->home_model->get_total_earned_week();

            $data['total_earned_today'] = $this->home_model->get_total_earned_today();

            $data['total_lost'] = $this->home_model->get_total_lost();

            $data['total_lost_month'] = $this->home_model->get_total_lost_month();

            $data['total_lost_week'] = $this->home_model->get_total_lost_week();

            $data['total_lost_today'] = $this->home_model->get_total_lost_today();

            $data['total_pipeline'] = $this->home_model->get_total_pipeline();

            $data['total_pipeline_month'] = $this->home_model->get_total_pipeline_month();

            $data['total_pipeline_week'] = $this->home_model->get_total_pipeline_week();

            $data['total_pipeline_today'] = $this->home_model->get_total_pipeline_today();

            //end transaction tab

            $date=date('Y-m-d');

            $week_first_date= get_first_day_of_week($date);

            $week_last_date= get_last_day_of_week($date);

            $data['week_first_date']=$week_first_date;

            $data['week_last_date']=$week_last_date;
        */
        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');

        $this->template->write('title', 'Dashboard', '', TRUE);

        $this->template->write_view('header', 'header', $data, TRUE);

        $this->template->write_view('main_content', 'dashboard', $data, TRUE);

        $this->template->write_view('footer', 'footer', '', TRUE);

        $this->template->render();

    }

    /*
     Function name :ajax_dhasboard_user()
    Parameter :none
    Return : none
    Use : To show or fetch user tab data on dashboard to admin to reduce page load it is called on ajax.
    */
    function ajax_dhasboard_user()
    {
        //For Fetching user and donor list
        $data['site_setting'] = site_setting();

        $data['users'] = $this->home_model->get_all_users('', 10);

        $data['inactive_users'] = $this->home_model->get_all_users('inactive', 5);

        $this->load->view('list_dashboard_user', $data);

    }

    /*
     Function name :logout()
    Parameter :none
    Return : none
    Use : to logout of current session.
    */
    function logout()

    {

        $this->session->unset_userdata('admin_id');

        redirect("admin/home/index/1");

    }

    /*
     Function name :error_page()
    Parameter :none
    Return : none
    Use : This function is use for displaying 404 page.
    Description : This error_page function is called http://hostname/home/error_page.
    Done By : Dhaval
    */

    function error_page()
    {
        $meta = meta_setting();

        $data['site_setting'] = site_setting();
        //$this->template->write('meta_title', $meta['title'], TRUE);
        //$this->template->write('meta_description', $meta['meta_description'], TRUE);
        //$this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $theme = 'admin';
        $this->template->set_master_template($theme . '/login_template.php');
        //$this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', '404_page', $data, TRUE);
        $this->template->write_view('footer', 'footer', $data, TRUE);
        $this->template->render();
    }

    /*
     Function name :my_account()
    Parameter :none
    Return : none
    Use : Access to my account page to admin.
    */
    function my_account()

    {

        $this->load->library('form_validation');


        $this->form_validation->set_rules('username', NAMES, 'required');

        if ($this->form_validation->run() == FALSE) {

            if (validation_errors()) {

                $data["error"] = validation_errors();

            } else {

                $data["error"] = "";

            }

            if ($this->input->post('admin_id')) {

                $data["admin_id"] = $this->input->post('admin_id');

                $data["username"] = $this->input->post('username');

                $data["password"] = $this->input->post('password');

                $data["email"] = $this->input->post('email');


            } else {

                $my_account = $this->home_model->get_my_account($this->session->userdata('admin_id'));

                $data["admin_id"] = $my_account['admin_id'];

                $data["username"] = $my_account['username'];

                $data["password"] = $my_account['password'];

                $data["email"] = $my_account['email'];

            }

            $data['site_setting'] = site_setting();

            $this->template->write('title', 'My Account', '', TRUE);

            $this->template->write_view('header', 'header', $data, TRUE);

            $this->template->write_view('main_content', 'my_account', $data, TRUE);

            $this->template->write_view('footer', 'footer', '', TRUE);

            $this->template->render();

        } else {

            $this->home_model->my_account_update();

            $data["error"] = "suceess";

            $data["admin_id"] = $this->input->post('admin_id');

            $data["username"] = $this->input->post('username');

            $data["password"] = $this->input->post('password');

            $data["email"] = $this->input->post('email');


            $data['site_setting'] = site_setting();


            $this->template->write('title', 'My Account', '', TRUE);

            $this->template->write_view('header', 'header', $data, TRUE);

            $this->template->write_view('main_content', 'my_account', $data, TRUE);

            $this->template->write_view('footer', 'footer', '', TRUE);

            $this->template->render();

        }

    }


    /*
     Function name :comments()
    Parameter :$id(project_id)
    Return : none
    Use : display project's comment to admin
    Description : Particular project's comment can be viewed by admin.

    function comments($id,$msg='')

    {
        $data['result'] = $this->home_model->get_project_comment_result($id);

        $data['msg'] = $msg;

        $data['site_setting'] = site_setting();

        $theme='admin';

        $this->template->set_master_template($theme .'/template.php');

        $this->template->write('title', 'Project Comments', '', TRUE);

        $this->template->write_view('header', 'header', $data, TRUE);

        $this->template->write_view('main_content', 'project/list_project_comments', $data, TRUE);

        $this->template->write_view('footer', 'footer', '', TRUE);

        $this->template->render();

    }
    */

}

?>
