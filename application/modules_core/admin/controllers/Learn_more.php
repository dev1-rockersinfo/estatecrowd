<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Learn_more extends ROCKERS_Controller
{
    function __construct()
    {
        parent:: __construct();
        $this->load->model('learn_more_model');
        $this->load->model('home_model');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('template');
        $this->load->library('session');
    }

    function index()
    {
        redirect('admin/learn_more/list_pages');
    }

    /*
     Function name :list_learn_category()
    Parameter :$msg=message string to notify admin about the operation he performed.
    Return : none
    Use : Function is used for listing all the learn more pages categories.
    */
    function list_learn_category($msg = '')
    {

        $check_rights = get_rights('list_pages');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        //$data['result'] = $this->learn_more_model->get_learn_category_result($offset, $limit);
        $data['result'] = get_table_data('learn_more_category', array('category_id' => 'desc'));
        $data['msg'] = $msg;

        $data['option'] = '';
        $data['keyword'] = '';
        $data['search_type'] = 'normal';

        $data['site_setting'] = site_setting();

        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Pages', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'content_pages/list_learn_category', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }

    /*
     Function name :add_category()
    Parameter :none
    Return : none
    Use : Function used adding learn more category.
    */
    function add_category()
    {

        $check_rights = get_rights('list_pages');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }


        $this->load->library('form_validation');

        $this->form_validation->set_rules('category_name', CATEGORY_NAME, 'required');
        $data['query'] = $this->home_model->get_language('language', array('active' => 1));

        if ($this->form_validation->run() == FALSE) {
            if (validation_errors()) {
                $data["error"] = validation_errors();
            } else {
                $data["error"] = "";
            }
            $data["pages_id"] = $this->input->post('category_id');
            $data["pages_title"] = $this->input->post('category_name');
            $data["active"] = $this->input->post('active');
            $data["right_side"] = $this->input->post('sidebar');
            $data["footer_bar"] = $this->input->post('footer_bar');
            $data["language"] = $this->input->post('language');
            $data['site_setting'] = site_setting();
            $theme = 'admin';

            $this->template->set_master_template($theme . '/template.php');
            $this->template->write('title', 'Pages', '', TRUE);
            $this->template->write_view('header', 'header', $data, TRUE);
            $this->template->write_view('main_content', 'content_pages/add_learn_category', $data, TRUE);
            $this->template->write_view('footer', 'footer', '', TRUE);
            $this->template->render();
        } else {
            $data = array(
                'category_name' => $this->input->post('category_name'),
                'active' => $this->input->post('active'),
                'footer' => $this->input->post('footer_bar'),
                'language_id' => $this->input->post('language'),
                'right_side' => $this->input->post('side_bar')
            );
            if ($this->input->post('category_id')) {
                $this->home_model->table_update('category_id', $this->input->post('category_id'), 'learn_more_category', $data);
                $msg = "update";
            } else {
                $this->home_model->table_insert('learn_more_category', $data);
                $msg = "insert";
            }
            redirect('admin/learn_more/list_learn_category/' . $msg);
        }
    }

    /*
     Function name :edit_learn_category()
    Parameter :$id=id of the category that is selected for editing or updating.
    Return : none
    Use : Function used to view information of category selected for updating.
    */
    function edit_learn_category($id = 0)
    {

        $check_rights = get_rights('list_pages');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }
        $data['query'] = $this->home_model->get_language('language', array('active' => 1));
        $one_pages = $this->home_model->get_one_tabledata('learn_more_category', array('category_id' => $id));
        $data["error"] = "";
        $data["pages_id"] = $id;
        $data["pages_title"] = $one_pages['category_name'];
        $data["active"] = $one_pages['active'];
        $data["footer_bar"] = $one_pages['footer'];
        $data["right_side"] = $one_pages['right_side'];
        $data["language_id"] = $one_pages['language_id'];
        $data['site_setting'] = site_setting();
        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Pages', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'content_pages/add_learn_category', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }

    /*
     Function name :delete_learn_category()
    Parameter :$id=id of the category that is selected for deleting.
    Return : none
    Use : Function used to delete particular learn more page category
    */
    function delete_learn_category($id = 0)
    {
        $check_rights = get_rights('list_pages');

        if ($check_rights == 0) {
            redirect('home/dashboard/no_rights');
        }

        $this->db->delete('learn_more_category', array('category_id' => $id));
        redirect('admin/learn_more/list_learn_category/delete');
    }

    /*
     Function name :action_learn_category()
    Parameter :none
    Return : none
    Use : Function used to delete,active or inactive selected learn more page categories.
    */
    function action_learn_category()
    {
        $check_rights = get_rights('list_pages');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        $offset = $this->input->post('offset');
        $action = $this->input->post('action');
        $page_id = $this->input->post('chk');

        //print_r($_POST);die();
        switch ($action) {
            case 'active':
                $msg = 'active';
                foreach ($page_id as $id) {
                    $this->db->query("update learn_more_category set active=1 where category_id='" . $id . "'");
                }
                break;
            case 'inactive':
                $msg = 'inactive';
                foreach ($page_id as $id) {
                    $this->db->query("update learn_more_category set active=0 where category_id='" . $id . "'");
                }
                break;
            case 'delete':
                $msg = 'delete';
                foreach ($page_id as $id) {
                    $this->db->query("delete from learn_more_category where category_id='" . $id . "'");
                }
                break;
        }
        redirect('admin/learn_more/list_learn_category/' . $msg);
    }


    /*
     Function name :add_pages()
    Parameter :none
    Return : none
    Use : Function used adding learn more pages.
    */
    function add_pages()
    {


        $check_rights = get_rights('list_pages');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        $data['query'] = $this->home_model->get_language('language', array('active' => 1));
        $this->load->library('form_validation');

        $this->form_validation->set_rules('pages_title', 'Pages Title', 'required');
        $this->form_validation->set_rules('description', 'Pages Description', 'required');
        //	$this->form_validation->set_rules('external_link', 'External URL', 'valid_url');
        $this->form_validation->set_rules('slug', 'Slug', 'required');
        $image_err = '';
        if ($_POST) {
            if ($_FILES["file_up"]["name"] != '') {
                if ($_FILES["file_up"]["type"] != "image/jpeg" and $_FILES["file_up"]["type"] != "image/pjpeg" and $_FILES["file_up"]["type"] != "image/png" and $_FILES["file_up"]["type"] != "image/x-png" and $_FILES["file_up"]["type"] != "image/gif") {

                    $image_err = ALLOW_ONLY_IMAGE_FILE_TO_UPLOAD;

                }
            }
        }
        if ($this->form_validation->run() == FALSE or $image_err != '') {
            if (validation_errors()) {
                $data["error"] = validation_errors() . $image_err;
            } else {
                $data["error"] = "" . $image_err;
            }
            $data["pages_id"] = $this->input->post('pages_id');
            $data["pages_title"] = $this->input->post('pages_title');
            $data["description"] = $this->input->post('description');
            $data["slug"] = $this->input->post('slug');
            $data["active"] = $this->input->post('active');
            $data["language"] = $this->input->post('language');
            $data["header_bar"] = $this->input->post('header_bar');
            $data["footer_bar"] = $this->input->post('footer_bar');
            $data["left_side"] = $this->input->post('left_side');
            $data["right_side"] = $this->input->post('right_side');
            $data["external_link"] = $this->input->post('external_link');

            $data["sub_title"] = $this->input->post('sub_title');
            $data["icon"] = $this->input->post('file_up');
            $data["prev_icon"] = $this->input->post('prev_icon');
            $data['learn_category'] = $this->input->post('learn_category');

            $data["meta_keyword"] = $this->input->post('meta_keyword');
            $data["meta_description"] = $this->input->post('meta_description');
            $data['learn_category_list'] = learn_category();

            $data['site_setting'] = site_setting();
            $theme = 'admin';

            $this->template->set_master_template($theme . '/template.php');
            $this->template->write('title', 'Pages', '', TRUE);
            $this->template->write_view('header', 'header', $data, TRUE);
            $this->template->write_view('main_content', 'content_pages/add_learn_more', $data, TRUE);
            $this->template->write_view('footer', 'footer', '', TRUE);
            $this->template->render();
        } else {
            if ($this->input->post('pages_id')) {
                $this->learn_more_model->pages_update();
                $msg = "update";
            } else {
                $this->learn_more_model->pages_insert();
                $msg = "insert";
            }
            redirect('admin/learn_more/list_pages/' . $msg);
        }
    }

    /*
     Function name :edit_pages()
    Parameter :$id=id of the learn more page that is selected for editing or updating.
    Return : none
    Use : Function used to view information of learn more page selected for updating.
    */
    function edit_pages($id = 0)
    {

        $check_rights = get_rights('list_pages');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }
        $data['query'] = $this->home_model->get_language('language', array('active' => 1));
        $one_pages = $this->home_model->get_one_tabledata('learn_more', array('pages_id' => $id));
        $data["error"] = "";
        $data["pages_id"] = $id;
        $data["pages_title"] = $one_pages['pages_title'];
        $data["description"] = $one_pages['description'];
        $data["slug"] = $one_pages['slug'];
        $data["active"] = $one_pages['active'];
        $data["meta_keyword"] = $one_pages['meta_keyword'];
        $data["meta_description"] = $one_pages['meta_description'];
        $data['learn_category_list'] = learn_category();
        $data["header_bar"] = $one_pages['header_bar'];
        $data["footer_bar"] = $one_pages['footer_bar'];
        $data["left_side"] = $one_pages['left_side'];
        $data["right_side"] = $one_pages['right_side'];
        $data["external_link"] = $one_pages['external_link'];
        $data["sub_title"] = $one_pages['right_side'];
        $data["prev_icon"] = $one_pages['icon_image'];
        $data["sub_title"] = $one_pages['sub_title'];
        $data["learn_category"] = $one_pages['learn_category'];
        $data["language_id"] = $one_pages['language_id'];
        $data['site_setting'] = site_setting();
        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Pages', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'content_pages/add_learn_more', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }

    /*
     Function name :delete_pages()
    Parameter :$id=id of the learn page that is selected for deleting.
    Return : none
    Use : Function used to delete particular learn more page.
    */
    function delete_pages($id = 0)
    {
        $check_rights = get_rights('list_pages');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        $this->db->delete('learn_more', array('pages_id' => $id));
        redirect('admin/learn_more/list_pages/delete');
    }

    /*
     Function name :list_pages()
    Parameter :$msg=message string to notify admin about the operation he performed.
    Return : none
    Use : Function is used for listing all the learn more pages
    */
    function list_pages($msg = '')
    {

        $check_rights = get_rights('list_pages');

        if ($check_rights == 0) {
            redirect('home/dashboard/no_rights');
        }

        //$data['result'] = $this->learn_more_model->get_pages_result($offset, $limit);
        $data['result'] = get_table_data('learn_more', array('pages_id' => 'desc'));
        $data['msg'] = $msg;

        $data['search_type'] = 'normal';

        $data['site_setting'] = site_setting();

        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Pages', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'content_pages/list_learn_more', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }

    /*
     Function name :action_learn_more()
    Parameter :none
    Return : none
    Use : Function used to delete,active or inactive selected learn more pages.
    */
    function action_learn_more()
    {
        //$limit=20;
        $check_rights = get_rights('list_pages');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        $offset = $this->input->post('offset');
        $action = $this->input->post('action');
        $page_id = $this->input->post('chk');

        //print_r($_POST);die();
        switch ($action) {
            case 'active':
                $msg = 'active';
                foreach ($page_id as $id) {
                    $this->db->query("update learn_more set active=1 where pages_id='" . $id . "'");
                }
                break;
            case 'inactive':
                $msg = 'inactive';
                foreach ($page_id as $id) {
                    $this->db->query("update learn_more set active=0 where pages_id='" . $id . "'");
                }
                break;
            case 'delete':
                $msg = 'delete';
                foreach ($page_id as $id) {
                    $this->db->query("delete from learn_more where pages_id='" . $id . "'");
                }
                break;
        }
        redirect('admin/learn_more/list_pages/' . $msg);
    }
    /*	function home_page()
     {

    $check_rights=$this->home_model->get_rights('home_page');

    if(	$check_rights==0) {
    redirect('home/dashboard/no_rights');
    }


    $this->load->library('form_validation');

    $this->form_validation->set_rules('home_description', 'Description', 'required');
    $this->form_validation->set_rules('home_title', 'Title', 'required');


    if($this->form_validation->run() == FALSE){
    if(validation_errors())
    {
    $data["error"] = validation_errors();
    }else{
    $data["error"] = "";
    }

    if($this->input->post('home_id'))
    {
    $data["home_description"] = $this->input->post('home_description');
    $data["home_title"] = $this->input->post('home_title');
    $data["active"] = $this->input->post('active');
    $data["home_id"] = $this->input->post('home_id');


    }else{
    $home_page= $this->db->query("select * from home_page where home_id='1'");

    $get_home_page=$home_page->row_array();

    $data["home_id"] = $get_home_page['home_id'];
    $data["home_description"] = $get_home_page['home_description'];
    $data["home_title"] = $get_home_page['home_title'];
    $data["active"] = $get_home_page['active'];
    }


    $data['site_setting'] = $this->home_model->select_site_setting();
    $theme='admin';

    $this->template->set_master_template($theme .'/template.php');
    $this->template->write('title', 'Home Page', '', TRUE);
    $this->template->write_view('header', 'header', $data, TRUE);
    $this->template->write_view('main_content', 'home_page', $data, TRUE);
    $this->template->write_view('footer', 'footer', '', TRUE);
    $this->template->render();

    }else{


    $this->db->query("update home_page set home_title='".$this->input->post('home_title')."', home_description='".$this->input->post('home_description')."',active='".$this->input->post('active')."' where home_id='1'");

    $data["error"] = "Home Page updated successfully";
    $data["home_description"] = $this->input->post('home_description');
    $data["home_title"] = $this->input->post('home_title');
    $data["active"] = $this->input->post('active');
    $data["home_id"] = $this->input->post('home_id');

    $data['site_setting'] = $this->home_model->select_site_setting();
    $theme='admin';

    $this->template->set_master_template($theme .'/template.php');
    $this->template->write('title', 'Home Page', '', TRUE);
    $this->template->write_view('header', 'header', $data, TRUE);
    $this->template->write_view('main_content', 'home_page', $data, TRUE);
    $this->template->write_view('footer', 'footer', '', TRUE);
    $this->template->render();

    }
    }

    */
}

?>
