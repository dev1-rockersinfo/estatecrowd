<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Newsletter extends ROCKERS_Controller

{
    function __construct()
    {
        parent::__construct();
        $this->load->model('newsletter_model');

        $this->load->model('home_model');
        $this->load->helper('download');
    }

    function index()
    {
        redirect('admin/newsletter/list_newsletter');
    }

    /*
     Function name :list_newsletter()
    Parameter :$msg (message string)
    Return : none
    Use : This function shows list of newsletter templates with it's users and created date.
    */
    function list_newsletter($msg = '')
    {
        $check_rights = get_rights('list_newsletter');
        if ($check_rights == 0) {
            redirect('home/dashboard/no_rights');
        }

        $tempo = date('d-m-Y');
        $data['result'] = $this->newsletter_model->get_template_result();

        // print_r($data['result']);

        $data['msg'] = $msg;
        $data['site_setting'] = site_setting();
        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Newsletters', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'newsletter/list_newsletter', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }


    /*
     Function name :add_newsletter()
    Return : none
    Use : Admin can add new newsletter.
    */
    function add_newsletter()
    {
        $check_rights = get_rights('list_newsletter');
        if ($check_rights == 0) {
            redirect('home/dashboard/no_rights');
        }

        // $this->load->library('form_validation');

        $this->form_validation->set_rules('subject', SUBJECT, 'required');
        if ($this->input->post('project_id') == '' || $this->input->post('project_id') > 0) {
            $this->form_validation->set_rules('template_content', CONTENT, 'required');
        } else {
            $this->form_validation->set_rules('project_id', PROJECT, 'required');
        }

        if ($this->form_validation->run() == FALSE) {
            if (validation_errors()) {
                $data["error"] = validation_errors();
            } else {
                $data["error"] = "";
            }

            $data["newsletter_id"] = $this->input->post('newsletter_id');
            $data["subject"] = $this->input->post('subject');
            $data["file_up"] = $this->input->post('file_up');
            $data["prev_attach_file"] = $this->input->post('prev_attach_file');
            $data["allow_subscribe_link"] = $this->input->post('allow_subscribe_link');
            $data["allow_unsubscribe_link"] = $this->input->post('allow_unsubscribe_link');
            $data["project_id"] = $this->input->post('project_id');
            $data['subscribe_to'] = $this->input->post('subscribe_to');
            $data['site_setting'] = site_setting();

            // $data['all_project'] = $this->newsletter_model->all_project();

            $data["template_content"] = $this->input->post('template_content');
            if ($this->input->post('offset') == "") {

                // $limit = '10';

                $totalRows = $this->newsletter_model->get_total_template_count();

                // $data["offset"] = (int)($totalRows/$limit)*$limit;

            } else {

                // $data["offset"] = $this->input->post('offset');

            }

            $theme = 'admin';
            $this->template->set_master_template($theme . '/template.php');
            $this->template->write('title', 'Add Newsletters', '', TRUE);
            $this->template->write_view('header', 'header', $data, TRUE);
            $this->template->write_view('main_content', 'newsletter/add_newsletter', $data, TRUE);
            $this->template->write_view('footer', 'footer', '', TRUE);
            $this->template->render();
        } else {
            $CI = &get_instance();
            $base_url = $CI->config->slash_item('base_url_site');
            $base_path = $CI->config->slash_item('base_path');
            if ($this->input->post('newsletter_id')) {
                $this->newsletter_model->newsletter_update();
                $msg = "update";
            } else {
                $this->newsletter_model->newsletter_insert();
                $msg = "insert";
            }

            // $offset = $this->input->post('offset');

            redirect('admin/newsletter/list_newsletter/' . $msg);
        }
    }

    /*
     Function name :edit_newsletter()
    Return : none
    Use : Admin can update newsletter.
    */
    function edit_newsletter($id = 0)
    {
        $check_rights = get_rights('list_newsletter');
        if ($check_rights == 0) {
            redirect('home/dashboard/no_rights');
        }

        if ($id == 0 || $id == '') {
            redirect('newsletter/list_newsletter/');
        }

        $one_newsletter = $this->newsletter_model->get_one_newsletter($id);
        $data["error"] = "";
        $data["newsletter_id"] = $id;
        $data["subject"] = $one_newsletter->subject;
        $data["template_content"] = $data["file_up"] = '';
        $data["prev_attach_file"] = $one_newsletter->attach_file;
        $data["allow_subscribe_link"] = $one_newsletter->allow_subscribe_link;
        $data["allow_unsubscribe_link"] = $one_newsletter->allow_unsubscribe_link;
        $data["project_id"] = $one_newsletter->project_id;
        $data['site_setting'] = site_setting();
        $data['all_project'] = $this->newsletter_model->all_project();
        $data["template_content"] = $one_newsletter->template_content;
        $data['subscribe_to'] = '';

        // $data["offset"] = $offset;

        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Edit Newsletters', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'newsletter/add_newsletter', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }

    /*
     Function name :action_newsletter()
    Return : none
    Use : Admin can delete newsletter.
    */
    function action_newsletter()
    {
        $check_rights = get_rights('list_newsletter');

        // $limit=20;

        if ($check_rights == 0) {
            redirect('home/dashboard/no_rights');
        }

        $action = $this->input->post('action');
        $newsletter_id = $this->input->post('chk');
        if ($action == 'delete') {
            foreach ($newsletter_id as $id) {
                $this->newsletter_model->delete_newsletter($id);
            }

            redirect('admin/newsletter/list_newsletter/delete');
        }
    }


    /*
     Function name :action_newsletter_subscriber()
     Return : none
     Use : Admin can delete newsletter user.
     */
    function action_newsletter_subscriber()
    {
        $check_rights = get_rights('list_newsletter');
        if ($check_rights == 0) {
            redirect('home/dashboard/no_rights');
        }

        $offset = $this->input->post('offset');
        $action = $this->input->post('action');
        $newsletter_user_id = $this->input->post('chk');
        $newsletter_id = $this->input->post('newsletter_id');
        if ($action == 'delete') {
            foreach ($newsletter_user_id as $id) {
                $this->newsletter_model->delete_user_subscription($id, $newsletter_id);
            }

            $offset = $this->input->post('offset');
            redirect('newsletter/show_all_subscriber/' . $newsletter_id . '/' . $offset . '/delete');
        }
    }

    // ////////////////===============subscriber part====================
    // //////////////////=============Newsletter User=================

    /*
     Function name :newsletter_job()
    Parameter :$msg (message string)
    Return : none
    Use : This function shows newsletter subscribe users.
    */
    function list_newsletter_user($msg = '')
    {
        $check_rights = get_rights('list_newsletter_user');
        if ($check_rights == 0) {
            redirect('home/dashboard/no_rights');
        }

        $this->load->library('pagination');
        $data['result'] = $this->newsletter_model->get_user_result();
        $data['msg'] = $msg;

        // $data['offset'] = $offset;
        // $data['limit']=$limit;

        $data['option'] = '';
        $data['keyword'] = '';
        $data['search_type'] = 'normal';
        $data['site_setting'] = site_setting();
        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Newsletter Users', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'newsletter/list_newsletter_user', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }


    /*Function name :email_check()
     Parameter :$email
    Return : none
    Use : This is the callback function used to check email address.
    */
    function email_check($email)
    {
        $username = $this->newsletter_model->user_unique($email);
        if ($username == TRUE) {
            return TRUE;
        } else {
            $this->form_validation->set_message('email_check', EXISTS_ACCOUNT_ASSOCIATE_WITHMAIL);
            return FALSE;
        }
    }

    /*
     Function name :add_newsletter_user()
    Use : With this function admin can add newsletter user.
    */
    function add_newsletter_user()
    {
        $check_rights = get_rights('list_newsletter_user');
        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        $username = $this->newsletter_model->user_unique($this->input->post('email'));
       
        $email_error = '';
        if($_POST)
        {
            if ($username == 'FALSE' ) {
                //$this->form_validation->set_message('email_check', EXISTS_ACCOUNT_ASSOCIATE_WITHMAIL);
                $email_error = EXISTS_ACCOUNT_ASSOCIATE_WITHMAIL;
                
            } else {
               $email_error = '';
            }
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', EMAIL, 'required|valid_email');
        $this->form_validation->set_rules('user_name', USER_NAME, 'required|alpha_space');
        if ($this->form_validation->run() == FALSE || $email_error != '') {
            if (validation_errors() || $email_error != '') {
                $data["error"] = validation_errors().$email_error;
            } else {
                $data["error"] = "";
            }

            $data["newsletter_user_id"] = $this->input->post('newsletter_user_id');
            $data["email"] = $this->input->post('email');
            $data["user_name"] = $this->input->post('user_name');
            $data['site_setting'] = site_setting();
            $data['all_newsletter'] = $this->newsletter_model->get_all_newsletter_templates();
            if ($this->input->post('newsletter_user_id') == '') {
                $data['all_subscription'] = '';
            } else {
                $data['all_subscription'] = $this->newsletter_model->get_all_subscription($this->input->post('newsletter_user_id'));
            }

            $theme = 'admin';
            $this->template->set_master_template($theme . '/template.php');
            $this->template->write('title', 'Add Users', '', TRUE);
            $this->template->write_view('header', 'header', $data, TRUE);
            $this->template->write_view('main_content', 'newsletter/add_user', $data, TRUE);
            $this->template->write_view('footer', 'footer', '', TRUE);
            $this->template->render();
        } else {
            $CI = &get_instance();
            $base_url = $CI->config->slash_item('base_url_site');
            $base_path = $CI->config->slash_item('base_path');
            if ($this->input->post('newsletter_user_id')) {
                $this->newsletter_model->user_update();
                $msg = "update";
            } else {
                $this->newsletter_model->user_insert();
                $msg = "insert";
            }

            redirect('admin/newsletter/list_newsletter_user/' . $msg);
        }
    }

    /*
     Function name :edit_newsletter_user()
    Parameter :$id (id of User)
    Return : none
    Use : This function is used to update the user with subscribe newsletter.
    */
    function edit_newsletter_user($id = 0)
    {
        $check_rights = get_rights('list_newsletter_user');
        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        $one_user = $this->newsletter_model->get_one_user($id);
        $data["error"] = "";
        $data["newsletter_user_id"] = $id;
        $data["email"] = $one_user['email'];
        $data["user_name"] = $one_user['user_name'];
        $data['all_newsletter'] = $this->newsletter_model->get_all_newsletter_templates();
        $data['site_setting'] = site_setting();
        $data['all_subscription'] = $this->newsletter_model->get_all_subscription($id);

        // $data["offset"] = $offset;

        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Edit Users', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'newsletter/add_user', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }

    /*
     Function name :action_newsletter_user()
    Parameter :none
    Return : none
    Use : This function is used to delete newsletter user.
    */
    function action_newsletter_user()
    {
        $check_rights = get_rights('list_newsletter_user');
        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        $action = $this->input->post('action');
        $newsletter_user_id = $this->input->post('chk');
        if ($action == 'delete') {
            foreach ($newsletter_user_id as $id) {
                $this->newsletter_model->delete_newsletter_user($id);
            }

            redirect('admin/newsletter/list_newsletter_user/delete');
        }
    }

    // ////===========CSV ======PART============

    /*
     Function name :force_download()
    Parameter :$filename, $data, $enable_partial, $speedlimit
    Return : none
    Use : This function is used to download newsletter csv file.
    */
    function force_download($filename = '', $data = false, $enable_partial = true, $speedlimit = 0)
    {
        if ($filename == '') {
            return FALSE;
        }

        if ($data === false && !file_exists($filename)) return FALSE;

        // Try to determine if the filename includes a file extension.
        // We need it in order to set the MIME type

        if (FALSE === strpos($filename, '.')) {
            return FALSE;
        }

        // Grab the file extension

        $x = explode('.', $filename);
        $extension = end($x);

        // Load the mime types

        @include(APPPATH . 'config/mimes' . EXT);

        // Set a default mime if we can't find it

        if (!isset($mimes[$extension])) {
            if (ereg('Opera(/| )([0-9].[0-9]{1,2})', $_SERVER['HTTP_USER_AGENT'])) $UserBrowser = "Opera";
            elseif (ereg('MSIE ([0-9].[0-9]{1,2})', $_SERVER['HTTP_USER_AGENT'])) $UserBrowser = "IE";
            else $UserBrowser = '';
            $mime = ($UserBrowser == 'IE' || $UserBrowser == 'Opera') ? 'application/octetstream' : 'application/octet-stream';
        } else {
            $mime = (is_array($mimes[$extension])) ? $mimes[$extension][0] : $mimes[$extension];
        }

        $size = $data === false ? filesize($filename) : strlen($data);
        if ($data === false) {
            $info = pathinfo($filename);
            $name = $info['basename'];
        } else {
            $name = $filename;
        }

        // Clean data in cache if exists

        @ob_end_clean();

        // Check for partial download

        if (isset($_SERVER['HTTP_RANGE']) && $enable_partial) {
            list($a, $range) = explode("=", $_SERVER['HTTP_RANGE']);
            list($fbyte, $lbyte) = explode("-", $range);
            if (!$lbyte) $lbyte = $size - 1;
            $new_length = $lbyte - $fbyte;
            header("HTTP/1.1 206 Partial Content", true);
            header("Content-Length: $new_length", true);
            header("Content-Range: bytes $fbyte-$lbyte/$size", true);
        } else {
            header("Content-Length: " . $size);
        }

        // Common headers

        header('Content-Type: ' . $mime, true);
        header('Content-Disposition: attachment; filename="' . $name . '"', true);
        header("Expires: 0", true);
        header('Accept-Ranges: bytes', true);
        header("Cache-control: private", true);
        header('Pragma: private', true);
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

        // Open file

        if ($data === false) {
            $file = fopen($filename, 'r');
            if (!$file) return FALSE;
        }

        // Cut data for partial download

        if (isset($_SERVER['HTTP_RANGE']) && $enable_partial)
            if ($data === false) fseek($file, $range);
            else $data = substr($data, $range);

        // Disable script time limit

        @set_time_limit(0);

        // Check for speed limit or file optimize

        if ($speedlimit > 0 || $data === false) {
            if ($data === false) {
                $chunksize = $speedlimit > 0 ? $speedlimit * 1024 : 512 * 1024;
                while (!feof($file) and (connection_status() == 0)) {
                    $buffer = fread($file, $chunksize);
                    echo $buffer;
                    flush();
                    if ($speedlimit > 0) sleep(1);
                }

                fclose($file);
            } else {
                $index = 0;
                $speedlimit *= 1024; //convert to kb
                while ($index < $size and (connection_status() == 0)) {
                    $left = $size - $index;
                    $buffersize = min($left, $speedlimit);
                    $buffer = substr($data, $index, $buffersize);
                    $index += $buffersize;
                    echo $buffer;
                    flush();
                    sleep(1);
                }
            }
        } else {
            echo $data;
        }

        $this->db->cache_delete_all();
        ob_clean();
        flush();
    }

    /*
     Function name :export_newsletter_user()
    Parameter :$time (time string)
    Use : This function downloads csv file of newsletter users.
    */
    function export_newsletter_user($time = '')
    {
        $check_rights = get_rights('list_newsletter_user');
        if ($check_rights == 0) {
            redirect('home/dashboard/no_rights');
        }

        $this->load->dbutil();
        $query = $this->db->query("SELECT * FROM newsletter_user");
        $delimiter = ",";
        $newline = "\r\n";
        $csv = $this->dbutil->csv_from_result($query, $delimiter, $newline);
        $file_name = "newsletter_user_" . date('d-m-Y') . ".csv";
        $this->force_download($file_name, $csv);
        $this->db->cache_delete_all();
    }

    /*
     Function name :import_newsletter_user()
    Use : With this function admin can import csv file of newsletter users.
    */
    function import_newsletter_user()
    {
        $check_rights = get_rights('list_newsletter_user');
        if ($check_rights == 0) {
            redirect('home/dashboard/no_rights');
        }

        $data['error'] = '';
        $data['site_setting'] = site_setting();
        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Import Users', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'newsletter/add_csv_user', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }

    /*
     Function name :add_csv_upload()
    Parameter :none
    Return : none
    Use : This function is used to upload csv file.
    */
    function add_csv_upload()
    {
        $check_rights = get_rights('list_newsletter_user');
        if ($check_rights == 0) {
            redirect('home/dashboard/no_rights');
        }

        $CI = &get_instance();
        $base_path = $CI->config->slash_item('base_path');
        $this->load->library('upload');
        $rand = rand(0, 100000);
        $_FILES['userfile']['name'] = $_FILES['upcsv']['name'];
        $_FILES['userfile']['type'] = $_FILES['upcsv']['type'];
        $_FILES['userfile']['tmp_name'] = $_FILES['upcsv']['tmp_name'];
        $_FILES['userfile']['error'] = $_FILES['upcsv']['error'];
        $_FILES['userfile']['size'] = $_FILES['upcsv']['size'];
        $config['file_name'] = $rand . $_FILES['userfile']['name'];
        $config['upload_path'] = $base_path . 'upload/'; /* NB! create this dir! */
        $config['allowed_types'] = 'csv';
        $config['max_size'] = '300';
        $this->upload->initialize($config);
        if (!$this->upload->do_upload()) {
            $error = array(
                'error' => $this->upload->display_errors()
            );
            $err = str_replace("<p>", "", $error['error']);
            $err = str_replace("</p>", "", $err);
            $data['error'] = $err;
            $data['site_setting'] = site_setting();
            $theme = 'admin';
            $this->template->set_master_template($theme . '/template.php');
            $this->template->write('title', 'Import User', '', TRUE);
            $this->template->write_view('header', 'header', $data, TRUE);
            $this->template->write_view('main_content', 'newsletter/add_csv_user', $data, TRUE);
            $this->template->write_view('footer', 'footer', '', TRUE);
            $this->template->render();
        }

        $picture = $this->upload->data();
        $file_name = $base_path . 'upload/' . $picture['file_name'];
        $fp = fopen("$file_name", "r"); //for open uploaded file
        $data = fgetcsv($fp, 500, ",");
        $cnt = 1;
        $users = array();
        while (($data = fgetcsv($fp, 500, ",")) != FALSE) //for get content
        {
            if ($data[0] != '') {
                $chk_user = $this->newsletter_model->user_unique(trim($data[1]));
                if ($chk_user == TRUE) {
                    $email_name = str_replace('"', '', str_replace(array(
                        "'",
                        ",",
                        "%",
                        "$",
                        "&",
                        "*",
                        "#",
                        "(",
                        ")",
                        ":",
                        ";",
                        ">",
                        "<",
                        "/"
                    ), '', $data[0]));
                    $this->db->query("insert into newsletter_user(`user_name`,`email`,`user_date`,`user_ip`)values('" . $email_name . "','" . trim($data[1]) . "','" . date('Y-m-d H:i:s') . "','" . $_SERVER['REMOTE_ADDR'] . "')");
                }
            }
        }

        fclose($fp);
        if (is_file($base_path . 'admin/upload_csv/' . $picture['file_name'])) {
            unlink($base_path . 'admin/upload_csv/' . $picture['file_name']);
        }

        $limit = 20;
        redirect('admin/newsletter/list_newsletter_user/insert');
    }

    // ////===========CSV ======PART============
    // //////////////////=============Newsletter Job=================

    /*
     Function name :newsletter_job()
    Parameter :$msg (message string)
    Return : none
    Use : This function shows newsletter templates with their status.
    */
    function newsletter_job($msg = '')
    {
        $check_rights = get_rights('newsletter_job');
        if ($check_rights == 0) {
            redirect('home/dashboard/no_rights');
        }

        // $this->load->library('pagination');

        $data['draft_result'] = $this->newsletter_model->get_all_newsletter_job(1, 0);
        $data['sent_result'] = $this->newsletter_model->get_all_newsletter_job(0, 1);
        $data['msg'] = $msg;
        $data['option'] = '';
        $data['keyword'] = '';
        $data['search_type'] = 'normal';
        $data['site_setting'] = site_setting();
        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Newsletters Job', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'newsletter/list_newsletter_job', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }

    function newsletter_job_sent($msg = '')
    {
        $check_rights = get_rights('newsletter_job');
        if ($check_rights == 0) {
            redirect('home/dashboard/no_rights');
        }

        // $this->load->library('pagination');

        $data['draft_result'] = $this->newsletter_model->get_all_newsletter_job(1, 0);
        $data['sent_result'] = $this->newsletter_model->get_all_newsletter_job(0, 1);
        $data['msg'] = $msg;
        $data['option'] = '';
        $data['keyword'] = '';
        $data['search_type'] = 'normal';
        $data['site_setting'] = site_setting();
        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Newsletters Job', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'newsletter/list_newsletter_job_new', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }


    /*
     Function name :add_newsletter_job()
    Use : Admin can start weekly, monthly, daily or a specific duration newsletter with selected users.
    */
    function add_newsletter_jobs()
    {
        $check_rights = get_rights('newsletter_job');
        if ($check_rights == 0) {
            redirect('home/dashboard/no_rights');
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('newsletter_id', SELECT_NEWSLETTER, 'required');
        $this->form_validation->set_rules('select_user', SELECT_USER, 'required');
        $lessdate = 0;
        if ($this->input->post('recursive') == 'yes') {
            $this->form_validation->set_rules('news_type', NEWSLETTER_TYPE, 'required');
            if ($this->input->post('news_type') == 'daily') {
                $this->form_validation->set_rules('job_start_date', JOB_START_DATE, 'required');
            } elseif ($this->input->post('news_type') == 'weekly') {
                $this->form_validation->set_rules('week_day', WEEKLY_DAY, 'required');
            } elseif ($this->input->post('news_type') == 'monthly') {
                $this->form_validation->set_rules('month_day', MONTHLY_DAY, 'required');
            } elseif ($this->input->post('news_type') == 'duration') {
                $this->form_validation->set_rules('start_date', START_DATE, 'required');
                $this->form_validation->set_rules('end_date', END_DATE, 'required');
                $start_date = $this->input->post('start_date');
                $end_date = $this->input->post('end_date');
                if ($end_date <= $start_date) {
                    $lessdate = 1;
                }
            }
        } else {
            $this->form_validation->set_rules('recursive', RECURSIVE, 'required');
        }

        $this->form_validation->set_rules('job_start_date', JOB_START_DATE, 'required');
        if ($this->form_validation->run() == FALSE || $lessdate) {
            if (validation_errors()) {
                $data["error"] = validation_errors();
            } elseif ($lessdate) {
                $data['error'] = '<p>' . END_DATE_MUST_BE_GREATER_THAN_START_DATE . '</p>';
            } else {
                $data["error"] = "";
            }

            $data["newsletter_id"] = $this->input->post('newsletter_id');
            $data['job_start_date'] = $this->input->post('job_start_date');
            $data['site_setting'] = site_setting();
            $data['all_newsletter'] = $this->newsletter_model->get_all_newsletter_templates();
            $data['user_register'] = $this->newsletter_model->GetNewsletterUser('1', 'register');
            $data['user_subscribe'] = $this->newsletter_model->GetNewsletterUser('1', 'subscriber');
            $data['user_both'] = $this->newsletter_model->GetNewsletterUser('', 'both');
            /*
             if($this->input->post('offset')=="")
             {

            // $limit = '10';

            $totalRows = $this->newsletter_model->get_count_newsletter_job();

            // $data["offset"] = (int)($totalRows/$limit)*$limit;

            }else{

            // $data["offset"] = $this->input->post('offset');

            }*/
            $theme = 'admin';
            $this->template->set_master_template($theme . '/template.php');
            $this->template->write('title', 'Add Job', '', TRUE);
            $this->template->write_view('header', 'header', $data, TRUE);
            $this->template->write_view('main_content', 'newsletter/add_newsletter_job', $data, TRUE);
            $this->template->write_view('footer', 'footer', '', TRUE);
            $this->template->render();
        } else {

            // print_r($_POST);

            if ($this->input->post('select_user') == '1') $emails = $this->input->post('subscribe_user');
            else $emails = $this->input->post('register_user');

            // print_r($emails);die;

            if ($emails) {
                foreach ($emails as $key => $val) {
                    $insert_userdata = array(
                        'email' => $val,
                        'user_name' => '',
                        'is_subscribe' => 1,
                        'user_date' => date('Y-m-d H:i:s'),
                        'user_ip' => $_SERVER['REMOTE_ADDR']
                    );
                    $this->home_model->AddUpdateData('newsletter_user', $insert_userdata);
                }
            }

            $final_date = date('Y-m-d');
            $start_date = NULL;
            $end_date = NULL;
            if ($this->input->post('week_day')) {
                $final_date = $this->input->post('week_day');
            }

            if ($this->input->post('month_day')) {
                $final_date = $this->input->post('month_day');
            }

            if ($this->input->post('duration')) {
                $start_date = $this->input->post('start_date');
                $end_date = $this->input->post('end_date');
            }

            $insert_data = array(
                'newsletter_id' => $this->input->post('newsletter_id'),
                'job_start_date' => date('Y-m-d', strtotime($this->input->post('job_start_date'))),
                'job_date' => $final_date,
                'start_date' => $start_date,
                'end_date' => $end_date,
                'newsletter_type' => $this->input->post('news_type')
            );
            $success = $this->home_model->AddUpdateData('newsletter_job', $insert_data);
            if ($success) $msg = "insert";

            // $offset = $this->input->post('offset');

            redirect('admin/newsletter/newsletter_job/' . $msg);
        }
    }

    function add_newsletter_job()
    {
        $check_rights = get_rights('newsletter_job');
        if ($check_rights == 0) {
            redirect('home/dashboard/no_rights');
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('newsletter_id', SELECT_NEWSLETTER, 'required');
        $this->form_validation->set_rules('select_user', SELECT_USER, 'required');
        $lessdate = 0;
        if ($this->input->post('recursive') == 'yes') {
            $this->form_validation->set_rules('news_type', NEWSLETTER_TYPE, 'required');
            if ($this->input->post('news_type') == 'daily') {
                $this->form_validation->set_rules('job_start_date', JOB_START_DATE, 'required');
                $this->form_validation->set_rules('job_start_date', JOB_START_DATE, 'required');
            } elseif ($this->input->post('news_type') == 'weekly') {
                $this->form_validation->set_rules('week_day', WEEKLY_DAY, 'required');
                $this->form_validation->set_rules('job_start_date', JOB_START_DATE, 'required');
            } elseif ($this->input->post('news_type') == 'monthly') {
                $this->form_validation->set_rules('month_day', MONTHLY_DAY, 'required');
                $this->form_validation->set_rules('job_start_date', JOB_START_DATE, 'required');
            } elseif ($this->input->post('news_type') == 'duration') {
                $this->form_validation->set_rules('start_date', START_DATE, 'required');
                $this->form_validation->set_rules('end_date', END_DATE, 'required');
                $start_date = $this->input->post('start_date');
                $end_date = $this->input->post('end_date');
                if ($end_date <= $start_date) {
                    $lessdate = 1;
                }
            }
        } else {
            $this->form_validation->set_rules('recursive', RECURSIVE, 'required');
            $this->form_validation->set_rules('job_start_date', JOB_START_DATE, 'required');
        }

        if ($this->form_validation->run() == FALSE || $lessdate) {
            if (validation_errors()) {
                $data["error"] = validation_errors();
            } elseif ($lessdate) {
                $data['error'] = '<p>' . END_DATE_MUST_BE_GREATER_THAN_START_DATE . '</p>';
            } else {
                $data["error"] = "";
            }

            $data["newsletter_id"] = $this->input->post('newsletter_id');
            $data['job_start_date'] = $this->input->post('job_start_date');
            $data['site_setting'] = site_setting();
            $data['all_newsletter'] = $this->newsletter_model->get_all_newsletter_templates();
            $data['user_register'] = $this->newsletter_model->GetNewsletterUser('2', 'register');
            $data['user_subscribe'] = $this->newsletter_model->GetNewsletterUser('1', 'subscriber');
            $data['user_both'] = $this->newsletter_model->GetNewsletterUser('', 'both');
            /*
             if($this->input->post('offset')=="")
             {

            // $limit = '10';

            $totalRows = $this->newsletter_model->get_count_newsletter_job();

            // $data["offset"] = (int)($totalRows/$limit)*$limit;

            }else{

            // $data["offset"] = $this->input->post('offset');

            }*/
            $theme = 'admin';
            $this->template->set_master_template($theme . '/template.php');
            $this->template->write('title', 'Add Job', '', TRUE);
            $this->template->write_view('header', 'header', $data, TRUE);
            $this->template->write_view('main_content', 'newsletter/add_newsletter_job', $data, TRUE);
            $this->template->write_view('footer', 'footer', '', TRUE);
            $this->template->render();
        } else {

            //print_r($_POST);
            //	die;

            if ($this->input->post('select_user') == '1') $emails = $this->input->post('subscribe_user');
            else $emails = $this->input->post('register_user');

            //print_r($emails);die;

            if ($emails) {
                foreach ($emails as $key => $val) {
                    $insert_userdata = array(
                        'email' => $val,
                        'user_name' => '',
                        'is_subscribe' => 1,
                        'user_date' => date('Y-m-d H:i:s'),
                        'user_ip' => $_SERVER['REMOTE_ADDR']
                    );
                    $newsletter_userid = $this->home_model->AddUpdateData('newsletter_user', $insert_userdata);

                    $insert_subscribe_data = array(
                        'newsletter_user_id' => $newsletter_userid,
                        'newsletter_id' => $this->input->post('newsletter_id'),
                        'subscribe_date' => date('Y-m-d H:i:s')
                    );
                    $this->home_model->AddUpdateData('newsletter_subscribe', $insert_subscribe_data);
                }
            }

            $final_date = NULL;
            $start_date = NULL;
            $end_date = NULL;
            if ($this->input->post('week_day')) {
                $final_date = $this->input->post('week_day');
            }

            if ($this->input->post('month_day')) {
                $final_date = $this->input->post('month_day');
            }

            if ($this->input->post('duration')) {
                $start_date = $this->input->post('start_date');
                $end_date = $this->input->post('end_date');
            }

            $insert_data = array(
                'newsletter_id' => $this->input->post('newsletter_id'),
                'job_start_date' => date('Y-m-d', strtotime($this->input->post('job_start_date'))),
                'job_date' => $final_date,
                'start_date' => $start_date,
                'end_date' => $end_date,
                'newsletter_type' => $this->input->post('news_type'),
                'draft' => 1
            );
            $success = $this->home_model->AddUpdateData('newsletter_job', $insert_data);
            if ($success) $msg = "insert";

            // $offset = $this->input->post('offset');

            redirect('admin/newsletter/newsletter_job/' . $msg);
        }
    }

    /*
     Function name :action_newsletter_job()
    Parameter :none
    Return : none
    Use : This function is used to delete newsletter job.
    */
    function action_newsletter_job()
    {
        $check_rights = get_rights('newsletter_job');

        // $limit=20;

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        // $offset=$this->input->post('offset');

        $action = $this->input->post('action');
        $job_id = $this->input->post('chk');
        if ($action == 'delete') {
            foreach ($job_id as $id) {
                $this->newsletter_model->delete_newsletter_job($id);
            }

            // $offset = $this->input->post('offset');

            redirect('admin/newsletter/newsletter_job/delete');
        }
    }

    /*
     Function name :index()
    Parameter :$job_id,  $newsletter_id
    Return : none
    Use : it shows statistics of the newsletter.
    */
    function newsletter_statistics($job_id, $newsletter_id)
    {
        $check_rights = get_rights('newsletter_job');
        if ($check_rights == 0) {
            redirect('home/dashboard/no_rights');
        }

        $data['newsletter'] = $this->newsletter_model->get_one_newsletter($newsletter_id);
        $data['job'] = $this->newsletter_model->get_one_job($job_id);
        $data['total_subscription'] = $this->newsletter_model->get_total_subscription($newsletter_id);
        $data['total_send'] = $this->newsletter_model->get_total_job_send($job_id);
        $data['total_read'] = $this->newsletter_model->get_total_job_open($job_id);
        $data['total_fail'] = $this->newsletter_model->get_total_job_fail($job_id);
        $data['site_setting'] = site_setting();
        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
        $this->template->write_view('main_content', 'newsletter/job_report', $data, TRUE);
        $this->template->render();
    }

    // /////////////////=============setting part========================

    /*
     Function name :index()
    Use : Admin can configure all the settings to the newsletter.
    */
    function newsletter_setting()
    {
        $check_rights = get_rights('newsletter_setting');
        $data['error1'] = '';
        if ($check_rights == 0) {
            redirect('home/dashboard/no_rights');
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('newsletter_from_name', FROM_NAME, 'required|alpha_space');
        $this->form_validation->set_rules('newsletter_from_address', FROM_EMAIL_ADDRESS, 'required|valid_email');
        $this->form_validation->set_rules('newsletter_reply_name', REPLY_NAME, 'required|alpha_space');
        $this->form_validation->set_rules('newsletter_reply_address', REPLY_EMAIL_ADDRESS, 'required|valid_email');
        $this->form_validation->set_rules('new_subscribe_email', NEW_SUBSCRIBE_EMAIL, 'required|valid_email');
        $this->form_validation->set_rules('unsubscribe_email', UNSUBSCRIBE_EMAIL, 'required|valid_email');
        $this->form_validation->set_rules('new_subscribe_to', USER_DEFAULT_NEWSLETTER, 'required');
        if ($this->input->post('new_subscribe_to') == 'selected') {
            $this->form_validation->set_rules('selected_newsletter_id', NEWSLETTER, 'required|is_natural_no_zero');
        }

        $this->form_validation->set_rules('number_of_email_send', NUMBER_OF_EMAIL_SEND, 'required|is_natural_no_zero');
        $this->form_validation->set_rules('break_between_email', BREAK_BETWEEN_NO_EMAIL_SEND, 'required|is_natural_no_zero');
        $this->form_validation->set_rules('break_type', BREAK_TYPE, 'required');
        $this->form_validation->set_rules('mailer', MAILER, 'required');
        if ($this->input->post('mailer') == 'sendmail') {
            $this->form_validation->set_rules('sendmail_path', SENDMAIL_PATH, 'required');
        }

        if ($this->input->post('mailer') == 'smtp') {
            $this->form_validation->set_rules('smtp_port', SMTP_PORT, 'required|is_natural_no_zero');
            $this->form_validation->set_rules('smtp_host', SMTP_HOST, 'required');
            $this->form_validation->set_rules('smtp_email', SMTP_EMAIL, 'required|valid_email');
            $this->form_validation->set_rules('smtp_password', SMTP_PASSWORD, 'required');
        }

        if ($this->form_validation->run() == FALSE) {
            if (validation_errors()) {
                $data["error"] = validation_errors();
            } else {
                $data["error"] = "";
            }

            if ($this->input->post('newsletter_setting_id')) {
                $newsletter_setting = $this->newsletter_model->get_newsletter_setting();
                $data["newsletter_setting_id"] = $this->input->post('newsletter_setting_id');
                $data["newsletter_from_name"] = $this->input->post('newsletter_from_name');
                $data["newsletter_from_address"] = $this->input->post('newsletter_from_address');
                $data["newsletter_reply_name"] = $this->input->post('newsletter_reply_name');
                $data["newsletter_reply_address"] = $this->input->post('newsletter_reply_address');
                $data["new_subscribe_email"] = $this->input->post('new_subscribe_email');
                $data["unsubscribe_email"] = $this->input->post('unsubscribe_email');
                $data["new_subscribe_to"] = $this->input->post('new_subscribe_to');
                $data["selected_newsletter_id"] = $this->input->post('selected_newsletter_id');
                $data["number_of_email_send"] = $this->input->post('number_of_email_send');
                $data["break_between_email"] = $this->input->post('break_between_email');
                $data["break_type"] = $this->input->post('break_type');
                $data["mailer"] = $this->input->post('mailer');
                $data["sendmail_path"] = $this->input->post('sendmail_path');
                $data["smtp_port"] = $this->input->post('smtp_port');
                $data["smtp_host"] = $this->input->post('smtp_host');
                $data["smtp_email"] = $this->input->post('smtp_email');
                $data["smtp_password"] = $this->input->post('smtp_password');
            } else {
                $newsletter_setting = $this->newsletter_model->get_newsletter_setting();
                $data["newsletter_setting_id"] = $newsletter_setting->newsletter_setting_id;
                $data["newsletter_from_name"] = $newsletter_setting->newsletter_from_name;
                $data["newsletter_from_address"] = $newsletter_setting->newsletter_from_address;
                $data["newsletter_reply_name"] = $newsletter_setting->newsletter_reply_name;
                $data["newsletter_reply_address"] = $newsletter_setting->newsletter_reply_address;
                $data["new_subscribe_email"] = $newsletter_setting->new_subscribe_email;
                $data["unsubscribe_email"] = $newsletter_setting->unsubscribe_email;
                $data["new_subscribe_to"] = $newsletter_setting->new_subscribe_to;
                $data["selected_newsletter_id"] = $newsletter_setting->selected_newsletter_id;
                $data["number_of_email_send"] = $newsletter_setting->number_of_email_send;
                $data["break_between_email"] = $newsletter_setting->break_between_email;
                $data["break_type"] = $newsletter_setting->break_type;
                $data["mailer"] = $newsletter_setting->mailer;
                $data["sendmail_path"] = $newsletter_setting->sendmail_path;
                $data["smtp_port"] = $newsletter_setting->smtp_port;
                $data["smtp_host"] = $newsletter_setting->smtp_host;
                $data["smtp_email"] = $newsletter_setting->smtp_email;
                $data["smtp_password"] = $newsletter_setting->smtp_password;
            }

            $data['all_newsletter'] = $this->newsletter_model->get_all_newsletter_templates();
            $data['site_setting'] = site_setting();
            $theme = 'admin';
            $this->template->set_master_template($theme . '/template.php');
            $this->template->write('title', 'Newsletter Settings', '', TRUE);
            $this->template->write_view('header', 'header', $data, TRUE);
            $this->template->write_view('main_content', 'newsletter/newsletter_setting', $data, TRUE);
            $this->template->write_view('footer', 'footer', '', TRUE);
            $this->template->render();
        } else {
            $this->newsletter_model->newsletter_setting_update();
            $data["error"] = "Newsletter settings updated successfully.";
            $newsletter_setting = $this->newsletter_model->get_newsletter_setting();

            // echo "<pre>";
            // print_r($newsletter_setting);
            // die;

            $data["newsletter_setting_id"] = $newsletter_setting->newsletter_setting_id;
            $data["newsletter_from_name"] = $newsletter_setting->newsletter_from_name;
            $data["newsletter_from_address"] = $newsletter_setting->newsletter_from_address;
            $data["newsletter_reply_name"] = $newsletter_setting->newsletter_reply_name;
            $data["newsletter_reply_address"] = $newsletter_setting->newsletter_reply_address;
            $data["new_subscribe_email"] = $newsletter_setting->new_subscribe_email;
            $data["unsubscribe_email"] = $newsletter_setting->unsubscribe_email;
            $data["new_subscribe_to"] = $newsletter_setting->new_subscribe_to;
            $data["selected_newsletter_id"] = $newsletter_setting->selected_newsletter_id;
            $data["number_of_email_send"] = $newsletter_setting->number_of_email_send;
            $data["break_between_email"] = $newsletter_setting->break_between_email;
            $data["break_type"] = $newsletter_setting->break_type;
            $data["mailer"] = $newsletter_setting->mailer;
            $data["sendmail_path"] = $newsletter_setting->sendmail_path;
            $data["smtp_port"] = $newsletter_setting->smtp_port;
            $data["smtp_host"] = $newsletter_setting->smtp_host;
            $data["smtp_email"] = $newsletter_setting->smtp_email;
            $data["smtp_password"] = $newsletter_setting->smtp_password;
            $data['site_setting'] = site_setting();
            $data['all_newsletter'] = $this->newsletter_model->get_all_newsletter_templates();
            $theme = 'admin';
            $this->template->set_master_template($theme . '/template.php');
            $this->template->write('title', 'Newsletter Settings', '', TRUE);
            $this->template->write_view('header', 'header', $data, TRUE);
            $this->template->write_view('main_content', 'newsletter/newsletter_setting', $data, TRUE);
            $this->template->write_view('footer', 'footer', '', TRUE);
            $this->template->render();
        }
    }

    /*
     Function name :send_testing_newsetter()
    Use : this function is used to send a testing email.
    */
    function send_testing_newsetter()
    {
        $check_rights = get_rights('newsletter_setting');
        $data['error'] = '';
        if ($check_rights == 0) {
            redirect('home/dashboard/no_rights');
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('sender_mail', SENDMAIL_MAIL, 'required|valid_email');
        $this->form_validation->set_rules('receiver_email', RECEIVER_MAIL, 'required|valid_email');
        $this->form_validation->set_rules('message_text', MESSAGE, 'required');
        if ($this->form_validation->run() == FALSE) {
            if (validation_errors()) {
                $data["disp_error"] = 1;
                $data["error1"] = validation_errors();
            } else {
                $data["error1"] = "";
                $data['disp_error'] = "";
            }

            $newsletter_setting = $this->newsletter_model->get_newsletter_setting();
            $data["newsletter_setting_id"] = $newsletter_setting->newsletter_setting_id;
            $data["newsletter_from_name"] = $newsletter_setting->newsletter_from_name;
            $data["newsletter_from_address"] = $newsletter_setting->newsletter_from_address;
            $data["newsletter_reply_address"] = $newsletter_setting->newsletter_reply_address;
            $data["mailer"] = $newsletter_setting->mailer;
            $data["sendmail_path"] = $newsletter_setting->sendmail_path;
            $data["smtp_port"] = $newsletter_setting->smtp_port;
            $data["smtp_host"] = $newsletter_setting->smtp_host;
            $data["smtp_email"] = $newsletter_setting->smtp_email;
            $data["smtp_password"] = $newsletter_setting->smtp_password;
            $data['site_setting'] = site_setting();
            $theme = 'admin';
            $this->template->set_master_template($theme . '/template.php');
            $this->template->write('title', 'Newsletter Settings', '', TRUE);
            $this->template->write_view('header', 'header', $data, TRUE);
            $this->template->write_view('main_content', 'newsletter/send_testing_newsletter', $data, TRUE);
            $this->template->write_view('footer', 'footer', '', TRUE);
            $this->template->render();
        } else {
            $newsletter_setting = $this->newsletter_model->get_newsletter_setting();
            $data["newsletter_setting_id"] = $newsletter_setting->newsletter_setting_id;
            $data["newsletter_from_name"] = $newsletter_setting->newsletter_from_name;
            $data["newsletter_from_address"] = $newsletter_setting->newsletter_from_address;
            $data["newsletter_reply_address"] = $newsletter_setting->newsletter_reply_address;
            $data["mailer"] = $newsletter_setting->mailer;
            $data["sendmail_path"] = $newsletter_setting->sendmail_path;
            $data["smtp_port"] = $newsletter_setting->smtp_port;
            $data["smtp_host"] = $newsletter_setting->smtp_host;
            $data["smtp_email"] = $newsletter_setting->smtp_email;
            $data["smtp_password"] = $newsletter_setting->smtp_password;
            /*Send Testing Mail*/
            $this->load->library('email');

            // /////====smtp====

            if ($newsletter_setting->mailer == 'smtp') {
                $config['protocol'] = 'smtp';
                $config['smtp_host'] = trim($newsletter_setting->smtp_host);
                $config['smtp_port'] = trim($newsletter_setting->smtp_port);
                $config['smtp_timeout'] = '30';
                $config['smtp_user'] = trim($newsletter_setting->smtp_email);
                $config['smtp_pass'] = trim($newsletter_setting->smtp_password);
            } // ///=====sendmail======

            elseif ($newsletter_setting->mailer == 'sendmail') {
                $config['protocol'] = 'sendmail';
                $config['mailpath'] = trim($newsletter_setting->sendmail_path);
            } // ///=====php mail default======

            else {
            }

            $config['wordwrap'] = TRUE;
            $config['mailtype'] = 'html';
            $config['crlf'] = '\n\n';
            $config['newline'] = '\n\n';
            $data['email_to'] = $this->input->post('receiver_email');
            $email_to = $data['email_to'];
            $data['email_address_from'] = $newsletter_setting->newsletter_from_address;
            $data['new_subscribe_email'] = $newsletter_setting->new_subscribe_email;
            $data['unsubscribe_email'] = $newsletter_setting->unsubscribe_email;
            $data['break_between_email'] = $newsletter_setting->break_between_email;
            $data['break_type'] = $newsletter_setting->break_type;
            $data['number_of_email_send'] = $newsletter_setting->number_of_email_send;
            $data["newsletter_reply_name"] = $newsletter_setting->newsletter_reply_name;
            $data['new_subscribe_to'] = $newsletter_setting->new_subscribe_to;
            $email_address_from = $data['email_address_from'];
            $data['email_address_reply'] = $newsletter_setting->newsletter_reply_address;
            $email_address_reply = $data['email_address_reply'];
            $data['email_subject'] = 'Testing Mail For Newsletter';
            $email_subject = $data['email_subject'];
            $data['email_message'] = $this->input->post('message_text');
            $email_message = $data['email_message'];
            $str = $email_message;
            $this->email->initialize($config);
            $this->email->from($email_address_from);
            $this->email->reply_to($email_address_reply);
            $this->email->to($email_to);
            $this->email->subject($email_subject);
            $this->email->message($str);
            if (!$this->email->send()) {
                $data["error1"] = $this->email->print_debugger();
                $data['disp_error'] = "";
            } /*End Testing Mail*/
            else {
                $data["error1"] = "sent";
                $data['disp_error'] = "";
            }

            $theme = 'admin';
            $this->template->set_master_template($theme . '/template.php');
            $this->template->write('title', 'Newsletter Settings', '', TRUE);
            $this->template->write_view('header', 'header', $data, TRUE);
            $this->template->write_view('main_content', 'newsletter/newsletter_setting', $data, TRUE);
            $this->template->write_view('footer', 'footer', '', TRUE);
            $this->template->render();
        }
    }

    function download_csv_file()
    {
        $data = '';
        $name = 'newsletter_user_format.csv';
        $pth = file_get_contents(base_url() . "upload/newsletter_user_format.csv");
        force_download($name, $pth);
    }

    /*End send Test News letter*/
}

?>
