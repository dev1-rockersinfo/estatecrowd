<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Message_setting extends ROCKERS_Controller
{

    function __construct()
    {
        parent::__construct();

        //	$this->load->model('message_setting_model');
        $this->load->model('home_model');
    }

    function index()

    {
        redirect('admin/message_setting/add_message_setting/');
    }


    /*
     Function name :add_message_setting()
    Parameter : none.
    Return : none
    Use : this function is used to update message settings.
    */
    function add_message_setting()

    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('default_message_subject', MESSAGE_SUBJECT, 'required');

        if ($this->form_validation->run() == FALSE) {
            if (validation_errors()) {
                $data["error"] = validation_errors();
            } else {
                $data["error"] = "";
            }

            if ($this->input->post('message_setting_id')) {
                $data["message_setting_id"] = $this->input->post('message_setting_id');

                $data["email_admin_on_new_message"] = $this->input->post('email_admin_on_new_message');

                $data["email_user_on_new_message"] = $this->input->post('email_user_on_new_message');

                $data["default_message_subject"] = $this->input->post('default_message_subject');

                $data["message_enable"] = $this->input->post('message_enable');

            } else {

                $one_message_setting = $this->home_model->get_one_message_setting();

                $data["message_setting_id"] = $one_message_setting['message_setting_id'];

                $data["email_admin_on_new_message"] = $one_message_setting['email_admin_on_new_message'];

                $data["email_user_on_new_message"] = $one_message_setting['email_user_on_new_message'];

                $data["default_message_subject"] = $one_message_setting['default_message_subject'];

                $data["message_enable"] = $one_message_setting['message_enable'];
            }

            $theme = 'admin';
            $this->template->set_master_template($theme . '/template.php');

            $this->template->write('title', 'Administrator', '', TRUE);
            $this->template->write_view('header', 'header', $data, TRUE);
            $this->template->write_view('main_content', 'setting/add_message_setting', $data, TRUE);
            $this->template->write_view('footer', 'footer', '', TRUE);
            $this->template->render();

        } else {

            $data1 = array(
                'email_admin_on_new_message' => $this->input->post('email_admin_on_new_message'),
                'email_user_on_new_message' => $this->input->post('email_user_on_new_message'),
                'default_message_subject' => $this->input->post('default_message_subject'),
                'message_enable' => $this->input->post('message_enable'),
            );

            $this->home_model->table_update('message_setting_id', $this->input->post('message_setting_id'), 'message_setting', $data1);

            deletecache("message_setting");

            $data["error"] = "success";

            $data["message_setting_id"] = $this->input->post('message_setting_id');

            $data["email_admin_on_new_message"] = $this->input->post('email_admin_on_new_message');

            $data["email_user_on_new_message"] = $this->input->post('email_user_on_new_message');

            $data["default_message_subject"] = $this->input->post('default_message_subject');

            $data["message_enable"] = $this->input->post('message_enable');

            $theme = 'admin';
            $this->template->set_master_template($theme . '/template.php');

            $this->template->write('title', 'Administrator', '', TRUE);
            $this->template->write_view('header', 'header', $data, TRUE);
            $this->template->write_view('main_content', 'setting/add_message_setting', $data, TRUE);
            $this->template->write_view('footer', 'footer', '', TRUE);
            $this->template->render();

        }
    }
}

?>
