<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Set_fund extends ROCKERS_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('set_fund_model');
        $this->load->model('user_model');
        $this->load->model('home_model');


    }

    function index()
    {

        redirect('admin/set_fund/list_project');
    }


    ///////////////////////==========================================*************************************************==========================

    function serch_user($uid = '', $msg = '')
    {
        //echo $uid;
        //exit;
        $check_rights = get_rights('set_fund');

        if ($check_rights == 0) {
            redirect('home/dashboard/no_rights');
        }
        $data['result'] = "";
        $limit = 20;
        $offset = 0;

        if ($uid != "") {

            $config['total_rows'] = $this->set_fund_model->get_serch_user($uid);
            $data['result'] = $this->set_fund_model->search_user($limit, $offset, $uid);


        }

        $this->load->library('pagination');


        if ($_POST) {
            $keyword = $this->input->post('keyword');
            $config['total_rows'] = $this->set_fund_model->get_serch_user($keyword);
            $data['result'] = $this->set_fund_model->search_user($limit, $offset, $keyword);


        } else {
            $config['total_rows'] = $this->set_fund_model->get_serch_user();
            $data['result'] = $this->set_fund_model->search_user($limit, $offset);
        }


        $config['uri_segment'] = '4';
        $config['base_url'] = site_url('admin/set_fund/serch_user/' . $uid . '/');
        $config['per_page'] = $limit;
        $this->pagination->initialize($config);
        $data['page_link'] = $this->pagination->create_links();
        $data['msg'] = $msg;
        $data['offset'] = $offset;


        $data['limit'] = $limit;
        $data['option'] = '';
        $data['keyword'] = '';
        $data['search_type'] = 'normal';

        $data['site_setting'] = site_setting();

        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Set Fund', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'wallet/set_fund_user', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }

    function add_fund($uid = '')
    {

        $check_rights = get_rights('set_fund');

        if ($check_rights == 0) {
            redirect('home/dashboard/no_rights');
        }
        $this->load->library('form_validation');

        $this->form_validation->set_rules('add_fund', ADD_FUND, 'required');
        $this->form_validation->set_rules('reason', REASON, 'required');

        $data['amount'] = $this->set_fund_model->user_wallet_amount($uid);
        $data['userid'] = $uid;
        $data['site_setting'] = site_setting();

        $amount_err = '';
        //echo $this->input->post('add_fund');die();
        if ($_POST) {
            if ($this->input->post('add_fund') <= 0) {
                $amount_err = "<p>" . PLEASE_ENTER_AMOUNT_GREATER_THAN_ZERO . "</p>";
            }
        }
        $data["error"] = "";
        if ($this->form_validation->run() == FALSE || $amount_err != '') {
            if (validation_errors()) {
                $data["error"] = validation_errors();
            } else {
                $data["error"] = "";
            }
        }

        $data["error"] .= $amount_err;

        if ($_POST) {
            if ($data["error"] == '') {
                $wallet = array(

                    'debit' => $this->input->post('add_fund'),
                    'reason' => $this->input->post('reason'),
                    'user_id' => $this->input->post('userid'),
                    'donate_status' => '1',
                    'wallet_transaction_id' => 'Admin',
                    'wallet_payee_email' => 'Internal',
                    'admin_status' => 'Confirm',
                    'wallet_ip' => $_SERVER['REMOTE_ADDR']


                );

                $this->db->insert('wallet', $wallet);

                redirect('admin/set_fund/serch_user/' . $this->input->post('userid') . '/add');
            }
        }

        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Add Fund', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'add_fund', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();


    }

    function deduct_fund($uid = '')
    {
        $check_rights = get_rights('set_fund');

        if ($check_rights == 0) {
            redirect('home/dashboard/no_rights');
        }
        $this->load->library('form_validation');

        $this->form_validation->set_rules('deduct_fund', DEDUCT_FUND, 'required');
        $this->form_validation->set_rules('reason', REASON, 'required');

        $data['amount'] = $this->set_fund_model->user_wallet_amount($uid);
        $data['userid'] = $uid;

        //$data['site_setting']['currency_symbol'].$data['amount']
        $data['site_setting'] = site_setting();

        $amount_err = '';
        $data["error"] = "";
        if ($_POST) {

            if ($this->input->post('deduct_fund') > $data['amount']) {
                $amount_err = "<p>You cannot deduct amount greator than " . $data['site_setting']['currency_symbol'] . $data['amount'] . "</p>";
            }
            if ($this->input->post('deduct_fund') <= 0) {
                $amount_err = "<p>" . AMOUNT_SHOULD_BE_GREATER_THAN_ZERO . ".</p>";
            }
        }


        if ($this->form_validation->run() == FALSE || $amount_err != '') {
            if (validation_errors()) {
                $data["error"] = validation_errors();
            } else {
                $data["error"] = "";
            }
        }

        $data["error"] .= $amount_err;

        if ($_POST) {
            if ($data["error"] == '') {
                $wallet = array(

                    'credit' => $this->input->post('deduct_fund'),
                    'reason' => $this->input->post('reason'),
                    'user_id' => $this->input->post('userid'),
                    'donate_status' => '1',
                    'wallet_transaction_id' => 'Admin',
                    'wallet_payee_email' => 'Internal',
                    'admin_status' => 'Confirm',
                    'wallet_ip' => $_SERVER['REMOTE_ADDR']


                );

                $this->db->insert('wallet', $wallet);

                redirect('admin/set_fund/serch_user/' . $this->input->post('userid') . '/deduct');
            }
        }


        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Deduct Fund', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'deduct_fund', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();


    }

}

?>
