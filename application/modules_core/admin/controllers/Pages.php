<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Pages extends ROCKERS_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('home_model');
    }

    function index()
    {
        redirect('admin/pages/list_pages');
    }

//==========Start of Pages tab code for content pages============= 	
    /*
     Function name :add_pages()
    Parameter :none
    Return : none
    Use : Function used adding pages to the website..
    */
    function add_pages($id='')
    {
        $check_rights = get_rights('list_pages');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        $data['pages_id']=$id;

        $data['query'] = $this->home_model->get_language('language', array('active' => 1));

        //print_r($data['query']);
        $this->load->library('form_validation');

        $this->form_validation->set_rules('pages_title', PAGES_TITLE, 'required');
        $this->form_validation->set_rules('description', PAGES_DESCRIPTION, 'required');
        $this->form_validation->set_rules('slug', SLUG, 'required');
        if($_POST && $_POST['link']!=''){
            $this->form_validation->set_rules('link', BUTTON_LINK, 'valid_url');
        }

        $slider_image = '';
        if ($id != '') {
            
            $one_pages = $this->home_model->get_one_tabledata('pages', array('pages_id' => $id));

            $slider_image = $one_pages['dynamic_image_image'];
        }

        $image_error = '';
        $data['error'] = '';

        if ($_FILES && $_POST){

            if ($_FILES['image_load']['name'] != '') {

                if ($_FILES['image_load']['tmp_name']) {
                    $needheight = 1300;
                    $needwidth = 600;
                    $fn = $_FILES['image_load']['tmp_name'];
                    $size = getimagesize($fn);
                    $actualwidth = $size[0];
                    $actualheight = $size[0];


                    if ($_FILES["image_load"]["type"] != "image/jpeg" and $_FILES["image_load"]["type"] != "image/pjpeg" and $_FILES["image_load"]["type"] != "image/png" and $_FILES["image_load"]["type"] != "image/x-png" and $_FILES["image_load"]["type"] != "image/gif") {
                        $image_error = PLEASE_UPLOAD_A_JPG_PNG_GIF_FILE;
                    } else if ($_FILES["image_load"]["size"] > 2000000) {
                        $image_error = SORRY_THIS_FILE_IS_TOO_LARGE_PLEASE_SELECT_A_JPG_FILE_THAT_IS_LESS_THAN_TWO_MB_OR_TRY_RESIZING_IT_USING_A_PHOTO_EDITOR;

                    } else if ($needwidth > $actualwidth || $needheight > $actualheight) {
                        $image_error = 'Please select image more than 1300 * 600.';
                    } else {

                        $image_error = '';
                    }
                } else {
                        if ($slider_image == '') {
                            $image_error = SLIDER_IMAGE_REQUIRED;
                        } else {
                            $image_error = '';
                        }   
                    }
            }else{
                if ($slider_image == '') {
                    $image_error = SLIDER_IMAGE_REQUIRED;
                } else {
                    $image_error = '';
                } 
            }
        }

        $this->form_validation->set_rules('external_link', EXTERNAL_URL, 'valid_url');
        if ($this->form_validation->run() == FALSE || $image_error != '') {
            if (validation_errors() || $image_error != '') {
                $data["error"] = $image_error.validation_errors();
            } else {
                $data["error"] = "";
            }
            $data["pages_id"] = $this->input->post('pages_id');
            $data["pages_title"] = $this->input->post('pages_title');
            $data["description"] = $this->input->post('description');
            $data["slug"] =makeSlugs($this->input->post('slug'));
            $data["active"] = $this->input->post('active');
            $data["language"] = $this->input->post('language');
            $data["footer_bar"] = $this->input->post('footer_bar');
            $data["right_side"] = $this->input->post('right_side');
            $data["external_link"] = $this->input->post('external_link');

            $data["header_title"] = $this->input->post('header_title');
            $data["header_content"] = $this->input->post('header_content');
            
            $data["link_name"] = $this->input->post('link_name');
            $data["link"] = $this->input->post('link');

            $data["meta_keyword"] = $this->input->post('meta_keyword');
            $data["meta_description"] = $this->input->post('meta_description');

            $data['site_setting'] = site_setting();

            $theme = 'admin';

            $this->template->set_master_template($theme . '/template.php');
            $this->template->write('title', 'Pages', '', TRUE);
            $this->template->write_view('header', 'header', $data, TRUE);
            $this->template->write_view('main_content', 'content_pages/add_pages', $data, TRUE);
            $this->template->write_view('footer', 'footer', '', TRUE);
            $this->template->render();
        } else {
            $content_image = '';



            if ($_FILES) {
                if ($_FILES['image_load']['name'] != '') {
                    $this->load->library('upload');
                    

                    $_FILES['userfile']['name'] = $_FILES['image_load']['name'];
                    $_FILES['userfile']['type'] = $_FILES['image_load']['type'];
                    $_FILES['userfile']['tmp_name'] = $_FILES['image_load']['tmp_name'];
                    $_FILES['userfile']['error'] = $_FILES['image_load']['error'];
                    $_FILES['userfile']['size'] = $_FILES['image_load']['size'];

                    $imagename = ContentImageUpload($_FILES);

                    $content_image = $imagename['orig'];

                } else {
                    if ($this->input->post('dynamic_image_image') != '') {
                        $content_image = $this->input->post('dynamic_image_image');
                    }
                }
        }

    

            $data = array(
                'pages_title' => $this->input->post('pages_title'),
                'description' => $this->input->post('description'),
                'slug' => makeSlugs($this->input->post('slug')),
                'active' => $this->input->post('active'),
                'meta_keyword' => $this->input->post('meta_keyword'),
                'meta_description' => $this->input->post('meta_description'),
                'footer_bar' => $this->input->post('footer_bar'),
                'right_side' => $this->input->post('right_side'),
                'external_link' => $this->input->post('external_link'),
                'language_id' => $this->input->post('language'),
                'header_title' => $this->input->post('header_title'),
                'header_content' => $this->input->post('header_content'),
                'dynamic_image_image' => $content_image,
                'link_name' => $this->input->post('link_name'),
                'link' => $this->input->post('link'),
            );

            if ($this->input->post('pages_id')) {
                $this->home_model->table_update('pages_id', $this->input->post('pages_id'), 'pages', $data);
                $msg = "update";
            } else {
                $this->home_model->table_insert('pages', $data);
                $msg = "insert";
            }
            redirect('admin/pages/list_pages/' . $msg);
        }
    }

    /*
     Function name :edit_pages()
    Parameter :$id=id of the learn more page that is selected for editing or updating.
    Return : none
    Use : Function used to view information of pages selected for updating.
    */
    function edit_pages($id = 0)
    {

        $check_rights = get_rights('list_pages');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }
        $data['query'] = $this->home_model->get_language('language', array('active' => 1));
        $one_pages = $this->home_model->get_one_tabledata('pages', array('pages_id' => $id));
        $data["error"] = "";
        $data["pages_id"] = $id;
        $data["pages_title"] = $one_pages['pages_title'];
        $data["description"] = $one_pages['description'];
        $data["slug"] = $one_pages['slug'];
        $data["active"] = $one_pages['active'];
        $data["meta_keyword"] = $one_pages['meta_keyword'];
        $data["meta_description"] = $one_pages['meta_description'];
        $data["language"] = $this->input->post('language');
        $data["footer_bar"] = $one_pages['footer_bar'];
        $data["right_side"] = $one_pages['right_side'];
        $data["external_link"] = $one_pages['external_link'];
        $data["header_title"] = $one_pages['header_title'];
        $data["header_content"] = $one_pages['header_content'];
        $data["image_load"] = $one_pages['dynamic_image_image'];
        $data["link_name"] = $one_pages['link_name'];
        $data["link"] = $one_pages['link'];

        $data['site_setting'] = site_setting();
        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Pages', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'content_pages/add_pages', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }

    /*
     Function name :delete_pages()
    Parameter :$id=id of the page that is to be deleted.
    Return : none
    Use : Function used to delete particular page.
    */
    function delete_pages($id = 0)
    {

        $check_rights = get_rights('list_pages');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        $this->db->delete('pages', array('pages_id' => $id));
        redirect('admin/pages/list_pages/delete');
    }

    /*
     Function name :list_pages()
    Parameter :$msg=message string to notify admin about the operation he performed.
    Return : none
    Use : Function is used for listing all the pages
    */
    function list_pages($msg = '')
    {

        $check_rights = get_rights('list_pages');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        //$data['result'] = $this->pages_model->get_pages_result($offset, $limit);
        $data['result'] = get_table_data('pages', array('pages_id' => 'desc'));
        $data['msg'] = $msg;

        $data['option'] = '';
        $data['keyword'] = '';
        $data['search_type'] = 'normal';

        $data['site_setting'] = site_setting();

        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Pages', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'content_pages/list_pages', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }
    //==========End of Pages tab code for content pages===============

    //==========Start of FAQ tab code for content pages===============
    /*
     Function name :action_faq()
    Parameter :none
    Return : none
    Use : Function used to delete,active or inactive selected faq records.
    */
    function action_faq()
    {
        $check_rights = get_rights('list_faq');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        $offset = $this->input->post('offset');
        $action = $this->input->post('action');
        $faq_id = $this->input->post('chk');

        switch ($action) {
            case 'active':
                $msg = 'active';
                foreach ($faq_id as $id) {

                    $this->db->query("update faq set active=1 where faq_id='" . $id . "'");
                }
                break;
            case 'inactive':
                $msg = 'inactive';
                foreach ($faq_id as $id) {
                    $this->db->query("update faq set active=0 where faq_id='" . $id . "'");
                }
                break;
            case 'delete':
                $msg = 'delete';
                foreach ($faq_id as $id) {
                    $this->db->query("delete from faq where faq_id='" . $id . "'");
                }
                break;
        }
        redirect('admin/pages/list_faq/' . $msg);
     
    }

    /*
     Function name :add_faq()
    Parameter :none
    Return : none
    Use : Function used adding faq records to the site.
    */
    function add_faq($msg = '')
    {

        $check_rights = get_rights('list_faq');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }
        $data['query'] = $this->home_model->get_language('language', array('active' => 1));

        $this->load->library('form_validation');
        $this->form_validation->set_rules('faq_category_id', FAQ_CATEGORY, 'required');
        $this->form_validation->set_rules('question', QUESTIONS, 'required');
        $this->form_validation->set_rules('answer', ANSWER, 'required');
        $this->form_validation->set_rules('question_type', INVESTORS, 'required');
        if ($this->form_validation->run() == FALSE) {
            if (validation_errors()) {
                $data["error"] = validation_errors();
            } else {
                $data["error"] = "";
            }
            $data["faq_id"] = $this->input->post('faq_id');
            $data["faq_category_id"] = 12;
            $data["question"] = $this->input->post('question');
            $data["answer"] = $this->input->post('answer');
            $data["active"] = $this->input->post('active');
            $data['faq_home'] = $this->input->post('faq_home');
            $data['faq_order'] = $this->input->post('faq_order');
            $data["language"] = $this->input->post('language');
            $data['question_type'] = $this->input->post('question_type');
            $data['site_setting'] = site_setting();
            //	$data['category'] = $this->faq_model->get_faq_category();

            $theme = 'admin';

            $this->template->set_master_template($theme . '/template.php');
            $this->template->write('title', 'FAQ', '', TRUE);
            $this->template->write_view('header', 'header', $data, TRUE);
            $this->template->write_view('main_content', 'content_pages/add_faq', $data, TRUE);
            $this->template->write_view('footer', 'footer', '', TRUE);
            $this->template->render();
        } else {
            $data = array(
                'faq_category_id' => $this->input->post('faq_category_id'),
                'question' => $this->input->post('question'),
                'answer' => $this->input->post('answer'),
                'active' => $this->input->post('active'),
                'faq_order' => $this->input->post('faq_order'),
                'language_id' => $this->input->post('language'),
                'faq_home' => 0,
                'date_added' => date("Y-m-d H:i:s"),
                'question_type' => $this->input->post('question_type'),
                'question_type_slug' => makeSlugs($this->input->post('question_type')),

            );
            if ($this->input->post('faq_id')) {
                $this->home_model->table_update('faq_id', $this->input->post('faq_id'), 'faq', $data);
                $msg = "update";
            } else {
                $this->home_model->table_insert('faq', $data);
                $msg = "insert";
            }
            redirect('admin/pages/list_faq/' . $msg);
        }
    }

    /*
     Function name :edit_faq()
    Parameter :$id=id of the faq record that is selected for editing or updating.
    Return : none
    Use : Function used to view information of faq record selected for updating.
    */
    function edit_faq($id = 0)
    {
        $check_rights = get_rights('list_faq');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }
        $data['site_setting'] = site_setting();
        $data['query'] = $this->home_model->get_language('language', array('active' => 1));
        $one_faq = $this->home_model->get_one_tabledata('faq', array('faq_id' => $id));
        // echo "<pre>";
        // print_r($one_faq);die;
        $data["error"] = "";
        $data["faq_id"] = $id;
        $data["faq_category_id"] = $one_faq['faq_category_id'];
        $data["question"] = $one_faq['question'];
        $data["answer"] = $one_faq['answer'];
        $data["active"] = $one_faq['active'];
        $data['faq_home'] = $one_faq['faq_home'];
        $data['faq_order'] = $one_faq['faq_order'];
        $data['question_type'] = $one_faq['question_type'];
        $data["language_id"] = $one_faq['language_id'];
        //$data['category'] = $this->faq_model->get_faq_category();

        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'FAQ', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'content_pages/add_faq', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }

    /*
     Function name :list_faq()
    Parameter :$msg=message string to notify admin about the operation he performed.
    Return : none
    Use : Function is used for listing all the faq records.
    */
    function list_faq($msg = '')
    {

        $check_rights = get_rights('list_faq');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        $data['site_setting'] = site_setting();
        //$data['result'] = $this->faq_model->get_faq_result($offset, $limit);
        $data['result'] = get_table_data('faq', array('faq_order' => 'asc'));
        $data['msg'] = $msg;
        $data['search_type'] = 'normal';

        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'FAQ', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'content_pages/list_faq', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }
    /*	function delete_faq($id=0)
     {
    $check_rights=get_rights('list_faq');

    if(	$check_rights==0) {
    redirect('admin/home/dashboard/no_rights');
    }

    $this->db->delete('faq',array('faq_id'=>$id));
    redirect('faq/list_faq/delete');
    }
    */
    //===============End of FAQ tab content pages..================
}

?>
