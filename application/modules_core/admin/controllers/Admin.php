<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Admin extends ROCKERS_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('admin_model');
        $this->load->model('home_model');

    }

    function index()
    {
        redirect('admin/admin/list_admin');
    }

    /*
     Function name :list_admin()
    Parameter :$msg=message string to notify admin about the operation he performed.
    Return : none
    Use : Function used for listing administrators
    */
    function list_admin($msg = '')
    {

        $check_rights = get_rights('list_admin');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }


        //$data['admin_list'] =get_table_data('admin',array('admin_id'=>'desc','admin_type !=' => 1 ));
        $data['admin_list'] = $this->admin_model->get_admin_result();
        $data['admin_login'] = $this->admin_model->get_adminlogin_result();
        $data['msg'] = $msg;
        //$data['offset'] = $offset;
        //$data['limit']=$limit;
        $data['option'] = '';
        $data['keyword'] = '';
        $data['search_type'] = 'normal';

        $data['site_setting'] = site_setting();

        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Administrator', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'user/list_admin', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }

    /*
     Function name :username_check()
    Parameter : $username=entered username for registering or updating admin.
    Return : returns true if username is unique else false with error message;
    Use : to check if entered username is unique or not..
    */
    function username_check($username)
    {
        $username = $this->admin_model->user_unique($username);
        if ($username == TRUE) {
            return TRUE;
        } else {
            $this->form_validation->set_message('username_check', THERE_IS_AN_EXISTING_ACCOUNT_ASSOCIATED_WITH_THIS_USERNAME);
            return FALSE;
        }
    }

    /*
     Function name :email_check()
    Parameter : $username=entered email for registering or updating admin.
    Return : returns true if email is unique else false with error message;
    Use : to check if entered email is unique or not..
    */
    function email_check($username)
    {
        $username = $this->admin_model->email_unique($username);
        if ($username == TRUE) {
            return TRUE;
        } else {
            $this->form_validation->set_message('email_check', THERE_IS_AN_EXISTING_ACCOUNT_ASSOCIATED_WITH_THIS_EMAIL_ID);
            return FALSE;
        }
    }


    /*
     Function name :add_admin()
    Parameter : none.
    Return : none.
    Use : to add or update existing admins of the site...
    */
    function add_admin($id = '')
    {
        $check_rights = get_rights('list_admin');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        $data['show_pass'] = '1';

        $this->load->library('form_validation');

        $this->form_validation->set_rules('email', EMAIL, 'required|valid_email');
        $this->form_validation->set_rules('username', USER_NAME, 'required|alpha_numeric');
        if (isset($_POST['password'])) $this->form_validation->set_rules('password', PASSWORD, 'required|min_length[5]|max_length[12]');
        //$this->form_validation->set_rules('login_ip', 'Login IP', 'required|valid_ip');

        $check_user = '';
        if ($_POST) {
            if ($this->adminemail_check(SecurePostData($this->input->post('email')), $id) == 1) {
                $check_user = EXISTS_ACCOUNT_ASSOCIATE_WITHMAIL . '<br />';
            }
        }
        if ($this->form_validation->run() == FALSE || $check_user != '') {
            if (validation_errors() || $check_user != '') {
                $data["error"] = validation_errors() . $check_user;
            } else {
                $data["error"] = "";
            }
            $data["admin_id"] = $this->input->post('admin_id');
            $data["email"] = $this->input->post('email');
            $data["username"] = $this->input->post('username');
            $data["password"] = $this->input->post('password');
            $data["login_ip"] = $this->input->post('login_ip');
            $data["admin_type"] = $this->input->post('admin_type');
            $data["active"] = $this->input->post('active');

            $data['site_setting'] = site_setting();


            $theme = 'admin';
            $this->template->set_master_template($theme . '/template.php');
            $this->template->write('title', 'Add Administrator', '', TRUE);
            $this->template->write_view('header', 'header', $data, TRUE);
            $this->template->write_view('main_content', 'user/add_admin', $data, TRUE);
            $this->template->write_view('footer', 'footer', '', TRUE);
            $this->template->render();
        } else {

            if ($this->input->post('admin_id') != '') {
                $data = array(
                    'email' => $this->input->post('email'),
                    'username' => $this->input->post('username'),
                    'admin_type' => $this->input->post('admin_type'),
                    'login_ip' => $_SERVER['REMOTE_ADDR'],
                    'active' => $this->input->post('active'),

                );
                $this->home_model->table_update('admin_id', $this->input->post('admin_id'), 'admin', $data);
                $msg = "update";
            } else {
                $this->admin_model->admin_insert();
                $msg = "insert";
                $email_template = $this->db->query("select * from `email_template` where task='Your Admin Account Created'");

                $email_temp = $email_template->row();


                $login_id = $this->session->userdata('admin_id');
                $admin_data = $this->db->query("select email from `admin` where admin_id=" . $login_id . "");
                $admin_detail = $admin_data->row();

                $admin_email = $admin_detail->email;

                $site_setting = site_setting();
                $site_name = $site_setting['site_name'];

                $email_address_from = $email_temp->from_address;

                $email_address_reply = $email_temp->reply_address;

                $email_subject = $email_temp->subject;

                $email_subject = str_replace('{site_name}', $site_name, $email_subject);


                $email_message = $email_temp->message;

                $username = $this->input->post('username');

                $password = $this->input->post('password');

                $email = $this->input->post('email');

                $login_link = base_url() . 'admin';

                $login_link_anchor = '<a href="' . $login_link . '">' . $login_link . '</a>';

                $email_to = $this->input->post('email');

                $email_message = str_replace('{user_name}', $username, $email_message);

                $email_message = str_replace('{username}', $username, $email_message);

                $email_message = str_replace('{email}', $email, $email_message);

                $email_message = str_replace('{login_link}', $login_link_anchor, $email_message);

                $email_message = str_replace('{site_name}', $site_name, $email_message);

                $email_message = str_replace('{password}', $password, $email_message);

                $email_message = str_replace('{admin_email}', $admin_email, $email_message);


                $str = $email_message;


                email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);
            }
            $offset = $this->input->post('offset');
            redirect('admin/admin/list_admin/' . $msg);
        }
    }

    /*
     Function name :edit_admin()
    Parameter : $id=admin id .
    Return : none.
    Use : to edit admin information of the selected site admin..
    */
    function edit_admin($id = 0)
    {

        $check_rights = get_rights('list_admin');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        $one_user = $this->home_model->get_one_tabledata('admin', array('admin_id' => $id));
        $data["error"] = "";
        $data["admin_id"] = $id;
        $data["email"] = $one_user['email'];
        $data["username"] = $one_user['username'];
        $data["password"] = $one_user['password'];
        $data["login_ip"] = $one_user['login_ip'];
        $data["admin_type"] = $one_user['admin_type'];
        $data["active"] = $one_user['active'];

        $data['site_setting'] = site_setting();
        $data['show_pass'] = '';

        //$data["offset"] = $offset;
        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Edit Administrator', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'user/add_admin', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }

    /*
     Function name :delete_admin()
    Parameter : $id=admin id .
    Return : none.
    Use : to delete admin information and account of the selected site admin..
    */
    function delete_admin($id = 0)
    {
        $check_rights = get_rights('list_admin');
        //$limit='20';
        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }
        $total_admin = $this->check_admin_active();
        if ($total_admin > 1) {
            $this->db->delete('rights_assign', array('admin_id' => $id));
            $this->db->delete('admin', array('admin_id' => $id));
            redirect('admin/list_admin/delete');
        } else {
            redirect('admin/list_admin/cantdelete');
        }
    }

    function check_admin_active()
    {
        $query = $this->db->query("select * from admin");
        return $query->num_rows();
    }

    /*
     Function name :action_login()
    Parameter : none.
    Return : none;
    Use : Action admin wants to perform on admin login list page like deleting login records..
    */
    function action_login()
    {
        $check_rights = get_rights('list_admin');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        $offset = $this->input->post('offset');
        $action = $this->input->post('action');
        $login_id = $this->input->post('chk');

        if ($action == 'delete') {
            foreach ($login_id as $id) {
                $this->db->query("delete from admin_login where login_id='" . $id . "'");
            }

            redirect('admin/list_admin/delete_login');
        }

    }

    /*
     Function name :assign_rights()
    Parameter : $id=admin id whose rights admin wants to see or check..
    Return : none;
    Use : Admin can see existing rights of other admins of the site..
    */
    function assign_rights($id)
    {
        if ($id == '') {
            redirect('admin/list_admin');
        }

        $data['admin_id'] = $id;

        $data['assign_rights'] = $this->admin_model->get_assign_rights($id);
        $data['rights'] = $this->admin_model->get_rights();

        $data['site_setting'] = site_setting();

        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Assign Rights', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'user/assign_rights', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();

    }

    /*
     Function name :add_rights()
    Parameter : $id=admin id whose rights admin wants to edit..
    Return : none;
    Use : Admin can add or update existing rights of other admins of the site..
    */
    function add_rights($id)
    {

        $this->admin_model->add_rights();

        redirect('admin/list_admin/rights');

    }

    /*
     Function name :change_password()
    Parameter : none
    Return : none;
    Use : Admin can change the password..
    */
    function change_password()
    {
        $check_rights = get_rights('list_admin');
        //echo $this->session->userdata('admin_id');die;
        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('old_password', OLD_PASSWORD, 'required|trim|min_length[5]|max_length[20]');
        $this->form_validation->set_rules('password', PASSWORD, 'required|trim|min_length[5]|max_length[20]|');
        $this->form_validation->set_rules('cpassword', CON_PASSWORD, 'required|trim|min_length[5]|max_length[20]|matches[password]');
        $data["error"] = '';
        $data["success"] = '';
        $chk_pass = 'error';
        if ($_POST) {
            if (isset($_POST['old_password'])) {
                $chk_pass = $this->admin_model->PasswordMatches();
            }
        }
        if ($this->form_validation->run() == FALSE || $chk_pass == FALSE) {
            if (validation_errors()) {
                $data["error"] = validation_errors();
            } elseif ($chk_pass == FALSE) {
                $data["error"] = OLD_PASSWORD_DOESNT_MATCH;
            } else {
                $data["error"] = "";
            }

        } else {
            $data1 = array('password' => md5(SecurePostData($this->input->post('password'))));

            $this->home_model->table_update('admin_id', $this->session->userdata('admin_id'), 'admin', $data1);

            $data["success"] = YOUR_PASSWORD_HAS_BEEN_UPDATED_SUCCESSFULLY;


        }
        $data['site_setting'] = site_setting();

        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Assign Rights', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'user/change_password', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();

    }

    /*
    Function name :password_matches()
    Parameter : None
    Return : TRUE or FALSE
    Use : to match the old password of admin.
    Description : To match the old password in admin table if it matched then returns TRUE otherwise returns FALSE.
    */

    function password_matches()
    {

        if ($this->admin_model->PasswordMatches()) {
            return TRUE;
        } else {
            $this->form_validation->set_message('password_matches', OLD_PASSWORD_DOESNT_MATCH);
            return FALSE;
        }

    }


    function versions()
    {
        $data['site_setting'] = site_setting();

        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Assign Rights', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'versions', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();

    }


    /*
        function admin_login($offset=0,$msg='')
        {

            $check_rights=$this->home_model->get_rights('list_admin');

            if(	$check_rights==0) {
                redirect('home/dashboard/no_rights');
            }



            $this->load->library('pagination');

            $limit = '10';

            $config['base_url'] = site_url('admin/admin_login/');
            $config['total_rows'] = $this->admin_model->get_total_adminlogin_count();
            $config['per_page'] = $limit;
            $this->pagination->initialize($config);
            $data['page_link'] = $this->pagination->create_links();

            $data['result'] = $this->admin_model->get_adminlogin_result($offset, $limit);
            $data['offset'] = $offset;

            $data['site_setting'] = site_setting();

            $data['msg']=$msg;

            $this->template->write('title', 'Administrator Logins', '', TRUE);
            $this->template->write_view('header', 'header', $data, TRUE);
            $this->template->write_view('main_content', 'list_admin_login', $data, TRUE);
            $this->template->write_view('footer', 'footer', '', TRUE);
            $this->template->render();
        }
    */
    /*
     function list_admin_superadmin($limit='20',$offset=0,$msg='')
     {
    $check_rights=$this->home_model->get_rights('list_admin');

    if(	$check_rights==0) {
    redirect('home/dashboard/no_rights');
    }

    $this->load->library('pagination');

    //$limit = '10';

    $config['base_url'] = site_url('admin/list_admin/');
    $config['total_rows'] = $this->admin_model->get_total_superadmin_count();
    $config['per_page'] = $limit;
    $this->pagination->initialize($config);
    $data['page_link'] = $this->pagination->create_links();

    $data['result'] = $this->admin_model->get_superadmin_result($offset, $limit);
    $data['msg'] = $msg;
    $data['offset'] = $offset;
    $data['limit']=$limit;
    $data['option']='';
    $data['keyword']='';
    $data['search_type']='normal';

    $data['site_setting'] = site_setting();

    $this->template->write('title', 'Administrator', '', TRUE);
    $this->template->write_view('header', 'header', $data, TRUE);
    $this->template->write_view('main_content', 'list_admin', $data, TRUE);
    $this->template->write_view('footer', 'footer', '', TRUE);
    $this->template->render();
    }

    function list_admin_admin($limit='20',$offset=0,$msg='')
    {
    $check_rights=$this->home_model->get_rights('list_admin');

    if(	$check_rights==0) {
    redirect('home/dashboard/no_rights');
    }

    $this->load->library('pagination');

    //$limit = '10';

    $config['base_url'] = site_url('admin/list_admin/');
    $config['total_rows'] = $this->admin_model->get_total_adninistrator_admin_count();
    $config['per_page'] = $limit;
    $this->pagination->initialize($config);
    $data['page_link'] = $this->pagination->create_links();

    $data['result'] = $this->admin_model->get_adninistrator_result($offset, $limit);
    $data['msg'] = $msg;
    $data['offset'] = $offset;
    $data['limit']=$limit;
    $data['option']='';
    $data['keyword']='';
    $data['search_type']='normal';

    $data['site_setting'] = site_setting();

    $this->template->write('title', 'Administrator', '', TRUE);
    $this->template->write_view('header', 'header', $data, TRUE);
    $this->template->write_view('main_content', 'list_admin', $data, TRUE);
    $this->template->write_view('footer', 'footer', '', TRUE);
    $this->template->render();
    }
    */
    /*
     function search_list_admin($limit=20,$option='',$keyword='',$offset=0,$msg='')
     {

    $check_rights=$this->home_model->get_rights('list_admin');

    if(	$check_rights==0) {
    redirect('home/dashboard/no_rights');
    }



    $this->load->library('pagination');


    if($_POST)
    {
    $option=$this->input->post('option');
    $keyword=$this->input->post('keyword');
    }
    else
    {
    $option=$option;
    $keyword=$keyword;
    }

    $keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',trim($keyword)));

    $config['uri_segment']='6';
    $config['base_url'] = site_url('admin/search_list_admin/'.$limit.'/'.$option.'/'.$keyword.'/');
    $config['total_rows'] = $this->admin_model->get_total_search_admin_count($option,$keyword);
    $config['per_page'] = $limit;
    $this->pagination->initialize($config);
    $data['page_link'] = $this->pagination->create_links();

    $data['result'] = $this->admin_model->get_search_admin_result($option,$keyword,$offset, $limit);
    $data['msg'] = $msg;
    $data['offset'] = $offset;


    //$data['statelist']=$this->project_category_model->get_state();

    $data['site_setting'] = site_setting();

    $data['limit']=$limit;
    $data['option']=$option;
    $data['keyword']=$keyword;
    $data['search_type']='search';

    $this->template->write('title', 'Search Admin', '', TRUE);
    $this->template->write_view('header', 'header', $data, TRUE);
    $this->template->write_view('main_content', 'list_admin', $data, TRUE);
    $this->template->write_view('footer', 'footer', '', TRUE);
    $this->template->render();
    }
    */
    /*
    Function name :username_check()
    Parameter :none
    Return : none
    Use : This is a callback function which is use to check this user exist or not.
    */
    function adminemail_check($email, $admin_id)
    {

        $username = $this->admin_model->email_validate($email, $admin_id);
        if ($username != 'FALSE') {
            // $this->form_validation->set_message('username_check', EXISTS_ACCOUNT_ASSOCIATE_WITHMAIL);
            return 1;
        } else {
            return 0;
        }
    }


}

?>
