<?php

/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Site_setting extends ROCKERS_Controller
{

    function __construct()

    {
        parent::__construct();
        $this->load->model('site_setting_model');
        $this->load->model('transaction_type_model');

        $this->load->model('home_model');
    }

    function add_site_setting()

    {


        $don_err = '';


        $check_rights = get_rights('add_site_setting');


        if ($check_rights == 0) {

            redirect('admin/home/dashboard/no_rights');

        }


        $one_site_setting = site_setting();


        $payment_gateway = 0; ////===default wallet


        $preapproval_enable = 0; ////=default inactive


        $donation_amount = $this->input->post('donation_amount');


        $minimum_goal = $this->input->post('minimum_goal');
        $maximum_goal = $this->input->post('maximum_goal');
        $project_min_days = $this->input->post('project_min_days');
        $project_max_days = $this->input->post('project_max_days');

        $this->load->library('form_validation');
        $this->form_validation->set_rules('site_name', SITE_NAME, 'required');

        $this->form_validation->set_rules('project_min_days', MINIMUM_DAYS_FOR_PROJECTS, 'required|numeric');
        $this->form_validation->set_rules('project_max_days', MAXIMUM_DAYS_FOR_PROJECTS, 'required|numeric');
        $this->form_validation->set_rules('maximum_project_per_year', MAXIMUM_NO_PROJECT_PER_YEAR_FOR_ONE_USER, 'required|numeric');

        $this->form_validation->set_rules('payment_gateway', PAYMENT_GATEWAY, 'required');


        $this->form_validation->set_rules('enable_funding_option', FUNDING_DONATION_TYPE_FOR_PROJECTS, 'required');
        if ($this->input->post('enable_funding_option') == 0) {
            $this->form_validation->set_rules('fixed_fees', FIXED_PROJECT_COMMISION, 'required|numeric');
        } else {
            $this->form_validation->set_rules('suc_flexible_fees', FLEXIBLE_SUCCESSFUL_PROJECT_COMMISION, 'required|numeric');
            $this->form_validation->set_rules('flexible_fees', FLEXIBLE_UNSUCCESSFUL_PROJECT_COMMISION, 'required|numeric');
        }


        if ($_POST) {
            if ($minimum_goal > $maximum_goal) $don_err .= '<p>' . PROJECT_ENTER_MAXIMUM_PROJECT_GOAL_AMOUNT_GREATER_THAN_MINIMUM_PROJECT_GOAL_AMOUNT . '</p>';
            if ($project_min_days > $project_max_days) $don_err .= '<p>' . PROJECT_ENTER_MAXIMUM_DAYS_FOR_PROJECT_GREATER_THAN_MINIMUM_DAYS_FOR_PROJECT . '</p>';
        

        }
         $error='';
         if($_FILES){
             foreach ($_FILES as $key=>$value) {
              
            
                if($_FILES[$key]['name']!=''){

                    $field_name = array('file_up' => SITE_LOGO,'favicon_file'=>FAVICON_IMAGE,'file_up2'=>SITE_LOGO_HOVER );
                    $this->load->library('upload');
                    $CI =& get_instance();
                    $base_path = $CI->config->slash_item('base_path');
                    $rand = rand(0, 100000);

                    $_FILES['userfile']['name'] = $_FILES[$key]['name'];

                    $_FILES['userfile']['type'] = $_FILES[$key]['type'];

                    $_FILES['userfile']['tmp_name'] = $_FILES[$key]['tmp_name'];

                    $_FILES['userfile']['error'] = $_FILES[$key]['error'];

                    $_FILES['userfile']['size'] = $_FILES[$key]['size'];

                   if ($_FILES["userfile"]["type"] != "image/jpeg" and $_FILES["userfile"]["type"] != "image/pjpeg" and $_FILES["userfile"]["type"] != "image/png" and $_FILES["userfile"]["type"] != "image/x-png" and $_FILES["userfile"]["type"] != "image/gif") {
                             $error.= '<p>'.$field_name[$key].' - '.PLEASE_UPLOAD_A_JPG_PNG_GIF_FILE.'</p>';
                            }
                    }
            }

        }
        if ($this->form_validation->run() == 0 || $don_err != ''||$error!='') {


            if (validation_errors() || $don_err != '' ||$error!='') {

                $data["error"] = $don_err . validation_errors().$error;

            } else {

                $data["error"] = '';

            }


            if ($this->input->post('site_setting_id')) {

                //$one_site_setting = site_setting();


                $data["site_setting_id"] = $this->input->post('site_setting_id');


                $data["site_name"] = $this->input->post('site_name');


                $data["site_version"] = $one_site_setting['site_version'];


                $data["site_language"] = $this->input->post('site_language');


                $data["currency_symbol_side"] = $this->input->post('currency_symbol_side');


                $data["currency_code"] = $this->input->post('currency_code');


                $data["date_format"] = $this->input->post('date_format');

                $data["time_format"] = $this->input->post('time_format');


                $data["project_min_days"] = $this->input->post('project_min_days');


                $data["project_max_days"] = $this->input->post('project_max_days');


                $data["time_zone"] = $this->input->post('time_zone');


                $data["enable_funding_option"] = $this->input->post('enable_funding_option');


                $data["flexible_fees"] = $this->input->post('flexible_fees');


                $data["suc_flexible_fees"] = $this->input->post('suc_flexible_fees');


                $data["fixed_fees"] = $this->input->post('fixed_fees');


                $data["minimum_goal"] = $this->input->post('minimum_goal');


                $data["maximum_goal"] = $this->input->post('maximum_goal');


                $data["maximum_project_per_year"] = $this->input->post('maximum_project_per_year');


                $data["maximum_donate_per_project"] = $this->input->post('maximum_donate_per_project');


                $data["site_tracker"] = $this->input->post('site_tracker');


                $data['payment_gateway'] = $this->input->post('payment_gateway');


                $data["site_logo"] = $this->input->post('file_up');


                $data["prev_site_logo"] = $this->input->post('prev_site_logo');


                $data["site_logo_hover"] = $this->input->post('file_up2');


                $data["prev_site_logo_hover"] = $this->input->post('prev_site_logo_hover');

                $data["favicon_logo"] = $this->input->post('favicon_file');


                $data["favicon_image"] = $this->input->post('favicon_image');

                $data['payment_gateway'] = $this->input->post('payment_gateway');


                $data['instant_fees'] = $this->input->post('instant_fees');


                $data['ending_days'] = $this->input->post('ending_days');

                $data['signup_captcha'] = $this->input->post('signup_captcha');

                $data['contact_us_captcha'] = $this->input->post('contact_us_captcha');

                $data['captcha_public_key']=$this->input->post('captcha_public_key');



                $data['captcha_private_key']=$this->input->post('captcha_private_key');

                


            } else {


                $data['payment_gateway'] = $one_site_setting['payment_gateway'];


                $data['instant_fees'] = $one_site_setting['instant_fees'];


                $data["site_setting_id"] = $one_site_setting['site_setting_id'];


                $data["site_name"] = $one_site_setting['site_name'];


                $data["site_version"] = $one_site_setting['site_version'];


                $data["site_language"] = $one_site_setting['site_language'];


                $data["currency_symbol_side"] = $one_site_setting['currency_symbol_side'];


                $data["currency_code"] = $one_site_setting['currency_code'];


                $data["date_format"] = $one_site_setting['date_format'];

                $data["time_format"] = $one_site_setting['time_format'];


                $data["maximum_project_per_year"] = $one_site_setting['maximum_project_per_year'];


                $data["maximum_donate_per_project"] = $one_site_setting['maximum_donate_per_project'];


                $data["project_min_days"] = $one_site_setting['project_min_days'];


                $data["project_max_days"] = $one_site_setting['project_max_days'];


                $data["time_zone"] = $one_site_setting['time_zone'];


                $data["enable_funding_option"] = $one_site_setting['enable_funding_option'];


                $data["flexible_fees"] = $one_site_setting['flexible_fees'];


                $data["suc_flexible_fees"] = $one_site_setting['suc_flexible_fees'];


                $data["fixed_fees"] = $one_site_setting['fixed_fees'];


                $data["minimum_goal"] = $one_site_setting['minimum_goal'];


                $data["maximum_goal"] = $one_site_setting['maximum_goal'];


                $data["site_tracker"] = $one_site_setting['site_tracker'];


                $data["payment_gateway"] = $one_site_setting['payment_gateway'];


                $data["site_logo"] = '';


                $data["prev_site_logo"] = $one_site_setting['site_logo'];


                $data["site_logo_hover"] = '';


                $data["prev_site_logo_hover"] = $one_site_setting['site_logo_hover'];

                $data["favicon_logo"] = '';


                $data["favicon_image"] = $one_site_setting['favicon_image'];


                $data["ending_days"] = $one_site_setting['ending_days'];

                $data['signup_captcha'] = $one_site_setting['signup_captcha'];

                $data['contact_us_captcha'] = $one_site_setting['contact_us_captcha'];
                $data['captcha_public_key']=$one_site_setting['captcha_public_key'];

                $data['captcha_private_key']=$one_site_setting['captcha_private_key'];



            }


            $data['languages'] = get_table_data('language', '', 'yes');


            $data['time_zone_list'] = get_table_data('timezone', '', 'yes');


            $data['currency'] = $this->site_setting_model->get_currency();


            $data['site_setting'] = site_setting();


        } else {


            $CI =& get_instance();

            $base_path = $CI->config->slash_item('base_path');

            $this->site_setting_model->site_setting_update();

            $site_setting = site_setting();

            $this->load->library('upload');
            $img_path = $base_path . 'upload/orig/' . $site_setting['site_logo'];
            $bg_img_path = $base_path . 'upload/bg.jpg';
            $save_path = $base_path . 'upload/';

            if(file_exists($img_path))
            {
                $this->merge($bg_img_path, $img_path, $save_path);
            }


            $query = 'update `language` set `default`=1 where language_id =' . $this->input->post('site_language');;
            $this->db->query($query);
            $query = 'update `language` set `default`=0 where language_id !=' . $this->input->post('site_language');;
            $this->db->query($query);
            //print_r(site_setting());
            //deletecache("site_setting");
            setting_deletecache('supported_languages');
            setting_deletecache('default_language');
            setting_deletecache('site_setting');
            setting_new_cache('site_setting', 'site_setting');


            $data['payment_gateway'] = $this->input->post('payment_gateway');


            $data['preapproval_enable'] = $preapproval_enable;


            $data['instant_fees'] = $one_site_setting['instant_fees'];


            $data["error"] = "success";


            $data["site_setting_id"] = $this->input->post('site_setting_id');


            $data["site_name"] = $this->input->post('site_name');


            $data["site_version"] = $one_site_setting['site_version'];


            $data["site_language"] = $this->input->post('site_language');


            $data["currency_symbol_side"] = $this->input->post('currency_symbol_side');


            $data["maximum_project_per_year"] = $this->input->post('maximum_project_per_year');


            $data["maximum_donate_per_project"] = $this->input->post('maximum_donate_per_project');


            $data["currency_code"] = $this->input->post('currency_code');


            $data["date_format"] = $this->input->post('date_format');

            $data["time_format"] = $this->input->post('time_format');


            $data["project_min_days"] = $this->input->post('project_min_days');


            $data["project_max_days"] = $this->input->post('project_max_days');


            $data["time_zone"] = $this->input->post('time_zone');


            $data["enable_funding_option"] = $this->input->post('enable_funding_option');


            $data["flexible_fees"] = $this->input->post('flexible_fees');


            $data["suc_flexible_fees"] = $this->input->post('suc_flexible_fees');


            $data["fixed_fees"] = $this->input->post('fixed_fees');


            $data["minimum_goal"] = $this->input->post('minimum_goal');


            $data["maximum_goal"] = $this->input->post('maximum_goal');


            $data["site_tracker"] = $this->input->post('site_tracker');


            $data["site_logo"] = '';


            $data["prev_site_logo"] = $site_setting['site_logo'];


            $data["site_logo_hover"] = '';


            $data["prev_site_logo_hover"] = $site_setting['site_logo_hover'];

            $data["favicon_logo"] = '';


            $data["favicon_image"] = $one_site_setting['favicon_image'];


            $data["ending_days"] = $one_site_setting['ending_days'];

            $data["contact_us_captcha"] = $one_site_setting['contact_us_captcha'];

            $data["signup_captcha"] = $one_site_setting['signup_captcha'];


            $data['site_setting'] = site_setting();


            $data['languages'] = get_table_data('language', '', 'yes');


            $data['time_zone_list'] = get_table_data('timezone', '', 'yes');

            $data['captcha_public_key']=$this->input->post('captcha_public_key');



            $data['captcha_private_key']=$this->input->post('captcha_private_key');



            $data['currency'] = $this->site_setting_model->get_currency();

            //redirect('admin/site_setting/add_site_setting');


        }


        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');


        $this->template->write('title', 'Administrator', '', TRUE);

        $this->template->write_view('header', 'header', $data, TRUE);

        $this->template->write_view('main_content', 'setting/add_site_setting', $data, TRUE);

        $this->template->write_view('footer', 'footer', '', TRUE);

        $this->template->render();


    }


    /*

     Function name :add_site_setting()

    Parameter : none.

    Return : noe

    Use : this function is used to update site setting data.

    */

    function merge($filename_x, $filename_y, $filename_result)
    {

        // Get dimensions for specified images

         $CI =& get_instance();
        $base_path = $CI->config->slash_item('base_path');
        require_once $base_path . 'thumbgenerator/ThumbLib.inc.php';

        list($width_x, $height_x) = getimagesize($filename_x);
        list($width_y, $height_y) = getimagesize($filename_y);

        // Create new image with desired dimensions

        $image = imagecreatetruecolor($width_x, $height_x);
        $image_x = '';
        // Load images and then copy to destination image


        switch (exif_imagetype($filename_x)) {
            case IMAGETYPE_GIF  :
                $image_x = imagecreatefromgif($filename_x);
                break;
            case IMAGETYPE_JPEG :
                $image_x = imagecreatefromjpeg($filename_x);
                break;
            case IMAGETYPE_PNG  :
                $image_x = imagecreatefrompng($filename_x);
                break;
            default :
                return;
        }

        switch (exif_imagetype($filename_y)) {
            case IMAGETYPE_GIF  :
                $image_y = imagecreatefromgif($filename_y);
                break;
            case IMAGETYPE_JPEG :
                $image_y = imagecreatefromjpeg($filename_y);
                break;
            case IMAGETYPE_PNG  :
                $image_y = imagecreatefrompng($filename_y);
                break;
            default :
                return;
        }

         if ($image_x != '') {
            $temp_x = intval(($width_x - $width_y) / 2);
            $temp_y = intval(($height_x - $height_y) / 2);

            imagecopy($image, $image_x, 0, 0, 0, 0, $width_x, $height_x);
            imagecopy($image, $image_y, $temp_x, $temp_y, 0, 0, $width_y, $height_y);
            // Save the resulting image to disk (as JPEG)
            imagejpeg($image, $filename_result . '/orig/no_img.jpg');
            $image_settings = get_image_setting_data();

            $image_info = getimagesize($base_path . 'upload/orig/no_img.jpg');
            
            $image_width = $image_info[0];
            $image_height = $image_info[1];

            //common card no image 

            $new_w = $image_settings['p_thumb_width']; //You can change these to fit the width and height you want
            $new_h = $image_settings['p_thumb_height'];
            $aspect_ratio = $image_settings['p_ratio'];

            if ($aspect_ratio) {
                if ($image_height > $image_width) {
                    $ratio = $new_h / $image_height;

                    $new_w = $image_width * $ratio;
                } else {
                    $ratio = $new_w / $image_width;

                    $new_h = $image_height * $ratio;
                }
            }

            $thumb = PhpThumbFactory::create($base_path . "upload/orig/no_img.jpg");
            $thumb->adaptiveResize($new_w, $new_h);
            $cache_path = $base_path . "upload/equity/commoncard/no_img.jpg";
            $thumb->save($cache_path);

            // small no image

            $new_w = $image_settings['p_small_width']; //You can change these to fit the width and height you want
            $new_h = $image_settings['p_small_height'];

            if ($aspect_ratio) {
                if ($image_height > $image_width) {
                    $ratio = $new_h / $image_height;

                    $new_w = $image_width * $ratio;
                } else {
                    $ratio = $new_w / $image_width;

                    $new_h = $image_height * $ratio;
                }
            }

            $thumb = PhpThumbFactory::create($base_path . "upload/orig/no_img.jpg");
            $thumb->adaptiveResize($new_w, $new_h);
            $cache_path = $base_path . "upload/equity/small/no_img.jpg";
            $thumb->save($cache_path);


              //medium no image 

            $new_w = $image_settings['p_medium_width']; //You can change these to fit the width and height you want
            $new_h = $image_settings['p_medium_height'];
            $aspect_ratio = $image_settings['p_ratio'];

            if ($aspect_ratio) {
                if ($image_height > $image_width) {
                    $ratio = $new_h / $image_height;

                    $new_w = $image_width * $ratio;
                } else {
                    $ratio = $new_w / $image_width;

                    $new_h = $image_height * $ratio;
                }
            }

            $thumb = PhpThumbFactory::create($base_path . "upload/orig/no_img.jpg");
            $thumb->adaptiveResize($new_w, $new_h);
            $cache_path = $base_path . "upload/equity/medium/no_img.jpg";
            $thumb->save($cache_path);

            // large no image

            $new_w = $image_settings['p_large_width']; //You can change these to fit the width and height you want
            $new_h = $image_settings['p_large_height'];

            if ($aspect_ratio) {
                if ($image_height > $image_width) {
                    $ratio = $new_h / $image_height;

                    $new_w = $image_width * $ratio;
                } else {
                    $ratio = $new_w / $image_width;

                    $new_h = $image_height * $ratio;
                }
            }

            $thumb = PhpThumbFactory::create($base_path . "upload/orig/no_img.jpg");
            $thumb->adaptiveResize($new_w, $new_h);
            $cache_path = $base_path . "upload/equity/large/no_img.jpg";
            $thumb->save($cache_path);

             // category no image

            $new_w = '370'; //You can change these to fit the width and height you want
            $new_h = '230';

            if ($aspect_ratio) {
                if ($image_height > $image_width) {
                    $ratio = $new_h / $image_height;

                    $new_w = $image_width * $ratio;
                } else {
                    $ratio = $new_w / $image_width;

                    $new_h = $image_height * $ratio;
                }
            }

            $thumb = PhpThumbFactory::create($base_path . "upload/orig/no_img.jpg");
            $thumb->adaptiveResize($new_w, $new_h);
            $cache_path = $base_path . "upload/category/no_img.jpg";
            $thumb->save($cache_path);

            // category no image

            $new_w = '900'; //You can change these to fit the width and height you want
            $new_h = '285';

            if ($aspect_ratio) {
                if ($image_height > $image_width) {
                    $ratio = $new_h / $image_height;

                    $new_w = $image_width * $ratio;
                } else {
                    $ratio = $new_w / $image_width;

                    $new_h = $image_height * $ratio;
                }
            }

            $thumb = PhpThumbFactory::create($base_path . "upload/orig/no_img.jpg");
            $thumb->adaptiveResize($new_w, $new_h);
            $cache_path = $base_path . "upload/category_search/no_img.jpg";
            $thumb->save($cache_path);

              // category no image

            $new_w = '1300'; //You can change these to fit the width and height you want
            $new_h = '600';

            if ($aspect_ratio) {
                if ($image_height > $image_width) {
                    $ratio = $new_h / $image_height;

                    $new_w = $image_width * $ratio;
                } else {
                    $ratio = $new_w / $image_width;

                    $new_h = $image_height * $ratio;
                }
            }

            $thumb = PhpThumbFactory::create($base_path . "upload/orig/no_img.jpg");
            $thumb->adaptiveResize($new_w, $new_h);
            $cache_path = $base_path . "upload/dynamic/no_img.jpg";
            $thumb->save($cache_path);

        }
    }

    /*

     Function name :add_other_setting()

    Parameter : none.

    Return : noe

    Use : this function is used to update site setting data.

    */

    function add_other_setting()

    {


        $don_err = '';


        $data['error'] = '';


        $check_rights = get_rights('add_site_setting');


        if ($check_rights == 0) {

            redirect('admin/home/dashboard/no_rights');

        }


        $one_site_setting = site_setting();
        $data['site_setting'] = $one_site_setting;


        $don_err = '';
        if ($_POST) {

            if ($_FILES['accredential_file']['name'] != '') {

                $type_file = explode('/', $_FILES['accredential_file']['type']);
                $upload_file_type = $type_file[1];
                $upload_file_type = str_replace('"', '', $upload_file_type);
                $file_type_array = array('zip', 'pdf', 'octet-stream');
                if (!in_array($upload_file_type, $file_type_array)) {


                    $don_err = '<p>' . PLEASE_UPLOAD_ZIP_OR_PDF_FILE . ' ' . FOR_ACCREDENTIAL_DOCUMENT . '</p>';

                } else if ($_FILES["accredential_file"]["size"] > 20000000) {


                    $don_err = '<p>' . SORRY_THIS_FILE_IS_TOO_LARGE . ' ' . FOR_ACCREDENTIAL_DOCUMENT . '</p>';

                }

            }

        }
        $data["bank_detail"] = $one_site_setting['bank_detail'];
        $data["accredential_status"] = $one_site_setting['accredential_status'];
        $data["contract_copy_status"] = $one_site_setting['contract_copy_status'];
        $data["accredential_file"] = $one_site_setting['accredential_file'];
        $data["sign_contract_upload_by"] = $one_site_setting['sign_contract_upload_by'];
        $data["accrediated_manage"] = $one_site_setting['accrediated_manage'];


        $data["site_setting_id"] = $one_site_setting['site_setting_id'];


        if ($don_err != '') {


            

                $data["error"] = $don_err;

            


            if ($this->input->post('site_setting_id')) {

                $data["site_setting_id"] = $this->input->post('site_setting_id');

                $data['accredential_status'] = $this->input->post('accredential_status');
                $data['accredential_file'] = $this->input->post('accredential_file');
                $data['bank_detail'] = $this->input->post('bank_detail');
                $data['sign_contract_upload_by'] = $this->input->post('sign_contract_upload_by');
                $data['accrediated_manage'] = $this->input->post('accrediated_manage');
                $data["contract_copy_status"] = $this->input->post('contract_copy_status');

            } else {

                $data["bank_detail"] = $one_site_setting['bank_detail'];
                $data["accredential_status"] = $one_site_setting['accredential_status'];
                $data["accredential_file"] = $one_site_setting['accredential_file'];
                $data["sign_contract_upload_by"] = $one_site_setting['sign_contract_upload_by'];
                $data['accrediated_manage'] = $one_site_setting['accrediated_manage'];
                $data["contract_copy_status"] = $one_site_setting['contract_copy_status'];
                $data["site_setting_id"] = $one_site_setting['site_setting_id'];


            }

        } else {


           if($_POST)
           {

               $this->site_setting_model->other_setting_update();
           
                $data["error"] = "success";

                setting_deletecache('site_setting');
                setting_new_cache('site_setting', 'site_setting');

            
            } else {
                
                $data["error"] ="";
                
                $data["bank_detail"] = $one_site_setting['bank_detail'];
                $data["accredential_status"] = $one_site_setting['accredential_status'];
                $data["accredential_file"] = $one_site_setting['accredential_file'];
                $data["sign_contract_upload_by"] = $one_site_setting['sign_contract_upload_by'];
                $data['accrediated_manage'] = $one_site_setting['accrediated_manage'];
                $data["contract_copy_status"] = $one_site_setting['contract_copy_status'];
                $data["site_setting_id"] = $one_site_setting['site_setting_id'];


            }
            
            

        }


        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');


        $this->template->write('title', 'Administrator', '', TRUE);

        $this->template->write_view('header', 'header', $data, TRUE);

        $this->template->write_view('main_content', 'setting/add_other_setting', $data, TRUE);

        $this->template->write_view('footer', 'footer', '', TRUE);

        $this->template->render();


    }

    function add_amount_formatting()
    {


        $check_rights = get_rights('add_site_setting');


        if ($check_rights == 0) {


            redirect('admin/home/dashboard/no_rights');


        }


        $one_site_setting = site_setting();


        $data['site_setting'] = site_setting();


        $data['languages'] = get_table_data('language', '', 'yes');


        $data['time_zone_list'] = get_table_data('timezone', '', 'yes');


        $data['currency'] = $this->site_setting_model->get_currency();


        $this->load->library('form_validation');


        $this->form_validation->set_rules('currency_code', CURRENCY_CODE, 'required');


        $this->form_validation->set_rules('currency_symbol_side', WHERE_TO_DISPLAY_CURRENCY_SYMBOL, 'required');


        $this->form_validation->set_rules('decimal_points', DECIMAL_POINTS_AFTER_AMOUNT, 'required');


        if ($this->form_validation->run() == FALSE) {


            if (validation_errors()) {


                $data["error"] = validation_errors();


            } else {


                $data["error"] = "";


            }


            if ($this->input->post('site_setting_id')) {


                $data["site_setting_id"] = $this->input->post('site_setting_id');


                $data["currency_code"] = $this->input->post('currency_code');


                $data["currency_symbol_side"] = $one_site_setting['currency_symbol_side'];


                $data["decimal_points"] = $this->input->post('decimal_points');


            } else {


                $data["site_setting_id"] = $one_site_setting['site_setting_id'];


                $data["currency_code"] = $one_site_setting['currency_code'];


                $data["currency_symbol_side"] = $one_site_setting['currency_symbol_side'];


                $data["decimal_points"] = $one_site_setting['decimal_points'];


            }

            $theme = 'admin';

            $this->template->set_master_template($theme . '/template.php');


            $this->template->write('title', 'Administrator', '', TRUE);

            $this->template->write_view('header', 'header', $data, TRUE);

            $this->template->write_view('main_content', 'setting/add_amount_formatting', $data, TRUE);

            $this->template->write_view('footer', 'footer', '', TRUE);

            $this->template->render();


        } else {


            $this->site_setting_model->amount_setting_update();


            setting_deletecache('site_setting');
            setting_new_cache('site_setting', 'site_setting');


            $data["error"] = AMOUNT_SETTING_UPDATED_SUCCESSFULLY;


            $data["site_setting_id"] = $this->input->post('site_setting_id');


            $data["currency_code"] = $this->input->post('currency_code');


            $data["currency_symbol_side"] = $one_site_setting['currency_symbol_side'];


            $data["decimal_points"] = $this->input->post('decimal_points');


            $theme = 'admin';

            $this->template->set_master_template($theme . '/template.php');


            $this->template->write('title', 'Administrator', '', TRUE);

            $this->template->write_view('header', 'header', $data, TRUE);

            $this->template->write_view('main_content', 'setting/add_amount_formatting', $data, TRUE);

            $this->template->write_view('footer', 'footer', '', TRUE);

            $this->template->render();

        }


    }


    /*

     Function name :add_amount_formatting()

    Parameter : none.

    Return : none

    Use : this function is used to update amount data in site setting

    */

    function add_image_setting()
    {


        $check_rights = get_rights('add_site_setting');


        if ($check_rights == 0) {


            redirect('admin/home/dashboard/no_rights');


        }


        $data['site_setting'] = site_setting();


        $this->load->library('form_validation');


        $this->form_validation->set_rules('p_s_width', PROJECT_SMALL_IMAGE_WIDTH, 'required');


        $this->form_validation->set_rules('p_s_height', PROJECT_SMALL_IMAGE_HEIGHT, 'required');


        $this->form_validation->set_rules('p_m_width', PROJECT_MEDIUM_IMAGE_WIDTH, 'required');


        $this->form_validation->set_rules('p_m_height', PROJECT_MEDIUM_IMAGE_HEIGHT, 'required');


        $this->form_validation->set_rules('p_l_width', PROJECT_LARGE_IMAGE_WIDTH, 'required');


        $this->form_validation->set_rules('p_l_height', PROJECT_LARGE_IMAGE_HEIGHT, 'required');


        $this->form_validation->set_rules('u_s_width', PROJECT_SMALL_IMAGE_WIDTH, 'required');


        $this->form_validation->set_rules('u_s_height', USER_SMALL_IMAGE_HEIGHT, 'required');


        $this->form_validation->set_rules('u_b_width', USER_BIG_IMAGE_WIDTH, 'required');


        $this->form_validation->set_rules('u_b_height', USER_BIG_IMAGE_HEIGHT, 'required');


        $this->form_validation->set_rules('u_m_width', USER_MEDIUM_WIDTH_IMAGE_WIDTH, 'required');


        $this->form_validation->set_rules('u_m_height', USER_MEDIUM_WIDTH_IMAGE_HEIGHT, 'required');


        $this->form_validation->set_rules('p_thumb_width', PROJECT_THUMB_IMAGE_WIDTH, 'required');


        $this->form_validation->set_rules('p_thumb_height', PROJECT_THUMB_IMAGE_HEIGHT, 'required');

        $this->form_validation->set_rules('upload_limit', IMAGE_UPLOAD_LIMIT, 'required');


        $err = '';


        if ($_POST) {


            if ($this->input->post('p_thumb_width') <= 0) {


                $err .= '<p>' . PROJECT_THUMBNAIL_WIDTH_SHOULD_BE_GREATE_THAN_ZERO . '.</p>';


            }


            if ($this->input->post('p_thumb_height') <= 0) {


                $err .= '<p>' . PROJECT_THUMBNAIL_HEIGHT_SHOULD_BE_GREATE_THAN_ZERO . '.</p>';


            }


            if ($this->input->post('p_s_width') <= 0) {


                $err .= '<p>' . PROJECT_SMALL_WIDTH_SHOULD_BE_GREATER_THAN_ZERO . '.</p>';


            }


            if ($this->input->post('p_s_height') <= 0) {


                $err .= '<p>' . PROJECT_SMALL_HEIGHT_SHOULD_BE_GREATER_THAN_ZERO . '.</p>';


            }


            if ($this->input->post('p_m_width') <= 0) {


                $err .= '<p>' . PROJECT_MEDIUM_WIDTH_SHOULD_BE_GREATER_THAN_ZERO . '.</p>';


            }


            if ($this->input->post('p_m_height') <= 0) {


                $err .= '<p>' . PROJECT_MEDIUM_HEIGHT_SHOULD_BE_GREATER_THAN_ZERO . '.</p>';


            }


            if ($this->input->post('p_l_width') <= 0) {


                $err .= '<p>' . PROJECT_LARGE_WIDTH_SHOULD_BE_GREATER_THAN_ZERO . '.</p>';


            }


            if ($this->input->post('p_l_height') <= 0) {


                $err .= '<p>' . PROJECT_LARGE_HEIGHT_SHOULD_BE_GREATER_THAN_ZERO . '.</p>';


            }


            if ($this->input->post('u_s_width') <= 0) {


                $err .= '<p>' . USER_SMALL_WIDTH_SHOULD_BE_GREATER_THAN_ZERO . '.</p>';


            }


            if ($this->input->post('u_s_height') <= 0) {


                $err .= '<p>' . USER_SMALL_HEIGHT_SHOULD_BE_GREATER_THAN_ZERO . '.</p>';


            }


            if ($this->input->post('u_m_width') <= 0) {


                $err .= '<p>' . USER_MEDIUM_WIDTH_SHOULD_BE_GREATER_THAN_ZERO . '.</p>';


            }


            if ($this->input->post('u_m_height') <= 0) {


                $err .= '<p>' . USER_MEDIUM_HEIGHT_SHOULD_BE_GREATER_THAN_ZERO . '.</p>';


            }


            if ($this->input->post('u_b_width') <= 0) {


                $err .= '<p>' . PROJECT_BIG_WIDTH_SHOULD_BE_GREATER_THAN_ZERO . '.</p>';


            }


            if ($this->input->post('u_b_height') <= 0) {


                $err .= '<p>' . PROJECT_BIG_HEIGHT_SHOULD_BE_GREATER_THAN_ZERO . '.</p>';


            }
             if ($this->input->post('upload_limit') <= 0) {


                $err .= '<p>' . UPLOAD_LIMIT_SHOULD_BE_GREATER_THAN_ZERO . '.</p>';


            }


        }


        if ($this->form_validation->run() == FALSE || $err != '') {


            if (validation_errors()) {


                $data["error"] = validation_errors();


            } else {


                $data["error"] = "";


            }


            if ($err != '') {


                $data["error"] .= $err;


            }


            if ($this->input->post('image_setting_id')) {


                $data["p_thumb_width"] = $this->input->post('p_thumb_width');


                $data["p_thumb_height"] = $this->input->post('p_thumb_height');


                $data["p_ratio"] = $this->input->post('p_ratio');


                $data["u_s_width"] = $this->input->post('u_s_width');


                $data["u_s_height"] = $this->input->post('u_s_height');


                $data["u_ratio"] = $this->input->post('u_ratio');


                $data["u_m_width"] = $this->input->post('u_m_width');


                $data["u_m_height"] = $this->input->post('u_m_height');


                $data["u_b_width"] = $this->input->post('u_b_width');


                $data["u_b_height"] = $this->input->post('u_b_height');


                $data["p_s_width"] = $this->input->post('p_s_width');


                $data["p_s_height"] = $this->input->post('p_s_height');


                $data["p_m_width"] = $this->input->post('p_m_width');


                $data["p_m_height"] = $this->input->post('p_m_height');


                $data["p_l_width"] = $this->input->post('p_l_width');


                $data["p_l_height"] = $this->input->post('p_l_height');


                $data["g_ratio"] = $this->input->post('g_ratio');


                $data["image_setting_id"] = $this->input->post('image_setting_id');
                $data["upload_limit"] = $this->input->post('upload_limit');


            } else {


                $one_img_setting = get_image_setting_data();


                $data["p_thumb_width"] = $one_img_setting['p_thumb_width'];


                $data["p_thumb_height"] = $one_img_setting['p_thumb_height'];


                $data["p_ratio"] = $one_img_setting['p_ratio'];


                $data["u_s_width"] = $one_img_setting['u_s_width'];


                $data["u_s_height"] = $one_img_setting['u_s_height'];


                $data["u_ratio"] = $one_img_setting['u_ratio'];


                $data["u_m_width"] = $one_img_setting['u_m_width'];


                $data["u_m_height"] = $one_img_setting['u_m_height'];


                $data["u_b_width"] = $one_img_setting['u_b_width'];


                $data["u_b_height"] = $one_img_setting['u_b_height'];


                $data["g_ratio"] = $one_img_setting['g_ratio'];


                $data["p_s_height"] = $one_img_setting['p_small_height'];


                $data["p_s_width"] = $one_img_setting['p_small_width'];


                $data["p_m_height"] = $one_img_setting['p_medium_height'];


                $data["p_m_width"] = $one_img_setting['p_medium_width'];


                $data["p_l_height"] = $one_img_setting['p_large_height'];


                $data["p_l_width"] = $one_img_setting['p_large_width'];


                $data["image_setting_id"] = $one_img_setting['image_setting_id'];
                $data["upload_limit"] = $one_img_setting['upload_limit'];

            }


            $theme = 'admin';

            $this->template->set_master_template($theme . '/template.php');


            $this->template->write('title', 'Administrator', '', TRUE);

            $this->template->write_view('header', 'header', $data, TRUE);

            $this->template->write_view('main_content', 'setting/add_image_setting', $data, TRUE);

            $this->template->write_view('footer', 'footer', '', TRUE);

            $this->template->render();


        } else {

            $data1 = array(

                'p_thumb_width' => $this->input->post('p_thumb_width'),

                'p_thumb_height' => $this->input->post('p_thumb_height'),

                'p_ratio' => $this->input->post('p_ratio'),

                'p_small_width' => $this->input->post('p_s_width'),

                'p_small_height' => $this->input->post('p_s_height'),

                'p_medium_width' => $this->input->post('p_m_width'),

                'p_medium_height' => $this->input->post('p_m_height'),

                'p_large_width' => $this->input->post('p_l_width'),

                'p_large_height' => $this->input->post('p_l_height'),

                'u_s_width' => $this->input->post('u_s_width'),

                'u_s_height' => $this->input->post('u_s_height'),

                'u_m_width' => $this->input->post('u_m_width'),

                'u_m_height' => $this->input->post('u_m_height'),

                'u_b_width' => $this->input->post('u_b_width'),

                'u_b_height' => $this->input->post('u_b_height'),

                'u_ratio' => $this->input->post('u_ratio'),

                'g_ratio' => $this->input->post('g_ratio'),

                'upload_limit' => $this->input->post('upload_limit')

            );


            $this->home_model->table_update('image_setting_id', $this->input->post('image_setting_id'), 'image_setting', $data1);

            setting_deletecache('image_setting');
            setting_new_cache('image_setting', 'image_setting');

            $data["error"] = "success";


            $data["p_thumb_width"] = $this->input->post('p_thumb_width');


            $data["p_thumb_height"] = $this->input->post('p_thumb_height');


            $data["p_ratio"] = $this->input->post('p_ratio');


            $data["u_s_width"] = $this->input->post('u_s_width');


            $data["u_s_height"] = $this->input->post('u_s_height');


            $data["u_ratio"] = $this->input->post('u_ratio');


            $data["u_b_width"] = $this->input->post('u_b_width');


            $data["u_b_height"] = $this->input->post('u_b_height');


            $data["u_m_width"] = $this->input->post('u_m_width');


            $data["u_m_height"] = $this->input->post('u_m_height');


            $data["p_s_width"] = $this->input->post('p_s_width');


            $data["p_s_height"] = $this->input->post('p_s_height');


            $data["p_m_width"] = $this->input->post('p_m_width');


            $data["p_m_height"] = $this->input->post('p_m_height');


            $data["p_l_width"] = $this->input->post('p_l_width');


            $data["p_l_height"] = $this->input->post('p_l_height');


            $data["g_ratio"] = $this->input->post('g_ratio');

            $data["upload_limit"] = $this->input->post('upload_limit');


            $data["image_setting_id"] = $this->input->post('image_setting_id');


            $theme = 'admin';

            $this->template->set_master_template($theme . '/template.php');


            $this->template->write('title', 'Administrator', '', TRUE);

            $this->template->write_view('header', 'header', $data, TRUE);

            $this->template->write_view('main_content', 'setting/add_image_setting', $data, TRUE);

            $this->template->write_view('footer', 'footer', '', TRUE);

            $this->template->render();


        }


    }


    /*

     Function name :add_image_setting()

    Parameter : none.

    Return : none

    Use : this function is used to update image size data of image table.

    */

    function add_filter_setting()

    {


        $check_rights = get_rights('add_filter_setting');


        if ($check_rights == 0) {


            redirect('admin/home/dashboard/no_rights');


        }


        $data['site_setting'] = site_setting();

        $this->load->library('form_validation');


        $this->form_validation->set_rules('ending_soon', ENDING_SOON, 'required|is_natural_no_zero');


        /*	$this->form_validation->set_rules('small_project', 'Small Project', 'required|is_natural_no_zero');*/


        $this->form_validation->set_rules('popular', POPULAR, 'required|numeric');

        $this->form_validation->set_rules('most_funded', MOST_FUNDED, 'required|numeric');


        $this->form_validation->set_rules('successful', SUCCESSFUL, 'required|numeric');


        if ($this->form_validation->run() == FALSE) {


            if (validation_errors()) {


                $data["error"] = validation_errors();


            } else {


                $data["error"] = "";


            }


            if ($this->input->post('site_setting_id')) {


                $data["site_setting_id"] = $this->input->post('site_setting_id');


                $data["ending_soon"] = $this->input->post('ending_soon');


                /*$data["small_project"] = $this->input->post('small_project');*/


                $data["most_funded"] = $this->input->post('most_funded');

                $data["popular"] = $this->input->post('popular');


                $data["successful"] = $this->input->post('successful');


            } else {


                $one_site_setting = site_setting();


                $data["site_setting_id"] = $one_site_setting['site_setting_id'];


                $data["ending_soon"] = $one_site_setting['ending_soon'];

                $data["most_funded"] = $one_site_setting['most_funded'];

                /*$data["small_project"] = $one_site_setting['small_project'];*/


                $data["popular"] = $one_site_setting['popular'];


                $data["successful"] = $one_site_setting['successful'];

            }


            $theme = 'admin';

            $this->template->set_master_template($theme . '/template.php');


            $this->template->write('title', 'Administrator', '', TRUE);

            $this->template->write_view('header', 'header', $data, TRUE);

            $this->template->write_view('main_content', 'setting/add_filter_setting', $data, TRUE);

            $this->template->write_view('footer', 'footer', '', TRUE);

            $this->template->render();


        } else {

            $data1 = array(

                'ending_soon' => $this->input->post('ending_soon'),

                /*	'small_project' => $this->input->post('small_project'),		*/

                'popular' => $this->input->post('popular'),

                'most_funded' => $this->input->post('most_funded'),

                'successful' => $this->input->post('successful'),

            );


            $this->home_model->table_update('site_setting_id', $this->input->post('site_setting_id'), 'site_setting', $data1);

            deletecache("site_setting");

            $data["error"] = "success";


            $data["site_setting_id"] = $this->input->post('site_setting_id');


            $data["ending_soon"] = $this->input->post('ending_soon');

            $data["most_funded"] = $this->input->post('most_funded');

            /*$data["small_project"] = $this->input->post('small_project');*/


            $data["popular"] = $this->input->post('popular');


            $data["successful"] = $this->input->post('successful');


            $theme = 'admin';

            $this->template->set_master_template($theme . '/template.php');


            $this->template->write('title', 'Administrator', '', TRUE);

            $this->template->write_view('header', 'header', $data, TRUE);

            $this->template->write_view('main_content', 'setting/add_filter_setting', $data, TRUE);

            $this->template->write_view('footer', 'footer', '', TRUE);

            $this->template->render();


        }

    }


    /*

     Function name :add_filter_setting()

    Parameter : none.

    Return : none

    Use : this function is used to update project fitler data in site setting table.

    */

    public function delete_cash($uri_string = null)
    {
        //echo '<pre>';
        $dir = scandir(base_path() . 'application/cache');
        // var_dump($dir);
        // die;
        foreach ($dir as $file) {
            if (!is_dir($file)) {
                //echo $file.'<br>';
                if (unlink(base_path() . 'application/cache/' . $file)) {
                    //echo 'DELETED: '.$file.'<br>';
                }
            }
        }

        $this->index();
    }

    function index()

    {


        redirect('admin/site_setting/add_site_setting');

    }

    function slider_setting($msg = '')
    {

        $check_rights = get_rights('add_banner_slider');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }
        $site_setting = site_setting();
        $data['site_setting'] = $site_setting;
        $data['site_setting_id'] = $site_setting['site_setting_id'];
        $data['slider_setting'] = $site_setting['slider_setting'];
        //$data['limit']=$limit;
        $data['option'] = '';
        $data['keyword'] = '';
        $data['search_type'] = 'normal';

        $data['result'] = $this->site_setting_model->GetAllSliderData(array('view_order' => 'asc'));

        $data['msg'] = $msg;
        //$data['offset'] = $offset;
        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Dynamic slider', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'setting/list_slider', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }

    function UpdateSliderOrder()
    {


        $newOrder = $_POST['id'];

        $counter = 1;

        $slider_setting = $this->input->post('slider_setting');
        $site_setting = site_setting();
        $data['site_setting'] = $site_setting;
        $site_setting_id = $site_setting['site_setting_id'];

        $this->db->query("UPDATE site_setting SET slider_setting = " . $slider_setting . " WHERE site_setting_id = " . $site_setting_id . "");

        foreach ($newOrder as $recordIDValue) {


            $this->db->query("UPDATE slider_setting SET view_order = " . $counter . " WHERE id = " . (int)$recordIDValue . "");


            $counter++;
        }


        redirect('admin/site_setting/slider_setting');
    }

    function UpdateSliderSetting()
    {


        $slider_setting = $this->input->post('slider_setting');
        $site_setting = site_setting();
        $site_setting_id = $site_setting['site_setting_id'];

        $this->db->query("UPDATE site_setting SET slider_setting = " . $slider_setting . " WHERE site_setting_id = " . $site_setting_id . "");

        echo "success";
        die;
    }

   /*
     Function name :add_taxonomy()
     Parameter : none.
     Return : none
     Use : this function is used to create url.
    */

    function add_taxonomy($lang_id=1)
    {

        $data['site_setting'] = site_setting();
        $this->load->library('form_validation');

        $this->form_validation->set_rules('project_name', PROJECT_NAME_SINGULAR, 'required');
        $this->form_validation->set_rules('project_name_plural', PROJECT_NAME_PLURAL, 'required');
        $this->form_validation->set_rules('project_url', PROJECT_URL, 'required');
        $this->form_validation->set_rules('project_owner', PROJECT_OWNER_SINGULAR, 'required');
        $this->form_validation->set_rules('project_owner_plural', PROJECT_OWNER_PLURAL, 'required');
        $this->form_validation->set_rules('funds', FUNDS_SINGULAR, 'required');
        $this->form_validation->set_rules('funds_plural', FUNDS_PLURAL, 'required');
        $this->form_validation->set_rules('funds_past', FUNDS_PAST_PARTICIPATE, 'required');
        $this->form_validation->set_rules('funds_gerund', FUNDS_GERUND, 'required');
        $this->form_validation->set_rules('updates', UPDATES_SINGULAR, 'required');
        $this->form_validation->set_rules('updates_plural', UPDATES_PLURAL, 'required');
        $this->form_validation->set_rules('updates_past', UPDATES_PAST_PARTICIPATE, 'required');
        $this->form_validation->set_rules('updates_gerund', UPDATES_GERUND, 'required');
        $this->form_validation->set_rules('comments', COMMENTS_SINGULAR, 'required');
        $this->form_validation->set_rules('comments_plural', COMMENTS_PLURAL, 'required');
        $this->form_validation->set_rules('comments_past', COMMENTS_PAST_PARTICIPATE, 'required');
        $this->form_validation->set_rules('comments_gerund', COMMENTS_GERUND, 'required');
        $this->form_validation->set_rules('follower', FOLLOWER_SINGULAR, 'required');
        $this->form_validation->set_rules('follower_plural', FOLLOWER_PLURAL, 'required');
        $this->form_validation->set_rules('follower_past', FOLLOWER_PAST_PARTICIPATE, 'required');
        $this->form_validation->set_rules('follower_gerund', FOLLOWER_GERUND, 'required');
        $this->form_validation->set_rules('investor', INVESTOR, 'required');
        $this->form_validation->set_rules('company_name', COMPANY_NAME, 'required');
        $this->form_validation->set_rules('company_url', COMPANY_URL, 'required');

		//check project url name,it should not be controller name
		 $dir = scandir(base_path() . 'application/controllers');
        // var_dump($dir);
        // die;
		$arr_controllerfiles=array();
        foreach ($dir as $file) 
		{
            if (!is_dir($file)) 
			{
                $filename=strtoupper (str_replace(".php","",$file))   ;  
				array_push($arr_controllerfiles,$filename);				
            }
        }
		$project_url=$this->input->post('project_url');
		$project_url_err="";
		if($_POST)
		{
			if(in_array(strtoupper($project_url),$arr_controllerfiles))
			{
				$project_url_err=PROJECT_URL_VALIDATION;
			}
			
		}
        if ($this->form_validation->run() == FALSE || $project_url_err!="") {
            if (validation_errors() || $project_url_err!="") {
                $data["error"] = validation_errors().$project_url_err;
            } else {
                $data["error"] = "";
            }
            if ($this->input->post('taxonomy_setting_id')) {
                $data["taxonomy_setting_id"] = $this->input->post('taxonomy_setting_id');
                $data["project_name"] = $this->input->post('project_name');
                $data["project_name_plural"] = $this->input->post('project_name_plural');
                $data["project_url"] = $this->input->post('project_url');
                $data["project_owner"] = $this->input->post('project_owner');
                $data["project_owner_plural"] = $this->input->post('project_owner_plural');
				$data["language_id"] = $this->input->post('language_id');
                $data["funds"] = $this->input->post('funds');
                $data["funds_plural"] = $this->input->post('funds_plural');
                $data["funds_past"] = $this->input->post('funds_past');
                $data["funds_gerund"] = $this->input->post('funds_gerund');

                $data["updates"] = $this->input->post('updates');
                $data["updates_plural"] = $this->input->post('updates_plural');
                $data["updates_past"] = $this->input->post('updates_past');
                $data["updates_gerund"] = $this->input->post('updates_gerund');

                $data["comments"] = $this->input->post('comments');
                $data["comments_plural"] = $this->input->post('comments_plural');
                $data["comments_past"] = $this->input->post('comments_past');
                $data["comments_gerund"] = $this->input->post('comments_gerund');

                $data["follower"] = $this->input->post('follower');
                $data["followers_plural"] = $this->input->post('follower_plural');
                $data["followers_past"] = $this->input->post('follower_past');
                $data["followers_gerund"] = $this->input->post('follower_gerund');
				
				 $data["unfollower"] = $this->input->post('unfollower');
                $data["unfollowers_plural"] = $this->input->post('unfollower_plural');
                $data["unfollowers_past"] = $this->input->post('unfollower_past');
                $data["unfollowers_gerund"] = $this->input->post('unfollower_gerund');
                
                $data["investor"] = $this->input->post('investor');

                $data["company_name"] = $this->input->post('company_name');

                $data["company_url"] = $this->input->post('company_url');
				
            } else {
                $one_taxonomy_setting = taxonomy_setting($lang_id);
                if(!empty($one_taxonomy_setting)){
				//print_r( $one_taxonomy_setting);
                $data["taxonomy_setting_id"] = $one_taxonomy_setting['taxonomy_setting_id'];
                $data["project_name"] = $one_taxonomy_setting['project_name'];
                $data["project_name_plural"] = $one_taxonomy_setting['project_name_plural'];
                $data["project_url"] = $one_taxonomy_setting['project_url'];
                $data["project_owner"] = $one_taxonomy_setting['project_owner'];
                $data["project_owner_plural"] = $one_taxonomy_setting['project_owner_plural'];
				
                $data["funds"] = $one_taxonomy_setting['funds'];
                $data["funds_plural"] = $one_taxonomy_setting['funds_plural'];
                $data["funds_past"] = $one_taxonomy_setting['funds_past'];
                $data["funds_gerund"] = $one_taxonomy_setting['funds_gerund'];

                $data["updates"] = $one_taxonomy_setting['updates'];
                $data["updates_plural"] = $one_taxonomy_setting['updates_plural'];
                $data["updates_past"] = $one_taxonomy_setting['updates_past'];
                $data["updates_gerund"] = $one_taxonomy_setting['updates_gerund'];

                $data["comments"] = $one_taxonomy_setting['comments'];
                $data["comments_plural"] = $one_taxonomy_setting['comments_plural'];
                $data["comments_past"] = $one_taxonomy_setting['comments_past'];
                $data["comments_gerund"] = $one_taxonomy_setting['comments_gerund'];

                $data["follower"] = $one_taxonomy_setting['follower'];
                $data["followers_plural"] = $one_taxonomy_setting['followers_plural'];
                $data["followers_past"] = $one_taxonomy_setting['followers_past'];
                $data["followers_gerund"] = $one_taxonomy_setting['followers_gerund'];
				 $data["unfollower"] = $one_taxonomy_setting['unfollower'];
                $data["unfollowers_plural"] = $one_taxonomy_setting['unfollowers_plural'];
                $data["unfollowers_past"] = $one_taxonomy_setting['unfollowers_past'];
                $data["unfollowers_gerund"] = $one_taxonomy_setting['unfollowers_gerund'];
                 
                $data["investor"] = $one_taxonomy_setting['investor'];

                $data["company_name"] = $one_taxonomy_setting['company_name'];

                $data["company_url"] = $one_taxonomy_setting['company_url'];

				$data["language_id"] = $one_taxonomy_setting['language_id'];
                } else { 
                    
                    $data["taxonomy_setting_id"] ='';
                    $data["project_name"] = '';
                    $data["project_name_plural"] = '';
                    $data["project_url"] = '';
                    $data["project_owner"] = '';
                    $data["project_owner_plural"] = '';

                    $data["funds"] = '';
                    $data["funds_plural"] = '';
                    $data["funds_past"] = '';
                    $data["funds_gerund"] = '';

                    $data["updates"] = '';
                    $data["updates_plural"] = '';
                    $data["updates_past"] = '';
                    $data["updates_gerund"] = '';

                    $data["comments"] = '';
                    $data["comments_plural"] = '';
                    $data["comments_past"] = '';
                    $data["comments_gerund"] = '';

                    $data["follower"] = '';
                    $data["followers_plural"] = '';
                    $data["followers_past"] = '';
                    $data["followers_gerund"] = '';
                    $data["unfollower"] = '';
                    $data["unfollowers_plural"] ='';
                    $data["unfollowers_past"] = '';
                    $data["unfollowers_gerund"] = '';
                    $data["language_id"] = $lang_id;
                    
                    $data["investor"] = '';
                    $data["company_name"] = '';
                    $data["company_url"] ='';
                    
                }
            }

        } else {

            $data1 = array(
                'project_name' => $this->input->post('project_name'),
                'project_name_plural' => $this->input->post('project_name_plural'),
                'project_url' => strtolower($this->input->post('project_url')),
                'project_owner' => $this->input->post('project_owner'),
                'project_owner_plural' => $this->input->post('project_owner_plural'),
                'funds' => $this->input->post('funds'),
                'funds_plural' => $this->input->post('funds_plural'),
                'funds_past' => $this->input->post('funds_past'),
                'funds_gerund' => $this->input->post('funds_gerund'),
                'updates' => $this->input->post('updates'),
                'updates_plural' => $this->input->post('updates_plural'),
                'updates_past' => $this->input->post('updates_past'),
                'updates_gerund' => $this->input->post('updates_gerund'),
                'comments' => $this->input->post('comments'),
                'comments_plural' => $this->input->post('comments_plural'),
                'comments_past' => $this->input->post('comments_past'),
                'comments_gerund' => $this->input->post('comments_gerund'),
                'follower' => $this->input->post('follower'),
                'followers_plural' => $this->input->post('follower_plural'),
                'followers_past' => $this->input->post('follower_past'),
                'followers_gerund' => $this->input->post('follower_gerund'),
				'unfollower' => $this->input->post('unfollower'),
                'unfollowers_plural' => $this->input->post('unfollower_plural'),
                'unfollowers_past' => $this->input->post('unfollower_past'),
                'unfollowers_gerund' => $this->input->post('unfollower_gerund'),
				'language_id' => $this->input->post('language_id'),
                 'investor' => $this->input->post('investor'),
                'company_name' => $this->input->post('company_name'),
                'company_url' => strtolower($this->input->post('company_url')),
            );
            
            
            $check_exists=$this->home_model->get_one_tabledata('taxonomy_setting',array('language_id' => $this->input->post('language_id')));
            
            if(empty($check_exists)) {            
                $this->home_model->table_insert('taxonomy_setting',$data1);
            } else {   
                $this->home_model->table_update('taxonomy_setting_id', $this->input->post('taxonomy_setting_id'), 'taxonomy_setting', $data1);
            }
            
            //update project url
			if($lang_id==1)
			{
				$sql="update taxonomy_setting set project_url='".strtolower($this->input->post('project_url'))."'  ";
				$this->db->query($sql);				
			}
			
			deletecache("taxonomy_setting".$lang_id);
			//clear all cache
			$query_lang = $this->db->query("select * from taxonomy_setting ");
			if ($query_lang->num_rows() > 0)
			{
				$query_lang_res= $query_lang->result_array();
				foreach($query_lang_res as $lang)
				{
					$taxonomy_lang_id=$lang['language_id'];
					deletecache("taxonomy_setting".$taxonomy_lang_id);
				}
			}
			
			
            $data["error"] = "success";
			$file="taxonomy_setting".$lang_id;
			if (file_exists(base_path() . 'application/cache/' . $file)) {
                //echo $file.'<br>';
                if (unlink(base_path() . 'application/cache/' . $file)) {
                    //echo 'DELETED: '.$file.'<br>';
                }
            }
			
            $filename = "taxonomy.json";
            $filepath = getcwd() . "/application/cache/" . $filename;
			if(!file_exists($filepath) or $lang_id==1)
			{
				$fp = fopen($filepath, 'w');
				fwrite($fp, json_encode($data1));
				fclose($fp);
			}
			
            

            $one_taxonomy_setting = taxonomy_setting($lang_id);
			//print_r( $one_taxonomy_setting);
            $data["taxonomy_setting_id"] = $one_taxonomy_setting['taxonomy_setting_id'];
            $data["project_name"] = $one_taxonomy_setting['project_name'];
            $data["project_name_plural"] = $one_taxonomy_setting['project_name_plural'];
            $data["project_url"] = $one_taxonomy_setting['project_url'];
            $data["project_owner"] = $one_taxonomy_setting['project_owner'];
            $data["project_owner_plural"] = $one_taxonomy_setting['project_owner_plural'];

            $data["funds"] = $one_taxonomy_setting['funds'];
            $data["funds_plural"] = $one_taxonomy_setting['funds_plural'];
            $data["funds_past"] = $one_taxonomy_setting['funds_past'];
            $data["funds_gerund"] = $one_taxonomy_setting['funds_gerund'];

            $data["updates"] = $one_taxonomy_setting['updates'];
            $data["updates_plural"] = $one_taxonomy_setting['updates_plural'];
            $data["updates_past"] = $one_taxonomy_setting['updates_past'];
            $data["updates_gerund"] = $one_taxonomy_setting['updates_gerund'];

            $data["comments"] = $one_taxonomy_setting['comments'];
            $data["comments_plural"] = $one_taxonomy_setting['comments_plural'];
            $data["comments_past"] = $one_taxonomy_setting['comments_past'];
            $data["comments_gerund"] = $one_taxonomy_setting['comments_gerund'];

            $data["follower"] = $one_taxonomy_setting['follower'];
            $data["followers_plural"] = $one_taxonomy_setting['followers_plural'];
            $data["followers_past"] = $one_taxonomy_setting['followers_past'];
            $data["followers_gerund"] = $one_taxonomy_setting['followers_gerund'];
			$data["unfollower"] = $one_taxonomy_setting['unfollower'];
            $data["unfollowers_plural"] = $one_taxonomy_setting['unfollowers_plural'];
            $data["unfollowers_past"] = $one_taxonomy_setting['unfollowers_past'];
            $data["unfollowers_gerund"] = $one_taxonomy_setting['unfollowers_gerund'];
			
			 $data["language_id"] = $one_taxonomy_setting['language_id'];
                         
                          
                $data["investor"] = $one_taxonomy_setting['investor'];

                $data["company_name"] = $one_taxonomy_setting['company_name'];

                $data["company_url"] = $one_taxonomy_setting['company_url'];

        }
        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
		
		$data['language'] = $this->home_model->get_language('language', array('active' => 1));

        $this->template->write('title', 'Administrator', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'setting/add_taxonomy_setting', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }

 /*
     Function name :add_taxonomy()
     Parameter : none.
     Return : none
     Use : this function is used to create url.
    */

    function add_property()
    {

        $data['site_setting'] = site_setting();
        $this->load->library('form_validation');

        $this->form_validation->set_rules('client_id', 'Client Id', 'required');
        $this->form_validation->set_rules('client_secret','Client Secret', 'required');
        //$this->form_validation->set_rules('access_token','Access Token', 'required');
        
     
        if ($this->form_validation->run() == FALSE) {
            if (validation_errors()) {
                $data["error"] = validation_errors();
            } else {
                $data["error"] = "";
            }
            if ($this->input->post('property_setting_id')) {
                $data["property_setting_id"] = $this->input->post('property_setting_id');
                $data["client_id"] = $this->input->post('client_id');
                $data["client_secret"] = $this->input->post('client_secret');
                // $data["access_token"] = $this->input->post('access_token');
              
                
            } else {
                $one_property_setting = property_setting();
                if(!empty($one_property_setting)){
                //print_r( $one_taxonomy_setting);
                $data["property_setting_id"] = $one_property_setting['property_setting_id'];
                $data["client_id"] = $one_property_setting['client_id'];
                $data["client_secret"] = $one_property_setting['client_secret'];
                 //$data["access_token"] = $one_property_setting['access_token'];
             

                } else { 
                    
                    $data["property_setting_id"] ='';
                    $data["client_id"] = '';
                    $data["client_secret"] = '';
                    
                }
            }

        } else {

            $data1 = array(
                'client_id' => $this->input->post('client_id'),
                'client_secret' => $this->input->post('client_secret'),
               
              
            );
            
            
            $check_exists=$this->home_model->get_one_tabledata('property_setting');
            
            if(empty($check_exists)) {            
                $this->home_model->table_insert('property_setting',$data1);
            } else {   
                $this->home_model->table_update('property_setting_id', $this->input->post('property_setting_id'), 'property_setting', $data1);
            }
            

            $one_property_setting = property_setting();
            //print_r( $one_taxonomy_setting);
            $data["property_setting_id"] = $one_property_setting['property_setting_id'];
             $data["client_id"] = $one_property_setting['client_id'];
             $data["client_secret"] = $one_property_setting['client_secret'];
            // $data["access_token"] = $one_property_setting['access_token'];
              $data["error"] = "success";
            
            

        }
        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
        
        $data['language'] = $this->home_model->get_language('language', array('active' => 1));

        $this->template->write('title', 'Administrator', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'setting/add_property_setting', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }

}
