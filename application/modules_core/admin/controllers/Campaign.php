<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Campaign extends ROCKERS_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('property_model');
        
        $this->load->model('transaction_type_model');
        $this->load->model('user_model');
        $this->load->model('admin_model');

        $this->load->model('home_model');
        
    }

    function list_campaign($msg = '')
    {
        $check_rights = get_rights('list_equity');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }
        $limit = '';
        $offset = '';
        $msg = unserialize(urldecode($msg));
        
        $data['result'] = $this->property_model->GetAllCampaigns(0, 0, '1,2,3,4,5,6,7,8', array(
            'user','property'
        ), $limit, array(
            'campaign_id' => 'desc'
        ), $offset,'','yes');

       

        $data['msg'] = $msg;

        $data['site_setting'] = site_setting();

        $data['taxonomy_setting'] = taxonomy_setting();


        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Equity List', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'equity/equity_list', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }

    function campaign_detail($property_url = '', $msg = '')
    {

        $check_rights = get_rights('list_equity');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }
        $data['msg'] = $msg;

        $data['taxonomy_setting'] = taxonomy_setting();

        $data['campaign_detail'] = $this->property_model->GetAllCampaigns(0, $property_url, 0, array('user','property'), $limit = 1000, array(
            'campaign_id' => 'desc'
        ));

        $property_id = $data['campaign_detail'][0]['property_id'];
        $user_id = $data['campaign_detail'][0]['user_id'];
        $data['updates'] = $this->property_model->GetUpdateCommentGallery(0, $property_id, '', $join = array(
                'updates'
            ), $limit = 5, $group = array(), $order = array(
                'updates.date_added' => 'desc'
            ), ''); // store in cache

       // $data['donations'] = $this->equity_model->get_donations($property_id);

       // $data['activities'] = $this->equity_model->equity_activities($property_id);


        $data['comments'] = $this->property_model->GetUpdateCommentGallery(0, $property_id, '', $join = array(
                'comment',
                'user'
            ), $limit = 5, $group = array(
                'comment_id' => 'comment.comment_id'
            ), $order = array(
                'property.property_id' => 'desc',
                'comment.comment_id' => 'desc'
            ), 1);


        $data['video_gallery'] = $this->property_model->GetUpdateCommentGallery(0, $property_id, '', $join = array(
                'video_gallery'
            ), $limit = 1000, $group = array(), $order = array(
                'video_gallery.id' => 'asc'
            ), '', '', '', '', $user_id); 


          $data['image_gallery']= $this->property_model->GetUpdateCommentGallery(0, $property_id, '', $join = array(
                'property_gallery'
            ), $limit = 1000, $group = array(), $order = array(
                'property.property_id' => 'desc'
            ), '', '', '', '', $user_id); // store in cache

        //$data['investors'] = $this->equity_model->contract_manage_data($property_id);

     
        $data['site_setting'] = site_setting();

        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Equity Detail', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'equity/equity_detail', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }
     function funder_download($equity_url='')
    {


        $check_rights = get_rights('list_transaction');


        if ($check_rights == 0) {


            redirect('admin/home/dashboard/no_rights');


        }


        $site_setting = site_setting();


        $data['equity_detail'] = $this->equity_model->GetAllEquities(0, $equity_url, 0, '', array('user'), $limit = 1000, array(
            'equity_id' => 'desc'
        ));
        $equity_id = $data['equity_detail'][0]['equity_id'];
        $investors = $this->equity_model->contract_manage_data($equity_id);
        $file = base_path()."upload/doc/test.csv";
        $fp = fopen($file,'w');  
        $header = array(NOS,
                        INVESTOR,
                        AMOUNT,
                         ACCREDIATION_STATUS,
                         INVESTMENT_STATUS,
                        TRANSACTION_ID,
                         DATE,
                        );  
     fputcsv($fp, $header);
     if($investors){
        $i = 1;

      foreach ($investors as $investor) {

                  $investor_user_name = $investor['user_name'] . ' ' . $investor['last_name'];
                    $invest_status_id = $investor['invest_status_id'];
                    $equity_id = $investor['equity_id'];
                    $investor_user_id = $investor['user_id'];
                    $in_process_id = $investor['id'];
                    $donation_amount = $investor['amount'];
                    $investor_profile_slug = $investor['profile_slug'];
                    $investor_accreditation_status = $investor['accreditation_status'];
                    $investor_date_time=$investor['transaction_date_time'];
                    $investor_image = $investor['image'];
                    if ($investor_image != '' && is_file("upload/user/user_small_image/" . $investor_image)) {
                        $investor_image_src = base_url() . 'upload/user/user_small_image/' . $investor_image;
                    } else {
                        $investor_image_src = base_url() . 'upload/user/user_small_image/no_man.jpg';
                    }
                    $preapproval_key = $investor['preapproval_key'];

                    if ($invest_status_id == 1) {

                        $status = PROCESSING_CONTRACT;

                    } elseif ($invest_status_id == 2) {

                        $status = DOWNLOADED_CONTRACT;
                    } elseif ($invest_status_id == 3) {

                        $status = UPLOADED_SIGNED_CONTRACT;
                    } elseif ($invest_status_id == 4) {

                        $status = SIGNED_CONTRACT_APPROVE;
                    } elseif ($invest_status_id == 5) {

                        $status = PAYMENT_PROCESS;
                    } elseif ($invest_status_id == 6) {

                        $status = PAYMENT_RECIEPT_UPLOADED;
                    } elseif ($invest_status_id == 7) {

                        $status = PAYMENT_CONFIRMED;
                    } elseif ($invest_status_id == 8) {

                        $status = TRACKING_SHIPMENT;
                    } elseif ($invest_status_id == 9) {

                        $status = DOCUMENT_REJECTED;
                    } elseif ($invest_status_id == 10) {

                        $status = TRACKING_SHIPMENT_CONFIRMED;
                    } else {

                        $status = TRACKING_SHIPMENT;

                    }
                     if ($investor_accreditation_status == 1) {
                                $is_accredited= ACCREDITED;
                            } else { 

                                 $is_accredited= NOT_ACCREDITED;
                             } 
                    $fp_data = array($i++,
                                    $investor_user_name,
                                    set_currency($donation_amount, $equity_id),
                                    $is_accredited,
                                    $status,
                                    $preapproval_key,
                                    date('d-M-Y',strtotime($investor_date_time)),
                                   );

                                fputcsv($fp, $fp_data);
                            }
                        }
                        fclose($fp);  
                        $this->load->helper('download');
                            $data = file_get_contents(base_url()."upload/doc/test.csv"); 
                            $name = 'Transaction Report.csv';
                            force_download($name, $data);   


    }

    function upload_contract_document($equity_id = '')

    {
        $data["msg"]["success"] = '';
        $data["msg"]["error"] = '';
        $data["contract_copy_file_error"] = '';
        if (isset($_FILES['contract_copy_file']['name'])) {
            $_FILES['userfile']['name'] = $_FILES['contract_copy_file']['name'];
            $_FILES['userfile']['type'] = $_FILES['contract_copy_file']['type'];
            $_FILES['userfile']['tmp_name'] = $_FILES['contract_copy_file']['tmp_name'];
            $image_info = getimagesize($_FILES['contract_copy_file']['tmp_name']);
            $image_width = $image_info[0];
            $image_height = $image_info[1];
            $_FILES['userfile']['error'] = $_FILES['contract_copy_file']['error'];
            $_FILES['userfile']['size'] = $_FILES['contract_copy_file']['size'];
            $_FILES['userfile']['max_width'] = $image_width;
            $_FILES['userfile']['max_height'] = $image_height;

            $type_file = explode('/', $_FILES['userfile']['type']);
            $upload_file_type = $type_file[1];
            $upload_file_type = str_replace('"', '', $upload_file_type);

            $file_type_array = array('zip', 'pdf', 'vnd.oasis.opendocument.text', 'vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'vnd.openxmlformats-officedocument.wordprocessingml.document', 'msword', 'octet-stream', 'excel', 'vnd.ms-excel', 'msexcel', 'powerpoint', 'vnd.ms-powerpoint');
            if (!in_array($upload_file_type, $file_type_array)) {
                $data["contract_copy_file_error"] = PLEASE_UPLOAD_DOC_PPT_ZIP_PDF;
                $data["msg"]["error"] = true;
                echo json_encode($data);
                die;
            } else if ($_FILES["userfile"]["size"] > 2000000) {
                $data["contract_copy_file_error"] = SORRY_THIS_FILE_IS_TOO_LARGE_PLEASE_SELECT_A_FILE_THAT_IS_LESS_THAN_TWO_MB;
                $data["msg"]["error"] = true;
                echo json_encode($data);
                die;
            } else {


                $files = $_FILES;
                $base_path = $this->config->slash_item('base_path');
                $rand = rand(0, 100000);
                $type_img = explode('/', $files['userfile']['type']);
                $new_file = $rand . $_FILES['userfile']['name'];
                move_uploaded_file($files["userfile"]["tmp_name"], $base_path . "upload/equity/investor/" . $new_file);
                $file_name = $new_file;
                $data["msg"]["success"] = 'success';
                $data["document_file"]["file_name"] = $file_name;
                $data["document_file"]["path"] = anchor(base_url() . "upload/equity/investor/" . $file_name, DOWNLOAD_CONTRACT_DOCUMENT, 'class="btn"');


            }
        }
        $data_array = array('contract_copy_file' => $file_name);

        if ($equity_id > 0) {
            $this->db->where('equity_id', $equity_id);

            $query = $this->db->update('equity', $data_array);
        }
        echo json_encode($data);

    }

    function action_campaign($id = '', $oneaction = '', $check_page = '')
    {


        $check_rights = get_rights('list_equity');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }
        $site_setting = site_setting();
        $site_name = $site_setting['site_name'];
        $taxonomy_setting = taxonomy_setting();
        $project_name = $taxonomy_setting['project_name'];
        $offset = $this->input->post('offset');
        $taxonomy_setting = taxonomy_setting();
        $taxonomy_project_url = 'properties';
        if ($this->input->post('action') == 'declined' || $this->input->post('action') == 'inactive') {
            $oneaction = $this->input->post('action');
            $id = $this->input->post('action_property_id');
        }

        if (is_array($this->input->post('chk'))) {


            $property_id = $this->input->post('chk');
            $action = $this->input->post('action');
        } else {

            $property_id = array('0' => $id);
            $action = $oneaction;
        }
        if($this->input->post('action') == 'update')
        {
            $action = $this->input->post('action');
            $property_id = $this->input->post('property_id_update');
            $check_page = 'detail';
        }

        $temp = array();
        
        switch ($action) {

            case 'delete':

                foreach ($property_id as $id) {

                    $property = GetOnePropertyCampaign('',$id);

                    $user_detail = UserData($property['user_id'], array('user_notification'));
                    $user = $user_detail[0];

                    if ($property['status'] == 2 || $property['status'] == '2' || $property['status'] == 3 || $property['status'] == '3'
                        || $property['status'] == 4 || $property['status'] == '4' || $property['status'] == 5 || $property['status'] == '5'
                    ) {

                        $temp[$property['property_url']] = 'cannot_delete_active';
                    } else {

                        /////////////============email===========	

                        //$user_not_own=$this->user_model->get_email_notification($project->user_id);


                        $email_template = $this->db->query("select * from `email_template` where task='Admin Project Delete Alert'");
                        $email_temp = $email_template->row();

                        $email_address_from = $email_temp->from_address;
                        $email_address_reply = $email_temp->reply_address;

                        $email_subject = $email_temp->subject;
                        $email_subject = str_replace('{project_name}', $project_name, $email_subject);
                        $email_subject = str_replace('{site_name}', $site_name, $email_subject);
                        $email_message = $email_temp->message;

                        $property_address = $property['property_address'];
                        $username = $user['user_name'];
                        $email = $user['email'];

                        $email_to = $email;

                        $login_id = $this->session->userdata('admin_id');
                        $admin_data = $this->db->query("select email from `admin` where admin_id=" . $login_id . "");
                        $admin_detail = $admin_data->row();

                        $admin_email = $admin_detail->email;

                        //$email_subject = str_replace('{site_name}', $site_name, $email_message);
                        //$email_message=str_replace('{site_name}', $site_name, $email_message);
                        $email_message = str_replace('{break}', '<br/>', $email_message);
                        $email_message = str_replace('{user_name}', $username, $email_message);
                        $email_message = str_replace('{company_name}', $property_address, $email_message);
                        $email_message = str_replace('{project_name}', $project_name, $email_message);
                        $email_message = str_replace('{site_name}', $site_name, $email_message);
                        $email_message = str_replace('{admin-email}', $admin_email, $email_message);

                        $str = $email_message;

                        email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);


                        //============email===========

                        project_activity_admin('project_deleted', $property['user_id'], '', $id);

                        $this->equity_model->delete_campaign($property['campaign_id']);

                        //property_deletecache($id,$project['equity_url']);

                        user_deletecache('property', $property['user_id']);

                        $temp[$property['property_url']] = 'delete';
                    }
                }
                break;

            case 'active':

                foreach ($property_id as $id) {


                    $property = GetOnePropertyCampaign('',$id);
                    //	print_r($equity);
                    $amount = $property['min_investment_amount'];


                    $user_detail = UserData($property['user_id'], array('user_notification'));
                    $user = $user_detail[0];

                    $get_reference_user_id = $user['reference_user_id'];
                    $get_user_id = $user['user_id'];


                    //if(strtotime($equity['end_date'])>=strtotime(date('Y-m-d')) && ($equity['status']==1 || $equity['status']==6))
                    if (($property['status'] == 6 || $property['status'] == 7 || $property['status'] == 8) && strtotime($property['investment_close_date']) >= strtotime(date('Y-m-d'))) {
                        /////////////============email===========
                        //$user_not_own=$this->user_model->get_email_notification($project->user_id);

                       
                        $email_template = $this->db->query("select * from `email_template` where task='Admin Project Activate Alert'");
                        $email_temp = $email_template->row();

                        $email_address_from = $email_temp->from_address;
                        $email_address_reply = $email_temp->reply_address;
                        $property_address = $property['property_address'];
                        $campaign_id = $id;
                        $equity_url = $property['property_url'];
                        if (!empty($property_address)) {

                            $str_name_url = site_url($taxonomy_project_url . '/' . $property_url);

                        } else {

                            $str_name_url = site_url($taxonomy_project_url . '/' . $campaign_id);
                        }

                        $campaign_name = anchor($str_name_url, $property_address);
                        $campaign_name_with_link = $campaign_name;
                        //$company_name_with_link='<a href="'.site_url('equity/'.$equity['equity_url'].'/'.$equity['equity_id']).'">'.site_url('equity/'.$equity['equity_url'].'/'.$equity['equity_id']).'</a>';
                        $username = $user['user_name'];
                        $email = $user['email'];

                        $login_id = $this->session->userdata('admin_id');
                        $admin_data = $this->db->query("select email from `admin` where admin_id=" . $login_id . "");
                        $admin_detail = $admin_data->row();


                        $admin_email = $admin_detail->email;
                        $email_to = $email;

                        $email_subject = $email_temp->subject;
                        $email_subject = str_replace('{project_name}', $project_name, $email_subject);
                        $email_subject = str_replace('{site_name}', $site_name, $email_subject);
                        $email_message = $email_temp->message;


                        $email_message = str_replace('{break}', '<br/>', $email_message);
                        $email_message = str_replace('{user_name}', $username, $email_message);
                        $email_message = str_replace('{equity_page_link}', $campaign_name_with_link, $email_message);
                        $email_message = str_replace('{project_name}', $project_name, $email_message);
                        $email_message = str_replace('{site_name}', $site_name, $email_message);
                        $email_message = str_replace('{admin-email}', $admin_email, $email_message);

                        $str = $email_message;

                        email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);

                        /////////////============email===========


                      
                        $this->db->query("update campaign set status=2 where campaign_id='" . $property['campaign_id'] . "'");
                       

                        project_activity_admin('project_active', $property['user_id'], '', $id);

                        $temp[$property['property_url']] = 'active';
                        $msg = $property['property_address'];
                        property_deletecache($id, $property['property_url']);
                        user_deletecache('property', $property['user_id']);

                        $prj = $property;
                    
                       

                    } else {
                        $temp[$property['property_url']] = 'cannot_active_expired';
                    }
                }
                break;

            case 'approve':

                foreach ($property_id as $id) {


                    $property = GetOnePropertyCampaign('',$id);

                    //	print_r($equity);
                    $amount = $property['min_investment_amount'];


                    $user_detail = UserData($property['user_id'], array('user_notification'));
                    $user = $user_detail[0];

                    $get_reference_user_id = $user['reference_user_id'];
                    $get_user_id = $user['user_id'];


                    //if(strtotime($equity['end_date'])>=strtotime(date('Y-m-d')) && ($equity['status']==1 || $equity['status']==6))
                    if ($property['status'] == 1 && strtotime($property['investment_close_date']) >= strtotime(date('Y-m-d'))) {
                        /////////////============email===========
                        //$user_not_own=$this->user_model->get_email_notification($project->user_id);
                       
                                $email_template = $this->db->query("select * from `email_template` where task='Admin Project Approved Alert'");
                                $email_temp = $email_template->row();

                                $email_address_from = $email_temp->from_address;
                                $email_address_reply = $email_temp->reply_address;
                                $property_address = $property['property_address'];

                                $campaign_id = $id;
                                $property_url = $property['property_url'];
                                if (!empty($property_address)) {

                                    $str_name_url = site_url($taxonomy_project_url . '/' . $property_url);

                                } else {

                                    $str_name_url = site_url($taxonomy_project_url . '/' . $campaign_id);
                                }

                                $campaign_name = anchor($str_name_url, $property_address);
                                $project_name_with_link = $campaign_name;
                                $username = $user['user_name'];
                                $email = $user['email'];

                                $login_id = $this->session->userdata('admin_id');
                                $admin_data = $this->db->query("select email from `admin` where admin_id=" . $login_id . "");
                                $admin_detail = $admin_data->row();

                                $admin_email = $admin_detail->email;
                                $email_to = $email;

                                $email_subject = $email_temp->subject;
                                $email_subject = str_replace('{project_name}', $project_name, $email_subject);
                                $email_subject = str_replace('{site_name}', $site_name, $email_subject);
                                $email_message = $email_temp->message;


                                $email_message = str_replace('{break}', '<br/>', $email_message);
                                $email_message = str_replace('{user_name}', $username, $email_message);
                                $email_message = str_replace('{equity_page_link}', $project_name_with_link, $email_message);
                                $email_message = str_replace('{project_name}', $project_name, $email_message);
                                $email_message = str_replace('{site_name}', $site_name, $email_message);

                                $str = $email_message;

                                email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);

                                /////////////============email===========


                              
                                $this->db->query("update campaign set status=2 where campaign_id='" .$property['campaign_id'] . "'");



                                $this->load->library("salesforce_api");

                                $InvestmentCloseDate = date("Y-m-d\TH:i:s.000\Z", strtotime($property['investment_close_date']));
                                $DateAdded = date("Y-m-d\TH:i:s.000\Z", strtotime($property['date_added']));

                                $require_data_array = array(                                    
                                    "CampaignId" => $property['campaign_id'],
                                    "UserId" => $property['user_id'],
                                    // "PropertyId" => "98765",
                                    "PropertyId" => $property['property_id_api'],
                                    "MinInvestmentAmount" => $property['min_investment_amount'],
                                    "AmountGet" => $property['amount_get'],
                                    "InvestmentAmountType" => $property['investment_amount_type'],
                                    "CampaignType" => $property['campaign_type'],
                                    "CampaignUnits" => $property['campaign_units'],
                                    "InvestmentCloseDate" => $InvestmentCloseDate,
                                    "DateAdded" => $DateAdded,
                                    "Status" => $property['status'],
                                );

                    
                                $salesforceSynId = $this->salesforce_api->create_new_syndicate($require_data_array);

                                $this->db->query("update campaign set salesforceSynId='".$salesforceSynId."' where campaign_id='" .$property['campaign_id'] . "'");
                                // exit;
                                
                                project_activity_admin('project_approve', $property['user_id'], '', $id);

                                $temp[$property['property_url']] = 'approve';
                                $msg = $property['project_name'];
                                property_deletecache($id, $property['property_url']);
                                user_deletecache('property', $property['user_id']);

                                $prj = $property;
                           
                        


                    } else {
                        $temp[$property['property_url']] = 'cannot_active_expired';
                    }
                }
                break;

            case 'declined':
                foreach ($property_id as $id) {

                    $property = GetOnePropertyCampaign('',$id);
                    $user_detail = UserData($property['user_id'], array('user_notification'));
                    $user = $user_detail[0];

                    if (($property['status'] == 1 || $property['status'] == '1') && strtotime($property['investment_close_date']) > strtotime(date('Y-m-d'))) {

                        $reason_decline = $this->input->post('reason_decline');
                        $save_the_reason = $this->input->post('save_the_reason');
                        $this->db->query("update campaign set status='6' , reason_decline='" . $reason_decline . "', save_the_reason='" . $save_the_reason . "' where campaign_id='" . $property['campaign_id'] . "'");

                        project_activity_admin('project_declined', $property['user_id'], '', $id);

                        $temp[$property['property_url']] = 'declined';

                        $email_template = $this->db->query("select * from `email_template` where task='Admin Project Declined Alert'");
                        $email_temp = $email_template->row();

                        $property_address = $property['property_address'];

                        $campaign_id = $id;
                        $property_url = $property['property_url'];
                        if (!empty($property_address)) {

                            $str_name_url = site_url($taxonomy_project_url . '/' . $property_url);

                        } else {

                            $str_name_url = site_url($taxonomy_project_url . '/' . $campaign_id);
                        }

                        $campaign_name = anchor($str_name_url, $property_address);
                        $project_name_with_link = $campaign_name;
                        $username = $user['user_name'];
                        $email = $user['email'];
                        $login_id = $this->session->userdata('admin_id');
                        $admin_data = $this->db->query("select email from `admin` where admin_id=" . $login_id . "");
                        $admin_detail = $admin_data->row();

                        $admin_email = $admin_detail->email;
                        $email_to = $email;


                        $email_address_from = $email_temp->from_address;
                        $email_address_reply = $email_temp->reply_address;

                        $email_subject = $email_temp->subject;
                        $email_subject = str_replace('{site_name}', $site_name, $email_subject);
                        $email_subject = str_replace('{project_name}', $project_name, $email_subject);
                        $email_message = $email_temp->message;

                        $equity_page_link = '<a href="' . site_url('equity/' . $equity['equity_url'] . '/' . $equity['equity_id']) . '">' . site_url('equity/' . $equity['equity_url'] . '/' . $equity['equity_id']) . '</a>';
                        $email_message = str_replace('{break}', '<br/>', $email_message);
                        $email_message = str_replace('{user_name}', $username, $email_message);
                        $email_message = str_replace('{project_name}', $project_name_with_link, $email_message);
                        $email_message = str_replace('{reason}', $reason_decline, $email_message);
                        $email_message = str_replace('{site_name}', $site_name, $email_message);
                        $email_message = str_replace('{admin-email}', $admin_email, $email_message);

                        $str = $email_message;

                        email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);


                    } else {

                        $temp[$property['property_url']] = 'declined_error';
                    }
                    property_deletecache($id, $property['property_address']);
                    user_deletecache('property', $property['user_id']);
                }
                break;
            case 'inactive':


                $status = $this->input->post('inactive_mode');
                if ($status == '' || $status == 0) {
                    $status = 7;
                } else {
                    $status = $this->input->post('inactive_mode');
                }
                $save_the_reason_inactive = $this->input->post('save_the_reason_inactive');

                $reason_inactive_hidden = stripcslashes($this->input->post('reason_inactive_hidden'));

                $fund_inactive_note = $this->input->post('fund_inactive_note');

                foreach ($property_id as $id) {

                    $property = GetOnePropertyCampaign('',$id);
                    $user_detail = UserData($property['user_id'], array('user_notification'));
                    $user = $user_detail[0];

                    if (($property['status'] == 2 || $property['status'] == '2') && strtotime($property['investment_close_date']) > strtotime(date('Y-m-d'))) {

                        $this->db->query("update campaign set status='" . $status . "',save_the_reason_inactive='" . $save_the_reason_inactive . "', reason_inactive_hidden='" . $reason_inactive_hidden . "' ,fund_inactive_note='" . $fund_inactive_note . "'  where property_id='" . $id . "'");

                        $temp[$property['equity_url']] = 'inactive';

                        project_activity_admin('project_inactive', $property['user_id'], '', $id);

                        $email_template = $this->db->query("select * from `email_template` where task='Admin Project deactivated Alert'");
                        $email_temp = $email_template->row();

                        $property_address = $property['property_address'];
                        $project_name_with_link = '<a href="' . site_url('properties/' . $property['property_url'] . '/' . $property['property_id']) . '">' . $property_address . '</a>';
                        $username = $user['user_name'];
                        $email = $user['email'];
                        $login_id = $this->session->userdata('admin_id');
                        $admin_data = $this->db->query("select email from `admin` where admin_id=" . $login_id . "");
                        $admin_detail = $admin_data->row();

                        $admin_email = $admin_detail->email;
                        $email_to = $email;

                        $email_address_from = $email_temp->from_address;
                        $email_address_reply = $email_temp->reply_address;

                        $email_subject = $email_temp->subject;
                        $email_subject = str_replace('{site_name}', $site_name, $email_subject);
                        $email_subject = str_replace('{project_name}', $project_name, $email_subject);
                        $email_message = $email_temp->message;

                        $property_address = $equity['property_address'];
                        $campaign_id = $id;
                        $property_url = $equity['property_url'];
                        if (!empty($property_address)) {

                            $str_name_url = site_url($taxonomy_project_url . '/' . $property_url);

                        } else {

                            $str_name_url = site_url($taxonomy_project_url . '/' . $campaign_id);
                        }

                        $campaign_name = anchor($str_name_url, $property_address);
                        $project_name_with_link = $campaign_name;
                        $email_message = str_replace('{break}', '<br/>', $email_message);
                        $email_message = str_replace('{user_name}', $username, $email_message);

                        $email_message = str_replace('{project_name}', $project_name, $email_message);
                        $email_message = str_replace('{reason}', $reason_inactive_hidden, $email_message);
                        $email_message = str_replace('{site_name}', $site_name, $email_message);
                        $email_message = str_replace('{admin-email}', $admin_email, $email_message);

                        $str = $email_message;

                        email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);

                    } else {

                        $temp[$property['property_url']] = 'cannot_inactive';
                    }
                    property_deletecache($id, $property['property_address']);
                    user_deletecache('property', $property['user_id']);
                }
                break;

           


               case 'update':
               
                    $equity = $this->GetOneEquity($equity_id);
                    
                    $this->db->query("update equity set cash_flow_status = '".$this->input->post('cash_flow_status')."' where equity_id='" . $equity_id . "'");
                   
                    $temp[$equity['equity_url']] = 'update_cash_flow';
                    property_deletecache($equity_id, $equity['equity_url']);
                    user_deletecache('equitys', $equity['user_id']);
                
                break;


        }
        if ($check_page == '') {

            redirect('admin/campaign/list_campaign/' . urlencode(serialize($temp)));
        } else {

            $url_data = each($temp);
            redirect('admin/campaign/campaign_detail/' . $url_data['key'] . '/' . $url_data['value']);
        }
    }

    /*
	Function name :GetOneEquity()
	Use : to get detail of equity by id.
	*/

    function GetOneEquity($id)
    {

        $query = $this->db->get_where('equity', array('equity_id' => $id));
        if ($query->num_rows() > 0) {

            $data = $query->row_array();
        }

        return $data;
    }

function add_dynamic_slider($id = '')
    {

        $check_rights = get_rights('add_banner_slider');
      
        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        
        $slider_image = '';
        if ($id) {
            $get_one_dynamic = $this->equity_model->get_one_dynamic($id);

            $slider_image = $get_one_dynamic['dynamic_image_image'];
        }
        
        
        $image_error = '';
        $data['error'] = '';
        
        
        
        if ($_FILES && $_POST){

            if ($_FILES['image_load']['name'] != '') {

                if ($_FILES['image_load']['tmp_name']) {
                    $needheight = 1300;
                    $needwidth = 600;
                    $fn = $_FILES['image_load']['tmp_name'];
                    $size = getimagesize($fn);
                    $actualwidth = $size[0];
                    $actualheight = $size[0];


                    if ($_FILES["image_load"]["type"] != "image/jpeg" and $_FILES["image_load"]["type"] != "image/pjpeg" and $_FILES["image_load"]["type"] != "image/png" and $_FILES["image_load"]["type"] != "image/x-png" and $_FILES["image_load"]["type"] != "image/gif") {
                        $image_error = PLEASE_UPLOAD_A_JPG_PNG_GIF_FILE;
                    } else if ($_FILES["image_load"]["size"] > 2000000) {
                        $image_error = SORRY_THIS_FILE_IS_TOO_LARGE_PLEASE_SELECT_A_JPG_FILE_THAT_IS_LESS_THAN_TWO_MB_OR_TRY_RESIZING_IT_USING_A_PHOTO_EDITOR;

                    } else if ($needwidth > $actualwidth || $needheight > $actualheight) {
                        $image_error =IMAGE_MORE_THAN_1300_600;
                    } else {

                        $image_error = '';
                    }
                } else {
                        if ($slider_image == '') {
                            $image_error = SLIDER_IMAGE_REQUIRED;
                        } else {
                            $image_error = '';
                        }   
                    }
            }else{
                if ($slider_image == '') {
                    $image_error = SLIDER_IMAGE_REQUIRED;
                } else {
                    $image_error = '';
                } 
            }
        }
        
        $this->load->library('form_validation');
        

        $data['fonts'] = $this->equity_model->fonts();
        
        
        $this->form_validation->set_rules('active', STATUS, 'required');
        
        if($_POST && $_POST['link']!=''){
            $this->form_validation->set_rules('link', BUTTON_LINK, 'valid_url');
        }
       
         if ($this->form_validation->run() == FALSE || $image_error != '') {

                if (validation_errors() || $image_error != '') {
                    $data["error"] = $image_error.validation_errors();
                } else {
                    $data["error"] = "";
                }
            

                $data["dynamic_id"] = $this->input->post('dynamic_id');
                $data["small_text"] = $this->input->post('small_text');
                $data["slider_name"] = $this->input->post('slider_name');
                $data["slider_content"] = $this->input->post('slider_content');
                $data["image_load"] = $this->input->post('image_load');
                $data["color_picker"] = $this->input->post('color_picker');
                $data["font_type_name"] = $this->input->post('font_type_name');
                $data["color_picker_content"] = $this->input->post('color_picker_content');

                $data["link"] = $this->input->post('link');
                $data["link_name"] = $this->input->post('link_name');

                $data["active"] = $this->input->post('active');


               
                
            } else {
                if ($this->input->post('dynamic_id')) {
                    $this->equity_model->dynamic_update();
                    $msg = "update";
                } else {
                   
                    $this->equity_model->dynamic_insert();
                    $msg = "insert";
                }
                redirect('admin/equity/list_dynamic_slider/' . $msg);
            }

        
        
            $data['site_setting'] = site_setting();
            $theme = 'admin';
            $this->template->set_master_template($theme . '/template.php');
            $this->template->write('title', 'Dynamic Slider', '', TRUE);
            $this->template->write_view('header', 'header', $data, TRUE);
            $this->template->write_view('main_content', 'equity/add_dynamic_slider', $data, TRUE);
            $this->template->write_view('footer', 'footer', '', TRUE);
            $this->template->render();
    }


    function valid_url($str)
    {

        $pattern = "/^(http|https|ftp):\/\/([A-Z0-9][A-Z0-9_-]*(?:\.[A-Z0-9][A-Z0-9_-]*)+):?(\d+)?\/?/i";
        if (!preg_match($pattern, $str)) {
            return '0';
        }

        return '1';
    }


    function edit_dynamic_slide($id = '')
    {

        $check_rights = get_rights('add_banner_slider');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }


        $data['fonts'] = $this->equity_model->fonts();
        $get_one_dynamic = $this->equity_model->get_one_dynamic($id);
        $data["error"] = "";
        $data["dynamic_id"] = $id;
        $data["slider_name"] = $get_one_dynamic['dynamic_image_title'];
        $data["slider_content"] = $get_one_dynamic['dynamic_image_paragraph'];
        $data["image_load"] = $get_one_dynamic['dynamic_image_image'];
        $data["color_picker"] = $get_one_dynamic['color_picker'];
        $data["font_type_name"] = $get_one_dynamic['font_type_id'];
        $data["color_picker_content"] = $get_one_dynamic['color_picker_content'];
        $data["small_text"] = $get_one_dynamic['small_text'];

        $data["link"] = $get_one_dynamic['link'];;
        $data["link_name"] = $get_one_dynamic['link_name'];

        $data["active"] = $get_one_dynamic['active'];
        //$data["offset"] = $offset;

        $data['site_setting'] = site_setting();
        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Dynamic slider', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'equity/add_dynamic_slider', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }

    function delete_dynamic_slider($id = 0)
    {
        $check_rights = get_rights('add_banner_slider');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        $this->db->delete('dynamic_slider', array('dynamic_slider_id' => $id));
        redirect('admin/equity/list_dynamic_slider/delete');
    }

    /*
	Function name :list_dynamic_slider()
	Use : list of information from dynamic slider.
	*/
    function list_dynamic_slider($msg = '')
    {

        $check_rights = get_rights('add_banner_slider');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        $data['site_setting'] = site_setting();
        //$data['limit']=$limit;
        $data['option'] = '';
        $data['keyword'] = '';
        $data['search_type'] = 'normal';


        $data['result'] = get_table_data('dynamic_slider', array('dynamic_slider_id' => 'desc'));

        $data['msg'] = $msg;
        //$data['offset'] = $offset;
        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', DYNAMIC_SLIDER, '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'equity/dynamic_slider_list', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }

    /*
	Function name :equity_contract()
	Parameter : $msg
	Return : none
	Use : update the information.
	*/

    function contract_update($msg = '')
    {
        check_admin_authentication();

        $data["error"] = "";
        $data['msg'] = '';
        $data['msg'] = $msg;

        if ($_POST) {

            $data_update = array(
                'detail' => $this->input->post('detail'),
                'update_date' => date('Y-m-d H:i:s')

            );

            $this->equity_model->equity_update('id', 1, 'equity_contract_document', $data_update);
            $msg = 'update';
            redirect('admin/equity/contract_update/' . $msg);
        }

        $equity = $this->equity_model->get_equity_tabledata('equity_contract_document', array('id' => 1));
        $data['detail'] = $equity['detail'];

        $data['site_setting'] = site_setting();
        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', CONTRACT_DOCUMENT, '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'equity/equity_contract', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }

    /*
	Function name :contract_manage()
	Parameter : none
	Return : none
	Use : list the data.
	*/

    function contract_manage($msg = '')
    {

        check_admin_authentication();

        $data['msg'] = '';
        $data['result'] = $this->equity_model->contract_manage_data();
        $data['msg'] = $msg;

        $data['site_setting'] = site_setting();
        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', CONTRACT_MANAGE, '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'equity/equity_manage', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();

    }

    /*
	Function name :detail()
	Parameter : $id
	Return : none
	Use : view the history data.
	*/

    function detail($id = '', $process_id = '')
    {
        check_admin_authentication();
        $data['site_setting'] = site_setting();
        $data['result'] = $this->equity_model->get_history('equity_investment_process_history', array('invest_status_id' => $id, 'investment_process_id' => $process_id));

        $this->load->view('admin/equity/equity_history', $data);

    }

    /*
	Function name :approve_contract()
	Parameter : $id
	Return : none
	Use :update status and fetch the data
	*/

    function approve_contract($id = '', $project_id = '', $user_id = '')
    {

        check_admin_authentication();

        $data['id'] = $id;
        $data['msg'] = '';
        $msg = '';
        $data['project_id'] = $project_id;
        $update_id = '';
        $data['user_id'] = $user_id;

        if ($_POST) {

            $project_id = $this->input->post('project_id');
            $status_id = $this->input->post('status_id');
            $user_id = $this->input->post('user_id');
            $shipment = $this->input->post('shipment');

            if ($status_id == 3) {

                $update_id = 4;

            }
            if ($status_id == 6) {

                $update_id = 7;
            }

            if ($shipment) {

                $data_update = array(
                    'invest_status_id' => $update_id,
                    'shipment' => $shipment,
                );

            } else {

                $data_update = array(
                    'invest_status_id' => $update_id
                );

            }

            $where = array(

                'invest_status_id' => $status_id,
                'equity_id' => $project_id,
            );

            $this->equity_model->equity_contract_update($where, 'equity_investment_process', $data_update);
            $msg = 'update';

            //admin send an email to user for approve 


            $where_equity = array(
                'project_id' => $project_id
            );


            $equity_data = $this->equity_model->get_equity_tabledata('project', $where_equity);


            $where_user = array(
                'user_id' => $user_id
            );

            $user_data = $this->equity_model->get_equity_tabledata('user', $where_user);

            if ($status_id == 3) {

                $email_template = $this->db->query("select * from `email_template` where task='Investmemt Document Approved'");

            }
            if ($status_id == 6) {

                $email_template = $this->db->query("select * from `email_template` where task='Payment Reciept Approved'");
            }

            $email_temp = $email_template->row();

            $email_address_from = $email_temp->from_address;
            $email_address_reply = $email_temp->reply_address;
            $email_subject = $email_temp->subject;
            $email_message = $email_temp->message;


            $username = $user_data['user_name'];
            $email = $user_data['email'];
            $email_to = $email;
            $equity_name = $equity_data['project_title'];


            $email_subject = str_replace('{equity_name}', $equity_name, $email_subject);
            $email_subject = str_replace('{status}', 'Approve', $email_subject);

            $project_link = '<a href="' . site_url('investment/investor/' . $project_id) . '">click here</a>';

            $email_message = str_replace('{break}', '<br/>', $email_message);
            $email_message = str_replace('{user_name}', $username, $email_message);
            $email_message = str_replace('{equity_name}', $equity_name, $email_message);
            $email_message = str_replace('{click_here}', $project_link, $email_message);

            $str = $email_message;

            email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);

            //send message
            $login_id = $this->session->userdata('admin_id');


            $data_insert = array(
                'sender_id' => $login_id,
                'receiver_id' => $user_id,
                'is_read' => 0,
                'message_subject' => $email_subject,
                'message_content' => $str,
                'date_added' => date('Y-m-d H:i:s'),
                'type' => $login_id,
                'project_id' => $project_id
            );

            $message_insert = $this->message_model->insert_project_profile_message($data_insert);
            $message_setting = message_setting();
            $user_id = $user_id;
            $user_detail = UserData($user_id, array());
            $login_user_detail = UserData($login_id, array());
            $message_setting->message_enable;

            //$message_user_profile_link = site_url('member/' . $user_id);

            $user_name = $user_detail[0]['user_name'];
            $message_user_name = $login_user_detail[0]['user_name'];
            $content = $str;
            $user_not_own = $this->message_model->get_email_notification($user_detail[0]['user_id']);


            if (isset($user_not_own)) {
                if (isset($user_not_own->update_alert) && $user_not_own->update_alert == '1') {
                    $user['user_name'] = $user_detail[0]['user_name'] . ' ' . $user_detail[0]['last_name'];
                    $user['email'] = $user_detail[0]['email'];
                    $user['message_user_name'] = $login_user_detail[0]['user_name'] . ' ' . $login_user_detail[0]['last_name'];
                    $user['dateadded'] = date('Y-m-d');
                    $user['subject'] = $email_subject;
                    $user['message'] = $str;
                    //$user['message_user_profile_link'] = '<a href="' . site_url('member/' . $login_user_detail[0]['user_id']) . '">' . site_url('member/' . $login_user_detail[0]['user_id']) . '</a>';
                    $this->mailalerts('user_message', '', '', $user, 'Send message');
                    $msg = 'sent';
                    echo "<script>parent.window.location.href='" . site_url('admin/equity/contract_manage/' . $msg) . "'</script>";
                }
            }
            echo "<script>parent.window.location.href='" . site_url('admin/equity/contract_manage/' . $msg) . "'</script>";

        }

        if ($id) {

            $result = $this->equity_model->get_equity_tabledata('equity_investment_process', array('invest_status_id' => $id, 'equity_id' => $project_id));
            $data['document_name'] = $result['document_name'];
            $data['acknowledge_doc'] = $result['acknowledge_doc'];

        } else {

            $data['document_name'] = '';
            $data['acknowledge_doc'] = '';
        }


        $data['site_setting'] = site_setting();
        $this->load->view('admin/equity/equity_approve', $data);
    }

    /*
	Function name :download_doc()
	Parameter : $doc_name
	Return : none
	Use :download file
	*/

    function download_doc($id = '', $doc_name = '')
    {

        check_admin_authentication();

        $this->load->helper('download');
        if ($id == 3) {

            $data = file_get_contents(base_url() . "upload/document/" . $doc_name); // Read the file's contents	

        }
        if ($id == 6) {

            $data = file_get_contents(base_url() . "upload/acknowledge/" . $doc_name); // Read the file's contents
        }

        $name = $doc_name;
        force_download($name, $data);

    }

    /*
	Function name :send_message()
	Parameter : $user_id,$equity_id
	Return : none
	Use :send a message to user
	*/

    function send_message($user_id = '', $equity_id = '')
    {

        check_admin_authentication();

        $data['errors'] = '';
        $msg = '';
        $data['user_id'] = $user_id;
        $data['equity_id'] = $equity_id;

        $login_id = $this->session->userdata('admin_id');

        $this->load->library('form_validation');
        $this->form_validation->set_rules('subject', SUBJECT, 'required');
        $this->form_validation->set_rules('comments', MESSAGES, 'required');

        if ($this->form_validation->run() == FALSE) {

            if (validation_errors()) {
                $data['errors'] = validation_errors();
            } else {
                $data['errors'] = '';
            }

        } else {

            $user_id = $this->input->post('user_id');
            $equity_id = $this->input->post('equity_id');

            $data_insert = array(
                'sender_id' => $login_id,
                'receiver_id' => $user_id,
                'is_read' => 0,
                'message_subject' => $this->input->post('subject'),
                'message_content' => $this->input->post('comments'),
                'date_added' => date('Y-m-d H:i:s'),
                'type' => $login_id,
                'project_id' => $equity_id
            );

            $message_insert = $this->message_model->insert_project_profile_message($data_insert);
            $message_setting = message_setting();
            $user_id = $user_id;
            $user_detail = UserData($user_id, array());
            $login_user_detail = UserData($login_id, array());
            $message_setting->message_enable;

            //$message_user_profile_link = site_url('member/' . $user_id);

            $user_name = $user_detail[0]['user_name'];
            $message_user_name = $login_user_detail[0]['user_name'];
            $content = $this->input->post('comments');
            $user_not_own = $this->message_model->get_email_notification($user_detail[0]['user_id']);


            if (isset($user_not_own)) {
                if (isset($user_not_own->update_alert) && $user_not_own->update_alert == '1') {
                    $user['user_name'] = $user_detail[0]['user_name'] . ' ' . $user_detail[0]['last_name'];
                    $user['email'] = $user_detail[0]['email'];
                    $user['message_user_name'] = $login_user_detail[0]['user_name'] . ' ' . $login_user_detail[0]['last_name'];
                    $user['dateadded'] = date('Y-m-d');
                    $user['subject'] = $this->input->post('subject');
                    $user['message'] = $this->input->post('comments');
                    //$user['message_user_profile_link'] = '<a href="' . site_url('member/' . $login_user_detail[0]['user_id']) . '">' . site_url('member/' . $login_user_detail[0]['user_id']) . '</a>';
                    $this->mailalerts('user_message', '', '', $user, 'Send message');
                    $msg = 'sent';
                    echo "<script>parent.window.location.href='" . site_url('admin/equity/contract_manage/' . $msg) . "'</script>";
                }
            }


        }


        $data['site_setting'] = site_setting();
        $this->load->view('admin/equity/send_message', $data);
    }

    /*
	Function name :reject_status()
	Parameter : none
	Return : none
	Use :redirect list contract manage page
	*/

    function reject_status($user_id = '', $project_id = '')
    {

        $where_equity = array(
            'project_id' => $project_id
        );

        $equity_data = $this->equity_model->get_equity_tabledata('project', $where_equity);

        $where_user = array(
            'user_id' => $user_id
        );

        $user_data = $this->equity_model->get_equity_tabledata('user', $where_user);


        $email_template = $this->db->query("select * from `email_template` where task='Document Rejected'");
        $email_temp = $email_template->row();
        $email_address_from = $email_temp->from_address;
        $email_address_reply = $email_temp->reply_address;
        $email_subject = $email_temp->subject;
        $email_message = $email_temp->message;


        $username = $user_data['user_name'];
        $email = $user_data['email'];
        $email_to = $email;
        $equity_name = $equity_data['project_title'];

        $email_subject = str_replace('{equity_name}', $equity_name, $email_subject);
        $email_subject = str_replace('{status}', 'Reject', $email_subject);


        $project_link = '<a href="' . site_url('investment/investor/' . $project_id) . '">click here</a>';

        $email_message = str_replace('{break}', '<br/>', $email_message);
        $email_message = str_replace('{user_name}', $username, $email_message);
        $email_message = str_replace('{equity_name}', $equity_name, $email_message);
        $email_message = str_replace('{click_here}', $project_link, $email_message);

        $str = $email_message;
        email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);

        $login_id = $this->session->userdata('admin_id');

        $data_insert = array(
            'sender_id' => $login_id,
            'receiver_id' => $user_id,
            'is_read' => 0,
            'message_subject' => $email_subject,
            'message_content' => $str,
            'date_added' => date('Y-m-d H:i:s'),
            'type' => $login_id,
            'project_id' => $project_id
        );

        $message_insert = $this->message_model->insert_project_profile_message($data_insert);
        $message_setting = message_setting();
        $user_id = $user_id;
        $user_detail = UserData($user_id, array());
        $login_user_detail = UserData($login_id, array());
        $message_setting->message_enable;

        //$message_user_profile_link = site_url('member/' . $user_id);

        $user_name = $user_detail[0]['user_name'];
        $message_user_name = $login_user_detail[0]['user_name'];
        $content = $str;
        $user_not_own = $this->message_model->get_email_notification($user_detail[0]['user_id']);


        if (isset($user_not_own)) {
            if (isset($user_not_own->update_alert) && $user_not_own->update_alert == '1') {
                $user['user_name'] = $user_detail[0]['user_name'] . ' ' . $user_detail[0]['last_name'];
                $user['email'] = $user_detail[0]['email'];
                $user['message_user_name'] = $login_user_detail[0]['user_name'] . ' ' . $login_user_detail[0]['last_name'];
                $user['dateadded'] = date('Y-m-d');
                $user['subject'] = $email_subject;
                $user['message'] = $str;
                //$user['message_user_profile_link'] = '<a href="' . site_url('member/' . $login_user_detail[0]['user_id']) . '">' . site_url('member/' . $login_user_detail[0]['user_id']) . '</a>';
                $this->mailalerts('user_message', '', '', $user, 'Send message');
                $msg = 'sent';
                echo "<script>parent.window.location.href='" . site_url('admin/equity/contract_manage/' . $msg) . "'</script>";
            }
        }

        echo "<script>parent.window.location.href='" . site_url('admin/equity/contract_manage') . "'</script>";

    }


     /*
    Function name :runningInvestment()
    Parameter :none
    Return : none
    Use : to show the display running investment user
   
    */
    function runningInvestment()
    {
        $data['site_setting'] = site_setting();
        $data['taxonomy_setting'] = taxonomy_setting();
        check_admin_authentication();

        
        $data['total_running_investment'] = $this->property_model->InvestorRunningProcessHistory();
        

        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', DEAL_TYPE_SETTING_LIST, '', TRUE);
        $this->template->write_view('header', 'header', '', TRUE);
        $this->template->write_view('main_content', 'equity/running_investment', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }

    
    function edit_equity($id)
    {
        
        
        $check_rights=get_rights('list_equity');
        
        if( $check_rights==0) {         
            redirect('home/dashboard/no_rights');   
        }
        
        $data['error'] = "";
        
        $equity = GetOneEquity($id);
         $user_id = $equity['user_id'];
         $user_detail = $this->user_model->get_one_user($user_id);
       
         if($this->session->userdata('user_id')){
             $this->session->unset_userdata('user_id');
         }
        if ($user_detail['active'] == '1')
                {
                    $data = array(
                        'user_id' => $user_detail['user_id'],
                        'user_name' => $user_detail['user_name'],
                        'last_name' => $user_detail['last_name'],
                        'email' => $user_detail['email'],
                    );
                    $this->session->set_userdata($data);
                    $projectControllerName=projectcontrollername();
                    redirect('start_'.$projectControllerName.'/create_step1/'.$id);
                }
        
        $data['site_setting'] = site_setting();     
        
        // $data['categorylist']=$this->project_category_model->get_category();
        // $data['statuslist']=$this->project_category_model->get_status();
        
        $this->template->write('title', 'Edit Project '.$project['project_title'], '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'edit_project', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
        
        
    }

    function savePhases($id=0){
        
        $check_rights = get_rights('list_equity');
        $data['redirect_url']='';
        if ($check_rights == 0) {
            $data['redirect_url'] = site_url('admin/home/dashboard/no_rights');
        }

        $equity = $this->GetOneEquity($id);
                    
        $insert = array(
            'main_phase'=>$this->input->post('parent_phase'),
            'sub_phase'=>$this->input->post('child_phase'),
            'cash_flow_status'=>$this->input->post('cash_flow_status')
            );
        $this->equity_model->equity_update('equity_id', $id, 'equity', $insert);
        property_deletecache($id, $equity['equity_url']);
        user_deletecache('equitys', $equity['user_id']);
        $data['success']=true;
        echo json_encode($data);
    }

  
    
}

?>
