<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Property extends ROCKERS_Controller
{

    function __construct()
    {
        parent::__construct();
         $this->load->model('user_model');
        $this->load->model('admin_model');

        $this->load->model('home_model');
        $this->load->model('property_model');
    
    }

    function suggestions(){

        $check_rights = get_rights('list_property');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }
        $this->form_validation->set_rules('suggesion_text','suggesion_text','required');

        $data['error']='';
        if($this->form_validation->run()===false){
            if(validation_errors()){
                $data['error'] = validation_errors();
            }
        }else{
            $token = $this->corelogic_auth();
            $suggestion = $this->input->post('suggesion_text');
            $response = $this->corelogiclib->get_properties_suggestion($token,$suggestion,100);
            $response = json_decode($response);

            // print_r($response);die;
            if($response){
                $suggestions = isset($response->suggestions)?$response->suggestions:'';
                if($suggestions){
                    foreach ($suggestions as $suggestion) {
                        
                        $postcode_id = $suggestion->postcodeId;
                        $locality_id = $suggestion->localityId;

                        $postcode_exists = $this->property_model->check_postcode($postcode_id);
                        if($postcode_exists===0){
                            $postcode = array(
                                'postcode'=>$postcode_id
                            );
                            $this->property_model->add_postcode($postcode);
                        }
                        
                        $locality_exists = $this->property_model->check_locality($locality_id);
                        if($locality_exists===0){
                            $locality = array(
                                'postcode_id'=>$postcode_id,
                                'localityId'=>$locality_id
                                );
                            $this->property_model->add_locality($locality);
                        }
                    }
                    redirect('admin/corelogic_api/index');
                }else{
                    if(isset($response->messages[0]->message)){
                        $data['error'] = $response->messages[0]->message;
                    }else{
                        $data['error'] = 'No records found.';
                    }
                }

            }
        }

        $data['site_setting'] = site_setting();
        $data['taxonomy_setting'] = taxonomy_setting();

        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Property List', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'corelogic_api/suggestions', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }

    function get_properties_by_postcode($postcode){

        $this->form_validation->set_rules('postcodeId','postcode_id','required');
        // $this->form_validation->set_rules('baths','bathrooms','integer');
        // $this->form_validation->set_rules('beds','bedrooms','integer');
        // $this->form_validation->set_rules('carSpaces','car spaces','integer');

        $data['postcode_id'] = $postcode;
        $data['postcode'] = $postcode;
        $data['size'] = $this->input->post('size');
        $data['page'] = $this->input->post('page');
        $data['baths'] = $this->input->post('baths');
        $data['beds'] = $this->input->post('beds');
        $data['carSpaces'] = $this->input->post('carSpaces');
        $data['landArea'] = $this->input->post('landArea');
        $data['pTypes'] = $this->input->post('pTypes');
        $data['error']='';
        $data['success']='';

        if($this->input->post()){
            $data['postcode_id'] = $this->input->post('postcodeId');
        }

        if($this->form_validation->run()===false){
            $data['error'] = validation_errors();
            
        }else{
            $token = $this->corelogic_auth();

            if($this->input->post('size'))$pass_array['size'] = $this->input->post('size');
            if($this->input->post('page'))$pass_array['page'] = $this->input->post('page');
            if($this->input->post('baths'))$pass_array['baths'] = $this->input->post('baths');
            if($this->input->post('beds'))$pass_array['beds'] = $this->input->post('beds');
            if($this->input->post('carSpaces'))$pass_array['carSpaces'] = $this->input->post('carSpaces');
            if($this->input->post('landArea'))$pass_array['landArea'] = $this->input->post('landArea');
            if($this->input->post('pTypes'))$pass_array['pTypes'] = $this->input->post('pTypes');
            $pass_array['access_token'] = $token;

            $country = $this->property_model->get_country('australia');

            $properties = $this->corelogiclib->get_properties_by_postcode_id($pass_array,$data['postcode_id']);
            $properties = json_decode($properties);
            // echo "<pre>";
            // print_r($properties);die;
            if(isset($properties->errors)){
                $data['error'] = $properties->errors[0]->msg;
            }else{
                if(isset($properties->_embedded)){
                    $properties_list = $properties->_embedded->propertySummaryList;
                    if($properties_list){
                        foreach ($properties_list as $property) {
                            $address = $property->propertySummary->address->singleLineAddress;

                            $bathrooms = $property->propertySummary->attributes->bathrooms;
                            $bedrooms = $property->propertySummary->attributes->bedrooms;
                            $carSpaces = $property->propertySummary->attributes->carSpaces;
                            $isCalculatedLandArea = $property->propertySummary->attributes->isCalculatedLandArea;
                            $landArea = $property->propertySummary->attributes->landArea;
                            $property_id = $property->propertySummary->id;

                            $localityId = $property->propertySummary->locationIdentifiers->localityId;
                            $postCodeId = $property->propertySummary->locationIdentifiers->postCodeId;
                            $streetId = $property->propertySummary->locationIdentifiers->streetId;

                            $propertySubType = $property->propertySummary->propertySubType;
                            $propertyType = $property->propertySummary->propertyType;

                            $property_url = makeSlugs($address);

                            $is_property = $this->property_model->check_property($property_id);
                            
                            if(!$is_property){

                                $insert_p = array(
                                    'property_address'=>SecurePostData($address),
                                    'property_postcode'=>SecurePostData($postCodeId),
                                    'property_address'=>SecurePostData($address),
                                    'property_url'=>$property_url,
                                    'property_id_api'=>$property_id,
                                    'locality_id'=>$localityId,
                                    'user_id'=>$this->session->userdata('admin_id'),
                                    'country_id'=>$country['country_id'],
                                    //'cover_image'=>$property_cover_image,
                                    'status'=>0,
                                    'bathrooms'=>SecurePostData($bathrooms),
                                    'bedrooms'=>SecurePostData($bedrooms),
                                    'cars'=>SecurePostData($carSpaces),
                                    'property_size' =>$landArea,
                                    'isCalculatedLandArea' =>$isCalculatedLandArea,
                                    'property_type'=>SecurePostData($propertyType),
                                    'property_sub_type'=>SecurePostData($propertySubType),
                                    'date_added'=>date('Y-m-d H:i:s')

                                );
                                $this->property_model->add_property($insert_p);
                            } else{
                                $update = array(
                                    'property_address'=>SecurePostData($address),
                                    'property_postcode'=>SecurePostData($postCodeId),
                                    'property_address'=>SecurePostData($address),
                                    'property_id_api'=>$property_id,
                                    'locality_id'=>$localityId,
                                    'country_id'=>$country['country_id'],
                                    'user_id'=>$this->session->userdata('admin_id'),
                                    'bathrooms'=>SecurePostData($bathrooms),
                                    'bedrooms'=>SecurePostData($bedrooms),
                                    'cars'=>SecurePostData($carSpaces),
                                    'property_size' =>$landArea,
                                    'isCalculatedLandArea' =>$isCalculatedLandArea,
                                    'property_type'=>SecurePostData($propertyType),
                                    'property_sub_type'=>SecurePostData($propertySubType),
                                    'updated_at'=>date('Y-m-d H:i:s')
                                );
                                $this->property_model->update_property($property_id,$update);
                            }
                        }
                        $total_pages = $properties->page->totalPages;
                    }
                    $data['success'] = "properties has been added. Please add more by changing 'page' value. There are $total_pages pages.";
                }else{
                    $data['error'] = "No properties for this postcode";
                }
                
            } 
        }

        $data['site_setting'] = site_setting();
        $data['taxonomy_setting'] = taxonomy_setting();

        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Property List', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'corelogic_api/get_property', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }

    private function corelogic_auth(){
        $check_rights = get_rights('list_property');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }
        $this->load->library('corelogiclib');

        $property_setting = property_setting();

        $client_id = $property_setting['client_id'];
        $client_secret = $property_setting['client_secret'];

        $expire_time = $property_setting['expire_time'];

        if($expire_time > date("Y-m-d H:i:s")){
            $token = $property_setting['access_token'];
        }else{
            $params = array(
                'client_id'=>$client_id,
                'client_secret'=>$client_secret,
                'grant_type'=>'client_credentials'
            );
            $res = $this->corelogiclib->create_access_token($params);

            $response = json_decode($res);
            $token = $response->access_token;
            $expires_in = $response->expires_in;

            $str = strtotime(date('Y-m-d H:i:s'));
            $str = $str+$expires_in;

            $this->db->where('property_setting_id',1);
            $this->db->update('property_setting',array('access_token'=>$token,'expire_time'=>date('Y-m-d H:i:s',$str)));
        }
        return $token;
    }

    function add_localities($post_code=0){

        $token = $this->corelogic_auth();

        $query = $this->db->query("select postcode from postcodes_geo where postcode=$post_code");
        $postcode_Data =  $query->row_array();

        if($postcode_Data){
            $prop_localities = $this->corelogiclib->get_properties_suggestion($token,$post_code,100);

            $prop_localities = json_decode($prop_localities);
            $suggestions = $prop_localities->suggestions;
        }

        if($suggestions){
            foreach ($suggestions as  $loc)
            {
                if(isset($loc->localityId ))
                {
                    $is_locality = $this->property_model->check_locality($loc->localityId);
                    if(!$is_locality){
                     $data = array(
                            'localityId' =>$loc->localityId ,
                            'postcode_id' =>$post_code
                        );
                        $this->db->insert('locality_detail', $data);
                    }
                   
                }
            }
            //update postcode
            $this->db->where('postcode',$post_code);
            $this->db->update('postcodes_geo',array('status'=>1));

            $this->session->set_flashdata('msg','Locality added successfully.');

            $pass = http_build_query(array('postcode'=>$post_code), NULL, '&' );
            redirect('admin/corelogic_api/locality_list?'.$pass);

        }else{
            $this->session->set_flashdata('msg','No data available for this postcode.');
            redirect('admin/corelogic_api/index');
        }

    }

    function add_properties($localityId){
        $token = $this->corelogic_auth();

        $query = $this->db->query("select localityId from locality_detail where localityId=$localityId");
        $locality_detail =  $query->row_array();

        if($locality_detail){
            $properties = $this->corelogiclib->get_property_from_locality($token,"locality",$localityId);

            $properties = json_decode($properties);
            $properties = $properties->records;
        }

        // echo "<pre>";
        //     print_r($properties);die;

        if($properties){
            foreach ($properties as  $property)
            {
                $propertyId = isset($property->propertyId)?$property->propertyId:'';
                $address = isset($property->address)?$property->address:'';
                $postcodeId = isset($property->postcodeId)?$property->postcodeId:'';
                $bathrooms = isset($property->propertyAttributes->bathrooms)?$property->propertyAttributes->bathrooms:'';
                $bedrooms = isset($property->propertyAttributes->bedrooms)?$property->propertyAttributes->bedrooms:'';
                $carSpaces = isset($property->propertyAttributes->carSpaces)?$property->propertyAttributes->carSpaces:'';
                $landArea =  isset($property->propertyAttributes->landArea)?$property->propertyAttributes->landArea:'';
                $floorArea =  isset($property->propertyAttributes->floorArea)?$property->propertyAttributes->floorArea:'';
                $isCalculatedLandArea =  isset($property->propertyAttributes->isCalculatedLandArea)?$property->propertyAttributes->isCalculatedLandArea:'';
                $propertyType = isset($property->propertyType)?$property->propertyType:'';
                $propertySubType = isset($property->propertySubType)?$property->propertySubType:'';
                $property_url = makeSlugs($address);

                $is_property = $this->property_model->check_property($propertyId);
                
                if(!$is_property){

                    $insert_p = array(
                        'property_address'=>SecurePostData($address),
                        'property_postcode'=>SecurePostData($postcodeId),
                        'property_address'=>SecurePostData($address),
                        'property_url'=>$property_url,
                        'property_id_api'=>$propertyId,
                        'locality_id'=>$localityId,
                        'user_id'=>$this->session->userdata('admin_id'),
                        //'cover_image'=>$property_cover_image,
                        'status'=>0,
                        'bathrooms'=>SecurePostData($bathrooms),
                        'bedrooms'=>SecurePostData($bedrooms),
                        'cars'=>SecurePostData($carSpaces),
                        'property_size' =>$landArea,
                        'lotsize' =>$floorArea,
                        'isCalculatedLandArea' =>$isCalculatedLandArea,
                        'property_type'=>SecurePostData($propertyType),
                        'property_sub_type'=>SecurePostData($propertySubType),
                        'date_added'=>date('Y-m-d H:i:s')

                    );
                    $this->property_model->add_property($insert_p);
                } else{
                    $update = array(
                        'property_address'=>SecurePostData($address),
                        'property_postcode'=>SecurePostData($postcodeId),
                        'property_address'=>SecurePostData($address),
                        'property_id_api'=>$propertyId,
                        'locality_id'=>$localityId,
                        'user_id'=>$this->session->userdata('admin_id'),
                        'bathrooms'=>SecurePostData($bathrooms),
                        'bedrooms'=>SecurePostData($bedrooms),
                        'cars'=>SecurePostData($carSpaces),
                        'property_size' =>$landArea,
                        'lotsize' =>$floorArea,
                        'isCalculatedLandArea' =>$isCalculatedLandArea,
                        'property_type'=>SecurePostData($propertyType),
                        'property_sub_type'=>SecurePostData($propertySubType),
                        'updated_at'=>date('Y-m-d H:i:s')
                    );
                    $this->property_model->update_property($propertyId,$update);
                }
            }
            //update localities
            $this->db->where('localityId',$localityId);
            $this->db->update('locality_detail',array('status'=>1));

            $this->session->set_flashdata('msg','Properties has been added successfully');
            redirect('admin/corelogic_api/property_list');
        }
        $this->session->set_flashdata('msg','No Property available for this locality.');
        redirect('admin/corelogic_api/property_list');
    }

    function add_properties_data($property_id){

        $property = $this->property_model->get_properties($property_id,array(0,1,2));
        if($property){
            $token = $this->corelogic_auth();
            $property_data = $this->corelogiclib->get_properties_data($token,$property_id,'address,coordinate,saleList,propertyPhotoList,avmDetailList');
            $property_data = json_decode($property_data);

            // echo "<pre>";
            // print_r($property_data);die;

            if($property_data){

                $property_id_table = $property['property_id'];

                $property_suburb = isset($property_data->property->address->street->locality->postcode->state)?$property_data->property->address->street->locality->postcode->state:'';

                $property_suburb_object = isset($property_data->property->address->street->locality->postcode)?$property_data->property->address->street->locality->postcode:'';

                if($property_suburb_object){
                    $postcode_id = $property_suburb_object->id;
                    $name = $property_suburb_object->name;
                    $single_line = $property_suburb_object->singleLine;
                    $state = $property_suburb_object->state;

                    $check_suburb =  $this->property_model->check_suburb($postcode_id);
                    if($check_suburb){
                        $suburb_id = $check_suburb['id'];
                    }else{
                        $insert_array = array(
                            'postcode_id'=>$postcode_id,
                            'name'=>$name,
                            'single_line'=>$single_line,
                            'state'=>$state,
                        );
                        $suburb_id = $this->property_model->add_suburb($insert_array);
                    }                    
                }

                $yearBuilt =  isset($property_data->property->attributes->yearBuilt)?$property_data->property->attributes->yearBuilt:'';

                $propertyThumbnailUrl = isset($property_data->property->propertyPhotoList[0]->largePhotoUrl)?$property_data->property->propertyPhotoList[0]->largePhotoUrl:'';

                $roofMaterial =  isset($property_data->property->attributes->roofMaterial)?$property_data->property->attributes->roofMaterial:'';

                $wallMaterial =  isset($property_data->property->attributes->wallMaterial)?$property_data->property->attributes->wallMaterial:'';

                $estimate = isset($property_data->property->avmDetailList[0]->estimate)?$property_data->property->avmDetailList[0]->estimate:'';

                $min_property_investment_range = isset($property_data->property->avmDetailList[0]->highEstimate)?$property_data->property->avmDetailList[0]->highEstimate:'';
                
                $max_property_investment_range = isset($property_data->property->avmDetailList[0]->lowEstimate)?$property_data->property->avmDetailList[0]->lowEstimate:'';

                $image_info = getimagesize($propertyThumbnailUrl);
                $mime = isset($image_info['mime'])?$image_info['mime']:'';
                if($mime){
                    $mime_array= explode('/',$image_info['mime']);
                    $ext = isset($mime_array[1])?$mime_array[1]:'';

                    $content = file_get_contents($propertyThumbnailUrl);
                    $property_img_name =$property_id.'-property.'.$ext;
                    file_put_contents(base_path() . 'upload/orig/'.$property_img_name, $content);

                    $property_cover_image = PropertyImageUpload($property_img_name);
                }

                $gallery = $this->property_model->check_property_gallery($property_id_table);
                if(!$gallery){
                    $gallery_images = isset($property_data->property->propertyPhotoList)?$property_data->property->propertyPhotoList:'';
                    if($gallery_images){
                        foreach ($gallery_images as $image){
                            $image_url = $image->largePhotoUrl;
                            $date_added = $image->scanDate;
                            $gallery_image = GalleryaddToFiles($image_url);
                            $gallery_insert = array(
                                'date_added' => date('Y-m-d h:i:s'),
                                'image' => $gallery_image,
                                'status' => 1,
                                'property_id' => $property_id_table,
                            );
                            $this->home_model->AddUpdateData('property_gallery', $gallery_insert);
                        }
                    }
                }else{
                    foreach ($gallery as $image) {
                        if(file_exists('upload/gallery/'.$image['image']))
                        {
                            unlink(base_path().'image/'.$image['image']);
                        }
                    }
                    $this->property_model->delete_gallery_images($property_id_table);

                    $gallery_images = isset($property_data->property->propertyPhotoList)?$property_data->property->propertyPhotoList:'';
                    if($gallery_images){
                        foreach ($gallery_images as $image){
                            $image_url = $image->largePhotoUrl;
                            $date_added = $image->scanDate;
                            $gallery_image = GalleryaddToFiles($image_url);
                            $gallery_insert = array(
                                'date_added' => date('Y-m-d h:i:s'),
                                'image' => $gallery_image,
                                'status' => 1,
                                'property_id' => $property_id_table,
                            );
                            $this->home_model->AddUpdateData('property_gallery', $gallery_insert);
                        }
                    }
                }

                $update_p = array(

                    'property_suburb'=>$suburb_id,
                    'year_built'=>SecurePostData($yearBuilt),
                    'cover_image' => $property_cover_image,
                    'roofMaterial'=>$roofMaterial,
                    'wallMaterial'=>$wallMaterial,
                    'status'=>2,
                    'property_id_api'=>$property_id,
                    'estimate'=>$estimate,
                    'min_property_investment_range'=>$min_property_investment_range,
                    'max_property_investment_range'=>$max_property_investment_range
                );

                $this->property_model->update_property($property_id, $update_p);
                $this->session->set_flashdata('msg','Data has been added');
                redirect('admin/corelogic_api/property_list');
            }
            $this->session->set_flashdata('msg','No Data available for this Property.');
            redirect('admin/corelogic_api/property_list');
        }
        $this->session->set_flashdata('msg',"Active properties can't be editable");
        redirect('admin/corelogic_api/property_list');
    }

    function list_property_location($msg = '')
    {

        $check_rights = get_rights('list_property');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        $property_setting = property_setting();

       

        $client_id = $property_setting['client_id'];
        $client_secret = $property_setting['client_secret'];
        
        $query = $this->db->query("select postcode from postcodes_geo group by postcode order by id asc limit 1");

        $postcode_Data =  $query->result_array();

      


        $access_token_data = $this->getAccessToken($client_id,$client_secret);

        $access_token_data =  json_decode($access_token_data);


        $access_token =  $access_token_data->access_token;

        $location_data = $this->GetLocationId($access_token);
        
            
        
        $localtion_all_data = array();

        foreach ($postcode_Data  as $key  => $postcode_sub) {
          # code...
        
            $arr = array('q' => $postcode_sub['postcode'], 'limit' => 100,'access_token'=>$access_token); 
            $location_url = http_build_query($arr);

            $url_request = 'https://property-uat-api.corelogic.asia/bsg-au/v1/suggest.json?'.$location_url;

            $location_data = file_get_contents($url_request);
        
            $location_data = json_decode($location_data);

            // echo "<pre>";
            // print_r($location_data);
           
            $localtion_all_data =  array_merge($localtion_all_data,$location_data->suggestions);

          
        }
    
         
       
        foreach ($localtion_all_data as  $value) 
        {
        
           if(isset($value->localityId ))
           {
           $data = array(
             'localityId' =>$value->localityId ,
           
            );


          $this->db->insert('locality_detail', $data); 
          }

          # code...
        }

       
    }

    function list_property_detail($limit=1,$offset =0)
    {
        $check_rights = get_rights('list_property');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        $property_setting = property_setting();



        $client_id = $property_setting['client_id'];
        $client_secret = $property_setting['client_secret'];

        $access_token_data = $this->getAccessToken($client_id,$client_secret);

        $access_token_data =  json_decode($access_token_data);

        $access_token =  $access_token_data->access_token;



        $query = $this->db->query("select localityId from locality_detail group by localityId order by localityId asc limit ".$limit." offset ".$offset." ");

        $location_id_data =  $query->result_array();

        
        $all_properties = array();

         foreach ($location_id_data  as $key  => $location_detail) 
         {
          # code...
        
            $localityId = $location_detail['localityId'];

              $url_property_fetch = "https://property-uat-api.corelogic.asia/bsg-au/v1/search/sales/locality/".$localityId."/property.json?returnFields=attributes%2CsaleList%2CpropertyPhotoList%2Caddress%2Ccoordinate%2CavmDetailList&access_token=".$access_token;

        

           $property_all_data = file_get_contents($url_property_fetch);
            $property_all_data = json_decode($property_all_data);

          
            $all_properties =  array_merge($all_properties,$property_all_data->properties);
          
          
        }

      


     
        $query = $this->db->query("select property_id_api from property_ids ");

        $property_id_data =  $query->result_array();


  
    
        foreach ($all_properties as $corelogic_properties) 
        {
           # code...



      
            if(isset($corelogic_properties->address->singleLine))
            {
                 $property_address = $corelogic_properties->address->singleLine;
            }
            else
            {
                $property_address = $corelogic_properties->address->street->singleLine;
            }


       
            $property_url = makeSlugs($property_address);
            $property_postcode  = $corelogic_properties->address->street->locality->postcode->name;
            $property_suburb  = $corelogic_properties->address->street->locality->postcode->state;
            $bathrooms = $corelogic_properties->attributes->bathrooms;
            $bedrooms = $corelogic_properties->attributes->bedrooms;
            $carSpaces = $corelogic_properties->attributes->carSpaces;
            $landArea='';
            if(isset($corelogic_properties->attributes->landArea))
            {
                $landArea =  $corelogic_properties->attributes->landArea;
            }

            $floorArea='';
            if(isset($corelogic_properties->attributes->floorArea))
            {
                $floorArea =  $corelogic_properties->attributes->floorArea;
            }

             $yearBuilt='';
            if(isset($corelogic_properties->attributes->yearBuilt))
            {
                $yearBuilt =  $corelogic_properties->attributes->yearBuilt;
            }
             $isCalculatedLandArea='';
            if(isset($corelogic_properties->attributes->isCalculatedLandArea))
            {
                $isCalculatedLandArea =  $corelogic_properties->attributes->isCalculatedLandArea;
            }
             $lockUpGarages='';
            if(isset($corelogic_properties->attributes->lockUpGarages))
            {
                $lockUpGarages =  $corelogic_properties->attributes->lockUpGarages;
            }
             $roofMaterial='';
            if(isset($corelogic_properties->attributes->roofMaterial))
            {
                $roofMaterial =  $corelogic_properties->attributes->roofMaterial;
            }
             $wallMaterial='';
            if(isset($corelogic_properties->attributes->wallMaterial))
            {
                $wallMaterial =  $corelogic_properties->attributes->wallMaterial;
            }
            
           
            $propertyType = $corelogic_properties->propertyType;
            $propertySubType = $corelogic_properties->propertySubType;
            $cover_image_name='';
            $property_cover_image='';
             if(isset($corelogic_properties->avmDetailList) && !empty($corelogic_properties->propertyPhotoList))
             {
         
                $cover_image_name = addToFiles($corelogic_properties->propertyPhotoList[0]->largePhotoUrl);
                $property_cover_image = PropertyImageUpload($cover_image_name);

                 $property_id_api = $corelogic_properties->id;
         
          
             $property_insert = array(
                'property_address' => $property_address,
                'property_url' => $property_url,
                'property_postcode' => $property_postcode,
                'property_suburb' => $property_suburb,
                'bedrooms' =>$bedrooms,
                'bathrooms' => $bathrooms,
                'cars' => $carSpaces,
                'year_built' => $yearBuilt,
                'property_type'=>$propertyType,
                'property_sub_type'=>$propertySubType,
                'cover_image' => $property_cover_image,
                'property_size' =>$landArea,
                'lotsize' =>$floorArea,
                'status'=>2,
                'isCalculatedLandArea' =>$isCalculatedLandArea,
                'roofMaterial'=>$roofMaterial,
                'isCalculatedLandArea'=>$isCalculatedLandArea,
                'wallMaterial'=>$wallMaterial,
                'property_id_api'=>$property_id_api,


              );

              if (in_array($property_id_api, $property_id_data)) 
              {

               // echo "update";die;

                  $this->home_model->AddUpdateData('property', $property_insert,$property_id_api,'property_id_api');

                  $query = $this->db->query("select property_id from property_ids where property_id_api = ".$property_id_api." ");

                  $property_ids =  $query->row_array();

                  $property_id = $property_ids['property_id'];




               }
              else
              {
                  $this->home_model->AddUpdateData('property', $property_insert);

                  $property_id = $this->db->insert_id();

                   $property_id_data = array(
                    'property_id_api' => $property_id_api,
                    'property_id'=> $property_id,
                    'status' => 0,
                    );

                    $this->home_model->AddUpdateData('property_ids', $property_id_data);
              }
            }
         
           
           


      }
        $data['result'] = array();

        $data['site_setting'] = site_setting();

        $data['taxonomy_setting'] = taxonomy_setting();


        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Equity List', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'property/property_list', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }

     function PropertyDescriptionInsert($limit=100,$offset =0)
    {


       $property_setting = property_setting();



        $client_id = $property_setting['client_id'];
        $client_secret = $property_setting['client_secret'];

        $access_token_data = $this->getAccessToken($client_id,$client_secret);

        $access_token_data =  json_decode($access_token_data);

        $access_token =  $access_token_data->access_token;

        $query = $this->db->query("select property_id_api,property_id from property_ids where status = 0 order by property_id asc limit ".$limit." offset ".$offset."");

        $property_id_data =  $query->result_array();

        

        $corelogic_properties_description = array();
       
         foreach ($property_id_data  as $key  => $property_detail) 
         {
         
        
              $property_id_api = $property_detail['property_id_api'];
              $property_id = $property_detail['property_id'];

              $url_property_fetch = "https://property-uat-api.corelogic.asia/bsg-au/v1/property/".$property_id_api."/advertisements.json?access_token=".$access_token;

                $property_all_data = file_get_contents($url_property_fetch);
                $property_all_data = json_decode($property_all_data);

               
                $new_array_push = array($property_all_data);


                 foreach ($new_array_push as $key_property => $value) 
                 {

                  $new_array_push[$key_property]->property_id = $property_id;
                 }

                $corelogic_properties_description =  array_merge($corelogic_properties_description,$new_array_push);

        }


        foreach ($corelogic_properties_description as $corelogic_properties) 
        {


           if(isset($corelogic_properties->advertisementList))
             {
              $corelogic_property_description_data = $corelogic_properties->advertisementList;

             
                $property_description = $corelogic_property_description_data[0]->advertisementDescription;
                $property_id = $corelogic_properties->property_id;


                      $update_data = array(
                        'property_description' => $property_description,
                       
                       
                      );
                      $this->home_model->AddUpdateData('property', $update_data,$property_id,'property_id');

                  
               

          }
        }

    }

    
    function PropertyGalleryInsert($limit=100,$offset =0)
    {


       $property_setting = property_setting();



        $client_id = $property_setting['client_id'];
        $client_secret = $property_setting['client_secret'];

        $access_token_data = $this->getAccessToken($client_id,$client_secret);

        $access_token_data =  json_decode($access_token_data);

        $access_token =  $access_token_data->access_token;

        $query = $this->db->query("select property_id_api,property_id from property_ids where status = 0 order by property_id asc limit ".$limit." offset ".$offset."");

        $property_id_data =  $query->result_array();

        

        $corelogic_properties_gallery = array();
       
         foreach ($property_id_data  as $key  => $property_detail) 
         {
         
        
              $property_id_api = $property_detail['property_id_api'];
              $property_id = $property_detail['property_id'];

              $url_property_fetch = "https://property-uat-api.corelogic.asia/bsg-au/v1/property/".$property_id_api.".json?returnFields=propertyPhotoList&access_token=".$access_token;

                $property_all_data = file_get_contents($url_property_fetch);
                $property_all_data = json_decode($property_all_data);
               
                $new_array_push = array($property_all_data->property);

                foreach ((array_slice($new_array_push, 0, 10)) as $key_property => $value) 
                {
                  $new_array_push[$key_property]->property_id = $property_id;
                }

                $corelogic_properties_gallery =  array_merge($corelogic_properties_gallery,$new_array_push);

        }


       
       
        foreach (array_slice($corelogic_properties_gallery, 0, 10) as $corelogic_properties) 
        {

       
           if(isset($corelogic_properties->propertyPhotoList))
             {


                $arr = $corelogic_properties->propertyPhotoList;
                $k_arr=array_keys($arr);
                rsort($arr);
                $gallery_data=array_combine($k_arr,$arr);
               

                

                foreach ($gallery_data as $key => $gallery) 
                {
                   $isDefaultPhoto = $gallery->isDefaultPhoto;
                   if($isDefaultPhoto != 1)
                   {
                      $image_url = $gallery->largePhotoUrl;
                      $date_added = $gallery->scanDate;
                      $gallery_image = GalleryaddToFiles($image_url);
                    

                      $gallery_insert = array(
                        'property_id' => $property_id,
                        'date_added' => $date_added,
                        'image' => $gallery_image,
                        'status' => 1,
                       
                      );
                      $this->home_model->AddUpdateData('property_gallery', $gallery_insert);


                      $update_status = array(
                        'status' => 1,
                       
                      );
                      $this->home_model->AddUpdateData('property_ids', $update_status,$property_id,'property_id');

                  }
                }

            }
        }

    }


    function PropertyInvestmentInsert($limit=100,$offset =0)
    {
      
      $property_setting = property_setting();
        $client_id = $property_setting['client_id'];
        $client_secret = $property_setting['client_secret'];

        $access_token_data = $this->getAccessToken($client_id,$client_secret);

        $access_token_data =  json_decode($access_token_data);

        $access_token =  $access_token_data->access_token;

        $query = $this->db->query("select property_id_api,property_id from property_ids where status in (0,1) order by property_id asc limit ".$limit." offset ".$offset." ");

        $property_id_data =  $query->result_array();



        $corelogic_properties_investment = array();
       
         foreach ($property_id_data  as $key  => $property_detail) 
         {
         
        
              $property_id_api = $property_detail['property_id_api'];
              $property_id = $property_detail['property_id'];

              $url_property_fetch = "https://property-uat-api.corelogic.asia/bsg-au/v1/property/".$property_id_api.".json?returnFields=avmDetailList&access_token=".$access_token;

                $property_all_data = file_get_contents($url_property_fetch);
                $property_all_data = json_decode($property_all_data);
               
                $new_array_push = array($property_all_data->property);

                foreach ($new_array_push as $key_property => $value) 
                {
                  $new_array_push[$key_property]->property_id = $property_id;
                }

                $corelogic_properties_investment =  array_merge($corelogic_properties_investment,$new_array_push);

        }
        // echo "<pre>";
        //   print_r($corelogic_properties_investment);die;

        foreach ($corelogic_properties_investment as $corelogic_properties) 
        {

           if(isset($corelogic_properties->avmDetailList))
             {
               $investment_data = $corelogic_properties->avmDetailList;
               $investment_property_id = $corelogic_properties->property_id;
                foreach ($investment_data as $key => $investment) 
                {
                    $estimate = $investment->estimate;
                    $fsd = $investment->fsd;
                    $highEstimate = $investment->highEstimate;
                    $score = $investment->score;
                    $source = $investment->source;
                    $valuationDate = $investment->valuationDate;
                    $lowEstimate = $investment->lowEstimate;
                    

                    $investment_insert = array(
                      'property_id' => $investment_property_id,
                      'estimate' => $estimate,
                      'fsd' => $fsd,
                      'highEstimate' => $highEstimate,
                      'lowEstimate' => $lowEstimate,
                      'score' => $score,
                      'source' => $source,
                      'valuationDate' => $valuationDate,
                     
                    );
                    $this->home_model->AddUpdateData('property_investment', $investment_insert);




                    $property_investment_data = GetPropertyInvestmentRange($investment_property_id);
                    $min_property_investment_range = $property_investment_data['lowEstimate'];
                    $max_property_investment_range = $property_investment_data['highEstimate'];
                    $property_investment = array(
                          'property_id' => $investment_property_id,
                          'estimate' => $property_investment_data['estimate'], 
                          'max_property_investment_range' => $max_property_investment_range,
                          'min_property_investment_range' => $min_property_investment_range,
                      
                         
                        );
                        $this->home_model->AddUpdateData('property', $property_investment,$investment_property_id,'property_id');

                }
          }
        }


    }

    function test(){

        $this->load->library('corelogiclib');

        $property_setting = property_setting();

        $client_id = $property_setting['client_id'];
        $client_secret = $property_setting['client_secret'];

        $expire_time = $property_setting['expire_time'];

        if($expire_time > date("Y-m-d h:i:s")){
            $token = $property_setting['access_token'];
        }else{
            $params = array(
                'client_id'=>$client_id,
                'client_secret'=>$client_secret,
                'grant_type'=>'client_credentials'
            );
            $res = $this->corelogiclib->create_access_token($params);

            $response = json_decode($res);
            $token = $response->access_token;
            $expires_in = $response->expires_in;

            $str = strtotime(date('Y-m-d H:i:s'));
            $str = $str+$expires_in;

            $this->db->where('property_setting_id',1);
            $this->db->update('property_setting',array('access_token'=>$token,'expire_time'=>date('Y-m-d H:i:s',$str)));
        }

        $prop_l = $this->corelogiclib->search_peroperties_by_lat_long($token,22,77);

        //print_r(json_decode($prop_l));

        $prop_s = $this->corelogiclib->get_properties_suggestion($token,113,30);

        //print_r(json_decode($prop_s));

        $properties = $this->corelogiclib->get_property_from_locality($token,"locality",22677);
        echo "<pre>";
        print_r(json_decode($properties));

    }

    function getProperties($vars_p){
        // create curl resource
        $ch = curl_init();

        // set url
        curl_setopt($ch, CURLOPT_URL, "https://property-sandbox-api.corelogic.asia/".$vars_p);


        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // $output contains the output string
        $output = curl_exec($ch);

        print_r($output);

        // close curl resource to free up system resources
        curl_close($ch);

        return $output;
    }

    function getAccessToken_($url=''){
        // create curl resource
        $ch = curl_init();

        // set url
        curl_setopt($ch, CURLOPT_URL, "https://access-api.corelogic.asia/".$url);


        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // $output contains the output string
        $output = curl_exec($ch);

        // close curl resource to free up system resources
        curl_close($ch);

        return $output;
    }

    function getAccessToken($client_id,$client_secret)
    {

       
      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://access-uat-api.corelogic.asia/access/oauth/token?grant_type=client_credentials&client_id=".$client_id."&client_secret=".$client_secret,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
          "cache-control: no-cache",
          "content-type: application/json",
          "postman-token: a094afb5-29f3-a8c1-5478-49fd0ff360c5"
        ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
        echo "cURL Error #:" . $err;
      } else {
        $response;;
      }

      return $response;
    }
     function getPropertyData($access_token)
    {

       
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://access-uat.corelogic.asia/access/oauth/token?grant_type=client_credentials&client_id=".$client_id."&client_secret=".$client_secret,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "content-type: application/json",
            "postman-token: a094afb5-29f3-a8c1-5478-49fd0ff360c5"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
          $response;;
        }

        return $response;
    }

    function GetLocationId($access_token)
    {
     

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://property-uat-api.corelogic.asia/bsg-au/v1/suggest.json?q=1 Aardvark Street Adelaide SA 5000&access_token=".$access_token,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_SSLVERSION => 6,
          CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "content-type: application/json",
            "postman-token: a094afb5-29f3-a8c1-5478-49fd0ff360c5"
          ),
        ));




        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
          $response;;
        }

        return $response;
      }

    function list_property($msg = '')
    {


       $check_rights = get_rights('list_property');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }
        $limit = '';
        $offset = '';
        $msg = unserialize(urldecode($msg));
        
        $data['result'] = $this->property_model->GetAllUserProperties(0, 0, '1,2,3,4,5,6,7,8', array(
            'user'
        ), $limit, array(
            'property_id' => 'desc'
        ));



        $data['msg'] = $msg;

        $data['site_setting'] = site_setting();

        $data['taxonomy_setting'] = taxonomy_setting();


        $theme = 'admin';
        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Property List', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'property/property_list', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
 
    }

   function action_property($id = '', $oneaction = '', $check_page = '')
    {


        $check_rights = get_rights('list_equity');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }
        $site_setting = site_setting();
        $site_name = $site_setting['site_name'];
        $taxonomy_setting = taxonomy_setting();
        $project_name = $taxonomy_setting['project_name'];
        $offset = $this->input->post('offset');
        $taxonomy_setting = taxonomy_setting();
        $taxonomy_project_url = 'properties';
        if ($this->input->post('action') == 'declined' || $this->input->post('action') == 'inactive') {
            $oneaction = $this->input->post('action');
            $id = $this->input->post('action_property_id');
        }

        if (is_array($this->input->post('chk'))) {


            $property_id = $this->input->post('chk');
            $action = $this->input->post('action');
        } else {

            $property_id = array('0' => $id);
            $action = $oneaction;
        }
        if($this->input->post('action') == 'update')
        {
            $action = $this->input->post('action');
            $property_id = $this->input->post('property_id_update');
            $check_page = 'detail';
        }

        $temp = array();
        
        switch ($action) {

            case 'delete':

                foreach ($property_id as $id) {

                    $property = GetOneProperty($id);

                    $user_detail = UserData($property['user_id'], array('user_notification'));
                    $user = $user_detail[0];

                    if ($property['status'] == 2 || $property['status'] == '2' || $property['status'] == 3 || $property['status'] == '3'
                        || $property['status'] == 4 || $property['status'] == '4' || $property['status'] == 5 || $property['status'] == '5'
                    ) {

                        $temp[$property['property_url']] = 'cannot_delete_active';
                    } else {

                        /////////////============email=========== 

                        //$user_not_own=$this->user_model->get_email_notification($project->user_id);


                        $email_template = $this->db->query("select * from `email_template` where task='Admin Project Delete Alert'");
                        $email_temp = $email_template->row();

                        $email_address_from = $email_temp->from_address;
                        $email_address_reply = $email_temp->reply_address;

                        $email_subject = $email_temp->subject;
                        $email_subject = str_replace('{project_name}', $project_name, $email_subject);
                        $email_subject = str_replace('{site_name}', $site_name, $email_subject);
                        $email_message = $email_temp->message;

                        $property_address = $property['property_address'];
                        $username = $user['user_name'];
                        $email = $user['email'];

                        $email_to = $email;

                        $login_id = $this->session->userdata('admin_id');
                        $admin_data = $this->db->query("select email from `admin` where admin_id=" . $login_id . "");
                        $admin_detail = $admin_data->row();

                        $admin_email = $admin_detail->email;

                        //$email_subject = str_replace('{site_name}', $site_name, $email_message);
                        //$email_message=str_replace('{site_name}', $site_name, $email_message);
                        $email_message = str_replace('{break}', '<br/>', $email_message);
                        $email_message = str_replace('{user_name}', $username, $email_message);
                        $email_message = str_replace('{company_name}', $property_address, $email_message);
                        $email_message = str_replace('{project_name}', $project_name, $email_message);
                        $email_message = str_replace('{site_name}', $site_name, $email_message);
                        $email_message = str_replace('{admin-email}', $admin_email, $email_message);

                        $str = $email_message;

                        email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);


                        //============email===========

                        project_activity_admin('project_deleted', $property['user_id'], '', $id);

                        $this->equity_model->delete_campaign($property['campaign_id']);

                        //property_deletecache($id,$project['equity_url']);

                        user_deletecache('property', $property['user_id']);

                        $temp[$property['property_url']] = 'delete';
                    }
                }
                break;

            case 'active':

                foreach ($property_id as $id) {


                    $property = GetOneProperty($id);
                  
                    $user_detail = UserData($property['user_id'], array('user_notification'));
                    $user = $user_detail[0];

                    $get_reference_user_id = $user['reference_user_id'];
                    $get_user_id = $user['user_id'];


                    //if(strtotime($equity['end_date'])>=strtotime(date('Y-m-d')) && ($equity['status']==1 || $equity['status']==6))
                    if (($property['status'] == 6 || $property['status'] == 7 || $property['status'] == 8) ) {
                        /////////////============email===========
                        //$user_not_own=$this->user_model->get_email_notification($project->user_id);

                       
                        $email_template = $this->db->query("select * from `email_template` where task='Admin Project Activate Alert'");
                        $email_temp = $email_template->row();

                        $email_address_from = $email_temp->from_address;
                        $email_address_reply = $email_temp->reply_address;
                        $property_address = $property['property_address'];
                        $campaign_id = $id;
                        $equity_url = $property['property_url'];
                        if (!empty($property_address)) {

                            $str_name_url = site_url($taxonomy_project_url . '/' . $property_url);

                        } else {

                            $str_name_url = site_url($taxonomy_project_url . '/' . $campaign_id);
                        }

                        $campaign_name = anchor($str_name_url, $property_address);
                        $campaign_name_with_link = $campaign_name;
                        //$company_name_with_link='<a href="'.site_url('equity/'.$equity['equity_url'].'/'.$equity['equity_id']).'">'.site_url('equity/'.$equity['equity_url'].'/'.$equity['equity_id']).'</a>';
                        $username = $user['user_name'];
                        $email = $user['email'];

                        $login_id = $this->session->userdata('admin_id');
                        $admin_data = $this->db->query("select email from `admin` where admin_id=" . $login_id . "");
                        $admin_detail = $admin_data->row();


                        $admin_email = $admin_detail->email;
                        $email_to = $email;

                        $email_subject = $email_temp->subject;
                        $email_subject = str_replace('{project_name}', $project_name, $email_subject);
                        $email_subject = str_replace('{site_name}', $site_name, $email_subject);
                        $email_message = $email_temp->message;


                        $email_message = str_replace('{break}', '<br/>', $email_message);
                        $email_message = str_replace('{user_name}', $username, $email_message);
                        $email_message = str_replace('{equity_page_link}', $campaign_name_with_link, $email_message);
                        $email_message = str_replace('{project_name}', $project_name, $email_message);
                        $email_message = str_replace('{site_name}', $site_name, $email_message);
                        $email_message = str_replace('{admin-email}', $admin_email, $email_message);

                        $str = $email_message;

                        email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);

                        /////////////============email===========


                      
                        $this->db->query("update property set status=2 where property_id='" . $id . "'");
                       

                        project_activity_admin('project_active', $property['user_id'], '', $id);

                        $temp[$property['property_url']] = 'active';
                        $msg = $property['property_address'];
                        property_deletecache($id, $property['property_url']);
                        user_deletecache('property', $property['user_id']);

                        $prj = $property;
                    
                       

                    } else {
                        $temp[$property['property_url']] = 'cannot_active_expired';
                    }
                }
                break;

            case 'approve':

                foreach ($property_id as $id) {


                    $property = GetOneProperty($id);

                  

                    $user_detail = UserData($property['user_id'], array('user_notification'));
                    $user = $user_detail[0];

                    $get_reference_user_id = $user['reference_user_id'];
                    $get_user_id = $user['user_id'];


                    //if(strtotime($equity['end_date'])>=strtotime(date('Y-m-d')) && ($equity['status']==1 || $equity['status']==6))
                  
                    if ($property['status'] == 1) {

                        /////////////============email===========
                        //$user_not_own=$this->user_model->get_email_notification($project->user_id);
                       
                                $email_template = $this->db->query("select * from `email_template` where task='Admin Project Approved Alert'");
                                $email_temp = $email_template->row();

                                $email_address_from = $email_temp->from_address;
                                $email_address_reply = $email_temp->reply_address;
                                $property_address = $equity['property_address'];

                                $campaign_id = $id;
                                $property_url = $property['property_url'];
                                if (!empty($property_address)) {

                                    $str_name_url = site_url($taxonomy_project_url . '/' . $property_url);

                                } else {

                                    $str_name_url = site_url($taxonomy_project_url . '/' . $campaign_id);
                                }

                                $campaign_name = anchor($str_name_url, $property_address);
                                $project_name_with_link = $campaign_name;
                                $username = $user['user_name'];
                                $email = $user['email'];

                                $login_id = $this->session->userdata('admin_id');
                                $admin_data = $this->db->query("select email from `admin` where admin_id=" . $login_id . "");
                                $admin_detail = $admin_data->row();

                                $admin_email = $admin_detail->email;
                                $email_to = $email;

                                $email_subject = $email_temp->subject;
                                $email_subject = str_replace('{project_name}', $project_name, $email_subject);
                                $email_subject = str_replace('{site_name}', $site_name, $email_subject);
                                $email_message = $email_temp->message;


                                $email_message = str_replace('{break}', '<br/>', $email_message);
                                $email_message = str_replace('{user_name}', $username, $email_message);
                                $email_message = str_replace('{equity_page_link}', $project_name_with_link, $email_message);
                                $email_message = str_replace('{project_name}', $project_name, $email_message);
                                $email_message = str_replace('{site_name}', $site_name, $email_message);

                                $str = $email_message;

                                email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);

                                /////////////============email===========


                              
                                $this->db->query("update property set status=2 where property_id='" .$id . "'");
                                
                                project_activity_admin('project_approve', $property['user_id'], '', $id);

                                $temp[$property['property_url']] = 'approve';
                                $msg = $equity['project_name'];
                                property_deletecache($id, $property['property_url']);
                                user_deletecache('property', $property['user_id']);

                                $prj = $property;
                           
                        


                    } else {
                        $temp[$property['property_url']] = 'cannot_active_expired';
                    }
                }
                break;

            case 'declined':
                foreach ($property_id as $id) {

                    $property = GetOneProperty($id);
                    $user_detail = UserData($property['user_id'], array('user_notification'));
                    $user = $user_detail[0];

                    if (($property['status'] == 1 || $property['status'] == '1')) {

                        
                        $this->db->query("update property set status='6'  where property_id='" . $id . "'");



                        project_activity_admin('project_declined', $property['user_id'], '', $id);

                        $temp[$property['property_url']] = 'declined';

                        $email_template = $this->db->query("select * from `email_template` where task='Admin Project Declined Alert'");
                        $email_temp = $email_template->row();

                        $property_address = $property['property_address'];

                        $campaign_id = $id;
                        $property_url = $property['property_url'];
                        if (!empty($property_address)) {

                            $str_name_url = site_url($taxonomy_project_url . '/' . $property_url);

                        } else {

                            $str_name_url = site_url($taxonomy_project_url . '/' . $campaign_id);
                        }

                        $campaign_name = anchor($str_name_url, $property_address);
                        $project_name_with_link = $campaign_name;
                        $username = $user['user_name'];
                        $email = $user['email'];
                        $login_id = $this->session->userdata('admin_id');
                        $admin_data = $this->db->query("select email from `admin` where admin_id=" . $login_id . "");
                        $admin_detail = $admin_data->row();

                        $admin_email = $admin_detail->email;
                        $email_to = $email;


                        $email_address_from = $email_temp->from_address;
                        $email_address_reply = $email_temp->reply_address;

                        $email_subject = $email_temp->subject;
                        $email_subject = str_replace('{site_name}', $site_name, $email_subject);
                        $email_subject = str_replace('{project_name}', $project_name, $email_subject);
                        $email_message = $email_temp->message;

                        $equity_page_link = '<a href="' . site_url('equity/' . $equity['equity_url'] . '/' . $equity['equity_id']) . '">' . site_url('equity/' . $equity['equity_url'] . '/' . $equity['equity_id']) . '</a>';
                        $email_message = str_replace('{break}', '<br/>', $email_message);
                        $email_message = str_replace('{user_name}', $username, $email_message);
                        $email_message = str_replace('{project_name}', $project_name_with_link, $email_message);
                        $email_message = str_replace('{reason}', $reason_decline, $email_message);
                        $email_message = str_replace('{site_name}', $site_name, $email_message);
                        $email_message = str_replace('{admin-email}', $admin_email, $email_message);

                        $str = $email_message;

                        email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);


                    } else {

                        $temp[$property['property_url']] = 'declined_error';
                    }
                    property_deletecache($id, $property['property_address']);
                    user_deletecache('property', $property['user_id']);
                }
                break;
            case 'inactive':


                $status = $this->input->post('inactive_mode');
                if ($status == '' || $status == 0) {
                    $status = 7;
                } else {
                    $status = $this->input->post('inactive_mode');
                }
               

                foreach ($property_id as $id) {

                    $property = GetOneProperty($id);
                    $user_detail = UserData($property['user_id'], array('user_notification'));
                    $user = $user_detail[0];

                    if (($property['status'] == 2 || $property['status'] == '2') ) {

                        $this->db->query("update property set status='" . $status . "'  where property_id='" . $id . "'");



                        $temp[$property['equity_url']] = 'inactive';

                        project_activity_admin('project_inactive', $property['user_id'], '', $id);

                        $email_template = $this->db->query("select * from `email_template` where task='Admin Project deactivated Alert'");
                        $email_temp = $email_template->row();

                        $property_address = $property['property_address'];
                        $project_name_with_link = '<a href="' . site_url('properties/' . $property['property_url'] . '/' . $property['property_id']) . '">' . $property_address . '</a>';
                        $username = $user['user_name'];
                        $email = $user['email'];
                        $login_id = $this->session->userdata('admin_id');
                        $admin_data = $this->db->query("select email from `admin` where admin_id=" . $login_id . "");
                        $admin_detail = $admin_data->row();

                        $admin_email = $admin_detail->email;
                        $email_to = $email;

                        $email_address_from = $email_temp->from_address;
                        $email_address_reply = $email_temp->reply_address;

                        $email_subject = $email_temp->subject;
                        $email_subject = str_replace('{site_name}', $site_name, $email_subject);
                        $email_subject = str_replace('{project_name}', $project_name, $email_subject);
                        $email_message = $email_temp->message;

                        $property_address = $equity['property_address'];
                        $campaign_id = $id;
                        $property_url = $equity['property_url'];
                        if (!empty($property_address)) {

                            $str_name_url = site_url($taxonomy_project_url . '/' . $property_url);

                        } else {

                            $str_name_url = site_url($taxonomy_project_url . '/' . $campaign_id);
                        }

                        $campaign_name = anchor($str_name_url, $property_address);
                        $project_name_with_link = $campaign_name;
                        $email_message = str_replace('{break}', '<br/>', $email_message);
                        $email_message = str_replace('{user_name}', $username, $email_message);

                        $email_message = str_replace('{project_name}', $project_name, $email_message);
                        $email_message = str_replace('{reason}', $reason_inactive_hidden, $email_message);
                        $email_message = str_replace('{site_name}', $site_name, $email_message);
                        $email_message = str_replace('{admin-email}', $admin_email, $email_message);

                        $str = $email_message;

                        email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);

                    } else {

                        $temp[$property['property_url']] = 'cannot_inactive';
                    }
                    property_deletecache($id, $property['property_address']);
                    user_deletecache('property', $property['user_id']);
                }
                break;

        }
        if ($check_page == '') {

            redirect('admin/property/list_property/' . urlencode(serialize($temp)));
        } else {

            $url_data = each($temp);
            redirect('admin/campaign/campaign_detail/' . $url_data['key'] . '/' . $url_data['value']);
        }
    }


    function edit_property($id)
    {
        $check_rights=get_rights('list_equity');
        
        if( $check_rights==0) {         
            redirect('home/dashboard/no_rights');   
        }
        
        $data['error'] = "";
        
        $property = GetOneProperty($id);
         $user_id = $property['user_id'];
         $user_detail = $this->user_model->get_one_user($user_id);
       
         if($this->session->userdata('user_id')){
             $this->session->unset_userdata('user_id');
         }
        if ($user_detail['active'] == '1')
        {
            $data = array(
                'user_id' => $user_detail['user_id'],
                'user_name' => $user_detail['user_name'],
                'last_name' => $user_detail['last_name'],
                'email' => $user_detail['email'],
            );
            $this->session->set_userdata($data);
            $projectControllerName=projectcontrollername();
            redirect('property/add_property/'.$id);
        }else{

            $admin = $this->home_model->get_one_tabledata('admin', array('admin_id' => $this->session->userdata('admin_id')));

            redirect('home/adminlog/'.base64_encode($admin['email']).'/'.$id);
        }
        
        // $data['site_setting'] = site_setting();     
        
        // // $data['categorylist']=$this->project_category_model->get_category();
        // // $data['statuslist']=$this->project_category_model->get_status();
        
        // // $this->template->write('title', 'Edit Project '.$project['project_title'], '', TRUE);
        // $this->template->write_view('header', 'header', $data, TRUE);
        // $this->template->write_view('main_content', 'edit_project', $data, TRUE);
        // $this->template->write_view('footer', 'footer', '', TRUE);
        // $this->template->render();
        
        
    }

    function docusignv2(){
        $docusign_setting = docusign_setting();
        $params = array(
            'username'=>$docusign_setting['username'],
            'password'=>$docusign_setting['password'],
            'integrator_key'=>$docusign_setting['integrator_key'],
            'host'=>$docusign_setting['host']
        );
        $this->load->library('docusign',$params);

        $apiClient = $this->docusign->initialize();

        $login = $this->docusign->login($apiClient);
        $login = json_decode($login);

        if($login)
        {
            $loginAccounts = $login->loginAccounts[0];
            $accountId = $loginAccounts->accountId;
        }

        // set recipient information
        $recipientName = "Rahul Patel";
        $recipientEmail = "rahul.rockersinfo@gmail.com";

        // configure the document we want signed
        $documentFileName = base_path()."upload/Test.pdf";
        $documentName = "Test.pdf";

        $envelop = $this->docusign->send_envelope($apiClient,$recipientName,$recipientEmail,$documentFileName,$documentName,$accountId);
        $envelop = json_decode($envelop);
        echo "<pre>";
        print_r($envelop);
        $sign_url = $this->docusign->sign_url($apiClient,$recipientName,$recipientEmail,$accountId,$envelop->envelopeId);
        print_r($sign_url);die;
    }
    function docusign(){

        require_once(APPPATH.'libraries/docusign-php-client/autoload.php');

        // DocuSign account credentials & Integrator Key
        $username = "dev1.rockersinfo@gmail.com";
        $password = "rockers123";
        $integrator_key = "REAL-b9d3f2bd-e3c4-4464-a203-5e77563d955a";
        $host = "https://demo.docusign.net/restapi";

        // create a new DocuSign configuration and assign host and header(s)
        $config = new DocuSign\eSign\Configuration();
        $config->setHost($host);
        $config->addDefaultHeader("X-DocuSign-Authentication", "{\"Username\":\"" . $username . "\",\"Password\":\"" . $password . "\",\"IntegratorKey\":\"" . $integrator_key . "\"}");

        /////////////////////////////////////////////////////////////////////////
        // STEP 1:  Login() API
        /////////////////////////////////////////////////////////////////////////

        // instantiate a new docusign api client
        $apiClient = new DocuSign\eSign\ApiClient($config);

        // we will first make the Login() call which exists in the AuthenticationApi...
        $authenticationApi = new DocuSign\eSign\Api\AuthenticationApi($apiClient);

        // optional login parameters
        $options = new \DocuSign\eSign\Api\AuthenticationApi\LoginOptions();

        // call the login() API
        $loginInformation = $authenticationApi->login($options);

        // parse the login results
        if(isset($loginInformation) && count($loginInformation) > 0)
        {
            // note: defaulting to first account found, user might be a
            // member of multiple accounts
            $loginAccount = $loginInformation->getLoginAccounts()[0];
            if(isset($loginInformation))
            {
                $accountId = $loginAccount->getAccountId();
                if(!empty($accountId))
                {
                    echo "Account ID = $accountId\n";
                }
            }
        }


        /////////////////////////////////////////////////////////////////////////
        // STEP 2:  Create & Send Envelope with Embedded Recipient
        /////////////////////////////////////////////////////////////////////////

        // set recipient information
        $recipientName = "Rahul Patel";
        $recipientEmail = "rahul.rockersinfo@gmail.com";

        // configure the document we want signed
        $documentFileName = base_path()."upload/Test.pdf";
        $documentName = "Test.pdf";

        // instantiate a new envelopeApi object
        $envelopeApi = new DocuSign\eSign\Api\EnvelopesApi($apiClient);

        // Add a document to the envelope
        $document = new DocuSign\eSign\Model\Document();
        $document->setDocumentBase64(base64_encode(file_get_contents($documentFileName)));
        $document->setName($documentName);
        $document->setDocumentId("1");

        // Create a |SignHere| tab somewhere on the document for the recipient to sign
        $signHere = new \DocuSign\eSign\Model\SignHere();
        $signHere->setXPosition("100");
        $signHere->setYPosition("100");
        $signHere->setDocumentId("1");
        $signHere->setPageNumber("1");
        $signHere->setRecipientId("1");

        // add the signature tab to the envelope's list of tabs
        $tabs = new DocuSign\eSign\Model\Tabs();
        $tabs->setSignHereTabs(array($signHere));

        // add a signer to the envelope
        $signer = new \DocuSign\eSign\Model\Signer();
        $signer->setEmail($recipientEmail);
        $signer->setName($recipientName);
        $signer->setRecipientId("1");
        $signer->setTabs($tabs);
        $signer->setClientUserId("1234");  // must set this to embed the recipient!

        // Add a recipient to sign the document
        $recipients = new DocuSign\eSign\Model\Recipients();
        $recipients->setSigners(array($signer));
        $envelop_definition = new DocuSign\eSign\Model\EnvelopeDefinition();
        $envelop_definition->setEmailSubject("[DocuSign PHP SDK] - Please sign this doc");

        // set envelope status to "sent" to immediately send the signature request
        $envelop_definition->setStatus("sent");
        $envelop_definition->setRecipients($recipients);
        $envelop_definition->setDocuments(array($document));

        // create and send the envelope! (aka signature request)
        $envelop_summary = $envelopeApi->createEnvelope($accountId, $envelop_definition, null);
        echo "$envelop_summary\n";
        $envelop = json_decode($envelop_summary);


        /////////////////////////////////////////////////////////////////////////
        // STEP 3:  Request Recipient View (aka signing URL)
        /////////////////////////////////////////////////////////////////////////

        // instantiate a RecipientViewRequest object
        $recipient_view_request = new \DocuSign\eSign\Model\RecipientViewRequest();

        // set where the recipient is re-directed once they are done signing
        $recipient_view_request->setReturnUrl("https://www.docusign.com/develcenter");

        // configure the embedded signer
        $recipient_view_request->setUserName($recipientName);
        $recipient_view_request->setEmail($recipientEmail);

        // must reference the same clientUserId that was set for the recipient when they
        // were added to the envelope in step 2
        $recipient_view_request->setClientUserId("1234");

        // used to indicate on the certificate of completion how the user authenticated
        $recipient_view_request->setAuthenticationMethod("email");

        // generate the recipient view! (aka embedded signing URL)
        $signingView = $envelopeApi->createRecipientView($accountId, $envelop_summary->getEnvelopeId(), $recipient_view_request);
        // echo "Signing URL = " . $signingView->getUrl() . "\n";



        $envelope_status = $envelopeApi->getEnvelope($accountId,$envelop->envelopeId);

        $status = $envelope_status->getStatus();
        var_dump($status);
    }
}

?>
