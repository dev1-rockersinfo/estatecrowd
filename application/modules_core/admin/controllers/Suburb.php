<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Suburb extends ROCKERS_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('home_model');

    }

    function index()
    {
        redirect('admin/suburb/list_suburb/');
    }

    

    /*
     Function name :edit_country()
    Parameter : $id:id of the country admin wish to edit.
    Return : none
    Use : to edit the information of particular country.
    */
    function edit_suburb($id = 0)
    {
        $check_rights = get_rights('list_suburb');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }



        $one_suburb = GetAllSuburb($id);
        $data["error"] = "";
        $data["id"] = $id;
        $data["suburb"] = $one_suburb[0]['suburb'];
        $data["state"] = $one_suburb[0]['state'];
        $data["status"] = $one_suburb[0]['status'];
        $data["postcode"] = $one_suburb[0]['postcode'];


        $this->load->library('form_validation');

        $this->form_validation->set_rules('suburb', 'Suburb', 'required');
        if($_POST)
        {
         if ($this->form_validation->run() == FALSE) {
            if (validation_errors() ) {
                $data["error"] = validation_errors() ;
            } else {
                $data["error"] = "";
            }

            $data["id"] = $this->input->post('id');
            $data["suburb"] = $this->input->post('suburb');
            $data["state"] = $this->input->post('state');
            $data["postcode"] = $this->input->post('postcode');
            $data["status"] = $this->input->post('status');
        }
        else
        {
            
             $data = array(
                'suburb' => $this->input->post('suburb'),
                'state' => $this->input->post('state'),
                'postcode' => $this->input->post('postcode'),
                'status' => $this->input->post('status'),
            );
            $this->home_model->table_update('id', $this->input->post('id'), 'postcodes_geo', $data);
            $msg = "update";
            setting_deletecache('suburb');
            setting_new_cache('suburb', 'postcodes_geo');
          
            redirect('admin/suburb/list_suburb/' . $msg);

        }

    }
      $data['site_setting'] = site_setting();
  
        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Countries', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'suburb/add_suburb', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }

    /*
     Function name :delete_country()
    Parameter : $id:id of the country admin wish to delete.
    Return : none
    Use : to delete particular country
    */
    function delete_country($id = 0)
    {
        $check_rights = get_rights('list_suburb');
        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        $this->db->delete('postcodes_geo', array('id' => $id));
        setting_deletecache('suburb');
        setting_new_cache('suburb', 'postcodes_geo');
        redirect('admin/suburb/list_suburb/delete');
    }

    /*
     Function name :list_country()
    Parameter : $msg:message string of the last operation performed by admin.
    Return : none
    Use : to display list of countries to admin.
    */
    function list_suburb($msg = '')
    {

        $check_rights = get_rights('list_suburb');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        //$data['result'] = $this->country_model->get_country_result();
        $data['result'] = GetAllSuburb();
        $data['msg'] = $msg;

        $data['site_setting'] = site_setting();
        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');
        $this->template->write('title', 'Suburbs', '', TRUE);
        $this->template->write_view('header', 'header', $data, TRUE);
        $this->template->write_view('main_content', 'suburb/list_suburb', $data, TRUE);
        $this->template->write_view('footer', 'footer', '', TRUE);
        $this->template->render();
    }

    /*
     Function name :action_country()
    Parameter : none
    Return : none
    Use : To activate or inactivate list of countries together.
    */
    function action_country()
    {
        if ($this->session->userdata('admin_id') == '') {
            redirect('admin/home/dashboard/no_rights');
        }


        $offset = $this->input->post('offset');
        $action = $this->input->post('action');
        $page_id = $this->input->post('chk');

        //print_r($_POST);die();
        switch ($action) {
            case 'active':
                $msg = 'active';
                foreach ($page_id as $id) {
                    $this->db->query("update postcodes_geo set active=1 where id='" . $id . "'");
                }
                break;
            case 'inactive':
                $msg = 'inactive';
                foreach ($page_id as $id) {
                    $this->db->query("update postcodes_geo set active=0 where id='" . $id . "'");
                }
                break;
        }
        setting_deletecache('suburb');
        setting_new_cache('suburb', 'postcodes_geo');
        redirect('admin/suburb/list_suburb/' . $msg);

    }



}

?>
