<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Gateways_details extends ROCKERS_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('gateways_detail_model');
        $this->load->model('home_model');


    }

    function index()
    {
        redirect('admin/gateways_details/list_gateway_detail');
    }


    function list_gateway_detail($msg = '')

    {

        $check_rights = get_rights('list_gateway_detail');
        if ($check_rights == 0) {

            redirect('admin/home/dashboard/no_rights');

        }
        //$data['result'] = $this->transaction_type_model->get_paypal_result($offset, $limit);
        //$data['result'] =get_table_data('gateways_details',array('gateways_details.id'=>'desc'));
        $data['result'] = get_payment_gateway($msg);

        $data['msg'] = $msg;

        $data['offset'] = '';
        $data['site_setting'] = site_setting();

        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');

        $this->template->write('title', 'Gateway Details', '', TRUE);

        $this->template->write_view('header', 'header', $data, TRUE);

        $this->template->write_view('main_content', 'payment/list_gateway_detail', $data, TRUE);

        $this->template->write_view('footer', 'footer', '', TRUE);

        $this->template->render();

    }


    function edit_status($id = 0, $offset = 0)
    {


        $check_rights = get_rights('list_payment_gateway');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }

        $one_pay = $this->payments_gateway_model->get_one_payment($id);

        if ($one_pay['status'] == 'Active' or $one_pay['status'] != 'Inactive') {

            $this->db->query("UPDATE `payments_gateways` SET `status`='Inactive' where `id`='" . $id . "'");
            redirect('admin/payments_gateways/list_payment_gateway/' . $offset . '/status');

        } else {

            $this->db->query("UPDATE `payments_gateways` SET `status`='Active' where `id`='" . $id . "'");
            redirect('admin/payments_gateways/list_payment_gateway/' . $offset . '/status');

        }


    }


    function delete_detail($id = 0, $offset = 0)
    {

        $check_rights = get_rights('list_gateway_detail');

        if ($check_rights == 0) {
            redirect('admin/home/dashboard/no_rights');
        }
        $this->db->delete('gateways_details', array('id' => $id));
        redirect('admin/gateways_details/list_gateway_detail/' . $pay . '/' . $offset . '/delete');

    }


    function edit_detail($id = 0)

    {

        $check_rights = get_rights('list_gateway_detail');

        if ($check_rights == 0) {

            redirect('admin/home/dashboard/no_rights');

        }

        $one_pay = $this->gateways_detail_model->get_one_detail($id);
        $data["error"] = "";
        $data["id"] = $id;
        $data["name"] = $one_pay['name'];
        $data["payment_gateway_id"] = isset($one_pay['payment_gateway_id']);
        $data["description"] = $one_pay['description'];
        $data["value"] = $one_pay['value'];
        $data["label"] = $one_pay['label'];

        $this->load->library('form_validation');

        $this->form_validation->set_rules('payment_gateway_id', PAYMENT_GATEWAY, 'required|numeric');
        $this->form_validation->set_rules('description', DESCRIPTION, 'required');
        $this->form_validation->set_rules('name', NAME, 'required');
        $this->form_validation->set_rules('value', VALUE, 'required');
        $this->form_validation->set_rules('label', LABEL, 'required');

        if ($this->form_validation->run() == FALSE) {

            if (validation_errors()) {
                $data["error"] = validation_errors();
            } else {
                $data["error"] = "";
            }
            if ($_POST) {
                $data['value'] = $this->input->post('value');
            } else {
                $data['value'] = $one_pay['value'];
            }
        } else {

            $update_array = array(
                'value' => $this->input->post('value')
            );

            gateway_update($update_array, $id);
            redirect('admin/gateways_details/list_gateway_detail/' . $data["payment_gateway_id"]);
        }


        //echo  '<pre>';
        //print_r($one_pay);
        $site_setting = site_setting();

        $data['site_setting'] = $site_setting;

        $theme = 'admin';

        $this->template->set_master_template($theme . '/template.php');

        $this->template->write('title', 'Edit Gateway Details', '', TRUE);

        $this->template->write_view('header', 'header', $data, TRUE);

        $this->template->write_view('main_content', 'payment/edit_gateway_detail', $data, TRUE);

        $this->template->write_view('footer', 'footer', '', TRUE);

        $this->template->render();

    }


    function add_detail()

    {


        $check_rights = get_rights('list_gateway_detail');

        $site_setting = site_setting();

        if ($check_rights == 0) {

            redirect('admin/home/dashboard/no_rights');

        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('payment_gateway_id', 'Payment Gateway', 'required|numeric');
        $this->form_validation->set_rules('description', 'Description', 'required');
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('value', 'Value', 'required');
        $this->form_validation->set_rules('label', 'Label', 'required');


        if ($this->form_validation->run() == FALSE) {

            if (validation_errors()) {
                $data["error"] = validation_errors();
            } else {
                $data["error"] = "";
            }


            $data["id"] = $this->input->post('id');
            $data["name"] = $this->input->post('name');
            $data["payment_gateway_id"] = $id;
            $data["value"] = $this->input->post('value');
            $data["label"] = $this->input->post('label');
            $data["description"] = $this->input->post('description');


            $data['site_setting'] = site_setting();


            $theme = 'admin';

            $this->template->set_master_template($theme . '/template.php');

            $this->template->write('title', 'Add Gateway Details', '', TRUE);

            $this->template->write_view('header', 'header', $data, TRUE);

            $this->template->write_view('main_content', 'payment/add_gateway_detail', $data, TRUE);

            $this->template->write_view('footer', 'footer', '', TRUE);

            $this->template->render();

        } else {


            if ($this->input->post('id')) {

                $this->gateways_detail_model->detail_update();

                $msg = "update";

            } else {

                $this->gateways_detail_model->detail_insert();

                $msg = "insert";

            }


            redirect('admin/gateways_details/list_gateway_detail/' . $msg);


        }

    }

}

?>
