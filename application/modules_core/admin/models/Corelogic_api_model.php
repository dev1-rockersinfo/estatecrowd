<?php

/**
 * Created by PhpStorm.
 * User: rocku28
 * Date: 1/7/16
 * Time: 1:54 PM
 */
class Corelogic_api_model extends Rockers_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    function get_postcode(){
        return $this->db->select('*')
            ->from('postcodes_geo')
            ->group_by('postcode')
            ->order_by('id','desc')
            ->get()
            ->result_array();
    }

    function get_localities($search){
        $this->db->select('*');
        $this->db->from('locality_detail');
        if($search){
            foreach ($search as $key=>$value){
                if($value)$this->db->like($key,$value);
            }
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_api_properties(){
        return $this->db->select('*')
                ->from('property')
                ->where('property_id_api is not null')
                ->order_by('property_id','desc')
                ->get()->result_array();
    }
}