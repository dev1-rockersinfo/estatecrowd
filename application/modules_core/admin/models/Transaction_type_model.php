<?php

/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
class Transaction_type_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function transaction_type_insert()
    {
        $data = array(
            'transaction_type_name' => $this->input->post('transaction_type_name'),
        );
        $this->db->insert('transaction_type', $data);
    }

    function transaction_type_update()
    {
        $data = array(
            'transaction_type_name' => $this->input->post('transaction_type_name'),
        );
        $this->db->where('transaction_type_id', $this->input->post('transaction_type_id'));
        $this->db->update('transaction_type', $data);
    }

    function get_one_transaction_type($id)
    {
        $query = $this->db->get_where('transaction_type', array('transaction_type_id' => $id));
        return $query->row_array();
    }

    function get_total_transaction_type_count()
    {
        return $this->db->count_all('transaction_type');
    }

    function get_transaction_type_result($offset, $limit)
    {
        $query = $this->db->get('transaction_type', $limit, $offset);

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return 0;
    }

    function get_total_transaction_count()
    {

        $this->db->select('transaction.*,equity.project_name,equity.equity_url');
        $this->db->from('transaction');
        $this->db->where('transaction.wallet_payment_status !=', '2');
        $this->db->join('equity', 'transaction.equity_id= equity.equity_id');
        $this->db->order_by('transaction.transaction_id', 'desc');

        $query = $this->db->get();
        return $query->num_rows();

    }

    function get_transaction_result()
    {
        $this->db->select('transaction.*,property.property_address,property.property_url,campaign.user_id as campaign_owner');
        $this->db->from('campaign');
        //$this->db->where('transaction.wallet_payment_status !=','2');
       


          if($_GET){
            // search variables
            $equity_name = $this->input->get('equity_name');
            $user_name = $this->input->get('user_name');
            $min_amount = $this->input->get('min_amount');
            $max_amount = $this->input->get('max_amount');
            $trans_id = $this->input->get('trans_id');
            $from_date = $this->input->get('from_date');
            $to_date = $this->input->get('to_date');

            //by project title
            if ($equity_name != '') {

                $where = "( `property`.`property_address` like '%" . $equity_name . "%' )";
                $this->db->where($where);
            }

            //by user name
            if ($user_name != '' && strtoupper($user_name)!='ANONYMOUS' ){
                $user_name_arr=explode(' ', $user_name);
                  $where='';
                foreach ($user_name_arr as $value) {
                    if($where==''){
                       $where = "( `user`.`user_name` like '%" . $value . "%' OR `user`.`last_name` like '%" . $value . "%') ";
                    }else{

                        $where =$where. "and ( `user`.`user_name` like '%" . $value . "%' OR `user`.`last_name` like '%" . $value . "%')";
                    }
                   
                }
                 $this->db->where($where); 
            }else if( strtoupper($user_name)=='ANONYMOUS'){
                $where = "( `transaction`.`user_id` = '0' )";
                $this->db->where($where);
            }

            // by amount

            if($min_amount && $max_amount){
                $where = "( `transaction`.`amount` BETWEEN '".$min_amount."' AND '".$max_amount."' )";
                $this->db->where($where);
            }else if($min_amount){
                $where = "( `transaction`.`amount` >= '".$min_amount."')";
                $this->db->where($where);
            }else if($max_amount){
                $where = "( `transaction`.`amount` <= '".$max_amount."')";
                $this->db->where($where);
            }

          

            //by transaction id
            if ($trans_id != '') {
                $where = "`transaction`.`preapproval_key` like '%" . $trans_id . "%'";
               
                $this->db->where($where);
            }

            //between dates
            if ($from_date != '' && $to_date!='') { 
                $where = "(`transaction`.`transaction_date_time` BETWEEN '".date('Y-m-d', strtotime($from_date))."' AND '".date('Y-m-d', strtotime($to_date))."')";       
                $this->db->where($where);
            }
        }


         $this->db->join('property', 'campaign.property_id= property.property_id');
         $this->db->join('transaction', 'campaign.campaign_id= campaign.campaign_id');
         $this->db->join('user', 'campaign.user_id= user.user_id', 'left');
         $this->db->group_by('transaction.transaction_id');
        $this->db->order_by('transaction.transaction_id', 'desc');
        //	$this->db->limit($limit,$offset);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return 0;
    }


    function get_total_search_transaction_count($option, $keyword)
    {
        if ($option != 'pay') {
            $keyword = str_replace('"', '', str_replace(array("'", ",", "%", "$", "&", "*", "#", "@", "(", ")", ":", ";", ">", "<", "/"), '', $keyword));
        }
        $this->db->select('transaction.*,equity.project_name,equity.equity_url,user.user_name,user.last_name');
        $this->db->from('transaction');
        $this->db->where('transaction.wallet_payment_status !=', '2');
        $this->db->join('equity', 'transaction.equity_id= equity.equity_id', 'left');
        $this->db->join('user', 'transaction.user_id= user.user_id', 'left');
        $this->db->order_by('transaction.transaction_id', 'desc');

        if ($option == 'title') {
            $this->db->like('equity.project_name', $keyword);

            if (substr_count($keyword, ' ') >= 1) {
                $ex = explode(' ', $keyword);

                foreach ($ex as $val) {
                    $this->db->or_like('equity.project_name', $val);
                }
            }


        }
        if ($option == 'user') {

            $this->db->like('user.user_name', $keyword);
            $this->db->or_like('user.last_name', $keyword);

            $this->db->or_like('user.user_name', substr($keyword, -3, 3));
            $this->db->or_like('user.user_name', substr($keyword, 0, 3));
            $this->db->or_like('user.last_name', substr($keyword, -3, 3));
            $this->db->or_like('user.last_name', substr($keyword, 0, 3));

            if (substr_count($keyword, ' ') >= 1) {

                $ex = explode(' ', $keyword);

                foreach ($ex as $val) {
                    $this->db->or_like('user.user_name', $val);
                    $this->db->or_like('user.last_name', $val);
                }

            }


        }

        if ($option == 'ip') {
            $this->db->like('transaction.host_ip', $keyword);

        }

        if ($option == 'trans') {
            $this->db->like('transaction.wallet_transaction_id', $keyword);
            $this->db->or_like('transaction.amazon_transaction_id', $keyword);
            $this->db->or_like('transaction.paypal_paykey', $keyword);
            $this->db->or_like('transaction.preapproval_pay_key', $keyword);
            $this->db->or_like('transaction.preapproval_key', $keyword);
            $this->db->or_like('transaction.paypal_transaction_id', $keyword);


        }

        if ($option == 'pay') {
            $this->db->like('transaction.paypal_email', $keyword);

        }

        $this->db->order_by('transaction.transaction_id', 'desc');

        $query = $this->db->get();
        return $query->num_rows();

    }


    function get_search_transaction_result($option, $keyword, $offset, $limit)
    {
        if ($option != 'pay') {
            $keyword = str_replace('"', '', str_replace(array("'", ",", "%", "$", "&", "*", "#", "@", "(", ")", ":", ";", ">", "<", "/"), '', $keyword));
        }
        $this->db->select('transaction.*,equity.project_name,equity.equity_url,user.user_name,user.last_name');
        $this->db->from('transaction');
        $this->db->where('transaction.wallet_payment_status !=', '2');
        $this->db->join('equity', 'transaction.equity_id= equity.equity_id', 'left');
        $this->db->join('user', 'transaction.user_id= user.user_id', 'left');
        $this->db->order_by('transaction.transaction_id', 'desc');


        if ($option == 'title') {
            $this->db->like('equity.project_name', $keyword);

            if (substr_count($keyword, ' ') >= 1) {
                $ex = explode(' ', $keyword);

                foreach ($ex as $val) {
                    $this->db->or_like('equity.project_name', $val);
                }
            }


        }
        if ($option == 'user') {

            $this->db->like('user.user_name', $keyword);
            $this->db->or_like('user.last_name', $keyword);

            $this->db->or_like('user.user_name', substr($keyword, -3, 3));
            $this->db->or_like('user.user_name', substr($keyword, 0, 3));
            $this->db->or_like('user.last_name', substr($keyword, -3, 3));
            $this->db->or_like('user.last_name', substr($keyword, 0, 3));

            if (substr_count($keyword, ' ') >= 1) {

                $ex = explode(' ', $keyword);

                foreach ($ex as $val) {
                    $this->db->or_like('user.user_name', $val);
                    $this->db->or_like('user.last_name', $val);
                }

            }


        }


        if ($option == 'ip') {
            $this->db->like('transaction.host_ip', $keyword);

        }


        if ($option == 'trans') {
            $this->db->like('transaction.wallet_transaction_id', $keyword);
            $this->db->or_like('transaction.amazon_transaction_id', $keyword);
            $this->db->or_like('transaction.paypal_paykey', $keyword);
            $this->db->or_like('transaction.preapproval_pay_key', $keyword);
            $this->db->or_like('transaction.preapproval_key', $keyword);
            $this->db->or_like('transaction.paypal_transaction_id', $keyword);


        }

        if ($option == 'pay') {
            // echo $keyword;
            // exit;
            $this->db->like('transaction.paypal_email', $keyword);

        }

        $this->db->order_by('transaction.transaction_id', 'desc');
        $this->db->limit($limit, $offset);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return 0;
    }


    function get_donor_detail($uid)
    {
        $query = $this->db->query("select * from user where user_id='" . $uid . "'");

        return $query->row();

    }


    function get_perk_detail($pid)
    {
        $query = $this->db->query("select * from perk where perk_id='" . $pid . "'");

        return $query->row();

    }


    /*********noraml paypal********/


    function get_normal_paypal_result()
    {

        $query = $this->db->query("select * from site_setting where site_setting_id='2'");

        return $query->row();
    }


    function normal_paypal_update()
    {


        if ($this->input->post('paypal_status') == 'sandbox') {

            $paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscri';
        } else {
            $paypal_url = 'https://www.paypal.com/cgi-bin/webscri';
        }


        $data = array(
            'paypal_status' => $this->input->post('paypal_status'),
            'paypal_url' => $paypal_url,
            'paypal_email' => $this->input->post('paypal_email'),
            'paypal_API_UserName' => $this->input->post('paypal_API_UserName'),
            'paypal_API_Password' => $this->input->post('paypal_API_Password'),
            'paypal_API_Signature' => $this->input->post('paypal_API_Signature'),
            'normal_paypal' => $this->input->post('normal_paypal'),

        );
        $this->db->where('site_setting_id', '2');
        $this->db->update('site_setting', $data);


        $query = $this->db->query("update equity_setting set `pay_fee`='" . $this->input->post('pay_fee') . "' where equity_setting_id='1'");


    }

    function get_normal_paypal()
    {
        $query = $this->db->get_where('site_setting', array('site_setting_id' => '2'));
        return $query->row_array();
    }


    function get_equity_setting()
    {
        $query = $this->db->get_where('equity_setting', array('equity_setting_id' => '1'));
        return $query->row_array();
    }


    /*********paypal********/

    function get_total_paypal_count()
    {
        return $this->db->count_all('paypal');
    }

    function get_paypal_result($offset, $limit)
    {
        //$this->db->select('paypal.*,equity.company_name');
        $this->db->from('paypal');
        //	$this->db->join('equity', 'transaction.equity_id= equity.equity_id');
        $this->db->order_by('paypal.id', 'desc');
        $this->db->limit($limit, $offset);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return 0;
    }

    function paypal_insert()
    {
        $data = array(
            'site_status' => $this->input->post('site_status'),
            'application_id' => $this->input->post('application_id'),
            'paypal_email' => $this->input->post('paypal_email'),
            'paypal_username' => $this->input->post('paypal_username'),
            'paypal_password' => $this->input->post('paypal_password'),
            'paypal_signature' => $this->input->post('paypal_signature'),
            'fees_taken_from' => $this->input->post('fees_taken_from'),
            'transaction_fees' => $this->input->post('transaction_fees'),
            'donate_limit' => $this->input->post('donate_limit'),
        );
        $this->db->insert('paypal', $data);
    }

    function paypal_update()
    {

        $data = array(
            'site_status' => $this->input->post('site_status'),
            'application_id' => $this->input->post('application_id'),
            'paypal_email' => $this->input->post('paypal_email'),
            'paypal_username' => $this->input->post('paypal_username'),
            'paypal_password' => $this->input->post('paypal_password'),
            'paypal_signature' => $this->input->post('paypal_signature'),
            'fees_taken_from' => $this->input->post('fees_taken_from'),
            'transaction_fees' => $this->input->post('transaction_fees'),
            'donate_limit' => $this->input->post('donate_limit'),
        );
        $this->db->where('id', $this->input->post('id'));
        $this->db->update('paypal', $data);


    }

    function get_one_paypal($id)
    {
        $query = $this->db->get_where('paypal', array('id' => $id));
        return $query->row_array();
    }


    /*******Amazon************/

    function get_total_amazon_count()
    {
        return $this->db->count_all('amazon');
    }

    function get_amazon_result($offset, $limit)
    {
        //$this->db->select('paypal.*,equity.company_name');
        $this->db->from('amazon');
        //	$this->db->join('equity', 'transaction.equity_id= equity.equity_id');
        $this->db->order_by('amazon.id', 'desc');
        $this->db->limit($limit, $offset);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return 0;
    }

    function amazon_insert()
    {
        $data = array(
            'site_status' => $this->input->post('site_status'),
            'aws_access_key_id 	' => $this->input->post('aws_access_key_id 	'),
            'aws_secret_access_key' => $this->input->post('aws_secret_access_key'),
            'variable_fees' => $this->input->post('variable_fees'),
            'fixed_fees' => $this->input->post('fixed_fees')
        );
        $this->db->insert('amazon', $data);
    }

    function amazon_update()
    {
        $data = array(
            'site_status' => $this->input->post('site_status'),
            'aws_access_key_id' => $this->input->post('aws_access_key_id'),
            'aws_secret_access_key' => $this->input->post('aws_secret_access_key'),
            'variable_fees' => $this->input->post('variable_fees'),
            'fixed_fees' => $this->input->post('fixed_fees')
        );
        $this->db->where('id', $this->input->post('id'));
        $this->db->update('amazon', $data);
    }

    function get_one_amazon($id)
    {
        $query = $this->db->get_where('amazon', array('id' => $id));
        return $query->row_array();
    }


    /*********paypal credit card********/

    function get_total_credit_card_count()
    {
        return $this->db->count_all('paypal_credit_card');
    }

    function get_credit_card_result($offset, $limit)
    {
        $this->db->select('*');
        $this->db->from('paypal_credit_card');

        $this->db->order_by('paypal_credit_card_id', 'desc');
        $this->db->limit($limit, $offset);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return 0;
    }

    function paypal_credit_card_insert()
    {
        $data = array(
            'credit_card_version' => $this->input->post('credit_card_version'),
            'credit_card_proxy_port' => $this->input->post('credit_card_proxy_port'),
            'credit_card_proxy_host' => $this->input->post('credit_card_proxy_host'),
            'credit_card_use_proxy' => $this->input->post('credit_card_use_proxy'),
            'credit_card_subject' => $this->input->post('credit_card_subject'),
            'credit_card_preapproval' => $this->input->post('credit_card_preapproval'),
            'credit_card_api_signature' => $this->input->post('credit_card_api_signature'),
            'credit_card_username' => $this->input->post('credit_card_username'),
            'credit_card_password' => $this->input->post('credit_card_password'),
            'credit_card_site_status' => $this->input->post('credit_card_site_status'),
            'credit_card_gateway_status' => $this->input->post('credit_card_gateway_status')
        );
        $this->db->insert('paypal_credit_card', $data);
    }

    function paypal_credit_card_update()
    {
        /*
        'credit_card_proxy_port' => $this->input->post('credit_card_proxy_port'),
        'credit_card_proxy_host' => $this->input->post('credit_card_proxy_host'),
        'credit_card_use_proxy' => $this->input->post('credit_card_use_proxy'),
        'credit_card_subject' => $this->input->post('credit_card_subject'),
        */


        $data = array(
            'credit_card_version' => $this->input->post('credit_card_version'),
            'credit_card_api_signature' => $this->input->post('credit_card_api_signature'),
            'credit_card_username' => $this->input->post('credit_card_username'),
            'credit_card_password' => $this->input->post('credit_card_password'),
            'credit_card_site_status' => $this->input->post('credit_card_site_status'),
        );
        $this->db->where('paypal_credit_card_id', $this->input->post('paypal_credit_card_id'));
        $this->db->update('paypal_credit_card', $data);


    }

    function get_paypal_credit_card_by_id($id)
    {
        $query = $this->db->get_where('paypal_credit_card', array('paypal_credit_card_id' => $id));
        return $query->row_array();
    }


}

?>