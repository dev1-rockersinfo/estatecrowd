<?php

/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
class Set_fund_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }


    function get_serch_user($keyword = '')
    {
        if ($keyword == '') {
            $qry = $this->db->query("select * from user where active='1' ");
        } else {
            $qry = $this->db->query("select * from user where active='1' and email like '%" . $keyword . "%' or user_id='$keyword' ");
        }
        if ($qry->num_rows() > 0) {

            return $qry->num_rows();
        }
    }

    function search_user($limit = 0, $offset = 0, $keyword = '')
    {
        if ($keyword == '') {
            $qry = $this->db->query("select * from user where active='1' ");
        } else {
            $qry = $this->db->query("select * from user where active='1' and  email like '%" . $keyword . "%' or user_id='$keyword' order by user_id desc LIMIT " . $limit . " OFFSET " . $offset . "");
        }
        if ($qry->num_rows() > 0) {
            return $qry->result();
        }
    }

    function user_wallet_amount($uid = "")
    {


        $query1 = $this->db->query("SELECT SUM(debit) as sumd FROM wallet where admin_status='Confirm' and user_id='$uid' and donate_status not in ('0','3','2')");

        $query2 = $this->db->query("SELECT SUM(credit) as sumc FROM wallet where admin_status='Confirm' and user_id='$uid' and donate_status not in ('2','3')");


        if ($query1->num_rows() > 0) {

            $result1 = $query1->row();
            $result2 = $query2->row();

            $debit = $result1->sumd;
            $credit = $result2->sumc;

            $total = $debit - $credit;

            return $total;

        }

        return 0;

    }

}

?>