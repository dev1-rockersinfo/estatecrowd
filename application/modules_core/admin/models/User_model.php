<?php

/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
class User_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    /*
     Function name :delete_user()
    Parameter : id=user_id.
    Return : none
    Use : to delete all the user data and account from the site.
    */
    function delete_user($id)
    {


        //////=====user login details====

        $chk_user_login = $this->db->query("select * from user_login where user_id='" . $id . "'");

        if ($chk_user_login->num_rows() > 0) {
            $this->db->delete('user_login', array('user_id' => $id));
        }


        //////=====user email notification====

        $chk_user_notify = $this->db->query("select * from user_notification where user_id='" . $id . "'");

        if ($chk_user_notify->num_rows() > 0) {
            $this->db->delete('user_notification', array('user_id' => $id));
        }

        //////=====user email notification====

        $chk_user_notify = $this->db->query("select * from invite_members where invite_user_id='" . $id . "'");

        if ($chk_user_notify->num_rows() > 0) {
            $this->db->delete('invite_members', array('invite_user_id' => $id));
        }
      
      

        //////===delete user====
        $this->db->delete('user', array('user_id' => $id));
    }


    /*
     Function name :get_email_notification()
    Parameter : id=user_id.
    Return : returns result if found else returns 0;
    Use : to get user notification details of particular user from user_notification table.
    */
    function get_email_notification($id)
    {

        $query = $this->db->query("select * from user_notification where user_id='" . $id . "'");

        if ($query->num_rows() > 0) {
            return $query->row();
        }

        return 0;

    }


    /*
     Function name :chk_user_project()
    Parameter : id=user_id.
    Return : returns 1 if user has projects posted or made donation or commented on project else 0.
    Use : to check user has projects posted or made donation or commented on project
    */
    function chk_user_project($user_id)
    {
        $chk = '0';

        $chk_project = $this->db->query("select * from equity where user_id='" . $user_id . "'");

        if ($chk_project->num_rows() > 0) {
            $chk = '1';
        }


        $chk_backers = $this->db->query("select * from transaction where user_id='" . $user_id . "'");

        if ($chk_backers->num_rows() > 0) {
            $chk = '1';
        }


        $chk_comment = $this->db->query("select * from comment where user_id='" . $user_id . "'");

        if ($chk_comment->num_rows() > 0) {
            $chk = '1';
        }


        if ($chk == '1' || $chk == 1) {
            return 1;
        } else {
            return 0;
        }

    }

    /*
     Function name :user_unique()
    Parameter : str=entered email address by user.
    Return : returns true if email is unique else false if email exists.
    Use : to check user's email is unique or not.
    */
    function user_unique($str)
    {
        if ($this->input->post('user_id')) {
            // $query = $this->db->get_where('user', array('user_id' => $this->input->post('user_id')));
            // $res = $query->row_array();
            // $email = $res['email'];

            $query = $this->db->query("select email from user where email = '$str' and user_id != '" . $this->input->post('user_id') . "'");
        } else {
            $query = $this->db->query("select email from user where email = '$str'");
        }
        if ($query->num_rows() > 0) {

            return 1;
        } else {
            return 0;
        }
    }


    /*
     Function name :user_insert_update()
    Parameter : type.
    type=insert-Used to insert user or register new user.
    type=update-used to update records of user.
    Return : none
    Use : this function is used to insert or update user data into table
    */
    function user_insert_update($type = "")
    {
        $rs = site_setting();
        $city = '';
        $state = '';
        $country = '';

        $address = explode(',', $this->input->post('address'));
        $cnt = count($address);

        $fetch = $cnt - $rs['address_limit'];


        if (count($address) <= $fetch || $fetch <= 0) {

            if (isset($address[$cnt - 3])) $city = $address[$cnt - 3];
            if (isset($address[$cnt - 2])) $state = $address[$cnt - 2];
            if (isset($address[$cnt - 1])) $country = $address[$cnt - 1];

        } else {

            $city = $address[$fetch];
            $state = $address[$fetch + 1];
            $country = $address[$fetch + 2];
        }
        if ($this->input->post('active') == 0) {
            $admin_active = 0;
        } else {
            $admin_active = $this->input->post('active');
        }
        //	echo $admin_active;
        $user_data = UserData($this->input->post('user_id'));
        if ($type == "update") {
            $password = $user_data[0]['password'];
        } else {
            $password = md5($this->input->post('password'));
        }
        $data = array(
            'email' => $this->input->post('email'),
            'user_name' => $this->input->post('user_name'),
            'last_name' => $this->input->post('last_name'),
            'password' => $password,
            'image' => $this->input->post('image'),
            'address' => $this->input->post('address'),
            'city' => $city,
            'state' => $state,
            'country' => $country,
            'zip_code' => $this->input->post('zip_code'),
            'paypal_email' => $this->input->post('paypal_email'),
            'active' => $this->input->post('active'),
            'user_about' => $this->input->post('user_about'),
            'user_occupation' => $this->input->post('user_occupation'),
            'user_interest' => $this->input->post('user_interest'),
            'user_skill' => $this->input->post('user_skill'),
            'user_website' => $this->input->post('user_website'),
            'facebook_url' => $this->input->post('facebook_url'),
            'twitter_url' => $this->input->post('twitter_url'),
            'linkedln_url' => $this->input->post('linkedln_url'),
            'googleplus_url' => $this->input->post('googleplus_url'),
            'bandcamp_url' => $this->input->post('bandcamp_url'),
            'youtube_url' => $this->input->post('youtube_url'),
            'myspace_url' => $this->input->post('myspace_url'),
            'unique_code' => unique_user_code(getrandomCode(12)),
            'admin_active' => $admin_active,
            'suspend_reason' => $this->input->post('reason')
        );

        if ($type == "update") {
            $this->db->where('user_id', $this->input->post('user_id'));
            $this->db->update('user', $data);
        } else {
            $data_insert = array(
                'signup_ip' => SecurePostData($_SERVER['REMOTE_ADDR']),
                'date_added' => SecurePostData(date('Y-m-d H:i:s'))
            );
            $data = $data + $data_insert;
            $this->db->insert('user', $data);
            $user_id = $this->db->insert_id();
            /*** user notification ****/

            $user_noti = array(
                'creator_pledge_alert' => 1,
                'creator_comment_alert' => 1,
                'creator_follow_alert' => 1,
                'creator_newup_alert' => 1,
                'user_id' => $user_id
            );


            $this->db->insert('user_notification', $user_noti);
            $id = $this->db->insert_id();
        }
    }

    /*
     Function name :get_userlogin_result()
    Parameter : none.
    Return : returns login results of users if found else returns 0;
    Use : this function is used to fetch user login data from user login table.
    */
    function get_userlogin_result()
    {
        //$query = $this->db->get('user_login',$limit,$offset);
        $this->db->select('user_login.user_id,
							user_login.login_id,
						   user_login.login_date_time,
						   user_login.login_ip,
						   user.user_id,
						   user.user_name,user.zip_code,user.address,user.email');
        $this->db->from('user_login');
        $this->db->join('user', 'user_login.user_id= user.user_id', 'left');
        $this->db->order_by('user_login.login_id', 'desc');
        $query = $this->db->get();


        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return 0;
    }

    function get_one_user($id)
    {
        $query = $this->db->get_where('user', array('user_id' => $id));
        return $query->row_array();
    }

    function get_one_span_user($id)
    {

        $this->db->select('*');
        $this->db->from('spam_report_ip');
        $this->db->join('user', ' spam_report_ip.spam_user_id = user.user_id', 'left');
        $this->db->where('spam_report_ip.spam_user_id', $id);
        $query = $this->db->get();

        return $query->row_array();

    }

}

?>