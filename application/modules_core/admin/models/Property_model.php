<?php

/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
class Property_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    
    //------Common Equity function-------
    /*
    Function name :GetAllProperties()
    Parameter : user_id,property_id,is_status,joinarr,limit,order
    Return : array of all Equities
    Use : Fetch all Equity particular Criteria

    Cases:

    case 1 : if user grater than zero then it fetch all the Equity with particular user id means it returns all Equity of  particular company owner.

    case 2 : if Equity grater than zero then it return particular Equity.


    case 3 : if Status equal to 0 than it returns Draft Equities.
             if Status equal to 1 than it returns Pending Equities.
             if Status equal to 2 than it returns Active Equities.
             if Status equal to 3 than it returns Success Equities.
             if Status equal to 4 than it returns unsuccess Equities.
             if Status equal to 5 than it returns Declined Equities.

             or

             if Status equal 0,1 than it returns both type of Equities.(Same for all others).

    case 4 : if all Equity will be sorted by given order variable values.


    */

    function GetAllCampaigns($user_id = 0, $campaign_id = '', $is_status = '', $joinarr = array('user'), $limit = 100, $order = array('campaign_id' => 'desc'), $offset = 0, $count = 'no',$campaign_data='no',$lead_investor_id='')
    {

        //default Equity fields
        $selectfields = 'campaign.*';
        //if require to join user
        if (in_array('user', $joinarr)) {
            $selectfields .= ',user.user_id,user.user_name,user.last_name,user.address, user.fb_uid, user.facebook_wall_post, user.fb_access_token,user.image,user.email,user.user_website,user.profile_slug,user.user_occupation,user.user_type,user.company_name';
        }
        if (in_array('property', $joinarr)) {
           
            $selectfields .= ',property.property_id,property.property_address,property.property_url,property.cars,property.bedrooms,property.bathrooms,property.property_type,property.property_sub_type,property.min_property_investment_range,property.max_property_investment_range,property.cover_image';
        }

       
      
        if ($user_id > 0) 
        {
            $selectfields .= ',invite_members.email as inviteemail';
        }
       

        $this->db->select($selectfields);
        $this->db->from('campaign');


        //if require to join user
        if (in_array('user', $joinarr)) {
            $this->db->join('user', 'campaign.user_id=user.user_id','left');

        }
       
        if (in_array('property', $joinarr)) {
            if($campaign_data == 'yes')
            {
                $this->db->join('property', 'campaign.property_id=property.property_id');
            }   
            else
            {
                $this->db->join('property', 'campaign.property_id=property.property_id','left');
            }
            if ($user_id > 0) {
                $this->db->join('invite_members', 'property.property_id=invite_members.property_id', 'left');
            }
        }
      
        //check all property status
        if ($is_status != '') {
            $this->db->where('campaign.status in (' . $is_status . ')');
        }

        //check property id
        if ($campaign_id > 0 or $campaign_id != '') {
            if (is_numeric($campaign_id)) {
                $this->db->where('campaign.campaign_id', $campaign_id);
                $this->db->or_where('property.property_url', $campaign_id);
            } else {
                $this->db->where('property.property_url', $campaign_id);
            }
        }

        //check user_id
        if ($user_id > 0) {
            // $this->db->where('user.user_id',$user_id);
            $where = '';
            $where .= "( `user`.`user_id` = '" . $user_id . "' ";
            $sql = " ( select i.email from invite_members as i inner join property as p on i.property_id=p.property_id inner join user as u  on u.email=i.email where i.status=1 and i.admin=1 and u.user_id= '" . $user_id . "' )";
            //$sql=" ( invite_members.status=1 and invite_members.admin=1 and  invite_members.email= user.email )";
            $where .= " or `invite_members`.`email` in " . $sql . " )";

            $this->db->where($where);

        }

        if($lead_investor_id > 0)
        {
            $this->db->where('campaign.lead_investor_id', $lead_investor_id);
        } 

        if (is_array($order)) {
            foreach ($order as $key => $val) {
                $this->db->order_by($key, $val);
            }
        }

        if ($user_id > 0) {
            $this->db->group_by('campaign.campaign_id');
        }
        if ($limit > 0) $this->db->limit($limit, $offset);

        $query = $this->db->get();
      
        if ($count == 'yes') {
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else {
                return 0;
            }
        }

    }

    /*

     Function name :delete_project()
     Parameter :$id
     Return : none
     Use : delete perticular id data

    */

    function delete_campaign($id)

    {

        $base_path = $this->config->slash_item('base_path');
       


        /////===transaction=====


        $chk_trans = $this->db->query("select * from transaction where campaign_id='" . $id . "'");
        if ($chk_trans->num_rows() > 0) {

            $trans = $chk_trans->result();

            foreach ($trans as $trn) {

                $this->db->query("delete from transaction where campaign_id='" . $trn->campaign_id . "'");


            }


        }


       

        //////======wallet======


        //////====project====


        $chk_campaign = $this->db->query("select * from campaign where campaign_id='" . $id . "'");

        if ($chk_project->num_rows() > 0) {

            $campaign = $chk_campaign->row();



            $this->db->query("delete from campaign where campaign_id='" . $id . "'");

        }


    }

 function GetUpdateCommentGallery($user_id = 0, $property_id = 0, $is_status = '', $join = array('user'), $limit = 10, $group = array('user_id'), $order = array('user_id' => 'desc'), $comment_status = '', $offset = 0, $count = 'no', $comment_id_for_parent = '0', $property_owner_id = 0)
    {
        $selectfields = 'property.property_address, property.property_url';
        //if require to join comment
        if (in_array('comment', $join)) {
            $selectfields .= ',comment.comment_id,comment.comments,comment.date_added,comment.user_id,comment.comment_ip,comment.status,comment.comment_type,comment.parent_id';
        }

        if (in_array('user', $join)) {
            $selectfields .= ',user.user_name,user.user_id,user.last_name,user.image,user.profile_slug';
        }

        if (in_array('perk', $join)) {
            $selectfields .= ',perk.perk_id,perk.property_id,perk.perk_title,perk.perk_description,perk.perk_amount,perk.perk_total,perk.perk_get,perk.estdate,perk.shipping_status';
        }
        if (in_array('previous_funding', $join)) {
            $selectfields .= ',previous_funding.previous_funding_id,previous_funding.property_id,previous_funding.funding_source,previous_funding.funding_type,previous_funding.funding_amount,previous_funding.funding_date';
        }
        if (in_array('investors', $join)) {
            $selectfields .= ',investors.investor_id,investors.property_id,investors.investor_description,investors.investor_type,investors.investor_name,investors.investor_role,investors.investor_image';
        }

        if (in_array('updates', $join)) {
            $selectfields .= ',updates.updates,updates.date_added,updates.update_id,user.user_name,user.user_id,user.last_name,user.image,user.profile_slug';
        }

        if (in_array('property_gallery', $join)) {
            $selectfields .= ',property_gallery.*';
        }
        if (in_array('video_gallery', $join)) {
            $selectfields .= ',video_gallery.image,video_gallery.media_video_title as image_name,video_gallery.status,video_gallery.property_id,video_gallery.media_video_name';
        }

        if (in_array('file_gallery', $join)) {
            $selectfields .= ',file_gallery.*';
        }

        $this->db->select($selectfields);

        $this->db->from('property');

        //if require to join comment

        if (in_array('comment', $join)) {
            $this->db->join('comment', 'property.property_id=comment.property_id');
        }

        if (in_array('perk', $join)) {
            $this->db->join('perk', 'property.property_id=perk.property_id');
        }
        if (in_array('previous_funding', $join)) {
            $this->db->join('previous_funding', 'property.property_id=previous_funding.property_id');
        }
        if (in_array('investors', $join)) {
            $this->db->join('investors', 'property.property_id=investors.property_id');
        }

        if (in_array('user', $join)) {
            $this->db->join('user', 'comment.user_id=user.user_id');
        }


        if (in_array('updates', $join)) {
            $this->db->join('updates', 'property.property_id=updates.property_id');
            $this->db->join('user', 'property.user_id=user.user_id');
        }

        if (in_array('property_gallery', $join)) {

            $this->db->join('property_gallery', 'property.property_id=property_gallery.property_id');


        }
        if (in_array('video_gallery', $join)) {
            $this->db->join('video_gallery', 'property.property_id=video_gallery.property_id');
        }

        if (in_array('file_gallery', $join)) {
            $this->db->join('file_gallery', 'property.property_id=file_gallery.property_id');
        }

        if (in_array('comment', $join)) {
            if ($comment_status == 1) {
                $this->db->where('comment.status in (' . $comment_status . ')');

            }
            $this->db->where("comment.parent_id = '0' ");
        }


        if ($comment_id_for_parent > 0) {
            $this->db->where("comment.comment_id != '" . $comment_id_for_parent . "' ");
        }

        if (in_array('video_gallery', $join)) {
            $this->db->where("video_gallery.media_video_title !='' ");

            if ($property_owner_id != $this->session->userdata('user_id')) {
                $this->db->where("video_gallery.status", '1');
            }
        }
        if (in_array('property_gallery', $join)) {
            $this->db->where("property_gallery.image !='' ");

            if ($property_owner_id != $this->session->userdata('user_id')) {
                $this->db->where("property_gallery.status", '1');
            }
        }
        if (in_array('file_gallery', $join)) {
            $this->db->where("file_gallery.file_path !='' ");
            if ($property_owner_id != $this->session->userdata('user_id')) {
                $this->db->where("file_gallery.status", '0');
            }
        }
        if ($is_status != '') {
            $this->db->where('property.status in (' . $is_status . ')');
        }

        if ($user_id > 0) {
            $this->db->where('property.user_id', $user_id);
        }
        if ($property_id > 0) {
            $this->db->where('property.property_id', $property_id);
        }
        if (is_array($group)) {
            foreach ($group as $key => $grp) {
                $this->db->group_by($grp);
            }
        }

        if (is_array($order)) {
            foreach ($order as $key => $val) {
                $this->db->order_by($key, $val);
            }
        }
        if ($limit > 0 || $offset > 0) $this->db->limit($limit, $offset);

        $query = $this->db->get();

        if ($count == 'yes') {
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else {
                return 0;
            }
        }
    }




    function get_donations($id)
    {

        $query2 = "select * from transaction where equity_id='" . $id . "' order by transaction_id desc";
        $s_result2 = $this->db->query($query2);


        if ($s_result2->num_rows() > 0) {
            return $s_result2->result_array();
        }
        return 0;
    }

    function equity_activities($equity_id)
    {
        $sql = "select user_activity.* , user.user_id,user.profile_slug,user.user_name,user.last_name,user.image from user_activity  inner join user  on user.user_id=user_activity.user_id    where user_activity.campaign_id='" . $equity_id . "' order by `read` asc,datetime desc ";
        $query = $this->db->query($sql);
        //echo $this->db->last_query(); 
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return 0;
    }

    function dynamic_update()

    {
        $CI =& get_instance();
        $base_path = $CI->config->slash_item('base_path');

        if ($_FILES) {
            if ($_FILES['image_load']['name'] != '') {
                $this->load->library('upload');
                $rand = rand(0, 100000);

                $_FILES['userfile']['name'] = $_FILES['image_load']['name'];
                $_FILES['userfile']['type'] = $_FILES['image_load']['type'];
                $_FILES['userfile']['tmp_name'] = $_FILES['image_load']['tmp_name'];
                $_FILES['userfile']['error'] = $_FILES['image_load']['error'];
                $_FILES['userfile']['size'] = $_FILES['image_load']['size'];

                $imagename = OnlyUpload($_FILES);

                $cat_image = $imagename['orig'];

            } else {

                if ($this->input->post('dynamic_image_image') != '') {
                    $cat_image = $this->input->post('dynamic_image_image');
                }
            }
        }


        $data = array(

            'dynamic_image_title' => $this->input->post('slider_name'),
            'dynamic_image_paragraph' => $this->input->post('slider_content'),
            'dynamic_image_image' => $cat_image,
            'color_picker' => $this->input->post('color_picker'),
            'small_text' => $this->input->post('small_text'),
            'color_picker_content' => $this->input->post('color_picker_content'),
            'link' => $this->input->post('link'),
            'link_name' => $this->input->post('link_name'),
            'font_type_id' => $this->input->post('font_type_name'),
            'active' => $this->input->post('active')
        );

        $this->db->where('dynamic_slider_id', $this->input->post('dynamic_id'));

        $this->db->update('dynamic_slider', $data);

        $query = $this->db->query("select max(view_order) as latest_order from slider_setting ");

        if ($query->num_rows() > 0) {

            $result = $query->row();

            $new_view_order = $result->latest_order + 1;

        } else {

            $new_view_order = 1;

        }

        $data_slider = array(

            'slider_id' => $this->input->post('dynamic_id'),
            'view_order' => $new_view_order

        );

        $this->db->where('slider_id', $this->input->post('dynamic_id'));

        $this->db->update('slider_setting', $data_slider);


    }

    function get_one_dynamic($id)
    {
        $query = $this->db->get_where('dynamic_slider', array('dynamic_slider_id' => $id));
        return $query->row_array();

    }


    function dynamic_insert()

    {
        $CI =& get_instance();
        $base_path = $CI->config->slash_item('base_path');
        $cat_image = '';
        if ($_FILES) {
            if ($_FILES['image_load']['name'] != '') {
                $this->load->library('upload');
                $rand = rand(0, 100000);

                $_FILES['userfile']['name'] = $_FILES['image_load']['name'];
                $_FILES['userfile']['type'] = $_FILES['image_load']['type'];
                $_FILES['userfile']['tmp_name'] = $_FILES['image_load']['tmp_name'];
                $_FILES['userfile']['error'] = $_FILES['image_load']['error'];
                $_FILES['userfile']['size'] = $_FILES['image_load']['size'];

                $imagename = OnlyUpload($_FILES);

                $cat_image = $imagename['orig'];

            } else {
                if ($this->input->post('dynamic_image_image') != '') {
                    $cat_image = $this->input->post('dynamic_image_image');
                }
            }
        }


        $data = array(

            'dynamic_image_title' => $this->input->post('slider_name'),
            'dynamic_image_paragraph' => $this->input->post('slider_content'),
            'dynamic_image_image' => $cat_image,
            'small_text' => $this->input->post('small_text'),
            'color_picker' => $this->input->post('color_picker'),
            'color_picker_content' => $this->input->post('color_picker_content'),
            'link' => $this->input->post('link'),
            'link_name' => $this->input->post('link_name'),
            'font_type_id' => $this->input->post('font_type_name'),
            'active' => $this->input->post('active')
        );

        $this->db->insert('dynamic_slider', $data);
        $slider_id = $this->db->insert_id();

        $query = $this->db->query("select max(view_order) as latest_order from slider_setting ");

        if ($query->num_rows() > 0) {

            $result = $query->row();

            $new_view_order = $result->latest_order + 1;

        } else {

            $new_view_order = 1;

        }
        $data_slider = array(

            'slider_id' => $slider_id,
            'view_order' => $new_view_order

        );

        $this->db->insert('slider_setting', $data_slider);


    }

    function fonts()
    {
        $query = $this->db->get('font_type');

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return 0;
    }

     /*

     Function name :InvestorRunningProcessHistory()

     Parameter :none

     Return : none

     Use : fetch the data of running investment

     */

    function InvestorRunningProcessHistory($count='no')
    {

        $query = $this->db->query("SELECT investment_process.*,campaign.property_id,campaign.investment_close_date,campaign.lead_investor_id as campaign_owner_id,campaign.property_id ,user.user_id,user.user_name,user.last_name,user.image,user.profile_slug FROM `investment_process` join campaign on campaign.campaign_id=investment_process.campaign_id join user on user.user_id=investment_process.user_id where campaign.status in ('2','3') and investment_process.status = '0' order by DATEDIFF(investment_process.created_date,campaign.investment_close_date) asc ");
     
          if ($count == 'yes') {
            return $query->num_rows();
        	} else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else {
                return 0;
            }
        }

    }

    function getPhases(){
        return $this->db->get('phases')->result_array(); 
    }

    function addPhases($id=0,$insert){
        $this->db->where('equity_id',$id);
        $this->db->delete('equity_phases');

        $this->db->insert('equity_phases',$insert);
        return true;
    }
      /*

     Function name :add_tax_relief_data()

     Parameter :$id=id

     Return : none

     Use : function to insert record and update record into table.

   */

    function add_tax_relief_data($id = '', $array1 = array())
    {

        if ($id == '') {
            $this->db->insert('investment_tax_relief', $array1);
            $msg = INSERT;
            return $msg;
        } else {
            $this->db->where('id', $this->input->post('id'));
            $this->db->update('investment_tax_relief', $array1);
            $msg = UPDATE_MSG;
            return $msg;
        }

    }

    function GetAllUserProperties($user_id = 0, $property_id = '', $is_status = '', $joinarr = array('user'), $limit = 100, $order = array('property_id' => 'desc'), $offset = 0, $count = 'no')
    {

        //default Equity fields
        $selectfields = 'property.*';
        //if require to join user
        if (in_array('user', $joinarr)) {
            $selectfields .= ',user.user_id,user.user_name,user.last_name,user.address, user.fb_uid, user.facebook_wall_post, user.fb_access_token,user.image,user.email,user.user_website,user.profile_slug,user.user_occupation,user.user_type,user.company_name';
        }
       
       
      
        if ($user_id > 0) 
        {
            $selectfields .= ',invite_members.email as inviteemail';
        }
       

        $this->db->select($selectfields);
        $this->db->from('property');


        //if require to join user
        if (in_array('user', $joinarr)) {
            $this->db->join('user', 'property.user_id=user.user_id','left');

        }
        if (in_array('property_type', $joinarr)) {
            $this->db->join('property_type', 'property.user_id=property_type.user_id');
        }
       
        if ($user_id > 0) {
            $this->db->join('invite_members', 'property.property_id=invite_members.property_id', 'left');
        }
        //check all property status
        if ($is_status != '') {

           
            $this->db->where('property.status in (' . $is_status . ')');
          
        }
        // $this->db->where('property.property_id_api','');

       $this->db->where('property.property_id not in ( select property_id from campaign)');
        //check property id
        if ($property_id > 0 or $property_id != '') {
            if (is_numeric($property_id)) {
                $this->db->where('property.property_id', $property_id);
                $this->db->or_where('property.property_url', $property_id);
            } else {
                $this->db->where('property.property_url', $property_id);
            }
        }

        
        //check user_id
        if ($user_id > 0) {
            // $this->db->where('user.user_id',$user_id);
            $where = '';
            $where .= "( `user`.`user_id` = '" . $user_id . "' ";
            $sql = " ( select i.email from invite_members as i inner join property as p on i.property_id=p.property_id inner join user as u  on u.email=i.email where i.status=1 and i.admin=1 and u.user_id= '" . $user_id . "' )";
            //$sql=" ( invite_members.status=1 and invite_members.admin=1 and  invite_members.email= user.email )";
            $where .= " or `invite_members`.`email` in " . $sql . " )";

            $this->db->where($where);

        }


        if (is_array($order)) {
            foreach ($order as $key => $val) {
                $this->db->order_by($key, $val);
            }
        }

        if ($user_id > 0) {
            $this->db->group_by('property.property_id');
        }
        if ($limit > 0) $this->db->limit($limit, $offset);

        $query = $this->db->get();

        // echo $this->db->last_query();die;
      
        if ($count == 'yes') {
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else {
                return 0;
            }
        }

    }

    function add_property($property=array()){
        $this->db->insert('property',$property);
        return $this->db->insert_id();
    }

    function update_property($property_id,$update){
        $this->db->where('property_id_api',$property_id);
        $this->db->update('property',$update);
    }

    function check_locality($locality_id=0)
    {
        return $this->db->select('localityId')
                ->from('locality_detail')
                ->where('localityId',$locality_id)
                ->get()
                ->num_rows();
    }

    function check_property($property_id=0){
         return $this->db->select('property_id')
                ->from('property')
                ->where('property_id_api',$property_id)
                ->get()
                ->num_rows();
    }

    function check_property_gallery($property_id=0)
    {
        return $this->db->select('*')
               ->from('property_gallery')
               ->where('property_id',$property_id)
               ->get()
               ->result_array();
    }

    function get_properties($property_id = 0, $status = array(0,1))
    {
        return $this->db->select('*')
                ->from('property')
                ->where('property_id_api',$property_id)
                ->where_in('status',$status)
                ->get()
                ->row_array();
    }

    function delete_gallery_images($property_id=0)
    {
        $this->db->where('property_id',$property_id);
        $this->db->delete('property_gallery');
    }

    function check_suburb($id=0){
        return $this->db->select('id, postcode_id')
        ->from('suburbs')
        ->where('postcode_id',$id)
        ->get()
        ->row_array();
    }

    function add_suburb($insert){
       $this->db->insert('suburbs',$insert);
       return $this->db->insert_id();
    }

    function check_postcode($postcode){
        return $this->db->select('id')
        ->from('postcodes_geo')
        ->where('postcode',$postcode)
        ->get()
        ->num_rows();
    }

    function add_postcode($insert){
        $this->db->insert('postcodes_geo', $insert);
        return $this->db->last_query(); 
    }

    function add_locality($insert){
        $this->db->insert('locality_detail', $insert);
        return $this->db->last_query(); 
    }

    function get_country($name=''){
        return $this->db->select('country_id')
        ->from('country')
        ->where('LOWER(country_name)',strtolower($name))
        ->get()
        ->row_array();
    }
}

?>
