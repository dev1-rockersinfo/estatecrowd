<?php

/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
class Home_model extends CI_Model
{

    function __construct()

    {

        parent::__construct();


    }


    /*

     Function name :get_one_message_setting()

     Parameter :none

     Return : none

     Use : function to fetch message settings.

     */

    function get_one_message_setting()

    {

        $query = $this->db->get_where('message_setting');

        return $query->row_array();

    }


    /*

     Function name :table_insert()

     Parameter : $table=table name, $data=data array of values you want to insert into table.

     Return : none

     Use : Common function for inserting values into database.

     */

    function table_insert($table, $data)

    {

        $this->db->insert($table, $data);

    }


    /*

     Function name :AddUpdateData()

     Parameter : $table=table name, $data=data array of values you want to insert into table.

     Return : none

     Use : Common function for inserting and updating values into database.

     */

    function AddUpdateData($table_name = '', $insert_array = array(''), $id = '', $field_name = '')

    {

        if (!$id) {

            $this->db->insert($table_name, $insert_array);

            return $this->db->insert_id();

        } else {

            $this->db->where($field_name, $id);

            $this->db->update($table_name,$insert_array);

        }

    }


    /*

     Function name :table_update()

     Parameter :$key_name=Field name for where clause,$key_value=Field value, $table=table name, $data=data array of values you want to insert into table.

     Return : none

     Use : Common function for updating values into database.

     */

    function table_update($key_name, $key_value, $table, $data)

    {

        $this->db->where($key_name, $key_value);

        $query = $this->db->update($table, $data);

        return true;

    }


    /*

     Function name :get_one_tabledata()

     Parameter :$table=table name, $where=array of values whose information or details you want to get.

     Return : none

     Use : Common function for getting single record using where clause.

     */

    function get_one_tabledata($table, $where = array())

    {

        $query = $this->db->get_where($table, $where);

        return $query->row_array();

    }


    /*

     Function name :is_login()

     Parameter : none.

     Return : return 1 if true or return 0 when false

     Use : to check admin login details on login.

     */

    function is_login()


    {

        $this->load->helper('cookie');


        $username = $this->input->post('username');


        $password = md5($this->input->post('password'));


        $query = $this->db->get_where('admin', array('username' => $username, 'password' => $password, 'active' => '1'));


        if ($query->num_rows() > 0) {


            $admin = $query->row_array();


            $admin_id = $admin['admin_id'];

            $email = $admin['email'];

            $data = array(


                'admin_id' => $admin_id,


                'username' => $username,

            );


            if ($this->input->post('remember') == 'on') {

                $cookie = array(

                    'name' => 'admin_user',

                    'value' => $this->input->post('username') . ' ' . md5($this->input->post('password')) . ' ' . $this->input->post('remember'),

                    'expire' => '15000000',

                    'prefix' => ''

                );


                $this->session->set_userdata($data);


                $this->input->set_cookie($cookie);

            }

            if ($this->input->post('remember') == '') {


                delete_cookie("admin_user");

            }


            $this->session->set_userdata($data);


            $login_add = $this->db->query("insert into admin_login(`admin_id`,`login_ip`,`login_date`)values('" . $admin_id . "','" . $_SERVER['REMOTE_ADDR'] . "','" . date('Y-m-d H:i:s') . "')");

            return "1";


        } else {


            return "0";


        }

    }


    /*

     Function name :forgot_email()

     Parameter : none.

     Return : return 1 if email exists or return 0 when email not found else 2 if record not found

     Use : to send forgot password email to admin.

     */

    function forgot_email()
    {

        $email = $this->input->post('email');

        $query = $this->db->get_where('admin', array('email' => $email));


        if ($query->num_rows() > 0) {
            $row = $query->row();

            if ($row->email != "") {

                $email_template = $this->db->query("select * from `email_template` where task='Forgot Password Request'");

                $email_temp = $email_template->row();
                $email_setting = $this->db->query("select * from `email_setting` where email_setting_id='1'");
                $email_set = $email_setting->row();
                $email_address_from = $email_temp->from_address;
                $email_address_reply = $email_temp->reply_address;
                $email_subject = $email_temp->subject;
                $email_message = $email_temp->message;
                $username = $row->username;
                $password = $row->password;
                $forgot_unique = $row->forgot_unique_code;


                $login_link = '<a href="' . site_url('admin/home/reset_password/' . $forgot_unique) . '">' . site_url('admin/home/reset_password/' . $forgot_unique) . '</a>';
                //$this->load->library('encrypt');
                //$pass = $this->encrypt->decode($password);
                //echo $pass; die;
                $email = $row->email;
                //$login_link=base_url().'admin/home/index';

                $email_message = str_replace('{break}', '\n\n', $email_message);
                $email_message = str_replace('{user_name}', $username, $email_message);
                //$email_message=str_replace('{password}',$password,$email_message);
                //$email_message=str_replace('{email}',$email,$email_message);
                $email_message = str_replace('{login_link}', $login_link, $email_message);
                $str = $email_message;

                email_send($email_address_from, $email_address_reply, $email, $email_subject, $str);

                return '1';

            } else {


                return '0';


            }


        } else {

            return 2;

        }

    }

    function checkForgetUniqueCode($forgot_unique_code = null)
    {
        $site_setting = site_setting();
        $query = $this->db->get_where('admin', array('forgot_unique_code' => $forgot_unique_code));

        if ($query->num_rows() > 0) {
            $row = $query->row_array();
            return $row['admin_id'];
        } else {
            return 'not_sent';
        }
    }

    function setNewPassword($user_id, $sd = array())
    {
        $rest_data = array(
            'forgot_unique_code' => '',
            'request_date' => '',
            'password' => SecureShowData($sd['password'])
        );

        $this->db->where('admin_id', $user_id);
        $this->db->update('admin', $rest_data);

        //print_r($this->db->last_query()); die;
        if (mysql_affected_rows() > 0) {
            return mysql_affected_rows();
        } else {
            return '';
        }
    }

    /*

     Function name :get_all_users()

     Parameter : $active=user status, $limit,$joinarr=to join project category or affiliate req table.

     Return : returns result if records found else 0.

     Use : used to fetch user data.

     */

    function get_all_users($active = '', $limit = 10, $joinarr = array(), $order = array('user_id' => 'desc'), $count = '')
    {


        $selectfields = 'user.signup_ip,user.active,user.country,user.state,user.date_added,user.city,user.user_id,user.user_name,user.last_name,
            
            user.address, user.fb_uid, user.facebook_wall_post, user.fb_access_token,user.image,user.email,user.user_website,user.profile_slug';


        $this->db->select($selectfields);

        $this->db->from('user');


        if ($active != '') {

            if ($active == 1)

                $this->db->where('user.active', 1);

            if ($active == 'inactive') {
                $this->db->where('user.active IS NULL');
                $this->db->or_where('user.active', 0);
            }

        }


        if ($limit > 0)

            $this->db->limit($limit);

        if (is_array($order)) {

            foreach ($order as $key => $val) {

                $this->db->order_by($key, $val);

            }

        }

        $query = $this->db->get();

        /*	echo $this->db->last_query();

         echo '<br><br>';die();

         */
        if ($count == 'yes') {

            return $query->num_rows();

        } else {

            if ($query->num_rows() > 0) {

                return $query->result_array();

            } else {

                return 0;

            }

        }

    }



    //------Common Equity function-------
    /*
    Function name :GetAllEquities()
    Parameter : user_id,equity_id,is_status,joinarr,limit,order
    Return : array of all Equities
    Use : Fetch all Equity particular Criteria
    
    Cases:
    
    case 1 : if user grater than zero then it fetch all the Equity with particular user id means it returns all Equity of  particular company owner.
    
    case 2 : if Equity grater than zero then it return particular Equity.
    
    
    case 3 : if Status equal to 0 than it returns Draft Equities.
             if Status equal to 1 than it returns Pending Equities.
             if Status equal to 2 than it returns Active Equities.
             if Status equal to 3 than it returns Success Equities.
             if Status equal to 4 than it returns unsuccess Equities.
             if Status equal to 5 than it returns Declined Equities.
             
             or
             
             if Status equal 0,1 than it returns both type of Equities.(Same for all others).
             
    case 4 : if all Equity will be sorted by given order variable values. 
            
            
    */

    function GetAllCampaigns($user_id = 0, $campaign_id = '',  $is_status = '', $joinarr = array('user'), $limit = 10, $order = array('campaign_id' => 'desc'), $offset = 0, $count = 'no')
    {

        //default Equity fields
        $selectfields = 'campaign.*';


        //if require to join user

        if (in_array('user', $joinarr)) {

            $selectfields .= ',user.user_id,user.user_name,user.last_name,user.address, user.fb_uid, user.facebook_wall_post, user.fb_access_token,user.image,user.email,user.user_website,user.profile_slug';

        }

         $selectfields .= ',property.property_address,property.property_url,property.property_id';

        //used to fetch single transaction data

        if (in_array('single_transaction', $joinarr)) {

            $selectfields .= ',transaction.amount as donation_amount,transaction.pay_fee,transaction.user_id,transaction.email,transaction.host_ip';

        }


        //if require to join transaction for total project donation data

        if (in_array('transaction', $joinarr)) {

            $selectfields .= ',sum(transaction.amount) as total_amount,sum(transaction.listing_fee) as total_listing_fee,sum(transaction.pay_fee) as total_pay_fee';

        }

        $this->db->select($selectfields);


        $this->db->from('campaign');


        //if require to join user
        if (in_array('user', $joinarr)) {
            $this->db->join('user', 'campaign.user_id=user.user_id');

        }
         $this->db->join('property', 'campaign.property_id=property.property_id');
        //if require to join transaction

        if (in_array('transaction', $joinarr) || in_array('single_transaction', $joinarr)) {

            $this->db->join('transaction', 'campaign.campaign_id= transaction.campaign_id');


        }

        //check all equity status
        if ($is_status != '') {
            $this->db->where('campaign.status in (' . $is_status . ')');
        }


        if (is_array($order)) {
            foreach ($order as $key => $val) {
                $this->db->order_by($key, $val);
            }
        }


        if (in_array('transaction', $joinarr)) {

            $this->db->group_by('campaign.campaign_id');

        }
        if ($limit > 0) $this->db->limit($limit, $offset);

        $query = $this->db->get();
        //  echo "<br><br><br>";
        //          echo $this->db->last_query();
        // die;
        //              echo "<br><br><br>";
        if ($count == 'yes') {
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else {
                return 0;
            }
        }

    }

    function getTags()
    {

        $this->db->select('tags.*');
        $this->db->from('tags');

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $ret = $query->result_array();
            return $ret;
        }
        return 0;
    }

    function getTags_interest()
    {

        $this->db->select('tag_interest.*');
        $this->db->from('tag_interest');

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $ret = $query->result_array();
            return $ret;
        }
        return 0;
    }

    function getTags_skill()
    {

        $this->db->select('tag_skill.*');
        $this->db->from('tag_skill');

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $ret = $query->result_array();
            return $ret;
        }
        return 0;
    }

    //===============Not used in this model================

    function get_total_pending_project()


    {


        $start = date("Y") . '-' . date("m") . '-1';


        $month = date("m");


        $year = date("Y");


        $month++;


        if ($month > 12) {


            $month = 1;


            $year++;


        }


        $end = $year . '-' . $month . '-1';


        $this->db->select('*');


        $this->db->from('equity');


        $this->db->where('(end_date between "' . $start . '" AND "' . $end . '") AND (status = "1") ');


        $query = $this->db->get();


        if ($query->num_rows() > 0) {


            return $query->num_rows();


        }


        return 0;


    }


    function get_total_completed_project()


    {


        $start = date("Y") . '-' . date("m") . '-1';


        $month = date("m");


        $year = date("Y");


        $month++;


        if ($month > 12) {


            $month = 1;


            $year++;


        }


        $end = $year . '-' . $month . '-1';


        $this->db->select('*');


        $this->db->from('equity');

        $this->db->where('equity.status in (3,4)');

        $this->db->where('(end_date between "' . $start . '" AND "' . $end . '") ');


        $query = $this->db->get();


        if ($query->num_rows() > 0) {


            return $query->num_rows();


        }


        return 0;


    }


    /*Start of transaction tab data for table used in old admin theme*/


    function get_total_earned()


    {

        $this->db->select('SUM(transaction.amount) as total_pipeline,SUM(transaction.pay_fee) as commission');


        $this->db->from('transaction');


        $this->db->join('equity', 'transaction.equity_id=equity.equity_id');


        $this->db->where('equity.status in (3,4)');


        $query = $this->db->get();


        if ($query->num_rows() > 0) {


            $res = $query->result_array();


            $ret['funding'] = $res[0]['total_pipeline'];


            $ret['commission'] = $res[0]['commission'];


            return $ret;


        }


        return 0;


        /*	$this->db->select('SUM(listing_fee) as total_listing_fee,SUM(pay_fee) as total_pay_fee,SUM(amount) as total_amount');



         $this->db->from('transaction');



         $query = $this->db->get();



         if ($query->num_rows() > 0) {



         $res = $query->result_array();



         $ret['commission'] = ($res[0]['total_listing_fee'] + $res[0]['total_pay_fee']);



         $ret['funding'] = $res[0]['total_amount'];



         return $ret;



         }



         return 0;

         */

    }


    function get_total_earned_month()


    {

        $start = date("Y") . '-' . date("m") . '-1';


        $month = date("m");


        $year = date("Y");


        $month++;


        if ($month > 12) {


            $month = 1;


            $year++;


        }


        $end = $year . '-' . $month . '-1';


        $this->db->select('SUM(transaction.amount) as total_pipeline,SUM(transaction.pay_fee) as commission');


        $this->db->from('transaction');


        $this->db->join('equity', 'transaction.equity_id=equity.equity_id');


        $this->db->where('(status in (3,4)) AND (transaction_date_time between "' . $start . '" AND "' . $end . '")');


        $query = $this->db->get();


        if ($query->num_rows() > 0) {


            $res = $query->result_array();


            $ret['funding'] = $res[0]['total_pipeline'];


            $ret['commission'] = $res[0]['commission'];


            return $ret;


        }


        return 0;


    }


    function get_total_earned_week()


    {

        $tm = time();


        $day_of_week = date('l', $tm);


        while ($day_of_week != 'Monday') {


            $tm -= 24 * 3600;


            $day_of_week = date('l', $tm);


        }


        $starttemp = mktime(0, 0, 0, date('m', $tm), date('d', $tm), date('Y', $tm));


        $endtemp = $starttemp + (7 * 24 * 3600) - 1;


        $start = date("Y-m-d", $starttemp);


        $end = date("Y-m-d", $endtemp);


        $this->db->select('SUM(transaction.amount) as total_pipeline,SUM(transaction.pay_fee) as commission');


        $this->db->from('transaction');


        $this->db->join('equity', 'transaction.equity_id=equity.equity_id');


        $this->db->where('(status in (3,4)) AND (transaction_date_time between "' . $start . '" AND "' . $end . '")');


        $query = $this->db->get();


        if ($query->num_rows() > 0) {


            $res = $query->result_array();


            $ret['funding'] = $res[0]['total_pipeline'];


            $ret['commission'] = $res[0]['commission'];

            return $ret;


        }


        return 0;

    }


    function get_total_earned_today()


    {

        $start = date("Y-m-d");


        $end = date("Y-m-d");


        $this->db->select('SUM(transaction.amount) as total_pipeline,SUM(transaction.pay_fee) as commission');


        $this->db->from('transaction');


        $this->db->join('equity', 'transaction.equity_id=equity.equity_id');


        $this->db->where('(equity.status in (3,4)) AND (DATE(transaction_date_time)="' . $start . '")');


        $query = $this->db->get();


        if ($query->num_rows() > 0) {


            $res = $query->result_array();


            $ret['funding'] = $res[0]['total_pipeline'];


            $ret['commission'] = $res[0]['commission'];

            return $ret;


        }


        return 0;


    }


    function get_total_lost()


    {


        $this->db->select('SUM(transaction.amount) as total_lost,SUM(transaction.pay_fee) as commission');


        $this->db->from('transaction');


        $this->db->join('equity', 'transaction.equity_id=equity.equity_id');


        $this->db->where("(status='5') AND (equity.goal > equity.amount_get) AND (end_date<'" . date('Y-m-d') . "')");


        $query = $this->db->get();


        if ($query->num_rows() > 0) {


            $res = $query->result_array();


            $ret['lost'] = $res[0]['total_lost'];


            $ret['commission'] = $res[0]['commission'];

            return $ret;


        }


        return 0;


    }


    function get_total_lost_month()


    {


        $start = date("Y") . '-' . date("m") . '-1';


        $month = date("m");


        $year = date("Y");


        $month++;


        if ($month > 12) {


            $month = 1;


            $year++;


        }


        $end = $year . '-' . $month . '-1';


        $this->db->select('SUM(transaction.amount) as total_lost,SUM(transaction.pay_fee) as commission');


        $this->db->from('transaction');


        $this->db->join('equity', 'transaction.equity_id=equity.equity_id');


        $this->db->where('(transaction_date_time between "' . $start . '" AND "' . $end . '") AND (equity.goal > equity.amount_get) AND (status="5")');


        $query = $this->db->get();


        if ($query->num_rows() > 0) {


            $res = $query->result_array();


            $ret['lost'] = $res[0]['total_lost'];


            $ret['commission'] = $res[0]['commission'];

            return $ret;


        }


        return 0;


    }


    function get_total_lost_week()


    {


        $tm = time();


        $day_of_week = date('l', $tm);


        while ($day_of_week != 'Monday') {


            $tm -= 24 * 3600;


            $day_of_week = date('l', $tm);


        }


        $starttemp = mktime(0, 0, 0, date('m', $tm), date('d', $tm), date('Y', $tm));


        $endtemp = $starttemp + (7 * 24 * 3600) - 1;


        $start = date("Y-m-d", $starttemp);


        $end = date("Y-m-d", $endtemp);


        $this->db->select('SUM(transaction.amount) as total_lost,SUM(transaction.pay_fee) as commission');


        $this->db->from('transaction');


        $this->db->join('equity', 'transaction.equity_id=equity.equity_id');


        $this->db->where('(transaction_date_time between "' . $start . '" AND "' . $end . '") AND (equity.goal > equity.amount_get)  AND (active="5")');


        $query = $this->db->get();


        if ($query->num_rows() > 0) {


            $res = $query->result_array();


            $ret['lost'] = $res[0]['total_lost'];


            $ret['commission'] = $res[0]['commission'];

            return $ret;


        }


        return 0;


    }


    function get_total_lost_today()


    {


        $start = date("Y-m-d");


        $end = date("Y-m-d");


        $this->db->select('SUM(transaction.amount) as total_lost,SUM(transaction.pay_fee) as commission');


        $this->db->from('transaction');


        $this->db->join('equity', 'transaction.equity_id=equity.equity_id');


        $this->db->where('(DATE(transaction_date_time)="' . $start . '") AND (equity.goal > equity.amount_get)  AND (status="5")');


        $query = $this->db->get();


        if ($query->num_rows() > 0) {


            $res = $query->result_array();


            $ret['lost'] = $res[0]['total_lost'];


            $ret['commission'] = $res[0]['commission'];


            return $ret;


        }


        return 0;


    }


    function get_total_pipeline()


    {


        $this->db->select('SUM(transaction.amount) as total_pipeline,SUM(transaction.pay_fee) as commission');


        $this->db->from('transaction');


        $this->db->join('equity', 'transaction.equity_id=equity.equity_id');


        $this->db->where("(status='2') AND (equity.amount > equity.amount_get)");


        $query = $this->db->get();


        if ($query->num_rows() > 0) {


            $res = $query->result_array();


            $ret['pipeline'] = $res[0]['total_pipeline'];


            $ret['commission'] = $res[0]['commission'];

            return $ret;


        }


        return 0;


    }


    function get_total_pipeline_month()


    {


        $start = date("Y") . '-' . date("m") . '-1';


        $month = date("m");


        $year = date("Y");


        $month++;


        if ($month > 12) {


            $month = 1;


            $year++;


        }


        $end = $year . '-' . $month . '-1';


        $this->db->select('SUM(transaction.amount) as total_pipeline,SUM(transaction.pay_fee) as commission');


        $this->db->from('transaction');


        $this->db->join('equity', 'transaction.equity_id=equity.equity_id');


        $this->db->where('(status="2") AND (transaction_date_time between "' . $start . '" AND "' . $end . '") AND (equity.goal > equity.amount_get)');


        $query = $this->db->get();


        if ($query->num_rows() > 0) {


            $res = $query->result_array();


            $ret['pipeline'] = $res[0]['total_pipeline'];


            $ret['commission'] = $res[0]['commission'];

            return $ret;


        }


        return 0;


    }


    function get_total_pipeline_week()


    {


        $tm = time();


        $day_of_week = date('l', $tm);


        while ($day_of_week != 'Monday') {


            $tm -= 24 * 3600;


            $day_of_week = date('l', $tm);


        }


        $starttemp = mktime(0, 0, 0, date('m', $tm), date('d', $tm), date('Y', $tm));


        $endtemp = $starttemp + (7 * 24 * 3600) - 1;


        $start = date("Y-m-d", $starttemp);


        $end = date("Y-m-d", $endtemp);


        $this->db->select('SUM(transaction.amount) as total_pipeline,SUM(transaction.pay_fee) as commission');


        $this->db->from('transaction');


        $this->db->join('equity', 'transaction.equity_id=equity.equity_id');


        $this->db->where('(status="2") AND (transaction_date_time between "' . $start . '" AND "' . $end . '") AND (equity.goal > equity.amount_get)');


        $query = $this->db->get();


        if ($query->num_rows() > 0) {


            $res = $query->result_array();


            $ret['pipeline'] = $res[0]['total_pipeline'];


            $ret['commission'] = $res[0]['commission'];

            return $ret;


        }


        return 0;


    }


    function get_total_pipeline_today()


    {


        $start = date("Y-m-d");


        $end = date("Y-m-d");


        $this->db->select('SUM(transaction.amount) as total_pipeline,SUM(transaction.pay_fee) as commission');


        $this->db->from('transaction');


        $this->db->join('equity', 'transaction.equity_id=equity.equity_id');


        $this->db->where('(status="2") AND (DATE(transaction_date_time)="' . $start . '") AND (equity.goal > equity.amount_get)');


        $query = $this->db->get();


        if ($query->num_rows() > 0) {


            $res = $query->result_array();


            $ret['pipeline'] = $res[0]['total_pipeline'];


            $ret['commission'] = $res[0]['commission'];

            return $ret;


        }


        return 0;


    }



    /*End of transaction tab data for table used in old admin theme*/


    /*	  function get_project_comment_result($id)



     {



     $this->db->select('comment.*,comment.user_id as comment_user_id,comment.date_added as date_added_comment,comment.status as comment_status,project.*,user.*');



     $this->db->from('comment');



     $this->db->join('project', 'comment.project_id= project.project_id');



     //added to join user table

     $this->db->join('user', 'comment.user_id= user.user_id');





     $this->db->where('project.project_id',$id);



     // $this->db->limit($limit,$offset);



     $query = $this->db->get();



     if ($query->num_rows() > 0) {



     return $query->result();

     }



     return 0;



     }

     */

    function my_account_update()


    {


        $data = array(


            'username' => $this->input->post('username'),


            'password' => md5($this->input->post('password')),


            'email' => $this->input->post('email'),


        );


        $this->db->where('admin_id', $this->input->post('admin_id'));


        $this->db->update('admin', $data);


        $admin_detail = $this->db->query("select * from admin where admin_id='" . $this->input->post('admin_id') . "'");


        $admin = $admin_detail->row();


        if ($admin->admin_type == '1') {


            $get_user = $this->db->query("select * from user where is_admin='1'");


            if ($get_user->num_rows() > 0) {


                $user = $get_user->row();


                $user_id = $user->user_id;


                $data = array(


                    'user_name' => $this->input->post('username'),


                    'password' => md5($this->input->post('password')),


                    'email' => $this->input->post('email'),


                );


                $this->db->where('user_id', $user_id);


                $this->db->update('user', $data);

            } else {

                $insert = $this->db->query("insert into user(user_name,email,password,active,signup_ip,date_added,is_admin)values('" . $this->input->post('username') . "','" . $this->input->post('email') . "','" . md5($this->input->post('password')) . "','1','" . $_SERVER['REMOTE_ADDR'] . "','" . date('Y-m-d') . "','1')");

            }

        }

    }


    function get_my_account($aid)


    {


        $query = $this->db->query("select * from admin where admin_id='" . $aid . "'");


        return $query->row_array();


    }

    function get_language($tablename, $where = array())
    {

        $query = $this->db->get_where($tablename, $where);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return 0;
    }

    function get_language_id($tablename, $language)
    {

        $query = $this->db->get_where($tablename, array('language_name' => $language));

        if ($query->num_rows() > 0) {
            $result = $query->row();
            return $result->language_id;

        }
        return 0;

    }

}


?>