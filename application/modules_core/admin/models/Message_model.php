<?php

/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
class Message_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }


    /*	function select_site_setting()
        {
            $query = $this->db->query("select * from site_setting");
            return $query->row_array();
        }

        function get_user_detail($id)
        {
            $query=$this->db->get_where("user",array("user_id"=>$id));
            return $query->row_array();
        }

        function get_total_message_count()
        {
            $query=$this->db->query("select * from message_conversation where reply_message_id = 0 order by message_id desc ");

            if ($query->num_rows() > 0) {
                return $query->num_rows();
            }
            return 0;
        }
    */
    /*
     Function name :get_message_result()
    Parameter :none
    Return : none
    Use : to get data or detail list of messages send by one user to another can be viewed by admin.
    */
    function get_message_result()
    {
        //$query=$this->db->query('select * from message_conversation where reply_message_id = 0 order by message_id desc limit '.$limit.' offset '.$offset.'');
        $query = $this->db->query('select * from message_conversation where reply_message_id = 0 order by message_id desc');
        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return 0;
    }
    
    
    
    /*
     Function name :get_message_thread()
    Parameter :$equity_id, $receiver_id, $msg_type of the conversation admin wants to view
    Return : none
    Use : to get details of single conversation between admin and users for inverstor process.
    */
    function get_message_thread($equity_id, $receiver_id, $msg_type)
    {
        $sql="select * from message_conversation where equity_id='" . $equity_id . "' and type=".$msg_type." and (sender_id = '".$receiver_id."' OR receiver_id='".$receiver_id."' )  order by message_id asc ";
        
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            return $query->result();
        }

        return 0;
    }
   
    
    /*
     Function name :get_unread_messsages()
    Parameter :$equity_id, $receiver_id, $msg_type of the unread conversation admin wants to view
    Return : none
    Use : to get details of single conversation between admin and users for inverstor process.
    */
    function get_unread_messsages($equity_id, $receiver_id, $msg_type)
    {
        $sql="select * from message_conversation where is_read=0 and equity_id='" . $equity_id . "' and admin_replay!='admin' and type=".$msg_type." and sender_id = " . $receiver_id ."  order by message_id asc ";
        
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            return $query->result();
        }

        return 0;
    }
    
    

    /*
     Function name :get_message_detail()
    Parameter :$message_id:message id of the conversation admin wants to view
    Return : none
    Use : to get details of single conversation between admin and users for inverstor process.
    */
    function get_message_detail($equity_id, $receiver_id, $msg_type)
    {
      $sql="select * from message_conversation where equity_id='" . $equity_id . "' and type=".$msg_type." and admin_replay='admin' and ( (sender_id =1 and receiver_id = " . $receiver_id .") OR (sender_id = " . $receiver_id ." and receiver_id = 1) ) order by message_id asc ";
        
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            return $query->result();
        }

        return 0;
    }

    /*
     Function name :get_one_message()
    Parameter :$message_id:message id of the conversation admin wants to view
    Return : none
    Use : to get information of single conversation.

    function get_one_message($message_id)
    {
        $query=$this->db->query("select * from message_conversation where message_id='".$message_id."'");

        if($query->num_rows()>0)
        {
            return $query->row();
        }

        return 0;
    }
    */

    function insert_project_profile_message($data)
    {

        if ($this->db->insert('message_conversation', $data)) {
              $message_id = $this->db->insert_id();
            return $message_id;
        } else {
            return 0;
        }
    }

    function get_email_notification($user_id)
    {

        $query = $this->db->query("select * from user_notification where user_id='" . $user_id . "'");

        if ($query->num_rows() > 0) {
            return $query->row();
        }

        return 0;

    }

    function get_message_admin_result()
    {
        //$query=$this->db->query('select * from message_conversation where reply_message_id = 0 order by message_id desc limit '.$limit.' offset '.$offset.'');
        $query = $this->db->query('select * from message_conversation where type in (1,2) and admin_replay = "admin" and reply_message_id = 0 order by message_id desc');
        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return 0;
    }
}

?>