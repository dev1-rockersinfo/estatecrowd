<?php

/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
class Learn_more_model extends CI_Model
{

    function Learn_more()
    {
        parent::Model();
        $this->load->helper('url');
        $this->load->database();
    }

    /*
     Function name :pages_insert()
    Parameter :none
    Return : none
    Use : Function used to add new learn more pages.
    */
    function pages_insert()
    {
        $CI =& get_instance();
        $base_path = $CI->config->slash_item('base_path');
        $icon_image = '';
        if ($_FILES['file_up']['name'] != '') {

            $this->load->library('upload');
            $rand = rand(0, 100000);

            $_FILES['userfile']['name'] = $_FILES['file_up']['name'];
            $_FILES['userfile']['type'] = $_FILES['file_up']['type'];
            $_FILES['userfile']['tmp_name'] = $_FILES['file_up']['tmp_name'];
            $_FILES['userfile']['error'] = $_FILES['file_up']['error'];
            $_FILES['userfile']['size'] = $_FILES['file_up']['size'];

            $config['file_name'] = $rand . 'icon';
            $config['upload_path'] = $base_path . 'upload/icon/';
            $config['allowed_types'] = 'jpg|jpeg|gif|png|bmp';

            $this->upload->initialize($config);
            if (!$this->upload->do_upload()) {
                $error = $this->upload->display_errors();
            }

            $picture = $this->upload->data();

            $icon_image = $picture['file_name'];


            if (is_file($base_path . 'upload/orig/' . $this->input->post('prev_icon'))) {

                if ($this->input->post('prev_icon') != 'no_img.jpg') {
                    $link2 = $base_path . 'upload/icon/' . $this->input->post('prev_icon');
                    unlink($link2);
                }

            }
        } else {
            if ($this->input->post('prev_icon') != '') {
                $icon_image = $this->input->post('prev_icon');
            }
        }
        $data = array(
            'pages_title' => $this->input->post('pages_title'),
            'description' => $this->input->post('description'),
            'slug' => $this->input->post('slug'),
            'active' => $this->input->post('active'),
            'meta_keyword' => $this->input->post('meta_keyword'),
            'meta_description' => $this->input->post('meta_description'),
            'header_bar' => $this->input->post('header_bar'),
            'footer_bar' => $this->input->post('footer_bar'),
            'left_side' => $this->input->post('left_side'),
            'right_side' => $this->input->post('right_side'),
            'external_link' => $this->input->post('external_link'),
            'sub_title' => $this->input->post('sub_title'),
            'icon_image' => $icon_image,
            'language_id' => $this->input->post('language'),
            'learn_category' => $this->input->post('learn_category')
        );
        $this->db->insert('learn_more', $data);
    }

    /*
     Function name :pages_update()
    Parameter :none
    Return : none
    Use : Function used to update or edit learn more pages.
    */
    function pages_update()
    {

        $CI =& get_instance();
        $base_path = $CI->config->slash_item('base_path');

        $icon_image = '';
        if ($_FILES['file_up']['name'] != '') {

            $this->load->library('upload');
            $rand = rand(0, 100000);

            $_FILES['userfile']['name'] = $_FILES['file_up']['name'];
            $_FILES['userfile']['type'] = $_FILES['file_up']['type'];
            $_FILES['userfile']['tmp_name'] = $_FILES['file_up']['tmp_name'];
            $_FILES['userfile']['error'] = $_FILES['file_up']['error'];
            $_FILES['userfile']['size'] = $_FILES['file_up']['size'];

            $config['file_name'] = $rand . 'icon';
            $config['upload_path'] = $base_path . 'upload/icon/';
            $config['allowed_types'] = 'jpg|jpeg|gif|png|bmp';


            $this->upload->initialize($config);


            if (!$this->upload->do_upload()) {
                $error = $this->upload->display_errors();
                echo $error;
                die;
            }

            $picture = $this->upload->data();

            $icon_image = $picture['file_name'];


            if (is_file($base_path . 'upload/icon/' . $this->input->post('prev_icon'))) {

                if ($this->input->post('prev_icon') != 'no_img.jpg') {
                    $link2 = $base_path . 'upload/icon/' . $this->input->post('prev_icon');
                    if (is_file($link2)) {
                        unlink($link2);
                    }
                }

            }

        } else {

            if ($this->input->post('prev_icon') != '') {
                $icon_image = $this->input->post('prev_icon');
            }
        }

        $data = array(
            'pages_title' => $this->input->post('pages_title'),
            'description' => $this->input->post('description'),
            'slug' => $this->input->post('slug'),
            'active' => $this->input->post('active'),
            'meta_keyword' => $this->input->post('meta_keyword'),
            'meta_description' => $this->input->post('meta_description'),
            'header_bar' => $this->input->post('header_bar'),
            'footer_bar' => $this->input->post('footer_bar'),
            'left_side' => $this->input->post('left_side'),
            'right_side' => $this->input->post('right_side'),
            'external_link' => $this->input->post('external_link'),
            'sub_title' => $this->input->post('sub_title'),
            'language_id' => $this->input->post('language'),
            'icon_image' => $icon_image,
            'learn_category' => $this->input->post('learn_category')
        );
        $this->db->where('pages_id', $this->input->post('pages_id'));
        $this->db->update('learn_more', $data);
    }

}

?>