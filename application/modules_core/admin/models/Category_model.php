<?php
class Category_model extends CI_Model{
	
	public $id;
	public $language_id;
	public $name;
	public $url;
	public $description;
	public $image;
	public $is_parent;
	public $active;
	public $created_at;
	public $custom_errors;

	function __construct(){
		parent::__construct();
		$this->postData();
		$this->setSegment();
	}

	function setSegment()
    {
        $id = $this->uri->segment(4,0);
        ctype_digit($id) and $this->id=$id;
    }

	function postData(){
		if($this->input->post()){
			foreach ($_POST as $key => $value) {
				$this->$key = $value;
			}
		}
	}

	function getAllCategories(){
		return $this->db->get('categories')->result_array();	
	}

	function getcategoryById(){
		$data = $this->db->get_where('categories',array('id'=>$this->id))->row_array();
		if($data){
			foreach ($data as $key => $value) {
				$this->$key=$value;
			}
		}
	}

	function addcategory(){
		$this->setRules();
		if($this->form_validation->run($this) == false)return false;
		$this->image = $this->uploadCategoryImage();
		$this->category_url($this->name);
		
		$insert = array(
				'name'=>$this->name,
				'url'=>$this->url,
				'description'=>$this->description,
				'language_id'=>$this->language_id,
				'is_parent'=>$this->is_parent,
				'active'=>$this->active,
				'created_at'=>date('Y-m-d h:i:s'),
				'image'=>$this->image

			);
		$this->db->insert('categories',$insert);
		$this->session->set_flashdata('msg','insert');
		return $this->db->insert_id();
	}

	function updateCategory(){

		$this->setRules();
		if($this->form_validation->run($this) == false)return false;
		$this->image = $this->uploadCategoryImage();
		if($this->image){
			$insert = array(
				'name'=>$this->name,
				'description'=>$this->description,
				'language_id'=>$this->language_id,
				'is_parent'=>$this->is_parent,
				'active'=>$this->active,
				'created_at'=>date('Y-m-d h:i:s'),
				'image'=>$this->image
			);
		}else{
			$insert = array(
				'name'=>$this->name,
				'description'=>$this->description,
				'language_id'=>$this->language_id,
				'is_parent'=>$this->is_parent,
				'active'=>$this->active,
				'created_at'=>date('Y-m-d h:i:s')
			);
		}
		
		$this->db->where('id',$this->id);
		$this->db->update('categories',$insert);
		$this->session->set_flashdata('msg','update');
		return true;
	}

	function deleteCategory(){
		$this->db->where('id',$this->id);
		$this->db->delete('categories');
		$this->session->set_flashdata('msg','delete');
		return true;
	}

	function setRules(){
		$this->form_validation->set_rules('name',NAME,'required|trim');
		$this->form_validation->set_rules('description',DESCRIPTION,'trim');
		$this->form_validation->set_rules('is_parent',TYPE,'required');
		$this->form_validation->set_rules('active',STATUS,'required');
		$this->form_validation->set_rules('language_id',LANGUAGE,'required');
		$this->custom_errors = $this->imagecheck();

	}

	public function imagecheck()
	{
		$error='';
		if ($_FILES) {
            if ($_FILES['image']['tmp_name']) {
                $needheight = 400;
                $needwidth = 600;
                $fn = $_FILES['image']['tmp_name'];
                if ($fn) {
                    $size = getimagesize($fn);
                    $actualwidth = $size[0];
                    $actualheight = $size[0];
                }
                if ($_FILES["image"]["type"] != "image/jpeg" and $_FILES["image"]["type"] != "image/pjpeg" and $_FILES["image"]["type"] != "image/png" and $_FILES["image"]["type"] != "image/x-png" and $_FILES["image"]["type"] != "image/gif") {
                    $error = PLEASE_UPLOAD_A_JPG_PNG_GIF_FILE;
					
                } else if ($_FILES["image"]["size"] > 2000000) {
                	$error = SORRY_THIS_FILE_IS_TOO_LARGE_PLEASE_SELECT_A_JPG_FILE_THAT_IS_LESS_THAN_TWO_MB_OR_TRY_RESIZING_IT_USING_A_PHOTO_EDITOR;
                   
                } else if ($needwidth > $actualwidth || $needheight > $actualheight) {
                    $error = 'Please select image more than 600 * 400';
                   
                }
            }
        }
        return $error;
	}

	function uploadCategoryImage(){

		if ($_FILES['image']['type'] != '') {
            $upload_img = $_FILES['image']['type'];
            $rand = rand(0, 100000);
            $type_img = explode('/', $_FILES['image']['type']);
            $new_img = 'category_' . $rand . '.' . $type_img[1];
            $base_path = $this->config->slash_item('base_path');
            require_once $base_path . 'thumbgenerator/ThumbLib.inc.php';

            move_uploaded_file($_FILES['image']['tmp_name'], $base_path . "upload/orig/" . $new_img);

            $new_w = 350;
            $new_h = 220;
            $thumb = PhpThumbFactory::create($base_path . "upload/orig/" . $new_img);
            $thumb->adaptiveResize($new_w, $new_h);
            $cache_path = $base_path . "upload/category/" . $new_img;
            $thumb->save($cache_path);

            $new_w = 900;
            $new_h = 285;
            $thumb = PhpThumbFactory::create($base_path . "upload/orig/" . $new_img);
            $thumb->adaptiveResize($new_w, $new_h);
            $cache_path = $base_path . "upload/category_search/" . $new_img;
            $thumb->save($cache_path);

            unlink($base_path . "upload/orig/" . $new_img);
            return $new_img;
        }
	}

	function category_url($url='',$count=1){
        $this->url = makeSlugs($url);
        $newcnt = $count;
        echo $this->url;
       
        $chk_url_exists = $this->db->query("select url from categories where url like '" . $this->url . "%'");
      
        if ($chk_url_exists->num_rows() > 0) {
            $get_pr = $chk_url_exists->row();
             
          	if($newcnt>100){
          		echo "error creating slug";die;
          	}
            $this->url = $this->url . $newcnt;
            $newcnt++;
            $this->category_url($this->url,$newcnt);
        }
	}
	function getAllCategoryWithEquityCount(){
		$query =$this->db->query("SELECT count(`equity_categories`.`equity_id`) as total_equity, `categories`.* FROM `categories` left join `equity_categories` on `equity_categories`.`category_id`=`categories`.`id`  and  `equity_categories`.`equity_id` IN (select `equity_id` from `equity` where `equity`.`status` IN (1,2,3,4,5,6,7,8))   GROUP BY `categories`.`id`");
		return $query->result_array();
	}
}