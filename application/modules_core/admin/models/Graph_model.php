<?php

/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid softwarc. It is released under the terms of
 * the following BSD Licensc.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
class Graph_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();

    }


    //----------------------registration report------------------


    function get_yearly_registration($first_date, $last_date)
    {

        $week_reg_arr = array();

        $temp = array();

        $this->db->select('COUNT(*) as total, YEAR(date_added) as date_added');
        $this->db->from('user');
        $this->db->where('YEAR(date_added) >=', $first_date);
        $this->db->where('YEAR(date_added) <=', $last_date);
        $this->db->group_by('YEAR(date_added)');

        $query = $this->db->get();


        if ($query->num_rows() > 0) {
            $res = $query->result();

            if ($res) {

                foreach ($res as $wtr) {
                    $week_reg_arr[$wtr->date_added] = $wtr->total;
                }

            }


        }
        for ($i = $first_date; $i <= $last_date; $i++) {

            if (array_key_exists($i, $week_reg_arr)) {
                $temp[$i] = $week_reg_arr[$i];
            } else {
                $temp[$i] = 0;
            }

        }
        return $temp;
    }

    function get_yearly_fb_registration($first_date, $last_date)
    {

        $week_reg_arr = array();

        $temp = array();


        $this->db->select('COUNT(*) as total, YEAR(date_added) as date_added');
        $this->db->from('user');
        $this->db->where('YEAR(date_added) >=', $first_date);
        $this->db->where('YEAR(date_added) <=', $last_date);
        $this->db->where('fb_uid !=', '');
        $this->db->where('fb_uid !=', 0);
        $this->db->group_by('YEAR(date_added)');

        $query = $this->db->get();


        if ($query->num_rows() > 0) {
            $res = $query->result();

            if ($res) {

                foreach ($res as $wtr) {
                    $week_reg_arr[$wtr->date_added] = $wtr->total;
                }

            }
        }

        for ($i = $first_date; $i <= $last_date; $i++) {

            if (array_key_exists($i, $week_reg_arr)) {
                $temp[$i] = $week_reg_arr[$i];
            } else {
                $temp[$i] = 0;
            }

        }


        return $temp;
    }


    function get_yearly_tw_registration($first_date, $last_date)
    {

        $week_reg_arr = array();

        $temp = array();


        $this->db->select('COUNT(*) as total, YEAR(date_added) as date_added');
        $this->db->from('user');
        $this->db->where('YEAR(date_added) >=', $first_date);
        $this->db->where('YEAR(date_added) <=', $last_date);
        $this->db->where('tw_id !=', '');
        $this->db->where('tw_id !=', 0);
        $this->db->group_by('YEAR(date_added)');

        $query = $this->db->get();


        if ($query->num_rows() > 0) {
            $res = $query->result();

            if ($res) {

                foreach ($res as $wtr) {
                    $week_reg_arr[$wtr->date_added] = $wtr->total;
                }

            }
        }

        for ($i = $first_date; $i <= $last_date; $i++) {

            if (array_key_exists($i, $week_reg_arr)) {
                $temp[$i] = $week_reg_arr[$i];
            } else {
                $temp[$i] = 0;
            }

        }

        return $temp;
    }


    function get_monthly_registration($first_date, $last_date)
    {

        $week_reg_arr = array();

        $temp = array();

        $month_year = date('Y');

        $this->db->select('COUNT(*) as total, MONTH(date_added) as date_added');
        $this->db->from('user');
        //$this->db->where('MONTH(date_added) >=',$first_date);
        //$this->db->where('MONTH(date_added) <=',$last_date);
        $this->db->where('YEAR(date_added) ', $month_year);
        $this->db->group_by('MONTH(date_added)');

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $res = $query->result();
            if ($res) {
                foreach ($res as $wtr) {
                    $week_reg_arr[$wtr->date_added] = $wtr->total;
                }
            }
        }

        for ($i = 1; $i <= 12; $i++) {

            if (array_key_exists($i, $week_reg_arr)) {
                $temp[$i] = $week_reg_arr[$i];
            } else {
                $temp[$i] = 0;
            }
        }
        return $temp;
    }

    function get_monthly_fb_registration($first_date, $last_date)
    {

        $week_reg_arr = array();

        $temp = array();

        $month_year = date('Y');

        $this->db->select('COUNT(*) as total, MONTH(date_added) as date_added');
        $this->db->from('user');
        //$this->db->where('MONTH(date_added) >=',$first_date);
        //$this->db->where('MONTH(date_added) <=',$last_date);
        $this->db->where('fb_uid !=', '');
        $this->db->where('fb_uid !=', 0);
        $this->db->where('YEAR(date_added) ', $month_year);
        $this->db->group_by('MONTH(date_added)');

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $res = $query->result();
            if ($res) {
                foreach ($res as $wtr) {
                    $week_reg_arr[$wtr->date_added] = $wtr->total;
                }

            }
        }

        for ($i = 1; $i <= 12; $i++) {

            if (array_key_exists($i, $week_reg_arr)) {
                $temp[$i] = $week_reg_arr[$i];
            } else {
                $temp[$i] = 0;
            }

        }
        return $temp;
    }


    function get_monthly_tw_registration($first_date, $last_date)
    {

        $week_reg_arr = array();

        $temp = array();

        $month_year = date('Y');

        $this->db->select('COUNT(*) as total, MONTH(date_added) as date_added');
        $this->db->from('user');
        //$this->db->where('MONTH(date_added) >=',$first_date);
        //$this->db->where('MONTH(date_added) <=',$last_date);
        $this->db->where('tw_id !=', '');
        $this->db->where('tw_id !=', 0);
        $this->db->where('YEAR(date_added) ', $month_year);
        $this->db->group_by('MONTH(date_added)');

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $res = $query->result();

            if ($res) {
                foreach ($res as $wtr) {
                    $week_reg_arr[$wtr->date_added] = $wtr->total;
                }
            }
        }

        for ($i = 1; $i <= 12; $i++) {

            if (array_key_exists($i, $week_reg_arr)) {
                $temp[$i] = $week_reg_arr[$i];
            } else {
                $temp[$i] = 0;
            }
        }
        return $temp;
    }

    function get_weekly_registration($first_date, $last_date)
    {

        $week_reg_arr = array();

        $temp = array();

        $this->db->select('COUNT(*) as total, DATE(date_added) as date_added');
        $this->db->from('user');
        $this->db->where('DATE(date_added) >=', $first_date);
        $this->db->where('DATE(date_added) <=', $last_date);
        $this->db->group_by('DATE(date_added)');

        $query = $this->db->get();


        if ($query->num_rows() > 0) {
            $res = $query->result();


            if ($res) {

                foreach ($res as $wtr) {
                    $week_reg_arr[$wtr->date_added] = $wtr->total;
                }

            }


        }


        while (strtotime($first_date) <= strtotime($last_date)) {

            if (array_key_exists($first_date, $week_reg_arr)) {
                $temp[$first_date] = $week_reg_arr[$first_date];
            } else {
                $temp[$first_date] = 0;
            }


            $first_date = date("Y-m-d", strtotime("+1 day", strtotime($first_date)));

        }


        return $temp;


    }

    function get_weekly_fb_registration($first_date, $last_date)
    {

        $week_reg_arr = array();

        $temp = array();

        $this->db->select('COUNT(*) as total, DATE(date_added) as date_added');
        $this->db->from('user');
        $this->db->where('DATE(date_added) >=', $first_date);
        $this->db->where('DATE(date_added) <=', $last_date);
        $this->db->where('fb_uid !=', '');
        $this->db->where('fb_uid !=', 0);
        $this->db->group_by('DATE(date_added)');

        $query = $this->db->get();


        if ($query->num_rows() > 0) {
            $res = $query->result();


            if ($res) {

                foreach ($res as $wtr) {
                    $week_reg_arr[$wtr->date_added] = $wtr->total;
                }

            }


        }


        while (strtotime($first_date) <= strtotime($last_date)) {

            if (array_key_exists($first_date, $week_reg_arr)) {
                $temp[$first_date] = $week_reg_arr[$first_date];
            } else {
                $temp[$first_date] = 0;
            }


            $first_date = date("Y-m-d", strtotime("+1 day", strtotime($first_date)));

        }


        return $temp;


    }


    function get_weekly_tw_registration($first_date, $last_date)
    {

        $week_reg_arr = array();

        $temp = array();

        $this->db->select('COUNT(*) as total, DATE(date_added) as date_added');
        $this->db->from('user');
        $this->db->where('DATE(date_added) >=', $first_date);
        $this->db->where('DATE(date_added) <=', $last_date);
        $this->db->where('tw_id !=', '');
        $this->db->where('tw_id !=', 0);
        $this->db->group_by('DATE(date_added)');

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $res = $query->result();


            if ($res) {

                foreach ($res as $wtr) {
                    $week_reg_arr[$wtr->date_added] = $wtr->total;
                }

            }
        }
        while (strtotime($first_date) <= strtotime($last_date)) {

            if (array_key_exists($first_date, $week_reg_arr)) {
                $temp[$first_date] = $week_reg_arr[$first_date];
            } else {
                $temp[$first_date] = 0;
            }
            $first_date = date("Y-m-d", strtotime("+1 day", strtotime($first_date)));
        }
        return $temp;
    }


    //----------------------project report------------------

    ////// yearly projects data


    function get_yearly_projects($first_date, $last_date)
    {

        $week_reg_arr = array();

        $temp = array();

        $this->db->select('COUNT(*) as total, YEAR(c.date_added) as date_added');
        $this->db->from('campaign c');
        $this->db->where('YEAR(c.date_added) >=', $first_date);
        $this->db->where('YEAR(c.date_added) <=', $last_date);
        $this->db->group_by('YEAR(c.date_added)');

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $res = $query->result();
            if ($res) {
                foreach ($res as $wtr) {
                    $week_reg_arr[$wtr->date_added] = $wtr->total;
                }
            }
        }
        for ($i = $first_date; $i <= $last_date; $i++) {

            if (array_key_exists($i, $week_reg_arr)) {
                $temp[$i] = $week_reg_arr[$i];
            } else {
                $temp[$i] = 0;
            }

        }
        return $temp;
    }

    function get_yearly_projects_success($first_date, $last_date)
    {

        $week_reg_arr = array();

        $temp = array();

        $this->db->select('COUNT(*) as total, YEAR(c.date_added) as date_added');
        $this->db->from('campaign c');
        $this->db->where('YEAR(c.date_added) >=', $first_date);
        $this->db->where('YEAR(c.date_added) <=', $last_date);
        $this->db->where('c.status in (3,4)');
        //$this->db->where('CAST(p.amount_get as decimal(10,2)) >= CAST(p.amount as decimal(10,2))');

        $this->db->group_by('YEAR(c.date_added)');

        $query = $this->db->get();
        //echo $this->db->last_query();die;
        if ($query->num_rows() > 0) {
            $res = $query->result();
            if ($res) {
                foreach ($res as $wtr) {
                    $week_reg_arr[$wtr->date_added] = $wtr->total;
                    //echo $week_reg_arr[$wtr->date_added].'<br>';
                }
            }//die;
        }
        for ($i = $first_date; $i <= $last_date; $i++) {

            if (array_key_exists($i, $week_reg_arr)) {
                $temp[$i] = $week_reg_arr[$i];
            } else {
                $temp[$i] = 0;
            }
        }
        return $temp;
    }


    function get_yearly_projects_active($first_date, $last_date)
    {

        $week_reg_arr = array();

        $temp = array();

        $this->db->select('COUNT(*) as total, YEAR(c.date_added) as date_added');
        $this->db->from('campaign c');
        $this->db->where('YEAR(c.date_added) >=', $first_date);
        $this->db->where('YEAR(c.date_added) <=', $last_date);
        $this->db->where('c.status', '2');
        //$this->db->where('p.end_date >= now()');

        $this->db->group_by('YEAR(c.date_added)');

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $res = $query->result();
            if ($res) {

                foreach ($res as $wtr) {
                    $week_reg_arr[$wtr->date_added] = $wtr->total;
                }
            }
        }
        for ($i = $first_date; $i <= $last_date; $i++) {

            if (array_key_exists($i, $week_reg_arr)) {
                $temp[$i] = $week_reg_arr[$i];
            } else {
                $temp[$i] = 0;
            }

        }
        return $temp;
    }


    function get_yearly_projects_new($first_date, $last_date)
    {

        $week_reg_arr = array();

        $temp = array();

        $this->db->select('COUNT(*) as total, YEAR(c.date_added) as date_added');
        $this->db->from('campaign c');
        $this->db->where('YEAR(c.date_added) >=', $first_date);
        $this->db->where('YEAR(c.date_added) <=', $last_date);
        $this->db->where('c.status', '1');

        $this->db->group_by('YEAR(c.date_added)');

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $res = $query->result();
            if ($res) {
                foreach ($res as $wtr) {
                    $week_reg_arr[$wtr->date_added] = $wtr->total;
                }
            }
        }
        for ($i = $first_date; $i <= $last_date; $i++) {

            if (array_key_exists($i, $week_reg_arr)) {
                $temp[$i] = $week_reg_arr[$i];
            } else {
                $temp[$i] = 0;
            }

        }
        return $temp;
    }

    ////// monthly projects data

    function get_monthly_projects($first_date, $last_date)
    {

        $week_reg_arr = array();

        $temp = array();
        $month_year = date('Y');

        $this->db->select('COUNT(*) as total, MONTH(c.date_added) as date_added');
        $this->db->from('campaign c');
        $this->db->where('MONTH(c.date_added) >=', $first_date);
        $this->db->where('MONTH(c.date_added) <=', $last_date);
        $this->db->where('YEAR(c.date_added) ', $month_year);
        $this->db->group_by('MONTH(c.date_added)');

        $query = $this->db->get();

        //echo $this->db->last_query();
        //die();
        if ($query->num_rows() > 0) {
            $res = $query->result();
            if ($res) {
                foreach ($res as $wtr) {
                    $week_reg_arr[$wtr->date_added] = $wtr->total;
                }
            }
        }

        for ($i = 1; $i <= 12; $i++) {

            if (array_key_exists($i, $week_reg_arr)) {
                $temp[$i] = $week_reg_arr[$i];
            } else {
                $temp[$i] = 0;
            }
        }
        return $temp;
    }

    function get_monthly_projects_success($first_date, $last_date)
    {

        $week_reg_arr = array();

        $temp = array();
        $month_year = date('Y');

        $this->db->select('COUNT(*) as total, MONTH(c.date_added) as date_added');
        $this->db->from('campaign c');
        //$this->db->where('MONTH(p.date_added) >=',$first_date);
        //$this->db->where('MONTH(p.date_added) <=',$last_date);
        $this->db->where('YEAR(c.date_added) ', $month_year);
        //$this->db->where('CAST(p.amount_get as decimal(10,2)) >= CAST(p.amount as decimal(10,2))');
        $this->db->where('c.status in (3,4)');
        $this->db->group_by('MONTH(c.date_added)');

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $res = $query->result();
            if ($res) {
                foreach ($res as $wtr) {
                    $week_reg_arr[$wtr->date_added] = $wtr->total;
                }
            }
        }
        for ($i = 1; $i <= 12; $i++) {
            if (array_key_exists($i, $week_reg_arr)) {
                $temp[$i] = $week_reg_arr[$i];
            } else {
                $temp[$i] = 0;
            }
        }
        return $temp;
    }


    function get_monthly_projects_active($first_date, $last_date)
    {

        $week_reg_arr = array();

        $temp = array();
        $month_year = date('Y');

        $this->db->select('COUNT(*) as total, MONTH(c.date_added) as date_added');
        $this->db->from('campaign c');
        //$this->db->where('MONTH(p.date_added) >=',$first_date);
        //$this->db->where('MONTH(p.date_added) <=',$last_date);
        $this->db->where('YEAR(c.date_added) ', $month_year);
        $this->db->where('c.status', '2');
        //$this->db->where('p.end_date >= now()');

        $this->db->group_by('MONTH(c.date_added)');

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $res = $query->result();


            if ($res) {

                foreach ($res as $wtr) {
                    $week_reg_arr[$wtr->date_added] = $wtr->total;
                }

            }


        }

        for ($i = 1; $i <= 12; $i++) {

            if (array_key_exists($i, $week_reg_arr)) {
                $temp[$i] = $week_reg_arr[$i];
            } else {
                $temp[$i] = 0;
            }
        }
        return $temp;
    }


    function get_monthly_projects_new($first_date, $last_date)
    {

        $week_reg_arr = array();

        $temp = array();
        $month_year = date('Y');


        $this->db->select('COUNT(*) as total, MONTH(c.date_added) as date_added');
        $this->db->from('campaign c');
        //$this->db->where('MONTH(p.date_added) >=',$first_date);
        //$this->db->where('MONTH(p.date_added) <=',$last_date);
        $this->db->where('YEAR(c.date_added) ', $month_year);
        $this->db->where('c.status', '1');

        $this->db->group_by('MONTH(c.date_added)');

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $res = $query->result();


            if ($res) {

                foreach ($res as $wtr) {
                    $week_reg_arr[$wtr->date_added] = $wtr->total;
                }
            }
        }
        for ($i = 1; $i <= 12; $i++) {

            if (array_key_exists($i, $week_reg_arr)) {
                $temp[$i] = $week_reg_arr[$i];
            } else {
                $temp[$i] = 0;
            }

        }
        return $temp;


    }


    ////// weekly projects data

    function get_weekly_projects($first_date, $last_date)
    {

        $week_reg_arr = array();

        $temp = array();

        $this->db->select('COUNT(*) as total, DATE(c.date_added) as date_added');
        $this->db->from('campaign c');
        $this->db->where('DATE(c.date_added) >=', $first_date);
        $this->db->where('DATE(c.date_added) <=', $last_date);
        $this->db->group_by('DATE(c.date_added)');

        $query = $this->db->get();


        if ($query->num_rows() > 0) {
            $res = $query->result();


            if ($res) {

                foreach ($res as $wtr) {
                    $week_reg_arr[$wtr->date_added] = $wtr->total;
                }

            }


        }


        while (strtotime($first_date) <= strtotime($last_date)) {

            if (array_key_exists($first_date, $week_reg_arr)) {
                $temp[$first_date] = $week_reg_arr[$first_date];
            } else {
                $temp[$first_date] = 0;
            }


            $first_date = date("Y-m-d", strtotime("+1 day", strtotime($first_date)));

        }


        return $temp;


    }


    function get_weekly_projects_success($first_date, $last_date)
    {

        $week_reg_arr = array();

        $temp = array();

        $this->db->select('COUNT(*) as total, DATE(c.date_added) as date_added');
        $this->db->from('campaign c');
        $this->db->where('DATE(c.date_added) >=', $first_date);
        $this->db->where('DATE(c.date_added) <=', $last_date);
        $this->db->where('c.status in (3,4)');

        $this->db->group_by('DATE(c.date_added)');

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $res = $query->result();

            if ($res) {

                foreach ($res as $wtr) {
                    $week_reg_arr[$wtr->date_added] = $wtr->total;
                }
            }
        }

        while (strtotime($first_date) <= strtotime($last_date)) {

            if (array_key_exists($first_date, $week_reg_arr)) {
                $temp[$first_date] = $week_reg_arr[$first_date];
            } else {
                $temp[$first_date] = 0;
            }
            $first_date = date("Y-m-d", strtotime("+1 day", strtotime($first_date)));
        }
        return $temp;
    }


    function get_weekly_projects_active($first_date, $last_date)
    {

        $week_reg_arr = array();

        $temp = array();

        $this->db->select('COUNT(*) as total, DATE(c.date_added) as date_added');
        $this->db->from('campaign c');
        $this->db->where('DATE(c.date_added) >=', $first_date);
        $this->db->where('DATE(c.date_added) <=', $last_date);
        $this->db->where('c.status', '2');
        $this->db->where('c.investment_close_date >= now()');

        $this->db->group_by('DATE(c.date_added)');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $res = $query->result();

            if ($res) {
                foreach ($res as $wtr) {
                    $week_reg_arr[$wtr->date_added] = $wtr->total;
                }
            }
        }
        while (strtotime($first_date) <= strtotime($last_date)) {

            if (array_key_exists($first_date, $week_reg_arr)) {
                $temp[$first_date] = $week_reg_arr[$first_date];
            } else {
                $temp[$first_date] = 0;
            }
            $first_date = date("Y-m-d", strtotime("+1 day", strtotime($first_date)));
        }
        return $temp;
    }

    function get_weekly_projects_new($first_date, $last_date)
    {

        $week_reg_arr = array();

        $temp = array();

        $this->db->select('COUNT(*) as total, DATE(c.date_added) as date_added');
        $this->db->from('campaign c');
        $this->db->where('DATE(c.date_added) >=', $first_date);
        $this->db->where('DATE(c.date_added) <=', $last_date);
        $this->db->where('c.status', '1');

        $this->db->group_by('DATE(c.date_added)');

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $res = $query->result();
            if ($res) {
                foreach ($res as $wtr) {
                    $week_reg_arr[$wtr->date_added] = $wtr->total;
                }
            }
        }

        while (strtotime($first_date) <= strtotime($last_date)) {

            if (array_key_exists($first_date, $week_reg_arr)) {
                $temp[$first_date] = $week_reg_arr[$first_date];
            } else {
                $temp[$first_date] = 0;
            }
            $first_date = date("Y-m-d", strtotime("+1 day", strtotime($first_date)));
        }
        return $temp;
    }

}

?>