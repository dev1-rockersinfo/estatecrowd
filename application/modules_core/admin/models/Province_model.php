<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

function __construct()
{
    parent::__construct();
}

/*
Function name :table_insert()
Parameter : $table=table name, $data=data array of values you want to insert into table.
Return : none
Use : Common function for inserting values into database.
*/

function table_insert($table, $data)
{
    $this->db->insert($table, $data);
}

/*
Function name :table_update()
Parameter :$key_name=Field name for where clause,$key_value=Field value, $table=table name, $data=data array of values you want to insert into table.
Return : none
Use : Common function for updating values into database.
*/

function table_update($key_name, $key_value, $table, $data)
{

    $this->db->where($key_name, $key_value);
    $query = $this->db->update($table, $data);
    return true;
}

/*
Function name :get_one_tabledata()
Parameter :$table=table name, $where=array of values whose information or details you want to get.
Return : none
Use : Common function for getting single record using where clause.
*/

function get_one_tabledata($table, $where = array())

{
    $query = $this->db->get_where($table, $where);
    return $query->row_array();

}

function get_paypal_data()
{

    $this->db->select('paypal_response_request.*,equity.company_name,user.user_name,user.last_name');
    $this->db->from('paypal_response_request');
    $this->db->join('equity', 'equity.equity_id = paypal_response_request.project_id', 'left');
    $this->db->join('user', 'user.user_id = paypal_response_request.donar_id', 'left');
    $query = $this->db->get();

    //echo $this->db->last_query();die;

    if ($query->num_rows() > 0) {
        return $query->result_array();
    }
    return 0;

    /*$query= " SELECT pr.* , p.project_title,u.user_name,u.last_name FROM paypal_response_request pr LEFT JOIN project p ON pr.project_id = p.project_id LEFT JOIN user u
    ON pr.donar_id = u.user_id";*/
}


?>