<?php

/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
class Site_setting_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /*
     Function name :site_setting_update()
    Parameter : none.
    Return : none
    Use : this function is used to update records into site_setting table.
    */
    function site_setting_update()

    {

        $CI =& get_instance();

        $base_path = $CI->config->slash_item('base_path');

        $site_logo = $this->input->post('prev_site_logo');

        $site_logo_hover = $this->input->post('prev_site_logo_hover');

        $favicon_image = $this->input->post('favicon_image');


        if ($_FILES) {

            if ($_FILES['file_up']['name'] != '') {

                $this->load->library('upload');

                $rand = rand(0, 100000);

                $_FILES['userfile']['name'] = $_FILES['file_up']['name'];

                $_FILES['userfile']['type'] = $_FILES['file_up']['type'];

                $_FILES['userfile']['tmp_name'] = $_FILES['file_up']['tmp_name'];

                $_FILES['userfile']['error'] = $_FILES['file_up']['error'];

                $_FILES['userfile']['size'] = $_FILES['file_up']['size'];

                $type_img = explode('/', $_FILES['userfile']['type']);
                $new_img = 'logo_' . $rand . '.' . $type_img[1];
                $config['file_name'] = $new_img;

                $config['upload_path'] = $base_path . 'upload/orig/';

                $config['allowed_types'] = 'jpg|jpeg|gif|png|bmp';

                $this->upload->initialize($config);

                if (!$this->upload->do_upload()) {
                    echo $error = $this->upload->display_errors();
                    die;
                }

                $picture = $this->upload->data();


                $site_logo = $picture['file_name'];

                if (is_file($base_path . 'upload/orig/' . $this->input->post('prev_site_logo'))) {

                    if ($this->input->post('prev_site_logo') != 'no_img.jpg') {
                        $link2 = $base_path . 'upload/orig/' . $this->input->post('prev_site_logo');
                        unlink($link2);
                    }
                }
            } else {
                if ($this->input->post('prev_site_logo') != '') {
                    $site_logo = $this->input->post('prev_site_logo');
                }
            }

            if ($_FILES['file_up2']['name'] != '') {

                $this->load->library('upload');

                $rand = rand(0, 100000);

                $_FILES['userfile']['name'] = $_FILES['file_up2']['name'];

                $_FILES['userfile']['type'] = $_FILES['file_up2']['type'];

                $_FILES['userfile']['tmp_name'] = $_FILES['file_up2']['tmp_name'];

                $_FILES['userfile']['error'] = $_FILES['file_up2']['error'];

                $_FILES['userfile']['size'] = $_FILES['file_up2']['size'];

                $type_img = explode('/', $_FILES['userfile']['type']);
                $new_img = 'logo_' . $rand . '.' . $type_img[1];
                $config['file_name'] = $new_img;


                $config['upload_path'] = $base_path . 'upload/orig/';

                $config['allowed_types'] = 'jpg|jpeg|gif|png|bmp';

                $this->upload->initialize($config);

                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                }
                $picture2 = $this->upload->data();
                $site_logo_hover = $picture2['file_name'];

                if (is_file($base_path . 'upload/orig/' . $this->input->post('prev_site_logo_hover'))) {
                    if ($this->input->post('prev_site_logo_hover') != 'no_img.jpg') {
                        $link2 = $base_path . 'upload/orig/' . $this->input->post('prev_site_logo_hover');
                        unlink($link2);
                    }
                }
            } else {
                if ($this->input->post('prev_site_logo_hover') != '') {
                    $site_logo_hover = $this->input->post('prev_site_logo_hover');
                }
            }


            if ($_FILES['favicon_file']['name'] != '') {

                $this->load->library('upload');

                $rand = rand(0, 100000);

                $_FILES['userfile']['name'] = $_FILES['favicon_file']['name'];

                $_FILES['userfile']['type'] = $_FILES['favicon_file']['type'];

                $_FILES['userfile']['tmp_name'] = $_FILES['favicon_file']['tmp_name'];

                $_FILES['userfile']['error'] = $_FILES['favicon_file']['error'];

                $_FILES['userfile']['size'] = $_FILES['favicon_file']['size'];

                $type_img = explode('/', $_FILES['userfile']['type']);
                $new_img = 'favicon_' . $rand . '.' . $type_img[1];
                $config['file_name'] = $new_img;


                $config['upload_path'] = $base_path . 'upload/orig/';


                $config['allowed_types'] = 'jpg|jpeg|gif|png|bmp';

                $this->upload->initialize($config);

                if (!$this->upload->do_upload()) {

                    $error = $this->upload->display_errors();
                }
                $picture2 = $this->upload->data();

                $favicon_image = $picture2['file_name'];

                if (is_file($base_path . 'upload/orig/' . $this->input->post('favicon_image'))) {
                    if ($this->input->post('favicon_image') != 'no_img.jpg') {
                        $link2 = $base_path . 'upload/orig/' . $this->input->post('favicon_image');
                        unlink($link2);
                    }
                }
            } else {
                if ($this->input->post('favicon_image') != '') {
                    $favicon_image = $this->input->post('favicon_image');
                }
            }

        }


        $currency = $this->db->query("select * from currency_code where currency_code='" . $this->input->post('currency_code') . "'");

        $cur = $currency->row();

        //print_r($cur);
        $currency_symbol = $cur->currency_symbol;

        $preapproval_enable = $this->input->post('preapproval_enable');

        if ($preapproval_enable == 0) {

            $enable_funding_option = 0;

        } else {
            $enable_funding_option = $this->input->post('enable_funding_option');

        }


        $data = array(

            'site_name' => $this->input->post('site_name'),

            'site_language' => $this->input->post('site_language'),

            'currency_symbol' => $currency_symbol,

            'currency_code' => $this->input->post('currency_code'),


            'maximum_project_per_year' => $this->input->post('maximum_project_per_year'),

            'maximum_donate_per_project' => $this->input->post('maximum_donate_per_project'),

            'date_format' => $this->input->post('date_format'),

            'time_format' => $this->input->post('time_format'),

            'site_tracker' => $this->input->post('site_tracker'),

            'site_logo' => $site_logo,

            'site_logo_hover' => $site_logo_hover,

            'favicon_image' => $favicon_image,

            'project_min_days' => $this->input->post('project_min_days'),

            'project_max_days' => $this->input->post('project_max_days'),

            'time_zone' => $this->input->post('time_zone'),

            'currency_symbol_side' => $this->input->post('currency_symbol_side'),

            'enable_funding_option' => $enable_funding_option,

            'flexible_fees' => $this->input->post('flexible_fees'),

            'suc_flexible_fees' => $this->input->post('suc_flexible_fees'),

            'fixed_fees' => $this->input->post('fixed_fees'),

            'instant_fees' => $this->input->post('instant_fees'),

            'minimum_goal' => $this->input->post('minimum_goal'),

            'maximum_goal' => $this->input->post('maximum_goal'),


            'payment_gateway' => $this->input->post('payment_gateway'),

            'ending_days' => $this->input->post('ending_days'),

            'signup_captcha' => $this->input->post('signup_captcha'),

            'contact_us_captcha' => $this->input->post('contact_us_captcha'),

            'captcha_public_key' => $this->input->post('captcha_public_key'),		

			'captcha_private_key' => $this->input->post('captcha_private_key'),


        );

        $this->db->where('site_setting_id', $this->input->post('site_setting_id'));

        $this->db->update('site_setting', $data);

        $payment_gateway = $this->input->post('payment_gateway');

        $paypal_credit_card_gateway_enable = 0;

        $paypal_gateway_enable = 0;

        $amazon_gateway_enable = 0;

        $wallet_gateway_enable = 0;


        $paypal_credit_card_preapprove_enable = 0;

        $paypal_preapprove_enable = 0;

        $amazon_preapprove_enable = 0;

        $wallet_preapprove_enable = 0;

        if ($payment_gateway == 1) {

            $paypal_gateway_enable = 1;

            if ($preapproval_enable == 1) {

                $paypal_preapprove_enable = 1;
            }


        } elseif ($payment_gateway == 3) {

            $paypal_credit_card_gateway_enable = 1;

            if ($preapproval_enable == 1) {

                $paypal_credit_card_preapprove_enable = 1;

            }

        } elseif ($payment_gateway == 2) {

            $amazon_gateway_enable = 1;

            if ($preapproval_enable == 1) {

                $amazon_preapprove_enable = 1;

            }

        } else {

            $wallet_gateway_enable = 1;

            if ($preapproval_enable == 1) {

                $wallet_preapprove_enable = 1;

            }

        }



    }

    /*
     Function name :other_setting_update()
    Parameter : none.
    Return : none
    Use : this function is used to update records into site_setting table.
    */
    function other_setting_update()

    {

        $CI =& get_instance();

        $base_path = $CI->config->slash_item('base_path');


        $accredential_file = $this->input->post('accredential_file_hidden');

        if ($_FILES) {


            if ($_FILES['accredential_file']['name'] != '') {

                $this->load->library('upload');

                $rand = rand(0, 100000);

                $_FILES['userfile']['name'] = $_FILES['accredential_file']['name'];

                $_FILES['userfile']['type'] = $_FILES['accredential_file']['type'];

                $_FILES['userfile']['tmp_name'] = $_FILES['accredential_file']['tmp_name'];

                $_FILES['userfile']['error'] = $_FILES['accredential_file']['error'];

                $_FILES['userfile']['size'] = $_FILES['accredential_file']['size'];

                $type_img = explode('/', $_FILES['userfile']['type']);
                $new_img = 'accredential_' . $rand . '.' . $type_img[1];
                $config['file_name'] = $new_img;

                $config['upload_path'] = $base_path . 'upload/doc/';

                $config['allowed_types'] = 'pdf|doc|docx|zip';

                $this->upload->initialize($config);

                if (!$this->upload->do_upload()) {
                    echo $error = $this->upload->display_errors();
                    die;
                }

                $picture = $this->upload->data();


                $accredential_file = $picture['file_name'];

                if (is_file($base_path . 'upload/doc/' . $this->input->post('accredential_file_hidden'))) {

                    if ($this->input->post('accredential_file_hidden') != 'no_img.jpg') {
                        $link2 = $base_path . 'upload/doc/' . $this->input->post('accredential_file_hidden');
                        unlink($link2);
                    }
                }
            } else {
                if ($this->input->post('accredential_file_hidden') != '') {
                    $accredential_file = $this->input->post('accredential_file_hidden');
                }
            }

        }


        $data = array(

            'bank_detail' => $this->input->post('bank_detail'),
            'accredential_status' => $this->input->post('accredential_status'),
            'accredential_file' => $accredential_file,
            'sign_contract_upload_by' => $this->input->post('sign_contract_upload_by'),
            'accrediated_manage' => $this->input->post('accrediated_manage'),
            'contract_copy_status' => $this->input->post('contract_copy_status'),


        );

        $this->db->where('site_setting_id', $this->input->post('site_setting_id'));

        $this->db->update('site_setting', $data);


    }

    /*
     Function name :get_currency()
    Parameter : none.
    Return : none
    Use : this function is used to get currency list which are active from currency table.
    */
    function get_currency()

    {
        $query = $this->db->get_where('currency_code', array('active' => 1));

        return $query->result();

    }

    /*
     Function name :amount_setting_update()
    Parameter : none.
    Return : none
    Use : this function is used to update records into site_setting table for amount fields.
    */
    function amount_setting_update()
    {

        $currency = $this->db->query("select * from currency_code where currency_code='" . $this->input->post('currency_code') . "'");

        $cur = $currency->row();

        $currency_symbol = $cur->currency_symbol;

        $data = array(

            'currency_symbol' => $currency_symbol,

            'currency_code' => $this->input->post('currency_code'),

            'decimal_points' => $this->input->post('decimal_points'),

            'currency_symbol_side' => $this->input->post('currency_symbol_side'),

        );

        $this->db->where('site_setting_id', $this->input->post('site_setting_id'));

        $this->db->update('site_setting', $data);

    }




// This function isn't used any more...
    /*
        function get_one_site_setting()

        {



            $query = $this->db->get_where('site_setting');



            return $query->row_array();



        }
    */

    function filter_setting_update()

    {

        $data = array(

            'ending_soon' => $this->input->post('ending_soon'),

            /*	'small_project' => $this->input->post('small_project'),		*/

            'popular' => $this->input->post('popular'),
            'most_funded' => $this->input->post('most_funded'),
            'successful' => $this->input->post('successful'),

        );
        //print_r($data);die;

        $this->db->where('site_setting_id', $this->input->post('site_setting_id'));
        $this->db->update('site_setting', $data);

    }


//=======Not used now========
    function get_languages()
    {
        $query = $this->db->get('translation');
        return $query->result_array();
    }

    function get_time_zone()
    {

        $query = $this->db->get('timezone');

        return $query->result_array();
    }


    function get_one_img_setting()
    {
        $query = $this->db->get_where('image_setting');

        return $query->row_array();

    }

    function img_setting_update()
    {
        $data = array(
            'p_thumb_width' => $this->input->post('p_thumb_width'),
            'p_thumb_height' => $this->input->post('p_thumb_height'),
            'p_ratio' => $this->input->post('p_ratio'),
            'p_small_width' => $this->input->post('p_s_width'),
            'p_small_height' => $this->input->post('p_s_height'),
            'p_medium_width' => $this->input->post('p_m_width'),
            'p_medium_height' => $this->input->post('p_m_height'),
            'p_large_width' => $this->input->post('p_l_width'),
            'p_large_height' => $this->input->post('p_l_height'),
            'u_s_width' => $this->input->post('u_s_width'),
            'u_s_height' => $this->input->post('u_s_height'),
            'u_m_width' => $this->input->post('u_m_width'),
            'u_m_height' => $this->input->post('u_m_height'),
            'u_b_width' => $this->input->post('u_b_width'),
            'u_b_height' => $this->input->post('u_b_height'),
            'u_ratio' => $this->input->post('u_ratio'),
            'g_ratio' => $this->input->post('g_ratio')
        );

        $this->db->where('image_setting_id', $this->input->post('image_setting_id'));

        $this->db->update('image_setting', $data);

    }

    function GetAllSliderData($order = array('view_order' => 'asc'), $limit = 100)

    {

        $site_setting = site_setting();

        $banner_setting = $site_setting['slider_setting'];

        $selectfields = 'slider_setting.view_order,slider_setting.id';

        //if require to join comment

        $selectfields .= ',dynamic_slider.dynamic_image_title ,dynamic_slider.dynamic_image_paragraph,dynamic_slider.dynamic_image_image,dynamic_slider.small_text,dynamic_slider.dynamic_slider_id';


        $selectfields .= ',equity.project_name, equity.equity_url, equity.video_image ,equity.equity_id ,equity.cover_photo,equity.project_description';


        $this->db->select($selectfields);


        $this->db->from('slider_setting');


        //if require to join comment


        $this->db->join('equity', 'slider_setting.equity_id=equity.equity_id', 'left');


        $this->db->join('dynamic_slider', 'slider_setting.slider_id=dynamic_slider.dynamic_slider_id', 'left');


        if ($banner_setting == 0) {
            $this->db->where('equity.is_featured', 1);

        } else if ($banner_setting == 1) {
            $this->db->or_where('dynamic_slider.active', 1);
        } else {

            $this->db->where('equity.is_featured', 1);

            $this->db->or_where('dynamic_slider.active', 1);

        }


        if (is_array($order)) {

            foreach ($order as $key => $val) {

                $this->db->order_by($key, $val);

            }

        }


        if ($limit > 0) $this->db->limit($limit);

        // echo $this->db->last_query();

        $query = $this->db->get();

        // echo "<br><br><br>";


        //  echo $this->db->last_query();

        //  die;

        //echo "<br><br><br>";


        if ($query->num_rows() > 0) {

            return $query->result_array();

        } else {

            return 0;

        }

    }

}


?>
