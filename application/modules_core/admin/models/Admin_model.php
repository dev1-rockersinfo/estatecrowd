<?php

/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
class Admin_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    /*
     Function name :get_admin_edit_rights()
     Parameter :$project_id=project id whose project admin wants to edit.
     Return : returns true or $valid=1 if admin has rights else false or $valid=0.
     Use : Check whether admin has rights to edit project or not.
     */
    function get_admin_edit_rights($project_id)
    {
        $query = $this->db->query("select u.email, a.email from equity e, user u, admin a where e.user_id=u.user_id and u.email=a.email and e.equity_id='$project_id' and a.admin_id='" . $this->session->userdata('admin_id') . "' ");

        if ($query->num_rows() > 0) {
            return $valid = 1;
        } else {
            return $valid = 0;
        }
    }

    /*
     Function name :user_unique()
     Parameter :$str=username entered by admin to register or update existing admin.
     Return : returns true if username is unique else false if username exists.
     Use : check username entered is unique or not.
     */
    function user_unique($str)
    {
        if ($this->input->post('admin_id')) {
            $query = $this->db->get_where('admin', array('admin_id' => $this->input->post('admin_id')));
            $res = $query->row_array();
            $email = $res['username'];

            $query = $this->db->query("select username from admin where username = '$str' and admin_id!='" . $this->input->post('admin_id') . "'");
        } else {
            $query = $this->db->query("select username from admin where username = '$str'");
        }
        if ($query->num_rows() > 0) {
            return FALSE;
        } else {
            return TRUE;
        }
    }


    /*
     Function name :email_unique()
     Parameter :$str=email entered by admin to register or update existing admin.
     Return : returns true if email is unique else false if email exists.
     Use : check email entered is unique or not.
     */
    function email_unique($str)
    {
        $flag = 0;
        if ($this->input->post('admin_id')) {
            $query = $this->db->get_where('admin', array('admin_id' => $this->input->post('admin_id')));
            $res = $query->row_array();
            $email = $res['email'];

            $query = $this->db->query("select username from admin where email = '$str' and admin_id!='" . $this->input->post('admin_id') . "'");
        } else {
            $query = $this->db->query("select username from admin where email = '$str'");
        }
        if ($query->num_rows() > 0) {
            $flag = 1;
        }


        $query_user = $this->db->query("select user_name from user where email = '$str'");
        if ($query_user->num_rows() > 0) {
            $flag = 1;
        }

        if ($flag == 1) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /*
     Function name :admin_insert()
     Parameter :none
     Return : returns user_id of new added admin else returns blank.
     Use : inserts data to admin table of newly created admin..
     */
    function admin_insert()
    {
        $data = array(
            'email' => $this->input->post('email'),
            'username' => $this->input->post('username'),
            'password' => md5($this->input->post('password')),
            'admin_type' => $this->input->post('admin_type'),
            'login_ip' => $_SERVER['REMOTE_ADDR'],
            'active' => $this->input->post('active'),
            'date_added' => date('Y-m-d'),

        );
        $this->db->insert('admin', $data);


    }


    /*
     Function name :get_adminlogin_result()
     Parameter :none
     Return : returns result of admin's login information.
     Use : getting information of admin login details.
     */
    function get_adminlogin_result()
    {

        $query = $this->db->query("select a.username,a.password,a.admin_type,a.email,ad.* from admin_login ad left join admin a on ad.admin_id=a.admin_id order by ad.login_id desc");


        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return 0;
    }

    /*
     Function name :get_assign_rights()
     Parameter :$id:id of admin whose rights you want to see.
     Return : returns result of admin's rights else returns 0
     Use : getting information of admin rights details.
     */
    //For admin rights
    function get_assign_rights($id)
    {
        $query = $this->db->query("select * from rights_assign where admin_id='" . $id . "'");

        if ($query->num_rows() > 0) {
            $rights = $query->result();

            $temp = array();

            foreach ($rights as $rig) {
                if ($rig->rights_set == 1 || $rig->rights_set == '1') {
                    $temp[] = $rig->rights_id;
                }
            }
            return $temp;
        } else {
            return 0;
        }
    }

    /*
     Function name :get_rights()
     Parameter :none.
     Return : returns result of list of assign rights from rights table else returns 0
     Use : getting list of rights that can be given to admin.
     */
    function get_rights()
    {
        $query = $this->db->query("select * from rights order by rights_id asc");
        return $query->result();
    }


    /*
     Function name :add_rights()
     Parameter :none.
     Return : none
     Use : adding or updating rights of any admin..
     */
    function add_rights()
    {

        $get_rights = $this->db->query("select * from rights order by rights_id asc");
        $all_rights = $get_rights->result();


        $rights_id = $this->input->post('rights_id');
        $admin_id = $this->input->post('admin_id');

        if ($rights_id) {
            foreach ($all_rights as $rig) {
                if (in_array($rig->rights_id, $rights_id)) {
                    $detail = $this->db->query("select * from rights_assign where rights_id='" . $rig->rights_id . "' and admin_id='" . $admin_id . "'");
                    if ($detail->num_rows() > 0) {
                        $update = $this->db->query("update rights_assign set rights_set='1' where rights_id='" . $rig->rights_id . "' and admin_id='" . $admin_id . "'");
                    } else {
                        $insert = $this->db->query("insert into rights_assign(`admin_id`,`rights_id`,`rights_set`)values('" . $admin_id . "','" . $rig->rights_id . "','1')");
                    }
                } else {
                    $detail = $this->db->query("select * from rights_assign where rights_id='" . $rig->rights_id . "' and admin_id='" . $admin_id . "'");
                    if ($detail->num_rows() > 0) {
                        $remove = $this->db->query("update rights_assign set rights_set='0' where rights_id='" . $rig->rights_id . "' and admin_id='" . $admin_id . "'");
                    } else {
                        $insert_remove = $this->db->query("insert into rights_assign(`admin_id`,`rights_id`,`rights_set`)values('" . $admin_id . "','" . $rig->rights_id . "','0')");
                    }
                }
            }
        }
    }

    //===========Functions Not used in new admin panel===============

    //Still need to check sooo dont delete it....=============
    function get_adninistrator_result()
    {
        $query = $this->db->query("select * from admin where admin_type = '2'");
        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return 0;
    }


    function get_admin_result()
    {
        $this->db->where('admin_type !=', 1);
        $this->db->order_by('admin_id', 'desc');
        $query = $this->db->get('admin');


        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return 0;
    }

    function admin_insert_ip()
    {
        $data = array(
            'email' => $this->input->post('email'),
            'username' => $this->input->post('username'),
            'password' => $this->input->post('password'),
            'admin_type' => $this->input->post('admin_type'),
            'login_ip' => $this->input->post('login_ip'),
            'active' => $this->input->post('active'),
            'date_added' => date('Y-m-d'),

        );
        $this->db->insert('admin', $data);

        $CI =& get_instance();
        $base_url = $CI->config->slash_item('base_url_site');
        $base_path = $CI->config->slash_item('base_path');

        $file = $base_path . 'admin/.htaccess';

        $put_content = 'allow from ' . $this->input->post('login_ip');

        $fh = fopen($file, 'r');

        $content = '';

        while (!feof($fh)) {
            $content .= fgets($fh) . "<br/>";
        }

        $content = $content . ' ' . $put_content;

        $content = str_replace("<br/>", '', $content);

        $new_content = $content;

        $fw = fopen($file, 'w');
        fwrite($fw, '');
        fwrite($fw, $new_content);

        fclose($fw);
        fclose($fh);


    }

    function admin_update_ip()
    {

        $get_details = $this->db->query("select * from admin where admin_id='" . $this->input->post('admin_id') . "'");
        $user_detail = $get_details->row();

        $orig_login_ip = $user_detail->login_ip;

        $content_original = 'allow from ' . $orig_login_ip;


        $CI =& get_instance();
        $base_url = $CI->config->slash_item('base_url_site');
        $base_path = $CI->config->slash_item('base_path');

        $file = $base_path . 'admin/.htaccess';

        $content_replace = 'allow from ' . $this->input->post('login_ip');

        $fh = fopen($file, 'r');

        $content = '';

        while (!feof($fh)) {
            $content .= fgets($fh) . "<br/>";
        }

        $content = str_replace($content_original, $content_replace, $content);

        $content = str_replace("<br/>", '', $content);

        $fw = fopen($file, 'w');
        fwrite($fw, '');
        fwrite($fw, $content);

        fclose($read_file);

        $data = array(
            'email' => $this->input->post('email'),
            'username' => $this->input->post('username'),
            'password' => $this->input->post('password'),
            'admin_type' => $this->input->post('admin_type'),
            'login_ip' => $this->input->post('login_ip'),
            'active' => $this->input->post('active'),
            'date_added' => date('Y-m-d'),

        );
        $this->db->where('admin_id', $this->input->post('admin_id'));
        $this->db->update('admin', $data);


    }

    /*
     Function name :PasswordMatches()
     Parameter : No.
     Return : TRUE Or FAlSE
     Use : It match the old password and user_id it exist in table admin then it returns TRUE Otherwise FALSE
     */
    function PasswordMatches()
    {
        $this->db->where('admin_id', $this->session->userdata('admin_id'));
        $this->db->where('password', md5($this->input->post('old_password')));
        $query = $this->db->get('admin');
        $this->db->last_query();
        if ($query->num_rows() > 0) {

            return true;
        } else {
            return false;
        }

    }
    //===========Functions Not used in new admin panel end===============
    /*	function get_total_admin_count()
     {
     return $this->db->count_all('admin');
     }
     *
     * 	function get_total_superadmin_count()
     {
     $query = $this->db->query("select * from admin where admin_type = '1'");
     return $query->num_rows();
     }

     function get_superadmin_result()
     {
     $query = $this->db->query("select * from admin where admin_type = '1'");
     if ($query->num_rows() > 0) {
     return $query->result();
     }
     return 0;
     }
     *
     * function get_total_adninistrator_admin_count()
     {
     $query = $this->db->query("select * from admin where admin_type = '2'");
     return $query->num_rows();
     }
     *
     * function get_total_adminlogin_count()
     {
     $query = $this->db->query("select a.username,a.password,a.admin_type,a.email,ad.* from admin_login ad left join admin a on ad.admin_id=a.admin_id order by ad.login_id desc");


     return $query->num_rows();
     }

     *
     function get_total_search_admin_count($option,$keyword)
     {
     $keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',$keyword));

     $this->db->select('admin.*');
     $this->db->from('admin');

     if($option=='username')
     {
     $this->db->like('username',$keyword);

     if(substr_count($keyword,' ')>=1)
     {
     $ex=explode(' ',$keyword);

     foreach($ex as $val)
     {
     $this->db->like('username',$val);
     }
     }

     }
     if($option=='email')
     {
     $this->db->like('email',$keyword);

     if(substr_count($keyword,' ')>=1)
     {
     $ex=explode(' ',$keyword);

     foreach($ex as $val)
     {
     $this->db->like('email',$val);
     }
     }

     }
     if($option=='admintype')
     {
     $this->db->like('admintype',$keyword);

     if(substr_count($keyword,' ')>=1)
     {
     $ex=explode(' ',$keyword);

     foreach($ex as $val)
     {
     $this->db->like('admintype',$val);
     }
     }

     }

     $this->db->order_by("admin_id", "desc");

     $query = $this->db->get();

     return $query->num_rows();
     }



     function get_search_admin_result($option,$keyword,$offset, $limit)
     {

     $keyword=str_replace('"','',str_replace(array("'",",","%","$","&","*","#","(",")",":",";",">","<","/"),'',$keyword));

     $this->db->select('admin.*');
     $this->db->from('admin');

     if($option=='username')
     {
     $this->db->like('username',$keyword);

     if(substr_count($keyword,' ')>=1)
     {
     $ex=explode(' ',$keyword);

     foreach($ex as $val)
     {
     $this->db->like('username',$val);
     }
     }

     }
     if($option=='email')
     {
     $this->db->like('email',$keyword);

     if(substr_count($keyword,' ')>=1)
     {
     $ex=explode(' ',$keyword);

     foreach($ex as $val)
     {
     $this->db->like('email',$val);
     }
     }

     }


     $this->db->order_by("admin_id", "desc");
     $this->db->limit($limit,$offset);
     $query = $this->db->get();
     if ($query->num_rows() > 0) {

     return $query->result();
     }
     return 0;
     } */

    /*
Function name :user_validate()
Parameter :user_data and check => used to when user login site
Return : user data in array
Use : This function is use to check this user is valid or not.
Description :
Done By : Dharmesh
*/
    function email_validate($email, $admin_id)
    {

        $this->db->where('email', $email);
        $this->db->where('admin_id != ', $admin_id);
        $query = $this->db->get('admin');

        if ($query->num_rows() > 0) {


            return $query->row_array();

        } else {
            return 0;
        }

    }

}

?>