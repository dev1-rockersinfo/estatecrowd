<?php
class Phase_model extends CI_Model{
	
	public $id;
	public $name;
	public $is_parent;
	public $status;
	public $created_at;
	public $table='phases';

	function __construct(){
		parent::__construct();
		$this->postData();
		$this->setSegment();
	}

	function setSegment()
    {
        $id = $this->uri->segment(4,0);
        ctype_digit($id) and $this->id=$id;
    }

	function postData(){
		if($this->input->post()){
			foreach ($_POST as $key => $value) {
				$this->$key = $value;
			}
		}
	}

	function getAllPhases(){
		$query = $this->db->query("SELECT count(`equity`.`equity_id`) as total_equity, `phases`.* FROM `phases` left join `equity` on (`equity`.`main_phase`=`phases`.`id` or `equity`.`sub_phase`=`phases`.`id`) and `equity`.`status` IN (1,2,3,4,5,6,7,8)   GROUP BY `phases`.`id`");
		return $query->result_array();
		//return $this->db->get($this->table)->result_array();	
	}

	function getPhaseById(){
		$data = $this->db->get_where($this->table,array('id'=>$this->id))->row_array();
		if($data){
			foreach ($data as $key => $value) {
				$this->$key=$value;
			}
		}
	}

	function addPhase(){
		$this->setRules();
		if($this->form_validation->run() == false)return false;

		$insert = array(
				'name'=>$this->name,
				'is_parent'=>$this->is_parent,
				'status'=>$this->status,
				'created_at'=>date('Y-m-d h:i:s')
			);
		$this->db->insert($this->table,$insert);
		$this->session->set_flashdata('msg','insert');
		return $this->db->insert_id();
	}

	function updatePhase(){
		$this->setRules();
		if($this->form_validation->run() == false)return false;

		$insert = array(
				'name'=>$this->name,
				'is_parent'=>$this->is_parent,
				'status'=>$this->status,
				'created_at'=>date('Y-m-d h:i:s')
			);
		
		$this->db->where('id',$this->id);
		$this->db->update($this->table,$insert);
		$this->session->set_flashdata('msg','update');
		return true;
	}

	function deletePhase(){
		$this->db->where('id',$this->id);
		$this->db->delete($this->table);
		$this->session->set_flashdata('msg','delete');
		return true;
	}

	function setRules(){
		$this->form_validation->set_rules('name',NAME,'required|trim');
		$this->form_validation->set_rules('is_parent',TYPE,'required');
		$this->form_validation->set_rules('status',STATUS,'required');
	}
}