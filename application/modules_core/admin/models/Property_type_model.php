<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Property_type_model extends Rockers_Model{
	
	public $id;
	public $language_id;
	public $name;
	public $description;
	public $is_parent;
	public $status;
	public $created_at;

	function __construct(){
		parent::__construct();
		$this->postData();
		$this->setSegment();
	}

	function setSegment()
    {
        $id = $this->uri->segment(4,0);
        ctype_digit($id) and $this->id=$id;
    }

	function postData(){
		if($this->input->post()){
			foreach ($_POST as $key => $value) {
				$this->$key = $value;
			}
		}
	}

	function getAllPropertyType(){
		return $this->db->get('property_type')->result_array();	
	}

	function getpropertytype(){
		$data = $this->db->get_where('property_type',array('id'=>$this->id))->row_array();
		if($data){
			foreach ($data as $key => $value) {
				$this->$key=$value;
			}
		}
	}

	function addPropertyType(){
		$this->setRules();
		if($this->form_validation->run($this) == false)return false;
		
		$insert = array(
				'name'=>$this->name,
				'description'=>$this->description,
				'language_id'=>$this->language_id,
				'is_parent'=>$this->is_parent,
				'status'=>$this->status,
				'created_at'=>date('Y-m-d h:i:s'),
				

			);
		$this->db->insert('property_type',$insert);
		$this->session->set_flashdata('msg','insert');
		return $this->db->insert_id();
	}

	function updatePropertyType(){

		$this->setRules();
		if($this->form_validation->run($this) == false)return false;
					

			$insert = array(
				'name'=>$this->name,
				'description'=>$this->description,
				'language_id'=>$this->language_id,
				'is_parent'=>$this->is_parent,
				'status'=>$this->status,
				'created_at'=>date('Y-m-d h:i:s'),
				);
		$this->db->where('id',$this->id);
		$this->db->update('property_type',$insert);
		$this->session->set_flashdata('msg','update');
		return true;
	}

	function deletePropertyType(){
		$this->db->where('id',$this->id);
		$this->db->delete('property_type');
		$this->session->set_flashdata('msg','delete');
		return true;
	}

	function setRules(){
		$this->form_validation->set_rules('name',NAME,'required|trim');
		$this->form_validation->set_rules('description',DESCRIPTION,'trim');
		$this->form_validation->set_rules('is_parent',TYPE,'required');
		$this->form_validation->set_rules('status',STATUS,'required');
		$this->form_validation->set_rules('language_id',LANGUAGE,'required');
		

	}

	




}