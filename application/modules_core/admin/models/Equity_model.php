<?php

/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
class Equity_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    /*

     Function name :equity_update()

     Parameter :$key_name=Field name for where clause,$key_value=Field value, $table=table name, $data=data array of values you want to insert into table.

     Return : none

     Use : Common function for updating values into database.

     */

    function equity_update($key_name, $key_value, $table, $data)

    {

        $this->db->where($key_name, $key_value);

        $query = $this->db->update($table, $data);

        return true;

    }

    /*

     Function name :get_equity_tabledata()

     Parameter :$table=table name, $where=array of values whose information or details you want to get.

     Return : none

     Use : Common function for getting single record using where clause.

     */

    function get_equity_tabledata($table, $where = array())

    {

        $query = $this->db->get_where($table, $where);

        return $query->row_array();

    }

    /*

     Function name :get_history()

     Parameter :$table=table name, $where=array of values whose information or details you want to get.

     Return : none

     Use : Common function for getting whole record using where clause.

     */

    function get_history($table, $where = array())

    {

        $query = $this->db->get_where($table, $where);

        return $query->result_array();

    }

    /*

     Function name :contract_manage_data()

     Parameter :none

     Return : none

     Use : fetch the data

     */

    function contract_manage_data($equity_id = '')
    {

        // $query = $this->db->query("SELECT equity_investment_process.*,transaction.*,accreditation.accreditation_status,equity.equity_id,equity.project_name,user.user_id,user.user_name,user.last_name,user.image,user.profile_slug FROM `equity_investment_process` join transaction on transaction.preapproval_key=equity_investment_process.transaction_id join equity on equity.equity_id=equity_investment_process.equity_id join user on user.user_id=equity_investment_process.user_id left join accreditation on accreditation.user_id=equity_investment_process.user_id where equity_investment_process.equity_id=" . $equity_id . " order by equity_investment_process.id desc ");
        $this->db->select('investment_process.*,transaction.*,accreditation.accreditation_status,property.property_id,property.property_address,user.user_id,user.user_name,user.last_name,user.image,user.profile_slug');
        $this->db->from('investment_process');
        $this->db->join('accreditation', 'accreditation.user_id=investment_process.user_id ');
         $this->db->join('transaction', 'transaction.preapproval_key= investment_process.transaction_id');
        $this->db->join('property', 'investment_process.property_id= property.property_id');
        $this->db->join('user', 'investment_process.user_id= user.user_id', 'left');
        $this->db->order_by('transaction.transaction_id', 'desc');
        $where = "( `property`.`property_id` = '" . $equity_id . "' )";
         $this->db->where($where);
          if($_GET){
            // search variables
            $user_name = $this->input->get('user_name');
            $min_amount = $this->input->get('min_amount');
            $max_amount = $this->input->get('max_amount');
            $trans_id = $this->input->get('trans_id');
            $from_date = $this->input->get('from_date');
            $to_date = $this->input->get('to_date');

            //by project title
           

            //by user name
            if ($user_name != '' && strtoupper($user_name)!='ANONYMOUS' ){
                $user_name_arr=explode(' ', $user_name);
                  $where='';
                foreach ($user_name_arr as $value) {
                    if($where==''){
                       $where = "( `user`.`user_name` like '%" . $value . "%' OR `user`.`last_name` like '%" . $value . "%') ";
                    }else{

                        $where =$where. "and ( `user`.`user_name` like '%" . $value . "%' OR `user`.`last_name` like '%" . $value . "%')";
                    }
                   
                }
                 $this->db->where($where); 
            }else if( strtoupper($user_name)=='ANONYMOUS'){
                $where = "( `transaction`.`user_id` = '0' )";
                $this->db->where($where);
            }

            // by amount

            if($min_amount && $max_amount){
                $where = "( `transaction`.`amount` BETWEEN '".$min_amount."' AND '".$max_amount."' )";
                $this->db->where($where);
            }else if($min_amount){
                $where = "( `transaction`.`amount` >= '".$min_amount."')";
                $this->db->where($where);
            }else if($max_amount){
                $where = "( `transaction`.`amount` <= '".$max_amount."')";
                $this->db->where($where);
            }

          

            //by transaction id
            if ($trans_id != '') {
                $where = "`transaction`.`preapproval_key` like '%" . $trans_id . "%'";
               
                $this->db->where($where);
            }

            //between dates
            if ($from_date != '' && $to_date!='') { 
                $where = "(`transaction`.`transaction_date_time` BETWEEN '".date('Y-m-d', strtotime($from_date))."' AND '".date('Y-m-d', strtotime($to_date))."')";       
                $this->db->where($where);
            }
        }
        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            return $query->result_array();

        }
        return 0;
    }

    /*

     Function name :equity_contract_update()
     Parameter :$where,$table,$data
     Return : none
     Use : update the data

     */

    function equity_contract_update($where = array(), $table = '', $data = '')
    {

        $this->db->where($where);
        $query = $this->db->update($table, $data);

    }

    /*

     Function name :check_admin_authentication()

     Parameter :none

     Return : admin id or redirect page.

     Use : function to check authentication or not.

    */
    function check_admin_authentication()
    {


        if ($this->session->userdata('admin_id') != '') {
            return $this->session->userdata('admin_id');
        } else {
            redirect('admin/home/index');
        }

    }

    //------Common Equity function-------
    /*
    Function name :GetAllEquities()
    Parameter : user_id,equity_id,is_status,joinarr,limit,order
    Return : array of all Equities
    Use : Fetch all Equity particular Criteria
    
    Cases:
    
    case 1 : if user grater than zero then it fetch all the Equity with particular user id means it returns all Equity of  particular company owner.
    
    case 2 : if Equity grater than zero then it return particular Equity.
    
    
    case 3 : if Status equal to 0 than it returns Draft Equities.
             if Status equal to 1 than it returns Pending Equities.
             if Status equal to 2 than it returns Active Equities.
             if Status equal to 3 than it returns Success Equities.
             if Status equal to 4 than it returns unsuccess Equities.
             if Status equal to 5 than it returns Declined Equities.
             
             or
             
             if Status equal 0,1 than it returns both type of Equities.(Same for all others).
             
    case 4 : if all Equity will be sorted by given order variable values. 
            
            
    */

    function GetAllEquities($user_id = 0, $equity_id = '', $is_featured = 0, $is_status = '', $joinarr = array('user'), $limit = 10, $order = array('equity_id' => 'desc'), $offset = 0, $count = 'no')
    {

          //default Equity fields
        $selectfields = 'equity.is_featured,equity.equity_id,equity.equity_url,equity.date_added,equity.end_date,equity.minimum_raise,equity.goal,equity.cover_photo,
        equity.video_url,equity.video_image,equity.elevator_pitch,equity.equity_currency_code,equity.equity_currency_symbol,equity.status,
        equity.amount_get,equity.deal_highlight1,equity.deal_highlight2,equity.deal_highlight3,equity.deal_highlight4,equity.elevator_pitch,equity.deal_type_name,
        equity.available_shares,equity.price_per_share,equity.equity_available,equity.company_valuation,equity.interest,equity.term_length,equity.valuation_cap,equity.conversation_discount,equity.warrant_coverage,
        equity.debt_interest,equity.debt_term_length,equity.return,equity.return_percentage,equity.maximum_return,equity.payment_frequency,equity.payment_start_date,equity.company_fundraising,equity.date_round_open,
        equity.amount_raise_current_round,equity.maximum_raise,equity.funding_type,equity.executive_summary_status,equity.executive_summary_file,equity.term_sheet_status,equity.term_sheet_file,
        equity.reason_inactive_hidden,equity.cash_flow_status,equity.project_timezone,equity.allowed_investor,
        equity.investment_summary_text,equity.deal_highlight_title1,equity.deal_highlight_description1,equity.deal_highlight_title2,equity.deal_highlight_description2,equity.deal_highlight_title3,equity.deal_highlight_description3,
        equity.deal_highlight_title4,equity.deal_highlight_description4,equity.project_description,equity.market_summary_text,equity.market_summary_image,equity.project_city,equity.project_state,equity.project_street,equity.project_country,equity.project_year_build,
        equity.main_phase,equity.sub_phase,equity.company_id,equity.project_name,equity.project_lot_size,equity.tax_relief_type,equity.bank_name,equity.bank_account_type,equity.bank_account_number,equity.routing_number,equity.contract_copy_file';

        //if require to join user
        if (in_array('user', $joinarr)) {
            $selectfields .= ',user.user_id,user.user_name,user.last_name,user.address, user.fb_uid, user.facebook_wall_post, user.fb_access_token,user.image,user.email,user.user_website,user.profile_slug,user.user_occupation';
        }
        if ($user_id > 0) {
            $selectfields .= ',invite_members.email as inviteemail';
        }


        $selectfields .= ',company_profile.company_name,company_profile.company_overview,company_profile.company_logo,company_profile.headquater_city,company_profile.headquater_state,company_profile.headquater_country,company_profile.company_logo,company_profile.company_linkedin_url,company_profile.company_facebook_url,company_profile.company_twitter_url,company_profile.website_url,company_profile.company_category,company_profile.year_founded,company_profile.company_industry,company_profile.assets_under_management,company_profile.square_footage,company_profile.company_email,company_profile.company_cover_photo,company_profile.company_url';
        

        $this->db->select($selectfields);
        $this->db->from('equity');
        /*        <!-- change by darshan start-->*/
        if ($_GET) {

            $search_by = $this->input->get('search_by');
            $where = '';
            $search_status = $this->input->get('search_status');
            if ($search_status != '') {

                $where .= "( `equity`.`status` = '" . $search_status . "' )";
                $this->db->where($where);
            }
            $search_goal_per = $this->input->get('search_goal_per');
            if ($search_goal_per != '') {
                $where = '';
                $where .= "( (`equity`.`amount_get`/`equity`.`goal`)*100 = '" . $search_goal_per . "')   ";
                $this->db->where($where);
            }
            $search_goal_from = $this->input->get('search_goal_from');
            $search_goal_to = $this->input->get('search_goal_to');
            if ($search_goal_from != '' and $search_goal_to != '') {
                $where = '';
                $where .= "( `equity`.`amount_get` between '" . $search_goal_from . "' and '" . $search_goal_to . "' )";
                $this->db->where($where);
            } else {
                if ($search_goal_from != '') {
                    $where = '';
                    $where .= "( `equity`.`amount_get` >= '" . $search_goal_from . "'  ) ";
                    $this->db->where($where);
                }
                if ($search_goal_to != '') {
                    $where = '';
                    $where .= "( `equity`.`amount_get` <= '" . $search_goal_to . "'  ) ";
                    $this->db->where($where);
                }

            }
            $search_category = $this->input->get('search_category');
            if ($search_category != '') {
                $where = '';
                $where .= "( `company_profile`.`company_category` = '" . $search_category . "' ) ";
                $this->db->where($where);
            }
            if ($search_by != '') {
                switch ($search_by) {

                    case 'project_name':
                        $search_name = $this->input->get('search_name');
                        $where = '';
                        $where .= "( `equity`.`project_name` like '%" . $search_name . "%' )";
                        $this->db->where($where);

                        break;
                    case 'owner_name':
                        $search_name = $this->input->get('search_name');
                         $val_array=explode(' ', trim($search_name));
                     
                         $where = '';
                         $this->db->group_start();
                         foreach ($val_array as $search_name) {
                          if($where==''){
                                $where .= " (`user`.`user_name` like '%" . $search_name . "%' or `user`.`last_name` like '%" . $search_name . "%') ";
                           }else{
                                $where .= " and ( `user`.`user_name` like '%" . $search_name . "%' or `user`.`last_name` like '%" . $search_name . "%') ";
                           }
                           
                          }
                           $this->db->where($where);
                           $this->db->group_end();

                        break;


                }
            }
        }
        /*        <!-- change by darshan end-->*/

        //if require to join user
        if (in_array('user', $joinarr)) {
            $this->db->join('user', 'equity.user_id=user.user_id');

        }
        if ($user_id > 0) {
            $this->db->join('invite_members', 'equity.equity_id=invite_members.equity_id', 'left');
        }

        //check all equity status
        if ($is_status != '') {
            $this->db->where('equity.status in (' . $is_status . ')');
        }

        
        $this->db->join('company_profile', 'equity.company_id=company_profile.company_id','left');

        //check equity id
        if ($equity_id > 0 or $equity_id != '') {
            if ($equity_id > 0) {
                $this->db->where('equity.equity_id', $equity_id);
                $this->db->or_where('equity.equity_url', $equity_id);
            } else {
                $this->db->where('equity.equity_url', $equity_id);
            }
        }

        //check user_id
        if ($user_id > 0) {
            // $this->db->where('user.user_id',$user_id);
            $where = '';
            $where .= "( `user`.`user_id` = '" . $user_id . "' ";
            $sql = " ( select i.email from invite_members as i inner join equity as e on i.equity_id=e.equity_id inner join user as u  on u.email=i.email where i.status=1 and i.admin=1 and u.user_id= '" . $user_id . "' )";
            //$sql=" ( invite_members.status=1 and invite_members.admin=1 and  invite_members.email= user.email )";
            $where .= " or `invite_members`.`email` in " . $sql . " )";

            $this->db->where($where);

        }
        if (is_array($order)) {
            foreach ($order as $key => $val) {
                $this->db->order_by($key, $val);
            }
        }

        if ($user_id > 0) {
            $this->db->group_by('equity.equity_id');
        }
        if ($limit > 0) $this->db->limit($limit, $offset);

        $query = $this->db->get();

        if ($count == 'yes') {
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else {
                return 0;
            }
        }

    }


    /*

     Function name :delete_project()
     Parameter :$id
     Return : none
     Use : delete perticular id data

    */

    function delete_equity($id)

    {

        $base_path = $this->config->slash_item('base_path');
        $chk_updates = $this->db->query("select * from updates where equity_id='" . $id . "'");

        if ($chk_updates->num_rows() > 0) {

            $updates = $chk_updates->result();

            foreach ($updates as $up) {
                /* if(is_file($base_path.'upload/thumb/'.$up->image))

                 {

                     if($up->image!='no_img.jpg')

                     {

                         $link1=$base_path.'upload/thumb/'.$up->image;

                         unlink($link1);

                     }



                 }*/


                if (is_file($base_path . 'upload/orig/' . $up->image)) {

                    if ($up->image != 'no_img.jpg') {

                        $link2 = $base_path . 'upload/orig/' . $up->image;

                        unlink($link2);

                    }
                }

                $this->db->query("delete from updates where update_id='" . $up->update_id . "'");

            }

        }

        /////===gallery=====


        $chk_gallery = $this->db->query("select * from equity_gallery where equity_id='" . $id . "'");

        if ($chk_gallery->num_rows() > 0) {

            $gallery = $chk_gallery->result();
            foreach ($gallery as $gal) {

                /* if(is_file($base_path.'upload/thumb/'.$gal->project_image))

                 {
                     if($gal->project_image!='no_img.jpg')

                     {

                         $link1=$base_path.'upload/thumb/'.$gal->project_image;

                         unlink($link1);

                     }



                 }*/

                if (is_file($base_path . 'upload/orig/' . $gal->image_name)) {
                    if ($gal->image_name != 'no_img.jpg') {

                        $link2 = $base_path . 'upload/orig/' . $gal->image_name;

                        unlink($link2);

                    }


                }


                $this->db->query("delete from equity_gallery where equity_gallery_id='" . $gal->equity_gallery_id . "'");

            }


        }


        /////===perk=====


        $chk_perk = $this->db->query("select * from perk where equity_id='" . $id . "'");

        if ($chk_perk->num_rows() > 0) {
            $perk = $chk_perk->result();

            foreach ($perk as $prk) {

                $this->db->query("delete from perk where equity_id='" . $prk->equity_id . "'");


            }


        }


        /////===transaction=====


        $chk_trans = $this->db->query("select * from transaction where equity_id='" . $id . "'");
        if ($chk_trans->num_rows() > 0) {

            $trans = $chk_trans->result();

            foreach ($trans as $trn) {

                $this->db->query("delete from transaction where equity_id='" . $trn->equity_id . "'");


            }


        }


        /////===comment=====


        $chk_comment = $this->db->query("select * from comment where equity_id='" . $id . "'");


        if ($chk_comment->num_rows() > 0) {


            $comment = $chk_comment->result();


            foreach ($comment as $cmt) {


                $this->db->query("delete from comment where comment_id='" . $cmt->comment_id . "'");

            }


        }

        //////======wallet======


        //////====project====


        $chk_project = $this->db->query("select * from equity where equity_id='" . $id . "'");

        if ($chk_project->num_rows() > 0) {

            $project = $chk_project->row();


            if (is_file($base_path . 'upload/orig/' . $project->cover_photo)) {
                if ($project->cover_photo != 'no_img.jpg') {

                    $link2 = $base_path . 'upload/orig/' . $project->cover_photo;

                    unlink($link2);

                }


            }


            $this->db->query("delete from equity where equity_id='" . $id . "'");

        }


    }


    /*
    Function name :GetUpdateCommentGallery()
    Use : function to get data of perk, updates, all gallery.
    */

    function GetUpdateCommentGallery($user_id = 0, $equity_id = 0, $is_status = '', $join = array('user'), $limit = 10, $group = array('user_id'), $order = array('user_id' => 'desc'), $comment_status = '', $offset = 0, $count = 'no')
    {
        $selectfields = 'equity.equity_url,equity.project_name';

        //if require to join comment

        if (in_array('comment', $join)) {
            $selectfields .= ',comment.comment_id,comment.comments,comment.date_added,comment.user_id,comment.comment_ip,comment.status,comment.comment_type';
        }


        if (in_array('user', $join)) {
            $selectfields .= ',user.user_name,user.user_id,user.last_name,user.image';
        }


        if (in_array('perk', $join)) {
            $selectfields .= ',perk.perk_id,perk.equity_id,perk.perk_title,perk.perk_description,perk.perk_amount,perk.perk_total,perk.perk_get,perk.estdate,perk.shipping_status';
        }


        if (in_array('updates', $join)) {
            $selectfields .= ',updates.updates,updates.date_added,updates.update_id,user.user_name,user.user_id,user.last_name,user.image';
        }


        if (in_array('project_gallery', $join)) {

            $selectfields .= ',project_gallery.*';

        }

        if (in_array('video_gallery', $join)) {

            $selectfields .= ',video_gallery.*';

        }


        if (in_array('file_gallery', $join)) {

            $selectfields .= ',file_gallery.*';

        }


        $this->db->select($selectfields);


        $this->db->from('equity');


        //if require to join comment


        if (in_array('comment', $join)) {

            $this->db->join('comment', 'equity.equity_id=comment.equity_id');

        }


        if (in_array('perk', $join)) {
            $this->db->join('perk', 'equity.equity_id=perk.equity_id');
        }


        if (in_array('user', $join)) {
            $this->db->join('user', 'comment.user_id=user.user_id');
        }


        if (in_array('updates', $join)) {
            $this->db->join('updates', 'equity.equity_id=updates.equity_id');
            $this->db->join('user', 'equity.user_id=user.user_id');
        }


        if (in_array('equity_gallery', $join)) {
            $this->db->join('equity_gallery', 'equity.equity_id=equity_gallery.equity_id');
        }

        if (in_array('video_gallery', $join)) {
            $this->db->join('video_gallery', 'equity.equity_id=video_gallery.equity_id');
        }

        if (in_array('file_gallery', $join)) {
            $this->db->join('file_gallery', 'equity.equity_id=file_gallery.equity_id');
        }

        if (in_array('comment', $join)) {
            if ($comment_status == 1) {
                $this->db->where('comment.status in (' . $comment_status . ')');
            }

        }


        if (in_array('video_gallery', $join)) {
            $this->db->where("video_gallery.media_video_title !='' ");
        }

        if (in_array('equity_gallery', $join)) {

            $this->db->where("equity_gallery.image !='' ");

        }

        if (in_array('file_gallery', $join)) {

            $this->db->where("file_gallery.file_path !='' ");

        }

        if ($is_status != '') {
            $this->db->where('equity.status in (' . $is_status . ')');
        }


        if ($user_id > 0) {
            $this->db->where('equity.user_id', $user_id);
        }

        if ($equity_id > 0) {
            $this->db->where('equity.equity_id', $equity_id);
        }

        if (is_array($group)) {

            foreach ($group as $key => $grp) {

                $this->db->group_by($grp);

            }

        }


        if (is_array($order)) {

            foreach ($order as $key => $val) {

                $this->db->order_by($key, $val);

            }

        }

        if ($limit > 0) $this->db->limit($limit, $offset);

        // echo $this->db->last_query();

        $query = $this->db->get();

        // echo "<br><br><br>";


        //echo $this->db->last_query();

        //die;

        //echo "<br><br><br>";

        if ($count == 'yes') {

            return $query->num_rows();

        } else {

            if ($query->num_rows() > 0) {

                return $query->result_array();

            } else {

                return 0;

            }

        }

    }


    function get_donations($id)
    {

        $query2 = "select * from transaction where equity_id='" . $id . "' order by transaction_id desc";
        $s_result2 = $this->db->query($query2);


        if ($s_result2->num_rows() > 0) {
            return $s_result2->result_array();
        }
        return 0;
    }

    function equity_activities($equity_id)
    {
        $sql = "select user_activity.* , user.user_id,user.profile_slug,user.user_name,user.last_name,user.image from user_activity  inner join user  on user.user_id=user_activity.user_id    where user_activity.campaign_id='" . $equity_id . "' order by `read` asc,datetime desc ";
        $query = $this->db->query($sql);
        //echo $this->db->last_query(); 
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return 0;
    }

    function dynamic_update()

    {
        $CI =& get_instance();
        $base_path = $CI->config->slash_item('base_path');

        if ($_FILES) {
            if ($_FILES['image_load']['name'] != '') {
                $this->load->library('upload');
                $rand = rand(0, 100000);

                $_FILES['userfile']['name'] = $_FILES['image_load']['name'];
                $_FILES['userfile']['type'] = $_FILES['image_load']['type'];
                $_FILES['userfile']['tmp_name'] = $_FILES['image_load']['tmp_name'];
                $_FILES['userfile']['error'] = $_FILES['image_load']['error'];
                $_FILES['userfile']['size'] = $_FILES['image_load']['size'];

                $imagename = OnlyUpload($_FILES);

                $cat_image = $imagename['orig'];

            } else {

                if ($this->input->post('dynamic_image_image') != '') {
                    $cat_image = $this->input->post('dynamic_image_image');
                }
            }
        }


        $data = array(

            'dynamic_image_title' => $this->input->post('slider_name'),
            'dynamic_image_paragraph' => $this->input->post('slider_content'),
            'dynamic_image_image' => $cat_image,
            'color_picker' => $this->input->post('color_picker'),
            'small_text' => $this->input->post('small_text'),
            'color_picker_content' => $this->input->post('color_picker_content'),
            'link' => $this->input->post('link'),
            'link_name' => $this->input->post('link_name'),
            'font_type_id' => $this->input->post('font_type_name'),
            'active' => $this->input->post('active')
        );

        $this->db->where('dynamic_slider_id', $this->input->post('dynamic_id'));

        $this->db->update('dynamic_slider', $data);

        $query = $this->db->query("select max(view_order) as latest_order from slider_setting ");

        if ($query->num_rows() > 0) {

            $result = $query->row();

            $new_view_order = $result->latest_order + 1;

        } else {

            $new_view_order = 1;

        }

        $data_slider = array(

            'slider_id' => $this->input->post('dynamic_id'),
            'view_order' => $new_view_order

        );

        $this->db->where('slider_id', $this->input->post('dynamic_id'));

        $this->db->update('slider_setting', $data_slider);


    }

    function get_one_dynamic($id)
    {
        $query = $this->db->get_where('dynamic_slider', array('dynamic_slider_id' => $id));
        return $query->row_array();

    }


    function dynamic_insert()

    {
        $CI =& get_instance();
        $base_path = $CI->config->slash_item('base_path');
        $cat_image = '';
        if ($_FILES) {
            if ($_FILES['image_load']['name'] != '') {
                $this->load->library('upload');
                $rand = rand(0, 100000);

                $_FILES['userfile']['name'] = $_FILES['image_load']['name'];
                $_FILES['userfile']['type'] = $_FILES['image_load']['type'];
                $_FILES['userfile']['tmp_name'] = $_FILES['image_load']['tmp_name'];
                $_FILES['userfile']['error'] = $_FILES['image_load']['error'];
                $_FILES['userfile']['size'] = $_FILES['image_load']['size'];

                $imagename = OnlyUpload($_FILES);

                $cat_image = $imagename['orig'];

            } else {
                if ($this->input->post('dynamic_image_image') != '') {
                    $cat_image = $this->input->post('dynamic_image_image');
                }
            }
        }


        $data = array(

            'dynamic_image_title' => $this->input->post('slider_name'),
            'dynamic_image_paragraph' => $this->input->post('slider_content'),
            'dynamic_image_image' => $cat_image,
            'small_text' => $this->input->post('small_text'),
            'color_picker' => $this->input->post('color_picker'),
            'color_picker_content' => $this->input->post('color_picker_content'),
            'link' => $this->input->post('link'),
            'link_name' => $this->input->post('link_name'),
            'font_type_id' => $this->input->post('font_type_name'),
            'active' => $this->input->post('active')
        );

        $this->db->insert('dynamic_slider', $data);
        $slider_id = $this->db->insert_id();

        $query = $this->db->query("select max(view_order) as latest_order from slider_setting ");

        if ($query->num_rows() > 0) {

            $result = $query->row();

            $new_view_order = $result->latest_order + 1;

        } else {

            $new_view_order = 1;

        }
        $data_slider = array(

            'slider_id' => $slider_id,
            'view_order' => $new_view_order

        );

        $this->db->insert('slider_setting', $data_slider);


    }

    function fonts()
    {
        $query = $this->db->get('font_type');

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return 0;
    }

     /*

     Function name :InvestorRunningProcessHistory()

     Parameter :none

     Return : none

     Use : fetch the data of running investment

     */

    function InvestorRunningProcessHistory($count='no')
    {

        $query = $this->db->query("SELECT investment_process.*,campaign.property_id,campaign.investment_close_date,campaign.lead_investor_id as property_owner_id,user.user_id,user.user_name,user.last_name,user.image,user.profile_slug FROM `investment_process` join campaign on campaign.campaign_id=investment_process.campaign_id join user on user.user_id=investment_process.user_id where campaign.status in ('2','3') and investment_process.status = '0' order by DATEDIFF(investment_process.created_date,campaign.investment_close_date) asc ");
     
          if ($count == 'yes') {
            return $query->num_rows();
        	} else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else {
                return 0;
            }
        }

    }

    function getPhases(){
        return $this->db->get('phases')->result_array(); 
    }

    function addPhases($id=0,$insert){
        $this->db->where('equity_id',$id);
        $this->db->delete('equity_phases');

        $this->db->insert('equity_phases',$insert);
        return true;
    }
      /*

     Function name :add_tax_relief_data()

     Parameter :$id=id

     Return : none

     Use : function to insert record and update record into table.

   */

    function add_tax_relief_data($id = '', $array1 = array())
    {

        if ($id == '') {
            $this->db->insert('investment_tax_relief', $array1);
            $msg = INSERT;
            return $msg;
        } else {
            $this->db->where('id', $this->input->post('id'));
            $this->db->update('investment_tax_relief', $array1);
            $msg = UPDATE_MSG;
            return $msg;
        }

    }

}

?>
