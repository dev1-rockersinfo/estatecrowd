<script type="text/javascript" language="javascript">
    function delete_rec(id) {
        var ans = confirm("<?php echo DELETE_CATEGORY_CONFIRMATION;?>");
        if (ans) {
            location.href = "<?php echo site_url('admin/language/delete_language/'); ?>" + "/" + id + "/";
        } else {
            return false;
        }
    }

</script>
<!-- BEGIN CONTAINER -->
<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php echo $this->load->view('admin_sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN THEME CUSTOMIZER-->
                    <div id="theme-change" class="hidden-phone">
                        <i class="icon-cogs"></i>
                        <span class="settings">
                          <span class="text"><?php echo THEME; ?>:</span> <!-- chaneg  by darshan -->
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
                    </div>
                    <!-- END THEME CUSTOMIZER-->
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        <?php echo LANGUAGES; ?>
                    </h3>
                    <ul class="breadcrumb">
                        <li>
                            <a href="<?php echo base_url() ?>admin/home/dashboard"><i class="icon-home"></i></a><span
                                class="divider">&nbsp;</span>
                        </li>

                        <li><a href="#"><?php echo LANGUAGES; ?></a><span class="divider-last">&nbsp;</span></li>
                    </ul>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <?php
            if ($msg) {
                ?>
                <div class="alert alert-success">
                    <button class="close" data-dismiss="alert">x</button>
                    <strong><?php echo SUCCESS; ?>!</strong> <?php echo $msg; ?>.
                </div>
            <?php
            }
            ?>
            <!-- BEGIN ADVANCED TABLE widget-->
            <?php /*?><a href="<?php echo base_url();?>admin/language/add_language" class="btn mini purple fr marB5"><i class="icon-edit"></i> <?php echo ADD_LANGUAGE; ?></a><?php */ ?>

            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN EXAMPLE TABLE widget-->
                    <div class="widget">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i><?php echo LANGUAGES; ?></h4>
                            <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                            </span>
                        </div>
                        <div class="widget-body">

                            <?php
                            /*$attributes = array('name'=>'frm_listproject','id'=>'frm_listproject');
                            echo form_open_multipart('language/action_language/',$attributes);*/
                            ?>
                            <input type="hidden" name="action" id="action"/>
                            <table class="table table-striped table-bordered" id="sample_1">
                                <thead>
                                <tr>

                                    <th style="width:8px;"><input type="checkbox" class="group-checkable"
                                                                  data-set="#sample_1 .checkboxes"/></th>

                                    <th><?php echo LANGUAGE_NAME; ?></th>
                                    <th><?php echo STATUS; ?></th>
                                    <th><?php echo LANGUAGE_FOLDER; ?></th>
                                    <th><?php echo COUNTRY_FLAG; ?></th>
                                    <th><?php echo ACTION; ?></th>

                                </tr>

                                </thead>
                                <tbody>

                                <?php
                                if ($result) {
                                    $i = 0;

                                    foreach ($result as $row) {
                                        ?>

                                        <tr class="odd gradeX">
                                            <td><input type="checkbox" name="chk[]" class="checkboxes" id="chk"
                                                       value="<?php echo $row->language_id; ?>"/></td>


                                            <td><?php echo $row->language_name; ?></td>

                                            <td><?php if ($row->active == "1") {
                                                    echo "Active";
                                                } else {
                                                    echo "Inactive";
                                                } ?></td>

                                            <td><?php echo $row->language_folder; ?></td>

                                            <td><?php echo $row->country_flag; ?></td>

                                            <td> <?php echo anchor('admin/language/edit_language/' . $row->language_id . '/', 'Edit'); ?>
                                                / <a href="#" onclick="delete_rec('<?php echo $row->language_id; ?>')">Delete</a>
                                            </td>

                                        </tr>

                                        <?php
                                        $i++;
                                    }


                                }


                                ?>

                                </tbody>
                            </table>
                            </form>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE widget-->
                </div>
            </div>

            <!-- END ADVANCED TABLE widget-->

            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>
<!-- END CONTAINER -->

<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/DT_bootstrap.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        App.init();
    });
</script>
