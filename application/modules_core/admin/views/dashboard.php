<link href="<?php echo base_url(); ?>assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet"/>

<link href="<?php echo base_url(); ?>assets/jqvmap/jqvmap/jqvmap.css" media="screen" rel="stylesheet" type="text/css"/>

<!-- BEGIN CONTAINER -->

<div id="container" class="row-fluid">

<!-- BEGIN SIDEBAR -->

<?php echo $this->load->view('admin_sidebar'); ?>

<!-- END SIDEBAR -->

<!-- BEGIN PAGE -->

<div id="main-content">

<!-- BEGIN PAGE CONTAINER-->

<div class="container-fluid">

<!-- BEGIN PAGE HEADER-->

<div class="row-fluid">

    <div class="span12">

        <!-- BEGIN THEME CUSTOMIZER-->

        <div id="theme-change" class="hidden-phone">

            <i class="icon-cogs"></i>

							<span class="settings">

                                <span class="text"><?php echo THEME; ?></span>

                                <span class="colors">

                                    <span class="color-default" data-style="default"></span>

                                    <span class="color-gray" data-style="gray"></span>

                                    <span class="color-purple" data-style="purple"></span>

                                    <span class="color-navy-blue" data-style="navy-blue"></span>

                                </span>

							</span>

        </div>

        <!-- END THEME CUSTOMIZER-->

        <!-- BEGIN PAGE TITLE & BREADCRUMB-->

        <h3 class="page-title">

            <?php echo DASHBOARD; ?>

            <small> <?php echo GENERAL_INFORMATION; ?> </small>

        </h3>

        <ul class="breadcrumb">

            <li>

                <a href="<?php echo site_url('admin/home/dashboard') ?>"><i class="icon-home"></i></a><span
                    class="divider">&nbsp;</span>

            </li>


            <li><a href="<?php echo site_url('admin/home/dashboard') ?>"><?php echo DASHBOARD; ?></a><span
                    class="divider-last">&nbsp;</span></li>


        </ul>

        <!-- END PAGE TITLE & BREADCRUMB-->

    </div>

</div>

<!-- END PAGE HEADER-->

<!-- BEGIN PAGE CONTENT-->

<div id="page" class="dashboard">

<!-- BEGIN OVERVIEW STATISTIC BLOCKS-->

<div class="row-fluid circle-state-overview">

                <?php if($total_users > 0)$total_value = (($active_users * 100) / $total_users) - (($inactive_users * 100) / $total_users); ?>

<!-- 
<a class="icon-btn span2" href="#">
                                <i class="icon-group"></i>
                                <div><?php echo NEW_USERS; ?></div>
                                <span class="badge badge-info"><?php echo $total_users; ?></span>
                            </a>


<a class="icon-btn span2" href="#">
                                <i class="icon-exclamation-sign"></i>
                                <div><?php echo PENDING_PROJECTS; ?></div>
                                <span class="badge badge-info"><?php echo $total_pending_proj; ?></span>
                            </a>

<a class="icon-btn span2" href="#">
                                <i class="icon-refresh"></i>
                                <div><?php echo RUNNING_PROJECTS; ?></div>
                                <span class="badge badge-info"><?php echo $total_active_proj; ?></span>
                            </a>
<a class="icon-btn span2" href="#">
                                <i class="icon-thumbs-up"></i>
                                <div><?php echo SUCCESSFUL_PROJECTS; ?></div>
                                <span class="badge badge-info"><?php echo $total_success_proj; ?></span>
                            </a>
<a class="icon-btn span2" href="#">
                                <i class="icon-thumbs-down"></i>
                                <div><?php echo FAILURE_PROJECTS; ?></div>
                                <span class="badge badge-info"><?php echo $total_failure_proj; ?></span>
                            </a>

<a class="icon-btn span2" href="#">
                                <i class="icon-money"></i>
                                <div><?php echo RUNNING_INVESTMENT; ?></div>
                                <span class="badge badge-info"><?php echo $total_running_investment; ?></span>
                            </a>  -->
    <div class="span2 responsive" data-tablet="span3" data-desktop="span2">

        <div class="circle-stat block">

            <div class="visual">

                <div class="circle-state-icon">

                    <i class="icon-user darkpurple-color" style="color:#625893"></i>

                </div>

                <input class="knob" data-width="100" data-height="100" data-displayPrevious=true data-thickness=".2"
                       value="<?php echo $total_users; ?>" data-fgColor="#625893" data-bgColor="#ddd">

            </div>

            <div class="details">

                <div class="number"><?php echo $total_users; ?></div>

                <div class="title"><?php echo NEW_USERS; ?></div>

            </div>


        </div>

    </div>

    <div class="span2 responsive" data-tablet="span3" data-desktop="span2">

        <div class="circle-stat block">

            <div class="visual">

                <div class="circle-state-icon">

                    <i class="icon-pushpin orange-color" style="color:#ee663c"></i>

                </div>

                <input class="knob" data-width="100" data-height="100" data-displayPrevious=true data-thickness=".2"
                       value="<?php echo $total_pending_proj; ?>" data-fgColor="#ee663c" data-bgColor="#ddd">

            </div>

            <div class="details">

                <div class="number"><?php echo $total_pending_proj; ?></div>

                <div class="title"><?php echo PENDING_PROJECTS; ?></div>

            </div>


        </div>

    </div>

    <div class="span2 responsive" data-tablet="span3" data-desktop="span2">

        <div class="circle-stat block">

            <div class="visual">

                <div class="circle-state-icon">

                    <i class="icon-ok-sign navblue-color" style="color:#235c87"></i>

                </div>

                <input class="knob" data-width="100" data-height="100" data-displayPrevious=true data-thickness=".2"
                       value="<?php echo $total_active_proj; ?>" data-fgColor="#235c87" data-bgColor="#ddd"/>

            </div>

            <div class="details">

                <div class="number"><?php echo $total_active_proj; ?></div>

                <div class="title"><?php echo RUNNING_PROJECTS; ?></div>

            </div>


        </div>

    </div>

    <div class="span2 responsive" data-tablet="span3" data-desktop="span2">

        <div class="circle-stat block">

            <div class="visual">

                <div class="circle-state-icon">

                    <i class="icon-thumbs-up darkgreen-color" style="color:#84c200"></i>

                </div>

                <input class="knob" data-width="100" data-height="100" data-displayPrevious=true data-thickness=".2"
                       value="<?php echo $total_success_proj; ?>" data-fgColor="#84c200" data-bgColor="#ddd"/>

            </div>

            <div class="details">

                <div class="number"><?php echo $total_success_proj; ?></div>

                <div class="title"><?php echo SUCCESSFUL_PROJECTS; ?></div>

            </div>


        </div>

    </div>

    <div class="span2 responsive" data-tablet="span3" data-desktop="span2">

        <div class="circle-stat block">

            <div class="visual">

                <div class="circle-state-icon">

                    <i class="icon-thumbs-down darkred-color" style="color:#fc0107"></i>

                </div>

                <input class="knob" data-width="100" data-height="100" data-displayPrevious=true data-thickness=".2"
                       value="<?php echo $total_failure_proj; ?>" data-fgColor="#fc0107" data-bgColor="#ddd"/>

            </div>

            <div class="details">

                <div class="number"><?php echo $total_failure_proj; ?></div>

                <div class="title"><?php echo FAILURE_PROJECTS; ?></div>

            </div>


        </div>

    </div>


    <div class="span2 responsive" data-tablet="span3" data-desktop="span2">

        <div class="circle-stat block">

            <div class="visual">

                <div class="circle-state-icon">

                    <i class="icon-thumbs-up lightgreen-color" style="color:#2F942D"></i>

                </div>

                <input class="knob" data-width="100" data-height="100" data-displayPrevious=true data-thickness=".2"
                       value="<?php echo $total_running_investment; ?>" data-fgColor="#2F942D" data-bgColor="#ddd"/>

            </div>

            <div class="details">

                <div class="number"><a href="<?php echo site_url('admin/equity/runningInvestment')?>"><?php echo $total_running_investment; ?></a></div>

                <div class="title"><?php echo RUNNING_INVESTMENT; ?></div>

            </div>


        </div>

    </div>



</div>

<!-- END OVERVIEW STATISTIC BLOCKS-->


<div class="row-fluid ">

<div class="span12">

<!-- BEGIN TAB PORTLET-->

<div class="widget widget-tabs">

<div class="widget-title">

    <h4><i class="icon-reorder"></i><?php echo DASHBOARD; ?></h4>


</div>


<div class="widget-body1">

<div class="tabbable portlet-tabs">

<ul class="nav nav-tabs">

    <li><a href="#portlet_tab3" data-toggle="tab"><?php echo TRANSACTION; ?></a></li>

    <li><a href="#portlet_tab2" data-toggle="tab"><?php echo USER; ?></a></li>

    <li class="active"><a href="#portlet_tab1" data-toggle="tab"><?php echo PROJECT; ?></a></li>

</ul>


<div class="tab-content">

<div class="tab-pane active" id="portlet_tab1">

<!--Last 10 Pending Projects-->

<div class="row-fluid">

    <div class="span12">

        <!-- BEGIN BORDERED TABLE widget-->

        <div class="widget">

            <div class="widget-title">

                <h4><i class="icon-reorder"></i><?php echo LAST_10_PENDING_PROJECTS; ?></h4>

                        <span class="tools">

                        <a href="javascript:;" class="icon-chevron-down"></a>

                        

                        </span>

            </div>

            <?php

            $taxonomy_setting = taxonomy_setting();
            $project_url = 'properties';
            ?>

            <div class="widget-body limit_height">

                <table class="table table-bordered table-hover">

                    <thead>

                    <?php

                    if ($pending_project)

                    {

                    // echo '<pre>';

                    // print_r($pending_project);

                    // die;

                    ?>

                    <tr>

                        <th class="tab_hidden ph_hidden"><?php echo NO; ?></th>

                        <th><?php echo PROJECT; ?></th>

                        <th class="ph_hidden"><?php echo USERNAME; ?></th>

                        <th class="ph_hidden"><?php echo POSTED_DATE; ?></th>

                        <th class="ph_hidden"><?php echo END_DATE; ?></th>

                        <th><?php echo PROJECT_GOAL; ?></th>


                    </tr>

                    </thead>

                    <tbody>

                    <?php

                    $i = 1;

                    foreach ($pending_project as $prj) {

                        $comp_equity_id = $prj['equity_id'];

                        $comp_project_url_title = $prj['property_url'];

                        $property_address = $prj['property_address'];

                        $comp_project_user = ucwords($prj['user_name'] . ' ' . $prj['last_name']);

                        $comp_project_date_added = ($prj['date_added'] != "") ? date($site_setting['date_format'], strtotime($prj['date_added'])) : '';

                        $comp_project_end_date = ($prj['end_date'] != "") ? date($site_setting['date_format'], strtotime($prj['end_date'])) : '';

                        $comp_project_amount = set_currency($prj['min_investment_amount'], $prj['equity_id']);

                        //$comp_project_fees=set_currency(($prj['total_listing_fee']+$prj['total_pay_fee']),$prj['equity_id']);


                        ?>

                        <tr>

                            <td class="tab_hidden ph_hidden"><?php echo $i; ?></td>

                            <td>

                                <a href="<?php echo site_url($project_url . '/' . $prj['property_url'] . '/' . $prj['equity_id']); ?>"
                                   target="_blank">

                                    <?php echo $property_address; ?></a></td>

                            <td class="ph_hidden"><a href="<?php echo site_url('member/' . $prj['profile_slug']); ?>"
                                                     target="_blank">

                                    <?php echo $comp_project_user; ?></a></td>

                            <td class="ph_hidden"><?php echo $comp_project_date_added; ?></td>

                            <td class="ph_hidden"><?php echo $comp_project_end_date; ?></td>

                            <td><?php echo $comp_project_amount; ?></td>


                        </tr>

                        <?php

                        $i++;

                    }

                    } else {
                        ?>

                        <tr>

                            <th class="ph_hidden"><strong><?php echo NO_RECORD_FOUND; ?></strong></th>

                        </tr>

                    <?php } ?>

                    </tbody>

                </table>

            </div>

        </div>

        <!-- END BORDERED TABLE widget-->

    </div>

</div>


<!--Last 5 New Running Projects-->

<div class="row-fluid">

    <div class="span12">

        <!-- BEGIN BORDERED TABLE widget-->

        <div class="widget">

            <div class="widget-title">

                <h4><i class="icon-reorder"></i><?php echo LAST_5_NEWRUNNING_PROJECTS; ?></h4>

                                        <span class="tools">

                                        <a href="javascript:;" class="icon-chevron-down"></a>

                                       

                                        </span>

            </div>

            <div class="widget-body limit_height">

                <table class="table table-bordered table-hover">

                    <thead>

                    <?php



                    if ($running_project)



                    {


                    ?>



                    <tr>

                        <th class="tab_hidden ph_hidden"><?php echo NO; ?></th>

                        <th><?php echo PROJECT; ?></th>

                        <th class="ph_hidden"><?php echo USERNAME; ?></th>

                        <th class="ph_hidden"><?php echo POSTED_DATE; ?></th>

                        <th><?php echo PROJECT_GOAL; ?></th>

                        <th class="ph_hidden"><?php echo AMOUNT_RAISED; ?></th>


                    </tr>

                    </thead>

                    <tbody>

                    <?php

                    $i = 1;

                    foreach ($running_project as $prj) {

                        $post_campaign_id = $prj['equity_id'];

                        $property_url = $prj['property_url'];

                        $property_address = $prj['property_address'];

                        $post_project_user = ucwords($prj['user_name'] . ' ' . $prj['last_name']);

                        $post_project_date_added = ($prj['date_added'] != "") ? date($site_setting['date_format'], strtotime($prj['date_added'])) : '';

                        $post_project_end_date = ($prj['end_date'] != "") ? date($site_setting['date_format'], strtotime($prj['end_date'])) : '';

                        $post_project_amount = set_currency($prj['min_investment_amount'], $post_campaign_id);

                        $equity_amount_get = set_currency($prj['amount_get'], $prj['equity_id']);





                        ?>

                        <tr>

                            <td class="tab_hidden ph_hidden"><?php echo $i; ?></td>

                            <td>
                                <a href="<?php echo site_url($project_url . '/' . $prj['property_url'] . '/' . $prj['equity_id']); ?>"
                                   target="_blank">

                                    <?php echo $property_address; ?></a></td>

                            <td class="ph_hidden"><a href="<?php echo site_url('user/' . $prj['profile_slug']); ?>"
                                                     target="_blank">

                                    <?php echo $post_project_user; ?></a></td>

                            <td class="ph_hidden"><?php echo $post_project_date_added; ?></td>

                            <td><?php echo $post_project_amount; ?></td>

                            <td><?php echo $equity_amount_get; ?></td>

                            <?php /*?>          <td class="ph_hidden"><?php if(($prj['amount']-$prj['total_amount'])<0){

                                    	echo set_currency(($prj['total_amount']-$prj['amount']),$prj['equity_id'])."&nbsp;<i class='icon-plus'></i>"; }

                                    	else{ echo set_currency(($prj['amount']-$prj['total_amount']),$prj['equity_id']); } ?></td> <?php */
                            ?>


                        </tr>

                        <?php

                        $i++;

                    }

                    } else {
                        ?>

                        <tr>

                            <th class="ph_hidden"><strong><?php echo NO_RECORD_FOUND; ?></strong></th>

                        </tr>

                    <?php } ?>

                    </tbody>

                </table>

            </div>

        </div>

        <!-- END BORDERED TABLE widget-->

    </div>

</div>


<!--Last 5 New Completed Projects-->

<div class="row-fluid">

    <div class="span12">

        <!-- BEGIN BORDERED TABLE widget-->

        <div class="widget">

            <div class="widget-title">

                <h4><i class="icon-reorder"></i><?php echo LAST_5_COMPLETED_PROJECTS; ?></h4>

                                        <span class="tools">

                                        <a href="javascript:;" class="icon-chevron-down"></a>

                                       

                                        </span>

            </div>

            <div class="widget-body limit_height">

                <table class="table table-bordered table-hover">

                    <thead>

                    <?php



                    if ($completed_project)



                    {


                    ?>



                    <tr>

                        <th class="tab_hidden ph_hidden"><?php echo NO; ?></th>

                        <th><?php echo PROJECT; ?></th>

                        <th class="ph_hidden"><?php echo USERNAME; ?></th>

                        <th class="ph_hidden"><?php echo POSTED_DATE; ?></th>

                        <th><?php echo DONATE_AMT; ?></th>

                        <th><?php echo EARNING; ?></th>


                    </tr>

                    </thead>

                    <tbody>

                    <?php

                    $i = 1;

                    foreach ($completed_project as $prj) {

                        $post_campaign_id = $prj['equity_id'];

                        $property_url = $prj['property_url'];

                        $property_address = $prj['property_address'];

                        $post_project_user = ucwords($prj['user_name'] . ' ' . $prj['last_name']);

                        $post_project_date_added = ($prj['date_added'] != "") ? date($site_setting['date_format'], strtotime($prj['date_added'])) : '';

                        $post_project_end_date = ($prj['end_date'] != "") ? date($site_setting['date_format'], strtotime($prj['end_date'])) : '';

                        $post_project_amount_get = set_currency($prj['total_amount']);

                        $post_project_fees = set_currency($prj['total_listing_fee'] + $prj['total_pay_fee']);



                        ?>

                        <tr>

                            <td class="tab_hidden ph_hidden"><?php echo $i; ?></td>

                            <td>
                                <a href="<?php echo site_url($project_url . '/' . $prj['property_url'] . '/' . $prj['equity_id']); ?>"
                                   target="_blank">

                                    <?php echo $property_address; ?></a></td>

                            <td class="ph_hidden"><a href="<?php echo site_url('user/' . $prj['profile_slug']); ?>"
                                                     target="_blank">

                                    <?php echo $post_project_user; ?></a></td>

                            <td class="ph_hidden"><?php echo $post_project_date_added; ?></td>

                            <td><?php echo $post_project_amount_get; ?></td>

                            <td><?php echo $post_project_fees; ?></td>


                        </tr>

                        <?php

                        $i++;

                    }

                    } else {
                        ?>

                        <tr>

                            <th class="ph_hidden"><strong><?php echo NO_RECORD_FOUND; ?></strong></th>

                        </tr>

                    <?php } ?>

                    </tbody>

                </table>

            </div>

        </div>

        <!-- END BORDERED TABLE widget-->

    </div>

</div>


<!--Last 5 Failed Projects-->

<div class="row-fluid">

    <div class="span12">

        <!-- BEGIN BORDERED TABLE widget-->

        <div class="widget">

            <div class="widget-title">

                <h4><i class="icon-reorder"></i><?php echo LAST_5_FAILED_PROJECTS; ?></h4>

                                        <span class="tools">

                                        <a href="javascript:;" class="icon-chevron-down"></a>

                                       

                                        </span>

            </div>

            <div class="widget-body limit_height">

                <table class="table table-bordered table-hover">

                    <thead>

                    <?php



                    if ($failure_project)



                    {


                    ?>



                    <tr>

                        <th class="tab_hidden ph_hidden"><?php echo NO; ?></th>

                        <th><?php echo PROJECT; ?></th>

                        <th class="ph_hidden"><?php echo USERNAME; ?></th>

                        <th class="ph_hidden"><?php echo POSTED_DATE; ?></th>

                        <th><?php echo DONATE_AMT; ?></th>


                    </tr>

                    </thead>

                    <tbody>

                    <?php

                    $i = 1;

                    foreach ($failure_project as $prj) {

                        $post_campaign_id = $prj['campaign_id'];

                        $property_url = $prj['property_url'];

                        $property_address = $prj['property_address'];

                        $post_project_user = ucwords($prj['user_name'] . ' ' . $prj['last_name']);

                        $post_project_date_added = ($prj['date_added'] != "") ? date($site_setting['date_format'], strtotime($prj['date_added'])) : '';

                        $post_project_end_date = ($prj['end_date'] != "") ? date($site_setting['date_format'], strtotime($prj['end_date'])) : '';

                        $post_project_amount_get = set_currency($prj['total_amount']);

                        $post_project_fees = set_currency($prj['total_listing_fee'] + $prj['total_pay_fee']);



                        ?>

                        <tr>

                            <td class="tab_hidden ph_hidden"><?php echo $i; ?></td>

                            <td>
                                <a href="<?php echo site_url($project_url . '/' . $prj['property_url'] . '/' . $prj['equity_id']); ?>"
                                   target="_blank">

                                    <?php echo $property_address; ?></a></td>

                            <td class="ph_hidden"><a href="<?php echo site_url('user/' . $prj['profile_slug']); ?>"
                                                     target="_blank">

                                    <?php echo $post_project_user; ?></a></td>

                            <td class="ph_hidden"><?php echo $post_project_date_added; ?></td>

                            <td><?php echo $post_project_amount_get; ?></td>


                        </tr>

                        <?php

                        $i++;

                    }

                    } else {
                        ?>

                        <tr>

                            <th class="ph_hidden"><strong><?php echo NO_RECORD_FOUND; ?></strong></th>

                        </tr>

                    <?php } ?>

                    </tbody>

                </table>

            </div>

        </div>

        <!-- END BORDERED TABLE widget-->

    </div>

</div>


</div>

<div class="tab-pane" id="portlet_tab2">


</div>

<div class="tab-pane" id="portlet_tab3">

<div class="row-fluid">

<div class="span12">

<!-- BEGIN INLINE NOTIFICATIONS widget-->

<div class="widget">

<div class="widget-title">

    <h4><i class="icon-cogs"></i><?php echo TRANSACTION; ?></h4>

    <div class="tools">

        <a href="javascript:;" class="collapse"></a>

        <a href="#widget-config" data-toggle="modal" class="config"></a>

        <a href="javascript:;" class="reload"></a>


    </div>

</div>

<div class="">

<!--Last 10 New Donor-->

<div class="row-fluid">

    <div class="span12">

        <!-- BEGIN BORDERED TABLE widget-->

        <div class="widget">

            <div class="widget-title">

                <h4><i class="icon-reorder"></i><?php echo LAST_5_NEW_PROJECTS; ?></h4>

                                        <span class="tools">

                                        <a href="javascript:;" class="icon-chevron-down"></a>

                                        </span>

            </div>

            <div class="widget-body limit_height">

                <table class="table table-bordered table-hover">

                    <thead>

                    <?php

                    if ($latest_donors)

                    {

                    ?>

                    <tr>

                        <th class="tab_hidden ph_hidden"><?php echo NO; ?></th>

                        <th><?php echo PROJECT; ?></th>

                        <th><?php echo USERNAME; ?></th>

                        <th class="ph_hidden"><?php echo EMAIL; ?></th>

                        <th class="ph_hidden"><?php echo PAYEE_EMAIL; ?></th>

                        <th class="ph_hidden tab_hidden"><?php echo IP_ADDRESS; ?></th>

                        <th><?php echo FUNDS; ?></th>

                        <th><?php echo EARNING; ?></th>


                    </tr>

                    </thead>

                    <tbody>

                    <?php

                    $i = 1;

                    foreach ($latest_donors as $cust) {

                        $donor_property_address = ucfirst($cust['property_address']);

                        $donor_project_url_title = $cust['property_url'];

                        $donor_equity_id = $cust['campaign_id'];

                        if ($cust['user_id'] != '' && $cust['user_id'] != 0) {

                            $userdata = UserData($cust['user_id']);

                            $donor_name = ucfirst($userdata[0]['user_name'] . ' ' . $userdata[0]['last_name']);

                            $donor_user_id = $cust['user_id'];

                        } else {

                            $donor_name = 'N/A';

                        }

                     

                        //$equity = GetOneEquity($donor_equity_id);

                        $campaign_owner_data = UserData($cust['user_id']);

                        $donor_profile_slug = $campaign_owner_data[0]['profile_slug'];
                        $donor_email = $cust['email'];

                        $donor_project_paypal_email = $campaign_owner_data[0]['email'];

                        $donor_ip = $cust['host_ip'];

                        $donor_amount = set_currency($cust['donation_amount'], $cust['equity_id']);

                        $donor_fees = set_currency($cust['pay_fee']);
                        $currency = set_currency('',0, 'yes');


                        ?>

                        <tr>

                            <td class="tab_hidden ph_hidden"><?php echo $i; ?></td>

                            <td>

                                <a href="<?php echo site_url($project_url . '/' . $donor_project_url_title . '/' . $donor_equity_id); ?>"
                                   target="_blank">

                                    <?php echo $donor_property_address; ?></a></td>

                            <td><?php if ($cust['user_id'] != '' && $cust['user_id'] != 0) { ?><a
                                    href="<?php echo site_url('user/' . $donor_profile_slug); ?>" target="_blank">

                                    <?php echo $donor_name; ?></a><?php } else {
                                    echo $donor_name;
                                } ?></td>

                            <td class="ph_hidden"><?php echo $donor_email; ?></td>

                            <td class="ph_hidden"><?php echo $donor_project_paypal_email; ?></td>

                            <td class="ph_hidden tab_hidden"><?php echo $donor_ip; ?></td>

                            <td><?php echo $donor_amount; ?></td>

                            <td><?php echo $donor_fees; ?></td>


                        </tr>

                        <?php

                        $i++;


                    }


                    } else {


                        ?>



                        <tr>

                            <th class="ph_hidden"><strong><?php echo NO_RECORD_FOUND; ?></strong></th>

                        </tr>

                    <?php

                    }

                    ?>

                    </tbody>

                </table>

            </div>

        </div>

        <!-- END BORDERED TABLE widget-->

    </div>

</div>


<!--Last 10 New Donor-->


<div class="row-fluid">


</div>


</div>

</div>

<!-- END INLINE NOTIFICATIONS widget-->

</div>

</div>

</div>

</div>

</div>

</div>

</div>

<!-- END TAB PORTLET-->

</div>


</div>


</div>

<!-- END PAGE CONTENT-->

</div>

<!-- END PAGE CONTAINER-->

</div>

<!-- END PAGE -->

</div>

<!-- END CONTAINER -->


<!-- BEGIN JAVASCRIPTS -->

<!-- Load javascripts at bottom, this will reduce page load time -->


<script src="<?php echo base_url(); ?>assets/jquery-slimscroll/jquery-ui-1.9.2.custom.min.js"></script>

<script src="<?php echo base_url(); ?>assets/jquery-slimscroll/jquery.slimscroll.min.js"></script>

<script src="<?php echo base_url(); ?>assets/fullcalendar/fullcalendar/fullcalendar.min.js"></script>

<script src="<?php echo base_url(); ?>js/jquery.cookie.js"></script>

<script src="<?php echo base_url(); ?>assets/jquery-knob/js/jquery.knob.js"></script>

<script src="<?php echo base_url(); ?>js/jquery.peity.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>

<script src="<?php echo base_url(); ?>js/scripts.js"></script>

<script>

    jQuery(document).ready(function () {

        // initiate layout and plugins

        if (jQuery.isReady) {

            setTimeout(bindButtonClick, 1500);

        }

        //App.setMainPage(true);

        App.init();

    });


    function bindButtonClick() {

        $.ajax({

            url: '<?php echo site_url('admin/home/ajax_dhasboard_user'); ?>',

            dataType: 'html',

            type: 'post',

            success: function (responseHtml) {

                // here your .myClickableElement dom object is replaced with new (unbound) elements

                $('#portlet_tab2').html(responseHtml);


            }

        });

    }

</script>

<!-- END JAVASCRIPTS -->

