<!-- BEGIN CONTAINER -->
<div id="container" class="row-fluid">
<!-- BEGIN SIDEBAR -->
<?php echo $this->load->view('admin_sidebar'); ?>
<!-- END SIDEBAR -->
<!-- BEGIN PAGE -->
<div id="main-content">
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN THEME CUSTOMIZER-->
        <div id="theme-change" class="hidden-phone">
            <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text"><?php echo THEME; ?></span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
        </div>
        <!-- END THEME CUSTOMIZER-->
        <h3 class="page-title">
            <?php echo ACCREDITATION_USER; ?>
        </h3>
        <ul class="breadcrumb">
            <li>
                <a href="<?php echo site_url('admin/home/dashboard') ?>"><i class="icon-home"></i></a><span
                    class="divider">&nbsp;</span>
            </li>

            <li><a href="<?php echo site_url('admin/accreditation') ?>"><?php echo ACCREDITATION_USERS; ?></a><span
                    class="divider-last">&nbsp;</span></li>
        </ul>
    </div>
</div>
<!-- END PAGE HEADER-->

<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
<div class="span12 sortable">
<!-- BEGIN SAMPLE FORMPORTLET-->
<div class="widget">
<div class="widget-title">
    <h4><i class="icon-reorder"></i><?php echo ACCREDITATION_USERS_DETAIL; ?></h4>
                                        <span class="tools">
                                        <a href="javascript:;" class="icon-chevron-down"></a>
                                        <a href="javascript:;" class="icon-remove"></a>
                                        </span>
</div>
<div class="widget-body form-horizontal">
<!-- BEGIN FORM-->


<div class="control-group">
    <label class="control-label"><?php echo 'Full Name'; ?></label>

    <div class="controls mtop10">
        <?php echo $accredited_user['first_name'].' '.$accredited_user['last_name']; ?>
    </div>
</div>

<div class="control-group">
    <label class="control-label"><?php echo ADDRESS; ?></label>

    <div class="controls mtop10">
        <?php echo $accredited_user['address']; ?>
    </div>
</div>

<div class="control-group">
    <label class="control-label"><?php echo 'Date of Birth'; ?></label>

    <div class="controls mtop10">
        <?php echo $accredited_user['dob']; ?>
    </div>
</div>
<div class="control-group">
    <label class="control-label"><?php echo 'Passport Number or Medicare Number'; ?></label>

    <div class="controls mtop10">
        <?php echo $accredited_user['passport_number']; ?>
    </div>
</div>
<div class="control-group">
    <label class="control-label"><?php echo 'Drivers License Number'; ?></label>

    <div class="controls mtop10">
        <?php echo $accredited_user['license_number']; ?>
    </div>
</div>

<div class="control-group">
    <label class="control-label"><?php echo 'Permanent Residency Number'; ?></label>

    <div class="controls mtop10">
        <?php echo CheckValue($accredited_user['residency_number']); ?>
    </div>
</div>
<div class="control-group">
    <label class="control-label"><?php echo 'Phone Number'; ?></label>

    <div class="controls mtop10">
        <?php
        if ($accredited_user['phone'] != "0") {
            echo $accredited_user['phone'];
        }
        ?>
    </div>
</div>





<div class="control-group">
    <label class="control-label"><?php echo STATUS; ?></label>

    <div class="controls mtop10">
        <?php if ($accredited_user['accreditation_status'] == '1') {
            ?>
            <?php echo APPROVE; ?>
        <?php
        } else if ($accredited_user['accreditation_status'] == '2') {
            ?>
            <?php echo REJECTED; ?>
        <?php
        } else {
            ?>
            <?php echo PENDING; ?>
        <?php
        }?>
    </div>
</div>

<div class="form-actions">
    <?php
    if ($accredited_user['accreditation_status'] == "0") {
        ?>
        <a href="<?php echo site_url('admin/accreditation/approve/' . $accredited_user['id']); ?>"
           class="btn blue"><i class="icon-ok"></i> <?php echo APPROV; ?></a>
        <a href="<?php echo site_url('admin/accreditation/reject/' . $accredited_user['id']); ?>"
           class="btn "><i class="icon-remove"></i> <?php echo REJECT; ?></a>
    <?php
    } else if ($accredited_user['accreditation_status'] == '1') {
        ?>
        <a href="<?php echo site_url('admin/accreditation/reject/' . $accredited_user['id']); ?>"
           class="btn "><i class="icon-remove"></i> <?php echo YOU_CAN_REJECT; ?></a>
    <?php
    } else if ($accredited_user['accreditation_status'] == '2') {
        ?>
        <a href="<?php echo site_url('admin/accreditation/approve/' . $accredited_user['id']); ?>"
           class="btn blue"><i class="icon-ok"></i> <?php echo YOU_CAN_APPROVE; ?></a> 
    <?php
    }?>

</div>

<!-- END FORM-->
</div>
</div>
<!-- END SAMPLE FORM PORTLET-->
</div>
</div>

<!-- END PAGE CONTENT-->
</div>
<!-- END PAGE CONTAINER-->
</div>
<!-- END PAGE -->
</div>
<!-- END CONTAINER -->
<script type="text/javascript"
        src="<?php echo base_url(); ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        App.init();
    });
</script>
