<script type="text/javascript" language="javascript">

    function delete_rec(id) {

        var ans = confirm("<?php echo DELETE_USER_CONFIRMATION;?>");

        if (ans) {

            location.href = "<?php echo site_url('admin/user/delete_user/'); ?>" + "/" + id;

        } else {

            return false;

        }

    }


</script>


<style>

    .dataTables_filter label input[type=text] {

        width: 125px !important;

    }

    #example_length select {

        width: 125px !important;

    }

    @media (max-width: 788px) {

        .tfoot {

            display: none;

        }

    }


</style>

<!-- BEGIN CONTAINER -->

<div id="container" class="row-fluid">

<!-- BEGIN SIDEBAR -->

<?php echo $this->load->view('admin_sidebar'); ?>

<!-- END SIDEBAR -->

<!-- BEGIN PAGE -->

<div id="main-content">

<!-- BEGIN PAGE CONTAINER-->

<div class="container-fluid">

<!-- BEGIN PAGE HEADER-->

<div class="row-fluid">

    <div class="span12">

        <!-- BEGIN THEME CUSTOMIZER-->

        <div id="theme-change" class="hidden-phone">

            <i class="icon-cogs"></i>

                        <span class="settings">

                            <span class="text"><?php echo THEME; ?></span>

                            <span class="colors">

                                <span class="color-default" data-style="default"></span>

                                <span class="color-gray" data-style="gray"></span>

                                <span class="color-purple" data-style="purple"></span>

                                <span class="color-navy-blue" data-style="navy-blue"></span>

                            </span>

                        </span>

        </div>

        <!-- END THEME CUSTOMIZER-->

        <!-- BEGIN PAGE TITLE & BREADCRUMB-->

        <h3 class="page-title">

            <?php echo ACCREDITATION_USERS; ?>


        </h3>

        <ul class="breadcrumb">

            <li>

                <a href="<?php echo site_url('admin/home/dashboard') ?>"><i class="icon-home"></i></a><span
                    class="divider">&nbsp;</span>

            </li>


            <li>
                <a href="<?php echo site_url('admin/accreditation/ACCREDITATION_USER') ?>"> <?php echo ACCREDITATION_USERS; ?>     </a><span
                    class="divider-last">&nbsp;</span></li>

        </ul>

        <!-- END PAGE TITLE & BREADCRUMB-->

    </div>

</div>

<!-- END PAGE HEADER-->

<?php

if ($msg != "") {

    if ($msg == "insert") {
        ?>
        <div class="alert alert-success">

            <button class="close" data-dismiss="alert">x</button>

            <strong><?php echo SUCCESS; ?>!</strong> <?php echo NEW_RECORD_HAS_BEEN_ADDED_SUCCESSFULLY; ?></div>

    <?php
    }

    if ($msg == "update") {
        ?>

        <div class="alert alert-success">

            <button class="close" data-dismiss="alert">x</button>

            <strong><?php echo SUCCESS; ?>!</strong> <?php echo RECORD_HAS_BEEN_UPDATED_SUCCESSFULLY; ?></div>

    <?php
    }

    if ($msg == "delete") {
        ?>

        <div class="alert alert-success">

            <button class="close" data-dismiss="alert">x</button>

            <strong><?php echo SUCCESS; ?>!</strong> <?php echo RECORD_HAS_BEEN_DELETED_SUCCESSFULLY; ?></div>

    <?php
    }
}

?>



<!-- BEGIN ADVANCED TABLE widget-->

<div class="row-fluid">

    <div class="span12">

        <!-- BEGIN EXAMPLE TABLE widget-->

        <div class="widget">

            <div class="widget-title">

                <h4><i class="icon-reorder"></i><?php echo ACCREDITATION_USERS; ?></h4>

                            <span class="tools">

                                <a href="javascript:;" class="icon-chevron-down"></a>

                            </span>

            </div>


            <div class="widget-body">

                <table class="table table-striped table-bordered list_user" id="example">


                    <thead>


                    <tr>

                        <th><?php echo USERNAME ?></th>

                        <th><?php echo PHONE_NUMBER; ?></th>

                        <th><?php echo 'Date of Birth'; ?></th>

                        <th><?php echo 'Passport Number'; ?></th>


                        <th><?php echo 'Drivers License Number'; ?></th>

                        <th><?php echo VIEW; ?></th>

                        <th><?php echo ACTION; ?></th>

                    </tr>

                    </thead>


                    <tbody>

                    <?php

                    if ($result) {


                        foreach ($result as $row) {
                            $id = $row['id'];
                            $user_name = $row['first_name'] . ' ' . $row['last_name'];
                            $phone_number = $row['phone'];
                            $dob = $row['dob'];
                            $passport_number = $row['passport_number'];
                            $license_number = $row['license_number'];
                            $status = $row['accreditation_status'];




                            ?>



                            <tr class="odd gradeX">

                                <td><?php echo $user_name; ?></td>

                                <td><?php
                                    if ($phone_number != "0") {
                                        echo $phone_number;
                                    }
                                    ?>
                                </td>

                                <td><?php echo dateformat($dob); ?></td>

                                <td> <?php  if ($passport_number != "0") {
                                        echo $passport_number;
                                    }
                                    ?>
                                </td>

                                <td><?php echo $license_number; ?></td>

                                <td>
                                    <a href="<?php echo site_url('admin/accreditation/accreditation_user_view/' . $id); ?>"><?php echo VIEW; ?></a>
                                </td>

                                <td><?php if ($status == '0') { ?>

                                        <a href="<?php echo site_url('admin/accreditation/approve/' . $id); ?>"
                                           onclick='return confirm("Are you sure you want to approve?")'> <?php echo APPROV; ?></a> |
                                        <a href="<?php echo site_url('admin/accreditation/reject/' . $id); ?>"
                                           onclick='return confirm("Are you sure you want to reject?")'> <?php echo REJECT; ?></a>
                                    <?php
                                    } else if ($status == '1') {
                                        echo APPROVE;
                                    } else if ($status == '2') {
                                        echo REJECTED;
                                    }?>
                                </td>

                            </tr>

                        <?php

                        }

                    }

                    ?>

                    </tbody>

                </table>


            </div>

        </div>

        <!-- END EXAMPLE TABLE widget-->

    </div>

</div>

<!-- END ADVANCED TABLE widget-->


<!-- END PAGE CONTENT-->

</div>

<!-- END PAGE CONTAINER-->

</div>

<!-- END PAGE -->

</div>

<!-- END CONTAINER -->


<!-- BEGIN JAVASCRIPTS -->

<!-- Load javascripts at bottom, this will reduce page load time -->


<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/jquery.dataTables.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/DT_bootstrap.js"></script>

<script src="<?php echo base_url(); ?>js/scripts.js"></script>

<script>


    jQuery(document).ready(function () {

        // initiate layout and plugins

        App.init();

    });

</script>


<script type="text/javascript" charset="utf-8">

    (function ($) {

        /*

         * Function: fnGetColumnData

         * Purpose:  Return an array of table values from a particular column.

         * Returns:  array string: 1d data array

         * Inputs:   object:oSettings - dataTable settings object. This is always the last argument past to the function

         *           int:iColumn - the id of the column to extract the data from

         *           bool:bUnique - optional - if set to false duplicated values are not filtered out

         *           bool:bFiltered - optional - if set to false all the table data is used (not only the filtered)

         *           bool:bIgnoreEmpty - optional - if set to false empty values are not filtered from the result array

         * Author:   Benedikt Forchhammer <b.forchhammer /AT\ mind2.de>

         */

        $.fn.dataTableExt.oApi.fnGetColumnData = function (oSettings, iColumn, bUnique, bFiltered, bIgnoreEmpty) {

            // check that we have a column id

            if (typeof iColumn == "undefined") return new Array();


            // by default we only want unique data

            if (typeof bUnique == "undefined") bUnique = true;


            // by default we do want to only look at filtered data

            if (typeof bFiltered == "undefined") bFiltered = true;


            // by default we do not want to include empty values

            if (typeof bIgnoreEmpty == "undefined") bIgnoreEmpty = true;


            // list of rows which we're going to loop through

            var aiRows;


            // use only filtered rows

            if (bFiltered == true) aiRows = oSettings.aiDisplay;

            // use all rows

            else aiRows = oSettings.aiDisplayMaster; // all row numbers


            // set up data array

            var asResultData = new Array();


            for (var i = 0, c = aiRows.length; i < c; i++) {

                iRow = aiRows[i];

                var aData = this.fnGetData(iRow);

                var sValue = aData[iColumn];


                // ignore empty values?

                if (bIgnoreEmpty == true && sValue.length == 0) continue;



                // ignore unique values?

                else if (bUnique == true && jQuery.inArray(sValue, asResultData) > -1) continue;



                // else push the value onto the result data array

                else asResultData.push(sValue);

            }


            return asResultData;

        }
    }(jQuery));


    function fnCreateSelect(aData) {

        var r = '<select style="width:150px;"><option value=""><?php echo SELECT; ?></option>', i, iLen = aData.length;

        for (i = 0; i < iLen; i++) {

            r += '<option value="' + aData[i] + '">' + aData[i] + '</option>';

        }

        return r + '</select>';

    }


    $(document).ready(function () {

        /* Initialise the DataTable */

        var oTable = $('#example').dataTable({
            "oLanguage": {


                "sLengthMenu": "<?php echo DISPLAY; ?> _MENU_ <?php echo RECORD_PER_PAGE; ?>&nbsp;&nbsp;",
                "sZeroRecords": "<?php echo NOTHING_FOUND_SORRY; ?>",
                "sInfo": "<?php echo SHOWING; ?> _START_ to _END_ of _TOTAL_ <?php echo RECORD; ?>",
                "sInfoEmpty": "<?php echo SHOWING; ?> 0 to 0 of 0 <?php echo RECORD; ?>",
                "sSearch": "<?php echo SEARCH; ?>: ",
                'oPaginate': {

                    'sPrevious': '<?php echo PREVIOUS; ?>',
                    'sNext': '<?php echo NEXT; ?>'

                }

            }

        });


        /* Add a select menu for each TH element in the table footer */

        $(".tfoot th").each(function (i) {

            this.innerHTML = fnCreateSelect(oTable.fnGetColumnData(i));

            $('select', this).change(function () {

                oTable.fnFilter($(this).val(), i);

            });

        });

    });

</script>
