<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/map/gmap3.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/map/jquery-autocomplete.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/map/jquery-autocomplete.css"/>
<script type="text/javascript">
    $(function () {

        $('#test').gmap3();

        $('#address').autocomplete({
            source: function () {
                $("#test").gmap3({
                    action: 'getAddress',
                    address: $(this).val(),
                    callback: function (results) {
                        if (!results) return;
                        $('#address').autocomplete(
                            'display',
                            results,
                            false
                        );
                    }
                });
            },
            cb: {
                cast: function (item) {

                    return item.formatted_address;
                },
                select: function (item) {
                    $("#test").gmap3(
                        {action: 'clear', name: 'marker'},
                        {
                            action: 'addMarker',
                            latLng: item.geometry.location,
                            map: {center: true}
                        }
                    );
                }
            }


        });


        <?php  if($address!= ''){ ?>
        $('#test').gmap3({
            action: 'addMarker',
            address: "<?php  echo $address; ?>",
            map: {
                center: true,
                zoom: 14
            },
            marker: {
                options: {
                    draggable: true
                }
            }
        });
        <?php  } ?>


    });
</script>
<!-- BEGIN CONTAINER -->
<?php
$user_title = ADD_USER;
if ($user_id != '') {
    $user_title = EDIT_USER;
}?>
<div id="container" class="row-fluid">
<!-- BEGIN SIDEBAR -->
<?php echo $this->load->view('admin_sidebar'); ?>
<!-- END SIDEBAR -->
<!-- BEGIN PAGE -->
<div id="main-content">
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN THEME CUSTOMIZER-->
        <div id="theme-change" class="hidden-phone">
            <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text"><?php echo THEME; ?></span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
        </div>
        <!-- END THEME CUSTOMIZER-->
        <h3 class="page-title">
            <?php echo $user_title; ?>
        </h3>
        <ul class="breadcrumb">
            <li>
                <a href="<?php echo site_url('admin/home/dashboard') ?>"><i class="icon-home"></i></a><span
                    class="divider">&nbsp;</span>
            </li>

            <li><a href="<?php echo site_url('admin/user/list_user') ?>"><?php echo USER; ?></a><span
                    class="divider-last">&nbsp;</span></li>
        </ul>
    </div>
</div>
<!-- END PAGE HEADER-->
<?php
if ($error != "") {
    ?>
    <div class="alert alert-error">
        <button class="close" data-dismiss="alert">x</button>
        <strong><?php echo ERRORS; ?>!</strong> <?php echo $error; ?></div>
<?php
}
?>
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
<div class="span12 sortable">
<!-- BEGIN SAMPLE FORMPORTLET-->
<div class="widget">
<div class="widget-title">
    <h4><i class="icon-reorder"></i><?php echo $user_title; ?></h4>
                                        <span class="tools">
                                        <a href="javascript:;" class="icon-chevron-down"></a>
                                        <a href="javascript:;" class="icon-remove"></a>
                                        </span>
</div>
<div class="widget-body">
    <!-- BEGIN FORM-->
    <?php
    $attributes = array('name' => 'frm_login', 'class' => 'form-horizontal');
    echo form_open('admin/user/add_user', $attributes);
    ?>

    <div class="control-group">
        <label class="control-label"><?php echo FIRST_NAME; ?></label>

        <div class="controls">
            <input type="text" name="user_name" id="user_name" value="<?php echo $user_name; ?>"
                   placeholder="<?php echo FIRST_NAME; ?>" class="input-xlarge"/>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label"><?php echo LAST_NAME; ?></label>

        <div class="controls">
            <input type="text" name="last_name" id="last_name" value="<?php echo $last_name; ?>"
                   placeholder="<?php echo LAST_NAME; ?>" class="input-xlarge"/>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label"><?php echo EMAIL; ?> </label>

        <div class="controls">
            <input type="text" name="email" id="email" value="<?php echo $email; ?>" placeholder="<?php echo EMAIL; ?>"
                   class="input-xlarge"/>
        </div>
    </div>
    <?php
    if ($user_id == "") {
        ?>
        <div class="control-group">
            <label class="control-label"><?php echo PASSWORD; ?> </label>

            <div class="controls">
                <input type="password" name="password" id="password" value="<?php echo $password; ?>"
                       placeholder="*********" class="input-xlarge"/>
            </div>
        </div>
    <?php } else { ?>
        <input type="hidden" name="password" id="password" value="<?php echo $password; ?>"/>
    <?php } ?>

    <div class="control-group">
        <label class="control-label"><?php echo ADDRESS; ?> </label>

        <div class="controls">
            <input type="text" name="address" id="address" value="<?php echo $address; ?>"
                   placeholder="<?php echo ADDRESS; ?>" class="input-xlarge"/>
        </div>
        <div id="test" class="gmap3" style="margin:5px 0 5px 284px;"></div>
    </div>

    <!--<div class="control-group">
                                    <label class="control-label">Zip Code </label>
                                    <div class="controls">
                                       <input type="text" name="zip_code" id="zip_code" value="<?php echo $zip_code; ?>" placeholder="zip-code" class="input-xlarge" />
                                    </div>
                                </div>-->


    <div class="control-group">
        <label class="control-label"><?php echo ABOUT_YOURSELF; ?></label>

        <div class="controls">
            <textarea name="user_about" style=" width:350px; height:170px;"
                      class="input-xlarge"><?php echo $user_about; ?></textarea>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label"><?php echo OCCUPATION; ?> </label>

        <div class="controls">
            <input type="text" name="user_occupation" id="user_occupation" value="<?php echo $user_occupation; ?>"
                   placeholder="<?php echo OCCUPATION; ?>" class="input-xlarge"/>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label"><?php echo INTEREST; ?> </label>

        <div class="controls">
            <textarea name="user_interest" style=" width:350px; height:170px;"
                      class="input-xlarge"><?php echo $user_interest; ?></textarea>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label"><?php echo SKILLS; ?> </label>

        <div class="controls">
            <textarea name="user_skill" style=" width:350px; height:170px;"
                      class="input-xlarge"><?php echo $user_skill; ?></textarea>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label"><?php echo WEBSITE; ?> </label>

        <div class="controls">
            <input type="text" name="user_website" id="user_website" value="<?php echo $user_website; ?>"
                   placeholder="<?php echo WEBSITE; ?>" class="input-xlarge"/>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label"><?php echo FACEBOOK_PROFILE_URL; ?> </label>

        <div class="controls">
            <input type="text" name="facebook_url" id="facebook_url" value="<?php echo $facebook_url; ?>"
                   placeholder="<?php echo FACEBOOK; ?>" class="input-xlarge"/>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label"><?php echo TWITTER_PROFILE_URL; ?> </label>

        <div class="controls">
            <input type="text" name="twitter_url" id="twitter_url" value="<?php echo $twitter_url; ?>"
                   placeholder="<?php echo TWITTER; ?>" class="input-xlarge"/>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label"><?php echo LINKEEDIN_PROFILE_URL; ?> </label>

        <div class="controls">
            <input type="text" name="linkedln_url" id="linkedln_url" value="<?php echo $linkedln_url; ?>"
                   placeholder="<?php echo LINKEDIN; ?>" class="input-xlarge"/>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label"><?php echo GOOGLE_PLUS_PROFILE_URL; ?> </label>

        <div class="controls">
            <input type="text" name="googleplus_url" id="googleplus_url" value="<?php echo $googleplus_url; ?>"
                   placeholder="<?php echo GOOGLE_PLUS; ?>" class="input-xlarge"/>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label"><?php echo BANDCAMP_PROFILE_URL; ?> </label>

        <div class="controls">
            <input type="text" name="bandcamp_url" id="bandcamp_url" value="<?php echo $bandcamp_url; ?>"
                   placeholder="<?php echo BANDCAMP; ?>" class="input-xlarge"/>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label"><?php echo YOUTUBE_PROFILE_URL; ?></label>

        <div class="controls">
            <input type="text" name="youtube_url" id="youtube_url" value="<?php echo $youtube_url; ?>"
                   placeholder="<?php echo YOUTUBE; ?>" class="input-xlarge"/>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label"><?php echo MYSPACE_PROFILE_URL; ?> </label>

        <div class="controls">
            <input type="text" name="myspace_url" id="myspace_url" value="<?php echo $myspace_url; ?>"
                   placeholder="<?php echo MYSPACE; ?>" class="input-xlarge"/>
        </div>
    </div>

    <?php if ($user_id != "") { ?>
        <script type="text/javascript">
            function add_reason(value) {
                if (value == 2) {
                    document.getElementById('reason_editor').style.display = "block";
                }
                else {
                    document.getElementById('reason_editor').style.display = "none";
                }
            }
        </script>
    <?php } ?>

    <div class="control-group">
        <label class="control-label"><?php echo STATUS; ?></label>

        <div class="controls">
            <?php if ($user_id != "") { ?>
                <select name="active" id="active" onchange="add_reason(this.value);" class="input-large m-wrap"
                        tabindex="1">
                    <option value="0" <?php if ($active == 0) {
                        echo "selected";
                    } ?>><?php echo INACTIVE; ?></option>
                    <option value="1" <?php if ($active == 1) {
                        echo "selected";
                    } ?>><?php echo ACTIVE; ?></option>
                    <option value="2" <?php if ($active == 2) {
                        echo "selected";
                    } ?>><?php echo SUSPEND; ?></option>
                </select>
            <?php } else { ?>
                <select name="active" id="active" class="input-large m-wrap" tabindex="1">
                    <option value="0" <?php if ($active == 0) {
                        echo "selected";
                    } ?>><?php echo INACTIVE; ?></option>
                    <option value="1" <?php if ($active == 1) {
                        echo "selected";
                    } ?>><?php echo ACTIVE; ?></option>
                </select>
            <?php } ?>

        </div>
    </div>
    <?php if ($user_id != "") { ?>
        <div id="reason_editor" class="control-group" <?php if ($active == '2'){ ?>style="display:block;"
             <?php }else{ ?>style="display:none;"<?php } ?>>
            <label class="control-label"><?php echo ADD_SUSPEND_REASON; ?></label>

            <div class="controls">
                <textarea name="reason" style=" width:350px; height:170px;"
                          class="input-xlarge"><?php echo $suspend_reason; ?></textarea>
            </div>
        </div>
    <?php } ?>
    <div class="form-actions">
        <?php
        if ($user_id == "") {
            ?>
            <button type="submit" class="btn blue"><i class="icon-ok"></i><?php echo SUBMIT; ?></button>
        <?php } else { ?>
            <button type="submit" class="btn blue"><i class="icon-ok"></i><?php echo UPDATE; ?></button>
        <?php } ?>
        <button type="button" class="btn" onClick="location.href='<?php echo site_url('admin/user/list_user'); ?>'"><i
                class=" icon-remove"></i> <?php echo CANCEL; ?></button>
        <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id; ?>"/>
        <input type="hidden" name="offset" id="offset" value=""/>
    </div>
    </form>
    <!-- END FORM-->
</div>
</div>
<!-- END SAMPLE FORM PORTLET-->
</div>
</div>

<!-- END PAGE CONTENT-->
</div>
<!-- END PAGE CONTAINER-->
</div>
<!-- END PAGE -->
</div>
<!-- END CONTAINER -->
<script type="text/javascript"
        src="<?php echo base_url(); ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        App.init();
    });
</script>
