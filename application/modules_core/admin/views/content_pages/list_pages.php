<script type="text/javascript" language="javascript">
    function delete_rec(id) {
        var ans = confirm("<?php echo DELETE_PAGE_CONFIRMATION;?>");
        if (ans) {
            location.href = "<?php echo site_url('admin/pages/delete_pages/'); ?>" + "/" + id + "/";
        } else {
            return false;
        }
    }
</script>
<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php echo $this->load->view('admin_sidebar'); ?>

    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN THEME CUSTOMIZER-->
                    <div id="theme-change" class="hidden-phone">
                        <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text"><?php echo THEME; ?></span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
                    </div>
                    <!-- END THEME CUSTOMIZER-->
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        <?php echo PAGES; ?>

                    </h3>
                    <ul class="breadcrumb">
                        <li>
                            <a href="<?php echo site_url('admin/home/dashboard'); ?>"><i class="icon-home"></i></a><span
                                class="divider">&nbsp;</span>
                        </li>

                        <li><?php echo anchor('admin/pages/list_pages', 'Pages', 'id="sp_1"'); ?><span
                                class="divider-last">&nbsp;</span></li>
                    </ul>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <?php if ($msg != '') {

                if ($msg == 'delete') {
                    ?>
                    <div class="alert alert-success">
                        <button class="close" data-dismiss="alert">x</button>
                        <strong><?php echo SUCCESS; ?>!</strong> <?php echo RECORD_HAS_BEEN_DELETED_SUCCESSFULLY; ?>
                    </div>

                <?php
                }
                if ($msg == 'insert') {
                    ?>
                    <div class="alert alert-success">
                        <button class="close" data-dismiss="alert">x</button>
                        <strong><?php echo SUCCESS; ?>!</strong> <?php echo NEW_RECORD_HAS_BEEN_ADDED_SUCCESSFULLY; ?>
                    </div>

                <?php
                }
                if ($msg == 'update') {
                    ?>
                    <div class="alert alert-success">
                        <button class="close" data-dismiss="alert">x</button>
                        <strong><?php echo SUCCESS; ?>!</strong> <?php echo RECORD_HAS_BEEN_UPDATED_SUCCESSFULLY; ?>
                    </div>

                <?php
                }
            } ?>

            <a href=<?php echo site_url('admin/pages/add_pages'); ?> class="btn mini purple fr marB5"><i
                class="icon-edit"></i> <?php echo ADD; ?></a>

            <!-- BEGIN ADVANCED TABLE widget-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN EXAMPLE TABLE widget-->
                    <div class="widget">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i> <?php echo PAGES; ?></h4>
                            <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                            </span>
                        </div>
                        <div class="widget-body">
                            <table class="table table-striped table-bordered" id="sample_1">
                                <thead>
                                <tr>
                                    <th><?php echo PAGES_TITLE; ?></th>
                                    <th class="mob_hidden"><?php echo SIDEBAR; ?></th>
                                    <th class="mob_hidden"><?php echo FOOTER; ?></th>
                                    <th><?php echo ACTIVE; ?></th>
                                    <th><?php echo ACTION; ?></th>

                                </tr>
                                </thead>

                                <tbody>
                                <?php

                                if ($result) {
                                    $i = 0;
                                    foreach ($result as $row) {
                                        ?>

                                        <tr class="odd gradeX">

                                            <td><?php echo $row->pages_title; ?></td>
                                            <td class="mob_hidden"><?php if ($row->right_side == 'yes') {
                                                    echo $row->right_side;
                                                } ?></td>

                                            <td class="mob_hidden"><?php if ($row->footer_bar == 'yes') {
                                                    echo $row->footer_bar;
                                                } ?></td>

                                            <td><?php if ($row->active == "1") {
                                                    echo ACTIVE;
                                                } else {
                                                    echo INACTIVE;
                                                } ?></td>

                                            <td><?php echo anchor('admin/pages/edit_pages/' . $row->pages_id . '/', EDIT); ?>
                                                / <a href="#"
                                                     onclick="delete_rec('<?php echo $row->pages_id; ?>')"><?php echo DELETE; ?></a>
                                            </td>

                                        </tr>
                                        <?php

                                        $i++;

                                    }

                                }

                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE widget-->
                </div>
            </div>

            <!-- END ADVANCED TABLE widget-->

            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>
<!-- BEGIN JAVASCRIPTS -->
<!-- Load javascripts at bottom, this will reduce page load time -->

<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/DT_bootstrap.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        App.init();
    });

    jQuery(document).ready(function () {


        $('#sample_1').dataTable({
            "bDestroy": true,

            "oLanguage": {
                "sLengthMenu": " _MENU_ <?php echo RECORD_PER_PAGE; ?>",
                "sZeroRecords": "<?php echo NOTHING_FOUND_SORRY; ?>",
                "sInfo": "<?php echo SHOWING; ?> _START_ to _END_ of _TOTAL_ <?php echo RECORD; ?>",
                "sInfoEmpty": "<?php echo SHOWING; ?> 0 to 0 of 0 <?php echo RECORD; ?>",
                "sSearch": "<?php echo SEARCH; ?>: ",
                'oPaginate': {

                    'sPrevious': '<?php echo PREVIOUS; ?>',
                    'sNext': '<?php echo NEXT; ?>'
                }
            }

        });


    });
</script>
