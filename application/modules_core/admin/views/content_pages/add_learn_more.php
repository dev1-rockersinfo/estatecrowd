<div id="container" class="row-fluid">
<?php echo $this->load->view('admin_sidebar'); ?>
<!-- END SIDEBAR -->
<!-- BEGIN PAGE -->
<div id="main-content">
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN THEME CUSTOMIZER-->
        <div id="theme-change" class="hidden-phone">
            <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text"><?php echo THEME; ?></span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
        </div>
        <!-- END THEME CUSTOMIZER-->
        <h3 class="page-title">
            <?php echo LEARN_MORE; ?>
        </h3>
        <ul class="breadcrumb">
            <li>
                <a href="<?php echo site_url('admin/home/dashboard') ?>"><i class="icon-home"></i></a><span
                    class="divider">&nbsp;</span>
            </li>

            <li><a href="<?php echo site_url('admin/learn_more/list_pages'); ?>"> <?php echo LEARN_MORE; ?></a><span
                    class="divider-last">&nbsp;</span></li>
        </ul>
    </div>
</div>
<!-- END PAGE HEADER-->
<?php
if ($error != "") {
    ?>
    <div class="alert alert-error">
        <button class="close" data-dismiss="alert">x</button>
        <strong><?php echo ERRORS ?>!</strong> <?php echo $error; ?></div>
<?php
}
?>
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
<div class="span12 sortable">
<!-- BEGIN SAMPLE FORMPORTLET-->
<div class="widget">
    <div class="widget-title">
        <h4><i class="icon-reorder"></i><?php echo LEARN_MORE; ?></h4>
                                        <span class="tools">
                                        <a href="javascript:;" class="icon-chevron-down"></a>
                                        <a href="javascript:;" class="icon-remove"></a>
                                        </span>
    </div>

    <div class="widget-body">

        <!-- BEGIN FORM-->
        <?php
        $attributes = array('name' => 'frm_pages', 'class' => 'form-horizontal');
        echo form_open_multipart('admin/learn_more/add_pages', $attributes);
        ?>

        <div class="control-group">

            <label class="control-label"><?php echo LEARN_CATEGORY; ?></label>

            <div class="controls">
                <?php if ($learn_category_list > 0) { ?>
                    <select name="learn_category" class="input-large m-wrap" tabindex="1" id="learn_category">
                        <?php foreach ($learn_category_list as $category) {
                            ?>
                            <option
                                value="<?php echo $category['category_id'] ?>" <?php if ($learn_category == $category['category_id']) { ?> selected<?php } ?>><?php echo $category['category_name']; ?></option>
                        <?php } ?>
                        <!--<option value="Features" <?php if ($learn_category == "Features") { ?> selected<?php } ?>>Features</option>
                                    <option value="Pricing" <?php if ($learn_category == "Pricing") { ?> selected<?php } ?>>Pricing</option> -->
                        <?php      /*          <option value="Get Going" <?php if($learn_category=="Get Going"){?> selected<?php } ?>>Get Going</option>
                                        <option value="why_nds" <?php if($learn_category=="why_nds"){?> selected<?php } ?>>Why <?php echo $site_setting['site_name'].' ?'; ?></option>
                                          <option value="how_works" <?php if($learn_category=="how_works"){?> selected<?php } ?>>How <?php echo $site_setting['site_name']; ?> Works?</option>
                                          <option value="guide_success" <?php if($learn_category=="guide_success"){?> selected<?php } ?>>Guide To a Successful Campaign</option>
                                    */
                        ?>
                    </select>
                <?php } ?>


            </div>
        </div>

        <div class="control-group">
            <label class="control-label"><?php echo PAGES_TITLE; ?></label>

            <div class="controls">
                <input type="text" placeholder="<?php echo PAGES_TITLE; ?>" class="input-xlarge"
                       name="pages_title" id="pages_title" value="<?php echo $pages_title; ?>"
                       onFocus="show_bg('pages_title')" onBlur="hide_bg('pages_title')"/>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label"><?php echo SUB_TITLE; ?></label>

            <div class="controls">
                <input type="text" name="sub_title" placeholder="<?php echo SUB_TITLE; ?>" class="input-xlarge"
                       id="sub_title" value="<?php echo $sub_title; ?>" onFocus="show_bg('sub_title')"
                       onBlur="hide_bg('sub_title')"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label"><?php echo ICON; ?></label>

            <div class="controls">
                <div class="fileupload fileupload-new" data-provides="fileupload">
                    <div class="input-append">
                        <div class="uneditable-input">
                            <i class="icon-file fileupload-exists"></i>
                            <span class="fileupload-preview"><?php echo $prev_icon; ?></span>
                        </div>
                                       <span class="btn btn-file">
                                       <span class="fileupload-new"><?php echo SELECT_FILE; ?></span>
                                       <span class="fileupload-exists"><?php echo CHANGE; ?></span>
                                       <input type="file" class="default" name="file_up" id="file_up">
                                       </span>
                        <a href="#" class="btn fileupload-exists"
                           data-dismiss="fileupload"><?php echo REMOVE; ?></a>
                    </div>
                </div>
            </div>
        </div>

        <input type="hidden" name="prev_icon" id="prev_icon" value="<?php echo $prev_icon; ?>"/>

        <?php if ($pages_id != '') { ?>

            <!--<div class="fleft">
									  <label>Pages URL<span>&nbsp;</span></label>
									
									<?php //echo anchor($base_url.'home/content/'.strtolower(str_replace(' ','-',$pages_title)).'/'.$pages_id,$base_url.'home/content/'.strtolower(str_replace(' ','-',$pages_title)).'/'.$pages_id,' target="_balnk" '); ?>
									  
									</div>
									<div style="clear:both;"></div>-->

        <?php } ?>
        <div class="control-group">
            <label class="control-label"><?php echo DESCRIPTION; ?> </label>

            <div class="controls">
                <textarea id="description" class="span12" name="description" rows="6">
                    <?php echo $description; ?>
                </textarea>

            </div>
        </div>


        <div class="control-group">
            <label class="control-label"><?php echo SLUG; ?> </label>

            <div class="controls">
                <input type="text" name="slug" placeholder="Slug" class="input-xlarge" id="slug"
                       value="<?php echo $slug; ?>" onFocus="show_bg('slug')" onBlur="hide_bg('slug')"/>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label"><?php echo META_KEYWORD; ?> </label>

            <div class="controls">
                <input type="text" name="meta_keyword" iplaceholder="Meta Keyword" class="input-xlarge"
                       d="meta_keyword" value="<?php echo $meta_keyword; ?>" onFocus="show_bg('meta_keyword')"
                       onBlur="hide_bg('meta_keyword')"/>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label"><?php echo META_DESCRIPTION; ?></label>

            <div class="controls">
                <input type="text" name="meta_description" placeholder="Meta Description " class="input-xlarge"
                       id="meta_description" value="<?php echo $meta_description; ?>"
                       onFocus="show_bg('meta_description')" onBlur="hide_bg('meta_description')"/>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label"><?php echo LANGUAGE; ?></label>
                                     <span onmouseover="show_bg('active')" onmouseout="hide_bg('active')">
                                    <div class="controls">
                                        <select name="language" id="language" class="input-large m-wrap" tabindex="1">
                                            <?php

                                            foreach ($query as $row1) {
                                                $select = '';
                                                if ($language == $row1['language_id']) {
                                                    $select = 'selected="selected"';
                                                }
                                                ?>

                                                <option
                                                    value="<?php echo $row1['language_id']; ?>" <?php echo $select ?>><?php echo $row1['language_name']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
        </div>
        <div class="control-group">
            <label class="control-label"><?php echo STATUS; ?> </label>

            <div class="controls">
                <select name="active" id="active" class="input-large m-wrap" tabindex="1">
                    <option value="0" <?php if ($active == '0') {
                        echo "selected";
                    } ?>><?php echo INACTIVE; ?></option>
                    <option value="1" <?php if ($active == '1') {
                        echo "selected";
                    } ?>><?php echo ACTIVE; ?></option>
                </select>
            </div>
        </div>

        <input type="hidden" name="pages_id" id="pages_id" value="<?php echo $pages_id; ?>"/>


        <div class="form-actions">
            <?php
            if ($pages_id == "") {
                ?>

                <button type="submit" class="btn blue" name="submit" value="Submit" onClick=""/><i
                    class="icon-ok"></i> <?php echo SUBMIT; ?></button>
            <?php
            } else {
                ?>
                <button type="submit" class="btn blue" name="submit" value="Update" onClick=""/><i
                    class="icon-ok"></i> <?php echo UPDATE; ?></button>
            <?php
            }
            ?>

            <button type="button" class="btn" name="cancel" value="Cancel"
                    onClick="location.href='<?php echo site_url('admin/learn_more/list_pages'); ?>'"/>
            <i class=" icon-remove"></i> <?php echo CANCEL; ?></button>
        </div>
        </form>
        <!-- END FORM-->
    </div>
</div>
<!-- END SAMPLE FORM PORTLET-->
</div>
</div>

<!-- END PAGE CONTENT-->
</div>
<!-- END PAGE CONTAINER-->
</div>
<!-- END PAGE -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->

<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS -->

<link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet">
<script type="text/javascript" src="<?php echo base_url(); ?>ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap-fileupload.js"></script>

<script type="text/javascript"
        src="<?php echo base_url(); ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        App.init();
    });
</script>
<script>
    CKEDITOR.replace('description', {
        filebrowserBrowseUrl: '<?php echo base_url();?>ckeditor/filemanager/browser/default/browser.html?Connector=<?php echo base_url(); ?>ckeditor/filemanager/connectors/php/connector.php',
        filebrowserImageBrowseUrl: '<?php echo base_url(); ?>ckeditor/filemanager/browser/default/browser.html?Type=Image&Connector=<?php echo base_url(); ?>ckeditor/filemanager/connectors/php/connector.php',
        filebrowserFlashBrowseUrl: '<?php echo base_url(); ?>ckeditor/filemanager/browser/default/browser.html?Type=Flash&Connector=<?php echo base_url(); ?>ckeditor/filemanager/connectors/php/connector.php',
        filebrowserUploadUrl: '<?php echo base_url(); ?>ckeditor/filemanager/connectors/php/upload.php?Type=File',
        filebrowserImageUploadUrl: '<?php echo base_url(); ?>ckeditor/filemanager/connectors/php/upload.php?Type=Image',
        filebrowserFlashUploadUrl: '<?php echo base_url();?>ckeditor/filemanager/connectors/php/upload.php?Type=Flash'
    });


</script>
