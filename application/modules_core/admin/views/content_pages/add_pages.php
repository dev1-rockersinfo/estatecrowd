<div id="container" class="row-fluid">
<!-- BEGIN SIDEBAR -->
<?php echo $this->load->view('admin_sidebar'); ?>
<!-- END SIDEBAR -->
<!-- BEGIN PAGE -->
<div id="main-content">
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN THEME CUSTOMIZER-->
        <div id="theme-change" class="hidden-phone">
            <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text"><?php echo THEME; ?></span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
        </div>
        <!-- END THEME CUSTOMIZER-->
        <h3 class="page-title">
            <?php echo PAGES; ?>
        </h3>
        <ul class="breadcrumb">
            <li>
                <a href="<?php echo site_url('admin/home/dashboard') ?>"><i class="icon-home"></i></a><span
                    class="divider">&nbsp;</span>
            </li>

            <li><?php echo anchor('admin/pages/list_pages', 'Pages', 'id="sp_1" style="color:#364852;background:#ececec;"'); ?>
                <span class="divider-last">&nbsp;</span></li>


        </ul>
    </div>
</div>
<!-- END PAGE HEADER-->
<?php
if ($error != "") {
    ?>
    <div class="alert alert-error">
        <button class="close" data-dismiss="alert">x</button>
        <strong><?php echo ERRORS; ?>!</strong> <?php echo $error; ?></div>
<?php
}
?>
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <div class="span12 sortable">
        <!-- BEGIN SAMPLE FORMPORTLET-->
        <div class="widget">
            <div class="widget-title">
                <h4><i class="icon-reorder"></i><?php echo ADD_PAGES; ?></h4>
                                        <span class="tools">
                                        <a href="javascript:;" class="icon-chevron-down"></a>
                                        <a href="javascript:;" class="icon-remove"></a>
                                        </span>
            </div>
            <div class="widget-body">
                <!-- BEGIN FORM-->
                <?php $CI =& get_instance();
                $base_url = $CI->config->slash_item('base_url_site');
                $base_path = $CI->config->slash_item('base_path');
                ?>
                <?php
                $attributes = array('name' => 'frm_pages', 'class' => 'form-horizontal');
                echo form_open_multipart('admin/pages/add_pages/'.$pages_id, $attributes);
                ?>


                <div class="control-group">
                    <label class="control-label"><?php echo PAGES_TITLE; ?></label>

                    <div class="controls">
                                 
									  <input type="text" name="pages_title" placeholder="<?php echo PAGES_TITLE; ?>"
                                             class="input-xlarge" id="pages_title" value="<?php echo $pages_title; ?>"
                                             />
									 

                    </div>
                </div>



                            <div class="control-group">
                                <label class="control-label"><?php echo 'Header Title'; ?></label>

                                <div class="controls">
                                    <input type="text" placeholder="<?php echo 'Header Title'; ?>" name="header_title"
                                           value="<?php echo $header_title; ?>" class="input-xlarge"/>

                                 
                                </div>
                            </div>
                    
                            <div class="control-group">
                                <label class="control-label"><?php echo BRIEF; ?></label>

                                <div class="controls">
                                    <input type="text" placeholder="<?php echo BRIEF; ?>" name="header_content"
                                           value='<?php echo $header_content; ?>' class="input-xlarge"/>


                                </div>
                            </div>


                            <div class="control-group">
                                <label class="control-label"><?php echo 'Header Image'; ?></label>

                                <div class="controls">
                                    <input type="file" name="image_load"/>
                                    <input type="hidden" name="dynamic_image_image" value="<?php echo $image_load; ?>"/>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label"><?php echo BUTTON_TITLE; ?></label>

                                <div class="controls">

                                    <input type="text" placeholder="<?php echo BUTTON_TITLE; ?>" name="link_name"
                                           value="<?php echo $link_name; ?>" class="input-xlarge"/>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label"><?php echo BUTTON_LINK; ?></label>

                                <div class="controls">
                                    <input type="text" placeholder="<?php echo BUTTON_LINK; ?>" name="link"
                                           value="<?php echo $link; ?>" class="input-xlarge"/>

                                </div>
                            </div>


             
                <div class="control-group">
                    <label class="control-label"><?php echo DESCRIPTION; ?> </label>

                    <div class="controls">
                        <textarea class="span12" id="description" name="description"
                                  rows="6"><?php echo $description; ?></textarea>
                    </div>
                </div>
                <input type="hidden" name="external_link" id="external_link" value="<?php echo $external_link; ?>"/>
                <input type="hidden" name="external_link" id="external_link" value="<?php echo $external_link; ?>"/>

                <div class="control-group">
                    <label class="control-label"><?php echo DISPLAY_PLACE; ?>  </label>
                                   
                                    <div class="controls">
                                        <input type="checkbox" name="footer_bar" id="footer_bar"
                                               value="yes" <?php if ($footer_bar == 'yes') { ?> checked="checked" <?php } ?>  />

                                        <span class="help-inline"><?php echo FOOTER; ?></span>
                                    </div>
                                    <div class="controls">
                                        <input type="checkbox" name="right_side" id="right_side"
                                               value="yes" <?php if ($right_side == 'yes') { ?> checked="checked" <?php } ?>  />

                                        <span class="help-inline"><?php echo SIDEBAR; ?></span>
                                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label"><?php echo SLUG; ?> </label>

                    <div class="controls">
                                         
									  <input type="text" placeholder="<?php echo SLUG; ?>" class="input-xlarge"
                                             name="slug" id="slug" value="<?php echo $slug; ?>"
                                            />
									  

                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label"><?php echo META_KEYWORD; ?> </label>

                    <div class="controls">
                                    
									  <input type="text" placeholder="<?php echo META_KEYWORD; ?>" class="input-xlarge"
                                             name="meta_keyword" id="meta_keyword" value="<?php echo $meta_keyword; ?>"
                                            />
									 

                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label"><?php echo META_DESCRIPTION; ?>  </label>

                    <div class="controls">
                                   
									  <input type="text" placeholder="<?php echo META_DESCRIPTION; ?> "
                                             class="input-xlarge" name="meta_description" id="meta_description"
                                             value="<?php echo $meta_description; ?>"
                                           />
									

                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label"><?php echo LANGUAGE; ?></label>
                                    
                                    <div class="controls">
                                        <select name="language" id="language" class="input-large m-wrap" tabindex="1">
                                            <?php

                                            foreach ($query as $row1) {
                                                $select = '';
                                                if ($language == $row1['language_id']) {
                                                    $select = 'selected="selected"';
                                                }
                                                ?>

                                                <option
                                                    value="<?php echo $row1['language_id']; ?>" <?php echo $select ?>><?php echo $row1['language_name']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                </div>


                <div class="control-group">
                    <label class="control-label"><?php echo STATUS; ?></label>
                                    
                                    <div class="controls">
                                        <select name="active" id="active" class="input-large m-wrap" tabindex="1">
                                            <option value="0" <?php if ($active == '0') {
                                                echo "selected";
                                            } ?>><?php echo INACTIVE; ?></option>
                                            <option value="1" <?php if ($active == '1') {
                                                echo "selected";
                                            } ?>><?php echo ACTIVE; ?></option>


                                        </select>
                                    </div>
                </div>
                <div class="control-group">
                    <label>&nbsp;<span>&nbsp;</span></label>
									  
									  <input type="hidden" name="pages_id" id="pages_id"
                                             value="<?php echo $pages_id; ?>"/>
								
                </div>


                <div class="form-actions">
                    <?php
                    if ($pages_id == "") {
                        ?>
                        <button type="submit" class="btn blue" name="submit" value="Submit" onclick=""><i
                                class="icon-ok"></i> <?php echo SUBMIT; ?> </button>
                    <?php
                    } else {
                        ?>

                        <button type="submit" class="btn" name="submit" value="Update" onclick=""><i
                                class="icon-ok"></i> <?php echo UPDATE; ?></button>
                    <?php
                    }
                    ?>
                    <button type="button" class="btn" name="cancel" value="Cancel"
                            onClick="location.href='<?php echo site_url('admin/pages/list_pages'); ?>'"><i
                            class=" icon-remove"> </i> <?php echo CANCEL; ?></button>
                </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->
    </div>
</div>

<!-- END PAGE CONTENT-->
</div>
<!-- END PAGE CONTAINER-->
</div>
<!-- END PAGE -->
</div>
<!-- END CONTAINER -->

<!-- BEGIN JAVASCRIPTS -->
<script type="text/javascript" src="<?php echo base_url(); ?>ckeditor/ckeditor.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap-fileupload.js"></script>


<!-- Load javascripts at bottom, this will reduce page load time -->

<script type="text/javascript"
        src="<?php echo base_url(); ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        App.init();
    });
</script>
<script>
    CKEDITOR.replace('description', {
        allowedContent: true,

        filebrowserBrowseUrl: '<?php echo base_url();?>ckeditor/filemanager/browser/default/browser.html?Connector=<?php echo base_url(); ?>ckeditor/filemanager/connectors/php/connector.php',
        filebrowserImageBrowseUrl: '<?php echo base_url(); ?>ckeditor/filemanager/browser/default/browser.html?Type=Image&Connector=<?php echo base_url(); ?>ckeditor/filemanager/connectors/php/connector.php',
        filebrowserFlashBrowseUrl: '<?php echo base_url(); ?>ckeditor/filemanager/browser/default/browser.html?Type=Flash&Connector=<?php echo base_url(); ?>ckeditor/filemanager/connectors/php/connector.php',
        filebrowserUploadUrl: '<?php echo base_url(); ?>ckeditor/filemanager/connectors/php/upload.php?Type=File',
        filebrowserImageUploadUrl: '<?php echo base_url(); ?>ckeditor/filemanager/connectors/php/upload.php?Type=Image',
        filebrowserFlashUploadUrl: '<?php echo base_url();?>ckeditor/filemanager/connectors/php/upload.php?Type=Flash'
    });


</script>
