<script type="text/javascript" language="javascript">

    function setaction(elename, actionval, actionmsg, formname) {
        vchkcnt = 0;

        elem = document.getElementsByName(elename);

        for (i = 0; i < elem.length; i++) {
            if (elem[i].checked) vchkcnt++;
        }
        if (vchkcnt == 0) {
            alert("<?php echo DELETE_SELECT_RECORD_CONFIRMATION;?>")
        } else {
            if (confirm(actionmsg)) {
                document.getElementById('action').value = actionval;
                document.getElementById(formname).submit();
            }
        }
    }
</script>

<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->

    <!-- END RESPONSIVE QUICK SEARCH FORM -->
    <!-- BEGIN SIDEBAR MENU -->
    <?php echo $this->load->view('admin_sidebar'); ?>
    <!-- END SIDEBAR MENU -->

    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">


        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN THEME CUSTOMIZER-->
                    <div id="theme-change" class="hidden-phone">
                        <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text"><?php echo THEME; ?></span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
                    </div>
                    <!-- END THEME CUSTOMIZER-->
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        <?php echo FAQ; ?>
                    </h3>
                    <ul class="breadcrumb">
                        <li>
                            <a href="<?php echo site_url('admin/home/dashboard') ?>"><i class="icon-home"></i></a><span
                                class="divider">&nbsp;</span>
                        </li>

                        <li><a href="#"><?php echo FAQ; ?></a><span class="divider-last">&nbsp;</span></li>
                    </ul>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <?php if ($msg != '') {
                if ($msg == 'delete') {
                    ?>
                    <div class="alert alert-success">
                        <button class="close" data-dismiss="alert">x</button>
                        <strong><?php echo SUCCESS; ?>!</strong> <?php echo RECORDS_HAS_BEEN_DELETED_SUCCESSFULLY; ?>
                    </div>
                <?php
                }
                if ($msg == 'insert') {
                    ?>
                    <div class="alert alert-success">
                        <button class="close" data-dismiss="alert">x</button>
                        <strong><?php echo SUCCESS; ?>!</strong> <?php echo NEW_RECORD_HAS_BEEN_ADDED_SUCCESSFULLY; ?>.
                    </div>
                <?php
                }
                if ($msg == 'update') {
                    ?>
                    <div class="alert alert-success">
                        <button class="close" data-dismiss="alert">x</button>
                        <strong><?php echo SUCCESS; ?>!</strong> <?php echo RECORD_HAS_BEEN_UPDATED_SUCCESSFULLY; ?>
                    </div>
                <?php
                }
                if ($msg == 'active') {
                    ?>
                    <div class="alert alert-success">
                        <button class="close" data-dismiss="alert">x</button>
                        <strong><?php echo SUCCESS; ?>!</strong> <?php echo RECORD_HAS_BEEN_ACTIVATED_SUCCESSFULLY; ?>
                    </div>
                <?php
                }
                if ($msg == 'inactive') {
                    ?>
                    <div class="alert alert-success">
                        <button class="close" data-dismiss="alert">x</button>
                        <strong><?php echo SUCCESS; ?>!</strong> <?php echo RECORD_HAS_BEEN_INACTIVATED_SUCCESSFULLY; ?>
                    </div>
                <?php
                }
                if ($msg == 'help_page') {
                    ?>
                    <div class="alert alert-success">
                        <button class="close" data-dismiss="alert">x</button>
                        <strong><?php echo SUCCESS; ?>
                            !</strong> <?php echo RECORD_HAS_BEEN_SHOW_ON_HELP_PAGE_SUCCESSFULLY; ?></div>
                <?php
                }
                if ($msg == 'not_help_page') {
                    ?>
                    <div class="alert alert-success">
                        <button class="close" data-dismiss="alert">x</button>
                        <strong><?php echo SUCCESS; ?>
                            !</strong> <?php echo RECORD_HAS_BEEN_REMOVE_ON_HELP_PAGE_SUCCESSFULLY; ?></div>
                <?php
                }
            } ?>



            <!-- BEGIN ADVANCED TABLE widget-->
            <div class="fr iconM">
                <button type="button" class="btn mini purple"
                        onclick="location.href='<?php echo site_url('admin/pages/add_faq'); ?>'"><i
                        class="icon-plus"></i> <?php echo ADD; ?></button>
                <button type="button" class="btn btn-danger"
                        onclick="setaction('chk[]','delete', '<?php echo ARE_YOU_SURE_YOU_WANT_TO_DELETE_SELECTED_RECORD; ?>', 'frm_listproject')">
                    <i class="icon-remove-sign icon-white"></i> <?php echo DELETE; ?></button>
                <button type="button" class="btn btn-success"
                        onclick="setaction('chk[]','active', '<?php echo ARE_YOU_SURE_YOU_WANT_TO_ACTIVE_SELECTED_RECORD; ?>', 'frm_listproject')">
                    <i class="icon-ok icon-white"></i> <?php echo ACTIVE; ?></button>
                <button type="button" class="btn btn-warning"
                        onclick="setaction('chk[]','inactive', '<?php echo ARE_YOU_SURE_YOU_WANT_TO_INACTIVE_SELECTED_RECORD; ?>', 'frm_listproject')">
                    <i class="icon-remove-sign icon-white"></i> <?php echo INACTIVE; ?></button>
            </div>
            <div class="row-fluid">


                <div class="span12">
                    <!-- BEGIN EXAMPLE TABLE widget-->
                    <div class="widget">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i><?php echo FAQ; ?></h4>
                            <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                            </span>
                        </div>

                        <div class="widget-body">
                            <?php
                            $attributes = array('name' => 'frm_listproject', 'id' => 'frm_listproject');
                            echo form_open('admin/pages/action_faq', $attributes);
                            ?>
                            <input type="hidden" name="action" id="action"/>

                            <table class="table table-striped table-bordered" id="sample_1">
                                <thead>
                                <tr>
                                    <th style="width:8px;"><input type="checkbox" class="group-checkable"
                                                                  data-set="#sample_1 .checkboxes"/></th>
                                    <th><?php echo QUESTION; ?></th>
                                    <th><?php echo ORDER; ?></th>
                                    <th><?php echo ACTIVE; ?></th>
                                    <th><?php echo ACTIONS; ?></th>

                                </tr>
                                </thead>

                                <tbody>
                                <?php
                                if ($result) {
                                    $i = 0;
                                    foreach ($result as $row) {

                                        ?>
                                        <tr class="odd gradeX">

                                            <td><input type="checkbox" name="chk[]" class="checkboxes" id="chk"
                                                       value="<?php echo $row->faq_id; ?>"/></td>
                                            <td><?php echo $row->question; ?></td>
                                            <td><?php echo $row->faq_order; ?></td>
                                            <td><?php if ($row->active == "1") {
                                                    echo ACTIVE;
                                                } else {
                                                    echo INACTIVE;
                                                } ?></td>
                                            <td><?php echo anchor('admin/pages/edit_faq/' . $row->faq_id . '/', EDIT); ?></td>

                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                            </form>
                        </div>

                    </div>
                    <!-- END EXAMPLE TABLE widget-->
                </div>
            </div>

            <!-- END ADVANCED TABLE widget-->

            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->

<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS -->
<!-- Load javascripts at bottom, this will reduce page load time -->

<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/DT_bootstrap.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        App.init();
    });

    jQuery(document).ready(function () {


        $('#sample_1').dataTable({
            "bDestroy": true,
            "aoColumnDefs": [{"bSortable": false, "aTargets": [0]}],
            "oLanguage": {
                "sLengthMenu": " _MENU_ <?php echo RECORD_PER_PAGE; ?>",
                "sZeroRecords": "<?php echo NOTHING_FOUND_SORRY; ?>",
                "sInfo": "<?php echo SHOWING; ?> _START_ to _END_ of _TOTAL_ <?php echo RECORD; ?>",
                "sInfoEmpty": "<?php echo SHOWING; ?> 0 to 0 of 0 <?php echo RECORD; ?>",
                "sSearch": "<?php echo SEARCH; ?>: ",
                'oPaginate': {

                    'sPrevious': '<?php echo PREVIOUS; ?>',
                    'sNext': '<?php echo NEXT; ?>'
                }
            }

        });


    });
</script>
