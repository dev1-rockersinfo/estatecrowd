<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php echo $this->load->view('admin_sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN THEME CUSTOMIZER-->
                    <div id="theme-change" class="hidden-phone">
                        <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text"><?php echo THEME; ?></span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
                    </div>
                    <!-- END THEME CUSTOMIZER-->
                    <h3 class="page-title">
                        <?php echo PAGES; ?>
                    </h3>
                    <ul class="breadcrumb">
                        <li>
                            <a href="<?php echo site_url('admin/home/dashboard') ?>"><i class="icon-home"></i></a><span
                                class="divider">&nbsp;</span>
                        </li>

                        <li><?php echo anchor('admin/learn_more/list_learn_category', 'Learn More Category'); ?><span
                                class="divider-last">&nbsp;</span></li>


                    </ul>
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <?php
            if ($error != "") {
                ?>
                <div class="alert alert-error">
                    <button class="close" data-dismiss="alert">x</button>
                    <strong><?php echo ERRORS; ?>!</strong> <?php echo $error; ?></div>
            <?php
            }
            ?>
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                <div class="span12 sortable">
                    <!-- BEGIN SAMPLE FORMPORTLET-->
                    <div class="widget">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i><?php echo ADD_PAGES; ?></h4>
                                        <span class="tools">
                                        <a href="javascript:;" class="icon-chevron-down"></a>
                                        <a href="javascript:;" class="icon-remove"></a>
                                        </span>
                        </div>
                        <div class="widget-body">
                            <!-- BEGIN FORM-->

                            <?php
                            $attributes = array('name' => 'frm_pages', 'class' => 'form-horizontal');
                            echo form_open_multipart('admin/learn_more/add_category', $attributes);
                            ?>

                            <div class="control-group">
                                <label class="control-label"><?php echo CATEGORY_NAME; ?></label>

                                <div class="controls">
                                    <input type="text" name="category_name" placeholder="Category Name"
                                           class="input-xlarge" id="category_name" value="<?php echo $pages_title; ?>"
                                           onFocus="show_bg('category_name')" onBlur="hide_bg('category_name')"/>
                                </div>
                            </div>


                            <div class="control-group">
                                <label class="control-label"><?php echo DISPLAY_PLACE; ?>  </label>

                                <div class="controls">
                                    <input type="checkbox" name="footer_bar" id="footer_bar"
                                           value="yes" <?php if ($footer_bar == 'yes') { ?> checked="checked" <?php } ?>  />

                                    <span class="help-inline"><?php echo FOOTER; ?></span>
                                </div>
                                <div class="controls">
                                    <input type="checkbox" name="side_bar" id="side_bar"
                                           value="yes" <?php if ($right_side == 'yes') { ?> checked="checked" <?php } ?>  />

                                    <span class="help-inline"><?php echo PAGES_SIDEBAR; ?></span>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label"><?php echo LANGUAGE; ?></label>
                                     <span onmouseover="show_bg('active')" onmouseout="hide_bg('active')">
                                    <div class="controls">
                                        <select name="language" id="language" class="input-large m-wrap" tabindex="1">
                                            <?php

                                            foreach ($query as $row1) {
                                                $select = '';
                                                if ($language == $row1['language_id']) {
                                                    $select = 'selected="selected"';
                                                }
                                                ?>

                                                <option
                                                    value="<?php echo $row1['language_id']; ?>" <?php echo $select ?>><?php echo $row1['language_name']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label"><?php echo STATUS; ?></label>

                                <div class="controls">
                                    <select name="active" id="active" class="input-large m-wrap" tabindex="1">
                                        <option value="0" <?php if ($active == '0') {
                                            echo "selected";
                                        } ?>>Inactive
                                        </option>
                                        <option value="1" <?php if ($active == '1') {
                                            echo "selected";
                                        } ?>>Active
                                        </option>


                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label>&nbsp;<span>&nbsp;</span></label>
                                <input type="hidden" name="category_id" id="category_id"
                                       value="<?php echo $pages_id; ?>"/>
                            </div>


                            <div class="form-actions">
                                <?php
                                if ($pages_id == "") {
                                    ?>
                                    <button type="submit" class="btn blue" name="submit" value="Submit" onclick=""><i
                                            class="icon-ok"></i><?php echo SUBMIT; ?> </button>
                                <?php
                                } else {
                                    ?>

                                    <button type="submit" class="btn blue" name="submit" value="Update" onclick=""><i
                                            class="icon-ok"></i><?php echo UPDATE; ?></button>
                                <?php
                                }
                                ?>

                                <button type="button" class="btn" name="cancel" value="Cancel"
                                        onClick="location.href='<?php echo site_url('admin/learn_more/list_learn_category'); ?>'"/>
                                <i class=" icon-remove"> </i> <?php echo CANCEL; ?></button>
                            </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>

            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>
<!-- END CONTAINER -->

<!-- BEGIN JAVASCRIPTS -->


<!-- Load javascripts at bottom, this will reduce page load time -->

<script type="text/javascript"
        src="<?php echo base_url(); ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        App.init();
    });
</script>
