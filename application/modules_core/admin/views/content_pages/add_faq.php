<div id="container" class="row-fluid">

<!-- BEGIN SIDEBAR -->

<?php echo $this->load->view('admin_sidebar'); ?>

<!-- END SIDEBAR -->

<!-- BEGIN PAGE -->

<div id="main-content">

<!-- BEGIN PAGE CONTAINER-->

<div class="container-fluid">

<!-- BEGIN PAGE HEADER-->

<div class="row-fluid">

    <div class="span12">

        <!-- BEGIN THEME CUSTOMIZER-->

        <div id="theme-change" class="hidden-phone">

            <i class="icon-cogs"></i>

                        <span class="settings">

                            <span class="text"><?php echo THEME; ?></span>

                            <span class="colors">

                                <span class="color-default" data-style="default"></span>

                                <span class="color-gray" data-style="gray"></span>

                                <span class="color-purple" data-style="purple"></span>

                                <span class="color-navy-blue" data-style="navy-blue"></span>

                            </span>

                        </span>

        </div>

        <!-- END THEME CUSTOMIZER-->

        <h3 class="page-title">

            <?php echo FAQ; ?>

        </h3>

        <ul class="breadcrumb">

            <li>

                <a href="<?php echo site_url('admin/home/dashboard'); ?>"><i class="icon-home"></i></a><span
                    class="divider">&nbsp;</span>

            </li>


            <li><a href=<?php echo site_url('admin/pages/list_faq'); ?>><?php echo FAQ; ?></a><span
                    class="divider-last">&nbsp;</span></li>

        </ul>

    </div>

</div>

<!-- END PAGE HEADER-->

<?php

if ($error != "") {
    ?>

    <div class="alert alert-error">

        <button class="close" data-dismiss="alert">x</button>

        <strong><?php echo ERRORS; ?>!</strong> <?php echo $error; ?></div>

<?php
}

?>

<!-- BEGIN PAGE CONTENT-->

<div class="row-fluid">

    <div class="span12 sortable">

        <!-- BEGIN SAMPLE FORMPORTLET-->

        <div class="widget">

            <div class="widget-title">

                <h4><i class="icon-reorder"></i><?php echo FAQ; ?></h4>

                                        <span class="tools">

                                        <a href="javascript:;" class="icon-chevron-down"></a>

                                        <a href="javascript:;" class="icon-remove"></a>

                                        </span>

            </div>

            <div class="widget-body">

                <!-- BEGIN FORM-->

                <?php



                $attributes = array('name' => 'frm_faq', 'class' => 'form-horizontal');


                echo form_open('admin/pages/add_faq', $attributes);



                ?>



                <input type="hidden" name="faq_category_id" id="faq_category_id"
                       value="<?php echo $faq_category_id; ?>"/>

                <div class="control-group">

                    <label class="control-label"><?php echo ORDER; ?></label>


                    <div class="controls">


                        <input type="text" placeholder="<?php echo ORDER; ?> " class="input-xlarge" name="faq_order"
                               id="faq_order"
                               value="<?php echo $faq_order; ?>"/>

                    </div>

                </div>


                <div class="control-group">

                    <label class="control-label"><?php echo STATUS; ?></label>


                    <div class="controls">


                        <select class="input-large m-wrap" tabindex="1" name="active" id="active">


                            <option value="0" <?php if ($active == '0') {
                                echo "selected";
                            } ?>><?php echo INACTIVE; ?></option>


                            <option value="1" <?php if ($active == '1') {
                                echo "selected";
                            } ?>><?php echo ACTIVE; ?></option>


                        </select>

                    </div>

                </div>

                <div class="control-group">
                    <label class="control-label"><?php echo LANGUAGE; ?></label>

                    <div class="controls">
                        <select name="language" id="language" class="input-large m-wrap" tabindex="1">
                            <?php

                            foreach ($query as $row1) {
                                $select = '';
                                if ($language == $row1['language_id']) {
                                    $select = 'selected="selected"';
                                }
                                ?>

                                <option
                                    value="<?php echo $row1['language_id']; ?>" <?php echo $select ?>><?php echo $row1['language_name']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>


                <div class="control-group">

                    <label class="control-label"><?php echo 'Question Type'; ?> </label>

                    <div class="controls">


                        <input type="text" placeholder="<?php echo 'Question Type'; ?>" class="input-xlarge" 
                        name="question_type"
                               id="question_type"
                               value="<?php echo $question_type; ?>"/>


                    </div>

                </div>

                <div class="control-group">

                    <label class="control-label"><?php echo QUESTION; ?> </label>

                    <div class="controls">


                        <input type="text" placeholder="<?php echo QUESTIONS; ?>" class="input-xlarge" name="question"
                               id="question"
                               value="<?php echo $question; ?>"/>


                    </div>

                </div>


                <div class="control-group">

                    <label class="control-label"><?php echo ANSWER; ?>  </label>

                    <div class="controls">

                        <textarea class="span12" id="answer" name="answer" rows="6">

                            <?php echo $answer; ?>

                        </textarea>

                    </div>


                </div>


                <input type="hidden" name="faq_id" id="faq_id" value="<?php echo $faq_id; ?>"/>


                <div class="form-actions">

                    <?php



                    if ($faq_id == "") {


                        ?>

                        <button type="submit" class="btn blue" name="submit" value="Submit" onClick=""/><i
                            class="icon-ok"></i>  <?php echo SUBMIT; ?></button>

                    <?php


                    } else {


                        ?>

                        <button type="submit" class="btn blue" name="submit" value="Update" onClick=""/><i
                            class="icon-ok"></i> <?php echo UPDATE; ?></button>

                    <?php


                    }



                    ?>

                    <button type="button" class="btn" name="cancel" value="Cancel"
                            onClick="location.href='<?php echo site_url('admin/pages/list_faq'); ?>'"/>
                    <i class=" icon-remove"></i> <?php echo CANCEL; ?></button>


                </div>

                </form>

                <!-- END FORM-->

            </div>

        </div>

        <!-- END SAMPLE FORM PORTLET-->

    </div>

</div>


<!-- END PAGE CONTENT-->

</div>

<!-- END PAGE CONTAINER-->

</div>

<!-- END PAGE -->

</div>

<!-- END CONTAINER -->

<!-- BEGIN FOOTER -->


<!-- END FOOTER -->

<!-- BEGIN JAVASCRIPTS -->

<script type="text/javascript" src="<?php echo base_url(); ?>ckeditor/ckeditor.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap-fileupload.js"></script>

<!-- Load javascripts at bottom, this will reduce page load time -->

<script type="text/javascript"
        src="<?php echo base_url(); ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>

<script src="<?php echo base_url(); ?>js/scripts.js"></script>

<script>

    jQuery(document).ready(function () {

        // initiate layout and plugins

        App.init();

    });

</script>


<script>
    CKEDITOR.replace('answer', {
        filebrowserBrowseUrl: '<?php echo base_url();?>ckeditor/filemanager/browser/default/browser.html?Connector=<?php echo base_url(); ?>ckeditor/filemanager/connectors/php/connector.php',
        filebrowserImageBrowseUrl: '<?php echo base_url(); ?>ckeditor/filemanager/browser/default/browser.html?Type=Image&Connector=<?php echo base_url(); ?>ckeditor/filemanager/connectors/php/connector.php',
        filebrowserFlashBrowseUrl: '<?php echo base_url(); ?>ckeditor/filemanager/browser/default/browser.html?Type=Flash&Connector=<?php echo base_url(); ?>ckeditor/filemanager/connectors/php/connector.php',
        filebrowserUploadUrl: '<?php echo base_url(); ?>ckeditor/filemanager/connectors/php/upload.php?Type=File',
        filebrowserImageUploadUrl: '<?php echo base_url(); ?>ckeditor/filemanager/connectors/php/upload.php?Type=Image',
        filebrowserFlashUploadUrl: '<?php echo base_url();?>ckeditor/filemanager/connectors/php/upload.php?Type=Flash'
    });


</script>


