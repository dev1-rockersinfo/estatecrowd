<?php

$campaign_name = $taxonomy_setting['project_name'];
$funds = $taxonomy_setting['funds'];
$updates = $taxonomy_setting['updates'];
$comments = $taxonomy_setting['comments'];
$taxonomy_project_url = 'properties';
$site_name = $site_setting['site_name'];
?>



<!-- BEGIN CONTAINER -->


<div id="container" class="row-fluid">


    <!-- BEGIN SIDEBAR -->



    <?php echo $this->load->view('admin_sidebar'); ?>



    <!-- END SIDEBAR -->


    <!-- BEGIN PAGE -->


    <div id="main-content">


        <!-- BEGIN PAGE CONTAINER-->


        <div class="container-fluid">


            <!-- BEGIN PAGE HEADER-->


            <div class="row-fluid">


                <div class="span12">


                    <!-- BEGIN THEME CUSTOMIZER-->


                    <div id="theme-change" class="hidden-phone">


                        <i class="icon-cogs"></i>



                        <span class="settings">



                            <span class="text"><?php echo THEME; ?></span>



                            <span class="colors">



                                <span class="color-default" data-style="default"></span>



                                <span class="color-gray" data-style="gray"></span>



                                <span class="color-purple" data-style="purple"></span>



                                <span class="color-navy-blue" data-style="navy-blue"></span>



                            </span>



                        </span>


                    </div>


                    <!-- END THEME CUSTOMIZER-->


                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->


                    <h3 class="page-title">


                        <?php echo 'Postcode List'; ?>


                    </h3>


                    <ul class="breadcrumb">


                        <li>


                            <a href="<?php echo site_url('admin/home/dashboard'); ?>"><i class="icon-home"></i></a><span
                                class="divider">&nbsp;</span>


                        </li>


                        <li><a href="#"><?php echo 'Postcode List'; ?>  </a><span class="divider-last">&nbsp;</span></li>


                    </ul>


                    <!-- END PAGE TITLE & BREADCRUMB-->


                </div>


            </div>


            <!-- END PAGE HEADER-->


            <!-- BEGIN ADVANCED TABLE widget-->

            <?php if($msg){ ?>
            <div class="alert alert-success">
                <button class="close" data-dismiss="alert">x</button>
                <strong><?php echo SUCCESS; ?>!</strong>
                </span> <?php echo $msg; ?>
            </div>
            <?php } ?>

            <div class="fr iconM">

            </div>


            <div class="row-fluid">


                <div class="span12">


                    <!-- BEGIN EXAMPLE TABLE widget-->


                    <div class="widget">


                        <div class="widget-title">


                            <h4><i class="icon-reorder"></i><?php echo 'Postcode List'; ?></h4>



                            <span class="tools">



                                <a href="javascript:;" class="icon-chevron-down"></a>



                            </span>


                        </div>


                        <div class="widget-body">



                            <?php



                            $attributes = array('name' => 'frm_listproject', 'id' => 'frm_listproject', 'method' => 'post');
                            echo form_open('admin/property/action_property/', $attributes);
                            ?>
                            <input type="hidden" name="action" id="action"/>


                            <table class="table table-striped table-bordered table_project" id="sample_1">


                                <thead>


                                <tr>
                                    <th style="width:8px;"><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes"/></th>
                                    <th><?php echo 'Postcode'; ?></th>

                                    <th class="hidden-phone"><?php echo 'Latitude'; ?></th>

                                    <th class="hidden-phone"><?php echo 'Longitude'; ?></th>

                                    <th><?php echo 'Status'; ?></th>
                                </tr>


                                </thead>


                                <tbody>
                                <?php



                                if ($result) {


                                    foreach ($result as $row) {

                                        $postcode = $row['postcode'];

                                        $postcode_id = $row['id'];

                                        $latitude = $row['latitude'];

                                        $longitude = $row['longitude'];

                                        if($row['status']==1){
                                            $status = "Fetched";
                                            $api_call = '<a href="'.site_url('admin/property/add_localities/'.$postcode).'">Fetch again</a>';
                                        }else{
                                            $status = "Not fetched";
                                            $api_call = '<a href="'.site_url('admin/property/add_localities/'.$postcode).'">Fetch Now</a>';
                                        }

                                        ?>


                                        <tr class="odd gradeX">
                                            <td><input type="checkbox" class="checkboxes" name="chk[]" value="<?php echo $postcode_id; ?>"/></td>
                                            <td class="positionR "><?=$postcode?></td>

                                            <td class="hidden-phone"><?=$latitude?></td>

                                            <td class="hidden-phone"><?=$longitude?></td>

                                            <?php/* ?> <td class="hidden-phone"><?=$status.' | '.$api_call?></td><?php */?>
                                            <td class="hidden-phone"><a href="<?=site_url('admin/property/get_properties_by_postcode/'.$postcode)?>">Add properties</a></td>

                                        </tr>

                                        <?php


                                    }


                                }



                                ?>


                                </tbody>


                            </table>


                            </form>


                        </div>


                    </div>


                    <!-- END EXAMPLE TABLE widget-->


                </div>


            </div>


            <!-- END ADVANCED TABLE widget-->


            <!-- END PAGE CONTENT-->


        </div>


        <!-- END PAGE CONTAINER-->


    </div>


    <!-- END PAGE -->


</div>


<!-- END CONTAINER -->


<!-- BEGIN JAVASCRIPTS -->


<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>


<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/jquery.dataTables.js"></script>


<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/DT_bootstrap.js"></script>


<script src="<?php echo base_url(); ?>js/scripts.js"></script>


<script type="text/javascript" charset="utf-8">


    jQuery(document).ready(function () {


        App.init();


    });


</script>