<script type="text/javascript">

    function setaction(elename, actionval, actionmsg, formname) {

        if (actionval == 'declined' || actionval == 'inactive') {
            vchkcnt = 1;
        }
        else {
            vchkcnt = 0;
            elem = document.getElementsByName(elename);
            for (i = 0; i < elem.length; i++) {
                if (elem[i].checked) vchkcnt++;
            }
        }
        if (vchkcnt == 0) {
            alert("<?php echo DELETE_SELECT_RECORD_CONFIRMATION;?>")
        } else {

            if (confirm(actionmsg)) {


                if (actionval == 'declined') {

                    var textarea = $("<textarea>").attr("name", "reason_decline").val($("#reason_decline_" + elename).val());

                    if ($("#save_reason_" + elename).is(':checked')) {
                        var save_the_reason = 1;
                    } else {
                        var save_the_reason = 0;
                    }
                    var input = $("<input>").attr("type", "hidden").attr("name", "save_the_reason").val(save_the_reason);
                    var input_property_id = $("<input>").attr("type", "hidden").attr("name", "action_property_id").val(elename);
                    $('#' + formname).append($(input_property_id));
                    $('#' + formname).append($(textarea));
                    $('#' + formname).append($(input));
                }
                if (actionval == 'inactive') {


                    var inactive_mode = $("input[name=inactive_equity_mode_" + elename + "]:checked").val();
                    var fund_inactive_value = $("input[name=fund_inactive_" + elename + "]:checked").val();


                    var textarea_inactive = $("<textarea>").attr("name", "reason_inactive_hidden").val($("#reason_inactive_" + elename).val());


                    if ($("#save_reason_inactive_" + elename).is(':checked')) {
                        var save_the_reason_inactive = 1;
                    } else {
                        var save_the_reason_inactive = 0;
                    }


                    var input_inactive = $("<input>").attr("type", "hidden").attr("name", "save_the_reason_inactive").val(save_the_reason_inactive);
                    var input_inactive_mode = $("<input>").attr("type", "hidden").attr("name", "inactive_mode").val(inactive_mode);
                    var fund_inactive_note = $("<input>").attr("type", "hidden").attr("name", "fund_inactive_note").val(fund_inactive_value);
                    var input_property_id = $("<input>").attr("type", "hidden").attr("name", "action_property_id").val(elename);
                    $('#' + formname).append($(input_property_id));
                    $('#' + formname).append($(textarea_inactive));
                    $('#' + formname).append($(input_inactive));
                    $('#' + formname).append($(fund_inactive_note));
                    $('#' + formname).append($(input_inactive_mode));
                }
                document.getElementById('action').value = actionval;
                document.getElementById(formname).submit();
            }
        }
    }

    function setoneaction(property_id, actionval, actionmsg, formname) {


        if (confirm(actionmsg)) {
            // document.getElementById('action').value=actionval;
            // document.getElementById(formname).submit();
            window.location = "<?php echo site_url('/admin/property/action_property')?>" + '/' + property_id + '/' + actionval;
        }
    }



</script>

<?php

$campaign_name = $taxonomy_setting['project_name'];
$funds = $taxonomy_setting['funds'];
$updates = $taxonomy_setting['updates'];
$comments = $taxonomy_setting['comments'];
$taxonomy_project_url = 'properties';
$site_name = $site_setting['site_name'];
?>



<!-- BEGIN CONTAINER -->


<div id="container" class="row-fluid">


    <!-- BEGIN SIDEBAR -->



    <?php echo $this->load->view('admin_sidebar'); ?>



    <!-- END SIDEBAR -->


    <!-- BEGIN PAGE -->


    <div id="main-content">


        <!-- BEGIN PAGE CONTAINER-->


        <div class="container-fluid">


            <!-- BEGIN PAGE HEADER-->


            <div class="row-fluid">


                <div class="span12">


                    <!-- BEGIN THEME CUSTOMIZER-->


                    <div id="theme-change" class="hidden-phone">


                        <i class="icon-cogs"></i>



                        <span class="settings">



                            <span class="text"><?php echo THEME; ?></span>



                            <span class="colors">



                                <span class="color-default" data-style="default"></span>



                                <span class="color-gray" data-style="gray"></span>



                                <span class="color-purple" data-style="purple"></span>



                                <span class="color-navy-blue" data-style="navy-blue"></span>



                            </span>



                        </span>


                    </div>


                    <!-- END THEME CUSTOMIZER-->


                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->


                    <h3 class="page-title">


                        <?php echo 'Property List'; ?>


                    </h3>


                    <ul class="breadcrumb">


                        <li>


                            <a href="<?php echo site_url('admin/home/dashboard'); ?>"><i class="icon-home"></i></a><span
                                class="divider">&nbsp;</span>


                        </li>


                        <li><a href="#"><?php echo 'Property List'; ?>  </a><span class="divider-last">&nbsp;</span></li>


                    </ul>


                    <!-- END PAGE TITLE & BREADCRUMB-->


                </div>


            </div>


            <!-- END PAGE HEADER-->


            <!-- BEGIN ADVANCED TABLE widget-->




            <?php

            if ($msg) { ?>
                <div class="alert alert-success">
                                    <button class="close" data-dismiss="alert">x</button>
                    <strong><?php echo SUCCESS; ?>!</strong>
                    <?php echo $msg; ?>.
                </div>
                <?php
            }

            ?>


            <div class="fr iconM">


            </div>


            <div class="row-fluid">


                <div class="span12">


                    <!-- BEGIN EXAMPLE TABLE widget-->


                    <div class="widget">


                        <div class="widget-title">


                            <h4><i class="icon-reorder"></i><?php echo 'Property List'; ?></h4>



                            <span class="tools">



                                <a href="javascript:;" class="icon-chevron-down"></a>



                            </span>


                        </div>


                        <div class="widget-body">



                            <?php



                            $attributes = array('name' => 'frm_listproject', 'id' => 'frm_listproject', 'method' => 'post');
                            echo form_open('admin/property/action_property/', $attributes);
                            ?>
                            <input type="hidden" name="action" id="action"/>


                            <table class="table table-striped table-bordered table_project" id="sample_1">


                                <thead>


                                <tr>
                                    <th style="width:8px;"><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes"/></th>
                                    <th><?php echo 'Property'; ?></th>

                                    <th class="hidden-phone"><?php echo CREATE_BY; ?></th>

                                    <th class="hidden-phone"><?php echo STATUS; ?></th>
                                    <th><?php echo ACTION; ?></th>
                                </tr>


                                </thead>


                                <tbody>
                                <?php



                                if ($result) {


                                    foreach ($result as $row) {


                                        $user_data = UserData($row['user_id']);
                                        $user_detail = $user_data[0];
                                        $profile_slug = $user_detail['profile_slug'];
                                        $creater_image = $user_detail['image'];


                                        $property_id = $row['property_id'];

                                        $property_id_api = $row['property_id_api'];

                                        $property_user_id = $row['user_id'];

                                        $property_address = $row['property_address'];



                                        $goal = $row['goal'];

                                        $property_url = $row['property_url'];

                                        $status = status_name($row['status']);
                                        $status_class = '';

                                        if ($row['status'] == 2 || $row['status'] == 3 || $row['status'] == 4) {
                                            $status_class = 'label-success';
                                        }
                                        if ($row['status'] == 6) {
                                            $status_class = 'label-warning';
                                        }
                                        if ($row['status'] == 7 || $row['status'] == 8 || $row['status'] == 5) {
                                            $status_class = 'label-danger';
                                        }



                                        // $amount_get = $row['amount_get'];

                                        $user_name = $row['user_name'];
                                        $last_name = $row['last_name'];
                                        $email = $row['email'];


                                        $str_name = $property_address;


                                        if ($creater_image != '' && is_file(base_path() . 'upload/user/user_small_image/' . $creater_image)) {
                                            $cimage = $creater_image;

                                        } else {
                                            $cimage = 'no_man.jpg';
                                        }
                                        if (!empty($str_name)) {

                                            $str_name_url = site_url($taxonomy_project_url . '/' . $property_url);

                                        } else {

                                            $str_name_url = site_url($taxonomy_project_url . '/' . $property_url);
                                        }

                                        //all reason task
                                        $save_the_reason = $row['save_the_reason'];
                                        $reason_decline = $row['reason_decline'];
                                        $save_the_reason_inactive = $row['save_the_reason_inactive'];
                                        $reason_inactive_hidden = $row['reason_inactive_hidden'];
                                        if ($save_the_reason != 1) {
                                            $temp = PreviousReasonEquity($property_id, 'decline');
                                            if (isset($temp['reason_decline'])) $reason_decline = $temp['reason_decline'];
                                        }
                                        if ($save_the_reason_inactive != 1) {
                                            $temp = PreviousReasonEquity($property_id, 'inactive');
                                            if (isset($temp['reason_inactive_hidden'])) $reason_inactive_hidden = $temp['reason_inactive_hidden'];
                                        }
                                        ?>


                                        <tr class="odd gradeX">
                                            <td><input type="checkbox" class="checkboxes" name="chk[]" value="<?php echo $property_id; ?>"/></td>
                                            <td class="positionR "><a href="<?php echo $str_name_url; ?>"
                                                                      target="_blank"><?php echo $str_name; ?> </a></td>

                                            <td class="hidden-phone">
                                                <a href="<?php echo site_url('user/' . $profile_slug); ?>" target="_blank" class="tooltips creater"
                                                   data-placement="bottom" data-original-title="<?php echo $user_name . ' ' . $last_name; ?>">
                                                    <img src="<?php echo base_url() ?>upload/user/user_small_image/<?php echo $cimage; ?>"></a></td>

                                            <td class="hidden-phone"><span class="label <?php echo $status_class; ?>"><?php echo $status; ?></span></td>
                                            <td class="actions">


                                                <a href="<?php echo base_url() . 'admin/message/view_message/' . $property_id . '/' . $equity_user_id; ?>"
                                                   class="item btnMessage tooltips" data-placement="bottom" data-original-title="<?php echo SEND_MESSAGE;?>"><i
                                                        class="icon-envelope"></i></a>
                                                <!-- Feature -->

                                                <!-- Delete -->
                                                <?php if ($row['status'] == 1 || $row['status'] == 6) { ?>

                                                    <a href="javascript://" class="item btnDelete tooltips" data-placement="bottom"
                                                       data-original-title="Delete"
                                                       onclick="setoneaction(<?php echo $property_id; ?>,'delete', '<?php echo ARE_YOU_SURE_YOU_WANT_TO_DELETE_SELECTED_PROJECT; ?>', 'frm_listproject')"><i
                                                            class="icon-trash"></i></a>

                                                <?php } else { ?>
                                                    <a href="javascript://" class="item btnDelete tooltips disable" data-placement="bottom"
                                                       data-original-title="<?php echo CANT_DELETE;?>"><i class="icon-trash"></i></a>
                                                <?php } ?>

                                                <!-- Inactive and active-->
                                                <?php if ($row['status'] == 2) { ?>

                                                    <a href="javascript://" role="button" class="item btnApprove tooltips"
                                                       data-placement="bottom" onclick="setoneaction(<?php echo $property_id; ?>,'inactive', '<?php echo ARE_YOU_SURE_YOU_WANT_TO_INACTIVE_SELECTED_RECORD; ?>', 'frm_listproject')" data-original-title="<?php echo INACTIVE_THIS;?>" data-toggle="modal"><i
                                                            class="icon-ban-circle"></i></a>

                                                    <?php
                                                } else if ($row['status'] == 1) {
                                                    ?>
                                                    <a href="javascript://" class="item btnApprove tooltips" data-placement="bottom"
                                                       data-original-title="<?php echo APPROVE;?>"
                                                       onclick="setoneaction(<?php echo $property_id; ?>,'approve', '<?php echo ARE_YOU_SURE_YOU_WANT_TO_APPROVE_SELECTED_PROJECT; ?>', 'frm_listproject')"><i
                                                            class="icon-ok"></i></a>

                                                    <?php
                                                } else if ($row['status'] == 7 || $row['status'] == 8) {
                                                    ?>
                                                    <a href="javascript://" class="item btnApprove tooltips" data-placement="bottom"
                                                       data-original-title="Active"
                                                       onclick="setoneaction(<?php echo $property_id; ?>,'active', '<?php echo ARE_YOU_SURE_YOU_WANT_TO_ACTIVE_SELECTED_PROJECT; ?>', 'frm_listproject')"><i
                                                            class="icon-ok"></i></a>
                                                <?php } else { ?>

                                                <?php } ?>

                                                <!-- Decline -->
                                                <?php if ($row['status'] == 1) { ?>
                                                    <a href="javascript://" role="button" class="item btnDecline tooltips"
                                                       data-placement="bottom"  onclick="setoneaction(<?php echo $property_id; ?>,'declined', '<?php echo ARE_YOU_SURE_YOU_WANT_TO_DECLINE_SELECTED_PROJECT; ?>', 'frm_listproject')" data-original-title="<?php echo DECLINE;?>" data-toggle="modal"><i
                                                            class="icon-remove"></i></a>
                                                <?php } ?>


                                                <a href="<?php echo site_url('admin/property/add_properties_data/'.$property_id_api);?>" role="button" class="item btnView tooltips"
                                                   data-placement="bottom" data-original-title="<?php echo "Get Metadata";?>" data-toggle="modal" target="_blank"><i
                                                        class="icon-edit"></i></a>

                                            </td>

                                            <div id="decline-<?php echo $property_id; ?>" class="modal hide fade" tabindex="-1" role="dialog"
                                                 aria-labelledby="myModalLabel1" aria-hidden="true">

                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h3 id="myModalLabel1"><?php echo DECLINE; ?></h3>
                                                </div>
                                                <div class="modal-body">
                                                    <p><?php echo sprintf(AS_YOU_HAVE_TO_DECIDED_TO_DECLINE, $campaign_name, $campaign_name, $user_name . ' ' . $last_name, $campaign_name) ?></p>

                                                    <div class="reasonArea">
                                                        <p> <?php echo REASON_TO_DECLINE_THIS . ' ' . $campaign_name; ?></p>
                    <textarea rows="3" name="reason_decline_equity"
                              id="reason_decline_<?php echo $property_id; ?>"><?php echo $reason_decline; ?></textarea>
                                                        <label><input type="checkbox" class="checkboxes" id="save_reason_<?php echo $property_id; ?>"
                                                                      name="save_the_reason" value="1"/>
                                                            <?php echo SAVE_THIS_REASON_TO_USE_ON_OTHER . ' ' . $campaign_name ?></label>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button class="btn" data-dismiss="modal" aria-hidden="true"><?php echo CANCEL; ?></button>
                                                    <button class="btn btn-primary"
                                                            onclick="setaction(<?php echo $property_id; ?>,'declined', '<?php echo ARE_YOU_SURE_YOU_WANT_TO_DECLINE_SELECTED_PROJECT; ?>', 'frm_listproject')"><?php echo SAVE; ?></button>
                                                </div>
                                            </div>
                                            <div id="inactive-<?php echo $property_id; ?>" class="modal hide fade" tabindex="-1" role="dialog"
                                                 aria-labelledby="myModalLabel1" aria-hidden="true">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h3 id="myModalLabel1"><?php echo INACTIVE; ?></h3>
                                                </div>
                                                <div class="modal-body">
                                                    <p><?php echo sprintf(AS_YOU_HAVE_TO_DECIDED_TO_INACTIVE, $campaign_name); ?></p>

                                                    <div class="inactivepopup">
                                                        <p>
                                                            <label class="radio">
                                                                <input type="radio" name="inactive_equity_mode_<?php echo $property_id; ?>"
                                                                       id="inactive_mode_<?php echo $property_id; ?>" value="8"/>
                                <span class="redioText"><strong><?php echo HIDDEN_MODE; ?>
                                        :</strong> <?php echo sprintf(SELECT_THIS_OPTION_IF_YOU_WANT_TO_HIDE, $campaign_name, $campaign_name); ?></span>
                                                            </label>
                                                        </p>

                                                        <p>
                                                            <label class="radio">
                                                                <input type="radio" name="inactive_equity_mode_<?php echo $property_id; ?>"
                                                                       id="inactive_mode_<?php echo $property_id; ?>" value="7"/>
                                <span class="redioText"><strong><?php echo INACTIVE_MODE; ?>
                                        :</strong> <?php echo sprintf(SELECT_THIS_OPTION_IF_YOU_WANT_TO_SHOW, $campaign_name, $campaign_name, $updates, $funds, $comments); ?></span>
                                                            </label>
                                                        </p>

                                                        <div class="reasonArea">
                                                            <p><?php echo WRITE_REASON_WHY_YOU_DEACTIVATED_THIS . ' ' . $campaign_name; ?></p>
                        <textarea rows="3" name="reason_inactive_equity"
                                  id="reason_inactive_<?php echo $property_id; ?>"><?php echo $reason_inactive_hidden; ?></textarea>
                                                            <label><input type="checkbox" class="checkboxes"
                                                                          id="save_reason_inactive_<?php echo $property_id; ?>"
                                                                          name="save_the_reason_inactive"
                                                                          value="1"/> <?php echo SAVE_THIS_REASON_TO_USE_ON_OTHER . ' ' . $campaign_name; ?>
                                                            </label>
                                                        </div>
                                                        <!--
                                                        <p class="marT15">
                                                            <label class="radio">
                                                             <input type="radio" name="optionsRadios2" value="option2" />
                                                                <span class="redioText"><strong>FundBack All:</strong> Select this option if you want to fund back all funders in their wallet or in thier payment gateway(paypal, stripe or other) accounts. ONCE FUNDBACK EXECUTED, IT CAN NOT BE UNDO.</span>
                                                             </label>
                                                        </p>
                                                        <p>
                                                            <label class="radio">
                                                             <input type="radio" name="optionsRadios2" value="option2" />
                                                                <span class="redioText"><strong>No FundBack:</strong> Select this option if you are deactivating this {campaign-dynmanic-name} temporarily. This option is useful if you want to resolve your doubts with {project-owner-dynamic-name} of this {campaign-dynmanic-name} so once your questions and doubts resolved you can make this {campaign-dynmanic-name} active again.</span>
                                                             </label>
                                                        </p>
                                                        -->
                                                        <p> <?php echo sprintf(PLEASE_NOTE_IF_ONCE_IS_INACTIVATED, $campaign_name, $funds); ?></p>

                                                        <p><strong><?php echo sprintf(IF_MANAGED_MANUALLY_BY_TEAM, $site_name); ?></strong></p>

                                                        <p class="marT15">
                                                            <label class="radio">
                                                                <input type="radio" name="fund_inactive_<?php echo $property_id; ?>"
                                                                       id="fund_inactive_<?php echo $property_id; ?>" value="1"/>
                                <span
                                    class="redioText"> <?php echo sprintf(YOU_CAN_HOLD_ALL_UNTILL_YOU_RESOLVE_YOUR_DOUBTS_AND_REASONS, $funds, $campaign_name, $user_name . ' ' . $last_name); ?></span>
                                                            </label>
                                                        </p>

                                                        <p><strong>OR</strong></p>

                                                        <p>
                                                            <label class="radio">
                                                                <input type="radio" name="fund_inactive_<?php echo $property_id; ?>"
                                                                       id="fund_inactive_<?php echo $property_id; ?>" value="0"/>
                                <span
                                    class="redioText"><?php echo sprintf(YOU_CAN_REFUND_ALL_FUND, $funds, $funds, $funds, $site_name); ?></span>
                                                            </label>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button class="btn" data-dismiss="modal" aria-hidden="true"><?php echo CANCEL; ?></button>
                                                    <button class="btn btn-primary"
                                                            onclick="setaction(<?php echo $property_id; ?>,'inactive', '<?php echo ARE_YOU_SURE_YOU_WANT_TO_INACTIVE_SELECTED_RECORD; ?>', 'frm_listproject')"><?php echo SAVE; ?></button>
                                                </div>
                                            </div>
                                            <input type="hidden" name="property_id" id="property_id" value="<?php echo $property_id; ?>">
                                        </tr>

                                        <?php


                                    }


                                }



                                ?>


                                </tbody>


                            </table>


                            </form>


                        </div>


                    </div>


                    <!-- END EXAMPLE TABLE widget-->


                </div>


            </div>


            <!-- END ADVANCED TABLE widget-->


            <!-- END PAGE CONTENT-->


        </div>


        <!-- END PAGE CONTAINER-->


    </div>


    <!-- END PAGE -->


</div>


<!-- END CONTAINER -->


<!-- BEGIN JAVASCRIPTS -->


<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>


<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/jquery.dataTables.js"></script>


<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/DT_bootstrap.js"></script>


<script src="<?php echo base_url(); ?>js/scripts.js"></script>


<script type="text/javascript" charset="utf-8">


    jQuery(document).ready(function () {


        App.init();


    });


</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#filterPill .filter-pill").click(function () {
            var res = document.URL;
            $(this).remove();
            id = $(this).attr('id');

            new_url = res.replace('', '');
            if (id == 'search_name_tag') {
                new_url = '<?php echo site_url('admin/equity/list_equity?search_by=&search_name=&search_status='.$search_status.'&search_goal_per='.$search_goal_per.'&search_goal_from='.$search_goal_from.'&search_goal_to='.$search_goal_to.'&search_category='.$search_category);?>';

            }
            if (id == 'search_status_tag') {
                new_url = '<?php echo site_url('admin/equity/list_equity?search_by='.$search_by.'&search_name='.$search_name.'&search_status=&search_goal_per='.$search_goal_per.'&search_goal_from='.$search_goal_from.'&search_goal_to='.$search_goal_to.'&search_category='.$search_category);?>';
            }
            if (id == 'search_goal_per_tag') {
                new_url = '<?php echo site_url('admin/equity/list_equity?search_by='.$search_by.'&search_name='.$search_name.'&search_status='.$search_status.'&search_goal_per=&search_goal_from='.$search_goal_from.'&search_goal_to='.$search_goal_to.'&search_category='.$search_category);?>';
            }
            if (id == 'search_goal_from_tag') {
                new_url = '<?php echo site_url('admin/equity/list_equity?search_by='.$search_by.'&search_name='.$search_name.'&search_status='.$search_status.'&search_goal_per='.$search_goal_per.'&search_goal_from=&search_goal_to='.$search_goal_to.'&search_category='.$search_category);?>';
            }
            if (id == 'search_goal_to_tag') {
                new_url = '<?php echo site_url('admin/equity/list_equity?search_by='.$search_by.'&search_name='.$search_name.'&search_status='.$search_status.'&search_goal_per='.$search_goal_per.'&search_goal_from='.$search_goal_from.'&search_goal_to=&search_category='.$search_category);?>';
            }
            if (id == 'search_category_tag') {
                new_url = '<?php echo site_url('admin/equity/list_equity?search_by='.$search_by.'&search_name='.$search_name.'&search_status='.$search_status.'&search_goal_per='.$search_goal_per.'&search_goal_from='.$search_goal_from.'&search_goal_to='.$search_goal_to.'&search_category=');?>';
            }

            window.location.href = new_url;
        });
        $("#filterClear").click(function () {
            $("#filterPill .filter-pill").remove();
            $(this).remove();
            window.location.href = '<?php echo site_url('admin/equity/list_equity'); ?>';
        });
    });
</script>













