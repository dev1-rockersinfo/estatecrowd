<?php

$campaign_name = $taxonomy_setting['project_name'];
$funds = $taxonomy_setting['funds'];
$updates = $taxonomy_setting['updates'];
$comments = $taxonomy_setting['comments'];
$taxonomy_project_url = 'properties';
$site_name = $site_setting['site_name'];
?>

<!-- BEGIN CONTAINER -->


<div id="container" class="row-fluid">


    <!-- BEGIN SIDEBAR -->


    <?php echo $this->load->view('admin_sidebar'); ?>

    <!-- END SIDEBAR -->

    <!-- BEGIN PAGE -->

    <div id="main-content">

        <!-- BEGIN PAGE CONTAINER-->

        <div class="container-fluid">


            <!-- BEGIN PAGE HEADER-->


            <div class="row-fluid">


                <div class="span12">


                    <!-- BEGIN THEME CUSTOMIZER-->


                    <div id="theme-change" class="hidden-phone">


                        <i class="icon-cogs"></i>



                        <span class="settings">



                            <span class="text"><?php echo THEME; ?></span>



                            <span class="colors">



                                <span class="color-default" data-style="default"></span>



                                <span class="color-gray" data-style="gray"></span>



                                <span class="color-purple" data-style="purple"></span>



                                <span class="color-navy-blue" data-style="navy-blue"></span>



                            </span>



                        </span>


                    </div>


                    <!-- END THEME CUSTOMIZER-->


                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->


                    <h3 class="page-title">


                        <?php echo 'Add properties by postcode'; ?>


                    </h3>


                    <ul class="breadcrumb">


                        <li>


                            <a href="<?php echo site_url('admin/home/dashboard'); ?>"><i class="icon-home"></i></a><span
                                class="divider">&nbsp;</span>


                        </li>


                        <li><a href="#"><?php echo 'Add properties by postcode'; ?>  </a><span class="divider-last">&nbsp;</span></li>


                    </ul>


                    <!-- END PAGE TITLE & BREADCRUMB-->


                </div>


            </div>


            <!-- END PAGE HEADER-->


            <!-- BEGIN ADVANCED TABLE widget-->

            <?php if($success){ ?>
            <div class="alert alert-success">
                <button class="close" data-dismiss="alert">x</button>
                <strong><?php echo SUCCESS; ?>!</strong>
                </span> <?php echo $success; ?>
            </div>
            <?php } ?>

             <?php if($error){ ?>
            <div class="alert alert-danger">
                <button class="close" data-dismiss="alert">x</button>
                <strong><?php echo ERROR; ?>!</strong>
                </span> <?php echo $error; ?>
            </div>
            <?php } ?>

            <div class="fr iconM">

            </div>


            <div class="row-fluid">


                <div class="span12">


                    <!-- BEGIN EXAMPLE TABLE widget-->


                    <div class="widget">


                        <div class="widget-title">


                            <h4><i class="icon-reorder"></i><?php echo 'Add properties by postcode'; ?></h4>



                            <span class="tools">



                                <a href="javascript:;" class="icon-chevron-down"></a>



                            </span>


                        </div>


                        <div class="widget-body">

 <!-- BEGIN FORM-->
                            <?php
                            $attributes = array('name' => 'frm_project_category', 'class' => 'form-horizontal');
                            echo form_open_multipart('admin/property/get_properties_by_postcode/'.$postcode, $attributes);


                            ?>
                           
                            <div class="control-group">
                                <label class="control-label"><?php echo "Postcode Id"; ?></label>

                                <div class="controls">
                                    <input type="text" name="postcodeId" value="<?=$postcode_id?>" class="input-xlarge"/>
                                    <span></span>
        
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label"><?php echo "Size"; ?></label>

                                <div class="controls">
                                    <input type="text" value="<?=$size?>" name="size" class="input-xlarge"/>
                                    <span>(Maximum value accepted is 20.)</span>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label"><?php echo "Page"; ?></label>

                                <div class="controls">
                                    <input type="text" name="page" value="<?=$page?>" class="input-xlarge"/>
                                    <span></span>
        
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label"><?php echo "Bathrooms"; ?></label>

                                <div class="controls">
                                    <input type="text" name="baths" value="<?=$baths?>" class="input-xlarge"/>
                                    <span>(4 = single exact match filter; 3-6 = range filter filter; 3- = range filter fetching 3 or more bathrooms.)</span>
        
                                </div>
                            </div>

                             <div class="control-group">
                                <label class="control-label"><?php echo "Bedrooms"; ?></label>

                                <div class="controls">
                                    <input type="text" name="beds" value="<?=$beds?>" class="input-xlarge"/>
                                    <span>(4 = single exact match filter; 3-6 = range filter filter; 3- = range filter fetching 3 or more bedrooms.)</span>
        
                                </div>
                            </div>

                             <div class="control-group">
                                <label class="control-label"><?php echo "Car spaces"; ?></label>

                                <div class="controls">
                                    <input type="text" name="carSpaces" value="<?=$carSpaces?>" class="input-xlarge"/>
                                    <span>(4 = single exact match filter; 3-6 = range filter filter; 3- = range filter fetching 3 or more car spaces.)</span>
                                </div>
                            </div>

                              <div class="control-group">
                                <label class="control-label"><?php echo "Land area"; ?></label>

                                <div class="controls">
                                    <input type="text" name="landArea" value="<?=$landArea?>" class="input-xlarge"/>
                                    <span>(300 = exact match filter; 0-300 = zero to 300m2 inclusive; 300-600 = 300m2 to 600m2 inclusive; 300- = 300m2 and above; -300 = zero to 300m2 inclusive.)</span>
        
                                </div>
                            </div>

                              <div class="control-group">
                                <label class="control-label"><?php echo "Property types"; ?></label>

                                <div class="controls">
                                    <input type="text" name="pTypes" value="<?=$pTypes?>" class="input-xlarge"/>
                                    <span>(Available values: 'UNIT', 'FLATS', 'COMMERCIAL', 'HOUSE', 'LAND', 'BUSINESS', 'OTHER', 'COMMUNITY', 'FARM'.)</span>
        
                                </div>
                            </div>
                        
                            <div class="form-actions">
                                <button type="submit" class="btn blue"><i class="icon-ok"></i> <?php echo SUBMIT; ?>
                                </button>
                            </div>

                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE widget-->
                </div>
            </div>
            <!-- END ADVANCED TABLE widget-->
            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN JAVASCRIPTS -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>


<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/jquery.dataTables.js"></script>


<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/DT_bootstrap.js"></script>


<script src="<?php echo base_url(); ?>js/scripts.js"></script>


<script type="text/javascript" charset="utf-8">


    jQuery(document).ready(function () {


        App.init();


    });


</script>