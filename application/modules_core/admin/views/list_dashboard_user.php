<!--Last 10 New Users-->

<div class="row-fluid">

    <div class="span12">

        <!-- BEGIN BORDERED TABLE widget-->

        <div class="widget">

            <div class="widget-title">

                <h4><i class="icon-reorder"></i><?php echo LAST_10_NEW_USERS; ?></h4>

                                        <span class="tools">

                                        <a href="javascript:;" class="icon-chevron-down downclass"></a>

                                       

                                        </span>

            </div>

            <div class="widget-body limit_height">

                <table class="table table-bordered table-hover">

                    <thead>

                    <?php



                    if ($users)



                    {


                    ?>

                    <tr>

                        <th class="tab_hidden ph_hidden"><?php echo NO; ?></th>

                        <th class="ph_hidden"><?php echo USERNAME; ?></th>

                        <th><?php echo EMAIL; ?></th>

                        <!-- <th class="ph_hidden tab_hidden"><?php //echo CITY; ?></th>

                                                    <th class="ph_hidden "><?php //echo STATE; ?></th>

                                                    <th class="ph_hidden"><?php //echo COUNTRY; ?></th>-->

                        <th class="ph_hidden tab_hidden"><?php echo SIGN_UP_IP; ?></th>

                        <th><?php echo REGISTERED_ON; ?></th>


                    </tr>

                    </thead>

                    <tbody>

                    <?php



                    $i = 1;


                    foreach ($users as $u) {

                        $new_user_name = ucfirst($u['user_name']) . ' ' . ucfirst($u['last_name']);

                        $new_user_id = $u['user_id'];

                        $active_profile_slug = $u['profile_slug'];

                        $new_user_email = $u['email'];

                        if ($u['city'] == '') {
                            $new_user_city = "N/A";
                        } else {
                            $new_user_city = ucfirst($u['city']);
                        }

                        if ($u['state'] == '') {
                            $new_user_state = "N/A";
                        } else {
                            $new_user_state = ucfirst($u['state']);
                        }

                        if ($u['country'] == '') {
                            $new_user_country = "N/A";
                        } else {
                            $new_user_country = ucfirst($u['country']);
                        }

                        $new_user_signup_ip = $u['signup_ip'];

                        $new_user_date_added = ($u['date_added'] != '') ? date($site_setting['date_format'], strtotime($u['date_added'])) : 'N/A';

                        ?>

                        <tr>

                            <td class="tab_hidden ph_hidden"><?php echo $i; ?></td>

                            <td class="ph_hidden"><a href="<?php echo site_url('user/' . $active_profile_slug); ?>"
                                                     target="_blank"><?php echo $new_user_name; ?></a></td>

                            <td><?php echo $new_user_email; ?></td>

                            <!--  <td class="ph_hidden tab_hidden"><?php //echo $new_user_city;?></td>

                                                    <td class="ph_hidden"><?php //echo $new_user_state;?></td>

                                                    <td class="ph_hidden"><?php //echo $new_user_country;?></td>-->

                            <td class="ph_hidden tab_hidden"><?php echo $new_user_signup_ip; ?></td>

                            <td><?php echo $new_user_date_added; ?></td>


                        </tr>

                        <?php

                        $i++;


                    }


                    } else {


                        ?>



                        <tr>

                            <th class="ph_hidden"><strong><?php echo NO_RECORD_FOUND; ?></strong></th>

                        </tr>

                    <?php

                    }

                    ?>

                    </tbody>

                </table>

            </div>

        </div>

        <!-- END BORDERED TABLE widget-->

    </div>

</div>


<!--Last 5 Inactive Users-->

<div class="row-fluid">

    <div class="span12">

        <!-- BEGIN BORDERED TABLE widget-->

        <div class="widget">

            <div class="widget-title">

                <h4><i class="icon-reorder"></i><?php echo LAST_5_INACTIVE_USERS; ?></h4>

                                        <span class="tools">

                                        <a href="javascript:;" class="icon-chevron-down downclass"></a>

                                       

                                        </span>

            </div>

            <div class="widget-body limit_height">

                <table class="table table-bordered table-hover">

                    <thead>

                    <?php



                    if ($inactive_users)



                    {


                    ?>

                    <tr>

                        <th class="tab_hidden ph_hidden"><?php echo NO; ?></th>

                        <th class="ph_hidden"><?php echo USERNAME; ?></th>

                        <th><?php echo EMAIL; ?></th>

                        <!-- <th class="ph_hidden tab_hidden"><?php //echo CITY; ?></th>

                                                    <th class="ph_hidden "><?php //echo STATE; ?></th>

                                                    <th class="ph_hidden"><?php //echo COUNTRY; ?></th>-->

                        <th class="ph_hidden tab_hidden"><?php echo SIGN_UP_IP; ?></th>

                        <th><?php echo REGISTERED_ON; ?></th>


                    </tr>

                    </thead>

                    <tbody>

                    <?php



                    $i = 1;


                    foreach ($inactive_users as $u) {

                        $new_user_name = ucfirst($u['user_name']) . ' ' . ucfirst($u['last_name']);

                        $inactive_profile_slug = $u['profile_slug'];

                        $new_user_id = $u['user_id'];

                        $new_user_email = $u['email'];

                        if ($u['city'] == '') {
                            $new_user_city = "N/A";
                        } else {
                            $new_user_city = ucfirst($u['city']);
                        }

                        if ($u['state'] == '') {
                            $new_user_state = "N/A";
                        } else {
                            $new_user_state = ucfirst($u['state']);
                        }

                        if ($u['country'] == '') {
                            $new_user_country = "N/A";
                        } else {
                            $new_user_country = ucfirst($u['country']);
                        }

                        $new_user_signup_ip = $u['signup_ip'];

                        $new_user_date_added = ($u['date_added'] != '') ? date($site_setting['date_format'], strtotime($u['date_added'])) : 'N/A';

                        ?>

                        <tr>

                            <td class="tab_hidden ph_hidden"><?php echo $i; ?></td>

                            <td class="ph_hidden"><a href="<?php echo site_url('user/' . $inactive_profile_slug); ?>"
                                                     target="_blank"><?php echo $new_user_name; ?></a></td>

                            <td><?php echo $new_user_email; ?></td>

                            <!-- <td class="ph_hidden tab_hidden"><?php //echo $new_user_city;?></td>

                                                    <td class="ph_hidden"><?php //echo $new_user_state;?></td>

                                                    <td class="ph_hidden"><?php //echo $new_user_country;?></td>-->

                            <td class="ph_hidden tab_hidden"><?php echo $new_user_signup_ip; ?></td>

                            <td><?php echo $new_user_date_added; ?></td>


                        </tr>

                        <?php

                        $i++;


                    }


                    } else {


                        ?>



                        <tr>

                            <th class="ph_hidden"><strong><?php echo NO_RECORD_FOUND; ?></strong></th>

                        </tr>

                    <?php

                    }

                    ?>

                    </tbody>

                </table>

            </div>

        </div>

        <!-- END BORDERED TABLE widget-->

    </div>

</div>


<!-- <div class="row-fluid">

           							    <div class="span6">

                                          <!-- BEGIN INLINE NOTIFICATIONS widget

                                          <div class="widget">

                                             <div class="widget-title">

                                                <h4><i class="icon-cogs"></i>Transaction</h4>

                                                <div class="tools">

                                                   <a href="javascript:;" class="collapse"></a>

                                                   <a href="#widget-config" data-toggle="modal" class="config"></a>

                                                   <a href="javascript:;" class="reload"></a>

                                                  

                                                </div>

                                             </div>

                                             <div class="widget-body">

                                             <div class="row-fluid">

                                                   

                                                </div>

                                                <div class="row-fluid">

                                                   <div class="span12">

                                                       <div class="pricing-table green">

                                                           <div class="pricing-head ">

                                                               <h3> Transactions($) </h3>

                                                               

                                                           </div>

                                                           <ul>

                                                               <li><strong>Total No. of Projects Posted:</strong><?php //echo $total_project_posted; ?></li>

                                                               <li><strong>Total Commission Amount Earned:</strong><?php //echo set_currency($total_earned['commission']); ?></li>

                                                               <li><strong>Total Commission Amount Earned (this month):</strong><?php //echo set_currency($total_earned_month['commission']); ?></li>

                                                               <li><strong>Total No. of Completed Project (this month):</strong><?php //echo $total_completed_project; ?></li>

                                                               <li><strong>Total Commission Amount Earned (this week):</strong> <?php //echo set_currency($total_earned_week['commission']); ?> </li>

                                                           </ul>

                                                          

                                                       </div>

                                                   </div>

                                                   <div class="spance10 visible-phone"></div>

                                                                            

                                                </div>

                                             </div>

                                          </div>

                                          <!-- END INLINE NOTIFICATIONS widget

                                       </div>

           							 </div>

            						 -->

<div class="spance10 visible-phone"></div>

<script>

    jQuery('.widget .tools .downclass, .widget .tools .upclass').click(function () {

        var el = jQuery(this).parents(".widget").children(".widget-body");

        if (jQuery(this).hasClass("icon-chevron-down")) {

            jQuery(this).removeClass("icon-chevron-down").addClass("icon-chevron-up upclass");

            el.slideUp(200);

        } else {

            jQuery(this).removeClass("icon-chevron-up").addClass("icon-chevron-down downclass");

            el.slideDown(200);

        }

    });

</script>      	
