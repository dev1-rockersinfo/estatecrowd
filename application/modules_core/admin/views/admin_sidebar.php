<?php



$this->active = $this->uri->uri_string();


$this->strdd = explode("/", $this->active);


//print_r($this->strdd);die();


if (!isset($this->strdd[1])) {


    $this->strdd[1] = '';


}


if (!isset($this->strdd[2])) {

    $this->strdd[2] = '';

}


$CI =& get_instance();


$site = $CI->config->slash_item('base_url_site');

$chk_admin = get_rights('list_admin');


$chk_admin_login = get_rights('admin_login');

$chk_user = get_rights('list_user');

$chk_user_login = get_rights('user_login');

$chk_manage_dropdown = get_rights('manage_dropdown');

$chk_equity = get_rights('list_equity');

$chk_property = get_rights('list_property');

$chk_idea = get_rights('list_idea');

$chk_photo_gallery = get_rights('list_gallery');


$chk_message = get_rights('list_message');

$chk_amazon = get_rights('list_amazon');

$chk_adaptive = get_rights('list_paypal');

$chk_paypal = get_rights('list_normal_paypal');

$chk_paypal_credit_card = get_rights('list_credit_card');

$chk_wallet_setting = get_rights('add_wallet_setting');

$chk_wallet_review = get_rights('list_wallet_review');

$chk_wallet_withdraw = get_rights('list_wallet_withdraw');

$chk_transaction = get_rights('list_transaction');

$chk_home_page = get_rights('home_page');

$chk_pages = get_rights('list_pages');

$chk_country = get_rights('list_country');

$chk_suburb = get_rights('list_suburb');

$chk_currency = get_rights('list_currency');

$chk_state = get_rights('list_state');

$chk_city = get_rights('list_city');

$chk_site = get_rights('add_site_setting');

$chk_meta = get_rights('add_meta_setting');

$chk_fb = get_rights('add_facebook_setting');

$chk_amount = get_rights('add_amount_setting');

$chk_tw = get_rights('add_twitter_setting');

$chk_li = get_rights('add_linkdin_setting');

$chk_google = get_rights('add_google_setting');

$chk_yahoo = get_rights('add_yahoo_setting');

$chk_email = get_rights('add_email_setting');

$chk_email_temp = get_rights('add_email_template');

$chk_img = get_rights('add_image_setting');

$chk_message_setting = get_rights('add_message_setting');

$chk_spam = get_rights('add_spam_setting');

$chk_spam_report = get_rights('spam_report');

$chk_spamer = get_rights('spamer');

$chk_newsletter_list = get_rights('list_newsletter');

$chk_list_newsletter_user = get_rights('list_newsletter_user');

$chk_newsletter_setting = get_rights('newsletter_setting');

$chk_newsletter_job = get_rights('newsletter_job');

$chk_faq_category = get_rights('list_faq_category');

$chk_faq = get_rights('list_faq');

$chk_school = get_rights('list_school');

$chk_guidelines = get_rights('guidelines');

$chk_youtube = get_rights('add_youtube_setting');

$chk_google_plus = get_rights('add_google_plus_setting');

$chk_cron = get_rights('list_cronjob');

$chk_taxonomy = get_rights('add_taxonomy');

$chk_banner_setting = get_rights('add_banner_slider');

$chk_deal_type_setting = get_rights('list_deal_type_setting');

$chk_other_setting = get_rights('add_other_setting');

$chk_accreditation_user_list = get_rights('accreditation_user_list');


?>



<div id="sidebar" class="nav-collapse collapse">
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="sidebar-toggler hidden-phone"></div>
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
<div class="navbar-inverse">
    <form class="navbar-search visible-phone">
        <input type="text" class="search-query" placeholder="Search"/>
    </form>
</div>

<!-- END RESPONSIVE QUICK SEARCH FORM -->
<!-- BEGIN SIDEBAR MENU -->

<ul class="sidebar-menu">
<li class=" <?php if (($this->strdd[0] == 'admin') && ($this->strdd[1] == 'home' || $this->strdd[2] == 'dashboard')) { ?> active <?php } ?>">
    <a href="<?php echo site_url('admin/home/dashboard'); ?>" class="">
        <span class="icon-box"> <i class="icon-dashboard"></i></span> <?php echo DASHBOARD; ?>
        <span class="arrow"></span>
    </a>


</li>


<li class="has-sub <?php if (($this->strdd[0] == 'admin') && $this->strdd[1] == 'graph') { ?> active <?php } ?>">

    <a href="javascript:;" class="">

        <span class="icon-box"> <i class="icon-cogs"></i></span> <?php echo GRAPHICAL_REPORT; ?>

        <span class="arrow"></span>

    </a>

    <ul class="sub">

        <li><a class="" href="<?php echo site_url('admin/graph/user'); ?>"><?php echo USERS; ?></a></li>

        <li><a class="" href="<?php echo site_url('admin/graph/'); ?>"><?php echo PROJECTS; ?></a></li>

    </ul>

</li>

<?php if ($chk_admin == 1 || $chk_user == 1 || $chk_user_login == 1 || $chk_accreditation_user_list == 1) { ?>

    <li class="has-sub <?php if (($this->strdd[0] == 'admin') && ($this->strdd[1] == 'user' || $this->strdd[1] == 'admin' || $this->strdd[1] == 'accreditation')) { ?> active <?php } ?>">

        <a href="javascript:;" class="">

            <span class="icon-box"> <i class="icon-user"></i></span> <?php echo USER; ?>

            <span class="arrow"></span>

        </a>

        <ul class="sub">

            <?php
            if ($chk_admin == 1) {
                ?>
                <li><a class=""
                       href="<?php echo site_url('admin/admin/list_admin'); ?>"><?php echo ADMINISTRATOR; ?></a></li>
            <?php } ?>

            <?php if ($chk_user == 1) { ?>
                <li><a class="" href="<?php echo site_url('admin/user/list_user'); ?>"><?php echo USER_LIST; ?></a></li>
            <?php } ?>



            <?php if ($chk_user_login == 1) { ?>
                <li><a class="" href="<?php echo site_url('admin/user/user_login'); ?>"><?php echo USER_LOGIN; ?></a>
                </li>
            <?php } ?>
            <?php if ($chk_accreditation_user_list == 1) { ?>
                <li><a class=""
                       href="<?php echo site_url('admin/accreditation/accreditation_user_list'); ?>"><?php echo ACCREDITATION_USERS; ?></a>
                </li>
            <?php } ?>
        </ul>


    </li>



<?php }
if ($chk_transaction == 1) { ?>



    <li class="has-sub  <?php if (($this->strdd[0] == 'admin') && $this->strdd[1] == 'transaction_type' && $this->strdd[2] == 'list_transaction') { ?> active <?php } ?>">


        <a href="javascript:;" class="">


            <span class="icon-box"> <i class="icon-credit-card"></i></span> <?php echo TRANSACTION; ?>



            <span class="arrow"></span>


        </a>

        <ul class="sub">

            <?php if ($chk_transaction == 1) { ?>

                <li><a class=""
                       href="<?php echo site_url('admin/transaction_type/list_transaction'); ?>"><?php echo TRANSACTIONS; ?></a>
                </li>

            <?php } ?>

        </ul>

    </li>

<?php } ?>

<?php if ($chk_equity == 1 || $chk_other_setting == 1 || $chk_deal_type_setting == 1) { ?>

    <!-- added by nirali-->
    <li class="has-sub <?php if (($this->strdd[0] == 'admin') && ($this->strdd[1] == 'campaign' || $this->strdd[1] == 'site_setting') && ($this->strdd[2] == 'list_campaign' || $this->strdd[2] == 'add_other_setting' || $this->strdd[2] == 'runningInvestment')) { ?> active <?php } ?> ">


        <a href="javascript:;" class="">


            <span class="icon-box"> <i class="icon-credit-card"></i></span> <?php echo EQUITY; ?>



            <span class="arrow"></span>


        </a>


        <ul class="sub">
            <?php if ($chk_equity == 1) { ?>

                <li><a class=""
                       href="<?php echo site_url('admin/campaign/list_campaign'); ?>"><?php echo EQUITY_LIST; ?></a></li>
            <?php }
           
            if ($chk_other_setting == 1) { ?>
                <li><a class=""
                       href="<?php echo site_url('admin/site_setting/add_other_setting'); ?>"><?php echo OTHER_SETTING; ?></a>
                </li>
            <?php } ?>

             <li><a class=""
                       href="<?php echo site_url('admin/campaign/runningInvestment'); ?>"><?php echo RUNNING_INVESTMENT; ?></a>
                </li>
               

        </ul>


    </li>

<?php } if($chk_property == 1) {?>

     <li class="has-sub <?php if (($this->strdd[0] == 'admin') && ($this->strdd[1] == 'property') && ($this->strdd[2] == 'list_property')) { ?> active <?php } ?> ">


        <a href="javascript:;" class="">


            <span class="icon-box"> <i class="icon-credit-card"></i></span> <?php echo 'Property'; ?>



            <span class="arrow"></span>


        </a>


        <ul class="sub">
            <li><a class=""
                       href="<?php echo site_url('admin/property/list_property'); ?>"><?php echo 'Property List'; ?></a></li>

        </ul>


    </li>
<?php }
if($chk_property == 1) {?>

    <li class="has-sub <?php if (($this->strdd[0] == 'admin') && ($this->strdd[1] == 'corelogic_api')) { ?> active <?php } ?> ">


        <a href="javascript:;" class="">


            <span class="icon-box"> <i class="icon-credit-card"></i></span> <?php echo 'Corelogic API'; ?>



            <span class="arrow"></span>


        </a>


        <ul class="sub">

            <li><a class=""
                   href="<?php echo site_url('admin/property/suggestions'); ?>"><?php echo 'Suggestions'; ?></a></li>

            <li><a class=""
                   href="<?php echo site_url('admin/corelogic_api/index'); ?>"><?php echo 'Postcode List'; ?></a></li>

            <li><a class=""
                   href="<?php echo site_url('admin/corelogic_api/locality_list'); ?>"><?php echo 'Locality List'; ?></a></li>

            <li><a class=""
                   href="<?php echo site_url('admin/corelogic_api/property_list'); ?>"><?php echo 'Property List'; ?></a></li>

        </ul>


    </li>
<?php }

if ($chk_home_page == 1 || $chk_pages == 1 || $chk_faq_category == 1 || $chk_faq == 1 || $chk_guidelines == 1) { ?>

    <li class="has-sub <?php if (($this->strdd[0] == 'admin') && ($this->strdd[1] == 'pages' || $this->strdd[1] == 'faq' || $this->strdd[1] == 'learn_more')) { ?> active <?php } ?>">


        <a href="javascript://;" class="">


            <span class="icon-box"> <i class="icon-file"></i></span> <?php echo CONTENT_PAGES; ?>



            <span class="arrow"></span>


        </a>


        <ul class="sub">

            <?php if ($chk_pages == 1) { ?>
                <li><a class="" href="<?php echo site_url('admin/pages/list_pages'); ?>"><?php echo PAGES; ?></a></li>
            <?php } ?>

            <?php if ($chk_faq == 1) { ?>
                <li><a class="" href="<?php echo site_url('admin/pages/list_faq'); ?>"><?php echo FAQ; ?></a></li>
            <?php } ?>

            <?php if ($chk_pages == 1) { ?>
                <li><a class=""
                       href="<?php echo site_url('admin/learn_more/list_pages'); ?>"><?php echo LEARN_MOREE; ?></a></li>
            <?php } ?>

            <?php if ($chk_pages == 1) { ?>
                <li><a class=""
                       href="<?php echo site_url('admin/learn_more/list_learn_category'); ?>"><?php echo LEARN_MORE_CATEGORIESS; ?></a>
                </li>
            <?php } ?>

        </ul>


    </li>

<?php }
if ($chk_banner_setting == 1) { ?>

    <li class="has-sub <?php if (($this->strdd[0] == 'admin') && ($this->strdd[1] == 'equity' || $this->strdd[1] == 'site_setting') && ($this->strdd[2] == 'list_dynamic_slider' || $this->strdd[2] == 'add_dynamic_slider' || $this->strdd[2] == 'slider_setting')) { ?> active <?php } ?>">


        <a href="javascript:;" class="">


            <span class="icon-box"> <i class="icon-picture"></i></span> <?php echo BANNER_SLIDER; ?>



            <span class="arrow"></span>


        </a>


        <ul class="sub">


            <li><a class=""
                   href="<?php echo base_url(); ?>admin/equity/list_dynamic_slider"><?php echo MANAGE_SLIDER; ?></a>
            </li>

            <li><a class=""
                   href="<?php echo site_url('admin/site_setting/slider_setting'); ?>"><?php echo SLIDER_SETTING; ?></a>
            </li>


        </ul>


    </li>



<?php }
if ($chk_country == 1 || $chk_suburb == 1) { ?>



    <li class="has-sub <?php if (($this->strdd[0] == 'admin') && $this->strdd[1] == 'country' || $this->strdd[1] == 'suburb') { ?> active <?php } ?>">


        <a href="javascript:;" class="">


            <span class="icon-box"> <i class="icon-globe"></i></span> <?php echo GLOBALIZATION; ?>



            <span class="arrow"></span>


        </a>


        <ul class="sub">


            <?php if ($chk_country == 1) { ?>
                <li><a class=""
                       href="<?php echo site_url('admin/country/list_country'); ?>"><?php echo COUNTRIES; ?></a></li>
            <?php } ?>


            <?php if ($chk_suburb == 1) { ?>
                <li><a class=""
                       href="<?php echo site_url('admin/suburb/list_suburb'); ?>"><?php echo 'Suburb'; ?></a></li>
            <?php } ?>

        </ul>

    </li>

<?php }
if ($chk_idea == 1 || $chk_currency == 1 || $chk_message == 1) { ?>

    <li class="has-sub <?php if (($this->strdd[0] == 'admin') && ($this->strdd[1] == 'message' || $this->strdd[1] == 'versions' || $this->strdd[1] == 'cronjob' || $this->strdd[1] == 'currency')) { ?> active <?php } ?>">

        <a href="javascript:;" class="">

            <span class="icon-box"> <i class="icon-bullhorn"></i></span> <?php echo OTHER_FEATURE; ?>

            <span class="arrow"></span>

        </a>

        <ul class="sub">

            <?php if ($chk_cron == 1) { ?>
                <li><a class=""
                       href="<?php echo site_url('admin/cronjob/list_cronjob'); ?>"><?php echo CRON_JOBS; ?></a>
                </li><?php } ?>


            <?php if ($chk_currency == 1) { ?>
                <li><a class="" href="<?php echo site_url('admin/currency'); ?>"><?php echo CURRENCY; ?></a>
                </li><?php } ?>

            <li><a class="" href="<?php echo site_url('admin/versions'); ?>"><?php echo VERSIONS; ?></a></li>

        </ul>

    </li>

<?php }
if ($chk_email_temp == 1) { ?>

    <li class="has-sub <?php if (($this->strdd[0] == 'admin') && $this->strdd[1] == 'email_template') { ?> active <?php } ?>">

        <a href="javascript://;" class="">

            <span class="icon-box"> <i class="icon-inbox"></i></span> <?php echo EMAIL; ?>

            <span class="arrow"></span>

        </a>

        <ul class="sub">

            <li><a class=""
                   href="<?php echo site_url('admin/email_template/list_email_template/'); ?>"><?php echo EMAIL_TEMPLATE; ?></a>
            </li>

        </ul>

    </li>

<?php }
if ($chk_site == 1 || $chk_meta == 1 || $chk_email == 1 || $chk_img == 1 || $chk_amount == 1 || $chk_taxonomy == 1) { ?>

    <li class="has-sub <?php if ($this->strdd[0] == 'admin' && ($this->strdd[1] == 'site_setting' || $this->strdd[1] == 'message_setting' || $this->strdd[1] == 'meta_setting' || $this->strdd[1] == 'email_setting') && ($this->strdd[2] == 'add_site_setting' || $this->strdd[2] == 'add_meta_setting'
            || $this->strdd[2] == 'add_amount_formatting' || $this->strdd[2] == 'add_email_setting' || $this->strdd[2] == 'add_image_setting' || $this->strdd[2] == 'add_message_setting' || $this->strdd[2] == 'add_taxonomy' || $this->strdd[2] == 'add_property')
    ) {
        ?> active <?php } ?>">


        <a href="javascript:;" class="">


            <span class="icon-box"> <i class="icon-cog"></i></span> <?php echo SETTING; ?>



            <span class="arrow"></span>


        </a>


        <ul class="sub">

            <?php if ($chk_site == 1) { ?>
                <li><a class=""
                       href="<?php echo site_url('admin/site_setting/add_site_setting'); ?>"><?php echo SITE; ?></a>
                </li>
            <?php } ?>

            <?php if ($chk_meta == 1) { ?>
                <li><a class=""
                       href="<?php echo site_url('admin/meta_setting/add_meta_setting'); ?>"><?php echo META; ?></a>
                </li>
            <?php } ?>

            <?php if ($chk_amount == 1) { ?>
                <li><a class=""
                       href="<?php echo site_url('admin/site_setting/add_amount_formatting'); ?>"><?php echo AMOUNT; ?></a>
                </li>
            <?php } ?>


            <?php if ($chk_email == 1) { ?>
                <li><a class=""
                       href="<?php echo site_url('admin/email_setting/add_email_setting'); ?>"><?php echo EMAIL; ?></a>
                </li>
            <?php } ?>

            <?php if ($chk_img == 1) { ?>
                <li><a class=""
                       href="<?php echo site_url('admin/site_setting/add_image_setting'); ?>"><?php echo IMAGE_SIZE; ?></a>
                </li>
            <?php } ?>

            <?php if ($chk_taxonomy == 1) { ?>
                <li><a class=""
                       href="<?php echo site_url('admin/site_setting/add_taxonomy'); ?>"><?php echo TAXONOMY; ?></a>
                </li>
            <?php } ?>


                <li><a class=""
                       href="<?php echo site_url('admin/site_setting/add_property'); ?>"><?php echo 'Property'; ?></a>
                </li>
           

        </ul>

    </li>

<?php }
if ($chk_fb == 1 || $chk_tw == 1 || $chk_li == 1 || $chk_google == 1 || $chk_yahoo == 1 || $chk_google_plus == 1 || $chk_youtube == 1) { ?>
    <li class="has-sub <?php if ($this->strdd[0] == 'admin' && $this->strdd[1] == 'social_setting') { ?> active <?php } ?>">

        <a href="javascript:;" class="">

            <span class="icon-box"> <i class="icon-cog"></i></span> <?php echo SOCIAL_SETTING; ?>

            <span class="arrow"></span>

        </a>

        <ul class="sub">

            <?php if ($chk_fb == 1) { ?>
                <li><a class=""
                       href="<?php echo site_url('admin/social_setting/add_facebook_setting'); ?>"><?php echo FACEBOOK; ?></a>
                </li>
            <?php } ?>

            <?php if ($chk_tw == 1) { ?>
                <li><a class=""
                       href="<?php echo site_url('admin/social_setting/add_twitter_setting'); ?>"><?php echo TWITTER; ?></a>
                </li>
            <?php } ?>

            <?php if ($chk_li == 1) { ?>
                <li><a class=""
                       href="<?php echo site_url('admin/social_setting/add_linkdin_setting'); ?>"><?php echo LINKEDIN; ?></a>
                </li>
            <?php } ?>

            <?php if ($chk_google == 1) { ?>
                <li><a class=""
                       href="<?php echo site_url('admin/social_setting/add_google_setting'); ?>"><?php echo GOOGLE; ?></a>
                </li>
            <?php } ?>

            <?php if ($chk_google_plus == 1) { ?>
                <li><a class=""
                       href="<?php echo site_url('admin/social_setting/add_google_plus_setting'); ?>"><?php echo GOOGLE_PLUS; ?></a>
                </li>
            <?php } ?>

            <?php if ($chk_youtube == 1) { ?>
                <li><a class=""
                       href="<?php echo site_url('admin/social_setting/add_youtube_setting'); ?>"><?php echo YOUTUBE; ?></a>
                </li>
            <?php } ?>

        </ul>


    </li>

<?php } ?>



<?php if ($chk_spam == 1 || $chk_spam_report == 1 || $chk_spamer == 1) { ?>



    <li class="has-sub <?php if (($this->strdd[0] == 'admin') && $this->strdd[1] == 'spam') { ?> active <?php } ?>">

        <a href="javascript:;" class="">

            <span class="icon-box"><i class="icon-cogs"></i></span> <?php echo SPAM_SETTING; ?>

            <span class="arrow"></span>

        </a>

        <ul class="sub">

            <?php if ($chk_spam_report == 1) { ?>
                <li><a class="" href="<?php echo site_url('admin/spam/spam_report'); ?>"><?php echo SPAM_REPORT; ?></a>
                </li>
            <?php } ?>



            <?php if ($chk_spamer == 1) { ?>
                <li><a class="" href="<?php echo site_url('admin/spam/spamer'); ?>"><?php echo SPAMMER; ?></a></li>
            <?php } ?>



            <?php if ($chk_spam == 1) { ?>
                <li><a class="" href="<?php echo site_url('admin/spam/add_spam_setting'); ?>"><?php echo SETTING; ?></a>
                </li>
            <?php } ?>

        </ul>


    </li>



<?php }
if ($chk_newsletter_list == 1 || $chk_list_newsletter_user == 1 || $chk_newsletter_setting == 1 || $chk_newsletter_job == 1) { ?>



    <li class="has-sub <?php if (($this->strdd[0] == 'admin') && $this->strdd[1] == 'newsletter') { ?> active <?php } ?>">

        <a href="javascript:;" class="">

            <span class="icon-box"><i class="icon-list-alt"></i></span><?php echo NEWLETTERS; ?>

            <span class="arrow"></span>

        </a>

        <ul class="sub">

            <?php if ($chk_newsletter_list == 1) { ?>
                <li><a class=""
                       href="<?php echo site_url('admin/newsletter/list_newsletter'); ?>"><?php echo NEWLETTERS; ?></a>
                </li>
            <?php } ?>

            <?php if ($chk_list_newsletter_user == 1) { ?>
                <li><a class=""
                       href="<?php echo site_url('admin/newsletter/list_newsletter_user'); ?>"><?php echo NEWLETTERS_USERS; ?></a>
                </li>
            <?php } ?>

            <?php if ($chk_newsletter_job == 1) { ?>
                <li><a class=""
                       href="<?php echo site_url('admin/newsletter/newsletter_job'); ?>"><?php echo NEWSLETTER_JOB; ?></a>
                </li>
            <?php } ?>

            <?php if ($chk_newsletter_setting == 1) { ?>
                <li><a class=""
                       href="<?php echo site_url('admin/newsletter/newsletter_setting'); ?>"><?php echo NEWLETTERS_SETTING; ?></a>
                </li>
            <?php } ?>

        </ul>


    </li>



<?php } ?>

</ul>


<!-- END SIDEBAR MENU -->


</div>



