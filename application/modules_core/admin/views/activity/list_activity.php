<script src="<?php echo base_url(); ?>js/jquery.pajinate.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#paging').pajinate({
            items_per_page: 100,
            abort_on_small_lists: true,
            item_container_id: '.paging',
            nav_panel_id: '.pro-pagination',

        });
    });
</script>

<!-- BEGIN CONTAINER -->
<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php echo $this->load->view('admin_sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN THEME CUSTOMIZER-->
                    <div id="theme-change" class="hidden-phone">
                        <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text"><?php echo THEME; ?>:</span> <!-- change by darshan -->
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
                    </div>
                    <!-- END THEME CUSTOMIZER-->
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        <?php echo ACTIVITIES; ?>
                    </h3>
                    <ul class="breadcrumb">
                        <li>
                            <a href="<?php echo site_url('admin/home/dashboard'); ?>"><i class="icon-home"></i></a><span
                                class="divider">&nbsp;</span>
                        </li>

                        <li>
                            <a href="<?php echo site_url('admin/activity/list_activity'); ?>"><?php echo ACTIVITIES; ?></a><span
                                class="divider-last">&nbsp;</span></li>
                    </ul>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->

            <!-- BEGIN ADVANCED TABLE widget-->

            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN EXAMPLE TABLE widget-->
                    <div class="widget">
                        <div class="widget-title">
                            <h4><i class="icon-bell-alt"></i><?php echo ACTIVITIES; ?></h4>
                            <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                            </span>
                        </div>


                        <div class="widget-body">
                            <div class="widget-body" id="paging">
                                <div class="timeline-messages paging">
                                    <!-- Activity -->
                                    <?php

                                    if ($activities) {
                                        foreach ($activities as $admin_activity) {

                                            $activity_action_detail = $admin_activity['action_detail'];
                                            $activity_profile_slug = $admin_activity['profile_slug'];
                                            $activity_user_name = $admin_activity['user_name'];
                                            $activity_last_name = $admin_activity['last_name'];
                                            $activity_image = $admin_activity['image'];
                                            $activity_datetime = $admin_activity['datetime'];
                                            $activity_action = $admin_activity['action'];

                                            $action_array = array('project_approve', 'project_inactive', 'project_deleted', 'project_approve', 'project_declined', 'approve_contract', 'declined_contract', 'upload_contract', 'acknowledge_contract', 'approve_acknowledge_contract', 'declined_acknowledge_contract', 'submmited_tracking', 'project_success', 'project_unsuccess');
                                            if (in_array($activity_action, $action_array)) {
                                                $activity_user_image_src = base_url() . 'upload/user/user_small_image/no_man.jpg';
                                                $activity_user_link = 'javascript://';
                                            } else {

                                                $activity_user_link = site_url('user/' . $activity_profile_slug);
                                                if ($activity_image != '' && is_file("upload/user/user_small_image/" . $activity_image)) {
                                                    $activity_user_image_src = base_url() . 'upload/user/user_small_image/' . $activity_image;
                                                } else {
                                                    $activity_user_image_src = base_url() . 'upload/user/user_small_image/no_man.jpg';
                                                }
                                            }


                                            ?>
                                            <div class="msg-time-chat">
                                                <a href="<?php echo $activity_user_link; ?>" class="message-img">
                                                    <img class="avatar" src="<?php echo $activity_user_image_src; ?>"
                                                         alt=""></a>

                                                <div class="message-body">
                                                    <div class="text">
                                                        <p class="attribution"><?php echo $activity_action_detail; ?></p>

                                                        <p class="small"><?php echo getDuration($activity_datetime); ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php
                                        }
                                    }
                                    ?>
                                    <!-- /Activity -->

                                </div>
                                <div class="pro-pagination"></div>

                            </div>
                        </div>
                    </div>

                </div>
                <!-- END EXAMPLE TABLE widget-->
            </div>
        </div>

        <!-- END ADVANCED TABLE widget-->

        <!-- END PAGE CONTENT-->
    </div>
    <!-- END PAGE CONTAINER-->
</div>
<!-- END PAGE -->
</div>
<!-- END CONTAINER -->

<!-- BEGIN JAVASCRIPTS -->
<!-- Load javascripts at bottom, this will reduce page load time -->


<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>


<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/jquery.dataTables.js"></script>


<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/DT_bootstrap.js"></script>


<script src="<?php echo base_url(); ?>js/scripts.js"></script>


<script type="text/javascript" charset="utf-8">


    jQuery(document).ready(function () {


        App.init();


    });


</script>
