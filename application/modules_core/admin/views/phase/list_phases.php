<script type="text/javascript" language="javascript">
    function deletePhase() {
        var ans = confirm("<?php echo DELETE_PHASE;?>");
        if (ans) {
           return true;
        } else {
            return false;
        }
    }

</script>
<!-- BEGIN CONTAINER -->
<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php echo $this->load->view('admin_sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN THEME CUSTOMIZER-->
                    <div id="theme-change" class="hidden-phone">
                        <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text"><?php echo THEME; ?>:</span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
                    </div>
                    <!-- END THEME CUSTOMIZER-->
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        <?php echo PHASE_LIST; ?>
                    </h3>
                    <ul class="breadcrumb">
                        <li>
                            <a href="<?php echo site_url('admin/home/dashboard'); ?>"><i class="icon-home"></i></a><span
                                class="divider">&nbsp;</span>
                        </li>

                        <li><a href="#"><?php echo PHASE_LIST; ?> </a><span
                                class="divider-last">&nbsp;</span></li>
                    </ul>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <?php if ($msg != '') {
                if ($msg == 'insert') {
                    ?>
                    <div class="alert alert-success">
                        <button class="close" data-dismiss="alert">x</button>
                        <strong><?php echo SUCCESS; ?>!</strong> <?php echo NEW_RECORD_HAS_BEEN_ADDED_SUCCESSFULLY; ?>
                    </div>

                <?php
                }
                if ($msg == 'update') {
                    ?>
                    <div class="alert alert-success">
                        <button class="close" data-dismiss="alert">x</button>
                        <strong><?php echo SUCCESS; ?>!</strong> <?php echo RECORD_HAS_BEEN_UPDATED_SUCCESSFULLY; ?>
                    </div>
                <?php
                }
                if ($msg == 'delete') {
                    ?>
                    <div class="alert alert-success">
                        <button class="close" data-dismiss="alert">x</button>
                        <strong><?php echo SUCCESS; ?>!</strong> <?php echo RECORD_HAS_BEEN_DELETED_SUCCESSFULLY; ?>
                    </div>
                <?php
                }

            }?>
            <!-- BEGIN ADVANCED TABLE widget-->
            <div class="fr iconM">


                <button type="button" class="btn mini purple"
                        onclick="location.href='<?php echo site_url('admin/phase/add_phase'); ?>'">
                    <i class="icon-plus"></i> <?php echo ADD_PHASE; ?></button>
            </div>

            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN EXAMPLE TABLE widget-->

                    <div class="widget">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i><?php echo PHASE_LIST; ?>   </h4>
                            <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                            </span>
                        </div>
                        <div class="widget-body">
                            <?php
                            if ($phases) {
                                $i = 1; ?>
                                <table class="table table-striped table-bordered trans" id="sample_1">
                                    <thead>
                                    <tr>
                                        <th><?php echo NO; ?></th>
                                        <th><?php echo NAME; ?></th>

                                        <th><?php echo TYPE; ?></th>
                                        <th><?php echo STATUS; ?></th>
                                        <th><?php echo DATE; ?></th>
                                        <th><?php echo ACTION; ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <?php

                                    if($phases){
                                       
                                        foreach ($phases as $row) {
                                            $is_parent = $row['is_parent']==1?MAIN:RELATED;
                                            $id = $row['id'];
                                            $name = $row['name'];
                                            $status = $row['status']==1?ACTIVE:INACTIVE;
                                            $created_at = $row['created_at'];
                                            $total_equity= $row['total_equity'];
                                            $is_delete=true;
                                            if($total_equity>0){
                                                $is_delete=false;
                                            }
                                                
                                            ?>
                                            <tr class="odd gradeX">

                                                <td><?php echo $i; ?></td>
                                                <td><?php echo $name; ?></td>


                                                <td><?=$is_parent?></td>
                                                <td><?php echo $status; ?></td>
                                                <td><?=$created_at?></td>
                                                <td><a href="<?=site_url('admin/phase/edit_phase/'.$id)?>"><?=EDIT?></a>  <?php if($is_delete){?>| <a href="<?=site_url('admin/phase/delete_phase/'.$id)?>" onclick='return deletePhase()'><?=DELETE?></a>  <?php } ?></td>
                                            </tr>
                                            <?php
                                            $i++;
                                        }
                                    } 
                                    ?>

                                    </tbody>
                                </table>
                            <?php
                            } else {
                                ?>
                                <div align="center">
                                    <b><?php echo NO_RECORD_FOUND; ?> </b>
                                </div>
                            <?php } ?>

                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE widget-->
                </div>
            </div>

            <!-- END ADVANCED TABLE widget-->

            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>
<!-- END CONTAINER -->

<!-- BEGIN JAVASCRIPTS -->

<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/DT_bootstrap.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        App.init();
    });
</script>
