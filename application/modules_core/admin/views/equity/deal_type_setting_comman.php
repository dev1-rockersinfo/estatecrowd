<!-- BEGIN CONTAINER -->
<div id="container" class="row-fluid">
<!-- BEGIN SIDEBAR -->
<?php echo $this->load->view('admin_sidebar'); ?>
<!-- END SIDEBAR -->
<!-- BEGIN PAGE -->
<div id="main-content">
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN THEME CUSTOMIZER-->
        <div id="theme-change" class="hidden-phone">
            <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text"><?php echo THEME; ?>:</span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
        </div>
        <!-- END THEME CUSTOMIZER-->
        <h3 class="page-title">

            <?php echo EDIT_DEAL_TYPE_SETTING; ?>
        </h3>
        <ul class="breadcrumb">
            <li>
                <a href="<?php echo site_url('admin/home/dashboard') ?>"><i class="icon-home"></i></a><span
                    class="divider">&nbsp;</span>
            </li>

            <li>
                <a href="<?php echo site_url('admin/equity/list_deal_type_setting'); ?>"> <?php echo DEAL_TYPE_SETTING_LIST; ?></a><span
                    class="divider-last">&nbsp;</span></li>
        </ul>
    </div>
</div>
<?php if ($slug != '' and $deal_type_id == 1) {
    ?>
    <!-- END PAGE HEADER-->
    <?php
    if ($error != "") {
        ?>
        <div class="alert alert-error">
            <button class="close" data-dismiss="alert">x</button>
            <strong><?php echo ERRORS; ?>!</strong>

            <p> <?php echo $error; ?></p>
        </div>

    <?php } ?>
    <!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid">
        <div class="span12 sortable">
            <!-- BEGIN SAMPLE FORMPORTLET-->
            <div class="widget">
                <div class="widget-title">
                    <h4><i class="icon-reorder"></i>  <?php echo EDIT_DEAL_TYPE_SETTING; ?></h4>
                             
                                        <span class="tools">
                                        <a href="javascript:;" class="icon-chevron-down"></a>
                                        <a href="javascript:;" class="icon-remove"></a>
                                        </span>
                </div>
                <div class="widget-body">
                    <!-- BEGIN FORM-->
                    <?php
                    $attributes = array('name' => 'frm_deal_type', 'class' => 'form-horizontal');
                    echo form_open_multipart('admin/equity/deal_type_setting/' . $slug, $attributes);
                    ?>


                    <div class="control-group">
                        <label class="control-label"><?php echo DEAL_TYPE_NAME; ?><font color="red"> *</font></label>

                        <div class="controls">
                            <input type="text" placeholder="Deal Type Name" name="deal_type_name" id="deal_type_name"
                                   value="<?php echo $deal_type_name; ?>" class="input-xlarge"/>

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"><?php echo DEAL_TYPE_DESCRIPTION; ?><font color="red">
                                *</font></label>

                        <div class="controls">
                            <textarea class="input-xlarge" style=" width:350px; height:170px;"
                                      placeholder="Deal Type Description"
                                      name="deal_type_description"><?php echo $deal_type_description; ?></textarea>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"><?php echo DEAL_TYPE_ICON; ?><font color="red"> *</font></label>

                        <div class="controls">
                            <input type="file" name="image_load"/>
                            <img src="<?php echo base_url("upload/equity/deal_icon/" . $image_load); ?>"
                                 style="max-width:75px;">
                            <input type="hidden" name="dynamic_image_image" value="<?php echo $image_load; ?>"/>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label"><?php echo MIN_PRICE_PER_SHARE; ?><font color="red">
                                *</font></label>

                        <div class="controls">
                            <input type="text" placeholder="Min Price Per Share" name="min_price_per_share"
                                   id="min_price_per_share" value="<?php echo $min_price_per_share; ?>"
                                   class="input-xlarge"/>

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"><?php echo MAX_PRICE_PER_SHARE; ?><font color="red">
                                *</font></label>

                        <div class="controls">
                            <input type="text" placeholder="Max Price Per Share" name="max_price_per_share"
                                   id="max_price_per_share" value="<?php echo $max_price_per_share; ?>"
                                   class="input-xlarge"/>

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"><?php echo MIN_EQUITY_AVAILABLE; ?><font color="red">
                                *</font></label>

                        <div class="controls">
                            <input type="text" placeholder="Min Equity Available" name="min_equity_available"
                                   id="min_equity_available" value="<?php echo $min_equity_available; ?>"
                                   class="input-xlarge"/>

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"><?php echo MAX_EQUITY_AVAILABLE; ?><font color="red">
                                *</font></label>

                        <div class="controls">
                            <input type="text" placeholder="Max Equity Available" name="max_equity_available"
                                   id="max_equity_available" value="<?php echo $max_equity_available; ?>"
                                   class="input-xlarge"/>

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"><?php echo MIN_COMPANY_VALUATION; ?><font color="red">
                                *</font></label>

                        <div class="controls">
                            <input type="text" placeholder="Min Company Valuation" name="min_company_valuation"
                                   id="min_company_valuation" value="<?php echo $min_company_valuation; ?>"
                                   class="input-xlarge"/>

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"><?php echo MAX_COMPANY_VALUATION; ?><font color="red">
                                *</font></label>

                        <div class="controls">
                            <input type="text" placeholder="Max Company Valuation" name="max_company_valuation"
                                   id="max_company_valuation" value="<?php echo $max_company_valuation; ?>"
                                   class="input-xlarge"/>

                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label"><?php echo STATUS; ?></label>

                        <div class="controls">
                            <select name="status" id="status">
                                <option value="1" <?php if ($status == "1") {
                                    echo "selected='selected'";
                                } ?>><?php echo ACTIVE; ?></option>
                                <option value="0" <?php if ($status == "0") {
                                    echo "selected='selected'";
                                } ?>><?php echo INACTIVE; ?></option>
                            </select>
                        </div>
                    </div>

                    <input type="hidden" name="deal_type_id" id="deal_type_id" value="<?php echo $deal_type_id; ?>"/>

                    <div class="form-actions">

                        <button type="submit" class="btn blue"><i class="icon-ok"></i> <?php echo UPDATE; ?></button>

                        <button type="button" class="btn"
                                onClick="location.href='<?php echo site_url('admin/equity/list_deal_type_setting'); ?>'">
                            <i class=" icon-remove"></i> <?php echo CANCEL; ?></button>
                    </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END SAMPLE FORM PORTLET-->
        </div>
    </div>
<?php
}
if ($slug != '' and $deal_type_id == 2) {
    ?>
    <?php
    if ($error != "") {
        ?>
        <div class="alert alert-error">
            <button class="close" data-dismiss="alert">x</button>
            <strong><?php echo ERRORS; ?>!</strong>

            <p> <?php echo $error; ?></p>
        </div>

    <?php } ?>
    <div class="row-fluid">
        <div class="span12 sortable">
            <!-- BEGIN SAMPLE FORMPORTLET-->
            <div class="widget">
                <div class="widget-title">
                    <h4><i class="icon-reorder"></i>  <?php echo EDIT_DEAL_TYPE_SETTING; ?></h4>
                             
                                        <span class="tools">
                                        <a href="javascript:;" class="icon-chevron-down"></a>
                                        <a href="javascript:;" class="icon-remove"></a>
                                        </span>
                </div>
                <div class="widget-body">
                    <!-- BEGIN FORM-->
                    <?php
                    $attributes = array('name' => 'frm_deal_type', 'class' => 'form-horizontal');
                    echo form_open_multipart('admin/equity/deal_type_setting/' . $slug, $attributes);
                    ?>
                    <div class="control-group">
                        <label class="control-label"><?php echo DEAL_TYPE_NAME; ?><font color="red"> *</font></label>

                        <div class="controls">
                            <input type="text" placeholder="Deal Type Name" name="deal_type_name" id="deal_type_name"
                                   value="<?php echo $deal_type_name; ?>" class="input-xlarge"/>

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"><?php echo DEAL_TYPE_DESCRIPTION; ?><font color="red">
                                *</font></label>

                        <div class="controls">
                            <textarea class="input-xlarge" style=" width:350px; height:170px;"
                                      placeholder="Deal Type Description"
                                      name="deal_type_description"><?php echo $deal_type_description; ?></textarea>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"><?php echo DEAL_TYPE_ICON; ?><font color="red"> *</font></label>

                        <div class="controls">
                            <input type="file" name="image_load"/>
                            <img src="<?php echo base_url("upload/equity/deal_icon/" . $image_load); ?>">
                            <input type="hidden" name="dynamic_image_image" value="<?php echo $image_load; ?>"/>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label"><?php echo MIN_CONVERTIBLE_INTEREST; ?><font color="red"> *</font></label>

                        <div class="controls">
                            <input type="text" placeholder="Min Convertible Interest" name="min_convertible_interest"
                                   id="min_convertible_interest" value="<?php echo $min_convertible_interest; ?>"
                                   class="input-xlarge"/>

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"><?php echo MAX_CONVERTIBLE_INTEREST; ?><font color="red"> *</font></label>

                        <div class="controls">
                            <input type="text" placeholder="Max Convertible Interest" name="max_convertible_interest"
                                   id="max_convertible_interest" value="<?php echo $max_convertible_interest; ?>"
                                   class="input-xlarge"/>

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"><?php echo MIN_VALUATION_CAP; ?><font color="red"> *</font></label>

                        <div class="controls">
                            <input type="text" placeholder="Min Valuation Cap" name="min_valuation_cap"
                                   id="min_valuation_cap" value="<?php echo $min_valuation_cap; ?>"
                                   class="input-xlarge"/>

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"><?php echo MAX_VALUATION_CAP; ?><font color="red"> *</font></label>

                        <div class="controls">
                            <input type="text" placeholder="Max Valuation Cap" name="max_valuation_cap"
                                   id="max_valuation_cap" value="<?php echo $max_valuation_cap; ?>"
                                   class="input-xlarge"/>

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"><?php echo MIN_WARRANT_COVERAGE; ?><font color="red">
                                *</font></label>

                        <div class="controls">
                            <input type="text" placeholder="Min Warrant Coverage" name="min_warrant_coverage"
                                   id="min_warrant_coverage" value="<?php echo $min_warrant_coverage; ?>"
                                   class="input-xlarge"/>

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"><?php echo MAX_WARRANT_COVERAGE; ?><font color="red">
                                *</font></label>

                        <div class="controls">
                            <input type="text" placeholder="Max Warrant Coverage" name="max_warrant_coverage"
                                   id="max_warrant_coverage" value="<?php echo $max_warrant_coverage; ?>"
                                   class="input-xlarge"/>

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"><?php echo MIN_CONVERTIBLE_TERM_LENGHT; ?><font color="red">
                                *</font></label>

                        <div class="controls">
                            <input type="text" placeholder="Min Convertible Term Length"
                                   name="min_convertible_term_length" id="min_convertible_term_length"
                                   value="<?php echo $min_convertible_term_length; ?>" class="input-xlarge"/>

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"><?php echo MAX_CONVERTIBLE_TERM_LENGHT; ?><font color="red">
                                *</font></label>

                        <div class="controls">
                            <input type="text" placeholder="Max Convertible Term Length"
                                   name="max_convertible_term_length" id="max_convertible_term_length"
                                   value="<?php echo $max_convertible_term_length; ?>" class="input-xlarge"/>

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"><?php echo MIN_CONVERSATION_DISCOUNT; ?><font color="red"> *</font></label>

                        <div class="controls">
                            <input type="text" placeholder="Min Conversation Discount" name="min_conversation_discount"
                                   id="min_conversation_discount" value="<?php echo $min_conversation_discount; ?>"
                                   class="input-xlarge"/>

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"><?php echo MAX_CONVERSATION_DISCOUNT; ?><font color="red"> *</font></label>

                        <div class="controls">
                            <input type="text" placeholder="Max Conversation Discount" name="max_conversation_discount"
                                   id="max_conversation_discount" value="<?php echo $max_conversation_discount; ?>"
                                   class="input-xlarge"/>

                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label"><?php echo STATUS; ?></label>

                        <div class="controls">
                            <select name="status" id="status">
                                <option value="1" <?php if ($status == "1") {
                                    echo "selected='selected'";
                                } ?>><?php echo ACTIVE; ?></option>
                                <option value="0" <?php if ($status == "0") {
                                    echo "selected='selected'";
                                } ?>><?php echo INACTIVE; ?></option>
                            </select>
                        </div>
                    </div>

                    <input type="hidden" name="deal_type_id" id="deal_type_id" value="<?php echo $deal_type_id; ?>"/>
                    <input type="hidden" name="slug" id="slug" value="<?php echo $slug; ?>"/>

                    <div class="form-actions">

                        <button type="submit" class="btn blue"><i class="icon-ok"></i> <?php echo UPDATE; ?></button>

                        <button type="button" class="btn"
                                onClick="location.href='<?php echo site_url('admin/equity/list_deal_type_setting'); ?>'">
                            <i class=" icon-remove"></i> <?php echo CANCEL; ?></button>
                    </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END SAMPLE FORM PORTLET-->
        </div>
    </div>

<?php
}
if ($slug != '' and $deal_type_id == 3) {
    ?>
    <?php
    if ($error != "") {
        ?>
        <div class="alert alert-error">
            <button class="close" data-dismiss="alert">x</button>
            <strong><?php echo ERRORS; ?>!</strong>

            <p> <?php echo $error; ?></p>
        </div>

    <?php } ?>
    <div class="row-fluid">
        <div class="span12 sortable">
            <!-- BEGIN SAMPLE FORMPORTLET-->
            <div class="widget">
                <div class="widget-title">
                    <h4><i class="icon-reorder"></i>  <?php echo EDIT_DEAL_TYPE_SETTING; ?></h4>
                             
                                        <span class="tools">
                                        <a href="javascript:;" class="icon-chevron-down"></a>
                                        <a href="javascript:;" class="icon-remove"></a>
                                        </span>
                </div>
                <div class="widget-body">
                    <!-- BEGIN FORM-->
                    <?php
                    $attributes = array('name' => 'frm_deal_type', 'class' => 'form-horizontal');
                    echo form_open_multipart('admin/equity/deal_type_setting/' . $slug, $attributes);
                    ?>
                    <div class="control-group">
                        <label class="control-label"><?php echo DEAL_TYPE_NAME; ?><font color="red"> *</font></label>

                        <div class="controls">
                            <input type="text" placeholder="Deal Type Name" name="deal_type_name" id="deal_type_name"
                                   value="<?php echo $deal_type_name; ?>" class="input-xlarge"/>

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"><?php echo DEAL_TYPE_DESCRIPTION; ?><font color="red">
                                *</font></label>

                        <div class="controls">
                            <textarea class="input-xlarge" style=" width:350px; height:170px;"
                                      placeholder="Deal Type Description"
                                      name="deal_type_description"><?php echo $deal_type_description; ?></textarea>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"><?php echo DEAL_TYPE_ICON; ?><font color="red"> *</font></label>

                        <div class="controls">
                            <input type="file" name="image_load"/>
                            <img src="<?php echo base_url("upload/equity/deal_icon/" . $image_load); ?>">
                            <input type="hidden" name="dynamic_image_image" value="<?php echo $image_load; ?>"/>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label"><?php echo MIN_DEBT_INTEREST; ?><font color="red"> *</font></label>

                        <div class="controls">
                            <input type="text" placeholder="Min Debt Interest" name="min_debt_interest"
                                   id="min_debt_interest" value="<?php echo $min_debt_interest; ?>"
                                   class="input-xlarge"/>

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"><?php echo MAX_DEBT_INTEREST; ?><font color="red"> *</font></label>

                        <div class="controls">
                            <input type="text" placeholder="Max Debt Interest" name="max_debt_interest"
                                   id="max_debt_interest" value="<?php echo $max_debt_interest; ?>"
                                   class="input-xlarge"/>

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"><?php echo MIN_DEBT_TERM_LENGTH; ?><font color="red">
                                *</font></label>

                        <div class="controls">
                            <input type="text" placeholder="Min Debt Term Length" name="min_debt_term_length"
                                   id="min_debt_term_length" value="<?php echo $min_debt_term_length; ?>"
                                   class="input-xlarge"/>

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"><?php echo MAX_DEBT_TERM_LENGTH; ?><font color="red">
                                *</font></label>

                        <div class="controls">
                            <input type="text" placeholder="Max Debt Term Length" name="max_debt_term_length"
                                   id="max_debt_term_length" value="<?php echo $max_debt_term_length; ?>"
                                   class="input-xlarge"/>

                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label"><?php echo STATUS; ?></label>

                        <div class="controls">
                            <select name="status" id="status">
                                <option value="1" <?php if ($status == "1") {
                                    echo "selected='selected'";
                                } ?>><?php echo ACTIVE; ?></option>
                                <option value="0" <?php if ($status == "0") {
                                    echo "selected='selected'";
                                } ?>><?php echo INACTIVE; ?></option>
                            </select>
                        </div>
                    </div>

                    <input type="hidden" name="deal_type_id" id="deal_type_id" value="<?php echo $deal_type_id; ?>"/>
                    <input type="hidden" name="slug" id="slug" value="<?php echo $slug; ?>"/>

                    <div class="form-actions">

                        <button type="submit" class="btn blue"><i class="icon-ok"></i> <?php echo UPDATE; ?></button>

                        <button type="button" class="btn"
                                onClick="location.href='<?php echo site_url('admin/equity/list_deal_type_setting'); ?>'">
                            <i class=" icon-remove"></i> <?php echo CANCEL; ?></button>
                    </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END SAMPLE FORM PORTLET-->
        </div>
    </div>


<?php
}
if ($slug != '' and $deal_type_id == 4) {
    ?>
    <?php
    if ($error != "") {
        ?>
        <div class="alert alert-error">
            <button class="close" data-dismiss="alert">x</button>
            <strong><?php echo ERRORS; ?>!</strong>

            <p> <?php echo $error; ?></p>
        </div>

    <?php } ?>
    <div class="row-fluid">
        <div class="span12 sortable">
            <!-- BEGIN SAMPLE FORMPORTLET-->
            <div class="widget">
                <div class="widget-title">
                    <h4><i class="icon-reorder"></i>  <?php echo EDIT_DEAL_TYPE_SETTING; ?></h4>
                             
                                        <span class="tools">
                                        <a href="javascript:;" class="icon-chevron-down"></a>
                                        <a href="javascript:;" class="icon-remove"></a>
                                        </span>
                </div>
                <div class="widget-body">
                    <!-- BEGIN FORM-->
                    <?php
                    $attributes = array('name' => 'frm_deal_type', 'class' => 'form-horizontal');
                    echo form_open_multipart('admin/equity/deal_type_setting/' . $slug, $attributes);
                    ?>
                    <div class="control-group">
                        <label class="control-label"><?php echo DEAL_TYPE_NAME; ?><font color="red"> *</font></label>

                        <div class="controls">
                            <input type="text" placeholder="Deal Type Name" name="deal_type_name" id="deal_type_name"
                                   value="<?php echo $deal_type_name; ?>" class="input-xlarge"/>

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"><?php echo DEAL_TYPE_DESCRIPTION; ?><font color="red">
                                *</font></label>

                        <div class="controls">
                            <textarea class="input-xlarge" style=" width:350px; height:170px;"
                                      placeholder="Deal Type Description"
                                      name="deal_type_description"><?php echo $deal_type_description; ?></textarea>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"><?php echo DEAL_TYPE_ICON; ?><font color="red"> *</font></label>

                        <div class="controls">
                            <input type="file" name="image_load"/>
                            <img src="<?php echo base_url("upload/equity/deal_icon/" . $image_load); ?>">
                            <input type="hidden" name="dynamic_image_image" value="<?php echo $image_load; ?>"/>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label"><?php echo MIN_MAXIMUM_RETURN; ?><font color="red">
                                *</font></label>

                        <div class="controls">
                            <input type="text" placeholder="Min Maximum Return" name="min_maximum_return"
                                   id="min_maximum_return" value="<?php echo $min_maximum_return; ?>"
                                   class="input-xlarge"/>

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"><?php echo MAX_MAXIMUM_RETURN; ?><font color="red">
                                *</font></label>

                        <div class="controls">
                            <input type="text" placeholder="Max Maximum Return" name="max_maximum_return"
                                   id="max_maximum_return" value="<?php echo $max_maximum_return; ?>"
                                   class="input-xlarge"/>

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"><?php echo MIN_RETURN_PERCENTAGE; ?><font color="red">
                                *</font></label>

                        <div class="controls">
                            <input type="text" placeholder="Min Return Percentage" name="min_return_percentage"
                                   id="min_return_percentage" value="<?php echo $min_return_percentage; ?>"
                                   class="input-xlarge"/>

                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"><?php echo MAX_RETURN_PERCENTAGE; ?><font color="red">
                                *</font></label>

                        <div class="controls">
                            <input type="text" placeholder="Max Return Percentage" name="max_return_percentage"
                                   id="max_return_percentage" value="<?php echo $max_return_percentage; ?>"
                                   class="input-xlarge"/>

                        </div>
                    </div>


                    <div class="control-group">
                        <label class="control-label"><?php echo STATUS; ?></label>

                        <div class="controls">
                            <select name="status" id="status">
                                <option value="1" <?php if ($status == "1") {
                                    echo "selected='selected'";
                                } ?>><?php echo ACTIVE; ?></option>
                                <option value="0" <?php if ($status == "0") {
                                    echo "selected='selected'";
                                } ?>><?php echo INACTIVE; ?></option>
                            </select>
                        </div>
                    </div>

                    <input type="hidden" name="deal_type_id" id="deal_type_id" value="<?php echo $deal_type_id; ?>"/>
                    <input type="hidden" name="slug" id="slug" value="<?php echo $slug; ?>"/>

                    <div class="form-actions">

                        <button type="submit" class="btn blue"><i class="icon-ok"></i> <?php echo UPDATE; ?></button>

                        <button type="button" class="btn"
                                onClick="location.href='<?php echo site_url('admin/equity/list_deal_type_setting'); ?>'">
                            <i class=" icon-remove"></i> <?php echo CANCEL; ?></button>
                    </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END SAMPLE FORM PORTLET-->
        </div>
    </div>
<?php } ?>
<!-- END PAGE CONTENT-->
</div>
<!-- END PAGE CONTAINER-->
</div>
<!-- END PAGE -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN JAVASCRIPTS -->
<script type="text/javascript"
        src="<?php echo base_url(); ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>

<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        App.init();
    });
</script>
  
