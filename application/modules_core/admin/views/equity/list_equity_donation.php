<!-- BEGIN CONTAINER -->
<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php echo $this->load->view('admin_sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN THEME CUSTOMIZER-->
                    <div id="theme-change" class="hidden-phone">
                        <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text"><?php echo THEME; ?></span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
                    </div>
                    <!-- END THEME CUSTOMIZER-->
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        <?php echo EQUITY_DONATION; ?>
                    </h3>
                    <ul class="breadcrumb">
                        <li>
                            <a href="<?php echo site_url('admin/equity/list_equity'); ?>"><i
                                    class="icon-home"></i></a><span class="divider">&nbsp;</span>
                        </li>

                        <li><a href="#"><?php echo EQUITY_DONATION; ?></a><span class="divider-last">&nbsp;</span></li>
                    </ul>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->


            <!-- BEGIN ADVANCED TABLE widget-->


            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN EXAMPLE TABLE widget-->
                    <div class="widget">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i><?php echo EQUITY_DONATION; ?></h4>
                            <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                            </span>
                        </div>
                        <div class="widget-body">
                            <?php
                            // echo "<pre>";

                            if ($donations) {
                                $i = 1; ?>
                                <table class="table table-striped table-bordered trans" id="sample_1">
                                    <thead>
                                    <tr>
                                        <th class="hidden-phone tab_hidden"><?php echo NO; ?></th>
                                        <th><?php echo DATE; ?></th>
                                        <th><?php echo DONOR; ?></th>
                                        <th class="hidden-phone"><?php echo EMAIL; ?></th>
                                        <th class="hidden-phone"><?php echo PERK; ?></th>
                                        <th class="hidden-phone"><?php echo AMOUNT; ?></th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php

                                    foreach ($donations as $row) {
                                        $transaction_date_time = $row['transaction_date_time'];
                                        $user_id = $row['user_id'];


                                        $donar_user = $this->project_category_model->get_user_detail($user_id);
                                        if ($user_id != 0) {
                                            $username = ucfirst($donar_user['user_name'] . ' ' . $donar_user['last_name']);
                                        } else {
                                            $username = '';
                                        }

                                        $email = $row['email'];
                                        $perk_id = $row['perk_id'];
                                        $perk_name = $this->project_category_model->get_perk_name($perk_id);
                                        $amount = $row['amount'];
                                        $project_id = $row['project_id'];




                                        ?>
                                        <tr class="odd gradeX">

                                            <td class="hidden-phone tab_hidden"><?php echo $i; ?></td>
                                            <td class="hidden-phone"><?php echo $transaction_date_time; ?></td>
                                            <td>
                                                <a href="<?php echo front_base_url() . 'member/' . $user_id; ?>"
                                                   target="_blank"><?php echo $username; ?></a>
                                            </td>
                                            <td class="hidden-phone"><?php echo $email; ?></td>
                                            <td class="hidden-phone"><?php echo $perk_name; ?></td>
                                            <td class="hidden-phone"><?php echo set_currency($amount, $project_id); ?></td>

                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                    ?>

                                    </tbody>
                                </table>
                            <?php } else { ?>
                                <div align="center">
                                    <b> <?php echo NO_DONATION; ?> </b>
                                </div>
                            <?php } ?>

                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE widget-->
                </div>
            </div>

            <!-- END ADVANCED TABLE widget-->

            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>
<!-- END CONTAINER -->

<!-- BEGIN JAVASCRIPTS -->

<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/DT_bootstrap.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        App.init();
    });
</script>
