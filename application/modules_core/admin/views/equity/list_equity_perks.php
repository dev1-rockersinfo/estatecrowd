<!-- BEGIN CONTAINER -->
<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php echo $this->load->view('admin_sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN THEME CUSTOMIZER-->
                    <div id="theme-change" class="hidden-phone">
                        <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text"><?php echo THEME; ?></span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
                    </div>
                    <!-- END THEME CUSTOMIZER-->
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        <?php echo EQUITY_PERK; ?>
                    </h3>
                    <ul class="breadcrumb">
                        <li>
                            <a href="<?php echo site_url('admin/equity/list_equity'); ?>"><i
                                    class="icon-home"></i></a><span class="divider">&nbsp;</span>
                        </li>

                        <li><a href="#"><?php echo EQUITY_PERK; ?></a><span class="divider-last">&nbsp;</span></li>
                    </ul>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->


            <!-- BEGIN ADVANCED TABLE widget-->


            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN EXAMPLE TABLE widget-->
                    <div class="widget">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i><?php echo EQUITY_PERK; ?></h4>
                            <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                            </span>
                        </div>
                        <div class="widget-body">
                            <?php
                            //print_r($all_perks); die;
                            if ($all_perks) {
                                $i = 1; ?>
                                <table class="table table-striped table-bordered trans" id="sample_1">
                                    <thead>
                                    <tr>
                                        <th><?php echo NO; ?></th>
                                        <th><?php echo PERK_TITLE; ?></th>
                                        <th><?php echo DESCRIPTION; ?></th>
                                        <th><?php echo AMOUNT; ?></th>
                                        <th><?php echo TOTAL_CLAIM; ?></th>
                                        <th><?php echo CLAIMED_PERK; ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($all_perks as $row) {
                                        $perk_title = $row['perk_title'];
                                        $perk_description = $row['perk_description'];
                                        $perk_amount = $row['perk_amount'];
                                        $perk_total = $row['perk_total'];
                                        if ($perk_total == '') {
                                            $perk_total = UNLIMITED;
                                        }
                                        $perk_get = $row['perk_get'];
                                        $perk_project_id = $row['project_id'];

                                        ?>
                                        <tr class="odd gradeX">

                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $perk_title; ?></td>
                                            <td><?php echo $perk_description; ?></td>
                                            <td><?php echo set_currency($perk_amount, $perk_project_id); ?></td>
                                            <td><?php echo $perk_total; ?></td>
                                            <td><?php if ($perk_get == '') {
                                                    echo '0';
                                                } else {
                                                    echo $perk_get;
                                                } ?></td>

                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                    ?>

                                    </tbody>
                                </table>
                            <?php } else { ?>
                                <div align="center">
                                    <b><?php echo NO_PERK; ?> </b>
                                </div>
                            <?php } ?>

                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE widget-->
                </div>
            </div>

            <!-- END ADVANCED TABLE widget-->

            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>
<!-- END CONTAINER -->

<!-- BEGIN JAVASCRIPTS -->

<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/DT_bootstrap.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        App.init();
    });
</script>
