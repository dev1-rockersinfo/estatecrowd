<html>
<title></title>
<head>
    <style type="text/css">
        .pt20_app {

            padding-top: 40px;
        }

    </style>
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
    <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet"/>


</head>
<body>
<?php
$attributes = array('class' => 'form-horizontal');
echo form_open('admin/equity_contract/approve_contract', $attributes);

$status_new = '';
$document_name_new = '';


if ($id == 1) {

    $status = PROCESSING_CONTRACT;

} elseif ($id == 2) {

    $status = DOWNLOADED_CONTRACT;
} elseif ($id == 3) {

    $status = UPLOADED_SIGNED_CONTRACT;
    $document_name_new = $document_name;
} elseif ($id == 4) {

    $status = SIGNED_CONTRACT_APPROVE;
} elseif ($id == 5) {

    $status = PAYMENT_PROCESS;
} elseif ($id == 6) {

    $status = PAYMENT_RECIEPT_UPLOADED;
    $document_name_new = $acknowledge_doc;
} elseif ($id == 7) {

    $status = PAYMENT_CONFIRMED;

} else {

    $status = TRACKING_SHIPMENT;
}

if ($id == 3) {

    $status_new = SIGNED_CONTRACT_APPROVE;
}
if ($id == 6) {

    $status_new = PAYMENT_CONFIRMED;

}

?>

<div class="pt20_app">

    <?php if ($msg == 'update') { ?>
        <div class="alert alert-success">
            <button class="close" data-dismiss="alert">x</button>
            <strong><?php echo SUCCESS; ?>!</strong>
            <?php echo RECORD_HAS_BEEN_UPDATED_SUCCESSFULLY; ?>.
        </div>
    <?php } ?>



    <div class="control-group">
        <label class="control-label"><?php echo CURRENT_STATUS; ?></label>

        <div class="controls">
            <?php echo $status; ?>
        </div>
    </div>

    <input type="hidden" name="project_id" value="<?php echo $project_id; ?>">
    <input type="hidden" name="user_id" value="<?php echo $user_id; ?>">

    <div class="control-group">
        <label class="control-label"><?php echo DOWNLOAD_ATTACH_FILE; ?></label>

        <div class="controls">
            <a href="<?php echo site_url('admin/equity_contract/download_doc/' . $id . '/' . $document_name_new); ?>"><?php echo $document_name_new; ?></a>
        </div>
    </div>


    <div class="control-group">
        <label class="control-label"><?php echo STATUS; ?></label>

        <div class="controls">
            <select name="status_id" class="input-large m-wrap" tabindex="1" id="status_id">
                <!-- <option >-- Select --</option> -->
                <option value="<?php echo $id ?>"><?php echo $status_new; ?></option>
            </select>

        </div>
    </div>
    <?php if ($id == 6) { ?>

        <div class="control-group">
            <label class="control-label"><?php echo SHIPMENT; ?> </label>

            <div class="controls">
                <textarea name="shipment" rows="5" cols="15"></textarea>
            </div>
        </div>

    <?php } ?>

    <div class="form-actions">

        <button type="submit" class="btn blue" name="submit" value="Approve">
            <i class="icon-ok"></i> <?php echo APPROVE; ?></button>

        <a href="<?php echo site_url('admin/equity_contract/reject_status/' . $id . '/' . $project_id . '/' . $user_id); ?>"
           class="btn blue"><?php echo REJECT; ?></a>

    </div>
    </form>
    <!-- END FORM-->
</div>
</body>
</html>
