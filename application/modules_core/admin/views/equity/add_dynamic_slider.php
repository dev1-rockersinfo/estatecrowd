<script type="text/javascript"
        src="<?php echo base_url(); ?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<link rel="stylesheet" type="text/css"
      href="<?php echo base_url(); ?>assets/bootstrap-colorpicker/css/colorpicker.css"/>

<!-- BEGIN CONTAINER -->
<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php echo $this->load->view('admin_sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN THEME CUSTOMIZER-->
                    <div id="theme-change" class="hidden-phone">
                        <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text"><?php echo THEME; ?></span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
                    </div>
                    <!-- END THEME CUSTOMIZER-->

                    <?php
                    $page_title = '';
                    if ($dynamic_id != '') {

                        $page_title = EDIT_SLIDER;
                    } else {
                        $page_title = ADD_SLIDER;
                    }
                    ?>
                    <h3 class="page-title">
                        <?php echo $page_title; ?>
                    </h3>
                    <ul class="breadcrumb">
                        <li>
                            <a href="<?php echo base_url() ?>admin/home/dashboard"><i class="icon-home"></i></a><span
                                class="divider">&nbsp;</span>
                        </li>

                        <li><a href="#"><?php echo $page_title; ?>   </a><span class="divider-last">&nbsp;</span></li>
                    </ul>
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <?php
            if ($error != "") {
                ?>
                <div class="alert alert-error">
                    <button class="close" data-dismiss="alert">x</button>
                    <strong><?php echo ERRORS; ?>!</strong> <?php echo $error; ?></div>
            <?php
            }
            ?>
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                <div class="span12 sortable">
                    <!-- BEGIN SAMPLE FORMPORTLET-->
                    <div class="widget">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i><?php echo $page_title; ?>   </h4>
                                        <span class="tools">
                                        <a href="javascript:;" class="icon-chevron-down"></a>
                                        <a href="javascript:;" class="icon-remove"></a>
                                        </span>
                        </div>
                        <div class="widget-body">
                            <!-- BEGIN FORM-->
                            <?php
                            $attributes = array('name' => 'frm_project_category', 'class' => 'form-horizontal');
                            echo form_open_multipart('admin/equity/add_dynamic_slider/' . $dynamic_id, $attributes);


                            ?>



                            <div class="control-group">
                                <label class="control-label"><?php echo TITLE; ?></label>

                                <div class="controls">
                                    <input type="text" placeholder="<?php echo TITLE; ?>" name="slider_name"
                                           value="<?php echo $slider_name; ?>" class="input-xlarge"/>

                                    <input type="text" style="width: 7%;" name="color_picker"
                                           class="colorpicker-default " value="<?php echo $color_picker; ?>"/>


                                </div>
                            </div>
                            <!--
                                 <div class="control-group">
                                    <label class="control-label"><?php echo SUB_TITLE; ?></label>
                                    <div class="controls">
                                        <input type="text" placeholder="<?php echo SUB_TITLE; ?>" name="small_text" value="<?php echo $small_text; ?>" class="input-xlarge" />
                                     
                                    </div>
                                </div>

                                -->

                            <div class="control-group">
                                <label class="control-label"><?php echo BRIEF; ?></label>

                                <div class="controls">
                                    <input type="text" placeholder="<?php echo BRIEF; ?>" name="slider_content"
                                           value='<?php echo $slider_content; ?>' class="input-xlarge"/>


                                    <input type="text" style="width: 7%;" name="color_picker_content"
                                           class="colorpicker-default " value="<?php echo $color_picker_content; ?>"/>
                                </div>
                            </div>


                            <div class="control-group">
                                <label class="control-label"><?php echo SLIDER_IMAGE; ?></label>

                                <div class="controls">
                                    <input type="file" name="image_load"/>
                                    <input type="hidden" name="dynamic_image_image" value="<?php echo $image_load; ?>"/>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label"><?php echo BUTTON_TITLE; ?></label>

                                <div class="controls">

                                    <input type="text" placeholder="<?php echo BUTTON_TITLE; ?>" name="link_name"
                                           value="<?php echo $link_name; ?>" class="input-xlarge"/>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label"><?php echo BUTTON_LINK; ?></label>

                                <div class="controls">
                                    <input type="text" placeholder="<?php echo BUTTON_LINK; ?>" name="link"
                                           value="<?php echo $link; ?>" class="input-xlarge"/>

                                </div>
                            </div>


                            <div class="control-group">
                                <label class="control-label"><?php echo STATUS; ?></label>

                                <div class="controls">
                                    <select class="input-large m-wrap" tabindex="1" name="active">
                                        <option value="0" <?php if ($active == '0') {
                                            echo "selected";
                                        } ?>><?php echo INACTIVE; ?></option>
                                        <option value="1" <?php if ($active == '1') {
                                            echo "selected";
                                        } ?>><?php echo ACTIVE; ?></option>
                                    </select>
                                </div>
                            </div>
                            <input type="hidden" name="dynamic_id" id="dynamic_id" value="<?php echo $dynamic_id; ?>"/>

                            <div class="form-actions">
                                <?php
                                if ($dynamic_id == "") {
                                    ?>
                                    <button type="submit" class="btn blue"><i class="icon-ok"></i> <?php echo SUBMIT; ?>
                                    </button>
                                <?php
                                } else {
                                    ?>
                                    <button type="submit" class="btn blue"><i class="icon-ok"></i> <?php echo UPDATE; ?>
                                    </button>
                                <?php
                                }
                                ?>
                                <button type="button" class="btn"
                                        onClick="location.href='<?php echo site_url('admin/equity/list_dynamic_slider'); ?>'">
                                    <i class=" icon-remove"></i> <?php echo CANCEL; ?></button>
                            </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>

            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN JAVASCRIPTS -->
<script type="text/javascript"
        src="<?php echo base_url(); ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        App.init();
    });
</script>
  
