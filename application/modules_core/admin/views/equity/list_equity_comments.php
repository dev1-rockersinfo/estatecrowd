<!-- BEGIN CONTAINER -->
<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php echo $this->load->view('admin_sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN THEME CUSTOMIZER-->
                    <div id="theme-change" class="hidden-phone">
                        <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text"><?php echo THEME; ?></span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
                    </div>
                    <!-- END THEME CUSTOMIZER-->
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        <?php echo EQUITY_COMMENT; ?>
                    </h3>
                    <ul class="breadcrumb">
                        <li>
                            <a href="<?php echo site_url('admin/project_category/list_project'); ?>"><i
                                    class="icon-home"></i></a><span class="divider">&nbsp;</span>
                        </li>

                        <li><a href="#"><?php echo EQUITY_COMMENT; ?></a><span class="divider-last">&nbsp;</span></li>
                    </ul>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->


            <!-- BEGIN ADVANCED TABLE widget-->


            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN EXAMPLE TABLE widget-->
                    <div class="widget">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i><?php echo EQUITY_COMMENT; ?></h4>
                            <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                            </span>
                        </div>
                        <div class="widget-body">
                            <?php
                            //echo '<pre>'; print_r($comments);
                            if ($comments) {
                                $i = 1; ?>
                                <table class="table table-striped table-bordered trans" id="sample_1">
                                    <thead>
                                    <tr>
                                        <th class="hidden-phone tab_hidden"><?php echo NO; ?></th>
                                        <th><?php echo PROJECT; ?></th>
                                        <th><?php echo USER; ?></th>
                                        <th class="hidden-phone"><?php echo EMAIL; ?></th>
                                        <th class="hidden-phone"><?php echo COMMENT; ?></th>
                                        <th class="hidden-phone"><?php echo DATE; ?></th>
                                        <th class="hidden-phone"><?php echo STATUS; ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($comments as $row) {
                                        $project_url_title = $row['url_project_title'];
                                        //$project_id=$row['project_id'];
                                        $project_title = $row['project_title'];
                                        $user_id = $row['user_id'];
                                        $comment = $row['comments'];

                                        $donar_user = $this->project_category_model->get_user_detail($user_id);

                                        $user_name = $donar_user['user_name'];
                                        $last_name = $donar_user['last_name'];
                                        $user_email = $donar_user['email'];

                                        if ($row['status'] == '1')
                                            $cmt_status = 'Approved';
                                        elseif ($row['status'] == '2')
                                            $cmt_status = 'Decline';
                                        elseif ($row['status'] == '3')
                                            $cmt_status = 'Spam';
                                        else
                                            $cmt_status = 'Pending';
                                        $comment_date = date($site_setting['date_format'], strtotime($row['date_added']));

                                        ?>
                                        <tr class="odd gradeX">

                                            <td class="hidden-phone tab_hidden"><?php echo $i; ?></td>
                                            <td>
                                                <a href="<?php echo site_url('projects/' . $project_url_title . '/' . $project_id); ?>"
                                                   target="_blank">
                                                    <?php echo $project_title; ?>
                                                </a></td>
                                            <td>
                                                <a href="<?php echo site_url('member/' . $user_id); ?>"
                                                   target="_blank"><?php echo $user_name . ' ' . $last_name; ?></a>
                                            </td>
                                            <td class="hidden-phone"><?php echo $user_email; ?></td>
                                            <td class="hidden-phone"><?php echo $comment; ?></td>
                                            <td class="hidden-phone"><?php echo $comment_date; ?></td>
                                            <td class="hidden-phone"><?php echo $cmt_status; ?></td>

                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                    ?>

                                    </tbody>
                                </table>
                            <?php } else { ?>
                                <div align="center">
                                    <b> <?php echo NO_COMMENTS; ?> </b>
                                </div>
                            <?php } ?>

                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE widget-->
                </div>
            </div>

            <!-- END ADVANCED TABLE widget-->

            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>
<!-- END CONTAINER -->

<!-- BEGIN JAVASCRIPTS -->

<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/DT_bootstrap.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        App.init();
    });
</script>
