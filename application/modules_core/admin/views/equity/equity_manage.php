<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php echo $this->load->view('admin_sidebar'); ?>

    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN THEME CUSTOMIZER-->
                    <div id="theme-change" class="hidden-phone">
                        <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text"><?php echo THEME; ?></span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
                    </div>
                    <!-- END THEME CUSTOMIZER-->
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        <?php echo CONTRACT_MANAGE; ?>

                    </h3>
                    <ul class="breadcrumb">
                        <li>
                            <a href="<?php echo site_url('admin/home/dashboard'); ?>"><i class="icon-home"></i></a><span
                                class="divider">&nbsp;</span>
                        </li>

                        <li><?php echo anchor('admin/equity_contract/contract_manage', CONTRACT_MANAGE, 'id="sp_1"'); ?>
                            <span class="divider-last">&nbsp;</span></li>
                    </ul>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <?php

            if ($msg == 'update') {
                ?>
                <div class="alert alert-success">
                    <button class="close" data-dismiss="alert">x</button>
                    <strong><?php echo SUCCESS; ?>!</strong> <?php echo RECORD_HAS_BEEN_UPDATED_SUCCESSFULLY; ?>.
                </div>

            <?php
            }
            if ($msg == 'sent') {
                ?>
                <div class="alert alert-success">
                    <button class="close" data-dismiss="alert">x</button>
                    <strong><?php echo SUCCESS; ?>!</strong> <?php echo YOUR_MESSAGE_HAS_BEEN_SUCCESSFULLY_SENT; ?>.
                </div>

            <?php } ?>


            <!-- END PAGE HEADER-->

            <!-- BEGIN ADVANCED TABLE widget-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN EXAMPLE TABLE widget-->
                    <div class="widget">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i> <?php echo CONTRACT_MANAGE; ?></h4>
                            <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                            </span>
                        </div>
                        <div class="widget-body">
                            <table class="table table-striped table-bordered" id="sample_1">
                                <thead>
                                <tr>
                                    <th><?php echo NO; ?></th>
                                    <th class="mob_hidden"><?php echo EQUITY_NAME; ?></th>
                                    <th class="mob_hidden"><?php echo USER_NAME; ?></th>
                                    <th><?php echo STATUS; ?></th>
                                    <th><?php echo HISTORY; ?></th>
                                    <th><?php echo ACTION; ?></th>

                                </tr>
                                </thead>

                                <tbody>
                                <?php

                                if ($result) {
                                    $i = 1;
                                    foreach ($result as $row) {

                                        $project_title = $row['project_title'];
                                        $user_name = $row['user_name'];
                                        $invest_status_id = $row['invest_status_id'];
                                        $equity_id = $row['equity_id'];
                                        $user_id = $row['user_id'];
                                        $in_process_id = $row['id'];


                                        if ($invest_status_id == 1) {

                                            $status = PROCESSING_CONTRACT;

                                        } elseif ($invest_status_id == 2) {

                                            $status = DOWNLOADED_CONTRACT;
                                        } elseif ($invest_status_id == 3) {

                                            $status = UPLOADED_SIGNED_CONTRACT;
                                        } elseif ($invest_status_id == 4) {

                                            $status = SIGNED_CONTRACT_APPROVE;
                                        } elseif ($invest_status_id == 5) {

                                            $status = PAYMENT_PROCESS;
                                        } elseif ($invest_status_id == 6) {

                                            $status = PAYMENT_RECIEPT_UPLOADED;
                                        } elseif ($invest_status_id == 7) {

                                            $status = PAYMENT_CONFIRMED;
                                        } else {

                                            $status = TRACKING_SHIPMENT;

                                        }

                                        ?>

                                        <tr class="odd gradeX">
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $project_title; ?></td>
                                            <td class="mob_hidden"><a class="fancybox fancybox.iframe"
                                                                      href="<?php echo site_url('admin/equity_contract/send_message/' . $user_id . '/' . $equity_id); ?>"><?php echo SEND_MESSAGE; ?> </a>
                                                to <?php echo $user_name; ?></td>

                                            <td><?php echo $status; ?></td>

                                            <td><a class="fancybox fancybox.iframe"
                                                   href="<?php echo site_url('admin/equity_contract/detail/' . '3' . '/' . $in_process_id); ?>"><?php echo VIEW; ?></a>
                                            </td>

                                            <td>
                                                <a href="<?php echo site_url('investment/adminstep/' . $in_process_id); ?>"
                                                   target="_blank">< <?php echo "View Page"; ?> ></a>

                                                <?php /*if($invest_status_id==3){ .'/'.$equity_id.'/'.$user_id?>
                                    <a class="fancybox fancybox.iframe" href="<?php echo site_url('admin/equity_contract/approve_contract/'.$invest_status_id.'/'.$equity_id.'/'.$user_id);?>">< <?php echo SIGNED_CONTRACT_APPROVE; ?> ></a>  
                                <?php  }?>
                                <?php if($invest_status_id==6){?>
                                    <a class="fancybox fancybox.iframe" href="<?php echo site_url('admin/equity_contract/approve_contract/'.$invest_status_id.'/'.$equity_id.'/'.$user_id);?>">< <?php echo PAYMENT_CONFIRMED; ?> ></a>  
                                <?php  } */
                                                ?>
                                            </td>

                                        </tr>

                                        <?php
                                        $i++;

                                    }

                                }

                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE widget-->
                </div>
            </div>

            <!-- END ADVANCED TABLE widget-->

            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>
<!-- BEGIN JAVASCRIPTS -->
<!-- Load javascripts at bottom, this will reduce page load time -->

<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/DT_bootstrap.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        App.init();
    });

    jQuery(document).ready(function () {


        $('#sample_1').dataTable({
            "bDestroy": true,

            "oLanguage": {
                "sLengthMenu": " _MENU_ <?php echo RECORD_PER_PAGE; ?>",
                "sZeroRecords": "<?php echo NOTHING_FOUND_SORRY; ?>",
                "sInfo": "<?php echo SHOWING; ?> _START_ to _END_ of _TOTAL_ <?php echo RECORD; ?>",
                "sInfoEmpty": "<?php echo SHOWING; ?> 0 to 0 of 0 <?php echo RECORD; ?>",
                "sSearch": "<?php echo SEARCH; ?>: ",
                'oPaginate': {

                    'sPrevious': '<?php echo PREVIOUS; ?>',
                    'sNext': '<?php echo NEXT; ?>'
                }
            }

        });


    });
</script>
