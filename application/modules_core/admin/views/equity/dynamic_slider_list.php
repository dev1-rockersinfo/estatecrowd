<script type="text/javascript" language="javascript">
    function delete_rec(id) {
        var ans = confirm("<?php echo ARE_YOU_SURE_TO_DELETE_IMAGE; ?>");
        if (ans) {
            location.href = "<?php echo site_url('admin/equity/delete_dynamic_slider/'); ?>" + "/" + id + "/";
        } else {
            return false;
        }
    }

</script>
<!-- BEGIN CONTAINER -->
<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php echo $this->load->view('admin_sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN THEME CUSTOMIZER-->
                    <div id="theme-change" class="hidden-phone">
                        <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text"><?php echo THEME; ?></span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
                    </div>
                    <!-- END THEME CUSTOMIZER-->
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        <?php echo MANAGE_SLIDER; ?>
                    </h3>
                    <ul class="breadcrumb">
                        <li>
                            <a href="<?php echo base_url() ?>admin/home/dashboard"><i class="icon-home"></i></a><span
                                class="divider">&nbsp;</span>
                        </li>

                        <li><a href="#"><?php echo MANAGE_SLIDER; ?></a><span class="divider-last">&nbsp;</span></li>
                    </ul>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <?php
            if ($msg != "") {
                if ($msg == "insert") {
                    ?>
                    <div class="alert alert-success">
                        <button class="close" data-dismiss="alert">x</button>
                        <strong><?php echo SUCCESS; ?>!</strong> <?php echo NEW_RECORD_HAS_BEEN_ADDED_SUCCESSFULLY; ?>.
                    </div>
                <?php
                }
                if ($msg == "update") {
                    ?>
                    <div class="alert alert-success">
                        <button class="close" data-dismiss="alert">x</button>
                        <strong><?php echo SUCCESS; ?>!</strong> <?php echo RECORD_HAS_BEEN_UPDATED_SUCCESSFULLY; ?>.
                    </div>
                <?php
                }
                if ($msg == "delete") {
                    ?>
                    <div class="alert alert-success">
                        <button class="close" data-dismiss="alert">x</button>
                        <strong><?php echo SUCCESS; ?>!</strong> <?php echo RECORD_HAS_BEEN_DELETED_SUCCESSFULLY; ?>.
                    </div>
                <?php
                }
            }
            ?>
            <!-- BEGIN ADVANCED TABLE widget-->
            <a href="<?php echo base_url(); ?>admin/equity/add_dynamic_slider" class="btn mini purple fr marB5"><i
                    class="icon-edit"></i> <?php echo ADD_SLIDER; ?> </a>

            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN EXAMPLE TABLE widget-->
                    <div class="widget">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i><?php echo MANAGE_SLIDER; ?></h4>
                            <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                            </span>
                        </div>
                        <div class="widget-body">
                            <p><a href="<?php echo site_url('admin/site_setting/slider_setting') ?>"
                                  target="_blank"><?php echo CLICK_HERE; ?> </a> <?php echo TO_GO_MANAGE_ORDER_OF_IMAGE; ?>
                            </p>
                            <?php
                            $attributes = array('name' => 'frm_listproject', 'id' => 'frm_listproject');
                            echo form_open_multipart('equity/add_dynamic_slider/', $attributes);
                            ?>
                            <input type="hidden" name="action" id="action"/>
                            <table class="table table-striped table-bordered admin" id="sample_1">
                                <thead>
                                <tr>
                                    <th><?php echo SLIDER_IMAGE; ?></th>
                                    <th><?php echo TITLE; ?></th>

                                    <th style="width:40%;"><?php echo BRIEF; ?></th>


                                    <th><?php echo STATUS; ?></th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php


                                if ($result) {
                                    foreach ($result as $row) {

                                        $dynamic_image_title = $row->dynamic_image_title;
                                        $dynamic_image_paragraph = $row->dynamic_image_paragraph;
                                        $dynamic_image_image = $row->dynamic_image_image;
                                        $color_picker = $row->color_picker;
                                        $color_picker_content = $row->color_picker_content;
                                        $dynamic_slider_id = $row->dynamic_slider_id;

                                        if ($row->active == "1") {
                                            $proj_cat_active = "Active";
                                        } else {
                                            $proj_cat_active = "Inactive";
                                        }



                                        ?>
                                        <tr class="odd gradeX">
                                            <td class="hidden-phone">
                                                <img
                                                    src="<?php echo site_url('upload/dynamic/' . $dynamic_image_image); ?> "
                                                    width="100" height="100"/></td>
                                            <td><?php echo $dynamic_image_title; ?></td>

                                            <td><?php echo $dynamic_image_paragraph; ?></td>


                                            <td><?php echo $proj_cat_active; ?></td>

                                            <td><?php echo anchor('admin/equity/edit_dynamic_slide/' . $row->dynamic_slider_id . '/', 'Edit'); ?>
                                                / <a href="#"
                                                     onClick="delete_rec('<?php echo $row->dynamic_slider_id; ?>')"><?php echo DELETE; ?></a>
                                            </td>
                                        </tr>
                                    <?php
                                    }
                                }
                                ?>

                                </tbody>
                            </table>
                            </form>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE widget-->
                </div>
            </div>

            <!-- END ADVANCED TABLE widget-->

            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>
<!-- END CONTAINER -->

<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/DT_bootstrap.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        App.init();
    });
</script>
