
<script src="<?php echo base_url(); ?>js/jquery.form.js"></script>
<!-- 
<script src="<?php echo base_url(); ?>js/jquery.pajinate.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#paging').pajinate({
            items_per_page: 100,
            abort_on_small_lists: true,
            item_container_id: '.paging',
            nav_panel_id: '.pro-pagination',

        });
    });
</script> -->
<?php

$campaign_name = $taxonomy_setting['project_name'];
$project_url = 'properties';


$site_name = $site_setting['site_name'];


$property_id = $campaign_detail[0]['property_id'];
$attributes = array(
    'id' => 'investor_deal_document_form',
    'name' => 'investor_deal_document_form',
    'enctype' => 'multipart/form-data',
    'class' => 'edit_project',
    'method' => 'post',
    'accept-charset' => 'UTF-8'
);
echo form_open_multipart('admin/equity/upload_contract_document/' . $property_id, $attributes);
?>
<div class="sr-only" id="investor_deal_document_image"></div>
</form>
<script type="text/javascript">

    function setaction(elename, actionval, actionmsg, formname) {


        if (confirm(actionmsg)) {


            if (actionval == 'declined') {

                var textarea = $("<textarea>").attr("name", "reason_decline").val($("#reason_decline_" + elename).val());

                if ($("#save_reason_" + elename).is(':checked')) {
                    var save_the_reason = 1;
                } else {
                    var save_the_reason = 0;
                }
                var input = $("<input>").attr("type", "hidden").attr("name", "save_the_reason").val(save_the_reason);
                var input_property_id = $("<input>").attr("type", "hidden").attr("name", "action_property_id").val(elename);
                $('#' + formname).append($(input_property_id));
                $('#' + formname).append($(textarea));
                $('#' + formname).append($(input));
            }
            if (actionval == 'inactive') {


                var inactive_mode = $("input[name=inactive_equity_mode_" + elename + "]:checked").val();
                var fund_inactive_value = $("input[name=fund_inactive_" + elename + "]:checked").val();


                var textarea_inactive = $("<textarea>").attr("name", "reason_inactive_hidden").val($("#reason_inactive_" + elename).val());


                if ($("#save_reason_inactive_" + elename).is(':checked')) {
                    var save_the_reason_inactive = 1;
                } else {
                    var save_the_reason_inactive = 0;
                }


                var input_inactive = $("<input>").attr("type", "hidden").attr("name", "save_the_reason_inactive").val(save_the_reason_inactive);
                var input_inactive_mode = $("<input>").attr("type", "hidden").attr("name", "inactive_mode").val(inactive_mode);
                var fund_inactive_note = $("<input>").attr("type", "hidden").attr("name", "fund_inactive_note").val(fund_inactive_value);
                var input_property_id = $("<input>").attr("type", "hidden").attr("name", "action_property_id").val(elename);
                $('#' + formname).append($(input_property_id));
                $('#' + formname).append($(textarea_inactive));
                $('#' + formname).append($(input_inactive));
                $('#' + formname).append($(fund_inactive_note));
                $('#' + formname).append($(input_inactive_mode));
            }

            if (actionval == 'update') {
                console.log(actionval);
                var cash_flow_investment = $("#cash_flow_investment").val();
                $('#' + formname).append($(cash_flow_investment));
            }
            document.getElementById('action').value = actionval;
            document.getElementById(formname).submit();
        }

    }


    function setoneaction(property_id, actionval, actionmsg, formname) {


        if (confirm(actionmsg)) {
            // document.getElementById('action').value=actionval;
            // document.getElementById(formname).submit();
            window.location = "<?php echo site_url('/admin/equity/action_equity')?>" + '/' + property_id + '/' + actionval + '/detail';
        }
    }

</script>
<?php



$cover_image = $campaign_detail[0]['cover_image'];
$user_id = $campaign_detail[0]['user_id'];

$profile_slug = $campaign_detail[0]['profile_slug'];
if ($profile_slug) {
    $slug = $campaign_detail[0]['profile_slug'];
} else {
    $slug = $campaign_detail[0]['user_id'];
}


$user_name = $campaign_detail[0]['user_name'];
$last_name = $campaign_detail[0]['last_name'];
$property_url = $campaign_detail[0]['property_url'];
$property_address = $campaign_detail[0]['property_address'];
$user_image = $campaign_detail[0]['image'];

$user_occupation = $campaign_detail[0]['user_occupation'];
$user_address = $campaign_detail[0]['address'];

$video_url = $campaign_detail[0]['video_url'];

$user_website = $campaign_detail[0]['user_website'];
$user_address = $campaign_detail[0]['address'];

$property_url_site = site_url($project_url . '/' . $property_url);


$min_investment_amount = $campaign_detail[0]['min_investment_amount'];

$equity_currency_symbol = set_currency(null,'', 'yes');

$get_percentage = GetCampaignPercentage($data = array('min_investment_amount' => $campaign_detail[0]['min_investment_amount'], 'amount_get' => $campaign_detail[0]['amount_get']));
$amount_get = $campaign_detail[0]['amount_get'];
$end_date_org = $campaign_detail[0]['investment_closing_date'];
$end_date = dateformat($campaign_detail[0]['investment_closing_date']);
$date_added = dateformat($campaign_detail[0]['date_added']);

$user_id = $campaign_detail[0]['user_id'];
$status = $campaign_detail[0]['status'];


$email = $campaign_detail[0]['email'];

$contract_copy_file = $campaign_detail[0]['contract_copy_file'];
$status = status_name($campaign_detail[0]['status']);
$status_class = '';

if ($campaign_detail[0]['status'] == 2 || $campaign_detail[0]['status'] == 3 || $campaign_detail[0]['status'] == 4) {
    $status_class = 'label-success';
}
if ($campaign_detail[0]['status'] == 6) {
    $status_class = 'label-warning';
}
if ($campaign_detail[0]['status'] == 7 || $campaign_detail[0]['status'] == 8 || $campaign_detail[0]['status'] == 5) {
    $status_class = 'label-danger';
}

$min_property_investment_range = set_currency($campaign_detail[0]['min_property_investment_range']);
$max_property_investment_range = set_currency($campaign_detail[0]['max_property_investment_range']);
$campaign_units = $campaign_detail[0]['campaign_units'];
$share_available = $campaign_units - $campaign_detail[0]['campaign_units_get'];
$investment_close_date = $campaign_detail[0]['investment_close_date'];
$date_added = $campaign_detail[0]['date_added'];

$bedrooms = $campaign_detail[0]['bedrooms'];
$bathrooms = $campaign_detail[0]['bathrooms'];
$cars = $campaign_detail[0]['cars'];
$property_type =  $campaign_detail[0]['property_type'];


$property_sub_type =  $campaign_detail[0]['property_sub_type'];

?>
<!-- BEGIN CONTAINER -->


<div id="container" class="row-fluid">


<!-- BEGIN SIDEBAR -->



<?php echo $this->load->view('admin_sidebar'); ?>



<!-- END SIDEBAR -->


<!-- BEGIN PAGE -->


<!-- BEGIN PAGE -->

<div id="main-content">


<!-- BEGIN PAGE CONTAINER-->


<div class="container-fluid">


<!-- BEGIN PAGE HEADER-->


<div class="row-fluid">


    <div class="span12">


        <!-- BEGIN THEME CUSTOMIZER-->


        <div id="theme-change" class="hidden-phone">


            <i class="icon-cogs"></i>



                            <span class="settings">



                            <span class="text"><?php echo THEME ?>:</span>



                            <span class="colors">



                                <span class="color-default" data-style="default"></span>



                            <span class="color-gray" data-style="gray"></span>



                            <span class="color-purple" data-style="purple"></span>



                            <span class="color-navy-blue" data-style="navy-blue"></span>



                            </span>



                            </span>


        </div>


        <!-- END THEME CUSTOMIZER-->


        <!-- BEGIN PAGE TITLE & BREADCRUMB-->


        <h3 class="page-title">


            <?php echo $property_address; ?>


        </h3>


        <ul class="breadcrumb">


            <li>


                <a href="<?php echo site_url('admin/home/dashboard'); ?>"><i class="icon-home"></i></a><span
                    class="divider">&nbsp;</span>


            </li>


            <li><a href="#"><?php echo EQUITY_OVERVIEW; ?></a><span class="divider-last">&nbsp;</span></li>


        </ul>


        <!-- END PAGE TITLE & BREADCRUMB-->


    </div>


</div>



<?php



if ($msg) {

    $project_title_error = $property_address;
    if ($msg == 'delete') {
        ?>
        <div class="alert alert-success">
            <button class="close" data-dismiss="alert">x</button>
            <strong><?php echo SUCCESS; ?>!</strong>
            "<?php echo $project_title_error; ?>"</span> <?php echo HAS_BEEN_DELETED_SUCCESSFULLY; ?>.
        </div>

    <?php
    }
    if ($msg == 'cannot_delete_active') {
        ?>
        <div class="alert alert-error">
            <button class="close" data-dismiss="alert">x</button>
            <strong><?php echo ERRORS; ?>!</strong> <?php echo YOU_CAN_NOT_DELETE_ACTIVE_PROJECT; ?>
            "<?php echo $project_title_error; ?>".
        </div>

    <?php
    }
    if ($msg == 'no_delete') {
        ?>
        <div class="msgdisp" style="color:#FF0000;"><?php echo YOU_CAN_NOT_DELETE_ACTIVE_PROJECT; ?>
            "<?php echo $project_title_error; ?>".
        </div>

    <?php
    }
    if ($msg == 'active') {
        ?>
        <div class="alert alert-success">
            <button class="close" data-dismiss="alert">x</button>
            <strong><?php echo SUCCESS; ?>!</strong>"<?php echo $project_title_error; ?>
            " <?php echo HAS_BEEN_ACTIVATED_SUCCESSFULLY; ?>.
        </div>

    <?php
    }
    if ($msg == 'approve') {
        ?>
        <div class="alert alert-success">
            <button class="close" data-dismiss="alert">x</button>
            <strong><?php echo SUCCESS; ?>!</strong>"<?php echo $project_title_error; ?>
            " <?php echo HAS_BEEN_APPROVED_SUCCESSFULLY; ?>.
        </div>

    <?php
    }
    if ($msg == 'cannot_active_expired') {
        ?>
        <div class="alert alert-error">
            <button class="close" data-dismiss="alert">x</button>
            <strong><?php echo ERRORS; ?>!</strong><?php echo YOU_CAN_NOT_ACTIVE_EXPIRED_OR_ALREADY_ACTIVE; ?>
            "<?php echo $project_title_error; ?>".
        </div>
    <?php
    }
 if ($msg == 'cannot_approve_contract') {
           ?>
           <div class="alert alert-error">
               <button class="close" data-dismiss="alert">x</button>
               <strong><?php echo ERRORS; ?>!</strong>
               <?php
                   $project_link = '<a href="' . site_url('admin/campaign/campaign_detail/'.$project_title_error) . '">' . THIS_LINK . '</a>';
                   echo sprintf(BEFORE_YOU_CAN_APPROVE_THIS_PROJECT,$project_link); ?>
               "<?php echo $project_title_error; ?>".
           </div>
       <?php
       }
 
    if ($msg == 'inactive') {
        ?>
        <div class="alert alert-success">
            <button class="close" data-dismiss="alert">x</button>
            <strong><?php echo SUCCESS; ?>!</strong>"<?php echo $project_title_error; ?>
            " <?php echo HAS_BEEN_INACTIVATED_SUCCESSFULLY; ?>.
        </div>
    <?php
    }
    if ($msg == 'cannot_inactive') {
        ?>
        <div class="alert alert-error">
            <button class="close" data-dismiss="alert">x</button>
            <strong><?php echo ERRORS; ?>!</strong><?php echo YOU_CAN_NOT_INACTIVE_EXPIRED_OR_ALREADY_ACTIVE; ?>
            "<?php echo $project_title_error; ?>".
        </div>
    <?php
    }
    if ($msg == 'declined') {
        ?>
        <div class="alert alert-success">
            <button class="close" data-dismiss="alert">x</button>
            <strong><?php echo SUCCESS; ?>!</strong>"<?php echo $project_title_error; ?>
            " <?php echo HAS_BEEN_DECLIENED_SUCCESSFULLY; ?>.
        </div>

    <?php
    }
    if ($msg == 'declined_error') {
        ?>
        <div class="alert alert-error">
            <button class="close" data-dismiss="alert">x</button>
            <strong><?php echo ERRORS; ?>!</strong>"<?php echo $project_title_error; ?>
            " <?php echo CANNOT_BE_DECLIENED; ?>.
        </div>



    <?php
    }
    if ($msg == 'feature') {
        ?>
        <div class="alert alert-success">

            <button class="close" data-dismiss="alert">x</button>
            <strong><?php echo SUCCESS; ?>!</strong>"<?php echo $project_title_error; ?>
            " <?php echo HAS_BEEN_FEATURED_SUCCESSFULLY; ?>.
        </div>

    <?php
    }
    if ($msg == 'set_featured_active') {
        ?>
        <div class="alert alert-success">

            <button class="close" data-dismiss="alert">x</button>
            <strong><?php echo SUCCESS; ?>!</strong>"<?php echo $project_title_error; ?>
            " <?php echo HAS_BEEN_ACTIVATED_AND_SET_FEATURED_SUCCESSFULLY; ?>.
        </div>

    <?php
    }
    if ($msg == 'feature_cancel') {
        ?>
        <div class="alert alert-error">
            <button class="close" data-dismiss="alert">x</button>
            <strong><?php echo ERRORS; ?>!</strong>"<?php echo $project_title_error; ?>
            " <?php echo CANNOT_BE_SET_TO_FEATURED_AS_IT_IS_EXPIRED_SUCCESSFUL_FAILURE; ?>.
        </div>

    <?php
    }
    if ($msg == 'not_feature') {
        ?>
        <div class="alert alert-success">

            <button class="close" data-dismiss="alert">x</button>
            <strong><?php echo SUCCESS; ?>!</strong>"<?php echo $project_title_error; ?>
            " <?php echo HAS_BEED_REMOVE_FROM_FEATURED_SUCCESSFULLY; ?>.
        </div>

    <?php
    }
    if ($msg == "insert") {
        ?>
        <div class="alert alert-success">
            <button class="close" data-dismiss="alert">x</button>
            <strong><?php echo SUCCESS; ?>!</strong><?php echo NEWS; ?> "<?php echo $project_title_error; ?>
            " <?php echo HAS_BEEDN_ADDED_SUCCESSFULLY; ?>.
        </div>
    <?php
    }
    if ($msg == "update") {
        ?>
        <div class="alert alert-success">
            <button class="close" data-dismiss="alert">x</button>
            <strong><?php echo SUCCESS; ?>!</strong>"<?php echo $project_title_error; ?>
            " <?php echo HAS_BEEDN_UPDATED_SUCCESSFULLY; ?>.
        </div>
    <?php
    }
   
    if ($msg == 'update_cash_flow') {
        ?>
        <div class="alert alert-success">

            <button class="close" data-dismiss="alert">x</button>
            <strong><?php echo SUCCESS; ?>!</strong>"<?php echo $project_title_error; ?>
            " <?php echo HAS_BEEN_UPDATED_SUCCESSFULLY; ?>.
        </div>

    <?php
    }

}




?>
<div class="alert alert-success show_success" style="display:none">
    <button class="close" data-dismiss="alert">x</button>
    <strong><?php echo SUCCESS; ?>!</strong>
    <?php echo RECORDS_HAS_BEEN_UPDATED_SUCCESSFULLY; ?>.
</div>

<!-- END PAGE HEADER-->


<!-- BEGIN ADVANCED TABLE widget-->


<div class="row-fluid">


<div class="span12">


<!-- BEGIN EXAMPLE TABLE widget-->


<div class="widget widget-tabs widget-tabs-left">
<div class="widget-title">

</div>
<div class="widget-body1">
<div class="tabbable portlet-tabs">
<ul class="nav nav-tabs">
    <li class="active"><a href="#overview" data-toggle="tab"><?php echo OVERVIEW; ?></a></li>
   <!--  <li class=""><a href="#activity" data-toggle="tab"><?php echo ACTIVITY; ?></a></li> -->
    <li class=""><a href="#investors" data-toggle="tab"><?php echo INVESTORS; ?></a></li>
</ul>
<div class="tab-content">
<div class="tab-pane active" id="overview">
<div class="widget-body">
<div class="span3">
    <div class="text-center profile-pic">
        <?php
        if ($cover_image != '' && is_file("upload/property/small/" . $cover_image)) {
            echo '<img src="' . base_url() . 'upload/property/small/' . $cover_image . '" alt="" class="img-thumbnail" title="' . ucfirst($property_address) . '"/>';
        } else {
            echo '<img  src="' . base_url() . 'upload/property/small/no_img.jpg" class="img-thumbnail" title="' . ucfirst($property_address) . '"/>';
        }
        ?>
    </div>
    <ul class="pro-overview-sidemenu">
     
        <li><a href="<?php echo site_url($project_url . '/' . $property_url . '#investment'); ?>"
               target="_blank"><?php echo INVESTMENT; ?></a></li>
        <?php if ($updates) { ?>
            <li><a href="<?php echo site_url($project_url . '/' . $property_url . '#updates-tab'); ?>"
                   target="_blank"><?php echo 'Updates'; ?></a></li>
        <?php } ?>
        
            <li><a href="<?php echo site_url($project_url . '/' . $property_url . '#investor'); ?>"
                   target="_blank"><?php echo 'Investor'; ?></a></li>
      
        <?php if ($comments) { ?>
            <li><a href="<?php echo site_url($project_url . '/' . $property_url . '#comment'); ?>"
                   target="_blank"><?php echo COMMENTS; ?></a></li>
        <?php } ?>
        

        <li><a href="<?php echo site_url($project_url . '/' . $property_url . '#attachments'); ?>"
               target="_blank"><?php echo 'Attachments'; ?></a></li>

    </ul>
</div>
<div class="span6">
    <h3><?php echo $property_address; ?> <span class="label <?php echo $status_class; ?>"><?php echo $status; ?></span>
        <small><a href="<?php echo site_url($project_url . '/' . $property_url); ?>" target="_blank"><i
                    class="icon-external-link"></i></a></small>
    </h3>
    <table class="table table-borderless">
        <tbody>
        <tr>
            <td class="span3"><?php echo 'bedrooms'; ?> :</td>
            <td>
                <?php echo $bedrooms; ?>
            </td>
        </tr>
        <tr>
            <td class="span3"><?php echo 'bathrooms'; ?> :</td>
            <td>
                <?php echo $bathrooms; ?>
            </td>
        </tr>
        <tr>
            <td class="span3"><?php echo 'cars'; ?> :</td>
            <td>
                <?php echo $cars; ?>
            </td>
        </tr>
        <tr>
            <td class="span3"><?php echo 'Mixed-Use'; ?> :</td>
            <td>
                <?php echo $property_sub_type;?>, <?php echo $property_type;?>
            </td>
        </tr>
        <tr>
            <td class="span3"><?php echo 'Location'; ?> :</td>
            <td>
                <a href="http://maps.google.com/?q=<?php echo $property_address; ?>" target="_blank"><span>View Map</span></a>
            </td>
        </tr>
       
        </tbody>
    </table>
   
   

</div>

<div class="span3">
<h4><?php echo ACTIONS; ?></h4>
<?php



$attributes = array('name' => 'frm_listproject', 'id' => 'frm_listproject', 'method' => 'post');
echo form_open('admin/campaign/action_campaign/', $attributes);
?>
<input type="hidden" name="action" id="action"/>


<div class="actions">
    <a href="<?php echo site_url('admin/campaign/edit_campaign/'.$property_id);?>" role="button" class="item btnView tooltips"
                       data-placement="bottom" data-original-title="<?php echo EDIT; ?>" data-toggle="modal" target="_blank"><i
                            class="icon-edit"></i></a>
     
    <a href="<?php echo base_url() . 'admin/message/view_message/' . $property_id . '/' . $user_id ; ?>"
       class="item btnMessage tooltips" data-placement="bottom" data-original-title="Send message"><i
            class="icon-envelope"></i></a>

   
    <?php if ($campaign_detail[0]['status'] == 1 || $campaign_detail[0]['status'] == 6) { ?>

        <a href="javascript://" class="item btnDelete tooltips" data-placement="bottom"
           data-original-title="<?php echo DELETE; ?>"
           onclick="setoneaction(<?php echo $property_id; ?>,'delete', '<?php echo ARE_YOU_SURE_YOU_WANT_TO_DELETE_SELECTED_PROJECT; ?>', 'frm_listproject')"><i
                class="icon-trash"></i></a>

    <?php } else { ?>
        <a href="javascript://" class="item btnDelete tooltips disable" data-placement="bottom"
           data-original-title="<?php echo CANT_DELETE; ?>"><i class="icon-trash"></i></a>
    <?php } ?>

    <!-- Inactive and active-->
    <?php if ($campaign_detail[0]['status'] == 2) { ?>

        <a href="#inactive-<?php echo $property_id; ?>" role="button" class="item btnApprove tooltips"
           data-placement="bottom" data-original-title="<?php echo INACTIVE_THIS; ?>" data-toggle="modal"><i
                class="icon-ban-circle"></i></a>

    <?php
    } else if ($campaign_detail[0]['status'] == 1) {
        ?>
        <a href="javascript://" class="item btnApprove tooltips" data-placement="bottom"
           data-original-title="<?php echo APPROVE; ?>"
           onclick="setoneaction(<?php echo $property_id; ?>,'approve', '<?php echo ARE_YOU_SURE_YOU_WANT_TO_APPROVE_SELECTED_PROJECT; ?>', 'frm_listproject')"><i
                class="icon-ok"></i></a>

    <?php
    } else if ($campaign_detail[0]['status'] == 7 || $campaign_detail[0]['status'] == 8) {
        ?>
        <a href="javascript://" class="item btnApprove tooltips" data-placement="bottom"
           data-original-title="<?php echo ACTIVE; ?>"
           onclick="setoneaction(<?php echo $property_id; ?>,'active', '<?php echo ARE_YOU_SURE_YOU_WANT_TO_ACTIVE_SELECTED_PROJECT; ?>', 'frm_listproject')"><i
                class="icon-ok"></i></a>
    <?php } else { ?>

    <?php } ?>

    <!-- Decline -->
    <?php if ($campaign_detail[0]['status'] == 1) { ?>
        <a href="#decline-<?php echo $property_id; ?>" role="button" class="item btnDecline tooltips"
           data-placement="bottom" data-original-title="<?php echo DECLINE; ?>" data-toggle="modal"><i
                class="icon-remove"></i></a>
    <?php } ?>


    <div id="decline-<?php echo $property_id; ?>" class="modal hide fade" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel1" aria-hidden="true">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel1"><?php echo DECLINE; ?></h3>
        </div>
        <div class="modal-body">
            <p><?php echo sprintf(AS_YOU_HAVE_TO_DECIDED_TO_DECLINE, $campaign_name, $campaign_name, $user_name . ' ' . $last_name, $campaign_name) ?></p>

            <div class="reasonArea">
                <p> <?php echo REASON_TO_DECLINE_THIS . ' ' . $campaign_name; ?></p>
                <textarea rows="3" name="reason_decline_equity"
                          id="reason_decline_<?php echo $property_id; ?>"></textarea>
                <label><input type="checkbox" class="checkboxes" id="save_reason_<?php echo $property_id; ?>"
                              name="save_the_reason" value="1"/>
                    <?php echo SAVE_THIS_REASON_TO_USE_ON_OTHER . ' ' . $campaign_name ?></label>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true"><?php echo CANCEL; ?></button>
            <button class="btn btn-primary"
                    onclick="setaction(<?php echo $property_id; ?>,'declined', '<?php echo ARE_YOU_SURE_YOU_WANT_TO_DECLINE_SELECTED_PROJECT; ?>', 'frm_listproject')"><?php echo SAVE; ?></button>
        </div>
    </div>
    <div id="inactive-<?php echo $property_id; ?>" class="modal hide fade" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel1"><?php echo INACTIVE; ?></h3>
        </div>
        <div class="modal-body">
            <p><?php echo sprintf(AS_YOU_HAVE_TO_DECIDED_TO_INACTIVE, $campaign_name); ?></p>

            <div class="inactivepopup">
                <p>
                    <label class="radio">
                        <input type="radio" name="inactive_equity_mode_<?php echo $property_id; ?>"
                               id="inactive_mode_<?php echo $property_id; ?>" value="8"/>
                        <span class="redioText"><strong><?php echo HIDDEN_MODE; ?>
                                :</strong> <?php echo sprintf(SELECT_THIS_OPTION_IF_YOU_WANT_TO_HIDE, $campaign_name, $campaign_name); ?></span>
                    </label>
                </p>

                <p>
                    <label class="radio">
                        <input type="radio" name="inactive_equity_mode_<?php echo $property_id; ?>"
                               id="inactive_mode_<?php echo $property_id; ?>" value="7"/>
                        <span class="redioText"><strong><?php echo INACTIVE_MODE; ?>
                                :</strong> <?php echo sprintf(SELECT_THIS_OPTION_IF_YOU_WANT_TO_SHOW, $campaign_name, $campaign_name, $updates, $funds, $comments); ?></span>
                    </label>
                </p>

                <div class="reasonArea">
                    <p><?php echo WRITE_REASON_WHY_YOU_DEACTIVATED_THIS . ' ' . $campaign_name; ?></p>
                    <textarea rows="3" name="reason_inactive_equity"
                              id="reason_inactive_<?php echo $property_id; ?>"></textarea>
                    <label><input type="checkbox" class="checkboxes" id="save_reason_inactive_<?php echo $property_id; ?>"
                                  name="save_the_reason_inactive"
                                  value="1"/> <?php echo SAVE_THIS_REASON_TO_USE_ON_OTHER . ' ' . $campaign_name; ?>
                    </label>
                </div>
                <!--
                <p class="marT15">
                    <label class="radio">
                     <input type="radio" name="optionsRadios2" value="option2" />
                        <span class="redioText"><strong>FundBack All:</strong> Select this option if you want to fund back all funders in their wallet or in thier payment gateway(paypal, stripe or other) accounts. ONCE FUNDBACK EXECUTED, IT CAN NOT BE UNDO.</span>
                     </label>
                </p>
                <p>
                    <label class="radio">
                     <input type="radio" name="optionsRadios2" value="option2" />
                        <span class="redioText"><strong>No FundBack:</strong> Select this option if you are deactivating this {campaign-dynmanic-name} temporarily. This option is useful if you want to resolve your doubts with {project-owner-dynamic-name} of this {campaign-dynmanic-name} so once your questions and doubts resolved you can make this {campaign-dynmanic-name} active again.</span>
                     </label>
                </p>
                -->
                <p> <?php echo sprintf(PLEASE_NOTE_IF_ONCE_IS_INACTIVATED, $campaign_name, $funds); ?></p>

                <p><strong><?php echo sprintf(IF_MANAGED_MANUALLY_BY_TEAM, $site_name); ?></strong></p>

                <p class="marT15">
                    <label class="radio">
                        <input type="radio" name="fund_inactive_<?php echo $property_id; ?>"
                               id="fund_inactive_<?php echo $property_id; ?>" value="1"/>
                        <span
                            class="redioText"> <?php echo sprintf(YOU_CAN_HOLD_ALL_UNTILL_YOU_RESOLVE_YOUR_DOUBTS_AND_REASONS, $funds, $campaign_name, $user_name . ' ' . $last_name); ?></span>
                    </label>
                </p>

                <p><strong>OR</strong></p>

                <p>
                    <label class="radio">
                        <input type="radio" name="fund_inactive_<?php echo $property_id; ?>"
                               id="fund_inactive_<?php echo $property_id; ?>" value="0"/>
                        <span
                            class="redioText"><?php echo sprintf(YOU_CAN_REFUND_ALL_FUND, $funds, $funds, $funds, $site_name); ?></span>
                    </label>
                </p>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true"><?php echo CANCEL; ?></button>
            <button class="btn btn-primary"
                    onclick="setaction(<?php echo $property_id; ?>,'inactive', '<?php echo ARE_YOU_SURE_YOU_WANT_TO_INACTIVE_SELECTED_RECORD; ?>', 'frm_listproject')"><?php echo SAVE; ?></button>
        </div>
    </div>






</div>
<input type="hidden" name="property_id_update" value="<?php echo $property_id?>"/>
</form>
<h4><?php echo CREATE_BY; ?></h4>
<?php
if ($user_image != '' && is_file("upload/user/user_small_image/" . $user_image)) {
    $user_image_src = base_url() . 'upload/user/user_small_image/' . $user_image;
} else {
    $user_image_src = base_url() . 'upload/user/user_small_image/no_man.jpg';
}

?>
<div class="pro-created-by">
    <div class="avtar">
        <img src="<?php echo $user_image_src; ?>">
    </div>
    <div class="item-media">
        <h3><a href="<?php echo site_url('user/' . $slug); ?>"
               target="_blank"><?php echo $user_name . ' ' . $last_name; ?></a></h3>
        <a href="mailto://<?php echo $email; ?>"><?php echo $email; ?></a>
    </div>
</div>

<h4><?php echo RAISE_DETAILS; ?></h4>
<table class="table table-borderless">
    <tbody>
    <tr>
        <td class="span3"><?php echo 'Investment Target'; ?> :</td>
        <td>
            <?php echo set_currency($min_investment_amount); ?>
        </td>
    </tr>
    <tr>
        <td class="span3"><?php echo 'Minimum Investment'; ?> :</td>
        <td>
            <?php echo set_currency($min_property_investment_range); ?>
        </td>
    </tr>
   
    <tr>
        <td class="span3"><?php echo 'Total Shares'; ?> :</td>
        <td>
            <?php echo $campaign_units; ?>
        </td>
    </tr>
  
   
    <tr>
        <td class="span3"><?php echo 'Shares Available'; ?> :</td>
        <td>
            <?php echo $share_available; ?>
        </td>
    </tr>
    <tr>
        <td class="span3"><?php echo 'Open Date'; ?> :</td>
        <td>
            <?php echo date("jS F, Y", strtotime($date_added));?>
        </td>
    </tr>

     <tr>
        <td class="span3"><?php echo 'Closing Date'; ?> :</td>
        <td>
           <?php echo date("jS M, Y", strtotime($investment_close_date));?>
        </td>
    </tr>

   
    </tbody>
</table>
</div>
<div class="space5"></div>
</div>
</div>
<!-- <div class="tab-pane" id="activity">
    <div class="widget-body" id="paging">
        <div class="timeline-messages paging">
          
            <?php
            if ($activities) {
                foreach ($activities as $equity_activity) {

                    $activity_action_detail = $equity_activity['action_detail'];
                    $activity_profile_slug = $equity_activity['profile_slug'];
                    $activity_user_name = $equity_activity['user_name'];
                    $activity_last_name = $equity_activity['last_name'];
                    $activity_image = $equity_activity['image'];
                    $activity_datetime = $equity_activity['datetime'];
                    $search_you = 'You have';
                    $replace_you = '<a href="' . site_url('user/' . $slug) . '" target="_blank">' . $user_name . ' ' . $last_name . '</a> has';
                    $activity_action_detail = str_replace($search_you, $replace_you, $activity_action_detail);

                    $search_you = 'Your';
                    $replace_you = '<a href="' . site_url('user/' . $slug) . '" target="_blank">' . $user_name . ' ' . $last_name . '</a>`s';
                    $activity_action_detail = str_replace($search_you, $replace_you, $activity_action_detail);


                    if ($activity_image != '' && is_file("upload/user/user_small_image/" . $activity_image)) {
                        $activity_user_image_src = base_url() . 'upload/user/user_small_image/' . $activity_image;
                    } else {
                        $activity_user_image_src = base_url() . 'upload/user/user_small_image/no_man.jpg';
                    }


                    ?>
                    <div class="msg-time-chat">
                        <a href="<?php echo site_url('user/' . $activity_profile_slug); ?>" class="message-img">
                            <img class="avatar" src="<?php echo $activity_user_image_src; ?>" alt=""></a>

                        <div class="message-body">
                            <div class="text">
                                <p class="attribution"><?php echo $activity_action_detail; ?></p>

                                <p>
                                    <a href="<?php echo site_url($project_url . '/' . $property_url); ?>"><?php echo $property_address; ?></a>
                                </p>

                                <p class="small"><?php echo getDuration($activity_datetime); ?></p>
                            </div>
                        </div>
                    </div>
                <?php
                }
            }
            ?>
         

        </div>
        <div class="pro-pagination"></div>

    </div>
</div> -->
<?php  $url_project_title=$campaign_detail[0]['property_url']; ?>
<div class="tab-pane" id="investors">
 <div class="widget-body">
   <?php if ($_GET) {
    $get_status = $this->input->get('get_status');
    $user_name = $this->input->get('user_name');
   
    $trans_id = $this->input->get('trans_id');
    $from_date = $this->input->get('from_date');
    $to_date = $this->input->get('to_date');
    $min_amount=$this->input->get('min_amount');
    $max_amount=$this->input->get('max_amount')   ;

} else {
    $get_status = '';
    $user_name = '';
   
    $trans_id = '';
    $from_date = '';
    $to_date = '';
    $min_amount='';
    $max_amount='';
}

?>
<div class="search-section pLeftRight10">
    <form action="" method="get">
        <div class="filter-area">
            <div class="filter-main">
                
                <div class="searchProject-area">
                      <input type="text" name="user_name" placeholder="<?php echo BY.' '.USER;?>" value="<?php echo $user_name; ?>">
                </div>
               <div class="searchProject-area">
                      <input type="number" name="min_amount" min='0' placeholder="<?php echo MIN.' '.AMOUNT;?>" value="<?php echo $min_amount; ?>">
                </div>
                <div class="searchProject-area">
                      <input type="number" name="max_amount" min='0' placeholder="<?php echo MAX.' '.AMOUNT;?>" value="<?php echo $max_amount; ?>">
                </div>
               <div class="searchProject-area">
                      <input type="text" name="trans_id" placeholder="<?php echo BY.' '.TRANSACTION_ID;?> " value="<?php echo $trans_id; ?>">
                </div>
                <!--  <div class="filter-category filter-item ">
                    <select name="get_status">
                        <option value=""><?php echo STATUS; ?></option>
                        
                                <option value="ON_HOLD" <?php if ($get_status == 'ON_HOLD') {echo 'selected="selected"';} ?> ><?php echo PENDING;?></option>
                                <option value="FAIL" <?php if ($get_status == 'FAIL') {echo 'selected="selected"';} ?>><?php echo FAIL;?></option>
                                <option value="SUCCESS" <?php if ($get_status == 'SUCCESS') {echo 'selected="selected"';} ?>><?php echo SUCCESS;?></option>


                    </select>
                </div> -->
            <div class="searchProject-area">             
                <div id="reportrange" class="date-range-style pull-right">
                    <i class="icon-calendar"></i>&nbsp;
                    <span></span> <b class="caret"></b>
                </div>
                 <input type="hidden" name="from_date" id="from_date"  value="<?php echo $from_date; ?>">
                  <input type="hidden" name="to_date" id="to_date"  value="<?php echo $to_date; ?>">
             </div>             
                <div class="filter-item">
                    <input type="submit" class="filter-btn" value="<?php echo SEARCH; ?>">
                </div>
                <div class="filter-item">
                    <a  class="btn mini purple"  href="<?php echo site_url('admin/equity/funder_download/'.$url_project_title.'?get_status='.$get_status.'&user_name='.$user_name.'&min_amount='.$min_amount.'&max_amount='.$max_amount.'&trans_id='.$trans_id.'&from_date='.$from_date.'&to_date='.$to_date);?>" target="_blank"><i class="icon-download-alt"></i> <?php echo 'Export'; ?></a>
                </div>
            </div>
        </div>
    </form>
    <?php  if ($get_status!='' ||$user_name != '' ||  $trans_id != '' || $from_date!=''  ||$max_amount!='' ||$min_amount!='' || $to_date!='') {

          $array = array('ON_HOLD' => PENDING,
                        'FAIL' => FAIL,
                        'SUCCESS' => SUCCESS
                    );
        ?>

        <div class="pill-container">
            <div id="filterPill">

                <?php if ($this->input->get('user_name') != '') { ?>
                    <a class="filter-pill" id="user_name_tag"><?php echo $this->input->get('user_name'); ?>
                        <span class="icon-remove"></span></a>
                <?php } ?>

                 <?php if ($this->input->get('min_amount')) { ?>
                    <a class="filter-pill" id="min_amount_tag"><?php echo $min_amount; ?> <span
                            class="icon-remove"></span></a>
                <?php } ?>

                <?php if ($this->input->get('max_amount')) { ?>
                    <a class="filter-pill" id="max_amount_tag"><?php echo $max_amount; ?> <span
                            class="icon-remove"></span></a>
                <?php } ?>

                <?php if ($this->input->get('trans_id')) { ?>
                    <a class="filter-pill" id="trans_id_tag"><?php echo $this->input->get('trans_id'); ?> <span
                            class="icon-remove"></span></a>
                <?php } ?>
               <?php if ($this->input->get('get_status') && isset($array[$this->input->get('get_status')])) { ?>
                    <a class="filter-pill" id="get_status_tag"><?php echo $array[$this->input->get('get_status')]; ?> <span
                            class="icon-remove"></span></a>
                <?php } ?> 
                 <?php if ($this->input->get('from_date')) { ?>
                    <a class="filter-pill" id="from_date_tag"><?php echo $this->input->get('from_date').' - '.$this->input->get('to_date'); ?> <span
                            class="icon-remove"></span></a>
                <?php } ?>
                 
                
            </div>
            <a class="filter-clear btn" id="filterClear"><?php echo CLEAR_ALL_FILTERS; ?></a>
        </div>
    <?php } ?>
</div>
   
        <table class="table table-striped table-bordered width100I" id="sample_new">

            <thead>
            <tr>
                <th class="mob_hidden"><?php echo NOS;?></th>
                <th><?php echo INVESTOR;?></th>
                 <th class="mob_hidden"><?php echo AMOUNT ?></th>
                <th class="mob_hidden"><?php echo ACCREDIATION_STATUS;?></th>
                <th class="mob_hidden"><?php echo INVESTMENT_STATUS;?></th>
                <th class="mob_hidden"><?php echo TRANSACTION_ID;?></th>
                <th class="hidden-phone"><?php echo DATE; ?></th>
                <th><?php echo ACTION;?></th>
            </tr>
            </thead>

            <tbody>
            <?php
            if ($investors) {
                $i = 1;
                foreach ($investors as $investor) {


                    $investor_user_name = $investor['user_name'] . ' ' . $investor['last_name'];
                    $invest_status_id = $investor['invest_status_id'];
                    $property_id = $investor['property_id'];
                    $investor_user_id = $investor['user_id'];
                    $in_process_id = $investor['id'];
                    $donation_amount = $investor['amount'];
                    $investor_profile_slug = $investor['profile_slug'];
                    $investor_accreditation_status = $investor['accreditation_status'];
                    $investor_date_time=$investor['transaction_date_time'];
                    $investor_image = $investor['image'];
                    if ($investor_image != '' && is_file("upload/user/user_small_image/" . $investor_image)) {
                        $investor_image_src = base_url() . 'upload/user/user_small_image/' . $investor_image;
                    } else {
                        $investor_image_src = base_url() . 'upload/user/user_small_image/no_man.jpg';
                    }
                    $preapproval_key = $investor['preapproval_key'];

                    if ($invest_status_id == 1) {

                        $status = PROCESSING_CONTRACT;

                    } elseif ($invest_status_id == 2) {

                        $status = DOWNLOADED_CONTRACT;
                    } elseif ($invest_status_id == 3) {

                        $status = UPLOADED_SIGNED_CONTRACT;
                    } elseif ($invest_status_id == 4) {

                        $status = SIGNED_CONTRACT_APPROVE;
                    } elseif ($invest_status_id == 5) {

                        $status = PAYMENT_PROCESS;
                    } elseif ($invest_status_id == 6) {

                        $status = PAYMENT_RECIEPT_UPLOADED;
                    } elseif ($invest_status_id == 7) {

                        $status = PAYMENT_CONFIRMED;
                    } elseif ($invest_status_id == 8) {

                        $status = TRACKING_SHIPMENT;
                    } elseif ($invest_status_id == 9) {

                        $status = DOCUMENT_REJECTED;
                    } elseif ($invest_status_id == 10) {

                        $status = TRACKING_SHIPMENT_CONFIRMED;
                    } else {

                        $status = TRACKING_SHIPMENT;

                    } ?>
                    <tr class="odd gradeX">
                        <td class="mob_hidden"><?php echo $i; ?></td>
                        <td><a href="<?php echo site_url('user/' . $investor_profile_slug) ?>" target="_blank">
                                          <span class="creater fl">
                                          <img src="<?php echo $investor_image_src; ?>">
                                          </span>
                                <span class="creater-name"><?php echo $investor_user_name; ?></span></a></td>
                        <td class="mob_hidden">
                            <?php echo set_currency($donation_amount) ?>
                        </td>
                        <td class="mob_hidden">
                            <?php if ($investor_accreditation_status == 1) { ?>
                                <span class="label label-success"><?php echo ACCREDITED; ?></span>
                            <?php } else { ?>

                                <span class="label"><?php echo NOT_ACCREDITED; ?></span>
                            <?php } ?>
                        </td>
                        <td class="mob_hidden"><?php echo $status; ?></td>
                           <td class="mob_hidden"><?php echo $preapproval_key; ?></td>
                        <td class="mob_hidden"><?php echo date('d-M-Y',strtotime($investor_date_time)); ?></td>
                        <td class="actions">
                            <a href="<?php echo site_url('investment/adminstep/' . $in_process_id); ?>"
                               class="item btnView tooltips" data-placement="bottom"
                               data-original-title="Investment Process"><i class="icon-money"></i></a>
                            <a href="<?php echo site_url('admin/message/view_message/' . $property_id . '/' . $investor_user_id); ?>"
                               class="item btnMessage tooltips" data-placement="bottom"
                               data-original-title="Send message"><i class="icon-envelope"></i></a>
                        </td>
                    </tr>

                    <?php


                    $i++;
                }


            }?>


            </tbody>
        </table>
    </div>
</div>
</div>
</div>

</div>


<!-- END EXAMPLE TABLE widget-->


</div>


</div>


<!-- END ADVANCED TABLE widget-->


<!-- END PAGE CONTENT-->


</div>


<!-- END PAGE CONTAINER-->


</div>


<!-- END PAGE -->


</div>

<!-- END PAGE CONTAINER-->


</div>


<!-- END PAGE -->


</div>


<!-- END CONTAINER -->


<!-- BEGIN JAVASCRIPTS -->


<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>


<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/jquery.dataTables.js"></script>


<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/DT_bootstrap.js"></script>


<script src="<?php echo base_url(); ?>js/scripts.js"></script>

<!-- Include Date Range Picker -->

<script type="text/javascript" src="<?php echo base_url('js/moment.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/daterangepicker.js');?>"></script>
 <link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url('css/daterangepicker.css');?>" />
 <!-- Include Required Prerequisites -->
<script type="text/javascript" src="<?php echo base_url('js/moment.min.js');?>"></script>


<script type="text/javascript" charset="utf-8">


    jQuery(document).ready(function () {


        App.init();
        $('body').on('click','#update_phases',function(event){
            $(this).html("<?=LOADING?>");
            event.preventDefault();
            var parent_phase = $('#parent_phase').val();
            var child_phase = $('#child_phase').val();
            var cash_flow_status = $('#cash_flow_status').val();
            
            
            url = "<?=site_url('admin/property/savePhases/'.$property_id)?>";
            var posting = $.post( url,{parent_phase:parent_phase,child_phase:child_phase,cash_flow_status:cash_flow_status} );
            posting.done(function(data) {
                var data = $.parseJSON(data);
                if(data.success){
                    $('.show_success').show();
                }
                if(data.redirect_url){
                    location.href = data.redirect_url;
                }
                $("#update_phases").html("<?=UPDATE?>");
               //if(data.highlight) show_highlights(data.id);
            });
        });

    });
    $(function () {
        $("body").on("change", "#contract_copy_file", function (event) {


            $('#investor_deal_document_image').children().remove();
            $(this).prependTo('#investor_deal_document_image').addClass('hidden-file-field');

            $('#investor_deal_document_form').submit();
            $(this).prependTo("#contract_copy_file_display").removeClass('hidden-file-field');
        });


    });

    // bind form id and provide a simple callback function
    if ((pic = jQuery('#investor_deal_document_form')).length)
        pic.ajaxForm({
            dataType: 'json',
            beforeSend: function () {
                // alert('test');
                $('.ploader').fadeIn();
            },
            success: function (result) {


                if (result.msg.success != '') {
                    $(".document_copy").hide();
                    $("#upload_document_link").show();
                    $("#upload_document_link").html(result.document_file.path);
                    $(".success_document_copy").show();
                }
                if (result.contract_copy_file_error != '') {
                    $(".success_document_copy").hide();
                    $(".upload_copy_error").html(result.contract_copy_file_error);
                    $(".document_copy").show();

                }
            },


        });


</script>

<link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet"/>
<script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {


$('#sample_new').dataTable({
            "bDestroy": true,
            "aoColumnDefs": [{'bSortable': false, 'aTargets': [0]}],

            "oLanguage": {
                "sLengthMenu": " _MENU_ <?php echo RECORD_PER_PAGE; ?>",
                "sZeroRecords": "<?php echo NOTHING_FOUND_SORRY; ?>",
                "sInfo": "<?php echo SHOWING; ?> _START_ to _END_ of _TOTAL_ <?php echo RECORD; ?>",
                "sInfoEmpty": "<?php echo SHOWING; ?> 0 to 0 of 0 <?php echo RECORD; ?>",
                "sSearch": "<?php echo SEARCH; ?>: ",
                'oPaginate': {

                    'sPrevious': '<?php echo PREVIOUS; ?>',
                    'sNext': '<?php echo NEXT; ?>'
                }
            },
                 "dom": '<"top">lt<"bottom"p><"clear">',

        });


    });
</script>


<style type="text/css">
.profile-pic .embed-responsive-item{
    width: 100%;
}
.daterangepicker.dropdown-menu {
    right: 344px !important;
    top: 295px;
}
.center-div:before{
right: 70px;
}
</style>


</script>
     <?php if ($_GET) {
    $get_status = $this->input->get('get_status');
    $user_name = $this->input->get('user_name');
    $amount = $this->input->get('amount');
    $trans_id = $this->input->get('trans_id');
    $from_date = $this->input->get('from_date');
    $to_date = $this->input->get('to_date');
    $min_amount=$this->input->get('min_amount');
    $max_amount=$this->input->get('max_amount')   ;

} else {
    $get_status = '';
    $user_name = '';
    $trans_id = '';
    $from_date = '';
    $to_date = '';
    $min_amount='';
    $max_amount='';
}
?>
<script type="text/javascript">
    $(document).ready(function () {
        $("#filterPill .filter-pill").click(function () {
            var res = document.URL;
            $(this).remove();
            id = $(this).attr('id');

            new_url = res.replace('', '');
          
            if (id == 'get_status_tag') {
                new_url = '<?php echo site_url('admin/campaign/campaign_detail/'.$url_project_title.'?get_status=&user_name='.$user_name.'&min_amount='.$min_amount.'&max_amount='.$max_amount.'&trans_id='.$trans_id.'&from_date='.$from_date.'&to_date='.$to_date);?>#investors';
            }
            if (id == 'user_name_tag') {
                new_url = '<?php echo site_url('admin/campaign/campaign_detail/'.$url_project_title.'?get_status='.$get_status.'&user_name=&min_amount='.$min_amount.'&max_amount='.$max_amount.'&trans_id='.$trans_id.'&from_date='.$from_date.'&to_date='.$to_date);?>#investors';
            }
            if (id == 'min_amount_tag') {
                new_url = '<?php echo site_url('admin/campaign/campaign_detail/'.$url_project_title.'?get_status='.$get_status.'&user_name='.$user_name.'&min_amount=&max_amount='.$max_amount.'&trans_id='.$trans_id.'&from_date='.$from_date.'&to_date='.$to_date);?>#investors';
            }
             if (id == 'max_amount_tag') {
                new_url = '<?php echo site_url('admin/campaign/campaign_detail/'.$url_project_title.'?get_status='.$get_status.'&user_name='.$user_name.'&min_amount='.$min_amount.'&max_amount=&trans_id='.$trans_id.'&from_date='.$from_date.'&to_date='.$to_date);?>#investors';
            }
            if (id == 'trans_id_tag') {
                new_url = '<?php echo site_url('admin/campaign/campaign_detail/'.$url_project_title.'?get_status='.$get_status.'&user_name='.$user_name.'&min_amount='.$min_amount.'&max_amount='.$max_amount.'&trans_id=&from_date='.$from_date.'&to_date='.$to_date);?>#investors';
            }
             if (id == 'from_date_tag') {
                new_url = '<?php echo site_url('admin/campaign/campaign_detail/'.$url_project_title.'?get_status='.$get_status.'&user_name='.$user_name.'&min_amount='.$min_amount.'&max_amount='.$max_amount.'&trans_id='.$trans_id.'&from_date=&to_date=');?>#investors';
            }
          

            window.location.href = new_url;
        });
        $("#filterClear").click(function () {
            $("#filterPill .filter-pill").remove();
            $(this).remove();
            window.location.href = '<?php echo site_url('admin/campaign/campaign_detail/'.$url_project_title); ?>#investors';
        });

      

    });
</script>


<script type="text/javascript">
$(function() {

    function cb(start, end) {
      
        var d1=new Date();
        var d2= new Date(start);
        if(d1.getTime() < d2.getTime()){
              $('#from_date').val('');
            $('#to_date').val('');
            $('#reportrange span').html('DD Month,Year - DD Month,Year');

        }else{

            $('#reportrange span').html(start.format('D MMMM, YYYY') + ' - ' + end.format('D MMMM, YYYY'));
            $('#from_date').val(start.format('D MMMM, YYYY'));
            $('#to_date').val(end.format('D MMMM, YYYY'));
        }
        
    }
    //cb(moment().subtract(-1, 'days'), moment().subtract(-1,'days'));
   
        $('#reportrange span').html('DD Month,Year - DD Month,Year');


    $('#reportrange').daterangepicker({
        ranges: {
           'Clear' : [moment().subtract(-1, 'days'), moment().subtract(-1, 'days')],
           'Today': [moment().subtract(0, 'days'), moment().subtract(0, 'days')],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);
     $('.ranges li:first-child').click();
 <?php
 if($_GET) 
 if($this->input->get('from_date')!=''){
            ?>
            $('#reportrange span').html('<?php echo $this->input->get('from_date').' - '.$this->input->get('to_date'); ?>');
            $('.ranges li:last-child').click();
            <?php }else{
                ?>
                    $('.ranges li:first-child').click();
                      $('#from_date').val('');
                  $('#to_date').val('');
                <?php
            }
            ?>
});
</script>

<style>
    .searchProject-area input[type="text"]{
        max-width: 105px;
    }
    .marl0t5{
        margin-left: 0;
        margin-top: 5px;
    }
.date-range-style {
    padding: 5px;
    cursor: pointer;
}
.searchProject-area .pull-right {
    float: none;
}
.date-range-style .caret {
    vertical-align: middle;
}
</style>
   



   



   



