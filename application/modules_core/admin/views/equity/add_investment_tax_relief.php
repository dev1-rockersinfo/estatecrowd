<!-- BEGIN CONTAINER -->
<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php echo $this->load->view('admin_sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN THEME CUSTOMIZER-->
                    <div id="theme-change" class="hidden-phone">
                        <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text"><?php echo THEME; ?>:</span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
                    </div>
                    <!-- END THEME CUSTOMIZER-->
                    <h3 class="page-title">

                        <?php if ($investment_tax_relief_name != '') {
                            echo EDIT_INVESTOR_TAX_RELIEF;
                        } else {
                            echo ADD_INVESTOR_TAX_RELIEF;
                        } ?>
                    </h3>
                    <ul class="breadcrumb">
                        <li>
                            <a href="<?php echo site_url('admin/home/dashboard') ?>"><i class="icon-home"></i></a><span
                                class="divider">&nbsp;</span>
                        </li>

                        <li>
                            <a href="<?php echo site_url('admin/equity/list_investment_tax_relief'); ?>"> <?php echo INVESTMENT_TAX_RELIEF; ?></a><span
                                class="divider-last">&nbsp;</span></li>
                    </ul>
                </div>
            </div>

            <!-- END PAGE HEADER-->
            <?php
            if ($error != "") {
                ?>
                <div class="alert alert-error">
                    <button class="close" data-dismiss="alert">x</button>
                    <strong><?php echo ERRORS; ?>!</strong>

                    <p> <?php echo $error; ?></p>
                </div>

            <?php } ?>
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                <div class="span12 sortable">
                    <!-- BEGIN SAMPLE FORMPORTLET-->
                    <div class="widget">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i>  <?php if ($investment_tax_relief_name != '') {
                                    echo EDIT_INVESTOR_TAX_RELIEF;
                                } else {
                                    echo ADD_INVESTOR_TAX_RELIEF;
                                } ?></h4>
                             
                                        <span class="tools">
                                        <a href="javascript:;" class="icon-chevron-down"></a>
                                        <a href="javascript:;" class="icon-remove"></a>
                                        </span>
                        </div>
                        <div class="widget-body">
                            <!-- BEGIN FORM-->
                            <?php
                            $attributes = array('name' => 'frm_investor', 'class' => 'form-horizontal');
                            echo form_open('admin/equity/add_investment_tax_relief/' . $id, $attributes);
                            ?>

                        <div class="control-group">
                                <label class="control-label"><?php echo INVESTMENT_TAX_RELIEF_TYPE; ?></label>

                                <div class="controls">
                                    <select name="investment_tax_relief_type" id="investment_tax_relief_type">
                                        <option value="SEIS" <?php if ($investment_tax_relief_type == "SEIS") {
                                            echo "selected='selected'";
                                        } ?>><?php echo "SEIS"; ?></option>
                                        <option value="EIS" <?php if ($investment_tax_relief_type == "EIS") {
                                            echo "selected='selected'";
                                        } ?>><?php echo "EIS"; ?></option>
                                         <option value="NONE" <?php if ($investment_tax_relief_type == "NONE") {
                                            echo "selected='selected'";
                                        } ?>><?php echo "NONE"; ?></option>
                                    </select>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label"><?php echo TITLE; ?><font color="red">
                                        *</font></label>

                                <div class="controls">
                                    <input type="text" placeholder="<?php echo TITLE;?>" name="investment_tax_relief_name"
                                           id="investment_tax_relief_name" value="<?php echo $investment_tax_relief_name; ?>"
                                           class="input-xlarge"/>

                                </div>
                            </div>

                                   <div class="control-group">
                                <label class="control-label"><?php echo DESCRIPTION; ?></label>

                                <div class="controls">
                                    <textarea type="text" placeholder="<?php echo DESCRIPTION;?>" name="investment_tax_relief_desc"
                                           id="investment_tax_relief_desc" 
                                           class="input-xlarge"><?php echo $investment_tax_relief_desc; ?></textarea>

                                </div>
                            </div>

                             <div class="control-group">
                                <label class="control-label"><?php echo LINK; ?></label>

                                <div class="controls">
                                    <input type="text" placeholder="<?php echo LINK;?>" name="investment_tax_relief_link"
                                           id="investment_tax_relief_link" value="<?php echo $investment_tax_relief_link; ?>"
                                           class="input-xlarge"/>

                                </div>
                            </div>


                            <div class="control-group">
                                <label class="control-label"><?php echo STATUS; ?></label>

                                <div class="controls">
                                    <select name="status" id="status">
                                        <option value="1" <?php if ($status == "1") {
                                            echo "selected='selected'";
                                        } ?>><?php echo ACTIVE; ?></option>
                                        <option value="0" <?php if ($status == "0") {
                                            echo "selected='selected'";
                                        } ?>><?php echo INACTIVE; ?></option>
                                    </select>
                                </div>
                            </div>

                            <input type="hidden" name="id" id="id" value="<?php echo $id; ?>"/>

                            <div class="form-actions">
                                <?php
                                if ($id == "") {
                                    ?>
                                    <button type="submit" class="btn blue" onclick="return validate();"><i
                                            class="icon-ok"></i> <?php echo SUBMIT; ?></button>
                                <?php
                                } else {
                                    ?>
                                    <button type="submit" class="btn blue"><i class="icon-ok"></i> <?php echo UPDATE; ?>
                                    </button>
                                <?php
                                }
                                ?>
                                <button type="button" class="btn"
                                        onClick="location.href='<?php echo site_url('admin/equity_company/list_investor_type'); ?>'">
                                    <i class=" icon-remove"></i> <?php echo CANCEL; ?></button>
                            </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>

            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN JAVASCRIPTS -->
<script type="text/javascript"
        src="<?php echo base_url(); ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>

<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        App.init();
    });
</script>
  
