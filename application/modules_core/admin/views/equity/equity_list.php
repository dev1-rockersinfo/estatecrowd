<?php

$campaign_name = $taxonomy_setting['project_name'];
$funds = $taxonomy_setting['funds'];
$updates = $taxonomy_setting['updates'];
$comments = $taxonomy_setting['comments'];
$taxonomy_project_url = 'properties';
$site_name = $site_setting['site_name'];
?>

<script>

    function setaction(elename, actionval, actionmsg, formname) {

        if (actionval == 'declined' || actionval == 'inactive') {
            vchkcnt = 1;
        }
        else {
            vchkcnt = 0;
            elem = document.getElementsByName(elename);
            for (i = 0; i < elem.length; i++) {
                if (elem[i].checked) vchkcnt++;
            }
        }
        if (vchkcnt == 0) {
            alert("<?php echo DELETE_SELECT_RECORD_CONFIRMATION;?>")
        } else {

            if (confirm(actionmsg)) {


                if (actionval == 'declined') {

                    var textarea = $("<textarea>").attr("name", "reason_decline").val($("#reason_decline_" + elename).val());

                    if ($("#save_reason_" + elename).is(':checked')) {
                        var save_the_reason = 1;
                    } else {
                        var save_the_reason = 0;
                    }
                    var input = $("<input>").attr("type", "hidden").attr("name", "save_the_reason").val(save_the_reason);
                    var input_property_id = $("<input>").attr("type", "hidden").attr("name", "action_property_id").val(elename);
                    $('#' + formname).append($(input_property_id));
                    $('#' + formname).append($(textarea));
                    $('#' + formname).append($(input));
                }
                if (actionval == 'inactive') {


                    var inactive_mode = $("input[name=inactive_equity_mode_" + elename + "]:checked").val();
                    var fund_inactive_value = $("input[name=fund_inactive_" + elename + "]:checked").val();


                    var textarea_inactive = $("<textarea>").attr("name", "reason_inactive_hidden").val($("#reason_inactive_" + elename).val());


                    if ($("#save_reason_inactive_" + elename).is(':checked')) {
                        var save_the_reason_inactive = 1;
                    } else {
                        var save_the_reason_inactive = 0;
                    }


                    var input_inactive = $("<input>").attr("type", "hidden").attr("name", "save_the_reason_inactive").val(save_the_reason_inactive);
                    var input_inactive_mode = $("<input>").attr("type", "hidden").attr("name", "inactive_mode").val(inactive_mode);
                    var fund_inactive_note = $("<input>").attr("type", "hidden").attr("name", "fund_inactive_note").val(fund_inactive_value);
                    var input_property_id = $("<input>").attr("type", "hidden").attr("name", "action_property_id").val(elename);
                    $('#' + formname).append($(input_property_id));
                    $('#' + formname).append($(textarea_inactive));
                    $('#' + formname).append($(input_inactive));
                    $('#' + formname).append($(fund_inactive_note));
                    $('#' + formname).append($(input_inactive_mode));
                }
                document.getElementById('action').value = actionval;
                document.getElementById(formname).submit();
            }
        }
    }

    function setoneaction(property_id, actionval, actionmsg, formname) {


        if (confirm(actionmsg)) {
            // document.getElementById('action').value=actionval;
            // document.getElementById(formname).submit();
            window.location = "<?php echo site_url('/admin/campaign/action_campaign')?>" + '/' + property_id + '/' + actionval;
        }
    }

</script>


<!-- BEGIN CONTAINER -->


<div id="container" class="row-fluid">


<!-- BEGIN SIDEBAR -->



<?php echo $this->load->view('admin_sidebar'); ?>



<!-- END SIDEBAR -->


<!-- BEGIN PAGE -->


<div id="main-content">


<!-- BEGIN PAGE CONTAINER-->


<div class="container-fluid">


<!-- BEGIN PAGE HEADER-->


<div class="row-fluid">


    <div class="span12">


        <!-- BEGIN THEME CUSTOMIZER-->


        <div id="theme-change" class="hidden-phone">


            <i class="icon-cogs"></i>



                        <span class="settings">



                            <span class="text"><?php echo THEME; ?></span>



                            <span class="colors">



                                <span class="color-default" data-style="default"></span>



                                <span class="color-gray" data-style="gray"></span>



                                <span class="color-purple" data-style="purple"></span>



                                <span class="color-navy-blue" data-style="navy-blue"></span>



                            </span>



                        </span>


        </div>


        <!-- END THEME CUSTOMIZER-->


        <!-- BEGIN PAGE TITLE & BREADCRUMB-->


        <h3 class="page-title">


            <?php echo EQUITY_LIST; ?>


        </h3>


        <ul class="breadcrumb">


            <li>


                <a href="<?php echo site_url('admin/home/dashboard'); ?>"><i class="icon-home"></i></a><span
                    class="divider">&nbsp;</span>


            </li>


            <li><a href="#"><?php echo EQUITY_LIST; ?>  </a><span class="divider-last">&nbsp;</span></li>


        </ul>


        <!-- END PAGE TITLE & BREADCRUMB-->


    </div>


</div>


<!-- END PAGE HEADER-->


<!-- BEGIN ADVANCED TABLE widget-->




<?php

if ($msg) {


    foreach ($msg as $key => $val) {


        if ($key) {

            $project_title_error = $key;
            if ($val == 'delete') {
                ?>
                <div class="alert alert-success">
                    <button class="close" data-dismiss="alert">x</button>
                    <strong><?php echo SUCCESS; ?>!</strong>
                    "<?php echo $project_title_error; ?>"</span> <?php echo HAS_BEEN_DELETED_SUCCESSFULLY; ?>.
                </div>

            <?php
            }
            if ($val == 'cannot_delete_active') {
                ?>
                <div class="alert alert-error">
                    <button class="close" data-dismiss="alert">x</button>
                    <strong><?php echo ERRORS; ?>!</strong> <?php echo YOU_CAN_NOT_DELETE_ACTIVE_PROJECT; ?>
                    "<?php echo $project_title_error; ?>".
                </div>

            <?php
            }
            if ($val == 'no_delete') {
                ?>
                <div class="msgdisp" style="color:#FF0000;"><?php echo YOU_CAN_NOT_DELETE_ACTIVE_PROJECT; ?>
                    "<?php echo $project_title_error; ?>".
                </div>

            <?php
            }
            if ($val == 'active') {
                ?>
                <div class="alert alert-success">
                    <button class="close" data-dismiss="alert">x</button>
                    <strong><?php echo SUCCESS; ?>!</strong>"<?php echo $project_title_error; ?>
                    " <?php echo HAS_BEEN_ACTIVATED_SUCCESSFULLY; ?>.
                </div>
            <?php
            }
            if ($val == 'approve') {
                ?>
                <div class="alert alert-success">
                    <button class="close" data-dismiss="alert">x</button>
                    <strong><?php echo SUCCESS; ?>!</strong>"<?php echo $project_title_error; ?>
                    " <?php echo HAS_BEEN_APPROVED_SUCCESSFULLY; ?>.
                </div>
            <?php
            }
            if ($val == 'cannot_active_expired') {
                ?>
                <div class="alert alert-error">
                    <button class="close" data-dismiss="alert">x</button>
                    <strong><?php echo ERRORS; ?>!</strong><?php echo YOU_CAN_NOT_ACTIVE_EXPIRED_OR_ALREADY_ACTIVE; ?>
                    "<?php echo $project_title_error; ?>".
                </div>
            <?php
            }
             if ($val == 'cannot_approve_contract') {
                ?>
                <div class="alert alert-error">
                    <button class="close" data-dismiss="alert">x</button>
                    <strong><?php echo ERRORS; ?>!</strong>
                    <?php
                        $project_link = '<a href="' . site_url('admin/campaign/campaign_detail/'.$project_title_error) . '">' . THIS_LINK . '</a>';
                        echo sprintf(BEFORE_YOU_CAN_APPROVE_THIS_PROJECT,$project_link); ?>
                    "<?php echo $project_title_error; ?>".
                </div>
            <?php
            }
            if ($val == 'inactive') {
                ?>
                <div class="alert alert-success">
                    <button class="close" data-dismiss="alert">x</button>
                    <strong><?php echo SUCCESS; ?>!</strong>"<?php echo $project_title_error; ?>
                    " <?php echo HAS_BEEN_INACTIVATED_SUCCESSFULLY; ?>.
                </div>
            <?php
            }
            if ($val == 'cannot_inactive') {
                ?>
                <div class="alert alert-error">
                    <button class="close" data-dismiss="alert">x</button>
                    <strong><?php echo ERRORS; ?>!</strong><?php echo YOU_CAN_NOT_INACTIVE_EXPIRED_OR_ALREADY_ACTIVE; ?>
                    "<?php echo $project_title_error; ?>".
                </div>
            <?php
            }
            if ($val == 'declined') {
                ?>
                <div class="alert alert-success">
                    <button class="close" data-dismiss="alert">x</button>
                    <strong><?php echo SUCCESS; ?>!</strong>"<?php echo $project_title_error; ?>
                    " <?php echo HAS_BEEN_DECLIENED_SUCCESSFULLY; ?>.
                </div>

            <?php
            }
            if ($val == 'declined_error') {
                ?>
                <div class="alert alert-error">
                    <button class="close" data-dismiss="alert">x</button>
                    <strong><?php echo ERRORS; ?>!</strong>"<?php echo $project_title_error; ?>
                    " <?php echo CANNOT_BE_DECLIENED; ?>.
                </div>



            <?php
            }
            if ($val == 'feature') {
                ?>
                <div class="alert alert-success">

                    <button class="close" data-dismiss="alert">x</button>
                    <strong><?php echo SUCCESS; ?>!</strong>"<?php echo $project_title_error; ?>
                    " <?php echo HAS_BEEN_FEATURED_SUCCESSFULLY; ?>.
                </div>

            <?php
            }
            if ($val == 'set_featured_active') {
                ?>
                <div class="alert alert-success">

                    <button class="close" data-dismiss="alert">x</button>
                    <strong><?php echo SUCCESS; ?>!</strong>"<?php echo $project_title_error; ?>
                    " <?php echo HAS_BEEN_ACTIVATED_AND_SET_FEATURED_SUCCESSFULLY; ?>.
                </div>

            <?php
            }
            if ($val == 'feature_cancel') {
                ?>
                <div class="alert alert-error">
                    <button class="close" data-dismiss="alert">x</button>
                    <strong><?php echo ERRORS; ?>!</strong>"<?php echo $project_title_error; ?>
                    " <?php echo CANNOT_BE_SET_TO_FEATURED_AS_IT_IS_EXPIRED_SUCCESSFUL_FAILURE; ?>.
                </div>

            <?php
            }
            if ($val == 'not_feature') {
                ?>
                <div class="alert alert-success">

                    <button class="close" data-dismiss="alert">x</button>
                    <strong><?php echo SUCCESS; ?>!</strong>"<?php echo $project_title_error; ?>
                    " <?php echo HAS_BEED_REMOVE_FROM_FEATURED_SUCCESSFULLY; ?>.
                </div>

            <?php
            }
            if ($val == "insert") {
                ?>
                <div class="alert alert-success">
                    <button class="close" data-dismiss="alert">x</button>
                    <strong><?php echo SUCCESS; ?>!</strong><?php echo NEWS; ?> "<?php echo $project_title_error; ?>
                    " <?php echo HAS_BEEDN_ADDED_SUCCESSFULLY; ?>.
                </div>
            <?php
            }
            if ($val == "update") {
                ?>
                <div class="alert alert-success">
                    <button class="close" data-dismiss="alert">x</button>
                    <strong><?php echo SUCCESS; ?>!</strong>"<?php echo $project_title_error; ?>
                    " <?php echo HAS_BEEDN_UPDATED_SUCCESSFULLY; ?>.
                </div>
            <?php
            }

        } else {
            if (in_array("insert", $msg)) {
                ?>
                <div class="alert alert-success">
                    <button class="close" data-dismiss="alert">x</button>
                    <strong><?php echo SUCCESS; ?>!</strong><?php echo NEW_RECORDS_HAS_BEEN_ADDED_SUCCESSFULLY; ?>.
                </div>

            <?php
            }
            if (in_array("delete", $msg)) {
                ?>
                <div class="alert alert-success">
                    <button class="close" data-dismiss="alert">x</button>
                    <strong><?php echo SUCCESS; ?>!</strong><?php echo RECORDS_HAS_BEEN_DELETED_SUCCESSFULLY; ?>.
                </div>

            <?php
            }

        }

    }

}

?>




<div class="fr iconM">


    <div class="bulkAction">
        <div class="btn-group">
            <button class="btn dropdown-toggle" data-toggle="dropdown">Bulk Action <span class="caret"></span></button>
            <ul class="dropdown-menu">
                <li>
                    <button type="button" class="btn"
                            onclick="setaction('chk[]','delete', '<?php echo ARE_YOU_SURE_YOU_WANT_TO_DELETE_SELECTED_PROJECT; ?>', 'frm_listproject')">
                        <i class="icon-remove icon-white"></i> <?php echo DELETE; ?></button>
                </li>

                <li>

                    <button type="button" class="btn"
                            onclick="setaction('chk[]','approve', '<?php echo ARE_YOU_SURE_YOU_WANT_TO_APPROVE_SELECTED_PROJECT; ?>', 'frm_listproject')">
                        <i class="icon-ok icon-white"></i> <?php echo APPROVE; ?></button>
                </li>


              

                <li>

                    <button type="button" class="btn"
                            onclick="setaction('chk[]','active', '<?php echo ARE_YOU_SURE_YOU_WANT_TO_ACTIVE_SELECTED_PROJECT; ?>', 'frm_listproject')">
                        <i class="icon-ok icon-white"></i> <?php echo ACTIVE; ?></button>
                </li>


            </ul>
        </div>
    </div>



    <?php $email = $this->home_model->get_one_tabledata('admin', array('admin_id' => $this->session->userdata('admin_id')));



    ?>




    <button type="button" class="btn mini purple"
            onclick="location.href='<?php echo site_url('home/adminlog/' . base64_encode($email['email'])); ?>'"><i
            class="icon-plus"></i> <?php echo ADD; ?></button>


</div>


<div class="row-fluid">


<div class="span12">


<!-- BEGIN EXAMPLE TABLE widget-->


<div class="widget">


<div class="widget-title">


    <h4><i class="icon-reorder"></i><?php echo EQUITY_LIST; ?></h4>



                            <span class="tools">



                                <a href="javascript:;" class="icon-chevron-down"></a>



                            </span>


</div>


<div class="widget-body">


<div class="alert alertMsg"><a href="<?php echo site_url('admin/site_setting/slider_setting') ?>"
                               target="_blank"><?php echo CLICK_HERE; ?> </a> <?php echo TO_GO_MANAGE_ORDER_OF_FEATURED; ?>
</div>

<?php if ($_GET) {
    $search_by = $_GET['search_by'];
    $search_name = $_GET['search_name'];
    $search_status = $_GET['search_status'];
    $search_goal_per = $_GET['search_goal_per'];
    $search_goal_to = $_GET['search_goal_to'];
    $search_category = $_GET['search_category'];
    $search_goal_from = $_GET['search_goal_from'];

} else {

    $search_by = '';
    $search_name = '';
    $search_status = '';
    $search_goal_per = '';
    $search_goal_to = '';
    $search_category = '';
    $search_goal_from = '';
}

?>
<div class="search-section">
    <form action="" method="get">
        <div class="filter-area">
            <div class="filter-main">
                <div class="searchProject-area">
                    <select name="search_by">

                        <option value="project_name" <?php if ($search_by == 'project_name') {
                            echo 'selected="selected"';
                        } ?>><?php echo BY_PROJECT; ?></option>
                        <option value="owner_name" <?php if ($search_by == 'owner_name') {
                            echo 'selected="selected"';
                        } ?>><?php echo BY_OWNER_NAME; ?></option>
                    </select>
                    <input type="text" name="search_name" value="<?php echo $search_name; ?>">
                </div>
                <div class="filter-status filter-item">
                    <select name="search_status">
                        <option value=""><?php echo STATUS; ?></option>

                        <option value="1" <?php if ($search_status == '1') {
                            echo 'selected="selected"';
                        } ?>><?php echo status_name('1'); ?></option>
                        <option value="2" <?php if ($search_status == '2') {
                            echo 'selected="selected"';
                        } ?>><?php echo status_name('2'); ?></option>
                        <option value="3" <?php if ($search_status == '3') {
                            echo 'selected="selected"';
                        } ?>><?php echo status_name('3'); ?></option>
                        <option value="4" <?php if ($search_status == '4') {
                            echo 'selected="selected"';
                        } ?>><?php echo status_name('4'); ?></option>
                        <option value="5" <?php if ($search_status == '5') {
                            echo 'selected="selected"';
                        } ?>><?php echo status_name('5'); ?></option>
                        <option value="6" <?php if ($search_status == '6') {
                            echo 'selected="selected"';
                        } ?>><?php echo status_name('6'); ?></option>
                        <option value="7" <?php if ($search_status == '7') {
                            echo 'selected="selected"';
                        } ?>><?php echo status_name('7'); ?></option>
                        <option value="8" <?php if ($search_status == '8') {
                            echo 'selected="selected"';
                        } ?>><?php echo status_name('8'); ?></option>
                    </select>
                </div>
                <div class="filter-percentage filter-item input-append">
                    <input type="text" placeholder="<?php echo BY_PAR_FUNDED; ?>" name="search_goal_per"
                           value="<?php echo $search_goal_per; ?>">
                    <span class="add-on">%</span>
                </div>
                <div class="filter-range">
                    <input type="number" placeholder="<?php echo MIN_FUNDED; ?>" class="input-small"
                           name="search_goal_from" value='<?php echo $search_goal_from; ?>'><span
                        class="help-inline">-</span>
                    <input type="number" placeholder="<?php echo MAX_FUNDED; ?>" class="input-small" name="search_goal_to"
                           value='<?php echo $search_goal_to; ?>'>
                </div>
                <div class="filter-category filter-item">
                    <select name="search_category">
                        <option value=""><?php echo SELECT_CATEGORY; ?></option>
                        <?php
                        if ($all_categories) {
                            foreach ($all_categories as $category) {

                                ?>
                                <option
                                    value="<?php echo $category['company_category_name']; ?>" <?php if ($search_category == $category['company_category_name']) {
                                    echo 'selected="selected"';
                                } ?> ><?php echo $category['company_category_name']; ?></option>

                            <?php
                            }
                        }
                        ?>
                    </select>
                </div>
                <div class="filter-item">
                    <input type="submit" class="filter-btn" value="<?php echo SEARCH; ?>">
                </div>
            </div>
        </div>
    </form>
    <?php  if ($search_name != '' || $search_status != '' || $search_goal_per != '' || $search_goal_to != '' || $search_category != '' || $search_goal_from != '') {


        ?>

        <div class="pill-container">
            <div id="filterPill">
                <?php if ($_GET['search_name']) { ?>
                    <a class="filter-pill" id="search_name_tag"><?php echo $_GET['search_name']; ?> <span
                            class="icon-remove"></span></a>
                <?php } ?>

                <?php if ($_GET['search_status'] != '') { ?>
                    <a class="filter-pill" id="search_status_tag"><?php echo status_name($_GET['search_status']); ?>
                        <span class="icon-remove"></span></a>
                <?php } ?>

                <?php if ($_GET['search_goal_from']) { ?>
                    <a class="filter-pill" id="search_goal_from_tag">>=<?php echo $_GET['search_goal_from']; ?> <span
                            class="icon-remove"></span></a>
                <?php } ?>

                <?php if ($_GET['search_goal_to']) { ?>
                    <a class="filter-pill" id="search_goal_to_tag"><=<?php echo $_GET['search_goal_to']; ?> <span
                            class="icon-remove"></span></a>
                <?php } ?>

                <?php if ($_GET['search_goal_per']) { ?>
                    <a class="filter-pill" id="search_goal_per_tag"><?php echo $_GET['search_goal_per']; ?>% <span
                            class="icon-remove"></span></a>
                <?php } ?>

                <?php if ($_GET['search_category']) { ?>
                    <a class="filter-pill" id="search_category_tag"><?php echo $_GET['search_category']; ?> <span
                            class="icon-remove"></span></a>
                <?php } ?>
            </div>
            <a class="filter-clear btn" id="filterClear"><?php echo CLEAR_ALL_FILTERS; ?></a>
        </div>
    <?php } ?>
</div>

<?php



$attributes = array('name' => 'frm_listproject', 'id' => 'frm_listproject', 'method' => 'post');
echo form_open('admin/campaign/action_campaign/', $attributes);
?>
<input type="hidden" name="action" id="action"/>


<table class="table table-striped table-bordered table_project" id="sample_1">


<thead>


<tr>
    <th style="width:8px;"><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes"/></th>
    <th><?php echo EQUITY; ?></th>
   
    <th class="hidden-phone"><?php echo CREATE_BY; ?></th>
    <th><?php echo PROGRESS; ?></th>
    <th><?php echo INVESTOR; ?></th>
    <th class="hidden-phone"><?php echo STATUS; ?></th>
    <th><?php echo ACTION; ?></th>
</tr>


</thead>


<tbody>
<?php



if ($result) {


    foreach ($result as $row) {


        $user_data = UserData($row['user_id']);
        $user_detail = $user_data[0];
        $profile_slug = $user_detail['profile_slug'];
        $creater_image = $user_detail['image'];


        $property_id = $row['property_id'];

        $property_user_id = $row['user_id'];

        $property_address = $row['property_address'];

       

        $min_investment_amount = $row['min_investment_amount'];

        $property_url = $row['property_url'];
        $status = status_name($row['status']);
        $status_class = '';

        if ($row['status'] == 2 || $row['status'] == 3 || $row['status'] == 4) {
            $status_class = 'label-success';
        }
        if ($row['status'] == 6) {
            $status_class = 'label-warning';
        }
        if ($row['status'] == 7 || $row['status'] == 8 || $row['status'] == 5) {
            $status_class = 'label-danger';
        }




        $amount_get = $row['amount_get'];

        $user_name = $row['user_name'];
        $last_name = $row['last_name'];
        $email = $row['email'];
        
        $investor = '';
        //$investor = GetEquityInvestor($property_id);
        $get_percentage = GetCampaignPercentage($data = array('min_investment_amount' => $min_investment_amount, 'amount_get' => $amount_get));

        if (strlen($property_address) > 25) {
            $str_name = substr(ucfirst($property_address), 0, 25) . '...';
        } else {
            $str_name = $property_address;
        }

      

        if ($creater_image != '' && is_file(base_path() . 'upload/user/user_small_image/' . $creater_image)) {
            $cimage = $creater_image;

        } else {
            $cimage = 'no_man.jpg';
        }
        if (!empty($str_name)) {

            $str_name_url = site_url($taxonomy_project_url . '/' . $property_url);

        } else {

            $str_name_url = site_url($taxonomy_project_url . '/' . $property_id);
        }

        //all reason task
        $save_the_reason = $row['save_the_reason'];
        $reason_decline = $row['reason_decline'];
        $save_the_reason_inactive = $row['save_the_reason_inactive'];
        $reason_inactive_hidden = $row['reason_inactive_hidden'];
        if ($save_the_reason != 1) {
            $temp = PreviousReasonEquity($property_id, 'decline');
            if (isset($temp['reason_decline'])) $reason_decline = $temp['reason_decline'];
        }
        if ($save_the_reason_inactive != 1) {
            $temp = PreviousReasonEquity($property_id, 'inactive');
            if (isset($temp['reason_inactive_hidden'])) $reason_inactive_hidden = $temp['reason_inactive_hidden'];
        }
        ?>


        <tr class="odd gradeX">
        <td><input type="checkbox" class="checkboxes" name="chk[]" value="<?php echo $property_id; ?>"/></td>
        <td class="positionR "><a href="<?php echo $str_name_url; ?>"
                                  target="_blank"><?php echo $str_name; ?> </a></td>
       
        <td class="hidden-phone">
            <a href="<?php echo site_url('user/' . $profile_slug); ?>" target="_blank" class="tooltips creater"
               data-placement="bottom" data-original-title="<?php echo $user_name . ' ' . $last_name; ?>">
                <img src="<?php echo base_url() ?>upload/user/user_small_image/<?php echo $cimage; ?>"></a></td>
        <td>
            <div class="progress progress-success">
                <div style="width: <?php echo floor($get_percentage); ?>%;"
                     class="bar"><?php echo floor($get_percentage); ?>%
                </div>
            </div>
            <div class="ProProgress"><span class="proGoal"><?php echo GOAL ?>
                    : <strong><?php echo set_currency($min_investment_amount); ?></strong></span> <span
                    class="proRaised"><?php echo RAISED; ?>: <strong><?php if ($amount_get > 0) {
                            echo set_currency($amount_get);
                        } else {
                            echo '--';
                        } ?></strong></span></div>
        </td>
        <td>
            <a href="<?php echo site_url('admin/campaign/campaign_detail/' . $property_url . '#investors'); ?>"><?php echo $investor; ?></a>
        </td>
        <td class="hidden-phone"><span class="label <?php echo $status_class; ?>"><?php echo $status; ?></span></td>
        <td class="actions">
            <a href="<?php echo site_url('admin/campaign/campaign_detail/' . $property_url); ?>" target="_blank"
               class="item btnView tooltips" data-placement="bottom" data-original-title="<?php echo VIEW_DETAIL;?>"><i
                    class="icon-eye-open"></i></a>

            <a href="<?php echo base_url() . 'admin/message/view_message/' . $property_id . '/' . $property_user_id; ?>"
               class="item btnMessage tooltips" data-placement="bottom" data-original-title="<?php echo SEND_MESSAGE;?>"><i
                    class="icon-envelope"></i></a>
            <!-- Feature -->

       
            <!-- Delete -->
            <?php if ($row['status'] == 1 || $row['status'] == 6) { ?>

                <a href="javascript://" class="item btnDelete tooltips" data-placement="bottom"
                   data-original-title="Delete"
                   onclick="setoneaction(<?php echo $property_id; ?>,'delete', '<?php echo ARE_YOU_SURE_YOU_WANT_TO_DELETE_SELECTED_PROJECT; ?>', 'frm_listproject')"><i
                        class="icon-trash"></i></a>

            <?php } else { ?>
                <a href="javascript://" class="item btnDelete tooltips disable" data-placement="bottom"
                   data-original-title="<?php echo CANT_DELETE;?>"><i class="icon-trash"></i></a>
            <?php } ?>

            <!-- Inactive and active-->
            <?php if ($row['status'] == 2) { ?>

                <a href="#inactive-<?php echo $property_id; ?>" role="button" class="item btnApprove tooltips"
                   data-placement="bottom" data-original-title="<?php echo INACTIVE_THIS;?>" data-toggle="modal"><i
                        class="icon-ban-circle"></i></a>

            <?php
            } else if ($row['status'] == 1) {
                ?>
                <a href="javascript://" class="item btnApprove tooltips" data-placement="bottom"
                   data-original-title="<?php echo APPROVE;?>"
                   onclick="setoneaction(<?php echo $property_id; ?>,'approve', '<?php echo ARE_YOU_SURE_YOU_WANT_TO_APPROVE_SELECTED_PROJECT; ?>', 'frm_listproject')"><i
                        class="icon-ok"></i></a>

            <?php
            } else if ($row['status'] == 7 || $row['status'] == 8) {
                ?>
                <a href="javascript://" class="item btnApprove tooltips" data-placement="bottom"
                   data-original-title="Active"
                   onclick="setoneaction(<?php echo $property_id; ?>,'active', '<?php echo ARE_YOU_SURE_YOU_WANT_TO_ACTIVE_SELECTED_PROJECT; ?>', 'frm_listproject')"><i
                        class="icon-ok"></i></a>
            <?php } else { ?>

            <?php } ?>

            <!-- Decline -->
            <?php if ($row['status'] == 1) { ?>
                <a href="#decline-<?php echo $property_id; ?>" role="button" class="item btnDecline tooltips"
                   data-placement="bottom" data-original-title="<?php echo DECLINE;?>" data-toggle="modal"><i
                        class="icon-remove"></i></a>
            <?php } ?>

              <?php if ($row['status'] <= 2) { ?>
                    <a href="<?php echo site_url('property/add_property/'.$property_id);?>" role="button" class="item btnView tooltips"
                       data-placement="bottom" data-original-title="<?php echo EDIT;?>" data-toggle="modal" target="_blank"><i
                            class="icon-edit"></i></a>
                <?php } ?>
        </td>

        <div id="decline-<?php echo $property_id; ?>" class="modal hide fade" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel1" aria-hidden="true">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel1"><?php echo DECLINE; ?></h3>
            </div>
            <div class="modal-body">
                <p><?php echo sprintf(AS_YOU_HAVE_TO_DECIDED_TO_DECLINE, $campaign_name, $campaign_name, $user_name . ' ' . $last_name, $campaign_name) ?></p>

                <div class="reasonArea">
                    <p> <?php echo REASON_TO_DECLINE_THIS . ' ' . $campaign_name; ?></p>
                    <textarea rows="3" name="reason_decline_equity"
                              id="reason_decline_<?php echo $property_id; ?>"><?php echo $reason_decline; ?></textarea>
                    <label><input type="checkbox" class="checkboxes" id="save_reason_<?php echo $property_id; ?>"
                                  name="save_the_reason" value="1"/>
                        <?php echo SAVE_THIS_REASON_TO_USE_ON_OTHER . ' ' . $campaign_name ?></label>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true"><?php echo CANCEL; ?></button>
                <button class="btn btn-primary"
                        onclick="setaction(<?php echo $property_id; ?>,'declined', '<?php echo ARE_YOU_SURE_YOU_WANT_TO_DECLINE_SELECTED_PROJECT; ?>', 'frm_listproject')"><?php echo SAVE; ?></button>
            </div>
        </div>
        <div id="inactive-<?php echo $property_id; ?>" class="modal hide fade" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel1"><?php echo INACTIVE; ?></h3>
            </div>
            <div class="modal-body">
                <p><?php echo sprintf(AS_YOU_HAVE_TO_DECIDED_TO_INACTIVE, $campaign_name); ?></p>

                <div class="inactivepopup">
                    <p>
                        <label class="radio">
                            <input type="radio" name="inactive_equity_mode_<?php echo $property_id; ?>"
                                   id="inactive_mode_<?php echo $property_id; ?>" value="8"/>
                                <span class="redioText"><strong><?php echo HIDDEN_MODE; ?>
                                        :</strong> <?php echo sprintf(SELECT_THIS_OPTION_IF_YOU_WANT_TO_HIDE, $campaign_name, $campaign_name); ?></span>
                        </label>
                    </p>

                    <p>
                        <label class="radio">
                            <input type="radio" name="inactive_equity_mode_<?php echo $property_id; ?>"
                                   id="inactive_mode_<?php echo $property_id; ?>" value="7"/>
                                <span class="redioText"><strong><?php echo INACTIVE_MODE; ?>
                                        :</strong> <?php echo sprintf(SELECT_THIS_OPTION_IF_YOU_WANT_TO_SHOW, $campaign_name, $campaign_name, $updates, $funds, $comments); ?></span>
                        </label>
                    </p>

                    <div class="reasonArea">
                        <p><?php echo WRITE_REASON_WHY_YOU_DEACTIVATED_THIS . ' ' . $campaign_name; ?></p>
                        <textarea rows="3" name="reason_inactive_equity"
                                  id="reason_inactive_<?php echo $property_id; ?>"><?php echo $reason_inactive_hidden; ?></textarea>
                        <label><input type="checkbox" class="checkboxes"
                                      id="save_reason_inactive_<?php echo $property_id; ?>"
                                      name="save_the_reason_inactive"
                                      value="1"/> <?php echo SAVE_THIS_REASON_TO_USE_ON_OTHER . ' ' . $campaign_name; ?>
                        </label>
                    </div>
                    <!--
                    <p class="marT15">
                        <label class="radio">
                         <input type="radio" name="optionsRadios2" value="option2" />
                            <span class="redioText"><strong>FundBack All:</strong> Select this option if you want to fund back all funders in their wallet or in thier payment gateway(paypal, stripe or other) accounts. ONCE FUNDBACK EXECUTED, IT CAN NOT BE UNDO.</span>
                         </label>
                    </p>
                    <p>
                        <label class="radio">
                         <input type="radio" name="optionsRadios2" value="option2" />
                            <span class="redioText"><strong>No FundBack:</strong> Select this option if you are deactivating this {campaign-dynmanic-name} temporarily. This option is useful if you want to resolve your doubts with {project-owner-dynamic-name} of this {campaign-dynmanic-name} so once your questions and doubts resolved you can make this {campaign-dynmanic-name} active again.</span>
                         </label>
                    </p>
                    -->
                    <p> <?php echo sprintf(PLEASE_NOTE_IF_ONCE_IS_INACTIVATED, $campaign_name, $funds); ?></p>

                    <p><strong><?php echo sprintf(IF_MANAGED_MANUALLY_BY_TEAM, $site_name); ?></strong></p>

                    <p class="marT15">
                        <label class="radio">
                            <input type="radio" name="fund_inactive_<?php echo $property_id; ?>"
                                   id="fund_inactive_<?php echo $property_id; ?>" value="1"/>
                                <span
                                    class="redioText"> <?php echo sprintf(YOU_CAN_HOLD_ALL_UNTILL_YOU_RESOLVE_YOUR_DOUBTS_AND_REASONS, $funds, $campaign_name, $user_name . ' ' . $last_name); ?></span>
                        </label>
                    </p>

                    <p><strong>OR</strong></p>

                    <p>
                        <label class="radio">
                            <input type="radio" name="fund_inactive_<?php echo $property_id; ?>"
                                   id="fund_inactive_<?php echo $property_id; ?>" value="0"/>
                                <span
                                    class="redioText"><?php echo sprintf(YOU_CAN_REFUND_ALL_FUND, $funds, $funds, $funds, $site_name); ?></span>
                        </label>
                    </p>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true"><?php echo CANCEL; ?></button>
                <button class="btn btn-primary"
                        onclick="setaction(<?php echo $property_id; ?>,'inactive', '<?php echo ARE_YOU_SURE_YOU_WANT_TO_INACTIVE_SELECTED_RECORD; ?>', 'frm_listproject')"><?php echo SAVE; ?></button>
            </div>
        </div>
        <input type="hidden" name="property_id" id="property_id" value="<?php echo $property_id; ?>">
        </tr>

    <?php


    }


}



?>


</tbody>


</table>


</form>


</div>


</div>


<!-- END EXAMPLE TABLE widget-->


</div>


</div>


<!-- END ADVANCED TABLE widget-->


<!-- END PAGE CONTENT-->


</div>


<!-- END PAGE CONTAINER-->


</div>


<!-- END PAGE -->


</div>


<!-- END CONTAINER -->


<!-- BEGIN JAVASCRIPTS -->


<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>


<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/jquery.dataTables.js"></script>


<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/DT_bootstrap.js"></script>


<script src="<?php echo base_url(); ?>js/scripts.js"></script>


<script type="text/javascript" charset="utf-8">


    jQuery(document).ready(function () {


        App.init();


    });


</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#filterPill .filter-pill").click(function () {
            var res = document.URL;
            $(this).remove();
            id = $(this).attr('id');

            new_url = res.replace('', '');
            if (id == 'search_name_tag') {
                new_url = '<?php echo site_url('admin/campaign/list_campaign?search_by=&search_name=&search_status='.$search_status.'&search_goal_per='.$search_goal_per.'&search_goal_from='.$search_goal_from.'&search_goal_to='.$search_goal_to.'&search_category='.$search_category);?>';

            }
            if (id == 'search_status_tag') {
                new_url = '<?php echo site_url('admin/campaign/list_campaign?search_by='.$search_by.'&search_name='.$search_name.'&search_status=&search_goal_per='.$search_goal_per.'&search_goal_from='.$search_goal_from.'&search_goal_to='.$search_goal_to.'&search_category='.$search_category);?>';
            }
            if (id == 'search_goal_per_tag') {
                new_url = '<?php echo site_url('admin/campaign/list_campaign?search_by='.$search_by.'&search_name='.$search_name.'&search_status='.$search_status.'&search_goal_per=&search_goal_from='.$search_goal_from.'&search_goal_to='.$search_goal_to.'&search_category='.$search_category);?>';
            }
            if (id == 'search_goal_from_tag') {
                new_url = '<?php echo site_url('admin/campaign/list_campaign?search_by='.$search_by.'&search_name='.$search_name.'&search_status='.$search_status.'&search_goal_per='.$search_goal_per.'&search_goal_from=&search_goal_to='.$search_goal_to.'&search_category='.$search_category);?>';
            }
            if (id == 'search_goal_to_tag') {
                new_url = '<?php echo site_url('admin/campaign/list_campaign?search_by='.$search_by.'&search_name='.$search_name.'&search_status='.$search_status.'&search_goal_per='.$search_goal_per.'&search_goal_from='.$search_goal_from.'&search_goal_to=&search_category='.$search_category);?>';
            }
            if (id == 'search_category_tag') {
                new_url = '<?php echo site_url('admin/campaign/list_campaign?search_by='.$search_by.'&search_name='.$search_name.'&search_status='.$search_status.'&search_goal_per='.$search_goal_per.'&search_goal_from='.$search_goal_from.'&search_goal_to='.$search_goal_to.'&search_category=');?>';
            }

            window.location.href = new_url;
        });
        $("#filterClear").click(function () {
            $("#filterPill .filter-pill").remove();
            $(this).remove();
            window.location.href = '<?php echo site_url('admin/campaign/list_campaign'); ?>';
        });
    });
</script>

   



   



   



