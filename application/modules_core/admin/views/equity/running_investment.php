
<!-- BEGIN CONTAINER -->


<div id="container" class="row-fluid">


<!-- BEGIN SIDEBAR -->



<?php echo $this->load->view('admin_sidebar'); ?>



<!-- END SIDEBAR -->


<!-- BEGIN PAGE -->


<!-- BEGIN PAGE -->

<div id="main-content">


<!-- BEGIN PAGE CONTAINER-->


<div class="container-fluid">


<!-- BEGIN PAGE HEADER-->


<div class="row-fluid">


    <div class="span12">


        <!-- BEGIN THEME CUSTOMIZER-->


        <div id="theme-change" class="hidden-phone">


            <i class="icon-cogs"></i>



                            <span class="settings">



                            <span class="text"><?php echo THEME ?>:</span>



                            <span class="colors">



                                <span class="color-default" data-style="default"></span>



                            <span class="color-gray" data-style="gray"></span>



                            <span class="color-purple" data-style="purple"></span>



                            <span class="color-navy-blue" data-style="navy-blue"></span>



                            </span>



                            </span>


        </div>


        <!-- END THEME CUSTOMIZER-->


        <!-- BEGIN PAGE TITLE & BREADCRUMB-->


        <h3 class="page-title">


            <?php echo RUNNING_INVESTMENT; ?>


        </h3>


        <ul class="breadcrumb">


            <li>


                <a href="<?php echo site_url('admin/home/dashboard'); ?>"><i class="icon-home"></i></a><span
                    class="divider">&nbsp;</span>


            </li>


            <li><a href="#"><?php echo RUNNING_INVESTMENT; ?></a><span class="divider-last">&nbsp;</span></li>


        </ul>


        <!-- END PAGE TITLE & BREADCRUMB-->


    </div>


</div>






<!-- END PAGE HEADER-->


<!-- BEGIN ADVANCED TABLE widget-->


<div class="row-fluid">


<div class="span12">


<!-- BEGIN EXAMPLE TABLE widget-->


<div class="widget widget-tabs widget-tabs-left">
<div class="widget-title">
                            <h4><i class="icon-reorder"></i><?php echo RUNNING_INVESTMENT; ?></h4>
                            <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                            </span>
                        </div>

    <div class="widget-body">
        <table class="table table-striped table-bordered width100I" id="sample_1">

            <thead>
            <tr>
                <th class="mob_hidden"><?php echo NOS;?></th>
                <th><?php echo INVESTOR;?></th>
                <th class="mob_hidden"><?php echo CREATED_DATE;?></th>
                <th class="mob_hidden"><?php echo INVESTMENT_STATUS;?></th>
                <th><?php echo INVESTOR_ACTION;?></th>
                <th><?php echo PROJECT;?></th>
                <th class="mob_hidden"><?php echo END_DATE;?></th>
                
                <th><?php echo OWNER_ACTION;?></th>
            </tr>
            </thead>

            <tbody>
            <?php
            $project_url = 'properties';
            if ($total_running_investment) {
                $i = 1;
                foreach ($total_running_investment as $investor) {


                    $investor_user_name = $investor['user_name'] . ' ' . $investor['last_name'];
                    $invest_status_id = $investor['invest_status_id'];
                    $campaign_id = $investor['campaign_id'];
                    $property_id = $investor['property_id'];
                    $investor_user_id = $investor['user_id'];
                    $created_date = $investor['created_date'];
                    $investment_close_date = $investor['investment_close_date'];

                    $property = GetOneProperty($property_id);
                    $property_address = $property['property_address'];
                    $property_url = $property['property_url'];
                    $in_process_id = $investor['id'];
                    $investor_profile_slug = $investor['profile_slug'];
                    $campaign_owner_id = $investor['campaign_owner_id'];
                   
                    $investor_image = $investor['image'];
                    if ($investor_image != '' && is_file("upload/user/user_small_image/" . $investor_image)) {
                        $investor_image_src = base_url() . 'upload/user/user_small_image/' . $investor_image;
                    } else {
                        $investor_image_src = base_url() . 'upload/user/user_small_image/no_man.jpg';
                    }


                    if ($invest_status_id == 1) {

                        $status = PROCESSING_CONTRACT;

                    } elseif ($invest_status_id == 2) {

                        $status = DOWNLOADED_CONTRACT;
                    } elseif ($invest_status_id == 3) {

                        $status = UPLOADED_SIGNED_CONTRACT;
                    } elseif ($invest_status_id == 4) {

                        $status = SIGNED_CONTRACT_APPROVE;
                    } elseif ($invest_status_id == 5) {

                        $status = PAYMENT_PROCESS;
                    } elseif ($invest_status_id == 6) {

                        $status = PAYMENT_RECIEPT_UPLOADED;
                    } elseif ($invest_status_id == 7) {

                        $status = PAYMENT_CONFIRMED;
                    } elseif ($invest_status_id == 8) {

                        $status = TRACKING_SHIPMENT;
                    } elseif ($invest_status_id == 9) {

                        $status = DOCUMENT_REJECTED;
                    } elseif ($invest_status_id == 10) {

                        $status = TRACKING_SHIPMENT_CONFIRMED;
                    } else {

                        $status = TRACKING_SHIPMENT;

                    } ?>
                    <tr class="odd gradeX">
                        <td class="mob_hidden"><?php echo $i; ?></td>
                        <td><a href="<?php echo site_url('user/' . $investor_profile_slug) ?>" target="_blank">
                                          <span class="creater fl">
                                          <img src="<?php echo $investor_image_src; ?>">
                                          </span>
                                <span class="creater-name"><?php echo $investor_user_name; ?></span></a></td>
                        <td class="mob_hidden">
                            <?php echo dateformat($created_date);?>
                        </td>
                        <td class="mob_hidden"><?php echo $status; ?></td>
                        <td class="actions">
                            <a href="<?php echo site_url('investment/adminstep/' . $in_process_id); ?>"
                               class="item btnView tooltips" data-placement="bottom"
                               data-original-title="Investment Process" target="_blank"><i class="icon-money"></i></a>
                            <a href="<?php echo site_url('admin/message/view_message/' . $property_id . '/' . $investor_user_id); ?>"
                               class="item btnMessage tooltips" data-placement="bottom"
                               data-original-title="Send message" target="_blank"><i class="icon-envelope"></i></a>
                        </td>
                         <td class="mob_hidden">
                            <a href="<?php echo site_url($project_url.'/' . $property_url) ?>" target="_blank"><?php echo $property_address;?></a>
                        </td>
                         <td class="mob_hidden">
                            <?php echo dateformat($investment_close_date);?>
                        </td>
                         <td class="actions">
                            <a href="<?php echo site_url('admin/campaign/campaign_detail/' . $property_url); ?>"
                               class="item btnView tooltips" data-placement="bottom"
                               data-original-title="View Details" target="_blank"><i class="icon-eye-open"></i></a>
                            <a href="<?php echo site_url('admin/message/view_message/' . $property_id . '/' . $equity_owner_id); ?>"
                               class="item btnMessage tooltips" data-placement="bottom"
                               data-original-title="Send message" target="_blank"><i class="icon-envelope"></i></a>
                        </td>
                    </tr>

                    <?php


                    $i++;
                }


            }?>


            </tbody>
        </table>
    </div>



<!-- END EXAMPLE TABLE widget-->


</div>


</div>


<!-- END ADVANCED TABLE widget-->


<!-- END PAGE CONTENT-->


</div>


<!-- END PAGE CONTAINER-->


</div>


<!-- END PAGE -->


</div>

<!-- END PAGE CONTAINER-->


</div>


<!-- END PAGE -->


</div>


<!-- END CONTAINER -->


<!-- BEGIN JAVASCRIPTS -->


<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>


<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/jquery.dataTables.js"></script>


<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/DT_bootstrap.js"></script>


<script src="<?php echo base_url(); ?>js/scripts.js"></script>


<script type="text/javascript" charset="utf-8">


    jQuery(document).ready(function () {


        App.init();


    });
    $(function () {
        $("body").on("change", "#contract_copy_file", function (event) {


            $('#investor_deal_document_image').children().remove();
            $(this).prependTo('#investor_deal_document_image').addClass('hidden-file-field');

            $('#investor_deal_document_form').submit();
            $(this).prependTo("#contract_copy_file_display").removeClass('hidden-file-field');
        });


    });

    // bind form id and provide a simple callback function
    if ((pic = jQuery('#investor_deal_document_form')).length)
        pic.ajaxForm({
            dataType: 'json',
            beforeSend: function () {
                // alert('test');
                $('.ploader').fadeIn();
            },
            success: function (result) {


                if (result.msg.success != '') {
                    $(".document_copy").hide();
                    $("#upload_document_link").show();
                    $("#upload_document_link").html(result.document_file.path);
                    $(".success_document_copy").show();
                }
                if (result.contract_copy_file_error != '') {
                    $(".success_document_copy").hide();
                    $(".upload_copy_error").html(result.contract_copy_file_error);
                    $(".document_copy").show();

                }
            },


        });


</script>

<link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet"/>
<script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap-fileupload.js"></script>



   



   



   



