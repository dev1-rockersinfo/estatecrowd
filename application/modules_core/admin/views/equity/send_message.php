<html>
<title></title>
<head>
    <style type="text/css">
        .pt20_app {

            padding-top: 40px;
        }

        .pd15 {
            padding: 15px 10px !important;
        }

        .mess {
            text-align: center;
            color: red;
        }

    </style>
    <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
    <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet"/>


</head>
<body>
<?php
$attributes = array('class' => 'form-horizontal');
echo form_open('admin/equity_contract/send_message', $attributes);

?>

<div class="pt20_app">


    <?php if ($errors) { ?>
        <div class="mess">
            <p><?php echo $errors; ?></p>
        </div>
    <?php } ?>

    <div class="control-group">
        <label class="control-label"><?php echo SUBJECT; ?></label>

        <div class="controls">
            <input type="text" placeholder="<?php echo SUBJECT; ?>" class="input-xlarge pd15" name="subject"
                   id="subject" value=""/>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label"><?php echo MESSAGE; ?> </label>

        <div class="controls">
            <textarea name="comments" id="comments1" rows="6" cols="60"></textarea>
        </div>
    </div>

    <input type="hidden" name="equity_id" value="<?php echo $equity_id; ?>">
    <input type="hidden" name="user_id" value="<?php echo $user_id; ?>">

    <div class="form-actions">

        <button type="submit" class="btn blue" name="submit" value="Submit">
            <i class="icon-ok"></i> <?php echo SEND_MESSAGE; ?></button>

    </div>
    </form>
    <!-- END FORM-->
</div>
</body>
</html>
