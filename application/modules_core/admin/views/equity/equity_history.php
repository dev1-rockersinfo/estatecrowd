<html>
<title></title>
<head>
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
    <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet"/>

</head>
<body>
<div class="bs-example" data-example-id="bordered-table">
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>No</th>
            <th><?php echo STATUS; ?></th>
            <th><?php echo DOCUMENT_NAME; ?></th>
            <th><?php echo DATE; ?></th>
        </tr>
        </thead>
        <tbody>
        <?php
        if ($result) {

            $i = 1;
            foreach ($result as $value) {


                $invest_status_id = $value['invest_status_id'];
                $update_date = $value['update_date'];


                if ($invest_status_id == 1) {

                    $status = PROCESSING_CONTRACT;

                } elseif ($invest_status_id == 2) {

                    $status = DOWNLOADED_CONTRACT;
                } elseif ($invest_status_id == 3) {

                    $status = UPLOADED_SIGNED_CONTRACT;
                    $document_name = $value['document_name'];
                } elseif ($invest_status_id == 4) {

                    $status = SIGNED_CONTRACT_APPROVE;
                } elseif ($invest_status_id == 5) {

                    $status = PAYMENT_PROCESS;
                } elseif ($invest_status_id == 6) {

                    $status = PAYMENT_RECIEPT_UPLOADED;
                    $document_name = $value['acknowledge_doc'];
                } elseif ($invest_status_id == 7) {

                    $status = PAYMENT_CONFIRMED;
                } else {

                    $status = TRACKING_SHIPMENT;

                }
                ?>

                <tr>
                    <th scope="row"><?php echo $i; ?></th>
                    <td><?php echo $status; ?></td>
                    <td><?php echo $document_name; ?></td>
                    <td><?php echo $update_date; ?></td>
                </tr>
                <?php
                $i++;
            }
        } else {
            ?>
            <tr>
                <td><?php echo NO_RECORDS_AVAILABLE; ?></td>
            </tr>

        <?php
        }

        ?>
        </tbody>
    </table>
</div>

</body>
</html>
