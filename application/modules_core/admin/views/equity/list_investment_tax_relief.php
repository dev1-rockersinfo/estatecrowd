<script type="text/javascript" language="javascript">
    function delete_rec(id) {
        var ans = confirm("<?php echo DELETE_INVESTOR_TYPE_CONFIRMATION;?>");
        if (ans) {
            location.href = "<?php echo site_url('admin/equity/delete_investment_tax_relief'); ?>" + "/" + id + "/";
        } else {
            return false;
        }
    }

</script>
<!-- BEGIN CONTAINER -->
<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php echo $this->load->view('admin_sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN THEME CUSTOMIZER-->
                    <div id="theme-change" class="hidden-phone">
                        <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text"><?php echo THEME; ?>:</span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
                    </div>
                    <!-- END THEME CUSTOMIZER-->
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        <?php echo INVESTMENT_TAX_RELIEF; ?>
                    </h3>
                    <ul class="breadcrumb">
                        <li>
                            <a href="<?php echo site_url('admin/home/dashboard'); ?>"><i class="icon-home"></i></a><span
                                class="divider">&nbsp;</span>
                        </li>

                        <li><a href="#"><?php echo INVESTMENT_TAX_RELIEF; ?> </a><span class="divider-last">&nbsp;</span>
                        </li>
                    </ul>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <?php if ($msg != '') {
                if ($msg == 'insert') {
                    ?>
                    <div class="alert alert-success">
                        <button class="close" data-dismiss="alert">x</button>
                        <strong><?php echo SUCCESS; ?>!</strong> <?php echo NEW_RECORD_HAS_BEEN_ADDED_SUCCESSFULLY; ?>
                    </div>

                <?php
                }
                if ($msg == 'update') {
                    ?>
                    <div class="alert alert-success">
                        <button class="close" data-dismiss="alert">x</button>
                        <strong><?php echo SUCCESS; ?>!</strong> <?php echo RECORD_HAS_BEEN_UPDATED_SUCCESSFULLY; ?>
                    </div>
                <?php
                }
                if ($msg == 'delete') {
                    ?>
                    <div class="alert alert-success">
                        <button class="close" data-dismiss="alert">x</button>
                        <strong><?php echo SUCCESS; ?>!</strong> <?php echo RECORD_HAS_BEEN_DELETED_SUCCESSFULLY; ?>
                    </div>
                <?php
                }

            }?>
            <!-- BEGIN ADVANCED TABLE widget-->
        <!--     <div class="fr iconM">


                <button type="button" class="btn mini purple"
                        onclick="location.href='<?php echo site_url('admin/equity/add_investment_tax_relief'); ?>'"><i
                        class="icon-plus"></i> <?php echo ADD_INVESTOR_TAX_RELIEF; ?></button>
            </div> -->

            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN EXAMPLE TABLE widget-->

                    <div class="widget">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i><?php echo INVESTMENT_TAX_RELIEF; ?>   </h4>
                            <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                            </span>
                        </div>
                        <div class="widget-body">
                            <?php
                            if ($result) {
                                $i = 1; ?>
                                <table class="table table-striped table-bordered trans" id="sample_1">
                                    <thead>
                                    <tr>
                                        <th><?php echo NO; ?></th>
                                        <th><?php echo INVESTOR_TYPE; ?></th>
                                         <th><?php echo TITLE; ?></th>
                                        
                                        <th><?php echo STATUS; ?></th>
                                        <th><?php echo ACTION; ?></th>

                                    </tr>
                                    </thead>
                                    <tbody>

                                    <?php foreach ($result as $row) {
                                        $investment_tax_relief_name = $row['investment_tax_relief_name'];
                                        $investment_tax_relief_desc = $row['investment_tax_relief_desc'];
                                         $investment_tax_relief_type = $row['investment_tax_relief_type'];
                                        $id = $row['id'];
                                        $s = $row['status'];
                                        $status = "Inactive";
                                        if ($s) {
                                            $status = "Active";
                                        }
                                        ?>
                                        <tr class="odd gradeX">

                                            <td><?php echo $i; ?></td>
                                             <td><?php echo $investment_tax_relief_type; ?></td>
                                            <td><?php echo ucfirst($investment_tax_relief_name); ?></td>
                                             
                                            <td><?php echo ucfirst($status); ?></td>
                                            <td><?php echo anchor('admin/equity/add_investment_tax_relief/' . $id, EDIT); ?>
                                               <!--  / <a href="#"
                                                     onClick="delete_rec('<?php echo $id; ?>')"><?php echo DELETE; ?></a> -->
                                            </td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                    ?>

                                    </tbody>
                                </table>
                            <?php
                            } else {
                                ?>
                                <div align="center">
                                    <b><?php echo NO_RECORD_FOUND; ?> </b>
                                </div>
                            <?php } ?>

                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE widget-->
                </div>
            </div>

            <!-- END ADVANCED TABLE widget-->

            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>
<!-- END CONTAINER -->

<!-- BEGIN JAVASCRIPTS -->

<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/DT_bootstrap.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        App.init();
    });
</script>
