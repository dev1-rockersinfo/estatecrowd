<script type="text/javascript" language="javascript">

    function delete_rec(id) {

        var ans = confirm("<?php echo DELETE_USER_CONFIRMATION;?>");

        if (ans) {

            location.href = "<?php echo site_url('admin/user/delete_user/'); ?>" + "/" + id;

        } else {

            return false;

        }

    }


</script>


<style>
 
    .dataTables_filter label input[type=search] {

        width: 125px !important;

    }

    #example_length select {

        width: 125px !important;

    }

    @media (max-width: 788px) {

        .tfoot {

            display: none;
            left: 316px;

        }

    }

        .tfoot {

            left: 337px;

        }


</style>

<!-- BEGIN CONTAINER -->

<div id="container" class="row-fluid">

<!-- BEGIN SIDEBAR -->

<?php echo $this->load->view('admin_sidebar'); ?>

<!-- END SIDEBAR -->

<!-- BEGIN PAGE -->

<div id="main-content">

<!-- BEGIN PAGE CONTAINER-->

<div class="container-fluid">

<!-- BEGIN PAGE HEADER-->

<div class="row-fluid">

    <div class="span12">

        <!-- BEGIN THEME CUSTOMIZER-->

        <div id="theme-change" class="hidden-phone">

            <i class="icon-cogs"></i>

                        <span class="settings">

                            <span class="text">Theme:</span>

                            <span class="colors">

                                <span class="color-default" data-style="default"></span>

                                <span class="color-gray" data-style="gray"></span>

                                <span class="color-purple" data-style="purple"></span>

                                <span class="color-navy-blue" data-style="navy-blue"></span>

                            </span>

                        </span>

        </div>

        <!-- END THEME CUSTOMIZER-->

        <!-- BEGIN PAGE TITLE & BREADCRUMB-->

        <h3 class="page-title">

            <?php echo USERS; ?>


        </h3>

        <ul class="breadcrumb">

            <li>

                <a href="<?php echo site_url('admin/home/dashboard') ?>"><i class="icon-home"></i></a><span
                    class="divider">&nbsp;</span>

            </li>


            <li><a href="<?php echo site_url('admin/user/list_user') ?>"><?php echo USER_LIST; ?></a><span
                    class="divider-last">&nbsp;</span></li>

        </ul>

        <!-- END PAGE TITLE & BREADCRUMB-->

    </div>

</div>

<!-- END PAGE HEADER-->

<?php

if ($msg != "") {

    if ($msg == "insert") {
        ?>
        <div class="alert alert-success">

            <button class="close" data-dismiss="alert">x</button>

            <strong><?php echo SUCCESS; ?>!</strong> <?php echo NEW_RECORD_HAS_BEEN_ADDED_SUCCESSFULLY; ?></div>

    <?php
    }

    if ($msg == "update") {
        ?>

        <div class="alert alert-success">

            <button class="close" data-dismiss="alert">x</button>

            <strong><?php echo SUCCESS; ?>!</strong> <?php echo RECORD_HAS_BEEN_UPDATED_SUCCESSFULLY; ?></div>

    <?php
    }

    if ($msg == "delete") {
        ?>

        <div class="alert alert-success">

            <button class="close" data-dismiss="alert">x</button>

            <strong><?php echo SUCCESS; ?>!</strong> <?php echo RECORD_HAS_BEEN_DELETED_SUCCESSFULLY; ?></div>

    <?php
    }
}

?>



<a href="<?php echo site_url('admin/user/add_user'); ?>" class="btn mini purple fr marB5"><i
        class="icon-plus"></i> <?php echo ADD_USER; ?></a>

<!-- BEGIN ADVANCED TABLE widget-->

<div class="row-fluid">

<div class="span12">

<!-- BEGIN EXAMPLE TABLE widget-->

<div class="widget">

<div class="widget-title">

    <h4><i class="icon-reorder"></i><?php echo ADD_USER; ?></h4>

                            <span class="tools">

                                <a href="javascript:;" class="icon-chevron-down"></a>

                            </span>

</div>


<div class="widget-body">

    <table class="table table-striped table-bordered list_user" id="example">


        <thead>


        <tr>

            <!--<th><?php echo NO; ?></th>       -->

            <th class="hidden-phone"><?php echo USERNAME ?></th>

            <th><?php echo EMAIL; ?></th>

            <th class="hidden-phone tab_hidden"><?php echo SIGNUP_IP_ADDRESS; ?></th>

            <th class="hidden-phone"><?php echo ADDRESS; ?></th>

            <!-- <th class="hidden-phone">City</th>

             <th class="hidden-phone">State</th>

             <th class="hidden-phone">Country</th>

             <th class="hidden-phone tab_hidden">Zip Code</th>-->

            <th><?php echo ACTIVE; ?></th>

            <th class="hidden-phone"><?php echo REGISTERED_ON; ?></th>

            <th><?php echo ACTION; ?></th>

        </tr>

        </thead>


        <thead class="tfoot">

        <tr>

            <th style="border:0px;"></th>

            <th style="border:0px;"></th>

            <th colspan="5" style="border:0px;"></th>


        </tr>

        </thead>

        <tbody>

        <?php

        if ($result) {

            $i = 0;

            foreach ($result as $row) {

                $user_name = $row->user_name;

                $user_email = $row->email;

                $user_signup_ip = $row->signup_ip;

                $user_address = $row->address;

                $user_city = get_city_name($row->city);

                $user_state = get_state_name($row->state);

                $user_country = get_country_name($row->country);

                $user_zip = $row->zip_code;

                if (intval($row->active) == 1) {

                    $user_status = ACTIVE;

                } else if (intval($row->active) == 2) {
                    $user_status = SUSPEND;
                } else {

                    $user_status = INACTIVE;

                }

                $user_date_added = ($row->date_added != "") ? date('d/m/Y', strtotime($row->date_added)) : 'N/A';

                ?>

                <div id="myModal_<?php echo $row->user_id; ?>" class="modal hide fade" tabindex="-1" role="dialog"
                     aria-labelledby="myModalLabel2" aria-hidden="true">

                    <div class="modal-header">

                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                        <h3 id="myModalLabel2"><?php echo ADDRESS; ?></h3><!-- change by darshan -->

                    </div>

                    <div class="modal-body">

                        <p><?php echo $user_address; ?></p>

                    </div>

                    <div class="modal-footer">

                        <button data-dismiss="modal" class="btn btn-primary"><?php echo OK; ?></button>
                        <!-- change by darshan -->

                    </div>

                </div>



                <tr class="odd gradeX">

                    <!--<td><?php echo $i + 1; ?></td>-->

                    <td class="hidden-phone"><?php echo $user_name; ?></td>

                    <td><?php echo $user_email; ?></td>

                    <td class="hidden-phone tab_hidden"><a href="http://whatismyipaddress.com/ip/<?php echo $user_signup_ip; ?>" target="_blank"><?php echo $user_signup_ip; ?></a></td>

                    <td class="hidden-phone"><a href="#myModal_<?php echo $row->user_id; ?>" role="button"
                                                class="btn btn-link" data-toggle="modal"><?php echo VIEW; ?></a></td>

                    <!-- <td class="hidden-phone"><?php echo $user_city; ?></td>

                                    <td class="hidden-phone"><?php echo $user_state; ?></td>

                                    <td class="hidden-phone"><?php echo $user_country; ?></td>

                                    <td class="hidden-phone tab_hidden"><?php echo $user_zip; ?></td>-->

                    <td><?php echo $user_status; ?></td>

                    <td class="hidden-phone"><?php echo $user_date_added; ?></td>

                    <td><?php echo anchor('admin/user/edit_user/' . $row->user_id . '/', EDIT); ?>

                        <?php $chk = $this->user_model->chk_user_project($row->user_id);

                        if ($chk == '1' || $chk == 1) {
                        } else {
                            ?>

                            / <a href="#" onclick="delete_rec('<?php echo $row->user_id; ?>')"><?php echo DELETE; ?></a>

                        <?php } ?>

                    </td>

                </tr>

                <?php

                $i++;

            }

        }

        ?>


        </tbody>


    </table>


</div>

</div>

<!-- END EXAMPLE TABLE widget-->

</div>

</div>


<!-- END ADVANCED TABLE widget-->


<!-- END PAGE CONTENT-->

</div>

<!-- END PAGE CONTAINER-->

</div>

<!-- END PAGE -->

</div>

<!-- END CONTAINER -->


<!-- BEGIN JAVASCRIPTS -->

<!-- Load javascripts at bottom, this will reduce page load time -->


<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/jquery.dataTables.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/DT_bootstrap.js"></script>

<script src="<?php echo base_url(); ?>js/scripts.js"></script>

<script>


    jQuery(document).ready(function () {

        // initiate layout and plugins

        App.init();

    });


</script>


<script type="text/javascript" charset="utf-8">

    (function ($) {

        /*

         * Function: fnGetColumnData

         * Purpose:  Return an array of table values from a particular column.

         * Returns:  array string: 1d data array

         * Inputs:   object:oSettings - dataTable settings object. This is always the last argument past to the function

         *           int:iColumn - the id of the column to extract the data from

         *           bool:bUnique - optional - if set to false duplicated values are not filtered out

         *           bool:bFiltered - optional - if set to false all the table data is used (not only the filtered)

         *           bool:bIgnoreEmpty - optional - if set to false empty values are not filtered from the result array

         * Author:   Benedikt Forchhammer <b.forchhammer /AT\ mind2.de>

         */

        $.fn.dataTableExt.oApi.fnGetColumnData = function (oSettings, iColumn, bUnique, bFiltered, bIgnoreEmpty) {

            // check that we have a column id

            if (typeof iColumn == "undefined") return new Array();


            // by default we only want unique data

            if (typeof bUnique == "undefined") bUnique = true;


            // by default we do want to only look at filtered data

            if (typeof bFiltered == "undefined") bFiltered = true;


            // by default we do not want to include empty values

            if (typeof bIgnoreEmpty == "undefined") bIgnoreEmpty = true;


            // list of rows which we're going to loop through

            var aiRows;


            // use only filtered rows

            if (bFiltered == true) aiRows = oSettings.aiDisplay;

            // use all rows

            else aiRows = oSettings.aiDisplayMaster; // all row numbers


            // set up data array

            var asResultData = new Array();


            for (var i = 0, c = aiRows.length; i < c; i++) {

                iRow = aiRows[i];

                var aData = this.fnGetData(iRow);

                var sValue = aData[iColumn];


                // ignore empty values?

                if (bIgnoreEmpty == true && sValue.length == 0) continue;



                // ignore unique values?

                else if (bUnique == true && jQuery.inArray(sValue, asResultData) > -1) continue;



                // else push the value onto the result data array

                else asResultData.push(sValue);

            }


            return asResultData;

        }
    }(jQuery));


    function fnCreateSelect(aData) {

        var r = '<select style="width:125px;"><option value=""><?php echo SELECT; ?></option>', i, iLen = aData.length;

        for (i = 0; i < iLen; i++) {
             var div = document.createElement("div");
             div.innerHTML = aData[i];
            r += '<option value="' + div.textContent + '">' + div.textContent + '</option>';

        }
        // console.log( r + '</select>');
        return r + '</select>';

    }


    $(document).ready(function () {

        /* Initialise the DataTable */
         $.extend($.fn.dataTableExt.oSort,{
            "date-uk-pre": function ( a ) {
                var ukDatea = a.split('/');
                return (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
            },

            "date-uk-asc": function ( a, b ) {
                return ((a < b) ? -1 : ((a > b) ? 1 : 0));
            },

            "date-uk-desc": function ( a, b ) {
                return ((a < b) ? 1 : ((a > b) ? -1 : 0));
            }
        } );
        var oTable = $('#example').dataTable({
            "aoColumnDefs": [{'bSortable': false, 'aTargets': [3]}],
             "aoColumns": [
                null,
                null,
                null,
                null,
                null,
                { "sType": "date-uk" },
                null
            ],
            "oLanguage": {


                "sLengthMenu": "<?php echo DISPLAY; ?> _MENU_ <?php echo RECORD_PER_PAGE; ?>&nbsp;&nbsp;",
                "sZeroRecords": "<?php echo NOTHING_FOUND_SORRY; ?>",
                "sInfo": "<?php echo SHOWING; ?> _START_ to _END_ of _TOTAL_ <?php echo RECORD; ?>",
                "sInfoEmpty": "<?php echo SHOWING; ?> 0 to 0 of 0 <?php echo RECORD; ?>",
                "sSearch": "<?php echo SEARCH; ?>: ",
                'oPaginate': {

                    'sPrevious': '<?php echo PREVIOUS; ?>',
                    'sNext': '<?php echo NEXT; ?>'

                }

            }

        });


        /* Add a select menu for each TH element in the table footer */

        $(".tfoot th").each(function (i) {

            this.innerHTML = fnCreateSelect(oTable.fnGetColumnData(i));

            $('select', this).change(function () {

                oTable.fnFilter($(this).val(), i);

            });

        });

    });

</script>
