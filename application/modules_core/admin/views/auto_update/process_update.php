<!-- BEGIN CONTAINER -->
<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php echo $this->load->view('admin_sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN THEME CUSTOMIZER-->
                    <div id="theme-change" class="hidden-phone">
                        <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text"><?php echo THEME; ?>:</span><!-- change by darshan -->
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
                    </div>
                    <!-- END THEME CUSTOMIZER-->
                    <h3 class="page-title">
                        <?php echo UPDATE.' '.VERSION; ?>
                    </h3>
                    <ul class="breadcrumb">
                        <li>
                            <a href="<?php echo base_url() ?>admin/home/dashboard"><i class="icon-home"></i></a><span
                                class="divider">&nbsp;</span>
                        </li>

                        <li><a href="#"><?php echo UPDATE.' '.VERSION; ?></a><span class="divider-last">&nbsp;</span></li>
                    </ul>
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                <div class="span12 sortable">
                    <!-- BEGIN SAMPLE FORMPORTLET-->
                    <div class="widget">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i><?php echo UPDATE.' '.VERSION; ?></h4>
                                        <span class="tools">
                                        <a href="javascript:;" class="icon-chevron-down"></a>
                                        <a href="javascript:;" class="icon-remove"></a>
                                        </span>
                        </div>
                        <div class="widget-body">
                            <!-- BEGIN FORM-->
                           
                            <form method="post" name="update_form" id="update_form" action="<?php echo site_url('admin/update/process_update/'); ?>">
 <input type="hidden" id="file_type_write" name="file_type_write" value="1" />
</form>
                            
          <div id="form_wizard_1">

                <div class="modal-header">

                     <h4><?php echo UPDATE; ?> - <span class="step-title"><?php echo STEP_1_OF_4; ?></span></h4>

                </div>

                <div class="container-fluid">

                <!-- BEGIN PAGE HEADER-->   

               <h3 class="page-title">

                    </h3>

                <!-- END PAGE HEADER-->

                <!-- BEGIN PAGE CONTENT-->

                <div class="row-fluid">

                   <div class="span12">

                         <div class="widget-body form">

                            <form action="#" class="form-horizontal">

                               <div class="form-wizard">

                                  <div class="navbar steps clearfix">

                                     <div class="navbar-inner">

                                        <ul class="row-fluid">

                                           <li class="span3">

                                              <a href="#tab1" data-toggle="tab" class="step active">

                                              <span class="number">1</span>

                                              <span class="desc"><i class="icon-ok"></i> <?php echo STEP_1; ?></span>

                                              </a>

                                           </li>

                                           <li class="span3">

                                              <a href="#tab2" data-toggle="tab" class="step">

                                              <span class="number">2</span>

                                              <span class="desc"><i class="icon-ok"></i> <?php echo STEP_2; ?></span>

                                              </a>

                                           </li>

                                           <li class="span3">

                                              <a href="#tab3" data-toggle="tab" class="step">

                                              <span class="number">3</span>

                                              <span class="desc"><i class="icon-ok"></i> <?php echo STEP_3; ?></span>

                                              </a>

                                           </li>

                                           <li class="span3">

                                              <a href="#tab4" data-toggle="tab" class="step">

                                              <span class="number">4</span>

                                              <span class="desc"><i class="icon-ok"></i> <?php echo FINAL_STEP; ?></span>

                                              </a> 

                                           </li>

                                        </ul>

                                     </div>

                                  </div>

                                  

                                  <div class="tab-content">

                                     <div class="tab-pane active" id="tab1">

                                        <h4><?php echo FILL_UP_STEP_1; ?></h4>

                                        <div class="bs-docs-example_12">

                                         

                                        </div>

                                        

                                     </div>

                                     <div class="tab-pane" id="tab2">

                                        <h4><?php echo FILL_UP_STEP_2; ?></h4>

                                        <div class="bs-docs-example_12">

                                          

                                        </div>

                                     </div>

                                     <div class="tab-pane" id="tab3">

                                        <h4><?php echo FILL_UP_STEP_3; ?></h4>

                                        <div class="bs-docs-example_12 clearfix">

                                         

                                        </div>

                                <div class="clear"></div> 

                                     

                                     

                                     </div>                                     

                                     <div class="tab-pane" id="tab4">

                                        <h4><?php echo FINAL_STEP; ?></h4>

                                        <div class="bs-docs-example_12">

                                          <p><?php echo OCCASIONALLY_WE_MAY_RELEASE_SMALL_UPDATE_IN_THE_FORM_OF_A_PATCH_TO_ONE_OR_SEVERAL_SOURCE_FILE; ?>

                                            </p>

                                        </div>

                                     </div>

                                  </div>

                                  

                               </div>
                                
                                
                               
                            </form>

                            

                         </div>

                   </div>

                </div>

                <!-- END PAGE CONTENT-->

             </div>         

                <div class="modal-footer clearfix">

                                     <a href="javascript:;" class="btn button-previous">

                                     <i class="icon-angle-left"></i> <?php echo BACK; ?>  

                                     </a>

                                     <a href="javascript:;" class="btn btn-primary blue button-next">

                                      <?php echo CONTINUE1; ?> <i class="icon-angle-right"></i>

                                     </a>

                                     <a href="javascript:;" class="btn btn-success button-submit button-submit1">

                                      <?php echo SUBMIT; ?> <i class="icon-ok"></i>

                                     </a>
                    
                                    <a href="javascript:;" class="btn btn-warning button-overwrite">

                                      <?php echo "Overwrite"; ?> <i class="icon-angle-right"></i>

                                     </a>
                    
                                    <a href="javascript:;" class="btn btn-success button-merge">

                                      <?php echo "Merge"; ?> <i class="icon-angle-right"></i>

                                     </a>

                </div>

            </div>

                            
                            
                            
                            
                            <!-- END FORM-->
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>

            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>
<!-- END CONTAINER -->


<style>

#modal_ajax a{

    background: #4d4d4d!important;

    color: #ffffff; 

}


</style>

<!-- BEGIN JAVASCRIPTS -->
 
<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<script>
    jQuery(document).ready(function(){
        // initiate layout and plugins
        App.init();
        
        $.ajax({            
            type: 'POST',            
            url:'<?php echo site_url('admin/update/step1/'); ?>',            
            dataType: 'html',            
            success: function(html){
                $('#tab1 div').html(html);                
                /*   $('#tab1 h2').html(data.step1_data.title);  //alert(data.step1_data.title);                
                         $('#tab1 p').html(data.step1_data.description);  //alert(data.step1_data.description);                
                         $('#tab2 p').html(data.step1_data.instructions);  //alert(data.step1_data.instructions);                
                         alert(data.allfiles);
                
                 */     
            },            
            complete:function(){                
                step2();                
            }            
        }); 
            
    function step2(){
        
        $.ajax({            
            type: 'POST',            
            url:'<?php echo base_url('admin/update/step2/'); ?>',            
            dataType: 'html',            
            success: function(html){
                $('#tab2 div').html(html);                
            },            
            complete:function(){
                step3();
            }
        }); 
    }
    
    function step3(){
        
        $.ajax({            
            type: 'POST',            
            url:'<?php echo base_url('admin/update/list_update/'); ?>',            
            dataType: 'html',            
            success: function(html){  
                $('#tab3 .clearfix').html(html);
            }
        }); 
    } 
    
    $('.button-submit1').click(function(){
        $('#update_form').submit();
    });    
    
});
</script>
<script src="<?php echo base_url(); ?>js/bootstrap.wizard.min.js"></script>
<!-- END JAVASCRIPTS -->
