<!-- BEGIN CONTAINER -->
<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php echo $this->load->view('admin_sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN THEME CUSTOMIZER-->
                    <div id="theme-change" class="hidden-phone">
                        <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text"><?php echo THEME; ?>:</span><!-- change by darshan -->
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
                    </div>
                    <!-- END THEME CUSTOMIZER-->
                    <h3 class="page-title">
                        <?php echo UPDATE; ?>
                    </h3>
                    <ul class="breadcrumb">
                        <li>
                            <a href="<?php echo base_url() ?>admin/home/dashboard"><i class="icon-home"></i></a><span
                                class="divider">&nbsp;</span>
                        </li>

                        <li><a href="#"><?php echo UPDATE; ?></a><span class="divider-last">&nbsp;</span></li>
                    </ul>
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                <div class="span12 sortable">
                    <!-- BEGIN SAMPLE FORMPORTLET-->
                    <div class="widget">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i><?php echo UPDATE; ?></h4>
                                        <span class="tools">
                                        <a href="javascript:;" class="icon-chevron-down"></a>
                                        <a href="javascript:;" class="icon-remove"></a>
                                        </span>
                        </div>
                        <div class="widget-body">
                            <!-- BEGIN FORM-->
                            <?php if ($msg != '') {
                                    $error_class='success';
                                if ($msg == "yes") { $error = YOUR_VERSION_UPDATED_SUCCESSFULLY; }
                                elseif ($msg == "back") { $error = YOUR_BACKUP_VERSION_UPDATED_SUCCESSFULLY; }
                                elseif ($msg == "no_back") { $error = THERE_IS_SOME_PROBLEM_WITH_UPDATING_YOUR_BACK_UP_VERSION; $error_class='danger'; }
                                else { $error_class='danger'; $error = urldecode($msg); } 
                                ?>
                                <div class="alert alert-<?php echo $error_class; ?>">
                                    <button class="close" data-dismiss="alert">x</button>
                                    <strong><?php echo SUCCESS; ?>! </strong> <?php echo $error; ?></div>
                                <?php if ($msg == "yes") { ?>
                                    <div class="form-actions">
                                        <button type="submit" class="btn blue"
                                                onClick="location.href='<?php echo site_url('admin/update/backupfile'); ?>'">
                                            <i class="icon-ok"></i><?php echo DO_YOU_WANT_TO_UPDATE_YOUR_BACK_UP_AGAIN_THEN_CLICK_HERE; ?>
                                        </button>
                                    </div>

                                <?php
                                }
                            }?>
                            <!-- END FORM-->
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>

            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>
<!-- END CONTAINER -->

<!-- BEGIN JAVASCRIPTS -->

<script type="text/javascript"
        src="<?php echo base_url(); ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        App.init();
    });
</script>
<!-- END JAVASCRIPTS -->
