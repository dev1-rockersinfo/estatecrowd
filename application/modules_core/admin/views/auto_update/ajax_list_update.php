<style>
    .mainsourcetitle{
        
    }
    
    .mainsourcetitle .bs-docs-exampleleft{
    width:47%;
    float: left;  
    
}

.mainsourcetitle .bs-docs-exampleright{
    width:47%;
    float: left;
    margin-left: 2%;
    padding: 5px;
}
    
.filesname{
   
    margin-bottom: 15px;
}

.filesname .bs-docs-exampleleft{
    width:47%;
    float: left;  
    padding: 5px;
     background: #f5f5f5;
    color: #333;
}

.filesname .bs-docs-exampleright{
    width:47%;
    float: left;
    margin-left: 2%;
    padding: 5px;
     background: #f5f5f5;
    color: #333;
}

.filediffsborder{
  
     border:1px solid #ccc;
    overflow-x:scroll;
    border-radius: 5px 5px;
    margin-bottom: 15px;
  
}
table.diff{
   
        
}
table.diff tr td {
    padding: 5px;
    width:47%;
}

.diffUnmodified {
     background-color: #ffffff;
    color: #333;
}
.diffBlank{
    background-color: #f5f5f5;
    color: #333;
}
.diffDeleted{
    background-color: #fee8e9;
    color: #333;
}
.diffInserted {
    background-color: #dfd;
    color: #333;
}
</style>
<?php
if ($msg == '') {
    ?>
    <?php
    if (count($allfiles) > 0) {

        ?>

<div class="mainsourcetitle">
        <div class="bs-docs-exampleleft">
            <div class="list_title"><h3><?php echo SOURCE_FILE_NAME; ?></h3></div>
           <?php /* <ul>
                <?php   foreach ($allfiles as $key => $val) {
                    ?>
                    <li><?php echo $val ?></li>

                <?php } ?>
            </ul> */ ?>

        </div>

        <div class="bs-docs-exampleright">
            <div class="list_title"><h3><?php echo DESTINATION_FILE_NAME; ?></h3></div>
         <?php /*   <ul>
                <?php   foreach ($allfiles as $key => $val) {
                    ?>
                    <li><?php
                        $version_folder = $this->session->userdata('version_folder');
                        echo str_replace($version_folder, "", $val)?></li>

                <?php } ?>
            </ul> */ ?>
        </div>
    
    </div>


    <?php
    $version_folder = $this->session->userdata('version_folder');
    
    
     
     
         foreach ($allfiles as $key => $val) {

             $new_file_path=$val;
              $old_file_path=str_replace($version_folder, "", $val);
             

             // compare two files character by character
  ?>
<div class="equityfund">
    
    <div class="filesname">
        <div class="bs-docs-exampleleft">           
            <?php echo $old_file_path; ?>        
        </div>

        <div class="bs-docs-exampleright">
           <?php echo $new_file_path; ?>
        </div>
        
        <div class="clear clearfix" />
    </div>
        <div class="filediffsborder">
                 <?php echo CstFileDiff::toTable(CstFileDiff::compareFiles($old_file_path, $new_file_path)); ?>
            <div class="clear clearfix" />
        </div>
    </div>
    <?php          
              
	
         }
         
         echo $res;
 
    } else {
        ?>

        <h4><?php echo NO_UPDATE_FOUND; ?>.</h4>

    <?php
    }
    ?>

<?php
} else {
    if ($msg == "yes") echo YOUR_VERSION_UPDATED_SUCCESSFULLY . '<input type="button" value="' . DO_YOU_WANT_TO_UPDATE_YOUR_BACK_UP_AGAIN_THEN_CLICK_HERE . '..."  onclick="location.href=\'' . site_url('update/backupfile') . '\'" >';// change by darshan
    if ($msg == "back") echo YOUR_BACKUP_VERSION_UPDATED_SUCCESSFULLY;// change by darshan
    if ($msg == "no_back") echo THERE_IS_SOME_PROBLEM_WITH_UPDATING_YOUR_BACK_UP_VERSION;// change by darshan
}
?>
        