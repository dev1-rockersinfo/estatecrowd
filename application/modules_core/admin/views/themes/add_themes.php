<?php //echo error_reporting(E_ALL);?>
<link href="<?php echo base_url(); ?>css/bootstrap-colorpicker.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css"
      href="<?php echo base_url(); ?>assets/bootstrap-colorpicker/css/colorpicker.css"/>


<div id="container" class="row-fluid">
<!-- BEGIN SIDEBAR -->
<?php echo $this->load->view('admin_sidebar'); ?>
<!-- END SIDEBAR -->
<!-- BEGIN PAGE -->
<div id="main-content">
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN THEME CUSTOMIZER-->
        <div id="theme-change" class="hidden-phone">
            <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text">Theme:</span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
        </div>
        <!-- END THEME CUSTOMIZER-->
        <h3 class="page-title">
            Themes
        </h3>
        <ul class="breadcrumb">
            <li>
                <a href="<?php echo site_url('admin/home/dashboard') ?>"><i class="icon-home"></i></a><span
                    class="divider">&nbsp;</span>
            </li>

        </ul>
    </div>
</div>

<!-- END PAGE HEADER-->
<?php
if ($update_msg != "") {
    ?>
    <div class="alert alert-success">
        <button class="close" data-dismiss="alert">x</button>
        <?php echo $update_msg; ?></div>
<?php
}
?>

<div class="span12 sortable">
<!-- BEGIN SAMPLE FORMPORTLET-->
<div class="widget">

<div class="widget-title">
    <h4><i class="icon-reorder"></i> <?php echo THEMES; ?></h4><!-- change  by darshan -->
                                        <span class="tools">
                                        <a href="javascript:;" class="icon-chevron-down"></a>
                                        <a href="javascript:;" class="icon-remove"></a>
                                        </span>
</div>
<div class="widget-body">
<!-- BEGIN FORM-->
<?php
$attributes = array('class' => 'form-horizontal');
echo form_open('admin/themes/index', $attributes);
?>

<div class="control-group">
    <label class="control-label"><?php echo COLOR_ORANGE; ?></label><!-- change  by darshan -->
    <div class="col-md-4 col-xs-11 controls">
        <div data-color-format="rgb" data-color="<?php echo $color_orange; ?>"
             class="input-append colorpicker-default color">
            <input type="text" class="form-control input-xlarge" name="color_orange"
                   value="<?php echo $color_orange; ?>" readonly/>
                                          
                                              
                                              <span class=" input-group-btn add-on">
                                                  <button class="btn btn-white" type="button" style="padding: 0px">
                                                      <i style="background-color: rgb(124, 66, 84);"></i>
                                                  </button>
                                              </span>
        </div>

    </div>
</div>
<div class="control-group">
    <label class="control-label"><?php echo COLOR_DARKORANGE; ?></label><!-- change  by darshan -->
    <div class=" controls col-md-4 col-xs-11 ">
        <div data-color-format="rgb" data-color="<?php echo $color_darkorange; ?>"
             class="input-append colorpicker-default color">
            <input type="text" class="input-xlarge form-control" name="color_darkorange"
                   id="color_darkorange" value="<?php echo $color_darkorange; ?>" readonly/>
                                            <span class=" input-group-btn add-on">
                                                  <button class="btn btn-white" type="button" style="padding: 0px">
                                                      <i style="background-color: rgb(124, 66, 84);"></i>
                                                  </button>
                                              </span>
        </div>
    </div>
</div>
<div class="control-group">
    <label class="control-label"><?php echo COLOR_GREY; ?></label><!-- change  by darshan -->
    <div class="col-md-4 col-xs-11 controls">
        <div data-color-format="rgb" data-color="<?php echo $color_grey; ?>"
             class="input-append colorpicker-default color">
            <input type="text" class="form-control input-xlarge" name="color_grey" id="color_grey"
                   value="<?php echo $color_grey; ?>" readonly/>
                                              <span class=" input-group-btn add-on">
                                                  <button class="btn btn-white" type="button" style="padding: 0px">
                                                      <i style="background-color: rgb(124, 66, 84);"></i>
                                                  </button>
                                              </span>
        </div>

    </div>
</div>

<div class="control-group">
    <label class="control-label"><?php echo COLOR_BLACK; ?></label><!-- change  by darshan -->
    <div class="col-md-4 col-xs-11 controls">
        <div data-color-format="rgb" data-color="<?php echo $color_black; ?>"
             class="input-append colorpicker-default color">
            <input type="text" class="form-control input-xlarge" name="color_black" id="color_black"
                   value="<?php echo $color_black; ?>" readonly/>
                                          <span class=" input-group-btn add-on">
                                                  <button class="btn btn-white" type="button" style="padding: 0px">
                                                      <i style="background-color: rgb(124, 66, 84);"></i>
                                                  </button>
                                              </span>
        </div>

    </div>
</div>
<div class="control-group">
    <label class="control-label"><?php echo COLOR_WHITE; ?></label><!-- change  by darshan -->
    <div class="col-md-4 col-xs-11 controls">
        <div data-color-format="rgb" data-color="<?php echo $color_white; ?>"
             class="input-append colorpicker-default color">
            <input type="text" class="form-control input-xlarge" name="color_white" id="color_white"
                   value="<?php echo $color_white; ?>" readonly/>
                                          <span class=" input-group-btn add-on">
                                                  <button class="btn btn-white" type="button" style="padding: 0px">
                                                      <i style="background-color: rgb(124, 66, 84);"></i>
                                                  </button>
                                              </span>
        </div>

    </div>
</div>
<div class="control-group">
    <label class="control-label"><?php echo COLOR_LIGHTGREY; ?></label><!-- change  by darshan -->
    <div class="col-md-4 col-xs-11 controls">
        <div data-color-format="rgb" data-color="<?php echo $color_lightgrey; ?>"
             class="input-append colorpicker-default color">
            <input type="text" class="form-control input-xlarge" name="color_lightgrey" id="color_lightgrey"
                   value="<?php echo $color_lightgrey; ?>" readonly/>
                                         	 <span class=" input-group-btn add-on">
                                                  <button class="btn btn-white" type="button" style="padding: 0px">
                                                      <i style="background-color: rgb(124, 66, 84);"></i>
                                                  </button>
                                              </span>
        </div>

    </div>
</div>
<div class="control-group">
    <label class="control-label"><?php echo COLOR_BLUE; ?></label><!-- change  by darshan -->
    <div class="col-md-4 col-xs-11 controls">
        <div data-color-format="rgb" data-color="<?php echo $color_blue; ?>"
             class="input-append colorpicker-default color">
            <input type="text" class="form-control input-xlarge" name="color_blue" id="color_blue"
                   value="<?php echo $color_blue; ?>" readonly/>
                                         	 <span class=" input-group-btn add-on">
                                                  <button class="btn btn-white" type="button" style="padding: 0px">
                                                      <i style="background-color: rgb(124, 66, 84);"></i>
                                                  </button>
                                              </span>
        </div>

    </div>
</div>
<div class="control-group">
    <label class="control-label"><?php echo COLOR_DARKBLUE; ?></label><!-- change  by darshan -->
    <div class="col-md-4 col-xs-11 controls">
        <div data-color-format="rgb" data-color="<?php echo $color_darkblue; ?>"
             class="input-append colorpicker-default color">
            <input type="text" class="form-control input-xlarge" name="color_darkblue" id="color_darkblue"
                   value="<?php echo $color_darkblue; ?>" readonly/>
                                          	 <span class=" input-group-btn add-on">
                                                  <button class="btn btn-white" type="button" style="padding: 0px">
                                                      <i style="background-color: rgb(124, 66, 84);"></i>
                                                  </button>
                                              </span>
        </div>

    </div>
</div>
<div class="control-group">
    <label class="control-label"><?php echo COLOR_CCC; ?></label><!-- change  by darshan -->
    <div class="col-md-4 col-xs-11 controls">
        <div data-color-format="rgb" data-color="<?php echo $color_ccc; ?>"
             class="input-append colorpicker-default color">
            <input type="text" class="form-control input-xlarge" name="color_ccc" id="color_ccc"
                   value="<?php echo $color_ccc; ?>" readonly/>
                                            <span class=" input-group-btn add-on">
                                                  <button class="btn btn-white" type="button" style="padding: 0px">
                                                      <i style="background-color: rgb(124, 66, 84);"></i>
                                                  </button>
                                              </span>
        </div>

    </div>
</div>
<?php //print_r($style_font1);?>
<!-- ---------------------------------------------------------------------------------- -->
<div class="control-group">
    <label class="control-label"><?php echo FONT_STYLE; ?>1</label><!-- change  by darshan -->
    <div class="col-md-4 col-xs-11 controls">
        <select name="style_font1">

            <option
                value="Arial, Helvetica, sans-serif" <?php if ($style_font1 == 'Arial, Helvetica, sans-serif') {
                echo "selected";
            } ?>>Arial, Helvetica, sans-serif
            </option>
            <option
                value='"Arial Black", Gadget, sans-serif' <?php if ($style_font1 == '"Arial Black", Gadget, sans-serif') {
                echo "selected";
            } ?>>"Arial Black", Gadget, sans-serif
            </option>
            <option
                value='"Comic Sans MS", cursive, sans-serif' <?php if ($style_font1 == '"Comic Sans MS", cursive, sans-serif') {
                echo "selected";
            } ?>>"Comic Sans MS", cursive, sans-serif
            </option>
            <option
                value='Impact, Charcoal, sans-serif' <?php if ($style_font1 == 'Impact, Charcoal, sans-serif') {
                echo "selected";
            } ?>>Impact, Charcoal, sans-serif
            </option>
            <option
                value='"Lucida Sans Unicode", "Lucida Grande", sans-serif' <?php if ($style_font1 == '"Lucida Sans Unicode", "Lucida Grande", sans-serif') {
                echo "selected";
            } ?>>"Lucida Sans Unicode", "Lucida Grande", sans-serif
            </option>
            <option
                value='Tahoma, Geneva, sans-serif' <?php if ($style_font1 == 'Tahoma, Geneva, sans-serif') {
                echo "selected";
            } ?>>Tahoma, Geneva, sans-serif
            </option>
            <option
                value='"Trebuchet MS", Helvetica, sans-serif' <?php if ($style_font1 == '"Trebuchet MS", Helvetica, sans-serif') {
                echo "selected";
            } ?>>"Trebuchet MS", Helvetica, sans-serif
            </option>
            <option
                value='Verdana, Geneva, sans-serif' <?php if ($style_font1 == 'Verdana, Geneva, sans-serif') {
                echo "selected";
            } ?>>Verdana, Geneva, sans-serif
            </option>

        </select>

    </div>
</div>
<div class="control-group">
    <label class="control-label"><?php echo FONT_STYLE; ?>2</label><!-- change  by darshan -->
    <div class="col-md-4 col-xs-11 controls">
        <select name="style_font2">
            <option
                value="Arial, Helvetica, sans-serif" <?php if ($style_font2 == 'Arial, Helvetica, sans-serif') {
                echo "selected";
            } ?>>Arial, Helvetica, sans-serif
            </option>
            <option
                value='"Arial Black", Gadget, sans-serif' <?php if ($style_font2 == '"Arial Black", Gadget, sans-serif') {
                echo "selected";
            } ?>>"Arial Black", Gadget, sans-serif
            </option>
            <option
                value='"Comic Sans MS", cursive, sans-serif' <?php if ($style_font2 == '"Comic Sans MS", cursive, sans-serif') {
                echo "selected";
            } ?>>"Comic Sans MS", cursive, sans-serif
            </option>
            <option
                value='Impact, Charcoal, sans-serif' <?php if ($style_font2 == 'Impact, Charcoal, sans-serif') {
                echo "selected";
            } ?>>Impact, Charcoal, sans-serif
            </option>
            <option
                value='"Lucida Sans Unicode", "Lucida Grande", sans-serif' <?php if ($style_font2 == '"Lucida Sans Unicode", "Lucida Grande", sans-serif') {
                echo "selected";
            } ?>>"Lucida Sans Unicode", "Lucida Grande", sans-serif
            </option>
            <option
                value='Tahoma, Geneva, sans-serif' <?php if ($style_font2 == 'Tahoma, Geneva, sans-serif') {
                echo "selected";
            } ?>>Tahoma, Geneva, sans-serif
            </option>
            <option
                value='"Trebuchet MS", Helvetica, sans-serif' <?php if ($style_font2 == '"Trebuchet MS", Helvetica, sans-serif') {
                echo "selected";
            } ?>>"Trebuchet MS", Helvetica, sans-serif
            </option>
            <option
                value='Verdana, Geneva, sans-serif' <?php if ($style_font2 == 'Verdana, Geneva, sans-serif') {
                echo "selected";
            } ?>>Verdana, Geneva, sans-serif
            </option>

        </select>

    </div>
</div>
<!-- -------------------------------------------------------------------------------------- -->

<div class="form-actions">
    <button type="submit" class="btn blue" name="submit" value="Submit"><i
            class="icon-ok"></i> <?php echo SUBMIT; ?></button>
    <input type="button" name="save" value="Preview" class="btn btn-lg btn-send" id="preview_btn">
    <a href="<?php echo base_url('admin/themes/set_default'); ?>"><input type="button" name="view"
                                                                         value="Set Default"
                                                                         class="btn btn-lg btn-send"></a>

</div>
</form>
<!-- END FORM-->
</div>
</div>
<!-- END SAMPLE FORM PORTLET-->
</div>
</div>

<!-- END PAGE CONTENT-->
</div>
<!-- END PAGE CONTAINER-->
</div>
<!-- END PAGE -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->

<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS -->
<!-- Load javascripts at bottom, this will reduce page load time -->


<script type="text/javascript"
        src="<?php echo base_url(); ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>


<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins

        App.init();

    });
</script>
<script type="text/javascript"
        src="<?php echo base_url(); ?>assets/bootstrap-fileupload/bootstrap-fileupload.js"></script>
<script type="text/javascript"
        src="<?php echo base_url(); ?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script src="<?php echo base_url(); ?>js/common-scripts.js"></script>
<script type="text/javascript">
    $("#preview_btn").on("click", function () {

        window.open('<?php echo base_url()."/preview/#u=".base_url()."#home|1024|768|1";?>', '_blank');


    });
</script>
