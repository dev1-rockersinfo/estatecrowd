<!-- BEGIN CONTAINER -->
<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php echo $this->load->view('admin_sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN THEME CUSTOMIZER-->
                    <div id="theme-change" class="hidden-phone">
                        <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text"><?php echo THEME; ?>:</span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
                    </div>
                    <!-- END THEME CUSTOMIZER-->
                    <h3 class="page-title">

                        <?php if ($property_types->id != '') {
                            echo EDIT_CATEGORY;
                        } else {
                            echo ADD_CATEGORY;
                        } ?>
                    </h3>
                    <ul class="breadcrumb">
                        <li>
                            <a href="<?php echo site_url('admin/home/dashboard') ?>"><i class="icon-home"></i></a><span
                                class="divider">&nbsp;</span>
                        </li>

                        <li>
                            <a href="<?php echo site_url('admin/category'); ?>"> <?php echo CATEGORY_LIST; ?></a><span
                                class="divider-last">&nbsp;</span></li>
                    </ul>
                </div>
            </div>

            <!-- END PAGE HEADER-->
            <?php
            if ($errors != "") {
                ?>
                <div class="alert alert-error">
                    <button class="close" data-dismiss="alert">x</button>
                    <strong><?php echo ERRORS; ?>!</strong>

                    <p> <?php echo $errors; ?></p>
                </div>

            <?php } ?>
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                <div class="span12 sortable">
                    <!-- BEGIN SAMPLE FORMPORTLET-->
                    <div class="widget">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i>  <?php if ($property_types->id != '') {
                                    echo 'Edit Property type';
                                } else {
                                    echo  'Add Property type';
                                } ?></h4>
                             
                                        <span class="tools">
                                        <a href="javascript:;" class="icon-chevron-down"></a>
                                        <a href="javascript:;" class="icon-remove"></a>
                                        </span>
                        </div>
                        <div class="widget-body">
                            <!-- BEGIN FORM-->
                            <?php
                            $attributes = array('name' => 'frm_campany_category', 'class' => 'form-horizontal');
                            if($property_types->id)echo form_open_multipart('admin/property_type/edit_property_type/' .$property_types->id, $attributes);
                            else echo form_open_multipart('admin/property_type/add_property_type', $attributes);
                            ?>

                            <div class="control-group">
                                <label class="control-label"><?php echo 'Name'; ?><font color="red"> *</font></label>

                                <div class="controls">
                                    <input type="text" name="name"
                                           id="name" value="<?=$property_types->name?>"
                                           class="input-xlarge"/>

                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label"><?php echo CATEGORY_DESCRIPTION; ?></label>

                               <div class="controls">
                                    <textarea type="text" name="description"
                                           id="description" rows="4" 
                                           class="input-xlarge"> <?=$property_types->description?></textarea>

                                </div>
                            </div>
                         
                            <div class="control-group">
                                <label class="control-label"><?php echo LANGUAGE; ?></label>

                                <div class="controls">
                                    <select name="language_id" id="language_id">
                                        <option value=""><?=SELECT?></option>
                                        <?php 
                                        if($language){
                                            foreach ($language as $lang) {
                                                $select = $lang['language_id']==$property_types->language_id?'selected':'';
                                                echo '<option value="'.$lang['language_id'].'" '.$select.'>'.$lang['language_name'].'</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label"><?php echo TYPE; ?></label>

                                <div class="controls">
                                    <select name="is_parent" id="is_parent">
                                        <option value=""><?=SELECT?></option>
                                        <option value="1" <?php if($property_types->is_parent==='1') echo 'selected'?>><?=MAIN?></option>
                                        <option value="0" <?php if($property_types->is_parent==='0') echo 'selected'?>><?=RELATED?></option>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label"><?php echo STATUS; ?></label>

                                 <div class="controls">
                                    <select name="status" id="status">
                                        <option value=""><?=SELECT?></option>
                                        
                                        <option value="1" <?php if($property_types->status==='1') echo 'selected'?>><?=ACTIVE?></option>
                                        <option value="0" <?php if($property_types->status==='0') echo 'selected'?>><?=INACTIVE?></option>

                                    </select>
                                </div>
                            </div>



                            <div class="form-actions">
                                <?php
                                if ($property_types->id == "") {
                                    ?>
                                    <button type="submit" class="btn blue" onclick="return validate();"><i
                                            class="icon-ok"></i> <?php echo SUBMIT; ?></button>
                                <?php
                                } else {
                                    ?>
                                    <button type="submit" class="btn blue"><i class="icon-ok"></i> <?php echo UPDATE; ?>
                                    </button>
                                <?php
                                }
                                ?>
                                <button type="button" class="btn"
                                        onClick="location.href='<?php echo site_url('admin/property_type'); ?>'">
                                    <i class=" icon-remove"></i> <?php echo CANCEL; ?></button>
                            </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>

            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN JAVASCRIPTS -->
<script type="text/javascript"
        src="<?php echo base_url(); ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>

<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        App.init();
    });
</script>
  
