<div id="container" class="row-fluid">
<!-- BEGIN SIDEBAR -->
<?php echo $this->load->view('admin_sidebar'); ?>
<!-- END SIDEBAR -->
<!-- BEGIN PAGE -->
<div id="main-content">
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN THEME CUSTOMIZER-->
        <div id="theme-change" class="hidden-phone">
            <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text"><?php echo THEME; ?></span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
        </div>
        <!-- END THEME CUSTOMIZER-->
        <h3 class="page-title">
            <?php echo ADD_NEWSLETTER_JOB; ?>
        </h3>
        <ul class="breadcrumb">
            <li>
                <a href="#"><i class="icon-home"></i></a><span class="divider">&nbsp;</span>
            </li>

            <li><a href="#"><?php echo ADD_NEWSLETTER_JOB; ?> </a><span class="divider-last">&nbsp;</span></li>
        </ul>
    </div>
</div>
<!-- END PAGE HEADER-->
<?php if ($error != '') {
    ?>
    <div class="alert alert-error">
        <button class="close" data-dismiss="alert">x</button>
        <strong><?php echo ERRORS; ?>!</strong> <?php echo $error; ?></div>
<?php
}
?>
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
<div class="span12 sortable">
<!-- BEGIN SAMPLE FORMPORTLET-->
<div class="widget">
<div class="widget-title">
    <h4><i class="icon-reorder"></i><?php echo ADD_NEWSLETTER; ?> </h4>
                                        <span class="tools">
                                        <a href="javascript:;" class="icon-chevron-down"></a>
                                        <a href="javascript:;" class="icon-remove"></a>
                                        </span>
</div>
<div class="widget-body">
<!-- BEGIN FORM-->
<form action="<?php echo site_url('admin/newsletter/add_newsletter_job') ?>" class="form-horizontal"
      name="newsletter_job" method="post">
<div class="control-group">
    <label class="control-label">*<?php echo SELECT_NEWSLETTER; ?></label>

    <div class="controls">
        <select class="input-large m-wrap" tabindex="1" id="newsletter_id" name="newsletter_id">
            <?php
            if ($all_newsletter) {
                foreach ($all_newsletter as $newsletter) {
                    $newsletterid = $newsletter->newsletter_id;
                    $subject = $newsletter->subject;
                    if ($newsletterid == $newsletter_id) $select = 'selected="selected"';
                    else $select = '';
                    echo '<option value="' . $newsletterid . '" ' . $select . '> ' . $subject . '</option>';
                }
            }
            ?>
        </select>
    </div>
</div>


<div class="control-group">
    <label class="control-label">*<?php echo SELECT_USER; ?></label>

    <div class="controls">
        <select class="input-large m-wrap" tabindex="1" name="select_user" id="select_user" onChange="user_type();">
            <option value=""><?php echo SELECT; ?></option>
            <option value="1"><?php echo SUBSCRIBER; ?></option>
            <option value="2"><?php echo REGISTERED; ?></option>
            <option value="3"><?php echo BOTH; ?></option>
        </select>
    </div>
</div>

<div class="control-group" style="display:none;" id="subscribe">
    <label class="control-label"><?php echo SUBSCRIBER_USER_LIST; ?></label>

    <div class="controls">
        <select class="input-large m-wrap" tabindex="1" multiple="multiple" name="subscribe_user[]" id="subscribe_user">
            <?php
            if ($user_subscribe) {
                foreach ($user_subscribe as $subscribe) {
                    $semail = $subscribe['email'];

                    echo '<option value="' . $semail . '"> ' . $semail . '</option>';
                }
            }
            ?>
        </select>
    </div>
</div>

<div class="control-group" style="display:none;" id="register">
    <label class="control-label"><?php echo REGISTER_USER_LIST; ?></label>

    <div class="controls">
        <select class="input-large m-wrap" tabindex="1" multiple="multiple" name="register_user[]" id="register_user">
            <?php
            if ($user_register) {
                foreach ($user_register as $register) {
                    $remail = $register['email'];

                    echo '<option value="' . $remail . '">' . $remail . '</option>';
                }
            }
            ?>
        </select>
    </div>
</div>
<div class="control-group" style="display:none;" id="both">
    <label class="control-label"><?php echo ALL_USER; ?></label>

    <div class="controls">
        <select class="input-large m-wrap" tabindex="1" multiple="multiple" name="both_user[]" id="both_user">
            <?php
            if ($user_both) {
                foreach ($user_both as $both) {
                    $bemail = $both['email'];
                    $buser_name = $both['user_name'];
                    $blast_name = $both['last_name'];
                    echo '<option value="' . $bemail . '"> ' . $bemail . '</option>';
                }
            }
            ?>
        </select>
    </div>
</div>

<div class="control-group">
    <label class="control-label">*<?php echo RECURSIVE; ?></label>

    <div class="controls">
        <select class="input-large m-wrap" tabindex="1" name="recursive" id="recursive" onChange="select_recursive();">
            <option value=""><?php echo SELECT; ?></option>
            <option value="yes"><?php echo YES; ?></option>
            <option value="no"><?php echo NOS; ?></option>

        </select>
    </div>
</div>


<div id="yes_recusive" style="display:none;">

    <div class="control-group">
        <label class="control-label"><?php echo NEWSLETTER_TYPE; ?> </label>

        <div class="controls">
            <select class="input-large m-wrap" tabindex="1" name="news_type" id="news_type"
                    onChange="newsletter_type();">
                <option value=""><?php echo SELECT; ?></option>
                <option value="daily"><?php echo DAILY; ?></option>
                <option value="weekly"><?php echo WEEKLY; ?></option>
                <option value="monthly"><?php echo MONTHLY; ?></option>
                <option value="duration"><?php echo DURATION; ?></option>

            </select>
        </div>
    </div>


    <div class="control-group" style="display:none;" id="weekly">
        <label class="control-label"><?php echo SELECT_A_DAY_OF_WEEK; ?> </label>

        <div class="controls">
            <select class="input-large m-wrap" tabindex="1" name="week_day" id="week_day">
                <?php $orig_date = strtotime(date('Y-m-d'));
                echo '<option value="">Select</option>';

                for ($i = 0; $i < 7; $i++) {
                    $date = strtotime("+" . $i . " day", $orig_date);
                    $dates = date('Y-m-d', $date);
                    $timestamp = strtotime($dates);
                    $day = date('l', $timestamp);
                    echo '<option value=' . $dates . '>' . $day . '</option>';
                }
                ?>

            </select>
        </div>
    </div>

    <div class="control-group" style="display:none;" id="month">
        <label class="control-label"><?php echo SELECT_A_DAY; ?> </label>

        <div class="controls">
            <select class="input-large m-wrap" tabindex="1" name="month_day" id="month_day">
                <?php $orig_date = strtotime(date('Y-m-01'));
                echo '<option value="">Select</option>';

                for ($i = 0; $i < 30; $i++) {
                    $date = strtotime("+" . $i . " day", $orig_date);
                    $dates = date('Y-m-d', $date);
                    echo '<option value=' . $dates . '>' . $dates . '</option>';
                }
                ?>


            </select>
        </div>
    </div>
    <div id="spec_duration" style="display:none;">
        <div class="control-group">
            <label class="control-label"><?php echo SELECT_START_DATE; ?></label>

            <div class="controls">
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-calendar"></i></span>
                    <input type="text" placeholder="01/01/2012" class="input-xlarge date-picker" name="start_date"
                           id="start_date"/>
                </div>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label"><?php echo SELECT_END_DATE; ?></label>

            <div class="controls">
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-calendar"></i></span>
                    <input type="text" placeholder="01/01/2012" class="input-xlarge date-picker" name="end_date"
                           id="end_date"/>
                </div>
            </div>
        </div>


    </div>

</div>
<div class="control-group" id="job_start">
    <label class="control-label"><?php echo JOB_START_DATE ?></label>

    <div class="controls">
        <div class="input-prepend">
            <span class="add-on"><i class="icon-calendar"></i></span>
            <input type="text" placeholder="01/01/2012" class="input-xlarge date-picker" name="job_start_date"
                   id="job_start_date"/>
        </div>
    </div>
</div>
<!--  <div class="control-group">
       <label class="control-label">Default datepicker</label>
       <div class="controls">
           <input class=" m-ctrl-medium date-picker" size="16" type="text" value="12-02-2012">
       </div>
   </div> -->
<div class="form-actions">
    <button type="submit" class="btn blue"><i class="icon-ok"></i> <?php echo SUBMIT; ?></button>
    <button type="button" class="btn"
            onClick="location.href='<?php echo site_url('admin/newsletter/newsletter_job'); ?>'"><i
            class=" icon-remove"></i> <?php echo CANCEL; ?></button>
</div>
</form>
<!-- END FORM-->
</div>
</div>
<!-- END SAMPLE FORM PORTLET-->
</div>
</div>

<!-- END PAGE CONTENT-->
</div>
<!-- END PAGE CONTAINER-->
</div>
<!-- END PAGE -->
</div>
<script>
    function user_type() {
        var chk = document.getElementById('select_user').value;

        if (chk == '1') {
            document.getElementById('subscribe').style.display = 'block';
            document.getElementById('register').style.display = 'none';
            document.getElementById('both').style.display = 'none';
        }
        if (chk == '2') {
            document.getElementById('register').style.display = 'block';
            document.getElementById('both').style.display = 'none';
            document.getElementById('subscribe').style.display = 'none';
        }
        if (chk == '3') {
            document.getElementById('both').style.display = 'block';
            document.getElementById('register').style.display = 'none';
            document.getElementById('subscribe').style.display = 'none';
        }
    }

    function select_recursive() {
        var chk_rec = document.getElementById('recursive').value;
        //alert(chk_rec);
        if (chk_rec == 'yes') {
            //document.getElementById('job_start').style.display='none';
            document.getElementById('yes_recusive').style.display = 'block';

        }
        else {
            //document.getElementById('job_start').style.display='block';
            document.getElementById('yes_recusive').style.display = 'none';
        }
    }
    function newsletter_type() {
        var chk_news = document.getElementById('news_type').value;
        //alert(chk_news);
        if (chk_news) {
            document.getElementById('yes_recusive').style.display = 'block';

        }
        if (chk_news == 'daily') {
            document.getElementById('spec_duration').style.display = 'none';
            document.getElementById('month').style.display = 'none';
            document.getElementById('weekly').style.display = 'none';
            document.getElementById('job_start').style.display = 'block';

        }
        if (chk_news == 'weekly') {
            document.getElementById('spec_duration').style.display = 'none';
            document.getElementById('month').style.display = 'none';
            document.getElementById('weekly').style.display = 'block';
            document.getElementById('job_start').style.display = 'block';
        }
        if (chk_news == 'monthly') {
            document.getElementById('spec_duration').style.display = 'none';
            document.getElementById('month').style.display = 'block';
            document.getElementById('weekly').style.display = 'none';
            document.getElementById('job_start').style.display = 'block';
        }
        if (chk_news == 'duration') {
            document.getElementById('spec_duration').style.display = 'block';
            document.getElementById('month').style.display = 'none';
            document.getElementById('weekly').style.display = 'none';
            document.getElementById('job_start').style.display = 'none';
        }

    }
</script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/bootstrap-datepicker/css/datepicker.css"/>
<script type="text/javascript"
        src="<?php echo base_url() ?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url() ?>js/scripts.js"></script>
<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        App.init();
        $('.date-picker').datepicker();
    });
</script>
  
