<script type="text/javascript" language="javascript">

    function setchecked(elemName, status) {

        elem = document.getElementsByName(elemName);

        for (i = 0; i < elem.length; i++) {

            elem[i].checked = status;

        }

    }


    function setaction(elename, actionval, actionmsg, formname) {

        vchkcnt = 0;

        elem = document.getElementsByName(elename);


        for (i = 0; i < elem.length; i++) {

            if (elem[i].checked) vchkcnt++;

        }

        if (vchkcnt == 0) {

            alert("<?php echo DELETE_SELECT_RECORD_CONFIRMATION;?>")

        } else {


            if (confirm(actionmsg)) {

                document.getElementById('action').value = actionval;

                document.getElementById(formname).submit();

            }


        }

    }


    function chk_valid() {


        var keyword = document.getElementById('keyword').value;


        if (keyword == '') {

            alert('Please enter search keyword');

            return false;


        }


        else {

            return true;

        }


    }


    function getlimit(limit) {

        if (limit == '0') {

            return false;

        }

        else {

            window.location.href = '<?php echo site_url('admin/newsletter/list_newsletter_user/');?>' + '/' + limit;

        }


    }


    function getsearchlimit(limit) {

        if (limit == '0') {

            return false;

        }

        else {


            window.location.href = '<?php echo site_url('admin/newsletter/search_newsletter_user');?>' + '/' + limit + '/' + '<?php echo $option.'/'.$keyword; ?>';

        }


    }


    function gomain(x) {


        if (x == 'all') {

            window.location.href = '<?php echo site_url('admin/newsletter/list_newsletter_user');?>';

        }

    }

</script>
<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php echo $this->load->view('admin_sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN THEME CUSTOMIZER-->
                    <div id="theme-change" class="hidden-phone">
                        <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text"><?php echo THEME; ?></span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
                    </div>
                    <!-- END THEME CUSTOMIZER-->
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        <?php echo NEWLETTERS_USERS; ?>
                    </h3>
                    <ul class="breadcrumb">
                        <li>
                            <a href="<?php echo site_url('admin/home/dashboard') ?>"><i class="icon-home"></i></a><span
                                class="divider">&nbsp;</span>
                        </li>

                        <li><a href="#"><?php echo NEWLETTERS_USERS; ?> </a><span class="divider-last">&nbsp;</span>
                        </li>
                    </ul>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <?php if ($msg != '') {


                if ($msg == 'delete') {
                    ?>
                    <div class="alert alert-success">
                        <button class="close" data-dismiss="alert">x</button>
                        <strong><?php echo SUCCESS; ?>!</strong> <?php echo RECORDS_HAS_BEEN_DELETED_SUCCESSFULLY; ?>
                    </div>


                <?php
                }
                if ($msg == 'insert') {
                    ?>
                    <div class="alert alert-success">
                        <button class="close" data-dismiss="alert">x</button>
                        <strong><?php echo SUCCESS; ?>!</strong> <?php echo NEW_RECORD_HAS_BEEN_ADDED_SUCCESSFULLY; ?>
                    </div>



                <?php
                }
                if ($msg == 'update') {
                    ?>
                    <div class="alert alert-success">
                        <button class="close" data-dismiss="alert">x</button>
                        <strong><?php echo SUCCESS; ?>!</strong> <?php echo RECORD_HAS_BEEN_UPDATED_SUCCESSFULLY; ?>
                    </div>

                <?php
                }
            } ?>


            <!-- BEGIN ADVANCED TABLE widget-->
            <div class="fr iconM">
                <button type="button" class="btn mini purple"
                        onclick="location.href='<?php echo site_url('admin/newsletter/add_newsletter_user') ?>'"><i
                        class="icon-plus"></i> <?php echo ADD; ?></button>
                <button type="button" class="btn mini purple"
                        onclick="setaction('chk[]','delete', 'Are you sure, you want to delete selected record(s)?', 'frm_listuser')">
                    <i class="icon-remove-sign"></i> <?php echo DELETE; ?> </button>

                <button type="button" class="btn mini purple"
                        onclick="location.href='<?php echo site_url('admin/newsletter/export_newsletter_user/' . strtotime(date('H:i:s'))); ?>'">
                    <i class="icon-plus"></i> <?php echo EXPORT_CSV; ?></button>
                <button type="button" class="btn mini purple"
                        onclick="location.href='<?php echo site_url('admin/newsletter/import_newsletter_user'); ?>'"><i
                        class="icon-plus"></i> <?php echo IMPORT_CSV; ?></button>

            </div>

            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN EXAMPLE TABLE widget-->
                    <div class="widget">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i><?php echo NEWLETTERS_USERS; ?> </h4>
                            <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                            </span>
                        </div>


                        <div class="widget-body">

                            <?php
                            $attributes = array('name' => 'frm_listuser', 'id' => 'frm_listuser');

                            echo form_open_multipart('admin/newsletter/action_newsletter_user/', $attributes);
                            ?>



                            <input type="hidden" name="action" id="action"/>

                            <table class="table table-striped table-bordered table_project" id="sample_1">
                                <thead>
                                <tr>
                                    <th style="width:8px;"><input type="checkbox" class="group-checkable"
                                                                  data-set="#sample_1 .checkboxes"/></th>
                                    <!-- <th class="hidden-phone"><?php echo NAMES; ?></th>-->
                                    <th><?php echo EMAIL ?></th>
                                    <th class="hidden-phone"><?php echo SIGNUP_IP_ADDRESS; ?></th>
                                    <th class="hidden-phone"><?php echo DATE; ?></th>
                                    <th><?php echo EDIT; ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if ($result) {
                                    $i = 0;
                                    foreach ($result as $row) {
                                        $user_name = $row->user_name;
                                        $email = $row->email;
                                        $user_ip = $row->user_ip;


                                        $user_date = date($site_setting['date_format'], strtotime($row->user_date));
                                        if ($i % 2 == "0") {
                                            $fc = "toggle";
                                            $cl = "alter";
                                        } else {
                                            $fc = "toggle1";
                                            $cl = "alter1";
                                        }
                                        ?>
                                        <tr class="odd gradeX">
                                            <td><input type="checkbox" class="checkboxes" name="chk[]" id="chk"
                                                       value="<?php echo $row->newsletter_user_id; ?>"/></td>
                                            <!-- <td class="hidden-phone"><?php if ($user_name == '') {
                                                echo "N/A";
                                            } else {
                                                echo $user_name;
                                            } ?></td>-->
                                            <td><?php echo $email; ?></td>
                                            <td class="hidden-phone"><?php echo $user_ip; ?></td>
                                            <td class="hidden-phone"><?php echo $user_date; ?></td>
                                            <td>
                                                <a href="<?php echo site_url('admin/newsletter/edit_newsletter_user/' . $row->newsletter_user_id); ?>"><?php echo EDIT; ?></a>
                                            </td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }

                                } else {

                                    ?>
                                    <!--<tr class="odd gradeX">
									<td colspan="6"  class="dataTables_empty"><?php echo NO_RECORDS_FOUND; ?>.</td> 
                                </tr>-->
                                <?php } ?>

                                </tbody>
                            </table>
                            </form>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE widget-->
                </div>
            </div>

            <!-- END ADVANCED TABLE widget-->

            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/DT_bootstrap.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        App.init();
    });
    jQuery(document).ready(function () {


        $('#sample_1').dataTable({
            "bDestroy": true,
            "aoColumnDefs": [{"bSortable": false, "aTargets": [0]}],
            "oLanguage": {
                "sLengthMenu": " _MENU_ <?php echo RECORD_PER_PAGE; ?>",
                "sZeroRecords": "<?php echo NOTHING_FOUND_SORRY; ?>",
                "sInfo": "<?php echo SHOWING; ?> _START_ to _END_ of _TOTAL_ <?php echo RECORD; ?>",
                "sInfoEmpty": "<?php echo SHOWING; ?> 0 to 0 of 0 <?php echo RECORD; ?>",
                "sSearch": "<?php echo SEARCH; ?>: ",
                'oPaginate': {

                    'sPrevious': '<?php echo PREVIOUS; ?>',
                    'sNext': '<?php echo NEXT; ?>'
                }
            }

        });


    });
</script>
