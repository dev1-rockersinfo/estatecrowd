<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php echo $this->load->view('admin_sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN THEME CUSTOMIZER-->
                    <div id="theme-change" class="hidden-phone">
                        <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text"><?php echo THEME; ?></span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
                    </div>
                    <!-- END THEME CUSTOMIZER-->

                    <h3 class="page-title">
                        <?php echo NEWLETTERS_USERS; ?>
                    </h3>
                    <ul class="breadcrumb">
                        <li>
                            <a href="#"><i class="icon-home"></i></a><span class="divider">&nbsp;</span>
                        </li>

                        <li><a href="#"><?php echo NEWLETTERS_USERS; ?></a><span class="divider-last">&nbsp;</span></li>
                    </ul>
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <?php if ($error != '') {

                ?>
                <div class="alert alert-error">
                    <button class="close" data-dismiss="alert">x</button>
                    <strong><?php echo ERRORS; ?>!</strong> <?php echo $error; ?></div>


            <?php
            } ?>

            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                <div class="span12 sortable">
                    <!-- BEGIN SAMPLE FORMPORTLET-->
                    <div class="widget">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i><?php echo NEWLETTERS_USERS; ?></h4>
                                        <span class="tools">
                                        <a href="javascript:;" class="icon-chevron-down"></a>
                                        <a href="javascript:;" class="icon-remove"></a>
                                        </span>
                        </div>
                        <div class="widget-body">
                            <!-- BEGIN FORM-->
                            <form action="<?php echo site_url('admin/newsletter/add_newsletter_user') ?>"
                                  class="form-horizontal" name="frm_login" method="post">

                                <div class="control-group">
                                    <label class="control-label"><?php echo USERNAME; ?> </label>

                                    <div class="controls">
                                        <input type="text" name="user_name" id="user_name"
                                               value="<?php echo $user_name; ?>" placeholder="username"
                                               class="input-xlarge"/>
                                    </div>
                                </div>


                                <div class="control-group">
                                    <label class="control-label"><?php echo EMAIL; ?> </label>

                                    <div class="controls">
                                        <input type="text" placeholder="demo@demo.com" class="input-xlarge" name="email"
                                               id="email" value="<?php echo $email; ?>"/>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label"> </label>

                                    <div class="controls">
                                        <table border="0" cellpadding="2" cellspacing="2" align="left">


                                            <?php if ($all_newsletter) {
                                                $cnt = 0; ?>
                                                <tr>
                                                    <?php foreach ($all_newsletter as $alnw) { ?>

                                                        <td align="left" valign="top"><input type="checkbox"
                                                                                             name="newsletter_id[]"
                                                                                             id="newsletter_id"
                                                                                             value="<?php echo $alnw->newsletter_id; ?>" <?php if ($all_subscription) {
                                                                if (in_array($alnw->newsletter_id, $all_subscription)) {
                                                                    ?> checked="checked" <?php
                                                                }
                                                            } ?> style="width:30px;"/></td>
                                                        <td align="left" valign="top"><?php echo $alnw->subject; ?></td>

                                                        <?php $cnt++;
                                                        if ($cnt > 4) {
                                                            echo "</tr><tr>";
                                                            $cnt = 0;
                                                        }
                                                    }
                                                    ?>
                                                </tr>

                                            <?php } ?>


                                            <input type="hidden" name="newsletter_user_id" id="newsletter_user_id"
                                                   value="<?php echo $newsletter_user_id; ?>"/>

                                        </table>
                                    </div>
                                </div>


                                <div class="form-actions">
                                    <?php
                                    if ($newsletter_user_id == "") {
                                        ?>
                                        <button type="submit" class="btn blue"><i
                                                class="icon-ok"></i> <?php echo SUBMIT; ?></button>
                                    <?php } else { ?>
                                        <button type="submit" class="btn blue"><i
                                                class="icon-ok"></i> <?php echo UPDATE; ?></button>
                                    <?php } ?>
                                    <button type="button" class="btn"
                                            onClick="location.href='<?php echo site_url('admin/newsletter/list_newsletter_user'); ?>'">
                                        <i class=" icon-remove"></i> <?php echo CANCEL; ?></button>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>

            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>
<script type="text/javascript"
        src="<?php echo base_url(); ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        App.init();
    });


</script>
<script type="text/javascript" language="javascript">
    function setchecked(elemName, status) {

        elem = document.getElementsByName(elemName);
        alert(status);
        alert(elem);
        for (i = 0; i < elem.length; i++) {
            alert(i);
            elem[i].checked = status;
        }
    }
</script>
