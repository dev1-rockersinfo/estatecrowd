<style>


    .tabs-pane .active {

        display: block !important;

    }

    .tabs-pane .tab-pane {

        display: none;

    }


</style>


<div id="container" class="row-fluid">


<!-- BEGIN SIDEBAR -->







<?php echo $this->load->view('admin_sidebar'); ?>







<!-- END SIDEBAR -->


<!-- BEGIN PAGE -->


<div id="main-content">


<!-- BEGIN PAGE CONTAINER-->


<div class="container-fluid">


<!-- BEGIN PAGE HEADER-->


<div class="row-fluid">


    <div class="span12">


        <!-- BEGIN THEME CUSTOMIZER-->


        <div id="theme-change" class="hidden-phone">


            <i class="icon-cogs"></i>



                        <span class="settings">



                            <span class="text"><?php echo THEME; ?></span>



                            <span class="colors">



                                <span class="color-default" data-style="default"></span>



                                <span class="color-gray" data-style="gray"></span>



                                <span class="color-purple" data-style="purple"></span>



                                <span class="color-navy-blue" data-style="navy-blue"></span>



                            </span>



                        </span>


        </div>


        <!-- END THEME CUSTOMIZER-->


        <!-- BEGIN PAGE TITLE & BREADCRUMB-->


        <h3 class="page-title">


            <?php echo NEWSLETTER_JOB; ?>


        </h3>


        <ul class="breadcrumb">


            <li>


                <a href="#"><i class="icon-home"></i></a><span class="divider">&nbsp;</span>


            </li>


            <li><a href="#"><?php echo NEWSLETTER_JOB; ?></a><span class="divider-last">&nbsp;</span></li>


        </ul>


        <!-- END PAGE TITLE & BREADCRUMB-->


    </div>


</div>


<!-- END PAGE HEADER-->



<?php if ($msg != '') {


    if ($msg == 'delete') {
        ?>
        <div class="alert alert-success">


            <button class="close" data-dismiss="alert">x</button>


            <strong><?php echo SUCCESS; ?>!</strong> <?php echo RECORDS_HAS_BEEN_DELETED_SUCCESSFULLY; ?></div>











    <?php
    }
    if ($msg == 'insert') {
        ?>
        <div class="alert alert-success">


            <button class="close" data-dismiss="alert">x</button>


            <strong><?php echo SUCCESS; ?>!</strong> <?php echo NEW_RECORD_HAS_BEEN_ADDED_SUCCESSFULLY; ?></div>















    <?php } ?>







<?php } ?>











<!-- BEGIN ADVANCED TABLE widget-->


<div class="row-fluid">


<div class="span12">


<!-- BEGIN EXAMPLE TABLE widget-->


<div class="widget">


<div class="widget-title">


    <h4><i class="icon-reorder"></i><?php echo NEWLETTERS_USERS; ?> </h4>



                            <span class="tools">



                                <a href="javascript:;" class="icon-chevron-down"></a>



                            </span>


</div>


<div class="widget-body">


<ul class="nav nav-tabs">


    <li><a href="<?php echo site_url('admin/newsletter/newsletter_job') ?>"><?php echo DRAFT; ?></a></li>


    <li class="active"><a href="<?php echo site_url('admin/newsletter/newsletter_job_sent') ?>"><?php echo SENT; ?></a>
    </li>


</ul>



<?php



$attributes = array('name' => 'frm_listuser', 'id' => 'frm_listuser');


echo form_open_multipart('admin/newsletter/action_newsletter_job', $attributes);



?>







<input type="hidden" name="action" id="action"/>


<div class="tabs-pane">


<div class="tab-pane" id="tab_1_2">


<table class="table table-striped table-bordered table_project" id="sample_21">


<thead>


<tr>


    <th style="width:8px;"><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes"/></th>


    <th><?php echo SUBJECT; ?></th>


    <th class="hidden-phone"><?php echo START_DATE; ?></th>


    <th class="hidden-phone"><?php echo STATISTICS; ?></th>


    <th class="hidden-phone"><?php echo SUBSCRIBER; ?></th>


    <th class="hidden-phone"><?php echo SENDS; ?></th>


    <th class="hidden-phone"><?php echo OPEN; ?></th>


    <th class="hidden-phone"><?php echo FAIL; ?></th>


    <th><?php echo CREATE_DATE; ?></th>


</tr>


</thead>


<tbody>


<?php







if ($sent_result) {


    $i = 0;


    foreach ($sent_result as $rows) {


        $job_id = $rows->job_id;


        $subject = $rows->subject;


        $job_start_date = date($site_setting['date_format'], strtotime($rows->job_start_date));


        $job_id = $rows->job_id;


        $newsletter_id = $rows->newsletter_id;


        $job_date = date($site_setting['date_format'], strtotime($rows->job_date));


        if ($i % 2 == "0") {


            $fc = "toggle";


            $cl = "alter";


        } else {


            $fc = "toggle1";


            $cl = "alter1";


        }



        ?>



        <tr class="odd gradeX">


            <td><input type="checkbox" class="checkboxes" name="chk[]" id="chk" value="<?php echo $job_id; ?>"/></td>


            <td> <?php echo $subject; ?></td>


            <td class="hidden-phone"><?php echo $job_start_date; ?></td>


            <td class="hidden-phone"><a
                    href="<?php echo site_url('admin/newsletter/newsletter_statistics/' . $job_id . '/' . $newsletter_id); ?>"
                    id="various<?php echo $job_id; ?>"><?php echo VIEW; ?></a></td>


            <td class="hidden-phone">


                <?php $total_subscription = $this->newsletter_model->get_total_subscription($newsletter_id);


                if ($total_subscription > 0) {


                    echo '(' . $this->newsletter_model->get_total_subscription($newsletter_id) . ')';


                } else {
                    echo "(0)";
                }



                ?>


            </td>


            <td class="hidden-phone">


                <?php $total_send = $this->newsletter_model->get_total_job_send($job_id);


                if ($total_send > 0) {


                    echo '(' . $this->newsletter_model->get_total_job_send($job_id) . ')';


                } else {
                    echo "(0)";
                }







                ?>


            </td>


            <td class="hidden-phone">


                <?php $total_read = $this->newsletter_model->get_total_job_open($job_id);


                if ($total_read > 0) {


                    echo '(' . $this->newsletter_model->get_total_job_open($job_id) . ')';


                } else {
                    echo "(0)";
                }







                ?></td>


            <td class="hidden-phone"><?php $total_fail = $this->newsletter_model->get_total_job_fail($job_id);


                if ($total_fail > 0) {


                    echo '(' . $this->newsletter_model->get_total_job_fail($job_id) . ')';


                } else {
                    echo "(0)";
                }  ?></td>


            <td><?php echo $job_date; ?></td>


        </tr>







        <?php



        $i++;


    }


} else {


    ?>







    <tr class="alter">


        <td colspan="15" align="center" valign="middle" height="30"><?php echo NO_RECORDS_FOUND; ?>.</td>
    </tr>



<?php } ?>


</tbody>


</table>


</div>


</div>


</form>


</div>


</div>


<!-- END EXAMPLE TABLE widget-->


</div>


</div>


<p><?php echo IMPORTANT_NOTE_PLEASE_SET_THE_CRON_JOB_ON_YOUR_SERVER_WITH_URL; ?> <?php echo site_url('newsletter_cron/send'); ?>
    <br/>(Ex :: curl -s -o /dev/null <?php echo site_url('newsletter_cron/send'); ?>)</p>


<!-- END ADVANCED TABLE widget-->


<!-- END PAGE CONTENT-->


</div>


<!-- END PAGE CONTAINER-->


</div>


<!-- END PAGE -->


</div>


<script>


    function setaction(elename, actionval, actionmsg, formname) {


        vchkcnt = 0;


        elem = document.getElementsByName(elename);


        for (i = 0; i < elem.length; i++) {


            if (elem[i].checked) vchkcnt++;


        }


        if (vchkcnt == 0) {


            alert("<?php echo DELETE_SELECT_RECORD_CONFIRMATION;?>")


        } else {


            if (confirm(actionmsg)) {


                document.getElementById('action').value = actionval;


                document.getElementById(formname).submit();


            }


        }


    }


</script>


<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>


<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/jquery.dataTables.js"></script>


<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/DT_bootstrap.js"></script>


<script src="<?php echo base_url(); ?>js/scripts.js"></script>


<script>


    jQuery(document).ready(function () {



        // initiate layout and plugins


        App.init();


    });


</script>
