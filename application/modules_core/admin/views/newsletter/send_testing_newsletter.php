<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php echo $this->load->view('admin_sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN THEME CUSTOMIZER-->
                    <div id="theme-change" class="hidden-phone">
                        <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text"><?php echo THEME; ?></span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
                    </div>
                    <!-- END THEME CUSTOMIZER-->


                    <h3 class="page-title">
                        <?php echo EMAIL_SETTING; ?>
                    </h3>
                    <ul class="breadcrumb">
                        <li>
                            <a href="#"><i class="icon-home"></i></a><span class="divider">&nbsp;</span>
                        </li>

                        <li><a href="#"><?php echo NEWLETTERS_SETTING; ?></a><span class="divider-last">&nbsp;</span>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <?php if ($disp_error == 1) { ?>
                <div class="alert alert-error">
                    <button class="close" data-dismiss="alert">x</button>
                    <strong><?php echo ERRORS; ?>!</strong><?php echo $error1; ?></div>

            <?php } else { ?>
                <?php if ($error != "" && $error == "sent") {
                    ?>
                    <div class="alert alert-success">
                        <button class="close" data-dismiss="alert">x</button>
                        <strong><?php echo SUCCESS; ?>
                            !</strong><?php echo "Please Check your inbox or also check Spam box."; ?></div>
                <?php
                }
            }
            ?>
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                <div class="span12 sortable">
                    <!-- BEGIN SAMPLE FORMPORTLET-->
                    <div class="widget">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i><?php echo NEWLETTERS_SETTING; ?></h4>
                                        <span class="tools">
                                        <a href="javascript:;" class="icon-chevron-down"></a>
                                        <a href="javascript:;" class="icon-remove"></a>
                                        </span>
                        </div>
                        <div class="widget-body">
                            <!-- BEGIN FORM-->
                            <form action="<?php echo site_url('admin/newsletter/send_testing_newsetter') ?>"
                                  class="form-horizontal" method="post" name="frm_send_testing_newsetter">


                                <div class="control-group">
                                    <label class="control-label">*<?php echo SENDER_EMAIL; ?></label>

                                    <div class="controls">
                                        <input type="text" placeholder="demo@demo.com" class="input-xlarge"
                                               name="sender_mail" id="sender_mail" value=""/>

                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label">*<?php echo RECEIVER_EMAIL; ?> </label>

                                    <div class="controls">
                                        <input type="text" placeholder="demo@demo.com" class="input-xlarge"
                                               name="receiver_email" id="receiver_email" value=""/>

                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label">*<?php echo MESSAGE; ?> </label>

                                    <div class="controls">
                                        <textarea name="message_text" id="message_text" rows="6"></textarea>


                                    </div>
                                </div>

                                <input type="hidden" name="newsletter_setting_id" id="newsletter_setting_id"
                                       value="<?php echo $newsletter_setting_id; ?>"/>

                                <div class="form-actions">
                                    <button type="submit" class="btn blue"><i class="icon-ok"></i> <?php echo SENDS; ?>
                                    </button>

                                </div>

                            </form>
                            <!-- END FORM-->

                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>

            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>
<script type="text/javascript"
        src="<?php echo base_url(); ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        App.init();
    });

</script>
