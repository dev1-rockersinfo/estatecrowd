<div id="container" class="row-fluid">


<!-- BEGIN SIDEBAR -->



<?php echo $this->load->view('admin_sidebar'); ?>



<!-- END SIDEBAR -->


<!-- BEGIN PAGE -->


<div id="main-content">


<!-- BEGIN PAGE CONTAINER-->


<div class="container-fluid">


<!-- BEGIN PAGE HEADER-->


<div class="row-fluid">


    <div class="span12">


        <!-- BEGIN THEME CUSTOMIZER-->


        <div id="theme-change" class="hidden-phone">


            <i class="icon-cogs"></i>



                        <span class="settings">



                            <span class="text"><?php echo THEME; ?>:</span> <!-- change by darshan -->



                            <span class="colors">



                                <span class="color-default" data-style="default"></span>



                                <span class="color-gray" data-style="gray"></span>



                                <span class="color-purple" data-style="purple"></span>



                                <span class="color-navy-blue" data-style="navy-blue"></span>



                            </span>



                        </span>


        </div>


        <!-- END THEME CUSTOMIZER-->


        <h3 class="page-title">


            <?php echo NEWSLETTER_SETTING; ?>


        </h3>


        <ul class="breadcrumb">


            <li>


                <a href="#"><i class="icon-home"></i></a><span class="divider">&nbsp;</span>


            </li>


            <li><a href="#"><?php echo NEWSLETTER_SETTING; ?></a><span class="divider-last">&nbsp;</span></li>


        </ul>


    </div>


</div>


<!-- END PAGE HEADER-->







<?php if ($error != "") {
    ?>



    <div class="alert alert-success">


        <button class="close" data-dismiss="alert">x</button>


        <strong><?php echo SUCCESS; ?> ! </strong><?php echo RECORD_HAS_BEEN_UPDATED_SUCCESSFULLY; ?>.
    </div>







<?php
}



?>



<?php if ($error1 != "") {
    ?>



    <div class="alert alert-success">


        <button class="close" data-dismiss="alert">x</button>


        <strong><?php echo SUCCESS; ?> ! </strong><?php echo SENT_MESSAGE_ON_SENDING_A_TEST_MAIL; ?></div>







<?php
}



?>



<!-- BEGIN PAGE CONTENT-->


<div class="row-fluid">


<div class="span12 sortable">


<!-- BEGIN SAMPLE FORMPORTLET-->


<div class="widget">


<div class="widget-title">


    <h4><i class="icon-reorder"></i><?php echo ADD_NEWSLETTER; ?> </h4>



                                        <span class="tools">



                                        <a href="javascript:;" class="icon-chevron-down"></a>



                                        <a href="javascript:;" class="icon-remove"></a>



                                        </span>


</div>


<div class="widget-body">


<!-- BEGIN FORM-->


<form action="<?php echo site_url('admin/newsletter/newsletter_setting') ?>" class="form-horizontal" method="post"
      name="frm_newsletter_setting">


<div class="control-group">


    <label class="control-label"><?php echo FROM_NAME ?></label>


    <div class="controls">


        <input type="text" placeholder="Fundraising Script" class="input-xlarge" name="newsletter_from_name"
               id="newsletter_from_name" value="<?php echo $newsletter_from_name; ?>"/>


    </div>


</div>


<div class="control-group">


    <label class="control-label"><?php echo FROM_EMAIL_ADDRESS ?> </label>


    <div class="controls">


        <input type="text" placeholder="jigar.rockersinfo@gmail.com" class="input-xlarge" name="newsletter_from_address"
               id="newsletter_from_address" value="<?php echo $newsletter_from_address; ?>"/>


    </div>


</div>


<div class="control-group">


    <label class="control-label"><?php echo REPLY_NAME; ?> </label>


    <div class="controls">


        <input type="text" placeholder="Fundraising Script" class="input-xlarge" name="newsletter_reply_name"
               id="newsletter_reply_name" value="<?php echo $newsletter_reply_name; ?>"/>


    </div>


</div>


<div class="control-group">


    <label class="control-label"><?php echo REPLY_EMAIL_ADDRESS; ?></label>


    <div class="controls">


        <input type="text" placeholder="abc@example.com" class="input-xlarge" name="newsletter_reply_address"
               id="newsletter_reply_address" value="<?php echo $newsletter_reply_address; ?>"/>
        <!-- change by darshan -->


    </div>


</div>


<div class="control-group">


    <label class="control-label"><?php echo NEW_SUBSCRIBE_EMAIL; ?> </label>


    <div class="controls">


        <input type="text" placeholder="abc@example.com" class="input-xlarge" name="new_subscribe_email"
               id="new_subscribe_email" value="<?php echo $new_subscribe_email; ?>"/> <!-- change by darshan -->


    </div>


</div>


<div class="control-group">


    <label class="control-label"><?php echo UNSUBSCRIBE_EMAIL; ?> </label>


    <div class="controls">


        <input type="text" placeholder="abc@example.com" class="input-xlarge" name="unsubscribe_email"
               id="unsubscribe_email" value="<?php echo $unsubscribe_email; ?>"/> <!-- change by darshan -->


    </div>


</div>


<input type="hidden" name="new_subscribe_to" value="all"/>


<div class="control-group" id="all_newsletter" style="display:<?php if ($new_subscribe_to == 'selected') {
    echo "block";
} else {
    echo "none";
} ?>;">


    <label class="control-label"><?php echo NEWSLETTERS; ?></label>


    <div class="controls">


        <select name="selected_newsletter_id" id="selected_newsletter_id">


            <?php   if ($all_newsletter) {


                foreach ($all_newsletter as $news) {
                    ?>



                    <option
                        value="<?php echo $news->newsletter_id; ?>" <?php if ($selected_newsletter_id == $news->newsletter_id) { ?> selected="selected" <?php } ?>
                        style="text-transform:capitalize;"><?php echo $news->subject; ?></option>







                <?php
                }
            } else {
                ?>







                <option value="">---<?php echo NO_TEMPLATE ?>---</option>











            <?php } ?>


        </select>


    </div>


</div>


<input type="hidden" placeholder="30" class="input-xlarge" name="number_of_email_send" id="number_of_email_send"
       value="<?php echo $number_of_email_send; ?>"/>
<!--  <div class="control-group">



                                    <label class="control-label"><?php //echo NUMBER_OF_EMAIL_SEND; ?></label>



                                    <div class="controls">



                                        <input type="text" placeholder="30" class="input-xlarge"  name="number_of_email_send" id="number_of_email_send" value="<?php //echo $number_of_email_send; ?>" />                                        



                                    </div>



                                </div>-->


<input type="hidden" placeholder="15" class="input-xlarge" name="break_between_email" id="break_between_email"
       value="<?php echo $break_between_email; ?>"/>
<input type="hidden" placeholder="15" class="input-xlarge" name="break_type" id="break_type" value="hours"/>

<!-- <div class="control-group">



                                    <label class="control-label"><?php //echo BREAK_BETWEEN_NO_EMAIL_SEND; ?> </label>



                                    <div class="controls">



                                        <input type="text" placeholder="15" class="input-xlarge" name="break_between_email" id="break_between_email" value="<?php //echo $break_between_email; ?>" />                                        



                                    </div>



                                </div>-->


<!-- <div class="control-group">



                                    <label class="control-label"><?php //echo BREAK_TYPE; ?> </label>



                                    <div class="controls">



                                        <select class="input-large m-wrap" tabindex="1" name="break_type" id="break_type">



                                           <option value="minutes" <?php if ($break_type == 'minutes') { ?> selected="selected" <?php } ?> ><?php //echo MINUTES; ?></option>



                           					 <option value="hours" <?php if ($break_type == 'hours') { ?> selected="selected" <?php } ?> ><?php //echo HOURS; ?></option>



                                           



                                        </select>



                                    </div>



                                </div>-->


<div class="control-group">


    <label class="control-label"><?php echo MAILER; ?> </label>


    <div class="controls">


        <select class="input-large m-wrap" tabindex="1" name="mailer" id="mailer">


            <option
                value="mail" <?php if ($mailer == 'mail') { ?> selected="selected" <?php } ?> ><?php echo PHP_MAIL; ?></option>


            <option
                value="smtp" <?php if ($mailer == 'smtp') { ?> selected="selected" <?php } ?> ><?php echo SMTP; ?></option>


            <option
                value="sendmail" <?php if ($mailer == 'sendmail') { ?> selected="selected" <?php } ?> ><?php echo SENDMAIL; ?></option>


        </select>


    </div>


</div>


<div class="control-group">


    <label class="control-label"><?php echo SEND_MAIL_PATH; ?></label>


    <div class="controls">


        <input type="text" placeholder="/usr/sbin/sendmail" class="input-xlarge" name="sendmail_path" id="sendmail_path"
               value="<?php echo $sendmail_path; ?>"/>


        <span class="help-inline"><?php echo IF_MAILER_IS_SENDMAIL; ?></span>


    </div>


</div>


<div class="control-group">


    <label class="control-label"><?php echo SMTP_PORT; ?> </label>


    <div class="controls">


        <input type="text" placeholder="0" class="input-xlarge" name="smtp_port" id="smtp_port"
               value="<?php echo $smtp_port; ?>"/>


        <span class="help-inline">(465 or 25 or 587)</span>


    </div>


</div>


<div class="control-group">


    <label class="control-label"><?php echo SMTP_HOST; ?> </label>


    <div class="controls">


        <input type="text" placeholder="0" class="input-xlarge" name="smtp_host" id="smtp_host"
               value="<?php echo $smtp_host; ?>"/>


        <span class="help-inline">(if smtp user is gmail then ssl://smtp.googlemail.com)</span>


    </div>


</div>


<div class="control-group">


    <label class="control-label"><?php echo SMTP_EMAIL; ?> </label>


    <div class="controls">


        <input type="text" placeholder="ritu" class="input-xlarge" name="smtp_email" id="smtp_email"
               value="<?php echo $smtp_email; ?>"/>


    </div>


</div>


<div class="control-group">


    <label class="control-label"><?php echo SMTP_PASSWORD; ?></label>


    <div class="controls">


        <input type="password" class="input-xlarge" name="smtp_password" id="smtp_password"
               value="<?php echo $smtp_password; ?>"/>


    </div>


</div>


<input type="hidden" name="newsletter_setting_id" id="newsletter_setting_id"
       value="<?php echo $newsletter_setting_id; ?>"/>


<div class="form-actions">


    <button type="submit" class="btn blue"><i class="icon-ok"></i> <?php echo SUBMIT; ?></button>


    <button type="button" class="btn" onClick="testmail();"><i class="icon-ok"></i> <?php echo SEND_TEST_MAIL; ?>
    </button>


</div>


</form>


<!-- END FORM-->


</div>


</div>


<!-- END SAMPLE FORM PORTLET-->


</div>


</div>


<!-- END PAGE CONTENT-->


</div>


<!-- END PAGE CONTAINER-->


</div>


<!-- END PAGE -->


</div>


<script>


    function testmail() {


        window.location.href = '<?php echo site_url('admin/newsletter/send_testing_newsetter');?>';


    }


    function show_newsletter() {


        document.getElementById('all_newsletter').style.display = 'block';


    }


    function unshow_newsletter() {


        document.getElementById('all_newsletter').style.display = 'none';


    }


</script>


<script type="text/javascript" src="<?php echo base_url(); ?>ckeditor/ckeditor.js"></script>


<script type="text/javascript"
        src="<?php echo base_url(); ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>


<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>


<script src="<?php echo base_url(); ?>js/scripts.js"></script>


<script>


    jQuery(document).ready(function () {



        // initiate layout and plugins


        App.init();


    });


</script>


<script>
    CKEDITOR.replace('description', {
        filebrowserBrowseUrl: '<?php echo base_url();?>ckeditor/filemanager/browser/default/browser.html?Connector=<?php echo base_url(); ?>ckeditor/filemanager/connectors/php/connector.php',
        filebrowserImageBrowseUrl: '<?php echo base_url(); ?>ckeditor/filemanager/browser/default/browser.html?Type=Image&Connector=<?php echo base_url(); ?>ckeditor/filemanager/connectors/php/connector.php',
        filebrowserFlashBrowseUrl: '<?php echo base_url(); ?>ckeditor/filemanager/browser/default/browser.html?Type=Flash&Connector=<?php echo base_url(); ?>ckeditor/filemanager/connectors/php/connector.php',
        filebrowserUploadUrl: '<?php echo base_url(); ?>ckeditor/filemanager/connectors/php/upload.php?Type=File',
        filebrowserImageUploadUrl: '<?php echo base_url(); ?>ckeditor/filemanager/connectors/php/upload.php?Type=Image',
        filebrowserFlashUploadUrl: '<?php echo base_url();?>ckeditor/filemanager/connectors/php/upload.php?Type=Flash'
    });


</script>


<script type="text/javascript" src=" <?php echo base_url(); ?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
