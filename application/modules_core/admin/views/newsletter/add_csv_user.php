<div id="container" class="row-fluid">

    <!-- BEGIN SIDEBAR -->

    <?php echo $this->load->view('admin_sidebar'); ?>

    <!-- END SIDEBAR -->

    <!-- BEGIN PAGE -->

    <div id="main-content">

        <!-- BEGIN PAGE CONTAINER-->

        <div class="container-fluid">

            <!-- BEGIN PAGE HEADER-->

            <div class="row-fluid">

                <div class="span12">

                    <!-- BEGIN THEME CUSTOMIZER-->

                    <div id="theme-change" class="hidden-phone">

                        <i class="icon-cogs"></i>

                        <span class="settings">

                            <span class="text"><?php echo THEME; ?>:</span> <!-- change by darshan -->
                            <span class="colors">

                                <span class="color-default" data-style="default"></span>

                                <span class="color-gray" data-style="gray"></span>

                                <span class="color-purple" data-style="purple"></span>

                                <span class="color-navy-blue" data-style="navy-blue"></span>

                            </span>

                        </span>

                    </div>

                    <!-- END THEME CUSTOMIZER-->


                    <h3 class="page-title">

                        <?php echo NEWLETTERS_USERS; ?>

                    </h3>

                    <ul class="breadcrumb">

                        <li>

                            <a href="#"><i class="icon-home"></i></a><span class="divider">&nbsp;</span>

                        </li>


                        <li><a href="#"><?php echo NEWLETTERS_USERS; ?> </a><span class="divider-last">&nbsp;</span>
                        </li>

                    </ul>

                </div>

            </div>

            <!-- END PAGE HEADER-->

            <?php if ($error != '') {


                ?>

                <div class="alert alert-error">

                    <button class="close" data-dismiss="alert">x</button>

                    <strong><?php echo ERRORS; ?>!</strong> <?php echo $error; ?></div>





            <?php

            } ?>



            <!-- BEGIN PAGE CONTENT-->

            <div class="row-fluid">

                <div class="span12 sortable">

                    <!-- BEGIN SAMPLE FORMPORTLET-->

                    <div class="widget">

                        <div class="widget-title">

                            <h4><i class="icon-reorder"></i><?php echo NEWLETTERS_USERS; ?></h4>

                                        <span class="tools">

                                        <a href="javascript:;" class="icon-chevron-down"></a>

                                        <a href="javascript:;" class="icon-remove"></a>

                                        </span>

                        </div>

                        <div class="widget-body">

                            <!-- BEGIN FORM-->

                            <form action="<?php echo site_url('admin/newsletter/add_csv_upload') ?>"
                                  class="form-horizontal" name="frm_addcsvreward" method="post"
                                  onSubmit="return Checkfiles();" enctype="multipart/form-data">


                                <div class="control-group">

                                    <label class="control-label"><?php echo UPLOAD_CSV; ?></label>

                                    <div class="controls">

                                        <input type="file" name="upcsv" id="upcsv"/>

                                        <a href="<?php echo site_url('admin/newsletter/download_csv_file'); ?>"
                                           style="color:#036;"><?php echo DOWNLOAD_SAMPLE_CSV_FILE; ?></a>

                                    </div>

                                </div>


                                <div class="form-actions">


                                    <button type="submit" class="btn blue"><i class="icon-ok"></i> <?php echo UPDATE; ?>
                                    </button>


                                    <button type="button" class="btn"
                                            onClick="location.href='<?php echo site_url('admin/newsletter/list_newsletter_user'); ?>'">
                                        <i class=" icon-remove"></i> <?php echo CANCEL; ?></button>

                                </div>

                            </form>

                            <!-- END FORM-->

                        </div>

                    </div>

                    <!-- END SAMPLE FORM PORTLET-->

                </div>

            </div>


            <!-- END PAGE CONTENT-->

        </div>

        <!-- END PAGE CONTAINER-->

    </div>

    <!-- END PAGE -->

</div>

<script type="text/javascript"
        src="<?php echo base_url(); ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>

<script src="<?php echo base_url(); ?>js/scripts.js"></script>

<script>

    jQuery(document).ready(function () {

        // initiate layout and plugins

        App.init();

    });


</script>

<script language="javascript">


    function Checkfiles() {


        var fup = document.getElementById('upcsv');


        var fileName = fup.value;


        if (fileName == '') {

            alert("<?php echo UPLOAD_CSV_ONLY;?>");//chaneg by darshan

            fup.focus();


            return false;

        }


        var ext = fileName.substring(fileName.lastIndexOf('.') + 1);


        if (ext == "csv") {

            return true;

        }


        else {

            alert("<?php echo UPLOAD_CSV_ONLY;?>");//change by darshan

            fup.focus();


            return false;

        }


    }


</script>

