<div id="container" class="row-fluid">


<!-- BEGIN SIDEBAR -->
<?php echo $this->load->view('admin_sidebar'); ?>
<!-- END SIDEBAR -->
<!-- BEGIN PAGE -->
<div id="main-content">
    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN THEME CUSTOMIZER-->
                <div id="theme-change" class="hidden-phone">
                    <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text"><?php echo THEME; ?></span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
                </div>
                <!-- END THEME CUSTOMIZER-->
                <h3 class="page-title">
                    <?php echo ADD_NEWSLETTER; ?>
                </h3>
                <ul class="breadcrumb">
                    <li>
                        <a href="#"><i class="icon-home"></i></a><span class="divider">&nbsp;</span>
                    </li>

                    <li><a href="#"><?php echo ADD_NEWSLETTER; ?>  </a><span class="divider-last">&nbsp;</span></li>
                </ul>
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <?php if ($error != '') {

            ?>
            <div class="alert alert-error">
                <button class="close" data-dismiss="alert">x</button>
                <strong><?php echo ERRORS; ?>!</strong> <?php echo $error; ?></div>

        <?php } ?>
        <!-- BEGIN PAGE CONTENT-->
        <div class="row-fluid">
            <div class="span12 sortable">
                <!-- BEGIN SAMPLE FORMPORTLET-->
                <div class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-reorder"></i><?php echo ADD_NEWSLETTER ?> </h4>
                                        <span class="tools">
                                        <a href="javascript:;" class="icon-chevron-down"></a>
                                        <a href="javascript:;" class="icon-remove"></a>
                                        </span>
                    </div>
                    <div class="widget-body">
                        <!-- BEGIN FORM-->
                        <form action="<?php echo site_url('admin/newsletter/add_newsletter'); ?>" name="frm_login"
                              enctype="multipart/form-data" class="form-horizontal" method="post">
                            <div class="control-group">
                                <label class="control-label"><?php echo SUBJECT; ?></label>

                                <div class="controls">
                                    <input type="text" placeholder="Subject" name="subject" id="subject"
                                           value="<?php echo $subject; ?>" class="input-xlarge"/>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label"><?php echo ATTACH_FILE ?> </label>

                                <div class="controls">
                                    <input type="file" name="file_up" id="file_up" value="<?php echo $file_up; ?>">
                                    <input type="hidden" name="prev_attach_file" id="prev_attach_file"
                                           value="<?php echo $prev_attach_file; ?>"/>
                                    <?php if (is_file(base_url() . 'upload/newsletter/' . $prev_attach_file)) { ?>

                                        <div style="padding:7px; float:left;"><a
                                                href="<?php echo site_url('upload/newsletter/' . $prev_attach_file); ?>"
                                                target="_blank"
                                                style="color:#666666;"><?php echo VIEW_ATTACHMENT; ?></a></div>

                                    <?php } ?>

                                </div>
                            </div>

                            <!--<div class="control-group">
                                <label class="control-label">Allow Subscribe Link</label>
                                <div class="controls">
                                    <label class="radio">
                                        <input type="radio" name="optionsRadios1" value="option1" />
                                         Yes
                                    </label>

                                    <label class="radio">
                                        <input type="radio" name="optionsRadios1" value="option1" />
                                         No
                                    </label>


                                </div>
                            </div>-->

                            <div class="control-group">
                                <label class="control-label"><?php echo ALLOW_UNSUBCRIBE_LINK; ?></label>

                                <div class="controls">
                                    <label class="radio">
                                        <input type="radio" name="allow_unsubscribe_link"
                                               id="allow_unsubscribe_link"
                                               value="1" <?php if ($allow_unsubscribe_link == '1') { ?> checked="checked" <?php } ?> />
                                        <?php echo YES; ?>
                                    </label>

                                    <label class="radio">
                                        <input type="radio" name="allow_unsubscribe_link"
                                               id="allow_unsubscribe_link"
                                               value="0" <?php if ($allow_unsubscribe_link == '0') { ?> checked="checked" <?php } ?> />
                                        <?php echo NOS; ?>
                                    </label>


                                </div>
                            </div>


                            <div class="control-group">
                                <label class="control-label"><?php echo SUBSCRIBE_TO; ?> </label>

                                <div class="controls">
                                    <label class="radio">
                                        <input type="radio" name="subscribe_to" id="subscribe_to"
                                               value="none" <?php if ($subscribe_to == 'none') { ?> checked="checked" <?php } ?> />
                                        <?php echo NONES; ?>
                                    </label>

                                    <label class="radio">
                                        <input type="radio" name="subscribe_to" id="subscribe_to"
                                               value="all" <?php if ($subscribe_to == 'all') { ?> checked="checked" <?php } ?> />
                                        <?php echo ALL; ?>
                                    </label>


                                </div>
                            </div>


                            <!--<div class="control-group">
                                <label class="control-label">Project </label>
                                <div class="controls">
                                    <select class="input-large m-wrap" tabindex="1">
                                        <option value="">project name</option>
                                        <option value="">project name</option>

                                    </select>
                                </div>
                            </div>-->

                            <div class="control-group">
                                <label class="control-label"><?php echo CONTENT; ?> </label>

                                <div class="controls">
                                    <textarea rows="6" id="template_content"
                                              name="template_content"> <?php echo $template_content; ?> </textarea>
                                </div>
                            </div>


                            <div class="form-actions">
                                <input type="hidden" name="newsletter_id" id="newsletter_id"
                                       value="<?php echo $newsletter_id; ?>"/>
                                <?php
                                if ($newsletter_id == "") {
                                    ?>
                                    <button type="submit" class="btn blue"><i
                                            class="icon-ok"></i> <?php echo SUBMIT; ?></button>
                                <?php } else { ?>
                                    <button type="submit" class="btn blue"><i
                                            class="icon-ok"></i> <?php echo UPDATE; ?></button>
                                <?php } ?>
                                <button type="button" class="btn"
                                        onClick="location.href='<?php echo site_url('admin/newsletter/list_newsletter'); ?>'">
                                    <i class=" icon-remove"></i> <?php echo CANCEL; ?></button>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>

        <!-- END PAGE CONTENT-->
    </div>
    <!-- END PAGE CONTAINER-->
</div>
<!-- END PAGE -->
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>ckeditor/ckeditor.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
<script type="text/javascript"
        src="<?php echo base_url(); ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        App.init();
    });
</script>
<!-- END JAVASCRIPTS -->
<script>
    CKEDITOR.replace('template_content', {
        filebrowserBrowseUrl: '<?php echo base_url();?>ckeditor/filemanager/browser/default/browser.html?Connector=<?php echo base_url(); ?>ckeditor/filemanager/connectors/php/connector.php',
        filebrowserImageBrowseUrl: '<?php echo base_url(); ?>ckeditor/filemanager/browser/default/browser.html?Type=Image&Connector=<?php echo base_url(); ?>ckeditor/filemanager/connectors/php/connector.php',
        filebrowserFlashBrowseUrl: '<?php echo base_url(); ?>ckeditor/filemanager/browser/default/browser.html?Type=Flash&Connector=<?php echo base_url(); ?>ckeditor/filemanager/connectors/php/connector.php',
        filebrowserUploadUrl: '<?php echo base_url(); ?>ckeditor/filemanager/connectors/php/upload.php?Type=File',
        filebrowserImageUploadUrl: '<?php echo base_url(); ?>ckeditor/filemanager/connectors/php/upload.php?Type=Image',
        filebrowserFlashUploadUrl: '<?php echo base_url();?>ckeditor/filemanager/connectors/php/upload.php?Type=Flash'
    });


</script>
