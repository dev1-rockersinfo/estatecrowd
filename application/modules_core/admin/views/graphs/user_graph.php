<!-- BEGIN CONTAINER -->
<div id="container" class="row-fluid">
<!-- BEGIN SIDEBAR -->
<?php echo $this->load->view('admin_sidebar'); ?>
<!-- END SIDEBAR -->
<!-- BEGIN PAGE -->
<div id="main-content">
    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN THEME CUSTOMIZER-->
                <div id="theme-change" class="hidden-phone">
                    <i class="icon-cogs"></i>
                            <span class="settings">
                                  <span class="text"><?php echo THEME; ?>:</span> <!-- chaneg  by darshan -->
                                <span class="colors">
                                    <span class="color-default" data-style="default"></span>
                                    <span class="color-gray" data-style="gray"></span>
                                    <span class="color-purple" data-style="purple"></span>
                                    <span class="color-navy-blue" data-style="navy-blue"></span>
                                </span>
                            </span>
                </div>
                <!-- END THEME CUSTOMIZER-->
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo GRAPHICAL_REPORT; ?>
                    <small><?php echo USER; ?></small>
                </h3>
                <ul class="breadcrumb">
                    <li>
                        <a href="<?php echo site_url('admin/home/dashboard'); ?>"><i class="icon-home"></i></a><span
                            class="divider">&nbsp;</span>
                    </li>
                    <li><a href="#"><?php echo GRAPHICAL_REPORT_OF_USER_REGISTRATION; ?></a><span
                            class="divider-last">&nbsp;</span></li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div id="page">
            <div class="row-fluid">
                <div class="span6">
                    <!-- BEGIN INTERACTIVE CHART PORTLET-->
                    <div class="widget">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i><?php echo WEEKLY_AVERAGE_REGISTRATION_LINE; ?></h4>
							<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
                            <a href="javascript:;" class="icon-remove"></a>
							</span>
                        </div>
                        <div class="widget-body">
                            <div id="chart_2" class="chart"></div>
                        </div>
                    </div>
                    <!-- END INTERACTIVE CHART PORTLET-->
                </div>
                <div class="span6">
                    <!-- BEGIN Bar Chat PORTLET-->
                    <div class="widget">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i><?php echo WEEKLY_AVERAGE_REGISTRATION_BAR; ?></h4>
							<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
                            <a href="javascript:;" class="icon-remove"></a>
							</span>
                        </div>
                        <div class="widget-body">
                            <div id="chart_5" style="height:350px;"></div>
                            <div class="btn-toolbar">
                                <div class="btn-group stackControls">
                                    <input type="button" class="btn btn-info" value="<?php echo WITH_STACKING; ?>"/><!-- change by darshan -->

                                    <input type="button" class="btn btn-danger"
                                           value="<?php echo WITHOUT_STACKING; ?>"/><!-- change by darshan -->

                                </div>
                                <div class="space5"></div>
                                <div class="btn-group graphControls">
                                    <input type="button" class="btn" value="<?php echo BARS; ?>"/><!-- change by darshan -->

                                    <input type="button" class="btn" value="<?php echo LINES; ?>"/><!-- change by darshan -->
                                    <input type="button" class="btn" value="<?php echo LINES_WITH_STEPS; ?>"/><!-- change by darshan -->

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Bar Chat PORTLET-->
                </div>
            </div>

            <div class="row-fluid">
                <div class="span6">
                    <!-- BEGIN INTERACTIVE CHART PORTLET-->
                    <div class="widget">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i><?php echo MONTHLY_AVERAGE_REGISTRATION_LINE; ?></h4>
							<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
                            <a href="javascript:;" class="icon-remove"></a>
							</span>
                        </div>
                        <div class="widget-body">
                            <div id="chart_month_line" class="chart"></div>
                        </div>
                    </div>
                    <!-- END INTERACTIVE CHART PORTLET-->
                </div>
                <div class="span6">
                    <!-- BEGIN Bar Chat PORTLET-->
                    <div class="widget">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i> <?php echo MONTHLY_AVERAGE_REGISTRATION_BAR; ?></h4>
							<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
                            <a href="javascript:;" class="icon-remove"></a>
							</span>
                        </div>
                        <div class="widget-body">
                            <div id="chart_month_bar" style="height:350px;"></div>
                            <div class="btn-toolbar">
                                <div class="btn-group stackControls">
                                    <input type="button" class="btn btn-info" value="<?php echo WITH_STACKING; ?>"/><!-- change by darshan -->

                                    <input type="button" class="btn btn-danger"
                                           value="<?php echo WITHOUT_STACKING; ?>"/><!-- change by darshan -->
                                </div>
                                <div class="space5"></div>
                                 <div class="btn-group graphControls">
                                <input type="button" class="btn" value="<?php echo BARS; ?>"/><!-- change by darshan -->

                                <input type="button" class="btn" value="<?php echo LINES; ?>"/><!-- change by darshan -->
                                <input type="button" class="btn" value="<?php echo LINES_WITH_STEPS; ?>"/><!-- change by darshan -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Bar Chat PORTLET-->
            </div>
        </div>

        <div class="row-fluid">
            <div class="span6">
                <!-- BEGIN INTERACTIVE CHART PORTLET-->
                <div class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-reorder"></i> <?php echo YEARLY_AVERAGE_REGISTRATION_LINE; ?></h4>
							<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
                            <a href="javascript:;" class="icon-remove"></a>
							</span>
                    </div>
                    <div class="widget-body">
                        <div id="chart_year_line" class="chart"></div>
                    </div>
                </div>
                <!-- END INTERACTIVE CHART PORTLET-->
            </div>
            <div class="span6">
                <!-- BEGIN Bar Chat PORTLET-->
                <div class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-reorder"></i> <?php echo YEARLY_AVERAGE_REGISTRATION_BAR; ?></h4>
							<span class="tools">
							<a href="javascript:;" class="icon-chevron-down"></a>
                            <a href="javascript:;" class="icon-remove"></a>
							</span>
                    </div>
                    <div class="widget-body">
                        <div id="chart_year_bar" style="height:350px;"></div>
                        <div class="btn-toolbar">
                            <div class="btn-group stackControls">
                                <input type="button" class="btn btn-info" value="<?php echo WITH_STACKING; ?>"/><!-- change by darshan -->

                                <input type="button" class="btn btn-danger"
                                       value="<?php echo WITHOUT_STACKING; ?>"/><!-- change by darshan -->

                            </div>
                            <div class="space5"></div>
                            <div class="btn-group graphControls">
                                <input type="button" class="btn" value="<?php echo BARS; ?>"/><!-- change by darshan -->

                                <input type="button" class="btn" value="<?php echo LINES; ?>"/><!-- change by darshan -->

                                <input type="button" class="btn" value="<?php echo LINES_WITH_STEPS; ?>"/><!-- change by darshan -->

                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Bar Chat PORTLET-->
            </div>
        </div>


    </div>
    <!-- END PAGE CONTENT-->
</div>
<!-- BEGIN PAGE CONTAINER-->
</div>
<!-- END PAGE -->
</div>
<!-- END CONTAINER -->

<!-- BEGIN JAVASCRIPTS -->
<!-- Load javascripts at bottom, this will reduce page load time -->

<script src="<?php echo base_url(); ?>assets/flot/jquery.flot.js"></script>
<script src="<?php echo base_url(); ?>assets/flot/jquery.flot.resize.js"></script>
<script src="<?php echo base_url(); ?>assets/flot/jquery.flot.pie.js"></script>
<script src="<?php echo base_url(); ?>assets/flot/jquery.flot.stack.js"></script>
<script src="<?php echo base_url(); ?>assets/flot/jquery.flot.crosshair.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script src="<?php echo base_url(); ?>assets/fancybox/source/jquery.fancybox.pack.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<script>
jQuery(document).ready(function () {
    // initiate layout and plugins
    App.init();

    chart2();
    chart5();
    chart_month_line();
    chart_month_bar();
    chart_year_line();
    chart_year_bar();
});
//Interactive Chart
function chart2() {

    var newuser = [
        <?php if($weekly_registration) {
            $i=1;
            foreach($weekly_registration as $date => $total) {
            echo '['.$i.','.$total.'],';
            $i++;
            }
        } ?>

    ];
    var fbuser = [
        <?php if($weekly_fb_registration) {
            $i=1;
            foreach($weekly_fb_registration as $date => $total) {
            echo '['.$i.','.$total.'],';
            $i++;
            }
        } ?>

    ];
    var twituser = [
        <?php if($weekly_tw_registration) {
            $i=1;
            foreach($weekly_tw_registration as $date => $total) {
            echo '['.$i.','.$total.'],';
            $i++;
            }
        } ?>
    ];

    var plot = $.plot($("#chart_2"), [{
        data: newuser,
        label: "<?php echo REGISTRATION;?>"
    }, {
        data: fbuser,
        label: "Facebook"
    }, {
        data: twituser,
        label: "Twitter"
    }], {
        series: {
            lines: {
                show: true,
                lineWidth: 2,
                fill: true,
                fillColor: {
                    colors: [{
                        opacity: 0.05
                    }, {
                        opacity: 0.01
                    }]
                }
            },
            points: {
                show: true
            },
            shadowSize: 2
        },
        grid: {
            hoverable: true,
            clickable: true,
            tickColor: "#eee",
            borderWidth: 0
        },
        colors: ["#FCB322", "#A5D16C", "#52e136"],
        xaxis: {
            ticks: [[1, 'Mon'], [2, 'Tue'], [3, 'Wed'], [4, 'Thurs'], [5, 'Fri'], [6, 'Sat'], [7, 'Sun']],
            tickDecimals: 0
        },
        yaxis: {
            ticks: 11,
            tickDecimals: 0
        }
    });


    function showTooltip(x, y, contents) {
        $('<div id="tooltip">' + contents + '</div>').css({
            position: 'absolute',
            display: 'none',
            top: y + 5,
            left: x + 15,
            border: '1px solid #333',
            padding: '4px',
            color: '#fff',
            'border-radius': '3px',
            'background-color': '#333',
            opacity: 0.80
        }).appendTo("body").fadeIn(200);
    }

    var previousPoint = null;
    $("#chart_2").bind("plothover", function (event, pos, item) {
        $("#x").text(pos.x.toFixed(2));
        $("#y").text(pos.y.toFixed(2));

        if (item) {
            if (previousPoint != item.dataIndex) {
                previousPoint = item.dataIndex;

                $("#tooltip").remove();
                var x = item.datapoint[0].toFixed(2),
                    y = item.datapoint[1].toFixed(2);

                showTooltip(item.pageX, item.pageY, item.series.label + " of " + x + " = " + y);
            }
        } else {
            $("#tooltip").remove();
            previousPoint = null;
        }
    });
}

//bars with controls
function chart5() {
    var newuser = [
        <?php if($weekly_registration) {
            $i=1;
            foreach($weekly_registration as $date => $total) {
            echo '['.$i.','.$total.'],';
            $i++;
            }
        } ?>

    ];
    var fbuser = [
        <?php if($weekly_fb_registration) {
            $i=1;
            foreach($weekly_fb_registration as $date => $total) {
            echo '['.$i.','.$total.'],';
            $i++;
            }
        } ?>

    ];
    var twituser = [
        <?php if($weekly_tw_registration) {
            $i=1;
            foreach($weekly_tw_registration as $date => $total) {
            echo '['.$i.','.$total.'],';
            $i++;
            }
        } ?>
    ];
    // for (var i = 0; i <= 10; i += 1)
    //d3.push([i, parseInt(Math.random() * 30)]);

    var stack = 0,
        bars = true,
        lines = false,
        steps = false;

    function plotWithOptions() {
        $.plot($("#chart_5"), [{
            data: newuser,
            label: "<?php echo REGISTRATION;?>"
        }, {
            data: fbuser,
            label: "Facebook"
        }, {
            data: twituser,
            label: "Twitter"
        }], {
            series: {
                stack: stack,
                lines: {
                    show: lines,
                    fill: true,
                    steps: steps
                },
                bars: {
                    show: bars,
                    barWidth: 0.6
                },
            },
            xaxis: {
                ticks: [[1, 'Mon'], [2, 'Tue'], [3, 'Wed'], [4, 'Thurs'], [5, 'Fri'], [6, 'Sat'], [7, 'Sun']],
                tickDecimals: 0
            },
            yaxis: {
                ticks: 11,
                tickDecimals: 0
            }
        });
    }

    $(".stackControls input").click(function (e) {
        e.preventDefault();
        stack = $(this).val() == "With stacking" ? true : null;
        plotWithOptions();
    });
    $(".graphControls input").click(function (e) {
        e.preventDefault();
        bars = $(this).val().indexOf("Bars") != -1;
        lines = $(this).val().indexOf("Lines") != -1;
        steps = $(this).val().indexOf("steps") != -1;
        plotWithOptions();
    });

    plotWithOptions();
}

function chart_month_line() {

    var newproj = [
        <?php if($monthly_registration) {
            $i=1;
            foreach($monthly_registration as $date => $total) {
            echo '['.$i.','.$total.'],';
            $i++;
            }
        } ?>

    ];
    var activeproj = [
        <?php if($monthly_fb_registration) {
            $i=1;
            foreach($monthly_fb_registration as $date => $total) {
            echo '['.$i.','.$total.'],';
            $i++;
            }
        } ?>

    ];
    var successproj = [
        <?php if($monthly_tw_registration) {
            $i=1;
            foreach($monthly_tw_registration as $date => $total) {
            echo '['.$i.','.$total.'],';
            $i++;
            }
        } ?>
    ];

    var plot = $.plot($("#chart_month_line"), [{
        data: newproj,
        label: "<?php echo REGISTRATION;?>"
    }, {
        data: activeproj,
        label: "Facebook"
    }, {
        data: successproj,
        label: "Twitter"
    }], {
        series: {
            lines: {
                show: true,
                lineWidth: 2,
                fill: true,
                fillColor: {
                    colors: [{
                        opacity: 0.05
                    }, {
                        opacity: 0.01
                    }]
                }
            },
            points: {
                show: true
            },
            shadowSize: 2
        },
        grid: {
            hoverable: true,
            clickable: true,
            tickColor: "#eee",
            borderWidth: 0
        },
        colors: ["#FCB322", "#A5D16C", "#52e136"],
        xaxis: {
            ticks: [[1, 'Jan'], [2, 'Feb'], [3, 'Mar'], [4, 'Apr'], [5, 'May'], [6, 'Jun'], [7, 'Jul'], [8, 'Aug'], [9, 'Sep'], [10, 'Oct'], [11, 'Nov'], [12, 'Dec']],
            tickDecimals: 0
        },
        yaxis: {
            ticks: 11,
            tickDecimals: 0
        }
    });


    function showTooltip(x, y, contents) {
        $('<div id="tooltip">' + contents + '</div>').css({
            position: 'absolute',
            display: 'none',
            top: y + 5,
            left: x + 15,
            border: '1px solid #333',
            padding: '4px',
            color: '#fff',
            'border-radius': '3px',
            'background-color': '#333',
            opacity: 0.80
        }).appendTo("body").fadeIn(200);
    }

    var previousPoint = null;
    $("#chart_month_line").bind("plothover", function (event, pos, item) {
        $("#x").text(pos.x.toFixed(2));
        $("#y").text(pos.y.toFixed(2));

        if (item) {
            if (previousPoint != item.dataIndex) {
                previousPoint = item.dataIndex;

                $("#tooltip").remove();
                var x = item.datapoint[0].toFixed(2),
                    y = item.datapoint[1].toFixed(2);

                showTooltip(item.pageX, item.pageY, item.series.label + " of " + x + " = " + y);
            }
        } else {
            $("#tooltip").remove();
            previousPoint = null;
        }
    });
}

//bars with controls
function chart_month_bar() {
    var newproj = [
        <?php if($monthly_registration) {
            $i=1;
            foreach($monthly_registration as $date => $total) {
            echo '['.$i.','.$total.'],';
            $i++;
            }
        } ?>

    ];
    var activeproj = [
        <?php if($monthly_fb_registration) {
            $i=1;
            foreach($monthly_fb_registration as $date => $total) {
            echo '['.$i.','.$total.'],';
            $i++;
            }
        } ?>

    ];
    var successproj = [
        <?php if($monthly_tw_registration) {
            $i=1;
            foreach($monthly_tw_registration as $date => $total) {
            echo '['.$i.','.$total.'],';
            $i++;
            }
        } ?>
    ];
    // for (var i = 0; i <= 10; i += 1)
    //d3.push([i, parseInt(Math.random() * 30)]);

    var stack = 0,
        bars = true,
        lines = false,
        steps = false;

    function plotWithOptions() {
        $.plot($("#chart_month_bar"), [{
            data: newproj,
            label: "<?php echo REGISTRATION;?>"
        }, {
            data: activeproj,
            label: "Facebook"
        }, {
            data: successproj,
            label: "Twitter"
        }], {
            series: {
                stack: stack,
                lines: {
                    show: lines,
                    fill: true,
                    steps: steps
                },
                bars: {
                    show: bars,
                    barWidth: 0.6
                },
            },
            xaxis: {
                ticks: [[1, 'Jan'], [2, 'Feb'], [3, 'Mar'], [4, 'Apr'], [5, 'May'], [6, 'Jun'], [7, 'Jul'], [8, 'Aug'], [9, 'Sep'], [10, 'Oct'], [11, 'Nov'], [12, 'Dec']],
                tickDecimals: 0
            },
            yaxis: {
                ticks: 11,
                tickDecimals: 0
            }
        });
    }

    $(".stackControls input").click(function (e) {
        e.preventDefault();
        stack = $(this).val() == "With stacking" ? true : null;
        plotWithOptions();
    });
    $(".graphControls input").click(function (e) {
        e.preventDefault();
        bars = $(this).val().indexOf("Bars") != -1;
        lines = $(this).val().indexOf("Lines") != -1;
        steps = $(this).val().indexOf("steps") != -1;
        plotWithOptions();
    });

    plotWithOptions();
}

function chart_year_line() {

    var newproj = [
        <?php if($yearly_registration) {
                    $i=1;
                    foreach($yearly_registration as $date => $total) {
                    echo '['.$i.','.$total.'],';
                    $i++;
                    }
                } ?>
    ];
    var activeproj = [
        <?php if($yearly_fb_registration) {
                        $i=1;
                        foreach($yearly_fb_registration as $date => $total) {
                        echo '['.$i.','.$total.'],';
                        $i++;
                        }
                    } ?>

    ];
    var successproj = [
        <?php if($yearly_tw_registration) {
                    $i=1;
                    foreach($yearly_tw_registration as $date => $total) {
                    echo '['.$i.','.$total.'],';
                    $i++;
                    }
                } ?>
    ];

    var plot = $.plot($("#chart_year_line"), [{
        data: newproj,
        label: "<?php echo REGISTRATION;?>"
    }, {
        data: activeproj,
        label: "Facebook"
    }, {
        data: successproj,
        label: "Twitter"
    }], {
        series: {
            lines: {
                show: true,
                lineWidth: 2,
                fill: true,
                fillColor: {
                    colors: [{
                        opacity: 0.05
                    }, {
                        opacity: 0.01
                    }]
                }
            },
            points: {
                show: true
            },
            shadowSize: 2
        },
        grid: {
            hoverable: true,
            clickable: true,
            tickColor: "#eee",
            borderWidth: 0
        },
        colors: ["#FCB322", "#A5D16C", "#52e136"],
        xaxis: {
            ticks: [<?php
					$j=1;
					for($k=$year_first;$k<=$year_last;$k++) {
					echo '['.$j.','.$k.'],';
					$j++;
					} 
				 ?>],
            tickDecimals: 0
        },
        yaxis: {
            ticks: 11,
            tickDecimals: 0
        }
    });


    function showTooltip(x, y, contents) {
        $('<div id="tooltip">' + contents + '</div>').css({
            position: 'absolute',
            display: 'none',
            top: y + 5,
            left: x + 15,
            border: '1px solid #333',
            padding: '4px',
            color: '#fff',
            'border-radius': '3px',
            'background-color': '#333',
            opacity: 0.80
        }).appendTo("body").fadeIn(200);
    }

    var previousPoint = null;
    $("#chart_year_line").bind("plothover", function (event, pos, item) {
        $("#x").text(pos.x.toFixed(2));
        $("#y").text(pos.y.toFixed(2));

        if (item) {
            if (previousPoint != item.dataIndex) {
                previousPoint = item.dataIndex;

                $("#tooltip").remove();
                var x = item.datapoint[0].toFixed(2),
                    y = item.datapoint[1].toFixed(2);

                showTooltip(item.pageX, item.pageY, item.series.label + " of " + x + " = " + y);
            }
        } else {
            $("#tooltip").remove();
            previousPoint = null;
        }
    });
}

//bars with controls
function chart_year_bar() {
    var newproj = [
        <?php if($yearly_registration) {
        $i=1;
        foreach($yearly_registration as $date => $total) {
        echo '['.$i.','.$total.'],';
        $i++;
        }
    } ?>

    ];
    var activeproj = [
        <?php if($yearly_fb_registration) {
            $i=1;
            foreach($yearly_fb_registration as $date => $total) {
            echo '['.$i.','.$total.'],';
            $i++;
            }
        } ?>

    ];
    var successproj = [
        <?php if($yearly_tw_registration) {
        $i=1;
        foreach($yearly_tw_registration as $date => $total) {
        echo '['.$i.','.$total.'],';
        $i++;
        }
    } ?>
    ];
    // for (var i = 0; i <= 10; i += 1)
    //d3.push([i, parseInt(Math.random() * 30)]);

    var stack = 0,
        bars = true,
        lines = false,
        steps = false;

    function plotWithOptions() {
        $.plot($("#chart_year_bar"), [{
            data: newproj,
            label: "<?php echo REGISTRATION;?>"
        }, {
            data: activeproj,
            label: "Facebook"
        }, {
            data: successproj,
            label: "Twitter"
        }], {
            series: {
                stack: stack,
                lines: {
                    show: lines,
                    fill: true,
                    steps: steps
                },
                bars: {
                    show: bars,
                    barWidth: 0.6
                },
            },
            xaxis: {
                ticks: [<?php
					$j=1;
					for($k=$year_first;$k<=$year_last;$k++) {
					echo '['.$j.','.$k.'],';
					$j++;
					} 
				 ?>],
                tickDecimals: 0
            },
            yaxis: {
                ticks: 11,
                tickDecimals: 0
            }
        });
    }

    $(".stackControls input").click(function (e) {
        e.preventDefault();
        stack = $(this).val() == "With stacking" ? true : null;
        plotWithOptions();
    });
    $(".graphControls input").click(function (e) {
        e.preventDefault();
        bars = $(this).val().indexOf("Bars") != -1;
        lines = $(this).val().indexOf("Lines") != -1;
        steps = $(this).val().indexOf("steps") != -1;
        plotWithOptions();
    });

    plotWithOptions();
} 
</script>
<!-- END JAVASCRIPTS -->
