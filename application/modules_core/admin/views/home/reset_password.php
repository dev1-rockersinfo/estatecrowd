<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/additional-methods.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.maskedinput.js"></script>
<script>
    // validations
    jQuery(function ($) {
        // validate signup form on keyup and submit
        var validator = $("#reset_frm").validate({
            rules: {
                password: {
                    required: true,
                    minlength: 8,
                    maxlength: 20
                },
                cpassword: {
                    required: true,
                    minlength: 8,
                    maxlength: 20,
                    equalTo: "#password"
                }
            },
            messages: {
                password: {
                    required: '<?php echo PASSWORD_IS_REQUIRED;?>',
                    minlength: '<?php echo ENTER_ATLEAST_EIGHT_CHARACTER;?>',
                    maxlength: '<?php echo YOU_CAN_NOT_ENTER_MORE_THAN_TWE_CHARACTER;?>'
                },
                cpassword: {
                    required: '<?php echo RETYPE_PASSWORD_IS_REQUIRED; ?>',
                    minlength: '<?php echo ENTER_ATLEAST_EIGHT_CHARACTER;?>',
                    maxlength: '<?php echo YOU_CAN_NOT_ENTER_MORE_THAN_TWE_CHARACTER;?>',
                    equalTo: '<?php echo PASSWORD_DOES_NOT_MATCH; ?>'
                }
            },
            // the errorPlacement has to take the table layout into account
            errorPlacement: function (error, element) {
                if (element.is(":radio"))
                    error.insertAfter(element.parent());
                else if (element.is(":checkbox"))
                    error.insertAfter(element.next());
                else if (element.hasClass('book_job_now_price_input')) {
                    error.insertAfter(element.next().next());
                }
                else {
                    if (element.attr('name') == 'job_price') {
                        error.insertAfter(element.next());
                    }
                    else
                        error.insertAfter(element);
                }
            },
            // specifying a submitHandler prevents the default submit, good for the demo
            /*submitHandler: function() {
             alert("submitted!");
             },*/
            // set this class to error-labels to indicate valid fields
            success: function (label) {
                // set &nbsp; as text for IE
                /*	label.html("&nbsp;").addClass("checked");*/
            },
            /*highlight: function(element, errorClass) {
             $(element).parent().next().find("." + errorClass).removeClass("checked");
             },*/
            onkeyup: function (element) {
                $(element).valid();
            },
            onfocusout: function (element) {
                $(element).valid();
            },
        });
    });
</script>


<?php if ($error) { ?>
    <div class="alert alert-message alert-danger" role="alert">
        <?php echo $error; ?>

    </div>
<?php } ?>
<section>
    <div class="container padTB50">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="page-header">
                    <h1><?php echo RESET_PASSWORD; ?></h1>
                </div>
                <div class="in-form">
                    <?php $att = array('id' => 'reset_frm', 'name' => 'reset_frm');
                    echo form_open('home/reset_password/' . $forgot_unique_code, $att);
                    ?>
                    <div class="form-group">
                        <input type="password" name="password" id="password" value="" class="form-control"
                               placeholder="<?php echo PASSWORD ?>">
                    </div>
                    <div class="form-group">
                        <input type="password" name="cpassword" id="cpassword" value="" class="form-control"
                               placeholder="<?php echo CONFIRM_PASSWORD; ?>">
                    </div>
                    <div class="in-form-footer text-right">
                        <input type="submit" value="<?php echo RESET; ?>" class="btn-main" name="<?php echo RESET; ?>">
                        <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id; ?>">
                        <input type="hidden" name="forget_unique_code" id="forget_unique_code"
                               value="<?php echo $forgot_unique_code; ?>">
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
