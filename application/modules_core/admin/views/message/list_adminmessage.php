<script>
    function setaction(elename, actionval, actionmsg, formname) {
        vchkcnt = 0;
        elem = document.getElementsByName(elename);
        for (i = 0; i < elem.length; i++) {
            if (elem[i].checked) vchkcnt++;
        }
        if (vchkcnt == 0) {
            alert('<?php echo DELETE_SELECT_RECORD_CONFIRMATION;?>')
        } else {
            if (confirm(actionmsg)) {
                document.getElementById('action').value = actionval;
                document.getElementById(formname).submit();
            }
        }
    }
</script>
<?php
$site_setting = site_setting();
$site_name = $site_setting['site_name'];
?>
<!-- BEGIN CONTAINER -->
<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php echo $this->load->view('admin_sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN THEME CUSTOMIZER-->
                    <div id="theme-change" class="hidden-phone">
                        <i class="icon-cogs"></i>
                        <span class="settings">
                        <span class="text"><?php echo THEME; ?>:</span> <!-- chaneg  by darshan -->
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
                    </div>
                    <!-- END THEME CUSTOMIZER-->
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        <?php echo INBOX; ?>
                    </h3>
                    <ul class="breadcrumb">
                        <li>
                            <a href="<?php echo site_url('admin/home/dashboard'); ?>"><i class="icon-home"></i></a><span
                                class="divider">&nbsp;</span>
                        </li>

                        <li>
                            <a href="<?php echo site_url('admin/message/list_adminmessage'); ?>"><?php echo INBOX; ?></a><span
                                class="divider-last">&nbsp;</span></li>
                    </ul>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->

            <?php if ($msg != '') {
                if ($msg == 'delete') {
                    ?>
                    <div class="alert alert-success">
                        <button class="close" data-dismiss="alert">x</button>
                        <strong><?php echo SUCCESS; ?>!</strong> <?php echo RECORDS_HAS_BEEN_DELETED_SUCCESSFULLY; ?>.
                    </div>
                <?php
                }
            } ?>

            <!-- BEGIN ADVANCED TABLE widget-->

            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN EXAMPLE TABLE widget-->
                    <div class="widget">
                        <div class="widget-title">
                            <h4><i class="icon-inbox"></i><?php echo INBOX; ?></h4>
                            <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                            </span>
                        </div>


                        <div class="widget-body">
                            <div class="mail-option">
                                <a href="javascript://" class="btn"
                                   onclick="setaction('chk[]','delete', '<?php echo ARE_YOU_SURE_YOU_WANT_TO_DELETE_SELECTED_MESSAGE; ?>', 'frm_listproject')"><i
                                        class="icon-trash"></i> <?php echo DELETE; ?>  </a> <!-- chaneg  by darshan -->
                            </div>
                            <?php
                            $attributes = array('name' => 'frm_listproject', 'id' => 'frm_listproject');

                            echo form_open('admin/message/action_message/', $attributes);

                            ?>
                            <input type="hidden" name="action" id="action"/>


                            <table class="table table-inbox table-hover" id="sample_1">
                                <thead style="display:none">

                                <tr>

                                    <th>
                                    </th>
                                    <th>
                                    </th>
                                    <th>
                                    </th>
                                    <th>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if ($result) {

                                    foreach ($result as $row) {
                                        
                                        $message_id = $row->message_id;
                                        
                                        $get_unread_count=getAdminMessageUnreadCount($message_id);
                                        
                                        $sender_id=$row->sender_id;
                                        $sender = UserData($sender_id);
                                        /*if ($row->admin_replay == 'admin') {
                                            $receiver = adminsendmsg($row->receiver_id);
                                        }*/
                                        
                                        
                                        $equity_id = $row->equity_id;
                                        $equity = GetOneEquity($equity_id);
                                        $equity_user_id = $equity['user_id'];
                                        
                                        
                                        $sender_user_id = $sender[0]['user_id'];
                                        $sender_user_name = $sender[0]['user_name'];
                                        $sender_user_last = $sender[0]['last_name'];
                                        $is_read = $row->is_read;
                                        if ($get_unread_count>0) {
                                            $is_read_class = 'unread';
                                        } else {
                                            $is_read_class = '';
                                        }
                                        //$receiver_user_id=$receiver[0]['user_id'];
                                        //$receiver_user_name=$receiver[0]['user_name'];
                                        //$receiver_last_name=$receiver[0]['last_name'];
                                        $message_subject = $row->message_subject;
                                        $message_date = date($site_setting['date_format'], strtotime($row->date_added));

                                        
                                            $user_name = $sender_user_name . ' ' . $sender_user_last;
                                            $sender_user_image = $sender[0]['image'];
                                            if ($sender_user_image != '') {

                                                if (file_exists(base_path() . 'upload/user/user_small_image/' . $sender_user_image)) {

                                                    $user_image = base_url() . 'upload/user/user_small_image/' . $sender_user_image;

                                                } else {

                                                    $user_image = base_url() . 'upload/user/user_small_image/no_man.jpg';

                                                }

                                            } else {

                                                $user_image = base_url() . 'upload/user/user_small_image/no_man.jpg';

                                            }
                                       

                                        if (isset($sender[0]['user_id']) && $sender[0]['user_id'] != '') {
                                            ?>

                                           
                                            <tr class="msg<?php echo $message_id; ?> <?php echo $is_read_class; ?>">
                                                <td class="inbox-small-cells"><input type="checkbox" class="checkboxes"
                                                                                     name="chk[]" id="chk"
                                                                                     value="<?php echo $message_id; ?>"/>
                                                </td>
                                                <td class="view-message dont-show">
                                                    <span class="creater fl"><img
                                                            src="<?php echo $user_image; ?>"></span>
                                                    <span class="creater-name"><?php echo ucwords($user_name); ?> <?php if($get_unread_count>0) { ?> <?php echo "(".$get_unread_count.")"; ?> <?php } ?></span>

                                                </td>

                                                <td class="view-message "><?php echo $message_subject; ?></td>

                                                <td class="text-right view-message"><?php echo $message_date; ?></td>

                                            </tr>
                                            
                                             <script type="text/javascript">
                                                $(document).ready(function () {
                                                    $(".table-inbox tr.msg<?php echo $message_id; ?> td:not(:first-child) ").click(function () {
                                                        window.location = "<?php echo site_url('admin/message/view_message/'.$equity_id.'/'.$sender_id)?>";
                                                    });
                                                });

                                            </script>
                                        <?php
                                        }
                                    }
                                }    ?>
                                </tbody>
                            </table>
                            </form>
                        </div>

                    </div>
                    <!-- END EXAMPLE TABLE widget-->
                </div>
            </div>

            <!-- END ADVANCED TABLE widget-->

            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>
<!-- END CONTAINER -->

<!-- BEGIN JAVASCRIPTS -->
<!-- Load javascripts at bottom, this will reduce page load time -->


<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>


<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/jquery.dataTables.js"></script>


<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/DT_bootstrap.js"></script>


<script src="<?php echo base_url(); ?>js/scripts.js"></script>

<script type="text/javascript" charset="utf-8">

    jQuery(document).ready(function () {


        App.init();


    });


</script>
