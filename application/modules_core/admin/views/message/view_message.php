<?php


$user_name = $user_detail[0]['user_name'];
$last_name = $user_detail[0]['last_name'];
$user_profile_slug = $user_detail[0]['profile_slug'];


$company_name = $equity_data['company_name'];
$equity_url = $equity_data['equity_url'];
$equity_user_id = $equity_data['user_id'];




$taxonomy_setting = taxonomy_setting();
$project_url = $taxonomy_setting['project_url'];
$funds = $taxonomy_setting['funds'];

?>
<!-- BEGIN CONTAINER -->

<div id="container" class="row-fluid">

<!-- BEGIN SIDEBAR -->

<?php echo $this->load->view('admin_sidebar'); ?>

<!-- END SIDEBAR -->

<!-- BEGIN PAGE -->

<div id="main-content">

<!-- BEGIN PAGE CONTAINER-->

<div class="container-fluid">

<!-- BEGIN PAGE HEADER-->

<div class="row-fluid">

    <div class="span12">

        <!-- BEGIN THEME CUSTOMIZER-->

        <div id="theme-change" class="hidden-phone">

            <i class="icon-cogs"></i>

                        <span class="settings">

                           <span class="text"><?php echo THEME; ?>:</span> <!-- chaneg  by darshan -->

                            <span class="colors">

                                <span class="color-default" data-style="default"></span>

                                <span class="color-gray" data-style="gray"></span>

                                <span class="color-purple" data-style="purple"></span>

                                <span class="color-navy-blue" data-style="navy-blue"></span>

                            </span>

                        </span>

        </div>

        <!-- END THEME CUSTOMIZER-->

        <!-- BEGIN PAGE TITLE & BREADCRUMB-->

        <h3 class="page-title">

            <?php echo CONVERSATION; ?>

            <small></small>

        </h3>

        <ul class="breadcrumb">

            <li>

                <a href="<?php echo site_url('admin/home/dashboard'); ?>"><i class="icon-home"></i></a><span
                    class="divider">&nbsp;</span>

            </li>


            <li><a href="#"><?php echo CONVERSATION; ?></a><span class="divider-last">&nbsp;</span></li>

        </ul>

        <!-- END PAGE TITLE & BREADCRUMB-->

    </div>

</div>

<!-- END PAGE HEADER-->

<!-- BEGIN PAGE CONTENT-->
<?php
if ($this->session->flashdata('sucess_message') != '') {
    ?>
    <div class="alert alert-success">
        <button class="close" data-dismiss="alert">x</button>
        <strong><?php echo SUCCESS; ?>!</strong> <?php echo $this->session->flashdata('sucess_message'); ?>.
    </div>
<?php } ?>



<?php

if ($error_message != '') { ?>




        <div class="alert alert-error">

            <button class="close" data-dismiss="alert">x</button>

            <strong><?php echo ERRORS; ?>!</strong> <?php echo $error_message; ?></div>







    <?php
  

} ?>



<div class="row-fluid">

<div class="span12">

<!-- BEGIN INLINE NOTIFICATIONS widget-->

<div class="widget">

<div class="widget-title">

    <h4> <a href="<?php echo site_url($project_url . '/' . $equity_url) ?>" target="_blank"><?php echo $company_name; ?></a>
        <?php
        if ($message_type == 2) {
            echo ' ' . $funds . ' ' . PROCESS;
        } else {
            echo " " . FEEDBACK;
        }
        ?>
    </h4>
    
    

    <div class="tools">

        <a href="javascript:;" class="collapse"></a>

        <a href="#widget-config" data-toggle="modal" class="config"></a>

        <a href="javascript:;" class="reload"></a>

        <a href="javascript:;" class="remove"></a>

    </div>

</div>


<div class="widget-body">


    <div class="row-fluid">

        <div class="span12">

            <!-- BEGIN CHAT PORTLET-->

            <div class="widget" id="">


                <div class="widget-body">

                    <ul class="chats normal-chat">

                        <h4> <?php echo CONVERSATION_WITH; ?> : <a
                                href="<?php echo site_url('user/' . $user_profile_slug); ?>"
                                target="_blank"><?php echo $user_name . ' ' . $last_name; ?> </a>
                        </h4>

                        <?php
                        
                        
                        
                        if ($message_thread) {
                            $i = 0;

                            foreach ($message_thread as $msg) {
                                
                               if ($msg->admin_replay == 'admin') { 
                                   $class = 'out';
                               } else {
                                   $class = 'in';
                               }

                                /*if ($msg->sender_id == $this->session->userdata('admin_id'))
                                    $class = 'out';

                                else $class = 'in';*/
                                

                                if ($msg->admin_replay == 'admin') {
                                   
                                    $admin_id=1;
                                    if($msg->created_id>0){
                                        $admin_id=$msg->created_id;
                                    }
                                    
                                    $sender = AdminUserData($admin_id);

                                    $image_sender = base_url() . 'upload/user/user_small_image/no_man.jpg';


                                    $sender_user_id = $sender[0]['admin_id'];

                                    $sender_user_name = $sender[0]['username'];

                                    $sender_user_last = '';

                                    $sender_profile_link = 'javascript:void(0)';
                                } else {
                                     $sender = UserData($msg->sender_id);


                                    if ($sender[0]['image'] != '') {

                                        if (file_exists(base_path() . 'upload/user/user_small_image/' . $sender[0]['image'])) {

                                            $image_sender = base_url() . 'upload/user/user_small_image/' . $sender[0]['image'];
                                        } else {

                                            $image_sender = base_url() . 'upload/user/user_small_image/no_man.jpg';

                                        }

                                    } else {

                                        $image_sender = base_url() . 'upload/user/user_small_image/no_man.jpg';

                                    }

                                    $sender_user_id = $sender[0]['user_id'];

                                    $sender_user_name = $sender[0]['user_name'];

                                    $sender_user_last = $sender[0]['last_name'];

                                    $sender_profile_slug = $sender[0]['profile_slug'];
                                    $sender_profile_link = site_url('user/' . $sender_profile_slug);
                                }


                                /*$receiver = UserData($msg->receiver_id);


                                if ($receiver[0]['image'] != '') {

                                    if (file_exists(base_path() . 'upload/user/user_small_image/' . $receiver[0]['image'])) {

                                        $image_receiver = base_url() . 'upload/user/user_small_image/' . $receiver[0]['image'];

                                    } else {

                                        $image_receiver = base_url() . 'upload/user/user_small_image/no_man.gif';

                                    }

                                } else {

                                    $image_receiver = base_url() . 'upload/user/user_small_image/no_man.gif';

                                }

                                $receiver_user_id = $receiver[0]['user_id'];

                                $receiver_user_name = $receiver[0]['user_name'];

                                $receiver_last_name = $receiver[0]['last_name'];*/


                                $message_content = $msg->message_content;

                                $message_date = date($site_setting['date_format'], strtotime($msg->date_added));

                                $message_subject = $msg->message_subject;?>



                                <li class="<?php echo $class; ?>">

                                    <a href="<?php echo $sender_profile_link; ?>" target="_blank"> <img class="avatar"
                                                                                                        alt=""
                                                                                                        src="<?php echo $image_sender; ?>"/></a>

                                    <div class="message ">

                                        <span class="arrow"></span>

                                        <a href="<?php echo $sender_profile_link; ?>" target="_blank"
                                           class="name"><?php echo ucwords($sender_user_name . ' ' . $sender_user_last); ?></a>

                                        <span class="datetime"><?php echo $message_date; ?></span>

													<span class="body">

													   <?php echo $message_content; ?>

													</span>

                                    </div>

                                </li>



                                <?php

                                $i++;

                            }

                        }


                        ?>


                    </ul>
                    <?php

                    $att = array('id' => 'message_form', 'name' => 'message_form');
                    echo form_open('admin/message/send_mail_project_profile/' . $equity_id . '/' . $user_id, $att);
                    ?>
                    <div class="chat-form">
                        <div class="input-cont">
                            <input type="text" name="comments" id="comments1"
                                   placeholder="<?php echo TYPE_A_MESSAGE_HERE; ?>">
                            <!-- change by darshan -->
                        </div>
                        <div class="btn-cont">
                            <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id; ?>"/>
                            
                            <input type="hidden" name="message_type" id="message_type" value="<?php echo $message_type; ?>"/>
                            
                            <button type="submit" value="" class="btn btn-primary" name="submit"
                                    id="submit"><?php echo SEND; ?></button>

                        </div>
                    </div>
                </form>

                </div>

            </div>

            <!-- END CHAT PORTLET-->

        </div>


    </div>

</div>

</div>

<!-- END INLINE NOTIFICATIONS widget-->

</div>

</div>

<!-- END PAGE CONTENT-->

</div>

<!-- END PAGE CONTAINER-->

</div>

<!-- BEGIN PAGE -->

</div>

<!-- END CONTAINER -->


<!-- BEGIN JAVASCRIPTS -->

<!-- Load javascripts at bottom, this will reduce page load time -->


<script type="text/javascript"
        src="<?php echo base_url(); ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>

<script src="<?php echo base_url(); ?>js/scripts.js"></script>

<script>

    jQuery(document).ready(function () {

        // initiate layout and plugins

        App.init();

    });

</script>

<!-- END JAVASCRIPTS -->

