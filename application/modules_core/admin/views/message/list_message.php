<script>
    function setaction(elename, actionval, actionmsg, formname) {
        vchkcnt = 0;
        elem = document.getElementsByName(elename);
        for (i = 0; i < elem.length; i++) {
            if (elem[i].checked) vchkcnt++;
        }
        if (vchkcnt == 0) {
            alert('<?php echo DELETE_SELECT_RECORD_CONFIRMATION;?>')
        } else {
            if (confirm(actionmsg)) {
                document.getElementById('action').value = actionval;
                document.getElementById(formname).submit();
            }
        }
    }
</script>
<!-- BEGIN CONTAINER -->
<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php echo $this->load->view('admin_sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN THEME CUSTOMIZER-->
                    <div id="theme-change" class="hidden-phone">
                        <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text"><?php echo THEME; ?>:</span> <!-- chaneg  by darshan -->
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
                    </div>
                    <!-- END THEME CUSTOMIZER-->
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        <?php echo MESSAGES; ?>
                    </h3>
                    <ul class="breadcrumb">
                        <li>
                            <a href="<?php echo site_url('admin/home/dashboard'); ?>"><i class="icon-home"></i></a><span
                                class="divider">&nbsp;</span>
                        </li>

                        <li>
                            <a href="<?php echo site_url('admin/message/list_message'); ?>"><?php echo MESSAGES; ?></a><span
                                class="divider-last">&nbsp;</span></li>
                    </ul>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->

            <?php if ($msg != '') {
                if ($msg == 'delete') {
                    ?>
                    <div class="alert alert-success">
                        <button class="close" data-dismiss="alert">x</button>
                        <strong><?php echo SUCCESS; ?>!</strong> <?php echo RECORDS_HAS_BEEN_DELETED_SUCCESSFULLY; ?>.
                    </div>
                <?php
                }
            } ?>

            <!-- BEGIN ADVANCED TABLE widget-->

            <div class="fr iconM">
                <button type="button" class="btn btn-danger"
                        onclick="setaction('chk[]','delete', '<?php echo ARE_YOU_SURE_YOU_WANT_TO_DELETE_SELECTED_MESSAGE; ?>?', 'frm_listproject')">
                    <i class="icon-remove-sign icon-white"></i> <?php echo DELETE; ?></button>
                <!-- change by darshan -->
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN EXAMPLE TABLE widget-->
                    <div class="widget">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i><?php echo MESSAGE_LIST; ?></h4>
                            <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                            </span>
                        </div>


                        <div class="widget-body">
                            <?php
                            $attributes = array('name' => 'frm_listproject', 'id' => 'frm_listproject');

                            echo form_open('admin/message/action_message/', $attributes);

                            ?>
                            <input type="hidden" name="action" id="action"/>

                            <table class="table table-striped table-bordered table_project" id="sample_1">
                                <thead>
                                <tr>
                                    <th style="width:8px;"><input type="checkbox" class="group-checkable"
                                                                  data-set="#sample_1 .checkboxes"/></th>
                                    <th><?php echo SENDER; ?></th>
                                    <th class="hidden-phone tab_hidden"><?php echo RECEIVER; ?></th>
                                    <th class="hidden-phone"><?php echo SUBJECT; ?></th>
                                    <th><?php echo VIEW; ?></th>
                                    <th class="hidden-phone"><?php echo DATE; ?></th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if ($result) {
                                    foreach ($result as $row) {
                                        $sender = UserData($row->sender_id);
                                        $receiver = UserData($row->receiver_id);
                                        $message_id = $row->message_id;
                                        $sender_user_id = $sender[0]['user_id'];
                                        $sender_user_name = $sender[0]['user_name'];
                                        $sender_user_last = $sender[0]['last_name'];
                                        $receiver_user_id = $receiver[0]['user_id'];
                                        $receiver_user_name = $receiver[0]['user_name'];
                                        $receiver_last_name = $receiver[0]['last_name'];
                                        $message_subject = $row->message_subject;
                                        $message_date = date($site_setting['date_format'], strtotime($row->date_added));
                                        if ($row->type != '0') {

                                            $getadmindata = adminsendmsg($row->type);
                                            
                                            $user_name = $getadmindata->username;
                                        } else {
                                            $user_name = $sender_user_name . ' ' . $sender_user_last;
                                        }

                                        if (isset($sender[0]['user_id']) && $sender[0]['user_id'] != '') {
                                            ?>
                                            <tr class="odd gradeX">
                                                <td><input type="checkbox" class="checkboxes" name="chk[]" id="chk"
                                                           value="<?php echo $message_id; ?>"/></td>
                                                <td>
                                                    <a href="<?php echo site_url('member/' . $sender_user_id); ?>"><?php echo ucwords($user_name); ?></a>
                                                </td>
                                                <td class="hidden-phone tab_hidden"><a
                                                        href="<?php echo site_url('member/' . $receiver_user_id); ?>"><?php echo ucwords($receiver_user_name . ' ' . $receiver_last_name); ?></a>
                                                </td>
                                                <td class="hidden-phone"><?php echo $message_subject; ?></td>
                                                <td class="text_center hidden-phone"><a
                                                        href="<?php echo site_url('admin/message/view_message/' . $message_id); ?>"><i
                                                            class="icon-edit"></i></a></td>
                                                <td class="text_center hidden-phone"><?php echo $message_date; ?></td>

                                            </tr>
                                        <?php
                                        }
                                    }
                                }    ?>
                                </tbody>
                            </table>
                            </form>
                        </div>

                    </div>
                    <!-- END EXAMPLE TABLE widget-->
                </div>
            </div>

            <!-- END ADVANCED TABLE widget-->

            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>
<!-- END CONTAINER -->

<!-- BEGIN JAVASCRIPTS -->
<!-- Load javascripts at bottom, this will reduce page load time -->

<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/DT_bootstrap.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        App.init();
    });

    jQuery(document).ready(function () {


        $('#sample_1').dataTable({
            "bDestroy": true,
            "aoColumnDefs": [{"bSortable": false, "aTargets": [0]}],
            "oLanguage": {
                "sLengthMenu": " _MENU_ <?php echo RECORD_PER_PAGE; ?>",
                "sZeroRecords": "<?php echo NOTHING_FOUND_SORRY; ?>",
                "sInfo": "<?php echo SHOWING; ?> _START_ to _END_ of _TOTAL_ <?php echo RECORD; ?>",
                "sInfoEmpty": "<?php echo SHOWING; ?> 0 to 0 of 0 <?php echo RECORD; ?>",
                "sSearch": "<?php echo SEARCH; ?>: ",
                'oPaginate': {

                    'sPrevious': '<?php echo PREVIOUS; ?>',
                    'sNext': '<?php echo NEXT; ?>'
                }
            }

        });


    });

</script>
