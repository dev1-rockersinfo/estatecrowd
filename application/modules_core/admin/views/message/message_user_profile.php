<?php $site_name = $site_setting['site_name']; ?>

<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/additional-methods.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.maskedinput.js"></script>


<div id="container" class="row-fluid">
    <?php
    $user_detail = UserData($user_id);
    $user_name = $user_detail[0]['user_name'];
    $last_name = $user_detail[0]['last_name'];

    ?>
    <!-- BEGIN SIDEBAR -->
    <?php echo $this->load->view('admin_sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN THEME CUSTOMIZER-->
                    <div id="theme-change" class="hidden-phone">
                        <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text"><?php echo THEME; ?></span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
                    </div>
                    <!-- END THEME CUSTOMIZER-->
                    <h3 class="page-title">
                        <?php echo MESSAGE; ?>
                    </h3>

                </div>
            </div>
            <!-- END PAGE HEADER-->
            <?php
            if ($error_message != "") {
                ?>
                <div class="alert alert-error">
                    <button class="close" data-dismiss="alert">x</button>
                    <strong><?php echo ERRORS; ?>!</strong> <?php echo $error_message; ?></div>
            <?php
            }
            ?>
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                <?php if ($sucess_message != "") {
                    ?>
                    <div class="alert alert-success">
                        <button class="close" data-dismiss="alert">x</button>
                        <strong><?php echo SUCCESS; ?>!</strong> <?php echo $sucess_message; ?>.
                    </div>
                <?php
                }
                ?>
                <div class="span12 sortable">
                    <!-- BEGIN SAMPLE FORMPORTLET-->
                    <div class="widget">

                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i> <?php echo MESSAGE; ?></h4>
                                        <span class="tools">
                                        <a href="javascript:;" class="icon-chevron-down"></a>
                                        <a href="javascript:;" class="icon-remove"></a>
                                        </span>
                        </div>
                        <div class="widget-body">
                            <!-- BEGIN FORM-->
                            <?php

                            $att = array('id' => 'message_form', 'name' => 'message_form');
                            echo form_open('admin/message/send_mail_project_profile/' . $id . '/' . $user_id, $att);
                            ?>

                            <div class="control-group">
                                <label class="control-label"><?php echo SUBJECT; ?></label>

                                <div class="controls">
                                    <input type="text" placeholder="<?php echo SUBJECT; ?>" class="input-xlarge"
                                           name="subject" id="subject" value=""/>

                                </div>
                            </div>


                            <div class="control-group">
                                <label class="control-label"><?php echo MESSAGE; ?></label>

                                <div class="controls">
                                    <textarea name="comments" id="comments1" rows="6" cols="60"></textarea>

                                </div>
                            </div>
                            <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id; ?>"/>


                            <div class="form-actions">


                                <button type="submit" value="<?php echo SEND_MESSAGE; ?>" class="btn" name="submit"
                                        id="submit"><?php echo SEND_MESSAGE; ?></button>

                            </div>
                            </form>
                            <!-- END F
   <script type="text/javascript" src="<?php echo base_url(); ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
   <script src="<?php echo base_url(); ?>js/scripts.js"></script>
   <script>
      jQuery(document).ready(function() {       
         // initiate layout and plugins
         App.init();
      });
   </script>ORM-->
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>

            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->

<!-- END FOOTER -->


<script type="text/javascript"
        src="<?php echo base_url(); ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        App.init();
    });
</script>
