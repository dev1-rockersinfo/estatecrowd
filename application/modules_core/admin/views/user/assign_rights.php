<!-- BEGIN CONTAINER -->
<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php echo $this->load->view('admin_sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN THEME CUSTOMIZER-->
                    <div id="theme-change" class="hidden-phone">
                        <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text"><?php echo THEME; ?></span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
                    </div>
                    <!-- END THEME CUSTOMIZER-->
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        <?php echo ASSIGN_RIGHTS; ?>
                    </h3>
                    <ul class="breadcrumb">
                        <li>
                            <a href="<?php echo site_url('admin/home/dashboard') ?>"><i class="icon-home"></i></a><span
                                class="divider">&nbsp;</span>
                        </li>

                        <li><a href="#"><?php echo ASSIGN_RIGHTS; ?></a><span class="divider-last">&nbsp;</span></li>
                    </ul>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->

            <!-- BEGIN ADVANCED TABLE widget-->

            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN EXAMPLE TABLE widget-->
                    <div class="widget">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i><?php echo USERS_LOGIN; ?></h4>
                            <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                            </span>
                        </div>

                        <div class="widget-body">
                            <?php
                            $attributes = array('name' => 'frm_assignrights');
                            echo form_open('admin/admin/add_rights/' . $admin_id, $attributes);
                            ?>

                            <table class="table table-striped table-bordered admin_log" id="sample_1">
                                <thead>
                                <tr>
                                    <th style="width:8px;"><input type="checkbox" class="group-checkable"
                                                                  data-set="#sample_1 .checkboxes"/></th>
                                    <th class="hidden-phone"><?php echo RIGHT_NAME; ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php

                                if ($rights) {
                                    foreach ($rights as $rig) {
                                        $right_id = $rig->rights_id;
                                        $chkd = '';
                                        if ($assign_rights) {
                                            if (in_array($rig->rights_id, $assign_rights)) {
                                                $chkd = 'checked="checked"';
                                            } else {
                                                $chkd = '';
                                            }
                                        }
                                        $right_name = str_replace('_', ' ', $rig->rights_name);
                                        ?>
                                        <tr class="odd gradeX">
                                            <td><input type="checkbox" class="checkboxes" name="rights_id[]"
                                                       value="<?php echo $right_id; ?>" <?php echo $chkd; ?> /></td>
                                            <td class="hidden-phone"><?php echo $right_name; ?></td>
                                        </tr>
                                    <?php
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                            <input type="hidden" name="admin_id" id="admin_id" value="<?php echo $admin_id; ?>"/>
                            <button type="submit" class="btn blue"><i class="icon-ok"></i><?php echo UPDATE; ?></button>
                            <button type="button" class="btn"
                                    onClick="location.href='<?php echo site_url('admin/admin/list_admin'); ?>'"><i
                                    class=" icon-remove"></i> <?php echo CANCEL; ?></button>
                            </form>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE widget-->
                </div>
            </div>

            <!-- END ADVANCED TABLE widget-->

            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>
<!-- END CONTAINER -->

<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/DT_bootstrap.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        App.init();

    });
</script>
