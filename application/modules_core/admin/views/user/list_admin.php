<script type="text/javascript" language="javascript">

    function delete_rec(id) {

        var ans = confirm("<?php echo DELETE_ADMINISTRATOR_CONFIRMATION;?>");

        if (ans) {

            location.href = "<?php echo site_url('admin/admin/delete_admin/'); ?>" + "/" + id;

        } else {

            return false;

        }

    }


    function setaction(elename, actionval, actionmsg, formname) {

        vchkcnt = 0;

        elem = document.getElementsByName(elename);


        for (i = 0; i < elem.length; i++) {

            if (elem[i].checked) vchkcnt++;

        }

        if (vchkcnt == 0) {

            alert("<?php echo DELETE_SELECT_RECORD_CONFIRMATION;?>");

            return false;

        } else {


            if (confirm(actionmsg)) {

                document.getElementById('action').value = actionval;

                document.getElementById(formname).submit();

                return true;

            }

        }

    }

</script>

<!-- BEGIN CONTAINER -->

<div id="container" class="row-fluid">

<!-- BEGIN SIDEBAR -->

<?php echo $this->load->view('admin_sidebar'); ?>

<!-- END SIDEBAR MENU -->

<!-- END SIDEBAR -->

<!-- BEGIN PAGE -->

<div id="main-content">

<!-- BEGIN PAGE CONTAINER-->

<div class="container-fluid">


<!-- BEGIN PAGE HEADER-->

<div class="row-fluid">

    <div class="span12">

        <!-- BEGIN THEME CUSTOMIZER-->

        <div id="theme-change" class="hidden-phone">

            <i class="icon-cogs"></i>

                        <span class="settings">

                            <span class="text">Theme:</span>

                            <span class="colors">

                                <span class="color-default" data-style="default"></span>

                                <span class="color-gray" data-style="gray"></span>

                                <span class="color-purple" data-style="purple"></span>

                                <span class="color-navy-blue" data-style="navy-blue"></span>

                            </span>

                        </span>

        </div>

        <!-- END THEME CUSTOMIZER-->

        <!-- BEGIN PAGE TITLE & BREADCRUMB-->

        <h3 class="page-title"><?php echo ADMINISTRATOR; ?></h3>

        <ul class="breadcrumb">

            <li>

                <a href="<?php echo site_url('admin/home/dashboard') ?>"><i class="icon-home"></i></a><span
                    class="divider">&nbsp;</span>

            </li>


            <li><a href="<?php echo site_url('admin/admin/list_admin') ?>"><?php echo ADMINISTRATOR; ?></a><span
                    class="divider-last">&nbsp;</span></li>

        </ul>

        <!-- END PAGE TITLE & BREADCRUMB-->

    </div>

</div>

<!-- END PAGE HEADER-->

<?php        if ($msg != "") {

    if ($msg == "insert") {
        ?>

        <div class="alert alert-success">

            <button class="close" data-dismiss="alert">x</button>

            <strong><?php echo SUCCESS; ?>!</strong> <?php echo NEW_RECORD_HAS_BEEN_ADDED_SUCCESSFULLY; ?></div>

    <?php
    }

    if ($msg == "cantdelete") {
        ?>

        <div class="alert alert-error">

            <button class="close" data-dismiss="alert">x</button>

            <strong><?php echo ERROR; ?>!</strong> <?php echo CANNOT_DELETE_ALL_THE_ADMIN; ?>.
        </div>

    <?php
    }

    if ($msg == "delete_login") {
        ?>

        <div class="alert alert-success">

            <button class="close" data-dismiss="alert">x</button>

            <strong><?php echo SUCCESS; ?>
                !</strong> <?php echo RECORD_HAS_BEEN_DELETED_SUCCESSFULLY_FROM_ADMIN_LOGIN; ?>.
        </div>

    <?php
    }

    if ($msg == "update") {
        ?>

        <div class="alert alert-success">

            <button class="close" data-dismiss="alert">x</button>

            <strong><?php echo SUCCESS; ?>!</strong> <?php echo RECORD_HAS_BEEN_UPDATED_SUCCESSFULLY; ?></div>

    <?php
    }

    if ($msg == "delete") {
        ?>

        <div class="alert alert-success">

            <button class="close" data-dismiss="alert">x</button>

            <strong><?php echo SUCCESS; ?>!</strong> <?php echo RECORD_HAS_BEEN_DELETED_SUCCESSFULLY; ?>.
        </div>

    <?php
    }

    if ($msg == "rights") {
        ?>

        <div class="alert alert-success">

            <button class="close" data-dismiss="alert">x</button>

            <strong><?php echo SUCCESS; ?>!</strong> <?php echo RIGHT_HAS_BEEN_UPDATED_SUCCESSFULLY; ?></div>

    <?php
    }
} ?>

<div id="page">

<div class="row-fluid ">

<div class="span12">

<!-- BEGIN TAB PORTLET-->

<div class="widget widget-tabs">

<div class="widget-title tit_hidd">

    <h4><i class="icon-reorder"></i><?php echo ADMINISTRATOR; ?></h4>

</div>

<div class="widget-body1">

<div class="tabbable portlet-tabs">

<ul class="nav nav-tabs">


    <li><a href="#portlet_tab2" data-toggle="tab"><?php echo LOGIN; ?></a></li>

    <li class="active"><a href="#portlet_tab1" data-toggle="tab"><?php echo ADMINISTRATOR; ?></a></li>

</ul>

<div class="tab-content">

<div class="tab-pane active" id="portlet_tab1">

    <!-- BEGIN ADVANCED TABLE widget-->

    <a href="<?php echo site_url('admin/admin/add_admin'); ?>" class="btn mini purple fr marB5"><i
            class="icon-edit"></i><?php echo ADD_ADMIN; ?></a>


    <div class="row-fluid">

        <div class="span12">

            <!-- BEGIN EXAMPLE TABLE widget-->

            <div class="widget">

                <div class="widget-title">

                    <h4><i class="icon-reorder"></i><?php echo ADMINISTRATOR_MANAGED_TABLE; ?></h4>

                                                    <span class="tools">

                                                        <a href="javascript:;" class="icon-chevron-down"></a>

                                                                                                              

                                                    </span>


                </div>


                <div class="widget-body">


                    <table class="table table-striped table-bordered admin_type" id="sample_new">

                        <thead>

                        <tr>


                            <th><?php echo USERNAME; ?></th>


                            <th><?php echo ADMIN_TYPE; ?></th>

                            <th class="hidden-phone"><?php echo EMAIL; ?></th>

                            <th class="hidden-phone"><?php echo SIGNUP_IP_ADDRESS; ?></th>

                            <th class="hidden-phone"><?php echo ACTIVE; ?></th>

                            <th class="hidden-phone"><?php echo REGISTERED_ON; ?></th>

                            <?php   $assign_rights = get_rights('assign_rights');

                            if ($assign_rights == 1) {
                                ?>

                                <th class="sorting_disabled"><?php echo RIGHTS; ?></th>

                            <?php } ?>

                            <th><?php echo ACTION; ?></th>

                        </tr>

                        </thead>

                        <tbody>

                        <?php



                        if ($admin_list) {

                            foreach ($admin_list as $row) {

                                $admin_username = $row->username;

                                $admin_password = $row->password;

                                if ($row->admin_type == 1) {

                                    $admin_type = SUPER_ADMIN;

                                } elseif ($row->admin_type == 2) {

                                    $admin_type = "Administrator";

                                }

                                $admin_email = $row->email;

                                $admin_login_ip = $row->login_ip;

                                if ($row->active == "1") {

                                    $admin_status = ACTIVE;

                                } else {

                                    $admin_status = INACTIVE;

                                }


                                $admin_date_added = date('d/m/Y', strtotime($row->date_added));

                                ?>

                                <tr class="odd gradeX">

                                    <td><?php echo $admin_username; ?></td>


                                    <td><?php echo $admin_type; ?></td>

                                    <td class="hidden-phone"><?php echo $admin_email; ?></td>

                                    <td class="hidden-phone"><a href="http://whatismyipaddress.com/ip/<?php echo $admin_login_ip; ?>" target="_blank"><?php echo $admin_login_ip; ?></a></td>

                                    <td class="hidden-phone"><?php echo $admin_status; ?></td>

                                    <td class="hidden-phone"><?php echo $admin_date_added; ?></td>

                                    <?php if ($assign_rights == 1) { ?>

                                        <td>
                                            <a href="<?php echo site_url('admin/admin/assign_rights/' . $row->admin_id); ?>"><span
                                                    class="label label-success"><?php echo RIGHT; ?></span></a></td>

                                    <?php } ?>

                                    <td>
                                        <a href="<?php echo site_url('admin/admin/edit_admin/' . $row->admin_id); ?>"><?php echo EDIT; ?> </a>

                                        <?php if ($this->session->userdata('admin_id') != $row->admin_id) { ?> / <a
                                            href="#"
                                            onclick="delete_rec('<?php echo $row->admin_id; ?>')"><?php echo DELETE; ?></a><?php } ?>
                                    </td>

                                </tr>

                            <?php
                            }


                        }



                        ?>


                        </tbody>

                    </table>


                </div>

            </div>

            <!-- END EXAMPLE TABLE widget-->

        </div>

    </div>


    <!-- END ADVANCED TABLE widget-->

</div>


<div class="tab-pane" id="portlet_tab2">

    <div class="navbar-inverse">

        <?php

        $attributes = array('name' => 'frm_listlogin', 'id' => 'frm_listlogin');


        echo form_open_multipart('admin/admin/action_login', $attributes);

        ?>



        <input type="hidden" name="action" id="action"/>

        <button type="button" class="btn btn-danger fr marB5"
                onclick="setaction('chk[]','delete', '<?php echo ARE_YOU_SURE_YOU_WANT_TO_DELETE_SELECTED_RECORD; ?>', 'frm_listlogin');">
            <!-- change by darshan -->

            <i class="icon-remove icon-white"></i> <?php echo DELETE; ?></button>


        <div class="row-fluid">

            <div class="span12">

                <!-- BEGIN EXAMPLE TABLE widget-->

                <div class="widget">

                    <div class="widget-title">

                        <h4><i class="icon-reorder"></i><?php echo LOGIN1; ?></h4>

                                                    <span class="tools">

                                                        <a href="javascript:;" class="icon-chevron-down"></a>

                                                        

                                                    </span>

                    </div>


                    <div class="widget-body">

                        <table class="table table-striped table-bordered admin_log" id="list_admin_"
                               style="width:100%!important;">

                            <thead>

                            <tr>

                                <th style="width:8px;"><input type="checkbox" class="group-checkable"
                                                              data-set="#list_admin_ .checkboxes"/></th>

                                <th><?php echo USERNAME; ?></th>

                                <th class="ph_hidden"><?php echo EMAIL; ?></th>

                                <th><?php echo ADMIN_TYPE; ?></th>

                                <th class="ph_hidden"><?php echo SIGNUP_IP_ADDRESS; ?></th>

                                <th><?php echo DATE; ?></th>

                            </tr>

                            </thead>

                            <tbody>

                            <?php

                            if ($admin_login) {


                                foreach ($admin_login as $login_row) {


                                    $login_id = $login_row->login_id;

                                    $login_username = $login_row->username;

                                    $login_email = $login_row->email;

                                    if ($login_row->admin_type == 1) {

                                        $login_type = "Super Admin";

                                    } elseif ($login_row->admin_type == 2) {

                                        $login_type = "Administrator";

                                    }

                                    $login_ip = $login_row->login_ip;

                                    $login_date = date($site_setting['date_format'], strtotime($login_row->login_date));

                                    ?>

                                    <tr class="odd gradeX">

                                        <td><input type="checkbox" class="checkboxes" value="<?php echo $login_id; ?>"
                                                   name="chk[]"/></td>

                                        <td><?php echo $login_username; ?></td>

                                        <td class="ph_hidden"><a href="#"><?php echo $login_email; ?></a></td>

                                        <td><?php echo $login_type; ?></td>

                                        <td class="ph_hidden"><?php echo $login_ip; ?></td>

                                        <td><?php echo $login_date; ?></td>

                                    </tr>

                                <?php

                                }

                            } else {

                                ?>



                                <tr class="odd gradeX">

                                    <td><?php echo NO_RECORDS_FOUND; ?>.</td>
                                </tr>



                            <?php } ?>

                            </tbody>

                        </table>

                    </div>

                </div>

                <!-- END EXAMPLE TABLE widget-->

            </div>

        </div>

        </form>

    </div>

</div>

</div>

</div>

</div>

<!-- END TAB PORTLET-->

</div>

</div>

</div>

<!-- END PAGE CONTENT-->

</div>

<!-- END PAGE CONTAINER-->

</div>

<!-- END PAGE -->

</div>

</div>

<!-- END CONTAINER -->


<!-- BEGIN JAVASCRIPTS -->

<!-- Load javascripts at bottom, this will reduce page load time -->


<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/jquery.dataTables.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/DT_bootstrap.js"></script>

<script src="<?php echo base_url(); ?>js/scripts.js"></script>


<script>

    jQuery(document).ready(function () {

        // initiate layout and plugins

        App.init();

    });

    jQuery(document).ready(function() {     
     
    
     $.extend($.fn.dataTableExt.oSort,{
            "date-uk-pre": function ( a ) {
                var ukDatea = a.split('/');
                return (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
            },

            "date-uk-asc": function ( a, b ) {
                return ((a < b) ? -1 : ((a > b) ? 1 : 0));
            },

            "date-uk-desc": function ( a, b ) {
                return ((a < b) ? 1 : ((a > b) ? -1 : 0));
            }
        } );
            var oTable = $('#sample_new').dataTable({
            "aoColumnDefs": [{'bSortable': false, 'aTargets': [7]}],
             "aoColumns": [
                null,
                null,
                null,
                null,
                null,
                { "sType": "date-uk" },
                null,
                null
            ],
            "oLanguage": {


                "sLengthMenu": "<?php echo DISPLAY; ?> _MENU_ <?php echo RECORD_PER_PAGE; ?>&nbsp;&nbsp;",
                "sZeroRecords": "<?php echo NOTHING_FOUND_SORRY; ?>",
                "sInfo": "<?php echo SHOWING; ?> _START_ to _END_ of _TOTAL_ <?php echo RECORD; ?>",
                "sInfoEmpty": "<?php echo SHOWING; ?> 0 to 0 of 0 <?php echo RECORD; ?>",
                "sSearch": "<?php echo SEARCH; ?>: ",
                'oPaginate': {

                    'sPrevious': '<?php echo PREVIOUS; ?>',
                    'sNext': '<?php echo NEXT; ?>'

                }

            }

        });
    $('#list_admin_').dataTable( { 
       
          "search": {
            "caseInsensitive": true
          }
        
        });
    }); 

   
</script>

