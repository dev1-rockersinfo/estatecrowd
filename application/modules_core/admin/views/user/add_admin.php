<!-- BEGIN CONTAINER -->
<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php echo $this->load->view('admin_sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN THEME CUSTOMIZER-->
                    <div id="theme-change" class="hidden-phone">
                        <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text"><?php echo THEME; ?></span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
                    </div>
                    <!-- END THEME CUSTOMIZER-->
                    <h3 class="page-title">
                        <?php echo ADD_ADMINISTRATOR; ?>

                    </h3>
                    <ul class="breadcrumb">
                        <li>
                            <a href="<?php echo site_url('admin/home/dashboard') ?>"><i class="icon-home"></i></a><span
                                class="divider">&nbsp;</span>
                        </li>

                        <li>
                            <a href="<?php echo site_url('admin/admin/list_admin') ?>"><?php echo ADMINISTRATOR; ?></a><span
                                class="divider-last">&nbsp;</span></li>
                    </ul>
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <?php
            if ($error != "") {
                ?>
                <div class="alert alert-error">
                    <button class="close" data-dismiss="alert">x</button>
                    <strong><?php echo ERRORS; ?>!</strong> <?php echo $error; ?></div>
            <?php
            }
            ?>
            <?php if ($error != "" && $error == 'insert') {
                ?>
                <div class="alert alert-success">
                    <button class="close" data-dismiss="alert">x</button>
                    <strong><?php echo SUCCESS; ?>!</strong> <?php echo RECORD_HAS_BEEN_UPDATED_SUCCESSFULLY; ?>.
                </div>
            <?php
            }
            ?>
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                <div class="span12 sortable">
                    <!-- BEGIN SAMPLE FORMPORTLET-->
                    <div class="widget">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i><?php echo ADD_ADMINISTRATOR; ?></h4>
                                        <span class="tools">
                                        <a href="javascript:;" class="icon-chevron-down"></a>
                                        <a href="javascript:;" class="icon-remove"></a>
                                        </span>
                        </div>
                        <div class="widget-body">
                            <!-- BEGIN FORM-->

                            <?php
                            $attributes = array('name' => 'frm_addadmin', 'class' => 'form-horizontal');
                            echo form_open('admin/admin/add_admin/' . $admin_id, $attributes);
                            ?>
                            <div class="control-group">
                                <label class="control-label">*<?php echo EMAIL; ?></label>

                                <div class="controls">
                                    <input type="text" placeholder="Email" name="email" id="email"
                                           value="<?php echo $email; ?>" class="input-xlarge"/>

                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">*<?php echo USERNAME; ?></label>

                                <div class="controls">
                                    <input type="text" placeholder="<?php echo USERNAME; ?>" name="username"
                                           id="username" value="<?php echo $username; ?>" class="input-xlarge"/>

                                </div>
                            </div>
                            <?php if ($show_pass != '') { ?>
                                <div class="control-group">
                                    <label class="control-label">*<?php echo PASSWORD; ?>  </label>

                                    <div class="controls">
                                        <input type="password" placeholder="<?php echo PASSWORD; ?>" name="password"
                                               id="password" value="<?php echo $password; ?>" class="input-xlarge"/>

                                    </div>
                                </div>
                            <?php } ?>


                            <div class="control-group">
                                <label class="control-label"><?php echo ADMIN_TYPE; ?></label>

                                <div class="controls">
                                    <select class="input-large m-wrap" name="admin_type"
                                            tabindex="1"  <?php if ($admin_type != '') {
                                        echo "disabled";
                                    } ?>>
                                        <!--  <option value="1" <?php if ($admin_type == '1') {
                                            echo "selected";
                                        } ?>><?php echo SUPER_ADMINISTRATOR; ?></option>  -->
                                        <option value="2" <?php if ($admin_type == '2') {
                                            echo "selected";
                                        } ?>><?php echo ADMINISTRATOR; ?></option>

                                    </select>
                                    <?php if ($admin_type != '') { ?>

                                        <input type="hidden" name="admin_type" value="<?php echo $admin_type; ?>">
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Status</label>

                                <div class="controls">
                                    <select name="active" class="input-large m-wrap" tabindex="1">
                                        <?php if ($this->session->userdata('admin_id') != $admin_id) { ?>
                                            <option value="0" <?php if ($active == '0') {
                                                echo "selected";
                                            } ?>><?php echo INACTIVE; ?></option>    <?php } ?>
                                        <option value="1" <?php if ($active == '1') {
                                            echo "selected";
                                        } ?>><?php echo ACTIVE; ?></option>

                                    </select>
                                </div>
                            </div>

                            <input type="hidden" name="admin_id" id="admin_id" value="<?php echo $admin_id; ?>"/>

                            <div class="form-actions">
                                <?php
                                if ($admin_id == "") {
                                    ?>
                                    <button type="submit" class="btn blue"><i class="icon-ok"></i><?php echo SUBMIT; ?>
                                    </button>
                                <?php
                                } else {
                                    ?>
                                    <button type="submit" class="btn blue"><i class="icon-ok"></i><?php echo UPDATE; ?>
                                    </button>
                                <?php
                                }
                                ?>
                                <button type="button" class="btn"
                                        onClick="location.href='<?php echo site_url('admin/admin/list_admin'); ?>'"><i
                                        class=" icon-remove"></i> <?php echo CANCEL; ?></button>
                            </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>

            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div id="footer">
    2013 &copy; Fundraising.
    <div class="span pull-right">
        <span class="go-top"><i class="icon-arrow-up"></i></span>
    </div>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS -->
<!-- Load javascripts at bottom, this will reduce page load time -->

<script type="text/javascript"
        src="<?php echo base_url(); ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        App.init();
    });
</script>
<!-- END JAVASCRIPTS -->
