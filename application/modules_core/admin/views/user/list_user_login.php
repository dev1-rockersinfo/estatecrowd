<script>
    function setaction(elename, actionval, actionmsg, formname) {
        vchkcnt = 0;
        elem = document.getElementsByName(elename);

        for (i = 0; i < elem.length; i++) {
            if (elem[i].checked) vchkcnt++;
        }
        if (vchkcnt == 0) {
            alert('<?php echo DELETE_SELECT_RECORD_CONFIRMATION;?>')
        } else {

            if (confirm(actionmsg)) {
                document.getElementById('action').value = actionval;
                document.getElementById(formname).submit();
            }

        }
    }
</script>
<!-- BEGIN CONTAINER -->
<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php echo $this->load->view('admin_sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN THEME CUSTOMIZER-->
                    <div id="theme-change" class="hidden-phone">
                        <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text">Theme:</span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
                    </div>
                    <!-- END THEME CUSTOMIZER-->
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        <?php echo USERS_LOGIN; ?>

                    </h3>
                    <ul class="breadcrumb">
                        <li>
                            <a href="<?php echo site_url('admin/home/dashboard') ?>"><i class="icon-home"></i></a><span
                                class="divider">&nbsp;</span>
                        </li>

                        <li>
                            <a href="<?php echo site_url('admin/user/user_login') ?>"><?php echo USERS_LOGIN; ?></a><span
                                class="divider-last">&nbsp;</span></li>
                    </ul>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->

            <?php if ($msg != '') {

                if ($msg == 'delete') {
                    ?>
                    <div class="alert alert-success">
                        <button class="close" data-dismiss="alert">x</button>
                        <strong><?php echo SUCCESS; ?>
                            !</strong> <?php echo LOGIN_DETAIL_HAS_BEEN_DELETED_SUCCESSFULLY; ?>.
                    </div>

                <?php
                }
            } ?>

            <!-- BEGIN ADVANCED TABLE widget-->
            <button type="button" class="btn btn-danger fr marB5"
                    onclick="setaction('chk[]','delete', '<?php echo ARE_YOU_SURE_YOU_WANT_TO_DELETE_SELECTED_RECORD; ?>', 'frm_listlogin')">
                <i class="icon-remove icon-white"></i> <?php echo DELETE; ?></button>
            <!-- change by darshan -->

            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN EXAMPLE TABLE widget-->
                    <div class="widget">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i><?php echo USERS_LOGIN; ?></h4>
                            <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                            </span>
                        </div>
                        <div class="widget-body">
                            <?php
                            $attributes = array('name' => 'frm_listlogin', 'id' => 'frm_listlogin');

                            echo form_open_multipart('admin/user/action_login', $attributes);
                            ?>
                            <input type="hidden" name="action" id="action"/>

                            <table class="table table-striped table-bordered" id="sample_1">
                                <thead>
                                <tr>
                                    <th style="width:8px;"><input type="checkbox" class="group-checkable"
                                                                  data-set="#sample_1 .checkboxes"/></th>
                                    <th class="hidden-phone"><?php echo USERNAME; ?></th>
                                    <th><?php echo EMAIL; ?></th>
                                    <th class="hidden-phone tab_hidden"><?php echo LOGIN_IP_ADDRESS; ?></th>
                                    <th class="hidden-phone tab_hidden"><?php echo ADDRESS; ?></th>
                                    <!-- <th class="hidden-phone tab_hidden">City</th>
                                    <th class="hidden-phone tab_hidden">State</th>
                                    <th class="hidden-phone tab_hidden">Country</th> -->
                                    <th class="hidden-phone tab_hidden"><?php echo ZIPCODE; ?></th>
                                    <th class="hidden-phone"><?php echo LOGIN_DATE; ?></th>
                                    <th class="hidden-phone"><?php echo LOGIN_TIME; ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if ($result) {
                                    foreach ($result as $row) {

                                        $temp = explode(" ", $row->login_date_time);
                                        $user_login_id = $row->login_id;
                                        $user_username = $row->user_name;
                                        $user_date = date($site_setting['date_format'], strtotime($temp[0]));
                                        $user_login_ip = $row->login_ip;
                                        $user_address = $row->address;
                                        $user_zipcode = $row->zip_code;
                                        $user_email = $row->email;
                                        $user_time = $temp[1];
                                        ?>
                                        <tr class="odd gradeX">
                                            <td><input type="checkbox" class="checkboxes" name="chk[]"
                                                       value="<?php echo $user_login_id; ?>"/></td>
                                            <td class="hidden-phone"><?php echo $user_username; ?></td>
                                            <td><?php echo $user_email; ?></td>
                                            <td class="hidden-phone tab_hidden"><a href="http://whatismyipaddress.com/ip/<?php echo $user_login_ip; ?>" target="_blank"><?php echo $user_login_ip; ?></a></td>
                                            <td class="hidden-phone tab_hidden"><?php echo $user_address; ?></td>
                                            <!-- <td class="hidden-phone tab_hidden">Baroda</td>
                                            <td class="hidden-phone tab_hidden">Gera</td>
                                            <td class="hidden-phone tab_hidden">Germany</td> -->
                                            <td class="hidden-phone tab_hidden"><?php echo $user_zipcode; ?></td>
                                            <td class="hidden-phone"><?php echo $user_date; ?></td>
                                            <td class="hidden-phone"><?php echo $user_time; ?></td>
                                        </tr>
                                    <?php
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE widget-->
                </div>
            </div>

            <!-- END ADVANCED TABLE widget-->

            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>
<!-- END CONTAINER -->

<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/DT_bootstrap.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        App.init();
    });

    jQuery(document).ready(function () {


        $('#sample_1').dataTable({
            "bDestroy": true,
            "aoColumnDefs": [{"bSortable": false, "aTargets": [0]}],
            "oLanguage": {
                "sLengthMenu": " _MENU_ <?php echo RECORD_PER_PAGE; ?>",
                "sZeroRecords": "<?php echo NOTHING_FOUND_SORRY; ?>",
                "sInfo": "<?php echo SHOWING; ?> _START_ to _END_ of _TOTAL_ <?php echo RECORD; ?>",
                "sInfoEmpty": "<?php echo SHOWING; ?> 0 to 0 of 0 <?php echo RECORD; ?>",
                "sSearch": "<?php echo SEARCH; ?>: ",
                'oPaginate': {

                    'sPrevious': '<?php echo PREVIOUS; ?>',
                    'sNext': '<?php echo NEXT; ?>'
                }
            }

        });


    });

</script>
