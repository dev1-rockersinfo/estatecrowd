<script>

    function setaction(elename, actionval, actionmsg, formname) {

        vchkcnt = 0;

        elem = document.getElementsByName(elename);

        for (i = 0; i < elem.length; i++) {

            if (elem[i].checked) vchkcnt++;

        }

        if (vchkcnt == 0) {

            alert("<?php echo DELETE_SELECT_RECORD_CONFIRMATION;?>")

        } else {

            if (confirm(actionmsg)) {

                document.getElementById('action').value = actionval;

                document.getElementById(formname).submit();

            }

        }

    }

</script>

<!-- BEGIN CONTAINER -->

<div id="container" class="row-fluid">

<!-- BEGIN SIDEBAR -->

<?php echo $this->load->view('admin_sidebar'); ?>

<!-- END SIDEBAR -->

<!-- BEGIN PAGE -->

<div id="main-content">

<!-- BEGIN PAGE CONTAINER-->

<div class="container-fluid">

<!-- BEGIN PAGE HEADER-->

<div class="row-fluid">

    <div class="span12">

        <!-- BEGIN THEME CUSTOMIZER-->

        <div id="theme-change" class="hidden-phone">

            <i class="icon-cogs"></i>

                        <span class="settings">

                            <span class="text">Theme:</span>

                            <span class="colors">

                                <span class="color-default" data-style="default"></span>

                                <span class="color-gray" data-style="gray"></span>

                                <span class="color-purple" data-style="purple"></span>

                                <span class="color-navy-blue" data-style="navy-blue"></span>

                            </span>

                        </span>

        </div>

        <!-- END THEME CUSTOMIZER-->

        <!-- BEGIN PAGE TITLE & BREADCRUMB-->

        <h3 class="page-title">

            <?php echo SPAMMER; ?>

        </h3>

        <ul class="breadcrumb">

            <li>

                <a href="<?php echo site_url('admin/home/dashboard') ?>"><i class="icon-home"></i></a><span
                    class="divider">&nbsp;</span>

            </li>


            <li><a href="#"><?php echo SPAMMER; ?></a><span class="divider-last">&nbsp;</span></li>

        </ul>

        <!-- END PAGE TITLE & BREADCRUMB-->

    </div>

</div>

<!-- END PAGE HEADER-->



<?php if ($msg != '') {


    if ($msg == 'delete') {
        ?>
        <div class="alert alert-success">

            <button class="close" data-dismiss="alert">x</button>

            <strong><?php echo SUCCESS; ?>!</strong> <?php echo RECORDS_HAS_BEEN_DELETED_SUCCESSFULLY; ?>.
        </div>



    <?php
    }
    if ($msg == 'make_spam_permenant') {
        ?>
        <div class="alert alert-success">

            <button class="close" data-dismiss="alert">x</button>

            <strong><?php echo SUCCESS; ?>!</strong> <?php echo RECORDS_HAS_BEEN_MAKE_PERMENANT_SUCCESSFULLY; ?>.
        </div>



    <?php
    }
    if ($msg == 'success') {
        ?>
        <div class="alert alert-success">

            <button class="close" data-dismiss="alert">x</button>

            <strong><?php echo SUCCESS; ?>!</strong> <?php echo NEW_RECORD_HAS_BEEN_ADDED_SUCCESSFULLY; ?>.
        </div>



    <?php
    }
} ?>



<!-- BEGIN ADVANCED TABLE widget-->

<div class="fr iconM">


    <button type="button" class="btn mini purple"
            onclick="location.href='<?php echo site_url('admin/spam/add_spammer'); ?>'"><i
            class="icon-plus"></i> <?php echo ADD; ?></button>


    <button type="button" class="btn mini purple" onclick="location.href='<?php echo site_url('admin/spam/spamer') ?>'">
        <i class="icon-refresh"></i> <?php echo REFRES; ?></button>

    <button type="button" class="btn btn-danger"
            onclick="setaction('chk[]','delete', '<?php echo ARE_YOU_SURE_YOU_WANT_TO_DELETE_SELECTED_PROJECT; ?>', 'frm_listspamer')">

        <i class="icon-remove-sign"></i> <?php echo DELETE; ?></button>
    <button type="button" class="btn btn-warning"
            onclick="setaction('chk[]','make_spam_permenant', '<?php echo ARE_YOU_SURE_YOU_WANT_TO_MAKE_PERMENANT_SPAMER_SELECTED_REPORT; ?>', 'frm_listspamer')">
        <!-- chnage by darshan -->

        <i class="icon-ban-circle"></i> <?php echo MAKE_SPAMER; ?></button>


</div>

<div class="row-fluid">

    <div class="span12">

        <!-- BEGIN EXAMPLE TABLE widget-->

        <div class="widget">

            <div class="widget-title">

                <h4><i class="icon-reorder"></i> <?php echo SPAMMER; ?></h4>

                            <span class="tools">

                                <a href="javascript:;" class="icon-chevron-down"></a>

                            </span>

            </div>



            <?php

            $attributes = array('name' => 'frm_listspamer', 'id' => 'frm_listspamer');


            echo form_open_multipart('admin/spam/spamer_action', $attributes);

            ?>

            <input type="hidden" name="action" id="action"/>


            <div class="widget-body">

                <table class="table table-striped table-bordered table_project" id="sample_1">

                    <thead>

                    <tr>

                        <th style="width:8px;"><input type="checkbox" class="group-checkable"
                                                      data-set="#sample_1 .checkboxes"/></th>

                        <th><?php echo SPAM_IP; ?></th>

                        <th class="hidden-phone tab_hidden"><?php echo START_DATE; ?></th>

                        <th class="hidden-phone"><?php echo END_DATE; ?></th>

                        <th><?php echo PERMENANT; ?></th>


                    </tr>

                    </thead>

                    <tbody>

                    <?php

                    if ($result) {

                        foreach ($result as $row) {

                            $spam_ip = $row->spam_ip;

                            $spam_start_date = date($site_setting['date_format'], strtotime($row->start_date));

                            $spam_end_date = date($site_setting['date_format'], strtotime($row->end_date));

                            $spam_permenant = 'No';

                            if ($row->permenant_spam == '1' || $row->permenant_spam == 1) {

                                $spam_permenant = 'Yes';

                            }

                            ?>

                            <tr class="odd gradeX">

                                <td><input type="checkbox" class="checkboxes" name="chk[]" id="chk"
                                           value="<?php echo $row->spam_ip; ?>"/></td>

                                <td><a href="http://whatismyipaddress.com/ip/<?php echo $spam_ip; ?>" target="_blank"><?php echo $spam_ip; ?></a></td>

                                <td class="hidden-phone tab_hidden"><?php echo $spam_start_date; ?></td>

                                <td class="hidden-phone"><?php echo $spam_end_date; ?></td>

                                <td class="center "><?php echo $spam_permenant; ?></td>

                            </tr>

                        <?php

                        }

                    }

                    ?>


                    </tbody>

                </table>

            </div>

            </form>

        </div>

        <!-- END EXAMPLE TABLE widget-->

    </div>

</div>


<!-- END ADVANCED TABLE widget-->


<!-- END PAGE CONTENT-->

</div>

<!-- END PAGE CONTAINER-->

</div>

<!-- END PAGE -->

</div>

<!-- END CONTAINER -->


<!-- BEGIN JAVASCRIPTS -->

<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/jquery.dataTables.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/DT_bootstrap.js"></script>

<script src="<?php echo base_url(); ?>js/scripts.js"></script>

<script>

    jQuery(document).ready(function () {

        // initiate layout and plugins

        App.init();

    });
    jQuery(document).ready(function () {


        $('#sample_1').dataTable({
            "bDestroy": true,
            "aoColumnDefs": [{"bSortable": false, "aTargets": [0]}],
            "oLanguage": {
                "sLengthMenu": " _MENU_ <?php echo RECORD_PER_PAGE; ?>",
                "sZeroRecords": "<?php echo NOTHING_FOUND_SORRY; ?>",
                "sInfo": "<?php echo SHOWING; ?> _START_ to _END_ of _TOTAL_ <?php echo RECORD; ?>",
                "sInfoEmpty": "<?php echo SHOWING; ?> 0 to 0 of 0 <?php echo RECORD; ?>",
                "sSearch": "<?php echo SEARCH; ?>: ",
                'oPaginate': {

                    'sPrevious': '<?php echo PREVIOUS; ?>',
                    'sNext': '<?php echo NEXT; ?>'
                }
            }

        });


    });
</script>

