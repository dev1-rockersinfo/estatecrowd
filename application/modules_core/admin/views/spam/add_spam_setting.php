<!-- BEGIN CONTAINER -->

<div id="container" class="row-fluid">

<!-- BEGIN SIDEBAR -->

<?php echo $this->load->view('admin_sidebar'); ?>

<!-- END SIDEBAR -->

<!-- BEGIN PAGE -->

<div id="main-content">

<!-- BEGIN PAGE CONTAINER-->

<div class="container-fluid">

<!-- BEGIN PAGE HEADER-->

<div class="row-fluid">

    <div class="span12">

        <!-- BEGIN THEME CUSTOMIZER-->

        <div id="theme-change" class="hidden-phone">

            <i class="icon-cogs"></i>

                        <span class="settings">

                            <span class="text"><?php echo THEME; ?></span>

                            <span class="colors">

                                <span class="color-default" data-style="default"></span>

                                <span class="color-gray" data-style="gray"></span>

                                <span class="color-purple" data-style="purple"></span>

                                <span class="color-navy-blue" data-style="navy-blue"></span>

                            </span>

                        </span>

        </div>

        <!-- END THEME CUSTOMIZER-->

        <h3 class="page-title">

            <?php echo SPAM_SETTING; ?>

        </h3>

        <ul class="breadcrumb">

            <li>

                <a href="<?php echo site_url('admin/home/dashboard'); ?>"><i class="icon-home"></i></a><span
                    class="divider">&nbsp;</span>

            </li>


            <li><a href="#"><?php echo SPAM_SETTING; ?></a><span class="divider-last">&nbsp;</span></li>

        </ul>

    </div>

</div>

<!-- END PAGE HEADER-->

<?php

if ($error != "") {
    ?>

    <div class="alert alert-error">

        <button class="close" data-dismiss="alert">x</button>

        <strong><?php echo ERRORS; ?>!</strong> <?php echo $error; ?></div>

<?php
}

if ($success != '') {

    ?>

    <div class="alert alert-success">

        <button class="close" data-dismiss="alert">x</button>

        <strong><?php echo SUCCESS; ?>!</strong> <?php echo $success; ?></div>

<?php } ?>

<!-- BEGIN PAGE CONTENT-->

<div class="row-fluid">

    <div class="span12 sortable">

        <!-- BEGIN SAMPLE FORMPORTLET-->

        <div class="widget">

            <div class="widget-title">

                <h4><i class="icon-reorder"></i><?php echo SPAM_SETTING; ?></h4>

                                        <span class="tools">

                                        <a href="javascript:;" class="icon-chevron-down"></a>

                                        <a href="javascript:;" class="icon-remove"></a>

                                        </span>

            </div>

            <div class="widget-body">

                <!-- BEGIN FORM-->

                <?php

                $attributes = array('name' => 'frm_spam_setting', 'class' => 'form-horizontal');

                echo form_open('admin/spam/add_spam_setting', $attributes);

                ?>

                <div class="control-group">

                    <label class="control-label"><?php echo TOTAL_SPAM_REPORT_ALLOW; ?></label>

                    <div class="controls">

                        <input type="text" placeholder="50" name="spam_report_total" id="spam_report_total"
                               value="<?php echo $spam_report_total; ?>" class="input-xlarge"/>

                    </div>

                </div>


                <div class="control-group">

                    <label class="control-label"><?php echo REPORT_SPAMER_EXPIRE; ?> </label>

                    <div class="controls">

                        <input type="text" name="spam_report_expire" id="spam_report_expire"
                               value="<?php echo $spam_report_expire; ?>" placeholder="70" class="input-xlarge"/>

                    </div>

                </div>


                <div class="control-group">

                    <label class="control-label"><?php echo TOTAL_REGISTRATION_ALLOW_FROM_SAME_IP; ?> </label>

                    <div class="controls">

                        <input type="text" name="total_register" id="total_register"
                               value="<?php echo $total_register; ?>" placeholder="100" class="input-xlarge"/>

                    </div>

                </div>


                <div class="control-group">

                    <label class="control-label"><?php echo REGISTRATION_SPMAER_EXPIRE; ?></label>

                    <div class="controls">

                        <input type="text" name="register_expire" id="register_expire"
                               value="<?php echo $register_expire; ?>" placeholder="70" class="input-xlarge"/>

                    </div>

                </div>


                <div class="control-group">

                    <label class="control-label"><?php echo TOTAL_COMMENT_ALLOW_FROM_SAME_IP_IN_ONE_WAY; ?> </label>

                    <div class="controls">

                        <input type="text" name="total_comment" id="total_comment" value="<?php echo $total_comment; ?>"
                               placeholder="100" class="input-xlarge"/>

                    </div>

                </div>


                <div class="control-group">

                    <label class="control-label"><?php echo COMMENT_SPAMER_EXPIRE; ?> </label>

                    <div class="controls">

                        <input type="text" name="comment_expire" id="comment_expire"
                               value="<?php echo $comment_expire; ?>" placeholder="100" class="input-xlarge"/>

                    </div>

                </div>


                <div class="control-group">

                    <label class="control-label"><?php echo TOTAL_INQUIRY_ALLOW_SAME_IP_IN_ONE_WAY; ?> </label>

                    <div class="controls">

                        <input type="text" name="total_contact" id="total_contact" value="<?php echo $total_contact; ?>"
                               placeholder="100" class="input-xlarge"/>

                    </div>

                </div>


                <div class="control-group">

                    <label class="control-label"><?php echo INQUIRY_SPAMER_EXPIRE; ?> </label>

                    <div class="controls">

                        <input type="text" name="contact_expire" id="contact_expire"
                               value="<?php echo $contact_expire; ?>" placeholder="100" class="input-xlarge"/>

                    </div>

                </div>


                <input type="hidden" name="spam_control_id" id="spam_control_id"
                       value="<?php echo $spam_control_id; ?>"/>


                <div class="form-actions">

                    <button type="submit" class="btn blue"><i class="icon-ok"></i> <?php echo UPDATE; ?></button>

                </div>

                </form>

                <!-- END FORM-->

            </div>

        </div>

        <!-- END SAMPLE FORM PORTLET-->

    </div>

</div>


<!-- END PAGE CONTENT-->

</div>

<!-- END PAGE CONTAINER-->

</div>

<!-- END PAGE -->

</div>

<!-- END CONTAINER -->


<!-- BEGIN JAVASCRIPTS -->


<script type="text/javascript"
        src="<?php echo base_url(); ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>

<script src="<?php echo base_url(); ?>js/scripts.js"></script>

<script>

    jQuery(document).ready(function () {

        // initiate layout and plugins

        App.init();

    });

</script>

<!-- END JAVASCRIPTS -->

