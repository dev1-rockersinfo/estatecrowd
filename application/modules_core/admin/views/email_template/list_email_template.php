<!-- BEGIN CONTAINER -->
<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php echo $this->load->view('admin_sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN THEME CUSTOMIZER-->
                    <div id="theme-change" class="hidden-phone">
                        <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text"><?php echo THEME; ?></span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
                    </div>
                    <!-- END THEME CUSTOMIZER-->
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        <?php echo EMAIL_TEMPLATE; ?>

                    </h3>
                    <ul class="breadcrumb">
                        <li>
                            <a href="<?php echo site_url('admin/home/dashboard'); ?>"><i class="icon-home"></i></a><span
                                class="divider">&nbsp;</span>
                        </li>

                        <li>
                            <a href="<?php echo site_url('admin/email_template/list_email_template'); ?>"><?php echo EMAIL_TEMPLATE; ?></a><span
                                class="divider-last">&nbsp;</span></li>
                    </ul>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <?php if ($msg != "") {
                if ($msg == 'update') {
                    ?>
                    <div class="alert alert-success">
                        <button class="close" data-dismiss="alert">x</button>
                        <strong><?php echo SUCCESS; ?>!</strong> <?php echo RECORDS_HAS_BEEN_UPDATED_SUCCESSFULLY; ?>.
                    </div>

                <?php
                }
            } ?>

            <a href="<?php echo site_url('admin/email_template/update_email_address'); ?>"
               class="btn mini purple fr marB5 btn-inverse"><i
                    class="icon-star"></i> <?php echo MASTER_EMAIL_TEMPLATE; ?></a>


            <!-- BEGIN ADVANCED TABLE widget-->


            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN EXAMPLE TABLE widget-->
                    <div class="widget">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i> <?php echo EMAIL_TEMPLATE; ?></h4>
                            <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                            </span>
                        </div>
                        <div class="widget-body">
                            <table class="table table-striped table-bordered trans" id="sample_1">
                                <thead>
                                <tr>

                                    <th class="hidden-phone tab_hidden"><?php echo NO; ?></th>
                                    <th><?php echo EMAIL_FOR_TASK; ?></th>
                                    <th><?php echo SUBJECT; ?></th>
                                    <th class="hidden-phone"><?php echo MESSAGE; ?></th>
                                    <th><?php echo FROM_ADDRESS; ?></th>
                                    <th><?php echo REPLY_ADDRESS; ?></th>
                                    <th class="hidden-phone"><?php echo ACTION; ?></th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if ($result) {
                                    $i = 1;
                                    foreach ($result as $row) {
                                        $email_task = $row->task;
                                        $email_from_addr = $row->from_address;
                                        $email_reply_addr = $row->reply_address;
                                        $email_temp_id = $row->email_template_id;
                                        $email_subject = $row->subject;
                                        $email_msg = $row->message;

                                        ?>
                                        <tr class="odd gradeX">

                                            <td class="hidden-phone tab_hidden"><?php echo $i; ?></td>
                                            <td><?php echo $email_task; ?></td>
                                            <td><?php echo $email_subject; ?></td>
                                            <td class="hidden-phone"><?php echo $email_msg; ?></td>
                                            <td><?php echo $email_from_addr; ?></td>
                                            <td><?php echo $email_reply_addr; ?></td>
                                            <td class="hidden-phone"><a
                                                    href="<?php echo site_url('admin/email_template/add_email_template/' . $email_temp_id); ?>"><?php echo EDIT; ?></a>
                                            </td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                }
                                ?>


                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE widget-->
                </div>
            </div>

            <!-- END ADVANCED TABLE widget-->

            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>
<!-- END CONTAINER -->

<!-- BEGIN JAVASCRIPTS -->

<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/DT_bootstrap.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        App.init();
    });

    jQuery(document).ready(function () {


        $('#sample_1').dataTable({
            "bDestroy": true,
            "aoColumnDefs": [{"bSortable": false, "aTargets": [0]}],
            "oLanguage": {
                "sLengthMenu": " _MENU_ <?php echo RECORD_PER_PAGE; ?>",
                "sZeroRecords": "<?php echo NOTHING_FOUND_SORRY; ?>",
                "sInfo": "<?php echo SHOWING; ?> _START_ to _END_ of _TOTAL_ <?php echo RECORD; ?>",
                "sInfoEmpty": "<?php echo SHOWING; ?> 0 to 0 of 0 <?php echo RECORD; ?>",
                "sSearch": "<?php echo SEARCH; ?>: ",
                'oPaginate': {

                    'sPrevious': '<?php echo PREVIOUS; ?>',
                    'sNext': '<?php echo NEXT; ?>'
                }
            }

        });


    });
</script>
