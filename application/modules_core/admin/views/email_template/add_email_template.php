<script type="text/javascript">
    function getlimit(id) {
        if (id == '0') {
            return false;
        }

        else {
            window.location.href = '<?php echo site_url('admin/email_template/add_email_template/');?>' + '/' + id;
        }
    }
</script>
<!-- BEGIN CONTAINER -->
<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php echo $this->load->view('admin_sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN THEME CUSTOMIZER-->
                    <div id="theme-change" class="hidden-phone">
                        <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text"><?php echo THEME; ?></span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
                    </div>
                    <!-- END THEME CUSTOMIZER-->
                    <h3 class="page-title">
                        <?php echo EMAIL_TEMPLATE; ?>
                    </h3>
                    <ul class="breadcrumb">
                        <li>
                            <a href="<?php echo site_url('admin/home/dashboard') ?>"><i class="icon-home"></i></a><span
                                class="divider">&nbsp;</span>
                        </li>

                        <li><a href="#"><?php echo EMAIL_TEMPLATE; ?></a><span class="divider-last">&nbsp;</span></li>
                    </ul>
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <?php if ($error != "") {
                ?>
                <div class="alert alert-error">
                    <button class="close" data-dismiss="alert">x</button>
                    <strong><?php echo ERRORS; ?>!</strong> <?php echo $error ?></div>
            <?php
            }
            ?>


            <!-- BEGIN PAGE CONTENT-->
            <div class="controls">
                <select class="input-large m-wrap" tabindex="1" onchange="getlimit(this.value)">
                    <option value=0><?php echo SELECT_EMAIL_TEMPLATE; ?></option>
                    <?php
                    if ($template) {

                        foreach ($template as $t) {
                            $template_name = $t->task;
                            $template_id = $t->email_template_id;
                            //if($email_template_id == $t['email_template_id'])
                            //{
                            ?>
                            <option value="<?php echo $template_id; ?>"
                                <?php if ($email_template_id == $t->email_template_id) echo "selected"; ?>>
                                <?php echo $template_name; ?></option>
                        <?php
                        }
                    }
                    ?>
                </select>
            </div>

            <div class="row-fluid">
                <div class="span12 sortable">
                    <!-- BEGIN SAMPLE FORMPORTLET-->
                    <div class="widget">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i><?php echo EMAIL_TEMPLATE; ?></h4>
                                        <span class="tools">
                                        <a href="javascript:;" class="icon-chevron-down"></a>
                                        <a href="javascript:;" class="icon-remove"></a>
                                        </span>
                        </div>


                        <div class="widget-body">
                            <!-- BEGIN FORM-->
                            <?php
                            $attributes = array('name' => 'frm_email_template', 'class' => 'form-horizontal');
                            echo form_open('admin/email_template/add_email_template', $attributes);
                            ?>
                            <div class="control-group">
                                <label class="control-label"><?php echo FROM_ADDRESS; ?> </label>

                                <div class="controls">
                                    <input type="text" placeholder="abc@example.com" name="from_address"
                                           id="from_address" value="<?php echo $from_address; ?>" class="input-xlarge"/>
                                </div>
                            </div>


                            <div class="control-group">
                                <label class="control-label"><?php echo REPLY_ADDRESS; ?></label>

                                <div class="controls">
                                    <input name="reply_address" id="reply_address" value="<?php echo $reply_address; ?>"
                                           type="text" placeholder="abc@example.com" class="input-xlarge"/>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label"><?php echo SUBJECT; ?> </label>

                                <div class="controls">
                                    <input type="text" name="subject" id="subject" value="<?php echo $subject; ?>"
                                           placeholder="Sign up successfully on Fundraisingscript"
                                           class="input-xlarge"/>
                                </div>
                            </div>


                            <div class="control-group">
                                <label class="control-label"><?php echo MESSAGE; ?> </label>

                                <div class="controls">
                                    <textarea class="span12" name="message" id="message" rows="6">
                                        <?php echo $message; ?>
                                    </textarea>
                                </div>
                            </div>
                            <input type="hidden" name="email_template_id" id="email_template_id"
                                   value="<?php echo $email_template_id; ?>"/>


                            <div class="form-actions">
                                <button type="submit" class="btn blue"><i class="icon-ok"></i> <?php echo UPDATE; ?>
                                </button>
                                <button type="button" class="btn" name="cancel" value="Cancel"
                                        onClick="location.href='<?php echo site_url('admin/email_template/list_email_template'); ?>'"/>
                                <i class=" icon-remove"></i> <?php echo CANCEL; ?></button>
                            </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>

            <div class="hidden-phone">
            <pre>
            Email Tag
            (copy paste the tags with braces into the message part)
            Welcome Email			:	{user_name}, {email}
            New User Join			:	{user_name}, {email}, {password}, {login_link}
            Forgot Password			:	{user_name}, {email}, {password}, {login_link}
            Admin User Active		:	{user_name}, {email}, {password}
            Admin User Inactive		:	{user_name}, {email}
            Admin User Delete		:	{user_name}
            Contact Us				:	{name}, {email}, {message}
            New Project Successful Alert	:	{user_name}, {email}, {project_name}, {project_page_link}
            Admin Project Activate Alert	:	{user_name}, {email}, {project_name}, {project_page_link}
            Admin Project Inactivate Alert	:	{user_name}, {email}, {project_name}, {project_page_link}
            Admin Project Delete Alert		:	{user_name}, {email}, {project_name}
            New Comment Admin Alert			:	{user_name}, {email}, {project_name}, {project_page_link}, {comment}, {comment_user_name}, {comment_user_profile_link}
            New Comment Owner Alert			:	{user_name}, {email}, {project_name}, {project_page_link}, {comment}, {comment_user_name}, {comment_user_profile_link}
            New Comment Poster Alert		:	{user_name}, {email}, {project_name}, {project_page_link}, {comment}, {comment_user_name}, {comment_user_profile_link}
            New Fund Admin Notification		:	{user_name}, {project_name}, {project_page_link}, {donor_name}, {donor_profile_link}, {donote_amount}
            New Fund Owner Notification		:	{user_name}, {project_name}, {project_page_link}, {donor_name}, {donor_profile_link}, {donote_amount}
            New Fund Donor Notification		:	{user_name}, {project_name}, {project_page_link}, {donor_name}, {donor_profile_link}, {donote_amount}
            Other HTML Tags					:	{break}
            
            </pre>

            </div>
            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>
<!-- END CONTAINER -->

<!-- BEGIN JAVASCRIPTS -->
<script type="text/javascript" src="<?php echo base_url(); ?>ckeditor/ckeditor.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap-fileupload.js"></script>


<!-- Load javascripts at bottom, this will reduce page load time -->

<script type="text/javascript"
        src="<?php echo base_url(); ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        App.init();
    });
</script>
<!-- END JAVASCRIPTS -->
<script>
    CKEDITOR.replace('message', {
        filebrowserBrowseUrl: '<?php echo base_url();?>ckeditor/filemanager/browser/default/browser.html?Connector=<?php echo base_url(); ?>ckeditor/filemanager/connectors/php/connector.php',
        filebrowserImageBrowseUrl: '<?php echo base_url(); ?>ckeditor/filemanager/browser/default/browser.html?Type=Image&Connector=<?php echo base_url(); ?>ckeditor/filemanager/connectors/php/connector.php',
        filebrowserFlashBrowseUrl: '<?php echo base_url(); ?>ckeditor/filemanager/browser/default/browser.html?Type=Flash&Connector=<?php echo base_url(); ?>ckeditor/filemanager/connectors/php/connector.php',
        filebrowserUploadUrl: '<?php echo base_url(); ?>ckeditor/filemanager/connectors/php/upload.php?Type=File',
        filebrowserImageUploadUrl: '<?php echo base_url(); ?>ckeditor/filemanager/connectors/php/upload.php?Type=Image',
        filebrowserFlashUploadUrl: '<?php echo base_url();?>ckeditor/filemanager/connectors/php/upload.php?Type=Flash'
    });


</script>
