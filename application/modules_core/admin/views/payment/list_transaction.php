<!-- Include Date Range Picker -->

<script type="text/javascript" src="<?php echo base_url('js/moment.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/daterangepicker.js');?>"></script>
 <link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url('css/daterangepicker.css');?>" />
 <!-- Include Required Prerequisites -->
<script type="text/javascript" src="<?php echo base_url('js/moment.min.js');?>"></script>
<!-- BEGIN CONTAINER -->
<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php echo $this->load->view('admin_sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN THEME CUSTOMIZER-->
                    <div id="theme-change" class="hidden-phone">
                        <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text"><?php echo THEME; ?></span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
                    </div>
                    <!-- END THEME CUSTOMIZER-->
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        <?php echo TRANSACTIONS; ?>
                    </h3>
                    <ul class="breadcrumb">
                        <li>
                            <a href="<?php echo site_url('admin/home/dashboard'); ?>"><i class="icon-home"></i></a><span
                                class="divider">&nbsp;</span>
                        </li>

                        <li>
                            <a href="<?php echo site_url('admin/transaction_type/list_transaction'); ?>"><?php echo TRANSACTIONS; ?></a><span
                                class="divider-last">&nbsp;</span></li>
                    </ul>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->


            <!-- BEGIN ADVANCED TABLE widget-->


            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN EXAMPLE TABLE widget-->
                    <div class="widget">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i><?php echo TRANSACTIONS; ?></h4>
                            <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                            </span>
                        </div>
                        <div class="widget-body">

                        <div class="search-section">
                            <form action="" method="get">
                                <div class="filter-area">
                                    <div class="filter-main">
                                        <div class="searchProject-area">
                                              <input type="text" name="equity_name" placeholder="<?php echo SEARCH_BY_PROJECT;?>" value="<?php echo $equity_name; ?>">
                                        </div>
                                        <div class="searchProject-area">
                                              <input type="text" name="user_name" placeholder="<?php echo BY.' '.INVESTOR;?>" value="<?php echo $user_name_get; ?>">
                                        </div>
                                       <div class="searchProject-area">
                                              <input type="number" name="min_amount" min='0' placeholder="<?php echo MIN.' '.AMOUNT;?>" value="<?php echo $min_amount; ?>">
                                        </div>
                                        <div class="searchProject-area">
                                              <input type="number" name="max_amount" min='0' placeholder="<?php echo MAX.' '.AMOUNT;?>" value="<?php echo $max_amount; ?>">
                                        </div>
                                       <div class="searchProject-area">
                                              <input type="text" name="trans_id" placeholder="<?php echo BY.' '.TRANSACTION_ID;?>" value="<?php echo $trans_id; ?>">
                                        </div>

                                        <input type="hidden" name="from_date" id="from_date"  value="<?=$from_date?>">
                                        <input type="hidden" name="to_date" id="to_date"  value="<?=$to_date?>">

                                    <div class="searchProject-area">             
                                        <div id="reportrange" class="date-range-style pull-right">
                                            <i class="icon-calendar"></i>&nbsp;
                                            <span></span> <b class="caret"></b>
                                        </div>
                                        
                                     </div>             
                                        <div class="filter-item">
                                            <input type="submit" class="filter-btn" value="<?php echo SEARCH; ?>">
                                        </div>
                                        <div class="filter-item marl0t5">
                                            <a  class="btn mini purple"  href="<?php echo site_url('admin/transaction_type/list_transaction_download?equity_name='.$equity_name.'&user_name='.$user_name_get.'&min_amount='.$min_amount.'&max_amount='.$max_amount.'&trans_id='.$trans_id.'&from_date='.$from_date.'&to_date='.$to_date);?>" target="_blank"><i class="icon-download-alt"></i> <?php echo 'Export'; ?></a>
                                        </div>
                                    </div>
                                </div>
                            </form>

                            <?php  if ($equity_name != '' || $user_name_get != '' || $min_amount != '' || $min_amount != '' || $trans_id != ''||$from_date!='' || $to_date!='') {


                                ?>

                                <div class="pill-container">
                                    <div id="filterPill">
                                        <?php if ($equity_name) { ?>
                                            <a class="filter-pill" id="equity_name_tag"><?php echo $equity_name; ?> <span
                                                    class="icon-remove"></span></a>
                                        <?php } ?>

                                        <?php if ($user_name_get) { ?>
                                            <a class="filter-pill" id="user_name_tag"><?php echo $user_name_get; ?>
                                                <span class="icon-remove"></span></a>
                                        <?php } ?>

                                        <?php if ($min_amount) { ?>
                                            <a class="filter-pill" id="min_amount_tag"><?php echo $min_amount; ?> <span
                                                    class="icon-remove"></span></a>
                                        <?php } ?>

                                        <?php if ($max_amount) { ?>
                                            <a class="filter-pill" id="max_amount_tag"><?php echo $max_amount; ?> <span
                                                    class="icon-remove"></span></a>
                                        <?php } ?>

                                        <?php if ($trans_id) { ?>
                                            <a class="filter-pill" id="trans_id_tag"><?php echo $trans_id; ?> <span
                                                    class="icon-remove"></span></a>
                                        <?php } ?>
                                         <?php if ($from_date) { ?>
                                            <a class="filter-pill" id="from_date_tag"><?php echo $from_date.' - '.$to_date; ?> <span
                                                    class="icon-remove"></span></a>
                                        <?php } ?>
                                         
                                        
                                    </div>
                                    <a class="filter-clear btn" id="filterClear"><?php echo CLEAR_ALL_FILTERS; ?></a>
                                </div>
                            <?php } ?>
                        </div>
                            <table class="table table-striped table-bordered " id="sample1">
                                <thead>
                                <tr>

                                    <th class="hidden-phone tab_hidden"><?php NO; ?></th>
                                    <th><?php echo PROJECT; ?></th>
                                    <th><?php echo INVESTOR; ?></th>

                                    <th><?php echo AMOUNT; ?></th>


                                    <th class="hidden-phone tab_hidden"><?php echo IP; ?></th>
                                    <th class="hidden-phone"><?php echo DATE; ?></th>
                                    <th><?php echo MORE; ?></th>
                                </tr>
                                </thead>
                                <tbody>


                                <?php


                                $taxonomy_setting = taxonomy_setting();
                                $project_url = 'properties';


                                if ($result) {
                                    $i = 1;
                                    foreach ($result as $row) {


                                        $property_url = $row->property_url;
                                        $campaign_id = $row->campaign_id;
                                       // $equity = GetOneEquity($campaign_id);
                                        $equity_owner_data = UserData($row->campaign_owner);
                                        $equity_currency_symbol = $equity['equity_currency_symbol'];
                                        $preapproval_key = $row->preapproval_key;
                                        $property_address = $row->property_address;
                                        if ($row->user_id != 0) {
                                            $user_details = UserData($row->user_id);

                                            $user_name = $user_details[0]['user_name'];
                                            $user_profile_slug = $user_details[0]['profile_slug'];
                                            $last_name = $user_details[0]['last_name'];
                                            $user_email = $user_details[0]['email'];
                                        } else {
                                            $user_email = $row->email;
                                        }

                                        $donor_amount = set_currency($row->amount);
                                        $trans_fee = set_currency($row->pay_fee);
                                        //$perk_amt=0;
                                        $currency = set_currency('','', 'yes');


                                     
                                        $host_ip = $row->host_ip;
                                        $date_transaction = date('d/m/Y', strtotime($row->transaction_date_time));

                                        ?>
                                        <tr class="odd gradeX">

                                            <td class="hidden-phone tab_hidden"><?php echo $i; ?></td>
                                            <td><a href="<?php echo site_url($project_url . '/' . $property_url); ?>"
                                                   target="_blank">

                                                    <?php echo $property_address; ?>

                                                </a></td>
                                            <td> <?php if ($row->campaign_owner != 0) { ?>
                                                    <a href="<?php echo site_url('user/' . $user_profile_slug); ?>"
                                                       target="_blank"><?php echo $user_name . ' ' . $last_name; ?></a>
                                                <?php
                                                } else {
                                                    echo "Anonymous";
                                                } ?></td>

                                            <td><?php echo $donor_amount; ?></td>


                                           
                                            <td class="hidden-phone tab_hidden"><a href="http://whatismyipaddress.com/ip/<?php echo $host_ip; ?>" target="_blank"><?php echo $host_ip; ?></a></td>
                                            <td class="hidden-phone"><?php echo $date_transaction; ?></td>

                                            <td style="position:relative"><strong><a href="javascript://"
                                                                                     class="view_detail btn mini green-stripe"
                                                                                     id="<?php echo $preapproval_key; ?>"><?php echo VIEW; ?></a></strong>

                                                <div id="divs<?php echo $preapproval_key; ?>" style="display:none;"
                                                     class="divsclass">


                                                    <div class="padd_row"><?php echo TRANSACTION_ID; ?> <br/> <span
                                                            class="mar_left_10"><?php echo $preapproval_key; ?></span>
                                                    </div>

                                                </div>

                                            </td>
                                        </tr>


                                        <?php
                                        $i++;
                                    }
                                }
                                ?>


                                </tbody>


                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE widget-->
                </div>
            </div>

            <!-- END ADVANCED TABLE widget-->

            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>
<!-- END CONTAINER -->

<!-- BEGIN JAVASCRIPTS -->

<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/DT_bootstrap.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<!-- <script>
    jQuery(document).ready(function() {
       // initiate layout and plugins
       App.init();
    });
 </script>-->

<script type="text/javascript" charset="utf-8">

    /* function fnFormatDetails ( nTr )
     {
     var aData = oTable.fnGetData( nTr );
     var sOut = '<table cellpadding="5" cellspacing="0" style="padding-left:50px; width:100%; backgroud-color:#ddd;" class="table2-striped";>';
     sOut += '<tr><td>Update:</td><td>'+aData[1]+'</td></tr>';
     sOut += '<tr><td>Perk:</td><td>'+aData[2]+'</td></tr>';
     sOut += '<tr><td>Comment:</td><td>'+aData[3]+'</td></tr>';
     sOut += '<tr><td>Email:</td><td>'+aData[4]+'</td></tr>';
     sOut += '</table>';

     return sOut;
     }*/

    jQuery(document).ready(function () {
        App.init();
        /*	$( ".view_detail" ).each(function( index ) {
         $(".view_detail").bind('click',function(){

         var divname= $(this).attr("id");




         var mydiv = ("#divs"+divname);



         if (!$(mydiv).is(":visible")) {


         $(mydiv).fadeIn();
         $('div .divsclass').not(mydiv).fadeOut();
         } else {

         $(mydiv).fadeOut();

         }



         });
         });
         */
        $(".view_detail").live('click', function () {

            var divname = $(this).attr("id");


            var mydiv = ("#divs" + divname);


            if (!$(mydiv).is(":visible")) {


                $(mydiv).fadeIn();
                $('div .divsclass').not(mydiv).fadeOut();
            } else {

                $(mydiv).fadeOut();

            }


        });


        /* $("#sample_1 tbody td .view_detail").mouseenter(function() {
         var divname= $(this).attr("id");



         $("#divs"+divname).show();
         }).mouseleave(function() {
         var divname= $(this).attr("id");
         $("#divs"+divname).hide();
         });*/

        /* oTable = $('#sample_1').dataTable();

         $('#sample_1 tbody td .view_detail').click(function () {

         var divname= $(this).attr("id");

         var nTr2 = $(this).parents('tr')[0];

         var mydiv = ("#divs"+divname);
         var nTr = oTable.fnGetPosition($(mydiv).closest('tr')[0]);

         if (oTable.fnIsOpen(nTr2) )
         {
         // This row is already open - close it

         oTable.fnClose( nTr2 );

         }
         else
         {
         // Open this row

         oTable.fnOpen( nTr2, fnFormatDetails(nTr), 'details' );

         }

         });*/
    });

jQuery(document).ready(function () {


        
  $.extend($.fn.dataTableExt.oSort,{
            "date-uk-pre": function ( a ) {
                var ukDatea = a.split('/');
                return (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
            },

            "date-uk-asc": function ( a, b ) {
                return ((a < b) ? -1 : ((a > b) ? 1 : 0));
            },

            "date-uk-desc": function ( a, b ) {
                return ((a < b) ? 1 : ((a > b) ? -1 : 0));
            }
        } );

         $('#sample_new').dataTable({
            "bDestroy": true,
            "aoColumnDefs": [{'bSortable': false, 'aTargets': [0,8]}],
             "aoColumns": [
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                { "sType": "date-uk" },
                null
            ],
            "oLanguage": {
                "sLengthMenu": " _MENU_ <?php echo RECORD_PER_PAGE; ?>",
                "sZeroRecords": "<?php echo NOTHING_FOUND_SORRY; ?>",
                "sInfo": "<?php echo SHOWING; ?> _START_ to _END_ of _TOTAL_ <?php echo RECORD; ?>",
                "sInfoEmpty": "<?php echo SHOWING; ?> 0 to 0 of 0 <?php echo RECORD; ?>",
                "sSearch": "<?php echo SEARCH; ?>: ",
                'oPaginate': {

                    'sPrevious': '<?php echo PREVIOUS; ?>',
                    'sNext': '<?php echo NEXT; ?>'
                }
            },
               "dom": '<"top">lt<"bottom"p><"clear">',
                

        });


    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#filterPill .filter-pill").click(function () {
            var res = document.URL;
            $(this).remove();
            id = $(this).attr('id');

            new_url = res.replace('', '');
          
            if (id == 'equity_name_tag') {
                new_url = '<?php echo site_url('admin/transaction_type/list_transaction?equity_name=&user_name='.$user_name_get.'&min_amount='.$min_amount.'&max_amount='.$max_amount.'&trans_id='.$trans_id.'&from_date='.$from_date.'&to_date='.$to_date);?>';
            }
            if (id == 'user_name_tag') {
                new_url = '<?php echo site_url('admin/transaction_type/list_transaction?equity_name='.$equity_name.'&user_name=&min_amount='.$min_amount.'&max_amount='.$max_amount.'&trans_id='.$trans_id.'&from_date='.$from_date.'&to_date='.$to_date);?>';
            }
            if (id == 'min_amount_tag') {
                new_url = '<?php echo site_url('admin/transaction_type/list_transaction?equity_name='.$equity_name.'&user_name='.$user_name_get.'&min_amount=&min_amount='.$max_amount.'&trans_id='.$trans_id.'&from_date='.$from_date.'&to_date='.$to_date);?>';
            }
            if (id == 'max_amount_tag') {
                new_url = '<?php echo site_url('admin/transaction_type/list_transaction?equity_name='.$equity_name.'&user_name='.$user_name_get.'&min_amount='.$min_amount.'&max_amount=&trans_id='.$trans_id.'&from_date='.$from_date.'&to_date='.$to_date);?>';
            }
            if (id == 'trans_id_tag') {
                new_url = '<?php echo site_url('admin/transaction_type/list_transaction?equity_name='.$equity_name.'&user_name='.$user_name_get.'&min_amount='.$min_amount.'&max_amount='.$max_amount.'&trans_id=&from_date='.$from_date.'&to_date='.$to_date);?>';
            }
             if (id == 'from_date_tag') {
                new_url = '<?php echo site_url('admin/transaction_type/list_transaction?equity_name='.$equity_name.'&user_name='.$user_name_get.'&min_amount='.$min_amount.'&max_amount='.$max_amount.'&trans_id='.$trans_id.'&from_date=&to_date=');?>';
            }
          

            window.location.href = new_url;
        });
        $("#filterClear").click(function () {
            $("#filterPill .filter-pill").remove();
            $(this).remove();
            window.location.href = '<?php echo site_url('admin/transaction_type/list_transaction'); ?>';
        });

      

    });
</script>


<script type="text/javascript">
$(function() {

    var to_date_time = '<?=strtotime($to_date)?>';
    var from_date_time = '<?=strtotime($from_date)?>';

    /*function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        $('#from_date').val(start.format('MMMM D, YYYY'));
        $('#to_date').val(end.format('MMMM D, YYYY'));
    }

    //cb(moment(), moment());

    if(to_date_time!="" && from_date_time!="")
        cb(moment(new Date(from_date_time*1000)), moment(new Date(to_date_time*1000)));
    else
        cb(moment(), moment());

    $('#reportrange').daterangepicker({
        ranges: {
           'Clear' : [moment().subtract(-1, 'days'), moment().subtract(-1, 'days')],
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);*/

    function cb(start, end) { 
        
        var d1= new Date();
        var d2= new Date(start);
        if(d1.getTime() < d2.getTime()){
            $('#from_date').val('');
            $('#to_date').val('');
            $('#reportrange span').html('Month DD, Year - Month DD, Year');
        }else{
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            $('#from_date').val(start.format('MMMM D, YYYY'));
            $('#to_date').val(end.format('MMMM D, YYYY'));
        }
    }
    
    cb(moment().subtract(-1, 'days'), moment().subtract(-1,'days'));
    $('#from_date').val('');
    $('#to_date').val('');

    $('#reportrange span').html('Month DD, Year - Month DD, Year');
    $('#reportrange').daterangepicker({
        ranges: {
           'Clear' : [moment().subtract(-1, 'days'), moment().subtract(-1, 'days')],
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);
    $('.ranges li:first-child').click();
 <?php 
 if($from_date!=''){
            ?>
            $('#reportrange span').html('<?php echo $_GET['from_date'].' - '.$_GET['to_date']; ?>');
            $('.ranges li:last-child').click();
            <?php }else{
                ?>
                    $('.ranges li:first-child').click();
                <?php
            } 
            ?>
});
</script>
<style>
    .searchProject-area input[type="text"]{
        max-width: 105px;
    }
    .marl0t5{
        margin-left: 0;
        margin-top: 5px;
    }
.date-range-style {
    padding: 5px;
    cursor: pointer;
}
.searchProject-area .pull-right {
    float: none;
}
.date-range-style .caret {
    vertical-align: middle;
}
</style>