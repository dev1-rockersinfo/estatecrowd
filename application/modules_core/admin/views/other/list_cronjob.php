<script type="text/javascript" language="javascript">


    $(document).ready(function () {

        setInterval("location.reload(true)", 300000);

    });


</script>


<script>

    function setaction(elename, actionval, actionmsg, formname) {

        vchkcnt = 0;

        elem = document.getElementsByName(elename);


        for (i = 0; i < elem.length; i++) {

            if (elem[i].checked) vchkcnt++;

        }


        if (vchkcnt == 0) {

            alert('<?php echo DELETE_SELECT_RECORD_CONFIRMATION;?>');

        } else {


            if (confirm(actionmsg)) {


                document.getElementById('action').value = actionval;

                document.getElementById(formname).submit();

            }


        }


    }


</script>

<!-- BEGIN CONTAINER -->

<div id="container" class="row-fluid">

<!-- BEGIN SIDEBAR -->

<?php echo $this->load->view('admin_sidebar'); ?>

<!-- END SIDEBAR -->

<!-- BEGIN PAGE -->

<div id="main-content">

<!-- BEGIN PAGE CONTAINER-->

<div class="container-fluid">

<!-- BEGIN PAGE HEADER-->

<div class="row-fluid">

    <div class="span12">

        <!-- BEGIN THEME CUSTOMIZER-->

        <div id="theme-change" class="hidden-phone">

            <i class="icon-cogs"></i>

                        <span class="settings">

                            <span class="text"><?php echo THEME; ?>:</span> <!-- change by darshan -->

                            <span class="colors">

                                <span class="color-default" data-style="default"></span>

                                <span class="color-gray" data-style="gray"></span>

                                <span class="color-purple" data-style="purple"></span>

                                <span class="color-navy-blue" data-style="navy-blue"></span>

                            </span>

                        </span>

        </div>

        <!-- END THEME CUSTOMIZER-->

        <!-- BEGIN PAGE TITLE & BREADCRUMB-->

        <h3 class="page-title">

            <?php echo CRON_JOBS; ?>

        </h3>

        <ul class="breadcrumb">

            <li>

                <a href="<?php echo site_url('admin/home/dashboard'); ?>"><i class="icon-home"></i></a><span
                    class="divider">&nbsp;</span>

            </li>


            <li><a href="#"><?php echo CRON_JOBS; ?></a><span class="divider-last">&nbsp;</span></li>

        </ul>

        <!-- END PAGE TITLE & BREADCRUMB-->

    </div>

</div>

<!-- END PAGE HEADER-->

<?php if ($msg != '') {


    if ($msg == 'delete') {
        ?>
        <div class="alert alert-success">

            <button class="close" data-dismiss="alert">x</button>

            <strong><?php echo SUCCESS; ?>!</strong> <?php echo RECORD_HAS_BEEN_DELETED_SUCCESSFULLY; ?></div>







    <?php
    }
    if ($msg == 'insert') {
        ?>
        <div class="alert alert-success">

            <button class="close" data-dismiss="alert">x</button>

            <strong><?php echo SUCCESS; ?>!</strong> <?php echo NEW_RECORD_HAS_BEEN_ADDED_SUCCESSFULLY ?>.
        </div>







    <?php
    }
    if ($msg == 'update') {
        ?>
        <div class="alert alert-success">

            <button class="close" data-dismiss="alert">x</button>

            <strong><?php echo SUCCESS; ?>!</strong> <?php echo RECORD_HAS_BEEN_UPDATED_SUCCESSFULLY; ?>.
        </div>







    <?php
    }
} ?>





<!-- BEGIN ADVANCED TABLE widget-->


<div class="row-fluid">


    <div class="span12">

        <!-- BEGIN EXAMPLE TABLE widget-->


        <button type="button" class="btn btn-danger fr marB5"
                onclick="setaction('chk[]','delete', '<?php echo ARE_YOU_SURE_YOU_WANT_TO_DELETE_SELECTED_RECORD; ?>', 'frm_listlogin')">
            <i class="icon-remove icon-white"></i> <?php echo DELETE; ?></button>
        <!-- change by darshan -->



        <?php



        $attributes = array('name' => 'frm_search', 'id' => 'frm_search');

        echo form_open('admin/cronjob/run', $attributes);

        ?>



        <div class="fl">

            <div class="control-group fl">

                <div class="controls">

                    <select class="input-large m-wrap" tabindex="1" name="option">

                        <option value=""><?php echo SELETE_CRON_JOB; ?></option>



                        <?php    foreach ($crons as $cr) {

                            ?>

                            <option value="<?php echo $cr->cron_function; ?>"><?php echo $cr->cron_title; ?></option>

                        <?php

                        }

                        ?>

                    </select>

                </div>

            </div>


            <button type="submit" class="btn blue fl"><?php echo RUN ?></button>

        </div>

        </form>

        <div class="widget">

            <div class="widget-title">

                <h4><i class="icon-reorder"></i><?php echo CRONJOB; ?></h4>

                            <span class="tools">

                                <a href="javascript:;" class="icon-chevron-down"></a>

                            </span>

            </div>


            <div class="widget-body">

                <?php

                $attributes = array('name' => 'frm_listlogin', 'id' => 'frm_listlogin');

                echo form_open_multipart('admin/cronjob/action_cronjob', $attributes);

                ?>

                <input type="hidden" name="action" id="action"/>

                <table class="table table-striped table-bordered" id="sample_1">

                    <thead>

                    <tr>

                        <th style="width:8px;"><input type="checkbox" class="group-checkable"
                                                      data-set="#sample_1 .checkboxes"/></th>

                        <th class="hidden-phone"><?php echo USER_NAME; ?></th>

                        <th><?php echo CRON_JOBS; ?></th>

                        <th><?php echo LAST_RUN_TIME; ?></th>

                        <th><?php echo STATUS; ?></th>

                    </tr>

                    </thead>

                    <tbody>

                    <?php //echo '<pre>'; print_r($result); die;

                    if ($result) {

                        $i = 0;

                        foreach ($result as $row) {

                            if ($row->cron_title != '') {
                                $cron_title = $row->cron_title;
                            } else {
                                $cron_title = 'SERVER';
                            }


                            if ($row->user_id > 0) {
                                $username = $row->username;
                            } else {
                                $username = 'N/A';
                            }

                            $date_cronjob = date('d M,Y H:i:s', strtotime($row->date_run));

                            if ($row->status == '1') {
                                $record_status = RECORDS_UPDATED_BY_THIS_CRONJOB;
                            } else {
                                $record_status = NO_RECORDS_UPDATED_BY_THIS_CRONJOB;
                            }                    //change by darshan

                            $cronjob_id = $row->cronjob_id;

                            ?>

                            <tr class="odd gradeX">

                                <td><input type="checkbox" class="checkboxes" name="chk[]"
                                           value="<?php echo $cronjob_id; ?>"/></td>

                                <td class="hidden-phone"><?php echo $username; ?></td>

                                <td><?php echo $cron_title; ?></td>

                                <td><?php echo $date_cronjob; ?></td>

                                <td><?php echo $record_status; ?></td>

                            </tr>

                            <?php

                            $i++;

                        }

                    }

                    ?>

                    </tbody>

                </table>


                </form>

            </div>


        </div>

        <!-- END EXAMPLE TABLE widget-->

    </div>

</div>


<!-- END ADVANCED TABLE widget-->


<!-- END PAGE CONTENT-->

</div>

<!-- END PAGE CONTAINER-->

</div>

<!-- END PAGE -->

</div>

<!-- END CONTAINER -->


<!-- BEGIN JAVASCRIPTS -->

<!-- Load javascripts at bottom, this will reduce page load time -->

<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/jquery.dataTables.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/DT_bootstrap.js"></script>

<script src="<?php echo base_url(); ?>js/scripts.js"></script>

<script>

    jQuery(document).ready(function () {

        // initiate layout and plugins

        App.init();

    });
    jQuery(document).ready(function () {


        $('#sample_1').dataTable({
            "bDestroy": true,
            "aoColumnDefs": [{"bSortable": false, "aTargets": [0]}],
            "oLanguage": {
                "sLengthMenu": " _MENU_ <?php echo RECORD_PER_PAGE; ?>",
                "sZeroRecords": "<?php echo NOTHING_FOUND_SORRY; ?>",
                "sInfo": "<?php echo SHOWING; ?> _START_ to _END_ of _TOTAL_ <?php echo RECORD; ?>",
                "sInfoEmpty": "<?php echo SHOWING; ?> 0 to 0 of 0 <?php echo RECORD; ?>",
                "sSearch": "<?php echo SEARCH; ?>: ",
                'oPaginate': {

                    'sPrevious': '<?php echo PREVIOUS; ?>',
                    'sNext': '<?php echo NEXT; ?>'
                }
            }

        });


    });
</script>

