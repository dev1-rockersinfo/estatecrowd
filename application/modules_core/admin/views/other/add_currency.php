<!-- BEGIN CONTAINER -->
<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php echo $this->load->view('admin_sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN THEME CUSTOMIZER-->
                    <div id="theme-change" class="hidden-phone">
                        <i class="icon-cogs"></i>
                        <span class="settings">
                             <span class="text"><?php echo THEME; ?>:</span> <!-- change by darshan -->

                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
                    </div>
                    <!-- END THEME CUSTOMIZER-->
                    <h3 class="page-title">
                        <?php echo CURRENCY; ?>
                    </h3>
                    <ul class="breadcrumb">
                        <li>
                            <a href="<?php echo site_url('admin/home/dashboard') ?>"><i class="icon-home"></i></a><span
                                class="divider">&nbsp;</span>
                        </li>

                        <li><a href="#"><?php echo CURRENCY; ?> </a><span class="divider-last">&nbsp;</span></li>
                    </ul>
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <?php
            if ($error != "") {
                ?>
                <div class="alert alert-error">
                    <button class="close" data-dismiss="alert">x</button>
                    <strong><?php echo ERRORS; ?>!</strong> <?php echo $error; ?></div>
            <?php
            }
            ?>
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                <div class="span12 sortable">
                    <!-- BEGIN SAMPLE FORMPORTLET-->
                    <div class="widget">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i><?php echo CURRENCY; ?></h4>
                                        <span class="tools">
                                        <a href="javascript:;" class="icon-chevron-down"></a>
                                        <a href="javascript:;" class="icon-remove"></a>
                                        </span>
                        </div>
                        <div class="widget-body">
                            <!-- BEGIN FORM-->

                            <?php $attributes = array('name' => 'frm_currency', 'class' => 'form-horizontal');
                            echo form_open('admin/currency/add_currency', $attributes);
                            ?>
                            <div class="control-group">
                                <label class="control-label"><?php echo CURRENCY_NAME; ?></label>

                                <div class="controls">
                                    <input type="text" placeholder="<?php echo CURRENCY_NAME; ?>" name="currency_name"
                                           id="currency_name" value="<?php echo $currency_name; ?>"
                                           class="input-xlarge"/> <!-- change by darshan -->
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label"><?php echo CURRENCY_CODE; ?> </label>

                                <div class="controls">
                                    <input type="text" placeholder="<?php echo CURRENCY_CODE; ?>" name="currency_code"
                                           id="currency_code" value="<?php echo $currency_code; ?>"
                                           class="input-xlarge"/> <!-- change by darshan -->
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label"><?php echo CURRENCY_SYMBOL; ?> </label>

                                <div class="controls">
                                    <input type="text" placeholder="<?php echo CURRENCY_SYMBOL; ?>"
                                           name="currency_symbol" id="currency_symbol"
                                           value="<?php echo $currency_symbol; ?>" class="input-xlarge"/>
                                    <!-- change by darshan -->
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label"><?php echo STATUS; ?></label>

                                <div class="controls">
                                    <select class="input-large m-wrap" name="active" id="active" tabindex="1">
                                        <option value="0" <?php if ($active == '0') {
                                            echo "selected";
                                        } ?>><?php echo INACTIVE; ?></option>
                                        <option value="1" <?php if ($active == '1') {
                                            echo "selected";
                                        } ?>><?php echo ACTIVE; ?></option>

                                    </select>
                                </div>
                            </div>

                            <input type="hidden" name="currency_code_id" id="currency_code_id"
                                   value="<?php echo $currency_code_id; ?>"/>

                            <div class="form-actions">
                                <?php
                                if ($currency_code_id == "") {
                                    ?>
                                    <button type="submit" class="btn blue"><i class="icon-ok"></i> <?php echo SUBMIT; ?>
                                    </button>
                                <?php
                                } else {
                                    ?>
                                    <button type="submit" class="btn blue"><i class="icon-ok"></i> <?php echo UPDATE; ?>
                                    </button>
                                <?php
                                }
                                ?>
                                <button type="button" class="btn"
                                        onClick="location.href='<?php echo site_url('admin/currency/list_currency'); ?>'">
                                    <i class=" icon-remove"></i> <?php echo CANCEL ?></button>
                            </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>

            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div id="footer">
    2013 &copy; Fundraising.
    <div class="span pull-right">
        <span class="go-top"><i class="icon-arrow-up"></i></span>
    </div>
</div>
<!-- END FOOTER -->
<!-- Load javascripts at bottom, this will reduce page load time -->

<script type="text/javascript"
        src="<?php echo base_url(); ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        App.init();
    });
</script>
<!-- END JAVASCRIPTS -->
