<div id="container" class="row-fluid">
<!-- BEGIN SIDEBAR -->
<?php echo $this->load->view('admin_sidebar'); ?>
<!-- END SIDEBAR -->
<!-- BEGIN PAGE -->
<div id="main-content">
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN THEME CUSTOMIZER-->
        <div id="theme-change" class="hidden-phone">
            <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text"><?php echo THEME; ?>:</span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
        </div>
        <!-- END THEME CUSTOMIZER-->

        <h3 class="page-title">
            <?php echo TAXONOMY_SETTING; ?>
        </h3>
        <ul class="breadcrumb">
            <li>
                <a href="<?php echo site_url('admin/home/dashboard') ?>"><i class="icon-home"></i></a><span
                    class="divider">&nbsp;</span>
            </li>

            <li><a href="#"><?php echo TAXONOMY_SETTING; ?></a><span class="divider-last">&nbsp;</span></li>
        </ul>
    </div>
</div>
<!-- END PAGE HEADER-->
<?php if ($error != '') {

    if ($error == 'success') {
        ?>
        <div class="alert alert-success">
            <button class="close" data-dismiss="alert">x</button>
            <strong><?php echo SUCCESS; ?>!</strong> <?php echo TAXONOMY_SETTING_UPDATED_SUCCESSFULLY; ?>.
        </div>

    <?php } else { ?>



        <div class="alert alert-error">
            <button class="close" data-dismiss="alert">x</button>
            <strong><?php echo ERRORS; ?>!</strong> <?php echo $error; ?></div>



    <?php
    }
} ?>
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
<div class="span12 sortable">
<!-- BEGIN SAMPLE FORMPORTLET-->
<div class="widget">
<div class="widget-title">
    <h4><i class="icon-reorder"></i><?php echo TAXONOMY_SETTING; ?></h4>
                                        <span class="tools">
                                        <a href="javascript:;" class="icon-chevron-down"></a>
                                        <a href="javascript:;" class="icon-remove"></a>
                                        </span>
</div>
<div class="widget-body common_sty">
       <!-- BEGIN FORM-->
    <form action="<?php echo site_url('admin/site_setting/add_taxonomy/'.$language_id); ?>" class="form-horizontal"
          name="frm_meta_setting" method="post">
        
    <div class="change_lang">
    <div class="control-group">
                    <label class="control-label"><?php echo CHANGE_LANGUAGE; ?></label>
                                     <span onmouseover="show_bg('active')" onmouseout="hide_bg('active')">
                                    <div class="controls">
                                        <select name="language_id" id="language_id" class="input-large m-wrap" tabindex="1" onchange="changelang(this)" >
                                            <?php

                                            foreach ($language as $row1) {
                                                $select = '';
                                                if ($language_id == $row1['language_id']) {
                                                    $select = 'selected="selected"';
                                                }
                                                ?>

                                                <option
                                                    value="<?php echo $row1['language_id']; ?>" <?php echo $select ?>><?php echo $row1['language_name']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                </div>
    </div>
    
 

        <h5><strong> <?php echo 'Project'; ?></strong></h5>

        <div class="control-group">
            <label class="control-label"><?php echo SINGULAR; ?> </label>

            <div class="controls">
                <input type="text" placeholder="Project" class="input-xlarge" name="project_name"
                       id="project_name" value="<?php echo $project_name; ?>"/>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label"><?php echo PLURAL; ?> </label>

            <div class="controls">
                <input type="text" placeholder="Projects" class="input-xlarge" name="project_name_plural"
                       id="project_name_plural" value="<?php echo $project_name_plural; ?>"/>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label"><?php echo PROJECT_URL; ?>  </label>
<?php
$project_url_readonly="";
if (intval($language_id) !=1)
{
	$project_url_readonly="readonly='readonly'";
}
?>
            <div class="controls">
                <?php echo base_url(); ?><input type="text" placeholder="Project URL" class="input-small"     name="project_url" id="project_url" value="<?php echo $project_url; ?>" <?php echo $project_url_readonly; ?> />/<?php echo PROJECT_TITLE_GOES_HERE;?>
            </div>
        </div>
        <h5><strong> <?php echo 'Project Owner'; ?></strong></h5>

        <div class="control-group">
            <label class="control-label"><?php echo SINGULAR; ?>  </label>

            <div class="controls">
                <input type="text" placeholder="Company" class="input-xlarge" name="project_owner"
                       id="project_owner" value="<?php echo $project_owner; ?>"/>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label"><?php echo PLURAL; ?>  </label>

            <div class="controls">
                <input type="text" placeholder="Companies" class="input-xlarge" name="project_owner_plural"
                       id="project_owner_plural" value="<?php echo $project_owner_plural; ?>"/>
            </div>
        </div>
        <h5><strong> <?php echo 'Funds'; ?></strong></h5>

        <div class="control-group">
            <label class="control-label"><?php echo SINGULAR; ?>  </label>

            <div class="controls">
                <input type="text" placeholder="Fund" class="input-xlarge" name="funds" id="funds"
                       value="<?php echo $funds; ?>"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label"><?php echo PLURAL; ?>  </label>

            <div class="controls">
                <input type="text" placeholder="Funds" class="input-xlarge" name="funds_plural"
                       id="funds_plural" value="<?php echo $funds_plural; ?>"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label"><?php echo PAST_PARTICIPATE; ?>  </label>

            <div class="controls">
                <input type="text" placeholder="Funded" class="input-xlarge" name="funds_past"
                       id="funds_past" value="<?php echo $funds_past; ?>"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label"><?php echo GERUND; ?>  </label>

            <div class="controls">
                <input type="text" placeholder="Funding" class="input-xlarge" name="funds_gerund"
                       id="funds_gerund" value="<?php echo $funds_gerund; ?>"/>
            </div>
        </div>
        <h5><strong> <?php echo 'Updates'; ?></strong></h5>

        <div class="control-group">
            <label class="control-label"><?php echo SINGULAR; ?>  </label>

            <div class="controls">
                <input type="text" placeholder="Update" class="input-xlarge" name="updates" id="updates"
                       value="<?php echo $updates; ?>"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label"><?php echo PLURAL; ?>  </label>

            <div class="controls">
                <input type="text" placeholder="Updates" class="input-xlarge" name="updates_plural"
                       id="updates_plural" value="<?php echo $updates_plural; ?>"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label"><?php echo PAST_PARTICIPATE; ?>  </label>

            <div class="controls">
                <input type="text" placeholder="Updated" class="input-xlarge" name="updates_past"
                       id="updates_past" value="<?php echo $updates_past; ?>"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label"><?php echo GERUND; ?>  </label>

            <div class="controls">
                <input type="text" placeholder="Updating" class="input-xlarge" name="updates_gerund"
                       id="updates_gerund" value="<?php echo $updates_gerund; ?>"/>
            </div>
        </div>
        <h5><strong> <?php echo 'Comments'; ?></strong></h5>

        <div class="control-group">
            <label class="control-label"><?php echo SINGULAR; ?>  </label>

            <div class="controls">
                <input type="text" placeholder="Comment" class="input-xlarge" name="comments" id="comments"
                       value="<?php echo $comments; ?>"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label"><?php echo PLURAL; ?>  </label>

            <div class="controls">
                <input type="text" placeholder="Comments" class="input-xlarge" name="comments_plural"
                       id="comments_plural" value="<?php echo $comments_plural; ?>"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label"><?php echo PAST_PARTICIPATE; ?>  </label>

            <div class="controls">
                <input type="text" placeholder="Commented" class="input-xlarge" name="comments_past"
                       id="comments_past" value="<?php echo $comments_past; ?>"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label"><?php echo GERUND; ?>  </label>

            <div class="controls">
                <input type="text" placeholder="Commenting" class="input-xlarge" name="comments_gerund"
                       id="comments_gerund" value="<?php echo $comments_gerund; ?>"/>
            </div>
        </div>

        <h5><strong> <?php echo 'Followers'; ?></strong></h5>

        <div class="control-group">
            <label class="control-label"><?php echo SINGULAR; ?>  </label>

            <div class="controls">
                <input type="text" placeholder="Follower" class="input-xlarge" name="follower" id="follower"
                       value="<?php echo $follower; ?>"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label"><?php echo PLURAL; ?>  </label>

            <div class="controls">
                <input type="text" placeholder="Followers" class="input-xlarge" name="follower_plural"
                       id="follower_plural" value="<?php echo $followers_plural; ?>"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label"><?php echo PAST_PARTICIPATE; ?>  </label>

            <div class="controls">
                <input type="text" placeholder="Followed" class="input-xlarge" name="follower_past"
                       id="follower_past" value="<?php echo $followers_past; ?>"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label"><?php echo GERUND; ?>  </label>

            <div class="controls">
                <input type="text" placeholder="Following" class="input-xlarge" name="follower_gerund"
                       id="follower_gerund" value="<?php echo $followers_gerund; ?>"/>
            </div>
        </div>
 <h5><strong> <?php echo 'Unfollowers'; ?></strong></h5>

        <div class="control-group">
            <label class="control-label"><?php echo SINGULAR; ?>  </label>

            <div class="controls">
                <input type="text" placeholder="Unfollower" class="input-xlarge" name="unfollower" id="unfollower"
                       value="<?php echo $unfollower; ?>"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label"><?php echo PLURAL; ?>  </label>

            <div class="controls">
                <input type="text" placeholder="Unfollowers" class="input-xlarge" name="unfollower_plural"
                       id="unfollower_plural" value="<?php echo $unfollowers_plural; ?>"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label"><?php echo PAST_PARTICIPATE; ?>  </label>

            <div class="controls">
                <input type="text" placeholder="Unfollowed" class="input-xlarge" name="unfollower_past"
                       id="unfollower_past" value="<?php echo $unfollowers_past; ?>"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label"><?php echo GERUND; ?>  </label>

            <div class="controls">
                <input type="text" placeholder="Unfollowing" class="input-xlarge" name="unfollower_gerund"
                       id="unfollower_gerund" value="<?php echo $unfollowers_gerund; ?>"/>
            </div>
        </div>
 
 
         <h5><strong> <?php echo COMPANY; ?></strong></h5>


         <div class="control-group">

           <label class="control-label"><?php echo INVESTOR; ?>  </label>
           <div class="controls">
               <input type="text" placeholder="<?php echo INVESTOR; ?>" class="input-xlarge" name="investor"
                     id="investor" value="<?php echo $investor; ?>"/>
                      </div>

         </div>
         

         <div class="control-group">

            <label class="control-label"><?php echo COMPANY_NAME; ?>  </label>
               <div class="controls">
             <input type="text" placeholder="<?php echo COMPANY_NAME; ?>" class="input-xlarge" name="company_name"
                    id="company_name" value="<?php echo $company_name; ?>"/>
            
                         </div>

         </div>
         
         <div class="control-group">

            <label class="control-label"><?php echo COMPANY_URL; ?>  </label>
            
            <div class="controls">
                <input type="text" placeholder="<?php echo COMPANY_URL; ?>" class="input-xlarge" name="company_url"
                      id="company_url" value="<?php echo $company_url; ?>"/>
                             </div>

         </div>
            
 
        <input type="hidden" name="taxonomy_setting_id" id="taxonomy_setting_id"
               value="<?php echo $taxonomy_setting_id; ?>"/>

        <div class="form-actions">
            <button type="submit" class="btn blue"><i class="icon-ok"></i> <?php echo UPDATES; ?></button>
            <!-- <button type="button" class="btn"><i class=" icon-remove"></i> Cancel</button> -->
        </div>
    </form>
    <!-- END FORM-->
</div>
</div>
<!-- END SAMPLE FORM PORTLET-->
</div>
</div>

<!-- END PAGE CONTENT-->
</div>
<!-- END PAGE CONTAINER-->
</div>
<!-- END PAGE -->
</div>
<script type="text/javascript"
        src="<?php echo base_url(); ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        App.init();
    });
function changelang(obj)
{
	var id =obj.value;
	location.href="<?php echo site_url('admin/site_setting/add_taxonomy'); ?>/"+id
	
}
</script>
