<div id="container" class="row-fluid">

<!-- BEGIN SIDEBAR -->

<?php echo $this->load->view('admin_sidebar'); ?>

<!-- END SIDEBAR -->

<!-- BEGIN PAGE -->

<div id="main-content">

<!-- BEGIN PAGE CONTAINER-->

<div class="container-fluid">

<!-- BEGIN PAGE HEADER-->

<div class="row-fluid">

    <div class="span12">

        <!-- BEGIN THEME CUSTOMIZER-->

        <div id="theme-change" class="hidden-phone">

            <i class="icon-cogs"></i>

                        <span class="settings">

                            <span class="text"><?php echo THEME; ?></span>

                            <span class="colors">

                                <span class="color-default" data-style="default"></span>

                                <span class="color-gray" data-style="gray"></span>

                                <span class="color-purple" data-style="purple"></span>

                                <span class="color-navy-blue" data-style="navy-blue"></span>

                            </span>

                        </span>

        </div>

        <!-- END THEME CUSTOMIZER-->


        <h3 class="page-title">

            <?php echo GOOGLE_PLUS_SETTING; ?>

        </h3>

        <ul class="breadcrumb">

            <li>

                <a href="<?php echo site_url('admin/home/dashboard') ?>"><i class="icon-home"></i></a><span
                    class="divider">&nbsp;</span>

            </li>


            <li><a href="#"><?php echo GOOGLE_PLUS_SETTING; ?></a><span class="divider-last">&nbsp;</span></li>

        </ul>

    </div>

</div>

<!-- END PAGE HEADER-->

<?php if ($error != "") {


    if ($error == 'success') {
        ?>

        <div class="alert alert-success">

            <button class="close" data-dismiss="alert">x</button>

            <strong><?php echo SUCCESS; ?>!</strong> <?php echo GOOGLE_PLUS_SETTING_UPDATED_SUCCESSFULLY; ?></div>



    <?php } else { ?>







        <div class="alert alert-error">

            <button class="close" data-dismiss="alert">x</button>

            <strong><?php echo ERRORS; ?>!</strong> <?php echo $error; ?></div>







    <?php
    }


}







?>

<!-- BEGIN PAGE CONTENT-->

<div class="row-fluid">

    <div class="span12 sortable">

        <!-- BEGIN SAMPLE FORMPORTLET-->

        <div class="widget">

            <div class="widget-title">

                <h4><i class="icon-reorder"></i><?php echo GOOGLE_PLUS_SETTING; ?></h4>

                                        <span class="tools">

<a href="https://fundraising.zendesk.com/attachments/token/8LlvAMSzKuVoOCqswSLoWX1vq/?name=Fundraising+Google+API.pdf"
   title="Google Helper Document" class="element icon-question-sign" data-placement="left" data-toggle="tooltip"></a>
                                        </span>

            </div>

            <div class="widget-body">

                <!-- BEGIN FORM-->

                <form action="<?php echo site_url('admin/social_setting/add_google_plus_setting') ?>"
                      class="form-horizontal" name="frm_google_setting" method="post">


                    <div class="control-group">

                        <label class="control-label"><?php echo GOOGLE_PROFILE_FULL_URL; ?></label>

                        <div class="controls">

                            <textarea name="google_plus_link"
                                      id="google_plus_link"><?php echo $google_plus_link; ?></textarea>

                        </div>

                    </div>


                    <div class="control-group">

                        <label class="control-label"><?php echo GOOGLE_PLUS_ENABLED; ?></label>

                        <div class="controls">

                            <select class="input-large m-wrap" tabindex="1" name="google_plus_enable"
                                    id="google_plus_enable">

                                <option value="1" <?php if ($google_plus_enable == '1') {
                                    echo 'selected="selected"';
                                } ?>><?php echo YES; ?></option>


                                <option value="0" <?php if ($google_plus_enable == '0') {
                                    echo 'selected="selected"';
                                } ?> ><?php echo NOS; ?></option>

                            </select>

                        </div>

                    </div>


                    <input type="hidden" name="google_plus_setting_id" id="google_plus_setting_id"
                           value="<?php echo $google_plus_setting_id; ?>"/>


                    <div class="form-actions">

                        <button type="submit" class="btn blue"><i class="icon-ok"></i> <?php echo UPDATES; ?></button>

                    </div>

                </form>

                <!-- END FORM-->

            </div>

        </div>

        <!-- END SAMPLE FORM PORTLET-->

    </div>

</div>


<!-- END PAGE CONTENT-->

</div>

<!-- END PAGE CONTAINER-->

</div>

<!-- END PAGE -->

</div>

<script type="text/javascript"
        src="<?php echo base_url(); ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>

<script src="<?php echo base_url(); ?>js/scripts.js"></script>

<script>

    jQuery(document).ready(function () {

        // initiate layout and plugins

        App.init();

    });


</script>
