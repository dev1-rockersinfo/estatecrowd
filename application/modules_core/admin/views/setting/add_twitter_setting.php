<div id="container" class="row-fluid">

<!-- BEGIN SIDEBAR -->

<?php echo $this->load->view('admin_sidebar'); ?>

<!-- END SIDEBAR -->

<!-- BEGIN PAGE -->

<div id="main-content">

<!-- BEGIN PAGE CONTAINER-->

<div class="container-fluid">

<!-- BEGIN PAGE HEADER-->

<div class="row-fluid">

    <div class="span12">

        <!-- BEGIN THEME CUSTOMIZER-->

        <div id="theme-change" class="hidden-phone">

            <i class="icon-cogs"></i>

                        <span class="settings">

                            <span class="text"><?php echo THEME; ?>:</span>

                            <span class="colors">

                                <span class="color-default" data-style="default"></span>

                                <span class="color-gray" data-style="gray"></span>

                                <span class="color-purple" data-style="purple"></span>

                                <span class="color-navy-blue" data-style="navy-blue"></span>

                            </span>

                        </span>

        </div>

        <!-- END THEME CUSTOMIZER-->


        <h3 class="page-title">

            <?php echo TWITTER_SETTING; ?>

        </h3>

        <ul class="breadcrumb">

            <li>

                <a href="<?php echo site_url('admin/home/dashboard') ?>"><i class="icon-home"></i></a><span
                    class="divider">&nbsp;</span>

            </li>


            <li><a href="#"><?php echo TWITTER_SETTING; ?></a><span class="divider-last">&nbsp;</span></li>

        </ul>

    </div>

</div>

<!-- END PAGE HEADER-->

<?php if ($error != "") {


    if ($error == 'success') {
        ?>

        <div class="alert alert-success">

            <button class="close" data-dismiss="alert">x</button>

            <strong><?php echo SUCCESS; ?>!</strong> <?php echo TWITTER_SETTING_UPDATED_SUCCESSFULLY; ?>.
        </div>



    <?php } else { ?>







        <div class="alert alert-error">

            <button class="close" data-dismiss="alert">x</button>

            <strong><?php echo ERRORS; ?>!</strong> <?php echo $error; ?></div>







    <?php
    }


}





?>



<?php if ($message != '') { ?>

    <div style="text-align:center;" class="msgdisp"><?php echo TWITTER_SETTING_YOU_HAVE_ENTERED_ARE_TRUE; ?> </div>

<?php } ?>

<!-- BEGIN PAGE CONTENT-->

<div class="row-fluid">

<div class="span12 sortable">

<!-- BEGIN SAMPLE FORMPORTLET-->

<div class="widget">

<div class="widget-title">

    <h4><i class="icon-reorder"></i><?php echo TWITTER_SETTING ?></h4>

                                        <span class="tools">
<a href="https://fundraising.zendesk.com/attachments/token/jEHKMcMsmJjje5gJH6oUbOACO/?name=Fundraising+Twitter+API.pdf"
   title="Twitter Helper Document" class="element icon-question-sign" data-placement="left" data-toggle="tooltip"></a>                                        </span>

</div>

<div class="widget-body">

<!-- BEGIN FORM-->

<form action="<?php echo site_url('admin/social_setting/add_twitter_setting') ?>" class="form-horizontal"
      name="frm_twitter_setting" method="post">

<input type="hidden" name="twitter_setting_id" id="twitter_setting_id" value="<?php echo $twitter_setting_id; ?>"/>

<div class="control-group">

    <label class="control-label"><?php echo TWITTER_PROFILE_FULL_URL; ?> </label>

    <div class="controls">

        <textarea rows="6" name="twitter_url" id="twitter_url"><?php echo $twitter_url; ?></textarea>

    </div>

</div>


<div class="control-group">

    <label class="control-label"><?php echo TWITTER_ENABLE; ?></label>

    <div class="controls">

        <select class="input-large m-wrap" tabindex="1" name="twitter_enable" id="twitter_enable">

            <option value="1" <?php if ($twitter_enable == '1') {
                echo 'selected="selected"';
            } ?>><?php echo ENABLE; ?></option>


            <option value="0" <?php if ($twitter_enable == '0') {
                echo 'selected="selected"';
            } ?> ><?php echo DISABLE; ?></option>


        </select>

    </div>

</div>

<?php



//$base_image = upload_url();


$CI =& get_instance();


$base_path = $CI->config->slash_item('base_path');


$base_url = $CI->config->slash_item('base_url');







?>

<div class="control-group">

    <label class="control-label"><?php echo CONSUMER_KEYS; ?>  </label>

    <div class="controls">

        <input type="text" placeholder="5AOnNPRCU8hC0Xn6YADANg" class="input-xlarge" name="consumer_key"
               id="consumer_key" value="<?php echo $consumer_key; ?>"/>

    </div>

</div>


<div class="control-group">

    <label class="control-label"><?php echo CONSUMER_SECRETS; ?> </label>

    <div class="controls">

        <input type="text" placeholder="Hn56MYsNkgVIpG9bZ1sV7vW6PvsscXEBwVA8m4dxuf0" class="input-xlarge"
               name="consumer_secret" id="consumer_secret" value="<?php echo $consumer_secret; ?>"/>

    </div>

</div>





<?php if ($autopost_site == '1') { ?>

    <div id="twitter_details" style="display:block;">


        <div class="control-group">

            <label class="control-label"><?php echo ACCESS_TOKEN_SECRET; ?> </label>

            <div class="controls">

                <input type="text" placeholder="Hn56MYsNkgVIpG9bZ1sV7vW6PvsscXEBwVA8m4dxuf0" class="input-xlarge"
                       name="tw_oauth_token_secret" id="tw_oauth_token_secret"
                       value="<?php echo $tw_oauth_token_secret; ?>" readonly="readonly"/>

            </div>

        </div>


        <div class="control-group">

            <label class="control-label"><?php echo ACCESS_TOKEN; ?> </label>

            <div class="controls">

                <input type="text" placeholder="Hn56MYsNkgVIpG9bZ1sV7vW6PvsscXEBwVA8m4dxuf0" class="input-xlarge"
                       name="tw_oauth_token" id="tw_oauth_token" value="<?php echo $tw_oauth_token; ?>"
                       readonly="readonly"/>

            </div>

        </div>


        <div class="control-group">

            <label class="control-label"><?php echo TWIITER_IMAGE; ?></label>

            <div class="controls">

                <?php if ($twiter_img != '' && is_file($base_path . 'upload/thumb/' . $twiter_img)) { ?>



                    <img src="<?php echo base_url(); ?>upload/thumb/<?php echo $twiter_img; ?>" id="media_image_img"/>



                <?php
                } else {
                    echo 'Not Available';
                } ?>


            </div>

        </div>


    </div>

<?php } else { ?>

    <div id="twitter_details" style="">


        <div class="control-group">

            <label class="control-label"><?php echo TWITTER_USERANME; ?> </label>

            <div class="controls">

                <input type="text" placeholder="" class="input-xlarge" name="tw_oauth_token_secret"
                       id="tw_oauth_token_secret" value="<?php echo $tw_oauth_token_secret; ?>"/>

            </div>

        </div>


        <!--
													<div class="control-group">

                                        <label class="control-label"><?php echo 'Widget ID'; ?> </label>

                                        <div class="controls">

                                            <input type="text" placeholder="" class="input-xlarge" name="tw_oauth_token" id="tw_oauth_token" value="<?php echo $tw_oauth_token; ?>"  />                                        

                                        </div>

                                    </div>

	-->

        <div class="control-group">

            <label class="control-label"><?php //echo TWIITER_IMAGE; ?></label>

            <div class="controls">


            </div>

        </div>


    </div>

<?php } ?>

<div class="form-actions">

    <button type="submit" class="btn blue"><i class="icon-ok"></i> <?php echo UPDATES; ?></button>

</div>

</form>

<!-- END FORM-->

</div>

</div>

<!-- END SAMPLE FORM PORTLET-->

</div>

</div>


<!-- END PAGE CONTENT-->

</div>

<!-- END PAGE CONTAINER-->

</div>

<!-- END PAGE -->

</div>

<script type="text/javascript">


    function display_twitter() {


        if (document.getElementById('autopost_site').checked == true) {


            document.getElementById('twitter_details').style.display = 'block';


        } else {

            document.getElementById('twitter_details').style.display = 'none';

        }

    }


</script>

<script type="text/javascript"
        src="<?php echo base_url(); ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>

<script src="<?php echo base_url(); ?>js/scripts.js"></script>

<script>

    jQuery(document).ready(function () {

        // initiate layout and plugins

        App.init();

    });


</script>
