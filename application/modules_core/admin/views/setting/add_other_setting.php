<div id="container" class="row-fluid">

<!-- BEGIN SIDEBAR -->

<?php echo $this->load->view('admin_sidebar'); ?>

<!-- END SIDEBAR -->

<!-- BEGIN PAGE -->

<div id="main-content">

<!-- BEGIN PAGE CONTAINER-->

<div class="container-fluid">

<!-- BEGIN PAGE HEADER-->

<div class="row-fluid">

    <div class="span12">

        <!-- BEGIN THEME CUSTOMIZER-->

        <div id="theme-change" class="hidden-phone">

            <i class="icon-cogs"></i>

                        <span class="settings">

                            <span class="text"><?php echo THEME; ?>:</span>

                            <span class="colors">

                                <span class="color-default" data-style="default"></span>

                                <span class="color-gray" data-style="gray"></span>

                                <span class="color-purple" data-style="purple"></span>

                                <span class="color-navy-blue" data-style="navy-blue"></span>

                            </span>

                        </span>

        </div>

        <!-- END THEME CUSTOMIZER-->





        <?php
        $attspop = array();




        ?>

        <h3 class="page-title">

            <?php echo OTHER_SETTING; ?>

        </h3>

        <ul class="breadcrumb">

            <li>

                <a href="<?php echo site_url('admin/home/dashboard') ?>"><i class="icon-home"></i></a><span
                    class="divider">&nbsp;</span>

            </li>


            <li><a href="#"><?php echo OTHER_SETTING; ?></a><span class="divider-last">&nbsp;</span></li>

        </ul>

    </div>

</div>

<!-- END PAGE HEADER-->



<?php

if ($error != '') {


    if ($error == 'success') {
        ?>

        <div class="alert alert-success">

            <button class="close" data-dismiss="alert">x</button>

            <strong><?php echo SUCCESS; ?>!</strong> <?php echo OTHER_SETTING_UPDATED_SUCCESSFULLY; ?>.
        </div>



    <?php } else { ?>







        <div class="alert alert-error">

            <button class="close" data-dismiss="alert">x</button>

            <strong><?php echo ERRORS; ?>!</strong> <?php echo $error; ?></div>







    <?php
    }

} ?>





<!-- BEGIN PAGE CONTENT-->


<div class="row-fluid">

    <div class="span12 sortable">

        <!-- BEGIN SAMPLE FORMPORTLET-->

        <div class="widget">

            <div class="widget-title">

                <h4><i class="icon-reorder"></i><?php echo OTHER_SETTING; ?></h4>
                            
                                        <span class="tools">

                                        <a href="javascript:;" class="icon-chevron-down"></a>

                                        <a href="javascript:;" class="icon-remove"></a>

                                        </span>

            </div>

            <div class="widget-body common_sty">

                <!-- BEGIN FORM-->


                <form action="<?php echo site_url('admin/site_setting/add_other_setting') ?>" class="form-horizontal"
                      name="frm_site_setting" enctype="multipart/form-data" method="post">


                    <h5><?php echo EQUITY_SETTING; ?></h5>


                    <div class="control-group">

                        <label class="control-label"><?php echo BANK_DETA; ?></label>

                        <div class="controls">
                            <textarea name="bank_detail" id="bank_detail" cols="40"
                                      rows="10"><?php echo $bank_detail; ?></textarea>


                        </div>

                    </div>
                    
                    
                    <hr>
                    
                      <h5><?php echo ACCREDENTIAL_SETTING; ?></h5>
                    
                    <div class="control-group">

                        <label class="control-label"><?php echo ACCREDENTIAL_REQUIRED; ?></label>

                        <div class="controls">

                            <select class="input-large m-wrap" tabindex="1" name="accredential_status"
                                    id="accredential_status">

                                <option value="0" <?php if ($accredential_status == 0) {
                                    echo "selected";
                                } ?>><?php echo YES; ?></option>


                                <option value="1" <?php if ($accredential_status == 1) {
                                    echo "selected";
                                } ?>><?php echo NO; ?></option>


                            </select>

                        </div>

                    </div>
                    <div class="control-group" id="box_accredential_file">


                        <label class="control-label"><?php echo ACCREDIATION_SELECT_MANAGE_TITLE; ?></label>

                        <div class="controls">
                                
                            
                            <input value="0" <?php if ($accrediated_manage == 0) {
                                    echo "checked";
                                } ?> type="radio" name="accrediated_manage" id="accrediated_manage" /> <?php echo ACCREDIATION_SELECT_ALLPROJECT; ?>
                            <br/>
                            
                             <input value="1" <?php if ($accrediated_manage == 1) {
                                    echo "checked";
                                } ?> type="radio" name="accrediated_manage" id="accrediated_manage" /> <?php echo ACCREDIATION_SELECT_PROJECT_OWNER_ADMIN; ?>
                            
                           

                        </div>

<div class="clearfix control-group"></div>

                        <label class="control-label"><?php echo ACCREDENTIAL_DOCUMENT; ?></label>

                        <div class="controls">

                            <input type="file" name="accredential_file" id="accredential_file"/>
                            <input type="hidden" name="accredential_file_hidden" id="accredential_file_hidden"
                                   value="<?php echo $accredential_file; ?>"/>
                            <?php
                            if ($accredential_file != '') {
                                ?>


                                <a href="<?php echo base_url() . 'upload/doc/' . $accredential_file; ?>"><?php echo VIEW; ?></a>
                                &nbsp;    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                            <?php
                            } ?>
                            <a href="<?php echo base_url() . 'upload/Accredited_Investor_Verification_Letter__all_entities_sample.doc'; ?>"><?php echo SAMPLE_FILE; ?></a>
                        </div>

                    </div>
                     <script>
                            var val = $('#accredential_status').val();
                            console.log(val);
                            if (val == 0)$('#box_accredential_file').show();
                            else $('#box_accredential_file').hide();
                            $("#accredential_status").change(function (evt) {
                                var val = $('#accredential_status').val();
                                console.log(val);
                                if (val == 0)$('#box_accredential_file').show();
                                else $('#box_accredential_file').hide();

                            });


                        </script>

                        
                        <hr>
                        
                          <h5><?php echo CONTRACT_SETTING; ?></h5>
                          
                    <div class="control-group">

                        <label class="control-label"><?php echo CONTRACT_COPY_REQUIRED; ?></label>

                        <div class="controls">

                            <select class="input-large m-wrap" tabindex="1" name="contract_copy_status"
                                    id="contract_copy_status">

                                <option value="0" <?php if ($contract_copy_status == 0) {
                                    echo "selected";
                                } ?>><?php echo YES; ?></option>


                                <option value="1" <?php if ($contract_copy_status == 1) {
                                    echo "selected";
                                } ?>><?php echo NO; ?></option>


                            </select>

                        </div>

                    </div>
                    
                    <div class="control-group" id="box_contract_copy_file">
                       
                        <label class="control-label"><?php echo CONTRACT_COPY_UPLOAD_BY; ?></label>

                        <div class="controls">

                            
                            
                             <input value="0" <?php if ($sign_contract_upload_by == 0) {
                                    echo "checked";
                                } ?> type="radio" name="sign_contract_upload_by" id="sign_contract_upload_by" /> <?php echo CONTRACT_COPY_UPLOAD_BY_OWNER; ?>
                            <br/>
                            
                             <input value="1" <?php if ($sign_contract_upload_by == 1) {
                                    echo "checked";
                                } ?> type="radio" name="sign_contract_upload_by" id="sign_contract_upload_by" /> <?php echo CONTRACT_COPY_UPLOAD_BY_ADMIN; ?>
                            
                            
                            
                           

                        </div>

                    </div>
                    <script type="text/javascript">
                         var val = $('#contract_copy_status').val();
                            console.log(val);
                            if (val == 0)$('#box_contract_copy_file').show();
                            else $('#box_contract_copy_file').hide();
                            $("#contract_copy_status").change(function (evt) {
                              
                                var val = $('#contract_copy_status').val();
                                console.log(val);
                                if (val == 0)$('#box_contract_copy_file').show();
                                else $('#box_contract_copy_file').hide();

                            });
                    </script>

                    <div class="form-actions">
                        <input type="hidden" name="site_setting_id" id="site_setting_id"
                               value="<?php echo $site_setting_id; ?>"/>


                        <button type="submit" class="btn blue"><i class="icon-ok"></i> <?php echo SUBMIT; ?></button>

                        <!-- <button type="button" class="btn"><i class=" icon-remove"></i> Cancel</button> -->

                    </div>

                </form>

                <!-- END FORM-->

            </div>

        </div>

        <!-- END SAMPLE FORM PORTLET-->

    </div>

</div>


<!-- END PAGE CONTENT-->

</div>

<!-- END PAGE CONTAINER-->

</div>

<!-- END PAGE -->

</div>

<!-- END CONTAINER -->

<!-- BEGIN JAVASCRIPTS -->

<!-- Load javascripts at bottom, this will reduce page load time -->


<!-- ie8 fixes -->

<!--[if lt IE 9]>

<script src="js/excanvas.js"></script>

<script src="js/respond.js"></script>

<![endif]-->

<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/bootstrap-datepicker/css/datepicker.css"/>
<script type="text/javascript"
        src="<?php echo base_url() ?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

<script type="text/javascript"
        src="<?php echo base_url(); ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>

<script src="<?php echo base_url(); ?>js/scripts.js"></script>

<script>

    jQuery(document).ready(function () {

        // initiate layout and plugins

        App.init();

        $('.date-picker').datepicker();

    });

    function set_enable_funding_type(val) {

        // alert(val)

        if (val == '0' || val == '2') {

            document.getElementById('donation_fixed').style.display = 'block';

        } else {

            document.getElementById('donation_fixed').style.display = 'none';

        }


        if (val == '1' || val == '2') {

            document.getElementById('donation_flexible1').style.display = 'block';

            document.getElementById('donation_flexible2').style.display = 'block';

        } else {

            document.getElementById('donation_flexible1').style.display = 'none';

            document.getElementById('donation_flexible2').style.display = 'none';

        }

    }

</script>

<!-- END JAVASCRIPTS -->
