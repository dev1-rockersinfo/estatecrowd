<div id="container" class="row-fluid">
<!-- BEGIN SIDEBAR -->
<?php echo $this->load->view('admin_sidebar'); ?>
<!-- END SIDEBAR -->
<!-- BEGIN PAGE -->
<div id="main-content">
    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN THEME CUSTOMIZER-->
                <div id="theme-change" class="hidden-phone">
                    <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text"><?php echo THEME; ?></span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
                </div>
                <!-- END THEME CUSTOMIZER-->


                <h3 class="page-title">
                    <?php echo AMOUNT; ?>
                </h3>
                <ul class="breadcrumb">
                    <li>
                        <a href="<?php echo site_url('admin/home/dashboard') ?>"><i class="icon-home"></i></a><span
                            class="divider">&nbsp;</span>
                    </li>

                    <li><a href="#"><?php echo AMOUNT; ?></a><span class="divider-last">&nbsp;</span></li>
                </ul>
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <?php if ($error != "") {
            ?>

            <div class="alert alert-success">
                <button class="close" data-dismiss="alert">x</button>
                <strong><?php echo SUCCESS ?>!</strong> <?php echo $error; ?></div>

        <?php
        }

        ?>


        <!-- BEGIN PAGE CONTENT-->
        <div class="row-fluid">
            <div class="span12 sortable">
                <!-- BEGIN SAMPLE FORMPORTLET-->
                <div class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-reorder"></i> <?php echo ADD_AMOUNT; ?></h4>
                                        <span class="tools">
                                        <a href="javascript:;" class="icon-chevron-down"></a>
                                        <a href="javascript:;" class="icon-remove"></a>
                                        </span>
                    </div>
                    <div class="widget-body">
                        <!-- BEGIN FORM-->
                        <form action="<?php echo site_url('admin/site_setting/add_amount_formatting') ?>"
                              class="form-horizontal" method="post" name="frm_site_setting"
                              enctype="multipart/form-data">


                            <div class="control-group">
                                <label class="control-label"><?php echo CURRENCY_CODE; ?></label>

                                <div class="controls">
                                    <select class="input-large m-wrap" tabindex="1" name="currency_code"
                                            id="currency_code">
                                        <?php

                                        if ($currency) {

                                            foreach ($currency as $cur) {

                                                ?>

                                                <option
                                                    value="<?php echo $cur->currency_code; ?>" <?php if ($cur->currency_code == $currency_code) { ?> selected="selected" <?php } ?>><?php echo $cur->currency_name . '&nbsp;-&nbsp;' . $cur->currency_code . '&nbsp;-&nbsp;' . $cur->currency_symbol; ?></option>

                                            <?php

                                            }

                                        }

                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label"><?php echo WHERE_TO_DISPLAY_CURRENCY_SYMBOL; ?>
                                    ?</label>

                                <div class="controls">
                                    <select class="input-large m-wrap" tabindex="1" name="currency_symbol_side"
                                            id="currency_symbol_side">
                                        <option value="left" <?php if ($currency_symbol_side == 'left') {
                                            echo 'selected="selected"';
                                        } ?>>$100
                                        </option>


                                        <option
                                            value="left_space" <?php if ($currency_symbol_side == 'left_space') {
                                            echo 'selected="selected"';
                                        } ?>>$ 100
                                        </option>


                                        <option value="right" <?php if ($currency_symbol_side == 'right') {
                                            echo 'selected="selected"';
                                        } ?>>100$
                                        </option>

                                        <option
                                            value="right_space" <?php if ($currency_symbol_side == 'right_space') {
                                            echo 'selected="selected"';
                                        } ?>>100 $
                                        </option>


                                        <option value="left_code" <?php if ($currency_symbol_side == 'left_code') {
                                            echo 'selected="selected"';
                                        } ?>>USD100
                                        </option>


                                        <option
                                            value="left_space_code" <?php if ($currency_symbol_side == 'left_space_code') {
                                            echo 'selected="selected"';
                                        } ?>>USD 100
                                        </option>


                                        <option
                                            value="right_code" <?php if ($currency_symbol_side == 'right_code') {
                                            echo 'selected="selected"';
                                        } ?>>100USD
                                        </option>

                                        <option
                                            value="right_space_code" <?php if ($currency_symbol_side == 'right_space_code') {
                                            echo 'selected="selected"';
                                        } ?>>100 USD
                                        </option>

                                         <option value="left_symbol_right_space_code" <?php if($currency_symbol_side=='left_symbol_right_space_code'){ echo 'selected="selected"';   }  ?>>$100 USD</option>                                         

                                    </select>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label"><?php echo DECIMAL_POINTS_AFTER_AMOUNT; ?> </label>

                                <div class="controls">
                                    <select class="input-large m-wrap" tabindex="1" name="decimal_points"
                                            id="decimal_points">
                                        <option value="0" <?php if ($decimal_points == 0) {
                                            echo 'selected="selected"';
                                        } ?>> 0
                                        </option>

                                        <option value="1" <?php if ($decimal_points == 1) {
                                            echo 'selected="selected"';
                                        } ?>> 1
                                        </option>

                                        <option value="2" <?php if ($decimal_points == 2) {
                                            echo 'selected="selected"';
                                        } ?>> 2
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <input type="hidden" name="site_setting_id" id="site_setting_id"
                                   value="<?php echo $site_setting_id; ?>"/>

                            <div class="form-actions">
                                <button type="submit" class="btn blue"><i class="icon-ok"></i> <?php echo UPDATE; ?>
                                </button>

                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>

        <!-- END PAGE CONTENT-->
    </div>
    <!-- END PAGE CONTAINER-->
</div>
<!-- END PAGE -->
</div>
<script type="text/javascript"
        src="<?php echo base_url(); ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        App.init();
    });

</script>
