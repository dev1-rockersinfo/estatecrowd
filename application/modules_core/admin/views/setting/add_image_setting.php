<div id="container" class="row-fluid">
<!-- BEGIN SIDEBAR -->
<?php echo $this->load->view('admin_sidebar'); ?>
<!-- END SIDEBAR -->
<!-- BEGIN PAGE -->
<div id="main-content">
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN THEME CUSTOMIZER-->
        <div id="theme-change" class="hidden-phone">
            <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text"><?php echo THEME; ?></span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
        </div>
        <!-- END THEME CUSTOMIZER-->

        <h3 class="page-title">
            <?php echo IMAGE_SETTING; ?>
        </h3>
        <ul class="breadcrumb">
            <li>
                <a href="<?php echo site_url('admin/home/dashboard') ?>"><i class="icon-home"></i></a><span
                    class="divider">&nbsp;</span>
            </li>

            <li><a href="#"><?php echo IMAGE_SETTING; ?></a><span class="divider-last">&nbsp;</span></li>
        </ul>
    </div>
</div>
<!-- END PAGE HEADER-->
<?php if ($error != "") {

    if ($error == 'success') {
        ?>
        <div class="alert alert-success">
            <button class="close" data-dismiss="alert">x</button>
            <strong><?php echo SUCCESS; ?>!</strong> <?php echo IMAGE_SIZE_SETTING_UPDATED_SUCCESSFULLY; ?>.
        </div>

    <?php } else { ?>



        <div class="alert alert-error">
            <button class="close" data-dismiss="alert">x</button>
            <strong><?php echo ERRORS; ?>!</strong> <?php echo $error; ?></div>



    <?php
    }

}



?>
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
<div class="span12 sortable">
<!-- BEGIN SAMPLE FORMPORTLET-->
<div class="widget">
<div class="widget-title">
    <h4><i class="icon-reorder"></i><?php echo IMAGE_SETTING; ?></h4>
                                        <span class="tools">
                                        <a href="javascript:;" class="icon-chevron-down"></a>
                                        <a href="javascript:;" class="icon-remove"></a>
                                        </span>
</div>
<div class="widget-body">
    <!-- BEGIN FORM-->
    <form action="<?php echo site_url('admin/site_setting/add_image_setting') ?>" class="form-horizontal"
          name="frm_site_setting" method="post" enctype="multipart/form-data">

        <div class="control-group">
            <label class="control-label"><?php echo PROJECT_THEUMBNAIL_WIDTH ?></label>

            <div class="controls">
                <input type="text" placeholder="305" class="input-xlarge" name="p_thumb_width"
                       id="p_thumb_width" value="<?php echo $p_thumb_width; ?>"/>
            </div>
        </div>


        <div class="control-group">
            <label class="control-label"><?php echo PROJECT_THEUMBNAIL_HEIGHT; ?> </label>

            <div class="controls">
                <input type="text" placeholder="212" class="input-xlarge" name="p_thumb_height"
                       id="p_thumb_height" value="<?php echo $p_thumb_height; ?>"/>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label"><?php echo PROJECT_THEUMBNAIL_ASPECT_RATIO; ?>  </label>

            <div class="controls">
                <select class="input-large m-wrap" tabindex="1" name="p_ratio" id="p_ratio">
                    <option value="1" <?php if ($p_ratio == '1') {
                        echo 'selected="selected"';
                    } ?> ><?php echo TRUE; ?></option>


                    <option value="0" <?php if ($p_ratio == '0') {
                        echo 'selected="selected"';
                    } ?> ><?php echo FALSE; ?></option>

                </select>
            </div>
        </div>


        <div class="control-group">
            <label class="control-label"><?php echo PROJECT_SMALL_WIDTH; ?></label>

            <div class="controls">
                <input type="text" placeholder="480" class="input-xlarge" name="p_s_width" id="p_s_width"
                       value="<?php echo $p_s_width; ?>"/>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label"><?php echo PROJECT_SMALL_HEIGHT ?> </label>

            <div class="controls">
                <input type="text" placeholder="360" class="input-xlarge" name="p_s_height" id="p_s_height"
                       value="<?php echo $p_s_height; ?>"/>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label"><?php echo PROJECT_MEDIUM_WIDTH; ?></label>

            <div class="controls">
                <input type="text" placeholder="640" class="input-xlarge" name="p_m_width" id="p_m_width"
                       value="<?php echo $p_m_width; ?>"/>
            </div>
        </div>


        <div class="control-group">
            <label class="control-label"><?php echo PROJECT_MEDIUM_HEIGHT; ?>  </label>

            <div class="controls">
                <input type="text" placeholder="426" class="input-xlarge" name="p_m_height" id="p_m_height"
                       value="<?php echo $p_m_height; ?>"/>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label"><?php echo PROJECT_LARGE_WIDTH; ?> </label>

            <div class="controls">
                <input type="text" placeholder="717" class="input-xlarge" name="p_l_width" id="p_l_width"
                       value="<?php echo $p_l_width; ?>"/>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label"><?php echo PROJECT_LARGE_HEIGHT; ?> </label>

            <div class="controls">
                <input type="text" placeholder="308" class="input-xlarge" name="p_l_height" id="p_l_height"
                       value="<?php echo $p_l_height; ?>"/>
            </div>
        </div>


        <div class="control-group">
            <label class="control-label"><?php echo USER_SMAIL_WIDTH ?> </label>

            <div class="controls">
                <input type="text" placeholder="200" class="input-xlarge" name="u_s_width" id="u_s_width"
                       value="<?php echo $u_s_width; ?>"/>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label"><?php echo USER_SMAIL_HEIGHT; ?> </label>

            <div class="controls">
                <input type="text" placeholder="200" class="input-xlarge" name="u_s_height" id="u_s_height"
                       value="<?php echo $u_s_height; ?>"/>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label"><?php echo USER_MEDIUM_WIDTH; ?> </label>

            <div class="controls">
                <input type="text" placeholder="133" class="input-xlarge" name="u_m_width" id="u_m_width"
                       value="<?php echo $u_m_width; ?>"/>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label"><?php echo USER_MEDIUM_HEIGHT; ?>  </label>

            <div class="controls">
                <input type="text" placeholder="133" class="input-xlarge" name="u_m_height" id="u_m_height"
                       value="<?php echo $u_m_height; ?>"/>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label"><?php echo USER_BIG_WIDTH ?></label>

            <div class="controls">
                <input type="text" placeholder="270" class="input-xlarge" name="u_b_width" id="u_b_width"
                       value="<?php echo $u_b_width; ?>"/>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label"><?php echo USER_BIG_HEIGHT; ?></label>

            <div class="controls">
                <input type="text" placeholder="318" class="input-xlarge" name="u_b_height" id="u_b_height"
                       value="<?php echo $u_b_height; ?>"/>
            </div>
        </div> 
        <div class="control-group">

            <label class="control-label"><?php echo IMAGE_UPLOAD_LIMIT; ?></label>

            <div class="controls">

                <input type="number"  max= "2" min="0" placeholder="318" class="input-xlarge" name="upload_limit" id="upload_limit"

                       value="<?php echo $upload_limit; ?>"/> <em>MB</em>

            </div>

        </div>


        <div class="control-group">
            <label class="control-label"><?php echo USER_THUMBAIL_ASPECT_RATIO; ?></label>

            <div class="controls">
                <select class="input-large m-wrap" tabindex="1" name="u_ratio" id="u_ratio">
                    <option value="1" <?php if ($u_ratio == '1') {
                        echo 'selected="selected"';
                    } ?> ><?php echo TRUE; ?></option>


                    <option value="0" <?php if ($u_ratio == '0') {
                        echo 'selected="selected"';
                    } ?> ><?php echo FALSE; ?></option>

                </select>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label"><?php echo PROJECT_GALLERY_THUMBNAIL_ASPECT_RATIO; ?></label>

            <div class="controls">
                <select class="input-large m-wrap" tabindex="1" name="g_ratio" id="g_ratio">
                    <option value="1" <?php if ($g_ratio == '1') {
                        echo 'selected="selected"';
                    } ?> ><?php echo TRUE; ?></option>


                    <option value="0" <?php if ($g_ratio == '0') {
                        echo 'selected="selected"';
                    } ?> ><?php echo FALSE; ?></option>


                </select>
            </div>
        </div>
        <input type="hidden" name="image_setting_id" id="image_setting_id"
               value="<?php echo $image_setting_id; ?>"/>

        <div class="form-actions">
            <button type="submit" class="btn blue"><i class="icon-ok"></i> <?php echo UPDATES; ?></button>
        </div>
    </form>
    <!-- END FORM-->
</div>
</div>
<!-- END SAMPLE FORM PORTLET-->
</div>
</div>

<!-- END PAGE CONTENT-->
</div>
<!-- END PAGE CONTAINER-->
</div>
<!-- END PAGE -->
</div>
<script type="text/javascript"
        src="<?php echo base_url(); ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        App.init();
    });

</script>
