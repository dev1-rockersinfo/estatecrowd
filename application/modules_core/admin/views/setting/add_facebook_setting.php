<?php

//$this->load->library('fb_connect');

//$fbLoginURL = $this->fb_connect->myloginurl('home/facebook/facebook_setting');

$data = array(
    'facebook' => $this->fb_connect->fb,
    'fbSession' => $this->fb_connect->fbSession,
    'user' => $this->fb_connect->user,
    'uid' => $this->fb_connect->user_id,
    'fbLogoutURL' => $this->fb_connect->fbLogoutURL,
    'fbLoginURL' => $this->fb_connect->fbLoginURL,
    'site_url' => site_url('home/facebook/facebook_admin'),
    'appkey' => $this->fb_connect->appkey,
);
$fbLoginURL = $data['fbLoginURL'];

/*$data = array(

    'facebook'		=> $this->fb_connect->fb,

    'fbSession'		=> $this->fb_connect->fbSession,

    'user'			=> $this->fb_connect->user,

    'uid'			=> $this->fb_connect->user_id,

    'fbLogoutURL'	=> $this->fb_connect->fbLogoutURL,

    'fbLoginURL'	=> $this->fb_connect->fbLoginURL,

    'base_url'		=> site_url('home/facebook/facebook_setting'),

    'appkey'		=> $this->fb_connect->appkey,

);
*/
//var_dump($data);

$base_image = upload_url();

$CI =& get_instance();

$base_path = $CI->config->slash_item('base_path');



?>
<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php echo $this->load->view('admin_sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN THEME CUSTOMIZER-->
                    <div id="theme-change" class="hidden-phone">
                        <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text"><?php echo THEME; ?></span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
                    </div>
                    <!-- END THEME CUSTOMIZER-->

                    <h3 class="page-title">
                        <?php echo FACEBOOK_SETTING; ?>
                    </h3>
                    <ul class="breadcrumb">
                        <li>
                            <a href="<?php echo site_url('admin/home/dashboard') ?>"><i class="icon-home"></i></a><span
                                class="divider">&nbsp;</span>
                        </li>

                        <li><a href="#"><?php echo FACEBOOK_SETTING; ?></a><span class="divider-last">&nbsp;</span></li>
                    </ul>
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <?php if ($error != "") {

                if ($error == 'success') {
                    ?>
                    <div class="alert alert-success">
                        <button class="close" data-dismiss="alert">x</button>
                        <strong><?php echo SUCCESS; ?>!</strong> <?php echo FACEBOOK_SETTING_UPDATED_SUCCESSFULLY; ?>.
                    </div>

                <?php } else { ?>



                    <div class="alert alert-error">
                        <button class="close" data-dismiss="alert">x</button>
                        <strong><?php echo ERRORS; ?>!</strong> <?php echo $error; ?></div>



                <?php
                }

            }


            ?>

            <?php if ($message != '') { ?>
                <div style="text-align:center;"
                     class="msgdisp"><?php echo FACEBOOK_SETTING_YOU_HAVE_ENTERED_ARE_TRUE; ?>.
                </div>
            <?php } ?>
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                <div class="span12 sortable">
                    <!-- BEGIN SAMPLE FORMPORTLET-->
                    <div class="widget">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i><?php echo FACEBOOK_SETTING; ?></h4>
                                        <span class="tools">
                                        <a href="https://fundraising.zendesk.com/attachments/token/igO7tdt6OvMxbx9Lr3UurNmsO/?name=Fundraising+Facebook+API.pdf"
                                           title="Facebook Helper Document" class="element icon-question-sign"
                                           data-placement="left" data-toggle="tooltip"></a>                                        </span>
                        </div>
                        <div class="widget-body">
                            <!-- BEGIN FORM-->
                            <form action="<?php echo site_url('admin/social_setting/add_facebook_setting'); ?>"
                                  class="form-horizontal" name="frm_facebook_setting" method="post">
                                <div class="control-group">
                                    <label class="control-label"><?php echo FACEBOOK_PROFILE_FULL_URL; ?> </label>

                                    <div class="controls">
                                        <textarea name="facebook_url" id="facebook_url"
                                                  rows="6"><?php echo $facebook_url; ?></textarea>
                                    </div>
                                </div>


                                <div class="control-group">
                                    <label class="control-label"><?php echo FACEBOOK_APPLICATION_ID; ?> </label>

                                    <div class="controls">
                                        <input type="text" placeholder="589123174457505" class="input-xlarge"
                                               name="facebook_application_id" id="facebook_application_id"
                                               value="<?php echo $facebook_application_id; ?>"/>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label"><?php echo FACEBOOK_LOGIN_ENABLE ?> </label>

                                    <div class="controls">
                                        <select class="input-large m-wrap" tabindex="1" name="facebook_login_enable"
                                                id="facebook_login_enable">
                                            <option value="1" <?php if ($facebook_login_enable == '1') {
                                                echo 'selected="selected"';
                                            } ?>><?php echo ENABLE; ?></option>


                                            <option value="0" <?php if ($facebook_login_enable == '0') {
                                                echo 'selected="selected"';
                                            } ?> ><?php echo DISABLE; ?></option>

                                        </select>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label"><?php echo FACEBOOK_APPLICATION_API_KEY; ?> </label>

                                    <div class="controls">
                                        <input type="text" placeholder="589123174457505" class="input-xlarge"
                                               name="facebook_api_key" id="facebook_api_key"
                                               value="<?php echo $facebook_api_key; ?>"/>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label"><?php echo FACEBOOK_APPLICATION_SECRET_KEY; ?></label>

                                    <div class="controls">
                                        <input type="text" placeholder="b79e5a000b737fcf9ac4c79aa6f70753"
                                               class="input-xlarge" name="facebook_secret_key" id="facebook_secret_key"
                                               value="<?php echo $facebook_secret_key; ?>"/>
                                    </div>
                                </div>


                                <div class="form-actions">
                                    <input type="hidden" name="facebook_setting_id" id="facebook_setting_id"
                                           value="<?php echo $facebook_setting_id; ?>"/>
                                    <button type="submit" class="btn blue"><i class="icon-ok"></i> <?php echo UPDATE; ?>
                                    </button>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>

            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>


<script type="text/javascript">

    window.fbAsyncInit = function () {

        FB.init({appId: '<?php echo $data['appkey'];?>', status: true, cookie: true, xfbml: true});


        /* All the events registered */

        FB.Event.subscribe('auth.login', function (response) {

            // do something with response

            //   login();

        });


        FB.Event.subscribe('auth.logout', function (response) {

            // do something with response

            // logout();

        });

    };


    (function () {

        var e = document.createElement('script');

        e.type = 'text/javascript';

        e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js';

        e.async = true;

        document.getElementById('fb-root').appendChild(e);

    }());


    function login() {

        //document.location.href = "<?php // echo $data['base_url']; ?>";

    }


    function logout() {

        //document.location.href = "<?php //echo $data['base_url']; ?>";

    }

    function display_facebook() {

        if (document.getElementById('facebook_wall_post').checked == true) {

            document.getElementById('facebook_details').style.display = 'block';

        } else {

            document.getElementById('facebook_details').style.display = 'none';

        }

    }


</script>
<script type="text/javascript"
        src="<?php echo base_url(); ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        App.init();
    });

</script>
