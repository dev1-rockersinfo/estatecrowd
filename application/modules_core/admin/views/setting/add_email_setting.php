<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php echo $this->load->view('admin_sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN THEME CUSTOMIZER-->
                    <div id="theme-change" class="hidden-phone">
                        <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text"><?php echo THEME; ?>:</span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
                    </div>
                    <!-- END THEME CUSTOMIZER-->

                    <h3 class="page-title">
                        <?php echo EMAIL_SETTING; ?>
                    </h3>
                    <ul class="breadcrumb">
                        <li>
                            <a href="<?php echo site_url('admin/home/dashboard') ?>"><i class="icon-home"></i></a><span
                                class="divider">&nbsp;</span>
                        </li>

                        <li><a href="#"><?php echo EMAIL_SETTING; ?> </a><span class="divider-last">&nbsp;</span></li>
                    </ul>
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <?php if ($error != "") {

                if ($error == 'success') {
                    ?>
                    <div class="alert alert-success">
                        <button class="close" data-dismiss="alert">x</button>
                        <strong><?php echo SUCCESS; ?>!</strong> <?php echo EMAIL_SETTING_UPDATED_SUCCESSFULLY ?>.
                    </div>

                <?php } else { ?>



                    <div class="alert alert-error">
                        <button class="close" data-dismiss="alert">x</button>
                        <strong><?php echo ERRORS; ?>!</strong> <?php echo $error; ?></div>



                <?php
                }

            }



            ?>            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                <div class="span12 sortable">
                    <!-- BEGIN SAMPLE FORMPORTLET-->
                    <div class="widget">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i><?php echo EMAIL_SETTING; ?></h4>
                                        <span class="tools">
                                        <a href="javascript:;" class="icon-chevron-down"></a>
                                        <a href="javascript:;" class="icon-remove"></a>
                                        </span>
                        </div>
                        <div class="widget-body">
                            <!-- BEGIN FORM-->
                            <form action="<?php echo site_url('admin/email_setting/add_email_setting') ?>"
                                  class="form-horizontal" method="post" name="frm_email_setting" autocomplete="off">


                                <div class="control-group">
                                    <label class="control-label"><?php echo MAILER; ?> </label>

                                    <div class="controls">
                                        <select class="input-large m-wrap" tabindex="1" name="mailer" id="mailer">
                                            <option
                                                value="mail" <?php if ($mailer == 'mail') { ?> selected="selected" <?php } ?> ><?php echo PHP_MAIL; ?></option>


                                            <option
                                                value="smtp" <?php if ($mailer == 'smtp') { ?> selected="selected" <?php } ?> ><?php echo SMTP; ?></option>


                                            <option
                                                value="sendmail" <?php if ($mailer == 'sendmail') { ?> selected="selected" <?php } ?> ><?php echo SENDMAIL; ?></option>
                                        </select>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label"><?php echo SEND_MAIL_PATH; ?></label>

                                    <div class="controls">
                                        <input type="text" placeholder="/usr/sbin/sendmail" class="input-xlarge"
                                               name="sendmail_path" id="sendmail_path"
                                               value="<?php echo $sendmail_path; ?>"/>
                                        <span class="help-inline">(<?php echo IF_MAILER_IS_SENDMAIL; ?>)</span>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label"><?php echo SMTP_PORT ?> </label>

                                    <div class="controls">
                                        <input type="text" placeholder="25" class="input-xlarge" name="smtp_port"
                                               id="smtp_port" value="<?php echo $smtp_port; ?>"/>
                                        <span class="help-inline">(465 or 25 or 587)</span>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label"><?php echo SMTP_HOST; ?> </label>

                                    <div class="controls">
                                        <input type="text" placeholder="smtp@groupfund.me" class="input-xlarge"
                                               name="smtp_host" id="smtp_host" value="<?php echo $smtp_host; ?>"/>
                                        <span class="help-inline">(<?php echo IF_SMTP_USER_IS_GMAIL_THEN; ?>)</span>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label"><?php echo SMTP_EMAIL; ?> </label>

                                    <div class="controls">
                                        <input type="text" placeholder="mail.groupfund.me" class="input-xlarge"
                                               name="smtp_email" id="smtp_email" value="<?php echo $smtp_email; ?>"/>

                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label"><?php echo SMTP_PASSWORD; ?></label>

                                    <div class="controls">
                                        <input type="password" class="input-xlarge" name="smtp_password"
                                               id="smtp_password" value="<?php echo $smtp_password; ?>"/>
                                    </div>
                                </div>

                                <input type="hidden" name="email_setting_id" id="email_setting_id"
                                       value="<?php echo $email_setting_id; ?>"/>

                                <div class="form-actions">
                                    <button type="submit" class="btn blue"><i class="icon-ok"></i> <?php echo UPDATE ?>
                                    </button>
                                    <?php echo anchor(site_url('admin/email_setting/send_test_mail'), SEND_TEST_MAIL, 'class=btn'); ?>
                                </div>

                            </form>
                            <!-- END FORM-->

                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>

            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>
<script type="text/javascript"
        src="<?php echo base_url(); ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        App.init();
    });

</script>
