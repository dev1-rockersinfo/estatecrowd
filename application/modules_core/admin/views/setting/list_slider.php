<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script type="text/javascript" language="javascript">
    function delete_rec(id) {
        var ans = confirm("<?php echo ARE_YOU_SURE_TO_DELETE_IMAGE;?>");
        if (ans) {
            location.href = "<?php echo site_url('admin/equity/delete_dynamic_slider/'); ?>" + "/" + id + "/";
        } else {
            return false;
        }
    }

</script>

<script>
    $(function () {
        $("#slider_setting").change(function () {
            //get the selected value
            var selectedValue = this.value;

            //make the ajax call
            $.ajax({
                url: '<?php echo site_url('admin/site_setting/UpdateSliderSetting')?>',
                type: 'POST',
                data: {slider_setting: selectedValue},
                success: function () {
                    location.reload();
                }
            });
        });
    });
    $(function () {
        $("#sortable").sortable({

            opacity: 0.6,
            cursor: 'move'
        });
        $("#sortable").disableSelection();
    });
</script>
<!-- BEGIN CONTAINER -->
<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php echo $this->load->view('admin_sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN THEME CUSTOMIZER-->
                    <div id="theme-change" class="hidden-phone">
                        <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text"><?php echo THEME; ?></span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
                    </div>
                    <!-- END THEME CUSTOMIZER-->
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        <?php echo SLIDER_SETTING; ?>
                    </h3>
                    <ul class="breadcrumb">
                        <li>
                            <a href="<?php echo base_url() ?>admin/home/dashboard"><i class="icon-home"></i></a><span
                                class="divider">&nbsp;</span>
                        </li>

                        <li><a href="#"><?php echo SLIDER_SETTING; ?></a><span class="divider-last">&nbsp;</span></li>
                    </ul>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->

            <!-- BEGIN ADVANCED TABLE widget-->

            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN EXAMPLE TABLE widget-->
                    <div class="widget">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i><?php echo SLIDER_SETTING; ?></h4>
                            <span class="tools">
                                <a href="javascript:;" class="icon-chevron-down"></a>
                            </span>
                        </div>
                        <div class="widget-body">
                            <p><a href="<?php echo site_url('admin/equity/list_dynamic_slider') ?>"
                                  target="_blank"><?php echo CLICK_HERE; ?> </a> <?php echo TO_GO_MANAGE_IMAGE; ?> </p>

                            <p><a href="<?php echo site_url('admin/equity/list_equity') ?>"
                                  target="_blank"><?php echo CLICK_HERE; ?> </a> <?php echo TO_GO_MANAGE_FEATURED; ?>
                            </p>

                            <form action="<?php echo site_url('admin/site_setting/UpdateSliderOrder') ?>"
                                  class="form-horizontal" name="frm_site_setting" enctype="multipart/form-data"
                                  method="post">

                                <div class="control-group">

                                    <label class="control-label"><?php echo HOME_SLIDER ?></label>

                                    <div class="controls">

                                        <select class="input-large m-wrap" tabindex="1" name="slider_setting"
                                                id="slider_setting">

                                            <option value="0" <?php if ($slider_setting == 0) {
                                                echo "selected";
                                            } ?>><?php echo FEATURED_PROJECTS; ?></option>


                                            <option value="1" <?php if ($slider_setting == 1) {
                                                echo "selected";
                                            } ?>><?php echo DYNAMIC_SLIDER; ?></option>

                                            <option value="2" <?php if ($slider_setting == 2) {
                                                echo "selected";
                                            } ?>><?php echo BOTH; ?></option>
                                        </select>

                                    </div>

                                </div>

                                <ul id="sortable" class="slider-drag">

                                    <?php if ($result) {

                                        foreach ($result as $row) {


                                            $equity_id = $row['equity_id'];
                                            $project_name = $row['project_name'];
                                            $project_description = $row['project_description'];
                                            $dynamic_image_paragraph = $row['dynamic_image_paragraph'];
                                            $dynamic_image_title = $row['dynamic_image_title'];
                                            $view_order = $row['view_order'];
                                            $dynamic_image_image = $row['dynamic_image_image'];
                                            $equity_url = $row['equity_url'];
                                            $cover_photo = $row['cover_photo'];
                                            if ($equity_id != '') {
                                                $slider_title = $project_name;
                                                $slider_image = base_url() . 'upload/equity/small/' . $cover_photo;
                                                $slider_content = '';
                                                $slider_type = PROJECT;
                                                $slider_edit = 'javascript://';
                                                $slider_edit_display = 'none';
                                            } else {
                                                $slider_title = $dynamic_image_title;
                                                $slider_image = base_url() . 'upload/dynamic/' . $dynamic_image_image;
                                                $slider_content = $dynamic_image_paragraph;
                                                $slider_type = BANNER;
                                                $slider_edit = site_url('admin/equity/edit_dynamic_slide/' . $row['dynamic_slider_id']);
                                                $slider_edit_display = 'block';
                                            }


                                            ?>


                                            <li class="slider-drag-item">
                                                <div class="media">
                                                    <a class="pull-left" href="#">
                                                        <img class="media-object" data-src="holder.js/64x64" alt="64x64"
                                                             src="<?php echo $slider_image; ?>"
                                                             style="width: 64px; height: 64px;">
                                                    </a>

                                                    <div class="media-body">
                                                        <h4 class="media-heading"><?php echo $slider_title; ?> <?php if ($equity_id == '') { ?>
                                                                <a href="<?php echo $slider_edit; ?>" target="_blank">
                                                                    <i class="icon-edit"></i></a> <?php } ?></h4>

                                                        <p><?php echo $slider_content; ?></p>

                                                        <p class="marB0">
                                                            <strong>Type:</strong> <?php echo $slider_type; ?></p>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="id[]" value="<?php echo $row['id']; ?>">

                                            </li>
                                        <?php

                                        }
                                    } else {
                                        ?>
                                        <li> No Records found..</li>
                                    <?php } ?>


                                </ul>
                                <?php if ($result) { ?>
                                    <div class="form-actions">
                                        <button type="submit" class="btn blue"><i
                                                class="icon-ok"></i> <?php echo UPDATES; ?></button>
                                        <!-- <button type="button" class="btn"><i class=" icon-remove"></i> Cancel</button> -->
                                    </div>

                                <?php } ?>

                            </form>


                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE widget-->
                </div>
            </div>

            <!-- END ADVANCED TABLE widget-->

            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>
<!-- END CONTAINER -->

<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/DT_bootstrap.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        App.init();
    });
</script>
