<div id="container" class="row-fluid">

<!-- BEGIN SIDEBAR -->

<?php echo $this->load->view('admin_sidebar'); ?>

<!-- END SIDEBAR -->

<!-- BEGIN PAGE -->

<div id="main-content">

<!-- BEGIN PAGE CONTAINER-->

<div class="container-fluid">

<!-- BEGIN PAGE HEADER-->

<div class="row-fluid">

    <div class="span12">

        <!-- BEGIN THEME CUSTOMIZER-->

        <div id="theme-change" class="hidden-phone">

            <i class="icon-cogs"></i>

                        <span class="settings">

                            <span class="text"><?php echo THEME; ?>:</span>

                            <span class="colors">

                                <span class="color-default" data-style="default"></span>

                                <span class="color-gray" data-style="gray"></span>

                                <span class="color-purple" data-style="purple"></span>

                                <span class="color-navy-blue" data-style="navy-blue"></span>

                            </span>

                        </span>

        </div>

        <!-- END THEME CUSTOMIZER-->





        <?php
        $attspop = array();
        if ($payment_gateway == 1) {


            $attspop = array(


                'width' => '700',


                'height' => '500',


                'scrollbars' => 'yes',


                'status' => 'yes',


                'resizable' => 'yes',


                'screenx' => '350',


                'screeny' => '75',


                'location' => 'no',


                'menubar' => 'no',


                'titlebar' => 'no',


                'toolbar' => 'no',

                'class' => 'btn mini purple fr marB5'


            );

        }



        ?>

        <h3 class="page-title">

            <?php echo SITE_SETTING; ?>

        </h3>

        <ul class="breadcrumb">

            <li>

                <a href="<?php echo site_url('admin/home/dashboard') ?>"><i class="icon-home"></i></a><span
                    class="divider">&nbsp;</span>

            </li>


            <li><a href="#"><?php echo SITE_SETTING; ?></a><span class="divider-last">&nbsp;</span></li>

        </ul>

    </div>

</div>

<!-- END PAGE HEADER-->



<?php

if ($error != '') {


    if ($error == 'success') {
        ?>

        <div class="alert alert-success">

            <button class="close" data-dismiss="alert">x</button>

            <strong><?php echo SUCCESS; ?>!</strong> <?php echo SITE_SETTING_UPDATED_SUCCESSFULLY; ?>.
        </div>



    <?php } else { ?>







        <div class="alert alert-error">

            <button class="close" data-dismiss="alert">x</button>

            <strong><?php echo ERRORS; ?>!</strong> <?php echo $error; ?></div>







    <?php
    }

} ?>





<!-- BEGIN PAGE CONTENT-->

<?php if ($payment_gateway == 1) {
    echo anchor_popup('admin/automation/index', TEST_YOUR_PAYPAL_SETTING, $attspop);
} ?>



<div class="row-fluid">

<div class="span12 sortable">

<!-- BEGIN SAMPLE FORMPORTLET-->

<div class="widget">

<div class="widget-title">

    <h4><i class="icon-reorder"></i><?php echo SITE_SETTING; ?></h4>
                            
                                        <span class="tools">

                                        <a href="javascript:;" class="icon-chevron-down"></a>

                                        <a href="javascript:;" class="icon-remove"></a>

                                        </span>

</div>

<div class="widget-body common_sty">

<!-- BEGIN FORM-->


<form action="<?php echo site_url('admin/site_setting/add_site_setting') ?>" class="form-horizontal"
      name="frm_site_setting" enctype="multipart/form-data" method="post">

<h5><?php echo SITE_SETTING; ?></h5>


<div class="control-group">

    <label class="control-label"><?php echo SITE_VERSION; ?></label>

    <div class="controls">

        <span class="help-inline"><?php echo $site_version; ?></span>

    </div>

</div>


<div class="control-group">

    <label class="control-label"><?php echo SITE_NAME; ?></label>

    <div class="controls">

        <input type="text" name="site_name" id="site_name" class="input-xlarge" value="<?php echo $site_name; ?>"/>

    </div>

</div>


<div class="control-group">

    <label class="control-label"><?php echo GOOGLE_ANALYTICS_CODE; ?></label>

    <div class="controls">

        <input type="text" placeholder="" class="input-xlarge" name="site_tracker" id="site_tracker"
               value="<?php echo $site_tracker; ?>"/>

        <span class="help-inline">(Ex :: UA-000000-0)</span>

    </div>

</div>


<h5><?php echo CAPTCHA_SETTING; ?></h5>

                               <div class="control-group">

                                   <a href="https://www.google.com/recaptcha/admin/create" target="_blank">

                                   <?php echo CREATE_A_RECAPTCHA_KEY_FOR_YOUR_SITE_FROM_HERE_AND_SET_THE_KEY_VALUES_BELOW; ?>.</a>

                                    <label class="control-label"><?php echo PRIVATE_KEY ?> </label>

                                    <div class="controls">

                                        <input type="text" placeholder=""  name="captcha_private_key" id="captcha_private_key"  value="<?php echo $captcha_private_key; ?>" class="input-xlarge" />

                                                                              

                                    </div>

                                </div>

                                

                                

                                 <div class="control-group">                                   

                                    <label class="control-label"><?php echo PUBLIC_KEY; ?></label>

                                    <div class="controls">

                                        <input type="text" placeholder="" name="captcha_public_key" id="captcha_public_key" value="<?php echo $captcha_public_key; ?>" class="input-xlarge" />

                                                                              

                                    </div>

                                </div>

<div class="control-group">

    <label class="control-label"><?php echo SIGN_UP_PAGE; ?> </label>

    <div class="controls">

        <select class="input-large m-wrap" tabindex="1" name="signup_captcha" id="signup_captcha">

            <option value="0" <?php if ($signup_captcha == 0) {
                echo "selected";
            } ?>><?php echo DISABLE; ?></option>


            <option value="1" <?php if ($signup_captcha == 1) {
                echo "selected";
            } ?>><?php echo ENABLE; ?></option>


        </select>

    </div>

</div>

<div class="control-group">

    <label class="control-label"><?php echo CONTACT_US_PAGE; ?> </label>

    <div class="controls">

        <select class="input-large m-wrap" tabindex="1" name="contact_us_captcha" id="contact_us_captcha">

            <option value="0" <?php if ($contact_us_captcha == 0) {
                echo "selected";
            } ?>><?php echo DISABLE; ?></option>


            <option value="1" <?php if ($contact_us_captcha == 1) {
                echo "selected";
            } ?>><?php echo ENABLE; ?></option>


        </select>


    </div>

</div>


<input type="hidden" name="preapproval_enable" id="preapproval_enable" value="1"/>

<h5><?php echo LOGO_SETTING; ?></h5>


<div class="control-group">

    <label class="control-label"><?php echo SITE_LOGO; ?></label>

    <div class="controls">

        <input name="file_up" type="file" class="" id="file_up" value=""/>

        <input type="hidden" name="prev_site_logo" id="prev_site_logo" value="<?php echo $prev_site_logo; ?>"/>

        <?php



        if ($prev_site_logo != '') {
            if (file_exists(base_path() . 'upload/orig/' . $prev_site_logo)) {
                ?>







                <img style="padding: 5px 5px;border-radius: 5px;"

                     src="<?php echo base_url() . 'upload/orig/' . $prev_site_logo; ?>" id="prev_logo"/>


                <img id="logo_prev_main" src="#" alt="Preview goes here" style="display:none;"/>




            <?php
            }
        } ?> <br/>
        <span class="help-inline">(<?php echo NOTES; ?> : <?php echo IMAGE_DIMENSIONS; ?> : 220 X 37 (pixels))</span>
    </div>


</div>

<script>
    function readURL(input) {
        if (input.files && input.files[0]) {

            var reader = new FileReader();

            reader.onload = function (e) {
                $('#logo_prev_main').attr('src', e.target.result);

                var image = new Image();
                image.src = $("#logo_prev_main").attr("src");
                var width = image.naturalWidth;
                var height = image.naturalHeight;

                if (width <= "400" && height <= "100") {
                    $('#logo_prev_main').css('display', 'inline-block');
                    $('#logo_prev_main').css('width', '220px');
                    $('#logo_prev_main').css('height', '37px');
                }
                else {
                    $('#logo_prev_main').css('display', 'none');
                    alert("Please select image with given dimensions for better preview");
                    $('#file_up').val("");
                }

            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#file_up").change(function () {
       // readURL(this);
    });
</script>


<div class="control-group">

    <label class="control-label"><?php echo SITE_LOGO_HOVER ?> </label>

    <div class="controls">

        <input name="file_up2" type="file" class="" id="file_up2" value=""/>


        <input type="hidden" name="prev_site_logo_hover" id="prev_site_logo_hover"
               value="<?php echo $prev_site_logo_hover; ?>"/>



        <?php if ($prev_site_logo_hover != '') {
            if (file_exists(base_path() . 'upload/orig/' . $prev_site_logo_hover)) {
                ?>





                <img id="test" style="padding: 5px 5px;border-radius: 5px;"

                     src="<?php echo base_url() . 'upload/orig/' . $prev_site_logo_hover; ?>"/>


                <img id="logo_prev_hover" src="#" alt="Preview goes here" width="220px" height="37px"
                     style="display:none;"/>




            <?php
            }
        } ?>


        <br/>
        <span class="help-inline">(<?php echo NOTES; ?> : <?php echo IMAGE_DIMENSIONS; ?> : 220 X 37 (pixels))</span>

    </div>


</div>
<script>
    function readURL2(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#logo_prev_hover').attr('src', e.target.result);

                var image = new Image();
                image.src = $("#logo_prev_hover").attr("src");
                var width = image.naturalWidth;
                var height = image.naturalHeight;

                if (width <= "400" && height <= "100") {
                    $('#logo_prev_hover').css('display', 'inline-block');
                    $('#logo_prev_hover').css('width', '220px');
                    $('#logo_prev_hover').css('height', '37px');
                }
                else {
                    $('#logo_prev_hover').css('display', 'none');
                    alert("Please select image with given dimensions for better preview");
                    $('#file_up2').val("");
                }
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#file_up2").change(function () {
        //readURL2(this);
    });
</script>

<div class="control-group">

    <label class="control-label"><?php echo 'Favicon Image' ?> </label>

    <div class="controls">

        <input name="favicon_file" type="file" class="" id="favicon_file" value=""/>


        <input type="hidden" name="favicon_image" id="favicon_image" value="<?php echo $favicon_image; ?>"/>



        <?php

        if ($favicon_image != '') {
            if (file_exists(base_path() . 'upload/orig/' . $favicon_image)) {

                ?>





                <img id="f_image" style="padding: 5px 5px;border-radius: 5px;"

                     src="<?php echo base_url() . 'upload/orig/' . $favicon_image; ?>"/>



            <?php
            }
        } ?>


        <br/>
        <span class="help-inline">(<?php echo NOTES; ?> : <?php echo IMAGE_DIMENSIONS; ?> : 32 X 32 (pixels))</span>

    </div>


</div>


<h5><?php echo LANGUAGE_AND_DATE_TIME_SETTING; ?></h5>


<div class="control-group">

    <label class="control-label"><?php echo SITE_LANGUAGE; ?></label>

    <div class="controls">

        <select class="input-large m-wrap" tabindex="1" name="site_language" id="site_language">

            <?php



            if ($languages) {

                foreach ($languages as $lang) {


                    ?>

                    <option
                        value="<?php echo $lang['language_id']; ?>" <?php if ($lang['language_id'] == $site_language) {
                        echo "selected";
                    } ?>><?php echo $lang['language_name']; ?></option>



                <?php


                }

            }

            ?>


        </select>

    </div>

</div>


<div class="control-group">

    <label class="control-label"><?php echo DATE_FORMAT; ?></label>

    <div class="controls">

        <input type="hidden" name="currency_symbol_side" id="currency_symbol_side"
               value="<?php echo $currency_symbol_side; ?>"/>

        <input type="hidden" name="currency_code" id="currency_code" value="<?php echo $currency_code; ?>"/>

        <select class="input-large m-wrap" tabindex="1" name="date_format" id="date_format">

            <option value='d M,Y' <?php if ($date_format == 'd M,Y') {
                echo 'selected="selected"';
            } ?>>d M,Y
            </option>


            <option value='Y-m-d' <?php if ($date_format == 'Y-m-d') {
                echo 'selected="selected"';
            } ?>>Y-m-d
            </option>


            <option value='m-d-Y' <?php if ($date_format == 'm-d-Y') {
                echo 'selected="selected"';
            } ?>>m-d-Y
            </option>


            <option value='d-m-Y' <?php if ($date_format == 'd-m-Y') {
                echo 'selected="selected"';
            } ?>>d-m-Y
            </option>


            <option value='Y/m/d' <?php if ($date_format == 'Y/m/d') {
                echo 'selected="selected"';
            } ?>>Y/m/d
            </option>


            <option value='m/d/Y' <?php if ($date_format == 'm/d/Y') {
                echo 'selected="selected"';
            } ?>>m/d/Y
            </option>


            <option value='d/m/Y' <?php if ($date_format == 'd/m/Y') {
                echo 'selected="selected"';
            } ?>>d/m/Y
            </option>


        </select>

    </div>

</div>

<div class="control-group">

    <label class="control-label"><?php echo TIME_FORMAT; ?></label>

    <div class="controls">

        <select class="input-large m-wrap" tabindex="1" name="time_format" id="time_format">

            <option value='g:i A' <?php if ($time_format == 'g:i A') {
                echo 'selected="selected"';
            } ?>>12 hours
            </option>


            <option value='H:i' <?php if ($time_format == 'H:i') {
                echo 'selected="selected"';
            } ?>>24 hours
            </option>


        </select>

    </div>

</div>


<div class="control-group">

    <label class="control-label"><?php echo SITE_TIME_ZONE; ?></label>

    <div class="controls">

        <select class="input-large m-wrap" tabindex="1" name="time_zone" id="time_zone">

            <?php



            if ($time_zone_list) {

                foreach ($time_zone_list as $time) {

                    ?>

                    <option value="<?php echo $time['name']; ?>" <?php if ($time['name'] == $time_zone) {
                        echo "selected";
                    } ?>><?php echo $time['name']; ?></option>

                <?php


                }


            }







            ?>


        </select>

    </div>

</div>


<h5><?php echo PROJECT_LIMITS ?></h5>

<div class="control-group">

    <label class="control-label"><?php echo MAXIMUM_NO_PROJECT_PER_YEAR_FOR_ONE_USER; ?> </label>

    <div class="controls">

        <input type="text" placeholder="" class="input-xlarge" name="maximum_project_per_year"
               id="maximum_project_per_year" value="<?php echo $maximum_project_per_year; ?>"/>
        <script>
            $("#maximum_project_per_year").keypress(function (evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    return false;
                }
                return true;
            });
        </script>


    </div>

</div>


<!-- <div class="control-group">

                                    <label class="control-label"><?php //echo MAXIMUM_NO_DONATION_PER_PROJECT ?> </label>

                                    <div class="controls">

                                        <input type="text" placeholder="10" class="input-xlarge" name="maximum_donate_per_project" id="maximum_donate_per_project" value="<?php //echo $maximum_donate_per_project; ?>" />
 										<script>
										$("#maximum_donate_per_project").keypress(function(evt) {
											   evt = (evt) ? evt : window.event;
												var charCode = (evt.which) ? evt.which : evt.keyCode;
												if (charCode > 31 && (charCode < 48 || charCode > 57)) {
													return false;
												}
												return true;
											});
										</script>
                                    </div>

                                </div>-->


<h5><?php echo PROJECT_GOAL_AMOUNT_SETTING ?></h5>


<div class="control-group">

    <label class="control-label"><?php echo MINIMUM_PROJECT_GOAL_AMOUNT; ?>
        (<?php echo $site_setting['currency_symbol']; ?>)</label>

    <div class="controls">

        <input type="text" placeholder="10" class="input-xlarge" name="minimum_goal" id="minimum_goal"
               value="<?php echo $minimum_goal; ?>"/>
        <script>
            $("#minimum_goal").keypress(function (evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    return false;
                }
                return true;
            });
        </script>
    </div>

</div>


<div class="control-group">

    <label class="control-label"><?php echo MAXIMUM_PROJECT_GOAL_AMOUNT; ?>
        (<?php echo $site_setting['currency_symbol']; ?>) </label>

    <div class="controls">

        <input type="text" placeholder="10" class="input-xlarge" value="<?php echo $maximum_goal; ?>"
               name="maximum_goal" id="maximum_goal"/>
        <script>
            $("#maximum_goal").keypress(function (evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    return false;
                }
                return true;
            });
        </script>
    </div>

</div>


<h5><?php echo PROJECT_TARGET_DAYS_SETTING; ?></h5>


<div class="control-group">

    <label class="control-label"><?php echo MINIMUM_DAYS_FOR_PROJECTS; ?> </label>

    <div class="controls">

        <input type="text" name="project_min_days" id="project_min_days" placeholder="1" class="input-xlarge"
               value="<?php echo $project_min_days; ?>"/>
        <script>
            $("#project_min_days").keypress(function (evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    return false;
                }
                return true;
            });
        </script>
    </div>

</div>


<div class="control-group">

    <label class="control-label"><?php echo MAXIMUM_DAYS_FOR_PROJECTS; ?> </label>

    <div class="controls">

        <input type="text" placeholder="60" class="input-xlarge" name="project_max_days" id="project_max_days"
               value="<?php echo $project_max_days; ?>"/>
        <script>
            $("#project_max_days").keypress(function (evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    return false;
                }
                return true;
            });
        </script>
    </div>

</div>


<h5><?php echo DONATION_TYPE_SETTING; ?></h5>


<div class="control-group">

    <label class="control-label"><?php echo PAYMENT_GATEWAY ?></label>

    <div class="controls">

        <select class="input-large m-wrap" tabindex="1" name="payment_gateway" id="payment_gateway">

        
            <option value="5" <?php if ($payment_gateway == 5) {
                echo "selected";
            } ?>><?php echo OFFLINE; ?></option>


            <?php /*?>         <option value="3" <?php if($payment_gateway == 3){ echo "selected"; } ?>>Paypal Credit Card</option>

<?php */
            ?>


        </select>

    </div>

</div>


<div class="control-group">

    <label class="control-label"><?php echo FUNDING_DONATION_TYPE_FOR_PROJECTS; ?> </label>

    <div class="controls">

        <select class="input-large m-wrap" tabindex="1" name="enable_funding_option" id="enable_funding_option"
                onchange="set_enable_funding_type(this.value);">

            <option value="0" <?php if ($enable_funding_option == '0') {
                echo "selected";
            } ?>><?php echo FIXED; ?></option>

            <option value="1" <?php if ($enable_funding_option == '1') {
                echo "selected";
            } ?>><?php echo FLEXIBLE; ?></option>

            <option value="2" <?php if ($enable_funding_option == '2') {
                echo "selected";
            } ?>><?php echo BOTH; ?></option>

        </select>

        <span class="help-inline"><?php echo NOTE_WORK_ONLY_FOR_PREAPPROVAL_IS_ENABLE; ?></span>

    </div>

</div>


<div class="control-group" id="donation_fixed"
     style=" display:<?php if ($enable_funding_option == 0 or $enable_funding_option == 2) {
         echo "block";
     } else {
         echo 'none';
     } ?>;">

    <label class="control-label"><?php echo FIXED_PROJECT_COMMISION; ?> </label>

    <div class="controls">

        <input type="text" name="fixed_fees" id="fixed_fees" placeholder="3" class="input-xlarge"
               value="<?php echo $fixed_fees; ?>"/>
        <script>
            $("#fixed_fees").keypress(function (evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                //alert(charCode)
                if (charCode == 46) {
                    return true;
                }
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    return false;
                }
                return true;
            });
        </script>
    </div>

</div>


<div class="control-group" id="donation_flexible2"
     style=" display:<?php if ($enable_funding_option == 1 or $enable_funding_option == 2) {
         echo "block";
     } else {
         echo 'none';
     } ?>;">

    <label class="control-label"><?php echo FLEXIBLE_SUCCESSFUL_PROJECT_COMMISION; ?> </label>

    <div class="controls">

        <input type="text" placeholder="3" class="input-xlarge" name="suc_flexible_fees" id="suc_flexible_fees"
               value="<?php echo $suc_flexible_fees; ?>"/>
        <script>
            $("#suc_flexible_fees").keypress(function (evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                //alert(charCode)
                if (charCode == 46) {
                    return true;
                }
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    return false;
                }
                return true;
            });
        </script>

    </div>

</div>


<div class="control-group" id="donation_flexible1"
     style=" display:<?php if ($enable_funding_option == 1 or $enable_funding_option == 2) {
         echo "block";
     } else {
         echo 'none';
     } ?>;">

    <label class="control-label"><?php echo FLEXIBLE_UNSUCCESSFUL_PROJECT_COMMISION; ?></label>

    <div class="controls">

        <input type="text" placeholder="10" class="input-xlarge" name="flexible_fees" id="flexible_fees"
               value="<?php echo $flexible_fees; ?>"/>
        <script>
            $("#flexible_fees").keypress(function (evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                //alert(charCode)
                if (charCode == 46) {
                    return true;
                }
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    return false;
                }
                return true;
            });
        </script>
    </div>

</div>


<h5><?php echo PROJECT_ENDING_SOON_SETTING; ?></h5>


<div class="control-group">

    <label class="control-label"><?php echo PROJECT_ENDING_SOON_DAYS; ?></label>

    <div class="controls">

        <input type="text" class="input-xlarge" name="ending_days" id="ending_days"
               value="<?php echo $ending_days; ?>"/>
        <script>
            $("#ending_days").keypress(function (evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    return false;
                }
                return true;
            });
        </script>
    </div>

</div>


<div class="control-group">
    <label class="control-label"><?php echo "Clear Cache"; ?></label>

    <div class="controls">
        <button type="button" class="btn blue" onclick="delete_cash();"><?php echo "Clear Cache"; ?></button>
    </div>
</div>
<script type="text/javascript">
    function delete_cash() {
        window.location.href = "<?php echo site_url('admin/site_setting/delete_cash'); ?>";
    }
</script>


<div class="form-actions">
    <input type="hidden" name="site_setting_id" id="site_setting_id" value="<?php echo $site_setting_id; ?>"/>


    <button type="submit" class="btn blue"><i class="icon-ok"></i> <?php echo SUBMIT; ?></button>

    <!-- <button type="button" class="btn"><i class=" icon-remove"></i> Cancel</button> -->

</div>

</form>

<!-- END FORM-->

</div>

</div>

<!-- END SAMPLE FORM PORTLET-->

</div>

</div>


<!-- END PAGE CONTENT-->

</div>

<!-- END PAGE CONTAINER-->

</div>

<!-- END PAGE -->

</div>

<!-- END CONTAINER -->

<!-- BEGIN JAVASCRIPTS -->

<!-- Load javascripts at bottom, this will reduce page load time -->


<!-- ie8 fixes -->

<!--[if lt IE 9]>

<script src="js/excanvas.js"></script>

<script src="js/respond.js"></script>

<![endif]-->

<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/bootstrap-datepicker/css/datepicker.css"/>
<script type="text/javascript"
        src="<?php echo base_url() ?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

<script type="text/javascript"
        src="<?php echo base_url(); ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>

<script src="<?php echo base_url(); ?>js/scripts.js"></script>

<script>

    jQuery(document).ready(function () {

        // initiate layout and plugins

        App.init();

        $('.date-picker').datepicker();

    });

    function set_enable_funding_type(val) {

        // alert(val)

        if (val == '0' || val == '2') {

            document.getElementById('donation_fixed').style.display = 'block';

        } else {

            document.getElementById('donation_fixed').style.display = 'none';

        }


        if (val == '1' || val == '2') {

            document.getElementById('donation_flexible1').style.display = 'block';

            document.getElementById('donation_flexible2').style.display = 'block';

        } else {

            document.getElementById('donation_flexible1').style.display = 'none';

            document.getElementById('donation_flexible2').style.display = 'none';

        }

    }

</script>

<!-- END JAVASCRIPTS -->
