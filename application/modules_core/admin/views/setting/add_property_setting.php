<div id="container" class="row-fluid">
<!-- BEGIN SIDEBAR -->
<?php echo $this->load->view('admin_sidebar'); ?>
<!-- END SIDEBAR -->
<!-- BEGIN PAGE -->
<div id="main-content">
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN THEME CUSTOMIZER-->
        <div id="theme-change" class="hidden-phone">
            <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text"><?php echo THEME; ?>:</span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
        </div>
        <!-- END THEME CUSTOMIZER-->

        <h3 class="page-title">
            <?php echo 'Property Setting'; ?>
        </h3>
        <ul class="breadcrumb">
            <li>
                <a href="<?php echo site_url('admin/home/dashboard') ?>"><i class="icon-home"></i></a><span
                    class="divider">&nbsp;</span>
            </li>

            <li><a href="#"><?php echo 'Property Setting'; ?></a><span class="divider-last">&nbsp;</span></li>
        </ul>
    </div>
</div>
<!-- END PAGE HEADER-->
<?php if ($error != '') {

    if ($error == 'success') {
        ?>
        <div class="alert alert-success">
            <button class="close" data-dismiss="alert">x</button>
            <strong><?php echo SUCCESS; ?>!</strong> <?php echo 'Property setting updated successfully'; ?>.
        </div>

    <?php } else { ?>



        <div class="alert alert-error">
            <button class="close" data-dismiss="alert">x</button>
            <strong><?php echo ERRORS; ?>!</strong> <?php echo $error; ?></div>



    <?php
    }
} ?>
<!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
<div class="span12 sortable">
<!-- BEGIN SAMPLE FORMPORTLET-->
<div class="widget">
<div class="widget-title">
    <h4><i class="icon-reorder"></i><?php echo 'Property Setting'; ?></h4>
                                        <span class="tools">
                                        <a href="javascript:;" class="icon-chevron-down"></a>
                                        <a href="javascript:;" class="icon-remove"></a>
                                        </span>
</div>
<div class="widget-body common_sty">
       <!-- BEGIN FORM-->
    <form action="<?php echo site_url('admin/site_setting/add_property'); ?>" class="form-horizontal"
          name="frm_meta_setting" method="post">
   
 

        <div class="control-group">
            <label class="control-label"><?php echo 'Client Id'; ?> </label>

            <div class="controls">
                <input type="text" placeholder="Client Id" class="input-xlarge" name="client_id"
                       id="client_id" value="<?php echo $client_id; ?>"/>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label"><?php echo 'Client Secret'; ?> </label>

            <div class="controls">
                <input type="text" placeholder="Client Secret" class="input-xlarge" name="client_secret"
                       id="client_secret" value="<?php echo $client_secret; ?>"/>
            </div>
        </div>

      <!--   <div class="control-group">
            <label class="control-label"><?php echo 'Access Token'; ?> </label>

            <div class="controls">
                <input type="text" placeholder="Access Token" class="input-xlarge" name="access_token"
                       id="access_token" value="<?php echo $access_token; ?>"/>
            </div>
        </div> -->


        
       
 
        <input type="hidden" name="property_setting_id" id="property_setting_id"
               value="<?php echo $property_setting_id; ?>"/>

        <div class="form-actions">
            <button type="submit" class="btn blue"><i class="icon-ok"></i> <?php echo UPDATES; ?></button>
            <!-- <button type="button" class="btn"><i class=" icon-remove"></i> Cancel</button> -->
        </div>
    </form>
    <!-- END FORM-->
</div>
</div>
<!-- END SAMPLE FORM PORTLET-->
</div>
</div>

<!-- END PAGE CONTENT-->
</div>
<!-- END PAGE CONTAINER-->
</div>
<!-- END PAGE -->
</div>
<script type="text/javascript"
        src="<?php echo base_url(); ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        App.init();
    });
function changelang(obj)
{
	var id =obj.value;
	location.href="<?php echo site_url('admin/site_setting/add_taxonomy'); ?>/"+id
	
}
</script>
