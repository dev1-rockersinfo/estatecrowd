<!-- BEGIN CONTAINER -->
<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php echo $this->load->view('admin_sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN THEME CUSTOMIZER-->
                    <div id="theme-change" class="hidden-phone">
                        <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text">Theme:</span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
                    </div>
                    <!-- END THEME CUSTOMIZER-->
                    <h3 class="page-title">
                        <?php echo FUNDS; ?>
                    </h3>
                    <ul class="breadcrumb">
                        <li>
                            <a href="<?php echo site_url('admin/home/dashboard') ?>"><i class="icon-home"></i></a><span
                                class="divider">&nbsp;</span>
                        </li>

                        <li>
                            <a href="<?php echo site_url('admin/payments_gateways/list_payments_gateway') ?>"><?php echo FUNDS; ?></a><span
                                class="divider-last">&nbsp;</span></li>
                    </ul>
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <?php if ($error != "") {
                ?>
                <div class="alert alert-error">
                    <button class="close" data-dismiss="alert">x</button>
                    <strong><?php echo ERRORS; ?>!</strong> <?php echo $error; ?></div>
            <?php
            }
            ?>
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                <div class="span12 sortable">
                    <!-- BEGIN SAMPLE FORMPORTLET-->
                    <div class="widget">
                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i> <?php echo FUNDS; ?></h4>
                                        <span class="tools">
                                        <a href="javascript:;" class="icon-chevron-down"></a>
                                        <a href="javascript:;" class="icon-remove"></a>
                                        </span>
                        </div>
                        <div class="widget-body">
                            <ul class="nav nav-tabs">

                                <?php   $check_wallet_setting = get_rights('add_wallet_setting');
                                $check_payment_gateway = get_rights('list_payment_gateway');
                                $check_wallet_review = get_rights('list_wallet_review');
                                $check_wallet_withdraw = get_rights('list_wallet_withdraw');
                                $check_gateway_detail = get_rights('list_gateway_detail');
                                $set_fund = get_rights('set_fund');

                                if ($check_wallet_setting == 1) {
                                    ?>

                                    <li><a href="#portlet_tab1" data-toggle="tab"><span
                                                style="cursor:pointer;"><?php echo anchor('admin/wallet_setting/add_wallet_setting', 'Wallet Setting', 'id="sp_1" style="color:#364852;background:#ececec;"'); ?></span></a>
                                    </li>

                                <?php }
                                if ($check_gateway_detail == 1 || $check_payment_gateway == 1) { ?>
                                    <li><a href="#portlet_tab2" data-toggle="tab"><span
                                                style="cursor:pointer;"><?php echo anchor('admin/payments_gateways/list_payment_gateway', 'Payment Gateways', 'id="sp_2" style="color:#364852;background:#ececec;"'); ?></span></a>
                                    </li>
                                <?php }
                                if ($check_wallet_review == 1) { ?>
                                    <li><a href="#portlet_tab3" data-toggle="tab"><span
                                                style="cursor:pointer;"><?php echo anchor('admin/wallet/list_wallet_review', 'Wallet Review', 'id="sp_3" style="color:#364852;background:#ececec;"'); ?></span></a>
                                    </li>
                                <?php }
                                if ($check_wallet_withdraw == 1) { ?>
                                    <li><a href="#portlet_tab4" data-toggle="tab"><span
                                                style="cursor:pointer;"><?php echo anchor('admin/wallet/list_wallet_withdraw', 'Withdraw Request', 'id="sp_4" style="color:#364852;background:#ececec;"'); ?></span></a>
                                    </li>


                                <?php }
                                if ($set_fund == 1) { ?>
                                    <li><a href="#portlet_tab5" data-toggle="tab"><span
                                                style="cursor:pointer;"><?php echo anchor('admin/set_fund/serch_user', 'Fund', 'id="sp_5" style="color:#364852;background:#ececec;"'); ?></span></a>
                                    </li>


                                <?php } ?>

                            </ul>
                            <!-- BEGIN FORM-->
                            <?php
                            $attributes = array('name' => 'frm_add_fund');
                            echo form_open('admin/set_fund/add_fund/' . $userid, $attributes);
                            ?>
                            <div class="control-group">
                                <label class="control-label"><?php echo AVAILABLE_BALANCE; ?>
                                    (<?php echo $site_setting['currency_symbol']; ?>)</label><!-- change by darshan -->
                                <div class="controls">
                                    <input type="text" name="balance" class="input-xlarge" id="balance"
                                           value="<?php echo $amount; ?>" readonly="readonly"/>
                                </div>
                            </div>


                            <div class="control-group">
                                <label class="control-label"><?php echo ADD_FUND; ?>
                                    (<?php echo $site_setting['currency_symbol']; ?>)</label><!-- change by darshan -->
                                <div class="controls">
                                    <input type="text" name="add_fund" class="input-xlarge" id="add_fund" value=""/>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label"><?php echo REASON; ?></label><!-- change by darshan -->
                                <div class="controls">
                                    <input type="text" name="reason" class="input-xlarge" id="reason"/>
                                </div>
                            </div>


                            <input type="hidden" name="userid" id="userid" value="<?php echo $userid; ?>"/>


                            <div class="form-actions">
                                <?php
                                if ($userid == "") {
                                    ?>
                                    <button type="submit" class="btn blue" name="submit" value="Submit" onclick=""><i
                                            class="icon-ok"></i> <?php echo SUBMIT; ?> </button>
                                <?php
                                } else {
                                    ?>

                                    <button type="submit" class="btn" name="submit" value="Update" onclick=""><i
                                            class="icon-ok"></i> <?php echo UPDATE; ?></button>
                                <?php
                                }
                                ?>
                                <button type="button" class="btn" name="cancel" value="Cancel"
                                        onClick="location.href='<?php echo site_url('admin/payments_gateways/list_payment_gateway'); ?>'">
                                    <i class=" icon-remove"> </i> <?php echo CANCEL; ?></button>
                            </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>

            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>
<!-- END CONTAINER -->

<!-- BEGIN JAVASCRIPTS -->
<!-- Load javascripts at bottom, this will reduce page load time -->

<script type="text/javascript"
        src="<?php echo base_url(); ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        App.init();
    });
</script>
<!-- END JAVASCRIPTS -->



































