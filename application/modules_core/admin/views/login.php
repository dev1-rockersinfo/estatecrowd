<?php $site_setting = site_setting();
$site_logo = $site_setting['site_logo'];
$site_logo_hover = $site_setting['site_logo_hover'];?>

<style>
    #logo {
        background-image: url(<?php echo base_url();?>upload/orig/<?php echo $site_logo;?>);

    }

    #logo:hover {
        background-image: url(<?php echo base_url();?>upload/orig/<?php echo $site_logo_hover;?>);
    }

</style>
<div class="login-header">
    <!-- BEGIN LOGO -->
    <div id="logo" class="center">
        <!-- <img src="<?php echo base_url(); ?>upload/orig/<?php echo $site_logo; ?>" alt="Admin" /> -->
    </div>
    <!-- END LOGO -->
</div>

<!-- BEGIN LOGIN -->
<div id="login">
    <!-- BEGIN LOGIN FORM -->
    <form id="loginform" class="form-vertical no-padding no-margin"
          action="<?php echo site_url('admin/home/check_login'); ?>" method="post"
        <?php if ($error != ''){ ?> style="display: none"
    ; <?php } else { ?> style="display: block;" <?php } ?>>

    <div class="control-wrap">
        <h4 class="loginHead"><?php echo ADMIN_LOGIN; ?></h4>
        <?php
        if ($login == '0') {
            echo "<span class='login-error' >" . INVALID_USERNAME_OR_PASSWORD . ".</span>";
        }
        if ($login == '1') {
            echo "<span class='login-success'>" . YOU_HAVE_LOGGED_OUT_SUCCESSFULLY . ".</span>";
        }
        $admin_username = '';
        $admin_password = '';
        $admin_remember = '';
        if (isset($_COOKIE['admin_user'])) {
            $admin_user_data = explode(' ', $_COOKIE['admin_user']);


            $admin_username = $admin_user_data[0];
            $admin_password = $admin_user_data[1];
            $admin_remember = $admin_user_data[2];


        }

        ?>
        <div class="control-group">
            <div class="controls">
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-user"></i></span><input id="input-username" type="text"
                                                                                placeholder="<?php echo USERNAME; ?>"
                                                                                name="username"
                                                                                value="<?php if ($admin_username != '') {
                                                                                    echo $admin_username;
                                                                                } ?>"/>
                </div>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-key"></i></span><input id="input-password" type="password"
                                                                               name="password"
                                                                               placeholder="<?php echo PASSWORD; ?>"
                                                                               value="<?php if ($admin_password != '') {
                                                                                   echo $admin_password;
                                                                               } ?>"/>
                </div>
                <div class="mtop10">
                    <div class="block-hint pull-left">
                        <input type="checkbox" id="remember" name="remember"
                            <?php if ($admin_remember != '') {
                                echo "checked=checked";
                            } ?>
                            > <?php echo REMEMBER_ME; ?>
                    </div>
                    <div class="block-hint pull-right">
                        <a href="javascript:;" class="" id="forget-password"><?php echo FORGOT_PASSWORD; ?>?</a>
                    </div>
                </div>

                <div class="clearfix space5"></div>
            </div>

        </div>
    </div>
    <input type="submit" id="login-btn" class="btn btn-block login-btn" value="<?php echo LOGIN; ?>"/>
    </form>
    <!-- END LOGIN FORM -->
    <!-- BEGIN FORGOT PASSWORD FORM -->
    <form id="forgotform" class="form-vertical no-padding no-margin hide"
          action="<?php echo site_url('admin/home/forgot_password'); ?>" method="post" <?php
        if ($error != ''){
        ?> style="display: block"
    ; <?php } else { ?> style="display: none;" <?php } ?>>
    <p class="center"><?php echo ENTER_YOUR_EMAIL_ADDRESS_BELOW_TO_RESET_YOUR_PASSWORD; ?></p>
    <?php

    if ($error != '') {

        if ($error == 'success') {
            echo "<span style='color:green;'>" . PASSWORD_IS_SEND_TO_YOUR_EMAIL_ADDRESS . ".</span>";
        } elseif ($error == 'email_not_found') {
            echo "<span style='color:green;'>" . USER_EMAIL_ADDRESS_IS_NOT_FOUND . ".</span>";
        } elseif ($error == 'record_not_found') {
            echo "<span style='color:red;'>" . USER_RECORD_IS_NOT_FOUND . ".</span>";
        } else {
            echo "<span style='color:red;'>" . $error . "</span>";
        }


    }
    ?>
    <div class="control-group">
        <div class="controls">
            <div class="input-prepend">
                <span class="add-on"><i class="icon-envelope"></i></span><input id="input-email" type="text"
                                                                                placeholder="Email" name="email"/>
            </div>
        </div>
        <div class="block-hint pull-right">
            <a href="javascript:;" class="" id="forget-btn"><?php echo BACK_TO_LOGIN; ?></a>
        </div>
        <div class="space20"></div>
    </div>
    <input type="Submit" class="btn btn-block login-btn" value="<?php echo SUBMIT; ?>"/>
    </form>
    <!-- END FORGOT PASSWORD FORM -->
</div>
<!-- END LOGIN -->
<!-- BEGIN COPYRIGHT -->
  <div id="login-copyright">
  <a href="http://www.fundraisingscript.com" title="Crowdfunding Software" target="_blank" style="color:#828C92; text-decoration:none;">Crowdfunding Software</a> <span> Powered by </span><a href="http://www.fundraisingscript.com" title="Crowdfunding Software" target="_blank" style="color:#828C92; text-decoration:none;">Fundraisingscript.com</a>. All rights reserved.
  </div>
<!-- END COPYRIGHT -->
<!-- BEGIN JAVASCRIPTS -->
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<script>
    jQuery(document).ready(function () {
        App.initLogin();
    });
</script>
<!-- END JAVASCRIPTS -->

