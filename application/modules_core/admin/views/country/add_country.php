<div id="container" class="row-fluid">
    <!-- BEGIN SIDEBAR -->
    <?php echo $this->load->view('admin_sidebar'); ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div id="main-content">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN THEME CUSTOMIZER-->
                    <div id="theme-change" class="hidden-phone">
                        <i class="icon-cogs"></i>
                        <span class="settings">
                            <span class="text"><?php echo THEME; ?></span>
                            <span class="colors">
                                <span class="color-default" data-style="default"></span>
                                <span class="color-gray" data-style="gray"></span>
                                <span class="color-purple" data-style="purple"></span>
                                <span class="color-navy-blue" data-style="navy-blue"></span>
                            </span>
                        </span>
                    </div>
                    <!-- END THEME CUSTOMIZER-->
                    <h3 class="page-title">
                        <?php echo COUNTRY; ?>
                    </h3>
                    <ul class="breadcrumb">
                        <li>
                            <a href="<?php echo site_url('admin/home/dashboard') ?>"><i class="icon-home"></i></a><span
                                class="divider">&nbsp;</span>
                        </li>

                        <li>
                            <a href="<?php echo site_url('admin/country/list_country') ?>"><?php echo COUNTRY; ?></a><span
                                class="divider-last">&nbsp;</span></li>
                    </ul>
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <?php
            if ($error != "") {
                ?>
                <div class="alert alert-error">
                    <button class="close" data-dismiss="alert">x</button>
                    <strong><?php echo ERRORS; ?>!</strong> <?php echo $error; ?></div>
            <?php
            }
            ?>
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                <?php if ($error != "" && $error == 'insert') {
                    ?>
                    <div class="alert alert-success">
                        <button class="close" data-dismiss="alert">x</button>
                        <strong><?php echo SUCCESS; ?>!</strong> <?php echo RECORD_HAS_BEEN_ADDED_SUCCESSFULLY; ?>.
                    </div>
                <?php
                }
                ?>
                <div class="span12 sortable">
                    <!-- BEGIN SAMPLE FORMPORTLET-->
                    <div class="widget">

                        <div class="widget-title">
                            <h4><i class="icon-reorder"></i> <?php echo COUNTRY; ?></h4>
                                        <span class="tools">
                                        <a href="javascript:;" class="icon-chevron-down"></a>
                                        <a href="javascript:;" class="icon-remove"></a>
                                        </span>
                        </div>
                        <div class="widget-body">
                            <!-- BEGIN FORM-->
                            <?php
                            $attributes = array('name' => 'frm_country', 'class' => 'form-horizontal');
                            echo form_open('admin/country/add_country', $attributes);
                            ?>

                            <div class="control-group">
                                <label class="control-label"><?php echo COUNTRY_NAME; ?></label>

                                <div class="controls">
                                    <input type="text" placeholder="Country Name" class="input-xlarge"
                                           name="country_name" id="country_name" value="<?php echo $country_name; ?>"/>

                                </div>
                            </div>


                            <div class="control-group">
                                <label class="control-label"><?php echo STATUS; ?></label>

                                <div class="controls">
                                    <select name="active" class="input-large m-wrap" tabindex="1" id="active">
                                        <option value="0" <?php if ($active == '0') {
                                            echo "selected";
                                        } ?>><?php echo INACTIVE; ?></option>
                                        <option value="1" <?php if ($active == '1') {
                                            echo "selected";
                                        } ?>><?php echo ACTIVE; ?></option>
                                    </select>

                                </div>
                            </div>
                            <input type="hidden" name="country_id" id="country_id" value="<?php echo $country_id; ?>"/>


                            <div class="form-actions">
                                <?php
                                if ($country_id == "") {
                                    ?>

                                    <button type="submit" class="btn blue" name="submit" value="Submit" onclick=""><i
                                            class="icon-ok"></i> <?php echo SUBMIT; ?></button>
                                <?php
                                } else {
                                    ?>
                                    <button type="submit" name="submit" class="btn blue" value="Update" onclick=""><i
                                            class="icon-ok"></i> <?php echo UPDATE; ?></button>
                                <?php
                                }
                                ?>

                                <button type="button" class="btn" name="cancel" value="Cancel"
                                        onClick="location.href='<?php echo site_url('admin/country/list_country'); ?>'">
                                    <i class=" icon-remove"></i> <?php echo CANCEL; ?></button>
                            </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                    <!-- END SAMPLE FORM PORTLET-->
                </div>
            </div>

            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->

<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS -->
<!-- Load javascripts at bottom, this will reduce page load time -->

<script type="text/javascript"
        src="<?php echo base_url(); ?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/uniform/jquery.uniform.min.js"></script>
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<script>
    jQuery(document).ready(function () {
        // initiate layout and plugins
        App.init();
    });
</script>
 
