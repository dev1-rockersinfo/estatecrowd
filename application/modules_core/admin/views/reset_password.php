<?php $site_setting = site_setting();
$site_logo = $site_setting['site_logo'];
$site_logo_hover = $site_setting['site_logo_hover'];?>

<style>
    #logo {
        background-image: url(<?php echo base_url();?>upload/orig/<?php echo $site_logo;?>);

        padding: 18px 35.5px;
        width: 144px;
        background-repeat: no-repeat;
    }

    #logo:hover {
        background-image: url(<?php echo base_url();?>upload/orig/<?php echo $site_logo_hover;?>);

        padding: 18px 35.5px;
        width: 144px;
        background-repeat: no-repeat;
    }

</style>

<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/additional-methods.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.maskedinput.js"></script>
<script>
    // validations
    jQuery(function ($) {
        // validate signup form on keyup and submit
        var validator = $("#reset_frm").validate({
            rules: {
                password: {
                    required: true,
                    minlength: 5,
                    maxlength: 20
                },
                cpassword: {
                    required: true,
                    minlength: 5,
                    maxlength: 20,
                    equalTo: "#password"
                }
            },
            messages: {
                password: {
                    required: '<?php echo PASSWORD_IS_REQUIRED;?>',
                    minlength: '<?php echo ENTER_ATLEAST_FIVE_CHARACTER;?>',
                    maxlength: '<?php echo YOU_CAN_NOT_ENTER_MORE_THAN_TWE_CHARACTER;?>'
                },
                cpassword: {
                    required: '<?php echo RETYPE_PASSWORD_IS_REQUIRED; ?>',
                    minlength: '<?php echo ENTER_ATLEAST_FIVE_CHARACTER;?>',
                    maxlength: '<?php echo YOU_CAN_NOT_ENTER_MORE_THAN_TWE_CHARACTER;?>',
                    equalTo: '<?php echo PASSWORD_DOES_NOT_MATCH; ?>'
                }
            },
            // the errorPlacement has to take the table layout into account
            errorPlacement: function (error, element) {
                if (element.is(":radio"))
                    error.insertAfter(element.parent());
                else if (element.is(":checkbox"))
                    error.insertAfter(element.next());
                else if (element.hasClass('book_job_now_price_input')) {
                    error.insertAfter(element.next().next());
                }
                else {
                    if (element.attr('name') == 'job_price') {
                        error.insertAfter(element.next());
                    }
                    else
                        error.insertAfter(element);
                }
            },
            // specifying a submitHandler prevents the default submit, good for the demo
            /*submitHandler: function() {
             alert("submitted!");
             },*/
            // set this class to error-labels to indicate valid fields
            success: function (label) {
                // set &nbsp; as text for IE
                /*	label.html("&nbsp;").addClass("checked");*/
            },
            /*highlight: function(element, errorClass) {
             $(element).parent().next().find("." + errorClass).removeClass("checked");
             },*/
            onkeyup: function (element) {
                $(element).valid();
            },
            onfocusout: function (element) {
                $(element).valid();
            },
        });
    });
</script>

<div class="login-header">
    <!-- BEGIN LOGO -->
    <div id="logo" class="center">
        <!-- <img src="<?php echo base_url(); ?>upload/orig/<?php echo $site_logo; ?>" alt="Admin" /> -->
    </div>
    <!-- END LOGO -->
</div>

<!-- BEGIN LOGIN -->
<div id="login">
    <!-- BEGIN LOGIN FORM -->

    <?php $att = array('id' => 'reset_frm', 'name' => 'reset_frm', 'class' => 'form-vertical no-padding no-margin');
    echo form_open('admin/home/reset_password/' . $forgot_unique_code, $att);
    ?>


    <div class="lock">
        <i class="icon-lock"></i>
    </div>

    <div class="control-wrap">
        <h4><?php echo 'Reset password'; ?></h4>
        <?php
        if ($error == '0') {
            echo "<span style='color:red;'>" . INVALID_USERNAME_OR_PASSWORD . ".</span>";
        }
        if ($error == '1') {
            echo "<span style='color:green;'>" . YOU_HAVE_LOGGED_OUT_SUCCESSFULLY . ".</span>";
        }


        ?>
        <div class="control-group">
            <div class="controls">
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-user"></i></span><input id="password" type="password"
                                                                                placeholder="Password" name="password"
                                                                                value=""/>
                </div>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-key"></i></span><input id="cpassword" type="password"
                                                                               name="cpassword"
                                                                               placeholder="Confirm password" value=""/>
                </div>


                <div class="clearfix space5"></div>
            </div>

        </div>
    </div>
    <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id; ?>">
    <input type="hidden" name="forget_unique_code" id="forget_unique_code" value="<?php echo $forgot_unique_code; ?>">
    <input type="submit" id="login-btn" class="btn btn-block login-btn" value="Reset"/>
    </form>
    <!-- END LOGIN FORM -->

</div>
<!-- END LOGIN -->
<!-- BEGIN COPYRIGHT -->
  <div id="login-copyright">
  <a href="http://www.fundraisingscript.com" title="Crowdfunding Software" target="_blank" style="color:#828C92; text-decoration:none;">Crowdfunding Software</a> <span> Powered by </span><a href="http://www.fundraisingscript.com" title="Crowdfunding Software" target="_blank" style="color:#828C92; text-decoration:none;">Fundraisingscript.com</a>. All rights reserved.
  </div>
<!-- END COPYRIGHT -->
<!-- BEGIN JAVASCRIPTS -->
<script src="<?php echo base_url(); ?>js/scripts.js"></script>
<script>
    jQuery(document).ready(function () {
        App.initLogin();
    });
</script>
<!-- END JAVASCRIPTS -->
<style>
    .error {
        color: #F30E0E;
        font-size: 12px;
    }
</style>
