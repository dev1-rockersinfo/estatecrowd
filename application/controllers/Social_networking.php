<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 * The Home class is basically the default controller for the script.
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Social_networking extends ROCKERS_Controller

{
    function __construct()
    {
        parent::__construct();
        $this->load->model('home_model');

        $this->load->model('follower_model');
        $this->load->model('following_model');
        $this->load->model('transaction_model');
        $this->load->model('account_model');
        $this->load->library('securimage');
    }

    /*
    Function name :index()
    Parameter :msg
    Return : none
    Use : Show current data of social networking
    */
    function index($msg = '')
    {
        $user_id = check_user_authentication(true);
        $meta = meta_setting();
        $data['site_setting'] = site_setting();
        $result = UserData($this->session->userdata('user_id'));

        $data['facebook_url'] = $result[0]['facebook_url'];

        $data['twitter_url'] = $result[0]['twitter_url'];

        $data['user_website'] = $result[0]['user_website'];
        $data['linkedln_url'] = $result[0]['linkedln_url'];
        $data['googleplus_url'] = $result[0]['googleplus_url'];
        $data['bandcamp_url'] = $result[0]['bandcamp_url'];
        $data['youtube_url'] = $result[0]['youtube_url'];
        $data['myspace_url'] = $result[0]['myspace_url'];
        $data['user_id'] = $result[0]['user_id'];
        $data['error'] = '';
        if ($msg == 1) {
            $data['success'] = "<p>" . SOCIAL_DETAIL_UPDATED_SUCCESSFULLY . "</p>";
        } else {
            $data['success'] = '';
        }

        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/social_networking/index', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
    }

    /*
    Function name :validate_social()
    Parameter :msg
    Return : none
    Use : Validate data of social networking and save it
    */
    function validate_social()
    {
        if ($this->session->userdata('user_id') == '') {
            redirect('home/login');
        }
        $meta = meta_setting();
        $data['site_setting'] = site_setting();
        $this->load->library('form_validation');
        $this->form_validation->set_rules('user_website', WEBSITE_URL, 'valid_url');

        $facebook_link = $this->input->post('facebook_url');
        $twitter_link = $this->input->post('twitter_url');
        $linkedln_url = $this->input->post('linkedln_url');
        $googleplus_url = $this->input->post('googleplus_url');
        $bandcamp_url = $this->input->post('bandcamp_url');
        $youtube_url = $this->input->post('youtube_url');
        $myspace_url = $this->input->post('myspace_url');
        $fb_error = '';
        $tw_error = '';
        $lin_error = '';
        $googleplus_error = '';
        $bandcamp_error = '';
        $youtube_error = '';
        $myspace_error = '';
        if ($facebook_link !== '') {
            if (!substr_count($facebook_link, 'facebook.com')) {
                $fb_error = INVALID_FACEBOOK_LINK . '<br/>';
            }
        }
        if ($twitter_link != '') {
            if (!substr_count($twitter_link, 'twitter.com')) {
                $tw_error = INVALID_TWITTER_LINK . '<br/>';
            }
        }
        if ($linkedln_url != '') {
            if (!substr_count($linkedln_url, 'linkedin.com')) {
                $lin_error = INVALID_LINKEDIN_LINK . '<br/>';
            }
        }
        if ($googleplus_url != '') {
            if (!substr_count($googleplus_url, 'plus.google.com')) {
                $googleplus_error = 'Invalid Goole Plus Link<br/>';
            }
        }
        if ($bandcamp_url != '') {
            if (!substr_count($bandcamp_url, 'basecamp.com')) {
                $bandcamp_error = INVALID_BANDCAMP_LINK . '<br/>';
            }
        }
        if ($youtube_url != '') {
            if (!substr_count($youtube_url, 'youtube.com')) {
                $youtube_error = INVALID_YOUTUBE_LINK . '<br/>';
            }
        }
        if ($myspace_url != '') {
            if (!substr_count($myspace_url, 'myspace.com')) {
                $myspace_error = INVALID_MYSPACE_LINK . '<br/>';
            }
        }
        if ($this->form_validation->run() == FALSE || $fb_error != '' || $tw_error != '' || $lin_error != '' || $googleplus_error != '' || $bandcamp_error != '' || $youtube_error != '' || $myspace_error != '') {
            $data["facebook_url"] = $this->input->post('facebook_url');
            $data["twitter_url"] = $this->input->post('twitter_url');
            $data["user_website"] = $this->input->post('user_website');
            $data["linkedln_url"] = $this->input->post('linkedln_url');
            $data["googleplus_url"] = $this->input->post('googleplus_url');
            $data["bandcamp_url"] = $this->input->post('bandcamp_url');
            $data["youtube_url"] = $this->input->post('youtube_url');
            $data["myspace_url"] = $this->input->post('myspace_url');
            $data['user_id'] = $this->input->post('user_id');
            $data["error"] = validation_errors() . $fb_error . $tw_error . $lin_error . $googleplus_error . $bandcamp_error . $youtube_error . $myspace_error;
            $data["success"] = '';
            $this->template->write('meta_title', $meta['title'], TRUE);
            $this->template->write('meta_description', $meta['meta_description'], TRUE);
            $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
            $this->template->write_view('header', 'default/common/header', $data, TRUE);
            $this->template->write_view('main_content', 'default/social_networking/index', $data, TRUE);
            $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
            $this->template->render();
        } else {
            $data_update = array(
                'user_website' => addhttp($this->input->post('user_website')),
                'facebook_url' => addhttp($this->input->post('facebook_url')),
                'twitter_url' => addhttp($this->input->post('twitter_url')),
                'linkedln_url' => addhttp($this->input->post('linkedln_url')),
                'googleplus_url' => addhttp($this->input->post('googleplus_url')),
                'bandcamp_url' => addhttp($this->input->post('bandcamp_url')),
                'youtube_url' => addhttp($this->input->post('youtube_url')),
                'myspace_url' => addhttp($this->input->post('myspace_url')),
            );
            $this->account_model->UpdateAccount('user_id', SecurePostData($this->input->post('user_id')), 'user', $data_update);
            $cache_file_name = 'user_detail' . $this->session->userdata('user_id');
            setting_deletecache($cache_file_name);
            $cache_file_name = 'user_detail_notification' . $this->session->userdata('user_id');
            setting_deletecache($cache_file_name);

            redirect('social_networking/index/1');
        }
    }
}

?>
