<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 * The Home class is basically the default controller for the script.
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Accreditation extends ROCKERS_Controller
{

    /*
        Load by default when controller call is made
        args: none
    */

    /**
     *
     */
    function __construct()
    {
        parent::__construct();
        $this->load->model('home_model');
        $this->load->library('securimage');
        $this->load->library('pagination');
        $this->load->model('accreditation_model');
    }


   
    /*
     Function name :personal()
     Parameter :none
     Return : none
     Use :  edit/add personal information controller

     */
    public function personal()
    {
        

        $user_id = check_user_authentication(true);
        $data = array();
        $data['user_id'] = $user_id;
        $data['user_data'] = UserData($user_id);
        $user_data = $data['user_data'];

        $data['passport_number'] =  $data['dob'] =  $data['license_number'] =  $data['residency_number'] =  $data['verify_user'] = $data['user_select_type'] = $data['FIRB_number'] = $data['accre_id'] = '';

        $accreditation_data = $this->accreditation_model->getTableData('accreditation', array('user_id' => $user_id));

        $redirect = true;
            
        if ($accreditation_data) 
        {
            if ($accreditation_data[0]['accreditation_status'] != '0') {
                /* set one time message */
                $redirect = false;
            }
        }
        if ($redirect == false) redirect('accreditation/status');

        if ($accreditation_data) 
        {


            $data['accre_id'] = $accreditation_data[0]['id'];

            $data['username'] = $accreditation_data[0]['first_name'].' '.$accreditation_data[0]['last_name'];

            $data['address'] = $accreditation_data[0]['address'];

            $data['mobile_number'] = $accreditation_data[0]['phone'];

            $data['passport_number'] = $accreditation_data[0]['passport_number'];

            $data['dob'] = $accreditation_data[0]['dob'];

            $data['license_number'] = $accreditation_data[0]['license_number'];

            $data['residency_number'] = $accreditation_data[0]['residency_number'];

            $data['user_select_type'] = $accreditation_data[0]['user_select_type'];

        } 
        else if ($user_data) 
        {

            $data['username'] = $user_data[0]['user_name'].' '.$user_data[0]['last_name'];
            $data['mobile_number'] = $user_data[0]['mobile_number'];
            $data['address'] = $user_data[0]['address'];
            



        }

        if($this->input->post('user_select_type') == 0)
        {

            $this->form_validation->set_rules('username', 'Full Name', 'required|min_length[3]|max_length[30]');

            $this->form_validation->set_rules('address', 'Residential Address', 'required');

            $this->form_validation->set_rules('dob', 'Date of Birth', 'required');

            $this->form_validation->set_rules('passport_number', 'Passport Number or Medicare Number', 'required');

            $this->form_validation->set_rules('license_number', 'Drivers License Number', 'required');

            $this->form_validation->set_rules('mobile_number', 'Mobile Number','required|numeric|min_length[10]|max_length[15]');

        }
        else if($this->input->post('user_select_type') == 1)
        {
            $this->form_validation->set_rules('FIRB_number', 'FIRB Approval Number', 'required');
        }

        

        if ($_POST) {
            /* set default form rules */

            /* check if rules are passes or not */
            if ($this->form_validation->run() == false) {


                /* put old values to form for better UX */
                $data['username'] = $this->input->post('username');

                $data['user_select_type'] = $this->input->post('user_select_type');

                $data['address'] = $this->input->post('address');

                $data['dob'] = $this->input->post('dob');

                $data['passport_number'] = $this->input->post('passport_number');

                $data['license_number'] = $this->input->post('license_number');

                $data['mobile_number'] = $this->input->post('mobile_number');

                $data['residency_number'] = $this->input->post('residency_number');

                $data['FIRB_number'] = $this->input->post('FIRB_number');

            } 
            else 
            {
                /* prepare array for database insertion */

                $username = explode(' ',$this->input->post('username'));
                $user_name = $username[0];
                $last_name = $username[1];

                $originalDate = $this->input->post('dob');
                $newDate = date("Y-m-d h:i:s", strtotime($originalDate));

                 if($this->input->post('user_select_type') == 0)
                {


                    $update = array(
                        'user_id' => $user_id,
                        'first_name' => $user_name,
                        'last_name' => $last_name,
                        'address' => $this->input->post('address'),
                        'dob' => $newDate,
                        'passport_number' => SecurePostData($this->input->post('passport_number')),
                        'license_number' => SecurePostData($this->input->post('license_number')),
                        'phone' => SecurePostData($this->input->post('mobile_number')),
                        'residency_number' => SecurePostData($this->input->post('residency_number')),
                        'user_select_type' => $this->input->post('user_select_type'),
                        'accreditation_date' => date('Y-m-d H:i:s'),

                        
                    );
                }
                else if($this->input->post('user_select_type') == 1)
                {
                     $update = array(
                        'user_id' => $user_id,
                         'FIRB_number' => SecurePostData($this->input->post('FIRB_number')),
                         'user_select_type' => $this->input->post('user_select_type'),
                         'verify_user' => 1
                        );
                }
                
               

                if ($this->input->post('accre_id') > 0) {
                    /* insert prepared array into database */
                   $this->accreditation_model->AddInsertUpdateTable('accreditation', 'id', $this->input->post('accre_id'), $update);

                } else {
                    $this->accreditation_model->AddInsertUpdateTable('accreditation', '', '', $update);
                }

                /* set one time message */
                $this->session->set_flashdata('success_message_user', 'User Information updated Successfully');

                redirect('accreditation/personal');

                  
            }
        }

        /* load meta/site information */
        $meta = meta_setting();
        $data['site_setting'] = site_setting();

        /* render meta information */
        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);

        /* render the template */
        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/accreditation/personal', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
    }


    function status()
    {

         $user_id = check_user_authentication(true);
        $data = array();

        $data['accreditation_data'] = $this->accreditation_model->getTableData('accreditation', array('user_id' => $user_id));


        $meta = meta_setting();
        $data['site_setting'] = site_setting();

        /* render meta information */
        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);

        /* render the template */
        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/accreditation/status', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
    }

    

}
/* end of file */
