<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 * The Home class is basically the default controller for the script.
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Investment extends ROCKERS_Controller
{
    /**
     * Constructor method
     */
    function __construct()
    {
        parent::__construct();
        $this->load->model('home_model');
        $this->load->model('investment_step_model');
        $this->load->model('message_model');
        $this->load->model('property_model');
         $this->load->model('content_model');
    }


    /*
    Function name :investment_step()
    Parameter :$equity_id
    Return : none
    Use : load the page and fetch the data related project_id and user_id
    */

    function investor($campaign_id = '',$investor_process_id=0)
    {

        $user_id = check_user_authentication(true);
        $Campaign_data = GetOnePropertyCampaign($campaign_id);

        $property_url = $Campaign_data['property_url'];
        $status = $Campaign_data['status'];
        $campaign_user_id = $Campaign_data['user_id'];
        $campaign_units = $Campaign_data['campaign_units'];
        $campaign_units_get = $Campaign_data['campaign_units_get'];
        $investment_close_date = $Campaign_data['investment_close_date'];
        $share_available = $campaign_units - $campaign_units_get;
        $campaign_user_type = $Campaign_data['campaign_user_type'];

        $is_allow_invest = is_allow_invest($campaign_user_id,$status,$investment_close_date,$campaign_user_type);
       

        if($is_allow_invest == true && $share_available <= $campaign_units )
        {
             
        }
        else
        {
            
            redirect('properties/'.$property_url);
        }


        $investment_array = ManageInvestmentSteps($campaign_id,$user_id,'yes');


        if(is_array($investment_array))
        {
            $dataarray['deposit_amount'] = $investment_array['deposit_amount'];
            $dataarray['units'] = $investment_array['units'];
            $_SESSION['deposit_data'] = $dataarray;
            $investor_process_id = $investment_array['id'];

            if($investment_array['status'] == 0 && $investment_array['deposit_amount'] > 0)
            {
                 redirect('investment/deposit/'.$campaign_id.'/'. $investor_process_id);
            }
           
           
        }

        $data['campaign_id'] = $campaign_id;
        $data['Campaign_data'] = $Campaign_data;

        $pages = $this->content_model->getpages(1, 'help', 0, 0, array('pages_id' => 'desc'));
        $data['pages'] = $pages[0];
    

        $this->load->library('form_validation');
        $this->form_validation->set_rules('term_investment', 'Terms and Conditions', 'required');
        if ($this->form_validation->run() == FALSE)

        {


            if (validation_errors()) 
            {
                $this->session->set_flashdata('error_message_investment', validation_errors());
            } 
            else 
            {
              
            }
        }
        else
        {

            
            $dataarray['deposit_amount'] = $this->input->post('deposit_amount');
            $dataarray['units'] = $this->input->post('units');
            $_SESSION['deposit_data'] = $dataarray;



           
            $deposit_data = array(
                    'user_id' => $user_id,
                    'campaign_id'=> $campaign_id,
                    'invest_status_id' =>1,
                    'created_date'=>date('Y-m-d'),
                    'units' => SecurePostData($this->input->post('units')),
                    'admin_fees' => SecurePostData($this->input->post('admin_fees')),
                    'deposit_amount' => SecurePostData($this->input->post('deposit_amount'))
                );

            $investor_process_id = $this->investment_step_model->AddUpdateData('investment_process',$deposit_data);

            $transaction_id = $this->payment_process($campaign_id,$user_id);
            $deposit_data = array(
                        'transaction_id'=>$transaction_id,
            );

            $this->investment_step_model->AddUpdateData('investment_process',$deposit_data,$investor_process_id,'id');

            redirect('investment/deposit/'.$campaign_id.'/'. $investor_process_id);
          

        }


        
        $meta = meta_setting();
        $data['site_setting'] = site_setting();
        $data['site_name'] = $data['site_setting']['site_name'];

        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);

        if ($this->agent->browser() == 'Internet Explorer' and $this->agent->version() <= 6) {
            $this->template->write_view('main_content', 'default/home/unsupported', $data, TRUE);
        } else {
            $this->template->write_view('main_content', 'default/investment_step/investment', $data, TRUE);
        }
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();

    }

    /*
    Function name :investment_step()
    Parameter :$equity_id
    Return : none
    Use : load the page and fetch the data related project_id and user_id
    */

    function deposit($campaign_id = '',$investor_process_id=0)
    {

        $user_id = check_user_authentication(true);

        $Campaign_data = GetOnePropertyCampaign($campaign_id);
        $data['Campaign_data'] = $Campaign_data;

        $property_url = $Campaign_data['property_url'];
        $status = $Campaign_data['status'];
        $campaign_user_id = $Campaign_data['user_id'];
        $campaign_units = $Campaign_data['campaign_units'];
        $campaign_units_get = $Campaign_data['campaign_units_get'];
        $share_available = $campaign_units - $campaign_units_get;
        $investment_close_date = $Campaign_data['investment_close_date'];
        $campaign_user_type = $Campaign_data['campaign_user_type'];
        $is_allow_invest = is_allow_invest($campaign_user_id,$status,$investment_close_date,$campaign_user_type);
       
        if($is_allow_invest == true && $share_available <= $campaign_units )
        {
          
        }
        else
        {
            redirect('properties/'.$property_url);
        }
        $data['investor_process_id'] = $investor_process_id;
        $investment_array = ManageInvestmentSteps($campaign_id,$user_id,'yes');
        // if(is_array($investment_array))
        // {
        //     if($investment_array['status'] == 0 && $investment_array['deposit_amount'] > 0)
        //     {
                 
        //         if($investment_array['invest_status_id'] == 2)
        //         {
        //             redirect('investment/investment_setup/'.$campaign_id.'/'. $investor_process_id);
        //         }
        //     }
        //     else
        //     {
        //         redirect('investment/investor/'.$campaign_id);
        //     }
           
        // }
        $user_id = check_user_authentication(true);
        $userdata = UserData($user_id);
        $investor_data = $userdata[0];

        $data['campaign_id'] = $campaign_id;
        $pages = $this->content_model->getpages(1, 'help', 0, 0, array('pages_id' => 'desc'));
        $data['pages'] = $pages[0];
        $data['deposit_amount'] = 0;
        if(isset($_SESSION['deposit_data']))
        {
            $deposit_amount = $_SESSION['deposit_data']['deposit_amount'];
             $data['deposit_amount'] = $deposit_amount;
        }
        else
        {
            redirect('investment/investor/'.$campaign_id);
        }

        

       
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('card_number', 'Card Number', 'required');
        $this->form_validation->set_rules('expiry_month', 'Expiry Month', 'required');
        $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'required');
        $this->form_validation->set_rules('cvv_number', 'CVV', 'required');
        if ($this->form_validation->run() == FALSE)
        {
            if (validation_errors()) 
            {
                $this->session->set_flashdata('error_message_investment', validation_errors());
                $data['expiry_year'] = $this->input->post('expiry_year');
                $data['expiry_month'] = $this->input->post('expiry_month');
                $data['cvv_number'] = $this->input->post('cvv_number');
                $data['name'] = $this->input->post('name');
                $data['card_number'] = $this->input->post('card_number');
            } 
            else
            {
                $data['expiry_year'] = $this->input->post('expiry_year');
                $data['expiry_month'] = $this->input->post('expiry_month');
                $data['cvv_number'] = $this->input->post('cvv_number');
                $data['name'] = $this->input->post('name');
                $data['card_number'] = $this->input->post('card_number');
            }
            $data['form_submit'] = 'fail';
           
        }
        else
        {
           
          
             $data['expiry_year'] = $this->input->post('expiry_year');
                $data['expiry_month'] = $this->input->post('expiry_month');
                $data['cvv_number'] = $this->input->post('cvv_number');
                $data['name'] = $this->input->post('name');
                $data['card_number'] = $this->input->post('card_number');


             $this->load->library('Ematterspayment'); 


             $memo = "testing Payment using ematters payment gateway";

            $params = array(
                'customerid' => 1, 
                'invoiceid' => 1,
                'cardnum' => $this->input->post('card_number'),
                'cardexp' => $this->input->post('expiry_month'),
                'cccvv' => $this->input->post('cvv_number'),
                'amount' => $deposit_amount,
                'description' => $memo,

                 );
            $clientdetails = array(
                'firstname' => $investor_data['user_name'],
                'lastname' => $investor_data['last_name'],
                'email' => $investor_data['email'],
                'address1' => $investor_data['address'],
                'city' =>'vadodara',
                'state' =>'Gujarat',
                'postcode' =>$investor_data['zip_code'],
                'country' => 'India',
               
                );

            $params['clientdetails'] = $clientdetails;
        

        //$ematters_request = $this->ematterspayment->ematters_capture($params);


         $data['form_submit'] = 'success';

            $deposit_data = array(
                  
                    'invest_status_id' =>2,
                    'campaign_id' => $campaign_id,
                    'card_holder_name' => SecurePostData($this->input->post('name')),
                    'card_number' => SecurePostData($this->input->post('card_number'))
                );


            $this->investment_step_model->AddUpdateData('investment_process',$deposit_data,$investor_process_id,'id');


             //redirect('investment/deposit_success/'.$campaign_id.'/'.$investor_process_id);
            
        }


        
        $meta = meta_setting();
        $data['site_setting'] = site_setting();
        $data['site_name'] = $data['site_setting']['site_name'];

        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);

        if ($this->agent->browser() == 'Internet Explorer' and $this->agent->version() <= 6) {
            $this->template->write_view('main_content', 'default/home/unsupported', $data, TRUE);
        } else {
            $this->template->write_view('main_content', 'default/investment_step/deposit', $data, TRUE);
        }
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();

    }

    function deposit_success($campaign_id='',$investor_process_id='')
    {

        echo "<pre>";
        print_r($_GET);die;

        $user_id = check_user_authentication(true);

        $Campaign_data = GetOnePropertyCampaign($campaign_id);
        $data['Campaign_data'] = $Campaign_data;

        $property_url = $Campaign_data['property_url'];
        $status = $Campaign_data['status'];
        $campaign_user_id = $Campaign_data['user_id'];
        $campaign_units = $Campaign_data['campaign_units'];
        $campaign_units_get = $Campaign_data['campaign_units_get'];
        $share_available = $campaign_units - $campaign_units_get;
        $investment_close_date = $Campaign_data['investment_close_date'];
         $campaign_user_type = $Campaign_data['campaign_user_type'];
        $is_allow_invest = is_allow_invest($campaign_user_id,$status,$investment_close_date, $campaign_user_type);
       
        if($is_allow_invest == true && $share_available <= $campaign_units )
        {
          
        }
        else
        {
            redirect('properties/'.$property_url);
        }
        $data['campaign_id'] = $campaign_id;
        $data['investor_process_id'] = $investor_process_id;

        $investment_array = ManageInvestmentSteps($campaign_id,$user_id);
        if(is_array($investment_array))
        {
           
            if($investment_array['invest_status_id'] == 2)
            {
                redirect('investment/investment_setup/'.$campaign_id.'/'. $investor_process_id);
            }
        }

        $pages = $this->content_model->getpages(1, 'help', 0, 0, array('pages_id' => 'desc'));
        $data['pages'] = $pages[0];

        $meta = meta_setting();
        $data['site_setting'] = site_setting();
        $data['site_name'] = $data['site_setting']['site_name'];

        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);

        if ($this->agent->browser() == 'Internet Explorer' and $this->agent->version() <= 6) {
            $this->template->write_view('main_content', 'default/home/unsupported', $data, TRUE);
        } else {
            $this->template->write_view('main_content', 'default/investment_step/deposit_success', $data, TRUE);
        }
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
        
    }
    function investment_setup($campaign_id='',$investor_process_id='')
    {
        $user_id = check_user_authentication(true);
        $Campaign_data = GetOnePropertyCampaign($campaign_id);
        $data['Campaign_data'] = $Campaign_data;

        $property_url = $Campaign_data['property_url'];
        $status = $Campaign_data['status'];
        $campaign_user_id = $Campaign_data['user_id'];
        $campaign_units = $Campaign_data['campaign_units'];
        $campaign_units_get = $Campaign_data['campaign_units_get'];
        $share_available = $campaign_units - $campaign_units_get;
        $investment_close_date = $Campaign_data['investment_close_date'];
        $campaign_user_type = $Campaign_data['campaign_user_type'];
        $is_allow_invest = is_allow_invest($campaign_user_id,$status,$investment_close_date,$campaign_user_type);
       
        if($is_allow_invest == true && $share_available <= $campaign_units )
        {
          
        }
        else
        {
            redirect('properties/'.$property_url);
        }
        $data['investor_process_id'] = $investor_process_id;

        $investment_array = ManageInvestmentSteps($campaign_id,$user_id);
        if(is_array($investment_array))
        {
           
            if($investment_array['invest_status_id'] == 3)
            {
                redirect('investment/invite_investor/'.$campaign_id.'/'. $investor_process_id);
            }
        }

        $data['campaign_id'] = $campaign_id;    
        $pages = $this->content_model->getpages(1, 'help', 0, 0, array('pages_id' => 'desc'));
        $data['pages'] = $pages[0];


         $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('company_name', 'Company Number', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('contact_number', 'Contact Number', 'required');
        if ($this->form_validation->run() == FALSE)
        {
            if (validation_errors()) 
            {
                $this->session->set_flashdata('error_message_investment', validation_errors());
                $data['name'] = $this->input->post('name');
                $data['company_name'] = $this->input->post('company_name');
                $data['email'] = $this->input->post('email');
                $data['contact_number'] = $this->input->post('contact_number');
              
            } 
            else
            {
                $data['name'] = $this->input->post('name');
                $data['company_name'] = $this->input->post('company_name');
                $data['email'] = $this->input->post('email');
                $data['contact_number'] = $this->input->post('contact_number');
            }
            
        }
        else
        {
            $data['name'] = $this->input->post('name');
            $data['company_name'] = $this->input->post('company_name');
            $data['email'] = $this->input->post('email');
            $data['contact_number'] = $this->input->post('contact_number');

            $this->document_sign($data['name'], $data['email']);

            redirect('investment/invite_investor/'.$campaign_id.'/'.$investor_process_id);
        }

        $meta = meta_setting();
        $data['site_setting'] = site_setting();
        $data['site_name'] = $data['site_setting']['site_name'];

        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);

        if ($this->agent->browser() == 'Internet Explorer' and $this->agent->version() <= 6) {
            $this->template->write_view('main_content', 'default/home/unsupported', $data, TRUE);
        } else {
            $this->template->write_view('main_content', 'default/investment_step/investment_setup', $data, TRUE);
        }
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
        
    }

    private function document_sign($name="",$email=""){
        $docusign_setting = docusign_setting();
        $params = array(
            'username'=>$docusign_setting['username'],
            'password'=>$docusign_setting['password'],
            'integrator_key'=>$docusign_setting['integrator_key'],
            'host'=>$docusign_setting['host']
        );
        $this->load->library('docusign',$params);

        $apiClient = $this->docusign->initialize();

        $login = $this->docusign->login($apiClient);
        $login = json_decode($login);

        if($login)
        {
            $loginAccounts = $login->loginAccounts[0];
            $accountId = $loginAccounts->accountId;
        }

        // set recipient information
        $recipientName = $name;
        $recipientEmail = $email;

        // configure the document we want signed
        $documentFileName = base_path()."upload/Test.pdf";
        $documentName = "Test.pdf";

        $envelop = $this->docusign->send_envelope($apiClient,$recipientName,$recipientEmail,$documentFileName,$documentName,$accountId);
        $envelop = json_decode($envelop);

        $sign_url = $this->docusign->sign_url($apiClient,$recipientName,$recipientEmail,$accountId,$envelop->envelopeId);

    }

    function invite_investor($campaign_id='',$investor_process_id='')
    {
        $user_id = check_user_authentication(true);
        $data['investor_process_id'] = $investor_process_id;
        $data['campaign_id'] = $campaign_id;    

        $Campaign_data = GetOnePropertyCampaign($campaign_id);
        $data['Campaign_data'] = $Campaign_data;

        $property_url = $Campaign_data['property_url'];
        $status = $Campaign_data['status'];
        $campaign_user_id = $Campaign_data['user_id'];
        $campaign_units = $Campaign_data['campaign_units'];
        $campaign_units_get = $Campaign_data['campaign_units_get'];
        $share_available = $campaign_units - $campaign_units_get;
        $investment_close_date = $Campaign_data['investment_close_date'];
        $is_allow_invest = is_allow_invest($campaign_user_id,$status,$investment_close_date);
       
        if($is_allow_invest == true && $share_available <= $campaign_units )
        {
          
        }
        else
        {
            redirect('properties/'.$property_url);
        }

       

        $property_id = $Campaign_data['property_id'];

        $get_property_user_detail = $this->property_model->GetAllProperties(0, $property_id,'',array('campaign'));

        $data['share_property_detail'] = $get_property_user_detail;
        
        $data['property_url'] = $get_property_user_detail[0]['property_url'];
        $data['property_address'] = $get_property_user_detail[0]['property_address'];
        $data['property_description'] = $get_property_user_detail[0]['property_description'];
        $data['cover_image'] = $get_property_user_detail[0]['cover_image'];

        $pages = $this->content_model->getpages(1, 'help', 0, 0, array('pages_id' => 'desc'));
        $data['pages'] = $pages[0];
        $meta = meta_setting();
        $data['site_setting'] = site_setting();
        $data['site_name'] = $data['site_setting']['site_name'];

        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);

        if ($this->agent->browser() == 'Internet Explorer' and $this->agent->version() <= 6) {
            $this->template->write_view('main_content', 'default/home/unsupported', $data, TRUE);
        } else {
            $this->template->write_view('main_content', 'default/investment_step/invite_investor', $data, TRUE);
        }
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
        
    }

    function end_campaign($campaign_id='',$investor_process_id='')
    {
        $user_id = check_user_authentication(true);
        $data['investor_process_id'] = $investor_process_id;
        $data['campaign_id'] = $campaign_id;   
         $Campaign_data = GetOnePropertyCampaign($campaign_id);
        $data['Campaign_data'] = $Campaign_data;

        $property_url = $Campaign_data['property_url'];
        $status = $Campaign_data['status'];
        $campaign_user_id = $Campaign_data['user_id'];
        $campaign_units = $Campaign_data['campaign_units'];
        $campaign_units_get = $Campaign_data['campaign_units_get'];
        $share_available = $campaign_units - $campaign_units_get;
        $investment_close_date = $Campaign_data['investment_close_date'];
        $is_allow_invest = is_allow_invest($campaign_user_id,$status,$investment_close_date);
       
        if($is_allow_invest == true && $share_available <= $campaign_units )
        {
          
        }
        else
        {
            redirect('properties/'.$property_url);
        }

        $property_id = $Campaign_data['property_id'];

        $get_property_user_detail = $this->property_model->GetAllProperties(0, $property_id,'',array('campaign'));
        
        $data['property_address'] = $get_property_user_detail[0]['property_address'];
        if($_POST)
        {

            $deposit_data = array(
                        'status'=>1,
            );

            $this->investment_step_model->AddUpdateData('investment_process',$deposit_data,$investor_process_id,'id');

            
            $where = array(
                'id' => $investor_process_id
            );

            $investor_data = $this->investment_step_model->get_investor_process('investment_process', $where);


            $preapprovalKey = $investor_data['transaction_id'];
            $get_other_detail_trans = $this->db->query("select * from temp_preapprove where preapprovalKey='" . $preapprovalKey . "'");
            $detail_trans = $get_other_detail_trans->row();
         
            if($get_other_detail_trans->num_rows() > 0)

            {

                $equity_id = $detail_trans->campaign_id;
                $donor_id = $detail_trans->user_id;
                $user_all_data = UserData($donor_id);
                $donor_id = $user_id;
                
                $donar_comment = SecurePostData($detail_trans->comment);
                $total_amount = $detail_trans->total_amount;
                $campaign_owner_amount = $detail_trans->total_amount;
                $units = $detail_trans->units;
               

                $get_other_detail_trans = $this->db->query("select * from transaction where preapproval_key='" . $preapprovalKey . "'");
                $nums_rows = $get_other_detail_trans->num_rows();
                if ($nums_rows <= 0) {
                    $insert = $this->db->query("insert into transaction (`user_id`,`campaign_id`,`amount`,`host_ip`,`comment`,`transaction_date_time`,`preapproval_key`,`preapproval_status`,`preapproval_total_amount`,`units`)values('" . $donor_id . "','" . $campaign_id . "','" . $campaign_owner_amount . "','" . $_SERVER['REMOTE_ADDR'] . "','" . $donar_comment . "','" . date("Y-m-d H:i:s") . "','" . $preapprovalKey . "','SUCCESS','" . $total_amount . "','".$units."')");

                    // activity added
                    $amount_array = array('amount' => set_currency($total_amount));
                   // project_activity('funded', $donor_id, $equity_data['user_id'], $equity_id, $amount_array);

                    $this->update_project_perk($campaign_id,$units, $campaign_owner_amount);

                    $get_project_user_detail =  $user_property_campaign = $this->property_model->GetAllProperties(0, 0, '', array(
                        'user','campaign'
                    ), 1000, array(
                        'property_id' => 'desc'
                    ), '','','yes',$user_id);

                    $prj = $get_project_user_detail[0];
                    $user_all_data = UserData($donor_id);
                    $user_detail = $user_all_data[0];
                    $this->adminalerts($prj, $detail_trans, $user_detail);


                //$insert = $this->db->query("delete from temp_preapprove where preapprovalKey='" . $preapprovalKey . "'" );
                }

            }
            else
            {
                redirect('properties/'.$get_property_user_detail[0]['property_url']);
            }

            redirect('investment/thankyou/'.$campaign_id.'/'.$investor_process_id);



        }

      

       

        

        $pages = $this->content_model->getpages(1, 'help', 0, 0, array('pages_id' => 'desc'));
        $data['pages'] = $pages[0];
        $meta = meta_setting();
        $data['site_setting'] = site_setting();
        $data['site_name'] = $data['site_setting']['site_name'];

        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);

        if ($this->agent->browser() == 'Internet Explorer' and $this->agent->version() <= 6) {
            $this->template->write_view('main_content', 'default/home/unsupported', $data, TRUE);
        } else {
            $this->template->write_view('main_content', 'default/investment_step/campaign_end', $data, TRUE);
        }
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
        
    }

    function thankyou($campaign_id='',$investor_process_id='')
    {
        $user_id = check_user_authentication(true);
        $data['investor_process_id'] = $investor_process_id;
          $Campaign_data = GetOnePropertyCampaign($campaign_id);
        $data['Campaign_data'] = $Campaign_data;

        $property_url = $Campaign_data['property_url'];
        $status = $Campaign_data['status'];
        $campaign_user_id = $Campaign_data['user_id'];
        $campaign_units = $Campaign_data['campaign_units'];
        $campaign_units_get = $Campaign_data['campaign_units_get'];
        $share_available = $campaign_units - $campaign_units_get;
        $investment_close_date = $Campaign_data['investment_close_date'];
        $is_allow_invest = is_allow_invest($campaign_user_id,$status,$investment_close_date);
       
        if($is_allow_invest == true && $share_available <= $campaign_units )
        {
          
        }
        else
        {
            redirect('properties/'.$property_url);
        }

        $data['campaign_id'] = $campaign_id;    
        $pages = $this->content_model->getpages(1, 'help', 0, 0, array('pages_id' => 'desc'));
        $data['pages'] = $pages[0];
        $meta = meta_setting();
        $data['site_setting'] = site_setting();
        $data['site_name'] = $data['site_setting']['site_name'];

        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);

        if ($this->agent->browser() == 'Internet Explorer' and $this->agent->version() <= 6) {
            $this->template->write_view('main_content', 'default/home/unsupported', $data, TRUE);
        } else {
            $this->template->write_view('main_content', 'default/investment_step/thankyou', $data, TRUE);
        }
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
        
    }

    /*
    Function name :payment_process()
    Parameter : $equity_id $user_id
    Return : none
    Use :initiate payment process
    */

    function payment_process($campaign_id = '', $user_id = '')
    {


        if (!is_array($_SESSION["deposit_data"])) {
            redirect('investment/investor/' . $campaign_id);
        }

        
        $post_amount = $_SESSION["deposit_data"]['deposit_amount'];
        $post_units = $_SESSION["deposit_data"]['units'];
        $post_docomment = '';
        $post_anonymous = '';
        $num = 'INV' . randomNumber(12);
        $temp_insert = $this->db->query("insert into temp_preapprove (`preapprovalKey`,`user_id`,`campaign_id`,`comment`,`host_ip`,`transaction_date_time`,`temp_anonymous`,`amount`,`total_amount`,`units`)values('" . $num . "','" . $user_id . "','" . $campaign_id . "','" . SecurePostData($post_docomment) . "','" . $_SERVER['REMOTE_ADDR'] . "','" . date("Y-m-d H:i:s") . "','" . $post_anonymous . "','" . $post_amount . "','" . $post_amount . "','".$post_units."')");
        return $num;

    }

   
}

?>


