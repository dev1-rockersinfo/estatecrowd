<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 * The Home class is basically the default controller for the script.
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Crowd extends ROCKERS_Controller
{

    function __construct()
    {
        parent::__construct();

      
        $this->load->helper('cookie');
        $this->load->library('securimage');
        $this->load->model('crowd_model');

    }


    /*
    Function name :crowdlist()
    Parameter :none
    Return : none
    Use : user can see inbox messages
    */

    function crowdlist()
    {
      
        $user_id = check_user_authentication(true);

        $where_array = array('user_id'=>$user_id);

        $data['user_crowds']= $this->crowd_model->getTableData('crowd',$where_array);

        $meta = meta_setting();
        $data['site_setting'] = site_setting();
        
        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/crowd/list-crowds', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();


    }
        /*
    Function name :crowdlist()
    Parameter :none
    Return : none
    Use : user can see inbox messages
    */

    function create_crowd($crowd_id='')
    {
      
        $user_id = check_user_authentication(true);
        $data['crowd_id'] = $crowd_id;

        $meta = meta_setting();
        $data['site_setting'] = site_setting();
        $data['title'] = '';
        $data['description']='';
        $data['member_data'] = array();
        if($crowd_id > 0 )
        {
            $where_array = array('crowd_id'=>$crowd_id);

            $user_crowds= $this->crowd_model->getTableData('crowd',$where_array);

            $data['title'] = $user_crowds[0]['title'];
            $data['description'] = $user_crowds[0]['description'];
            $data['member_data'] = GetAllCrowdMember($crowd_id,$user_id);
           // echo $this->db->last_query();die;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        if($_POST)
        {
            if ($this->form_validation->run() == FALSE)
            {
                $data['title'] = $this->input->post('title');
                $data['description'] = $this->input->post('description');
            }
            else
            {
               if($this->input->post('crowd_id') > 0 && $this->input->post('crowd_id') != '')
               {
                 
                     $data_update = array(
                        'title' => SecurePostData($this->input->post('title')), 
                        'description' => SecurePostData($this->input->post('description')),
                        'user_id' => $user_id,
                       
                    );

                    $this->crowd_model->AddUpdateData('crowd',$data_update,$this->input->post('crowd_id'),'crowd_id');
               }
               else
               {
                $crowd_url = makeSlugs($this->input->post('title'));
                    $data_insert = array(
                        'title' => SecurePostData($this->input->post('title')),
                        'crowd_url' => $crowd_url,
                        'description' => SecurePostData($this->input->post('description')),
                        'user_id' => $user_id,
                        'date_added' => date('Y-m-d H:i:s'),
                        'host_ip' => $_SERVER['REMOTE_ADDR'],
                        'status' => 1,

                    );

                    $this->crowd_model->AddUpdateData('crowd',$data_insert);
               }

               

                redirect('crowd/crowdlist');
            }
        }
        
        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/crowd/create-crowd', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();


    }

    /*
    Function name :crowd_detail()
    Parameter :none
    Return : none
    Use : user can see inbox messages
    */

    function crowd_detail($crowd_url)
    {
      
        $user_id = check_user_authentication(true);

        $where_array = array('crowd_url'=>$crowd_url);

        $data['crowds']= $this->crowd_model->getTableData('crowd',$where_array);


        $crowd_id = $data['crowds'][0]['crowd_id'];

        $data['share_crowd_detail'] = $data['crowds'];

        $data['member_data'] = GetAllCrowdMember($crowd_id);


        $data['loginuserdata'] = Userdata($user_id);

        $meta = meta_setting();
        $data['site_setting'] = site_setting();
        
        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/crowd/crowd_detail', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();


    }

    function search_investors($text=''){
        $data = array();
        $offset = 0;
        $limit = 10;
        if($text==''){
            exit();
        }

        $search_propertes = $this->crowd_model->getInvestors($text,$limit);

        if ($search_propertes) {
            $str = '<ul id="srhdiv">';
            foreach ($search_propertes as $search_property) {
                $user_id=$search_property['user_id'];

                $str .= '<li onclick="selecttext(this);" data-id='."$user_id".'>' . $search_property['user_name'].'</li>';
            }
            $str .= '</ul>';
            echo $str;
            die();
        }
    }

    function invite_investor(){
        $data['error']='';
        $data['success']='';

        if ($this->session->userdata('user_id') == '') {
            $data['error']="Something went wrong.";
        }
        $uid = $this->session->userdata('user_id');
        $user_info = UserData($uid);
        if (!$user_info) {
            $data['error']="Something went wrong.";
        }

        $recipient_name = $this->input->post('name');
        $crowd_id = $this->input->post('crowd_id');
        $user_id = $this->input->post('id');

        if(!$recipient_name){
            $data['error']="Please enter investor.";
        }
        if($user_id){
            $invited_user = UserData($user_id);
            $recipient_email = $invited_user[0]['email'];
        }else{
            $invited_user='';
            $recipient_email = '';
        }

        if(!$invited_user){
            $data['error']="Investor doesn't exist.";
        }

        if ($user_info[0]['email'] == $recipient_email) {
            $data['error']="You can't invite yourself $recipient_email";
        }
        $search_arr = array(
            'email'=>$recipient_email,
            'crowd_id'=>$crowd_id
        );
        $crowd = $this->crowd_model->getCrowdRequestByEmail($search_arr);
        //print_r($crowd);die;
        if ($crowd) {
            $data['error']="You already sent Invitation to this email $recipient_email. ";
        }

        if($recipient_email && $data['error']==''){
            $code = $user_info[0]['unique_code'];
            if($crowd_id > 0)
            {

            }
            else
            {
                $data = array(
                    'date_added' => date('Y-m-d H:i:s'),
                    'host_ip' => getRealIP(),
                    'user_id' => $uid,
                );
                $this->db->insert('crowd', $data);
                $crowd_id = $this->db->insert_id();
            }

            $data = array(
                'code' => $code,
                'email' => $recipient_email,
                'date_added' => date('Y-m-d H:i:s'),
                'host_ip' => getRealIP(),
                'invite_user_id' => $uid,
                'crowd_id' => $crowd_id,

            );
            $this->db->insert('crowd_invite_request', $data);
            $response = array(
                'status' => 'success',
                'email' => $recipient_email,
                'crowd_id'=>$crowd_id
            );
            $invitation_link = '<a href="' . site_url('crowd/invited/' . $code . '/' . base64_encode($recipient_email)) . '">';
            $end_invitation_link = '</a>';
            $login_user_name = ucfirst($user_info[0]['user_name']) . ' ' . ucfirst($user_info[0]['last_name']);
            // /===report comment to pinner
            $email_template = $this->db->query("select * from " . $this->db->dbprefix('email_template') . " where task='Crowd Invitation'");
            $email_temp = $email_template->row();
            if ($email_temp) {
                $email_address_from = $email_temp->from_address;
                $email_from_name = '';
                $email_address_reply = $email_temp->reply_address;
                $email_subject = $email_temp->subject;
                $email_message = $email_temp->message;
                $email_subject = str_replace('{login_user_name}', $login_user_name, $email_subject);
                $email_address_from = $user_info[0]['email'];
                $email_to = $recipient_email;
                $email_message = str_replace('{break}', '<br/>', $email_message);
                $email_message = str_replace('{invitation_link}', $invitation_link, $email_message);
                $email_message = str_replace('{end_invitation_link}', $end_invitation_link, $email_message);

                $email_message = str_replace('{login_user_name}', $login_user_name, $email_message);
                $str = $email_message;
                /** custom_helper email function **/
                if ($email_to) {
                    email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str, $email_from_name);
                }
            }
            $data['success']="Invitation Has been sent to $recipient_email";
        }
        echo json_encode($data);
    }
    /*
    Function name :save_comment()
    Parameter :none;
    Return : Array for crowd comment 
    Use :comment  for crowd
   
    */
    function save_comment()
    {
       
        
        $data = array();
        $data["error"] = false;
        $data["msg"] = '';
        $data["url"] = '';
        $site_setting = site_setting();
        
     
        $crowd_id = SecurePostData($this->input->post('crowd_id'));
        $comment = SecurePostData($this->input->post('comment'));
        
        $comment_id = SecurePostData($this->input->post('comment_id'));

        $property_address = SecurePostData($this->input->post('property_address'));


        $property_id = 0;
        if($property_address != '')
        {
            
            $property_data = $this->crowd_model->GetPropertyId($property_address);

            $property_id = $property_data['property_id'];
        }
      


        $this->load->library('form_validation');
        $this->form_validation->set_rules('comment', 'comment', 'required');
      

        if ($this->session->userdata('user_id') == '') {
            $data["url"] = 'home/login';
            $data["error"] = true;
            echo json_encode($data);
            die;
        } else if ($crowd_id == null || $crowd_id == '' || $crowd_id == '0') {
            $data["url"] = "";
            $data["error"] = true;
            echo json_encode($data);
            die;
        }
      
        //   Check validations
        if ($this->form_validation->run($this) == FALSE) 
        {

     
            $data['comment_error'] = form_error('comment');
          
            $data["error"] = true;
            echo json_encode($data);
            die;

        }
        $imagename = '';


        if (isset($_FILES['comment_image']['name'])) {
            $_FILES['userfile']['name'] = $_FILES['comment_image']['name'];
            $_FILES['userfile']['type'] = $_FILES['comment_image']['type'];
            $_FILES['userfile']['tmp_name'] = $_FILES['comment_image']['tmp_name'];
            $image_info = getimagesize($_FILES['comment_image']['tmp_name']);
            $image_width = $image_info[0];
            $image_height = $image_info[1];
            $_FILES['userfile']['error'] = $_FILES['comment_image']['error'];
            $_FILES['userfile']['size'] = $_FILES['comment_image']['size'];
            $_FILES['userfile']['max_width'] = $image_width;
            $_FILES['userfile']['max_height'] = $image_height;
            // image validation
             $image_settings = get_image_setting_data();
            if ($_FILES["userfile"]["type"] != "image/jpeg" and $_FILES["userfile"]["type"] != "image/pjpeg" and $_FILES["userfile"]["type"] != "image/png" and $_FILES["userfile"]["type"] != "image/x-png" and $_FILES["userfile"]["type"] != "image/gif") {
                $data["image_error"] = PLEASE_UPLOAD_A_JPG_PNG_GIF_FILE;
                $data["error"] = true;
                echo json_encode($data);
                die;
            } else if ($_FILES["userfile"]["size"] >  $image_settings['upload_limit']*1000000) {
                $data["image_error"] = sprintf(SORRY_THIS_FILE_IS_TOO_LARGE_PLEASE_SELECT_A_JPG_FILE_THAT_IS_LESS_THAN_MB_OR_TRY_RESIZING_IT_USING_A_PHOTO_EDITOR,$image_settings['upload_limit']);
                $data["error"] = true;
                echo json_encode($data);
                die;
            } else {


                $files = $_FILES;
                $base_path = $this->config->slash_item('base_path');
                $rand = rand(0, 100000);
                  
                $path_parts = pathinfo($_FILES['comment_image']['name']);
                $date = new DateTime();
                
                $file_extention = $path_parts['extension'];
                $new_img = $rand .'-crowd-'.$date->getTimestamp().'.'.$file_extention;
               
                move_uploaded_file($files["userfile"]["tmp_name"], $base_path . "upload/crowd/" . $new_img);
                $imagename = $new_img;

                $data["image"]["image_name"] = $imagename;
                $data["image"]["path"] = base_url() . "upload/crowd/" . $imagename;


            }
        }
       
      

        if ($comment_id > 0) 
        {
              $data_array = array('comment' => $comment, 'crowd_id' => $crowd_id, 'property_id' => $property_id, 'user_id' =>
            $this->session->userdata('user_id'), 'updated_date' => date('Y-m-d H:i:s'));

            //echo $imagename;die;
            if ($imagename != '') {

                $data_array = array('comment' => $comment, 'crowd_id' => $crowd_id, 'property_id' => $property_id, 'user_id' =>
                $this->session->userdata('user_id'), 'updated_date' => date('Y-m-d H:i:s'),'image'=>$imagename,'image_name'=>$_FILES['comment_image']['name']);

            }


            $this->crowd_model->AddUpdateData('crowd_comment',$data_array, $comment_id, 'id' );

        } else {

             $data_array = array('comment' => $comment, 'crowd_id' => $crowd_id, 'property_id' => $property_id, 'user_id' =>
            $this->session->userdata('user_id'),'date_added' => date('Y-m-d H:i:s'), 
            'updated_date' => date('Y-m-d H:i:s'));

              //echo $imagename;die;
            if ($imagename != '') {

                $data_array = array('comment' => $comment, 'crowd_id' => $crowd_id, 'property_id' => $property_id, 'user_id' =>
                $this->session->userdata('user_id'), 'updated_date' => date('Y-m-d H:i:s'),'image'=>$imagename,'image_name'=>$_FILES['comment_image']['name']);

            }

            $crowd_comment_id = $this->crowd_model->AddUpdateData('crowd_comment', $data_array);

            $user_comment_array = array('user_id'=>$this->session->userdata('user_id'),'crowd_id' => $crowd_id,'crowd_comment_id' => $crowd_comment_id);

            $this->crowd_model->AddUpdateData('user_comment', $user_comment_array);


        }
     
        echo json_encode($data);


    }

    /*
    Function name :list_comment()
    Parameter :none;
    Return : Result array
    Use : Show Comment list for Crowd ;
   
    */
    function list_comment()
    {

       
        $data = array();
        $data["error"] = false;
        $data["msg"] = '';
        $data["url"] = '';
    
        $data['crowd_id'] = $crowd_id = SecurePostData($this->input->post('crowd_id'));

    
        if ($this->session->userdata('user_id') == '') {
            $data["url"] = 'home/login';
            $data["error"] = true;
            echo json_encode($data);
            die;
        } 
        //check user select value from dropdown
        if ($crowd_id == null || $crowd_id == '' || $crowd_id == '0') {
            $data["msg"] = INVALID_DATA;
            $data["error"] = true;
            echo json_encode($data);
            die;
        }
        
        $comment_data = $this->crowd_model->GetAllCrowdComments($crowd_id);
       
        $data["comment_data"] = $comment_data;

        $this->load->view('default/crowd/list_comment', $data);
    }

    /*
    Function name :edit_comment()
    Parameter :none;
    Return : Result array
    Use : Edit Comment for Crowd ;
   
    */
    function edit_comment()
    {
        $data = array();
        $data["error"] = false;
        $data["msg"] = '';
        $data["url"] = '';
       
       $crowd_id = SecurePostData($this->input->post('crowd_id'));
       
       $comment_id = SecurePostData($this->input->post('comment_id'));

        if ($this->session->userdata('user_id') == '') {
            $data["url"] = 'home/login';
            $data["error"] = true;
            echo json_encode($data);
            die;
        } 
        //check user select value from dropdown
        if ($crowd_id == null || $crowd_id == '' || $crowd_id == '0') {
            $data["msg"] = INVALID_DATA;
            $data["error"] = true;
            echo json_encode($data);
            die;
        }
        //check user select value from dropdown
        if ($comment_id == null || $comment_id == '' || $comment_id == '0') {
            $data["msg"] = PLEASE_CLICK_ON_TEAM_MEMBER_TO_EDIT;
            $data["error"] = true;
            echo json_encode($data);
            die;
        }
        $comment_data = $this->crowd_model->GetAllCrowdComments($crowd_id, $comment_id);
        $data["comment_data"] = $comment_data[0];

        echo json_encode($data);
        die;

    }
      /*
    Function name :delete_team_member()
    Parameter :none;
    Return : none
    Use : Delete Team Member for equity ;
    Auther:Rakesh
    */
    function delete_comment()
    {
        $data = array();
        $data["error"] = false;
        $data["msg"] = '';
        $data["url"] = '';
        $crowd_id = SecurePostData($this->input->post('crowd_id'));
        $comment_id = SecurePostData($this->input->post('comment_id'));
      
        if ($this->session->userdata('user_id') == '') {
            $data["url"] = 'home/login';
            $data["error"] = true;
            echo json_encode($data);
            die;
        }

        //check user select value from dropdown
        if ($comment_id == null || $comment_id == '' || $comment_id == '0') {
            $data["msg"] = PLEASE_CLICK_ON_TEAM_MEMBER_TO_DELETE;
            $data["error"] = true;
            echo json_encode($data);
            die;
        }

        $site_setting = site_setting();
        $site_name = $site_setting['site_name'];
        
        $sql = "delete from crowd_comment where id='" . $comment_id . "' ";
        $this->db->query($sql);
        echo json_encode($data);
    }

      /*
    Function name :delete_team_member()
    Parameter :none;
    Return : none
    Use : Delete Team Member for equity ;
  
    */
    function delete_image_comment()
    {
        $data = array();
        $data["error"] = false;
        $data["msg"] = '';
        $data["url"] = '';
        $crowd_id = SecurePostData($this->input->post('crowd_id'));
        $comment_id = SecurePostData($this->input->post('comment_id'));
      
        if ($this->session->userdata('user_id') == '') {
            $data["url"] = 'home/login';
            $data["error"] = true;
            echo json_encode($data);
            die;
        }

        //check user select value from dropdown
        if ($comment_id == null || $comment_id == '' || $comment_id == '0') {
            $data["msg"] = PLEASE_CLICK_ON_TEAM_MEMBER_TO_DELETE;
            $data["error"] = true;
            echo json_encode($data);
            die;
        }

        $site_setting = site_setting();
        $site_name = $site_setting['site_name'];
        
        $sql = "update crowd_comment set image = '' , image_name = ''   where id='" . $comment_id . "' ";
        $this->db->query($sql);
        echo json_encode($data);
    }
   function search_auto_property($text = '')
   {
        $data = array();
        $offset = 0;
        $limit = 10;
        $keyword = SecurePostData($text);
        $search_criteria = array(
            'property_address' => $keyword
        );
        $data['search_criteria'] = $search_criteria;
        $search_propertes = $this->crowd_model->GetAllSearchProperties($search_criteria,$limit);
        // var_dump($search_equities);
        if ($search_propertes) {
            $str = '<ul id="srhdiv">';
            foreach ($search_propertes as $search_property) {
                $str .= '<li onclick="selecttext(this);">' . $search_property['property_address'].'</li>';
            }
            $str .= '</ul>';
            echo $str;
            die();
        }
        die;
   }

    /*
    Function name :invite_member()
    Parameter :none
    Return : none
    Use : to send invititation via email

    */
    function invite_member()
    {
        $email = 0;
        if ($this->session->userdata('user_id') == '') {
            redirect('home/login');
        }
        $uid = $this->session->userdata('user_id');
        $user_info = UserData($uid);
        if (!$user_info) {
            redirect('home/login');
        }
        $crowd_id = SecurePostData($this->input->post('crowd_id'));
        $recipient_email = trim(strip_tags($_REQUEST['remail']));
       
        if ($user_info[0]['email'] == $recipient_email) {
            $email = 1;
        }
        $check_invite_email = $this->db->get_where('crowd_invite_request', array(
            'email' => $recipient_email,
            'crowd_id' => $crowd_id,
        ));
        if ($check_invite_email->num_rows() > 0) {
            $email = 1;
        }
        if ($email == 0) {
            if ($recipient_email) {
                $code = $user_info[0]['unique_code'];
                if($crowd_id > 0)
                {

                }
                else
                {   
                    $data = array(
                    
                    
                    'date_added' => date('Y-m-d H:i:s'),
                    'host_ip' => getRealIP(),
                    'user_id' => $uid,
                   

                    );
                    $this->db->insert('crowd', $data);
                    $crowd_id = $this->db->insert_id();

                }


                $data = array(
                    'code' => $code,
                    'email' => $recipient_email,
                    'date_added' => date('Y-m-d H:i:s'),
                    'host_ip' => getRealIP(),
                    'invite_user_id' => $uid,
                    'crowd_id' => $crowd_id,

                );
                $this->db->insert('crowd_invite_request', $data);
                $response = array(
                    'status' => 'success',
                    'email' => $recipient_email,
                    'crowd_id'=>$crowd_id
                );
                $invitation_link = '<a href="' . site_url('crowd/invited/' . $code . '/' . base64_encode($recipient_email)) . '">';
                $end_invitation_link = '</a>';
                $login_user_name = ucfirst($user_info[0]['user_name']) . ' ' . ucfirst($user_info[0]['last_name']);
                // /===report comment to pinner
                $email_template = $this->db->query("select * from " . $this->db->dbprefix('email_template') . " where task='Crowd Invitation'");
                $email_temp = $email_template->row();
                if ($email_temp) {
                    $email_address_from = $email_temp->from_address;
                    $email_from_name = '';
                    $email_address_reply = $email_temp->reply_address;
                    $email_subject = $email_temp->subject;
                    $email_message = $email_temp->message;
                    $email_subject = str_replace('{login_user_name}', $login_user_name, $email_subject);
                    $email_address_from = $user_info[0]['email'];
                    $email_to = $recipient_email;
                    $email_message = str_replace('{break}', '<br/>', $email_message);
                    $email_message = str_replace('{invitation_link}', $invitation_link, $email_message);
                    $email_message = str_replace('{end_invitation_link}', $end_invitation_link, $email_message);
                   
                    $email_message = str_replace('{login_user_name}', $login_user_name, $email_message);
                    $str = $email_message;
                    /** custom_helper email function **/
                    if ($email_to) {
                        email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str, $email_from_name);
                    }
                }
            } else {
                $response = array(
                    'status' => 'fail'
                );
            }
        } else if ($user_info[0]['email'] == $recipient_email) {
            $response = array(
                'status' => 'not_invite',
                'email' => $recipient_email
            );
        } else {
            $response = array(
                'status' => 'exist',
                'email' => $recipient_email
            );
        }
        echo json_encode($response);
        exit; // only print out the json version of the response
    }

     /*
    Function name :invited()
    Parameter : code , invite_email
    Return : none
    Use : saved the data in user table  */
    function invited($code, $invite_email = '')
    {
        if ($this->session->userdata('user_id') != '') {
            redirect('home/main_dashboard');
        }
        $email = '';
        if ($invite_email != 'facebook') {
            
          
            $check_request_exists = $this->crowd_model->get_code(array(
                'code' => $code,
                'email' => base64_decode($invite_email)
            ));
            if ($check_request_exists) {
                $user_id = $check_request_exists['invite_by'];
            }
            $email = $invite_email;
        } elseif ($invite_email == 'facebook') {
        }

        $domain_name = '';
        $get_domain_name = get_domain_name(base_url());
        if ($get_domain_name) {
            $domain_name = $get_domain_name;
        }

        $this->load->helper('cookie');
        $cookie = array(
            'name' => 'invite_code',
            'value' => $code,
            'expire' => time(),
            'domain' => $domain_name,
            'secure' => false
        );
        set_cookie($cookie);

        $this->session->set_userdata(array(
            'invite_code' => $code
        ));
        $db_values = array(
            'fb_uid' => '',
            'user_name' => '',
            'last_name' => '',
            'email' => $email,
            'tw_id' => '',
            'fb_img' => '',
            'fb_access_token' => '',
            'tw_screen_name' => '',
            'oauth_token' => '',
            'oauth_token_secret' => '',
            'invite_code' => $code,
        );

            

            $user_data = array(
                'email' => $email
            );
            $query = $this->db->get_where('user', $user_data);

            if ($query->num_rows() > 0) {
                $chk = $query->row_array();
                $invite_user_id = $chk['user_id'];

                $sql = "update crowd_invite_request set status= 1 ,invite_user_id=" . $invite_user_id . " ,code='' where code='1";
                $this->db->query($sql);
                // logout
                $this->session->sess_destroy();
                // login
                if ($chk['active'] == '1') {
                    $data_session = array(
                        'user_id' => $chk['user_id'],
                        'user_name' => $chk['user_name'],
                        'email' => $chk['email'],
                    );
                    $data1 = array(
                        'user_id' => $chk['user_id'],
                        'login_date_time' => date('Y-m-d H:i:s'),
                        'login_ip' => $_SERVER['REMOTE_ADDR']
                    );
                    $this->db->insert('user_login', $data1);
                    $this->session->set_userdata($data_session);

                  $redirct = 'crowd/crowdlist';
                }

            } 
            else 
            {
                $this->session->sess_destroy();
                $redirct = 'home/signup/' . $code.'/'.$invite_email;
            }
            redirect($redirct);
        // data ready, try to create the new user
       
    }
      /*
    Function name :delete_team_member()
    Parameter :none;
    Return : none
    Use : Delete Team Member for equity ;
    Auther:Rakesh
    */
    function remove_member()
    {
        $data = array();
         $user_id = check_user_authentication(true);
        $data["error"] = false;
        $data["msg"] = '';
        $data["url"] = '';
        $crowd_id = SecurePostData($this->input->post('crowd_id'));
        $invite_request_id = SecurePostData($this->input->post('invite_request_id'));
      
        if ($this->session->userdata('user_id') == '') {
            $data["url"] = 'home/login';
            $data["error"] = true;
            echo json_encode($data);
            die;
        }

       $sql = "delete from crowd_invite_request where id='" . $invite_request_id . "' ";

        $this->db->query($sql);
        echo json_encode($data);
    }

     /*
    Function name :AddCrowdRequest()
    Parameter :no
    Return : success message
    Use : interested user can request to owner for join crowd
    */
    function AddCrowdRequest()
    {
        $data = array();
         $user_id = check_user_authentication(true);
        $crowd_id = SecurePostData($this->input->post('crowd_id'));
        $site_setting = site_setting();
        $site_name = $site_setting['site_name'];
       

        $check_id_array = array("crowd_id" => $crowd_id, "user_id" => $this->session->userdata('user_id'));
        $crowd_data = $this->crowd_model->getTableData('crowd_request', $check_id_array);
        $crowd_request_data = $crowd_data[0];

        if ($crowd_request_data['crowd_request_id'] > 0) 
        {
             $add_request_data = array(
                'user_id' => $this->session->userdata('user_id'),
                'crowd_id' => $crowd_id,
                'status' => 1,
                'updated_date' => date('Y-m-d H:i:s'),
            );
            $this->crowd_model->AddUpdateData('crowd_request',$add_request_data,  $crowd_id, 'crowd_id');
        } 
        else 
        {
             $add_request_data = array(
                'user_id' => $this->session->userdata('user_id'),
                'crowd_id' => $crowd_id,
                'status' => 1,
                'created_date' => date('Y-m-d H:i:s'),
                'updated_date' => date('Y-m-d H:i:s'),
            );
            $this->crowd_model->AddUpdateData('crowd_request', $add_request_data );
            $interest_request_id = $this->db->insert_id();
        }


        $user_data = UserData($this->session->userdata('user_id'));

        $crowd_data = $this->crowd_model->GetAllCrowds($crowd_id);
        $crowds = $crowd_data[0];

        $crowd_name = $crowds['title'];
        $crowd_url = $crowds['crowd_url'];
        $email = $user_data[0]['email'];
     
        $crowd_page_link = site_url('crowds/' . $crowd_url);
        $crowd_title_anchor = '<a href="' . $crowd_page_link . '">' . $crowd_name . '</a>';


        $owner_data = UserData($crowds['user_id']);
        $owner_name = $owner_data[0]['user_name'];
        $owner_email = $owner_data[0]['email'];
        $user_profile_slug = $user_data[0]['profile_slug'];
        $username = $user_data[0]['user_name'] . ' ' . $user_data[0]['last_name'];
        $requester_page_link = site_url('user/' . $user_profile_slug);
        $requester_title_anchor = '<a href="' . $requester_page_link . '">' . $username . '</a>';


        $request_page_link = site_url('crowd/dashboard/' . $crowd_id);
        $crowd_title_anchor_owner = '<a href="' . $request_page_link . '">' . $crowd_name . '</a>';
        //============== Send access request to owner================================
        
        $email_template = $this->db->query("select * from `email_template` where task='Crowd request received' ");
        $email_temp = $email_template->row();
        $email_message = $email_temp->message;
        $email_subject = $email_temp->subject;
        $email_subject = str_replace('{crowd_name}', $crowd_name, $email_subject);

        $email_address_from = $email_temp->from_address;
        $email_address_reply = $email_temp->reply_address;
        $email_to = $owner_email;
        $email_message = str_replace('{break}', '<br/>', $email_message);
        $email_message = str_replace('{user_name}', $owner_name, $email_message);
       
        $email_message = str_replace('{requestor_name}', $requester_title_anchor, $email_message);
        $email_message = str_replace('{crowd_name}', $crowd_name, $email_message);
        $email_message = str_replace('{crowd_name_link}', $crowd_title_anchor_owner, $email_message);
        $email_message = str_replace('{intrest_link}', $request_page_link, $email_message);
        $email_message = str_replace('{site_name}', $site_name, $email_message);

        $str = $email_message;
        email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);

     

        //============== Send access request to investor user================================
         
        $email_template = $this->db->query("select * from `email_template` where task='Crowd request sent'  ");
        $email_temp = $email_template->row();
        $email_message = $email_temp->message;
        $email_subject = $email_temp->subject;
        $email_subject = str_replace('{crowd_name}', $crowd_name, $email_subject);
       
        $email_address_from = $email_temp->from_address;
        $email_address_reply = $email_temp->reply_address;
        $email_to = $email;
        $email_message = str_replace('{break}', '<br/>', $email_message);
        $email_message = str_replace('{user_name}', $username, $email_message);
        $email_message = str_replace('{crowd_name}', $crowd_name, $email_message);
       
        $email_message = str_replace('{crowd_name_link}', $crowd_title_anchor, $email_message);

        $email_message = str_replace('{site_name}', $site_name, $email_message);
        $str = $email_message;
        email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);


        $data['success'] = 'success';
        echo json_encode($data);


    }
     /*
    Function name :leaveCrowdRequest()
    Parameter :no
    Return : success message
    Use : interested user can request to owner for join crowd
    */
    function leaveCrowdRequest()
    {
        $data = array();
         $user_id = check_user_authentication(true);
        $crowd_id = SecurePostData($this->input->post('crowd_id'));
        $site_setting = site_setting();
        $site_name = $site_setting['site_name'];
       

        $check_id_array = array("crowd_id" => $crowd_id, "user_id" => $this->session->userdata('user_id'));
       
        $crowd_request_data = $this->crowd_model->deleteData('crowd_request',$check_id_array);

        $crowd_invite_request_data = $this->crowd_model->deleteData('crowd_invite_request',$check_id_array);

        $data['success'] = 'success';
        echo json_encode($data);


    }
    function dashboard($crowd_id)
    {

        $user_id = check_user_authentication(true);
        $crowd_request_data = $this->crowd_model->GetCrowdRequest($user_id,$crowd_id,'1',array('crowd_request','crowd'));
       

        $data['crowd_request_data'] =  $crowd_request_data;
        $data['title'] = $crowd_request_data[0]['title'];
        $data['crowd_id'] = $crowd_request_data[0]['crowd_id'];
        $meta = meta_setting();
        $data['site_setting'] = site_setting();
        
        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/crowd/dashboard', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();

    }
    /*
    Function name :updateintrestrequest()
    Parameter :no
    Return : success message
    Use : interest request approve or reject
    */
    function updateCrowdRequest()
    {

        $user_id = check_user_authentication(true);
        $data = array();

        $crowd_id = SecurePostData($this->input->post('crowd_id'));
        $crowd_request_id = SecurePostData($this->input->post('crowd_request_id'));
       
        $request_user_id = SecurePostData($this->input->post('request_user_id'));
     
        $site_setting = site_setting();
        $site_name = $site_setting['site_name'];
       
        $request_type = SecurePostData($this->input->post('request_type'));

        if($request_type == 'approve')
        {
            $status = 2;
        }
        elseif ($request_type == 'reject') 
        {
            $status = 3;
        }
        $add_request_data = array(
            'user_id' => $request_user_id,
            'crowd_id' => $crowd_id,
            'status' => $status,
            'updated_date' => date('Y-m-d H:i:s'),

        );
        if($request_type == 'approve')
        {
            $add_member_data = array(
                'user_id' => $request_user_id,
                'crowd_id' => $crowd_id,
                'status' => 1,
                'invite_user_id' =>$user_id,
                'date_added' => date('Y-m-d H:i:s'),
                'code' => 1,

            );
            $this->crowd_model->AddUpdateData('crowd_invite_request', $add_member_data);
        }
       

        $this->crowd_model->AddUpdateData('crowd_request', $add_request_data, $crowd_request_id, 'crowd_request_id');

        $check_id_array = array("crowd_request_id" => $crowd_request_id);
        $interest_data = $this->crowd_model->getTableData('crowd_request', $check_id_array);
        $interest_request_data = $interest_data[0];

        $crowd_data = $this->crowd_model->GetAllCrowds($crowd_id);
        $crowds = $crowd_data[0];

       
        $userdata = UserData($interest_request_data['user_id']);


        //============== Approve access request by owner================================
        $crowd_name = $crowds['title'];
        $crowd_url = $crowds['crowd_url'];
        $username = $userdata[0]['user_name'];
        $email = $userdata[0]['email'];
     

        $crowd_page_link = site_url('crowds/' . $crowd_url);
        $crowd_title_anchor = '<a href="' . $crowd_page_link . '">' . $crowd_name . '</a>';
         
        if ($request_type == 'approve') {
            $email_template = $this->db->query("select * from `email_template` where task='Crowd request approved'");
            $email_temp = $email_template->row();
            $email_message = $email_temp->message;
            $email_subject = $email_temp->subject;
            $email_subject = str_replace('{crowd_name}', $crowd_name, $email_subject);
           
            $email_address_from = $email_temp->from_address;
            $email_address_reply = $email_temp->reply_address;
            $email_to = $email;
            $email_message = str_replace('{break}', '<br/>', $email_message);
            $email_message = str_replace('{user_name}', $username, $email_message);
            $email_message = str_replace('{crowd_name}', $crowd_title_anchor, $email_message);
         
            $email_message = str_replace('{crowd_page_link}', $crowd_page_link, $email_message);
            $email_message = str_replace('{site_name}', $site_name, $email_message);
            $str = $email_message;
            email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);
        } //============== Reject access request by owner================================
        else { 
            $email_template = $this->db->query("select * from `email_template` where task='Crowd request reject'");
            $email_temp = $email_template->row();
            $email_message = $email_temp->message;
            $email_subject = $email_temp->subject;
            $email_subject = str_replace('{crowd_name}', $crowd_name, $email_subject);
          
            $email_address_from = $email_temp->from_address;
            $email_address_reply = $email_temp->reply_address;
            $email_to = $email;
            $email_message = str_replace('{break}', '<br/>', $email_message);
            $email_message = str_replace('{user_name}', $username, $email_message);
            $email_message = str_replace('{crowd_name}', $crowd_title_anchor, $email_message);
            $email_message = str_replace('{crowd_page_link}', $crowd_page_link, $email_message);
            $email_message = str_replace('{site_name}', $site_name, $email_message);
            $str = $email_message;
            email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);
        }

        $data['success'] = 'success';
        echo json_encode($data);
    }

   

}

?>
