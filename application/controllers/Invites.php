<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 * The Home class is basically the default controller for the script.
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Invites extends ROCKERS_Controller

{
    function __construct()
    {
        parent::__construct();
        $this->load->model('home_model');

        $this->load->library('securimage');


    }

    /*
    Function name :sendinvite()
    Parameter :$msg (message string)
    Return : none
    Use : to send facebook invite
    Description : this is used to send facebook invite
    */
    public function sendinvite($msg = '')

    {
        $strtemp = '';

        foreach ($_POST["facebook_invites"] as $key => $value) {
            // $strtemp.= $key."=".$value.",";
            if (is_array($value)) {
                foreach ($value as $key => $v) {
                    if (is_array($v)) {
                        foreach ($v as $key => $vv) {
                            if (is_array($vv)) {
                                // $this->aarraytostr($value);
                            } else {
                                $strtemp .= $key . "=" . $vv . ",";
                            }
                        }
                    } else {
                        $strtemp .= $key . "=" . $v . ",";
                    }
                }
            } else {
                $strtemp .= $key . "=" . $value . ",";
            }
        }
        $arr = explode(",", $strtemp);
        $str = '';
        foreach ($arr as $id) {
            $arr1 = explode("=", $id);
            if (isset($arr1[1])) {
                if (is_numeric($arr1[1])) {
                    if ($str == '') {
                        $str .= $arr1[1];
                    } else {
                        $str .= ',' . $arr1[1];
                    }
                }
            }
        }
        echo $str;
        die;
    }

    /*
    Function name :index()
    Parameter :$msg (message string)
    Return : none
    Use : to show the invites page
    Description : this is used to show invite page data
    */
    public function index($msg = '')

    {
        $uid = check_user_authentication(true);

        if (check_user_authentication() != true) {
            redirect('home/login');
        }
        $uid = $this->session->userdata('user_id');
        $user_info = UserData($uid);
        if (!$user_info) {
            //redirect('home/login');
        }

        $data['user_info'] = $user_info;
        $meta = meta_setting();
        $data['site_setting'] = site_setting();
        $facebook_setting = facebook_setting();
        $google_setting = google_setting();
     
        $data['facebook_enable'] = $facebook_setting->facebook_login_enable;
        $data['google_enable'] = $google_setting->google_enable;
     
        if ($msg == 'disconnect') {
            $this->load->helper('cookie');
            delete_cookie('fbs_' . $this->fb_connect->appkey, '', '', '');
            if (isset($_COOKIE['fbs_' . $this->fb_connect->appkey])) {
                unset($_COOKIE['fbs_' . $this->fb_connect->appkey]);
            }
            redirect('invites');
        }
        $data['friend_list'] = $this->fb_connect->friends;
        //$data['friend_list'] = array();
        $data['unfollow_list'] = '';
        $data['follow_list'] = '';
        $view = 'invite_email';
     
        if ($google_setting->google_enable) {
            $view = 'invite_gmail';
        }
        if ($facebook_setting->facebook_login_enable) {
            $view = 'invite_facebook';
        }

        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/invites/' . $view, $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
    }

    /*
    Function name :close()
    Parameter :none
    Return : none
    Use : to close facebook window

    */
    public function close()
    {
        echo "<script>window.close();</script>";
    }

    /*
    Function name :facebook()
    Parameter :$msg (message string)
    Return : none
    Use : to get facebook friend from facebook

    */
    public function facebook($msg = '')

    {
        if ($this->session->userdata('user_id') == '') {
            redirect('home/login');
        }
        $uid = $this->session->userdata('user_id');
        $user_info = get_user_detail($uid);
        if (!$user_info) {
            redirect('home/login');
        }
        $data['user_info'] = $user_info;
        $data['select'] = 'facebook';
        $data['msg'] = $msg;
        $data['header_menu'] = dynamic_menu(0);
        $data['footer_menu'] = dynamic_menu_footer(0);
        $data['right_menu'] = dynamic_menu_right(0);
        $meta_setting = meta_setting();
        $site_setting = site_setting();
        $data['site_setting'] = $site_setting;
        $data['friend_list'] = $this->fb_connect->friends;
        $data['unfollow_list'] = '';
        $data['follow_list'] = '';
        $pageTitle = $site_setting['site_name'] . ' / invites / ' . $meta_setting['title'];
        $metaDescription = $site_setting['site_name'] . ' / invites / ' . $meta_setting['meta_description'];
        $metaKeyword = $site_setting['site_name'] . ' / invites / ' . $meta_setting['meta_keyword'];
        $this->template->write('meta_title', $pageTitle, TRUE);
        $this->template->write('meta_description', $metaDescription, TRUE);
        $this->template->write('meta_keyword', $metaKeyword, TRUE);

        if ($this->agent->is_mobile('iphone') && $data['site_setting']['mobile_site'] == 1) {
            $this->template->set_master_template('iphone/template.php');
            $this->template->write_view('main_content', 'iphone/invites/invitefacebook', $data, TRUE);
            $this->template->render();
        } elseif (($this->agent->is_mobile() || $this->agent->is_robot()) && $data['site_setting']['mobile_site'] == 1) {
            $this->template->set_master_template('mobile/template.php');
            $this->template->write_view('main_content', 'mobile/invites/invitefacebook', $data, TRUE);
            $this->template->render();
        } else {
            $this->template->write_view('header', 'default/header_login', $data, TRUE);
            $this->template->write_view('main_content', 'default/invites/invitefacebook', $data, TRUE);
            $this->template->write_view('footer', 'default/footer', $data, TRUE);
            $this->template->render();
        }
    }

    /*
    Function name :email()
    Parameter :$msg (message string)
    Return : none
    Use : to send invititation via email

    */
    public function email($msg = '')

    {
        if ($this->session->userdata('user_id') == '') {
            redirect('home/login');
        }
        $uid = $this->session->userdata('user_id');
        $user_info = UserData($uid);
        if (!$user_info) {
            redirect('home/login');
        }
        $data['user_info'] = $user_info;
        $meta = meta_setting();
        $data['site_setting'] = site_setting();
        $facebook_setting = facebook_setting();
        $google_setting = google_setting();
      
        $data['facebook_enable'] = $facebook_setting->facebook_login_enable;
        $data['google_enable'] = $google_setting->google_enable;
        
        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/invites/invite_email', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
    }

    /*
    Function name :gmail()
    Parameter :none
    Return : none
    Use : to send invititation via gmail account

    */
    public function gmail()
    {
        $uid = check_user_authentication(true);


        $user_info = UserData($uid);
        if (!$user_info) {
            redirect('home/login');
        }
        $data['user_info'] = $user_info;
        $data = array();
        if (isset($_GET['error'])) {
            if ($_GET['error'] == 'access_denied') {
                redirect('invites');
            }
        }
        $meta_setting = meta_setting();
        $site_setting = site_setting();
        $data['site_setting'] = $site_setting;
        $facebook_setting = facebook_setting();
        $google_setting = google_setting();
       
        $data['facebook_enable'] = $facebook_setting->facebook_login_enable;
        $data['google_enable'] = $google_setting->google_enable;
       
        if ($google_setting->google_enable == 0) {
            redirect('invites/index');
        }
        $debug = 1;
        $argarray = array();
        $consumer_key = $google_setting->consumer_key;
        $consumer_secret = $google_setting->consumer_secret;
        $callback = site_url('invites/gmail');
        $emails_count = '5000';
        $data['gmail_connect'] = 'https://www.google.com/accounts/OAuthAuthorizeToken?oauth_token=';
        $data['friend_list'] = '';
        $data['unfollow_list'] = '';
        $data['follow_list'] = '';
        require_once APPPATH . '/third_party/oauth-api/http.php';

        require_once APPPATH . '/third_party/oauth-api/oauth_client.php';

        $client = new oauth_client_class;
        $client->server = 'Google';
        // set the offline access only if you need to call an API
        // when the user is not present and the token may expire
        $client->offline = true;
        $client->debug = false;
        $client->debug_http = true;
        $client->redirect_uri = $callback;
        $client->client_id = $consumer_key;
        $application_line = __LINE__;
        $client->client_secret = $consumer_secret;
        if (strlen($client->client_id) == 0 || strlen($client->client_secret) == 0) die('Please go to Google APIs console page ' . 'http://code.google.com/apis/console in the API access tab, ' . 'create a new client ID, and in the line ' . $application_line . ' set the client_id to Client ID and client_secret with Client Secret. ' . 'The callback URL must be ' . $client->redirect_uri . ' but make sure ' . 'the domain is valid and can be resolved by a public DNS.');
        /* API permissions
        */
        $client->scope = 'https://www.googleapis.com/auth/userinfo.email ' . 'https://www.googleapis.com/auth/userinfo.profile ' . 'https://www.google.com/m8/feeds';

        if (($success = $client->Initialize())) {
            if (($success = $client->Process())) {
                if (strlen($client->authorization_error)) {
                    $client->error = $client->authorization_error;
                    $success = false;
                } elseif (strlen($client->access_token)) {
                    $success = $client->CallAPI('https://www.googleapis.com/oauth2/v1/userinfo', 'GET', array(), array(
                        'FailOnAccessError' => true
                    ), $user);
                    $success = $client->CallAPI('https://www.google.com/m8/feeds/contacts/default/full', 'GET', array(
                        'max-results' => 1000
                    ), array(
                        'FailOnAccessError' => true
                    ), $user1);
                }
            }
            $success = $client->Finalize($success);
        }
        if ($success) {
            $xml = simplexml_load_string($user1);
            $xml->registerXPathNamespace('gd', 'http://schemas.google.com/g/2005');
            $output_array = array();
            $friend_list = array();
            $unfollow_list = array();
            $follow_list = array();
            $all_email = array();
            $all_name = array();
            $multiple_name = '';

            foreach ($xml->entry as $entry) {
           
                foreach ($entry->xpath('gd:email') as $email) {
                    $output_array[] = array(
                        (string)$entry->title,
                        (string)$email->attributes()->address
                    );
                    $name = '';
                    if (isset($entry->title)) {
                        $name = (string)$entry->title;
                    }


                    $email["address"] = (string)$email->attributes()->address;
                    // /====check follow==
                    $query = $this->db->query("select * from " . $this->db->dbprefix('user us') . " where us.email='" . addslashes($email["address"]) . "'");
                    if ($query->num_rows() > 0) {
                        $res = $query->row();
                        $unfollow_list[] = $res->user_id;
                    } // //====
                    else {
                        $all_email[] = $email["address"];
                        $friend_list[] = $email["address"] . '||' . $name;
                        if ($name != '') {
                            $multiple_name .= $name . ',';
                        }
                    }
                }
            }

            /// Convert All email into one string for Brodcast Mail
            $multiple_email = '';
            $i = 1;
            $first = '';
            foreach ($all_email as $value) {
                if ($i == count($all_email)) {
                    $multiple_email .= $value['0'];
                } else {
                    $multiple_email .= $value['0'] . ', ';
                }
                $i++;
            }

            $data['multiple_email'] = $multiple_email;
            $data['multiple_name'] = $multiple_name;

            $data['friend_list'] = $friend_list;
            $data['unfollow_list'] = $unfollow_list;
            $data['follow_list'] = $follow_list;
            $data['gmail_connect'] = '';
            // echo '<pre>', HtmlSpecialChars(print_r($output_array, 1)), '</pre>';
        } else {
            echo HtmlSpecialChars($client->error);
            die;
        }

        $this->template->write('meta_title', $meta_setting['title'], TRUE);
        $this->template->write('meta_description', $meta_setting['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta_setting['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/invites/invite_gmail', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
    }

    /*
    Function name :sendinvitation()
    Parameter :none
    Return : none
    Use : to send invititation via email using multiple email address

    */
    function sendinvitation()
    {
        if ($this->session->userdata('user_id') == '') {
            redirect('home/login');
        }
        $uid = $this->session->userdata('user_id');
        $user_info = UserData($uid);
        if (!$user_info) {
            redirect('home/login');
        }
        $recipient_name = trim(strip_tags($_REQUEST['rname']));
        $recipient_email = trim(strip_tags($_REQUEST['remail']));
        $recipient_message = trim(strip_tags($_REQUEST['rmessage']));
        if ($recipient_email) {
            $code = $user_info[0]['unique_code'];
            $data = array(
                'invite_code' => $code,
                'invite_email' => $recipient_email,
                'invite_date' => date('Y-m-d H:i:s'),
                'invite_ip' => getRealIP(),
                'invite_by' => $uid
            );
            $this->db->insert('invite_request', $data);
            $response = array(
                'status' => 'success'
            );
            $invitation_link = '<a href="' . site_url('invited/' . $code . '/' . base64_encode($recipient_email)) . '">';
            $end_invitation_link = '</a>';
            $login_user_name = ucfirst($user_info[0]['user_name']) . ' ' . ucfirst($user_info[0]['last_name']);
            $email_template = $this->db->query("select * from " . $this->db->dbprefix('email_template') . " where task='Email Invitation'");
            $email_temp = $email_template->row();
            if ($email_temp) {
                $email_address_from = $email_temp->from_address;
                $email_from_name = '';
                $email_address_reply = $email_temp->reply_address;
                $email_subject = $email_temp->subject;
                $email_message = $email_temp->message;
                $email_subject = str_replace('{login_user_name}', $login_user_name, $email_subject);
                $email_to = $recipient_email;
                $email_address_from = $user_info[0]['email'];
                $email_message = str_replace('{break}', '<br/>', $email_message);
                $email_message = str_replace('{invitation_link}', $invitation_link, $email_message);
                $email_message = str_replace('{end_invitation_link}', $end_invitation_link, $email_message);
                $email_message = str_replace('{message}', $recipient_message, $email_message);
                $email_message = str_replace('{login_user_name}', $login_user_name, $email_message);
                $str = $email_message;
                /** custom_helper email function **/
                if ($email_to) {
                    email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str, $email_from_name);
                }
            }
        } else {
            $response = array(
                'status' => 'fail'
            );
        }
        echo json_encode($response);
        exit; // only print out the json version of the response
    }

    /*
    Function name :sendinvitation_all()
    Parameter :none
    Return : none
    Use : to send invititation via email using multiple email address

    */
    function sendinvitation_all()
    {
        if ($this->session->userdata('user_id') == '') {
            redirect('home/login');
        }
        $uid = $this->session->userdata('user_id');
        $user_info = UserData($uid);
        if (!$user_info) {
            redirect('home/login');
        }
        $recipient_name = trim(strip_tags($_REQUEST['rname']));
        $recipient_email = ($_REQUEST['remail']);
        $recipient_message = trim(strip_tags($_REQUEST['rmessage']));
        if ($recipient_email) {
            
            
            $login_user_name = ucfirst($user_info[0]['user_name']) . ' ' . ucfirst($user_info[0]['last_name']);
            $email_template = $this->db->query("select * from " . $this->db->dbprefix('email_template') . " where task='Email Invitation'");
            $email_temp = $email_template->row();
            
            $code = $user_info[0]['unique_code'];
            
            
            $exp_recipient_email=explode(',',$recipient_email);
            
            
            if(!empty($exp_recipient_email)){
                
                foreach($exp_recipient_email as $invite_user_email){
                
                    $data = array(
                        'invite_code' => $code,
                        'invite_email' => $invite_user_email,
                        'invite_date' => date('Y-m-d H:i:s'),
                        'invite_ip' => getRealIP(),
                        'invite_by' => $uid
                    );
                    $this->db->insert('invite_request', $data);

                    $invitation_link = '<a href="' . site_url('invited/' . $code . '/' . base64_encode($invite_user_email)) . '">';
                    $end_invitation_link = '</a>';

                    if ($email_temp) {
                        $email_address_from = $email_temp->from_address;
                        $email_from_name = '';
                        $email_address_reply = $email_temp->reply_address;
                        $email_subject = $email_temp->subject;
                        $email_message = $email_temp->message;
                        $email_subject = str_replace('{login_user_name}', $login_user_name, $email_subject);
                        $email_to = $invite_user_email;
                        $email_address_from = $user_info[0]['email'];
                        $email_message = str_replace('{break}', '<br/>', $email_message);
                        $email_message = str_replace('{invitation_link}', $invitation_link, $email_message);
                        $email_message = str_replace('{end_invitation_link}', $end_invitation_link, $email_message);
                        $email_message = str_replace('{message}', $recipient_message, $email_message);
                        $email_message = str_replace('{login_user_name}', $login_user_name, $email_message);
                        $str = $email_message;
                        /** custom_helper email function **/

                        email_send($email_address_from, $email_address_reply, $invite_user_email, $email_subject, $str, $email_from_name);

                    }
                                
                }
            }
            
            
            $response = array(
                'status' => 'success'
            );
            
            
        } else {
            $response = array(
                'status' => 'fail'
            );
        }
        echo json_encode($response);
        exit; // only print out the json version of the response
    }

    /*
    Function name :sendemailinvitation()
    Parameter :none
    Return : none
    Use : to send invititation via email

    */
    function sendemailinvitation()
    {
        $email = 0;
        if ($this->session->userdata('user_id') == '') {
            redirect('home/login');
        }
        $uid = $this->session->userdata('user_id');
        $user_info = UserData($uid);
        if (!$user_info) {
            redirect('home/login');
        }
        $recipient_email = trim(strip_tags($_REQUEST['remail']));
        $recipient_message = nl2br(trim(strip_tags($_REQUEST['rmessage'])));
        if ($user_info[0]['email'] == $recipient_email) {
            $email = 1;
        }
        $check_invite_email = $this->db->get_where('invite_request', array(
            'invite_email' => $recipient_email
        ));
        if ($check_invite_email->num_rows() > 0) {
            $email = 1;
        }
        if ($email == 0) {
            if ($recipient_email) {
                $code = $user_info[0]['unique_code'];
                $data = array(
                    'invite_code' => $code,
                    'invite_email' => $recipient_email,
                    'invite_date' => date('Y-m-d H:i:s'),
                    'invite_ip' => getRealIP(),
                    'invite_by' => $uid
                );
                $this->db->insert('invite_request', $data);
                $response = array(
                    'status' => 'success',
                    'email' => $recipient_email
                );
                $invitation_link = '<a href="' . site_url('invited/' . $code . '/' . base64_encode($recipient_email)) . '">';
                $end_invitation_link = '</a>';
                $login_user_name = ucfirst($user_info[0]['user_name']) . ' ' . ucfirst($user_info[0]['last_name']);
                // /===report comment to pinner
                $email_template = $this->db->query("select * from " . $this->db->dbprefix('email_template') . " where task='Email Invitation'");
                $email_temp = $email_template->row();
                if ($email_temp) {
                    $email_address_from = $email_temp->from_address;
                    $email_from_name = '';
                    $email_address_reply = $email_temp->reply_address;
                    $email_subject = $email_temp->subject;
                    $email_message = $email_temp->message;
                    $email_subject = str_replace('{login_user_name}', $login_user_name, $email_subject);
                    $email_address_from = $user_info[0]['email'];
                    $email_to = $recipient_email;
                    $email_message = str_replace('{break}', '<br/>', $email_message);
                    $email_message = str_replace('{invitation_link}', $invitation_link, $email_message);
                    $email_message = str_replace('{end_invitation_link}', $end_invitation_link, $email_message);
                    $email_message = str_replace('{message}', $recipient_message, $email_message);
                    $email_message = str_replace('{login_user_name}', $login_user_name, $email_message);
                    $str = $email_message;
                    /** custom_helper email function **/
                    if ($email_to) {
                        email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str, $email_from_name);
                    }
                }
            } else {
                $response = array(
                    'status' => 'fail'
                );
            }
        } else if ($user_info[0]['email'] == $recipient_email) {
            $response = array(
                'status' => 'not_invite',
                'email' => $recipient_email
            );
        } else if ($recipient_message == '') {
            $response = array(
                'status' => 'message_required'
            );
        } else {
            $response = array(
                'status' => 'exist',
                'email' => $recipient_email
            );
        }
        echo json_encode($response);
        exit; // only print out the json version of the response
    }

    /*
    Function name :send_invite()
    Parameter :none
    Return : none
    Use : page load using ajax

    */
    function send_invite($email, $name = '')
    {
        $data = array();
        $data['email'] = $email;
        $data['name'] = str_replace('WEKREV', '(', str_replace('KQWER', ')', urldecode($name)));
        $meta_setting = meta_setting();
        $site_setting = site_setting();
        $data['site_setting'] = $site_setting;
        $this->load->view('default/invites/send_invite', $data);
    }

    /*
    Function name :send_invite()
    Parameter :none
    Return : none
    Use : Invitation send

    */
    function send_invite_all()
    {

        $uid = check_user_authentication(true);

        $user_info = UserData($uid);
        if (!$user_info) {
            redirect('home/login');
        }
        $data = array();
        $meta_setting = meta_setting();
        $site_setting = site_setting();
        $data['site_setting'] = $site_setting;
        $facebook_setting = facebook_setting();
        $google_setting = google_setting();
      
        $data['facebook_enable'] = $facebook_setting->facebook_login_enable;
        $data['google_enable'] = $google_setting->google_enable;
      

        $data['emailarray'] = $this->input->post('check');
        $name = array();
        $email_array = array();
        foreach ($data['emailarray'] as $email) {
            $email_ex = explode('/', $email);
            array_push($name, $email_ex[1]);
            array_push($email_array, $email_ex[0]);
        }
        $data['namearray'] = $name;

        $data['email'] = implode(', ', $email_array);
        $data['name'] = implode(', ', $name);

        $this->template->write('meta_title', $meta_setting['title'], TRUE);
        $this->template->write('meta_description', $meta_setting['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta_setting['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/invites/send_invite_all', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
    }
}

?>
