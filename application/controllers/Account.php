<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 * The Home class is basically the default controller for the script.
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Account extends ROCKERS_Controller

{
    /**
     *
     */
    function __construct()
    {
        parent::__construct();
        $this->load->model('home_model');
        $this->load->model('equity_model');

        $this->load->model('account_model');
        $this->load->library('securimage');
        $this->load->library('pagination');
    }

    /*
    Function name :index()
    Parameter :$msg (message string)
    Return : none
    Use : to show the user detail
    Description : this is used to show the user detail and notification on account page
    */
    /**
     * @param string $msg
     */
    function index()
    {
        $user_id = check_user_authentication(true);

        $data = array();
       
        $meta = meta_setting();
        $data['site_setting'] = site_setting();
      
        $usedata = UserData($user_id, $join = array('user_notification'));
      
        $data['user_id'] = $usedata[0]['user_id'];
        $data['user_name'] = $usedata[0]['user_name'];
        $data['last_name'] = $usedata[0]['last_name'];
        $data['user_type'] = json_decode($usedata[0]['user_type']);

        $data['email'] = $usedata[0]['email'];
        $data['address'] = $usedata[0]['address'];
        if (is_file('upload/user/user_medium_image/' . $usedata[0]['image'])) $data['image'] = $usedata[0]['image'];
        else $data['image'] = '';
        if (isset($usedata[0]['zip_code']) and $usedata[0]['zip_code'] != 0 or $usedata[0]['zip_code'] != '') $data['zip_code'] = $usedata[0]['zip_code'];
        else $data['zip_code'] = '';
        $data['user_about'] = $usedata[0]['user_about'];
        $data['user_occupation'] = $usedata[0]['user_occupation'];
        $data['user_interest'] = $usedata[0]['user_interest'];
        $data['user_skill'] = $usedata[0]['user_skill'];
        $data['id'] = $usedata[0]['id'];
        $data['profile_slug'] = $usedata[0]['profile_slug'];
        $data['mobile_number'] = $usedata[0]['mobile_number'];
        $data['company_name'] = $usedata[0]['company_name'];
        $data['abn_number'] = $usedata[0]['abn_number'];
        $data['company_address'] = $usedata[0]['company_address'];

        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/account/index', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
    }

    /*
    Function name :commentdelete()
    Parameter :none
    Return : none
    Use :  Delete comment from equity
    */
    function commentdelete()
    {

        check_user_authentication(true);
        $comment_id = SecurePostData($this->input->post('comment_id'));
        $type = SecurePostData($this->input->post('type'));
        $data["msg"]['error'] = '';
        $data["msg"]['success'] = '';
        $data1['comment_delete'] = $this->equity_model->commentaction($comment_id, $type, 0, 0, '', '', '', '');
        $data["msg"]["success"] = '<span>' . RECORD_DELETED_SUCCESSFULLY . '</span>';

        echo json_encode($data);
    }

    /*
    Function name :validate_slug()
    Parameter :none
    Return : none
    Use :  Profile slug validation which used for profile page
    */
    function validate_slug()
    {

        check_user_authentication(true);
        $profile_slug = SecurePostData($this->input->post('profile_slug'));

        $data["msg"]['error'] = '';
        $data["msg"]['success'] = '';

        $profile_slug_check = $this->account_model->check_slug_exist($profile_slug);
        if ($profile_slug_check == "1") {
            $data["msg"]["error"] = '<span>' . THIS_SLUG_IS_ALREADY_EXIST_PLEASE_ENTER_NEW_SLUG . '</span>';
        }

        echo json_encode($data);

    }

    /*
    Function name :UserImageAjax()
    Parameter :id , type
    Return : none
    Use : Upload image using ajax for user;
    */
    /**
     * @param null $id
     * @param int $type
     */
    function UserImageAjax($id = null, $type = 0)
    {
        check_user_authentication(true);

        $this->load->library('form_validation');
        $data = array();
        $data["msg"]["error"] = '';
        $data["msg"]["success"] = '';
        $data["redirect"]["url"] = '';
        $data["image"]["path"] = '';
        $data["msg"]["image_media"] = '';
        $data["msg"]["video_media"] = '';


        if ($this->session->userdata('user_id') == '') {
            $data["redirect"]["url"] = 'home/login';
        } else
            if ($id == null || $id == '' || $id == '0') {
                $data["redirect"]["url"] = "account";
            } else {


                switch ($type) {
                    case 0:
                        $_FILES['userfile']['name'] = $_FILES['file1']['name'];
                        $_FILES['userfile']['type'] = $_FILES['file1']['type'];
                        $_FILES['userfile']['tmp_name'] = $_FILES['file1']['tmp_name'];
                        $_FILES['userfile']['error'] = $_FILES['file1']['error'];
                        $_FILES['userfile']['size'] = $_FILES['file1']['size'];
                         $image_settings = get_image_setting_data();
                        // image validation

                        if ($_FILES["userfile"]["type"] != "image/jpeg" and $_FILES["userfile"]["type"] != "image/pjpeg" and $_FILES["userfile"]["type"] != "image/png" and $_FILES["userfile"]["type"] != "image/x-png" and $_FILES["userfile"]["type"] != "image/gif") {
                            $data["msg"]["error"] = PLEASE_UPLOAD_A_JPG_PNG_GIF_FILE;
                        } else
                            if ($_FILES["userfile"]["size"] > $image_settings['upload_limit']*1000000) {
                                $data["msg"]["error"] = sprintf(SORRY_THIS_FILE_IS_TOO_LARGE_PLEASE_SELECT_A_JPG_FILE_THAT_IS_LESS_THAN_MB_OR_TRY_RESIZING_IT_USING_A_PHOTO_EDITOR,$image_settings['upload_limit']);
                            } else {
                                $imagename = UserImageUpload($_FILES);
                                $data_gallery = array(
                                    'image' => $imagename
                                );

                                $new_img = $this->account_model->UpdateAccount('user_id', $id, 'user', $data_gallery);
                                $cache_file_name = 'user_detail' . $this->session->userdata('user_id');
                                setting_deletecache($cache_file_name);
                                $cache_file_name = 'user_detail_notification' . $this->session->userdata('user_id');
                                setting_deletecache($cache_file_name);
                                if ($new_img) {
                                    setting_deletecache('user_detail_notification' . $this->session->userdata('user_id'));
                                    $data["image"]["path"] = base_url() . "upload/user/user_big_image/" . $imagename;
                                    $data["msg"]["success"] = '<span>' . IMAGE_UPLOADED_SUCCESSFULLY . '</span>';
                                } else {
                                    $data["msg"]["error"] = THERE_IS_SOME_PROBLEM_SITE_TRY_AFTER_SOMETIME;
                                }
                            }

                        $data["msg"]["successbox"] = 1;
                        break;
                }
            }

        echo json_encode($data);
    }

    /*
    Function name :validate_account()
    Parameter : None
    Return : none
    Use : to validate the user input.
    Description : to validate the user detail while user update their account
    */
    function validate_account()
    {


       
        if(!$_POST) redirect('account');
        check_user_authentication(true);
        $data = array();
        $user_id = $this->session->userdata('user_id');
        $usedata = UserData($user_id, $join = array(
            'user_notification'
        ));
        $data['email'] = $usedata[0]['email'];
        $this->load->library('form_validation');
        $this->form_validation->set_rules('user_name', FIRST_NAME, 'required|trim|max_length[40]');
        $this->form_validation->set_rules('last_name', LAST_NAME, 'required|trim|max_length[40]');
        $this->form_validation->set_rules('mobile_number', 'Mobile Number', 'required|is_numeric');
        $this->form_validation->set_rules('address', ADDRESS, 'required|trim');
        $this->form_validation->set_rules('profile_slug', 'profile Slug is required', 'required|trim');
        $this->form_validation->set_rules('zip_code', ZIPCODE, 'required');
        $this->form_validation->set_rules('user_about', ABOUT_YOURSELF, 'trim|at_least_one_letter');
        $this->form_validation->set_rules('user_occupation', OCCUPATION, 'trim|at_least_one_letter');
      
        if(in_array(2, $this->input->post('user_type')) || in_array(4, $this->input->post('user_type')))
        {

            if(in_array(2, $this->input->post('user_type')))
            {
                $user_type_post = 2;
            }
            else if(in_array(4, $this->input->post('user_type')))
            {
                $user_type_post = 4;
            }
            
            if($user_type_post == 2 || $user_type_post == 4)
            {
               
                $this->form_validation->set_rules('company_name', 'Company Name', 'required');
                $this->form_validation->set_rules('abn_number', 'ABN Number', 'required');
                $this->form_validation->set_rules('company_address', 'Company Address', 'required');
            }
        }
        $abn_numer_error='';
        if($this->input->post('abn_number') != '')
        {  
            if($this->isValidAbn($this->input->post('abn_number')) == '')
            {
                $abn_numer_error = "Please enter valid ABN Number";
            }
        }
        if ($this->form_validation->run() && $abn_numer_error == '')  {

            $data['user_name'] = SecurePostData($this->input->post('user_name'));
            $data['last_name'] = SecurePostData($this->input->post('last_name'));
            $data['profile_slug'] = SecurePostData($this->input->post('profile_slug'));
            $data['address'] = SecurePostData($this->input->post('address'));
            $data['image'] = $usedata[0]['image'];
            $data['zip_code'] = SecurePostData($this->input->post('zip_code'));
            $data['user_about'] = $this->input->post('user_about');
            $data['user_occupation'] = $this->input->post('user_occupation');
            $data['mobile_number'] = $this->input->post('mobile_number');
            $data['company_name'] = $this->input->post('company_name');
            $data['abn_number'] = $this->input->post('abn_number');
            $data['company_address'] = $this->input->post('company_address');

            $user_type = json_encode($this->input->post('user_type'));
            $data_user_update =
                array(
                    'user_name' => SecurePostData($this->input->post('user_name')),
                    'last_name' => SecurePostData($this->input->post('last_name')),
                    'profile_slug' => SecurePostData($this->input->post('profile_slug')),
                    'mobile_number' => SecurePostData($this->input->post('mobile_number')),
                    'address' => SecurePostData($this->input->post('address')),
                    'image' => $usedata[0]['image'],
                    'zip_code' => SecurePostData($this->input->post('zip_code')),
                    'user_about' => $this->input->post('user_about'),
                    'user_occupation' => $this->input->post('user_occupation'),
                    'user_type' => $user_type,
                    'company_name'=>SecurePostData($this->input->post('company_name')),
                    'abn_number'=>SecurePostData($this->input->post('abn_number')),
                    'company_address'=>$this->input->post('company_address'),
                  
                );

            $profile_slug = SecurePostData($this->input->post('profile_slug'));

            $result_profile_slug = $this->account_model->check_slug_exist($profile_slug);

            // if ($result_profile_slug == "0") {
            //      $this->session->set_flashdata('error_message_account', PROFILE_SLUG_ALREADY_EXIST);
            //     redirect('account');
            // } else {
                if ($this->account_model->UpdateAccount('user_id', SecurePostData($this->input->post('user_id')), 'user', $data_user_update)) {
                    user_activity('update_account', $this->session->userdata('user_id'), 0, 0);
                    $cache_file_name = 'user_detail' . $this->session->userdata('user_id');
                    setting_deletecache($cache_file_name);
                    $cache_file_name = 'user_detail_notification' . $this->session->userdata('user_id');
                    setting_deletecache($cache_file_name);
                    $data_session = array(
                        'user_name' => $data['user_name'],
                        'last_name' => $data['last_name'],
                        'email' => $data['email'],
                    );
                    $this->session->set_userdata($data_session);

                    
                    $result = UserData($user_id);
                    // salesforce api call for new user 
                    $this->load->library("salesforce_api");

                    $temp_usertype = json_decode($result[0]['user_type']);
                    $isInvestor=$isREAgent=$isOwner=$isDeveloper=false;

                    if(in_array(1, $temp_usertype)){
                        $isInvestor=true;
                    }
                    if(in_array(2, $temp_usertype)){
                        $isREAgent = true;
                    }
                    if(in_array(3, $temp_usertype)){
                        $isOwner = true;
                    }
                    if(in_array(4, $temp_usertype)){
                        $isDeveloper = true;
                    }

                    $residentialInterest=$commercialInterest=$industrialInterest=$ruralInterest=false;

                    if(in_array(1, $temp_usertype) && count($temp_usertype)==1){
                        $mobile_number = "976543210";
                        $zip_code = "32525";
                        $address = "test";
                        if($result[0]['mobile_number']!=''){
                            $mobile_number = $result[0]['mobile_number'];
                        }
                        if($result[0]['zip_code']!=''){
                            $zip_code = $result[0]['zip_code'];
                        }
                        if($result[0]['address']!=''){
                            $address = $result[0]['address'];
                        }

                        $require_data_array = array(
                            "salesforceUserId" => $result[0]['salesforceuserid'],
                            "covestaUserId" => $result[0]['user_id'],
                            "firstName" => $result[0]['user_name'],
                            "lastName" => $result[0]['last_name'],
                            "email" => $result[0]['email'],
                            "mobile" => $mobile_number,
                            "postcode" => $zip_code,
                            "address" => $address,
                            "isDeveloper" => $isDeveloper,
                            "isREAgent" => $isREAgent,
                            "isInvestor" => $isInvestor,
                            "isOwner" => $isOwner,
                            "residentialInterest" => $residentialInterest,
                            "commercialInterest" => $commercialInterest,
                            "industrialInterest" => $industrialInterest,
                            "ruralInterest" => $ruralInterest
                        );
                    } else {
                        if($result[0]['company_name']!=''){
                        $company_name = $result[0]['company_name'];
                        }
                        if($result[0]['abn_number']!=''){
                            $abn_number = $result[0]['abn_number'];
                        }
                        if($result[0]['company_website']!=''){
                            $company_website = $result[0]['company_website'];
                        }
                        if($result[0]['company_address']!=''){
                            $company_address = $result[0]['company_address'];
                        }
                        if($result[0]['mobile_number']!=''){
                            $mobile_number = $result[0]['mobile_number'];
                        }
                        if($result[0]['zip_code']!=''){
                            $zip_code = $result[0]['zip_code'];
                        }
                        if($result[0]['address']!=''){
                            $address = $result[0]['address'];
                        }

                        $require_data_array = array(
                            "salesforceUserId" => $result[0]['salesforceuserid'],
                            "covestaUserId" => $result[0]['user_id'],
                            "companyName" => $company_name,
                            "ABN" => $abn_number,
                            "companyWebsite" => $company_website,
                            "companyAddress" => $company_address,
                            "firstName" => $result[0]['user_name'],
                            "lastName" => $result[0]['last_name'],
                            "email" => $result[0]['email'],
                            "mobile" => $mobile_number,
                            "postcode" => $zip_code,
                            "address" => $address,
                            "isDeveloper" => $isDeveloper,
                            "isREAgent" => $isREAgent,
                            "isInvestor" => $isInvestor,
                            "isOwner" => $isOwner,
                            "residentialInterest" => $residentialInterest,
                            "commercialInterest" => $commercialInterest,
                            "industrialInterest" => $industrialInterest,
                            "ruralInterest" => $ruralInterest
                        );
                    }

                    

                    // $temp_user_interest = json_decode($result[0]->user_interest);
                    

                    

                    $salesforceUserId = $this->salesforce_api->update_existing_user($require_data_array);


                    $this->session->set_flashdata('success_message_account', RECORD_UPDATED_SUCCESSFULLY);
                    redirect('account');
                }
           // }
        } else {

            
            $meta = meta_setting();
            $data['site_setting'] = site_setting();


            $this->session->set_flashdata('error_message_account', validation_errors().$abn_numer_error);
            $data['user_id'] = $this->input->post('user_id');
            $data['user_name'] = $this->input->post('user_name');
            $data['last_name'] = $this->input->post('last_name');
            $data['profile_slug'] = $this->input->post('profile_slug');
            $data['address'] = $this->input->post('address');
            $data['image'] = $usedata[0]['image'];
            $user_type = json_encode($this->input->post('user_type'));
            $data['user_type'] =$user_type;
            $data['zip_code'] = $this->input->post('zip_code');
            $data['user_about'] = $this->input->post('user_about');
            $data['user_occupation'] = $this->input->post('user_occupation');
            $data['company_name'] = $this->input->post('company_name');
            $data['abn_number'] = $this->input->post('abn_number');
            $data['company_address'] = $this->input->post('company_address');
            $data['mobile_number'] = $usedata[0]['mobile_number'];
            $data['id'] = $usedata[0]['id'];
         
            $this->template->write('meta_title', $meta['title'], TRUE);
            $this->template->write('meta_description', $meta['meta_description'], TRUE);
            $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
            $this->template->write_view('header', 'default/common/header', $data, TRUE);
            $this->template->write_view('main_content', 'default/account/index', $data, TRUE);
            $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
            $this->template->render();
        }
    }
      /**
   * Validate an Australian Business Number (ABN)
   * @param string $abn
   * @link http://www.ato.gov.au/businesses/content.asp?doc=/content/13187.htm
   * @return bool True if $abn is a valid ABN, false otherwise
   */
    function isValidAbn($abn) {
        $weights = array(10, 1, 3, 5, 7, 9, 11, 13, 15, 17, 19);
        // Strip non-numbers from the acn
        $abn = preg_replace('/[^0-9]/', '', $abn);
        // Check abn is 11 chars long
        if(strlen($abn) != 11) {
          return false;
        }
        // Subtract one from first digit
        $abn[0] = ((int)$abn[0] - 1);
        // Sum the products
        $sum = 0;
        foreach(str_split($abn) as $key => $digit) {
          $sum += ($digit * $weights[$key]);
        }
        if(($sum % 89) != 0) {
          return false;
        }
        return true;
      }
  /**
   * Validate an Australian Business Number (ABN)
   * @param string $abn
   * @link http://www.ato.gov.au/businesses/content.asp?doc=/content/13187.htm
   * @return bool True if $abn is a valid ABN, false otherwise
   */
    function CheckValidAbn() 
    {

        $abn = $this->input->post('abnnumber');
        $weights = array(10, 1, 3, 5, 7, 9, 11, 13, 15, 17, 19);
        // Strip non-numbers from the acn
        $abn = preg_replace('/[^0-9]/', '', $abn);
        // Check abn is 11 chars long
        if(strlen($abn) != 11) {
            $data["error"] = "error";
            echo json_encode($data);
            die;
        }
        // Subtract one from first digit
        $abn[0] = ((int)$abn[0] - 1);
        // Sum the products
        $sum = 0;
        foreach(str_split($abn) as $key => $digit) {
          $sum += ($digit * $weights[$key]);
        }
        if(($sum % 89) != 0) {
           $data["error"] = "error";
           echo json_encode($data);
           die;
        }
            $data["success"] = "success";
            echo json_encode($data);
            die;
    }


    /*
    Function name :notification()
    Parameter : None
    Return : none
    Use : to update user notification.
    Description : to update user notification in account page
    */
    function notification()
    {
       
        $user_id = check_user_authentication(true);
        $data = array();
        $meta = meta_setting();
        $site_setting = site_setting();
        $data['site_setting'] = $site_setting;
        $usedata = UserData($user_id, $join = array('user_notification'));
        $data['id'] = $usedata[0]['id'];
        $data['user_id'] = $user_id;
        $data['update_alert'] = $usedata[0]['update_alert'];
        $data['user_alert'] = $usedata[0]['user_alert'];
        $data['add_fund'] = $usedata[0]['add_fund'];
        $data['project_alert'] = $usedata[0]['project_alert'];
        $data['comment_alert'] = $usedata[0]['comment_alert'];
        $data['creator_pledge_alert'] = $usedata[0]['creator_pledge_alert'];
        $data['creator_follow_alert'] = $usedata[0]['creator_follow_alert'];
        $data['creator_newup_alert'] = $usedata[0]['creator_newup_alert'];
        $data['creator_pledge_alert'] = $usedata[0]['creator_pledge_alert'];
        $data['new_follow_alert'] = $usedata[0]['new_follow_alert'];
        $data['new_updates_alert'] = $usedata[0]['new_updates_alert'];
        $data['new_comment_alert'] = $usedata[0]['new_comment_alert'];
        $data['new_pledge_alert'] = $usedata[0]['new_pledge_alert'];
        $data['creator_comment_alert'] = $usedata[0]['creator_comment_alert'];
        $data['social_notification_alert'] = $usedata[0]['social_notification_alert'];
        $data['you_back_alert'] = $usedata[0]['you_back_alert'];
        $data['you_follow_alert'] = $usedata[0]['you_follow_alert'];
        $data['comment_reply_alert'] = $usedata[0]['comment_reply_alert'];

        $data_notification_update = array(
            'user_id'=>$user_id,
            'add_fund'=>SecurePostData($this->input->post('add_fund')),
            'comment_alert'=>SecurePostData($this->input->post('comment_alert')),
            'update_alert'=>SecurePostData($this->input->post('update_alert')),
            'you_follow_alert'=>SecurePostData($this->input->post('you_follow_alert')),
            'new_pledge_alert'=>SecurePostData($this->input->post('new_pledge_alert')),
            'new_comment_alert'=>SecurePostData($this->input->post('new_comment_alert')),
            'new_follow_alert'=>SecurePostData($this->input->post('new_follow_alert')),
            'new_updates_alert'=>SecurePostData($this->input->post('new_updates_alert')),
            'social_notification_alert'=> SecurePostData($this->input->post('social_notification_alert')),
            'creator_pledge_alert'=>SecurePostData($this->input->post('creator_pledge_alert')),
            'creator_comment_alert'=>SecurePostData($this->input->post('creator_comment_alert')),
            'creator_follow_alert'=>SecurePostData($this->input->post('creator_follow_alert')),
            'creator_newup_alert'=>SecurePostData($this->input->post('creator_newup_alert')),
            'comment_reply_alert'=>SecurePostData($this->input->post('comment_reply_alert')),

            );
        if($_POST)
        {

            if ($this->account_model->UpdateAccount('id', SecurePostData($this->input->post('id')), 
                'user_notification', $data_notification_update)) 
            {

                $cache_file_name = 'user_detail' . $this->session->userdata('user_id');
                setting_deletecache($cache_file_name);
                $cache_file_name = 'user_detail_notification' . $this->session->userdata('user_id');
                setting_deletecache($cache_file_name);
                $this->session->set_flashdata('success_message_notification', RECORD_UPDATED_SUCCESSFULLY);
                redirect('account/notification');
            }
        }

           $this->template->write('meta_title', $meta['title'], TRUE);
            $this->template->write('meta_description', $meta['meta_description'], TRUE);
            $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
            $this->template->write_view('header', 'default/common/header', $data, TRUE);
            $this->template->write_view('main_content', 'default/account/notification', $data, TRUE);
            $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
            $this->template->render();
    }

    /*
    Function name :changepassword()
    Parameter : None
    Return : none
    Use : to change password.
    Description : to update user password in account page
    */
    function changepassword()
    {


        $data = array();
        $user_id = check_user_authentication(true);
        $usedata = UserData($user_id, $join = array(
            'user_notification'
        ));
        $meta = meta_setting();
        $data['site_setting'] = site_setting();
            
      
        $this->load->library('form_validation');
        $this->form_validation->set_rules('old_password', OLD_PASSWORD, 'required|trim|min_length[8]|max_length[20]');
        $this->form_validation->set_rules('password', PASSWORD, 'required|trim|min_length[8]|max_length[20]');
        $this->form_validation->set_rules('confirm_password', CON_PASSWORD, 'required|trim|min_length[8]|max_length[20]|matches[password]');
        $match_pass = '';
        if ($_POST) {
            if($this->input->post('password') != '' && $this->input->post('password') != '')
            {
                if ($this->account_model->PasswordMatches() == false) 
                {
                    
                        $match_pass = OLD_PASSWORD_DOESNT_MATCH;
                    
                }
            }   
        }
        if($_POST)
        {

                if ($this->form_validation->run() == TRUE && $match_pass == '') 
                {
                   
                    $user_data['password'] = SecurePostData(md5($this->input->post('password')));
                    if ($this->account_model->UpdateAccount('user_id', $this->session->userdata('user_id'), 'user', $user_data)) {
                     
                        $cache_file_name = 'user_detail' . $this->session->userdata('user_id');
                        setting_deletecache($cache_file_name);
                        $cache_file_name = 'user_detail_notification' . $this->session->userdata('user_id');
                        setting_deletecache($cache_file_name);

                        $email_template = $this->db->query("select * from `email_template` where task='Change Password'");
                        $email_temp = $email_template->row();
                        $email_address_from = $email_temp->from_address;
                        $email_address_reply = $email_temp->reply_address;
                        $email_subject = $email_temp->subject;
                        $email_message = $email_temp->message;
                        $username = $usedata[0]['user_name'];
                        $password = SecureShowData($this->input->post('password'));
                        $email = $usedata[0]['email'];
                        $email_to = $email;
                        $email_message = str_replace('{break}', '<br/>', $email_message);
                        $email_message = str_replace('{user_name}', $username, $email_message);
                        $email_message = str_replace('{password}', $password, $email_message);
                        $email_message = str_replace('{email}', $email, $email_message);
                        $str = $email_message;
                        email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);
                      

                        $this->session->set_flashdata('success_message_change_password', RECORD_UPDATED_SUCCESSFULLY);
                        redirect('account/changepassword');
                    }
                } else {
                    if (validation_errors() || $match_pass != '') {
                       
                         $this->session->set_flashdata('error_message_change_password', validation_errors() .$match_pass);
                    } else {
                        
                    }

                }
            }
            $this->template->write('meta_title', $meta['title'], TRUE);
            $this->template->write('meta_description', $meta['meta_description'], TRUE);
            $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
            $this->template->write_view('header', 'default/common/header', $data, TRUE);
            $this->template->write_view('main_content', 'default/account/change_password', $data, TRUE);
            $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
            $this->template->render();


    }

  

    /*
	Function name :social()
	Parameter : None
	Return : none
	Use : to validate the user input.
	Description : to validate the user detail for social networking data while user update their account
	*/
    function social()
    {

        $data = array();
        $user_id = check_user_authentication(true);
        $usedata = UserData($user_id, $join = array(
            'user_notification'
        ));
        $meta = meta_setting();
        $data['site_setting'] = site_setting();
        $this->load->library('form_validation');
        $this->form_validation->set_rules('user_website', WEBSITE_URL, 'valid_url');

        $data['facebook_url'] = $usedata[0]['facebook_url'];
        $data['twitter_url'] = $usedata[0]['twitter_url'];
        $data['user_website'] = $usedata[0]['user_website'];
        $data['linkedln_url'] = $usedata[0]['linkedln_url'];
        $data['googleplus_url'] = $usedata[0]['googleplus_url'];


        $facebook_link = $this->input->post('facebook_url');
        $twitter_link = $this->input->post('twitter_url');
        $linkedln_url = $this->input->post('linkedln_url');
        $googleplus_url = $this->input->post('googleplus_url');
        
        $fb_error = '';
        $tw_error = '';
        $lin_error = '';
        $googleplus_error = '';
   
        if ($facebook_link !== '') {
            if (!substr_count($facebook_link, 'facebook.com')) {
                $fb_error = INVALID_FACEBOOK_LINK . '<br/>';
            }
        }

        if ($twitter_link != '') {
            if (!substr_count($twitter_link, 'twitter.com')) {
                $tw_error = INVALID_TWITTER_LINK . '<br/>';
            }
        }

        if ($linkedln_url != '') {
            if (!substr_count($linkedln_url, 'linkedin.com')) {
                $lin_error = INVALID_LINKEDIN_LINK . '<br/>';
            }
        }

        if ($googleplus_url != '') {
            if (!substr_count($googleplus_url, 'plus.google.com')) {
                $googleplus_error = INVALID_GOOGLE_PLUS_LINK.'<br/>';
            }
        }

        $facebook_link = $this->input->post('facebook_url');
        $twitter_link = $this->input->post('twitter_url');
        $linkedln_url = $this->input->post('linkedln_url');
        $googleplus_url = $this->input->post('googleplus_url');
      
        $validate_form = 'no';
        if ($facebook_link != '' || $twitter_link != '' || $linkedln_url != '' || $googleplus_url != '' ||  $this->input->post('user_website') != '') {
            $validate_form = 'yes';
        }

        if($_POST)
        {

            if ($validate_form == 'yes') {

                if ($this->form_validation->run() == FALSE || $fb_error != '' || $tw_error != '' || $lin_error != '' || $googleplus_error != '' ) {


                    $data["facebook_url"] = $this->input->post('facebook_url');
                    $data["twitter_url"] = $this->input->post('twitter_url');
                    $data["user_website"] = $this->input->post('user_website');
                    $data["linkedln_url"] = $this->input->post('linkedln_url');
                    $data["googleplus_url"] = $this->input->post('googleplus_url');
                  
                    $data['user_id'] = $this->input->post('user_id');
                    $error = validation_errors() . $fb_error . $tw_error . $lin_error . $googleplus_error;
                     $this->session->set_flashdata('error_message_social', $error);


                } else {

                   
                    $data_update = array(
                        'user_website' => addhttp($this->input->post('user_website')),
                        'facebook_url' => addhttp($this->input->post('facebook_url')),
                        'twitter_url' => addhttp($this->input->post('twitter_url')),
                        'linkedln_url' => addhttp($this->input->post('linkedln_url')),
                        'googleplus_url' => addhttp($this->input->post('googleplus_url')),
                       
                    );

                    $this->account_model->UpdateAccount('user_id', $this->session->userdata('user_id'), 'user', $data_update);
                    $cache_file_name = 'user_detail' . $this->session->userdata('user_id');

                    setting_deletecache($cache_file_name);

                    $cache_file_name = 'user_detail_notification' . $this->session->userdata('user_id');

                    setting_deletecache($cache_file_name);

                    $this->session->set_flashdata('success_message_social', RECORD_UPDATED_SUCCESSFULLY);
                    redirect('account/social');
                   


                }

            } else {
                redirect('account');
            }
        }

        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/account/social', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
    }

    /*
    Function name :index()
    Parameter :$msg (message string)
    Return : none
    Use : to show the user detail
    Description : this is used to show the user detail and notification on account page
    */
    /**
     * @param string $msg
     */
    function first_step()
    {
        $user_id = check_user_authentication(true);

        $data = array();
       
        $meta = meta_setting();
        $site_setting = site_setting();
        $data['site_setting'] = $site_setting;
      
        $usedata = UserData($user_id, $join = array('user_notification'));
        $data['user_id'] = $usedata[0]['user_id'];
        $data['user_name'] = $usedata[0]['user_name'];
        $data['last_name'] = $usedata[0]['last_name'];
        $password=$data['password'] = $usedata[0]['password'];
        $data['user_type'] = json_decode($usedata[0]['user_type']);
        $data['email'] = $usedata[0]['email'];
        $data['address'] = $usedata[0]['address'];
        if (isset($usedata[0]['zip_code']) and $usedata[0]['zip_code'] != 0 or $usedata[0]['zip_code'] != '') $data['zip_code'] = $usedata[0]['zip_code'];
        else $data['zip_code'] = '';
        $data['user_about'] = $usedata[0]['user_about'];
        $data['user_occupation'] = $usedata[0]['user_occupation'];
        $data['mobile_number'] = $usedata[0]['mobile_number'];

        if($this->input->post('email')!=''){
            $username = $this->home_model->email_validate($data_check, $this->session->userdata('user_id'));
            if ($username) {
                //$this->form_validation->set_message('email_check', EXISTS_ACCOUNT_ASSOCIATE_WITHMAIL);
                $email_error = EXISTS_ACCOUNT_ASSOCIATE_WITHMAIL;
                
            } else {
               $email_error = '';
            }
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('user_name', FIRST_NAME, 'required|trim|max_length[40]');
        $this->form_validation->set_rules('last_name', LAST_NAME, 'required|trim|max_length[40]');
        $this->form_validation->set_rules('mobile_number', 'Mobile Number', 'required|is_numeric');
        if($password =='')
        {
             $this->form_validation->set_rules('password', PASSWORD, 'required|trim|min_length[8]|max_length[20]');
        }
        $this->form_validation->set_rules('address', ADDRESS, 'required|trim');
        if($usedata[0]['email'] == '')
        {
            $this->form_validation->set_rules('email', EMAIL, 'required|valid_email');
        }
        $this->form_validation->set_rules('zip_code', ZIPCODE, 'required');
      
        if($_POST)
        {
        
            if ($this->form_validation->run() || $email_error == '') {


                $data['user_name'] = SecurePostData($this->input->post('user_name'));
                $data['last_name'] = SecurePostData($this->input->post('last_name'));
              
                $data['address'] = SecurePostData($this->input->post('address'));
               
                $data['zip_code'] = SecurePostData($this->input->post('zip_code'));
              
                $data['mobile_number'] = $this->input->post('mobile_number');

                $user_type = json_encode($this->input->post('user_type'));
                $data_user_update =
                    array(
                        'user_name' => SecurePostData($this->input->post('user_name')),
                        'last_name' => SecurePostData($this->input->post('last_name')),
                        'password' => md5(SecurePostData($this->input->post('password'))),
                        'mobile_number' => SecurePostData($this->input->post('mobile_number')),
                        'address' => SecurePostData($this->input->post('address')),
                       
                        'zip_code' => SecurePostData($this->input->post('zip_code')),
                       
                        'user_type' => $user_type,

                       
                    );
                      if($password !='')
                     {
                        unset($data_user_update['password']);
                    }   
            $update = $this->account_model->UpdateAccount('user_id',$this->session->userdata('user_id'), 'user', $data_user_update);
                        user_activity('update_account', $this->session->userdata('user_id'), 0, 0);
                        $cache_file_name = 'user_detail' . $this->session->userdata('user_id');
                        setting_deletecache($cache_file_name);
                        $cache_file_name = 'user_detail_notification' . $this->session->userdata('user_id');
                        setting_deletecache($cache_file_name);
                        $data_session = array(
                            'user_name' => $data['user_name'],
                            'last_name' => $data['last_name'],
                            'email' => $data['email'],
                        );
                        $this->session->set_userdata($data_session);
                        
                         if ($update) {
                                // ///////////============new user email===========
                                $result = UserData($this->session->userdata('user_id'));
                                $email_template = $this->db->query("select * from `email_template` where task='Update Profile'");
                                $email_temp = $email_template->row();
                                if ($email_temp) {
                                    $email_address_from = $email_temp->from_address;
                                    $email_address_reply = $email_temp->reply_address;
                                    $email_subject = $email_temp->subject;
                                    $email_message = $email_temp->message;
                                    $username = SecureShowData($this->input->post('user_name'));
                                    $last_name = SecureShowData($this->input->post('last_name'));
                                    $email = SecureShowData($this->input->post('email'));
                                    $password = SecureShowData($this->input->post('password'));
                                    if ($this->input->post('address') != '') {
                                        $address = SecureShowData($this->input->post('address'));
                                    } else {
                                        $address = "N/A";;
                                    }
                                    $zip_code = SecureShowData($this->input->post('zip_code'));
                                    $email_to = SecureShowData($this->input->post('email'));
                                    $email_message = str_replace('{break}', '<br/>', $email_message);
                                    $email_message = str_replace('{user_name}', $username, $email_message);
                                    $email_message = str_replace('{last_name}', $last_name, $email_message);
                                    $email_message = str_replace('{email}', $email, $email_message);
                                    $email_message = str_replace('{password}', $password, $email_message);
                                    $email_message = str_replace('{address}', $address, $email_message);
                                    $email_message = str_replace('{zip_code}', $zip_code, $email_message);
                                    $str = $email_message;
                                    email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);
                                }
                            }

                        redirect('account/second_step');
                    
                
                } else {

              
              
                $this->session->set_flashdata('error_message_account', validation_errors().$email_error);
                $data['user_id'] = $this->input->post('user_id');
                $data['user_name'] = $this->input->post('user_name');
                $data['last_name'] = $this->input->post('last_name');
                
                $data['address'] = $this->input->post('address');
                
                $data['zip_code'] = $this->input->post('zip_code');
                $data["password"] = $this->input->post('password');
               
                $data['mobile_number'] = $this->input->post('mobile_number');
              
                
                 }
            }
            $this->template->write('meta_title', $meta['title'], TRUE);
            $this->template->write('meta_description', $meta['meta_description'], TRUE);
            $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
            $this->template->write_view('header', 'default/common/header', $data, TRUE);
            $this->template->write_view('main_content', 'default/account/first_step', $data, TRUE);
            $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
            $this->template->render();



    }

/*
    Function name :index()
    Parameter :$msg (message string)
    Return : none
    Use : to show the user detail
    Description : this is used to show the user detail and notification on account page
    */
    /**
     * @param string $msg
     */
    function second_step()
    {
        $user_id = check_user_authentication(true);

        $data = array();
       
        $meta = meta_setting();
        $site_setting = site_setting();
        $data['site_setting'] = $site_setting;
       
        $usedata = UserData($user_id, $join = array('user_property_detail'));
        $data['user_type'] = json_decode($usedata[0]['user_type']);
        $data['user_id'] = $usedata[0]['user_id'];
        $data['user_property_id'] = $usedata[0]['user_property_id'];
        $data['user_property_type'] = json_decode($usedata[0]['user_property_type']);
        $data['min_investment_range'] = $usedata[0]['min_investment_range'];
        $data['max_investment_range'] = $usedata[0]['max_investment_range'];
       
        $data['investment_suburbs'] = $usedata[0]['investment_suburbs'];
        $data['company_name'] = $usedata[0]['company_name'];
        $data['company_website'] = $usedata[0]['company_website'];
        $data['company_logo'] = $usedata[0]['company_logo'];
        $data['company_address'] = $usedata[0]['company_address'];


        $this->load->library('form_validation');
        $this->form_validation->set_rules('investment_suburbs', 'Investment Suburbs', 'required');
        $this->form_validation->set_rules('min_investment_range', 'Min Investment Range', 'required');
        $this->form_validation->set_rules('max_investment_range', 'Max Investment Range', 'required');
        if($this->input->post('user_type')!='')
        {

        
         if(in_array(2, $this->input->post('user_type')) || in_array(4, $this->input->post('user_type')))
        
        {

            if(in_array(2, $this->input->post('user_type')))
            {
                $user_type_post = 2;
            }
            else if(in_array(4, $this->input->post('user_type')))
            {
                $user_type_post = 4;
            }
            
            if($user_type_post == 2 || $user_type_post == 4)
            {
               
                $this->form_validation->set_rules('company_name', 'Company Name', 'required');
                $this->form_validation->set_rules('abn_number', 'ABN Number', 'required');
                $this->form_validation->set_rules('company_address', 'Company Address', 'required');
            }
        }
    }
        if($_POST)
        {
            $investment_range_error = '';
            $findvalue  = array('$',"K");
            $min_investment_range = str_replace($findvalue, '',SecurePostData($this->input->post('min_investment_range')));
            $max_investment_range = str_replace($findvalue, '',SecurePostData($this->input->post('max_investment_range')));

            if($min_investment_range < 20)
            {
                $investment_range_error = "Investment must be greater than $20k";
            }
            else if($max_investment_range >  200)
            {
                $investment_range_error = "Investment must be less than $200k";
            }
            
            else if($min_investment_range < 20 and $max_investment_range > 200)
            {
                $investment_range_error = "Investment must be between $20k and $200k";
            }
            $company_logo = '';
            $image_error='';

            if($_FILES)
            {
                if($usedata[0]['company_logo'] != '')
                {
                    if($_FILES['file1']['name'] != '')
                    {
                        $_FILES['userfile']['name'] = $_FILES['file1']['name'];
                        $_FILES['userfile']['type'] = $_FILES['file1']['type'];
                        $_FILES['userfile']['tmp_name'] = $_FILES['file1']['tmp_name'];
                        $_FILES['userfile']['error'] = $_FILES['file1']['error'];
                        $_FILES['userfile']['size'] = $_FILES['file1']['size'];
                        $image_settings = get_image_setting_data();
                                // image validation

                                if ($_FILES["userfile"]["type"] != "image/jpeg" and $_FILES["userfile"]["type"] != "image/pjpeg" and $_FILES["userfile"]["type"] != "image/png" and $_FILES["userfile"]["type"] != "image/x-png" and $_FILES["userfile"]["type"] != "image/gif") {
                                    $image_error = PLEASE_UPLOAD_A_JPG_PNG_GIF_FILE;
                                } else
                                    if ($_FILES["userfile"]["size"] > $image_settings['upload_limit']*1000000) {
                                        $image_error = sprintf(SORRY_THIS_FILE_IS_TOO_LARGE_PLEASE_SELECT_A_JPG_FILE_THAT_IS_LESS_THAN_MB_OR_TRY_RESIZING_IT_USING_A_PHOTO_EDITOR,$image_settings['upload_limit']);
                                    } else {
                                        $company_logo = CompanyLogoUpload($_FILES);
                                      
                                    }
                    }
                    else
                    {
                        $company_logo = $usedata[0]['company_logo'];
                    }
                }
                else
                {
                    if($_FILES['file1']['name'] != '')
                    {
                        $_FILES['userfile']['name'] = $_FILES['file1']['name'];
                        $_FILES['userfile']['type'] = $_FILES['file1']['type'];
                        $_FILES['userfile']['tmp_name'] = $_FILES['file1']['tmp_name'];
                        $_FILES['userfile']['error'] = $_FILES['file1']['error'];
                        $_FILES['userfile']['size'] = $_FILES['file1']['size'];
                        $image_settings = get_image_setting_data();
                                // image validation

                                if ($_FILES["userfile"]["type"] != "image/jpeg" and $_FILES["userfile"]["type"] != "image/pjpeg" and $_FILES["userfile"]["type"] != "image/png" and $_FILES["userfile"]["type"] != "image/x-png" and $_FILES["userfile"]["type"] != "image/gif") {
                                    $image_error = PLEASE_UPLOAD_A_JPG_PNG_GIF_FILE;
                                } else
                                    if ($_FILES["userfile"]["size"] > $image_settings['upload_limit']*1000000) {
                                        $image_error = sprintf(SORRY_THIS_FILE_IS_TOO_LARGE_PLEASE_SELECT_A_JPG_FILE_THAT_IS_LESS_THAN_MB_OR_TRY_RESIZING_IT_USING_A_PHOTO_EDITOR,$image_settings['upload_limit']);
                                    } else {
                                        $company_logo = CompanyLogoUpload($_FILES);
                                      
                                    }
                    }
                    else
                    {
                        $image_error = "Company Logo is required.";
                    }
                }

            }
            
            if ($this->form_validation->run() && $investment_range_error == '' && $image_error == '') {


                $data['max_investment_range'] = SecurePostData($this->input->post('max_investment_range'));
               
                $data['min_investment_range'] = SecurePostData($this->input->post('min_investment_range'));
              
                $data['investment_suburbs'] = SecurePostData($this->input->post('investment_suburbs'));
               
                $user_property_type = json_encode($this->input->post('user_property_type'));
                $data_user_update =
                    array(
                        'user_id'=>$this->session->userdata('user_id'),
                        'investment_suburbs' => SecurePostData($this->input->post('investment_suburbs')),
                        'min_investment_range' => $min_investment_range,
                        'max_investment_range' => $max_investment_range,
                        'user_property_type' => $user_property_type,
                       
                    );
                $data_user_company = array(
                        'company_name' =>  SecurePostData(str_replace('"', "'", $this->input->post('company_name'))),
                        'company_website' =>  SecurePostData($this->input->post('company_website')),
                        'company_address' =>  SecurePostData($this->input->post('company_address')),
                        'company_logo' => $company_logo,

                    );
                if($data['user_property_id'] > 0)
                {
                     $this->account_model->UpdateAccount('user_property_id', $data['user_property_id'], 'user_property_detail', $data_user_update);
                }
                else
                {
                     $this->account_model->UpdateAccount('','', 'user_property_detail', $data_user_update);
                }
                   

                $this->account_model->UpdateAccount('user_id', $this->session->userdata('user_id'), 'user', $data_user_company);   


                    user_activity('update_account', $this->session->userdata('user_id'), 0, 0);
                    $cache_file_name = 'user_detail' . $this->session->userdata('user_id');
                    setting_deletecache($cache_file_name);
                    $cache_file_name = 'user_detail_notification' . $this->session->userdata('user_id');
                    setting_deletecache($cache_file_name);
                   $this->session->set_flashdata('success_message_account', 'Your Profile has been successfully updated');
                    redirect('account');
                
                
                } else {

             
                $this->session->set_flashdata('error_message_account', validation_errors().$investment_range_error.$image_error);
                $data['user_id'] = $this->input->post('user_id');
                $data['min_investment_range'] = $this->input->post('min_investment_range');
                $data['max_investment_range'] = $this->input->post('max_investment_range');
                
                $data['investment_suburbs'] = $this->input->post('investment_suburbs');
                
                $data['company_name'] =  SecurePostData(str_replace('"', "'", $this->input->post('company_name')));
                $data['company_website'] =  SecurePostData($this->input->post('company_website'));
                $data['company_address'] =  SecurePostData($this->input->post('company_address'));

                
                 }
            }
            $this->template->write('meta_title', $meta['title'], TRUE);
            $this->template->write('meta_description', $meta['meta_description'], TRUE);
            $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
            $this->template->write_view('header', 'default/common/header', $data, TRUE);
            $this->template->write_view('main_content', 'default/account/second_step', $data, TRUE);
            $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
            $this->template->render();



    }

     /*
    Function name :search_auto()
    Parameter :text ,id
    Return : none
    Use : use for auto search
    */
    function search_auto($text = '', $id = '')
    {
      
          
       
        $data = array();
        $offset = 0;
        $limit = 3;
        $keyword = SecurePostData($text);
        $search_criteria = array(
            'investment_suburbs' => $keyword
        );
      
        $data['search_criteria'] = $search_criteria;
        $search_suburbs = $this->home_model->GetAllSearchSuburbs($search_criteria);
         if ($search_suburbs) {
            $str = '<ul id="srhdiv">';
            foreach ($search_suburbs as $search_suburb) {
                $str .= '<li onclick="selecttext(this);">' .$search_suburb['suburb'].' , '.$search_suburb['state'] . '</li>';
            }
            $str .= '</ul>';
            echo $str;
            die();
        }

        // echo json_encode($search_suburbs);
        die;
    }

    /*
    Function name :search_auto()
    Parameter :text ,id
    Return : none
    Use : use for auto search
    */
    function search_auto_postcode($text = '', $id = '')
    {
        $data = array();
        $offset = 0;
        $limit = 3;
        $keyword = SecurePostData($text);
        $search_criteria = array(
            'postcode' => $keyword
        );
        $data['search_criteria'] = $search_criteria;
        $search_suburbs = $this->home_model->GetAllSearchSuburbs($search_criteria,'',array(),'yes');
        // var_dump($search_equities);
        if ($search_suburbs) {
            $str = '<ul id="srhdiv">';
            foreach ($search_suburbs as $search_suburb) {
                $str .= '<li onclick="selecttext(this);">' . $search_suburb['postcode'] . '</li>';
            }
            $str .= '</ul>';
            echo $str;
            die();
        }
        die;
    }
}

/* end of file */
