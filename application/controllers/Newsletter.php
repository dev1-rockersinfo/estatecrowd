<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 * The Home class is basically the default controller for the script.
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Newsletter extends ROCKERS_Controller

{
    function __construct()
    {
        parent::__construct();
        $this->load->model('newsletter_model');
        $this->load->model('home_model');
        
    }

    /*
    Function name :subscribe()
    Parameter :none
    Return : none
    Use : Newsletters subscription.
    */

    function subscribe()
    {
        $subscribe_email = SecurePostData($this->input->post('subscribe_email'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('subscribe_email', EMAIL_ADDRESS, 'required|trim|valid_email');
        if ($this->form_validation->run($this) == FALSE) {
            $data['error'] = validation_errors();
            $redir = "home/index/subscribe_fail";
            redirect($redir);
        }
        if ($subscribe_email == '') {
            if ($this->input->post('subscribe_email') == '') {
                redirect('home');
            } else {
            }
        }
        $redir='';
        $make_subscribe = $this->newsletter_model->make_new_subscription($subscribe_email);
        if ($make_subscribe == 2) {
            $redir = "home/index/subscribe_fail";
        }
        if ($make_subscribe == 1) {
            $redir = "home/index/subscribe_succsess";
        }
        redirect($redir);
    }
}

/* end of file */
