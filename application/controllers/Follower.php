
<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 * The Home class is basically the default controller for the script.
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Follower extends ROCKERS_Controller

{
    function __construct()
    {
        parent::__construct();
        $this->load->model('home_model');
        $this->load->model('equity_model');
        $this->load->model('account_model');
        $this->load->model('follower_model');
        $this->load->model('following_model');
        $this->load->model('activity_model');
        $this->load->library('securimage');
    }

    /*
    Function name :equity_follower()
    Parameter :No parameter
    Return : none
    Use : this is used in equity detail page
    Description :  will show you list of follower for particular equity, according to equity id
    */
    function property_follower($property_id='')
    {
        $followers = $this->follower_model->GetAllFollower($type = 'property_follow', $property_id, 0, $join = array(
            'user'
        ), $limit = 1000, $order = array(
            'property_follow_id' => 'desc'
        ));

        
    }

    
    function ajax_property_follower($property_id='')
    {
        $data['follower'] = $this->follower_model->GetAllFollower($type = 'property_follow', $property_id, 0, $join = array(
            'user'
        ), $limit = 1000, $order = array(
            'property_follow_id' => 'desc'
        ));

        $this->load->view('default/property/ajax_savefollower', $data);
    }
    /*
    Function name :user_follower()
    Parameter :user_id
    Return : none
    Use : this is used in member detail page
    Description : will show you list of follower for particular user, according to user id
    */
    // =====================  inser follow user====================
    function user_follower($user_id = 0)
    {
        if ($user_id == '' or $user_id <= 0) {
            redirect('home');
        }

        $follow_user = $this->follower_model->follow_user($user_id);
        $userdata = UserData($this->session->userdata('user_id'));
        $follower = UserData($user_id);
        $user_not_own = $this->account_model->get_email_notification($follower[0]['user_id']);
       

        if ($user_not_own != '0' || is_object($user_not_own)) {
            if ($user_not_own->social_notification_alert == '1') {
                $user['user_name'] = $follower[0]['user_name'] . ' ' . $follower[0]['last_name'];
                $user['email'] = $follower[0]['email'];

                $profile_slug = $userdata[0]['profile_slug'];

                $follower_profile_link = site_url('user/' . $profile_slug);

                $followr_user_name = $userdata[0]['user_name'] . ' ' . $userdata[0]['last_name'];
                $user['followr_user_name_without_link'] = $followr_user_name;
                $user['follow_user_name'] = '<a href="' . $follower_profile_link . '">' . $followr_user_name . '</a>';

                $user['follower_user_link'] = '<a href="' . site_url('user/' . $follower[0]['profile_slug']) . '">' . site_url('user/' . $follower[0]['profile_slug']) . '</a>';
                $this->mailalerts('user_follower', '', '', $user, 'User Follower');
            }
        }
        user_deletecache('followers', $this->session->userdata('user_id'));
        user_deletecache('followings', $this->session->userdata('user_id'));
        user_deletecache('followers', $user_id);
        user_deletecache('followings', $user_id);

        ///////// start activities	/////////////////////

        user_activity('follow', $this->session->userdata('user_id'), $user_id, 0);
        ///////// end activities	/////////////////////

        echo 'Follow';
        die();
    }

   
    

   

    function ajax_following()
    {
        $data['site_setting'] = site_setting();
        $user_id = SecurePostData($this->input->post('user_id'));

        $data['user_followings'] = $this->following_model->GetAllFollowing($type = 'user_following', $user_id, $join = array(
            'user'
        ), $limit = 1000, $order = array(
            'follower_id' => 'desc'
        ));


        $this->load->view('default/profile/ajax_savefollowering', $data);
    }

    /*
    Function name :user_unfollower()
    Parameter :user_id
    Return : none
    Use : Used for Unfollow the follower for particualr user.
    */
    // =========================user unfollower=============
    function user_unfollower($user_id = 0)
    {
        // $user=103;
        if ($user_id == '' or $user_id <= 0) {
            redirect('home');
        }
        $checkfollow = $this->follower_model->follower_list($user_id);
        //echo $checkfollow;

        if ($checkfollow) {
            $this->db->delete('user_follow', array(
                "follow_user_id" => $user_id,
                "follow_by_user_id" => $this->session->userdata('user_id')
            ));
            //echo 'unfollow';
            user_deletecache('followers', $this->session->userdata('user_id'));
            user_deletecache('followings', $this->session->userdata('user_id'));
            user_deletecache('followers', $user_id);
            user_deletecache('followings', $user_id);

        }
        user_activity('unfollow', $this->session->userdata('user_id'), $user_id, 0);
        echo 'unfollow';    
        die();
    }
    /*
    Function name :equity_follower_insert()
    Parameter :equity_id
    Return : none
    Use : Used for insert follower data for pariticular equity.
    */
    // ================== equity follow insert================
    function property_follower_insert($property_id = 0)
    {
        if ($property_id == '' or $property_id <= 0) {
            redirect('home');
        }
        $follow_equity = $this->following_model->follow_property_insert($property_id);
        $equity_id ='';

        echo 'Follow';
        $property = GetOneProperty($property_id);

        $property_address = $property['property_address'];
        $property_url = $property['property_url'];

      
        $property_page_link = site_url( 'properties/' . $property_url);
        $property_title_anchor = '<a href="' . $property_page_link . '">' . $property_address . '</a>';


        ///////// start count latest activities	/////////////////////

        project_activity('follow', $this->session->userdata('user_id'), 0, $property_id);
        ////////////////////////////////////////////////////////////////
        // ///////////////============== compaign you created ================================

        $site_setting = site_setting();
        $site_name = $site_setting['site_name'];
        $taxonomy_setting = taxonomy_setting();
        $project_name = $taxonomy_setting['project_name'];
        $follower_name = $taxonomy_setting['follower'];
        $followers_plural = $taxonomy_setting['followers_plural'];
        $followers_past = $taxonomy_setting['followers_past'];
        $funds_past = $taxonomy_setting['funds_past'];
        $user_not_own = $this->account_model->get_email_notification($property['user_id']);

        if ($user_not_own != '0') {
            if ($user_not_own->creator_follow_alert == 1) {

                $follower_user = UserData($property['user_id']);
                $follower_user_name = $follower_user[0]['user_name'];
                $follower_last_name = $follower_user[0]['last_name'];
                $follower_email = $follower_user[0]['email'];

                $following_user = UserData($this->session->userdata['user_id']);

                $profile_slug = $following_user[0]['profile_slug'];

                $follow_profile_link = site_url('user/' . $profile_slug);

                $follow_name = $following_user[0]['user_name'] . ' ' . $following_user[0]['last_name'];
                $following_name_anchor = '<a href="' . $follow_profile_link . '">' . $follow_name . '</a>';


                $email_template = $this->db->query("select * from `email_template` where task='New follower on campaign you created'");
                $email_temp = $email_template->row();
                $email_message = $email_temp->message;
                $email_subject = $email_temp->subject;
                $email_subject = str_replace('{company_name}', $property_address, $email_subject);
                $email_subject = str_replace('{project_name}', $project_name, $email_subject);
                $email_subject = str_replace('{follower_name}', $follower_name, $email_subject);
                $email_address_from = $email_temp->from_address;
                $email_address_reply = $email_temp->reply_address;
                $email_to = $follower_email;
                $email_message = str_replace('{break}', '<br/>', $email_message);
                $email_message = str_replace('{user_name}', $follower_user_name, $email_message);
                $email_message = str_replace('{project_name}', $project_name, $email_message);
                $email_message = str_replace('{follower_plural_name}', $followers_plural, $email_message);
                $email_message = str_replace('{company_name}', $property_title_anchor, $email_message);
                $email_message = str_replace('{follow_name}', $following_name_anchor, $email_message);
                $email_message = str_replace('{equity_page_link}', $property_title_anchor, $email_message);
                $email_message = str_replace('{site_name}', $site_name, $email_message);
                $email_message = str_replace('{funds_past}', $funds_past, $email_message);
                $str = $email_message;


                email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);

                # code...
            }
        }


        // ///////////////============== Project you back notification================================
/*
        $equity_back_follow = $this->db->query("SELECT un.you_follow_alert,un.user_id,u.email,u.user_name,u.last_name FROM `user_notification` un
			inner join user u on u.user_id=un.user_id
			inner join transaction tr on tr.user_id=u.user_id
			where  un.you_follow_alert=1 and tr.equity_id=" . $equity_id . "
			group by u.user_id");

        $all_back_user = $equity_back_follow->result_array();

        $all_follow_user = UserData($this->session->userdata('user_id'));

        $profile_slug = $all_follow_user[0]['profile_slug'];

        $follow_profile_link = site_url('user/' . $profile_slug);

        $follow_name = $all_follow_user[0]['user_name'] . ' ' . $all_follow_user[0]['last_name'];
        $following_name_anchor = '<a href="' . $follow_profile_link . '">' . $follow_name . '</a>';


        if (is_array($all_back_user)) {
            foreach ($all_back_user as $back_user) {

                $donated_user_name = $back_user['user_name'];
                $donated_last_name = $back_user['last_name'];
                $donated_email = $back_user['email'];

                $email_template = $this->db->query("select * from `email_template` where task='Someone follow on your backed equity'");
                $email_temp = $email_template->row();
                $email_message = $email_temp->message;
                $email_subject = $email_temp->subject;
                $email_subject = str_replace('{company_name}', $property_address, $email_subject);
                $email_subject = str_replace('{project_name}', $project_name, $email_subject);
                $email_subject = str_replace('{follower_name}', $follower_name, $email_subject);
                $email_subject = str_replace('{funds_past}', $funds_past, $email_subject);
                $email_address_from = $email_temp->from_address;
                $email_address_reply = $email_temp->reply_address;
                $email_to = $donated_email;
                $email_message = str_replace('{break}', '<br/>', $email_message);
                $email_message = str_replace('{user_name}', $donated_user_name, $email_message);
                $email_message = str_replace('{project_name}', $project_name, $email_message);
                $email_message = str_replace('{company_name}', $property_title_anchor, $email_message);
                $email_message = str_replace('{follow_name}', $following_name_anchor, $email_message);
                $email_message = str_replace('{funds_past}', $funds_past, $email_message);
                $email_message = str_replace('{equity_page_link}', $property_title_anchor, $email_message);
                $email_message = str_replace('{site_name}', $site_name, $email_message);

                $str = $email_message;

                email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);

                # code...
            }
        }
    */
        // ///////////////============== Project you follow notification================================


        $equity_you_follow = $this->db->query("SELECT un.new_follow_alert,un.user_id,u.email,u.user_name,u.last_name FROM `user_notification` un
						inner join user u on u.user_id=un.user_id
						inner join property_follower pf on pf.property_follow_user_id=u.user_id
						where  un.new_follow_alert=1 and pf.property_id=" . $property_id . "
						group by u.user_id");

        $all_equity_follow_user = $equity_you_follow->result_array();

        if (is_array($all_equity_follow_user)) {
            foreach ($all_equity_follow_user as $follower_user) {

                $follower_user_name = $follower_user['user_name'];
                $follower_last_name = $follower_user['last_name'];
                $follower_email = $follower_user['email'];

                $email_template = $this->db->query("select * from `email_template` where task='New follower on campaign you followed'");
                $email_temp = $email_template->row();
                $email_message = $email_temp->message;
                $email_subject = $email_temp->subject;
                $email_subject = str_replace('{company_name}', $property_address, $email_subject);
                $email_subject = str_replace('{followers_past}', $followers_past, $email_subject);
                $email_subject = str_replace('{project_name}', $project_name, $email_subject);
                $email_subject = str_replace('{follower_name}', $follower_name, $email_subject);
                $email_address_from = $email_temp->from_address;
                $email_address_reply = $email_temp->reply_address;
                $email_to = $follower_email;

                $email_message = str_replace('{user_name}', $follower_user_name, $email_message);
                $email_message = str_replace('{follower_plural_name}', $followers_plural, $email_message);
                $email_message = str_replace('{project_name}', $project_name, $email_message);
                $email_message = str_replace('{company_name}', $property_title_anchor, $email_message);
                $email_message = str_replace('{follow_name}', $following_name_anchor, $email_message);
                $email_message = str_replace('{equity_page_link}', $property_title_anchor, $email_message);
                $email_message = str_replace('{funds_past}', $funds_past, $email_message);
                $email_message = str_replace('{site_name}', $site_name, $email_message);

                $str = $email_message;

                email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);

                # code...
            }
        }

        die();
    }
    /*
    Function name :equity_unfollower()
    Parameter :user_id
    Return : none
    Use : Used for Unfollow the follower for particualr equity.
    */
    // ================== equity follow insert================
    function property_unfollower($property_id = 0)
    {
        if ($property_id == '' or $property_id <= 0) {
            redirect('home');
        }
        $this->db->delete('property_follower', array(
            "property_id" => $property_id,
            "property_follow_user_id" => $this->session->userdata('user_id')
        ));
        project_activity('unfollow', $this->session->userdata('user_id'), 0, $property_id);
        echo "Unfollow";


        die();
    }

     /*
    Function name :user_following()
    Parameter :No parameter
    Return : none
    Use : this is used in member detail page
    Description : will show you list of follower for particular user, according to user id
    */
    function user_following()
    {
        $user_id = check_user_authentication(true);
        $data['user_following'] = $this->following_model->GetAllFollowing($type = 'user_following', $user_id, $join = array(
            'user'
        ), $limit = 1000, $order = array(
            'follower_id' => 'desc'
        ));
        $meta = meta_setting();
        $data['site_setting'] = site_setting();
        

        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/following/user_following', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();

    }

     /*
    Function name :user_following()
    Parameter :No parameter
    Return : none
    Use : this is used in member detail page
    Description : will show you list of follower for particular user, according to user id
    */
    function userFollowers()
    {
        $user_id = check_user_authentication(true);
          $data['user_followers'] = $this->follower_model->GetAllFollower($type = 'user_follow', 0, $user_id, $join = array(
                'user'
            ), $limit = 1000, $order = array(
                'follower_id' => 'desc'
            ));
        $meta = meta_setting();
        $data['site_setting'] = site_setting();
        

        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/following/user_followers', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();

    }
        /*
    Function name :user_following()
    Parameter :No parameter
    Return : none
    Use : this is used in member detail page
    Description : will show you list of follower for particular user, according to user id
    */
    function ajax_user_following()
    {
        $user_id = check_user_authentication(true);
        $data['user_following'] = $this->following_model->GetAllFollowing($type = 'user_following', $user_id, $join = array(
            'user'
        ), $limit = 1000, $order = array(
            'follower_id' => 'desc'
        ));
        $this->load->view('default/following/ajax_user_following', $data);

    

    }
    /*
    Function name :ajax_user_follower()
    Parameter :No parameter
    Return : none
    Use : Used for getting all users follower list.
    */
    // ==================ajax user follower================
    function ajax_user_follower()
    {
        $data['site_setting'] = site_setting();
         $user_id = check_user_authentication(true);
        $data['user_followers'] = $this->follower_model->GetAllFollower($type = 'user_follow', 0, $user_id, $join = array(
            'user'
        ), $limit = 1000, $order = array(
            'follower_id' => 'desc'
        ));
        $this->load->view('default/following/ajax_savefollower', $data);
    }

    /*
    Function name :user_unfollower()
    Parameter :user_id
    Return : none
    Use : Used for Unfollow the follower for particualr user.
    */
    // =========================user unfollower=============
    function userUnfollower($user_id = 0)
    {
        // $user=103;
        if ($user_id == '' or $user_id <= 0) {
            redirect('home');
        }
        $checkfollow = $this->follower_model->follower_list($user_id);
        //echo $checkfollow;

        if ($checkfollow) {
            $this->db->delete('user_follow', array(
                "follow_user_id" => $user_id,
                "follow_by_user_id" => $this->session->userdata('user_id')
            ));
            //echo 'unfollow';
            user_deletecache('followers', $this->session->userdata('user_id'));
            user_deletecache('followings', $this->session->userdata('user_id'));
            user_deletecache('followers', $user_id);
            user_deletecache('followings', $user_id);

        }
        user_activity('unfollow', $this->session->userdata('user_id'), $user_id, 0);
        echo 'unfollow';    
        die();
    }

     /*
    Function name :user_follower()
    Parameter :user_id
    Return : none
    Use : this is used in member detail page
    Description : will show you list of follower for particular user, according to user id
    */
    // =====================  inser follow user====================
    function userFollower($user_id = 0)
    {
        if ($user_id == '' or $user_id <= 0) {
            redirect('home');
        }

        $follow_user = $this->follower_model->follow_user($user_id);
        $userdata = UserData($this->session->userdata('user_id'));
        $follower = UserData($user_id);
        $user_not_own = $this->account_model->get_email_notification($follower[0]['user_id']);
       

        if ($user_not_own != '0' || is_object($user_not_own)) {
            if ($user_not_own->social_notification_alert == '1') {
                $user['user_name'] = $follower[0]['user_name'] . ' ' . $follower[0]['last_name'];
                $user['email'] = $follower[0]['email'];

                $profile_slug = $userdata[0]['profile_slug'];

                $follower_profile_link = site_url('user/' . $profile_slug);

                $followr_user_name = $userdata[0]['user_name'] . ' ' . $userdata[0]['last_name'];
                $user['followr_user_name_without_link'] = $followr_user_name;
                $user['follow_user_name'] = '<a href="' . $follower_profile_link . '">' . $followr_user_name . '</a>';

                $user['follower_user_link'] = '<a href="' . site_url('user/' . $follower[0]['profile_slug']) . '">' . site_url('user/' . $follower[0]['profile_slug']) . '</a>';
                $this->mailalerts('user_follower', '', '', $user, 'User Follower');
            }
        }
        user_deletecache('followers', $this->session->userdata('user_id'));
        user_deletecache('followings', $this->session->userdata('user_id'));
        user_deletecache('followers', $user_id);
        user_deletecache('followings', $user_id);

        ///////// start activities  /////////////////////

        user_activity('follow', $this->session->userdata('user_id'), $user_id, 0);
        ///////// end activities    /////////////////////

        echo 'Follow';
        die();
    }

    
   
}



?>

