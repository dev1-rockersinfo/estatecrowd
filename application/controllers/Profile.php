<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 * The Home class is basically the default controller for the script.
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Profile extends ROCKERS_Controller

{
    function __construct()
    {
        parent::__construct();
        $this->load->model('account_model');
        $this->load->model('activity_model');
        $this->load->model('follower_model');
        $this->load->model('following_model');
        $this->load->model('crowd_model');
        $this->load->model('home_model');
        $this->load->model('inbox_model');
        $this->load->model('message_model');
        $this->load->model('property_model');

        $this->load->helper('cookie');
        $this->load->library('securimage');
    }

    /*
    Function name :user_profile()
    Parameter :user_id ,msg
    Return : none
    Use :User profile page.
    */
    function user_profile($user_id = 0, $msg = '')
    {

        $data = array();
        $offset = 0;
        $limit = 10;
        $data['taxonomy_setting'] = taxonomy_setting();
        if (!is_numeric($user_id)) {
            $user_id = $this->property_model->get_user_profile_id($user_id);
        }

        $user_data = UserData($user_id, array('user_property_detail'));
        if (!isset($user_data[0]['user_id'])) redirect('home');
        $data['result'] = $user_data;
        $data['msg'] = $msg;
       
        if (!$this->simple_cache->is_cached('user_followers_' . $user_id)) {
            $data['user_followers'] = $this->follower_model->GetAllFollower($type = 'user_follow', 0, $user_id, $join = array(
                'user'
            ), $limit = 1000, $order = array(
                'follower_id' => 'desc'
            ));
            $this->simple_cache->cache_item('user_followers_' . $user_id, $data['user_followers']);
        } else {
            $data['user_followers'] = $this->simple_cache->get_item('user_followers_' . $user_id);
        }
        if (!$this->simple_cache->is_cached('user_followings_' . $user_id)) {
            $data['user_followings'] = $this->following_model->GetAllFollowing($type = 'user_following', $user_id, $join = array(
                'user'
            ), $limit = 1000, $order = array(
                'follower_id' => 'desc'
            ));
            $this->simple_cache->cache_item('user_followings_' . $user_id, $data['user_followings']);
        } else {
            $data['user_followings'] = $this->simple_cache->get_item('user_followings_' . $user_id);
        }
        user_deletecache('my_donation', $user_id);


        $data['user_crowds'] = $this->crowd_model->GetAllCrowds('',$user_id,array('user','crowd_invite_request'));
        

        $data['user_property_campaign'] = $this->property_model->GetAllProperties(0, 0, '2,3,4', array(
            'user','campaign'
        ), 1000, array(
            'property_id' => 'desc'
        ), '','','yes',$user_id);
       
        

        $data['user_followers_user_id'] = $data['user_followers'][0]['user_id'];
       
        $meta = meta_setting();
        $data['site_setting'] = site_setting();
        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        //$this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/profile/user_profile', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
        // $this->output->enable_profiler(TRUE);
    }

    /*
    Function name :comment_list()
    Parameter :$user_id(user id),$offset (number)
    Return : none
    Use : to show the comments on profile page using ajax
        this is used to show the all or more comments on profile page
    */
    function comment_list($user_id = '', $offset = 0)
    {
        $limit = 10;
        $data['offset'] = $offset;
        $data['site_setting'] = site_setting();

        $data['user_comment_row'] = $this->account_model->GetUserComments($user_id, array(
            'user',
            'equity'
        ), 100, array(
            'comment_id' => 'desc'
        ), '1');
        $count_user_comments = count($data['user_comment_row']);

        $data['total_row_display'] = $count_user_comments - $offset;

        $data['user_comment'] = $this->account_model->GetUserComments($user_id, array(
            'user',
            'equity'
        ), $limit, array(
            'comment_id' => 'desc'
        ), '1', $offset);
        //echo $this->db->last_query();die;

        $this->load->view('default/profile/comment_list', $data);
    }

    /*
    Function name :comment_list()
    Parameter :$user_id(user id),$offset (number)
    Return : none
    Use : to show the comments on profile page using ajax
        this is used to show the all or more comments on profile page
    */
    function contribution_list($user_id = '', $offset = 0)
    {
        $limit = 10;
        $data['offset'] = $offset;
        $data['site_setting'] = site_setting();

        $data['my_donation_row'] = $this->account_model->GetDonation('my_donation_trans_anonymous', $user_id, 0, '2,3,4', array(
            'equity',
            'perk'
        ), 100, array(
            'transaction_id' => 'desc'
        ));


        $count_user_contribution = count($data['my_donation_row']);

        $data['total_row_display'] = $count_user_contribution - $offset;

        $data['my_donation'] = $this->account_model->GetDonation('my_donation_trans_anonymous', $user_id, 0, '2,3,4', array(
            'equity',
            'perk'
        ), $limit, array(
            'transaction_id' => 'desc'
        ), $offset);

        $this->load->view('default/profile/contribution_list', $data);
    }
}

?>
