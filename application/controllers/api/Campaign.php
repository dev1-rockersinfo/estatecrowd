<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Campaign extends Restapi_Controller {

  function __construct() {
    parent::__construct();
    // $this->load->model("property_model");
    $this->load->model("cpapi_model");
  }


  function index(){
    $token = SecurePostData($this->input->post('token'));
    // $property_suburb = SecurePostData($this->input->post('suburb'));
    $r_campaign_id = 0;
    $msg = "Inserted successfully.";
    $auth = $this->tokenValidation($token);    

    // insert array
    $data = array(
      "user_id" => SecurePostData($this->input->post('r_userid')),
      "lead_investor_id" => SecurePostData($this->input->post('r_userid')),      
      "property_id" => SecurePostData($this->input->post('r_property_id')),
      "campaign_user_type" => SecurePostData($this->input->post('campaign_user_type')),
      "min_investment_amount" => SecurePostData($this->input->post('min_investment_amount')),
      "investment_amount_type" => SecurePostData($this->input->post('investment_amount_type')),
      "campaign_type" => SecurePostData($this->input->post('campaign_type')),
      "campaign_units" => SecurePostData($this->input->post('campaign_units')),
      "investment_close_date" => SecurePostData($this->input->post('investment_close_date')),
      "campaign_units_get"=>0,
      "campaign_document"=>'',
      "host_ip" => $_SERVER['REMOTE_ADDR'],
      "save_the_reason_inactive"=>0,
      "reason_inactive_hidden"=>'',
      "fund_inactive_note"=>'',
      "date_added" => date("Y-m-d H:i:s"),
      "status" => 1,
    );

    if($this->input->post('r_campaign_id')>0 && $this->input->post('r_campaign_id') !=null ){
      $r_campaign_id = $this->input->post('r_campaign_id');
      $msg = "Updated successfully.";
    }

    if($data['user_id']==''){
      $response_data['success'] = False;
      $response_data['message'] = "User ID Missing.";
      $response_data['error_code'] = 3001;
      $this->convert_response_data($response_data);
    }

    if($data['property_id']==''){
      $response_data['success'] = False;
      $response_data['message'] = "Property ID Missing.";
      $response_data['error_code'] = 3002;
      $this->convert_response_data($response_data);
    }
    if($data['campaign_user_type']==''){
      $response_data['success'] = False;
      $response_data['message'] = "Campaign usertype Missing.";
      $response_data['error_code'] = 3003;
      $this->convert_response_data($response_data);
    }
    if($data['min_investment_amount']==''){
      $response_data['success'] = False;
      $response_data['message'] = "Min. investment amount Missing.";
      $response_data['error_code'] = 3004;
      $this->convert_response_data($response_data);
    }
    if($data['investment_amount_type']==''){
      $response_data['success'] = False;
      $response_data['message'] = "Investment amount type Missing.";
      $response_data['error_code'] = 3005;
      $this->convert_response_data($response_data);
    }
    if($data['campaign_type']==''){
      $response_data['success'] = False;
      $response_data['message'] = "Campaign type Missing.";
      $response_data['error_code'] = 3006;
      $this->convert_response_data($response_data);
    }
    if($data['campaign_units']==''){
      $response_data['success'] = False;
      $response_data['message'] = "Campaign units Missing.";
      $response_data['error_code'] = 3007;
      $this->convert_response_data($response_data);
    }
    if($data['investment_close_date']==''){
      $response_data['success'] = False;
      $response_data['message'] = "Investment close date Missing.";
      $response_data['error_code'] = 3008;
      $this->convert_response_data($response_data);
    }
    

    $return_val = $this->cpapi_model->create_campaign($data,$r_campaign_id);
    
    if($return_val){
      $response_data['success'] = True;
      $response_data['message'] = $msg;
      $response_data['r_campaign_id'] = $return_val;
      $this->convert_response_data($response_data);
    } else {
      $response_data['success'] = False;
      $response_data['message'] = "Something went wrong. Try again";
      $this->convert_response_data($response_data);
    }
  }
}
