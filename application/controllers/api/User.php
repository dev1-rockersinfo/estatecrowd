<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends Restapi_Controller {

    function __construct() {
        parent::__construct();
    }

    function test(){
        // $success = false;
        // $msg = "Service temporarily unavailable.";
        // $code = 1001;

        $token = SecurePostData($this->input->post('token'));

        $auth = $this->tokenValidation($token);



        $response_data['success'] = $auth;
        // $response_data['message'] = (string) $msg;
        // $response_data['error_code'] = $token;
        //*** Display JSON output ***//
        $this->convert_response_data($response_data);        
    }

}
