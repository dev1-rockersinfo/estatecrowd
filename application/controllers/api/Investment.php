<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Investment extends Restapi_Controller {

  function __construct() {
    parent::__construct();
    // $this->load->model("property_model");
    $this->load->model("cpapi_model");
  }


  function index(){
    $token = SecurePostData($this->input->post('token'));
    // $property_suburb = SecurePostData($this->input->post('suburb'));
    
    $r_investment_id = 0;
    $msg = "Inserted successfully.";
    $auth = $this->tokenValidation($token);

    $data = array(
      "user_id" => SecurePostData($this->input->post('r_userid')),
      "campaign_id" => SecurePostData($this->input->post('r_campaign_id')),
      "invest_status_id" => SecurePostData($this->input->post('invest_status_id')),
      "transaction_id" => SecurePostData($this->input->post('transaction_id')),
      "units" => SecurePostData($this->input->post('units')),
      "deposit_amount" => SecurePostData($this->input->post('amount')),
      "card_holder_name" => SecurePostData($this->input->post('card_holder_name')),
      "card_number" => SecurePostData($this->input->post('card_number')),
      "created_date" => date("Y-m-d"),
      "admin_fees" => 0,
      "status" => 2,
    );
    if($this->input->post('r_investment_id')>0 && $this->input->post('r_investment_id') !=null ){
      $r_investment_id = $this->input->post('r_investment_id');
      $msg = "Updated successfully.";
    }

    if($data['user_id']==''){
      $response_data['success'] = False;
      $response_data['message'] = "User id Missing.";
      $response_data['error_code'] = 3001;
      $this->convert_response_data($response_data);
    }

    if($data['campaign_id']==''){
      $response_data['success'] = False;
      $response_data['message'] = "Campaign id Missing.";
      $response_data['error_code'] = 3002;
      $this->convert_response_data($response_data);
    }
    if($data['invest_status_id']==''){
      $response_data['success'] = False;
      $response_data['message'] = "Investment status id Missing.";
      $response_data['error_code'] = 3003;
      $this->convert_response_data($response_data);
    }
    if($data['transaction_id']==''){
      $response_data['success'] = False;
      $response_data['message'] = "Transaction ID Missing.";
      $response_data['error_code'] = 3004;
      $this->convert_response_data($response_data);
    }
    if($data['units']==''){
      $response_data['success'] = False;
      $response_data['message'] = "Units Missing.";
      $response_data['error_code'] = 3005;
      $this->convert_response_data($response_data);
    }
    if($data['deposit_amount']==''){
      $response_data['success'] = False;
      $response_data['message'] = "Amount Missing.";
      $response_data['error_code'] = 3006;
      $this->convert_response_data($response_data);
    }
    if($data['card_holder_name']==''){
      $response_data['success'] = False;
      $response_data['message'] = "Card holder name Missing.";
      $response_data['error_code'] = 3007;
      $this->convert_response_data($response_data);
    }
    if($data['card_number']==''){
      $response_data['success'] = False;
      $response_data['message'] = "Car number Missing.";
      $response_data['error_code'] = 3008;
      $this->convert_response_data($response_data);
    }

    $return_val = $this->cpapi_model->create_investment($data,$r_investment_id);
    
    if($return_val){
      $response_data['success'] = True;
      $response_data['r_investment_id'] = $return_val;
      $response_data['message'] = $msg;
      $this->convert_response_data($response_data);
    } else {
      $response_data['success'] = False;
      $response_data['message'] = "Something went wrong. Try again";
      $this->convert_response_data($response_data);
    }
  }
}
