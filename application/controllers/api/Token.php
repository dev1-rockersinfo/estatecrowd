<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Token extends Restapi_Controller {

  function __construct() {
    parent::__construct();
    $this->load->model('cpapi_model');
  }


  public function index() {
    $token = $this->createToken();
    $success = true;
    $response_data['success'] = $success;
    $response_data['token'] = $token;
    $this->convert_response_data($response_data);
  }   
}
