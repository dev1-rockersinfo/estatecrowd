<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Property extends Restapi_Controller {

  function __construct() {
    parent::__construct();
    // $this->load->model("property_model");
    $this->load->model("cpapi_model");
  }


  function index(){
    $token = SecurePostData($this->input->post('token'));
    $property_suburb = SecurePostData($this->input->post('suburb'));
    
    $auth = $this->tokenValidation($token);



    $search_properties = $this->cpapi_model->property_search($property_suburb);
  

    $response_data['success'] = True;
    $response_data['propertyData'] = $search_properties;
    $this->convert_response_data($response_data);
  }

  // ===========================widget code====================
  /*
  * Used for generating widget of project commoncard
  */
  /**
   * @param $w
   * @param $c
   * @param $n
   */
  function widgets()
  {
      $data['w'] = 'm';
      // $data['c'] = $c;
      // $data['n'] = $n;
      $data['offset'] = 0;
      $data['url'] = "api/property/widget_search/";
      // $data['limit'] = 8;
      // $this->template->write_view('main_content', 'default/widget/widgets_code', $data, TRUE);
      // $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
      // $this->template->render();
      $this->load->view('default/widget/widgets_code', $data);
  }
  function widget_search(){
    // $token = SecurePostData($this->input->post('token'));
    // $property_suburb = SecurePostData($this->input->post('suburb'));
    
    // $auth = $this->tokenValidation($token);

    // $data['search_properties'] = $this->cpapi_model->property_search($property_suburb,1);

   
    // $this->template->add_css($data['color'] . '/fund-' . $data['color'] . '.css');
    // $this->template->front_write_view('main_content', 'widgets_page', $data, TRUE);
    // $this->template->render();

    $data["search_page"] = 1;

    $data = array();
    $offset =0;
    $limit = 12;
    $meta = meta_setting();
    $data['site_setting'] = site_setting();
    $this->load->model('property_model');
    $data['main_search'] = $this->input->get('main_search');

    $investment_amount = $this->input->get('investment_amount');
    $investment_suburb = $this->input->get('investment_suburb');
    $investment_address = $this->input->get('investment_address');

    if($data['main_search']==1){
      $investment_suburb='';
      $investment_address='';
    }else if($data['main_search']==2){
      $investment_amount='';
      $investment_address='';
    }else{
      $investment_amount='';
      $investment_suburb='';
    }


    if(!is_null($this->input->get('investment_amount')) || !is_null($this->input->get('investment_suburb')) || !is_null($this->input->get('investment_address'))){
      $data['investment_amount'] = $investment_amount; 
      $data['investment_suburb'] = $investment_suburb; 
      $data['investment_address'] = $investment_address; 
      $data['site_setting'] = site_setting();
  
      $count_search_properties = $this->cpapi_model->GetAllSearchProperties(0, 0, '2', array(
          'user','campaign'
      ), 10000, array(
          'campaign_id' => 'desc'
      ), $offset,'yes');

      $data['total_rows'] = $count_search_properties;

      $search_properties = $this->cpapi_model->GetAllSearchProperties(0, 0, '2', array(
          'user','campaign'
      ), $limit, array(
          'campaign_id' => 'desc'
      ), $offset,'','no');

      $data['search_properties'] = $search_properties;
    } else {
      $data['search_properties'] = NULL;
    }
    
    $this->template->write_view('main_content', 'default/widget/search_page', $data, TRUE);
   
    // $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
    $this->template->render();
  }

  function search(){
    $data = array();
    $offset =0;
    $limit = 12;
    $meta = meta_setting();
    $this->load->model('property_model');
    $data['main_search'] = $this->input->get('main_search');

      $investment_amount = $this->input->get('investment_amount');
      $investment_suburb = $this->input->get('investment_suburb');
      $investment_address = $this->input->get('investment_address');

      if($data['main_search']==1){
        $investment_suburb='';
        $investment_address='';
      }else if($data['main_search']==2){
        $investment_amount='';
        $investment_address='';
      }else{
        $investment_amount='';
        $investment_suburb='';
      }

      $data['investment_amount'] = $investment_amount; 
      $data['investment_suburb'] = $investment_suburb; 
      $data['investment_address'] = $investment_address; 
      $data['site_setting'] = site_setting();


      $bedrooms = $data['bedrooms'] = $this->input->get('bedrooms');
      $bathrooms = $data['bathrooms'] = $this->input->get('bathrooms');
      $property_type = $data['property_type'] = $this->input->get('property_type');
      $cars = $data['cars'] = $this->input->get('cars');
      $investment_range = $data['investment_range'] = $this->input->get('investment_range');
      $min_property_investment_range = $data['min_property_investment_range'] = $this->input->get('min_property_investment_range');
      $max_property_investment_range = $data['max_property_investment_range'] = $this->input->get('max_property_investment_range');

      if($bathrooms) $bathrooms = str_replace('--',' , ',$bathrooms);
      if($bedrooms) $bedrooms = str_replace('--',' , ',$bedrooms);
      if($property_type=="other") $property_type="";
      if($min_property_investment_range) $min_property_investment_range = $min_property_investment_range*1000000;
      if($max_property_investment_range) $max_property_investment_range = $max_property_investment_range*1000000;

        $search_criteria = array(
            'investment_amount' => $investment_amount,
            'investment_suburb' => $investment_suburb,
            'investment_address' => $investment_address,
            'bedrooms' => $bedrooms,
            'bathrooms' => $bathrooms,
            'cars' => $cars,
            'property_type' => $property_type,
            'investment_range' => $investment_range,
            'min_property_investment_range' => $min_property_investment_range,
            'max_property_investment_range' => $max_property_investment_range,
           
        );
        $data['search_criteria'] = $search_criteria;

        $count_search_properties = $this->property_model->GetAllSearchProperties(0, 0, '2', array(
            'user','campaign'
        ), 10000, array(
            'campaign_id' => 'desc'
        ),$search_criteria, $offset,'yes');

        $data['total_rows'] = $count_search_properties;

         $search_properties = $this->property_model->GetAllSearchProperties(0, 0, '2', array(
            'user','campaign'
        ), $limit, array(
            'campaign_id' => 'desc'
        ),$search_criteria, $offset,'');

        $data['search_properties'] = $search_properties;

      //echo $this->db->last_query();die;

      
     
        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/home/search_property', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
  }
}
