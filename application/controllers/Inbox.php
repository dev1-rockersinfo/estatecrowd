<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 * The Home class is basically the default controller for the script.
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Inbox extends ROCKERS_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->model('inbox_model');
        $this->load->model('home_model');
        $this->load->model('startequity_model');
        $this->load->helper('cookie');
        $this->load->library('securimage');

    }


    /*
    Function name :index()
    Parameter :$msg (message string)
    Return : none
    Use : user can see inbox messages
    */

    function index($msg = '')
    {
        check_user_authentication(true);
        $data = array();
        $limit = '1000';
        if (is_numeric($msg)) {
            $id = $msg;
            $msg = '';
        }

        if ($this->session->userdata('user_id') == '') {
            redirect('home/index');
        }
        $user_id = $this->session->userdata('user_id');
        $meta = meta_setting();
        $data['site_setting'] = site_setting();
        $data['msg'] = $msg;
        $data['inbox_messages'] = $this->inbox_model->GetAllMessage($this->session->userdata('user_id'), '0', '0', array('user'), $limit, array('message_id' => 'desc'));
        

        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/inbox/index', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();


    }

    /*
    Function name :message_conversation()
    Parameter :$id,$msg
    Return : none
    Use : user can see messages for particular subject
    */

    function message_conversation()
    {
        check_user_authentication(true);
        $data = array();
        
        $message_id = SecurePostData($this->input->post('message_id'));

        $limit = 1000;
        $user_id = $this->session->userdata('user_id');

        $chk_message_exist = $this->inbox_model->check_message_exist($message_id);

        if ($chk_message_exist != '') {
            $message_receiver_id = $chk_message_exist['receiver_id'];

        } else {
            redirect('inbox');
        }

        
        //===unread to read for new messages
        $chk_reply_message_exist = $this->inbox_model->chk_reply_message_exist($message_id);


        if ($chk_reply_message_exist) {
            foreach ($chk_reply_message_exist as $reply_message) {
                $reply_id = $reply_message['message_id'];
                $message_receiver_id = $reply_message['receiver_id'];

                if ($reply_id > 0) {
                    $message_id = $reply_id;
                }
                if ($message_receiver_id == $this->session->userdata('user_id')) {

                    $data_read = array('is_read' => 1);
                    $this->db->where('message_id', $message_id);
                    $this->db->update('message_conversation', $data_read);
                }
            }
        }
        //


        $data['list_messages'] = $this->inbox_model->GetAllMessage($this->session->userdata('user_id'), $message_id, '', array('user'), $limit, array('message_id' => 'asc'));


        $data['conversation_received_id'] = $data['list_messages'][0]['sender_id'];
        $data['conversation_sender_id'] = $data['list_messages'][0]['receiver_id'];
        
        $data['conversation_type'] =  (int) $data['list_messages'][0]['type'];
        $data['main_message_id'] = $message_id;

        $message['check_message_id'] = $message_id;
        

        $this->load->view('default/inbox/inbox_details', $data);

        
    }

    /*
    Function name :message_reply()
    Parameter :none
    Return : none
    Use : user can reply for the particular message
    */
    function message_reply()
    {



        check_user_authentication(true);
        $data = array();
        $message_id = $this->input->post('message_id');
        $message_setting = message_setting();
        $sender_info = UserData($this->session->userdata('user_id'));
         $data['error'] = '';
          $data['success'] = '';
        $receiver_info = UserData($this->input->post('receiver_id'));

        $site_setting = site_setting();
        $this->load->library('form_validation');
        $this->form_validation->set_rules('conversation', "Conversation", 'required');
        if ($this->form_validation->run() == FALSE) {
              if (validation_errors()) {
                $data['error'] = validation_errors();
            } else {
                $data['error'] = '';
            }
        } else {


            if ($message_setting->message_enable == 1 && $message_setting->email_user_on_new_message == 1) {

                $insert_reply = $this->inbox_model->insert_reply();
               
                $message_id = $this->input->post('message_id');
                $message_detail = $this->inbox_model->get_one_message($message_id);
                $property_id = $message_detail['property_id'];
                $conversation_type = $this->input->post('conversation_type');
              
                if ($conversation_type==1 || $conversation_type==2) 
                {
                    $getadmindata = adminsendmsg(1);
               
                    $user['user_name'] = $getadmindata->username;
                    $user['email'] = $getadmindata->email;
                    $user['view_message_link'] = '<a href="' . site_url('admin/message/view_message/'.$property_id.'/'.$this->session->userdata('user_id')) . '">' .CLICK_HERE . '</a>';
                    
                }
                else
                {
                    $user['user_name'] = $receiver_info[0]['user_name'] . ' ' . $receiver_info[0]['last_name'];
                    $user['email'] = $receiver_info[0]['email'];
                    $user['view_message_link'] = '<a href="' . site_url('inbox/message_conversation/'.$message_id) . '">' .CLICK_HERE . '</a>';
                }
                $user['message_user_name'] = '<a href="' . site_url('user/'.$sender_info[0]['profile_slug']) . '">'.$sender_info[0]['user_name'] . ' ' . $sender_info[0]['last_name'].'</a>';

                $user['dateadded'] = date('Y-m-d');
                $user['subject'] = $message_detail['message_subject'];
                $user['message'] = $this->input->post('conversation');
               
                $this->mailalerts('user_message', '', '', $user, 'Send message');
                $data['success'] = MESSAGE_SENT_SUCCESSFULLY_HOME;
            }

            $data['message_id'] = $message_id;
            
        }
        echo json_encode($data);
    }

    /*
    Function name :sendmessage()
    Parameter :equity_id, msg
    Return : none
    Use : user can send the message to the site team
    */
    function sendmessage($equity_id = '', $msg = '')
    {
        $msg_type=1; //=project owner discuss
        
        if($msg=='investor') {
            $msg_type=2;  //project investor discuss
        }

      

        $equity = GetOneEquity($equity_id);
        if(!empty($equity)){
            $equity_url = $equity['equity_url'];
            $company_name = $equity['company_name'];
            $taxonomy_setting = taxonomy_setting();
            $project_url = $taxonomy_setting['project_url'];
            $funds = $taxonomy_setting['funds'];
            $company_name_link = '<a href="' . site_url($project_url . '/' . $equity_url) . '">' . $company_name . '</a>';
            
            if ($msg == 'investor') {
                $subject = $company_name_link . ' ' . $funds . ' ' . PROCESS;
            } else {
                $subject = $company_name_link . ' ' . FEEDBACK;
            }
            
           
            $message_all_data = $this->inbox_model->GetAllEquityMessage($equity_id, $this->session->userdata('user_id'),$msg_type);
            //echo "<pre>"; print_r($message_all_data); die;
            //$message_data = $message_all_data[0];
              
            if (is_array($message_all_data)) {
                $message_id = $message_all_data[0]['message_id'];
            } else {

                $data_message =
                    array(
                        'sender_id' => $this->session->userdata('user_id'),
                        'receiver_id' => 1,
                        'is_read' => 1,
                        'message_subject' => $subject,
                        'message_content' => '',
                        'type' => $msg_type,
                        'equity_id' => $equity_id,
                        'date_added' => date('Y-m-d H:i:s'),
                        'admin_replay' => 'admin',
                    );


                $message_id = $this->inbox_model->AddInsertUpdateTable('message_conversation', '', '', $data_message);
                //if ($equity['user_id'] == $this->session->userdata('user_id')) {
                    project_activity_feedback('submit_by_owner', $this->session->userdata('user_id'), 1, $equity_id);
                //}
            }
            
            
            redirect('inbox/message_conversation/' . $message_id);
        } else {
            redirect('home');
        }
    }

}

?>
