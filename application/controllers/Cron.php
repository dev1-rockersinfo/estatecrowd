<?php ob_start();
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 * The Home class is basically the default controller for the script.
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Cron extends ROCKERS_Controller

{
    function __construct()
    {
        parent::__construct();
        $this->load->model('home_model');
        $this->load->model('equity_model');
        $this->load->model('account_model');
    }
    /*
    Function name :check_spam_report()
    Parameter :none
    Return : none
    Use :check spam data which are genrated as per spam setting and block ips if its consider as spam.
    */
    // ////////////===============check spam ip===========
    function check_spam_report()
    {
        $spam_control = $this->db->query("select * from spam_control");
        $control = $spam_control->row();
        $report_total = $control->spam_report_total;
        $total_register = $control->total_register;
        $report_expire = date('Y-m-d', strtotime('+' . $control->spam_report_expire . ' days'));
        $register_expire = date('Y-m-d', strtotime('+' . $control->register_expire . ' days'));
        $total_contact = $control->total_contact;
        $contact_expire = date('Y-m-d', strtotime('+' . $control->contact_expire . ' days'));
        // //////////////=======common check=================
        $get_report = $this->db->query("select spam_ip, count(*) as total from spam_report_ip group by spam_ip HAVING COUNT(*) >= " . $report_total);
        if ($get_report->num_rows() > 0) {
            $report = $get_report->result();
            foreach ($report as $rs) {
                $insert_spam = $this->db->query("insert into spam_ip(`spam_ip`,`start_date`,`end_date`)values('" . $rs->spam_ip . "','" . date('Y-m-d') . "','" . $report_expire . "')");
                $delete_from_report = $this->db->query("delete from spam_report_ip where spam_ip='" . $rs->spam_ip . "'");
            }
        }
        // //////////////=======common check=================
        // //////////////=======Registration check=================
        $chk_register = $this->db->query("select signup_ip, count(*) as total from user group by signup_ip HAVING COUNT(*) >= " . $total_register);
        if ($chk_register->num_rows() > 0) {
            $register_report = $chk_register->result();
            foreach ($register_report as $rs) {
                $insert_spam = $this->db->query("insert into spam_ip(`spam_ip`,`start_date`,`end_date`)values('" . $rs->signup_ip . "','" . date('Y-m-d') . "','" . $register_expire . "')");
            }
        }
        // //////////////=======Registration check=================
        // //////////////=======Inquire check=================
        $chk_inquiry = $this->db->query("select inquire_spam_ip, count(*) as total from spam_inquiry group by inquire_spam_ip HAVING COUNT(*) >= " . $total_contact);
        if ($chk_inquiry->num_rows() > 0) {
            $inquiry_report = $chk_inquiry->result();
            foreach ($inquiry_report as $rs) {
                $insert_spam = $this->db->query("insert into spam_ip(`spam_ip`,`start_date`,`end_date`)values('" . $rs->inquire_spam_ip . "','" . date('Y-m-d') . "','" . $contact_expire . "')");
            }
        }
        // //////////////=======Inquire check=================
    }
    // ////////////===============make spam ip expire===========
    /*
    Function name :allow_block_ip()
    Parameter :none
    Return : none
    Use :release black ip which are consider as spam
    */
    function allow_block_ip()
    {
        $check_expire = $this->db->query("select * from spam_ip where permenant_spam!='1' and end_date='" . date('Y-m-d') . "'");
        if ($check_expire->num_rows() > 0) {
            $expire = $check_expire->result();
            foreach ($expire as $exp) {
                $delete_spam = $this->db->query("delete from spam_ip where spam_ip='" . $exp->spam_ip . "'");
            }
        }
    }

    /*
    Function name :cron_preapprove()
    Parameter :none
    Return : none
    Use :this function used for relased all pre approval payment,end the equity ,and update all data according to equity status.
    */
    function cron_preapprove($admin = '')
    {
        $this->user_login_del();
        $site_setting = site_setting();

        if ($site_setting['currency_code'] != '') {
            $currency_code = $site_setting['currency_code'];
        } else {
            $currency_code = 'USD';
        }


        // //////=============email=====================
        $this->load->library('email');
        $email_setting = $this->db->query("select * from `email_setting` where email_setting_id='1'");
        $email_set = $email_setting->row();
        // /////====smtp====
        if ($email_set->mailer == 'smtp') {
            $config['protocol'] = 'smtp';
            $config['smtp_host'] = trim($email_set->smtp_host);
            $config['smtp_port'] = trim($email_set->smtp_port);
            $config['smtp_timeout'] = '30';
            $config['smtp_user'] = trim($email_set->smtp_email);
            $config['smtp_pass'] = trim($email_set->smtp_password);
        } // ///=====sendmail======
        elseif ($email_set->mailer == 'sendmail') {
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = trim($email_set->sendmail_path);
        } // ///=====php mail default======
        else {
        }
        $config['wordwrap'] = TRUE;
        $config['mailtype'] = 'html';
        $config['crlf'] = '\n\n';
        $config['newline'] = '\n\n';
        $this->email->initialize($config);

        $sql = "select project_timezone,equity_id from equity where status='2'";
        $get_timezoneproject = $this->db->query($sql);
        if ($get_timezoneproject->num_rows() > 0)
        {
            $timezoneproject = $get_timezoneproject->result();
                            
            foreach($timezoneproject as $timezoneprjs)
            {
                $timezone= $timezoneprjs->project_timezone;                     
                set_timezone($timezone);
                  log_message('error', "TIMEZONE:" . $timezone);        
                        
                            $sql = "select * from equity where end_date<='" . date("Y-m-d H:i:s") . "' and status='2'";
                            $get_equity = $this->db->query($sql);
                       
                        
                         log_message('error', "SQL:" . $sql);

                    if ($get_equity->num_rows() > 0) {
                        $email_template = $this->db->query("select * from `email_template` where task='New Fund Admin Notification'");
                        $email_temp = $email_template->row();
                        $email_address_from = $email_temp->from_address;
                        $email_address_reply = $email_temp->reply_address;
                        $equity = $get_equity->result();

                        foreach ($equity as $prjs) {
                            if ($prjs->funding_type == 'Fixed') {
                                // if achieved goal
                                if ($prjs->amount_get >= $prjs->goal) {
                                    $this->fixed_success($prjs, $currency_code);
                                } else {
                                    $this->fixed_failure($prjs, $currency_code);
                                }
                            } else {
                                // //////=======FLXIBLE PROJECT
                                if ($prjs->amount_get >= $prjs->goal) {
                                    $this->flexible_success($prjs, $currency_code);
                                } else {
                                    $this->flexible_unsuccess($prjs, $currency_code);
                                }
                            }

                            if ($prjs->amount_get >= $prjs->goal) {
                                // for success equity with achieve goal
                                $this->db->query("update equity set `status`='3' ,is_featured = '0'  where equity_id='" . $prjs->equity_id . "'");
                            } else {
                                // for unsuccess equity with not achieve goal
                                $this->db->query("update equity set `status`='5' ,is_featured = '0' where equity_id='" . $prjs->equity_id . "'");
                            }
                        }
                        $user_id = $prjs->user_id;
                        $equity_id = $prjs->equity_id;
                        $equity_url = $prjs->equity_url;
                        project_other_deletecache('equity_donation', $equity_id);
                        project_other_deletecache('perk', $equity_id);
                        project_deletecache($equity_id, $equity_url);
                        project_other_deletecache('equity_donation', $equity_url);
                        setting_deletecache('equity_detail_perks' . $equity_url);
                        setting_deletecache('user_equitys_' . $user_id);
                        setting_deletecache('user_my_donation_' . $user_id);
                        /////===foreach equity
                    }
            }
        }

        if ($admin == 'yes') {
            ////=====get equity
            $data = array(
                'user_id' => $this->session->userdata('admin_id'),
                'cronjob' => 'set_auto_ending',
                'date_run' => date('Y-m-d H:i:s'),
                'status' => '1',
            );
            $this->db->insert('cronjob', $data);
            // other cron job function
            $this->check_spam_report();
            $this->allow_block_ip();
            $this->cron_forgot_password();
            setting_deletecache('spam_protectd');
            redirect('admin/cronjob/list_cronjob/insert');
        } else {
            ////=====get equity
            $data = array(
                'user_id' => '0',
                'cronjob' => '',
                'date_run' => date('Y-m-d H:i:s'),
                'status' => '1',
            );
            $this->db->insert('cronjob', $data);
            // other cron job function
            $this->check_spam_report();
            $this->allow_block_ip();
            $this->cron_forgot_password();
            setting_deletecache('spam_protectd');

        }
        die;
    }
    /*
    Function name :fixed_success()
    Parameter :prjs,currency_code
    Return : none
    Use :this function used for adaptive pre approval paypal.
    Used in case of equity type is fixed and get successfully  all donation
    */
    // fixed equity achieved goal
    function fixed_success($prjs, $currency_code)
    {
        $site_setting = site_setting();

        $id = $prjs->equity_id;
        $get_equity_user_detail = $this->equity_model->GetAllEquities(0, $id);
        $prj = $get_equity_user_detail[0];
        if ($prj['equity_currency_code'] != '') {
            $currency_code = $prj['equity_currency_code'];
        }
        $user_detail = $this->account_model->GetAllUsers($prj['user_id']);
        $user_detail = $user_detail[0];
        $company_name = $prj['company_name'];
        $equity_id = $prjs->equity_id;

        $equity_create_name = $user_detail['user_name'] . ' ' . $user_detail['last_name'];
        $sql = "select *,transaction.email as donor_email  from transaction where equity_id='" . $prjs->equity_id . "' and preapproval_key!='' and (preapproval_status ='SUCCESS' )";
        $get_preapproval = $this->db->query($sql);
        if ($get_preapproval->num_rows() > 0) {
            $transaction_detail = $get_preapproval->result();

            foreach ($transaction_detail as $transd) {


                $update_equity_inactive = $this->db->query("update equity set status=3 ,is_featured = '0' where equity_id='" . $transd->equity_id . "'");
                $donar_proj = $this->account_model->GetAllUsers($transd->user_id);
                $donar_proj = $donar_proj[0];
                admin_activity('project_success', $transd->user_id, '', $transd->equity_id);

                $this->adminalerts($prj, $transd, $donar_proj);

                // ///////===========for payment redirect user to this link=============
            } ///=====foreach preapproval
        } ////====if found any key


    }
    /*
    Function name :fixed_failure()
    Parameter :prjs,currency_code
    Return : none
    Use :this function used for adaptive pre approval paypal.
    Used in case of equity type is fixed and not get successfully all donation.
    */
    // fixed equity not achieved goal
    function fixed_failure($prjs, $currency_code)
    {
        // for failure equity
        $site_setting = site_setting();


        $subsql = "select equity_id from equity where  equity_id='" . $prjs->equity_id . "'";
        $sql = "select *,transaction.email as donor_email from transaction where equity_id in (" . $subsql . ") and preapproval_key!='' and preapproval_status!='FAIL'";
        $get_preapproval = $this->db->query($sql);
        if ($get_preapproval->num_rows() > 0) {
            $transaction_detail = $get_preapproval->result();
            foreach ($transaction_detail as $transd) {
                if ($site_setting['currency_code'] != '') {
                    $currency_code = $site_setting['currency_code'];
                } else {
                    $currency_code = 'USD';
                }
                $preapprovalKey = $transd->preapproval_key;
                if (!empty($preapprovalKey)) {
                    $update_equity_inactive = $this->db->query("update equity set status=5 ,is_featured = '0' where equity_id='" . $transd->equity_id . "'");
                    $update_status = $this->db->query("update transaction set `preapproval_status`='FAIL' , payment_status = 1 where `preapproval_key`='" . $preapprovalKey . "'");
                    $equity_id = $transd->equity_id;
                    $perk_id = $transd->perk_id;
                    $equity_owner_amount = $transd->amount;
                    $get_don_equity = $this->db->get_where('equity', array(
                        'equity_id' => $equity_id
                    ));
                    $don_prj = $get_don_equity->row_array();
                    admin_activity('project_unsuccess', $transd->user_id, '', $transd->equity_id);
                    $equity_url = $don_prj['equity_url'];
                    project_other_deletecache('equity_donation', $equity_id);
                    project_other_deletecache('perk', $equity_id);
                    project_deletecache($equity_id, $equity_url);
                    project_other_deletecache('equity_donation', $equity_url);
                    project_deletecache('equity_detail_perks' . $equity_url);
                }
            }
        }
    }
    /*
    Function name :flexible_success()
    Parameter :prjs,currency_code
    Return : none
    Use :this function used for adaptive pre approval paypal.
    Used in case of equity type is flexible and  get successfully all donation.
    */
    // flexible equity achieved goal
    function flexible_success($prjs, $currency_code)
    {
        // //////=======FLXIBLE PROJECT
        $site_setting = site_setting();

        $id = $prjs->equity_id;
        $get_equity_user_detail = $this->equity_model->GetAllEquities(0, $id);
        $prj = $get_equity_user_detail[0];
        if ($prj['equity_currency_code'] != '') {
            $currency_code = $prj['equity_currency_code'];
        }
        $user_detail = $this->account_model->GetAllUsers($prj['user_id']);
        $user_detail = $user_detail[0];
        $company_name = $prj['company_name'];
        $equity_id = $prjs->equity_id;

        $equity_create_name = $user_detail['user_name'] . ' ' . $user_detail['last_name'];
        $sql = "select *,transaction.email as donor_email from transaction where equity_id='" . $prjs->equity_id . "' and preapproval_key!='' and (preapproval_status='SUCCESS')";
        $get_preapproval = $this->db->query($sql);
        if ($get_preapproval->num_rows() > 0) {
            $transaction_detail = $get_preapproval->result();
            foreach ($transaction_detail as $transd) {

                $update_equity_inactive = $this->db->query("update equity set status=3 ,is_featured = '0' where equity_id='" . $transd->equity_id . "'");
                $donar_proj = $this->account_model->GetAllUsers($transd->user_id);
                $donar_proj = $donar_proj[0];
                admin_activity('project_success', $transd->user_id, '', $transd->equity_id);
                $this->adminalerts($prj, $transd, $donar_proj);


                // /redirect($payPalURL);
                // ///////===========for payment redirect user to this link=============
            } ///=====foreach preapproval
        } ////====if found any key


    }
    /*
    Function name :flexible_unsuccess()
    Parameter :prjs,currency_code
    Return : none
    Use :this function used for adaptive pre approval paypal.
    Used in case of equity type is flexible and  not get successfully all donation.
    */
    // flexible equity not achieved goal
    function flexible_unsuccess($prjs, $currency_code)
    {
        // //////=======FLXIBLE PROJECT
        $site_setting = site_setting();

        $id = $prjs->equity_id;
        $get_equity_user_detail = $this->equity_model->GetAllEquities(0, $id);
        $prj = $get_equity_user_detail[0];
        if ($prj['equity_currency_code'] != '') {
            $currency_code = $prj['equity_currency_code'];
        }
        $user_detail = $this->account_model->GetAllUsers($prj['user_id']);
        $user_detail = $user_detail[0];
        $company_name = $prj['company_name'];
        $equity_id = $prjs->equity_id;

        $equity_create_name = $user_detail['user_name'] . ' ' . $user_detail['last_name'];
        $get_preapproval = $this->db->query("select *,transaction.email as donor_email from transaction where equity_id='" . $prjs->equity_id . "' and preapproval_key!='' and  (preapproval_status='ON_HOLD')");
        if ($get_preapproval->num_rows() > 0) {
            $transaction_detail = $get_preapproval->result();
            foreach ($transaction_detail as $transd) {

                if ($site_setting['currency_code'] != '') {
                    $currency_code = $site_setting['currency_code'];
                } else {
                    $currency_code = 'USD';
                }
                $preapprovalKey = $transd->preapproval_key;
                if (!empty($preapprovalKey)) {
                    $update_equity_inactive = $this->db->query("update equity set status=5 ,is_featured = '0' where equity_id='" . $transd->equity_id . "'");
                    $update_status = $this->db->query("update transaction set `preapproval_status`='FAIL' , payment_status = 1  where `preapproval_key`='" . $preapprovalKey . "'");
                    $equity_id = $transd->equity_id;
                    $perk_id = $transd->perk_id;
                    $equity_owner_amount = $transd->amount;
                    $get_don_equity = $this->db->get_where('equity', array(
                        'equity_id' => $equity_id
                    ));
                    $don_prj = $get_don_equity->row_array();
                    admin_activity('project_unsuccess', $transd->user_id, '', $transd->equity_id);
                    $equity_url = $don_prj['equity_url'];
                    project_other_deletecache('equity_donation', $equity_id);
                    project_other_deletecache('perk', $equity_id);
                    project_deletecache($equity_id, $equity_url);
                    project_other_deletecache('equity_donation', $equity_url);
                    project_deletecache('equity_detail_perks' . $equity_url);
                }

            }
        }


    }

    /*
    Function name :cron_forgot_password()
    Parameter :none
    Return : none
    Use : Delete the all forget password links after specific duration which is set from Admin.
    */
    function cron_forgot_password()
    {
        $site_setting = site_setting();
        $query = $this->db->get_where('user', array(
            'forgot_unique_code !=' => ''
        ));
        if ($query->num_rows() > 0) {
            $result = $query->result();
            foreach ($result as $row) {
                if (strtotime(date("Y-m-d H:i:s")) > strtotime(date("Y-m-d H:i:s", strtotime($row->request_date . ' + ' . $site_setting['forget_time_limit'] . ' hours')))) {
                    $this->db->query("update user set forgot_unique_code='' where forgot_unique_code='" . $row->forgot_unique_code . "'");
                }
            }
        }
    }

    /*
    Function name :cron_forgot_password()
    Parameter :none
    Return : none
    Use : Delete the all login histroy data after a week.
    */
    function user_login_del()
    {
        $date = date("Y-m-d H:i:s");
        $timestamp = strtotime($date);
        $day = date('l', $timestamp);
        $delete_date = date("Y-m-d");
        if ($day == 'Monday') {
            $this->db->where('DATE(user_login.login_date_time) <', $delete_date);
            $this->db->delete('user_login');

            $this->db->where('DATE(admin_login.login_date) <', $delete_date);
            $this->db->delete('admin_login');

            $this->db->where('DATE(cronjob.date_run) <', $delete_date);
            $this->db->delete('cronjob');
        }
    }

}

?>
