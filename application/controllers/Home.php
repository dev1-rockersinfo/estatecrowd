<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 * The Home class is basically the default controller for the script.
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Home extends ROCKERS_Controller
{
    /**
     * Constructor method
     */
    function __construct()
    {
        parent::__construct();
        $this->load->model('account_model');
        $this->load->model('activity_model');

        $this->load->model('follower_model');
        $this->load->model('following_model');
        $this->load->model('home_model');
        $this->load->model('property_model');
        $this->load->helper('cookie');
        $this->load->library('securimage');
        $this->load->library('email');
        $this->load->library('password');
        $this->load->library('fb_connect');

    }

    /*
	Function name :index()
	Parameter :$msg (message string)
	Return : none
	Use : user can see feature equities,latest equities and other detail
	Description : site home page its default function which called http://hostname/home
	SEO friendly URL which is declare in config route.php file  http://hostname/
	*/
    function index($msg = '')
    {

        image_clener_db();
        $user_id = check_user_authentication(false);
        $data = array();
        $offset = 0;
        $limit = 100;
        if (is_numeric($msg)) 
        {
            $id = $msg;
            $msg = '';
        }
        $meta = meta_setting();
        $data['site_setting'] = site_setting();
        $data['limit'] = $limit;
        $data['offset'] = $offset;
       
        $latest_properties = $this->property_model->GetAllProperties(0, 0, '2', array(
            'user','campaign'
        ), $limit, array(
            'property_id' => 'desc'
        ), $offset,'','yes');
        $data['latest_properties'] = $latest_properties;
        
        
        $get_all_slider_data = $this->home_model->GetAllSliderData(array('view_order' => 'asc'));
        $data['allbanner'] = $get_all_slider_data;

        $data['suburbs'] = $this->home_model->get_properties_suburbs(array(2),10);


        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        //$this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->load->helper('url');
        $this->load->library('user_agent');
        if ($this->agent->browser() == 'Internet Explorer' and $this->agent->version() <= 6) {
            $this->template->write_view('main_content', 'default/home/unsupported', $data, TRUE);
        } else {
            $this->template->write_view('main_content', 'default/home/index', $data, TRUE);
        }
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
        //$this->output->enable_profiler(TRUE);
    }

    /*
	Function name :my_equity()
	Parameter :$msg (message string)
	Return : none
	Use : user can see feature equities,latest equities and other detail
	Description : site home page its default function which called http://hostname/home
	SEO friendly URL which is declare in config route.php file  http://hostname/
	*/
    function my_campaign($msg = '')
    {
      //  redirect('home/main_dashboard');
        $user_id = check_user_authentication(true);
        $data = array();
        $offset = 0;
        $limit = 3;
        if (is_numeric($msg)) {
            $id = $msg;
            $msg = '';
        }
        $meta = meta_setting();
        $data['site_setting'] = site_setting();
        $data['msg'] = $msg;
        // all latest equity
         $user_property_campaign = $this->property_model->GetAllProperties(0, 0, '', array(
            'user','campaign'
        ), 1000, array(
            'campaign_id' => 'desc'
        ), '','','yes',$user_id);

        $data['user_property_campaign'] = $user_property_campaign;

        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/home/my_campaign', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
    }

    /*
	Function name :export_user_equity_data()
	Parameter :1
	Return : none
	Use : This function is used for generating csv file about user's equities(Note:But it is not used in actual script).
	*/
    function export_user_equity_data($time = '')
    {
        $user_id = check_user_authentication(true);
        $this->load->dbutil();
        $query = $this->db->query("SELECT equity.project_name , equity.date_added, equity.end_date, equity.amount, IFNULL( equity.amount_get, 0 ) AS amount_get, 

        CASE equity.active

        WHEN  '0'

        THEN  'draft'

        WHEN  '1'

        THEN  'pending'

        WHEN  '2'

        THEN  'active'

        WHEN  '3'

        THEN  'successful'

        WHEN  '4'

        THEN  'unsuccessful'

        WHEN  '5'

        THEN  'failure'

        WHEN  '6'

        THEN  'decline_admin'

        END AS active_stat

        FROM equity

        WHERE user_id =" . $user_id . "

        AND equity.active

        IN ( 1, 2, 3, 4, 5 ) ");
        $delimiter = ",";
        $newline = "\r\n";
        $csv = $this->dbutil->csv_from_result($query, $delimiter, $newline);
        $file_name = "active_user_" . date('d-m-Y') . ".csv";
        $this->force_download($file_name, $csv);
        $this->db->cache_delete_all();
    }

    /*
	Function name :main_dashboard()
	Parameter :1
	Return : none
	Use : This function is used for displaying user dashboard after login.
	Description : This function is called http://hostname/home/main_dashboard.
	*/
    function main_dashboard($status = '', $msg = '')
    {

        $user_id = check_user_authentication(true);
        $meta = meta_setting();
        $data['site_setting'] = site_setting();
        $data['msg'] = $msg;
        // user equities
        
        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/home/main_dashboard', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
    }

    /*
	Function name :dashboard_search()
	Parameter :none
	Return : none
	Use : This function is use for equity status.
		
	*/

    function dashboard_search($str)
    {
        $user_id = check_user_authentication(true);
        if ($str == 'all') {
            $user_equities = $this->equity_model->GetAllEquities($user_id, 0, 0, 0, array(
                'user'
            ), null, array(
                'equity_id' => 'desc'
            ));
        } else {
            $user_equities = $this->equity_model->GetAllEquities($user_id, 0, 0, $str, array(
                'user'
            ), null, array(
                'equity_id' => 'desc'
            ));
        }


        $data['user_equities'] = $user_equities;
        $this->load->view('default/home/ajax_search_equity_status', $data);
    }


    function error()
    {

        $meta = meta_setting();
        $data['site_setting'] = site_setting();
        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/home/fatal', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
    }

    /*
	Function name :error_page()
	Parameter :none
	Return : none
	Use : This function is use for displaying 404 page.
		This error_page function is called http://hostname/home/error_page.

	*/
    function error_page()
    {
        $meta = meta_setting();
        $data['site_setting'] = site_setting();
        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/home/404_page', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
    }

    /*
	Function name :login()
	Parameter :none
	Return : none
	Use : This function is use for login user.
	Description : This login function which called http://hostname/home/login.
	Done By : Dharmesh
	*/
    function login($return_url = '')
    {
        if (check_user_authentication() == true) {
            redirect('home');
        }
        $meta = meta_setting();
        $site_setting = site_setting();
        $data['site_setting'] = site_setting();
        // this varible come from start equity invite member
        $invite_data = '';

        if ($return_url == "invite") {
            $referral_cookie = get_cookie('fundraising_member', TRUE);
            if (isset($referral_cookie)) {
                $invite_data = $referral_cookie;
            } else {
                if (isset($_COOKIE['fundraising_member'])) {
                    $invite_data = $_COOKIE['fundraising_member'];
                } elseif (isset($HTTP_COOKIE_VARS['fundraising_member'])) {
                    $invite_data = $HTTP_COOKIE_VARS['fundraising_member'];
                }
            }

            if ($invite_data != "") {
                $invite_data_msg = '';
                $arr = explode("/", $invite_data);
                if (count($arr) > 1) {
                    $equity_id = $arr[3];
                    $user_id = $arr[4];
                    $get_equity_user_detail = $this->equity_model->GetAllEquities(0, $equity_id);
                    $equity = $get_equity_user_detail[0];
                    if ($user_id <= 0) $user_id = $prj['user_id'];
                    $user_detail = $this->account_model->GetAllUsers($user_id);
                    $user_detail = $user_detail[0];
                    $company_name = $equity['company_name'];
                    if ($company_name == '') $company_name = 'Untitled';
                    $user_name = $user_detail['user_name'] . " " . $user_detail['last_name'];
                    $user_email = $arr[1];
                    $redirct = 'equity/invite_thankyou/' . $arr[2] . '/' . $arr[3];
                    $return_url = base64_encode($redirct);
                    $invite_data_msg = sprintf(INVITE_MSGS, $user_name, $company_name, $site_setting['site_name']);
                }
            } else {
                $return_url = '';
            }
        }
        $data['femail'] = '';
        $temp = $this->input->cookie('tubestart');
        $data['email'] = '';
        $data['password'] = '';
        $data['remember'] = '';
       
        $data['page'] = $return_url;
        if (!empty($temp)) {
            $arr = explode("^", $temp);
            if (isset($arr[0])) $data['email'] = $arr[0];
            if (isset($arr[1])) $data['password'] = $arr[1];
            $data['remember'] = '1';
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', EMAIL_ADDRESS, 'required|valid_email|trim');
        $this->form_validation->set_rules('password', PASSWORD, 'required|trim|min_length[8]|max_length[20]');
        $chk_user = 'false';
        //   Check spamer

        $spamer = $this->home_model->spam_protection();
        if ($spamer) {
            $chk_user = 'true';
        }
        if ($this->form_validation->run() == FALSE || $chk_user == 'true') {
            
            if (validation_errors()) {
               
                $this->session->set_flashdata('error_message_login', validation_errors());
            } else {
              
            }
            if ($chk_user == 'true') {
                $spam_message = "<p>" . IPBAND_CANNOTREG_NOW_SIGNIN . "</p>";
                $this->session->set_flashdata('error_message_login', $spam_message);
            }
            if ($_POST) {
                $data['email'] = SecurePostData($this->input->post('email'));
                $data['page'] = SecurePostData($this->input->post('page'));
                $data['password'] = '';
                $data['remember'] = SecurePostData($this->input->post('remember'));
            }
        } else {
            $login_data = array(
                'email' => SecurePostData($this->input->post('email')),
                'password' => SecurePostData(md5($this->input->post('password'))),
            );


            $chk = $this->home_model->user_validate($login_data, 'login');

            if ($chk != 'FALSE') {
                if ($chk['active'] == '1') {
                    $data = array(
                        'user_id' => $chk['user_id'],
                        'user_name' => $chk['user_name'],
                        'last_name' => $chk['last_name'],
                        'email' => $chk['email'],
                    );
                    $this->session->set_userdata($data);
                    if ($this->input->post('remember') == 1) {
                        // delete cokiies and reset it
                        $cookie = array(
                            'name' => 'tubestart',
                            'value' => SecurePostData($this->input->post('email')) . "^" . SecurePostData($this->input->post('password')),
                            'expire' => -time() + (3600 * 24),
                            'domain' => '',
                            'path' => '/',
                            'prefix' => ''
                        );
                        $this->input->set_cookie($cookie);
                        $cookie = array(
                            'name' => 'tubestart',
                            'value' => SecurePostData($this->input->post('email')) . "^" . SecurePostData($this->input->post('password')),
                            'expire' => time() + (3600 * 24),
                            'domain' => '',
                            'path' => '/',
                            'prefix' => ''
                        );
                        $this->input->set_cookie($cookie);
                    } else {
                        set_cookie(false);
                    }
                    if ($invite_data != "") {
                        $arr = explode("/", $invite_data);
                        $sql = "update invite_members set status=1 ,code='' where code='" . $arr[0] . "'";
                        $this->db->query($sql);
                        delete_cookie('fundraising_member');
                    }
                    $data['page'] = SecurePostData($this->input->post('page'));
                    if ($data['page'] != '') {
                        $return_page = base64_decode($data['page']);
                        redirect($return_page."/login");
                    } else {
                        $user_name = $this->session->userdata('user_name');
                        $last_name = $this->session->userdata('last_name');
                        $full_name = $user_name . ' ' . $last_name;
                        //  echo $full_name;

                        $login_user_data = $this->home_model->getTableData('user_login',array('user_id'=>$this->session->userdata('user_id')));
                        $user_login_count = count($login_user_data);
                        if($user_login_count == 1)
                        {
                            redirect('account/first_step');
                        }
                        else
                        {   
                            $message_show = sprintf(WELCOME_LOGIN_SUCCESSFULL, $full_name);
                            $this->session->set_flashdata('success_message', $message_show);
                            redirect('');
                           
                        }
                    }

                } elseif ($chk['active'] == '2') {
                    
                    $this->session->set_flashdata('error_message_login', YOUR_EMAIL_ADDRESS_IS_SUSPENDED);
                } elseif ($chk['active'] == '0') {
                 
                    $this->session->set_flashdata('error_message_login', YOUR_ACCOUNT_INACTIVE_ADMIN);
                } else {
                    $inactive_user = INACTIVE_ACCOUNT_AFTER_SIGNUP . "<br/>" . RESEND_VERIFICATION_LINK . " " . anchor('home/resend', CLICK_HERE, 'style="color:#870835"');
                    $this->session->set_flashdata('error_message_login', $inactive_user);
                }
            } else {
                
                $this->session->set_flashdata('error_message_login', EMAIL_ADDRESS_OR_PASSWORD_ARE_INVALID);
            }
        }
        if ($invite_data != "") {
          
             $this->session->set_flashdata('error_message_login', $invite_data_msg);
        }

        if ($return_url == "inactive") {
            
             $this->session->set_flashdata('error_message_login', YOUR_ACCOUNT_INACTIVE_ADMIN);

        } else if ($return_url == "suspend") {
            
            $this->session->set_flashdata('error_message_login', YOUR_EMAIL_ADDRESS_IS_SUSPENDED);
        }

        $data['page'] = $return_url;


        $this->template->write('meta_title', LOGIN . ' -' . $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/home/login', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
    }

    /*
	Function name :forget_password()
	Parameter :none
	Return : none
	Use : This function is use to send forget password to user's account.
	Description : forget_password function which called http://hostname/home/forget_password.
	*/
    function forget_password()
    {
        $data['email'] = '';
        $data['femail'] = '';
        $data['page'] = '';
        $data['password'] = '';
        $data['remember'] = '';
       
        $this->load->library('form_validation');
        $this->form_validation->set_rules('femail', EMAIL_ADDRESS, 'required|valid_email|trim');
        $chk_user = 'false';
        //   Check spamer


        $spamer = $this->home_model->spam_protection();
        if ($spamer == 1 || $spamer == '1') {
            $chk_user = 'true';
        }
        if ($this->form_validation->run() == FALSE || $chk_user == 'true') {
            $data['error_forget'] = "";
            if (validation_errors()) {
                $this->session->set_flashdata('error_message_login', validation_errors());
            } else {
                
            }
            if ($chk_user == 'true') {
                
                 $this->session->set_flashdata('error_message_login', IPBAND_CANNOTREG_NOW);
            }
            $data['femail'] = SecurePostData($this->input->post('femail'));
        } else {
            $em = array(
                'email' => SecurePostData($this->input->post('femail'))
            );


            $check_email = $this->home_model->user_validate($em);
            if ($check_email != '') {
                if ($check_email['active'] == 1) {
                    $forgot_unique_code = randomNumber(20);
                    $fdata = array(
                        'forgot_unique_code' => $forgot_unique_code,
                        'request_date' => date('Y-m-d H:i:s'),
                    );


                    $set = $this->account_model->UpdateAccount('user_id', $check_email['user_id'], 'user', $fdata);
                    $cache_file_name = 'user_detail' . $check_email['user_id'];
                    setting_deletecache($cache_file_name);
                    $cache_file_name = 'user_detail_notification' . $check_email['user_id'];
                    setting_deletecache($cache_file_name);
                    if ($set != '') {
                        $result = UserData($check_email['user_id']);
                        $email_template = $this->db->query("select * from `email_template` where task='Forgot Password Request'");
                        $email_temp = $email_template->row();
                        $email_address_from = $email_temp->from_address;
                        $email_address_reply = $email_temp->reply_address;
                        $email_subject = $email_temp->subject;
                        $email_message = $email_temp->message;
                        $username = $result[0]['user_name'];
                        $email = $result[0]['email'];
                        $email_to = $email;
                        // $login_link=site_url('home/reset_password/'.$result[0]['forgot_unique_code']);
                        $login_link = '<a href="' . site_url('home/reset_password/' . $result[0]['forgot_unique_code']) . '">' . site_url('home/reset_password/' . $result[0]['forgot_unique_code']) . '</a>';
                        $email_message = str_replace('{break}', '<br/>', $email_message);
                        $email_message = str_replace('{user_name}', $username, $email_message);
                        $email_message = str_replace('{login_link}', $login_link, $email_message);
                        $str = $email_message;
                        email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);
                    } else {
                        
                        $this->session->set_flashdata('error_message_login', RESET_PASSWORD_FAILED);
                    }
                    $this->session->set_flashdata('success_message_login',RESET_PASSWORD_LINK_SENT_SUCCESSFULLY_VIEW);
                } elseif ($check_email['active'] == 2) {
                   
                    $this->session->set_flashdata('error_message_login', YOUR_EMAIL_ADDRESS_IS_SUSPENDED);
                } elseif (is_null($check_email['active']) || $check_email['active'] == 0) {
                    $resend_message =INACTIVE_ACCOUNT_AFTER_SIGNUP . "<br/>" . RESEND_VERIFICATION_LINK . " " . anchor('home/resend', CLICK_HERE, 'style="color:#870835"');
                     $this->session->set_flashdata('error_message_login', $resend_message);

                }
            } else {
                
                 $this->session->set_flashdata('error_message_login', EMAIL_ADDRESS_NOT_FOUND);
            }
        }
        $meta = meta_setting();
        $data['site_setting'] = site_setting();
        $this->template->write('meta_title', LOGIN . ' -' . $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/home/login', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
    }

    /*
	Function name :reset_password()
	Parameter :forgot_unique_code
	Return : none
	Use : This function is use for reset the user password.
	*/
    function reset_password($forgot_unique_code = null)
    {
        $chk_forget_code = $this->home_model->checkForgetUniqueCode(SecurePostData($forgot_unique_code));
        if ($chk_forget_code == 'expire') {
            redirect('home/index/expire');
        }
        if ($chk_forget_code == 'not_sent') {
            redirect('home/index/not_sent');
        }
        $user_id = $chk_forget_code;
        $this->load->library('form_validation');
        $this->form_validation->set_rules('password', PASSWORD, 'required|min_length[8]|max_length[20]|trim');
        $this->form_validation->set_rules('cpassword', RETYPE_PASSWORD, 'required|min_length[8]|max_length[20]|matches[password]|trim');
        $chk_user = 'false';
        $spamer = $this->home_model->spam_protection();
        if ($spamer == 1 || $spamer == '1') {
            $chk_user = 'true';
        }
        if ($this->form_validation->run() == FALSE || $chk_user == 'true') {
            $data['error'] = "";
            if (validation_errors()) {
                $data['error'] = validation_errors();
            } else {
                $data['error'] = '';
            }
            if ($chk_user == 'true') {
                $spam_message = "<p>" . IPBAND_CANNOTREG_NOW . "</p>";
                $data['error'] = $spam_message;
            }
            if ($this->input->post('user_id')) {
                $data['user_id'] = SecurePostData($this->input->post('user_id'));
                $data['forgot_unique_code'] = SecurePostData($this->input->post('forget_unique_code'));
            } else {
                $data['user_id'] = $user_id;
                $data['forgot_unique_code'] = $forgot_unique_code;
            }
            $data['password'] = '';
            $data['cpassword'] = '';
        } else {
            $spass = array(
                'password' => SecurePostData(md5($this->input->post('password')))
            );
            $set = $this->home_model->setNewPassword($user_id, $spass);
            if ($set != '') {
                redirect('home/index/reset_success');
            } else {
                redirect('home/index/reset_fail');
            }
        }
        $meta = meta_setting();
        $data['site_setting'] = site_setting();
        $this->template->write('meta_title', RESET_PASSWORD . ' -' . $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/home/reset_password', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
    }

    /*
	Function name :findreferid($findreferid='')
	Parameter :findreferid
	Return : none
	Use : This function is use to .
	*/
    function findreferid($findreferid = '')
    {
        $reference_user_id = 0;
        if ($findreferid != '') {
            $invite_code = $findreferid;
        } else {
            // $referral_cookie=get_cookie('invite_code',TRUE);
            $referral_cookie = get_cookie('invite_code', TRUE);
            if (isset($referral_cookie)) {
                $invite_code = $referral_cookie;
            } else {
                if (isset($_COOKIE['invite_code'])) {
                    $invite_code = $_COOKIE['invite_code'];
                } elseif (isset($HTTP_COOKIE_VARS['invite_code'])) {
                    $invite_code =  $HTTP_COOKIE_VARS['invite_code'];
                }
                else
                {
                    $invite_code = '';
                }
            }
        }
        if ($invite_code != '') {
            $check_reference_user = $this->db->get_where('user', array(
                'unique_code' => $invite_code
            ));
            if ($check_reference_user->num_rows() > 0) {
                $get_reference_user = $check_reference_user->row();
                $reference_user_id = $get_reference_user->user_id;
                $reference_user_id;
            } else {
                $reference_user_id = 0;
            }
        }

    }

    /*
	Function name :signup($invite_code='')
	Parameter :code
	Return : none
	Use : This function is use for register user.
	     site register page its Signup function which called http://hostname/home/signup.
	*/
    function signup($invite_code = '',$invite_email='')
    {
        $sf_referrer = '';
        $sf_syndicate = '';
               

        $projectControllerName=projectcontrollername();
        $site_setting = site_setting();
        $data['site_setting'] = $site_setting;
        $this->load->library('form_validation');
        $this->form_validation->set_rules('user_name', NAME, 'required|trim|min_length[3]|max_length[40]');
        $this->form_validation->set_rules('email', EMAIL_ADDRESS, 'required|trim|valid_email');
      
        $this->form_validation->set_rules('password', PASSWORD, 'required|trim|min_length[8]|max_length[20]');
        $user_type_error='';
        if($_POST)
        {
            if(!is_array($this->input->post('user_type')))
            {
                $user_type_error = 'Please Select Atleast One Option';
             
            }
        }

        if ($invite_code != '') {
            $data['user_invite_code'] = $invite_code;
        } else {
            $data['user_invite_code'] = '';
        }
        
         if ($invite_email != '') {
            $data['email'] = base64_decode($invite_email);
        } else {
            $data['email'] = "";
        }
       
        $data['user_name'] = "";
        
        $data['agree'] = "";
        $chk_user = 'false';
        //   Check spamer
        setting_deletecache('spam_protectd');
        $spamer = $this->home_model->spam_protection();
        if ($spamer == 1 || $spamer == '1') {
            $chk_user = 'true';
        }
        $check_user = '';
        if ($_POST) {
            if ($this->username_check(SecurePostData($this->input->post('email'))) == 1) {
                $check_user = EXISTS_ACCOUNT_ASSOCIATE_WITHMAIL . '<br />';
            }
        }

        $captcha_result = '';
        
        require(base_path().'/application/libraries/ReCaptcha/src/autoload.php'); 

        $siteKey = $site_setting['captcha_private_key'];
        $secret = $site_setting['captcha_public_key'];

        
        if ($_POST)
        {
            if ($site_setting['signup_captcha'] == 1) { 
                $recaptcha = new \ReCaptcha\ReCaptcha($secret);
                $session_ip_address = $this->session->userdata('ip_address');
                $resp = $recaptcha->verify($_POST['g-recaptcha-response'], $session_ip_address);
                
                if ($resp->isSuccess())
                {
                    $captcha_result = '';
                }
                else
                {
                    $captcha_result = '<p>' . IMAGE_VERIFICATION_WRONG . '</p>';
                }
         }
        }

        if ($this->form_validation->run($this) == FALSE || $chk_user == 'true' || $check_user != '' || $captcha_result != "" || $user_type_error != '') {
            $data['error'] = "";
            if (validation_errors() || $check_user != '' || $captcha_result != "" || $user_type_error != '') {
                $signup_error_message =  validation_errors() . $check_user . $captcha_result . 
                $user_type_error;

                $this->session->set_flashdata('error_message_signup', $signup_error_message);

            } else {
                
            }
            if ($chk_user == 'true') {
            
                 $this->session->set_flashdata('error_message_signup', IPBAND_CANNOTREG_NOW);
            }
            if ($_POST) {
                $data['user_name'] = SecurePostData($this->input->post('user_name'));
                
                $data['email'] = SecurePostData($this->input->post('email'));
                $data['user_invite_code'] = SecurePostData($this->input->post('user_invite_code'));
            }
        } else {
            //   Check spam register
            $chk_spam_register = $this->home_model->check_spam_register();
            if ($chk_spam_register == 1) {
                $data['user_name'] = SecurePostData($this->input->post('user_name'));
                $data['email'] = SecurePostData($this->input->post('email'));
               
                 $this->session->set_flashdata('error_message_signup', IPBAND_CANNOTREG_NOW);
            } else {
                if($this->input->post('referral')!=null){
                    $sf_referrer = $this->input->post('referral');
                }
                if($this->input->post('syndicate')!=null){
                    $sf_syndicate = $this->input->post('syndicate');
                }

                $invite_code_id = SecurePostData($this->input->post('user_invite_code'));
                $find_refeer_id = $this->findreferid($invite_code_id);
                $name = explode(' ', $this->input->post('user_name'));
                $user_type = json_encode($this->input->post('user_type'));
              
                //first name and last name from full name
                $f_name = isset($name[0]) ? $name[0] : '';
                $l_name = isset($name[1]) ? $name[1] : '';
                //echo $hash = $this->password->create_hash(SecurePostData($this->input->post('password')));
                $user_data = array(
                    'user_name' => SecurePostData($f_name),
                    'last_name' => SecurePostData($l_name),
                    'email' => SecurePostData($this->input->post('email')),
                    'password' => SecurePostData(md5($this->input->post('password'))),
                    'user_type' => $user_type,
                    //'password' => SecurePostData($this->password->create_hash($this->input->post('password'))),
                    'signup_ip' => SecurePostData($_SERVER['REMOTE_ADDR']),
                    'date_added' => SecurePostData(date('Y-m-d H:i:s')),
                    'confirm_key' => randomNumber(20),
                    'unique_code' => unique_user_code(getrandomCode(12)),
                    'reference_user_id' => $find_refeer_id,
                    'sf_referrer'=>$sf_referrer,
                    'sf_syndicate'=>$sf_syndicate,
                );
               

                $sign = $this->home_model->register($user_data);


                //////////end of activities////////////////////////////////
                if ($sign) {
                    // ///////////============new user email===========
                   
                    $site_name = $site_setting['site_name'];
                    $taxonomy_setting = taxonomy_setting();
                    $project_name = $taxonomy_setting['project_name'];
                    $project_owner_name = $taxonomy_setting['project_owner'];
                    $funds_name = $taxonomy_setting['funds'];
                    $funds_gerund = $taxonomy_setting['funds_gerund'];
                    $funds_plural = $taxonomy_setting['funds_plural'];
                    $result = UserData($sign);
                    $email_template = $this->db->query("select * from `email_template` where task='New User Join'");
                    $email_temp = $email_template->row();
                    $email_address_from = $email_temp->from_address;
                    $email_address_reply = $email_temp->reply_address;
                    $email_subject = $email_temp->subject;
                    $email_subject = str_replace('{site_name}', $site_name, $email_subject);
                    $email_message = $email_temp->message;
                    $username = SecureShowData($this->input->post('user_name'));
                    $password = SecureShowData($this->input->post('password'));
                    $email = SecureShowData($this->input->post('email'));
                    $email_to = SecureShowData($this->input->post('email'));
                    $login_link = '<a href="' . site_url('home/confirm_account/' . $result[0]['user_id'] . '/' . $result[0]['confirm_key']) . '">' . site_url('home/confirm_account/' . $result[0]['user_id'] . '/' . $result[0]['confirm_key']) . '</a>';
                    $create_step_link = '<a href="' . site_url('property/add_property') . '">' . site_url('property/add_property') . '</a>';
                    $user_accrediation_link = '<a href="' . site_url('accreditation/personal') . '">' . site_url('accreditation/personal') . '</a>';
                    $email_message = str_replace('{break}', '<br/>', $email_message);
                    $email_message = str_replace('{user_name}', $username, $email_message);
                    $email_message = str_replace('{project_name}', $project_name, $email_message);
                    $email_message = str_replace('{project_owner_name}', $project_owner_name, $email_message);
                    $email_message = str_replace('{funds_name}', $funds_name, $email_message);
                    $email_message = str_replace('{funds_plural}', $funds_plural, $email_message);
                    $email_message = str_replace('{funds_gerund}', $funds_gerund, $email_message);
                    $email_message = str_replace('{site_name}', $site_name, $email_message);
                    $email_message = str_replace('{verify_link}', $login_link, $email_message);
                    $email_message = str_replace('{create_step_link}', $create_step_link, $email_message);
                    $email_message = str_replace('{user_accrediation_link}', $user_accrediation_link, $email_message);
                    $str = $email_message;

                    email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);
                    
                    $this->session->set_flashdata('success_message', SIGN_UP_SUCCESSFULLY_DETAILS_SENT);
                    redirect('');
                } else {
                    redirect('');
                }
            }
        }
        $meta = meta_setting();
        
        $this->template->write('meta_title', SIGN_UP . ' -' . $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/home/signup', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
    }

    /*
	Function name :username_check()
	Parameter :none
	Return : none
	Use : This is a callback function which is use to check this user exist or not.
	*/
    function username_check($email)
    {
        $email1 = array(
            'email' => $email
        );
        $username = $this->home_model->user_validate($email1);
        if ($username != 'FALSE') {
            // $this->form_validation->set_message('username_check', EXISTS_ACCOUNT_ASSOCIATE_WITHMAIL);
            return 1;
        } else {
            return 0;
        }
    }

    /*
	Function name :logout()
	Parameter :none
	Return : none
	Use : This is used to logout here it destroy the session and cookie.
	*/
    function logout($type = '')
    {
        $this->load->helper('cookie');

        //$this->session->sess_destroy();
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('twitter');
        $_SESSION=array();
        $data['logged_out'] = TRUE;
        if ($type == '') {
            redirect("");
        } else if ($type == '0') {
            redirect("home/index/0");
        } else if ($type == '2') {
            redirect("home/index/2");
        }
    }

    /*
	Function name :confirm_account()
	Parameter :id is userid and key is confirm key
	Return : none
	Use : This is used to match the confirm key and active the user.
	*/
    function confirm_account($id = null, $key = '', $admin = '')
    {
        $ud = array(
            'user_id' => SecurePostData($id)
        );
        $cuser = $this->home_model->user_validate($ud);
        //print_r($cuser);
        if ($cuser) {

            if (intval($cuser['active']) == 1 && $admin != '') {
                $msg = 'update';
                redirect('admin/user/list_user/' . $msg);
            }

            if (intval($cuser['active']) == 1) {
                $msg = 'cant_click';
                redirect('home/index/' . $msg);
            }

            $msg = '';
            if ($cuser['confirm_key'] == $key) {
                $ud = array(
                    'active' => 1,
                    'confirm_key' => '1',
                );


                $cuser = $this->account_model->UpdateAccount('user_id', $id, 'user', $ud);
                user_activity('signup', $id, 0, 0);


                $result = UserData($id);

                // salesforce api call for new user 
                $this->load->library("salesforce_api");

                $temp_usertype = json_decode($result[0]['user_type']);

                $isInvestor=$isREAgent=$isOwner=$isDeveloper=false;

                if(in_array(1, $temp_usertype)){
                    $isInvestor=true;
                }
                if(in_array(2, $temp_usertype)){
                    $isREAgent = true;
                }
                if(in_array(3, $temp_usertype)){
                    $isOwner = true;
                }
                if(in_array(4, $temp_usertype)){
                    $isDeveloper = true;
                }

                $residentialInterest=$commercialInterest=$industrialInterest=$ruralInterest=false;


                if(in_array(1, $temp_usertype) && count($temp_usertype)==1){
                    $mobile_number = "976543210";
                    $zip_code = "32525";
                    $address = "test";
                    if($result[0]['mobile_number']!=''){
                        $mobile_number = $result[0]['mobile_number'];
                    }
                    if($result[0]['zip_code']!=''){
                        $zip_code = $result[0]['zip_code'];
                    }
                    if($result[0]['address']!=''){
                        $address = $result[0]['address'];
                    }

                    $require_data_array = array(
                        "covestaUserId" => $result[0]['user_id'],
                        "firstName" => $result[0]['user_name'],
                        "lastName" => $result[0]['last_name'],
                        "email" => $result[0]['email'],
                        "mobile" => $mobile_number,
                        "postcode" => $zip_code,
                        "address" => $address,
                        "isDeveloper" => $isDeveloper,
                        "isREAgent" => $isREAgent,
                        "isInvestor" => $isInvestor,
                        "isOwner" => $isOwner,
                        "residentialInterest" => $residentialInterest,
                        "commercialInterest" => $commercialInterest,
                        "industrialInterest" => $industrialInterest,
                        "ruralInterest" => $ruralInterest
                    );
                } else {
                    $company_name = "test";
                    $abn_number = "test";
                    $company_website = "http://www.testweb.com";
                    $company_address = "test";
                    $mobile_number = "976543210";
                    $zip_code = "32525";
                    $address = "test";

                    if($result[0]['company_name']!=''){
                        $company_name = $result[0]['company_name'];
                    }
                    if($result[0]['abn_number']!=''){
                        $abn_number = $result[0]['abn_number'];
                    }
                    if($result[0]['company_website']!=''){
                        $company_website = $result[0]['company_website'];
                    }
                    if($result[0]['company_address']!=''){
                        $company_address = $result[0]['company_address'];
                    }
                    if($result[0]['mobile_number']!=''){
                        $mobile_number = $result[0]['mobile_number'];
                    }
                    if($result[0]['zip_code']!=''){
                        $zip_code = $result[0]['zip_code'];
                    }
                    if($result[0]['address']!=''){
                        $address = $result[0]['address'];
                    }

                    // $temp_user_interest = json_decode($result[0]->user_interest);
                    
                    $require_data_array = array(
                        "covestaUserId" => $result[0]['user_id'],
                        "companyName" => $company_name,
                        "ABN" => $abn_number,
                        "companyWebsite" => $company_website,
                        "companyAddress" => $company_address,
                        "firstName" => $result[0]['user_name'],
                        "lastName" => $result[0]['last_name'],
                        "email" => $result[0]['email'],
                        "mobile" => $mobile_number,
                        "postcode" => $zip_code,
                        "address" => $address,
                        "isDeveloper" => $isDeveloper,
                        "isREAgent" => $isREAgent,
                        "isInvestor" => $isInvestor,
                        "isOwner" => $isOwner,
                        "residentialInterest" => $residentialInterest,
                        "commercialInterest" => $commercialInterest,
                        "industrialInterest" => $industrialInterest,
                        "ruralInterest" => $ruralInterest
                    );
                }
                

                $salesforceUserId = $this->salesforce_api->create_new_user($require_data_array);
                $updatearray = array("salesforceuserid"=>$salesforceUserId);



                $this->db->update('user', $updatearray,array('user_id'=>$result[0]['user_id']));

                // print_r($updatearray);
                // exit;

                // salesforce api call end

                $check_team_member_array = array("email" => $result[0]['email']);
                $team_member_signup = $this->property_model->getTableData('invite_members', $check_team_member_array);
                $team_member_data = $team_member_signup[0];
                if (is_array($team_member_data)) {
                    if ($result[0]['email'] == $team_member_data['email']) {
                        project_activity_feedback('team_member_added', $id, '', $team_member_data['equity_id']);
                    }
                }

                $crowd_invite_request_data = $this->property_model->getTableData('crowd_invite_request', $check_team_member_array);
                $crowd_invite_request = $crowd_invite_request_data[0];
                if (is_array($crowd_invite_request)) {
                        
                    $crowd_request_array = array('code'=>1,'user_id'=>$id,'status'=>1);
                    $this->property_model->AddInsertUpdateTable('crowd_invite_request','email',$result[0]['email'],$crowd_request_array);

                }

                $site_setting = site_setting();
                $site_name = $site_setting['site_name'];
                $email_template = $this->db->query("select * from `email_template` where task='Welcome Email'");
                $email_temp = $email_template->row();
                $email_address_from = $email_temp->from_address;
                $email_address_reply = $email_temp->reply_address;
                $email_subject = $email_temp->subject;
                $email_message = $email_temp->message;
                $username = $result[0]['user_name'];
                $email = $result[0]['email'];
                $email_to = $email;
                $email_message = str_replace('{user_name}', $username, $email_message);
                $email_message = str_replace('{email}', $email, $email_message);
                $email_message = str_replace('{site_name}', $site_name, $email_message);
                $str = $email_message;

                email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);
                // ///////////============welcome email===========

                $msg = 'success';
            } elseif ($cuser['confirm_key'] == '1' and $cuser['active'] == '1') {
                $msg = 'active';
            } else {
                $msg = 'error';
            }
        } else {
            $msg = 'error';
        }

        if ($admin != '') {
            $msg = 'update';
            redirect('admin/user/list_user/' . $msg);
        }
        redirect('home/index/' . $msg);
    }

    /*
	Function name :resend()
	Parameter :none
	Return : none
	Use : This is used when the user login in without user activate then it send a link you confirm account first.
	*/
    function resend()
    {
        $meta = meta_setting();
        $site_setting = site_setting();
        $data['site_setting'] = $site_setting;
        $data['email'] = '';
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', EMAIL_ADDRESS, 'required|valid_email|trim');
        $chk_user = 'false';
        //   Check spamer
        $spamer = $this->home_model->spam_protection();
        if ($spamer == 1 || $spamer == '1') {
            $chk_user = 'true';
        }
        //   Check validations
        if ($this->form_validation->run() == FALSE || $chk_user == 'true') {
           
            if (validation_errors()) {
                
                 $this->session->set_flashdata('error_message_resend', validation_errors());
            } else {
                
            }
            if ($chk_user == 'true') {
               
                $this->session->set_flashdata('error_message_resend', IPBAND_CANNOTREG_NOW);
            }
        } else {
            $em = array(
                'email' => SecurePostData($this->input->post('email'))
            );
            $check_email = $this->home_model->user_validate($em);
            // print_r($check_email);
            if ($check_email) {
                if ($check_email['active'] == 0) {
                    $confirm_key = randomNumber(20);
                    $fdata = array(
                        'confirm_key' => $confirm_key
                    );
                    $set = $this->account_model->UpdateAccount('user_id', $check_email['user_id'], 'user', $fdata);

                    // ///////////============new user email===========
                    if ($set != '') {

                        $result = UserData($check_email['user_id']);

                        $email_template = $this->db->query("select * from `email_template` where task='Email Verification'");
                        $email_temp = $email_template->row();
                        $site_name = $site_setting['site_name'];
                        $email_address_from = $email_temp->from_address;
                        $email_address_reply = $email_temp->reply_address;
                        $email_subject = $email_temp->subject;
                        $email_message = $email_temp->message;
                        $username = SecureShowData($result[0]['user_name']);
                        $password = SecureShowData($result[0]['password']);
                        $email = SecureShowData($result[0]['email']);
                        $email_to = SecureShowData($result[0]['email']);
                        $taxonomy_setting = taxonomy_setting();
                        $project = $taxonomy_setting['project_name'];
                        $projects = $taxonomy_setting['project_name_plural'];

                        $funds = $taxonomy_setting['funds'];
                        $projectControllerName=projectcontrollername();
                        $project_explor_link = '<a href="' . site_url('category/search') . '">' . $projects . '</a>';
                        $create_step_link = '<a href="' . site_url('start_'.$projectControllerName.'/create_prestep') . '">' . CREATE . '</a>';

                        $login_link = '<a href="' . site_url('home/confirm_account/' . $result[0]['user_id'] . '/' . $result[0]['confirm_key']) . '">Click Here</a>';
                        $email_message = str_replace('{break}', '<br/>', $email_message);
                        $email_message = str_replace('{user_name}', $username, $email_message);
                        $email_message = str_replace('{password}', $password, $email_message);
                        $email_message = str_replace('{site_name}', $site_name, $email_message);

                        $email_message = str_replace('{email}', $email, $email_message);
                        $email_message = str_replace('{project_name}', $project, $email_message);
                        $email_message = str_replace('{donation_name}', $funds, $email_message);
                        $email_message = str_replace('{login_link}', $login_link, $email_message);
                        $email_message = str_replace('{create_step_link}', $create_step_link, $email_message);
                        $email_message = str_replace('{project_name_explor}', $project_explor_link, $email_message);
                        $email_subject = str_replace('{site_name}', $site_name, $email_subject);
                        $str = $email_message;
                        email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);
                         $this->session->set_flashdata('success_message', EMAIL_VERIFICATION_LINK_RESEND);
                        redirect('');
                    } else {
                        
                         $this->session->set_flashdata('error_message_resend', RESEND_VERIFICATION_FAILED);

                    }
                    // ///////////============new user email===========
                } elseif ($check_email['active'] == 2) {
                  
                     $this->session->set_flashdata('error_message_resend', YOUR_EMAIL_ADDRESS_IS_SUSPENDED);
                } else {
                   
                     $this->session->set_flashdata('error_message_resend', YOUR_ACCOUNT_ALREADY_ACTIVE);
                }
            } else {
               
                 $this->session->set_flashdata('error_message_resend', EMAIL_ADDRESS_NOT_FOUND);
            }
        }
        $this->template->write('meta_title', RESEND_VARIFICATION_LINK . ' -' . $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/home/resend', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
    }

    /*
	Function name :facebook()
	Parameter : unique_code
	Return : register or update user with facebook data
	Use : register or update user with facebook data
	*/
    function facebook($unique_code = '')
    {

        if ($unique_code == 'facebook_admin') {
            redirect('admin/social_setting/add_facebook_setting/true');
        }
        $spamer = $this->home_model->spam_protection();
        if ($spamer == 1 || $spamer == '1') {
            redirect('home/signup');
        }
        
        if (!$this->fb_connect->fbSession) {
            // 2. If No, bounce back to login
            redirect('home/login');
        }
        var_dump($this->fb_connect->fbSession);;
        $fb_uid = $this->fb_connect->fbSession;
        $fb_usr = $this->fb_connect->user;
        $fb_fbaccesstoken = $this->fb_connect->fbaccesstoken;

        //Validate user if user status is Active / Inactive / Suspend
        $this->_facebook_validate($fb_uid, $fb_usr["email"]);
        //----------------

        if ($fb_uid != '') {
            $this->session->set_userdata(array(
                "facebook_id" => $fb_uid,
                "fb_access_token" => $fb_fbaccesstoken
            ));
        }

        if (isset($fb_usr["email"])) $email = $fb_usr["email"];
        else $email = '';
        if (isset($fb_usr["first_name"])) $fname = $fb_usr["first_name"];
        else $fname = '';
        if (isset($fb_usr["last_name"])) $lname = $fb_usr["last_name"];
        else $lname = '';
        $pwd = ''; //left blank so user can modify this later
        $redir = 'home';
        if ($unique_code == 'backinvite') {
            $redir = 'invites/facebook';
        }
        if ($unique_code == "social") {
            $redir = "social_networking/index/";
        }
        $usr = $this->home_model->check_social_user(array(
            "fb_uid" => $fb_uid,
            "email" => $email
        ));
        if ($usr) {
            $redir = 'home/index/login';
            if ($this->session->userdata('user_id') > 0) {

                $data = array(
                    'fb_uid' => "" . $fb_uid,
                    'fb_access_token' => "" . $fb_fbaccesstoken,
                );
                if ($unique_code == "social") {
                    $data['facebook_wall_post'] = 1;
                }
                $wheredata = array(
                    'user_id' => $this->session->userdata('user_id')
                );
                $bQry = $this->home_model->update_social_user($wheredata, $data);
                redirect($redir);
            } else {
                $data_update = array(
                    'fb_uid' => "" . $fb_uid,
                    'fb_access_token' => "" . $fb_fbaccesstoken,
                    'email' => "" . $email,
                );
                if ($unique_code == "social") {
                    $data_update['facebook_wall_post'] = 1;
                }
                $bQry = $this->home_model->update_social_user(array(
                    "fb_uid" => $fb_uid,
                    "email" => $email
                ), $data_update);
                redirect($redir);
            }
        } else {

            try {

                $CI = &get_instance();
                $fb_img = '';
                $base_path = $CI->config->slash_item('base_path');
                $image_settings = get_image_setting_data();
                if (isset($fb_usr["picture"])) {
                    $inPath = $fb_usr["picture"];
                    $outPath = $base_path . 'upload/orig/' . $fb_uid . '.jpg';
                    $in = fopen($inPath, "rb");
                    $out = fopen($outPath, "wb");
                    while ($chunk = fread($in, 8192)) {
                        fwrite($out, $chunk, 8192);
                    }
                    fclose($in);
                    fclose($out);
                    $fb_img = $fb_uid . '.jpg';
                    $files["userfile"]["name"] = $fb_img;
                    $files["userfile"]["tmp_name"] = '';
                    UserImageUpload($files, true);
                }
                $db_values = array(
                    'fb_uid' => "" . $fb_uid,
                    'fb_access_token' => "" . $fb_fbaccesstoken,
                    'user_name' => strtolower(str_replace(" ", "", $fname)),
                    'last_name' => strtolower(str_replace(" ", "", $lname)),
                    'password' => $pwd,
                    'email' => $email,
                    'image' => $fb_img,
                    'active' => 1,
                    'unique_code' => $unique_code,
                    'date_added' => date('Y-m-d H:i:s'),
                );

            } catch (Exception $ex) {

                throw new Exception('Error occurred in PayReceipt method');
                $error = WRONG_FACEDBOOK_SETTINGS;
                $data['error'] = $error;
                $this->load->view('default/paypal_error', $data);
            }
            $this->social_signup($db_values, 'fb_uid');
        }

    }

    /*
	Function name :_facebook_validate()
	Parameter : uid , email
	Return : redirect or return 1
	Use : check social signup user exists or not
	*/
    function _facebook_validate($uid = 0, $email = '')

    {

        //this query basically sees if the users facebook user id is associated with a user.

        $chk = $this->db->query("SELECT * FROM `user` WHERE email='" . $email . "'");

        if ($chk->num_rows() > 0) {
            $chk = $chk->result();
            //echo '<pre>'; print_r($chk); die;
            $bQry = $chk[0]->active;
            //echo $bQry; die;
            $meta = meta_setting();
            if ($bQry == '2') {
                redirect('home/login/suspend');
            } elseif ($bQry == 0) { // if the user's credentials validated...		
                redirect('home/login/inactive');
            } elseif ($bQry) { // if the user's credentials validated...	
                return 1;
            } else {
                return 1;
            }
        } else {
            return 1;
        }

    }
 /*
    Function name :_facebook_validate()
    Parameter : uid , email
    Return : redirect or return 1
    Use : check social signup user exists or not
    */
    function _twitter_validate($uid = 0, $email = '')

    {

        //this query basically sees if the users facebook user id is associated with a user.

        $chk = $this->db->query("SELECT * FROM `user` WHERE tw_id='" . $uid . "'");

        if ($chk->num_rows() > 0) {
            $chk = $chk->result();
            //echo '<pre>'; print_r($chk); die;
            $bQry = $chk[0]->active;
            //echo $bQry; die;
            $meta = meta_setting();
            if ($bQry == '2') {
                redirect('home/login/suspend');
            } elseif ($bQry == 0) { // if the user's credentials validated...       
                redirect('home/login/inactive');
            } elseif ($bQry) { // if the user's credentials validated...    
                return 1;
            } else {
                return 1;
            }
        } else {
            return 1;
        }

    }
    /*
	Function name :twitter_auth()
	Parameter : unique_code
	Return : register or update user with twitter data
	Use : register or update user with twitter data
	*/
    function twitter_authss($unique_code = '')
    {
        $spamer = $this->home_model->spam_protection();
        if ($spamer == 1 || $spamer == '1') {
            redirect('home/signup');
        }
        $CII = &get_instance();
        $base_path = $CII->config->slash_item('base_path');

        $includes_url = $base_path . 'twitteroauth/twitteroauth.php';
        $this->load->file($includes_url);
        $twitter_setting = twitter_setting();
        define('CONSUMER_KEY', $twitter_setting['consumer_key']);
        define('CONSUMER_SECRET', $twitter_setting['consumer_secret']);
        define('OAUTH_CALLBACK', site_url('home/auth/' . $unique_code));
        /* Build TwitterOAuth object with client credentials. */

        $connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET);
        /* Get temporary credentials. */
        $request_token = $connection->getRequestToken(OAUTH_CALLBACK);
        /* Save temporary credentials to session. */
        $_SESSION['oauth_token'] = $token = $request_token['oauth_token'];
        $_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];
        /* If last connection failed don't display authorization link. */
        switch ($connection->http_code) {
            case 200:
                /* Build authorize URL and redirect user to Twitter. */
                $url = $connection->getAuthorizeURL($token);
                header('Location: ' . $url);
                break;

            default:
                /* Show notification if something went wrong. */
                echo TWITTER_CONNECTION_ERROR;
        }
    }

    /*
	Function name :twitter_auth()
	Parameter : unique_code
	Return : register or update user with twitter data
	Use : register or update user with twitter data
	*/
    function twitter_auth($unique_code = '')
    {
        // error_reporting(E_ALL ^ E_NOTICE);
        $spamer = $this->home_model->spam_protection();
        if ($spamer == 1 || $spamer == '1') {
            redirect('home/signup');
        }
        $CII = &get_instance();
        $base_path = $CII->config->slash_item('base_path');
        // include_once $base_path.'../lib/GoCardless.php';
        $includes_url = $base_path . 'twitteroauth/twitteroauth.php';
        $this->load->file($includes_url);
        $twitter_setting = twitter_setting();
        define('CONSUMER_KEY', $twitter_setting['consumer_key']);
        define('CONSUMER_SECRET', $twitter_setting['consumer_secret']);
        define('OAUTH_CALLBACK', site_url('home/auth/' . $unique_code));
        /* Build TwitterOAuth object with client credentials. */

        $connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET);
        /* Get temporary credentials. */
        $request_token = $connection->getRequestToken(OAUTH_CALLBACK);


        /* Save temporary credentials to session. */
        $_SESSION['oauth_token'] = $token = $request_token['oauth_token'];
        $_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];
        /* If last connection failed don't display authorization link. */
        switch ($connection->http_code) {
            case 200:
                /* Build authorize URL and redirect user to Twitter. */
                $url = $connection->getAuthorizeURL($token);
                header('Location: ' . $url);
                break;

            default:
                /* Show notification if something went wrong. */
                echo TWITTER_CONNECTION_ERROR;
        }
    }

    /*
	Function name :auth()
	Parameter : unique_code
	*/
    function auth($unique_code = '')
    {
        if (isset($_GET['denied'])) {
            redirect('home');
        }
        try {
            $redir = 'home';
            if ($unique_code == "social") $redir = "social_networking/index/";
            $CII = &get_instance();
            $base_path = $CII->config->slash_item('base_path');
            // include_once $base_path.'../lib/GoCardless.php';
            $includes_url = $base_path . 'twitteroauth/twitteroauth.php';
            $this->load->file($includes_url);
            $twitter_setting = twitter_setting();
            define('CONSUMER_KEY', $twitter_setting['consumer_key']);
            define('CONSUMER_SECRET', $twitter_setting['consumer_secret']);
            define('OAUTH_CALLBACK', site_url('home/auth/' . $unique_code));
            /* Create TwitteroAuth object with app key/secret and token key/secret from default phase */
            $connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
            /* Request access tokens from twitter */
            $access_token = $connection->getAccessToken($_GET['oauth_verifier']);
            /* Save the access tokens. Normally these would be saved in a database for future use. */
            $_SESSION['access_token'] = $access_token;
            /* Remove no longer needed request tokens */
            unset($_SESSION['oauth_token']);
            unset($_SESSION['oauth_token_secret']);
            $access_token = $_SESSION['access_token'];
            /* Create a TwitterOauth object with consumer/user tokens. */
            $connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);
            if (isset($access_token['oauth_token'])) {
                $oauth_token = $access_token['oauth_token'];
            }
            if (isset($access_token['oauth_token_secret'])) {
                $oauth_token_secret = $access_token['oauth_token_secret'];
            }
            /* If method is set change API call made. Test is called by default. */
            $content = $connection->get('account/verify_credentials');
            $user = $content;

            if ($unique_code == 'twitter_admin') {
                redirect('admin/social_setting/add_twitter_setting/true');
            } else {
                if (isset($content->errors)) {
                    $obj = $content->errors[0];
                    if ($obj->message) {
                        $error = $obj->message;
                        $data['error'] = $error;
                        $this->load->view('default/paypal_error', $data);
                    }
                } elseif (isset($user->screen_name)) {
                    $screen_name = $user->screen_name;
                    $twiter_img_url = $user->profile_image_url_https;
                    if (isset($user->name)) {
                        $first_name = $user->name;
                        if (substr_count($first_name, ' ') >= 1) {
                            $ex = explode(' ', $first_name);
                            $first_name = $ex[0];
                            if (!isset($ex[1])) {
                                $last_name = '';
                            } else {
                                $last_name = $ex[1];
                            }
                        } else {
                            $last_name = '';
                        }
                    }
                    $name = $user->name;
                    $twitter_id = $user->id;

                    $usr = $this->home_model->check_social_user(array(
                        "tw_id" => $twitter_id
                    ));
                    $return_email = $this->home_model->get_user_info($twitter_id);

                    if ($return_email != '0' && $return_email != '') {
                        $this->_twitter_validate($twitter_id, $return_email);
                    }
//echo $usr; die;

                    if ($usr) { 
                         $redir = 'home/index/login';
                        $data = array(
                            'tw_id' => $twitter_id,
                            'tw_screen_name' => $screen_name,
                            'tw_oauth_token' => $oauth_token,
                            'tw_oauth_token_secret' => $oauth_token_secret
                        );
                        $this->session->set_userdata('twitter', $data);
                        $this->db->delete('user', array('user_id' => $this->session->userdata('user_id')));

                        if ($unique_code == "social") {
                            $data['autopost_site'] = 1;
                        }

                        if ($this->session->userdata('user_id') > 0) {
                            $wheredata = array(
                                "tw_id" => $twitter_id,
                                "email" => $return_email
                            );
                            $bQry = $this->home_model->update_social_user($wheredata, $data);
                            // ====put condition for invite code related if user is alreadt logged in
                        } else {
                            $bQry = $this->home_model->update_social_user(array(
                                "tw_id" => $twitter_id,
                                "email" => $return_email,
                            ), $data);
                        }

                        redirect($redir);
                    } else {
                        if ($this->session->userdata('user_id') > 0) {
                            $data = array(
                                'tw_id' => $twitter_id,
                                'tw_screen_name' => $screen_name,
                                'tw_oauth_token' => $oauth_token,
                                'tw_oauth_token_secret' => $oauth_token_secret
                            );
                            $this->session->set_userdata('twitter', $data);
                            $this->db->delete('user', array('user_id' => $this->session->userdata('user_id')));

                            if ($unique_code == "social") {
                                $data['autopost_site'] = 1;
                            }
                            $wheredata = array(
                                'user_id' => $this->session->userdata('user_id')
                            );
                            $bQry = $this->home_model->update_social_user($wheredata, $data);
                            // ====put condition for invite code related if user is alreadt logged in
                            redirect($redir);
                        } else {
                            $twiter_img = '';
                            if (isset($twiter_img_url)) {
                                $CI = &get_instance();
                                $base_path = $CI->config->slash_item('base_path');
                                $image_settings = get_image_setting_data();
                                $inPath = $twiter_img_url;
                                $outPath = $base_path . 'upload/orig/' . $screen_name . '.jpg';
                                $in = fopen($inPath, "rb");
                                $out = fopen($outPath, "wb");
                                while ($chunk = fread($in, 8192)) {
                                    fwrite($out, $chunk, 8192);
                                }
                                fclose($in);
                                fclose($out);
                                $twiter_img = $screen_name . '.jpg';
                                $files["userfile"]["name"] = $twiter_img;
                                $files["userfile"]["tmp_name"] = '';
                                UserImageUpload($files, true);
                            }
                            $db_values = array(
                                'user_name' => strtolower(str_replace(" ", "", $first_name)),
                                'last_name' => $last_name,
                                'email' => '',
                                'tw_id' => $twitter_id,
                                'image' => $twiter_img,
                                'tw_screen_name' => $screen_name,
                                'tw_oauth_token' => $oauth_token,
                                'tw_oauth_token_secret' => $oauth_token_secret,
                                'active' => 1,
                                'unique_code' => $unique_code,
                                'date_added' => date('Y-m-d H:i:s'),
                            );

                            $data = array(
                                'tw_id' => $twitter_id,
                                'tw_screen_name' => $screen_name,
                                'tw_oauth_token' => $oauth_token,
                                'tw_oauth_token_secret' => $oauth_token_secret
                            );
                            $this->session->set_userdata('twitter', $data);

                            $this->social_signup($db_values, 'tw_id');
                        }
                    }
                }
            }
        } catch (Exception $ex) {
            throw new Exception('Error occurred in PayReceipt method');
            $error = WRONG_TWITTER_SETTINGS;
            $data['error'] = $error;
            $this->load->view('default/paypal_error', $data);
        }
    }

    /*
	Function name :social_signup()
	Parameter : data and social_site_name it comes from social site id
	Return : register or update user with social networking
	Use : register or update user with social networking data
	*/
    function social_signup($data = array(), $social_site_name)
    {
        $data['signup_ip'] = SecurePostData($_SERVER['REMOTE_ADDR']);
        $data['date_added'] = SecurePostData(date('Y-m-d H:i:s'));
        $data['unique_code'] = unique_user_code(getrandomCode(12));
        $find_refeer_id = $this->findreferid();
        $data['reference_user_id'] = $find_refeer_id;
        if ($data['email'] != "") {
            $email = $data['email'];
            $query = $this->db->query("select user_id from user where email='$email'");
            if ($query->num_rows() > 0) {
                $res = $query->row_array();

                //print_r($res);die;
                $sign = $res['user_id'];
                $this->db->where('user_id', $sign);
                $this->db->update('user', $data);
            } else {
                $sign = $this->home_model->register($data);

            }
        } else {
            $sign = $this->home_model->register($data);

        }

        if ($sign) {
            $user_detail = $this->account_model->GetAllUsers($sign);
            $user_detail = $user_detail[0];
            if ($social_site_name == 'fb_uid') {
                $login_data = array(
                    'fb_uid' => SecurePostData($user_detail['fb_uid'])
                );
            }
            if ($social_site_name == 'tw_id') {
                $login_data = array(
                    'tw_id' => SecurePostData($user_detail['tw_id'])
                );
            }
            if ($social_site_name == 'linkdin_id') {
                $login_data = array(
                    'linkdin_id' => SecurePostData($user_detail['linkdin_id'])
                );
            }
            $chk = $this->home_model->user_validate($login_data, 'login');
            $set_session_data = array(
                'user_id' => $user_detail['user_id'],
                'user_name' => $user_detail['user_name'],
                'email' => $user_detail['email'],
            );
            $this->session->set_userdata($set_session_data);

            $email_template = $this->db->query("select * from `email_template` where task='Welcome Email'");
            $email_temp = $email_template->row();
            $email_address_from = $email_temp->from_address;
            $email_address_reply = $email_temp->reply_address;
            $email_subject = $email_temp->subject;
            $email_message = $email_temp->message;
            $username = $user_detail['user_name'];
            $email = $user_detail['email'];
            $password = $user_detail['password'];
            $email_to = $user_detail['email'];
            $email_message = str_replace('{break}', '<br/>', $email_message);
            $email_message = str_replace('{user_name}', $username, $email_message);
            $email_message = str_replace('{email}', $email, $email_message);
            $str = $email_message;
            email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);
            // ///////////============welcome email===========
            // ///////////============new user email===========
            $email_template = $this->db->query("select * from `email_template` where task='Social User Join'");
            $email_temp = $email_template->row();
            $email_address_from = $email_temp->from_address;
            $email_address_reply = $email_temp->reply_address;
            $email_subject = $email_temp->subject;
            $email_message = $email_temp->message;
            $login_link = '<a href="' . site_url('home/login') . '">' . site_url('home/login') . '</a>';
            $email_message = str_replace('{break}', '<br/>', $email_message);
            $email_message = str_replace('{user_name}', $username, $email_message);
            $email_message = str_replace('{email}', $email, $email_message);
            $email_message = str_replace('{login_link}', $login_link, $email_message);
            $str = $email_message;
            email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);
            // ///////////============new user email===========
            redirect('account/first_step');
        } else {
            redirect('');
        }
    }

    /*
	Function name :linkdin_getAuthorizationCode()
	Parameter : none
	Use: generate authorize URL for linkdin.
	*/
    function linkdin_getAuthorizationCode()
    {
        $params = array(
            'response_type' => 'code',
            'client_id' => API_KEY,
            'scope' => SCOPE,
            'state' => uniqid('', true), // unique long string
            'redirect_uri' => REDIRECT_URI,
        );
        // Authentication request
        $url = 'https://www.linkedin.com/uas/oauth2/authorization?' . http_build_query($params);
        // Needed to identify request when it returns to us
        $_SESSION['state'] = $params['state'];
        // Redirect user to authenticate
        header("Location: $url");
        exit;
    }

    /*
	Function name :linkdin_getAccessToken()
	Parameter : none
	Use: generate Access Token.
	*/
    function linkdin_getAccessToken()
    {
        $params = array(
            'grant_type' => 'authorization_code',
            'client_id' => API_KEY,
            'client_secret' => API_SECRET,
            'code' => $_GET['code'],
            'redirect_uri' => REDIRECT_URI,
        );
        // Access Token request
        $url = 'https://www.linkedin.com/uas/oauth2/accessToken?' . http_build_query($params);
        // Tell streams to make a POST request
        $context = stream_context_create(array(
            'http' => array(
                'method' => 'POST',
            )
        ));
        // Retrieve access token information
        $response = file_get_contents($url, false, $context);
        // Native PHP object, please
        $token = json_decode($response);
        // Store access token and expiration time
        $_SESSION['access_token'] = $token->access_token; // guard this!
        $_SESSION['expires_in'] = $token->expires_in; // relative time (in seconds)
        $_SESSION['expires_at'] = time() + $_SESSION['expires_in']; // absolute time
        return true;
    }

    /*
	Function name :linkdin_fetch()
	Parameter : none
	Use: Fetch data from linkdin according to accesstoken
	*/
    function linkdin_fetch($method, $resource, $body = '')
    {
        $params = array(
            'oauth2_access_token' => $_SESSION['access_token'],
            'format' => 'json',
        );
        // Need to use HTTPS
        $url = 'https://api.linkedin.com' . $resource . '?' . http_build_query($params);
        // Tell streams to make a (GET, POST, PUT, or DELETE) request
        $context = stream_context_create(array(
            'http' => array(
                'method' => $method,
            )
        ));
        // Hocus Pocus
        $response = file_get_contents($url, false, $context);
        // Native PHP object, please
        return json_decode($response);
    }

    /*
	Function name :linkdin_auth()
	Parameter : none
	Use: Authorize user for linkdin data.
	*/
    function linkdin_auth($unique_code = '')
    {
        $spamer = $this->home_model->spam_protection();
        if ($spamer == 1 || $spamer == '1') {
            redirect('home/signup');
        }
        $linkdin_setting = linkdin_setting();
        // $l_query = $this->db->getwhere('linkdin_setting');
        // $l_setting = $l_query->row_array();
        $linkedin_access = $linkdin_setting['linkedin_access'];
        $linkedin_secret = $linkdin_setting['linkedin_secret'];
        if (!defined("API_KEY")) define('API_KEY', $linkedin_access);
        if (!defined("API_SECRET")) define('API_SECRET', $linkedin_secret);
        if (!defined("REDIRECT_URI")) define('REDIRECT_URI', site_url('home/linkdin_callback'));
        if (!defined("SCOPE")) define('SCOPE', 'r_basicprofile r_emailaddress');
        session_name('linkedin');
        // session_start();
        // OAuth 2 Control Flow
        if (isset($_GET['error'])) {
            // LinkedIn returned an error
            print $_GET['error'] . ': ' . $_GET['error_description'];
            exit;
        } elseif (isset($_GET['code'])) {
            // User authorized your application
            if ($_SESSION['state'] == $_GET['state']) {
                // Get token so you can make API calls
                $this->linkdin_getAccessToken();
            } else {
                // CSRF attack? Or did you mix up your states?
                exit;
            }
        } else {
            if ((empty($_SESSION['expires_at'])) || (time() > $_SESSION['expires_at'])) {
                // Token has expired, clear the state
                $lang=$_SESSION['lang_code'] ;
                $_SESSION = array();
                $_SESSION['lang_code'] = $lang;
            }
            if (empty($_SESSION['access_token'])) {

                // Start authorization process
                $this->linkdin_getAuthorizationCode();
            }
        }
        redirect('home/linkdin_callback');
    }

    /*
	Function name :linkdin_callback()
	Parameter : none
	Use: user redirect back to site after completion of linkdin authorization processs.
	*/
    function linkdin_callback($unique_code = '')
    {


        $linkdin_setting = linkdin_setting();

        $linkedin_access = $linkdin_setting['linkedin_access'];

        $linkedin_secret = $linkdin_setting['linkedin_secret'];

        if (isset($_GET['error']) == 'access_denied') {
            redirect('home');
        }
        try {

            if (!defined("API_KEY")) define('API_KEY', $linkedin_access);

            if (!defined("API_SECRET")) define('API_SECRET', $linkedin_secret);

            if (!defined("REDIRECT_URI")) define('REDIRECT_URI', site_url('home/linkdin_callback'));

            if (!defined("SCOPE")) define('SCOPE', 'r_fullprofile r_emailaddress rw_nus');


            session_name('linkedin');

            ///session_start();

            // OAuth 2 Control Flow

            if (isset($_GET['error'])) {

                // LinkedIn returned an error

                print $_GET['error'] . ': ' . $_GET['error_description'];

                exit;

            } elseif (isset($_GET['code'])) {

                // User authorized your application

                if ($_SESSION['state'] == $_GET['state']) {

                    // Get token so you can make API calls

                    $this->linkdin_getAccessToken();

                } else {

                    // CSRF attack? Or did you mix up your states?

                    exit;

                }

            } else {

                if ((empty($_SESSION['expires_at'])) || (time() > $_SESSION['expires_at'])) {

                    // Token has expired, clear the state

                $lang=$_SESSION['lang_code'] ;
                $_SESSION = array();
                $_SESSION['lang_code'] = $lang;

                }

                if (empty($_SESSION['access_token'])) {

                    // Start authorization process

                    $this->linkdin_getAuthorizationCode();

                }

            }

            $user = $this->linkdin_fetch('GET', '/v1/people/~:(id,firstName,lastName,email-address,picture-url)');


            if ($user->id == '') {

                //2. If No, bounce back to login   			

                redirect('home/login');


            } else {


                $this->_facebook_validate($user->id, $user->emailAddress);
                $linkdin_id = $user->id;

                $firstname = $user->firstName;
                $emailAddress = $user->emailAddress;
                $pictureUrl = $user->pictureUrl;

                $lastName = $user->lastName;
                $tw_img = '';

                if ($pictureUrl != '') {

                    $inPath = $pictureUrl;
                    $base_path = $this->config->slash_item('base_path');
                    $outPath = $base_path . 'upload/orig/' . $linkdin_id . '.jpg';

                    $in = fopen($inPath, "rb");

                    $out = fopen($outPath, "wb");

                    while ($chunk = fread($in, 8192)) {

                        fwrite($out, $chunk, 8192);

                    }

                    fclose($in);

                    fclose($out);

                    $tw_img = $linkdin_id . '.jpg';

                    $files["userfile"]["name"] = $tw_img;

                    $files["userfile"]["tmp_name"] = '';

                    UserImageUpload($files, true);

                }

                $usr = $this->home_model->check_social_user(array("linkdin_id" => $linkdin_id));

                if ($usr) {

                    if ($this->session->userdata('user_id') > 0) {

                        $data = array(

                            'linkdin_id' => $linkdin_id

                        );

                        $wheredata = array(

                            'user_id' => $this->session->userdata('user_id')

                        );

                        $bQry = $this->home_model->update_social_user($wheredata, $data);

                        //====put condition for invite code related if user is alreadt logged in					

                    } else {

                        $bQry = $this->home_model->update_social_user(array("linkdin_id" => $linkdin_id), array("linkdin_id" => $linkdin_id));

                    }

                    if ($bQry == '2') {

                        redirect('home/index/login');


                    } else { // if the user's credentials validated...


                        redirect('home/account');


                    }

                } else {


                    if ($this->session->userdata('user_id') > 0) {

                        $data = array(

                            'linkdin_id' => $linkdin_id

                        );

                        $wheredata = array(

                            'user_id' => $this->session->userdata('user_id')

                        );

                        $bQry = $this->home_model->update_social_user($wheredata, $data);

                        //====put condition for invite code related if user is alreadt logged in					

                    } else {


                        $db_values = array(

                            'user_name' => strtolower(str_replace(" ", "", $firstname)),

                            'last_name' => $lastName,
                            'email' => $emailAddress,
                            'image' => $tw_img,

                            'linkdin_id' => $linkdin_id,

                            'active' => 1,

                            'unique_code' => $unique_code,
                            'date_added' => date('Y-m-d H:i:s'),

                        );


                        $this->social_signup($db_values, 'linkdin_id');


                    }

                }

            }

        } catch (Exception $ex) {

            //throw new Exception('Error occurred in PayReceipt method');

            $error = WRONG_LINKEDIN_SETTINGS;

            $data['error'] = $error;

            $this->load->view('default/paypal_error', $data);

        }


    }
    /*
	Function name :invited()
	Parameter : code , invite_email
	Return : none
	Use : saved the data in user table	*/
    function invited($code, $invite_email = '')
    {
        if ($this->session->userdata('user_id') != '') {
            redirect('home/main_dashboard');
        }
        $email = '';
        if ($invite_email != 'facebook') {
            
          
            $check_request_exists = $this->home_model->get_code(array(
                'invite_code' => $code,
                'invite_email' => base64_decode($invite_email)
            ));
            if ($check_request_exists) {
                $user_id = $check_request_exists['invite_by'];
            }
            $email = $invite_email;
        } elseif ($invite_email == 'facebook') {
        }

        $domain_name = '';
        $get_domain_name = get_domain_name(base_url());
        if ($get_domain_name) {
            $domain_name = $get_domain_name;
        }

        $this->load->helper('cookie');
        $cookie = array(
            'name' => 'invite_code',
            'value' => $code,
            'expire' => time(),
            'domain' => $domain_name,
            'secure' => false
        );
        set_cookie($cookie);

        $this->session->set_userdata(array(
            'invite_code' => $code
        ));
        $db_values = array(
            'fb_uid' => '',
            'user_name' => '',
            'last_name' => '',
            'email' => $email,
            'tw_id' => '',
            'fb_img' => '',
            'fb_access_token' => '',
            'tw_screen_name' => '',
            'oauth_token' => '',
            'oauth_token_secret' => '',
            'invite_code' => $code,
        );
        // data ready, try to create the new user
        redirect('home/signup/' . $code.'/'.$invite_email);
    }

    /*
	Function name :adminlog()
	Parameter :admin_email
	Return : none
	Use : Admin user can take login to front site
	
	*/
    function adminlog($admin_email = '',$property_id=0)
    {

        $email = base64_decode($admin_email);
        $login_data = array(
            'email' => SecurePostData($email)
        );
        $chk = $this->home_model->user_validate($login_data, 'login');
        if ($chk != 'FALSE') {
            if ($chk['active'] == 1) {
                $data = array(
                    'user_id' => $chk['user_id'],
                    'user_name' => $chk['user_name'],
                    'email' => $chk['email'],
                );
            }
            $this->session->set_userdata($data);
        } else {
            $query = $this->db->get_where('admin', $login_data);
            $admin_detail = $query->row_array();
            $user_data = array(
                'user_name' => $admin_detail['username'],
                'last_name' => '',
                'email' => $email,
                'password' => $admin_detail['password'],
                'signup_ip' => SecurePostData($_SERVER['REMOTE_ADDR']),
                'date_added' => SecurePostData(date('Y-m-d H:i:s')),
                'confirm_key' => randomNumber(20),
                'active' => '1',
                'unique_code' => unique_user_code(getrandomCode(12))
            );
            $sign = $this->home_model->register($user_data);
        }

        if($property_id){
            redirect('property/add_property/'.$property_id);
        }
        else if($this->session->userdata('user_id') != '') {
            $projectControllerName=projectcontrollername();
            redirect('start_'.$projectControllerName.'/create_step1');
        }
    }

    /*
	Function name :delete_equity()
	Parameter :equity_id
	Return : none
	Use :Delete Particular equity
	
	*/

    function delete_property($property_id = '')
    {
        $property = GetOneProperty($property_id);
        if ($property['user_id'] == $this->session->userdata('user_id')) {

            $this->equity_model->delete_equity($property_id);
        }
    }

    /*
	Function name :valid_facebook_url()
	Parameter :field
	Return : none
	Use :validation of facebook url
	
	*/
    function valid_facebook_url($field)
    {
        if (!preg_match('/^(http\:\/\/|https\:\/\/)?(?:www\.)?facebook\.com\/(?:(?:\w\.)*#!\/)?(?:pages\/)?(?:[\w\-\.]*\/)*([\w\-\.]*)/', $field)) {
            return false;
        }
        return true;
    }

    /*
	Function name :valid_basecamp_url()
	Parameter :field
	Return : none
	Use :validation of basecamp url
	
	*/

    function valid_basecamp_url($field)
    {
        if (!preg_match('/^(http\:\/\/|https\:\/\/)?(?:www\.)?basecamp\.com\/(?:(?:\w\.)*#!\/)?(?:pages\/)?(?:[\w\-\.]*\/)*([\w\-\.]*)/', $field)) {
            return false;
        }
        return true;
    }

    /*
	Function name :valid_twitter_url()
	Parameter :field
	Return : none
	Use :validation of twitter url
	
	*/
    function valid_twitter_url($field)
    {
        if (!preg_match('/^(http\:\/\/|https\:\/\/)?(?:www\.)?twitter\.com\/(?:(?:\w\.)*#!\/)?(?:pages\/)?(?:[\w\-\.]*\/)*([\w\-\.]*)/', $field)) {
            return false;
        }
        return true;
    }

    /*
	Function name :valid_linkedin_url()
	Parameter :field
	Return : none
	Use :validation of linkedin url
	
	*/
    function valid_linkedin_url($field)
    {
        if (!preg_match('/^(http\:\/\/|https\:\/\/)?(?:www\.)?linkedin\.com\/(?:(?:\w\.)*#!\/)?(?:pages\/)?(?:[\w\-\.]*\/)*([\w\-\.]*)/', $field)) {
            return false;
        }
        return true;
    }

    /*
	Function name :valid_googleplus_url()
	Parameter :field
	Return : none
	Use :validation of googleplus url
	
	*/
    function valid_googleplus_url($field)
    {
        if (!preg_match('/^(http\:\/\/|https\:\/\/)?(?:www\.)?plus.google\.com\/(?:(?:\w\.)*#!\/)?(?:pages\/)?(?:[\w\-\.]*\/)*([\w\-\.]*)/', $field)) {
            return false;
        }
        return true;
    }

    /*
	Function name :valid_youtube_url()
	Parameter :field
	Return : none
	Use :validation of youtube url
	
	*/
    function valid_youtube_url($field)
    {
        if (!preg_match('/https?:\/\/(?:[a-zA_Z]{2,3}.)?(?:youtube\.com\/watch\?)((?:[\w\d\-\_\=]+&amp;(?:amp;)?)*v(?:&lt;[A-Z]+&gt;)?=([0-9a-zA-Z\-\_]+))/i', $field)) {
            return false;
        }
        return true;
    }

    /*
	Function name :valid_myspace_url()
	Parameter :field
	Return : none
	Use :validation of myspace url
	
	*/
    function valid_myspace_url($field)
    {
        if (!preg_match('/^(http\:\/\/|https\:\/\/)?(?:www\.)?myspace\.com\/(?:(?:\w\.)*#!\/)?(?:pages\/)?(?:[\w\-\.]*\/)*([\w\-\.]*)/', $field)) {
            return false;
        }
        return true;
    }

    /*
	Function name :change_perk_status()
	Parameter :field
	Return : none
	Use :Change perk status if its delivered.
	
	*/
    function change_perk_status()
    {
        $data['id'] = $_POST['id'];
        $id = $data['id'];
        $query = $this->db->query("update transaction set perk_delivered=1 where transaction_id='" . $id . "'");
        $data['perk_detail'] = get_one_transaction($id);
        $data['perk_status'] = $data['perk_detail']['perk_delivered'];
        echo $status = $data['perk_status'];
    }

    /*
	Function name :email_authenticate()
	Parameter :field
	Return : none
	Use :Check Email address exist or not
	
	*/

    function email_authenticate()
    {
        $email_id = $_POST['email_id'];

        $result = $this->db->query("SELECT * FROM user WHERE email='" . $email_id . "' And user_id !='".$this->session->userdata('user_id')."'");
        echo $result->num_rows();
    }

    /*
	Function name :merge_twitter_with_email()
	Parameter :field
	Return : none
	Use :Check Email address exist or not if exist than you want merge or not
	
	*/
    function merge_twitter_with_email()
    {
        $email = $_POST['email_id'];
        $tw_id = $_POST['tw_id'];
        $tw_screen_name = $_POST['tw_screen_name'];
        $tw_oauth_token = $_POST['tw_oauth_token'];
        $tw_oauth_token_secret = $_POST['tw_oauth_token_secret'];

        //echo $email. '-->'. $tw_id.'-->'.$tw_screen_name.'-->'.$tw_oauth_token.'-->'.$tw_oauth_token_secret;

        $data = array(
            'tw_id' => $tw_id,
            'tw_screen_name' => $tw_screen_name,
            'tw_oauth_token' => $tw_oauth_token,
            'tw_oauth_token_secret' => $tw_oauth_token_secret
        );
        if($this->session->userdata('user_id')!='')
        {
           
            $this->db->where('user_id', $this->session->userdata('user_id'));
            $this->db->delete('user');
             $this->session->unset_userdata('user_id');
        }
        $this->db->where('email', $email);
        $this->db->update('user', $data);

        if (mysql_affected_rows() > 0) {
            echo mysql_affected_rows();
        } else {
            echo '0';
        }
    }

    /*
    Function name :equity_following()
    Parameter :No parameter
    Return : none
    Use : this is used in equity detail page
    Description :  will show you list of follower for particular equity, according to equity id
    */
    function property_following()
    {
        check_user_authentication(true);
        $meta = meta_setting();
        $data['site_setting'] = site_setting();
        $data['taxonomy_setting'] = taxonomy_setting();
        $user_id = $this->session->userdata('user_id');
        $data['property_following'] = $this->following_model->GetAllFollowing($type = 'property_following', $user_id, $join = array(
            'property'
        ), $limit = 1000, $order = array(
            'property_follow_id' => 'desc'
        ));


        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/property/property_following', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
    }

     /*
    Function name :equity_following()
    Parameter :No parameter
    Return : none
    Use : this is used in equity detail page
    Description :  will show you list of follower for particular equity, according to equity id
    */
    function user_investments()
    {
        $user_id = check_user_authentication(true);
        $meta = meta_setting();
        $data['site_setting'] = site_setting();
        $this->load->library("pagination");
        
        $total_running_investment = $this->property_model->Campaign_counter('transaction', 0, 0, $user_id, '2', array(
            'user','campaign'
        ), 50, array(
            'campaign_id' => 'desc'
        ));

        
        // count total posted equity which are completed
        $total_completed_investment = $this->property_model->Campaign_counter('transaction', 0, 0, $user_id, '3', array(
            'user','campaign'
        ), 50, array(
            'campaign_id' => 'desc'
        ));

        $total_fail_investment = $this->property_model->Campaign_counter('transaction', 0, 0, $user_id, '5', array(
            'user','campaign'
        ), 50, array(
            'campaign_id' => 'desc'
        ));
        $total_investment = $total_running_investment['total'] + $total_completed_investment['total'];
        $data['total_running_investment'] = $total_running_investment['total'];
        $data['total_completed_investment'] = $total_completed_investment['total'];
        $data['total_fail_investment'] = $total_fail_investment['total'];
        $data['total_investment'] = $total_investment;

        $total_investment_campaign = $this->property_model->Campaign_counter('', 0, 0, $user_id, '2,3,4,5', array(
            'user','transaction','property'
        ), 1000, array(
            'transaction.transaction_id' => 'desc'
        ),'','yes');

        $config = array();
        $config["base_url"] = base_url() . "home/user_investments";
        $config["total_rows"] = $total_investment_campaign;
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;

        $config['first_tag_open'] = $config['last_tag_open']= $config['next_tag_open']= $config['prev_tag_open'] = 
        $config['num_tag_open'] = '<li>';
        $config['first_tag_close'] = $config['last_tag_close']= $config['next_tag_close']= $config['prev_tag_close'] = $config['num_tag_close'] = '</li>';
         
        $config['cur_tag_open'] = "<li class='active'><span>";
        $config['cur_tag_close'] = "</span></li>";

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["results"] =  $total_investment_campaign = $this->property_model->Campaign_counter('', 0, 0, $user_id, '2,3,4,5', array(
            'user','transaction','property'
        ), $config["per_page"], array(
            'transaction.transaction_id' => 'desc'
        ), $page);
        $data["links"] = $this->pagination->create_links();

        

      
        $data['total_investment_campaign'] = $total_investment_campaign;


        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/home/user_investments', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
    }

    function test_user(){
        
        $select = 'user_id,user_name as first_name,last_name,user_type,email,image,address,city,state,country,zip_code,signup_ip,date_added,user_about,user_website,user_occupation,user_interest,user_skill,facebook_url,twitter_url,linkedln_url,googleplus_url,bandcamp_url,youtube_url,myspace_url,profile_slug as username';

        $user = $this->db->select($select)
        ->from('user')
        ->order_by('user_id','desc')
        ->get()
        ->row_array();

        echo "<pre>";
        print_r($user);
    }

    function test_campaign(){
        
        $select = 'property.*, campaign.campaign_id,campaign.user_id as owner,campaign.amount_get as received,campaign.investment_close_date, campaign.date_added, campaign.status,suburbs.name suburb_name, suburbs.single_line,suburbs.state';

        $user = $this->db->select($select)
        ->from('campaign')
        ->join('property','campaign.property_id = property.property_id')
        ->join('suburbs','suburbs.id = property.property_suburb')
        ->order_by('campaign.campaign_id','desc')
        ->get()
        ->row();

        echo "<pre>";
        print_r($user);
    }

    function test_investor(){
        
        $select = 'transaction_id,user_id,campaign_id,property_id,perk_id,amount,email,host_ip,comment,transaction_date_time';

        $user = $this->db->select($select)
        ->from('transaction')
        ->order_by('transaction_id','desc')
        ->get()
        ->row();

        echo "<pre>";
        print_r($user);
    }


    function test_salesforceapi(){
        $this->load->library("salesforce_api");

        $res = $this->salesforce_api->get_token();

        if($res==false){
            echo "Token not generate";
        } else {
            echo "Token generated <br> ::  ".$res['access_token'];
        }
    }

    function test_api_update_user_salesfrc(){
        $result = UserData(1);
        // salesforce api call for new user 
        $this->load->library("salesforce_api");

        $temp_usertype = json_decode($result[0]['user_type']);
        $isInvestor=$isREAgent=$isOwner=$isDeveloper=false;

        if(in_array(1, $temp_usertype)){
            $isInvestor=true;
        }
        if(in_array(2, $temp_usertype)){
            $isREAgent = true;
        }
        if(in_array(3, $temp_usertype)){
            $isOwner = true;
        }
        if(in_array(4, $temp_usertype)){
            $isDeveloper = true;
        }

        // $temp_user_interest = json_decode($result[0]->user_interest);
        $residentialInterest=$commercialInterest=$industrialInterest=$ruralInterest=false;

        $require_data_array = array(
            "salesforceUserId" => $result[0]['salesforceuserid'],
            "covestaUserId" => $result[0]['user_id'],
            "companyName" => $result[0]['company_name'],
            "ABN" => $result[0]['abn_number'],
            "companyWebsite" => $result[0]['company_website'],
            "companyAddress" => $result[0]['company_address'],
            "firstName" => $result[0]['user_name'],
            "lastName" => $result[0]['last_name'],
            "email" => $result[0]['email'],
            "mobile" => $result[0]['mobile_number'],
            "postcode" => $result[0]['zip_code'],
            "address" => $result[0]['address'],
            "isDeveloper" => $isDeveloper,
            "isREAgent" => $isREAgent,
            "isInvestor" => $isInvestor,
            "isOwner" => $isOwner,
            "residentialInterest" => $residentialInterest,
            "commercialInterest" => $commercialInterest,
            "industrialInterest" => $industrialInterest,
            "ruralInterest" => $ruralInterest
        );

        $salesforceUserId = $this->salesforce_api->update_existing_user($require_data_array);
    }
    public function landing_subscribe($value='')
    {
        
        if($_POST){
            // var_dump($_POST);die;

                 $email_template = $this->db->query("select * from `email_template` where task='Contact Us'");

                    $email_temp = $email_template->row();

                    $email_subject = $email_temp->subject;

                    $email_message = $email_temp->message;

                    $name = $this->input->post('name');

                    $message = '<br/>';
                             
                    $email = $this->input->post('email');

                    $email_to = $email_temp->from_address;

                    $email_address_from = $email_temp->from_address;

                    $email_address_reply = $email;

                    $email_message = str_replace('{break}', '<br/>', $email_message);

                    $email_message = str_replace('{name}', $name, $email_message);

                    $email_message = str_replace('{message}', $message, $email_message);

                    $email_message = str_replace('{email}', $email, $email_message);

                    $str = $email_message;
                    $email_to="daniel@covesta.com.au";
                    // $email_to="darshan.rockersinfo@gmail.com";
                    $email_subject="New register in your site";

                    if (email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str)) {

                        // $this->home_model->insert_inquiry();

                    }

/////////////============email===========   


                    $msg = "" . 'Message sent successfully.' . "";
                    redirect('landing?mail_sent='.$msg);
        }else{ redirect('home');}
        }

}

?>
