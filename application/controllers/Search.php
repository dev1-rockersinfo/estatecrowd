<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 * The Home class is basically the default controller for the script.
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Search extends ROCKERS_Controller

{
    function __construct()
    {
        parent::__construct();
        $this->load->model('home_model');
        $this->load->model('property_model');
        $this->load->library('securimage');
    }

    function advance_search($type = '')
    {
         $user_id = check_user_authentication(true);
        $data = array();
        $offset =0;
        $limit = 12;
        $meta = meta_setting();
        $data['site_setting'] = site_setting();
   


      $suburb = $investment_amount ='';
      if(isset($_GET['suburb']))
      {
        $suburb = $_GET['suburb'];
      }
      if(isset($_GET['investment_amount']))
      {
        $investment_amount = $_GET['investment_amount'];
      }
      $investment_suburb='';
      if(isset($_GET['investment_suburb']))
      {
        $investment_suburb = $_GET['investment_suburb'];
      }
      $investment_address = '';
      if(isset($_GET['investment_address']))
      {
        $investment_address = $_GET['investment_address'];
      }
       $bedrooms = '';
      if(isset($_GET['bedrooms']))
      {
        $bedrooms = str_replace('--',' , ',$_GET['bedrooms']);
      }
        $bathrooms = '';
      if(isset($_GET['bathrooms']))
      {
        $bathrooms = str_replace('--',' , ',$_GET['bathrooms']);
      }
      $cars = '';
      if(isset($_GET['cars']))
      {
            $cars = str_replace('--',' , ',$_GET['cars']);
           
      }

      $property_type = '';
      if(isset($_GET['property_type']))
      {
            $property_type = $_GET['property_type'];
           
      }
     $investment_range = '';
      if(isset($_GET['investment_range']))
      {
            $investment_range = $_GET['investment_range'];
           
      }

      $min_property_investment_range = '';
      if(isset($_GET['min_property_investment_range']))
      {
            $min_property_investment_range = $_GET['min_property_investment_range'] * 1000000;
           
      }
      $max_property_investment_range = '';
      if(isset($_GET['max_property_investment_range']))
      {
            $max_property_investment_range = $_GET['max_property_investment_range']* 1000000;
           
      }
        $search_criteria = array(
            'investment_amount' => $investment_amount,
            'investment_suburb' => $investment_suburb,
            'investment_address' => $investment_address,
            'bedrooms' => $bedrooms,
            'bathrooms' => $bathrooms,
            'cars' => $cars,
            'property_type' => $property_type,
            'investment_range' => $investment_range,
            'min_property_investment_range' => $min_property_investment_range,
            'max_property_investment_range' => $max_property_investment_range,
            'suburb' => $suburb,
           
        );
        $data['search_criteria'] = $search_criteria;

         $search_properties = $this->property_model->GetAllSearchProperties(0, 0, '2', array(
            'user','campaign'
        ), $limit, array(
            'property_id' => 'desc'
        ),$search_criteria, $offset,'');
        $data['search_properties'] = $search_properties;

      // echo $this->db->last_query();die;

        $case = $type;
        if ($type == '') {
            $case = 'all';
        }
        $data['type'] = $type;
       
        $search_msg = '';
     
        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/home/property_search', $data, TRUE);
        //$this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
    }

    /*
    Function name :index()
    Parameter :none
    Return : none
    Use : user can see feature projects,latest projects and other detail
    Description : site home page its default function which called http://hostname/home
    SEO friendly URL which is declare in config route.php file  http://hostname/
    */
    /**
     * @param string $type
     */
    function property()
    {
        $user_id = check_user_authentication(true);
        $data = array();
        $offset =0;
        $limit = 12;
        $meta = meta_setting();
   
      $data['main_search'] = $this->input->get('main_search');

      $investment_amount = $this->input->get('investment_amount');
      $investment_suburb = $this->input->get('investment_suburb');
      $investment_address = $this->input->get('investment_address');

      if($data['main_search']==1){
        $investment_suburb='';
        $investment_address='';
      }else if($data['main_search']==2){
        $investment_amount='';
        $investment_address='';
      }else{
        $investment_amount='';
        $investment_suburb='';
      }

      $data['investment_amount'] = $investment_amount; 
      $data['investment_suburb'] = $investment_suburb; 
      $data['investment_address'] = $investment_address; 
      $data['site_setting'] = site_setting();

      $suburb =  $data['suburb'] = $this->input->get('suburb');
      $bedrooms = $data['bedrooms'] = $this->input->get('bedrooms');
      $bathrooms = $data['bathrooms'] = $this->input->get('bathrooms');
      $property_type = $data['property_type'] = $this->input->get('property_type');
      $cars = $data['cars'] = $this->input->get('cars');
      $investment_range = $data['investment_range'] = $this->input->get('investment_range');
      $min_property_investment_range = $data['min_property_investment_range'] = $this->input->get('min_property_investment_range');
      $max_property_investment_range = $data['max_property_investment_range'] = $this->input->get('max_property_investment_range');

      if($bathrooms) $bathrooms = str_replace('--',' , ',$bathrooms);
      if($bedrooms) $bedrooms = str_replace('--',' , ',$bedrooms);
      if($property_type=="other") $property_type="";
      if($min_property_investment_range) $min_property_investment_range = $min_property_investment_range*1000000;
      if($max_property_investment_range) $max_property_investment_range = $max_property_investment_range*1000000;

        $search_criteria = array(
            'investment_amount' => $investment_amount,
            'investment_suburb' => $investment_suburb,
            'investment_address' => $investment_address,
            'bedrooms' => $bedrooms,
            'bathrooms' => $bathrooms,
            'cars' => $cars,
            'property_type' => $property_type,
            'investment_range' => $investment_range,
            'min_property_investment_range' => $min_property_investment_range,
            'max_property_investment_range' => $max_property_investment_range,
           
        );
        $data['search_criteria'] = $search_criteria;

        $count_search_properties = $this->property_model->GetAllSearchProperties(0, 0, '2', array(
            'user','campaign'
        ), 10000, array(
            'campaign_id' => 'desc'
        ),$search_criteria, $offset,'yes');

        $data['total_rows'] = $count_search_properties;

         $search_properties = $this->property_model->GetAllSearchProperties(0, 0, '2', array(
            'user','campaign'
        ), $limit, array(
            'campaign_id' => 'desc'
        ),$search_criteria, $offset,'');

        $data['search_properties'] = $search_properties;

      //echo $this->db->last_query();die;

      
     
        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/home/search_property', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
    }

     /*
    Function name :index()
    Parameter :none
    Return : none
    Use : user can see feature projects,latest projects and other detail
    Description : site home page its default function which called http://hostname/home
    SEO friendly URL which is declare in config route.php file  http://hostname/
    */
    /**
     * @param string $type
     */
    function ajax_property($offset='')
    {
        $user_id = check_user_authentication(true);
        $data = array();
       
        $limit = 10000;
        $data['offset'] = $offset;
        $meta = meta_setting();
        $data['site_setting'] = site_setting();
        $investment_amount ='';
      if(isset($_GET['investment_amount']))
      {
        $investment_amount = $_GET['investment_amount'];
      }
      $investment_suburb='';
      if(isset($_GET['investment_suburb']))
      {
        $investment_suburb = $_GET['investment_suburb'];
      }
      $investment_address = '';
      if(isset($_GET['investment_address']))
      {
        $investment_address = $_GET['investment_address'];
      }
       $bedrooms = '';
      if(isset($_GET['bedrooms']))
      {
        $bedrooms = str_replace('--',' , ',$_GET['bedrooms']);
      }
        $bathrooms = '';
      if(isset($_GET['bathrooms']))
      {
        $bathrooms = str_replace('--',' , ',$_GET['bathrooms']);
      }
      $cars = '';
      if(isset($_GET['cars']))
      {
            $cars = str_replace('--',' , ',$_GET['cars']);
           
      }

      $property_type = '';
      if(isset($_GET['property_type']))
      {
            $property_type = $_GET['property_type'];
           
      }
        $search_criteria = array(
            'investment_amount' => $investment_amount,
            'investment_suburb' => $investment_suburb,
            'investment_address' => $investment_address,
            'bedrooms' => $bedrooms,
            'bathrooms' => $bathrooms,
            'cars' => $cars,
            'property_type' => $property_type,
           
        );
        $data['search_criteria'] = $search_criteria;

         $count_search_properties = $this->property_model->GetAllSearchProperties(0, 0, '2', array(
            'user','campaign'
        ), $limit, array(
            'campaign_id' => 'desc'
        ),$search_criteria, $offset,'yes');

        $data['total_row_display'] = $count_search_properties - $offset;

        $search_properties = $this->property_model->GetAllSearchProperties(0, 0, '2', array(
            'user','campaign'
        ), 12, array(
            'campaign_id' => 'desc'
        ),$search_criteria, $offset,'no');
        $data['search_properties'] = $search_properties;

      $this->load->view('default/home/ajax_search_property', $data);

     
    }

    /*
    Function name :index()
    Parameter :none
    Return : none
    Use : user can see feature projects,latest projects and other detail
    Description : site home page its default function which called http://hostname/home
    SEO friendly URL which is declare in config route.php file  http://hostname/
    */
    /**
     * @param string $type
     */
    function campaign()
    {
        $user_id = check_user_authentication(true);
        $data = array();
        $offset =0;
        $limit = 12;
        $meta = meta_setting();
        $data['site_setting'] = site_setting();

      $data['main_search'] = $this->input->get('main_search');

      $investment_amount = $this->input->get('investment_amount');
      $investment_suburb = $this->input->get('investment_suburb');
      $investment_address = $this->input->get('investment_address');

      if($data['main_search']==1){
        $investment_suburb='';
        $investment_address='';
      }else if($data['main_search']==2){
        $investment_amount='';
        $investment_address='';
      }else{
        $investment_amount='';
        $investment_suburb='';
      } 

      $data['investment_amount'] = $investment_amount; 
      $data['investment_suburb'] = $investment_suburb; 
      $data['investment_address'] = $investment_address; 

      $bedrooms =  $data['bedrooms'] = $this->input->get('bedrooms');
      $bathrooms =  $data['bathrooms'] = $this->input->get('bathrooms');
      $cars = $data['cars'] = $this->input->get('cars');
      $property_type = $data['property_type'] = $this->input->get('property_type');

      $investment_range = $data['investment_range'] = $this->input->get('investment_range');
      $min_property_investment_range = $data['min_property_investment_range'] = $this->input->get('min_property_investment_range');
      $max_property_investment_range = $data['max_property_investment_range'] = $this->input->get('max_property_investment_range');

      if($bathrooms) $bathrooms = str_replace('--',' , ',$bathrooms);
      if($bedrooms) $bedrooms = str_replace('--',' , ',$bedrooms);
      if($property_type=="other") $property_type="";


    //   $investment_amount ='';
    //   if(isset($_GET['investment_amount']))
    //   {
    //     $investment_amount = $_GET['investment_amount'];
    //   }
    //   $investment_suburb='';
    //   if(isset($_GET['investment_suburb']))
    //   {
    //     $investment_suburb = $_GET['investment_suburb'];
    //   }
    //   $investment_address = '';
    //   if(isset($_GET['investment_address']))
    //   {
    //     $investment_address = $_GET['investment_address'];
    //   }
    //    $bedrooms = '';
    //   if(isset($_GET['bedrooms']))
    //   {
    //     $bedrooms = str_replace('--',' , ',$_GET['bedrooms']);
    //   }
    //     $bathrooms = '';
    //   if(isset($_GET['bathrooms']))
    //   {
    //     $bathrooms = str_replace('--',' , ',$_GET['bathrooms']);
    //   }
    //   $cars = '';
    //   if(isset($_GET['cars']))
    //   {
    //         $cars = str_replace('--',' , ',$_GET['cars']);
           
    // }

    //     $property_type = '';
    //     if(isset($_GET['property_type']))
    //     {
    //       $property_type = $_GET['property_type'];
    //       if($property_type=="other"){
    //         $property_type="";
    //       }
    //     }


        $search_criteria = array(
            'investment_amount' => $investment_amount,
            'investment_suburb' => $investment_suburb,
            'investment_address' => $investment_address,
            'bedrooms' => $bedrooms,
            'bathrooms' => $bathrooms,
            'cars' => $cars,
            'property_type' => $property_type,
           
        );
        $data['search_criteria'] = $search_criteria;

         $count_search_properties = $this->property_model->GetAllSearchProperties(0, 0, '2', array(
            'user','campaign'
        ), 10000, array(
            'campaign_id' => 'desc'
        ),$search_criteria, $offset,'yes','yes');

        $data['total_rows'] = $count_search_properties;

         $search_properties = $this->property_model->GetAllSearchProperties(0, 0, '2', array(
            'user','campaign'
        ), $limit, array(
            'property_id' => 'desc'
        ),$search_criteria, $offset,'','yes');
        $data['search_properties'] = $search_properties;
      
     
        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/home/search_campaign', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
    }

    /*
    Function name :index()
    Parameter :none
    Return : none
    Use : user can see feature projects,latest projects and other detail
    Description : site home page its default function which called http://hostname/home
    SEO friendly URL which is declare in config route.php file  http://hostname/
    */
    /**
     * @param string $type
     */
    function ajax_campaign($offset = '')
    {
         $user_id = check_user_authentication(true);
        $data = array();
        
        $limit = 10000;
        $meta = meta_setting();
        $data['site_setting'] = site_setting();
   


        $investment_amount ='';
      if(isset($_GET['investment_amount']))
      {
        $investment_amount = $_GET['investment_amount'];
      }
      $investment_suburb='';
      if(isset($_GET['investment_suburb']))
      {
        $investment_suburb = $_GET['investment_suburb'];
      }
      $investment_address = '';
      if(isset($_GET['investment_address']))
      {
        $investment_address = $_GET['investment_address'];
      }
       $bedrooms = '';
      if(isset($_GET['bedrooms']))
      {
        $bedrooms = str_replace('--',' , ',$_GET['bedrooms']);
      }
        $bathrooms = '';
      if(isset($_GET['bathrooms']))
      {
        $bathrooms = str_replace('--',' , ',$_GET['bathrooms']);
      }
      $cars = '';
      if(isset($_GET['cars']))
      {
            $cars = str_replace('--',' , ',$_GET['cars']);
           
    }

         $property_type = '';
      if(isset($_GET['property_type']))
      {
            $property_type = $_GET['property_type'];
           
        }
        $search_criteria = array(
            'investment_amount' => $investment_amount,
            'investment_suburb' => $investment_suburb,
            'investment_address' => $investment_address,
            'bedrooms' => $bedrooms,
            'bathrooms' => $bathrooms,
            'cars' => $cars,
            'property_type' => $property_type,
           
        );
        $data['search_criteria'] = $search_criteria;

         

         $count_search_properties = $this->property_model->GetAllSearchProperties(0, 0, '2', array(
            'user','campaign'
        ), $limit, array(
            'campaign_id' => 'desc'
        ),$search_criteria, $offset,'yes','yes');

        $data['total_row_display'] = $count_search_properties - $offset;

        $search_properties = $this->property_model->GetAllSearchProperties(0, 0, '2', array(
            'user','campaign'
        ), 12, array(
            'campaign_id' => 'desc'
        ),$search_criteria, $offset,'no','yes');
        $data['search_properties'] = $search_properties;

      $this->load->view('default/home/ajax_search_campaign', $data);

      // echo $this->db->last_query();die;

      
    }



/*
    Function name :search_auto()
    Parameter :text ,id
    Return : none
    Use : use for auto search
    */
    function search_auto($text = '', $id = '')
    {
        $data = array();
        $offset = 0;
        $limit = 3;
        $keyword = SecurePostData($text);
        $search_criteria = array(
            'investment_suburbs' => $keyword
        );
        $data['search_criteria'] = $search_criteria;
        $search_suburbs = $this->home_model->GetAllSearchSuburbs($search_criteria);
        // var_dump($search_equities);
        if ($search_suburbs) {
            $str = '<ul id="srhdiv">';
            foreach ($search_suburbs as $search_suburb) {
                $str .= '<li onclick="selecttext_suburb(this);">' . $search_suburb['suburb'] . ' , '.$search_suburb['state'].'</li>';
            }
            $str .= '</ul>';
            echo $str;
            die();
        }
        die;
    }

    /*
    Function name :investors()
    Parameter :text ,id
    Return : none
    Use : use for auto search
    */
    function investors($text = '', $id = '')
    {
        $user_id = check_user_authentication(true);
        $data = array();
        $offset =0;
        $limit = 12;
        $meta = meta_setting();
        $data['site_setting'] = site_setting();
        $data['investors'] = $this->home_model->GetAllInvestors($user_id,array('user_property_detail','user_follow')); 
        
       
        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/home/investors', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
    }

    function ListInvestors()
    {
       $user_id = check_user_authentication(true);
      if($this->input->post('keyword') == 'size_of_investment')
      {

        $minvalue = str_replace(array('$', 'M'), '', $this->input->post('minvalue'));
        $maxvalue = str_replace(array('$', 'M'), '', $this->input->post('maxvalue'));
        
        

        $search_criteria = array(
          'size_of_investment'=>$this->input->post('keyword'),
          'minvalue'=>$minvalue,
          'maxvalue'=>$maxvalue,


          );
      }
      else
      {
        $search_criteria = array($this->input->post('keyword')=>$this->input->post('value'));
       
      }
    

      $data['investors'] = $this->home_model->GetAllInvestors($user_id,array('user_property_detail','user_follow'),array('user.user_id' => 'asc'),100,$search_criteria); 
     
       $this->load->view('default/home/ajax_list_investors', $data);
    }
}

?>
