<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 * The Home class is basically the default controller for the script.
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Comment extends ROCKERS_Controller

{
    function __construct()
    {
        parent::__construct();
        $this->load->model('home_model');
        $this->load->model('account_model');

    }

    /*
    Function name :index()
    Parameter :none
    Return : none
    Use : to show the user comment
    Description : this is used to show the user comment on comment page
    */
    function index()
    {
        $user_id = check_user_authentication(true);
        $data['user_id'] = $user_id;
        $data = array();


        $meta = meta_setting();
        $data['site_setting'] = site_setting();


        $usedata = UserData($user_id, $join = array(
            'user_notification'
        ));


        $data['user_id'] = $usedata[0]['user_id'];
        $data['user_comment'] = $this->account_model->GetUserComments($user_id, array(
            'user',
        ), 50, array(
            'user_comment.id' => 'desc'
        ));

       

        // echo "<pre>";
        // print_r($data['user_comment']);die;



        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/comment/index', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
    }

    /*
    Function name :delete_comment()
    Parameter :comment_id
    Return : none
    Use :Delete Particular comment

    */

    function delete_comment($comment_id = '')
    {
        $user_id = check_user_authentication(true);
       
        $comment_id = $this->input->post('comment_id');
        $crowd_id = $this->input->post('crowd_id');
        $property_id = $this->input->post('property_id');
        $this->home_model->delete_comment($comment_id,$crowd_id,$property_id);

         user_deletecache('comments',$user_id);

        $data["success"] = 'success';
        echo json_encode($data);
    }

    /*
    Function name :search_comment()
    Parameter :comment_id
    Return : none
    Use :Delete Particular comment

    */

    function search_comment()
    {

        $user_id = check_user_authentication(true);
        $type = $this->input->post('search_value');
         $data['crowd_comment_data'] ='';
         $data['property_comments_data']='';
       switch ($type) {
            case 'all':
               # code...
                    $property_comment = PropertyComments($user_id, '', $join = array(
                                                'comment','campaign'
                                               
                                            ), $limit = 100, $group = array(
                                                'comment_id' => 'comment.comment_id'
                                            ), $order = array(
                                                'property.property_id' => 'desc',
                                                'comment.comment_id' => 'desc'
                                            ), 1,'','','','yes');

                      //echo $this->db->last_query();die;

                   

                    $crowd_comment = CrowdComments($user_id, '', $join = array(
                                        'crowd_comment'
                                       
                                    ), $limit = 100, $group = array(
                                        'id' => 'crowd_comment.id'
                                    ), $order = array(
                                        'crowd.crowd_id' => 'desc',
                                        'crowd_comment.id' => 'desc'
                                    ),'','','yes');

                 //$data['crowd_comment_data'] = $crowd_comment;

                if(is_array($property_comment))
                {
                    $array_merge =  array_merge($property_comment,$crowd_comment);
                }
                else
                {
                    $array_merge =  $crowd_comment;
                }

                 


                  $data['property_comments_data'] =$array_merge;
               break;

            case 'property':

                $property_comment = PropertyComments($user_id, '', $join = array(
                                        'comment','campaign'
                                       
                                    ), $limit = 100, $group = array(
                                        'comment_id' => 'comment.comment_id'
                                    ), $order = array(
                                        'property.property_id' => 'desc',
                                        'comment.comment_id' => 'desc'
                                    ), 1,'','','','yes','property');

               
                $data['property_comments_data'] =$property_comment;
                



               break;

            case 'campaign':

              $property_comment = PropertyComments($user_id, '', $join = array(
                                        'comment','campaign'
                                       
                                    ), $limit = 100, $group = array(
                                        'comment_id' => 'comment.comment_id'
                                    ), $order = array(
                                        'property.property_id' => 'desc',
                                        'comment.comment_id' => 'desc'
                                    ), 1,'','','','yes','campaign');

              //echo $this->db->last_query();die;

                $data['property_comments_data'] =$property_comment;
               
               # code...
               break;
            case 'crowd':
            $crowd_comment = CrowdComments($user_id, '', $join = array(
                                        'crowd_comment'
                                       
                                    ), $limit = 100, $group = array(
                                        'id' => 'crowd_comment.id'
                                    ), $order = array(
                                        'crowd.crowd_id' => 'desc',
                                        'crowd_comment.id' => 'desc'
                                    ),'','','yes');

                 $data['property_comments_data'] =$crowd_comment;
                 
               # code...
               break;
           
           default:
               # code...
               break;
       }

        $this->load->view('default/comment/search_comment', $data);
       
    }

}

/* end of file */
