<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 * The Home class is basically the default controller for the script.
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Message extends ROCKERS_Controller

{
    function __construct()
    {
        parent::__construct();
        $this->load->model('message_model');

        $this->load->model('home_model');
        $this->load->model('account_model');
    }

    /*
        Function name :send_mail_project_profile()
        Parameter :id ,user_id
        Return : none
        Use : User can send message to project owner from project detail.
        */
    function send_mail_project_profile($id = '', $user_id = 0)
    {
        $data['error_message'] = "";
        $data['id'] = $id;
        
        $equity=array();
         if ($id > 0) {
             $equity = GetOneEquity($id);
              $equity_url = $equity['equity_url'];
         }
        $user_detail = UserData($user_id, array());       
        $user_profile_slug = $user_detail[0]['profile_slug'];
        
        $taxonomy_setting = taxonomy_setting();
        $project_url = $taxonomy_setting['project_url'];
        
        if ($id > 0) {
            
            $redirect = $project_url . '/' . $equity_url . '/' . $id;
        } else {
            $redirect = 'user/' . $user_profile_slug;
        }

        $return_url = base64_encode($redirect);
        if ($this->session->userdata('user_id') == '') {
            echo "<script>parent.window.location.href='" . site_url('home/login/' . $return_url) . "'</script>";
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('subject', SUBJECT, 'required');
        $this->form_validation->set_rules('comments', MESSAGES, 'required');
        if ($this->form_validation->run() == FALSE) {
            $data['error_message'] = "";
            if (validation_errors()) {
                $data['error_message'] = validation_errors();
            } else {
                $data['error_message'] = '';
            }

            $meta = meta_setting();
            $data['site_setting'] = site_setting();
            $data["error"] = "";
            $data['user_id'] = $user_id;
            $this->template->write('meta_title', 'Reward-' . $meta['title'], TRUE);
            $this->template->write('meta_description', $meta['meta_description'], TRUE);
            $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
			$this->template->write_view('header', 'default/common/header', $data, TRUE);
            $this->template->write_view('main_content', 'default/message/message_user_profile', $data, TRUE);
			$this->template->write_view('footer', 'default/common/footer', $data, TRUE);
            $this->template->render();
        } else {
            $login_user_detail = UserData($this->session->userdata('user_id'), array());
            $login_user_profile_slug = $login_user_detail[0]['profile_slug'];
            if ($id > 0) {
                $msg = 'send_success';
                $redir = $project_url . '/' . $equity_url . '/' . $id . '/' . $msg;;
            } else {
                $msg = 'send_success';
                $redir = 'user/' . $login_user_profile_slug . '/' . $msg;
            }

            $msgsend = YOUR_MESSAGE_HAS_BEEN_SUCCESSFULLY;
            
            $sender_id=$this->session->userdata('user_id');
            $receiver_id=$this->input->post('user_id');
            
            $reply_message_id=$this->message_model->get_user_message_thread($sender_id,$receiver_id);
            
            
            
            $data = array(
                'sender_id' => $sender_id,
                'receiver_id' => $receiver_id,
                'is_read' => 0,
                'message_subject' => $this->input->post('subject'),
                'message_content' => $this->input->post('comments'),
                'date_added' => date('Y-m-d H:i:s'),
                'equity_id' => $id,
                'reply_message_id'=>$reply_message_id
            );
            
            
            $message_insert = $this->message_model->insert_project_profile_message($data);
            $message_setting = message_setting();
            $user_id = $this->input->post('user_id');


            $message_setting->message_enable;
            $message_user_profile_link = site_url('user/' . $login_user_profile_slug);
            $user_name = $user_detail[0]['user_name'];
            $message_user_name = $login_user_detail[0]['user_name'];
            $content = $this->input->post('comments');

            $user['user_name'] = $user_detail[0]['user_name'] . ' ' . $user_detail[0]['last_name'];
            $user['email'] = $user_detail[0]['email'];
            $user['message_user_name'] = '<a href="' . site_url('user/'.$login_user_profile_slug) . '">'.$login_user_detail[0]['user_name'] . ' ' . $login_user_detail[0]['last_name'].'</a>';
            $user['dateadded'] = date('Y-m-d');
            $user['subject'] = $this->input->post('subject');
            $user['message'] = $this->input->post('comments');
            $user['view_message_link'] = '<a href="' . site_url('inbox/message_conversation/'.$message_insert) . '">' .CLICK_HERE . '</a>';            $this->mailalerts('user_message', '', '', $user, 'Send message');

            echo "<script>parent.window.location.href='" . site_url($redir) . "'</script>";
        }
    }

    /*
        Function name :send_mail_project_profile()
        Parameter :id ,user_id
        Return : none
        Use : User can send message to project owner from project detail.
        */
    function send_message($user_id = 0,$id = '' )
    {
        $data=array();
        $data['error'] ='';
        $data['success'] = '';
        $data['id'] = $id;
        
        $data['user_id'] = $user_id;
        $property=array();
         if ($id > 0) {
             $property = GetOneProperty($id);
              $property_url = $property['equity_url'];
         }
        $user_detail = UserData($user_id, array());       
        $user_profile_slug = $user_detail[0]['profile_slug'];
        
        
        if ($id > 0) {
            
            $redirect = $project_url . '/' . $property_url . '/' . $id;
        } else {
            $redirect = 'user/' . $user_profile_slug;
        }

        $return_url = base64_encode($redirect);
        if ($this->session->userdata('user_id') == '') {
            echo "<script>parent.window.location.href='" . site_url('home/login/' . $return_url) . "'</script>";
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('subject', SUBJECT, 'required');
        $this->form_validation->set_rules('comments', MESSAGES, 'required');
        if ($this->form_validation->run() == FALSE) {
            $data['error_message'] = "";
            if (validation_errors()) {
              $data['error'] = validation_errors();
            } else {
              $data['error'] = '';
            }

        } else {
            $login_user_detail = UserData($this->session->userdata('user_id'), array());
            $login_user_profile_slug = $login_user_detail[0]['profile_slug'];
            if ($id > 0) {
                $msg = 'send_success';
                $redir =  'properties/' . $property_url . '/' . $id . '/' . $msg;;
            } else {
                $msg = 'send_success';
                $redir = 'user/' . $login_user_profile_slug . '/' . $msg;
            }

           
            
            $sender_id=$this->session->userdata('user_id');
            $receiver_id=$user_id;
            
            $reply_message_id=$this->message_model->get_user_message_thread($sender_id,$receiver_id);
            
            
            
            $data = array(
                'sender_id' => $sender_id,
                'receiver_id' => $receiver_id,
                'is_read' => 0,
                'message_subject' => $this->input->post('subject'),
                'message_content' => $this->input->post('comments'),
                'date_added' => date('Y-m-d H:i:s'),
                'property_id' => $id,
                'reply_message_id'=>$reply_message_id
            );
            
            
            $message_insert = $this->message_model->insert_project_profile_message($data);
            $message_setting = message_setting();
            $user_id = $this->input->post('user_id');


            $message_setting->message_enable;
            $message_user_profile_link = site_url('user/' . $login_user_profile_slug);
            $user_name = $user_detail[0]['user_name'];
            $message_user_name = $login_user_detail[0]['user_name'];
            $content = $this->input->post('comments');

            $user['user_name'] = $user_detail[0]['user_name'] . ' ' . $user_detail[0]['last_name'];
            $user['email'] = $user_detail[0]['email'];
            $user['message_user_name'] = '<a href="' . site_url('user/'.$login_user_profile_slug) . '">'.$login_user_detail[0]['user_name'] . ' ' . $login_user_detail[0]['last_name'].'</a>';
            $user['dateadded'] = date('Y-m-d');
            $user['subject'] = $this->input->post('subject');
            $user['message'] = $this->input->post('comments');
            $user['view_message_link'] = '<a href="' . site_url('inbox/message_conversation/'.$message_insert) . '">' .CLICK_HERE . '</a>';            $this->mailalerts('user_message', '', '', $user, 'Send message');

           // echo "<script>parent.window.location.href='" . site_url($redir) . "'</script>";
            $data['success'] = YOUR_MESSAGE_HAS_BEEN_SUCCESSFULLY;
        }
          echo json_encode($data);
    }
}

?>
