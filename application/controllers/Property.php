<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 * The Home class is basically the default controller for the script.
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Property extends ROCKERS_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('message_model');

        $this->load->model('home_model');
        $this->load->model('account_model');
        $this->load->model('property_model');
        $this->load->model('follower_model');
        $this->load->model('following_model');
    }
    /*
        Function name :investment_summary()
        Parameter :id 
        Return : none
        Use : User can add new property.
        */
    function index()
    {
        $user_id = check_user_authentication(true);
       
        $data = array();
     
        $meta = meta_setting();
        $data['site_setting'] = site_setting();

        $data['properties'] = $this->property_model->GetAllProperties($user_id);



        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/property/index', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
    }

     /*
        Function name :investment_summary()
        Parameter :id 
        Return : none
        Use : User can add new property.
        */
    function campaign()
    {
        $user_id = check_user_authentication(true);
       
        $data = array();
     
        $meta = meta_setting();
        $data['site_setting'] = site_setting();

         $user_property_campaign = $this->property_model->GetAllProperties(0, 0, '', array(
            'user','campaign'
        ), 1000, array(
            'property_id' => 'desc'
        ), '','','yes',$user_id);

        $data['user_property_campaign'] = $user_property_campaign;

        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/property/campaigns', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
    }
     /*
        Function name :investment_summary()
        Parameter :id 
        Return : none
        Use : User can add new property.
        */
    function property_detail($property_url='')
    {
        
       
        $data = array();
     
        $meta = meta_setting();
        $data['site_setting'] = site_setting();

        $properties = $this->property_model->GetAllProperties('',$property_url,'',array('campaign','user'));
        $data['properties'] = $properties[0];




        $data['share_property_detail'] = $properties;

        $user_id = $properties[0]['user_id'];

        $property_id =  $properties[0]['property_id'];
        $campaign_id =  $properties[0]['campaign_id'];

        $data['comment_url'] = site_url('property/comment_list/' . $property_id);
        $data['updates_url'] = site_url('property/updates_list/' . $property_id . '/');

        $data['follower'] = $this->follower_model->GetAllFollower($type = 'property_follow', $property_id, 0, $join = array(
                'user'
            ), $limit = 1000, $order = array(
                'property_follow_id' => 'desc'
            ));

        if (!$this->simple_cache->is_cached('property_detail_total_comment' . $property_url)) {
            $data['comment_total'] = $this->account_model->GetUpdateCommentGallery(0, $property_id, '', $join = array(
                'comment',
                'user'
            ), 100, $group = array(
                'comment_id' => 'comment.comment_id'
            ), $order = array(
                'property.property_id' => 'desc',
                'comment.comment_id' => 'desc'
            ), 1);
            $this->simple_cache->cache_item('property_detail_total_comment' . $property_url, $data['comment_total']);
        } else {
            $data['comment_total'] = $this->simple_cache->get_item('property_detail_total_comment' . $property_url);
        }
        $data['comment_count'] = count($data['comment_total']);

        if (!$this->simple_cache->is_cached('property_detail_comments' . $property_url)) {
            $data['comment'] = $this->account_model->GetUpdateCommentGallery(0, $property_id, '', $join = array(
                'comment',
                'user'
            ), $limit = 5, $group = array(
                'comment_id' => 'comment.comment_id'
            ), $order = array(
                'property.property_id' => 'desc',
                'comment.comment_id' => 'desc'
            ), 1);
            $this->simple_cache->cache_item('property_detail_comments' . $property_url, $data['comment']);
        } else {
            $data['comment'] = $this->simple_cache->get_item('property_detail_comments' . $property_url);
        }

         if (!$this->simple_cache->is_cached('property_detail_all_gallery' . $property_id)) {

           
            // not cached, do our things that need caching
            $get_property_gallery = $this->account_model->GetUpdateCommentGallery(0, $property_id, '', $join = array(
                'property_gallery'
            ), $limit = 1000, $group = array(), $order = array(
                'property.property_id' => 'desc'
            ), '', '', '', '', $user_id); // store in cache

            $this->simple_cache->cache_item('property_detail_all_gallery' . $property_id, $get_property_gallery);
        } else {
                 
            $get_property_gallery = $this->simple_cache->get_item('property_detail_all_gallery' . $property_id);
        }
        $data['gallery'] = $get_property_gallery;




        if (!$this->simple_cache->is_cached('property_detail_all_video' . $property_id)) {
            // not cached, do our things that need caching
            $get_property_video = $this->account_model->GetUpdateCommentGallery(0, $property_id, '', $join = array(
                'video_gallery'
            ), $limit = 1000, $group = array(), $order = array(
                'video_gallery.id' => 'asc'
            ), '', '', '', '', $user_id); // store in cache

            $this->simple_cache->cache_item('property_detail_all_video' . $property_id, $get_property_video);
        } else {
            $get_property_video = $this->simple_cache->get_item('property_detail_all_video' . $property_id);
        }
        $data['property_video'] = $get_property_video;


        $data['funder_total'] = array();
        if($campaign_id){
            if (!$this->simple_cache->is_cached('property_detail_all_funder_total' . $property_url)) {
                $data['funder_total'] = $this->account_model->GetDonation('funder', 0, $campaign_id, '', $join = array(
                    'user'
                ), 100, $order = array(
                    'transaction_id' => 'desc'
                ));
                $this->simple_cache->cache_item('property_detail_all_funder_total' . $property_url, $data['funder_total']);
            } else {
                $data['funder_total'] = $this->simple_cache->get_item('property_detail_all_funder_total' . $property_url);
            }
        }
        $data['funder_count'] = count($data['funder_total']);

        $data['funder'] = array();
        if($campaign_id){
            if (!$this->simple_cache->is_cached('property_detail_all_funder' . $property_url)) {
                $data['funder'] = $this->account_model->GetDonation('funder', 0, $campaign_id, '', $join = array(
                    'user'
                ), $limit = 5, $order = array(
                    'transaction_id' => 'desc'
                ));
                $this->simple_cache->cache_item('property_detail_all_funder' . $property_url, $data['funder']);

            } else {
                $data['funder'] = $this->account_model->GetDonation('funder', 0, $campaign_id, '', $join = array(
                    'user'
                ), $limit = 5, $order = array(
                    'transaction_id' => 'desc'
                ));
            }
        }

        if (!$this->simple_cache->is_cached('property_detail_all_update_count' . $property_id)) {
            // not cached, do our things that need caching
            $get_property_update_count = $this->account_model->GetUpdateCommentGallery(0, $property_id, '', $join = array(
                'updates'
            ), 100, $group = array(), $order = array(
                'updates.date_added' => 'desc'
            ), ''); // store in cache

            $this->simple_cache->cache_item('property_detail_all_update_count' . $property_id, $get_property_update_count);
        } else {
            $get_property_update_count = $this->simple_cache->get_item('property_detail_all_update_count' . $property_id);
        }
        $data['update_on_my_property_total'] = $get_property_update_count;
        $data['update_on_my_property_count'] = count($data['update_on_my_property_total']);
        if (!$this->simple_cache->is_cached('property_detail_all_update' . $property_id)) {
            // not cached, do our things that need caching
            $get_property_update = $this->account_model->GetUpdateCommentGallery(0, $property_id, '', $join = array(
                'updates'
            ), $limit = 5, $group = array(), $order = array(
                'updates.date_added' => 'desc'
            ), ''); // store in cache

            $this->simple_cache->cache_item('property_detail_all_update' . $property_id, $get_property_update);
        } else {
            $get_property_update = $this->simple_cache->get_item('property_detail_all_update' . $property_id);
        }
       
        $data['update_on_my_property'] = $get_property_update;

        $view_draft_equity = '';
        $view_hidden_equity = '';
        // display all tabs in detail page

       
        ($campaign_id > 0) ? $status = $properties[0]['campaign_status'] : $status = $properties[0]['status'];
        $is_status = $status;
        $status_check = array(0, 1);
        if (!in_array($is_status, $status_check)) {
            $view_draft_equity = 'yes';
        }


        $status_check = array(8);
        if (!in_array($is_status, $status_check)) {
            $view_hidden_equity = 'yes';
        }

        $data['attachments'] = $this->property_model->getattachments($property_id,0);

        

        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);
       
         if ($view_draft_equity != 'yes') {
            $this->template->write_view('main_content', 'default/property/404_page', $data, TRUE);
        } else if ($view_hidden_equity != 'yes') {
            $this->template->write_view('main_content', 'default/property/hidden_page', $data, TRUE);
        } else {
            $this->template->write_view('main_content', 'default/property/property_detail', $data, TRUE);
        }
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
    }

       /*
        Function name :dashboard()
        Parameter :id 
        Return : none
        Use : dashboard.
        */
    function dashboard($property_id='')
    {
        
        $user_id = check_user_authentication(true);

        // if($property_id > 0)
        // {
        //     if (!$this->property_model->is_property_owner($property_id, false)) {
        //         redirect('home/notedit_property');
        //     }
        // }

        $data = array();
     
        $meta = meta_setting();
        $data['site_setting'] = site_setting();

        $properties = $this->property_model->GetAllProperties('',$property_id,'',array('campaign','user'));
        $data['properties'] = $properties[0];

         $data['property_comments'] = $this->account_model->GetUpdateCommentGallery(0, $property_id, '', $join = array(
            'comment',
            'user'
        ), $limit = 0, $group = array(
            'comment_id' => 'comment.comment_id'
        ), $order = array(
            'property.property_id' => 'desc',
            'comment.comment_id' => 'desc'
        ), '');

        $data['is_property_owner'] = $this->property_model->is_property_owner($property_id, false);



        $data['follower'] = $this->follower_model->GetAllFollower($type = 'property_follow', $property_id, 0, $join = array(
            'user'
        ), $limit = 1000, $order = array(
            'property_follow_id' => 'desc'
        ));

        $data['property_updates'] = $this->account_model->GetUpdateCommentGallery(0, $property_id, '', $join = array(
            'updates'
        ), $limit = 0, $group = array(), $order = array(
            'updates.date_added' => 'desc'
        ), '');


        $data['funder'] = $this->account_model->GetDonation('funder', 0, $data['properties']['campaign_id'], '', $join = array(
            'user'
        ), $limit = 50, $order = array(
            'transaction_id' => 'desc'
        ));

       



        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/property/dashboard', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
    }
    /*
    Function name :commentaction()
    Parameter :none
    Return : none
    Use :  Comments reply,approve,decline,spam,delete
    */
    function commentaction()
    {

        $property_id = SecurePostData($this->input->post('property_id'));
        $user_id = check_user_authentication(true);
        // if ($property_id > 0) {
        //     if (!$this->property_model->is_property_owner($property_id, false)) {
        //         redirect('home/my_equity');
        //     }
        // }
        $data1['property_id'] = SecurePostData($this->input->post('property_id'));
        $data1['user_id'] = $this->session->userdata('user_id');
        $data1['comments'] = strip_tags($this->input->post('comment_reply'));
        $data1['comment_type'] = SecurePostData($this->input->post('comment_type'));
        $data1['parent_id'] = SecurePostData($this->input->post('comment_id'));
        $comment_user_id = SecurePostData($this->input->post('comment_user_id'));
        $data1['status'] = 1;
        $data1['date_added'] = date("Y-m-d H:i:s");
        $data1['comment_ip'] = $_SERVER['REMOTE_ADDR'];


        $property_id = $data1['property_id'];
        $comment_id = SecurePostData($this->input->post('comment_id'));
        $type = SecurePostData($this->input->post('type'));

        $type1 = SecurePostData($this->input->post('type1'));
        $comment = $data1['comments'];
        $comment_user_ip = SecurePostData($this->input->post('comment_ip'));
        $property_data = GetOneProperty($property_id);
        $property_url = $property_data['property_url'];
        $this->form_validation->set_rules('comment', COMMENT, 'required');
        $data["msg"]['error'] = '';
        $data["msg"]['success'] = '';

        if ($this->form_validation->run() == FALSE) {
            if (validation_errors()) {
                $data["msg"]['error'] = validation_errors();
            } else {
                $data["msg"]['error'] = "";
            }
        } else {
            if ($type == 'approved') {
                $data1['comment_approve'] = $this->property_model->commentaction($comment_id, $type, $property_id, 1, '', '', '', '');


                $data["msg"]["success"] = '<span>' . RECORD_APPROVED_SUCCESSFULLY . '</span>';
            }
            if ($type == 'decline') {
                $data1['comment_decline'] = $this->property_model->commentaction($comment_id, $type, $property_id, 2, '', '', '', '');
                $data["msg"]["success"] = '<span>' . RECORD_DECLINED_SUCCESSFULLY . '</span>';
            }
            if ($type == 'reply') {
                if ($type1 == 'edit') {
                    $data1['comment_edit'] = $this->property_model->commentaction($comment_id, $type, $property_id, 1, '', '', $type1, $comment);
                    $data["msg"]["success"] = '<span>' . RECORD_UPDATED_SUCCESSFULLY . '</span>';
                } else {
                    $data1['comment_reply'] = $this->property_model->commentaction($comment_id, $type, $property_id, 1, $data1, '', '', '');
                    $data["msg"]["success"] = '<span>' . RECORD_REPLIED_SUCCESSFULLY . '</span>';


                    $site_setting = site_setting();
                    $site_name = $site_setting['site_name'];
                    $taxonomy_setting = taxonomy_setting();
                    $taxo_comments = $taxonomy_setting['comments'];
                    $project_url = $taxonomy_setting['project_url'];
                    $comments_plural = $taxonomy_setting['comments_plural'];
                    $project_name = SecureShowData($taxonomy_setting['project_name']);

                    $user_not_own = $this->account_model->get_email_notification($comment_user_id);
                    $property_address = SecureShowData($property_data['property_address']);
                    $property_page_link = site_url( 'properties/' . $property_url);
                    $property_title_anchor = '<a href="' . $property_page_link . '">' . $property_address . '</a>';

                    $comment_user_data = UserData($comment_user_id);
                    $comment_user_name = $comment_user_data[0]['user_name'];
                    $comment_last_name = $comment_user_data[0]['last_name'];
                    $comment_email = $comment_user_data[0]['email'];

                    $reply_user_data = UserData($user_id);

                    $profile_slug = $reply_user_data[0]['profile_slug'];

                    $comment_profile_link = site_url('user/' . $profile_slug);

                    $reply_user_name = $reply_user_data[0]['user_name'] . ' ' . $reply_user_data[0]['last_name'];
                    $comment_name_anchor = '<a href="' . $comment_profile_link . '">' . $reply_user_name . '</a>';

                    ///////// add latest activities
                   // project_activity('commented', $data1['user_id'], $comment_user_id, $property_id);
                    ///////// end

                    if ($user_not_own != '0') {
                        if ($user_not_own->comment_reply_alert == '1') {
                             $language_id=GetUserLangCode($comment_user_data[0]['user_id']);
                            $email_template = $this->db->query("select * from `email_template` where task='You have reply on your comment on'  and language_id=".$language_id);
                            $email_temp = $email_template->row();
                            $email_message = $email_temp->message;
                            $email_subject = $email_temp->subject;
                            $email_subject = str_replace('{company_name}', $property_address, $email_subject);
                            $email_subject = str_replace('{comment}', $taxo_comments, $email_subject);
                            $email_address_from = $email_temp->from_address;
                            $email_address_reply = $email_temp->reply_address;
                            $email_to = $comment_email;

                            $email_message = str_replace('{break}', '<br/>', $email_message);
                            $email_message = str_replace('{user_name}', $comment_user_name, $email_message);
                            $email_message = str_replace('{last_name}', $comment_last_name, $email_message);
                            $email_message = str_replace('{company_name}', $property_title_anchor, $email_message);
                            $email_message = str_replace('{comment_user_name}', $comment_name_anchor, $email_message);
                            $email_message = str_replace('{comments}', $comment, $email_message);
                            $email_message = str_replace('{comment}', $taxo_comments, $email_message);
                            $email_message = str_replace('{project_name}', $project_name, $email_message);
                            $email_message = str_replace('{comment_full_name}', $reply_user_name, $email_message);
                            $email_message = str_replace('{equity_page_link}', $property_page_link, $email_message);
                            $email_message = str_replace('{site_name}', $site_name, $email_message);

                            $str = $email_message;

                            email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);
                        }
                    }

                }
            }
            if ($type == 'spam') {
                $data1['comment_spam'] = $this->property_model->commentaction($comment_id, $type, $property_id, 3, '', $comment_user_ip, '', '');
                $data["msg"]["success"] = '<span>' . RECORD_SPAM_SUCCESSFULLY . '</span>';
            }
            if ($type == 'delete') {
                $data1['comment_delete'] = $this->property_model->commentaction($comment_id, $type, 0, 0, '', '', '', '');
                $data["msg"]["success"] = '<span>' . RECORD_DELETED_SUCCESSFULLY . '</span>';
            }
        }
        /* commment is approved , delete or declined so delete cache*/
        property_other_deletecache('comment',$property_id);
        echo json_encode($data);
    }


     /*
    Function name :ajax_updatecomments()
    Parameter :none
    Return : none
    Use : Update comments data .
    */
    function ajax_updatecomments()
    {
        $property_id = SecurePostData($this->input->post('property_id'));
        $user_id = check_user_authentication(true);
        // if($property_id > 0)
        // {
        //     if (!$this->property_model->is_property_owner($property_id, false)) {
        //         redirect('home/my_campaign');
        //     }
        // }
        $data['is_property_owner'] = $this->property_model->is_property_owner($property_id, false);
        $data['site_setting'] = site_setting();
        $data["msg"]['type'] = 'comment_validation';
        $property_id = SecurePostData($this->input->post('property_id'));
        $data['property_comments'] = $this->account_model->GetUpdateCommentGallery(0, $property_id, '', $join = array(
            'comment',
            'user'
        ), $limit = 0, $group = array(
            'comment_id' => 'comment.comment_id'
        ), $order = array(
            'comment.comment_id' => 'desc'
        ), '');
        $this->load->view('default/property/ajax_updatedashboardcomments', $data);
    }


    /*
        Function name :add_property()
        Parameter :id 
        Return : none
        Use : User can add new property.
        */
    function add_property($id = '')
    {
   
        $user_id = check_user_authentication(true);

        $data = array();
        if($this->input->post('property_id') > 0 && $this->input->post('property_id') != '')
        {
            $id = $this->input->post('property_id');
            $data['id']=$id;
        }
        else
        {
            $data['id']=$id;        
        }

        if($id > 0)
        {
            if (!$this->property_model->is_property_creator($id, false)) {
                redirect('home/notedit_property');
            }
        }
       

        $meta = meta_setting();
        $data['site_setting'] = site_setting();

        $data['types']= $this->property_model->getTypes(1);
        $data['sub_types']= $this->property_model->getTypes();

        $country_data = $this->property_model->getTableData('country',array('active' => 1));
        $data['country_data'] = $country_data;

        $property = GetOneProperty($id);
        $status = $property['status'];
        $status_check = validatestatus_isedit($status);
        $data['status_check'] = $status_check;

        $data['property_address'] = $property['property_address'];
        $data['property_url'] = $property['property_url'];
        $data['property_description'] = SecureShowData($property['property_description']);
        $data['property_type'] = $property['property_type'];
        $data['property_sub_type'] = $property['property_sub_type'];
        $data['bedrooms'] = $property['bedrooms'];
        $data['bathrooms'] = $property['bathrooms'];
        $data['cars'] = $property['cars'];
        $data['min_property_investment_range'] = $property['min_property_investment_range'];
        $data['max_property_investment_range'] = $property['max_property_investment_range'];
        $data['property_size'] = $property['property_size'];
        $data['lotsize'] = $property['lotsize'];
        $data['year_built'] = $property['year_built'];
        $data['property_postcode'] = $property['property_postcode'];
        $data['country_id'] = $property['country_id'];
        $data['property_suburb'] = $property['property_suburb'];
        $data['cover_image'] = $property['cover_image'];

        $data['year_error'] = '';
        $data['check_url'] = '';
         $year_error = '';
        $cover_image_error = '';
        $data['cover_image_error'] = '';
        if ($_POST) 
        {

            $property_url = makeSlugs($this->input->post('property_url'));
            $check_url = '';
            if($id > 0)
            {
                $chk_url_exists = $this->db->query("select MAX(property_url) as property_url from property where property_url like '" . $property_url . "%' and property_id !=" . $id );
                $fetch_url_data = $chk_url_exists->row_array();

                

                if ($fetch_url_data['property_url'] == $property_url) {
                    $check_url = URL_ALREADY_EXISTS;
                }
            }
            

        
            if ($this->input->post('year_built') != '') {
              

                $year1 = date('Y');

                $diff = $this->input->post('year_built') - $year1;
                if ($diff > 0) {
                    $year_error = YEAR_FOUNDED_CANNOT_BE;
                }

            }

           
            if ($property['cover_image'] == '') {

                $cover_image_error = THE_COMPANY_COVER_FIEID_IS_REQUIRED;

            }


            $this->load->library('form_validation');
            $this->form_validation->set_rules('property_address','Property Address', 'required|trim');
            $this->form_validation->set_rules('property_url', 'Estate Crowd url', 'required|trim');
            $this->form_validation->set_rules('property_description', 'Property Desctiption', 'required|trim');
            $this->form_validation->set_rules('year_built', 'Year Built', 'required|greater_than[1969]|less_than[date("Y")]');
            $this->form_validation->set_rules('bedrooms', 'Bedrooms', 'required|trim');
            $this->form_validation->set_rules('bathrooms', 'Bathrooms', 'required|trim');
            $this->form_validation->set_rules('cars', 'Cars', 'required|trim');
            $this->form_validation->set_rules('min_property_investment_range', 'Min Investment Range', 'required|trim');
            $this->form_validation->set_rules('max_property_investment_range', 'Max Investment Range', 'required|trim');
            $this->form_validation->set_rules('property_size', 'Propery Size', 'required|trim');
            $this->form_validation->set_rules('lotsize', 'Lot Size', 'required|trim');
            $this->form_validation->set_rules('property_postcode', 'Postcode', 'required|trim');
            $this->form_validation->set_rules('property_suburb', 'Suburb', 'required|trim');
            $this->form_validation->set_rules('country_id', 'Country', 'required|trim');

            if ($this->form_validation->run() === FALSE || $check_url != '' || $cover_image_error != '')
            {
                 $data['check_url'] = $check_url;
                if (form_error('year_founded') != '') {
                    $data['year_error'] = form_error('year_built');
                } else {
                    $data['year_error'] = $year_error;
                }
                 $data['cover_image_error'] = $cover_image_error;
                $data['property_address'] = $this->input->post('property_address');
                $data['property_url'] = $property_url;
                $data['property_description'] = $this->input->post('property_description');
                $data['year_built'] = $this->input->post('year_built');
                $data['bedrooms'] = $this->input->post('bedrooms');
                $data['bathrooms'] = $this->input->post('bathrooms');
                $data['cars'] = $this->input->post('cars');
                $data['min_property_investment_range'] = $this->input->post('min_property_investment_range');

                $data['max_property_investment_range'] = $this->input->post('max_property_investment_range');
                $data['property_size'] = $this->input->post('property_size');
                $data['lotsize'] = $this->input->post('lotsize');
                $data['property_postcode'] = $this->input->post('property_postcode');
                $data['property_suburb'] = $this->input->post('property_suburb');
                $data['country_id'] = $this->input->post('country_id');
                $data['property_type'] = $this->input->post('property_type');
                $data['property_sub_type'] = $this->input->post('property_sub_type');

            }
            else
            {

                    //update data 
                if($status_check)
                {

                     $property_data_array = array(
                        'user_id' => $this->session->userdata('user_id'),
                       
                    );
                }
                else
                {
                    $property_data_array = array(
                        'user_id' => $this->session->userdata('user_id'),
                        'property_address' => SecurePostData($this->input->post('property_address')),
                        'property_description' => SecurePostData($this->input->post('property_description')),
                        'property_url' => $property_url,
                        'year_built' => SecurePostData($this->input->post('year_built')),
                        'bedrooms' => SecurePostData($this->input->post('bedrooms')),
                        'bathrooms' => SecurePostData($this->input->post('bathrooms')),
                        'cars' => SecurePostData($this->input->post('cars')),
                        'min_property_investment_range' => SecurePostData($this->input->post('min_property_investment_range')),

                        'max_property_investment_range' => SecurePostData($this->input->post('max_property_investment_range')),
                        'property_size' => SecurePostData($this->input->post('property_size')),
                        'lotsize' => SecurePostData($this->input->post('lotsize')),
                        'property_postcode' => SecurePostData($this->input->post('property_postcode')),
                        'property_sub_type' => SecurePostData($this->input->post('property_sub_type')),
                        'property_type' => SecurePostData($this->input->post('property_type')),
                        'property_suburb' => SecurePostData($this->input->post('property_suburb')),
                        'country_id' => SecurePostData($this->input->post('country_id')),
                        'date_added' => date('Y-m-d H:i:s'),
                        'host_ip' => $_SERVER['REMOTE_ADDR'],
                        'status' => 0

                       
                    );

                }

                if ($id > 0) 
                {
                    $this->property_model->AddInsertUpdateTable('property', 'property_id', $id, $property_data_array);
                } 
                else 
                {
                    $this->property_model->AddInsertUpdateTable('property', '','', $property_data_array);
                    $property_id = $this->db->insert_id();
                     project_activity('draft_project', $this->session->userdata('user_id'), '', $property_id);

                }
                
                redirect('property/investment_summary/' . $id);


            }
         
        }

        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/property/add_property', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
    }
    /*
    Function name :cover_image_ajax()
    Parameter :id 
    Return : Image name
    Use : Cover Photo upload using ajax;
    */
    function cover_image_ajax($id = null)
    {
        $site_setting = site_setting();
        $this->load->library('form_validation');
        $data = array();
        $data["msg"]["error"] = '';
        $data["msg"]["success"] = '';
        $data["redirect"]["url"] = '';
        $data["image"]["path"] = '';

        if ($this->session->userdata('user_id') == '') {
            $data["redirect"]["url"] = 'home/login';
        } else {


            $_FILES['userfile']['name'] = $_FILES['cover_image']['name'];
            $_FILES['userfile']['type'] = $_FILES['cover_image']['type'];
            $_FILES['userfile']['tmp_name'] = $_FILES['cover_image']['tmp_name'];
            $image_info = getimagesize($_FILES['cover_image']['tmp_name']);
            $image_width = $image_info[0];
            $image_height = $image_info[1];
            $_FILES['userfile']['error'] = $_FILES['cover_image']['error'];
            $_FILES['userfile']['size'] = $_FILES['cover_image']['size'];
            $_FILES['userfile']['max_width'] = $image_width;
            $_FILES['userfile']['max_height'] = $image_height;
            // image validation
            $image_settings = get_image_setting_data();

            if ($_FILES["userfile"]["type"] != "image/jpeg" and $_FILES["userfile"]["type"] != "image/pjpeg" and $_FILES["userfile"]["type"] != "image/png" and $_FILES["userfile"]["type"] != "image/x-png" and $_FILES["userfile"]["type"] != "image/gif") {
                $data["msg"]["error"] = PLEASE_UPLOAD_A_JPG_PNG_GIF_FILE;
            } else if ($_FILES["userfile"]["size"] > $image_settings['upload_limit']*1000000) {
                $data["msg"]["error"] = sprintf(SORRY_THIS_FILE_IS_TOO_LARGE_PLEASE_SELECT_A_JPG_FILE_THAT_IS_LESS_THAN_MB_OR_TRY_RESIZING_IT_USING_A_PHOTO_EDITOR,$image_settings['upload_limit']);
            } else if ($_FILES['userfile']['max_width'] < 800) {
                $data["msg"]["error"] = IMAGE_MUST_BE_GREATER_THAN;
            } else if ($_FILES['userfile']['max_height'] < 450) {
                $data["msg"]["error"] = IMAGE_MUST_BE_GREATER_THAN;
            } else {

                $imagename = ImageUpload($_FILES, false, false);
                $data_gallery = array(
                    'cover_image' => $imagename,
                    'user_id' => $this->session->userdata('user_id')
                );
                // save data
                if($id > 0 && $id != '' )
                {
                     $new_cover_photo = $this->property_model->AddInsertUpdateTable('property', 'property_id', $id, $data_gallery);
                     $property_id = $id;
                }
                else
                {
                     $new_cover_photo = $this->property_model->AddInsertUpdateTable('property', 
                        '', '', $data_gallery);
                     $property_id = $new_cover_photo;
                   
                }


               

                if ($property_id > 0) {
                    $data["image"]["path"] = base_url() . "upload/property/small/" . $imagename;
                    $data["msg"]["success"] = 'success';
                    $data["msg"]["property_id"] = $property_id;
                } else {
                    $data["msg"]["error"] = THERE_IS_SOME_PROBLEM_SITE_TRY_AFTER_SOMETIME;
                }
            }
            echo json_encode($data);
            die;


        }

    }

      /*
    Function name :search_auto()
    Parameter :text ,id
    Return : none
    Use : use for auto search
    */
    function search_auto($text = '', $id = '')
    {
        $data = array();
        $offset = 0;
        $limit = 3;
        $keyword = SecurePostData($text);
        $search_criteria = array(
            'investment_suburb' => $keyword
        );
        $data['search_criteria'] = $search_criteria;
        $search_suburbs = $this->home_model->GetAllSearchSuburbs($search_criteria);
        // var_dump($search_equities);
        if ($search_suburbs) {
            $str = '<ul id="srhdiv">';
            foreach ($search_suburbs as $search_suburb) {
                $str .= '<li onclick="selecttext_suburb(this);">' . $search_suburb['suburb'] . ' , '.$search_suburb['state'].'</li>';
            }
            $str .= '</ul>';
            echo $str;
            die();
        }
        die;
    }

    /*
    Function name :search_auto_postcode()
    Parameter :text ,id
    Return : none
    Use : search post code
    */
    function search_auto_postcode($text = '', $id = '')
    {
        $data = array();
        $offset = 0;
        $limit = 3;
        $keyword = SecurePostData($text);
        $search_criteria = array(
            'postcode' => $keyword
        );
        $data['search_criteria'] = $search_criteria;
        $search_suburbs = $this->home_model->GetAllSearchSuburbs($search_criteria,'',array(),'yes');
        // var_dump($search_equities);
        if ($search_suburbs) {
            $str = '<ul id="srhdiv_postcode">';
            foreach ($search_suburbs as $search_suburb) {
                $str .= '<li onclick="selecttext(this);">' . $search_suburb['postcode'] . '</li>';
            }
            $str .= '</ul>';
            echo $str;
            die();
        }
        die;
    }

     /*
        Function name :investment_summary()
        Parameter :id 
        Return : none
        Use : User can add new property.
        */
    function investment_summary($id = '')
    {
        $user_id = check_user_authentication(true);
        if ($id > 0) 
        {
              if (!$this->property_model->is_property_creator($id, false))
            {
                redirect('home/notedit_property');
            }
       
        }
        else
        {
            redirect('property/add_property');
        }

        $data = array();
        $data['id']=$id;
        $meta = meta_setting();
        $data['site_setting'] = site_setting();

        $property = GetOneProperty($id);
        $status = $property['status'];
        $status_check = validatestatus_isedit($status);
        $data['status_check'] = $status_check;

        $data['investment_summary'] = $property['investment_summary'];
     
        if ($_POST) 
        {

            
            $this->load->library('form_validation');
           
            $this->form_validation->set_rules('investment_summary', 'Investment Summary', 'required|trim');
       

            if ($this->form_validation->run() === FALSE)
            {
                

                $data['investment_summary'] = $this->input->post('investment_summary');
               

            }
            else
            {

                    //update data 
                if($status_check)
                {

                     $property_data_array = array(
                        'user_id' => $this->session->userdata('user_id'),
                       
                    );
                }
                else
                {
                    $property_data_array = array(
                        'user_id' => $this->session->userdata('user_id'),
                        'investment_summary' => $this->input->post('investment_summary'),
                        
                       
                    );

                }

                if ($id > 0) 
                {
                    $this->property_model->AddInsertUpdateTable('property', 'property_id', $id, $property_data_array);
                } 
                else 
                {
                    $this->property_model->AddInsertUpdateTable('property', '','', $property_data_array);
                  
                   
                }
                redirect('property/gallery/' . $id);


            }
         
        }

        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/property/investment_summary', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
    }

     /*
        Function name :gallery()
        Parameter :id 
        Return : none
        Use : User can add new property.
        */
    function gallery($id = '')
    {
        $user_id = check_user_authentication(true);
        if ($id > 0) {
             if (!$this->property_model->is_property_creator($id, false)) 
             {
                redirect('home/notedit_property');
            }
        }
         else
        {
            redirect('property/add_property');
        }
        $data = array();
        $data['id']=$id;
        $meta = meta_setting();
        $data['site_setting'] = site_setting();

        $property = GetOneProperty($id);
        $status = $property['status'];
        $status_check = validatestatus_isedit($status);
        $data['status_check'] = $status_check;

        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/property/gallery', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
    }


      /*
    Function name :video_save()
    Parameter :none;
    Return : Array for video with property
    Use :video for property
    Auther:Rakesh
    */
    function video_save()
    {
        $data = array();
        $data["error"] = false;
        $data["msg"] = '';
        $data["url"] = '';
        $data['media_video_title_error'] = '';
        $data['media_video_name_error'] = '';
        $data['media_video_desc_error'] = '';
        $property_id = SecurePostData($this->input->post('property_id'));
      
        if ($property_id > 0) {
            if (!$this->property_model->is_property_creator($property_id, false)) {
                redirect('home/notedit_property');
            }
        }
        $video_id = SecurePostData($this->input->post('video_id'));
        $media_video_title = SecurePostData($this->input->post('media_video_title'));
        $media_video_name = SecurePostData($this->input->post('media_video_name'));
        $media_video_desc = stripslashes($this->input->post('media_video_desc'));
        $media_video_status = SecurePostData($this->input->post('media_video_status'));

        $this->load->library('form_validation');
        $this->form_validation->set_rules('media_video_title', TITLE, 'required|trim|min_length[3]|max_length[70]');
        $this->form_validation->set_rules('media_video_name', URL, 'required|trim');
        $this->form_validation->set_rules('media_video_desc', DESCRIPTION, 'at_least_one_letter');

        if ($this->session->userdata('user_id') == '') {
            $data["url"] = 'home/login';
            $data["error"] = true;
            echo json_encode($data);
            die;
        } else if ($property_id == null || $property_id == '' || $property_id == '0') {
            $data["url"] = "property/add_property";
            $data["error"] = true;
            echo json_encode($data);
            die;
        }
        //   Check validations
        if ($this->form_validation->run($this) == FALSE) {

            $data['media_video_title_error'] = form_error('media_video_title');
            $data['media_video_name_error'] = form_error('media_video_name');
             $data['media_video_desc_error'] = form_error('media_video_desc');

            $data["error"] = true;
            echo json_encode($data);
            die;


        }

        $url = $media_video_name;
        $video_image = '';
        //
        if (substr_count($url, 'youtube') > 0) {
            $url = str_replace('youtu.be', 'www.youtube.com/v', $url);
            $url = str_replace(array(
                "v=",
                "v/",
                "vi/"
            ), "v=", $url);
            preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $url, $matches);
            if (isset($matches[0])) {
                $item = $this->parse_youtube_url('http://www.youtube.com/embed/' . $matches[0], 'hqthumb');
                $video_image = $item;
            }
        } elseif (substr_count($url, 'youtube') > 0) {
            $url = str_replace(array(
                "v=",
                "v/",
                "vi/"
            ), "v=", $url);
            preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $url, $matches);
            if (isset($matches[0])) {
                $item = $this->parse_youtube_url('http://www.youtube.com/embed/' . $matches[0], 'hqthumb');
                $video_image = $item;
            }
        } elseif (substr_count($url, 'vimeo') > 0) {
            $vid_code = explode("/", $url);
            $vid = $vid_code[count($vid_code) - 1];
            $item = $this->getVimeoInfo($vid, 'thumbnail_large');
            $video_image = $item;
        } else {
            $data['media_video_name_error'] = INVALID_VIDEO_URL;
            $data["error"] = true;
            echo json_encode($data);
            die;
        }
      if($video_image==''){
          $data['media_video_name_error'] = INVALID_VIDEO_URL;
             $data["error"] = true;
              echo json_encode($data);
             die;

        }
        $data["image"]["media_url"] = $video_image;
        $data_array = array('media_video_title' => $media_video_title, 'media_video_name' => $media_video_name, 'media_video_desc' =>
            $media_video_desc, 'status' => $media_video_status, 'property_id' => $property_id);
        if ($video_image != '') {
            $data_array = array('media_video_title' => $media_video_title, 'media_video_name' => $media_video_name, 'media_video_desc' =>
                $media_video_desc, 'status' => $media_video_status, 'property_id' => $property_id, 'image' => $video_image);


        }
        

        if ($video_id > 0) $property_video_id = $this->property_model->AddInsertUpdateTable('video_gallery', 'id', $video_id, $data_array);
        else $property_video_id = $this->property_model->AddInsertUpdateTable('video_gallery', '', '', $data_array);
       
         property_other_deletecache('video',$property_id);
        echo json_encode($data);


    }
    
    /*
    Function name :parse_youtube_url()
    Parameter :url ,url,width,height
    Return : none
    Use : Parse the youtube URL and return iframe or embded code    */
    // return image from youtube url
    function parse_youtube_url($url, $return = 'embed', $width = '', $height = '', $rel = 0)
    {
        $v = '';
        $urls = parse_url($url);
        if (isset($urls['host'])) {
            // url is http://youtu.be/xxxx
            if ($urls['host'] == 'youtu.be') {
                $id = ltrim($urls['path'], '/');
            } // url is http://www.youtube.com/embed/xxxx
            else if (strpos($urls['path'], 'embed') == 1) {
                $arr=explode('/', $urls['path']);
                $id = end($arr);
            } // url is xxxx only
            else if (strpos($url, '/') === false) {
                $id = $url;
            }
            // http://www.youtube.com/watch?feature=player_embedded&v=m-t4pcO99gI
            // url is http://www.youtube.com/watch?v=xxxx
            else {
                parse_str($urls['query']);
                $id = $v;
                if (!empty($feature)) {
                    $id = end(explode('v=', $urls['query']));
                }
            }
            // return embed iframe
            if ($return == 'embed') {
                return '<iframe width="' . ($width ? $width : 560) . '" height="' . ($height ? $height : 349) . '" src="http://www.youtube.com/embed/' . $id . '?rel=' . $rel . '" frameborder="0" allowfullscreen></iframe>';
            } // return normal thumb
            else if ($return == 'thumb') {
                return 'http://i1.ytimg.com/vi/' . $id . '/default.jpg';
            } // return hqthumb
            else if ($return == 'hqthumb') {
                return 'http://i1.ytimg.com/vi/' . $id . '/hqdefault.jpg';
            } // else return id
            else {
                return $id;
            }
        } ///===isset
        return false;
    }

    /*
    Function name :list_video()
    Parameter :none;
    Return : Result array
    Use : Show Video list for property ;
    Auther:Rakesh
    */
    function list_video()
    {

        $data = array();
        $data["error"] = false;
        $data["msg"] = '';
        $data["url"] = '';
        $property_id = SecurePostData($this->input->post('property_id'));
        if ($property_id > 0) {
            if (!$this->property_model->is_property_creator($property_id, false)) {
                redirect('home/notedit_property');
            }
        }

        if ($this->session->userdata('user_id') == '') {
            $data["url"] = 'home/login';
            $data["error"] = true;
            echo json_encode($data);
            die;
        } else if ($property_id == null || $property_id == '' || $property_id == '0') {
            $data["url"] = "property/add_property";
            $data["error"] = true;
            echo json_encode($data);
            die;
        }

        //check user select value from dropdown
        if ($property_id == null || $property_id == '' || $property_id == '0') {
            $data["msg"] = INVALID_DATA;
            $data["error"] = true;
            echo json_encode($data);
            die;
        }
        $video_data = $this->property_model->property_video_gallery_data($property_id);
        $data["video_data"] = $video_data;


        $this->load->view('default/property/list_video', $data);
    }

    /*
    Function name :delete_video()
    Parameter :none;
    Return : none
    Use : Delete Video for property ;
    Auther:Rakesh
    */
    function delete_video()
    {
        $data = array();
        $data["error"] = false;
        $data["msg"] = '';
        $data["url"] = '';
        $property_id = SecurePostData($this->input->post('property_id'));
        if ($property_id > 0) {
            if (!$this->property_model->is_property_creator($property_id, false)) {
                redirect('home/notedit_property');
            }
        }
        $video_id = SecurePostData($this->input->post('video_id'));

        if ($this->session->userdata('user_id') == '') {
            $data["url"] = 'home/login';
            $data["error"] = true;
            echo json_encode($data);
            die;
        } else if ($property_id == null || $property_id == '' || $property_id == '0') {
            $data["url"] = "property/add_property";
            $data["error"] = true;
            echo json_encode($data);
            die;
        }

        //check user select value from dropdown
        if ($video_id == null || $video_id == '' || $video_id == '0') {
            $data["msg"] = PLEASE_CLICK_ON_VIDEO_TO_DELETE;
            $data["error"] = true;
            echo json_encode($data);
            die;
        }
        $sql = "delete from video_gallery where id='" . $video_id . "' ";
        $this->db->query($sql);
         property_other_deletecache('video',$property_id);

        echo json_encode($data);
    }

    /*
    Function name :edit_video()
    Parameter :none;
    Return : Result array
    Use : Show video for property ;
    Auther:Rakesh
    */
    function edit_video()
    {

        $data = array();
        $data["error"] = false;
        $data["msg"] = '';
        $data["url"] = '';
        $property_id = SecurePostData($this->input->post('property_id'));
        if ($property_id > 0) {
            if (!$this->property_model->is_property_creator($property_id, false)) {
                redirect('home/notedit_property');
            }
        }
        $video_id = SecurePostData($this->input->post('video_id'));

        if ($this->session->userdata('user_id') == '') {
            $data["url"] = 'home/login';
            $data["error"] = true;
            echo json_encode($data);
            die;
        } else if ($property_id == null || $property_id == '' || $property_id == '0') {
            $data["url"] = "property/add_property";
            $data["error"] = true;
            echo json_encode($data);
            die;
        }

        //check user select value from dropdown
        if ($property_id == null || $property_id == '' || $property_id == '0') {
            $data["msg"] = INVALID_DATA;
            $data["error"] = true;
            echo json_encode($data);
            die;
        }
        //check user select value from dropdown
        if ($video_id == null || $video_id == '' || $video_id == '0') {
            $data["msg"] = PLEASE_CLICK_ON_VIDEO_TO_EDIT;
            $data["error"] = true;
            echo json_encode($data);
            die;
        }
        $video_data = $this->property_model->property_video_gallery_data($property_id, $video_id);
        $data["video"] = $video_data[0];
        echo json_encode($data);
        die;

    }

    /*
    Function name :image_save()
    Parameter :none;
    Return : Array for Image  with property
    Use :Image for property
    Auther:Rakesh
    */
    function image_save()
    {


        $data = array();
        $data["error"] = false;
        $data["msg"] = '';
        $data["url"] = '';
        $data['image_name_error'] = '';
        $data['image_desc_error'] = '';
        $property_id = SecurePostData($this->input->post('property_id'));
        if ($property_id > 0) {
            if (!$this->property_model->is_property_creator($property_id, false)) {
                redirect('home/notedit_property');
            }
        }
        $image_id = SecurePostData($this->input->post('image_id'));
        $image_name = SecurePostData($this->input->post('image_name'));
        $image_desc = stripslashes($this->input->post('image_desc'));
        $image_status = SecurePostData($this->input->post('image_status'));

        $this->load->library('form_validation');
        $this->form_validation->set_rules('image_name', TITLE, 'required|trim|at_least_one_letter');
        $this->form_validation->set_rules('image_desc', DESCRIPTION, 'at_least_one_letter');
        //$this->form_validation->set_rules('image', IMAGE, 'required|trim');


        if ($this->session->userdata('user_id') == '') {
            $data["url"] = 'home/login';
            $data["error"] = true;
            echo json_encode($data);
            die;
        } else if ($property_id == null || $property_id == '' || $property_id == '0') {
            $data["url"] = "property/add_property";
            $data["error"] = true;
            echo json_encode($data);
            die;
        }
        //   Check validations
        if ($this->form_validation->run($this) == FALSE) {
            $data['image_name_error'] = form_error('image_name');
            $data['image_desc_error'] = form_error('image_desc');
            //$data['image_error'] = form_error('image');
            if (isset($_FILES['image']['name'])) {
                $data['image_error'] = "";

            } else {
                $image_id_array = array("property_gallery_id" => $image_id);
                $image_data = $this->property_model->getTableData('property_gallery', $image_id_array);
                $images = $image_data[0];
                if ($images['image']) {
                    $data['image_error'] = "";
                } else {
                    $data['image_error'] = "Image is required";
                }


            }
            $data["error"] = true;
            echo json_encode($data);
            die;


        }

        $imagename = '';
        if (isset($_FILES['image']['name'])) {


            $_FILES['userfile']['name'] = $_FILES['image']['name'];
            $_FILES['userfile']['type'] = $_FILES['image']['type'];
            $_FILES['userfile']['tmp_name'] = $_FILES['image']['tmp_name'];
            $image_info = getimagesize($_FILES['image']['tmp_name']);
            $image_width = $image_info[0];
            $image_height = $image_info[1];
            $_FILES['userfile']['error'] = $_FILES['image']['error'];
            $_FILES['userfile']['size'] = $_FILES['image']['size'];
            $_FILES['userfile']['max_width'] = $image_width;
            $_FILES['userfile']['max_height'] = $image_height;
            // image validation
            $image_settings = get_image_setting_data();
            if ($_FILES["userfile"]["type"] != "image/jpeg" and $_FILES["userfile"]["type"] != "image/pjpeg" and $_FILES["userfile"]["type"] != "image/png" and $_FILES["userfile"]["type"] != "image/x-png" and $_FILES["userfile"]["type"] != "image/gif") {
                $data["image_error"] = PLEASE_UPLOAD_A_JPG_PNG_GIF_FILE;
                $data["error"] = true;
                echo json_encode($data);
                die;
            } else if ($_FILES["userfile"]["size"] > $image_settings['upload_limit']*1000000) {
                $data["image_error"] = sprintf(SORRY_THIS_FILE_IS_TOO_LARGE_PLEASE_SELECT_A_JPG_FILE_THAT_IS_LESS_THAN_MB_OR_TRY_RESIZING_IT_USING_A_PHOTO_EDITOR,$image_settings['upload_limit']);
                $data["error"] = true;
                echo json_encode($data);
                die;
            } else {


                $files = $_FILES;
                $base_path = $this->config->slash_item('base_path');
              
                $rand = rand(0, 100000);
                       
                $path_parts = pathinfo($_FILES['image']['name']);
                $date = new DateTime();
                
                $file_extention = $path_parts['extension'];
                $new_img = $rand .'-image-'.$date->getTimestamp().'.'.$file_extention;
               

                move_uploaded_file($files["userfile"]["tmp_name"], $base_path . "upload/gallery/" . $new_img);
                $imagename = $new_img;

                $data["image"]["image_name"] = $imagename;
                $data["image"]["path"] = anchor(base_url() . "upload/gallery/" . $imagename, $imagename);


            }
        }
        $data_array = array('image_name' => $image_name, 'image_desc' => $image_desc, 'status' => $image_status, 'property_id' => $property_id);
        if ($imagename != '') {
            $data_array = array('image_name' => $image_name, 'image_desc' => $image_desc, 'status' => $image_status, 'property_id' => $property_id, 'image' => $imagename);


        }

        if ($image_id > 0) $property_gallery_id = $this->property_model->AddInsertUpdateTable('property_gallery', 'property_gallery_id', $image_id, $data_array);
        else $property_gallery_id = $this->property_model->AddInsertUpdateTable('property_gallery', '', '', $data_array);


        property_other_deletecache('image',$property_id);

        echo json_encode($data);


    }

    /*
    Function name :list_image()
    Parameter :none;
    Return : Result array
    Use : Show Image list for property ;
    Auther:Rakesh
    */
    function list_image()
    {
        $data = array();
        $data["error"] = false;
        $data["msg"] = '';
        $data["url"] = '';
        $property_id = SecurePostData($this->input->post('property_id'));
        if ($property_id > 0) {
            if (!$this->property_model->is_property_creator($property_id, false)) {
                redirect('home/notedit_property');
            }
        }

        if ($this->session->userdata('user_id') == '') {
            $data["url"] = 'home/login';
            $data["error"] = true;
            echo json_encode($data);
            die;
        } else if ($property_id == null || $property_id == '' || $property_id == '0') {
            $data["url"] = "property/add_property";
            $data["error"] = true;
            echo json_encode($data);
            die;
        }

        //check user select value from dropdown
        if ($property_id == null || $property_id == '' || $property_id == '0') {
            $data["msg"] = INVALID_DATA;
            $data["error"] = true;
            echo json_encode($data);
            die;
        }
        $image_data = $this->property_model->property_image_gallery_data($property_id);
        $data["image_data"] = $image_data;
        $this->load->view('default/property/list_image', $data);
    }

    /*
    Function name :delete_image()
    Parameter :none;
    Return : none
    Use : Delete Image for property ;
    Auther:Rakesh
    */
    function delete_image()
    {
        $data = array();
        $data["error"] = false;
        $data["msg"] = '';
        $data["url"] = '';
        $property_id = SecurePostData($this->input->post('property_id'));
        if ($property_id > 0) {
            if (!$this->property_model->is_property_creator($property_id, false)) {
                redirect('home/notedit_property');
            }
        }
        $image_id = SecurePostData($this->input->post('image_id'));

        if ($this->session->userdata('user_id') == '') {
            $data["url"] = 'home/login';
            $data["error"] = true;
            echo json_encode($data);
            die;
        } else if ($property_id == null || $property_id == '' || $property_id == '0') {
            $data["url"] = "property/add_property";
            $data["error"] = true;
            echo json_encode($data);
            die;
        }

        //check user select value from dropdown
        if ($image_id == null || $image_id == '' || $image_id == '0') {
            $data["msg"] = PLEASE_CLICK_ON_IMAGE_TO_DELETE;
            $data["error"] = true;
            echo json_encode($data);
            die;
        }
        $sql = "delete from property_gallery where property_gallery_id='" . $image_id . "' ";
        $this->db->query($sql);

        property_other_deletecache('image',$property_id);

        echo json_encode($data);
    }

    /*
    Function name :edit_image()
    Parameter :none;
    Return : Result array
    Use : Show Image for property ;
    Auther:Rakesh
    */
    function edit_image()
    {
        $data = array();
        $data["error"] = false;
        $data["msg"] = '';
        $data["url"] = '';
        $property_id = SecurePostData($this->input->post('property_id'));
        if ($property_id > 0) {
            if (!$this->property_model->is_property_creator($property_id, false)) {
                redirect('home/notedit_property');
            }
        }
        $image_id = SecurePostData($this->input->post('image_id'));

        if ($this->session->userdata('user_id') == '') {
            $data["url"] = 'home/login';
            $data["error"] = true;
            echo json_encode($data);
            die;
        } else if ($property_id == null || $property_id == '' || $property_id == '0') {
            $data["url"] = "property/add_property";
            $data["error"] = true;
            echo json_encode($data);
            die;
        }

        //check user select value from dropdown
        if ($property_id == null || $property_id == '' || $property_id == '0') {
            $data["msg"] = 'Invalid data';
            $data["error"] = true;
            echo json_encode($data);
            die;
        }
        //check user select value from dropdown
        if ($image_id == null || $image_id == '' || $image_id == '0') {
            $data["msg"] = PLEASE_CLICK_ON_IMAGE_TO_EDIT;
            $data["error"] = true;
            echo json_encode($data);
            die;
        }
        $image_data = $this->property_model->property_image_gallery_data($property_id, $image_id);
        $data["image"] = $image_data[0];
        echo json_encode($data);
        die;

    }

    /*
    Function name :launch_property()
    Parameter :id 
    Return : none
    Use :Launch Equity
    */
    function launch_property($id = null)
    {
        $user_id = check_user_authentication(true);
        if ($id > 0) {
            if (!$this->property_model->is_property_creator($id)) {
                redirect('home/notedit_project');
            }
        }
        $this->db->query("update property set status=1 where property_id='" . $id . "'");
        $property = GetOneProperty($id);

        //start count latest activities 
        project_activity('submit_project', $user_id, '', $id);

        if ($this->session->userdata('user_id') != $property['user_id']) {
            redirect('home');
        }


        redirect('property/thankyou/' . $user_id . '/' . $id);
    } 

     /*
    Function name :thankyou()
    Parameter :none
    Return : none
    Use : When user confirm property create then he/she can see thank you page
    */

    function thankyou($user_id = '', $property_id = 0)
    {
        $user_id = check_user_authentication(true);
        $site_setting = site_setting();
        $data['site_setting'] = $site_setting;


        $get_property_user_detail = $this->property_model->GetAllProperties(0, $property_id);
        $property = $get_property_user_detail[0];


        if ($user_id <= 0) $user_id = $property['user_id'];
        $user_detail = $this->account_model->GetAllUsers($user_id);
        $user_detail = $user_detail[0];
        $data['email'] = $property['email'];
        $data['user_email'] = $user_detail['email'];
        $property_address = SecureShowData($property['property_address']);
        $property_url = $property['property_url'];
        if ($property_address == '') $property_address = 'Untitled';
        $user_name = $property['user_name'] . " " . $property['last_name'];
        $invite_data_msg = sprintf(INVITE_MSGS, $user_name, SecureShowData($property_address), $site_setting['site_name']);
        $meta = meta_setting();

        $data['property_url'] = $property_url;
        $data['user_name'] = $user_name;
        $data['property_address'] = $property_address;
        $data['site_name'] = $site_setting['site_name'];
        $data['property_id'] = $property_id;


        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/property/submit_thankyou', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
    }
    /*
    Function name :thankyou()
    Parameter :none
    Return : none
    Use : When user confirm property create then he/she can see thank you page
    */

    function start_campaign($property_id = '', $campaign_id = 0)
    {

        $user_id = check_user_authentication(true);
        $site_setting = site_setting();
        $data['site_setting'] = $site_setting;
        $meta = meta_setting();
        $data['property_id'] = $property_id;
        $data['campaign_id'] = $campaign_id;
 
        $get_property_user_detail = $this->property_model->GetAllProperties(0, $property_id,'',array('campaign'));
        $data['property'] = $get_property_user_detail[0];

        $data['campaign_user_type'] = $data['property']['campaign_user_type'];
        
        $data['campaign_type'] = $data['property']['campaign_type'];
        $data['campaign_units'] = $data['property']['campaign_units'];
        $data['investment_close_date'] = $data['property']['investment_close_date'];
        $data['min_investment_amount'] = $data['property']['min_investment_amount'];
        $data['investment_amount_type'] = $data['property']['investment_amount_type'];
        $data['min_property_investment_range'] = $data['property']['min_property_investment_range'];
        $data['max_property_investment_range'] = $data['property']['max_property_investment_range'];


        $this->load->library('form_validation');
        $this->form_validation->set_rules('campaign_user_type', 'Campaign User Type', 'required');
        $this->form_validation->set_rules('min_investment_amount', 'Min. Investment Amount', 'required');
        $this->form_validation->set_rules('campaign_units', 'Campaign Units', 'required');
        $this->form_validation->set_rules('investment_close_date', 'Investment Clost Date', 'required');
       
        $campaign_units_error = '';
        $findvalue  = array('$',"K",",");
        
        $min_investment_amount = str_replace($findvalue,'',SecurePostData($this->input->post('min_investment_amount')));


        $campaign_units = SecurePostData($this->input->post('campaign_units'));
        $data['investment_range_error'] = '';
      
        $data['campaign_units_error'] = '';
        $investment_range_error ='';
        
        $campaign_units_error = '';
       
        if($_POST)
        {
            if($this->input->post('campaign_type') == 1)
            {
                if($campaign_units == '')
                {
                    $campaign_units_error = "Number of Units field is required.";
                }
                else if($campaign_units > 20)
                {
                    $campaign_units_error = "Number of Units must be less than 20";
                }
                
            }
            else
            {
                if($campaign_units == '')
                {
                    $campaign_units_error = "Number of Units field is required.";
                }
                else if($campaign_units < 10)
                {
                    $campaign_units_error = "Number of Units must be greater than 10";
                }
                else if($campaign_units >  20)
                {
                    $campaign_units_error = "Number of Units must be less than 20";
                }
                
                else if($campaign_units < 10 and $campaign_units > 20)
                {
                    $campaign_units_error = "Number of Units must be between 10 and 20";
                }

            }
            if($this->input->post('investment_amount_type') == 1)
            {
                if($min_investment_amount == '')
                {
                    $investment_range_error = "Property Valuation field is required.";
                }
                else if($min_investment_amount < $data['min_property_investment_range'])
                {
                    $investment_range_error = "Property Valuation must be greater than ".set_currency($data['min_property_investment_range']);
                }
                else if($min_investment_amount >  $data['max_property_investment_range'])
                {
                    $investment_range_error = "Property Valuation must be less than ".set_currency($data['max_property_investment_range']);
                }
                
                else if($min_investment_amount < $data['min_property_investment_range'] and $min_investment_amount > $data['max_property_investment_range'])
                {
                    $investment_range_error = "Property Valuation must be between ".set_currency($data['min_property_investment_range'])." and ".set_currency($data['max_property_investment_range']);
                }
                
            }
            else
            {
                if($min_investment_amount == '')
                {
                    $investment_range_error = "Property Valuation field is required.";
                }

            }
       
            if ($this->form_validation->run() && $investment_range_error == '' && $campaign_units_error == '') 
            {

                    $data['campaign_user_type'] = SecurePostData($this->input->post('campaign_user_type'));
                    $data['min_investment_amount'] = $min_investment_amount;
                  
                    $data['campaign_units'] = SecurePostData($this->input->post('campaign_units'));
                    $data['campaign_type'] = SecurePostData($this->input->post('campaign_type'));
                    $data['investment_close_date'] = SecurePostData($this->input->post('investment_close_date'));
                    $data['investment_amount_type'] = SecurePostData($this->input->post('investment_amount_type'));
                     if($this->input->post('campaign_user_type') == 1)
                     {
                        $status = 0;
                     }
                     else
                     {
                        $status = 1;
                     }
                  

                   $investment_close_date = date("Y-m-d H:i:s", strtotime($this->input->post('investment_close_date')));
                   $property_id_api  = $data['property']['property_id_api'];
                  
                    $lead_investor_id = $this->session->userdata('user_id');
                   
                    $data_campaign_insert =
                        array(
                            'user_id'=>$this->session->userdata('user_id'),
                            'property_id' => $property_id,
                            'campaign_user_type' => SecurePostData($this->input->post('campaign_user_type')),
                            'min_investment_amount' => $min_investment_amount,
                           
                            'investment_close_date' => $investment_close_date,
                           
                            'campaign_units' => SecurePostData($this->input->post('campaign_units')),
                            'investment_amount_type' => SecurePostData($this->input->post('investment_amount_type')),
                            'date_added' => date('Y-m-d H:i:s'),
                            'host_ip' => $_SERVER['REMOTE_ADDR'],
                            'status' => $status,
                            'lead_investor_id' => $lead_investor_id

                           
                        );


                    $data_campaign_update =
                        array(
                            'user_id'=>$this->session->userdata('user_id'),
                            'property_id' => $property_id,
                            'campaign_user_type' => SecurePostData($this->input->post('campaign_user_type')),
                            'min_investment_amount' => $min_investment_amount,
                            'campaign_units' => SecurePostData($this->input->post('campaign_units')),
                            'investment_close_date' => $investment_close_date,
                            
                            'investment_amount_type' => SecurePostData($this->input->post('investment_amount_type')),
                            'status' => $status,
                             'lead_investor_id' => $lead_investor_id
                           
                        );
                        $campaign_id = $data['property']['campaign_id'];
                        if( $campaign_id > 0)
                        {
                             $this->account_model->UpdateAccount('campaign_id', $data['property']['campaign_id'], 'campaign', $data_campaign_update);
                        }
                        else
                        {
                             $this->account_model->UpdateAccount('','', 'campaign', $data_campaign_insert);
                             $campaign_id = $this->db->insert_id();
                        }
                        
                         if($this->input->post('campaign_user_type') == 1)
                         {
                           
                           redirect('investment/investor/'.$campaign_id);
                         }
                         else
                         {
                            redirect('property/thankyou/'.$property_id);
                         }
                    
                    
                    } 
                    else 
                    {

                        

                        $data['investment_range_error'] = $investment_range_error;
                        
                        $data['campaign_units_error'] = $campaign_units_error;
                    
                        $data['campaign_user_type'] = SecurePostData($this->input->post('campaign_user_type'));
                        $data['min_investment_amount'] = $min_investment_amount;
                  
                        $data['campaign_units'] = SecurePostData($this->input->post('campaign_units'));
                        $data['campaign_type'] = SecurePostData($this->input->post('campaign_type'));
                        $data['investment_close_date'] = SecurePostData($this->input->post('investment_close_date'));
                        $data['investment_amount_type'] = SecurePostData($this->input->post('investment_amount_type'));
                    
                     }
        }
        
        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/property/start_campaign', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
    }

    /*
    Function name :image_save()
    Parameter :none;
    Return : Array for Image  with property
    Use :Image for property
    Auther:Rakesh
    */
    function document_save()
    {

        $user_id = check_user_authentication(true);
        $data = array();
        $data["error"] = false;
        $data["msg"] = '';
        $data["url"] = '';
        $data['image_name_error'] = '';
        $data['image_desc_error'] = '';
       
        $image_id = SecurePostData($this->input->post('image_id'));
        $image_name = SecurePostData($this->input->post('name'));
        $document_name = SecurePostData($this->input->post('document_name'));
        $property_id = SecurePostData($this->input->post('property_id'));
        $campaign_id = SecurePostData($this->input->post('campaign_id'));


       
        if ($this->session->userdata('user_id') == '') {
            $data["url"] = 'home/login';
            $data["error"] = true;
            echo json_encode($data);
            die;
        } else if ($property_id == null || $property_id == '' || $property_id == '0') {
           $data["url"] = "property/start_campaign/".$property_id;
            $data["error"] = true;
            echo json_encode($data);
            die;
        }
    

        $document_name = '';
        if (isset($_FILES['file']['name'])) 
        {


            $_FILES['userfile']['name'] = $_FILES['file']['name'];
            $_FILES['userfile']['type'] = $_FILES['file']['type'];
            $_FILES['userfile']['tmp_name'] = $_FILES['file']['tmp_name'];
           
           
            $_FILES['userfile']['error'] = $_FILES['file']['error'];
            $_FILES['userfile']['size'] = $_FILES['file']['size'];
         
            // image validation
            $image_settings = get_image_setting_data();
            if ($_FILES["userfile"]["type"] != "image/jpeg" and $_FILES["userfile"]["type"] != "image/pjpeg" and $_FILES["userfile"]["type"] != "image/png" and $_FILES["userfile"]["type"] != "image/x-png" and $_FILES["userfile"]["type"] != "image/gif") {
                $data["image_error"] = PLEASE_UPLOAD_A_JPG_PNG_GIF_FILE;
                $data["error"] = true;
                echo json_encode($data);
                die;
            } else if ($_FILES["userfile"]["size"] > $image_settings['upload_limit']*1000000) {
                $data["image_error"] = sprintf(SORRY_THIS_FILE_IS_TOO_LARGE_PLEASE_SELECT_A_JPG_FILE_THAT_IS_LESS_THAN_MB_OR_TRY_RESIZING_IT_USING_A_PHOTO_EDITOR,$image_settings['upload_limit']);
                $data["error"] = true;
                echo json_encode($data);
                die;
            } else {

                $data["image_error"] = '';
                $files = $_FILES;
                $base_path = $this->config->slash_item('base_path');
              
                $rand = rand(0, 100000);
                       
                $path_parts = pathinfo($_FILES['file']['name']);
                $date = new DateTime();
                
                $file_extention = $path_parts['extension'];
                $new_img = $rand .'-campaign-document-'.$date->getTimestamp().'.'.$file_extention;
               

                move_uploaded_file($files["userfile"]["tmp_name"], $base_path . "upload/property/campaign/" . $new_img);
                $document_name = $new_img;



                $data["image"]["image_name"] = $document_name;
                $data["image"]["path"] = anchor(base_url() . "upload/property/campaign/" . $document_name, $document_name);


            }
        }
        
        

        $data_array = array('name' => $_FILES['file']['name'], 'document_name' => $document_name,  'campaign_id' => $campaign_id,'property_id' => $property_id);

        $document_id = $this->property_model->AddInsertUpdateTable('campaign_document', '','', $data_array);
        

        $data['document_data'] = $this->property_model->property_document_data($campaign_id,$property_id,$document_id);


 
        echo json_encode($data);


    }

    /*
    Function name :list_image()
    Parameter :none;
    Return : Result array
    Use : Show Image list for property ;
    Auther:Rakesh
    */
    function list_document()
    {
        check_user_authentication(true);
        $data = array();
        $data["error"] = false;
        $data["msg"] = '';
        $data["url"] = '';
        $property_id = SecurePostData($this->input->post('property_id'));
        
        $campaign_id = SecurePostData($this->input->post('campaign_id'));
        if ($this->session->userdata('user_id') == '') 
        {
            $data["url"] = 'home/login';
            $data["error"] = true;
            echo json_encode($data);
            die;
        } 
        else if ($property_id == null || $property_id == '' || $property_id == '0') 
        {
            $data["url"] = "property/start_campaign/".$property_id;
            $data["error"] = true;
            echo json_encode($data);
            die;
        }
      
        $document_data = $this->property_model->property_document_data($campaign_id,$property_id);
        $data["document_data"] = $document_data;
        $this->load->view('default/property/list_document', $data);
    }

    /*
    Function name :delete_image()
    Parameter :none;
    Return : none
    Use : Delete Image for property ;
    Auther:Rakesh
    */
    function delete_document()
    {
        check_user_authentication(true);
        $data = array();
        $data["error"] = false;
        $data["msg"] = '';
        $data["url"] = '';
        $campaign_id = SecurePostData($this->input->post('campaign_id'));
        $property_id = SecurePostData($this->input->post('property_id'));
        
        $image_id = SecurePostData($this->input->post('image_id'));

        if ($this->session->userdata('user_id') == '') {
            $data["url"] = 'home/login';
            $data["error"] = true;
            echo json_encode($data);
            die;
        } else if ($property_id == null || $property_id == '' || $property_id == '0') {
            $data["url"] = "property/start_campaign/".$property_id;
            $data["error"] = true;
            echo json_encode($data);
            die;
        }

        //check user select value from dropdown
        if ($image_id == null || $image_id == '' || $image_id == '0') {
            $data["msg"] = PLEASE_CLICK_ON_IMAGE_TO_DELETE;
            $data["error"] = true;
            echo json_encode($data);
            die;
        }

        $document_data = $this->property_model->property_document_data($campaign_id,$property_id,$image_id);
     

        $sql = "delete from campaign_document where id='" . $image_id . "' ";
        $this->db->query($sql);

        unlink(base_path()."upload/property/campaign/".$document_data[0]['document_name']);

        echo json_encode($data);
    }

    /*
    Function name :add_comment()
    Parameter :none
    Return : none
    Use : Add comments data.
    */
    function add_comment()
    {

        $data1['property_id'] = SecurePostData($this->input->post('property_id'));
        $campaign_id = SecurePostData($this->input->post('campaign_id'));
        $data1['user_id'] = SecurePostData($this->input->post('comment_user_id'));
        $comment_user_id = SecurePostData($this->input->post('comment_user_id'));
        $data1['comments'] = strip_tags($this->input->post('comment'));
        $data1['comment_type'] = SecurePostData($this->input->post('comment_type'));
        $data1['date_added'] = date("Y-m-d H:i:s");
        $data1['comment_ip'] = SecurePostData($this->input->post('comment_ip'));
        $property_id = $data1['property_id'];
        $property_data = GetOnePropertyCampaign($campaign_id,$property_id);
        $property_owner_id = $property_data['user_id'];
        $property_address = SecureShowData($property_data['property_address']);
        $property_url = $property_data['property_url'];
        if ($property_owner_id == $data1['user_id'] || $property_owner_id == '') {
            $data1['status'] = 1;
        } else {
            $data1['status'] = 0;
        }
        $chk_user = 'false';
        setting_deletecache('spam_protectd');
        $spamer = $this->home_model->spam_protection();
        if ($spamer == 1 || $spamer == '1') {
            $chk_user = 'true';
        }
        $this->form_validation->set_rules('comment', COMMENT, 'required');
        $data["msg"]['error'] = '';
        $data["msg"]['success'] = '';

        if ($this->form_validation->run() == FALSE || $chk_user == 'true') {
            if (validation_errors()) {
                $data["msg"]['error'] = validation_errors();
            } else {
                $data["msg"]['error'] = "";
            }
            if ($chk_user == 'true') {
                $spam_message = "<p>" . YOUR_IP_HAS_BEEN_BAND_DUE_SPAM_YOU_CAN_NOT_POST_MORE_COMMENT . "</p>";
                $data["msg"]['error'] = $spam_message;
            }
        } else {
            $property_comment_id = $this->property_model->AddComment('comment', $data1);


            $user_comment_array = array('user_id'=>$comment_user_id,'property_id' => $this->input->post('property_id'),'property_comment_id' =>$property_comment_id);
            $this->property_model->AddUpdateComment('user_comment', $user_comment_array, array(
                'user_comment'
            ));
            ///////// add latest activities
            //project_activity('commented', $data1['user_id'], $property_owner_id, $property_id);
            ///////// end
            if ($property_owner_id == $data1['user_id'] || $property_owner_id == '') {


                $data["msg"]["success"] = '<span>' . COMMENT_POSTED_SUCCESSFULLY . '</span>';
                $data["msg"]["owner_cmt"] = 'yes';
            } else {
                $data["msg"]["success"] = '<span>' . SHOW_COMMENT_AFTER_APPROVED . '</span>';
            }
            $userdata = UserData(SecurePostData($this->input->post('comment_user_id')));
            $get_property_user_detail = $this->property_model->GetAllProperties(0, $this->input->post('property_id'), $is_status = '', $joinarr = array(
                'user'
            ), $limit = 10, $order = array(
                'property_id' => 'desc'
            ));

            $property_address = SecureShowData($property_data['property_address']);
            $taxonomy_setting = taxonomy_setting();
            $project_url = $taxonomy_setting['project_url'];
            $property_page_link = site_url('properties/' . $property_url);
            $property_title_anchor = '<a href="' . $property_page_link . '">' . $property_address . '</a>';

            $all_back_user_comment = UserData($this->session->userdata('user_id'));

            $profile_slug = $all_back_user_comment[0]['profile_slug'];

            $comment_profile_link = site_url('user/' . $profile_slug);

            $comment_user_name = $all_back_user_comment[0]['user_name'] . ' ' . $all_back_user_comment[0]['last_name'];
            $comment_name_anchor = '<a href="' . $comment_profile_link . '">' . $comment_user_name . '</a>';


            $user_not_own = $this->account_model->get_email_notification($get_property_user_detail[0]['user_id']);

            if ($user_not_own != '0') {
                if ($user_not_own->creator_comment_alert == '1') {
                    $comment['user_name'] = $get_property_user_detail[0]['user_name'] . ' ' . $get_property_user_detail[0]['last_name'];
                    $comment['comment_user_name'] = $userdata[0]['user_name'] . ' ' . $userdata[0]['last_name'];
                    $comment['property_address'] = SecureShowData($get_property_user_detail[0]['property_address']);
                    $comment['comment'] = SecurePostData($this->input->post('comment'));
                    $comment['email'] = $get_property_user_detail[0]['email'];
                    $comment['comment_name_anchor'] = $comment_name_anchor;
                    $comment['property_title_anchor'] = $property_title_anchor;
                    $comment['property_page_link'] = $property_page_link;
                    $comment['user_id'] = $get_property_user_detail[0]['user_id'];
                    $comment['comment_profile_link'] = '<a href="' . site_url('property/dashboard/' . $get_property_user_detail[0]['property_id'] . '#comments-tab') . '">' . SecureShowData($get_property_user_detail[0]['property_address']) . '</a>';
                    $this->mailalerts('comment_alert', $comment, '', '', 'New Comment Owner Alert');
                    $this->mailalerts('comment_admin_alert', $comment, '', '', 'New Comment Admin Alert');
                }
            }


            // ///////////////============== Project you back notification================================

            $site_setting = site_setting();
            $site_name = $site_setting['site_name'];

            $taxonomy_setting = taxonomy_setting();
            $project_name = SecureShowData($taxonomy_setting['project_name']);
            $comments_plural = $taxonomy_setting['comments_plural'];
            $comments_past = $taxonomy_setting['comments_past'];
            $comment = $taxonomy_setting['comments'];
            $funds_past = $taxonomy_setting['funds_past'];
            $followers_past = $taxonomy_setting['followers_past'];

            $property_back_comment = $this->db->query("SELECT un.comment_alert,un.user_id,u.email,u.user_name,u.last_name FROM `user_notification` un
            inner join user u on u.user_id=un.user_id
            inner join transaction tr on tr.user_id=u.user_id
            where  un.comment_alert=1 and tr.property_id=" . $property_id . "
            group by u.user_id");

            $all_back_user = $property_back_comment->result_array();
            $comment_add = SecurePostData($this->input->post('comment'));

            if (is_array($all_back_user)) {
                foreach ($all_back_user as $back_user) {

                    $donated_user_name = $back_user['user_name'];
                    $donated_last_name = $back_user['last_name'];
                    $donated_email = $back_user['email'];

                     $language_id=GetUserLangCode( $back_user['user_id']);
                    $email_template = $this->db->query("select * from `email_template` where task='Someone comment on your backed property' and language_id=".$language_id);
                    $email_temp = $email_template->row();
                    $email_message = $email_temp->message;
                    $email_subject = $email_temp->subject;
                    $email_subject = str_replace('{company_name}', $company_name, $email_subject);
                    $email_subject = str_replace('{comment}', $comment, $email_subject);
                    $email_subject = str_replace('{project_name}', $project_name, $email_subject);
                    $email_address_from = $email_temp->from_address;
                    $email_address_reply = $email_temp->reply_address;
                    $email_to = $donated_email;

                    $email_message = str_replace('{user_name}', $donated_user_name, $email_message);
                    $email_message = str_replace('{project_name}', $project_name, $email_message);
                    $email_message = str_replace('{company_name}', $property_title_anchor, $email_message);
                    $email_message = str_replace('{project}', $company_name, $email_message);
                    $email_message = str_replace('{comment_user_name}', $comment_name_anchor, $email_message);
                    $email_message = str_replace('{comment_full_name}', $comment_user_name, $email_message);
                    $email_message = str_replace('{comment}', $comment, $email_message);
                    $email_message = str_replace('{comments}', $comment_add, $email_message);
                    $email_message = str_replace('{comments_plural}', $comments_plural, $email_message);
                    $email_message = str_replace('{comment_past}', $comments_past, $email_message);
                    $email_message = str_replace('{funds_past}', $funds_past, $email_message);
                    $email_message = str_replace('{property_page_link}', $property_page_link, $email_message);
                    $email_message = str_replace('{site_name}', $site_name, $email_message);

                    $str = $email_message;

                    email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);

                    # code...
                }
            }

            // ///////////////============== Project you follow notification================================


            $property_you_follow = $this->db->query("SELECT un.new_comment_alert,un.user_id,u.email,u.user_name,u.last_name FROM `user_notification` un
                        inner join user u on u.user_id=un.user_id
                        inner join property_follower ef on ef.property_follow_user_id=u.user_id
                        where  un.new_comment_alert=1 and ef.property_id=" . $property_id . "
                        group by u.user_id");

            $all_property_follow_user = $property_you_follow->result_array();

            if (is_array($all_property_follow_user)) {
                foreach ($all_property_follow_user as $follower_user) {

                    $follower_user_name = $follower_user['user_name'];
                    $follower_last_name = $follower_user['last_name'];
                    $follower_email = $follower_user['email'];
                    $language_id=GetUserLangCode( $follower_user['user_id']);
                    $email_template = $this->db->query("select * from `email_template` where task='New comment on campaign you followed' and language_id=".$language_id);
                    $email_temp = $email_template->row();
                    $email_message = $email_temp->message;
                    $email_subject = $email_temp->subject;
                    $email_subject = str_replace('{company_name}', $company_name, $email_subject);
                    $email_subject = str_replace('{comment}', $comment, $email_subject);
                    $email_subject = str_replace('{project_name}', $project_name, $email_subject);
                    $email_subject = str_replace('{followers_past}', $followers_past, $email_subject);
                    $email_address_from = $email_temp->from_address;
                    $email_address_reply = $email_temp->reply_address;
                    $email_to = $follower_email;

                    $email_message = str_replace('{user_name}', $follower_user_name, $email_message);
                    $email_message = str_replace('{project_name}', $project_name, $email_message);
                    $email_message = str_replace('{company_name}', $property_title_anchor, $email_message);
                    $email_message = str_replace('{comment_user_name}', $comment_name_anchor, $email_message);
                    $email_message = str_replace('{comment_full_name}', $comment_user_name, $email_message);
                    $email_message = str_replace('{comments_plural}', $comments_plural, $email_message);
                    $email_message = str_replace('{comment_past}', $comments_past, $email_message);
                    $email_message = str_replace('{comments}', $comment_add, $email_message);
                    $email_message = str_replace('{comment}', $comment, $email_message);
                    $email_message = str_replace('{followers_past}', $followers_past, $email_message);
                    $email_message = str_replace('{property_page_link}', $property_page_link, $email_message);
                    $email_message = str_replace('{site_name}', $site_name, $email_message);

                    $str = $email_message;

                    email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);

                    # code...
                }
            }


        }
        property_other_deletecache('comment', $property_id, $property_url);
        user_deletecache('comments', $comment_user_id);
        echo json_encode($data);
    }

     /*
    Function name :comment_list()
    Parameter :$property_id(equity id),$offset (number)
    Return : none
    Use : to show the comments on equity detail page using ajax
        this is used to show the all or more comments on equity detail page
    */
    function comment_list($property_id = '', $offset = 0)
    {
        $data['property_user_id'] = $this->session->userdata('user_id');

        $data['offset'] = $offset;
        $data['comment_row'] = $this->account_model->GetUpdateCommentGallery(0, $property_id, '', $join = array(
            'comment',
            'user'
        ), 100, $group = array(
            'comment_id' => 'comment.comment_id'
        ), $order = array(
            'property.property_id' => 'desc',
            'comment.comment_id' => 'desc'
        ), 1);
        $count_comments = count($data['comment_row']);

        $data['total_row_display'] = $count_comments - $offset;
        $data['comment'] = $this->account_model->GetUpdateCommentGallery(0, $property_id, '', $join = array(
            'comment',
            'user'
        ), $limit = 5, $group = array(
            'comment_id' => 'comment.comment_id'
        ), $order = array(
            'property.property_id' => 'desc',
            'comment.comment_id' => 'desc'
        ), 1, $offset);
        $this->load->view('default/property/comment_list', $data);
    }

    /*
    Function name :updates_list()
    Parameter :$equity_id(equity id),$offset (number)
    Return : none
    Use : to show the updates on equity detail page using ajax
         this is used to show the all or more updates on equity detail page
    */
    function updates_list($property_id = '', $offset = 0)
    {
        $property_data = GetOneProperty($property_id);
        $data['user_id'] = $property_data['user_id'];
        $user_data = UserData($property_data['user_id']);

        $data['user_name'] = $user_data[0]['user_name'];
        $data['last_name'] = $user_data[0]['last_name'];
        $data['profile_slug'] = $user_data[0]['profile_slug'];

        $data['property_user_id'] = $this->session->userdata('user_id');
        $data['offset'] = $offset;
        $data['update_on_my_property_row'] = $this->account_model->GetUpdateCommentGallery(0, $property_id, '', $join = array(
            'updates'
        ), $limit = 100, $group = array(), $order = array(
            'updates.date_added' => 'desc'
        ), '', '', $count = 'yes');
        $count_updates = $data['update_on_my_property_row'];

        $data['total_row_display'] = $count_updates - $offset;
        $data['update_on_my_property'] = $this->account_model->GetUpdateCommentGallery(0, $property_id, '', $join = array(
            'updates'
        ), $limit = 5, $group = array(), $order = array(
            'updates.date_added' => 'desc'
        ), '', $offset);
        $this->load->view('default/property/updates_list', $data);
    }

    /*
    Function name :ajax_update_save()
    Parameter :none
    Return : none
    Use : Add update of equity data.
    */
    function ajax_update_save()
    {
        //print_r($_POST);die;
        $property_id = SecurePostData($this->input->post('property_id'));


        $user_id = check_user_authentication(true);
        if ($property_id > 0) {
            if (!$this->property_model->is_property_owner($property_id, false)) {
                redirect('home/my_campaign');
            }
        }
        $update_id = SecurePostData($this->input->post('update_id'));
        $action = SecurePostData($this->input->post('type'));

        $data1['property_id'] = SecurePostData($this->input->post('property_id'));
        $data1['updates'] = $this->input->post('updates');
        $data1['status'] = '0';
        $data1['date_added'] = date("Y-m-d H:i:s");
        $update_value_check = '';

        if (strip_tags($this->input->post('updates')) == '') {
            $update_value_check = UPDATES_IS_REQURED;
        }

        //$this->form_validation->set_rules('updates', UPDATES, 'required');
        $data["msg"]['error'] = '';
        $data["msg"]['success'] = '';

        if ($update_value_check != '') {
            if ($update_value_check != '') {
                $data["msg"]['error'] = $update_value_check;
            } else {
                $data["msg"]['error'] = "";
            }
        } else {
            if ($update_id != '') {
                if ($action != '') {
                    $this->property_model->DeleteUpdateById('update_id', $update_id, 'updates', '', array(
                        'update'
                    ));
                    $data["msg"]["success"] = '<span>' . RECORD_DELETED_SUCCESSFULLY . '</span>';
                } else {
                    $this->property_model->UpdatePerkUpdates('update_id', $update_id, 'updates', $data1, array(
                        'update'
                    ));
                    $data["msg"]["success"] = '<span>' . RECORD_UPDATED_SUCCESSFULLY . '</span>';
                }
            } else {
                $this->property_model->AddUpdateComment('updates', $data1, array(
                    'update'
                ));

                // activity notification
                project_activity('update', $this->session->userdata('user_id'), $user_id, $property_id);

                $data["msg"]["success"] = '<span>' . RECORD_ADDED_SUCCESSFULLY . '</span>';
            }
             property_other_deletecache('update',$property_id);

            $property = GetOneProperty($property_id);

            $property_address = SecureShowData($property['property_address']);

            $property_url = $property['property_url'];
            $taxonomy_setting = taxonomy_setting();
            $project_url = $taxonomy_setting['project_url'];
            $property_page_link = site_url('properties/' . $property_url);
            $property_title_anchor = '<a href="' . $property_page_link . '">' . $property_address . '</a>';


            $all_back_user_update = UserData($property['user_id']);

            $profile_slug = $all_back_user_update[0]['profile_slug'];

            $update_profile_link = site_url('user/' . $profile_slug);
            $username = $all_back_user_update[0]['user_name'];
            $lastname = $all_back_user_update[0]['last_name'];
            $email = $all_back_user_update[0]['email'];

            $update_user_name = $username . ' ' . $lastname;
            $update_name_anchor = '<a href="' . $update_profile_link . '">' . $update_user_name . '</a>';

            $site_setting = site_setting();
            $site_name = $site_setting['site_name'];
            $taxonomy_setting = taxonomy_setting();
            $project_name = SecureShowData($taxonomy_setting['project_name']);
            $updates_plural = $taxonomy_setting['updates_plural'];
            $funds_past = $taxonomy_setting['funds_past'];
            $followers_past = $taxonomy_setting['followers_past'];

            // ///////////////============== Project you created notification================================

            $user_not_own = $this->account_model->get_email_notification($property['user_id']);

            if ($user_not_own != '0') {
                if ($user_not_own->creator_newup_alert == 1) {

                    $language_id=GetUserLangCode($all_back_user_update[0]['user_id']);
                    $updates = $this->input->post('updates');

                    $email_template = $this->db->query("select * from `email_template` where task='New updates on campaign you created' and language_id=".$language_id);
                    $email_temp = $email_template->row();
                    $email_message = $email_temp->message;
                    $email_subject = $email_temp->subject;
                    $email_subject = str_replace('{company_name}', $property_address, $email_subject);
                    $email_subject = str_replace('{updates_plural}', $updates_plural, $email_subject);
                    $email_subject = str_replace('{project_name}', $project_name, $email_subject);

                    $email_address_from = $email_temp->from_address;
                    $email_address_reply = $email_temp->reply_address;
                    $email_to = $email;
                    $email_message = str_replace('{break}', '<br/>', $email_message);
                    $email_message = str_replace('{user_name}', $username, $email_message);
                    $email_message = str_replace('{project_name}', $project_name, $email_message);
                    $email_message = str_replace('{company_name}', $property_title_anchor, $email_message);
                    $email_message = str_replace('{update_user_name}', $update_name_anchor, $email_message);
                    $email_message = str_replace('{updates_plural}', $updates_plural, $email_message);
                    $email_message = str_replace('{equity_page_link}', $property_page_link, $email_message);
                    $email_message = str_replace('{site_name}', $site_name, $email_message);

                    $str = $email_message;

                    email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);

                    # code...
                }
            }


            // ///////////////============== Project you back notification================================

            $property_back_update = $this->db->query("SELECT un.update_alert,un.user_id,u.email,u.user_name,u.last_name FROM `user_notification` un
                inner join user u on u.user_id=un.user_id
                inner join transaction tr on tr.user_id=u.user_id
                where  un.update_alert=1 and tr.property_id=" . $property_id . "
                group by u.user_id");


            $all_back_user = $property_back_update->result_array();


            if (is_array($all_back_user)) {
                foreach ($all_back_user as $back_user) {

                    $donated_user_name = $back_user['user_name'];
                    $donated_last_name = $back_user['last_name'];
                    $donated_email = $back_user['email'];
                    $updates = $this->input->post('updates');
                     $language_id=GetUserLangCode( $back_user['user_id']);
                    $email_template = $this->db->query("select * from `email_template` where task='update on your backed equity' and language_id=".$language_id);
                    $email_temp = $email_template->row();
                    $email_message = $email_temp->message;
                    $email_subject = $email_temp->subject;

                    $email_subject = str_replace('{company_name}', $property_address, $email_subject);
                    $email_subject = str_replace('{updates_plural}', $updates_plural, $email_subject);
                    $email_subject = str_replace('{project_name}', $project_name, $email_subject);
                    $email_subject = str_replace('{funds_past}', $funds_past, $email_subject);

                    $email_address_from = $email_temp->from_address;
                    $email_address_reply = $email_temp->reply_address;
                    $email_to = $donated_email;
                    $email_message = str_replace('{break}', '<br/>', $email_message);
                    $email_message = str_replace('{user_name}', $donated_user_name, $email_message);
                    $email_message = str_replace('{project_name}', $project_name, $email_message);
                    $email_message = str_replace('{funds_past}', $funds_past, $email_message);
                    $email_message = str_replace('{company_name}', $property_title_anchor, $email_message);
                    $email_message = str_replace('{update_user_name}', $update_name_anchor, $email_message);
                    $email_message = str_replace('{updates_plural}', $updates_plural, $email_message);
                    $email_message = str_replace('{equity_page_link}', $property_page_link, $email_message);
                    $email_message = str_replace('{site_name}', $site_name, $email_message);
                    $str = $email_message;

                    email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);

                    # code...
                }
            }

            // ///////////////============== Project you follow notification================================


            $property_you_follow = $this->db->query("SELECT un.new_updates_alert,un.user_id,u.email,u.user_name,u.last_name FROM `user_notification` un
                        inner join user u on u.user_id=un.user_id
                        inner join property_follower pf on pf.property_follow_user_id=u.user_id
                        where  un.new_updates_alert=1 and pf.property_id=" . $property_id . "
                        group by u.user_id");

            $all_property_follow_user = $property_you_follow->result_array();

            if (is_array($all_property_follow_user)) {
                foreach ($all_property_follow_user as $follower_user) {

                    $follower_user_name = $follower_user['user_name'];
                    $follower_last_name = $follower_user['last_name'];
                    $follower_email = $follower_user['email'];
                     $language_id=GetUserLangCode( $follower_user['user_id']);
                    $email_template = $this->db->query("select * from `email_template` where task='New updates on campaign you followed' and language_id=".$language_id);
                    $email_temp = $email_template->row();
                    $email_message = $email_temp->message;
                    $email_subject = $email_temp->subject;
                    $email_subject = str_replace('{company_name}', $property_address, $email_subject);
                    $email_subject = str_replace('{updates_plural}', $updates_plural, $email_subject);
                    $email_subject = str_replace('{project_name}', $project_name, $email_subject);
                    $email_subject = str_replace('{followers_past}', $followers_past, $email_subject);
                    $email_address_from = $email_temp->from_address;
                    $email_address_reply = $email_temp->reply_address;
                    $email_to = $follower_email;
                    $email_message = str_replace('{break}', '<br/>', $email_message);
                    $email_message = str_replace('{user_name}', $follower_user_name, $email_message);
                    $email_message = str_replace('{project_name}', $project_name, $email_message);
                    $email_message = str_replace('{company_name}', $property_title_anchor, $email_message);
                    $email_message = str_replace('{followers_past}', $followers_past, $email_message);
                    $email_message = str_replace('{update_user_name}', $update_name_anchor, $email_message);
                    $email_message = str_replace('{updates_plural}', $updates_plural, $email_message);
                    $email_message = str_replace('{equity_page_link}', $property_page_link, $email_message);
                    $email_message = str_replace('{site_name}', $site_name, $email_message);
                    $str = $email_message;

                    email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);

                    # code...
                }
            }


        }
        echo json_encode($data);
    }
    /*
    Function name :ajax_updateupdates()
    Parameter :none
    Return : none
    Use : Update update of property data.
    */
    // ==========================update updates================
    function ajax_updateupdates()
    {
        $property_id = SecurePostData($this->input->post('property_id'));
        $user_id = check_user_authentication(true);
        if ($property_id > 0) {
            if (!$this->property_model->is_property_owner($property_id, false)) {
                redirect('home/my_campaign');
            }
        }
        $data['is_property_owner'] = $this->property_model->is_property_owner($property_id, false);
        $property_id = SecurePostData($this->input->post('property_id'));
        $data['site_setting'] = site_setting();
        $data['property_updates'] = $this->account_model->GetUpdateCommentGallery(0, $property_id, '', $join = array(
            'updates'
        ), $limit = 0, $group = array(), $order = array(
            'updates.date_added' => 'desc'
        ), '');
      
        $this->load->view('default/property/ajax_updateupdates', $data);
    }
    function widgets_code($w, $c, $n)
    {

        $data['w'] = $w;
        $data['c'] = $c;
        $data['n'] = $n;
        $data['offset'] = 0;
        $data['limit'] = 8;
        $this->load->view('default/widgets_code', $data);
    }

    function widgets_page($w, $c, $n)
    {
        $data['color'] = $c;
        $data['width'] = $w;
        $get_property_user_detail = $this->property_model->GetAllProperties(0, $n,'',array('campaign','user'));
        $data['property'] = $get_property_user_detail[0];
      
        //$data['equity_gallery'] = $this->equity_model->get_all_equity_gallery($n);
        $data['site_setting'] = site_setting();
        $data['offset'] = 0;
        $data['limit'] = 8;
        $this->template->add_css($data['color'] . '/fund-' . $data['color'] . '.css');
        $this->template->write_view('main_content', 'default/widgets_page', $data, TRUE);
        $this->template->render();
    }

    function widgets($id = 0)
    {
        if ($id != '') {
            $property_detail = $this->db->query("select * from property where property_id='" . $id . "'");
            $property = $property_detail->row();
            $this->session->set_userdata('property_id', $this->security->xss_clean($id));
            $this->session->set_userdata('property_address', $this->security->xss_clean($property->property_address));
            $this->session->set_userdata('property_url', $this->security->xss_clean($property->property_url));
        }
        $data['pid'] = $id;
        $meta = meta_setting();
        $data['site_setting'] = site_setting();
        $data['offset'] = 0;
        $data['limit'] = 8;
        $data['header_menu'] = dynamic_menu(0);
        $data['footer_menu'] = dynamic_menu_footer(0);
        $data['right_menu'] = dynamic_menu_right(0);
        $this->home_model->select_text();
        $this->template->write('meta_title', 'Widgets-' . $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/header_login', $data, TRUE);
        $this->template->write_view('main_content', 'default/widgets', $data, TRUE);
        $this->template->write_view('footer', 'default/footer', $data, TRUE);
        $this->template->render();
    }
    /*
    Function name :equity_following()
    Parameter :No parameter
    Return : none
    Use : this is used in equity detail page
    Description :  will show you list of follower for particular equity, according to equity id
    */
    function property_following()
    {
        check_user_authentication(true);
        $meta = meta_setting();
        $data['site_setting'] = site_setting();
        $data['taxonomy_setting'] = taxonomy_setting();
        $user_id = $this->session->userdata('user_id');
        $data['property_following'] = $this->following_model->GetAllFollowing($type = 'property_following', $user_id, $join = array(
            'property'
        ), $limit = 1000, $order = array(
            'property_follow_id' => 'desc'
        ));


        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/property/property_following', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
    }
    /*
    Function name :equity_unfollower()
    Parameter :user_id
    Return : none
    Use : Used for Unfollow the follower for particualr equity.
    */
    // ================== equity follow insert================
    function property_unfollower($property_id = 0)
    {
        if ($property_id == '' or $property_id <= 0) {
            redirect('home');
        }
        $this->db->delete('property_follower', array(
            "property_id" => $property_id,
            "property_follow_user_id" => $this->session->userdata('user_id')
        ));
        project_activity('unfollow', $this->session->userdata('user_id'), 0, $property_id);
        echo "Unfollow";


        die();
    }
     function ajax_property_follower_list($property_id='')
    {
        $user_id = check_user_authentication(true);
         $data['property_following'] = $this->following_model->GetAllFollowing($type = 'property_following', $user_id, $join = array(
            'property'
        ), $limit = 1000, $order = array(
            'property_follow_id' => 'desc'
        ));

        $this->load->view('default/property/ajax_savefollower_list', $data);
    }

    /*
    Function name :attachments()
    Parameter :id 
    Return : none
    Use : User can add new attachments.
    */
    function attachments($id = '')
    {
   
        $user_id = check_user_authentication(true);

        $data = array();
        if($this->input->post('property_id') > 0 && $this->input->post('property_id') != '')
        {
            $id = $this->input->post('property_id');
            $data['id']=$id;
        }
        else
        {
            $data['id']=$id;        
        }

        if($id > 0)
        {
            if (!$this->property_model->is_property_creator($id, false)) {
                redirect('home/notedit_property');
            }
        }
        
        $data['attachments'] = $this->property_model->getattachments($id,1);


        $meta = meta_setting();
        $data['site_setting'] = site_setting();


        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);
        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/property/property_attachments', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
    }

    /*
    Function name :atatchment_save()
    Parameter : none 
    Return : none
    Use : Upload attachments.
    */
    function atatchment_save(){
        $id = $this->input->post('propertyid');

        $config['upload_path']   = './upload/property/attachments/'; 
        $config['allowed_types'] = 'pdf'; 
        $this->load->library('upload', $config);

        if ($this->upload->do_upload('attachfile')) {
            $data = array('upload_data' => $this->upload->data());

            $this->property_model->atatchment_save($data,$id);
            // echo "<pre>";
            // print_r($data);
            // exit;
            
        }

        redirect(base_url()."property/attachments/".$id);

        // else { 
        //     $error = array('error' => $this->upload->display_errors());
        //     echo "<pre>";
        //     print_r($error);
        // }

    }

    function updateattach($pattachid,$isdelete,$id){
        $this->db->where("pattachid",$pattachid);
        $this->db->update("property_attachments",array("isdelete"=>$isdelete));
        redirect(base_url()."property/attachments/".$id);
    }

}

?>
