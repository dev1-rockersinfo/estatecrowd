<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 * The Home class is basically the default controller for the script.
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Controllers
 */
class Content extends ROCKERS_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('securimage');
        $this->load->model('content_model');
        $this->load->model('home_model');
    }

    /*
    Function name :faq()
    Parameter :no
    Return : none
    Use : to show faq data
    Description :  to show faq data
    */
    function faq()
    {
        $data['result'] = $this->content_model->faq(1, 0, array('faq_category'), array('faq_order' => 'ASC'));
        $meta = meta_setting();

        $data['site_setting'] = site_setting();
        $spamer = $this->home_model->spam_protection();
        if ($spamer == 1 || $spamer == '1') {
            $chk_user = 'true';
        }

        $this->template->write('meta_title', $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'], TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);

        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/content/faq', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
    }

    /*
    Function name :pages()
    Parameter :title or slug
    Return : none
    Use : to show particular page data from page slug
    Description :  to particular page data
    */
    function pages($title = '')
    {


        $pages = $this->content_model->getpages(1, $title, 0, 0, array('pages_id' => 'desc'));
        $data['pages'] = $pages;
        $data['title'] = $pages[0]['pages_title'];
        $data['header_title'] = $pages[0]['header_title'];
        $data['header_content'] = $pages[0]['header_content'];
        $data['link_name'] = $pages[0]['link_name'];
        $data['link'] = $pages[0]['link'];
        $data['dynamic_image_image'] = $pages[0]['dynamic_image_image'];
        $pages_title = '';
        $meta_keyword = '';
        $meta_description = '';
        $meta = meta_setting();
        if (isset($pages[0]['pages_title'])) $pages_title = $pages[0]['pages_title'];
        if (isset($pages[0]['meta_keyword'])) $meta_keyword = ', ' . $pages[0]['meta_keyword'];
        if ($pages[0]['meta_description'] != '') {
            $meta_description = $pages[0]['meta_description'];
        } else {
            $meta_description = $meta['meta_description'];
        }

        $data['site_setting'] = site_setting();
        $this->template->write('meta_title', $pages_title . " | " . $meta['title'], TRUE);
        $this->template->write('meta_description', $meta_description, TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'] . " " . $meta_keyword, TRUE);
        if($data['dynamic_image_image'] == '' && !is_file(base_path().'upload/content/'.$data['dynamic_image_image']))
        {
            $this->template->write_view('header', 'default/common/header', $data, TRUE);
        }
        $this->template->write_view('main_content', 'default/content/pages', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
    }

    /*
    Function name :term()
    Parameter :none
    Return : none
    Use : to show terms and condition page
    Description : to show terms and condition page
    */
    function term()
    {
        $title = 'Terms-of-service';
        $data['title'] = $title;


        $pages = $this->content_model->getpages(1, $title, 0, 0, array('pages_id' => 'desc'));
        $data['pages'] = $pages;
        $pages_title = '';
        $meta_keyword = '';
        $meta_description = '';
        if (isset($pages[0]['pages_title'])) $pages_title = $pages[0]['pages_title'];
        if (isset($pages[0]['meta_keyword'])) $meta_keyword = $pages[0]['meta_keyword'];
        if (isset($pages[0]['meta_description'])) $meta_description = $pages[0]['meta_description'];
        $meta = meta_setting();
        $data['site_setting'] = site_setting();
        $this->template->write('meta_title', $pages_title . " | " . $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'] . " " . $meta_description, TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'] . " " . $meta_keyword, TRUE);


        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/content/term', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);

        $this->template->render();
    }

    /*
    Function name :contact_us()
    Parameter :none
    Return : none
    Use : user can contact admin using this page
    Description : user can contact admin using this page
    */
    function contact_us()
    {

        $meta = meta_setting();
        $site_setting = site_setting();
        $data['site_setting'] = $site_setting;
        $chk_user = 'false';
        $spamer = $this->home_model->spam_protection();

        if ($spamer == 1 || $spamer == '1') {

            $chk_user = 'true';

        }
        $data["success"] = '';
        $data["error"] = '';
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('yname', CONTACT_US_NAME, 'required|trim');
        $this->form_validation->set_rules('que', SUBJECT, 'trim');
         $this->form_validation->set_rules('yemail', CONTACT_US_EMAIL, 'required|valid_email');
        $this->form_validation->set_rules('message', CONTACT_US_MESSAGE, 'required|trim|at_least_one_letter');
        $this->form_validation->set_rules('links', CONTACT_US_LINK, 'trim|at_least_one_letter|valid_url');
        
       
        $captcha_result = '';
        
        require(base_path().'/application/libraries/ReCaptcha/src/autoload.php'); 

        $siteKey = $site_setting['captcha_private_key'];
        $secret = $site_setting['captcha_public_key'];

        
        if ($_POST)
        {
             if ($site_setting['contact_us_captcha'] == 1) { 
                    $recaptcha = new \ReCaptcha\ReCaptcha($secret);
                    $session_ip_address = $this->session->userdata('ip_address');
                    $resp = $recaptcha->verify($_POST['g-recaptcha-response'], $session_ip_address);
                    
                    if ($resp->isSuccess())
                    {
                        $captcha_result = '';
                    }
                    else
                    {
                        $captcha_result = '<p>' . IMAGE_VERIFICATION_WRONG . '</p>';
                    }
            }   
        }

        if ($this->form_validation->run() == FALSE || $captcha_result != "") {

            if (validation_errors() || $captcha_result != "") {

                $spam_message = '';

                if ($chk_user == 'true') {

                    $spam_message = '<p>' . IPBAND_CANNOT_INQUIRY . '</p>';

                }


                $data["error"] = $spam_message . validation_errors() . $captcha_result;

            } else {


                if ($chk_user == 'true') {

                    $data["error"] = '<p>' . IPBAND_CANNOT_INQUIRY . '</p>';

                } else {

                    $data["error"] = "";

                }

            }


            $data["yname"] = SecurePostData(SecurePostData($this->input->post('yname')));

            $data["yemail"] = SecurePostData(SecurePostData($this->input->post('yemail')));

            $data["message"] = SecurePostData(SecurePostData($this->input->post('message')));

            $data["about"] = SecurePostData(SecurePostData($this->input->post('about')));

            $data["links"] = SecurePostData(SecurePostData($this->input->post('links')));

            $data["que"] = SecurePostData(SecurePostData($this->input->post('que')));

        } else {


            if ($chk_user == 'true') {

                $data["error"] = '<p>' . IPBAND_CANNOT_INQUIRY . '</p>';

                $data["yname"] = "";

                $data["yemail"] = "";

                $data["message"] = "";

                $data["about"] = "";

                $data["links"] = "";

                $data["que"] = "";

            } else {


                $chk_spam_inquiry = $this->home_model->check_spam_inquiry();


                if ($chk_spam_inquiry == 1) {

                    $data["error"] = '<p>' . IPBAND_CANNOT_INQUIRY . '</p>';

                    $data["yname"] = "";

                    $data["yemail"] = "";

                    $data["message"] = "";

                    $data["about"] = "";

                    $data["links"] = "";

                    $data["que"] = "";

                } else {

                    $email_template = $this->db->query("select * from `email_template` where task='Contact Us'");

                    $email_temp = $email_template->row();

                    $email_subject = $email_temp->subject;

                    $email_message = $email_temp->message;

                    $name = $this->input->post('yname');

                    $message = '<br/>';

                    $message .= '<br/>' . CONTACT_US_QUESTIONS . " : " . $this->input->post('que');

                    $message .= '<br/>' . DETAILS . $this->input->post('message');

                    $message .= '<br/>' . CONTACT_US_LINK . " : " . $this->input->post('links');

                    $message .= '<br/>' . CONTACT_US_ABOUT . " : " . $this->input->post('about') . '<br/><br/>';

                    $email = $this->input->post('yemail');

                    $email_to = $email_temp->from_address;

                    $email_address_from = $email;

                    $email_address_reply = $email;

                    $email_message = str_replace('{break}', '<br/>', $email_message);

                    $email_message = str_replace('{name}', $name, $email_message);

                    $email_message = str_replace('{message}', $message, $email_message);

                    $email_message = str_replace('{email}', $email, $email_message);

                    $str = $email_message;

                    if (email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str)) {

                        $this->home_model->insert_inquiry();

                    }

/////////////============email===========	


                    $data["success"] = "" . INQUIRY_MESSAGE_SENT_SUCCESSFULLY . "";

                    $data["yname"] = "";

                    $data["yemail"] = "";

                    $data["message"] = "";

                    $data["about"] = "";

                    $data["links"] = "";

                    $data["que"] = "";

                }

            }
        }


        $this->template->write('meta_title', $meta['title'], TRUE);

        $this->template->write('meta_description', $meta['meta_description'], TRUE);

        $this->template->write('meta_keyword', $meta['meta_keyword'], TRUE);

        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/content/contact_us', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();

    }

    /*
    Function name :learn_more()
    Parameter :none
    Return : none
    Use : show pages from learn more module
    Description : show pages from learn more module
    */
    function learn_more()
    {

        $data['pagess'] = '';
        $data['category_details'] = get_learn_category_name();

        $data['pages_count'] = $this->content_model->learnmore(1, '', array('pages_id' => 'desc'), 'yes');

        $data['faq_count'] = $this->content_model->faq(1, 0, array('faq_category'), array('faq_order' => 'ASC'), 'yes');

        $meta_keyword = '';
        $meta_description = '';
        $meta = meta_setting();
        $pages_title = '';
        $data['site_setting'] = site_setting();
        $this->template->write('meta_title', $pages_title . " | " . $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'] . " " . $meta_description, TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'] . " " . $meta_keyword, TRUE);

        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/content/learn_more', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
    }

    /*
        Function name :learn_more_page()
        Parameter :id
        Return : none
        Use : show single page using id from learn more module
        Description : show single page using id from learn more module
        */
    function learn_more_page($id='')
    {
        $site_setting = site_setting();
        $data['learn_pages'] = $this->content_model->get_one_learnMorepage($id);

        if ($data['learn_pages'] == 0) {
            redirect('content/learn_more');
        }

        $data['sub_pages'] = $this->content_model->get_learnMorepage($data['learn_pages'][0]['learn_category'], $id);


        $meta_keyword = '';
        $meta_description = '';
        $meta = meta_setting();
        $pages_title = '';
        $data['site_setting'] = site_setting();
        $this->template->write('meta_title', $pages_title . " | " . $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'] . " " . $meta_description, TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'] . " " . $meta_keyword, TRUE);

        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/content/learn_more_page', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();


    }

    /*
        Function name :learn_more_category()
        Parameter :id
        Return : none
        Use : show learn more module category
        Description : show learn more module category
        */
    function learn_more_category($id)
    {
        $site_setting = site_setting();

        $data['learn_category_name'] = get_title_learn_more($id);
        $data['pages'] = $this->content_model->learnmore(1, $id, array('pages_id' => 'desc'));

        if ($data['pages'] == 0) {
            redirect('content/learn_more');
        }

        $meta_keyword = '';
        $meta_description = '';
        $meta = meta_setting();
        $pages_title = '';
        $data['site_setting'] = site_setting();
        $this->template->write('meta_title', $pages_title . " | " . $meta['title'], TRUE);
        $this->template->write('meta_description', $meta['meta_description'] . " " . $meta_description, TRUE);
        $this->template->write('meta_keyword', $meta['meta_keyword'] . " " . $meta_keyword, TRUE);

        $this->template->write_view('header', 'default/common/header', $data, TRUE);
        $this->template->write_view('main_content', 'default/content/learn_more_category', $data, TRUE);
        $this->template->write_view('footer', 'default/common/footer', $data, TRUE);
        $this->template->render();
    }

}

?>
