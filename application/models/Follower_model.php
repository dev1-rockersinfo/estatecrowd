<?php

/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
class Follower_model extends ROCKERS_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }


    /*
    Function name :CountAllFollowerofEquity()
    Parameter : type,user_id,joinarr,limit,order
    Return : row of all followers
    Use : Fetch counts of follower for  particular Criteria

    */
    function CountAllFollowerofEquity($type = '', $user_id = 0, $join = array('user'), $limit = 10, $order = array('equity_follow_id' => 'desc'))
    {
        $selectfields = 'equity_follower.equity_follow_id, equity_follower.equity_id';

        $this->db->select($selectfields);

        $this->db->from('equity_follower');

        if (in_array('equity', $join)) {
            $this->db->join('equity', 'equity_follower.equity_id=equity.equity_id');
        }

        if ($user_id > 0) {
            $this->db->where('equity.user_id', $user_id);
        }

        if ($type == 'equity_week') {
            $date = date('Y-m-d');
            $first_day = get_first_day_of_week($date);
            $last_day = get_last_day_of_week($date);
            $this->db->where('DATE(equity_follow_date) >=', $first_day);
            $this->db->where('DATE(equity_follow_date) <=', $last_day);
        }

        if (is_array($order)) {
            foreach ($order as $key => $val) {
                $this->db->order_by($key, $val);
            }
        }
        $this->db->limit($limit);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }

    }

    /*
    Function name :GetAllFollower()
    Parameter : is_follower_type,equity_id,user_id,joinarr,limit,order
    Return : array of all followers
    Use : Fetch all followers for particular project or equity or user

    */
    function GetAllFollower($is_follower_type = '', $equity_id = 0, $user_id = 0, $join = array('user'), $limit = 10, $order = array('property_follow_id' => 'desc'), $cnt_rec = '',$company_id='')
    {

        //default Project fields
        if ($is_follower_type == 'property_follow') {
           
            $selectfields = 'property_follower.property_follow_id, property_follower.property_id, property_follower.property_follow_user_id, property_follower.property_follow_date';
        }
         //default Project fields
        if ($is_follower_type == 'company_follow') {
            $selectfields = 'company_follower.company_follow_id, company_follower.company_id, company_follower.company_follow_user_id, company_follower.company_follow_date';
        }

        if ($is_follower_type == 'user_follow') {
            $selectfields = 'user_follow.follower_id, user_follow.follow_user_id, user_follow.follow_by_user_id, user_follow.user_follow_date';
        }


        if (in_array('user', $join)) {
            $selectfields .= ',user.user_id,user.user_name,user.last_name,user.address,user.image,user.profile_slug';
        }

        $this->db->select($selectfields);

        if ($is_follower_type == 'property_follow') {
            $this->db->from('property_follower');
        } 
        else if ($is_follower_type == 'company_follow')
        {
            $this->db->from('company_follower');
        }
        else {
            $this->db->from('user_follow');
        }

        if (in_array('user', $join)) {
            if ($is_follower_type == 'property_follow') {
                $this->db->join('user', 'property_follower.property_follow_user_id=user.user_id');
            } 
            else if ($is_follower_type == 'company_follow')
            {
                $this->db->join('user', 'company_follower.company_follow_user_id=user.user_id');
            }
            else {
                $this->db->join('user', 'user_follow.follow_by_user_id=user.user_id');
            }
        }

        if ($equity_id > 0) {
            $this->db->where('property_follower.property_id', $equity_id);
        }
        if ($company_id > 0) {
            $this->db->where('company_follower.company_id', $company_id);
        }

        if ($user_id > 0) {
            $this->db->where('user_follow.follow_user_id', $user_id);
        }

        if (is_array($order)) {
            foreach ($order as $key => $val) {
                $this->db->order_by($key, $val);
            }
        }
        $this->db->limit($limit);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            if ($cnt_rec == "count_record") {
                return $query->num_rows();
            } else {
                return $query->result_array();
            }

        } else {
            return 0;
        }

    }

    /*
    Function name :follower_list()
    Parameter : follow_user_id
    Return : 0 or 1
    Use : Fetch user follow data

    */
    function follower_list($follow_user_id)
    {

        $query = $this->db->query('select * from user_follow where follow_by_user_id=' . $this->session->userdata('user_id') . ' and follow_user_id=' . $follow_user_id);
        if ($query->num_rows() > 0) {
            return 1;
        } else {
            return 0;
        }

    }

    /*
    Function name :follow_user()
    Parameter : user_id
    Return : 0 or 1
    Use : Data insert for user follow to other user follow

    */
    function follow_user($user_id)
    {
        $data = array(
            'follow_by_user_id' => $this->session->userdata('user_id'),
            'follow_user_id' => $user_id,
            'user_follow_date' => date('Y-m-d H:i:s')
        );
        if ($this->db->insert('user_follow', $data)) {
            return 1;
        } else {
            return 0;
        }
    }

    /*
    Function name :selet_follow_user()
    Parameter : user_id
    Return : 0 or array of follower
    Use : Fetch the follow user list

    */
    function selet_follow_user($user_id)
    {
        $query = $this->db->query("select * from user_follow where follow_user_id='" . $user_id . "' and follow_by_user_id='" . $this->session->userdata('user_id') . "'");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return 0;
    }

    /*
    Function name :selet_follow_equity()
    Parameter : user_id
    Return : 0 or array of follower
    Use : Fetch the equity follow list

    */
    function selet_follow_equity($user_id)
    {
        $query = $this->db->query("select * from equity_follower where equity_follow_user_id    ='" . $user_id . "'");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return 0;
    }

    /*
    Function name :user_own_comment()
    Parameter : user_id
    Return : 0 or array of comments
    Use : Fetch own comments

    */
    function user_own_comment($user_id)
    {
        $query = $this->db->query("select * from comment where user_id='" . $user_id . "' and status=1");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return 0;
    }

    /*
    Function name :udpated_on_equity()
    Parameter : user_id
    Return : 0 or array of updates
    Use : Fetch equity wise uddates

    */
    function udpated_on_equity($equity_id)
    {
        $query = $this->db->query("select * from updates where equity_id='" . $equity_id . "'");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return 0;
    }
}

?>