<?php

/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
class Property_model extends ROCKERS_Model
{

    function __construct()
    {
        parent::__construct();
    }


    /*
    Function name :AddInsertUpdateTable()
    Parameter : key_name-> is the feild name,key_value-> value of the feild name,table-> table name,data-> data to be update.
    Return : fetch data if true or return 0 when false
    Use : It Update the user account details
    */

    function AddInsertUpdateTable($table, $key_name, $key_value, $data)
    {
        if ($key_name != '' && $key_value != '') {
            $this->db->where($key_name, $key_value);
            $query = $this->db->update($table, $data);

            return true;
        } else {
            $query = $this->db->insert($table, $data);
            return $this->db->insert_id();
        }
    }

    /*
	Function name :getTableData()
	Parameter : table-> table name,where-> data to be fetch.
	Return : fetch data if true or return 0 when false
	Use : It fetch data from any table
	*/

    function getTableData($table, $where = array(), $order = array())
    {
        if (is_array($where)) {

            $this->db->where($where);
            if (is_array($order)) {

                foreach ($order as $key => $val) {

                    $this->db->order_by($key, $val);
                }
            }
            $query = $this->db->get($table);
        } else {
            if (is_array($order)) {

                foreach ($order as $key => $val) {

                    $this->db->order_by($key, $val);
                }
            }
            $query = $this->db->get($table);
        }


        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return 0;


    }


    /*
    Function name :GetVideoLink()
    Parameter : id
    Return : project id wise video gallery
    Use : get video gallery in database



    */
    function GetVideoLink($id = null)
    {

        $project_cnt = $this->db->get_where("video_gallery", array("project_id" => $id));
        if ($project_cnt->num_rows() > 0) {
            return $project_cnt->result_array();
        }
        return 0;

    }

    /*
    Function name :GetImageLink()
    Parameter : id
    Return : project id wise image gallery
    Use : get image gallery in database



    */
    function GetImageLink($id = null)
    {
        $project_cnt = $this->db->get_where("project_gallery", array("project_id" => $id));
        if ($project_cnt->num_rows() > 0) {
            return $project_cnt->result_array();
        }
        return 0;

    }

     /*
      Function name :is_property_owner()
      Parameter : property_id
      Return : true for active
      Use : getting status for property
      */
    function is_property_creator($property_id = '', $chk_status = false)
    {

        $property = GetOneProperty($property_id);

        if ($property['user_id'] != $this->session->userdata('user_id') and $property_id > 0) {

            $property_cnt = $this->db->query("select p.property_id,i.email,i.code from invite_members as i inner join property as p on i.property_id=p.property_id inner join user as u  on u.email=i.email where i.status=1 and i.admin=1 and  i.property_id='" . $property_id . "' and i.invite_user_id='" . $this->session->userdata('user_id') . "'");


            if ($property_cnt->num_rows() > 0) {
                return true;
            }

            return false;
        }
        else
        {

            $user_type_array = array('2','3','4');
            $usercheck =  CheckUserType($user_type_array,$this->session->userdata('user_id'));
            
            if ($usercheck == 1) {
                return true;
            }
            else
            {
                return false;
            }


        }
        if ($chk_status == true) {
            $is_status = $campaign['status'];
            $status = array(0, 1);
            if (!in_array($is_status, $status)) {
                return false;
            }
        }
        return true;
    }



    /*
      Function name :is_property_owner()
      Parameter : property_id
      Return : true for active
      Use : getting status for property
      */
    function is_property_owner($property_id = '', $chk_status = false)
    {

        $campaign = GetOneCampaign($property_id);

        if ($campaign['user_id'] != $this->session->userdata('user_id') and $property_id > 0) {

            $property_cnt = $this->db->query("select c.property_id,i.email,i.code from invite_members as i inner join campaign as c on i.property_id=c.property_id inner join user as u  on u.email=i.email where i.status=1 and i.admin=1 and  i.property_id='" . $property_id . "' and i.invite_user_id='" . $this->session->userdata('user_id') . "'");


            if ($property_cnt->num_rows() > 0) {
                return true;
            }

            return false;
        }
        else
        {

            $user_type_array = array('2','3','4');
            $usercheck =  CheckUserType($user_type_array,$this->session->userdata('user_id'));
            
            if ($usercheck == 1) {
                return true;
            }
            else
            {
                return false;
            }


        }
        if ($chk_status == true) {
            $is_status = $campaign['status'];
            $status = array(0, 1);
            if (!in_array($is_status, $status)) {
                return false;
            }
        }
        return true;
    }


    /*
    Function name :property_video_gallery_data()
    Parameter :property_id
    Return : Previous Video Gallery Result Array
    Use : Previous Video Gallery Array for all or individual Equity.
    Auther : Rakesh
    */
    function property_video_gallery_data($property_id = 0, $file_video_id = 0)
    {
        $sql = "SELECT pf.* FROM `video_gallery` as pf inner join property as pr on pr.property_id=pf.property_id where 1=1";
        if ($property_id > 0) $sql .= " and pf.property_id ='" . $property_id . "'";
        if ($file_video_id > 0) $sql .= " and pf.id ='" . $file_video_id . "'";
        $sql .= " order by pf.id asc";

        $property_cnt = $this->db->query($sql);
        if ($property_cnt->num_rows() > 0) {
            return $property_cnt->result_array();
        } else {
            return 0;

        }
    }

    /*
    Function name :property_image_gallery_data()
    Parameter :property_id
    Return : Previous Image Gallery Result Array
    Use : Previous Image Gallery Array for all or individual Equity.
    Auther : Rakesh
    */
    function property_image_gallery_data($property_id = 0, $file_image_id = 0)
    {
        $sql = "SELECT pf.* FROM `property_gallery` as pf inner join property as e on e.property_id=pf.property_id where 1=1";
        if ($property_id > 0) $sql .= " and pf.property_id ='" . $property_id . "'";
        if ($file_image_id > 0) $sql .= " and pf.property_gallery_id ='" . $file_image_id . "'";
        $sql .= " order by pf.property_gallery_id asc";
        $property_cnt = $this->db->query($sql);
        if ($property_cnt->num_rows() > 0) {
            return $property_cnt->result_array();
        } else {
            return 0;

        }
    }

    /*
    Function name :property_image_gallery_data()
    Parameter :property_id
    Return : Previous Image Gallery Result Array
    Use : Previous Image Gallery Array for all or individual Equity.
    Auther : Rakesh
    */
    function property_document_data($campaign_id = 0,$property_id=0, $image_id = 0)
    {
        $sql = "SELECT cd.* FROM `campaign_document` as cd left join campaign as c on c.campaign_id=cd.campaign_id where 1=1";
        if($campaign_id > 0)$sql .= " and cd.campaign_id ='" . $campaign_id . "'";
        if($property_id > 0)$sql .= " and cd.property_id ='" . $property_id . "'";
        if ($image_id > 0) $sql .= " and cd.id ='" . $image_id . "'";
        $sql .= " order by cd.id asc";
        $campaign_cnt = $this->db->query($sql);
        if ($campaign_cnt->num_rows() > 0) {
            return $campaign_cnt->result_array();
        } else {
            return 0;

        }
    }


 

    /**
     * get master types of projects.
     * @param int $parent is it's passed non zero then returns parents types 
     * @param int $parent_id used to get child category of specific parent 
     * @return array of result
     * @access public
     * @author Rahul Patel
     */
    function getTypes($parent=0){
        $this->db->select('*');
        $this->db->where('status',1);
        $this->db->from('property_type');
        if($parent==1) $this->db->where('is_parent',$parent);

        else $this->db->where('is_parent',0);
        return $this->db->get()->result_array();
    }

    /**
     * A common function to delete single or more rows 
     * @param string $table name of the table
     * @param string $key colomn of the database
     * @param string $value value of the colomn
     * @return boolean true
     * @access public
     * @author Rahul Patel
     */
    function deleteTableData($table='',$key='',$value=''){
        $this->db->where($key,$value)->delete($table);
        return true;
    }

    /**
     * Update project types table. project has only one parent type so update everytime.
     * @param int $id project id 
     * @param array $update array of data to update single row
     * @return boolean true
     * @access public
     * @author Rahul Patel
     */
    function updateTypes($id=0,$update=array()){

        $this->db->where('property_id',$id);
        $this->db->where('is_parent',1);
        $this->db->delete('property_categories');

        $this->db->insert('property_categories',$update);
        return true;
    }

    //------Common Equity function-------
    /*
    Function name :GetAllProperties()
    Parameter : user_id,property_id,is_status,joinarr,limit,order
    Return : array of all Equities
    Use : Fetch all Equity particular Criteria

    Cases:

    case 1 : if user grater than zero then it fetch all the Equity with particular user id means it returns all Equity of  particular company owner.

    case 2 : if Equity grater than zero then it return particular Equity.


    case 3 : if Status equal to 0 than it returns Draft Equities.
             if Status equal to 1 than it returns Pending Equities.
             if Status equal to 2 than it returns Active Equities.
             if Status equal to 3 than it returns Success Equities.
             if Status equal to 4 than it returns unsuccess Equities.
             if Status equal to 5 than it returns Declined Equities.

             or

             if Status equal 0,1 than it returns both type of Equities.(Same for all others).

    case 4 : if all Equity will be sorted by given order variable values.


    */

    function GetAllProperties($user_id = 0, $property_id = '', $is_status = '', $joinarr = array('user'), $limit = 100, $order = array('property_id' => 'desc'), $offset = 0, $count = 'no',$campaign_data='no',$lead_investor_id='')
    {

        //default Equity fields
        $selectfields = 'property.*';
        //if require to join user
        if (in_array('user', $joinarr)) {
            $selectfields .= ',user.user_id,user.user_name,user.last_name,user.address, user.fb_uid, user.facebook_wall_post, user.fb_access_token,user.image,user.email,user.user_website,user.profile_slug,user.user_occupation,user.user_type,user.company_name';
        }
        if (in_array('campaign', $joinarr)) {
            $selectfields .= ',campaign.campaign_id,campaign.campaign_user_type,campaign.min_investment_amount,campaign.campaign_type,campaign.campaign_units,campaign.investment_close_date,campaign.campaign_document,campaign.investment_amount_type,campaign.campaign_units_get,campaign.date_added,campaign.investment_close_date,campaign.lead_investor_id,campaign.amount_get,campaign.status as campaign_status,campaign.reason_inactive_hidden,campaign.user_id as campaign_user_id';
        }

       
      
        if ($user_id > 0) 
        {
            $selectfields .= ',invite_members.email as inviteemail';
        }
       

        $this->db->select($selectfields);
        $this->db->from('property');


        //if require to join user
        if (in_array('user', $joinarr)) {
            $this->db->join('user', 'property.user_id=user.user_id','left');

        }
        if (in_array('property_type', $joinarr)) {
            $this->db->join('property_type', 'property.user_id=property_type.user_id');
        }
        if (in_array('campaign', $joinarr)) {
            if($campaign_data == 'yes')
            {
                $this->db->join('campaign', 'property.property_id=campaign.property_id');
            }   
            else
            {
                $this->db->join('campaign', 'property.property_id=campaign.property_id','left');
            }
        }
        if ($user_id > 0) {
            $this->db->join('invite_members', 'property.property_id=invite_members.property_id', 'left');
        }
        //check all property status
        if ($is_status != '') {

             if (in_array('campaign', $joinarr)) {
                $this->db->where('campaign.status in (' . $is_status . ')');
            }
            else
            {
                $this->db->where('property.status in (' . $is_status . ')');
            }
        }

        //check property id
        if ($property_id > 0 or $property_id != '') {
            if (is_numeric($property_id)) {
                $this->db->where('property.property_id', $property_id);
                $this->db->or_where('property.property_url', $property_id);
            } else {
                $this->db->where('property.property_url', $property_id);
            }
        }

        if (in_array('campaign', $joinarr) && $campaign_data == 'yes' && $property_id > 0) {
             $this->db->where('campaign.property_id', $property_id);
        }
        //check user_id
        if ($user_id > 0) {
            // $this->db->where('user.user_id',$user_id);
            $where = '';
            $where .= "( `user`.`user_id` = '" . $user_id . "' ";
            $sql = " ( select i.email from invite_members as i inner join property as p on i.property_id=p.property_id inner join user as u  on u.email=i.email where i.status=1 and i.admin=1 and u.user_id= '" . $user_id . "' )";
            //$sql=" ( invite_members.status=1 and invite_members.admin=1 and  invite_members.email= user.email )";
            $where .= " or `invite_members`.`email` in " . $sql . " )";

            $this->db->where($where);

        }


        if($lead_investor_id > 0)
        {
            $this->db->where('campaign.lead_investor_id', $lead_investor_id);
        } 

        if (is_array($order)) {
            foreach ($order as $key => $val) {
                $this->db->order_by($key, $val);
            }
        }

        if ($user_id > 0) {
            $this->db->group_by('property.property_id');
        }
        if ($limit > 0) $this->db->limit($limit, $offset);

        $query = $this->db->get();
      
        if ($count == 'yes') {
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else {
                return 0;
            }
        }

    }

    //------Common Equity function-------
    /*
    Function name :GetAllProperties()
    Parameter : user_id,property_id,is_status,joinarr,limit,order
    Return : array of all Equities
    Use : Fetch all Equity particular Criteria

    Cases:

    case 1 : if user grater than zero then it fetch all the Equity with particular user id means it returns all Equity of  particular company owner.

    case 2 : if Equity grater than zero then it return particular Equity.


    case 3 : if Status equal to 0 than it returns Draft Equities.
             if Status equal to 1 than it returns Pending Equities.
             if Status equal to 2 than it returns Active Equities.
             if Status equal to 3 than it returns Success Equities.
             if Status equal to 4 than it returns unsuccess Equities.
             if Status equal to 5 than it returns Declined Equities.

             or

             if Status equal 0,1 than it returns both type of Equities.(Same for all others).

    case 4 : if all Equity will be sorted by given order variable values.


    */

    function GetAllSearchProperties($user_id = 0, $property_id = '', $is_status = '', $joinarr = array('user'), $limit = 100, $order = array('property_id' => 'desc'),$search_criteria = array(), $offset = 0, $count = 'no',$campaign_data='no')
    {

        //default Equity fields
        $selectfields = 'property.*';
        //if require to join user
        if (in_array('user', $joinarr)) {
            $selectfields .= ',user.user_id,user.user_name,user.last_name,user.address, user.fb_uid, user.facebook_wall_post, user.fb_access_token,user.image,user.email,user.user_website,user.profile_slug,user.user_occupation,user.user_type,user.company_name';
        }
        if (in_array('campaign', $joinarr)) {
            $selectfields .= ',campaign.campaign_id,campaign.campaign_user_type,campaign.min_investment_amount,campaign.campaign_type,campaign.campaign_units,campaign.investment_close_date,campaign.campaign_document,campaign.investment_amount_type,campaign.campaign_units_get,campaign.date_added,campaign.investment_close_date';
        }

       
      
        if ($user_id > 0) 
        {
            $selectfields .= ',invite_members.email as inviteemail';
        }
       

        $this->db->select($selectfields);
        $this->db->from('property');


        //if require to join user
        if (in_array('user', $joinarr)) {
            $this->db->join('user', 'property.user_id=user.user_id','left');

        }
        if (in_array('property_type', $joinarr)) {
            $this->db->join('property_type', 'property.user_id=property_type.user_id');
        }
        if (in_array('campaign', $joinarr)) {
            if($campaign_data == 'yes')
            {
                $this->db->join('campaign', 'property.property_id=campaign.property_id');
            }   
            else
            {
                $this->db->join('campaign', 'property.property_id=campaign.property_id','left');
            }
        }
        if ($user_id > 0) {
            $this->db->join('invite_members', 'property.property_id=invite_members.property_id', 'left');
        }
        //check all property status
        if ($is_status != '') {
            $this->db->where('property.status in (' . $is_status . ')');
        }

        //check property id
        if ($property_id > 0 or $property_id != '') {
            if (is_numeric($property_id)) {
                $this->db->where('property.property_id', $property_id);
                $this->db->or_where('property.property_url', $property_id);
            } else {
                $this->db->where('property.property_url', $property_id);
            }
        }

        if (in_array('campaign', $joinarr) && $campaign_data == 'yes' && $property_id > 0) {
             $this->db->where('campaign.property_id', $property_id);
        }
        //check user_id
        if ($user_id > 0) {
            // $this->db->where('user.user_id',$user_id);
            $where = '';
            $where .= "( `user`.`user_id` = '" . $user_id . "' ";
            $sql = " ( select i.email from invite_members as i inner join property as p on i.property_id=p.property_id inner join user as u  on u.email=i.email where i.status=1 and i.admin=1 and u.user_id= '" . $user_id . "' )";
            //$sql=" ( invite_members.status=1 and invite_members.admin=1 and  invite_members.email= user.email )";
            $where .= " or `invite_members`.`email` in " . $sql . " )";

            $this->db->where($where);

        }

         if (is_array($search_criteria)) {


            foreach ($search_criteria as $key => $val) 
            {
                
                if ($key == 'investment_suburb' && $val != '') 
                {
                     $val_array=explode(' ', $val);
                      
                     foreach ($val_array as $value) {
                      
                        $this->db->group_start();
                        $this->db->or_like('property.property_address', $value,'both');
                       
                        $this->db->group_end();
                        
                      } 
                   

                }

                if ($key == 'investment_suburb' && $val != '') 
                {
                     $val_array=explode(' ', $val);
                      
                     foreach ($val_array as $value) {
                      
                        $this->db->group_start();
                        $this->db->or_like('property.property_address', $value,'both');
                       
                        $this->db->group_end();
                        
                      } 
                   

                }


                if ($key == 'investment_address' && $val != '') 
                {
                     $val_array=explode(' ', $val);
                      
                     foreach ($val_array as $value) {
                      
                       // $this->db->group_start();
                        $this->db->or_like('property.property_address', $value,'both');
                       
                      //  $this->db->group_end();
                      } 
                }

                if ($key == 'investment_range' && $val != '') 
                {


                    if($val == 1)
                    {
                        $min_invest_val = 1 * 1000000;
                        $max_invest_val = 2 * 1000000;

                        $this->db->where('property.min_property_investment_range>',$min_invest_val);
                        $this->db->where('property.max_property_investment_range<',$max_invest_val);
                    }
                    elseif($val == 2)
                    {
                        $min_invest_val = 3 * 1000000;
                        $max_invest_val = 4 * 1000000;
                        $this->db->where('property.min_property_investment_range>',$min_invest_val);
                        $this->db->where('property.max_property_investment_range<',$max_invest_val);
                    }
                    elseif($val == 3)
                    {
                        $min_invest_val = 5 * 1000000;
                        $max_invest_val = 6* 1000000;
                        $this->db->where('property.min_property_investment_range>',$min_invest_val);
                        $this->db->where('property.max_property_investment_range<',$max_invest_val);
                    }
                }

                if ($key == 'min_property_investment_range' && $val != '') 
                {
                    $this->db->or_where('property.min_property_investment_range>',$val);
                }
                if ($key == 'max_property_investment_range' && $val != '') 
                {
                    $this->db->or_where('property.max_property_investment_range<',$val);
                }
                if ($key == 'property_postcode' && $val != '') 
                {
                    $this->db->where('property.property_postcode' ,$val );
                }
                if ($key == 'bedrooms' && $val != '') 
                {
                    $this->db->where('property.bedrooms in (' . $val . ')');
                }
                if ($key == 'bathrooms' && $val != '') 
                {
                    $this->db->where('property.bathrooms in (' . $val . ')');
                }
                if ($key == 'cars' && $val != '') 
                {
                    $this->db->where('property.cars in (' . $val . ')');      
                }
                if ($key == 'property_type' && $val != '') 
                {
                        $val_array=explode(' ', $val);
                      
                     foreach ($val_array as $value) {
                      
                        $this->db->group_start();
                        $this->db->or_like('LOWER(property.property_type)', strtolower($value),'both');
                        $this->db->or_like('LOWER(property.property_sub_type)', strtolower($value),'both');
                       
                        $this->db->group_end();
                        
                      } 
                       
                }
         
             
            }
        }


        if (is_array($order)) {
            foreach ($order as $key => $val) {
                $this->db->order_by($key, $val);
            }
        }

        if ($user_id > 0) {
            $this->db->group_by('property.property_id');
        }
        if ($limit > 0) $this->db->limit($limit, $offset);

        $query = $this->db->get();
      
        if ($count == 'yes') {
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else {
                return 0;
            }
        }

    }
    
    /*
    Function name :delete_property()
    Parameter :property_id
    Return : none
    Use : Delete property from database

    */

    function delete_property($property_id = '')
    {
        $this->db->where('property_id', $property_id);
        $this->db->delete('property');
    }

    /*
    Function name :commentaction()
    Parameter :comment_id,type,equity_id,status,data1,comment_user_ip,type1,comment
    Return : none
    Use : Perform comments action like reply,approve,spam,add,update,delete



    */
    //============================== comment action =================================

    function commentaction($comment_id = 0, $type = '', $property_id = 0, $status = 0, $data1, $comment_user_ip = '', $type1 = '', $comment = '')
    {

        if ($type == 'approved') {


            $this->db->where('comment_id', $comment_id);
            $this->db->update('comment', array('status' => $status));


        }

        if ($type == 'spam') {

        }

        if ($type == 'decline') {

            $this->db->where('comment_id', $comment_id);
            $this->db->update('comment', array('status' => $status));
        }

        if ($type == 'reply') {
            if ($type1 == 'edit') {
                $this->db->where('comment_id', $comment_id);
                $this->db->update('comment', array('comments' => $comment));

            } else {
                $this->db->insert('comment', $data1);

            }
        }

        if ($type == 'spam') {
            $data = array('spam_ip' => $comment_user_ip,
                'spam_user_id' => SecurePostData($this->input->post('comment_user_id')),
                'reported_user_id' => $this->session->userdata('user_id')
            );
            if ($comment_user_ip != '') {
                $this->db->insert('spam_report_ip', $data);

            }

            $this->db->where('comment_id', $comment_id);
            $this->db->update('comment', array('status' => $status));

        }

        if ($type == 'delete') {
            $this->db->where('comment_id', $comment_id);
            $this->db->delete('comment');

        }


    }
     /*
    Function name :AddUpdateComment()
    Parameter : table,data,type
    Return : none
    Use : Added data to database using table and data variable



    */
    //====================function add gallery, perk, comment and gallery====================
    function AddUpdateComment($table, $data, $type = array('user'))
    {

        if (in_array('update', $type)) {
            $this->db->insert($table, $data);
            //echo $this->db->last_query();
            return true;
        }
        if (in_array('comment', $type)) {
            $this->db->insert($table, $data);
            //echo $this->db->last_query();
            return true;
        }
        if (in_array('user_comment', $type)) {
            $this->db->insert($table, $data);
            //echo $this->db->last_query();
            return true;
        }


    }

    function AddComment($table, $data)
    {

        
        $this->db->insert($table, $data);

         return $this->db->insert_id();
            //echo $this->db->last_query();
        
    }
    /*
    Function name :DeleteUpdateById()
    Parameter :key_name,key_value table,data,type
    Return : none
    Use : Delete data from database using table,key_name,key_value and data variable



    */
    //======================================= delete perk and updates and gallery =====================
    function DeleteUpdateById($key_name = '', $key_value = '', $table = '', $image_path = '', $type = array('user'))
    {

        if (in_array('update', $type)) {
            $this->db->where($key_name, $key_value);
            $this->db->delete($table);
            //echo $this->db->last_query();
            return true;
        }
    }


    /*
    Function name :UpdatePerkUpdates()
    Parameter :key_name,key_value table,data,type
    Return : none
    Use : Update data to database using table,key_name,key_value and data variable



    */
    //======================================= update perk and updates =====================

    function UpdatePerkUpdates($key_name, $key_value, $table, $data, $type = array('user'))
    {

        if (in_array('update', $type)) {
            $this->db->where($key_name, $key_value);
            $this->db->update($table, $data);
            //echo $this->db->last_query();
            return true;
        }

    }
     /*
    Function name :get_user_profile_id()
    Parameter :profile_slug
    Return :user id
    Use : fetch user id from profile_slug

    */

    function get_user_profile_id($profile_slug)
    {
        $query = $this->db->query("SELECT user_id FROM user WHERE profile_slug='" . $profile_slug . "'");

        if ($query->num_rows() > 0) {
            $result = $query->result();
            return $result[0]->user_id;
        }
    }
      /*
Function name :Equity_Counter()
Parameter :type, user_id,equity_id,equity_owner_id,is_status,joinarr,limit,order
Return : Number of total records
Use : Fetch all equity particular Criteria

Cases:
Same as above function ,but return total record for pagination


*/
    function Campaign_counter($type, $user_id = 0, $equity_id = '', $campaign_owner_id = 0, $is_status = '', $joinarr = array('user'), $limit = 10, $order = array('equity_id' => 'desc'),$offset='',$count = 'no')
    {
        $site_setting = site_setting();
        $payment_gateway = $site_setting['payment_gateway'];

        switch ($type) {
            case 'campaign':
                $selectfields = 'campaign.campaign_id';
                $this->db->select($selectfields);
                $this->db->from('campaign');
                if ($is_status != '') {
                    $this->db->where('campaign.status in (' . $is_status . ')');


                }
                if ($campaign_owner_id > 0) {
                    $this->db->where('campaign.user_id', $campaign_owner_id);
                }
                $query = $this->db->get();

                return $query->num_rows();
                break;
    

            case 'transaction':

                $selectfields = 'sum(transaction.amount) as total';
                $this->db->select($selectfields);
                $this->db->from('transaction');
                if (in_array('campaign', $joinarr)) {
                    $this->db->join('campaign', 'transaction.campaign_id=campaign.campaign_id');
                }

                if ($is_status != '') {
                    $this->db->where('campaign.status in (' . $is_status . ')');
                }
                if ($user_id > 0) {
                    $this->db->where('transaction.user_id', $user_id);
                }
                if ($campaign_owner_id > 0) {
                    $this->db->where('campaign.user_id', $campaign_owner_id);
                }
               
                $this->db->where('transaction.preapproval_status !=', 'FAIL');
                


                $query = $this->db->get();

                return $query->row_array();
                break;


                
        case 'runninginvestmentprocess':
                    
                $selectfields = '*';
                $this->db->select($selectfields);
                $this->db->from('investment_process');
                
                $this->db->join('campaign', 'investment_process.campaign_id=campaign.campaign_id');
                 $this->db->where('campaign.status in (2,3)');
                
                $this->db->where('investment_process.status in (' . $is_status . ')');
               
                if ($user_id > 0) {
                    $this->db->where('investment_process.user_id', $user_id);
                }
                    
                $query = $this->db->get();

                return $query->num_rows();
                break;     

        }
        //default Project fields
        $selectfields = 'campaign.campaign_id,campaign.user_id,campaign.investment_close_date';

        if (in_array('property', $joinarr)) {
            $selectfields .= ',property.*';
        }

        if (in_array('transaction', $joinarr)) {
            $selectfields .= ',transaction.campaign_id as tra_campaign_id,transaction.*';
            $selectfields .= ',user.*';
        }

        $this->db->select($selectfields);
        $this->db->from('campaign');
       

      
        if (in_array('property', $joinarr)) {
            $this->db->join('property', 'campaign.property_id=property.property_id');
        }
        if (in_array('transaction', $joinarr)) {
            $this->db->join('transaction', 'transaction.campaign_id=campaign.campaign_id');
            $this->db->join('user', 'transaction.user_id=user.user_id');
        }

        //check all equity status
        if ($is_status != '') {
            $this->db->where('campaign.status in (' . $is_status . ')');


        }

        //check user_id
        if ($user_id > 0) {
            $this->db->where('transaction.user_id', $user_id);
        }

        if ($campaign_owner_id > 0) {
            $this->db->where('campaign.user_id', $campaign_owner_id);
        }


        if (is_array($order)) {
            foreach ($order as $key => $val) {
                $this->db->order_by($key, $val);
            }
        }
        $this->db->limit($limit);

        $query = $this->db->get();



       if ($count == 'yes') {
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else {
                return 0;
            }
        }

    }


    function atatchment_save($data,$id){

        $this->load->helper('string');
        $new_name = random_string('alnum', 16);

        $src = "./upload/property/attachments/".$data["upload_data"]['file_name'];
        $dst = "./upload/property/attachments/".$new_name.$data["upload_data"]['file_ext'];

        copy($src,$dst);
        unlink($src);

        $insert_array = array(
                "property_id" => $id,
                "file_name" => $data["upload_data"]['file_name'],
                "new_name" => $new_name.$data["upload_data"]['file_ext'],
                "added_time" => date("Y-m-d H:i:s"),
            );

        $this->db->insert("property_attachments",$insert_array);
    }

    function getattachments($property_id,$isdelete=0){
        if($isdelete=0){
            return $this->db->select('*')->from("property_attachments")->where(array("property_id"=>$property_id,"isdelete"=>$isdelete))->get()->result_array();
        } else {
            return $this->db->select('*')->from("property_attachments")->where(array("property_id"=>$property_id))->get()->result_array();
        }
    }

}

?>