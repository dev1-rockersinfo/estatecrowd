<?php

/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
class Equity_model extends ROCKERS_Model
{

    function __construct()
    {
        parent::__construct();
    }


    //------Common Equity function-------
    /*
    Function name :GetAllEquities()
    Parameter : user_id,equity_id,is_status,joinarr,limit,order
    Return : array of all Equities
    Use : Fetch all Equity particular Criteria

    Cases:

    case 1 : if user grater than zero then it fetch all the Equity with particular user id means it returns all Equity of  particular company owner.

    case 2 : if Equity grater than zero then it return particular Equity.


    case 3 : if Status equal to 0 than it returns Draft Equities.
             if Status equal to 1 than it returns Pending Equities.
             if Status equal to 2 than it returns Active Equities.
             if Status equal to 3 than it returns Success Equities.
             if Status equal to 4 than it returns unsuccess Equities.
             if Status equal to 5 than it returns Declined Equities.

             or

             if Status equal 0,1 than it returns both type of Equities.(Same for all others).

    case 4 : if all Equity will be sorted by given order variable values.


    */

    function GetAllEquities($user_id = 0, $equity_id = '', $is_featured = 0, $is_status = '', $joinarr = array('user'), $limit = 10, $order = array('equity_id' => 'desc'), $offset = 0, $count = 'no')
    {

        //default Equity fields
        $selectfields = 'equity.equity_id,equity.equity_url,equity.date_added,equity.end_date,equity.minimum_raise,equity.goal,equity.cover_photo,
        equity.video_url,equity.video_image,equity.elevator_pitch,equity.equity_currency_code,equity.equity_currency_symbol,equity.status,
        equity.amount_get,equity.deal_highlight1,equity.deal_highlight2,equity.deal_highlight3,equity.deal_highlight4,equity.elevator_pitch,equity.deal_type_name,
        equity.available_shares,equity.price_per_share,equity.equity_available,equity.company_valuation,equity.interest,equity.term_length,equity.valuation_cap,equity.conversation_discount,equity.warrant_coverage,
        equity.debt_interest,equity.debt_term_length,equity.return,equity.return_percentage,equity.maximum_return,equity.payment_frequency,equity.payment_start_date,equity.company_fundraising,equity.date_round_open,
        equity.amount_raise_current_round,equity.maximum_raise,equity.funding_type,equity.executive_summary_status,equity.executive_summary_file,equity.term_sheet_status,equity.term_sheet_file,
        equity.reason_inactive_hidden,equity.cash_flow_status,equity.project_timezone,equity.allowed_investor,
        equity.investment_summary_text,equity.deal_highlight_title1,equity.deal_highlight_description1,equity.deal_highlight_title2,equity.deal_highlight_description2,equity.deal_highlight_title3,equity.deal_highlight_description3,
        equity.deal_highlight_title4,equity.deal_highlight_description4,equity.project_description,equity.market_summary_text,equity.market_summary_image,equity.project_city,equity.project_state,equity.project_street,equity.project_country,equity.project_year_build,
        equity.main_phase,equity.sub_phase,equity.company_id,equity.project_name,equity.project_lot_size,equity.tax_relief_type,equity.bank_name,equity.bank_account_type,equity.bank_account_number,equity.routing_number,equity.contract_copy_file';



        //if require to join user
        if (in_array('user', $joinarr)) {
            $selectfields .= ',user.user_id,user.user_name,user.last_name,user.address, user.fb_uid, user.facebook_wall_post, user.fb_access_token,user.image,user.email,user.user_website,user.profile_slug,user.user_occupation';
        }
        if ($user_id > 0) {
            $selectfields .= ',invite_members.email as inviteemail';
        }
       
        $selectfields .= ',company_profile.company_name,company_profile.company_overview,company_profile.company_logo,company_profile.headquater_city,company_profile.headquater_state,company_profile.headquater_country,company_profile.company_logo,company_profile.company_linkedin_url,company_profile.company_facebook_url,company_profile.company_twitter_url,company_profile.website_url,company_profile.company_category,company_profile.year_founded,company_profile.company_industry,company_profile.assets_under_management,company_profile.square_footage,company_profile.company_email,company_profile.company_cover_photo,company_profile.company_url';
        

        $this->db->select($selectfields);
        $this->db->from('equity');


        //if require to join user
        if (in_array('user', $joinarr)) {
            $this->db->join('user', 'equity.user_id=user.user_id');

        }
        if ($user_id > 0) {
            $this->db->join('invite_members', 'equity.equity_id=invite_members.equity_id', 'left');
        }
       
        $this->db->join('company_profile', 'equity.company_id=company_profile.company_id','left');



        //check all equity status
        if ($is_status != '') {
            $this->db->where('equity.status in (' . $is_status . ')');
        }


        //check equity id
        if ($equity_id > 0 or $equity_id != '') {
            if ($equity_id > 0) {
                $this->db->where('equity.equity_id', $equity_id);
                $this->db->or_where('equity.equity_url', $equity_id);
            } else {
                $this->db->where('equity.equity_url', $equity_id);
            }
        }

        //check user_id
        if ($user_id > 0) {
            // $this->db->where('user.user_id',$user_id);
            $where = '';
            $where .= "( `user`.`user_id` = '" . $user_id . "' ";
            $sql = " ( select i.email from invite_members as i inner join equity as e on i.equity_id=e.equity_id inner join user as u  on u.email=i.email where i.status=1 and i.admin=1 and u.user_id= '" . $user_id . "' )";
            //$sql=" ( invite_members.status=1 and invite_members.admin=1 and  invite_members.email= user.email )";
            $where .= " or `invite_members`.`email` in " . $sql . " )";

            $this->db->where($where);

        }
        if (is_array($order)) {
            foreach ($order as $key => $val) {
                $this->db->order_by($key, $val);
            }
        }

        if ($user_id > 0) {
            $this->db->group_by('equity.equity_id');
        }
        if ($limit > 0) $this->db->limit($limit, $offset);

        $query = $this->db->get();
        //  echo "<br><br><br>";
        //          echo $this->db->last_query();
        // die;
        //              echo "<br><br><br>";
        if ($count == 'yes') {
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else {
                return 0;
            }
        }

    }

    /*
    Function name :GetAllSearchEquities()
    Parameter :is_featured,is_status,joinarr,limit,order,search_criteria,weekly_equity,duration
    Return : Return equities
    Use : Fetch all equity with search criteria


    */
    function GetAllSearchEquities($is_featured = 0, $is_status = '', $joinarr = array('user'), $limit = 10, $order = array('equity_id' => 'desc'), $search_criteria = array(), $weekly_equity = '', $duration = '')
    {

        $site_setting = site_setting();

        $ending_days = $site_setting['ending_days'] + 1;

        if ($duration == 'end_soon')// after tendays from current date
        {
            $end_date = date("Y-m-d H:i:s", mktime(0, 0, 0, date('m'), intval(date('d') + $ending_days), date('y')));

        }

        //default Project fields
        $selectfields = 'equity.equity_id,equity.equity_url,equity.date_added,equity.end_date,equity.minimum_raise,equity.goal,equity.cover_photo,equity.video_url,equity.video_image,equity.elevator_pitch,equity.equity_currency_code,equity.equity_currency_symbol,equity.status,equity.amount_get,equity.deal_highlight1,equity.deal_highlight2,equity.deal_highlight3,equity.deal_highlight4,equity.elevator_pitch,equity.deal_type_name,equity.available_shares,equity.price_per_share,equity.equity_available,equity.company_valuation,equity.interest,equity.term_length,equity.valuation_cap,equity.conversation_discount,equity.warrant_coverage,equity.debt_interest,equity.debt_term_length,equity.return,equity.return_percentage,equity.maximum_return,equity.payment_frequency,equity.payment_start_date,equity.company_fundraising,equity.date_round_open,equity.cash_flow_status,equity.project_timezone,equity.company_id,equity.project_name,equity.tax_relief_type';
       
        //if require to join category
        if (in_array('categories', $joinarr)) {
            $selectfields .= ',categories.name,categories.url';
        }
        //if require to join user
        if (in_array('user', $joinarr)) {
            $selectfields .= ',user.user_id,user.user_name,user.last_name,user.address,user.image,user.email,user.user_website,user.profile_slug';
        }

        $selectfields .= ',company_profile.company_name,company_profile.company_name,company_profile.company_overview,company_profile.company_logo,company_profile.headquater_city,company_profile.headquater_state,company_profile.headquater_country,company_profile.company_logo,company_profile.company_linkedin_url,company_profile.company_facebook_url,company_profile.company_twitter_url,company_profile.website_url,company_profile.company_category,company_profile.year_founded,company_profile.company_industry,company_profile.assets_under_management,company_profile.square_footage,company_profile.company_email,company_profile.company_cover_photo,company_profile.company_url';
        
        $this->db->select($selectfields);
        $this->db->from('equity');
        if (in_array('equity_categories', $joinarr)) {
            $this->db->join('equity_categories', 'equity.equity_id=equity_categories.equity_id');
        }
        //if require to join category
        if (in_array('categories', $joinarr)) {
            $this->db->join('categories', 'equity_categories.category_id=categories.id');
        }

        //if require to join user
        if (in_array('user', $joinarr)) {
            $this->db->join('user', 'equity.user_id=user.user_id');
        }
       
        $this->db->join('company_profile', 'equity.company_id=company_profile.company_id','left');


        //check featured Project status
        if ($is_featured > 0) {
            // if only for the featured Project
            if ($is_featured == 1) {
                $this->db->where('is_featured', $is_featured);
            }
            // if only for the non-featured Project
            if ($is_featured == 2) {
                $this->db->where('is_featured', 0);
            }
        }

        //check all equity status
        if ($is_status != '') {
            $this->db->where('equity.status in (' . $is_status . ')');


        }
        if ($weekly_equity != '') {
            $this->db->where('equity.date_added >', $weekly_equity);

        }
        //check ending_soon

        if ($duration == 'end_soon') {
            $this->db->where('equity.end_date >=', date('Y-m-d H:i:s'));
            $this->db->where('equity.end_date <', $end_date);
        }


        //check equity_id

        $count_search = count($search_criteria);

        if (is_array($search_criteria)) {
            foreach ($search_criteria as $key => $val) {
                if ($key == 'keyword' && $val != '') {
                     $val_array=explode(' ', $val);
                      
                     foreach ($val_array as $value) {
                      
                        $this->db->group_start();
                        $this->db->or_like('equity.project_name', $value,'both');
                        $this->db->or_like('equity.project_description', $value,'both');
                        $this->db->or_like('company_profile.company_name', $value,'both');
                        $this->db->or_like('company_profile.company_overview', $value,'both');
                        $this->db->group_end();
                        
                      } 
                   

                }
                if ($key == 'title' && $val != '') {

                    $val_array=explode(' ', trim($val));
                     $i=0;
                      $this->db->group_start();
                     foreach ($val_array as $value) {
                        if($i==0){
                         
                          $this->db->like('`equity`.`project_name`', $value,'both');
                          $this->db->or_like('`company_profile`.`company_name`', $value,'both');
                          
                        }else{
                               
                            $this->db->or_like('`equity`.`project_name`', $value,'both');
                            $this->db->or_like('`company_profile`.`company_name`', $value,'both');
                                                       
                        }
                       $i++;
                      }
                       $this->db->group_end();     
                }

                if ($key == 'category' && $val != '') {
                   
                    if ($val != '' && $count_search < 2) $this->db->where('url', $val);
                    else $this->db->where('categories.name', $val);

                }
                if ($key == 'city' && $val != '') {
                    $this->db->group_start();
                    $this->db->like('project_city', $val);
                    $this->db->or_like('company_profile.headquater_city', $val);
                    $this->db->group_end();
                }
                if ($key == 'state' && $val != '') {
                    $this->db->group_start();
                    $this->db->where('project_state', $val);
                    $this->db->or_where('company_profile.headquater_state', $val);
                    $this->db->group_end();
                }


                if ($key == 'goal_size' && $val != '' && $val > 0) {
                    if ($val > 0) {
                        //$this->db->select('if(amount>0,if(((amount_get/amount)*100)>0,((amount_get/amount)*100),0),0) as `goal`',false);

                        if ($val == 1) {
                            $goal_size_from = "0";
                            $goal_size_to = "25";
                            $this->db->where('if(goal>0,if(((amount_get/goal)*100)>0,((amount_get/goal)*100),0),0) BETWEEN ' . $goal_size_from . ' AND ' . $goal_size_to);

                        }
                        if ($val == 2) {
                            $goal_size_from = "25";
                            $goal_size_to = "50";
                            $this->db->where('if(goal>0,if(((amount_get/goal)*100)>0,((amount_get/goal)*100),0),0) BETWEEN ' . $goal_size_from . ' AND ' . $goal_size_to);

                        }
                        if ($val == 3) {
                            $goal_size_from = "50";
                            $goal_size_to = "75";
                            $this->db->where('if(goal>0,if(((amount_get/goal)*100)>0,((amount_get/goal)*100),0),0) BETWEEN ' . $goal_size_from . ' AND ' . $goal_size_to);

                        }
                        if ($val == 4) {
                            $goal_size_from = "75";
                            $goal_size_to = "1000";
                            $this->db->where('if(goal>0,if(((amount_get/goal)*100)>0,((amount_get/goal)*100),0),0) BETWEEN ' . $goal_size_from . ' AND ' . $goal_size_to);

                        }
                    }
                }
                 if ($key == 'goal_type' && $val != '') {
                    if ($val > 0) {
                        //$this->db->select('if(amount>0,if(((amount_get/amount)*100)>0,((amount_get/amount)*100),0),0) as `goal`',false);

                            if ($val == 1) {
                                
                                $this->db->where('equity.funding_type',"Flexible");

                            }
                            if ($val == 2) {
                               $this->db->where('equity.funding_type',"Fixed");
                            }
                    }
                }
                if ($key == 'status' && $val != '') {
                    //for all val==0.
                    if ($val > 0) { 
                        //1 for open ,meas status should be 2
                        $is_status = 2;
                        if ($val == 1) {
                            $this->db->where('equity.status in (' . $is_status . ')');
                        }
                        $is_status = 3;
                        if ($val == 2) {
                            $this->db->where('equity.status  in (' . $is_status . ')');
                        }
                        $is_status = "3,4,5";
                        if ($val == 3) {
                            $this->db->where('equity.status  in (' . $is_status . ')');
                        }
                    } 
                    else if($val == 0)
                    {
                         $is_status = "2,3,4,5";
                         $this->db->where('equity.status in (' . $is_status . ')');
                    }
                    else {
                        $is_status = 2;
                        $this->db->where('equity.status  in (' . $is_status . ')');
                    }
                }else  if ($key == 'status' && $val == '') {
                       $is_status = "2,3,4,5";
                         $this->db->where('equity.status in (' . $is_status . ')');
                }
            }
        }

        if (is_array($order)) {
            foreach ($order as $key => $val) {
                $this->db->order_by($key, $val);
            }
        }

        $this->db->group_by('equity.equity_id');
      
        $this->db->limit($limit);

        $query = $this->db->get();
        
       
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }

    }

    /*
Function name :Equity_Counter()
Parameter :type, user_id,equity_id,equity_owner_id,is_status,joinarr,limit,order
Return : Number of total records
Use : Fetch all equity particular Criteria

Cases:
Same as above function ,but return total record for pagination


*/
    function Equity_Counter($type, $user_id = 0, $equity_id = '', $equity_owner_id = 0, $is_status = '', $joinarr = array('user'), $limit = 10, $order = array('equity_id' => 'desc'))
    {
        $site_setting = site_setting();
        $payment_gateway = $site_setting['payment_gateway'];

        switch ($type) {
            case 'equity':
                $selectfields = 'equity.equity_id';
                $this->db->select($selectfields);
                $this->db->from('equity');
                if ($is_status != '') {
                    $this->db->where('equity.status in (' . $is_status . ')');


                }
                if ($equity_owner_id > 0) {
                    $this->db->where('equity.user_id', $equity_owner_id);
                }
                $query = $this->db->get();

                return $query->num_rows();
                break;
            case 'equity_week':
                $selectfields = 'equity.equity_id';
                $this->db->select($selectfields);
                $this->db->from('equity');

                $date = date('Y-m-d');
                $first_day = get_first_day_of_week($date);
                $last_day = get_last_day_of_week($date);
                if ($is_status != '') {
                    $this->db->where('equity.status in (' . $is_status . ')');


                }
                if ($equity_owner_id > 0) {
                    $this->db->where('equity.user_id', $equity_owner_id);
                }

                $this->db->where('DATE(date_added) >=', $first_day);
                $this->db->where('DATE(date_added) <=', $last_day);

                $query = $this->db->get();

                return $query->num_rows();
                break;

            case 'transaction':

                $selectfields = 'sum(transaction.amount) as total';
                $this->db->select($selectfields);
                $this->db->from('transaction');
                if (in_array('equity', $joinarr)) {
                    $this->db->join('equity', 'transaction.equity_id=equity.equity_id');
                }

                if ($is_status != '') {
                    $this->db->where('equity.status in (' . $is_status . ')');
                }
                if ($user_id > 0) {
                    $this->db->where('transaction.user_id', $user_id);
                }
                if ($equity_owner_id > 0) {
                    $this->db->where('equity.user_id', $equity_owner_id);
                }
                if ($payment_gateway == '0') {
                    $this->db->where('transaction.wallet_payment_status !=', '2');
                } else {
                    $this->db->where('transaction.preapproval_status !=', 'FAIL');
                }


                $query = $this->db->get();

                return $query->row_array();
                break;


            case 'transaction_currency':


                $selectfields = 'sum(transaction.preapproval_total_amount) as total,equity.equity_currency_code ,equity.equity_currency_symbol';

                if ($equity_owner_id > 0) {
                    $selectfields = 'sum(transaction.amount) as total,equity.equity_currency_code ,equity.equity_currency_symbol';
                }

                $this->db->select($selectfields);
                $this->db->from('transaction');
                if (in_array('equity', $joinarr)) {
                    $this->db->join('equity', 'transaction.equity_id=equity.equity_id');
                }

                if ($is_status != '') {
                    $this->db->where('equity.status in (' . $is_status . ')');


                }

                if ($user_id > 0) {
                    $this->db->where('transaction.user_id', $user_id);

                }
                if ($equity_owner_id > 0) {
                    $this->db->where('equity.user_id', $equity_owner_id);
                }
                if ($payment_gateway == '0') {
                    $this->db->where('transaction.wallet_payment_status !=', '2');
                } else {
                    $this->db->where('transaction.preapproval_status !=', 'FAIL');
                }
                $this->db->group_by('equity.equity_currency_code');
                $query = $this->db->get();

                return $query->result_array();
                break;


            case 'transaction_weekly':
                $selectfields = 'sum(transaction.amount) as total, sum(equity.goal) as equity_amount';
                $this->db->select($selectfields);
                $this->db->from('transaction');

                $date = date('Y-m-d');
                $first_day = get_first_day_of_week($date);
                $last_day = get_last_day_of_week($date);
                if (in_array('equity', $joinarr)) {
                    $this->db->join('equity', 'transaction.equity_id=equity.equity_id');
                }

                if ($is_status != '') {
                    $this->db->where('equity.status in (' . $is_status . ')');


                }

                if ($user_id > 0) {
                    $this->db->where('transaction.user_id', $user_id);

                }
                if ($equity_owner_id > 0) {
                    $this->db->where('equity.user_id', $equity_owner_id);
                }

                $this->db->where('DATE(transaction_date_time) >=', $first_day);
                $this->db->where('DATE(transaction_date_time) <=', $last_day);
                if ($payment_gateway == '0') {
                    $this->db->where('transaction.wallet_payment_status !=', '2');
                } else {
                    $this->db->where('transaction.preapproval_status !=', 'FAIL');
                }
                $query = $this->db->get();

                return $query->row_array();
                break;

            case 'comment_weekly':

                $selectfields = 'comment.comment_id';
                $this->db->select($selectfields);
                $this->db->from('comment');

                $date = date('Y-m-d');
                $first_day = get_first_day_of_week($date);
                $last_day = get_last_day_of_week($date);

                if (in_array('equity', $joinarr)) {
                    $this->db->join('equity', 'comment.equity_id=equity.equity_id');
                }

                if ($user_id > 0) {
                    $this->db->where('equity.user_id', $user_id);
                }


                if ($equity_owner_id > 0) {
                    $this->db->where('equity.user_id', $equity_owner_id);
                }

                $this->db->where('DATE(comment.date_added) >=', $first_day);
                $this->db->where('DATE(comment.date_added) <=', $last_day);

                if ($is_status != '') {
                    $this->db->where('equity.status in (' . $is_status . ')');
                }

                $query = $this->db->get();

                return $query->num_rows();
                break;
                
        case 'runninginvestmentprocess':
                    
                $selectfields = '*';
                $this->db->select($selectfields);
                $this->db->from('equity_investment_process');
                
                $this->db->join('equity', 'equity_investment_process.equity_id=equity.equity_id');
                 $this->db->where('equity.status in (2,3)');
                
                $this->db->where('equity_investment_process.status in (' . $is_status . ')');
               
                if ($user_id > 0) {
                    $this->db->where('equity_investment_process.user_id', $user_id);
                }
                    
                $query = $this->db->get();

                return $query->num_rows();
                break;     

        }
        //default Project fields
        $selectfields = 'equity.equity_id';

        $this->db->select($selectfields);
        $this->db->from('equity');
        //if require to join category
        if (in_array('company_category', $joinarr)) {
            $this->db->join('company_category', 'equity.company_category=company_category.company_category_name');
        }

        //if require to join user
        if (in_array('user', $joinarr)) {
            $this->db->join('user', 'equity.user_id=user.user_id');
        }
        if (in_array('transaction', $join)) {
            $this->db->join('transaction', 'transaction.equity_id=equity.equity_id');
            $this->db->join('user', 'transaction.user_id=user.user_id');
        }

        //check all equity status
        if ($is_status != '') {
            $this->db->where('equity.status in (' . $is_status . ')');


        }

        //check user_id
        if ($user_id > 0) {
            $this->db->where('transaction.user_id', $user_id);
        }

        if ($equity_owner_id > 0) {
            $this->db->where('equity.user_id', $equity_owner_id);
        }


        if (is_array($order)) {
            foreach ($order as $key => $val) {
                $this->db->order_by($key, $val);
            }
        }
        $this->db->limit($limit);

        $query = $this->db->get();

        return $query->num_rows();

    }

    /*
    Function name :get_all_equity_gallery($id)
    Parameter : id
    Return : equity id wise equity gallery
    Use : get equity gallery in database

    */

    function get_all_equity_gallery($id)
    {

        $query = $this->db->query("select * from equity_gallery where equity_id='" . $id . "' order by equity_gallery_id asc ");

        if ($query->num_rows() > 0) {
            return $query->result_array();
        }

        return 0;
    }

    /*
Function name :GetAccessRequest()
Parameter :user_id,GetAccessRequest,joinarr,limit,order,offset,count,is_interest_request_status
Return : Return all access request
Use : Fetch all request from detail page


*/

    function GetAccessRequest($user_id = 0, $equity_id = 0, $is_access_request_status = '', $join = array('access_request'), $limit = 10, $order = array('user_id' => 'desc'), $offset = 0, $count = 'no', $is_interest_request_status = '')
    {
        $selectfields = 'user.user_name,user.last_name,user.user_id,user.image,user.profile_slug,user.user_occupation,user.address';


        if (in_array('access_request', $join)) {
            $selectfields .= ',access_request.updates,access_request.user_id as access_user_id,access_request.comments,access_request.funders,access_request.docs_media,access_request.request_name,access_request.access_request_id,access_request.deny_reason,access_request.reason_save';
            $this->db->group_by('access_request.access_request_id');
            if (in_array('equity', $join)) {
                $selectfields .= ',equity.deny_reason_access,equity.reason_save_aceess';
            }
            //if require to join commen
        }
        if (in_array('accreditation', $join)) {
            $selectfields .= ',accreditation.accreditation_status';

        }

        //  if (in_array('equity_investment_process', $join)) {
        //     $selectfields .= ',equity_investment_process.invest_status_id';

        // }

        if (in_array('interest_request', $join)) {
            $selectfields .= ',interest_request.status,interest_request.payment_status,interest_request.user_id as interest_request_user_id,interest_request.interest_request_id,interest_request.equity_id as interest_request_equity_id';
            $this->db->group_by('interest_request.interest_request_id');
            if (in_array('equity', $join)) {
                $selectfields .= ',equity.deny_reason_interest,equity.reason_save_interest';
            }
        }


        $this->db->select($selectfields);

        $this->db->from('user');

        //if require to join comment


        if (in_array('access_request', $join)) {
            $this->db->join('access_request', 'user.user_id=access_request.user_id');
            if (in_array('equity', $join)) {
                $this->db->join('equity', 'access_request.equity_id=equity.equity_id');
            }
        }

        if (in_array('accreditation', $join)) {
            $this->db->join('accreditation', 'accreditation.user_id=user.user_id', 'left');
        }

        // if (in_array('equity_investment_process', $join)) {
        //     $this->db->join('equity_investment_process', 'equity_investment_process.user_id=user.user_id', 'left');
        // }
        if (in_array('interest_request', $join)) {
            $this->db->join('interest_request', 'interest_request.user_id=user.user_id');
            if (in_array('equity', $join)) {
                $this->db->join('equity', 'interest_request.equity_id=equity.equity_id');
            }
        }
        if (in_array('access_request', $join)) {
            $this->db->where('access_request.equity_id', $equity_id);
        }
        if (in_array('interest_request', $join)) {
            $this->db->where('interest_request.equity_id', $equity_id);
        }
        if ($is_access_request_status != '') {


            $where = '';
            if ($count == 'yes') {
                
                $where .= "( `access_request`.`comments` in (1) ";
                $where .= "or `access_request`.`updates` in (1) ";
                $where .= "or `access_request`.`funders` in (1) ";
                $where .= " or `access_request`.`docs_media` in (1) )";

               
            } else {
          
                 $where .= "( `access_request`.`comments` in (" . $is_access_request_status . ") ";
                $where .= "or `access_request`.`updates` in (" . $is_access_request_status . ") ";
                $where .= "or `access_request`.`funders` in (" . $is_access_request_status . ") ";
                $where .= " or `access_request`.`docs_media` in (" . $is_access_request_status . ") )";

            }
            $this->db->where($where);
        }
        if ($is_interest_request_status != '') {
            $this->db->where('interest_request.status in (' . $is_interest_request_status . ')');

        }


        if (is_array($order)) {
            foreach ($order as $key => $val) {
                $this->db->order_by($key, $val);
            }
        }
        if ($limit > 0 || $offset > 0) $this->db->limit($limit, $offset);


        $query = $this->db->get();
        //echo $this->db->last_query();die;
        if ($count == 'yes') {
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else {
                return 0;
            }
        }
    }

    /*
    Function name :GetEquityCompanyIndustry()
    Parameter :equity_id,joinarr,limit,order
    Return : Return all company industry equity wise
    Use : Fetch all company industry equity wise


    */

    function GetEquityCompanyIndustry($equity_id = '', $join = array('company_industry'), $limit = 10, $order = array('company_industry_id' => 'desc'))
    {
        $selectfields = '';


        if (in_array('company_industry', $join)) {
            $selectfields .= ',company_industry.company_industry_name';
        }


        $this->db->select($selectfields);

        $this->db->from('equity_company_industry');

        //if require to join comment


        if (in_array('company_industry', $join)) {
            $this->db->join('company_industry', 'equity_company_industry.company_industry_id=company_industry.id', 'left');
        }

        if (in_array('equity', $join)) {
            $this->db->join('equity', 'equity_company_industry.equity_id=equity.equity_id', 'left');
        }

        if ($equity_id > 0) {
            $this->db->where('equity_company_industry.equity_id', $equity_id);
        }

        if (is_array($order)) {
            foreach ($order as $key => $val) {
                $this->db->order_by($key, $val);
            }
        }
        if ($limit > 0) $this->db->limit($limit);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }

    }

    /*
    Function name :GetHigherCategoriesCompany()
    Parameter : no
    Return : Return all company Category
    Use : Fetch all company category

    */


    function GetHigherCategoriesCompany()
    {
       
       $this->db->where(array('categories.active' => 1));
        //$this->db->order_by('project_category_name','asc');
       if ($_SESSION['lang_code'] != '')
            $this->db->where('iso2', $_SESSION['lang_code']);
        if ($_SESSION['lang_folder'] != '')
            $this->db->where('language_folder', $_SESSION['lang_folder']);

        $this->db->from('categories');

        $this->db->join('language', 'language.language_id = categories.language_id', 'inner');
        
        $this->db->order_by('id', 'RANDOM');
       
        $this->db->limit(5);
        $query = $this->db->get();
       //cho $this->db->last_query();die;
        if ($query->num_rows()) {
            return $result = $query->result();
        } else {
            return 0;
        }
    }

    /*
    Function name :get_all_dynamic_slider()
    Parameter : no
    Return : Return all slider content
    Use : Fetch all slider content

    */
    function get_all_dynamic_slider()
    {

        $query = $this->db->query("select * from dynamic_slider where active='1' order by dynamic_slider_id desc ");

        if ($query->num_rows() > 0) {
            return $query->result_array();
        }

        return 0;
    }

    /*
    Function name :commentaction()
    Parameter :comment_id,type,equity_id,status,data1,comment_user_ip,type1,comment
    Return : none
    Use : Perform comments action like reply,approve,spam,add,update,delete



    */
    //============================== comment action =================================

    function commentaction($comment_id = 0, $type = '', $equity_id = 0, $status = 0, $data1, $comment_user_ip = '', $type1 = '', $comment = '')
    {

        if ($type == 'approved') {


            $this->db->where('comment_id', $comment_id);
            $this->db->update('comment', array('status' => $status));


        }

        if ($type == 'spam') {

        }

        if ($type == 'decline') {

            $this->db->where('comment_id', $comment_id);
            $this->db->update('comment', array('status' => $status));
        }

        if ($type == 'reply') {
            if ($type1 == 'edit') {
                $this->db->where('comment_id', $comment_id);
                $this->db->update('comment', array('comments' => $comment));

            } else {
                $this->db->insert('comment', $data1);

            }
        }

        if ($type == 'spam') {
            $data = array('spam_ip' => $comment_user_ip,
                'spam_user_id' => SecurePostData($this->input->post('comment_user_id')),
                'reported_user_id' => $this->session->userdata('user_id')
            );
            if ($comment_user_ip != '') {
                $this->db->insert('spam_report_ip', $data);

            }

            $this->db->where('comment_id', $comment_id);
            $this->db->update('comment', array('status' => $status));

        }

        if ($type == 'delete') {
            $this->db->where('comment_id', $comment_id);
            $this->db->delete('comment');

        }


    }

    
    /*
    Function name :AddUpdateComment()
    Parameter : table,data,type
    Return : none
    Use : Added data to database using table and data variable



    */
    //====================function add gallery, perk, comment and gallery====================
    function AddUpdateComment($table, $data, $type = array('user'))
    {

        if (in_array('update', $type)) {
            $this->db->insert($table, $data);
            //echo $this->db->last_query();
            return true;
        }
        if (in_array('comment', $type)) {
            $this->db->insert($table, $data);
            //echo $this->db->last_query();
            return true;
        }

    }

    
    /*
    Function name :DeleteUpdateById()
    Parameter :key_name,key_value table,data,type
    Return : none
    Use : Delete data from database using table,key_name,key_value and data variable



    */
    //======================================= delete perk and updates and gallery =====================
    function DeleteUpdateById($key_name = '', $key_value = '', $table = '', $image_path = '', $type = array('user'))
    {

        if (in_array('update', $type)) {
            $this->db->where($key_name, $key_value);
            $this->db->delete($table);
            //echo $this->db->last_query();
            return true;
        }
    }


    /*
    Function name :UpdatePerkUpdates()
    Parameter :key_name,key_value table,data,type
    Return : none
    Use : Update data to database using table,key_name,key_value and data variable



    */
    //======================================= update perk and updates =====================

    function UpdatePerkUpdates($key_name, $key_value, $table, $data, $type = array('user'))
    {

        if (in_array('update', $type)) {
            $this->db->where($key_name, $key_value);
            $this->db->update($table, $data);
            //echo $this->db->last_query();
            return true;
        }

    }

    /*
    Function name :delete_equity()
    Parameter :equity_id
    Return : none
    Use : Delete equity from database

    */

    function delete_equity($equity_id = '')
    {
        $this->db->where('equity_id', $equity_id);
        $this->db->delete('equity');
    }

    /*
    Function name :get_user_profile_id()
    Parameter :profile_slug
    Return :user id
    Use : fetch user id from profile_slug

    */

    function get_user_profile_id($profile_slug)
    {
        $query = $this->db->query("SELECT user_id FROM user WHERE profile_slug='" . $profile_slug . "'");

        if ($query->num_rows() > 0) {
            $result = $query->result();
            return $result[0]->user_id;
        }
    }

    function getProjectTypes($equity_id=0){

        $this->db->select('categories.name,equity_categories.id,categories.is_parent,equity_categories.equity_id');
        $this->db->from('equity_categories');
        $this->db->join('categories','categories.id=equity_categories.category_id');
        $this->db->where('equity_categories.equity_id',$equity_id);

        return $this->db->get()->result_array();
    }

    function getEquityHighlights($id){
        return $this->db->get_where('equity_highlights',array('equity_id'=>$id))->result_array();
    }


}

?>