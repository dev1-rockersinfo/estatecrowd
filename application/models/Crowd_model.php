<?php

/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
class Crowd_model extends ROCKERS_Model
{

    function __construct()
    {
        parent::__construct();
    }

      /*
    Function name :AddUpdateData()
    Parameter :string $table, array $data, integer $value, string $key
    Return : $value
    Use : update and insert data into table
    */

    function AddUpdateData($table = '', $data = '', $value = '', $key = '')
    {

        if ($value) {

            $this->db->where($key, $value);
            $this->db->update($table, $data);
            return $value;

        } else {

            $this->db->insert($table, $data);
            return $this->db->insert_id();

        }

    }
      /*
    Function name :AddUpdateData()
    Parameter :string $table, array $data, integer $value, string $key
    Return : $value
    Use : update and insert data into table
    */

    function deleteData($table = '', $data = '')
    {
            $this->db->where($data);
            $this->db->delete($table);

    }

     /*
    Function name :getTableData()
    Parameter : table-> table name,where-> data to be fetch.
    Return : fetch data if true or return 0 when false
    Use : It fetch data from any table
    */

    function getTableData($table, $where = array(), $order = array())
    {
        if (is_array($where)) {

            $this->db->where($where);
            if (is_array($order)) {

                foreach ($order as $key => $val) {

                    $this->db->order_by($key, $val);
                }
            }
            $query = $this->db->get($table);
        } else {
            if (is_array($order)) {

                foreach ($order as $key => $val) {

                    $this->db->order_by($key, $val);
                }
            }
            $query = $this->db->get($table);
        }


        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return 0;


    }

    function GetAllCrowdComments($crowd_id='',$comment_id='', $joinarr = array('user'), $limit = 100, $order = array('id' => 'asc'))
    {

        //default Equity fields
        $selectfields = 'crowd_comment.*,crowd_comment.image as comment_image';
        //if require to join user
        if (in_array('user', $joinarr)) {
            $selectfields .= ',user.user_id,user.user_name,user.last_name,user.address, user.fb_uid, user.facebook_wall_post, user.fb_access_token,user.image,user.email,user.user_website,user.profile_slug,user.user_occupation,user.user_type';
        }
      

        $this->db->select($selectfields);
        $this->db->from('crowd_comment');


        //if require to join user
        if (in_array('user', $joinarr)) {
            $this->db->join('user', 'crowd_comment.user_id=user.user_id','left');

        }
    
        if($comment_id != '' && $comment_id > 0)
        {
            $this->db->where('crowd_comment.id',$comment_id);
        }
       
      
        if (is_array($order)) {
            foreach ($order as $key => $val) {
                $this->db->order_by($key, $val);
            }
        }

       
        if ($limit > 0) $this->db->limit($limit);

        $query = $this->db->get();
      
      
        if ($query->num_rows() > 0) 
        {
            
            return $query->result_array();
        } 
        else 
        {
            return 0;
        }
        

    }


    function GetAllSearchProperties($search_criteria = array(),$limit = 10,$order = array('property_id' => 'asc'))
    {
        
        $this->db->select('property_address,property_id');
        $this->db->from('property');
       
      

        if (is_array($search_criteria)) {
            foreach ($search_criteria as $key => $val) {
             
                if ($key == 'property_address' && $val != '') {

                    $val_array=explode(' ', trim($val));
                     $i=0;
                      $this->db->group_start();
                     foreach ($val_array as $value) {
                        if($i==0){
                         
                          $this->db->like('`property`.`property_address`', $value,'left');
                         
                          
                        }else{
                               
                            $this->db->or_like('`property`.`property_address`', $value,'left');
                         
                        }
                       $i++;
                      }
                       $this->db->group_end();     
                }


            }
        }
        $this->db->where('`property`.`status`', 2);

        if (is_array($order)) {
            foreach ($order as $key => $val) {
                $this->db->order_by($key, $val);
            }
        }
      
        $this->db->group_by('property.property_id');
       
      
        $this->db->limit($limit);

        $query = $this->db->get();
      
       
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

     function GetPropertyId($property_address='')
    {
        
        $this->db->select('property_id');
        $this->db->from('property');
       
        $this->db->where('property_address', $property_address);

        $query = $this->db->get();
      
       
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return 0;
        }
    }

     /*
    Function name :get_code()
    Parameter :user_data and check => used to when user login site
    Return : user data in array
    Use : This function is use to check this user is valid or not.
    Description :
    Done By : Dharmesh
    */
    function get_code($user_data = array())
    {
        $query = $this->db->get_where('crowd_invite_request', $user_data);
        if ($query->num_rows() > 0) {
            return $query->row_array();

        } else {
            return 0;
        }
    }

  

function GetAllCrowds($crowd_id='',$user_id='', $joinarr = array('user'), $limit = 100, $order = array('crowd_id' => 'asc'))
    {

        //default Equity fields
        $selectfields = 'crowd.*';
        //if require to join user
        if (in_array('user', $joinarr)) 
        {
            $selectfields .= ',user.user_id,user.user_name,user.last_name,user.address, user.fb_uid, user.facebook_wall_post, user.fb_access_token,user.image,user.email,user.user_website,user.profile_slug,user.user_occupation,user.user_type';
        }
       

        $this->db->select($selectfields);
        $this->db->from('crowd');


        //if require to join user
        if (in_array('user', $joinarr)) {
            $this->db->join('user', 'crowd.user_id=user.user_id','left');

        }

       
    
        if($user_id != '' && $user_id > 0)
        {

            $this->db->where('crowd.user_id',$user_id);
          
        }
       
      
        if (is_array($order)) {
            foreach ($order as $key => $val) {
                $this->db->order_by($key, $val);
            }
        }

       
        if ($limit > 0) $this->db->limit($limit);

        $query = $this->db->get();
      
      
        if ($query->num_rows() > 0) 
        {
            
            return $query->result_array();
        } 
        else 
        {
            return 0;
        }
        

    }
     /*
Function name :GetAccessRequest()
Parameter :user_id,GetAccessRequest,joinarr,limit,order,offset,count,is_interest_request_status
Return : Return all access request
Use : Fetch all request from detail page


*/

    function GetCrowdRequest($user_id = 0, $crowd_id = 0, $is_request_status = '', $join = array('crowd_request'), $limit = 10, $order = array('crowd_request_id' => 'desc'), $offset = 0, $count = 'no')
    {
        $selectfields = 'user.user_name,user.last_name,user.user_id,user.image,user.profile_slug,user.user_occupation,user.address';


        if (in_array('crowd_request', $join)) {
            $selectfields .= ',crowd_request.*';
            $this->db->group_by('crowd_request.crowd_request_id');
           if (in_array('crowd', $join)) {
                $selectfields .= ',crowd.title,crowd.crowd_id';
            }
            //if require to join commen
        }
       
        $this->db->select($selectfields);

        $this->db->from('user');

        //if require to join comment


        if (in_array('crowd_request', $join)) {
            $this->db->join('crowd_request', 'user.user_id=crowd_request.user_id');
            if (in_array('crowd', $join)) {
                $this->db->join('crowd', 'crowd_request.crowd_id=crowd.crowd_id');
            }
        }

        if (in_array('crowd_request', $join)) {
            $this->db->where('crowd_request.crowd_id', $crowd_id);
        }

        if($user_id > 0)
        {
             $this->db->where('crowd.user_id', $user_id);
        }
      
        if ($is_request_status != '') 
        {
            $this->db->where('crowd_request.status in (' . $is_request_status . ')');

        }


        if (is_array($order)) {
            foreach ($order as $key => $val) {
                $this->db->order_by($key, $val);
            }
        }
        if ($limit > 0 || $offset > 0) $this->db->limit($limit, $offset);


        $query = $this->db->get();
        //echo $this->db->last_query();die;
        if ($count == 'yes') {
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else {
                return 0;
            }
        }
    }
  
    function getInvestors($text='',$limit=10){
        return $this->db->select('transaction.user_id, user.user_name,user.last_name')
            ->from('transaction')
            ->join('user','user.user_id=transaction.user_id','left')
            ->like('user.user_name',$text)
            ->or_like('user.last_name',$text)
            ->where('transaction.preapproval_status','SUCCESS')
            ->limit($limit)
            ->get()
            ->result_array();
    }

    function getCrowdRequestByEmail($where){
        return $this->db->get_where('crowd_invite_request', $where)->row();
    }
  
   
}

?>