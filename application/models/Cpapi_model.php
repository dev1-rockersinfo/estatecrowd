<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 * The Home class is basically the default controller for the script.
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Models
 */
class Cpapi_model extends ROCKERS_Model
{

    function __construct(){
        parent::__construct();
    }

    //------Common UpdateAccount function-------
    /*
    Function name :property_search()
    Parameter : $property_suburb.
    Return : fetch data
    Use : It Update the user account details
    */
    function property_search($property_suburb,$widget=0){
        $selectfields = 'property.*';
        //if require to join user
        // if (in_array('user', $joinarr)) {
        //     $selectfields .= ',user.user_id,user.user_name,user.last_name,user.address, user.fb_uid, user.facebook_wall_post, user.fb_access_token,user.image,user.email,user.user_website,user.profile_slug,user.user_occupation,user.user_type,user.company_name';
        // }
        // if (in_array('campaign', $joinarr)) {
            $selectfields .= ',campaign.campaign_id,campaign.campaign_user_type,campaign.min_investment_amount,campaign.campaign_type,campaign.campaign_units,campaign.investment_close_date,campaign.campaign_document,campaign.investment_amount_type,campaign.campaign_units_get,campaign.date_added,campaign.investment_close_date';
        // }
       

        $this->db->select($selectfields);
        $this->db->from('property');
        // $this->db->join('property_type', 'property.user_id=property_type.user_id');
        $this->db->join('campaign', 'property.property_id=campaign.property_id','left');
        
        //check all property status
        $this->db->where('property.status in (2)');
        $this->db->like('property.property_suburb',$property_suburb,'both');
        $query = $this->db->get();
        $rawdata = $query->result();
        if($widget==0){
            if(count($rawdata)==0){
                return "Properties are not available in $property_suburb.";
            }

            $response_array = array();
            $temp_array = array();

            foreach ($rawdata as $key => $value) {
                $temp_array['property_id'] = $value->property_id;
                $temp_array['locality_id'] = $value->locality_id;
                $temp_array['property_url'] = base_url().'properties/'.$value->property_url;
                $temp_array['property_address'] = $value->property_address;
                $temp_array['property_type'] = $value->property_type;
                $temp_array['property_sub_type'] = $value->property_sub_type;
                $temp_array['property_suburb'] = $value->property_suburb;
                $temp_array['property_postcode'] = $value->property_postcode;
                $temp_array['date_added'] = $value->date_added;
                $temp_array['property_postcode'] = $value->property_postcode;

                array_push($response_array,$temp_array);
                $temp_array = array();
            }
            return $response_array;
        } else {
            return $rawdata;
        }
          
    }    

    function GetAllSearchProperties($user_id = 0, $property_id = '', $is_status = '', $joinarr = array('user'), $limit = 100, $order = array('property_id' => 'desc'), $offset = 0, $count = 'no',$campaign_data='no'){

        $investment_amount = $this->input->get('investment_amount');
        $investment_suburb = $this->input->get('investment_suburb');
        $investment_address = $this->input->get('investment_address');

        //default Equity fields
        $selectfields = 'property.*';
        //if require to join user
        if (in_array('user', $joinarr)) {
            $selectfields .= ',user.user_id,user.user_name,user.last_name,user.address, user.fb_uid, user.facebook_wall_post, user.fb_access_token,user.image,user.email,user.user_website,user.profile_slug,user.user_occupation,user.user_type,user.company_name';
        }
        if (in_array('campaign', $joinarr)) {
            $selectfields .= ',campaign.campaign_id,campaign.campaign_user_type,campaign.min_investment_amount,campaign.campaign_type,campaign.campaign_units,campaign.investment_close_date,campaign.campaign_document,campaign.investment_amount_type,campaign.campaign_units_get,campaign.date_added,campaign.investment_close_date';
        }       

        $this->db->select($selectfields);
        $this->db->from('property');


        //if require to join user
        if (in_array('user', $joinarr)) {
            $this->db->join('user', 'property.user_id=user.user_id','left');

        }
        if (in_array('property_type', $joinarr)) {
            $this->db->join('property_type', 'property.user_id=property_type.user_id');
        }
        if (in_array('campaign', $joinarr)) {
            if($campaign_data == 'yes')
            {
                $this->db->join('campaign', 'property.property_id=campaign.property_id');
            }   
            else
            {
                $this->db->join('campaign', 'property.property_id=campaign.property_id','left');
            }
        }
        if ($user_id > 0) {
            $this->db->join('invite_members', 'property.property_id=invite_members.property_id', 'left');
        }
        //check all property status
        if ($is_status != '') {
            $this->db->where('property.status in (' . $is_status . ')');
        }

        // filters

        // if ($investment_amount!=''){
        //     $this->db->where('campaign.min_investment_amount <= "' . $investment_amount . '"');
        // } else if($investment_suburb!=''){
        //     $this->db->like('property.property_suburb',$investment_suburb,"both");
        // } else if($investment_address!=''){
        //     $this->db->like('property.property_suburb',$investment_address,"both");
        // }
        
        $this->db->where('campaign.campaign_id',NULL);

        //check property id
        if ($property_id > 0 or $property_id != '') {
            if (is_numeric($property_id)) {
                $this->db->where('property.property_id', $property_id);
                $this->db->or_where('property.property_url', $property_id);
            } else {
                $this->db->where('property.property_url', $property_id);
            }
        }

        if (in_array('campaign', $joinarr) && $campaign_data == 'yes' && $property_id > 0) {
             $this->db->where('campaign.property_id', $property_id);
        }        

         
        if (is_array($order)) {
            foreach ($order as $key => $val) {
                $this->db->order_by($key, $val);
            }
        }

        // if ($user_id > 0) {
        //     $this->db->group_by('property.property_id');
        // }
        // if ($limit > 0) $this->db->limit($limit, $offset);

        $query = $this->db->get();
      
        if ($count == 'yes') {
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else {
                return 0;
            }
        }

    }


    /*
    Function name :create_campaign()
    Parameter :array $data
    Return : $lastID
    Use : insert data into campaign
    */

    function create_campaign($data,$r_campaign_id){
        if($r_campaign_id==0){
            if($this->db->insert('campaign',$data)){
                return $this->db->insert_id();
            } else {
                return false;
            }
        } else {
            $this->db->where('campaign_id',$r_campaign_id);
            $this->db->update('campaign',$data);
            return $r_campaign_id;
        }
    }

    /*
    Function name :create_investment()
    Parameter :array $data
    Return : $lastID
    Use : insert data into campaign
    */

    function create_investment($data,$r_investment_id){
        if($r_investment_id==0){
            if($this->db->insert('investment_process',$data)){
                return $this->db->insert_id();
            } else {
                return false;
            }
        } else {
            $this->db->where('id',$r_investment_id);
            $this->db->update('investment_process',$data);
            return $r_investment_id;
        }
    }
}

?>
