<?php

/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
class Inbox_model extends ROCKERS_Model
{

    var $lang = array();


    function __construct()
    {
        parent::__construct();
    }

    //------Common message function-------
    /*
    Function name :GetAllMessage()
    Parameter : user_id, reply_message_id, is_read, joinarr,limit,order
    Return : array of all Messages
    Use : Fetch all Message particular Criteria

    Cases:

    case 1 : if user grater than zero then it fetch all the message with particular user id means it returns all message .

    case 2 : if message_reply_id greater than 0 than return message in message detail page.
    */

    function GetAllMessage($user_id = 0, $reply_message_id, $is_read, $joinarr = array('user'), $limit, $order)
    {
        $selectfields = "message_conversation.message_id,message_conversation.sender_id,message_conversation.receiver_id,message_conversation.is_read,message_conversation.message_subject,message_conversation.message_content,message_conversation.property_id,message_conversation.date_added,message_conversation.date_added,message_conversation.reply_message_id,message_conversation.type,message_conversation.admin_replay";
        //if require to join user
        if (in_array('user', $joinarr)) {
            $selectfields .= ',user.user_name,user.last_name,user.address,user.image,user.tw_id,user.fb_uid,user.tw_screen_name';
        }
        $this->db->select($selectfields);
        $this->db->from('message_conversation');
        //if require to join user
        if (in_array('user', $joinarr)) {
            $this->db->join('user', 'message_conversation.receiver_id=user.user_id');
        }

        if ($user_id > 0) {
            
            //$this->db->group_start();
            $this->db->where('(((message_conversation.sender_id = ' . $user_id . ' OR message_conversation.receiver_id = ' . $user_id . ') AND message_conversation.type=0 ) OR ( ( (message_conversation.sender_id = ' . $user_id . ' and message_conversation.receiver_id =1) OR ( message_conversation.sender_id = 1 and message_conversation.receiver_id = ' . $user_id . ') ) AND message_conversation.type=2 ) OR ( ((message_conversation.sender_id = ' . $user_id . ' and message_conversation.receiver_id =1) OR ( message_conversation.sender_id = 1 and message_conversation.receiver_id = ' . $user_id . ') ) AND message_conversation.type=1 ))');
            //$this->db->group_end();
 
        }
        if ($reply_message_id == 0) {
            $this->db->where('message_conversation.reply_message_id', $reply_message_id);


        } else {
            $this->db->where('(message_conversation.reply_message_id = ' . $reply_message_id . ' OR message_conversation.message_id = ' . $reply_message_id . ')');

        }


        if (is_array($order)) {
            foreach ($order as $key => $val) {
                $this->db->order_by($key, $val);
            }
        }

        $this->db->limit($limit);
        $query = $this->db->get();


        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }


    }

    
    
    /*
    Function name :check_message_exist()
    Parameter : message_id
    Return : array of messages
    Use : check message exist or not

    */
    function check_message_exist($message_id)
    {
        $query = $this->db->query("select * from message_conversation where message_id='" . $message_id . "'");

        if ($query->num_rows() > 0) {
            return $query->row_array();
        }

        return 0;
    }

        /*
    Function name :chk_reply_message_exist()
    Parameter : message_id
    Return : array of messages
    Use : check reply message exist or not

    */

    function chk_reply_message_exist($message_id)

    {

        $query = $this->db->query("select * from message_conversation where (message_id='" . $message_id . "' or reply_message_id='" . $message_id . "') and is_read=0 and ( (admin_replay='admin' and (type=1 OR type=2) ) OR (type=0 and admin_replay!='admin') ) and receiver_id ='" . $this->session->userdata('user_id') . "' ");
        if ($query->num_rows() > 0) {
            return $query->result_array();

        }
        return 0;

    }

    /*
    Function name :get_one_message()
    Parameter : message_id
    Return : array of message
    Use : Fetch message from particular message id

    */
    function get_one_message($message_id)
    {
        $query = $this->db->query("select * from message_conversation where message_id='" . $message_id . "'");

        if ($query->num_rows() > 0) {
            return $query->row_array();
        }

        return 0;
    }

    /*
    Function name :get_read_message()
    Parameter : message_id
    Return : array of message
    Use : Fetch read message from particular message id

    */

    function get_read_message($message_id)

    {

        $query = $this->db->query("select * from message_conversation where reply_message_id='" . $message_id . "' and is_read=0");


        if ($query->num_rows() > 0) {

            return $query->row_array();

        }


        return 0;

    }

    /*
    Function name :insert_reply()
    Parameter : none
    Return : 0 or 1
    Use : reply message insert

    */


    function insert_reply()
    {
        $message_id = $this->input->post('message_id');
        $message_detail = $this->get_one_message($message_id);
        $conversation_type = $this->input->post('conversation_type');
        $admin_rply = '';
        
        if ($conversation_type==1 || $conversation_type==2) {
            $receiver_id=1;
        } else {
            $receiver_id=$this->input->post('receiver_id');
        }
        $data = array('sender_id' => $this->session->userdata('user_id'),
            'receiver_id' => $receiver_id,
            'is_read' => 0,
            'message_subject' => $message_detail['message_subject'],
            'message_content' => $this->input->post('conversation'),
            'property_id' => $message_detail['property_id'],
            'date_added' => date('Y-m-d H:i:s'),
            'reply_message_id' => $message_id,
            'admin_replay' => $admin_rply,
            'type'=>$conversation_type
        );
        if ($this->db->insert('message_conversation', $data)) {
            return 1;
        } else {
            return 0;
        }


    }

    /*
    Function name :GetAllEquityMessage($id)
    Parameter : id
    Return : equity id wise message
    Use : get message in database

    */

    function GetAllEquityMessage($id, $user_id = '',$msg='')
    {
        
        $sql="select * from message_conversation where reply_message_id=0 and equity_id='" . $id . "' and admin_replay='admin' and ";
        
        
        
        $sql.=" ( (receiver_id = 1 and sender_id = '" . $user_id . "') OR (sender_id = 1 and receiver_id = '" . $user_id . "') ) ";
        
        if($msg!='' && $msg>0) {
            $sql.=" and type= ".$msg;
        }
        
        $sql.=" order by message_id asc ";

        $query = $this->db->query($sql);
        
        //echo $sql; //die;

        if ($query->num_rows() > 0) {
            return $query->result_array();
        }

        return 0;
    }
    
     /*
    Function name :AddInsertUpdateTable()
    Parameter : key_name-> is the feild name,key_value-> value of the feild name,table-> table name,data-> data to be update.
    Return : fetch data if true or return 0 when false
    Use : It Update the user account details
    */

    function AddInsertUpdateTable($table, $key_name, $key_value, $data)
    {
        if ($key_name != '' && $key_value != '') {
            $this->db->where($key_name, $key_value);
            $query = $this->db->update($table, $data);
        } else {
            $query = $this->db->insert($table, $data);
            return $this->db->insert_id();
        }

        return true;
    }


}

?>