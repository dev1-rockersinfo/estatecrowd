<?php

/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
class Content_model extends ROCKERS_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /*
   Function name :faq()
   Parameter : active, faq_id , joinarr,order,count
   Return : result array or row
   Use : Fetch data using FAQ category or status
   */
    function faq($active = 0, $faq_id = '', $joinarr = array('faq_category'), $order = array('faq_id' => 'desc'), $count = 'no')
    {

        $selectfields = 'faq.faq_category_id, faq.question, faq.answer, faq.faq_home, faq.faq_order , faq.question_type , faq.question_type_slug';
        if (in_array('faq_category', $joinarr)) {
            $selectfields .= ',faq_category.faq_category_name,faq_category.faq_category_id';
        }
        $this->db->select($selectfields);
        $this->db->from('faq');

        if (in_array('faq_category', $joinarr)) {
            $this->db->join('faq_category', 'faq.faq_category_id=faq_category.faq_category_id');
        }

        $this->db->where('faq.active', $active);

        if (is_array($order)) {
            foreach ($order as $key => $val) {
                $this->db->order_by($key, $val);
            }
        }

        if ($_SESSION['lang_folder'] != '')
            $this->db->where('language_folder', $_SESSION['lang_folder']);


        $this->db->join('language', 'language.language_id = faq.language_id', 'inner');
        //echo "as";
        $query = $this->db->get();

        if ($count == 'yes') {
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else {
                return 0;
            }
        }


    }

    /*
   Function name :learnmore()
   Parameter : active, learn_category , joinarr,order,count
   Return : result array or row
   Use : Fetch learnmore page data using learnmore category or status
   */
    function learnmore($active = 0, $learn_category = '', $order = array('pages_id' => 'desc'), $count = 'no')
    {

        $this->db->select('learn_more.slug,learn_more.pages_id,learn_more.pages_title, learn_more.sub_title, learn_more.icon_image,learn_more.description, learn_more.slug, learn_more.active, learn_more.meta_keyword, learn_more.meta_description');
        $this->db->where('learn_more.active', $active);

        if ($learn_category != '') {
            $this->db->where('learn_category', $learn_category);
        }
        if (is_array($order)) {
            foreach ($order as $key => $val) {
                $this->db->order_by($key, $val);
            }
        }


        if ($_SESSION['lang_folder'] != '')
            $this->db->where('language_folder', $_SESSION['lang_folder']);


        $this->db->from('learn_more');
        $this->db->join('language', 'language.language_id = learn_more.language_id', 'inner');

        $query = $this->db->get();

        if ($count == 'yes') {
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else {
                return 0;
            }
        }


    }

    /*
   Function name :getpages()
   Parameter : active, slug , header,footer,order
   Return : result array
   Use : Fetch pages data using slug or status
   */
    function getpages($active = 0, $slug = '', $header = 0, $footer = 0, $order = array('pages_id' => 'desc'))
    {
        //echo $title;
        $this->db->select('pages_title, description, slug, footer_bar, header_bar, meta_keyword, meta_description , header_title,header_content,link_name,link ,dynamic_image_image');
        $this->db->where('pages.active', 1);
        if ($slug != '') {
            $this->db->where('slug', $slug);
        }

        if ($header != '' && $header > 0) {
            $this->db->where('header_bar', $header);
        }
        if ($footer != '' && $footer > 0) {
            $this->db->where('footer_bar', $footer);
        }


        if ($_SESSION['lang_folder'] != '')
            $this->db->where('language_folder', $_SESSION['lang_folder']);


        $this->db->from('pages');
        $this->db->join('language', 'language.language_id = pages.language_id', 'inner');

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return 0;
    }

    /*
    Function name :get_learn_more_guide_success()
    Parameter : no
    Return : result array
    Use : Fetch learnmore project quidelines
    */
    function get_learn_more_guide_success()
    {
        $this->db->select('*');
        $this->db->from('learn_more');
        $this->db->where('learn_category', 'guide_success');
        $this->db->where('learn_more.active', '1');

        if ($_SESSION['lang_folder'] != '')
            $this->db->where('language_folder', $_SESSION['lang_folder']);
        $this->db->join('language', 'language.language_id = learn_more.language_id', 'inner');

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return 0;
    }

    /*
    Function name :get_one_page()
    Parameter : title
    Return : result array
    Use : Fetch particular page content using title
    */
    function get_one_page($title = '')
    {
        //$CI =& get_instance();
        $this->db->where('learn_category', $title);
        $this->db->where('active', 1);


        $query = $this->db->get('learn_more');


        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return 0;

    }

    /*
    Function name :get_one_learnMorepage()
    Parameter : id
    Return : result array
    Use : Fetch particular page content using id
    */
    function get_one_learnMorepage($id = '')
    {
        //$CI =& get_instance();

        $this->db->select('*');
        $this->db->from('learn_more');

        $this->db->where('learn_more.pages_id', $id);
        $this->db->where('learn_more.active', 1);

        if ($_SESSION['lang_folder'] != '')
            $this->db->where('language_folder', $_SESSION['lang_folder']);
        $this->db->join('language', 'language.language_id = learn_more.language_id', 'inner');

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return 0;

    }

    /*
    Function name :get_learnMorepage()
    Parameter : id,page_id,order
    Return : result array
    Use : Fetch all pages using id or page_id and active pages
    */
    function get_learnMorepage($id = '', $pages_id = '', $order = array('pages_id' => 'desc'))
    {

        $this->db->select('*');
        $this->db->from('learn_more');

        $this->db->where('learn_category', $id);
        $this->db->where('pages_id !=', $pages_id);
        $this->db->where('learn_more.active', 1);
        if (is_array($order)) {
            foreach ($order as $key => $val) {
                $this->db->order_by($key, $val);
            }
        }

        if ($_SESSION['lang_folder'] != '')
            $this->db->where('language_folder', $_SESSION['lang_folder']);
        $this->db->join('language', 'language.language_id = learn_more.language_id', 'inner');


        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return 0;

    }

}

?>