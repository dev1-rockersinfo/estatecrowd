<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

/**
 * The Home class is basically the default controller for the script.
 *
 * @author       Rockers Technology Team
 * @copyright   Copyright (c) 2012-15, Rockers Technology Inc
 * @package    Application\Models
 */
class Account_model extends ROCKERS_Model
{

    function __construct()
    {
        parent::__construct();
    }

    //------Common UpdateAccount function-------
    /*
    Function name :UpdateAccount()
    Parameter : key_name-> is the feild name,key_value-> value of the feild name,table-> table name,data-> data to be update.
    Return : fetch data if true or return 0 when false
    Use : It Update the user account details
    */

    function UpdateAccount($key_name, $key_value, $table, $data)
    {
        if ($key_value) {
            $this->db->where($key_name, $key_value);
            $query = $this->db->update($table, $data);
        } else {
            $query = $this->db->insert($table, $data);
        }
        setting_deletecache('user_detail_notification' . $key_value);
        setting_deletecache('user_detail' . $key_value);

        return true;
    }


    /*
    Function name :PasswordMatches()
    Parameter : No.
    Return : TRUE Or FAlSE
    Use : It match the old password and user_id it exist in table user then it returns TRUE Otherwise FALSE
    */
    function PasswordMatches()
    {
        $this->db->where('user_id', $this->session->userdata('user_id'));
        $this->db->where('password', md5($this->input->post('old_password')));
        $query = $this->db->get('user');
        if ($query->num_rows() > 0) {

            return true;
        } else {
            return false;
        }

    }

    /*
    Function name :check_slug_exist()
    Parameter : profile_slug.
    Return : 0 Or 1
    Use : It match the profile slug it exist in table user then it returns 0 Otherwise 1
    */
    function check_slug_exist($profile_slug)
    {
        $result = $this->db->query("SELECT * FROM user WHERE profile_slug='" . $profile_slug . "' and user_id !='" . 
            $this->session->userdata('user_id') . "'");
        
        if ($result->num_rows() > 0) {
            return '0';
        } else
            return '1';

    }

    /*
    Function name :GetDonation()
    Parameter : type,user_id,property_id,$status,join array ,limit ,offset ,perk_id ,perk_get
    Return : array of all Equities Transaction
    Use : Fetch all transaction particular Criteria

    Cases:

    case 1 : if type define for like owner transcation history , investor transaction history


    case 2 : if user grater than zero then it fetch all the Equity with particular user id means it returns all Equity of  particular investor or owner.

    case 3 : if Equity grater than zero then it return particular Equity.


    case 4 : if Status equal to 0 than it returns Draft Equities.
             if Status equal to 1 than it returns Pending Equities.
             if Status equal to 2 than it returns Active Equities.
             if Status equal to 3 than it returns Success Equities.
             if Status equal to 4 than it returns unsuccess Equities.
             if Status equal to 5 than it returns Declined Equities.

             or

             if Status equal 0,1 than it returns both type of Equities.(Same for all others).

    case 4 : if all Transactin will be sorted by given order variable values.


    */
    function GetDonation($type = '', $user_id = 0, $campaign_id = 0, $is_status = '', $join = array('user'), $limit = 10, $order = array('user_id' => 'desc'), $offset = 0, $perk_id = 0, $perk_get = 0)
    {

        if ($type == 'my_donation') {
            $selectfields = 'transaction.*';
        }

        if ($type == 'my_donation_trans_anonymous') {

            $selectfields = 'transaction.*';
        }

        if ($type == 'incoming') {
            $selectfields = 'property.property_id,property.property_address,property.amount_get, property.end_date, property.status,property.project_description, property.cover_photo, property.property_url';

            $selectfields = 'campaign.campaign_id';
        }
        if ($type == 'funder') {
            $selectfields = 'transaction.* ,transaction.user_id as donor_id';

        }

        //if require to join user
        if (in_array('user', $join)) {
            $selectfields .= ',user.user_id,user.user_name, user.last_name, user.image,user.address,user.profile_slug';
        }

        if (in_array('property', $join)) {

            if ($type == 'my_donation') {
                $selectfields .= ',property.property_id,property.property_address, property.goal, property.amount_get, property.end_date, property.status,property.project_description, property.cover_photo, property.property_url';
            } else {
                $selectfields .= ',property.property_id,property.property_address, property.goal, property.amount_get, property.end_date, property.status,property.project_description, property.cover_photo, property.property_url';
            }

        }

        if (in_array('perk', $join)) {
            $selectfields .= ',perk.perk_title,perk.perk_get';
        }


        $this->db->select($selectfields);

        if ($type == 'my_donation') {
            $this->db->from('transaction');
        }

        if ($type == 'my_donation_trans_anonymous') {
            $this->db->from('transaction');
        }

        if ($type == 'incoming') {
            $this->db->from('property');
        }
        if ($type == 'funder') {
            $this->db->from('transaction');
        }

        if (in_array('campaign', $join)) {
            $this->db->join('campaign', 'transaction.campaign_id=campaign.campaign_id');
            if ($type == 'my_donation') {
                $this->db->join('user', 'campaign.user_id=user.user_id');
            }
        }
        if ($type == 'funder') {
            if (in_array('user', $join)) {
                $this->db->join('user', 'transaction.user_id=user.user_id', 'left');
            }
        } else {
            if (in_array('user', $join)) {
                $this->db->join('user', 'transaction.user_id=user.user_id');
            }

        }

        //if require to join user
        if (in_array('perk', $join)) {
            $this->db->join('perk', 'transaction.perk_id=perk.perk_id', 'left');
        }


        if ($is_status != '') {
            $this->db->where('campaign.status in (' . $is_status . ')');
        }


        if ($user_id > 0) {
            if ($type == 'my_donation') {
                $this->db->where('transaction.user_id', $user_id);
            }
            if ($type == 'incoming') {
                $this->db->where('campaign.user_id', $user_id);
            }

            if ($type == 'my_donation_trans_anonymous') {
                $this->db->where('transaction.user_id', $user_id);
            }
        }

        if ($campaign_id > 0) {
            if ($type == 'funder') {
                $this->db->where('transaction.campaign_id', $campaign_id);
            }
        }
       
        if (is_array($order)) {
            foreach ($order as $key => $val) {
                $this->db->order_by($key, $val);
            }
        }
        $this->db->limit($limit, $offset);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $result = $query->result_array();

            return $result;
        } else {
            return 0;
        }
    }

    /*
    Function name :GetUserComments()
    Parameter : user_id,joinarr,limit,order
    Return : array of all users comments
    Use : Fetch all comments

    Cases:

    case 1 : if user grater than zero then it fetch all the Comments with particular user id means it returns all Comments of  particular user


    case 2 : if all comments will be sorted by given order variable values.


    */

    function GetUserComments($user_id = 0, $join = array('user'), $limit = 10, $order = array('user_id' => 'desc'), $comment_status = '', $offset = 0, $count = 'no')
    {


        $selectfields = 'user_comment.property_id,user_comment.crowd_id,user_comment.user_id,user_comment.crowd_comment_id,user_comment.property_comment_id';


        if (in_array('user', $join)) {
            $selectfields .= ',user.user_name,user.user_id as cuser_id,user.last_name,user.image,user.profile_slug';
        }
       
        
        $this->db->select($selectfields);

        $this->db->from('user_comment');

        if (in_array('user', $join)) 
        {
            $this->db->join('user', 'user_comment.user_id=user.user_id');
        }
       
        if ($user_id > 0) {
            $this->db->where('user_comment.user_id', $user_id);
        }

        if (is_array($order)) {
            foreach ($order as $key => $val) {
                $this->db->order_by($key, $val);
            }
        }
        if ($limit > 0 || $offset > 0) $this->db->limit($limit, $offset);
        $query = $this->db->get();

        if ($count == 'yes') {
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else {
                return 0;
            }
        }

    }

    /*
    Function name :GetUpdateCommentGallery()
    Parameter : user_id,property_id,is_status,joinarr,limit,order
    Return : array of all data you have to join array
    Use : Fetch all data you have join.

    Cases:

    case 1 : if user grater than zero then it fetch all the Equity with particular user id means it returns all Equity of  particular company owner.


    case 2 : if Equity grater than zero then it return particular Equity.


    case 3 : if Status equal to 0 than it returns Draft Equities.
             if Status equal to 1 than it returns Pending Equities.
             if Status equal to 2 than it returns Active Equities.
             if Status equal to 3 than it returns Success Equities.
             if Status equal to 4 than it returns unsuccess Equities.
             if Status equal to 5 than it returns Declined Equities.

             or

             if Status equal 0,1 than it returns both type of Equities.(Same for all others).


    case 4 : if all comments will be sorted by given order variable values.


    */
    function GetUpdateCommentGallery($user_id = 0, $property_id = 0, $is_status = '', $join = array('user'), $limit = 10, $group = array('user_id'), $order = array('user_id' => 'desc'), $comment_status = '', $offset = 0, $count = 'no', $comment_id_for_parent = '0', $property_owner_id = 0)
    {
        $selectfields = 'property.property_address, property.property_url';
        //if require to join comment
        if (in_array('comment', $join)) {
            $selectfields .= ',comment.comment_id,comment.comments,comment.date_added,comment.user_id,comment.comment_ip,comment.status,comment.comment_type,comment.parent_id';
        }

        if (in_array('user', $join)) {
            $selectfields .= ',user.user_name,user.user_id,user.last_name,user.image,user.profile_slug';
        }

        if (in_array('perk', $join)) {
            $selectfields .= ',perk.perk_id,perk.property_id,perk.perk_title,perk.perk_description,perk.perk_amount,perk.perk_total,perk.perk_get,perk.estdate,perk.shipping_status';
        }
        if (in_array('previous_funding', $join)) {
            $selectfields .= ',previous_funding.previous_funding_id,previous_funding.property_id,previous_funding.funding_source,previous_funding.funding_type,previous_funding.funding_amount,previous_funding.funding_date';
        }
        if (in_array('investors', $join)) {
            $selectfields .= ',investors.investor_id,investors.property_id,investors.investor_description,investors.investor_type,investors.investor_name,investors.investor_role,investors.investor_image';
        }

        if (in_array('updates', $join)) {
            $selectfields .= ',updates.updates,updates.date_added,updates.update_id,user.user_name,user.user_id,user.last_name,user.image,user.profile_slug';
        }

        if (in_array('property_gallery', $join)) {
            $selectfields .= ',property_gallery.*';
        }
        if (in_array('video_gallery', $join)) {
            $selectfields .= ',video_gallery.image,video_gallery.media_video_title as image_name,video_gallery.status,video_gallery.property_id,video_gallery.media_video_name';
        }

        if (in_array('file_gallery', $join)) {
            $selectfields .= ',file_gallery.*';
        }

        $this->db->select($selectfields);

        $this->db->from('property');

        //if require to join comment

        if (in_array('comment', $join)) {
            $this->db->join('comment', 'property.property_id=comment.property_id');
        }

        if (in_array('perk', $join)) {
            $this->db->join('perk', 'property.property_id=perk.property_id');
        }
        if (in_array('previous_funding', $join)) {
            $this->db->join('previous_funding', 'property.property_id=previous_funding.property_id');
        }
        if (in_array('investors', $join)) {
            $this->db->join('investors', 'property.property_id=investors.property_id');
        }

        if (in_array('user', $join)) {
            $this->db->join('user', 'comment.user_id=user.user_id');
        }


        if (in_array('updates', $join)) {
            $this->db->join('updates', 'property.property_id=updates.property_id');
            $this->db->join('user', 'property.user_id=user.user_id');
        }

        if (in_array('property_gallery', $join)) {

            $this->db->join('property_gallery', 'property.property_id=property_gallery.property_id');


        }
        if (in_array('video_gallery', $join)) {
            $this->db->join('video_gallery', 'property.property_id=video_gallery.property_id');
        }

        if (in_array('file_gallery', $join)) {
            $this->db->join('file_gallery', 'property.property_id=file_gallery.property_id');
        }

        if (in_array('comment', $join)) {
            if ($comment_status == 1) {
                $this->db->where('comment.status in (' . $comment_status . ')');

            }
            $this->db->where("comment.parent_id = '0' ");
        }


        if ($comment_id_for_parent > 0) {
            $this->db->where("comment.comment_id != '" . $comment_id_for_parent . "' ");
        }

        if (in_array('video_gallery', $join)) {
            $this->db->where("video_gallery.media_video_title !='' ");

            if ($property_owner_id != $this->session->userdata('user_id')) {
                $this->db->where("video_gallery.status", '1');
            }
        }
        if (in_array('property_gallery', $join)) {
            $this->db->where("property_gallery.image !='' ");

            if ($property_owner_id != $this->session->userdata('user_id')) {
                $this->db->where("property_gallery.status", '1');
            }
        }
        if (in_array('file_gallery', $join)) {
            $this->db->where("file_gallery.file_path !='' ");
            if ($property_owner_id != $this->session->userdata('user_id')) {
                $this->db->where("file_gallery.status", '0');
            }
        }
        if ($is_status != '') {
            $this->db->where('property.status in (' . $is_status . ')');
        }

        if ($user_id > 0) {
            $this->db->where('property.user_id', $user_id);
        }
        if ($property_id > 0) {
            $this->db->where('property.property_id', $property_id);
        }
        if (is_array($group)) {
            foreach ($group as $key => $grp) {
                $this->db->group_by($grp);
            }
        }

        if (is_array($order)) {
            foreach ($order as $key => $val) {
                $this->db->order_by($key, $val);
            }
        }
        if ($limit > 0 || $offset > 0) $this->db->limit($limit, $offset);

        $query = $this->db->get();

        if ($count == 'yes') {
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else {
                return 0;
            }
        }
    }


    /*
    Function name :GetAllUsers()
    Parameter : user_id,is_status,joinarr,limit,order
    Return : array of all users
    Use : Fetch all user particular Criteria

    */

    function GetAllUsers($user_id = 0, $is_status = '', $joinarr = array('user'), $limit = 10, $order = array('user_id' => 'desc'))
    {

        $selectfields = 'user.user_id,user.reference_user_id, user.user_name,user.last_name,user.email,user.image,user.is_admin, user.password, user.fb_uid, user.fb_access_token, user.tw_id, user.linkdin_id, user.paypal_email';

        //if require to join category
        if (in_array('user_notification', $joinarr)) {
            $selectfields .= ',add_fund,user_alert,project_alert,comment_alert';
        }
        $this->db->select($selectfields);
        $this->db->from('user');
        //if require to join category
        if (in_array('user_notification', $joinarr)) {
            $this->db->join('user_notification', 'user_notification.user_id=user.user_id');
        }
        if ($is_status != '') {
            $this->db->where('user.status in (' . $is_status . ')');


        }

        //check user_id
        if ($user_id > 0) {
            $this->db->where('user.user_id', $user_id);
        }
        if (is_array($order)) {
            foreach ($order as $key => $val) {
                $this->db->order_by($key, $val);
            }
        }
        $this->db->limit($limit);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();

        } else {
            return 0;
        }
    }

    /*
    Function name :get_email_notification()
    Parameter : user_id
    Return : It returns notification userwise
    Use : Fetch particular notification data


    */

    function get_email_notification($user_id)
    {

        $query = $this->db->query("select * from user_notification where user_id='" . $user_id . "'");

        if ($query->num_rows() > 0) {
            return $query->row();
        }

        return 0;

    }
}

?>
