<?php

/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
class Startequity_model extends ROCKERS_Model
{

    function __construct()
    {
        parent::__construct();
    }


    /*
    Function name :AddInsertUpdateTable()
    Parameter : key_name-> is the feild name,key_value-> value of the feild name,table-> table name,data-> data to be update.
    Return : fetch data if true or return 0 when false
    Use : It Update the user account details
    */

    function AddInsertUpdateTable($table, $key_name, $key_value, $data)
    {
        if ($key_name != '' && $key_value != '') {
            $this->db->where($key_name, $key_value);
            $query = $this->db->update($table, $data);

            return true;
        } else {
            $query = $this->db->insert($table, $data);
            return $this->db->insert_id();
        }
    }

    /*
	Function name :getTableData()
	Parameter : table-> table name,where-> data to be fetch.
	Return : fetch data if true or return 0 when false
	Use : It fetch data from any table
	*/

    function getTableData($table, $where = array(), $order = array())
    {
        if (is_array($where)) {

            $this->db->where($where);
            if (is_array($order)) {

                foreach ($order as $key => $val) {

                    $this->db->order_by($key, $val);
                }
            }
            $query = $this->db->get($table);
        } else {
            if (is_array($order)) {

                foreach ($order as $key => $val) {

                    $this->db->order_by($key, $val);
                }
            }
            $query = $this->db->get($table);
        }


        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return 0;


    }


    /*
    Function name :GetVideoLink()
    Parameter : id
    Return : project id wise video gallery
    Use : get video gallery in database



    */
    function GetVideoLink($id = null)
    {

        $project_cnt = $this->db->get_where("video_gallery", array("project_id" => $id));
        if ($project_cnt->num_rows() > 0) {
            return $project_cnt->result_array();
        }
        return 0;

    }

    /*
    Function name :GetImageLink()
    Parameter : id
    Return : project id wise image gallery
    Use : get image gallery in database



    */
    function GetImageLink($id = null)
    {
        $project_cnt = $this->db->get_where("project_gallery", array("project_id" => $id));
        if ($project_cnt->num_rows() > 0) {
            return $project_cnt->result_array();
        }
        return 0;

    }

    /*
    Function name :GetFileLink()
    Parameter : id
    Return : project id wise file gallery
    Use : get file gallery in database

    */

    function GetFileLink($id = null)
    {

        $project_cnt = $this->db->get_where("file_gallery", array("project_id" => $id));
        if ($project_cnt->num_rows() > 0) {
            return $project_cnt->result_array();
        }
        return 0;

    }

    /*
    Function name :GetInvite()
    Parameter : id
    Return : project id wise invite members
    Use : get invite members in database

    */
    function GetInvite($id = null)
    {

        $subsql = 'select user_id from user';
        $sql = "select * from invite_members where project_id=" . $id . " and (invite_user_id in (" . $subsql . ") or invite_user_id=0)";
        $project_cnt = $this->db->query($sql);
        if ($project_cnt->num_rows() > 0) {
            return $project_cnt->result_array();
        }
        return 0;

    }

    /*
    Function name :team_member()
    Parameter :equity_id
    Return : user data in array
    Use : This function is use to fetach member of project.
    Description :
    Done By : Dharmesh
    */
    function team_member($equity_id = '')
    {
        $equity_cnt = $this->db->query("select e.equity_id,e.equity_url,i.email,i.code,i.member_role,u.image,u.user_id,u.user_name,u.last_name,u.profile_slug,i.code,i.admin from invite_members as i inner join equity as e on i.equity_id=e.equity_id inner join user as u  on u.email=i.email where i.status=1 and  i.equity_id='" . $equity_id . "'");
        if ($equity_cnt->num_rows() > 0) {
            return $equity_cnt->result_array();
        } else {
            return 0;

        }
    }

    /*
      Function name :is_equity_owner()
      Parameter : equity_id
      Return : true for active
      Use : getting status for equity
      */
    function is_equity_owner($equity_id = '', $chk_status = false)
    {
        $equity = GetOneEquity($equity_id);

        if ($equity['user_id'] != $this->session->userdata('user_id')) {
            $equity_cnt = $this->db->query("select e.equity_id,e.equity_url,i.email,i.code from invite_members as i inner join equity as e on i.equity_id=e.equity_id inner join user as u  on u.email=i.email where i.status=1 and i.admin=1 and  i.equity_id='" . $equity_id . "' and i.invite_user_id='" . $this->session->userdata('user_id') . "'");


            if ($equity_cnt->num_rows() > 0) {
                return true;
            }

            return false;
        }
        if ($chk_status == true) {
            $is_status = $equity['status'];
            $status = array(0, 1);
            if (!in_array($is_status, $status)) {
                return false;
            }
        }
        return true;
    }

    /*
Function name :user_validate()
Parameter :user_data and check => used to when user login site
Return : user data in array
Use : This function is use to check this user is valid or not.
Description :

*/
    function member_validate($email = '', $equity_id = '')
    {

        $user_data = array('email' => $email, 'equity_id' => $equity_id);
        $query = $this->db->get_where('invite_members', $user_data);
        if ($query->num_rows() > 0) {

            return $query->row_array();

        } else {
            return FALSE;
        }
    }


    /*
    Function name :equity_company_industry_data()
    Parameter :equity_id
    Return : Company Industries Result Array
    Use : Company Industries Result Array for all or individual Equity.
    Auther : Rakesh
    */
    function equity_company_industry_data($equity_id = 0, $company_industry_id = 0)
    {
        $sql = "select ci.company_industry_name,ci.id from company_industry as ci inner join   equity_company_industry as eci on eci.company_industry_id=ci.id inner join equity as e on e.equity_id =eci.equity_id where 1=1 ";
        if ($equity_id > 0) $sql .= " and eci.equity_id ='" . $equity_id . "'";
        if ($company_industry_id > 0) $sql .= " and eci.company_industry_id ='" . $company_industry_id . "'";
        // echo $sql;
        $equity_cnt = $this->db->query($sql);

        if ($equity_cnt->num_rows() > 0) {
            return $equity_cnt->result_array();
        } else {
            return 0;

        }
    }


    /*
    Function name :company_team_member_data()
    Parameter :equity_id
    Return : Team Member Result Array
    Use : Team Member Result Array for all or individual Equity.
    Auther : Rakesh
    */
    function company_team_member_data($company_id = 0, $team_member_id = 0, $visible_profile = '')
    {
        $sql = "SELECT im.id as team_member_id,im.invite_user_id as user_id,im.company_id,im.team_member_name,im.team_member_type,im.team_memberd_description ,im.member_role,im.email,im.admin,im.visible_profile,im.member_image,ur.profile_slug FROM `invite_members` as im inner join company_profile as c on c.company_id=im.company_id left join user as ur on ur.user_id=im.invite_user_id where 1=1 ";
        if ($company_id > 0) $sql .= " and im.company_id ='" . $company_id . "'";
        if ($team_member_id > 0) $sql .= " and im.id ='" . $team_member_id . "'";
        if ($visible_profile != '') $sql .= " and im.visible_profile = 1";
        // echo $sql;
        $equity_cnt = $this->db->query($sql);
        if ($equity_cnt->num_rows() > 0) {
            return $equity_cnt->result_array();
        } else {
            return 0;

        }
    }


    /*
    Function name :team_memmber_email_validate()
    Parameter :user_data
    Return : team member data in array
    Use : This function is use to check this email is valid or not.

    */
    function team_memmber_email_validate($user_data = array(), $check = '')
    {


        $query = $this->db->get_where('invite_members', $user_data);
        if ($query->num_rows() > 0) {

            return $query->row_array();

        } else {
            return 0;
        }

    }


    /*
    Function name :equity_team_member_data()
    Parameter :equity_id
    Return : Team Member Result Array
    Use : Team Member Result Array for all or individual Equity.
    Auther : Rakesh
    */
    function equity_perk_data($equity_id = 0, $perk_id = 0)
    {
        $sql = "SELECT p.perk_id as equity_perk_id,p.equity_id,p.perk_title,p.perk_description,p.perk_amount,p.perk_get FROM `perk` as p inner join equity as e on e.equity_id=p.equity_id where 1=1 ";
        if ($equity_id > 0) $sql .= " and p.equity_id ='" . $equity_id . "'";
        if ($perk_id > 0) $sql .= " and p.perk_id ='" . $perk_id . "'";
        $sql .= "order by p.perk_id asc";
        $equity_cnt = $this->db->query($sql);
        if ($equity_cnt->num_rows() > 0) {
            return $equity_cnt->result_array();
        } else {
            return 0;

        }
    }

    /*
    Function name :equity_previous_funding_data()
    Parameter :equity_id
    Return : Previous Funding Result Array
    Use : Previous FundingResult Array for all or individual Equity.
    Auther : Rakesh
    */
    function equity_previous_funding_data($equity_id = 0, $prev_funding_id = 0)
    {
        $sql = "SELECT pf.* FROM `previous_funding` as pf inner join equity as e on e.equity_id=pf.equity_id where 1=1";
        if ($equity_id > 0) $sql .= " and pf.equity_id ='" . $equity_id . "'";
        if ($prev_funding_id > 0) $sql .= " and pf.previous_funding_id ='" . $prev_funding_id . "'";
        $sql .= "order by pf.previous_funding_id asc";
        // echo $sql;
        $equity_cnt = $this->db->query($sql);
        if ($equity_cnt->num_rows() > 0) {
            return $equity_cnt->result_array();
        } else {
            return 0;

        }
    }

    /*
    Function name :equity_investor_data()
    Parameter :equity_id
    Return : Team Member Result Array
    Use : Team Member Result Array for all or individual Equity.
    Auther : Rakesh
    */
    function equity_investor_data($equity_id = 0, $investor_id = 0)
    {
        $sql = "SELECT im.investor_id,im.equity_id,im.investor_name,im.investor_type,im.investor_description ,im.investor_email,im.investor_image FROM `investors` as im inner join equity as e on e.equity_id=im.equity_id where 1=1 ";
        if ($equity_id > 0) $sql .= " and im.equity_id ='" . $equity_id . "'";
        if ($investor_id > 0) $sql .= " and im.investor_id ='" . $investor_id . "'";
        $sql .= "order by im.investor_id asc";
        $equity_cnt = $this->db->query($sql);
        if ($equity_cnt->num_rows() > 0) {
            return $equity_cnt->result_array();
        } else {
            return 0;

        }
    }

    /*
    Function name :investor_email_validate()
    Parameter :user_data
    Return : team member data in array
    Use : This function is use to check this email is valid or not.

    */
    function investor_email_validate($user_data = array(), $check = '')
    {


        $query = $this->db->get_where('investors', $user_data);
        if ($query->num_rows() > 0) {

            return $query->row_array();

        } else {
            return 0;
        }

    }

    /*
    Function name :equity_file_gallery_data()
    Parameter :equity_id
    Return : Previous File Gallery Result Array
    Use : Previous File Gallery Array for all or individual Equity.
    Auther : Rakesh
    */
    function equity_file_gallery_data($equity_id = 0, $file_gallery_id = 0)
    {
        $sql = "SELECT pf.* FROM `file_gallery` as pf inner join equity as e on e.equity_id=pf.equity_id where 1=1";
        if ($equity_id > 0) $sql .= " and pf.equity_id ='" . $equity_id . "'";
        if ($file_gallery_id > 0) $sql .= " and pf.id ='" . $file_gallery_id . "'";
        $sql .= "order by pf.id asc";

        $equity_cnt = $this->db->query($sql);
        if ($equity_cnt->num_rows() > 0) {
            return $equity_cnt->result_array();
        } else {
            return 0;

        }
    }

    /*
    Function name :equity_video_gallery_data()
    Parameter :equity_id
    Return : Previous Video Gallery Result Array
    Use : Previous Video Gallery Array for all or individual Equity.
    Auther : Rakesh
    */
    function equity_video_gallery_data($equity_id = 0, $file_video_id = 0)
    {
        $sql = "SELECT pf.* FROM `video_gallery` as pf inner join equity as e on e.equity_id=pf.equity_id where 1=1";
        if ($equity_id > 0) $sql .= " and pf.equity_id ='" . $equity_id . "'";
        if ($file_video_id > 0) $sql .= " and pf.id ='" . $file_video_id . "'";
        $sql .= "order by pf.id asc";
        $equity_cnt = $this->db->query($sql);
        if ($equity_cnt->num_rows() > 0) {
            return $equity_cnt->result_array();
        } else {
            return 0;

        }
    }

    /*
    Function name :equity_image_gallery_data()
    Parameter :equity_id
    Return : Previous Image Gallery Result Array
    Use : Previous Image Gallery Array for all or individual Equity.
    Auther : Rakesh
    */
    function equity_image_gallery_data($equity_id = 0, $file_image_id = 0)
    {
        $sql = "SELECT pf.* FROM `equity_gallery` as pf inner join equity as e on e.equity_id=pf.equity_id where 1=1";
        if ($equity_id > 0) $sql .= " and pf.equity_id ='" . $equity_id . "'";
        if ($file_image_id > 0) $sql .= " and pf.equity_gallery_id ='" . $file_image_id . "'";
        $sql .= "order by pf.equity_gallery_id asc";
        $equity_cnt = $this->db->query($sql);
        if ($equity_cnt->num_rows() > 0) {
            return $equity_cnt->result_array();
        } else {
            return 0;

        }
    }

    /*
    Function name :equity_image_gallery_data()
    Parameter :equity_id
    Return : Previous Image Gallery Result Array
    Use : Previous Image Gallery Array for all or individual Equity.
    Auther : Rakesh
    */
    function GetUserCompnaies($user_id = 0)
    {
        $sql = "SELECT cp.* FROM `company_profile` as cp inner join user as u on u.user_id=cp.user_id where u.user_id = " . $user_id . " and cp.company_name != ''";

        // echo $sql;
        $equity_cnt = $this->db->query($sql);
        if ($equity_cnt->num_rows() > 0) {
            return $equity_cnt->result_array();
        } else {
            return 0;

        }
    }

    /*
    Function name :equity_deal_document_type_data()
    Parameter :equity_id
    Return : deal document type Result Array
    Use : deal document type Result Array for all or individual Equity.
    Auther : Rakesh
    */
    function equity_deal_document_type_data($equity_id = 0)
    {
        $sql = "SELECT * FROM `document_type` where document_type_name not in (select file_name from file_gallery where equity_id = " . $equity_id . ") ";

        $sql .= "order by id asc";

        $equity_cnt = $this->db->query($sql);
        if ($equity_cnt->num_rows() > 0) {
            return $equity_cnt->result_array();
        } else {
            return 0;

        }
    }

    /**
     * To get project sub types
     * Join with master types table
     * @param int $project_id project id 
     * @param int $parent to check whether category child or parent 
     * @return array of result
     * @access public
     * @author Rahul Patel
     */
    function getProjectTypes($equity_id=0,$parent=2){
        $this->db->select('equity_categories.equity_id,equity_categories.id,equity_categories.category_id,categories.id as tid,categories.name,categories.is_parent');
        $this->db->from('equity_categories');
        $this->db->join('categories','categories.id=equity_categories.category_id');
        $this->db->where('equity_categories.equity_id',$equity_id);
        if($parent==1){
            $this->db->where('categories.is_parent',1);
        }
        else if($parent==0){
            $this->db->where('categories.is_parent',$parent);
        }
        else{

        }
        $query = $this->db->get();
        return $query->result_array();
    }

    /**
     * get master types of projects.
     * @param int $parent is it's passed non zero then returns parents types 
     * @param int $parent_id used to get child category of specific parent 
     * @return array of result
     * @access public
     * @author Rahul Patel
     */
    function getTypes($parent=0){
        $this->db->select('*');
        $this->db->where('active',1);
        $this->db->from('categories');
        if($parent==1) $this->db->where('is_parent',$parent);

        else $this->db->where('is_parent',0);
        return $this->db->get()->result_array();
    }

    /**
     * A common function to delete single or more rows 
     * @param string $table name of the table
     * @param string $key colomn of the database
     * @param string $value value of the colomn
     * @return boolean true
     * @access public
     * @author Rahul Patel
     */
    function deleteTableData($table='',$key='',$value=''){
        $this->db->where($key,$value)->delete($table);
        return true;
    }

    /**
     * Update project types table. project has only one parent type so update everytime.
     * @param int $id project id 
     * @param array $update array of data to update single row
     * @return boolean true
     * @access public
     * @author Rahul Patel
     */
    function updateTypes($id=0,$update=array()){

        $this->db->where('equity_id',$id);
        $this->db->where('is_parent',1);
        $this->db->delete('equity_categories');

        $this->db->insert('equity_categories',$update);
        return true;
    }

    /**
     * get market summary file name from database used to unset from folder
     * @param int $id project id 
     * @return array single row with name 
     * @access public
     * @author Rahul Patel
     */
    function getFileName($id=0){
        return $this->db->select('market_summary_image')->from('equity')->where('equity_id',$id)->get()->row_array();
    }

    function saveHighlights($insert,$id=0){
        if($id){
            $this->db->where('id',$id);
            $this->db->update('equity_highlights',$insert);
            return true;
        }else{
            $this->db->insert('equity_highlights',$insert);
            return $this->db->insert_id(); 
        }  
    }

    function getHighlights($id){
        return $this->db->get_where('equity_highlights',array('equity_id'=>$id))->result_array();
    }

    function getOneHighlight($id){
        return $this->db->get_where('equity_highlights',array('id'=>$id))->row_array(); 
    }

    function deleteOneHighlight($id){
        $this->db->where('id',$id);
        $this->db->delete('equity_highlights');
        return true;
    }

    function getOneProjectType($equity_id=0,$category_id=0){
        return $this->db->get_where('equity_categories',array('equity_id'=>$equity_id,'category_id'=>$category_id))->row_array(); 
    }
    
    /*
    Function name :company_team_member_data()
    Parameter :equity_id
    Return : Team Member Result Array
    Use : Team Member Result Array for all or individual Equity.
    Auther : Rakesh
    */
    function company_featured_project_data($company_id = 0)
    {
        $sql = "SELECT * FROM `company_featured_projects` where 1=1 ";
        if ($company_id > 0) $sql .= " and company_id ='" . $company_id . "'";
        
        $equity_cnt = $this->db->query($sql);
        if ($equity_cnt->num_rows() > 0) {
            return $equity_cnt->result_array();
        } else {
            return 0;

        }
    }
}

?>