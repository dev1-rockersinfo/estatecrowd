<?php

/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
class Investment_step_model extends ROCKERS_Model
{

    function __construct()
    {
        parent::__construct();
    }

    /*
	Function name :AddUpdateData()
	Parameter :string $table, array $data, integer $value, string $key
	Return : $value
	Use : update and insert data into table
	*/

    function AddUpdateData($table = '', $data = '', $value = '', $key = '')
    {

        if ($value) {

            $this->db->where($key, $value);
            $this->db->update($table, $data);
            return $value;

        } else {

            $this->db->insert($table, $data);
            return $this->db->insert_id();

        }

    }

    /*
    Function name :check_equity_investment_process()
    Parameter :$project_id, $user_id, $invest_status_id
    Return : data or 0
    Use : check value is already exist or not
    */

    function check_equity_investment_process($project_id = '', $user_id = '', $invest_status_id = '')
    {

        if ($invest_status_id == 0) {

            $query = $this->db->query("select * from equity_investment_process where status = 0 and equity_id=" . $project_id . " and user_id=" . $user_id);

        } else {

            $query = $this->db->query("select * from equity_investment_process where status = 0 and equity_id=" . $project_id . " and user_id=" . $user_id . " and invest_status_id=" . $invest_status_id);
        }


        if ($query->num_rows() > 0) {

            return $query->row_array();

        } else {

            return 0;
        }

    }

    /*
    Function name :acknowdledge_update()
    Parameter :$where,$table,$data
    Return : none
    Use : update the data
    */

    function acknowdledge_update($where = array(), $table = '', $data = '')
    {


        $this->db->where($where);
        $query = $this->db->update($table, $data);

    }

        /*
    Function name :acknowdledge_insert()
    Parameter :$table,$data
    Return : none
    Use : insert the data
    */

    function acknowdledge_insert( $table = '', $data = '')
    {


        
        $query = $this->db->insert($table, $data);
        
        return $this->db->insert_id();

    }

    /*
     Function name :get_equity_process()
     Parameter :$table=table name, $where=array of values whose information or details you want to get.
     Return : none
     Use : Common function for getting single record using where clause.
     */

    function get_investor_process($table, $where = array(), $order = array())
    {

       if (is_array($where)) {

            $this->db->where($where);
            if (is_array($order)) {

                foreach ($order as $key => $val) {

                    $this->db->order_by($key, $val);
                }
            }
            $query = $this->db->get($table);
        } else {
            if (is_array($order)) {

                foreach ($order as $key => $val) {

                    $this->db->order_by($key, $val);
                }
            }
            $query = $this->db->get($table);
        }


        if ($query->num_rows() > 0) {

            return $query->row_array();

        } else {

            return 0;
        }


    }

     /*
     Function name :get_equity_process()
     Parameter :$table=table name, $where=array of values whose information or details you want to get.
     Return : none
     Use : Common function for getting single record using where clause.
     */

    function GetCampaignData($campaign_id='')
    {
        $this->db->select('property_id');
        $this->db->where('campaign_id',$campaign_id);
        $query = $this->db->get('campaign');
        if ($query->num_rows() > 0) {

            return $query->row_array();

        } else {

            return 0;
        }


    }

    function save_docusign_data($insert)
    {
        $this->db->insert('user_signed_docusign',$insert);
    }

    function update_docusign_data($update,$envelope_id)
    {
        $this->db->where('envelope_id',$envelope_id);
        $this->db->update('user_signed_docusign',$update);
    }

    function get_docusign_sign($envelope_id=0)
    {
        return $this->db->select('id')
        ->from('user_signed_docusign')
        ->where('envelope_id',$envelope_id)
        ->get()
        ->row_array();
    }

    function add_document_docusign($insert)
    {
        $this->db->insert('user_documents_docusign',$insert);
    }
}

?>
