<?php

/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
class Home_model extends ROCKERS_Model
{

    var $lang = array();
    var $user_id = "";
    var $full_name = "";
    var $pwd = "";
    var $fb_uid = "";

    function __construct()
    {
        parent::__construct();
    }

     /*
    Function name :get_category()
    Use : This function is use to get the project category where parent project category not =0.
    */
    /**
     * @return mixed
     */
    function get_category()
    {
        $this->db->where(array('categories.active' => 1));
        if ($_SESSION['lang_code'] != '')
            $this->db->where('iso2', $_SESSION['lang_code']);
        if ($_SESSION['lang_folder'] != '')
            $this->db->where('language_folder', $_SESSION['lang_folder']);
        $this->db->select('categories.*');
        $this->db->from('categories');

        $this->db->join('language', 'language.language_id = categories.language_id', 'inner');
        $this->db->order_by('name', 'asc');
        $query = $this->db->get();
        return $query->result_array();
    }


    /*
    Function name :getallcategory()
    Use : This function is use to get the all acitve project.
    */


    function getallcategory()
    {
       $this->db->where(array('categories.active' => 1));
        //$this->db->order_by('company_category_name','asc');
        if ($_SESSION['lang_code'] != '')
            $this->db->where('iso2', $_SESSION['lang_code']);
        if ($_SESSION['lang_folder'] != '')
            $this->db->where('language_folder', $_SESSION['lang_folder']);
        $this->db->select('categories.*');
        $this->db->from('categories');

        $this->db->join('language', 'language.language_id = categories.language_id', 'inner');
        $this->db->order_by('categories.active','desc');
        $this->db->order_by('name', 'asc');

        $query = $this->db->get();
        return $query->result_array();
    }

    /*
    Function name :getallcategory()
    Use : This function is use to get the all acitve project.
    */


    function getallsearchcategory()
    {
        $status = array(1,2);
       $this->db->where_in('categories.active',$status);
        //$this->db->order_by('company_category_name','asc');
       if ($_SESSION['lang_code'] != '')
            $this->db->where('iso2', $_SESSION['lang_code']);
        if ($_SESSION['lang_folder'] != '')
            $this->db->where('language_folder', $_SESSION['lang_folder']);
       
        $this->db->select('categories.*');
        $this->db->from('categories');

        $this->db->join('language', 'language.language_id = categories.language_id', 'inner');
        $this->db->order_by('categories.active','desc');
        $this->db->order_by('categories.name', 'asc');

        $query = $this->db->get();
      
        return $query->result_array();
    }


    /*
    Function name :get_country()
    Parameter :none
    Return : data in array
    Use : This function is use to get all active country.
    */
    function get_country()
    {
        $this->db->where('active', 1);
        $this->db->order_by('country_name', 'asc');
        $query = $this->db->get('country');
        return $query->result_array();
    }

    /*
    Function name :get_country()
    Parameter :none
    Return : data in array
    Use : This function is use to get all active country.
    */
    function get_state()
    {
        $this->db->where('active', 1);
        $this->db->order_by('state_name', 'asc');
        $query = $this->db->get('state');
        return $query->result_array();
    }

    /*
     Function name :get_currency()
     Parameter :none
     Return : data in array
     Use : This function is use to get all active currency.
     */
    function get_currency()
    {
        $this->db->where('active', 1);
        $this->db->order_by('currency_code', 'desc');
        $query = $this->db->get('currency_code');
        return $query->result_array();
    }

    /*
   Function name :get_one_currency()
   Parameter :curreny_code_id
   Return : currencty id in array
   Use : This function is use to get one active currency.
   */
    function get_one_currency($curreny_code_id)
    {
        $this->db->where('currency_code', $curreny_code_id);
        $query = $this->db->get('currency_code');
        $curreny_id = $query->row_array();
        return $curreny_id['currency_code_id'];
    }

    function text_echo($text)
    {
        return array_key_exists($text, $this->lang) ? $this->lang[$text] : $text;
    }

    /*
    Function name :spam_protection()
    Parameter :none
    Return : return 1 or 0
    Use : Check the user spamer or not.
    */
    function spam_protection()
    {

        $data = 0;
        if (!$this->simple_cache->is_cached('spam_protectd')) {
            // not cached, do our things that need caching
            //$this->db->order_by('spam_id','desc');
            $query = $this->db->query("select * from spam_ip where spam_ip='" . $_SERVER['REMOTE_ADDR'] . "' order by spam_id desc limit 1");

            if ($query->num_rows() > 0) {

                $data = $query->row();
                $data = 1;
            }
            // store in cache
            $this->simple_cache->cache_item('spam_protectd', $data);
        } else {
            $data = $this->simple_cache->get_item('spam_protectd');
        }


        return $data;


    }

    /*
    Function name :check_spam_

    )
    Parameter :none
    Return : return 1 or 0

    */
    function check_spam_register()
    {
        $spam_control = $this->db->query("select * from spam_control");
        $control = $spam_control->row();
        $total_register = $control->total_register;
        $register_expire = date('Y-m-d', strtotime('+' . $control->register_expire . ' days'));

        $chk_spam = $this->db->query("select * from user where signup_ip='" . $_SERVER['REMOTE_ADDR'] . "' and DATE(date_added)='" . date('Y-m-d') . "'");
        if ($chk_spam->num_rows() > 0) {
            $total_posted_register = $chk_spam->num_rows();
            if ($total_posted_register >= $total_register) {
                $make_spam = $this->db->query("insert into spam_ip(`spam_ip`,`start_date`,`end_date`)values('" . $_SERVER['REMOTE_ADDR'] . "','" . date('Y-m-d') . "','" . $register_expire . "')");
                return 1;
            } else {
                return 0;
            }
        }
        return 0;
    }


    /*
    Function name :check_spam_inquiry()
    Parameter :none
    Return : return 1 or 0
    use: This banned the ip when their limit is exceeded, and remove the all ip from spam_inquiry table
    */
    function check_spam_inquiry()
    {
        $spam_control = $this->db->query("select * from spam_control");
        $control = $spam_control->row();
        $total_contact = $control->total_contact;
        $contact_expire = date('Y-m-d', strtotime('+' . $control->contact_expire . ' days'));
        $chk_spam = $this->db->query("select * from spam_inquiry where inquire_spam_ip='" . $_SERVER['REMOTE_ADDR'] . "' and inquire_date='" . date('Y-m-d') . "'");
        if ($chk_spam->num_rows() > 0) {
            $total_posted_inquire = $chk_spam->num_rows();
            if ($total_posted_inquire >= $total_contact) {
                $make_spam = $this->db->query("insert into spam_ip(`spam_ip`,`start_date`,`end_date`)values('" . $_SERVER['REMOTE_ADDR'] . "','" . date('Y-m-d') . "','" . $contact_expire . "')");
                $delete_inquiry = $this->db->query("delete from spam_inquiry where inquire_spam_ip='" . $_SERVER['REMOTE_ADDR'] . "'");
                return 1;
            } else {
                return 0;
            }
        }
        return 0;
    }

    /*
    Function name :insert_inquiry()
    Parameter :none
    Return : none
    use:It only stores the ip address in the spam_inquiry table
    */
    function insert_inquiry()
    {
        $query = $this->db->query("insert into spam_inquiry(`inquire_spam_ip`,`inquire_date`)values('" . $_SERVER['REMOTE_ADDR'] . "','" . date('Y-m-d') . "')");
    }

    /*
    Function name :get_code()
    Parameter :user_data and check => used to when user login site
    Return : user data in array
    Use : This function is use to check this user is valid or not.
    Description :
    Done By : Dharmesh
    */
    function get_code($user_data = array())
    {
        $query = $this->db->get_where('invite_request', $user_data);
        if ($query->num_rows() > 0) {
            return $query->row_array();

        } else {
            return 0;
        }
    }

    /*
    Function name :email_validate()
    Parameter :email and user_id
    Return : user email in array
    Use : This function is use to check duplicate email exist except login user.
    Description :
    */
    function email_validate($email, $user_id)
    {
        $query = $this->db->query("select email from user where email='$email' and user_id!='$user_id'");

        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return 0;
        }
    }

    /*
    Function name :user_validate()
    Parameter :user_data and check => used to when user login site
    Return : user data in array
    Use : This function is use to check this user is valid or not.
    Description :
    Done By : Dharmesh
    */
    function user_validate($user_data = array(), $check = '')
    {


        $query = $this->db->get_where('user', $user_data);
        //print_r($query); die;

        if ($query->num_rows() > 0) {
            if ($check == 'login') {
                $user_info = $query->row_array();
                $data1 = array(
                    'user_id' => $user_info['user_id'],
                    'login_date_time' => date('Y-m-d H:i:s'),
                    'login_ip' => $_SERVER['REMOTE_ADDR']
                );
                $this->db->insert('user_login', $data1);
            }

            return $query->row_array();

        } else {
            return 0;
        }

    }

    /*
    Function name :register()
    Parameter :none
    Return : return 1 or 0
    Use : Check the user spamer or not.
    */
    function register($user_data = array())
    {
        //$this->load->helper('cookie');
        $this->db->insert('user', $user_data);
        $user_id = $this->db->insert_id();
        // make slug at registration time

        $profile_slug = makeSlugs($user_data['user_name'] . $user_data['last_name'],15);
        $data_slug = array(
            'profile_slug' => $profile_slug
        );

        $this->db->where('user_id', $user_id);
        $this->db->update('user', $data_slug);
        /*** user notification ****/
        $user_noti = array(
            'creator_pledge_alert' => 1,
            'creator_comment_alert' => 1,
            'creator_follow_alert' => 1,
            'creator_newup_alert' => 1,
            'user_id' => $user_id
        );


        $this->db->insert('user_notification', $user_noti);
        $id = $this->db->insert_id();

        $query = $this->db->get_where('user', array('user_id' => $user_id));//'active'=>'1'
        if ($query->num_rows() > 0) {
            $user = $query->row_array();
            return $user_id;
        } else {
            return '';
        }
    }

    /*
    Function name :checkForgetUniqueCode()
    Parameter :forgot_unique_code
    Return : return expire or not_sent or user_id
    Use : Check the unique forget code is exist or not.
    */
    function checkForgetUniqueCode($forgot_unique_code = null)
    {
        $site_setting = site_setting();
        $query = $this->db->get_where('user', array('forgot_unique_code' => $forgot_unique_code));
        if ($query->num_rows() > 0) {
            $row = $query->row_array();
            if (strtotime(date("Y-m-d H:i:s")) <= strtotime(date("Y-m-d H:i:s", strtotime($row['request_date'] . ' + ' . $site_setting['forget_time_limit'] . ' hours')))) {
                return $row['user_id'];
            } else {
                return 'expire';
            }
        } else {
            return 'not_sent';
        }
    }

    /*
    Function name :setNewPassword()
    Parameter :user_id contain user id and sd ia an array have value to update
    Return : return mysql_affected_rows or blank
    Use : Check the user spamer or not.
    */
    function setNewPassword($user_id, $sd = array())
    {
        $rest_data = array(
            'forgot_unique_code' => '',
            'request_date' => '',
            'password' => SecureShowData($sd['password'])
        );
        $this->db->where('user_id', $user_id);
        $this->db->update('user', $rest_data);
        if (mysql_affected_rows() > 0) {
            return mysql_affected_rows();
        } else {
            return '';
        }
    }

    /*
    Function name :check_social_user()
    Parameter :data
    Return : return 0 or array
    Use : Check the user from social
    */
    function check_social_user($data = array())
    {

        $selectfields = 'user_id';
        $this->db->select($selectfields);
        $this->db->from('user');
        if (is_array($data)) {
            $i = 0;
            foreach ($data as $key => $val) {
                if ($i == 0) $this->db->where($key, $val);
                else {
                    if ($val != '') $this->db->or_where($key, $val);
                }
                $i++;
            }
        }

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }

        /*
    Function name :update_social_user()
    Parameter :wheredata,data
    Return : return 2 for update social user
    Use : update social user profile data
    */

    function update_social_user($wheredata = array(), $data = array())
    {

        $selectfields = 'user_id,unique_code,user_name,email';
        $this->db->select($selectfields);
        $this->db->from('user');
        if (is_array($wheredata)) {
            $i = 0;
            foreach ($wheredata as $key => $val) {
                if ($val != "") {
                    if ($i == 0) $this->db->where($key, $val);
                    else $this->db->or_where($key, $val);
                    $i++;
                }
            }
        }

        $query = $this->db->get();
        // echo $this->db->last_query();
        if ($query->num_rows() > 0) {
            $user = $query->row_array();
            //start to build update query
            $sql = "Update user ";
            $i = 0;


            if ($user['unique_code'] == '') {
                $unique_code = unique_user_code(getrandomCode(12));
                $sql .= " set unique_code='" . $unique_code . "'";
                $i++;
            }
            if (is_array($data)) {

                foreach ($data as $key => $val) {
                    if ($val != "") {
                        if ($i == 0) {
                            $sql .= " set " . $key . "='" . $val . "'";
                        } else $sql .= " , " . $key . "='" . $val . "'";

                        $i++;
                    }
                }
            }


            if (is_array($wheredata)) {
                $i = 0;
                foreach ($wheredata as $key => $val) {
                    if ($val != "") {
                        if ($i == 0) $sql .= " where " . $key . "='" . $val . "'";
                        else $sql .= " or  " . $key . "='" . $val . "'";
                        $i++;
                    }
                }
            }

            $this->db->query($sql);

            //update login table
            $data1 = array(
                'user_id' => $user['user_id'],
                'login_date_time' => date('Y-m-d H:i:s'),
                'login_ip' => $_SERVER['REMOTE_ADDR']
            );
            $this->db->insert('user_login', $data1);

            $query2 = $this->db->get_where('equity', array('user_id' => $user['user_id']));

            if ($query2->num_rows() > 0) {
                $equity = $query2->row_array();
                $equity_id = $equity['equity_id'];
                $company_name = $equity['company_name'];
                $equity_url = $equity['equity_url'];
            } else {
                $equity_id = 0;
                $company_name = '';
                $equity_url = '';
            }

            //generate project session
            // echo $this->db->last_query();
            $data = array(
                'user_id' => $user['user_id'],
                'user_name' => $user['user_name'],
                'email' => $user['email'],
                'equity_id' => $equity_id,
                'company_name' => $company_name,
                'equity_url' => $equity_url,
                'facebook_id' => $this->user_id,
            );
            $this->session->set_userdata($data);
            //var_dump($data);
            return 2;
        }

        return 2;

    }

    /*
    Function name :select_text()
    Parameter : none
    Return : langauge name
    Use : fetch the language site team has set in admin
    */


    function select_text()
    {
        $query = $this->db->query("select site_language from site_setting");
        $res_lang = $query->row_array();

        $query = $this->db->query("select tk.*,tt.text_id,tt.translation_id,tt.text from translation_key tk LEFT JOIN (select * from translation_text where translation_id=" . $res_lang['site_language'] . ") tt ON tk.key_id=tt.key_id");

        $res = $query->result();

        foreach ($res as $r) {
            $this->lang[$r->key] = ($r->text != "") ? $r->text : $r->key;
        }
        return $this->lang;
    }

    /*
    Function name :get_user_info()
    Parameter : id
    Return : email or 0
    Use : fetch the user data who are signup with twitter
    */
    function get_user_info($id = '')
    {
        $query = $this->db->query("SELECT * FROM user WHERE tw_id='" . $id . "'");


        if ($query->num_rows() > 0) {
            $result = $query->result();
            $result = $result[0];
            return $result->email;
        } else {
            return 0;
        }
    }

    /*
	Function name :delete_comment()
	Parameter : none
	Return : none
	Use : Delete comment using particular comment id
	*/
    function delete_comment($comment_id = '',$crowd_id='',$property_id='')
    {
        

        if($crowd_id > 0 && $crowd_id != '')
        {
            $this->db->where('id', $comment_id);
            $this->db->delete('crowd_comment');

            $this->db->where('crowd_comment_id', $comment_id);
            $this->db->delete('user_comment');
        }
        if($property_id > 0 && $property_id != '')
        {
            $this->db->where('comment_id', $comment_id);
            $this->db->delete('comment');

           

            $this->db->where('property_comment_id', $comment_id);
            $this->db->delete('user_comment');
        }



        
    }

    /*
    Function name :getTableData()
    Parameter : table-> table name,where-> data to be fetch.
    Return : fetch data if true or return 0 when false
    Use : It fetch data from any table
    */

    function getTableData($table, $where = array(), $order = array())
    {
        if (is_array($where)) {

            $this->db->where($where);
            if (is_array($order)) {

                foreach ($order as $key => $val) {

                    $this->db->order_by($key, $val);
                }
            }
            $query = $this->db->get($table);
        } else {
            if (is_array($order)) {

                foreach ($order as $key => $val) {

                    $this->db->order_by($key, $val);
                }
            }
            $query = $this->db->get($table);
        }


        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return 0;


    }

    function GetAllSearchSuburbs($search_criteria = array(),$limit = 10,$order = array('suburb' => 'asc'),
        $postcode ='')
    {
        
        $this->db->select('*');
        $this->db->from('postcodes_geo');
       
      

        if (is_array($search_criteria)) {
            foreach ($search_criteria as $key => $val) {
             
                if ($key == 'investment_suburbs' && $val != '') {

                    $val_array=explode(' ', trim($val));
                     $i=0;
                      $this->db->group_start();
                     foreach ($val_array as $value) {
                        if($i==0){
                         
                          $this->db->like('`postcodes_geo`.`suburb`', $value,'after');
                         
                          
                        }else{
                               
                            $this->db->or_like('`postcodes_geo`.`suburb`', $value,'after');
                         
                        }
                       $i++;
                      }
                       $this->db->group_end();     
                }


                if ($key == 'postcode' && $val != '') {

                    $val_array=explode(' ', trim($val));
                     $i=0;
                      $this->db->group_start();
                     foreach ($val_array as $value) {
                        if($i==0){
                         
                          $this->db->like('`postcodes_geo`.`postcode`', $value,'after');
                         
                          
                        }else{
                               
                            $this->db->or_like('`postcodes_geo`.`postcode`', $value,'after');
                         
                        }
                       $i++;
                      }
                       $this->db->group_end();     
                }

            }
        }
        $this->db->where('`postcodes_geo`.`status`', 1);

        if (is_array($order)) {
            foreach ($order as $key => $val) {
                $this->db->order_by($key, $val);
            }
        }
        if($postcode != '')
        {
            $this->db->group_by('postcodes_geo.postcode');
        }
        else
        {
            $this->db->group_by('postcodes_geo.suburb');
        }
      
        $this->db->limit($limit);

        $query = $this->db->get();
      
       
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }
/*
    Function name :GetAllSliderData()
    Parameter : order,joinarray
    Return : Return all slider sorting from admin
    Use : Fetch all slider sortiing data

    */

    function GetAllSliderData($order = array('view_order' => 'asc'), $limit = 100)

    {

        $site_setting = site_setting();

        $banner_setting = $site_setting['slider_setting'];

        $selectfields = 'slider_setting.view_order,slider_setting.id';

        //if require to join comment

        $selectfields .= ',dynamic_slider.dynamic_image_title ,dynamic_slider.dynamic_image_paragraph,dynamic_slider.dynamic_image_image,dynamic_slider.small_text,dynamic_slider.dynamic_slider_id,dynamic_slider.color_picker,dynamic_slider.color_picker_content,dynamic_slider.link,dynamic_slider.link_name';

       

        $this->db->select($selectfields);


        $this->db->from('slider_setting');

        $this->db->join('dynamic_slider', 'slider_setting.slider_id=dynamic_slider.dynamic_slider_id', 'left');

        $this->db->or_where('dynamic_slider.active', 1);

        

        if (is_array($order)) {

            foreach ($order as $key => $val) {

                $this->db->order_by($key, $val);

            }

        }

        if ($limit > 0) $this->db->limit($limit);


        $query = $this->db->get();


        if ($query->num_rows() > 0) {

            return $query->row_array();

        } else {

            return 0;

        }

    }

    /*
    Function name :GetAllSliderData()
    Parameter : order,joinarray
    Return : Return all slider sorting from admin
    Use : Fetch all slider sortiing data

    */

    function GetAllInvestors($user_id=0,$joinarr = array('user_property_detail'),$order = array('user.user_id' => 'asc'), $limit = 100,$search_criteria = array())

    {

        $selectfields = 'user.user_name,user.last_name,user.profile_slug,user.image,user.address,user.user_id';

        //if require to join comment

        if (in_array('user_property_detail', $joinarr)) {
            $selectfields .= ',user_property_detail.*';
        }

         if (in_array('user_follow', $joinarr)) {
            $selectfields .= ', tbl_follower.total_follower';
        }



        $this->db->select($selectfields);


        $this->db->from('user');

        //if require to join user
        if (in_array('user_property_detail', $joinarr)) 
        {
            $this->db->join('user_property_detail', 'user.user_id=user_property_detail.user_id','left');
        }

         if (in_array('user_follow', $joinarr)) 
        {
             
            
            $this->db->join('(select user_follow.follow_user_id as followe_user_id, count(*) as total_follower from user_follow  group by followe_user_id ) as tbl_follower', 'user.user_id=tbl_follower.followe_user_id','left');
        }

        if($user_id > 0 )
        {
            $this->db->where('user.user_id !=', $user_id);
        }
        
        $this->db->like('user.user_type', 1,'both');

        if (is_array($search_criteria)) {


            foreach ($search_criteria as $key => $val) 
            {
                
                if ($key == 'user_name' && $val != '') 
                {
                     $val_array=explode(' ', $val);
                      
                     foreach ($val_array as $value) {
                      
                       
                        $this->db->like('user.user_name', $value,'both');
                       
                       
                        
                      } 
                   

                }

                if ($key == 'address' && $val != '') 
                {
                     $val_array=explode(' ', $val);
                      
                     foreach ($val_array as $value) {
                      
                       
                        $this->db->like('user.address', $value,'both');
                       
                       
                        
                      } 
                   

                }

                
                if ($key == 'property_type' && $val != '') 
                {
                     $val_array=explode(' ', $val);
                      
                     foreach ($val_array as $value) {
                      
                       
                        $this->db->like('user_property_detail.user_property_type', $value,'both');
                       
                       
                        
                      } 
                   

                }

                if ($key == 'followers' && $val != '') 
                {
                     $val_array=explode(' ', $val);
                      
                     foreach ($val_array as $value) {
                      
                       
                        $this->db->where('tbl_follower.total_follower >=',$val);
                       
                       
                        
                      } 
                   

                }




             
            }
        }
        

        if (is_array($order)) {

            foreach ($order as $key => $val) {

                $this->db->order_by($key, $val);

            }

        }

        if ($limit > 0) $this->db->limit($limit);


        $query = $this->db->get();


        if ($query->num_rows() > 0) {

            return $query->result_array();

        } else {

            return 0;

        }

    }

    function get_properties_suburbs($property_status=array(2),$limit=10){
        return $this->db->select('count(property.property_id) as suburb_used, suburbs.id,
            suburbs.postcode_id, suburbs.name,suburbs.single_line,suburbs.state')
        ->from('suburbs')
        ->join('property','property.property_suburb=suburbs.id')
        ->where_in('property.status',$property_status)
        ->group_by('suburbs.id')
        ->order_by('suburb_used', 'desc')
        ->limit($limit)
        ->get()
        ->result_array();
    }

}


?>
