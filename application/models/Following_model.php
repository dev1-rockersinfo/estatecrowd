<?php

/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
class Following_model extends ROCKERS_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }


    /*
    Function name :GetAllFollowing()
    Parameter : is_follower_type,user_id,joinarr,limit,order
    Return : array of all following users
    Use : Fetch all following user for particular project or equity or user

    */
    function GetAllFollowing($is_follower_type = '', $user_id = 0, $join = array('user'), $limit = 10, $order = array('property_follow_id' => 'desc'))
    {

        //default Project fields
        if ($is_follower_type == 'property_following') {
            $selectfields = 'property_follower.property_follow_id, property_follower.property_id, property_follower.property_follow_user_id, property_follower.property_follow_date';
        }


        if ($is_follower_type == 'user_following') {
            $selectfields = 'user_follow.follower_id, user_follow.follow_user_id, user_follow.follow_by_user_id, user_follow.user_follow_date';
        }

        //if require to join category
        if (in_array('property', $join)) {
            $selectfields .= ',property.*';
        }

        if (in_array('user', $join)) {
            $selectfields .= ',user.user_name,user.last_name,user.address,user.image,user.profile_slug';
        }

        $this->db->select($selectfields);

        if ($is_follower_type == 'property_following') {
            $this->db->from('property_follower');
        } else {
            $this->db->from('user_follow');
        }

        if (in_array('property', $join)) {
            $this->db->join('property', 'property_follower.property_id=property.property_id');
        }

        if (in_array('user', $join)) {

            $this->db->join('user', 'user_follow.follow_user_id=user.user_id');
        }

        if ($user_id > 0) {
            if ($is_follower_type == 'property_following') {
                $this->db->where('property_follower.property_follow_user_id', $user_id);
            } else {
                $this->db->where('user_follow.follow_by_user_id', $user_id);
            }
        }

        if (is_array($order)) {
            foreach ($order as $key => $val) {
                $this->db->order_by($key, $val);
            }
        }
        $this->db->limit($limit);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            return $result;
        } else {
            return 0;
        }

    }

    /*
Function name :GetAllCountFollowing()
Parameter : is_follower_type,user_id,joinarr,limit,order
Return : row of all following users
Use : Fetch counts of following user for  particular Criteria

*/
    function GetAllCountFollowing($is_follower_type = '', $user_id = 0, $join = array('user'), $limit = 10, $order = array('property_follow_id' => 'desc'))
    {

        //default Project fields
        if ($is_follower_type == 'property_following') {
            $selectfields = 'property_follower.property_follow_id, property_follower.property_id, property_follower.property_follow_user_id, property_follower.property_follow_date';
        }


        if ($is_follower_type == 'user_following') {
            $selectfields = 'user_follow.follower_id, user_follow.follow_user_id, user_follow.follow_by_user_id, user_follow.user_follow_date';
        }

        //if require to join category
        //if require to join category
        if (in_array('property', $join)) {
            $selectfields .= ',property.*';
        }
        if (in_array('user', $join)) {
            $selectfields .= ',user.user_name,user.last_name,user.address,user.image';
        }

        $this->db->select($selectfields);

        if ($is_follower_type == 'property_following') {
            $this->db->from('property_follower');
        } else {
            $this->db->from('user_follow');
        }

        if (in_array('property', $join)) {
            $this->db->join('property', 'property_follower.property_id=property.property_id');
        }

        if (in_array('user', $join)) {

            $this->db->join('user', 'user_follow.follow_user_id=user.user_id');
        }

        if ($user_id > 0) {
            if ($is_follower_type == 'property_following') {
                $this->db->where('property_follower.property_follow_user_id', $user_id);
            } else {
                $this->db->where('user_follow.follow_by_user_id', $user_id);
            }
        }

        if (is_array($order)) {
            foreach ($order as $key => $val) {
                $this->db->order_by($key, $val);
            }
        }
        $this->db->limit($limit);

        $query = $this->db->get();
        /* echo "<br><br><br>";
        echo $this->db->last_query();
         die;*/
        //echo "<br><br><br>";
        if ($query->num_rows() > 0) {
            $result = $query->num_rows();
            return $result;
        } else {
            return 0;
        }

    }

    /*
Function name :follow_property_insert()
Parameter : property_id
Return : 0 or 1
Use : Insert data for project or property follow

*/
    function follow_property_insert($property_id = '')
    {
        $data = array(
            'property_id' => $property_id,
            'property_follow_user_id' => $this->session->userdata('user_id'),
            'property_follow_date' => date('Y-m-d H:i:s')
        );
        if ($this->db->insert('property_follower', $data)) 
        {
            return 1;
        } 
        else {
            return 0;
        }
    }

    /*
    Function name :follow_company_insert()
    Parameter : company_id
    Return : 0 or 1
    Use : Insert data for project or equity follow

    */
    function follow_company_insert($company_id = '')
    {
        $data = array(
            'company_id' => $company_id,
            'company_follow_user_id' => $this->session->userdata('user_id'),
            'company_follow_date' => date('Y-m-d H:i:s')
        );
        if ($this->db->insert('company_follower', $data)) {
            return 1;
        } else {
            return 0;
        }
    }
}

?>