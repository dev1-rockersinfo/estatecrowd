<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="width=device-width, initial-scale=1.0" name="viewport" />   
<meta charset="utf-8" />
   <title>admin</title>
   <meta content="" name="description" />
   <meta content="" name="author" />

	<script src="<?php echo base_url();?>js/jquery-1.8.3.min.js"></script>
	  <script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
	    <script src="<?php echo base_url();?>js/jquery.blockui.js"></script>
	  
 <link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
 	<link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
  <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link href="<?php echo base_url();?>admin_css/style.css" rel="stylesheet" />
  <link href="<?php echo base_url();?>admin_css/style_responsive.css" rel="stylesheet" />
 <link href="<?php echo base_url();?>admin_css/style_default.css" rel="stylesheet" id="style_color" />
 <link href="<?php echo base_url();?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/uniform/css/uniform.default.css" />
</head>
<script>

</script>
<body id="login-body" class="fixed-top">
 <?php echo $header; ?>
      <!-- <h1 style="text-transform:capitalize;" class="h1"><?php //echo $title; ?>&nbsp;</h1> -->
<?php echo $main_content; ?>        
<?php echo $footer; ?>
</body>
</html>
