<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="width=device-width, initial-scale=1.0" name="viewport" />   
<meta charset="utf-8" />
   <title>:: Welcome to FundraisingScript Admin::</title>
   <meta content="" name="description" />
   <meta content="" name="author" />

   
<!-- Favicon -->

<?php $site_setting = site_setting(); 

  $favicon_image = $site_setting['favicon_image'];
?>

<link rel="icon" href="<?php echo base_url().'upload/orig/'.$favicon_image; ?>"/>

	<script src="<?php echo base_url();?>js/jquery-1.8.3.min.js"></script>
	  <script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script> 
	    <script src="<?php echo base_url();?>js/jquery.blockui.js"></script>
	  
 <link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
 	<link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
  <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link href="<?php echo base_url();?>admin_css/style.css" rel="stylesheet" />
  <link href="<?php echo base_url();?>admin_css/style_responsive.css" rel="stylesheet" />
 <link href="" rel="stylesheet" id="style_color" />
 <link href="<?php echo base_url();?>css/jquery.fancybox.css" rel="stylesheet" />
  <script src="<?php echo base_url();?>js/jquery.fancybox.js?v=2.1.5"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/uniform/css/uniform.default.css" />
	<!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="<?php echo base_url();?>js/excanvas.js"></script>
   <script src="<?php echo base_url();?>js/respond.js"></script>
   <![endif]-->
</head>
<script>
var handleStyler = function () {
    var scrollHeight = '25px';

    jQuery('#theme-change').click(function () {
        if ($(this).attr("opened") && !$(this).attr("opening") && !$(this).attr("closing")) {
            $(this).removeAttr("opened");
            $(this).attr("closing", "1");

            $("#theme-change").css("overflow", "hidden").animate({
                width: '20px',
                height: '22px',
                'padding-top': '3px'
            }, {
                complete: function () {
                    $(this).removeAttr("closing");
                    $("#theme-change .settings").hide();
                }
            });
        } else if (!$(this).attr("closing") && !$(this).attr("opening")) {
            $(this).attr("opening", "1");
            $("#theme-change").css("overflow", "visible").animate({
                width: '190px',
                height: scrollHeight,
                'padding-top': '3px'
            }, {
                complete: function () {
                    $(this).removeAttr("opening");
                    $(this).attr("opened", 1);
                }
            });
            $("#theme-change .settings").show();
        }
    });

    jQuery('#theme-change .colors span').click(function () {
        var color = $(this).attr("data-style");
        setColor(color);
    });

    jQuery('#theme-change .layout input').change(function () {
        setLayout();
    });

    var setColor = function (color) {
        $('#style_color').attr("href", "<?php echo base_url();?>admin_css/style_" + color + ".css");
        document.cookie = "themecolor=; expires=Thu, 01 Jan 1970 00:00:00 GMT";
        setCookie("themecolor",color,30);
    }

  $(window).bind("load", function() {
    
     $('#loader-wrapper').fadeOut(1000);

  });

}
function setCookie(cname,cvalue,exdays)
{
	var d = new Date();
	d.setTime(d.getTime()+(exdays*24*60*60*1000));
	var c_value=escape(cvalue) + ((exdays==null)
            ? "" : "; expires="+d.toUTCString())
           + "; path=/";
document.cookie=cname + "=" + c_value;
	//var expires = "expires="+d.toGMTString();
	//document.cookie = cname+"="+cvalue+"; "+expires;
}

function getCookie(cname)
{
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for(var i=0; i<ca.length; i++) 
	  {
	  var c = ca[i].trim();
	  if (c.indexOf(name)==0) return c.substring(name.length,c.length);
	  }
	return "";
}

function checkCookie()
{
	var color=getCookie("themecolor");
	//alert(color);return false;
if (color!="")
  {
	$('#style_color').attr("href", "<?php echo base_url();?>admin_css/style_" + color + ".css");
  }
else 
  {
	$('#style_color').attr("href", "<?php echo base_url();?>admin_css/style_default.css");
  }
}
jQuery(document).ready(function() {       
	   // initiate layout and plugins
	   $('.fancybox').fancybox();
	   checkCookie();
	   
	});
</script>
 <script type="text/javascript">
       $( document ).ready(function() {
           // Javascript to enable link to tab
           var url = document.location.toString();
           var tabName = url.split('#')[1];
           if (url.match('#')) {                
               $('.nav-tabs a[href=#'+tabName+']').tab('show') ;
               window.scrollTo(0, 0);
           } 
           
           // Change hash for page-reload
            $('.nav-tabs a').on('shown.bs.tab', function (e) {
               window.location.hash = e.target.hash;
               window.scrollTo(0, 0);
            })
       });

   </script>
<body class="fixed-top">
 <?php echo $header; ?>
      <!-- <h1 style="text-transform:capitalize;" class="h1"><?php //echo $title; ?>&nbsp;</h1> -->
<?php echo $main_content; ?>        
<?php echo $footer; ?>
</body>
</html>
