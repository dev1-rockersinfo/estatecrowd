  

   <ul class="following-list">
                          <?php
$date_format = $site_setting['date_format'];    
            if ($user_followers) {
                foreach ($user_followers as $follower) {
                    $user_name = $follower['user_name'];
                    $last_name = $follower['last_name'];
                    $user_follow_date = $follower['user_follow_date'];
                    $user_follow = $follower['follow_by_user_id'];
                    $equity_user_id = $follower['user_id'];
                    $follower_profile_slug = $follower['profile_slug'];

                    if ($follower['image'] != '') {
                        if (file_exists(base_path() . "upload/user/user_small_image/" . $follower['image'])) {
                            $image = $follower['image'];
                        } else {
                            $image = 'no_man.jpg';
                        }
                    } else {
                        $image = 'no_man.jpg';
                    }
                    ?>
                        <li class="clearfix">
                        <div class="following-lt avtar-sm">
                       <a href="<?php echo base_url() . "user/" . $follower_profile_slug; ?>">
                                    <img src="<?php echo base_url() . "upload/user/user_small_image/" . $image; ?>">
                                </a>
                        </div>
                         <div class="following-rt clearfix">
                         <h3>
                            <a  href="<?php echo base_url() . "user/" . $follower_profile_slug; ?>"><?php echo $user_name . ' ' . $last_name; ?></a>
                         </h3>
                        <span class="following-time"><?php echo date($date_format, strtotime($user_follow_date)); ?></span>
                        </div>
                        <div class="following-btn" id="follow_unfollow_follower<?php echo $equity_user_id; ?>">
                        <span class="follow-btn">
                       
                        
                                                    
                                <?php
                                if ($this->session->userdata('user_id')) {
                                    if ($equity_user_id != $this->session->userdata('user_id')) {
                                        $chk_follower = check_is_follower($equity_user_id, $this->session->userdata('user_id'));

                                        if ($chk_follower) {
                                            ?>
                                            <a href="javascript://" class="btn btn-primary btn-small"
                                               id="followme<?php echo $equity_user_id; ?>"
                                               onclick="unfollowuser(<?php echo $equity_user_id; ?>,'1')"
                                               onmouseover="set_followingtext(<?php echo $equity_user_id; ?>,0);"
                                               onmouseout="set_followingtext(<?php echo $equity_user_id; ?>,1);"><i
                                                    class="fa fa-heart"></i> <?php echo FOLLOWING; ?></a>
                                        <?php
                                        } else {
                                            ?>
                                            <a href="javascript://" class="btn btn-primary btn-small"
                                               id="followme<?php echo $equity_user_id; ?>"
                                               onclick="followuser(<?php echo $equity_user_id; ?>,'1')"><i
                                                    class="fa fa-heart"></i> <?php echo FOLLOW; ?></a>
                                        <?php
                                        }

                                        ?>



                                    <?php } 
                                }
                                ?>
                                </span>
                            </div>

                        </li>    

                        <?php } 

                        } else {
                ?>
                <li class="no-data"><?php echo NO_FOLLOWERS_FOUND; ?></li>
            <?php } ?>
                  
                        
                    </ul>