   
<?php


$about_user = '';

$user_id = '';
$date_format = $site_setting['date_format'];
if ($result) {
    foreach ($result as $result)
        $user_name = $result['user_name'];
    if ($result['image'] != '') {
        $user_image = $result['image'];
    } else {
        $user_image = 'no_man.jpg';
    }
    $last_name = $result['last_name'];
    $address = $result['address'];
    $user_id = $result['user_id'];

    $user_total_member = GetAllCrowdMember('',$user_id);
     $total_crowd =0;
    if(is_array($user_crowds))
    {
      $total_crowd =  count($user_crowds);
    }

    $profile_slug = $result['profile_slug'];
    $user_website = $result['user_website'];
    $about_user = SecureShowData($result['user_about']);
   
    $facebook_url = $result['facebook_url'];
    $twitter_url = $result['twitter_url'];
    $linkedln_url = $result['linkedln_url'];
    $googleplus_url = $result['googleplus_url'];
    $myspace_url = $result['myspace_url'];
    $bandcamp_url = $result['bandcamp_url'];
    $youtube_url = $result['youtube_url'];
    $company_name = $result['company_name'];
    $company_address = $result['company_address'];
    $company_website = $result['company_website'];
    $company_logo = $result['company_logo'];

    $user_property_type  = json_decode($result['user_property_type']);
    $user_type  = json_decode($result['user_type']);

}
$project_url = $taxonomy_setting['project_url'];
?>

 <body  data-spy="scroll" data-target="#affix-nav">
    <header class="navbar-transparent">
    <?php echo $this->load->view('default/common/header'); ?>
    </header>
   <header class="navbar-transparent-inner">

        <div class="pa-header">
          <div class="image-wrapper">
            <div class="image" style="background-image:url(<?php echo base_url();?>images/profile-agent-bg.jpg)"></div>
            <div class="overlay"></div>
          </div>
          <div class="content-area">
            <div class="main-wrap">
                  <div class="main-pic">
                    
                   <a class="agent-pic" href="javascript:void(0)"> 
                    <?php if ($user_image != '' && is_file("upload/user/user_medium_image/" . $user_image)) {
                        ?>
                        <img src="<?php echo base_url(); ?>upload/user/user_medium_image/<?php echo $user_image; ?>"/>
                    <?php
                    } else {
                        ?>
                        <img src="<?php echo base_url(); ?>upload/user/user_medium_image/no_man.jpg"/>
                    <?php } ?> </a>
                     <a class="agent-icon"><i class="fa fa-user"></i> </a>
                     </div>
                     <div class="main-text">
              <h2><?php echo $user_name . ' ' . $last_name; ?></h2>
               <?php if ($address != '') { ?>
                    <span class="location"><?php echo $address; ?></span>
                <?php } ?>
                <?php 
 if(UserTypeExists(2) == 1 || UserTypeExists(4) == 1) {    
  if($company_name != '') {
?>
              <span class="company-name"><a href="javascript:void(0)"><?php echo $company_name;?></a></span>

              <?php

              }
               } ?>
          
        </div>
                </div>
          </div>
            <div class="nav-overlay">
            </div>
        </div>
    </header>
    
    <section>
     <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id; ?>">
    <input type="hidden" name="user_followers_user_id" id="user_followers_user_id"
           value="<?php echo $user_followers_user_id; ?>">
    <div class="pa-content">
    <div class="container">
    <div class="row">
            <div class="col-md-4 col-md-push-8  clearfix">
            <div class="pa-sidebare-wrap clearfix">
    <div class="pa-sidebar">
    <div class="agent-info">
       
   <a class="agent-profile" href="javascript:void(0)">
   <?php if ($user_image != '' && is_file("upload/user/user_big_image/" . $user_image)) {
                        ?>
                        <img src="<?php echo base_url(); ?>upload/user/user_big_image/<?php echo $user_image; ?>"/>
                    <?php
                    } else {
                        ?>
                        <img src="<?php echo base_url(); ?>upload/user/user_big_image/no_man.jpg"/>
                    <?php } ?>

   </a>
   

   <?php
                        $redirect = 'user/' . $profile_slug;
                        $return_url = base64_encode($redirect);
                        if ($this->session->userdata('user_id') > 0) {
                            if ($this->session->userdata('user_id') != $user_id) {
                                ?>
              <a class="agent-icon" data-target="#agentMail" data-toggle="modal" href="javascript:void(0)">
              <i class="fa fa-envelope-o"></i>
              </a>
                            <?php
                            }
                        } else {
                            ?>
                            <a class="agent-icon"  href="<?php echo site_url('home/login/' . $return_url); ?>" data-toggle="modal">
                                    <i class="fa fa-envelope-o"></i></a>
                        <?php
                        }
                        ?>
        
<!-- Modal -->
<div class="modal fade" id="agentMail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
  <div class="modal-dialog" role="document">
    <div class="modal-content ec-form">
      <div class="modal-header ">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Send a message to <?php echo $user_name.' '.$last_name;?></h4>
      </div>
      <div class="alert alert-message alert-danger marB20" id="error_message" style="display:none">

    </div>
    <div class="alert alert-message alert-success marB20" id="success_message" style="display:none">

    </div>
      <div class="modal-body clearfix send_message_div">
       
       <div class="form-group col-md-12">
                     
                     <input type="text" placeholder="Subject" id="subject" name="subject" class="form-control ">
                 </div>
                 <div class="form-group col-md-12">
                    
                    <textarea  placeholder="Message" id="comments" name="comments" class="form-control"></textarea>
                 </div>
       
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" id="send_message" class="btn btn-primary">Send Message</button>
      </div>
     
    </div>
  </div>
</div>
</div>
       <div class="agent-text">
         <h3><?php echo $user_name.' '.$last_name;?></h3>
         <?php if($address != ''){?>
         <span><?php echo $address; ?></span>
         <?php } ?>
             <a class="btn btn-default sd" data-target="#agentMail" data-toggle="modal" href="javascript:void(0)"><i class="fa fa-envelope-o"></i> Message</a>

       
          <span id="follow_unfollow">
         <?php
                        if ($this->session->userdata('user_id') > 0) {
                            if ($this->session->userdata('user_id') != $user_id) {
                                $chk_follower = check_is_follower($result['user_id'], $this->session->userdata('user_id'));
                                if ($chk_follower) {
                                    ?>
                                    <a href="javascript://" class="btn btn-default" id="followme"
                                       onclick="unfollowuser(<?php echo $user_id; ?>,'0')"
                                       onmouseover="set_followingtext(0);" onmouseout="set_followingtext(1);"><i
                                            class="fa fa-heart"></i> <?php echo FOLLOWING; ?></a>
                                <?php
                                } else {
                                    ?>
                                    <a href="javascript://" class="btn btn-default" id="followme"
                                       onclick="followuser(<?php echo $user_id; ?>,'0')"><i
                                            class="fa fa-heart"></i> <?php echo FOLLOW; ?></a>
                                <?php
                                }
                            }
                        } else {
                            $return_url = base64_encode('user/' . $profile_slug);
                            ?>
                            <a href="<?php echo site_url('home/login/' . $return_url); ?>" class="btn btn-default"
                               id="followme"><i class="fa fa-heart"></i> <?php echo FOLLOW; ?></a>
                        <?php
                        }
                        ?>
                  </span>
         
    </div>
</div>
    <div class="pa-sidebar-social">
<ul class="socail-icon">
 <?php
                    if ($facebook_url != '' || $twitter_url != '' || $linkedln_url != '' || $googleplus_url != '' || $user_website != '' ) {
                        if ($facebook_url != '') { ?>
                          <li><a href="<?php echo $facebook_url; ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        <?php }   if ($twitter_url != '') {  ?>
                          <li><a href="<?php echo $twitter_url; ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
                            <?php }   if ($linkedln_url != '') {  ?>
                          <li><a href="<?php echo $linkedln_url; ?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                            <?php }   if ($googleplus_url != '') {  ?>
                          <li><a href="<?php echo $googleplus_url; ?>" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                            <?php } if($user_website != ''){ ?>
                          <li><a href="<?php echo $user_website;?>"><i class="fa fa-share-square-o"></i></a></li>
                          <?php } ?>
                       
<?php } ?>
</ul>
</div>
    <div class="pa-sidebar-follow">
    <ul class="follow" id="custom_tab">
    <?php 
    $count_campaigns = 0;
    if(is_array($user_property_campaign))
    {
      $count_campaigns = count($user_property_campaign);
    }

    $count_followers = 0;
    if(is_array($user_followers))
    {
      $count_followers = count($user_followers);
    }

    ?>
  <li>
 <a href="#my-campaigns" aria-controls="my-campaigns" data-tag="my-campaigns" role="tab" data-toggle="tab"><?php echo $count_campaigns;?> <span>Campaigns</span></a>
   </li>
    <li>
    <a href="#crowds" aria-controls="crowds" role="tab"  data-tag="crowds" data-toggle="tab"><?php echo $total_crowd;?> <span>Crowds</span></a></li>
    <li> <a href="#followers" aria-controls="followers" data-tag="followers"  role="tab" data-toggle="tab"><?php echo $count_followers;?> <span>Followers</span></a></li>
    </ul>
    </div>
    </div>
    <div class="pa-sidebar-bottom m-t">
    <?php 
    if(is_array($user_type))
    { ?>
   <span> User Registration:</span>
   <ul class="uri">
    <?php if(UserTypeExists(1,$user_id) == 1) { ?> 
    <li><a href="javascript:void(0)"><i class="fa fa-check-circle"></i> Investor</a></li>
      <?php } if(UserTypeExists(2,$user_id) == 1) { ?> 
      <li><a href="javascript:void(0)"><i class="fa fa-check-circle"></i> Real Estate Agent</a></li>
        <?php } if(UserTypeExists(3,$user_id) == 1) { ?> 
        <li><a href="javascript:void(0)"><i class="fa fa-check-circle"></i> Property Owner</a></li>
  <?php } if(UserTypeExists(4,$user_id) == 1) { ?> 
        <li><a href="javascript:void(0)"><i class="fa fa-check-circle"></i> Property Developer</a></li>
      <?php } ?>
    
   
   </ul>
   <?php } ?>
    </div>
</div>
            <div class="col-md-8 col-md-pull-4">
    
    <div class="pa-nav">
   <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active" ><a href="#overview" aria-controls="overview" role="tab" data-toggle="tab">Overview</a></li>
    <li role="presentation" data-tag="my-campaigns"><a href="#my-campaigns"  aria-controls="my-campaigns" role="tab" data-toggle="tab">Campaigns</a></li>
   
    <li role="presentation" data-tag="crowds"><a href="#crowds"  aria-controls="crowds" role="tab" data-toggle="tab">Crowds</a></li>
  
    <li role="presentation" data-tag="followers" ><a href="#followers" aria-controls="followers" role="tab" data-toggle="tab">Followers</a></li>
     <li role="presentation"><a href="#following" aria-controls="following" role="tab" data-toggle="tab">Following</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content pa-tab">
    <div role="tabpanel" class="tab-pane active" id="overview">
    
    <div class="panel panel-default ec-panel">
  <div class="panel-heading">
    <h3 class="panel-title">About <?php echo $user_name;?></h3>
  </div>
  <div class="panel-body">
     <?php
        if ($about_user != '') {
            ?>
            <p><?php echo $about_user; ?></p>
        <?php
        } else {
            ?>
            <div class="pad20"><?php echo THIS_USER_HAS_NOT_ENTERED ?></div>
        <?php
        }
        ?>       
  </div>
</div>
<?php 

if(is_array($user_type))
{
 if(UserTypeExists(2,$user_id) == 1 || UserTypeExists(4,$user_id) == 1) {    

?>
    <div class="panel panel-default ec-panel">
  <div class="panel-heading">
    <h3 class="panel-title">Company Info</h3>
  </div>
  <div class="panel-body">
        <div class="ci">
        <a class="ci-left" href="javascript:void(0)">
        <img src="<?php echo base_url();?>images/company-logo.jpg" > </a>
        <div class="ci-right">
        <a href="javascript:void(0)"><h3><?php echo $company_name;?></h3></a>
        <span> <?php echo $company_address;?></span>
        <a class="website" href="<?php echo $company_website;?>"><?php echo $company_website;?></a>
        
        </div>
        </div>
        
  </div>
</div>

<?php 
    }
} ?>
    
    <div class="panel panel-default ec-panel">
  <div class="panel-heading">
    <h3 class="panel-title">Property Profile</h3>
  </div>
  <div class="panel-body">
  <?php 

  if(is_array($user_property_type)){?>
        <div class="pp">
        <span>Property Types:</span>
            <ul class="pt">
            <?php if(PropertyTypeExists(1,$user_id) == 1) { ?> 
            <li><a href="javascript:void(0)"><i class="fa fa-check-circle"></i> Residential</a></li>
            <?php }  if(PropertyTypeExists(2,$user_id) == 1) { ?>
            <li><a href="javascript:void(0)"><i class="fa fa-check-circle"></i> Commercial</a></li>
             <?php }  if(PropertyTypeExists(3,$user_id) == 1) { ?>
            <li><a href="javascript:void(0)"><i class="fa fa-check-circle"></i> Industrial</a></li>
             <?php }  if(PropertyTypeExists(4,$user_id) == 1) { ?>
            <li><a href="javascript:void(0)"><i class="fa fa-check-circle"></i> Rural </a></li>
            <?php } ?>
            </ul>   
               
        </div>
      <?php } ?>
        <div class="pp">
        <span>Investment Suburbs:</span>
            <ul class="pt">
            <li><a href="javascript:void(0)"><i class="fa fa-check-circle"></i> Brisbane, QLD</a></li>
            <li><a href="javascript:void(0)"><i class="fa fa-check-circle"></i> St Kilda, VIC</a></li>
            <li><a href="javascript:void(0)"><i class="fa fa-check-circle"></i> Blacktown, NSW</a></li>
            <li><a href="javascript:void(0)"><i class="fa fa-check-circle"></i> Broadbeach, QLD </a></li>
            <li><a href="javascript:void(0)"><i class="fa fa-check-circle"></i> Point Cook, VIC </a></li>
             
            </ul>   
                
               


        
        </div>
        
  </div>
</div>
    </div>
    <div role="tabpanel" class="tab-pane" id="my-campaigns"><div class="panel panel-default ec-panel">
  <div class="panel-heading">
    <h3 class="panel-title">Campaigns</h3>
  </div>
  <div class="panel-body row">
        <ul class="db-mc-list agent-campaign">
                <?php if(is_array($user_property_campaign)) {

                    foreach ($user_property_campaign as $key => $user_campaigns) {
                    

                    $property_id = $user_campaigns['property_id'];
                    $property_user_id = $user_campaigns['user_id'];
                    $property_address = $user_campaigns['property_address'];
                    $property_url = $user_campaigns['property_url'];
                    $cars = $user_campaigns['cars'];
                    $bedrooms = $user_campaigns['bedrooms'];
                    $bathrooms = $user_campaigns['bathrooms'];
                    $status = $user_campaigns['status'];
                 
                   
                    if(is_file(base_path().'upload/property/commoncard/'.$user_campaigns['cover_image']) && $user_campaigns['cover_image'] != '')
                    {
                         $cover_image = base_url().'upload/property/commoncard/'.$user_campaigns['cover_image'];
                    }
                    else
                    {
                        $cover_image = base_url().'upload/property/commoncard/no_img.jpg';
                    }

                  ?>
                  <li class="col-md-4"> 
                  <div class="db-mc-wrap">
                        <div class="db-mc-thumb">
                       <a href="<?php echo site_url('properties/'.$property_url);?>"><img src="<?php echo $cover_image;?>"></a>
                     
                          <span class="overlay"></span>
                        </div>
                        <div class="db-mc-title">
                            <h3><a href="<?php echo site_url('properties/'.$property_url);?>"><?php echo $property_address;?></a></h3>
                        </div>
                        <div class="db-mc-btn">
                        <ul class="property-info clearfix">
                      <li>
                        <span class="info-num"><?php echo $bedrooms; ?></span>
                        <span class="icon-wrapper"><img src="<?php echo base_url();?>images/icon-bed.png"></span>
                        <span class="info-name">beds</span>
                      </li>
                      <li>
                        <span class="info-num"><?php echo $bathrooms; ?></span>
                        <span class="icon-wrapper"><img src="<?php echo base_url();?>images/icon-bath.png"></span>
                        <span class="info-name">baths</span>
                      </li>
                      <li>
                        <span class="info-num"><?php echo $cars; ?></span>
                        <span class="icon-wrapper"><img src="<?php echo base_url();?>images/icon-car.png"></span>
                        <span class="info-name">cars</span>
                      </li>
                    </ul>   
                        </div>                        
                        
                  </div>
                  </li>
                
              <?php
                  }

               } else{
                  ?>
                 
                 <li class="no_record_found">There are no Campaigns.</li>
              <?php } ?> 
                      
                 
                  
            </ul>  
  </div>
</div></div>

    <div role="tabpanel" class="tab-pane" id="crowds"><div class="panel panel-default ec-panel">
  <div class="panel-heading">
    <h3 class="panel-title">Crowds</h3>
  </div>
  <div class="panel-body">
  
  <div class="suggest-pro" id="SuggestArea">
  <div class="input-group">
 
 

  <form id="live-search" action="" class="styled" method="post">
    <fieldset>
         <input  type="text" class="ui-autocomplete-input form-control"  autocomplete="off" aria-describedby
  ="basic-addon1" id="crowd_search" placeholder="Search Crowds">
       
    </fieldset>
</form>
  <span id="MemClear" class="input-group-addon"><i class="fa fa-search"></i>
</span>
</div>
</div>
    <ul class="crowd-list">

    <?php 
if(is_array($user_crowds)) {
      foreach ($user_crowds as $key => $user_crowd) {
        # code...
     


          if ($user_crowd['image'] != '') {
          $user_image = $user_crowd['image'];
          } else {
              $user_image = 'no_man.jpg';
          }
          $comment_user_id = $user_crowd['user_id'];
          $profile_slug = $user_crowd['profile_slug'];
          if($profile_slug != '')
          {
            $profile_url = site_url('user/'.$profile_slug);
          }
          else
          {
            $profile_url = site_url('user/'.$comment_user_id);
          }
          $crowd_member_full_name = $user_crowd['user_name'].' '.$user_crowd['last_name'];
          $title = $user_crowd['title'];
          $description = $user_crowd['description'];
          $crowd_id = $user_crowd['crowd_id'];
          $crowd_url = $user_crowd['crowd_url'];
          $crowd_user_id =  $user_crowd['user_id'];
          $members = GetAllCrowdMember($crowd_id,$user_id,array('user'),'',array('id'=>'asc'),$crowd_user_id);
                    $count_member = 0;
                    if(is_array($members))
                    {
                      $count_member = count($members) + 1;
                    }
    ?>
                        <li class="clearfix">                        
                         <div class="crowd-lt clearfix">
                         <h3><a href="<?php echo site_url('crowds/'.$crowd_url)?>"><?php echo  $title;?></a></h3>
                        <span class="crowd-desc"><?php echo  $description;?></span>
                        </div>
                        <div class="crowd-rt">
                                <div class="crowd-btn">
                              <span class="view-btn"> <a href="javascript://"><span>  <?php echo $count_member;?></span> Members </a></span> 
                            </div>
                            
                            
                          </div>
                        </li>  
               
                    <?php }  }  else{
                  ?>
                 
                 <li class="no_record_found">There are no Crowds.</li>
              <?php } ?> 
                     
                        
                    </ul>
                   
  </div>
</div></div>

    <div role="tabpanel" class="tab-pane" id="followers"><div class="panel panel-default ec-panel">
  <div class="panel-heading">
    <h3 class="panel-title">Followers</h3>
  </div>
  <div class="panel-body">
    <div class="following" id="user_follower_id">
                        <ul class="following-list">
                          <?php

            if (is_array($user_followers)) {
                foreach ($user_followers as $follower) {
                    $user_name = $follower['user_name'];
                    $last_name = $follower['last_name'];
                    $user_follow_date = $follower['user_follow_date'];
                    $user_follow = $follower['follow_by_user_id'];
                    $equity_user_id = $follower['user_id'];
                    $follower_profile_slug = $follower['profile_slug'];

                    if ($follower['image'] != '') {
                        if (file_exists(base_path() . "upload/user/user_small_image/" . $follower['image'])) {
                            $image = $follower['image'];
                        } else {
                            $image = 'no_man.jpg';
                        }
                    } else {
                        $image = 'no_man.jpg';
                    }
                    ?>
                        <li class="clearfix">
                        <div class="following-lt avtar-sm">
                       <a href="<?php echo base_url() . "user/" . $follower_profile_slug; ?>">
                                    <img src="<?php echo base_url() . "upload/user/user_small_image/" . $image; ?>">
                                </a>
                        </div>
                         <div class="following-rt clearfix">
                         <h3>
                            <a  href="<?php echo base_url() . "user/" . $follower_profile_slug; ?>"><?php echo $user_name . ' ' . $last_name; ?></a>
                         </h3>
                        <span class="following-time"><?php echo date($date_format, strtotime($user_follow_date)); ?></span>
                        </div>
                        <div class="following-btn" id="follow_unfollow_follower<?php echo $equity_user_id; ?>">
                        <span class="follow-btn">
                       
                        
                                                    
                                <?php
                                if ($this->session->userdata('user_id')) {
                                    if ($equity_user_id != $this->session->userdata('user_id')) {
                                        $chk_follower = check_is_follower($equity_user_id, $this->session->userdata('user_id'));

                                        if ($chk_follower) {
                                            ?>
                                            <a href="javascript://" class="btn btn-primary btn-small"
                                               id="followme<?php echo $equity_user_id; ?>"
                                               onclick="unfollowuser(<?php echo $equity_user_id; ?>,'1')"
                                               onmouseover="set_followingtext(<?php echo $equity_user_id; ?>,0);"
                                               onmouseout="set_followingtext(<?php echo $equity_user_id; ?>,1);"><i
                                                    class="fa fa-heart"></i> <?php echo FOLLOWING; ?></a>
                                        <?php
                                        } else {
                                            ?>
                                            <a href="javascript://" class="btn btn-primary btn-small"
                                               id="followme<?php echo $equity_user_id; ?>"
                                               onclick="followuser(<?php echo $equity_user_id; ?>,'1')"><i
                                                    class="fa fa-heart"></i> <?php echo FOLLOW; ?></a>
                                        <?php
                                        }

                                        ?>



                                    <?php } 
                                }
                                ?>
                                </span>
                            </div>

                        </li>    

                        <?php } 

                        } else {
                ?>
                <li class="no-data"><?php echo NO_FOLLOWERS_FOUND; ?></li>
            <?php } ?>
                  
                        
                    </ul>
                     </div>
  </div>
</div></div>
    <div role="tabpanel" class="tab-pane" id="following"><div class="panel panel-default ec-panel">
  <div class="panel-heading">
    <h3 class="panel-title">Following</h3>
  </div>
  <div class="panel-body">
    <div class="following" id="user_following_id">
                        <ul class="following-list" id="user_following_id">
                        <?php
            if (is_array($user_followings)) {
                foreach ($user_followings as $following) {
                    $user_name = $following['user_name'];
                     $last_name = $following['last_name'];
                    $user_follow_date = $following['user_follow_date'];
                    $user_follower = $following['follow_user_id'];
                    $equity_user_id = $following['follow_user_id'];
                    $following_profile_slug = $following['profile_slug'];

                    if ($following['image'] != '') {
                        if (file_exists(base_path() . "upload/user/user_small_image/" . $following['image'])) {
                            $image = $following['image'];
                        } else {
                            $image = 'no_man.jpg';
                        }
                    } else {
                        $image = 'no_man.jpg';
                    }
                    ?>
                        <li class="clearfix">
                        <div class="following-lt avtar-sm">
                        <a href="<?php echo base_url() . "user/" . $following_profile_slug; ?>">
                                    <img src="<?php echo base_url() . "upload/user/user_small_image/" . $image; ?>">
                                </a>
                        </div>
                         <div class="following-rt clearfix">
                         <h3><a href="<?php echo base_url() . "user/" . $following_profile_slug; ?>"><?php echo $user_name . ' ' . $last_name; ?></a></h3>
                        <span class="following-time"><?php echo date($date_format, strtotime($user_follow_date)); ?></span>
                        </div>
                        <div class="following-btn" >
                          <span class="follow-btn">

                           <?php
                            if ($this->session->userdata('user_id')) {
                                if ($equity_user_id != $this->session->userdata('user_id')) {
                                    $chk_follower = check_is_follower($equity_user_id, $this->session->userdata('user_id'));

                                    if ($chk_follower) {
                                        ?>
                                        <a href="javascript://" class="btn btn-primary btn-small"
                                           id="followme<?php echo $equity_user_id; ?>"
                                           onclick="unfollowuser(<?php echo $equity_user_id; ?>,'2')"
                                           onmouseover="set_followingtext(<?php echo $equity_user_id; ?>,0);"
                                           onmouseout="set_followingtext(<?php echo $equity_user_id; ?>,1);"><i
                                                class="fa fa-heart"></i> <?php echo FOLLOWING; ?></a>
                                      <?php
                                        } else {
                                            ?>
                                            <a href="javascript://" class="btn btn-primary btn-small"
                                               id="followme<?php echo $equity_user_id; ?>"
                                               onclick="followuser(<?php echo $equity_user_id; ?>,'1')"><i
                                                    class="fa fa-heart"></i> <?php echo FOLLOW; ?></a>
                                        <?php
                                        }

                                        ?>




                                <?php } 
                            }
                            ?>
                          </span>
                          </div>
                        </li>    
                        
                    <?php
                }
            } else {
                ?>
                <li class="no-data"><?php echo NO_FOLLOWINGS_FOUND; ?></li>
            <?php } ?>
                        
                    </ul>
                     </div> 
  </div>
</div></div>
  </div>

</div>
</div>

    </div>
</div>
</div>
    </section>


<script>
    function savefollowerlist() {
        var user_id_value = document.getElementById("user_id").value;
        //alert(user_id_value);
        var mapdata = {"user_id": user_id_value};
        // var mapdata = "'perk_title=' + perk_title_value" ;
        //var mapdata_description = 'perk_description=' + perk_description_value;
        var getStatusUrl = '<?php echo site_url('follower/ajax_user_follower');?>';
        ////alert(getStatusUrl)
        $.ajax({
            url: getStatusUrl,
            dataType: 'text',
            type: 'POST',
            timeout: 99999,
            global: false,
            data: mapdata,
            //data:{'perk[]':mapdata},
            success: function (data) {
                //alert(data);
                document.getElementById('user_follower_id').innerHTML = data;
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
            }
        });
    }

    function savefollowinglist() {
        var user_id_value = document.getElementById("user_id").value;

        var mapdata = {"user_id": user_id_value};
        // var mapdata = "'perk_title=' + perk_title_value" ;
        //var mapdata_description = 'perk_description=' + perk_description_value;
        var getStatusUrl = '<?php echo site_url('follower/ajax_following');?>';
        ////alert(getStatusUrl)
        $.ajax({
            url: getStatusUrl,
            dataType: 'text',
            type: 'POST',
            timeout: 99999,
            global: false,
            data: mapdata,
            //data:{'perk[]':mapdata},
            success: function (data) {
                // alert(data);
                document.getElementById('user_following_id').innerHTML = data;
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
            }
        });
    }

    function followuser(id, user) {
        if (id == '') {
            return false;
        }

        var strURL = '<?php echo base_url() . 'follower/user_follower/';?>' + id;
         PopupOpen();
        var xmlhttp;
        if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        }
        else {// code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                 PopupClose();
            //console.log("out if");console.log($.trim(xmlhttp.responseText));
                if ($.trim(xmlhttp.responseText) == 'Follow') {//console.log("in if")
                    if (user == '1') {
                        var txt = '<span class="follow-btn"><a href="javascript:void(0)" class="btn btn-primary btn-small" id="followme' + id + '" onclick="unfollowuser(' + id + ',1);" onmouseover="set_followingtext(' + id + ',0);" onmouseout="set_followingtext(' + id + ',1);"><i class="fa fa-heart"></i> <?php echo FOLLOWING; ?></a></span>';
                        console.log(txt);
                        document.getElementById("follow_unfollow_follower" + id).innerHTML = txt;

                        savefollowinglist();

                    } else {
                        document.getElementById("follow_unfollow").innerHTML = '<span class="follow-btn"><a href="javascript:void(0)" class="btn btn-primary btn-small" id="followme" onclick="unfollowuser(<?php echo $user_id; ?>,0)" onmouseover="set_followingtext(0);" onmouseout="set_followingtext(1);"><i class="fa fa-heart"></i> <?php echo FOLLOWING; ?></a></span>';
                        savefollowerlist();
                    }
                    //document.getElementById("follow_unfollow").innerHTML='following';
                }
            }
        }
        xmlhttp.open("GET", strURL, true);
        xmlhttp.send();
    }
    function unfollowuser(id, user) {

        $('#followme').removeAttr('onmouseover').removeAttr('onmouseout');
        //alert(id);
        if (id == '') {
            return false;
        }
        var strURL = '<?php echo site_url('follower/user_unfollower');?>/' + id;
         PopupOpen();
        var xmlhttp;
        if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        }
        else {// code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {

            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                PopupClose();
            //console.log("out if");console.log($.trim(xmlhttp.responseText));
                if ($.trim(xmlhttp.responseText) == 'unfollow') {//console.log("in if");
                    if (user == '1') {
                        var txt = '<span class="follow-btn"><a href="javascript:void(0)" class="btn btn-primary btn-small" id="followme' + id + '"  onclick="followuser(' + id + ',1)"><i class="fa fa-heart"></i> <?php echo FOLLOW; ?></a></span>';
                        document.getElementById("follow_unfollow_follower" + id).innerHTML = txt;
                       
                        savefollowinglist();

                    }
                    else if (user == '2') {
                        savefollowerlist();
                        savefollowinglist();

                    } else {
                        document.getElementById("follow_unfollow").innerHTML = '<span class="follow-btn"><a href="javascript:void(0)" class="btn btn-primary btn-small" id="followme"  onclick="followuser(<?php echo $user_id; ?>,0)"><i class="fa fa-heart"></i> <?php echo FOLLOW; ?></a></span>';
                        savefollowerlist();
                    }
                }
            }
        }
        xmlhttp.open("GET", strURL, true);
        xmlhttp.send();
    }
    function set_followingtext(id, set) {
        if (set == 0) {
          $('.follow-btn').attr('class', function() {
            return $(this).attr('class').replace('follow-btn', 'unfollow-btn');
          });
            document.getElementById("followme" + id).innerHTML = '<i class="fa fa-heart-o"></i> <?php echo UNFOLLOW; ?>';
        }
        else {
           $('.unfollow-btn').attr('class', function() {
            return $(this).attr('class').replace('unfollow-btn', 'follow-btn');
          });
            document.getElementById("followme" + id).innerHTML = '<i class="fa fa-heart"></i> <?php echo FOLLOWING; ?>';
        }
    }


    $('#send_message').click(function (e) {
        //tinyMCE.triggerSave();
        
        $(".error").hide();
        $.ajax({
            type: 'POST',
            url: '<?php echo site_url('message/send_message/' . $user_id); ?>',
            data: $('.send_message_div').find('input,textarea,select').serializeArray(),
            dataType: 'json',
            beforeSend: function () {
                // alert('test');
                $('#step1').fadeIn();
            },
            success: function (data) {

                if(data.success != '')
                {
                  $("#error_message").hide();
                  $("#success_message").show();
                  $("#success_message").html(data.success);
                  $("#success_message").fadeOut(5000);
                }
                else if(data.error != '')
                {
                  $("#success_message").hide();
                  $("#error_message").show();
                  $("#error_message").html(data.error);
                }
                $("#subject").val('');
                $("#comments").val('');

            },
            
            complete: function () {
                $("#error_box2").hide();
               
            }
        });
    });

    $(function () {
    

    $("#custom_tab li a").on('click', function (e) {
      $(".nav-tabs li").removeClass("active"); 
      var tag_value = $(this).data("tag");
      $(".nav-tabs").find('[data-tag='+tag_value+']').addClass("active");
      $('html,body').animate({
        scrollTop: $(".nav-tabs").offset().top-59},
        'slow');
        
    });

    

    

});

</script>

        <script type="text/javascript">
        $(document).ready(function(){
    $("#crowd_search").keyup(function(){
 
        // Retrieve the input field text and reset the count to zero
        var filter = $(this).val(), count = 0;
 
        // Loop through the comment list
        $(".crowd-list li").each(function(){
 
            // If the list item does not contain the text phrase fade it out
            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                $(this).fadeOut();
 
            // Show the list item if the phrase matches and increase the count by 1
            } else {
                $(this).show();
                count++;
            }
        });

         // Update the count
        var numberItems = count;
        $("#filter-count").text("Number of Comments = "+count);
 
       
    });
});
   
    </script>
