<?php
if ($my_donation) {
    foreach ($my_donation as $my_donations) {
        if ($my_donations['image'] != '') {
            if (file_exists(base_path() . "upload/project/small/" . $my_donations['image'])) {
                $image = $my_donations['image'];
            } else {
                $image = 'no_img.jpg';
            }
        } else {
            $image = 'no_img.jpg';
        }
        $project_title = trans_anonymous('name', $my_donations['project_title'], '', $my_donations['preapproval_total_amount'], $my_donations['trans_anonymous']);
        $project_ids = $my_donations['project_id'];
        $project_summary = $my_donations['project_summary'];
        $project_title_url = $my_donations['url_project_title'];
        $preapproval_status = $my_donations['preapproval_status'];
        $amount_anonymous = trans_anonymous('amount', $my_donations['project_title'], '', $my_donations['preapproval_total_amount'], $my_donations['trans_anonymous']);
        if ($amount_anonymous > 0) {
            $amount = set_currency($my_donations['preapproval_total_amount'], $project_ids);
        } else {
            $amount = "Anonymous";
        }
        if ($preapproval_status != 'FAIL') {
            ?>
            <li class="media clearfix">
                <div class="pro-thumbs">
                    <?php if ($image) { ?>
                        <a href="<?php echo site_url('projects/' . $project_title_url . '/' . $project_ids); ?>"><img
                                class="img-thumbnail"
                                src="<?php echo base_url() . "upload/project/small/" . $image; ?>"/> </a>
                    <?php } else { ?>
                        <a href="<?php echo site_url('projects/' . $project_title_url . '/' . $project_ids); ?>"><img
                                class="img-thumbnail"
                                src="<?php echo base_url() . "upload/project/small/no_man.jpg"; ?>"/></a>
                    <?php } ?>
                </div>
                <div class="project-list-content">
                    <h2>
                        <a href="<?php echo site_url('projects/' . $project_title_url . '/' . $project_ids); ?>"><?php echo $project_title; ?></a>
                    </h2>

                    <p><?php echo $project_summary; ?></p>

                    <p><strong><?php echo $amount; ?> <?php echo CONTRIBUTED; ?></strong></p>
                </div>
            </li>
        <?php
        }
    }

    ?>
    <?php
    if ($total_row_display <= 10) {
        $display = 'none';
    } else {
        $display = 'block';
    }
    ?>
    <li class="show-more more_comments" style="display:<?php echo $display; ?>"><a href="javascript:void(0)"
                                                                                   onclick="show_more_contributions(<?php echo $offset + 10; ?>)"><?php echo SHOW_MORE_CONTRIBUTIONS; ?></a>
    </li>
<?php
} else {
    ?>
    <li class="no-data"><?php echo NO_CONTRIBUTIONS; ?>.</li>
<?php
}
?>
