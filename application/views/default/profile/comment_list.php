<?php



if ($user_comment) {
    foreach ($user_comment as $comment) {
        $site_setting_date = $site_setting['date_format'];
        $date_added = $comment['date_added'];
        $comments = $comment['comments'];
        $comment_type = $comment['comment_type'];
        $user_id = $comment['user_id'];
        $project_title_comment = $comment['project_title'];
        $url_project_title_comment = $comment['url_project_title'];
        $project_id_comment = $comment['project_id'];
        $project_user_id = $comment['puser_id'];
        $image = $comment['image'];
        $comment_profile_slug = $comment['profile_slug'];

        if ($image != '') {
            if (file_exists(base_path() . 'upload/user/user_small_image/' . $image)) {
                $src = base_url() . 'upload/user/user_small_image/' . $image;
            } else {
                $src = base_url() . 'upload/user/user_small_image/no_man.jpg';
            }
        } else {
            $src = base_url() . 'upload/user/user_small_image/no_man.jpg';
        }

        if ($comment_type == 1) {
            if ($this->session->userdata('user_id') > 0) {
                if ($this->session->userdata('user_id') == $user_id || $this->session->userdata('user_id') == $project_user_id) {
                    ?>
                    <li class="media">
                        <div class="media-left"><a href="<?php echo site_url('user/' . $comment_profile_slug); ?>"><img
                                    class="img-thumbnail" src="<?php echo $src; ?>"></a></div>
                        <div class="media-body">
                            <p class="comment-no"><?php echo COMMENTED_ON; ?>:</p>
                            <h4 class="media-heading"><a
                                    href="<?php echo site_url('projects/' . $url_project_title_comment . '/' . $project_id_comment); ?>"><?php echo $project_title_comment; ?></a>
                            </h4>

                            <p><?php echo $comments; ?></p>

                            <p><span
                                    class="media-time"><?php echo date($site_setting_date, strtotime($date_added)) . " " . AT . " " . date("H:i a", strtotime($date_added)); ?></span>
                            </p>
                        </div>
                    </li>
                <?php
                }
            }
        } else {
            ?>
            <li class="media">
                <div class="media-left"><a href="<?php echo site_url('user/' . $comment_profile_slug); ?>"><img
                            class="img-thumbnail" src="<?php echo $src; ?>"></a></div>
                <div class="media-body">
                    <p class="comment-no"><?php echo COMMENTED_ON; ?>:</p>
                    <h4 class="media-heading"><a
                            href="<?php echo site_url('projects/' . $url_project_title_comment . '/' . $project_id_comment); ?>"><?php echo $project_title_comment; ?></a>
                    </h4>

                    <p><?php echo $comments; ?></p>

                    <p><span
                            class="media-time"><?php echo date($site_setting_date, strtotime($date_added)) . " " . AT . " " . date("H:i a", strtotime($date_added)); ?></span>
                    </p>
                </div>
            </li>
        <?php
        }
    }

    ?>

    <?php

    if ($total_row_display <= 10) {
        $display = 'none';
    } else {
        $display = 'block';
    }
    ?>
    <li class="show-more more_comments" style="display:<?php echo $display; ?>"><a href="javascript:void(0)"
                                                                                   onclick="show_more_comments(<?php echo $offset + 10; ?>)"><?php echo SHOW_MORE_COMMENTS; ?></a>
    </li>

<?php
} else {
    ?>
    <li class="no-data"><?php echo THERE_IS_NO_COMMENT_FOR_THIS_PROJECT; ?>.</li>
<?php
}
?>
								
