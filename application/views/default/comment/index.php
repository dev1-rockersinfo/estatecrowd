  
<?php //echo "<pre>";print_r($user_comment);die;?>

   <section>
        <div class="db-head">
            <div class="container">
                <div class="page-title">
                    <h2>My Comments</h2>
                </div>
            </div>
        </div>
        <div class="grey-bg" id="section-area">         
            <div class="container">
                <div class="row">
                    <div class="col-md-3 db-mc-left">
                    <?php echo $this->load->view('default/dashboard_sidebar'); ?>
                        
                    </div>
                    <div class="col-md-9 db-mc-right">
                    <div class="explore-head-right mch">
                   
                    <div class="select-search pull-right" id="searchType">
                  
                 <select class="selectpicker" onchange="getcomments(this.value)" >
                 <option value="all" data-tokens="Property">All</option>
                    <option value="property" data-tokens="Property">Property</option>
                    <option value="campaign" data-tokens="Campaign">Campaign</option>
                     <option value="crowd" data-tokens="Crowd">Crowd</option>
                  </select>
              
                </div>
                </div>
                <div class="cc-comments">
                        <ul class="comments-list">
                      
                    </ul>
                </div>
            </div>
        </div>
    </section>     
 
    <script type="text/javascript">
      $(document).ready(function(){      
 // $( ".selectpicker" ).before( "<span>Sort By:</span>" );
     
        
        // Change Placeholder
         $(document).on("click", "#searchType li a", function(){ 
            $("#searchBox").attr("placeholder", $(this).data("tokens"));
               $(".dropdown-menu li a .text .av").remove();
            
        });        
      });
      
    </script>

    <script type="text/javascript">

    getcomments('all');

  function deletecomment(id,crowd_id,property_id) {


        if (confirm("Are you sure you want to delete comment ?") == true) 
        {
          PopupOpen();
          var mapdata = {
              "comment_id":id,
              "crowd_id": crowd_id,
              "property_id":property_id
          };
         
         
          var getStatusUrl = '<?php echo site_url('comment/delete_comment')?>';
          $.ajax({
              url: getStatusUrl,
              dataType: 'json',
              type: 'POST',
              timeout: 99999,
              global: false,
              data: mapdata,
              success: function (data) {

                  if (data.success != "") {
                    $("#remove_div_"+id).hide();
                   
                  }
                  PopupClose();
              },
              error: function (XMLHttpRequest, textStatus, errorThrown) {
              }
          });
        }
    }
    function getcomments(value)
    {
         var mapdata = {
              "search_value":value,
              
          };
         
       var getStatusUrl = '<?php echo site_url('comment/search_comment')?>';
          $.ajax({
              url: getStatusUrl,
              dataType: 'text',
              type: 'POST',
              timeout: 99999,
              global: false,
              data: mapdata,
              success: function (data) {

                 $(".comments-list").html('');
                
                $(".comments-list").html(data);
              },
              error: function (XMLHttpRequest, textStatus, errorThrown) {
              }
          });
    }
</script>