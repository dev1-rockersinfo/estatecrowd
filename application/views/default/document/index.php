
    <section>
	  	<div class="db-head">
	      	<div class="container">
	      		<div class="page-title">
		        	<h2>Document Centre</h2>
		        </div>
	      	</div>
	  	</div>
	  	<div class="grey-bg" id="section-area">	  		
	      	<div class="container">
	      		<div class="row">
		      		<div class="col-md-3 db-mc-left">
                <?php echo $this->load->view('default/dashboard_sidebar'); ?>
		      			
		      		</div>
		      		<div class="col-md-9 db-mc-right">

                <?php
                  if($documents){ ?>
                    <ul class="doc-list">
                    <?php
                    foreach ($documents as $document) { 
                        $name = $document['document'];
                        $id = $document['doc_id'];
                      ?>

                        <li><a href="<?=site_url("document/download_doc/$id")?>"><i class="fa fa-file-text-o"></i><?=$name?></a>
                         <div class="badge-box">
                         <span class="download"><a href="<?=site_url("document/download_doc/$id")?>"><i class="fa fa-download"></i></a></span>
                         </div>
                         </li> 
                    <?php 
                    }?>

                    </ul>
                    <?php
                  }else{ ?>
                   <ul class="doc-list">

                    <li class="no_record_found">There are no documents.</li>
                    </ul>

                  <?php } ?>
					 </div>
			      		
	      		</div>
	      	</div>
	  	</div>
    </section>