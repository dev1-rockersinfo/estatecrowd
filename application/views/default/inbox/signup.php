<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/validation.css"/>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/additional-methods.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.maskedinput.js"></script>

<script>
    // validations
    jQuery(function ($) {

        $("#user_name").mask("hhh?hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh", {placeholder: ""});
        // validate signup form on keyup and submit

        var validator = $("#signup_frm").validate({
            rules: {
                user_name: {
                    required: true,
                    maxlength: 40,
                    minlength: 3,
                    maxWords: 2,
                    minWords: 2
                },

                email: {
                    required: true,
                    email: true
                },
                remail: {
                    required: true,
                    email: true,
                    equalTo: "#email"
                },
                password: {
                    required: true,
                    minlength: 8,
                    maxlength: 20
                },
                cpassword: {
                    required: true,
                    minlength: 8,
                    maxlength: 20,
                    equalTo: "#password"
                },

                agree: {
                    required: true,
                }
            },
            messages: {
                user_name: {
                    required: "Enter first & last name.",
                    maxlength: "User name can not be more than 39 character. ",
                    minlenght: "User name can not be less than 3 character. ",
                    maxWords: "Enter first & last name only. ",
                    minWords: "Enter first & last name."
                },

                email: {
                    required: "Email is required.",
                    email: "Enter valid Email."
                },

                remail: {
                    required: "Retype Email is required.",
                    email: "Enter valid Email.",
                    equalTo: "Email does not match."
                },

                password: {
                    required: 'Password is required.',
                    minlength: "Enter atleast 8 character.",
                    maxlength: "You can not enter more than 20 characters."
                },
                cpassword: {
                    required: "Retype Password is required.",
                    minlength: "Enter atleast 8 character.",
                    maxlength: "You can not enter more than 20 characters.",
                    equalTo: "Password does not match."
                },
                agree: "Please Accept Terms & Conditions."


            },
            // the errorPlacement has to take the table layout into account
            errorPlacement: function (error, element) {
                if (element.is(":radio"))
                    error.insertAfter(element.parent());
                else if (element.is(":checkbox"))
                    error.insertAfter(".redioB_C_T");
                else if (element.hasClass('book_job_now_price_input')) {
                    error.insertAfter(element.next().next());

                }
                else {
                    if (element.attr('name') == 'job_price') {
                        error.insertAfter(element.next());
                    }
                    else
                        error.insertAfter(element);
                }
            },
            // specifying a submitHandler prevents the default submit, good for the demo
            /*submitHandler: function() {
             alert("submitted!");
             },*/
            // set this class to error-labels to indicate valid fields
            success: function (label) {
                // set &nbsp; as text for IE
                /*	label.html("&nbsp;").addClass("checked");*/

            },
            /*highlight: function(element, errorClass) {
             $(element).parent().next().find("." + errorClass).removeClass("checked");
             },*/
            onkeyup: function (element) {
                $(element).valid();
            },
            onfocusout: function (element) {
                $(element).valid();
            },

        });

    });

</script>
<?php $site_name = $site_setting['site_name'];

$data = array(
    'facebook' => $this->fb_connect->fb,
    'fbSession' => $this->fb_connect->fbSession,
    'user' => $this->fb_connect->user,
    'uid' => $this->fb_connect->user_id,
    'fbLogoutURL' => $this->fb_connect->fbLogoutURL,
    'fbLoginURL' => $this->fb_connect->fbLoginURL,
    'base_url' => site_url('home/facebook'),
    'appkey' => $this->fb_connect->appkey,
);
?>
<section>
    <div class="content_part_top pTB18">
        <div class="main">

            <!--Left part-->
            <div class="left_columm">
                <?php
                $att = array('id' => 'signup_frm', 'name' => 'signup_frm');
                echo form_open('home/signup', $att);
                ?>
                <!--<form method="get" action="dashboard.php">-->
                <?php if ($error) { ?>
                    <div class="validation">
                        <?php echo $error; ?>
                    </div>
                <?php } ?>
                <div class="heading_strip"><h1><?php echo SIGNUP; ?></h1></div>
                <div class="signup_form">
                    <h2><?php echo NEW_TO, $site_name; ?>?</h2>

                    <div class="fill_item">
                        <div class="login-label"><?php echo NAME; ?><span class="fonts_red">*</span> :</div>
                        <div class="login-fild">
                            <input type="text" name="user_name" id="user_name" value="<?php echo $user_name ?>"
                                   class="btn_input"></div>
                        <div class="clear"></div>
                    </div>

                    <div class="fill_item">
                        <div class="login-label"><?php echo EMAIL_ADDRESS; ?><span class="fonts_red">*</span> :</div>
                        <div class="login-fild"><input type="text" name="email" id="email" value="<?php echo $email; ?>"
                                                       class="btn_input"></div>
                        <div class="clear"></div>
                    </div>

                    <div class="fill_item">
                        <div class="login-label"><?php echo RETYPE_EMAIL; ?><span class="fonts_red">*</span> :</div>
                        <div class="login-fild"><input type="text" name="remail" id="remail" value="" class="btn_input">
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div class="fill_item">
                        <div class="login-label"><?php echo PASSWORD; ?><span class="fonts_red">*</span> :</div>
                        <div class="login-fild"><input type="password" name="password" id="password" value=""
                                                       class="btn_input"></div>
                        <div class="clear"></div>
                    </div>

                    <div class="fill_item">
                        <div class="login-label"><?php echo RETYPE_PASSWORD; ?><span class="fonts_red">*</span> :</div>
                        <div class="login-fild"><input type="password" name="cpassword" id="cpassword" value=""
                                                       class="btn_input"></div>
                        <div class="clear"></div>
                    </div>

                    <div class="fill_item">
                        <div class="fild-redio">
                            <div class="redioB_C"><input type="checkbox" name="agree" id="agree" value="newsletter">
                            </div>
                            <div
                                class="redioB_C_T"><?php echo DISCOVER_NEW_PROJECTS_WITH_WEEKLY_NEWSLETTER_BY_SIGNING_UP_YOU_AGREE_TO_OUR ?>
                                <a href="<?php echo site_url('content/term'); ?>"
                                   class="fancybox fancybox.iframe"><?php echo TERMS_OF_USE; ?></a>.
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <input type="hidden" value="<?php echo $invite_code; ?>" name="invite_code">
                    <input type="submit" value="Sign Up" class="signup" name="<?php echo SIGN_UP; ?>"></form>

                </div>

                <div class="clear"></div>
            </div>
            <!--Left Part End-->

            <!--right Part Start-->
            <div class="right_columm">

                <div class="heading_strip"><h1>Social sign up</h1></div>

                <h4><?php echo NEW_TO . ' ' . $site_name; ?>?</h4>

                <div class="pTB18 facebook-signup"><a href="<?php echo $data['fbLoginURL']; ?>"><img
                            src="<?php echo base_url(); ?>images/facebook-strip.jpg"></a>
                    <a href="<?php echo $data['fbLogoutURL']; ?>"><?php //echo SIGN_OUT; ?></a>
                </div>
                <div class="pTB18 facebook-signup"><a href="<?php echo site_url("home/twitter_auth"); ?>"><img
                            src="<?php echo base_url(); ?>images/twitter-strip.jpg"></a>
                    <a href="<?php echo $data['fbLogoutURL']; ?>"><?php //echo SIGN_OUT; ?></a>
                </div>
                <div class="pTB18 facebook-signup"><a href="<?php echo site_url("home/linkdin_auth"); ?>"><img
                            src="<?php echo base_url(); ?>images/linkedin_strip.jpg"></a>
                    <a href="<?php echo $data['fbLogoutURL']; ?>"><?php //echo SIGN_OUT; ?></a>
                </div>
                <p><?php echo WE_WILL_NEVER_POST_ANYTHING_WITHOUT_YOUR_PERMISSION; ?></p>

            </div>


            <script type="text/javascript">
                function terms() {
                    window.open('<?php echo 'terms_and_conditions';?>', '<?php echo TERMS_CONDITIONS; ?>', 'height=500,width=500,top=250,left=300,scrollbars=1');
                }
            </script>


            <!--right Part End-->
            <div class="clear"></div>
        </div>
    </div>
</section>
