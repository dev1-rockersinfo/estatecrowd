  <script type="text/javascript">
      
    function list_message(message_id)
    {
            
            list_message_data(message_id);
    }

     var first_message_id = '<?php echo  $inbox_messages[0]['message_id']?>';
     if(first_message_id > 0)
     {
        list_message_data(first_message_id);
    }
    
    function list_message_data(message_id) 
    {
        $.post("<?php echo site_url('inbox/message_conversation'); ?>", {message_id: message_id}, function (result) {

            $('#load-message-list').html(result);
            var current_message_id = $("#message_id").val();
            
            if(current_message_id == message_id)
            {
                $(".msg-list li").removeClass('active');
                $(".common_message_"+message_id).addClass('active');
            }
           

        });

    }

  </script>

  <section>
        <div class="faqs-head">
            <div class="container">
                <div class="page-title">
                    <h2>Messages</h2>
                </div>
            </div>
        </div>
        <div class="grey-bg" id="section-area">         
            <div class="container">
                <div class="row">
                    <div class="col-md-3 db-chat-left">
                      <?php echo $this->load->view('default/dashboard_sidebar'); ?>
                    </div>
                    <div class="col-md-9 db-chat-right">
                    <div class="row">
                    <div class="col-md-4 msg-sort ">
                  
                    <ul class="msg-list">
 <?php
                $site_setting = site_setting();
                $site_name = $site_setting['site_name'];
                if ($inbox_messages) {
                    $is_read = 1; ?>
               
                <?php
                    foreach ($inbox_messages as $inbox_message) {
                        $receiver_id = $inbox_message['receiver_id'];
                        $sender_id = $inbox_message['sender_id'];
                        $message_type = $inbox_message['type'];
                        if ($receiver_id == $this->session->userdata('user_id')) {
                            $user_data = UserData($sender_id);
                        } else {
                            if ($message_type == 2) {
                                $user_data = UserData($sender_id);
                            } else {
                                $user_data = UserData($receiver_id);
                            }
                        }
                        $user_name = $user_data[0]['user_name'];
                        $last_name = $user_data[0]['last_name'];
                        $profile_slug = $user_data[0]['profile_slug'];
                        $user_image = $user_data[0]['image'];
                        $tw_id = $inbox_message['tw_id'];
                        $fb_uid = $inbox_message['fb_uid'];
                        $tw_screen_name = $inbox_message['tw_screen_name'];
                        $message_subject = $inbox_message['message_subject'];
                        $message_content = $inbox_message['message_content'];
                        $message_id = $inbox_message['message_id'];
                        $date_added = $inbox_message['date_added'];
                        $reply_message_id = $inbox_message['reply_message_id'];
                        $is_read = $inbox_message['is_read'];
                        $is_read_message = $this->inbox_model->get_read_message($message_id);
                        if ($is_read_message != 0) {
                            $is_read_test = $is_read_message['is_read'];
                        }
                       
                        
                        
                        $get_unread_count=getMessageUnreadCount($message_id,$this->session->userdata('user_id'));
                      
                        $red = '';
                        if ($get_unread_count>0) {
                            $red = 'red';
                        }
                        

                        ?>
                        <?php
                        if ($message_type == 1 || $message_type == 2) {

                            //$getadmindata = adminsendmsg(1);


                            ?>
                        <li class="common_message_<?php echo $message_id; ?>" onclick="list_message(<?php echo $message_id; ?>)">
                        
                        <div class="msg-left">
                        <a href="<?php echo site_url('user/'.$profile_slug);?>">
                             <?php
                                        if (isset($user_image) && $user_image != '') {
                                            if (file_exists(base_path() . 'upload/user/user_small_image/' . $user_image)) {
                                                ?>
                                                <img
                                                    src="<?php echo base_url(); ?>upload/user/user_small_image/<?php echo $user_image; ?>"
                                                    alt="noimage"/>
                                            <?php } else { ?>
                                                <img src="<?php echo base_url(); ?>upload/user/user_small_image/no_man.jpg"
                                                    />
                                            <?php }
                                        } else { ?>
                                            <img src="<?php echo base_url(); ?>upload/user/user_small_image/no_man.jpg"
                                                 lt="noimage"/>
                                        <?php } ?>

                        </a></div>
                          <div class="msg-right">
                                 <a href="<?php echo site_url('user/'.$profile_slug);?>"><h3> <?php echo $user_name . ' ' . $last_name; ?></h3> </a>
                                 <span class="date"> <?php echo getDuration($date_added); ?></span>
                                 <span class="text"><?php echo $message_subject; ?>  </span>
                              </div>
                            
                        </li>

                         <?php } else { ?>

                          <li class="common_message_<?php echo $message_id; ?>" onclick="list_message(<?php echo $message_id; ?>)">
                        
                        <div class="msg-left">
                        <a href="<?php echo site_url('user/'.$profile_slug);?>">
                             <?php
                                        if (isset($user_image) && $user_image != '') {
                                            if (file_exists(base_path() . 'upload/user/user_small_image/' . $user_image)) {
                                                ?>
                                                <img
                                                    src="<?php echo base_url(); ?>upload/user/user_small_image/<?php echo $user_image; ?>"
                                                    alt="noimage"/>
                                            <?php } else { ?>
                                                <img src="<?php echo base_url(); ?>upload/user/user_small_image/no_man.jpg"
                                                    />
                                            <?php }
                                        } else { ?>
                                            <img src="<?php echo base_url(); ?>upload/user/user_small_image/no_man.jpg"
                                                 lt="noimage"/>
                                        <?php } ?>

                        </a></div>
                          <div class="msg-right">
                                 <a href="<?php echo site_url('user/'.$profile_slug);?>"><h3> <?php echo $user_name . ' ' . $last_name; ?></h3> </a>
                                 <span class="date"> <?php echo getDuration($date_added); ?></span>
                                 <span class="text"><?php echo $message_subject; ?>  </span>
                              </div>
                             
                        </li>
                     <?php } ?>

                    <?php
                    } ?>
                           
                <?php } else {
                    ?>
                   <li class="no_record_found"> <?php echo THERE_ARE_NO_MESSAGE_IN_INBOX;?> </li>
                <?php
                }
                ?>
                    </ul>
                  
                    </div>
                   <div class="col-md-8 msg-full" id="load-message-list">
              
                        
                            
                                               
                   </div>                 
                            
                    </div>
                        
                </div>
            </div>
        </div>
    </section>

