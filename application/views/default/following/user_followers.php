<section>
      <div class="db-head">
          <div class="container">
            <div class="page-title">
              <h2>Followers</h2>
            </div>
          </div>
      </div>
      <div class="grey-bg" id="section-area">       
          <div class="container">
            <div class="row">
              <div class="col-md-3 db-mc-left">
                      <?php echo $this->load->view('default/dashboard_sidebar'); ?>
                
              </div>
              <div class="col-md-9 db-mc-right">
                <div class="following">
              <ul class="following-list" id="user_follower_id">

                <?php 


                if($user_followers)
                { 
                          foreach ($user_followers as $key => $user_follower_data) {
                            # code...
                       
                            $user_image = $user_follower_data['image'];
                            $user_name = $user_follower_data['user_name'];
                            $last_name = $user_follower_data['last_name'];
                            $profile_slug = $user_follower_data['profile_slug'];
                            $user_follow_date = $user_follower_data['user_follow_date'];
                            $follow_user_id = $user_follower_data['follow_by_user_id'];

                  ?>
                      <li class="clearfix">
                        <div class="following-lt avtar-sm">
                     <a href="javascript:void(0)"> 
                    <?php if ($user_image != '' && is_file("upload/user/user_small_image/" . $user_image)) {
                        ?>
                        <img src="<?php echo base_url(); ?>upload/user/user_small_image/<?php echo $user_image; ?>"/>
                    <?php
                    } else {
                        ?>
                        <img src="<?php echo base_url(); ?>upload/user/user_small_image/no_man.jpg"/>
                    <?php } ?> </a>
                        </div>
                         <div class="following-rt clearfix">
                         <h3><a href="<?php echo site_url('user/'.$profile_slug);?>"><?php echo $user_name.' '.$last_name;?></a></h3>
                        <span class="following-time"><?php echo dateformat($user_follow_date);?></span>
                      </div>
                        <div class="following-btn"  id="follow_unfollow_follower<?php echo $follow_user_id; ?>">
                        <span class="follow-btn">
                        <?php
                            if ($this->session->userdata('user_id')) {
                                if ($follow_user_id != $this->session->userdata('user_id')) {
                                    $chk_follower = check_is_follower($follow_user_id, $this->session->userdata('user_id'));

                                    if ($chk_follower) {
                                        ?>
                                        <a href="javascript://" class="btn btn-primary btn-small"
                                           id="followme<?php echo $follow_user_id; ?>"
                                           onclick="unfollowuser(<?php echo $follow_user_id; ?>,'2')"
                                           onmouseover="set_followingtext(<?php echo $follow_user_id; ?>,0);"
                                           onmouseout="set_followingtext(<?php echo $follow_user_id; ?>,1);"><i
                                                class="fa fa-heart"></i> <?php echo FOLLOWING; ?></a>
                                      <?php
                                        } else {
                                            ?>
                                            <a href="javascript://" class="btn btn-primary btn-small"
                                               id="followme<?php echo $follow_user_id; ?>"
                                               onclick="followuser(<?php echo $follow_user_id; ?>,'1')"><i
                                                    class="fa fa-heart"></i> <?php echo FOLLOW; ?></a>
                                        <?php
                                        }

                                        ?>




                                <?php } 
                            }
                            ?>

                        </span>
                          </div>
                        </li>    
                    <?php 
                      }

                  } else
                      { ?>
                        <li class="no_record_found">There are no following user.</li>
                    <?php } ?>   
                        
                    </ul>
             </div>
                
            </div>
          </div>
      </div>
    </section>

    <script type="text/javascript">
       function followuser(id, user) {
        if (id == '') {
            return false;
        }

        var strURL = '<?php echo base_url() . 'follower/userFollower/';?>' + id;
         PopupOpen();
        var xmlhttp;
        if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        }
        else {// code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                 PopupClose();
            //console.log("out if");console.log($.trim(xmlhttp.responseText));
                if ($.trim(xmlhttp.responseText) == 'Follow') 
                {//console.log("in if")
                   document.getElementById("follow_unfollow_follower"+id).innerHTML = '<span class="follow-btn"><a href="javascript:void(0)" class="btn btn-primary btn-small" id="followme" onclick="unfollowuser(' + id + ',0)" onmouseover="set_followingtext(0);" onmouseout="set_followingtext(1);"><i class="fa fa-heart"></i> <?php echo FOLLOWING; ?></a></span>';
                        savefollowerlist(id);
                    //document.getElementById("follow_unfollow").innerHTML='following';
                }
            }
        }
        xmlhttp.open("GET", strURL, true);
        xmlhttp.send();
    }
    function unfollowuser(id, user) {

        $('#followme').removeAttr('onmouseover').removeAttr('onmouseout');
        //alert(id);
        if (id == '') {
            return false;
        }
        var strURL = '<?php echo site_url('follower/userUnfollower');?>/' + id;
         PopupOpen();
        var xmlhttp;
        if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        }
        else {// code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {

            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                PopupClose();
            //console.log("out if");console.log($.trim(xmlhttp.responseText));
                if ($.trim(xmlhttp.responseText) == 'unfollow') {//console.log("in if");
                   
                     document.getElementById("follow_unfollow_follower"+id).innerHTML = '<span class="follow-btn"><a href="javascript:void(0)" class="btn btn-primary btn-small" id="followme"  onclick="followuser(' + id + ',0)"><i class="fa fa-heart"></i> <?php echo FOLLOW; ?></a></span>';
                        savefollowerlist(id);
                }
            }
        }
        xmlhttp.open("GET", strURL, true);
        xmlhttp.send();
    }
    function set_followingtext(id, set) {
        if (set == 0) {
          $('.follow-btn').attr('class', function() {
            return $(this).attr('class').replace('follow-btn', 'unfollow-btn');
          });
            document.getElementById("followme" + id).innerHTML = '<i class="fa fa-heart-o"></i> <?php echo UNFOLLOW; ?>';
        }
        else {
           $('.unfollow-btn').attr('class', function() {
            return $(this).attr('class').replace('unfollow-btn', 'follow-btn');
          });
            document.getElementById("followme" + id).innerHTML = '<i class="fa fa-heart"></i> <?php echo FOLLOWING; ?>';
        }
    }

      function savefollowerlist(id) {
        var user_id_value = id;
        //alert(user_id_value);
        var mapdata = {"user_id": user_id_value};
        // var mapdata = "'perk_title=' + perk_title_value" ;
        //var mapdata_description = 'perk_description=' + perk_description_value;
        var getStatusUrl = '<?php echo site_url('follower/ajax_user_follower');?>';
        ////alert(getStatusUrl)
        $.ajax({
            url: getStatusUrl,
            dataType: 'text',
            type: 'POST',
            timeout: 99999,
            global: false,
            data: mapdata,
            //data:{'perk[]':mapdata},
            success: function (data) {
                //alert(data);
                document.getElementById('user_follower_id').innerHTML = data;
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
            }
        });
    }

    </script>