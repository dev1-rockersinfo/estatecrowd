<?php 


                if($user_followers)
                { 
                          foreach ($user_followers as $key => $user_follower_data) {
                            # code...
                       
                            $user_image = $user_follower_data['image'];
                            $user_name = $user_follower_data['user_name'];
                            $last_name = $user_follower_data['last_name'];
                            $profile_slug = $user_follower_data['profile_slug'];
                            $user_follow_date = $user_follower_data['user_follow_date'];
                            $follow_user_id = $user_follower_data['follow_by_user_id'];

                  ?>
                      <li class="clearfix">
                        <div class="following-lt avtar-sm">
                     <a href="javascript:void(0)"> 
                    <?php if ($user_image != '' && is_file("upload/user/user_small_image/" . $user_image)) {
                        ?>
                        <img src="<?php echo base_url(); ?>upload/user/user_small_image/<?php echo $user_image; ?>"/>
                    <?php
                    } else {
                        ?>
                        <img src="<?php echo base_url(); ?>upload/user/user_small_image/no_man.jpg"/>
                    <?php } ?> </a>
                        </div>
                         <div class="following-rt clearfix">
                         <h3><a href="<?php echo site_url('user/'.$profile_slug);?>"><?php echo $user_name.' '.$last_name;?></a></h3>
                        <span class="following-time"><?php echo dateformat($user_follow_date);?></span>
                      </div>
                        <div class="following-btn"  id="follow_unfollow_follower<?php echo $follow_user_id; ?>">
                        <span class="follow-btn">
                        <?php
                            if ($this->session->userdata('user_id')) {
                                if ($follow_user_id != $this->session->userdata('user_id')) {
                                    $chk_follower = check_is_follower($follow_user_id, $this->session->userdata('user_id'));

                                    if ($chk_follower) {
                                        ?>
                                        <a href="javascript://" class="btn btn-primary btn-small"
                                           id="followme<?php echo $follow_user_id; ?>"
                                           onclick="unfollowuser(<?php echo $follow_user_id; ?>,'2')"
                                           onmouseover="set_followingtext(<?php echo $follow_user_id; ?>,0);"
                                           onmouseout="set_followingtext(<?php echo $follow_user_id; ?>,1);"><i
                                                class="fa fa-heart"></i> <?php echo FOLLOWING; ?></a>
                                      <?php
                                        } else {
                                            ?>
                                            <a href="javascript://" class="btn btn-primary btn-small"
                                               id="followme<?php echo $follow_user_id; ?>"
                                               onclick="followuser(<?php echo $follow_user_id; ?>,'1')"><i
                                                    class="fa fa-heart"></i> <?php echo FOLLOW; ?></a>
                                        <?php
                                        }

                                        ?>




                                <?php } 
                            }
                            ?>

                        </span>
                          </div>
                        </li>    
                    <?php 
                      }

                  } else
                      { ?>
                        <li class="no_record_found">There are no following user.</li>
                    <?php } ?> 