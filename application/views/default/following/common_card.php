<?php

                    $property_id = $property['property_id'];
                    $property_user_id = $property['user_id'];
                    $property_address = $property['property_address'];
                    $property_url = $property['property_url'];
                    $status = $property['status'];
                    $my_campaign='';
                    if(isset($property['my_campaign']))
                    {
                        $my_campaign = $property['my_campaign'];
                    }
                    $is_delete = 'no';
                    if($status == 0 or $status == 1)
                    {
                        $is_delete ='yes';
                    }
                    if(is_file(base_path().'upload/property/small/'.$property['cover_image']) && $property['cover_image'] != '')
                    {
                         $cover_image = base_url().'upload/property/small/'.$property['cover_image'];
                    }
                    else
                    {
                        $cover_image = base_url().'upload/property/small/no_img.jpg';
                    }

                    $bedrooms = $property['bedrooms'];
                    $bathrooms = $property['bathrooms'];
                    $cars = $property['cars'];

            ?>

                      <li class="col-md-4"> 
                  <div class="db-fp-wrap">
                        <div class="db-fp-thumb">
                         <a href="<?php echo site_url('properties/'.$property_url);?>"><img src="<?php echo $cover_image;?>"></a>
                        <span class="overlay"></span>
                        <span class="lbl"><a href="javascript://" onclick="unfollowproperty(<?php echo $property_id; ?>)"><i class="fa fa-heart"></i></a></span>
                        
                       
                        </div>
                        <div class="db-fp-title">
                           <h3><a href="<?php echo site_url('properties/'.$property_url);?>"><?php echo $property_address;?></a></h3>
                            
                        </div>
                       
                        <div class="db-fp-btn">
                        <ul class="db-fp-btn-grp">
                       
                      <li>
                        <span class="info-num"><?php echo $bedrooms;?></span>
                        <span class="icon-wrapper"><img src="<?php echo base_url();?>images/icon-bed.png"></span>
                        <span class="info-name">bedrooms</span>
                      </li>
                      <li>
                        <span class="info-num"><?php echo $bathrooms;?></span>
                        <span class="icon-wrapper"><img src="<?php echo base_url();?>images/icon-bath.png"></span>
                        <span class="info-name">bathrooms</span>
                      </li>
                      <li>
                        <span class="info-num"><?php echo $cars;?></span>
                        <span class="icon-wrapper"><img src="<?php echo base_url();?>images/icon-car.png"></span>
                        <span class="info-name">cars</span>
                      </li>
                   
                        </ul>   
                        </div>
                        
                        
                  </div>
                  </li>

              