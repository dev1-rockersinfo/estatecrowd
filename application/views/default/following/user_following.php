<section>
      <div class="db-head">
          <div class="container">
            <div class="page-title">
              <h2>Following</h2>
            </div>
          </div>
      </div>
      <div class="grey-bg" id="section-area">       
          <div class="container">
            <div class="row">
              <div class="col-md-3 db-mc-left">
                      <?php echo $this->load->view('default/dashboard_sidebar'); ?>
                
              </div>
              <div class="col-md-9 db-mc-right">
                <div class="following">
              <ul class="following-list" id="user_following_id">

                <?php 
                if($user_following)
                { 
                          foreach ($user_following as $key => $user_following_data) {
                            # code...
                       
                            $user_image = $user_following_data['image'];
                            $user_name = $user_following_data['user_name'];
                            $last_name = $user_following_data['last_name'];
                            $profile_slug = $user_following_data['profile_slug'];
                            $user_follow_date = $user_following_data['user_follow_date'];
                            $follow_user_id = $user_following_data['follow_user_id'];

                  ?>
                      <li class="clearfix">
                        <div class="following-lt avtar-sm">
                     <a href="javascript:void(0)"> 
                    <?php if ($user_image != '' && is_file("upload/user/user_small_image/" . $user_image)) {
                        ?>
                        <img src="<?php echo base_url(); ?>upload/user/user_small_image/<?php echo $user_image; ?>"/>
                    <?php
                    } else {
                        ?>
                        <img src="<?php echo base_url(); ?>upload/user/user_small_image/no_man.jpg"/>
                    <?php } ?> </a>
                        </div>
                         <div class="following-rt clearfix">
                         <h3><a href="<?php echo site_url('user/'.$profile_slug);?>"><?php echo $user_name.' '.$last_name;?></a></h3>
                        <span class="following-time"><?php echo dateformat($user_follow_date);?></span>
                      </div>
                        <div class="following-btn">
                        <span class="unfollow-btn"><a type="button" class="btn btn-primary btn-small" href="javascript://" onclick="unfollowuser(<?php echo $follow_user_id; ?>,'2')"><i class="fa fa-heart"></i> Unfollow</a></span>
                          </div>
                        </li>    
                    <?php 
                      }

                  } else
                      { ?>
                        <li class="no_record_found">There are no following user.</li>
                    <?php } ?>   
                        
                    </ul>
             </div>
                
            </div>
          </div>
      </div>
    </section>

    <script type="text/javascript">
      
  function unfollowuser(id, user) {

        $('#followme').removeAttr('onmouseover').removeAttr('onmouseout');
        //alert(id);
        if (id == '') {
            return false;
        }
        var strURL = '<?php echo site_url('follower/user_unfollower');?>/' + id;
         PopupOpen();
        var xmlhttp;
        if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        }
        else {// code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {

            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                PopupClose();
              savefollowinglist(id);
            }
        }
        xmlhttp.open("GET", strURL, true);
        xmlhttp.send();
    }

function savefollowinglist(id) {
        var user_id_value = id;

        var mapdata = {"user_id": user_id_value};
        // var mapdata = "'perk_title=' + perk_title_value" ;
        //var mapdata_description = 'perk_description=' + perk_description_value;
        var getStatusUrl = '<?php echo site_url('follower/ajax_user_following');?>';
        ////alert(getStatusUrl)
        $.ajax({
            url: getStatusUrl,
            dataType: 'text',
            type: 'POST',
            timeout: 99999,
            global: false,
            data: mapdata,
            //data:{'perk[]':mapdata},
            success: function (data) {
                // alert(data);
                document.getElementById('user_following_id').innerHTML = data;
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
            }
        });
    }

    </script>