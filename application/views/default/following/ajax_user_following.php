<?php 
                if($user_following)
                { 
                          foreach ($user_following as $key => $user_following_data) {
                            # code...
                       
                            $user_image = $user_following_data['image'];
                            $user_name = $user_following_data['user_name'];
                            $last_name = $user_following_data['last_name'];
                            $profile_slug = $user_following_data['profile_slug'];
                            $user_follow_date = $user_following_data['user_follow_date'];

                  ?>
                      <li class="clearfix">
                        <div class="following-lt avtar-sm">
                     <a href="javascript:void(0)"> 
                    <?php if ($user_image != '' && is_file("upload/user/user_small_image/" . $user_image)) {
                        ?>
                        <img src="<?php echo base_url(); ?>upload/user/user_small_image/<?php echo $user_image; ?>"/>
                    <?php
                    } else {
                        ?>
                        <img src="<?php echo base_url(); ?>upload/user/user_small_image/no_man.jpg"/>
                    <?php } ?> </a>
                        </div>
                         <div class="following-rt clearfix">
                         <h3><a href="<?php echo site_url('user/'.$profile_slug);?>"><?php echo $user_name.' '.$last_name;?></a></h3>
                        <span class="following-time"><?php echo dateformat($user_follow_date);?></span>
                      </div>
                        <div class="following-btn">
                        <span class="unfollow-btn"><a type="button" class="btn btn-primary btn-small" href=""><i class="fa fa-heart"></i> Unfollow</a></span>
                          </div>
                        </li>    
                    <?php 
                      }

                  } else
                      { ?>
                        <li class="no_record_found">There are no following user.</li>
                    <?php } ?>   