
    <section>
	  	<div class="investment-head">
	      	<div class="container" >
           
	      		<div class="inv-counter col-md-11 col-md-offset-1">
                <div class="s-unit col-xs-2 col-sm-2">
                <div class="num active"><i class="fa fa-check"></i></div>
                <div class="text active">Select Unit</div>
                </div>
                <div class="s-unit col-xs-2 col-sm-2">
                <div class="num active">2</div>
                <div class="text active">Pay Deposit</div>
                </div>
                <div class="s-unit col-xs-2 col-sm-2">
                <div class="num">3</div>
                <div class="text ">Investment Confirmation</div>
                </div>
                <div class="s-unit col-xs-2 col-sm-2">
                <div class="num">4</div>
                <div class="text ">Invite Investors</div>
                </div>
                <div class="s-unit col-xs-2 col-sm-2">
                <div class="num">5</div>
                <div class="text ">End of Campaign</div>
                </div>
                
                </div>
                                  
	      	</div>
	  	</div>
	  		  		
	      	
    </section>
    <section class="investment-content">
    <div class="container">
  			
            <div class="investment-left col-md-8">
  				<div class="panel panel-default ec-panel">

 					 <div class="panel-heading">
   						 <h3 class="panel-title">Pay Deposit</h3>
 					 </div>
            <?php
                  if($this->session->flashdata('error_message_investment') != '') {

                ?>
                <div class="alert-danger alert alert-message">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <?php echo $this->session->flashdata('error_message_investment'); ?>
                </div>
            <?php
            } 
            ?>
 				 <div class="panel-body">
   					<form class="ec-form" method="post" action="<?php echo site_url('investment/deposit/'.$campaign_id.'/'.$investor_process_id);?>">
                    <div class="row">
                 <div class="form-group col-md-6">
   					 <label for="name">Name</label>
    				 <input  type="text" name="name" class="form-control" value="<?php echo $name;?>" id="name" placeholder="">
 				 </div>
                   <div class="form-group col-md-6">
   					 <label for="card-number">Card Number</label>
    				 <input   type="text" class="form-control " name="card_number" value="<?php echo $card_number;?>" id="card-number" placeholder="">
 				 </div>
                 </div>
                 <div class="row">
                 <div class="form-group col-md-9">
                 <div class="row">
                 <div class="col-md-6">
                  <label >Expiry Date</label>
                 <select class="selectpicker" name="expiry_month">
  					<option value="">Month</option>
 					 <option value="01"  <?php if($expiry_month == '01' ) echo 'selected="selected"' ;?>>January</option>
  					<option value="02" <?php if($expiry_month == '02' ) echo 'selected="selected"' ;?> >February</option>
 					 <option value="03" <?php if($expiry_month == '03' ) echo 'selected="selected"' ;?>>March</option>
 					 <option value="04" <?php if($expiry_month == '04') echo 'selected="selected"' ;?>>April</option>
            <option value="05" <?php if($expiry_month == '05' ) echo 'selected="selected"' ;?>>May </option>
            <option value="06" <?php if($expiry_month == '06' ) echo 'selected="selected"' ;?>>June</option>
           <option value="07" <?php if($expiry_month == '07' ) echo 'selected="selected"' ;?>>July</option>
           <option value="08" <?php if($expiry_month == '08') echo 'selected="selected"' ;?>>August</option>
            <option value="09" <?php if($expiry_month == '09' ) echo 'selected="selected"' ;?>>September</option>
            <option value="10" <?php if($expiry_month == '10' ) echo 'selected="selected"' ;?>>October</option>
           <option value="11" <?php if($expiry_month == '11' ) echo 'selected="selected"' ;?>>November</option>
           <option value="12" <?php if($expiry_month == '12' ) echo 'selected="selected"' ;?>>December</option>
				</select>
                </div>
                
                <div class="col-md-6">
                  <label>&nbsp;</label>
                 <select class="selectpicker" name="expiry_year">
  					<option >Year</option>
                    <?php 
         					$firstYear = (int)date('Y');
                  $lastYear = $firstYear + 20;
                  for($i=$firstYear;$i<=$lastYear;$i++)
                  { ?>
                      <option value="<?php echo $i;?>" <?php if($i == $expiry_year) echo 'selected="selected"' ;?>><?php echo $i ?></option>
            <?php      }

        ?>
				</select>
                </div>
				</div>
				</div>
                 
                <div class="form-group col-md-3">
   					 <label for="cvv">CVV</label>
    				 <input   type="text" class="form-control" id="cvv" name="cvv_number" value="<?php echo $cvv_number;?>" placeholder="">
 				 </div>
                 </div>
                 
                 <div class="btn-group">
                 <button type="submit" class="btn btn-primary btn-lg btn-block text-uppercase ">Pay <?php echo set_currency($deposit_amount);?></button>
                   <input type="hidden" name="deposite_status" id="deposite_status" value="<?php echo $form_submit;?>">
                 </div>
                    </form>	
                        
  				</div>

			</div>

       

  			</div>            
  <?php echo $this->load->view('default/investment_step/investment_sidebar'); ?>
</div>
</section>

<script type="text/javascript">
  $(function () {
 
var deposit_status = $("#deposite_status").val();

if(deposit_status == 'success')
{
  $( "#deposit_form" ).submit();
}

});

</script>
   