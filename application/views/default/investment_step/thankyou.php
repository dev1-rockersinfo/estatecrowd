
    <section>
	  	<div class="investment-head">
	      	<div class="container" >
           
	      		<div class="inv-counter col-md-11 col-md-offset-1">
                <div class="s-unit col-xs-2 col-sm-2">
                <div class="num active"><i class="fa fa-check"></i></div>
                <div class="text active">Select Unit</div>
                </div>
                <div class="s-unit col-xs-2 col-sm-2">
                <div class="num active"><i class="fa fa-check"></i></div>
                <div class="text active">Pay Deposit</div>
                </div>
                <div class="s-unit col-xs-2 col-sm-2">
                <div class="num active"><i class="fa fa-check"></i></div>
                <div class="text active">Investment Confirmation</div>
                </div>
                <div class="s-unit col-xs-2 col-sm-2">
                <div class="num active"><i class="fa fa-check"></i></div>
                <div class="text active">Invite Investors</div>
                </div>
                <div class="s-unit col-xs-2 col-sm-2">
                <div class="num active"><i class="fa fa-check"></i></div>
                <div class="text active">End of Campaign</div>
                </div>
                
                </div>
                                  
	      	</div>
	  	</div>
	  		  		
	      	
    </section>
    <section class="investment-content">
    <div class="container">
  			
            <div class="investment-left col-md-8">
  				<div class="panel panel-default ec-panel">
 					 <div class="panel-heading">
   						 <h3 class="panel-title">Thank you </h3>
 					 </div>
 				 <div class="panel-body">
   				   
              <p> Your have completed successfully</p>                    
             
  				</div>
			</div>
  			</div>            
  <?php echo $this->load->view('default/investment_step/investment_sidebar'); ?>
</div>
</section>
    