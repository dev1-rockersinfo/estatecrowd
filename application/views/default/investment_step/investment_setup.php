  <section>
      <div class="investment-head">
          <div class="container" >
           
            <div class="inv-counter col-md-11 col-md-offset-1">
                <div class="s-unit col-xs-2 col-sm-2">
                <div class="num active"><i class="fa fa-check"></i></div>
                <div class="text active">Select Unit</div>
                </div>
                <div class="s-unit col-xs-2 col-sm-2">
                <div class="num active"><i class="fa fa-check"></i></div>
                <div class="text active">Pay Deposit</div>
                </div>
                <div class="s-unit col-xs-2 col-sm-2">
                <div class="num active">3</div>
                <div class="text active">Investment Confirmation</div>
                </div>
                <div class="s-unit col-xs-2 col-sm-2">
                <div class="num">4</div>
                <div class="text ">Invite Investors</div>
                </div>
                <div class="s-unit col-xs-2 col-sm-2">
                <div class="num">5</div>
                <div class="text ">End of Campaign</div>
                </div>
                
                </div>
                                  
          </div>
      </div>
              
          
    </section>
    <section class="investment-content">
    <div class="container">
        
            <div class="investment-left col-md-8">
          <div class="panel panel-default ec-panel">
           <div class="panel-heading">
               <h3 class="panel-title">Investment Setup Form</h3>
           </div>
              <?php
                  if($this->session->flashdata('error_message_investment') != '') {

                ?>
                <div class="alert-danger alert alert-message">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <?php echo $this->session->flashdata('error_message_investment'); ?>
                </div>
            <?php
            } 
            ?>

              <?php
              if($success != '') {
                  ?>
                  <div class="alert-success alert alert-message">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                      <?php echo $success;
                            echo "<br>";
                            echo '<a href="'.$signurl.'" target="_blank">'.$signurl.'</a>';
                      ?>
                  </div>

                  <?php
              }
              ?>


         <div class="panel-body">
                 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis malesuada nunc eu velit commodo efficitur at quis nulla. Etiam suscipit sem id rhoncus congue.</p>
            <form class="ec-form" method="post" action="<?php echo site_url('investment/investment_setup/'.$campaign_id.'/'.$investor_process_id);?>">
                    <div class="row">
                 <div class="form-group col-md-6">
             <label for="name">Name</label>
             <input  type="text" class="form-control" name="name" id="name" value="<?php echo $name;?>" placeholder="">
         </div>
                   <div class="form-group col-md-6">
             <label for="company-name">Company Name</label>
             <input   type="text" class="form-control " id="company-name" name="company_name" placeholder="" value="<?php echo $company_name;?>">
         </div>
                 </div>
                 
                 <div class="row">
                 <div class="form-group col-md-6">
             <label for="email">Email</label>
             <input  type="text" class="form-control " id="email" placeholder="" name="email" value="<?php echo $email;?>">
         </div>
                   <div class="form-group col-md-6">
             <label for="contact">Contact Number</label>
             <input   type="text" class="form-control " id="contact" placeholder="" name="contact_number" value="<?php echo $contact_number;?>">
         </div>
                 </div>
                 
                 <div class="btn-group">
                 <button type="submit" class="btn btn-primary btn-lg btn-block text-uppercase ">SIGN & SUBMIT NOW</button>
                 </div>

              <div class="investment-setup-btm"> <span> or </span> <a href="">Sign Later</a></div>

                <?php if($success){ ?>
                <div class="btn-group" style="float: right">
                    <a href="<?=site_url('investment/invite_investor/'.$campaign_id.'/'.$investor_process_id)?>" class="btn btn-primary btn-lg btn-block text-uppercase ">Next</a>
                </div>
                <?php } ?>

                    </form>


                        
          </div>
      </div>
        </div>
            
             <?php echo $this->load->view('default/investment_step/investment_sidebar'); ?>
    </div>
    </section>
    