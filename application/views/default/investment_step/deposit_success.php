 <section>
	  	<div class="investment-head">
	      	<div class="container" >
           
	      		<div class="inv-counter col-md-11 col-md-offset-1">
                <div class="s-unit col-xs-2 col-sm-2">
                <div class="num active"><i class="fa fa-check"></i></div>
                <div class="text active">Select Unit</div>
                </div>
                <div class="s-unit col-xs-2 col-sm-2">
                <div class="num active">2</div>
                <div class="text active">Pay Deposit</div>
                </div>
                <div class="s-unit col-xs-2 col-sm-2">
                <div class="num active">3</div>
                <div class="text ">Investment Confirmation</div>
                </div>
                <div class="s-unit col-xs-2 col-sm-2">
                <div class="num">4</div>
                <div class="text ">Invite Investors</div>
                </div>
                <div class="s-unit col-xs-2 col-sm-2">
                <div class="num">5</div>
                <div class="text ">End of Campaign</div>
                </div>
                
                </div>
                                  
	      	</div>
	  	</div>
	  		  		
	      	
    </section>
    <section class="investment-content">
    <div class="container">
  			
            <div class="investment-left col-md-8">
  				<div class="panel panel-default ec-panel">
 					 <div class="panel-heading">
   						 <h3 class="panel-title">Payment Confirmation</h3>
 					 </div>
 				 <div class="panel-body">
   				    <div class="invite-investor">
              <p> Your payment completed successfully</p>
                     <div class="btn-group">
                 <a href="<?php echo site_url('investment/investment_setup/'.$campaign_id.'/'.$investor_process_id);?>" type="button" class="btn btn-primary btn-lg btn-block text-uppercase ">Next</a>
                 </div>   
                 </div>
  				</div>
			</div>
  			</div>            
  <?php echo $this->load->view('default/investment_step/investment_sidebar'); ?>
</div>
</section>