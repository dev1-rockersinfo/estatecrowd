<?php 

$property_share_url = site_url('properties/'.$property_url);
$property_image = base_url().'upload/property/commoncard/'.$cover_image;

?>


  <section>
	  	<div class="investment-head">
	      	<div class="container" >
           
	      		<div class="inv-counter col-md-11 col-md-offset-1">
                <div class="s-unit col-xs-2 col-sm-2">
                <div class="num active"><i class="fa fa-check"></i></div>
                <div class="text active">Select Unit</div>
                </div>
                <div class="s-unit col-xs-2 col-sm-2">
                <div class="num active"><i class="fa fa-check"></i></div>
                <div class="text active">Pay Deposit</div>
                </div>
                <div class="s-unit col-xs-2 col-sm-2">
                <div class="num active"><i class="fa fa-check"></i></div>
                <div class="text active">Investment Confirmation</div>
                </div>
                <div class="s-unit col-xs-2 col-sm-2">
                <div class="num active">4</div>
                <div class="text active">Invite Investors</div>
                </div>
                <div class="s-unit col-xs-2 col-sm-2">
                <div class="num">5</div>
                <div class="text ">End of Campaign</div>
                </div>
                
                </div>
                                  
	      	</div>
	  	</div>
	  		  		
	      	
    </section>
    <section class="investment-content">
    <div class="container">
  			
            <div class="investment-left col-md-8">
  				<div class="panel panel-default ec-panel">
 					 <div class="panel-heading">
   						 <h3 class="panel-title">Invite Other Investors</h3>
 					 </div>
 				 <div class="panel-body">
                <div class="invite-investor m-t-md">
              <span> Copy and Paste Link</span>
              <input class="invite-link" type="text"   id="disabledInput" value="<?php echo site_url('properties/'.$property_url);?>"  readonly>
                 <span>Or Invite other investors on social media</span>
               
                 <div class="row soc-invite">
                 <?php 

                 $facebook_share_link = 'http://www.facebook.com/share.php?u='.$property_share_url.'&amp;t='.$property_address.'';

                 $twitter_share_link ='http://twitter.com/home?status='.$property_address.' '.$property_share_url.'';

                $google_share_link ='https://plusone.google.com/_/+1/confirm?hl=en&url='.$property_share_url.'';

                $linkedin_share_link = 'https://www.linkedin.com/shareArticle?mini=true&url='.$property_share_url.'&title='.$property_address.'&summary='.$property_description.'&source=LinkedIn';
                 ?>
                <div class="btn-group">
              
                    		<div class="fbk">
                               <a onclick="return popitup('<?php echo $facebook_share_link;?>')" href="<?php echo $facebook_share_link;?>"  class="btn btn-main" ><i class="fa fa-facebook"></i> Facebook</a>  
                         	</div>   
                 	
                </div>
                 <div class="btn-group">
              
                    		<div class="twt">
                               <a onclick="return popitup('<?php echo $twitter_share_link;?>')" href="<?php echo $twitter_share_link;?>" class="btn btn-main"><i class="fa fa-twitter"></i> Twitter</a>  
                         	</div>   
                 	
                </div>
                <div class="btn-group">
              
                    		<div class="gp">
                               <a onclick="return popitup('<?php echo $google_share_link;?>')" href="<?php echo $google_share_link;?>" class="btn btn-main"><i class="fa fa-google-plus"></i> Google</a>  
                         	</div>   
                 	
                </div>
                <div class="btn-group">
              
                    		<div class="lein">
                               <a onclick="return popitup('<?php echo $linkedin_share_link;?>')" href="<?php echo $linkedin_share_link;?>" class="btn btn-main"><i class="fa fa-linkedin"></i> Linkedin</a>  
                         	</div>   
                 	
                </div>

                <div class="btn-group">              
                    		<div class="email">
                               <a  href="mailto:?&subject=<?php echo $property_address;?>&body=<?php echo $property_description;?>" class="btn btn-main"><i class="fa fa-envelope"></i> Email</a>  
                         	</div>   
                 	
                </div>
                </div>
                <div class="next-inv " >
   						<div class="btn-group">
                 <a class="btn btn-primary btn-lg btn-block text-uppercase " type="button" href="<?php echo site_url('investment/end_campaign/'.$campaign_id.'/'.$investor_process_id);?>">Continue</a>
                 </div>
                        </div>
  				</div>
			</div>
  			</div></div>
          <?php echo $this->load->view('default/investment_step/investment_sidebar'); ?>
    </div>
    </section>
<script type="text/javascript">

function popitup(url) {
  
    newwindow = window.open(url, 'name', 'height=400,width=450');
    if (window.focus) {
        newwindow.focus()
    }
    return false;
}
</script>