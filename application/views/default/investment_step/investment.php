
    <section>
	  	<div class="investment-head">
	      	<div class="container" >
           
	      		<div class="inv-counter col-md-11 col-md-offset-1">
                <div class="s-unit col-xs-2 col-sm-2">
                <div class="num active">1</div>
                <div class="text active">Select Unit</div>
                </div>
                <div class="s-unit col-xs-2 col-sm-2">
                <div class="num">2</div>
                <div class="text ">Pay Deposit</div>
                </div>
                <div class="s-unit col-xs-2 col-sm-2">
                <div class="num">3</div>
                <div class="text ">Investment Confirmation</div>
                </div>
                <div class="s-unit col-xs-2 col-sm-2">
                <div class="num">4</div>
                <div class="text ">Invite Investors</div>
                </div>
                <div class="s-unit col-xs-2 col-sm-2">
                <div class="num">5</div>
                <div class="text ">End of Campaign</div>
                </div>
                
                </div>
                                  
	      	</div>
	  	</div>
	  <?php 

    
       

        $campaign_units = $Campaign_data['campaign_units'];
        $investment_amount = $Campaign_data['min_investment_amount'];
        $AmountPerUnit = $investment_amount/ $campaign_units;
        $admin_fees = $AmountPerUnit*2.2 / 100;
        $total_investment = $AmountPerUnit +  $admin_fees ;
        $deposit_amount = $total_investment*1/100;


    ?>		  		
	      	
    </section>
    <section class="investment-content">
    <div class="container rc-form">
  			
            <div class="investment-left col-md-8">
  				<div class="panel panel-default ec-panel investment-calculator">
           
 					 <div class="panel-heading">
   						 <h3 class="panel-title">Calculate Investment</h3>
 					 </div>
            <?php
                  if ($this->session->flashdata('error_message_investment') != '') {

                ?>
                <div class="alert-danger alert alert-message">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <?php echo $this->session->flashdata('error_message_investment'); ?>
                </div>
            <?php
            } 
            ?>
           <form method="post" action="<?php echo site_url('investment/investor/'.$campaign_id);?>">
 				 <div class="panel-body investment-calculator  ec-form ">
   						<div class="inv-calculator">
                               <ul class="row" >
                                <li class="col-md-3">
                                <div class="calc-title">Units</div>
                                <div class="select-search investment-units">
                                <select class="selectpicker" id="change_units" name="units">
                                <?php 
                               
                                for($i=1;$i<=$campaign_units;$i++){?>
                                <option value="<?php echo $i;?>"> <?php echo $i;?> </option>
                                <?php } ?>
                                </select>
                                </div>
                                <div class="sign">
                               <i class="fa fa-close"></i>
                                </div>
                                </li>
                                <li class="col-md-3">
                                <div class="calc-title">Units Price</div>
                                <div class="calc-amount" id="amount-per-unit"><?php echo set_currency($AmountPerUnit);?></div>
                                 <div class="sign">
                               <i class="fa fa-plus"></i>
                                </div>
                                </li>
                                <li class="col-md-3">
                                <div class="calc-title">Admin Fee</div>
                                    <div class="calc-amount" >2.2% inc GST   </div>
                                  <span id="admin-amount">(<?php echo set_currency($admin_fees);?>)</span>

                                  <input type="hidden" name="admin_fees" id="admin_fees" value="<?php echo round($admin_fees);?>">
                                   <div class="sign equalto">
                              <span class="glyphicon glyphicon-menu-hamburger"></span>
                                </div>
                                </li>
                                <li class="col-md-3">
                               <div class="calc-title">Total Investment</div>
                                   <div class="calc-amount" id="total-amount"><?php echo set_currency($total_investment); ?></div>
                                  
                                </li>
                               </ul>
                        </div>
                        <div class="deposit-calc-bottom m-t-xl">
                       <div class="deposit">1% Deposit Required</div>
                      <div class="refund"> Fully Refundable*</div>
                      <div class="deposit-value" id="deposite-amount"> <?php echo set_currency($deposit_amount);?>   </div>
                      <div class="deposit-term  m-t-xl">
                      <div class="checkbox">
                          <label class="ec-checkbox">
                          
                            <input type="checkbox" name="term_investment" class="ut-checkbox"> <i></i> Accepts Estate Crowd<a href="<?php echo site_url('content/pages/Terms-of-service')?>"> terms and conditions </a> of investment
                            
                          </label>
                        </div>
                                            </div>
                     <div class="secure-investment m-t-md">
                      <button class="btn btn-primary btn-lg btn-block text-uppercase" type="submit">Secure Investment</button>
                     </div>

                     <input type="hidden" name="deposit_amount" id="deposit_amount" value="<?php echo round($deposit_amount);?>">
                   
                      
                        </div>
  				</div>
          </form>
			</div>
  			</div>

            
             <?php echo $this->load->view('default/investment_step/investment_sidebar'); ?>
    </div>

    </section>
    
    
    <script type="text/javascript">
    	$('.panel-heading a').click(function() {
    		if($(this).parents( ".panel-heading" ).hasClass( 'active' )==true) {
			    $(this).parents('.panel-heading').removeClass('active');
		    } else {
		    	$(this).parents('.panel-heading').removeClass('active');
			    $(this).parents('.panel-heading').addClass('active');		    	
		    }
		});
    </script>
    <script type="text/javascript">
		$(function () {
 
    var amount_currency = '$';
   
    // change chart on dropdown item selected.

    $("#change_units").change(function() {

     

      var units = $(this).val();
       
      var investment_amount = '<?php echo $investment_amount;?>';
      var total_units = '<?php echo $campaign_units;?>';
      var amount_per_unit = investment_amount/total_units;
      var amount_calculate_units = amount_per_unit*units;
      var admin_fees = amount_calculate_units* parseFloat(2.2) / 100;
      var total_investment = amount_calculate_units + admin_fees;
      var deposit_amount = total_investment * 1 /100;
      

      $("#amount-per-unit").html(amount_currency+amount_calculate_units.toFixed());
      $("#admin-amount").html('('+amount_currency+admin_fees.toFixed()+')');
      $("#admin_fees").val(admin_fees.toFixed());
      $("#total-amount").html(amount_currency+total_investment.toFixed());
      $("#deposite-amount").html(amount_currency+deposit_amount.toFixed());
       $("#deposit_amount").val(deposit_amount.toFixed());


    });
     });
		</script>
