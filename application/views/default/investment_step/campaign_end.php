 <section>
	  	<div class="investment-head">
	      	<div class="container" >
           
	      		<div class="inv-counter col-md-11 col-md-offset-1">
                <div class="s-unit col-xs-2 col-sm-2">
                <div class="num active"><i class="fa fa-check"></i></div>
                <div class="text active">Select Unit</div>
                </div>
                <div class="s-unit col-xs-2 col-sm-2">
                <div class="num active"><i class="fa fa-check"></i></div>
                <div class="text active">Pay Deposit</div>
                </div>
                <div class="s-unit col-xs-2 col-sm-2">
                <div class="num active"><i class="fa fa-check"></i></div>
                <div class="text active">Investment Confirmation</div>
                </div>
                <div class="s-unit col-xs-2 col-sm-2">
                <div class="num active"><i class="fa fa-check"></i></div>
                <div class="text active">Invite Investors</div>
                </div>
                <div class="s-unit col-xs-2 col-sm-2">
                <div class="num active">5</div>
                <div class="text active">End of Campaign</div>
                </div>
                
                </div>
                                  
	      	</div>
	  	</div>
	  		  		
	      	
    </section>
    <section class="investment-content">
    <div class="container">
  			 
            <div class="investment-left col-md-8">
              <form class="ec-form" method="post" action="<?php echo site_url('investment/end_campaign/'.$campaign_id.'/'.$investor_process_id);?>">
  				<div class="panel panel-default ec-panel">
 					 <div class="panel-heading">
   						 <h3 class="panel-title">Congratulations!</h3>
 					 </div>
 				 <div class="panel-body">
                <div class="end-camp m-t-md">
             <div class="end-camp-head">You have secured your investment in: <span><?php echo $property_address;?></span></div>
             
             <div class="end-camp-body m-t-lg ">
             <span>You will receive the following documents via email:</span>
             <ul class="end-camp-doc m-t-lg">
            <li> <i class="fa fa-check-circle "> </i> 1 x Acknowledgement of Estate Investment Terms & Conditions </li>
                        <li> <i class="fa fa-check-circle "> </i> 1 x Investment Confirmation  </li>
                                    <li> <i class="fa fa-check-circle "> </i> 1 x Deposit Receipt  </li>
                                                <li> <i class="fa fa-check-circle "> </i>1 x Copy of the Trust Agreement & Deed</li>
                                                 <li> <i class="fa fa-check-circle "> </i> 1 x Investor Guide </li>
             
             </ul>
             <strong class="m-t-xl">What Next?</strong>
             <span>We will send you updates on the investment campaign status.</span>
             <div class="clearfix"></div>
              <div class="btn-group  m-t-lg">
              <input type="hidden" name="invest_status_id" value="4">
                <button type="submit" class="btn btn-primary text-uppercase ">Done</button>
                 </div>
              </div>                        
            
                               
  				</div>
			</div>
            </form>
            
  			</div></div>
            
            <?php echo $this->load->view('default/investment_step/investment_sidebar'); ?>
    </div>
    </section>