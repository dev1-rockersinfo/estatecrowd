<?php $site_name = $site_setting['site_name']; ?>

<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/additional-methods.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.maskedinput.js"></script>
<script>

    // validations

    jQuery(function ($) {



        // $("#message_form").mask("hhh?hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh",{placeholder:""});

        // validate signup form on keyup and submit


        var validator = $("#message_form").validate({
            rules: {

                subject: {
                    required: true

                },

                comments: {
                    required: true
                }

            },

            messages: {


                subject: {
                    required: '<?php echo SUBJECT_IS_REQUIRED;?>',

                },


                comments: {

                    required: '<?php echo MESSAGE_IS_REQUIRED;?>',
                }


            },

            // the errorPlacement has to take the table layout into account

            errorPlacement: function (error, element) {

                if (element.is(":radio"))

                    error.insertAfter(element.parent());

                else if (element.is(":checkbox"))

                    error.insertAfter(element.parent().next());

                else {


                    error.insertAfter(element);

                }

            },

            // specifying a submitHandler prevents the default submit, good for the demo

            /*submitHandler: function() {

             alert("submitted!");

             },*/

            // set this class to error-labels to indicate valid fields

            success: function (label) {

                // set &nbsp; as text for IE

                label.html("&nbsp;").addClass("valid");


            },

            /*highlight: function(element, errorClass) {

             $(element).parent().next().find("." + errorClass).removeClass("checked");

             },*/

            onkeyup: function (element) {
                $(element).valid();

                var test_class = $(element).next().html();

                if (test_class != '')

                    $(element).next().addClass('error_label');

                else

                    $(element).next().removeClass('error_label');
            },

            onfocusout: function (element) {
                $(element).valid();

                var test_class = $(element).next().html();

                if (test_class != '')

                    $(element).next().addClass('error_label');

                else

                    $(element).next().removeClass('error_label');
            },


        });


    });


</script>


<div class="white-popup-block popup-max500">
    <?php $user_detail = UserData($user_id);
    $user_name = $user_detail[0]['user_name'];
    $last_name = $user_detail[0]['last_name'];
    ?>
    <div class="popup-header">
        <h3><?php echo SEND_A_MESSAGE_TO; ?> <?php echo $user_name . '&nbsp;' . $last_name; ?></h3>
    </div>

    <div class="in-form">
        <?php if ($error_message) { ?>
            <div class="alert alert-danger alert-message">

                <?php echo $error_message; ?>
            </div>
        <?php } ?>
        <?php
        $att = array('id' => 'message_form', 'name' => 'message_form');
        echo form_open('message/send_mail_project_profile/' . $id . '/' . $user_id, $att);
        ?>
        <div class="form-group">
            <input type="text" name="subject" id="subject" value="" class="form-control"
                   placeholder="<?php echo SUBJECT; ?>">
        </div>
        <div class="form-group">
            <textarea name="comments" id="comments1" value="" class="form-control" rows="6"
                      placeholder="<?php echo MESSAGE; ?>"></textarea>
            <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id; ?>"/>
        </div>
        <div>
            <input type="submit" value="<?php echo SEND_MESSAGE; ?>" class="btn-main" name="submit" id="submit">
            <a href="javascript://" class="btn-grey mfp-cancel marL10"><?php echo CANCEL ?></a>
        </div>
        </form>
    </div>
</div>
