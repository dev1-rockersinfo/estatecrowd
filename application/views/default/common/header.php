<?php 

$sitename = $site_setting['site_name'];
$session = check_user_authentication($redirect = false);
$site_logo = $site_setting['site_logo'];
$site_logo_hover = $site_setting['site_logo_hover'];

$user_id = $this->session->userdata('user_id');
$user_name = $this->session->userdata('user_name');
$last_name = $this->session->userdata('last_name');

$langs = get_supported_lang();
$this->active = $this->uri->uri_string();
$this->strdd = explode("/", $this->active);
//print_r($this->strdd) ;
if (!isset($this->strdd[1])) {
    $this->strdd[1] = '';
}
if (!isset($this->strdd[2])) {
    $this->strdd[2] = '';
}

$projectControllerName=projectcontrollername();
?>

<?php
    if ($this->session->flashdata('success_message') != '') {
        ?>
        <div class="alert-success alert alert-message alert-header">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('success_message'); ?>
        </div>
    <?php
    }   if ($this->session->flashdata('error_message') != '') {

        ?>
        <div class="alert-danger alert alert-message alert-header">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('error_message'); ?>
        </div>
    <?php
    } 
    ?>
<nav id="affix-nav" class="navbar navbar-default"  data-spy="affix" data-offset-top="62">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="push" data-target="#navbar" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand ec-navbar-brand" href="<?php echo base_url();?>">
      <img src="<?php echo base_url(); ?>upload/orig/<?php echo $site_logo; ?>"/></a>
    </div>

    <div class="collapse navbar-collapse" id="navbar">
 <?php
      if ($session) 
      {
       ?>

              <ul class="nav navbar-nav navbar-right db-navbar-right">
          <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="">

            <?php
                                        $get_user_info = UserData($this->session->userdata('user_id'), $join = array('user_notification'));
                                        $profile_slug = $get_user_info[0]['profile_slug'];
                                        if ($get_user_info[0]['image'] != '') {

                                            if (is_file('upload/user/user_small_image/' . $get_user_info[0]['image'])) {
                                                ?>
                                                <img src="<?php echo base_url(); ?>upload/user/user_small_image/<?php echo $get_user_info[0]['image']; ?>"/>

                                            <?php } else { ?>

                                                <img  src="<?php echo base_url(); ?>upload/user/user_small_image/no_man.jpg"/>

                                            <?php }
                                        } else { ?>

                                            <img src="<?php echo base_url(); ?>upload/user/user_small_image/no_man.jpg"/>

                                        <?php } ?>

         <span> <?php echo $user_name; ?></span><b class="caret"></b></a>

        <ul class="dropdown-menu">
            <li><a href="<?php echo site_url('home/dashboard'); ?>"><i class="fa fa-th-large"></i> <?php echo DASHBOARD; ?></a></li>
            <li><a href="<?php echo site_url('account'); ?>"><i class="fa fa-pencil-square-o"></i>  <?php echo EDIT_PROFILE; ?></a></li>
            <li><a href="<?php echo site_url('inbox'); ?>"><i class="fa fa-inbox"></i> Messages</a></li>
            <li><a href="<?php echo site_url('home/logout'); ?>"><i class="fa fa-sign-out"></i> Logout</a></li>      
                      
          </ul>

           </li>
      
      </ul>

     

      <?php } else { 

           $currenct_url='';
                if(isset($_SERVER['REDIRECT_QUERY_STRING']))
                {
                     $currenct_url = $_SERVER['REDIRECT_QUERY_STRING'];
           
                }
          if(isset($this->strdd[1]))
           {
             if($this->strdd[1] == "index")
             {
               if(isset($this->strdd[2]))
               {
                 if($this->strdd[2] == "success")
                 {
                   $currenct_url = ''; 
                   
                 }
                 
               }
               
             }
             
           }
                if($this->strdd[0] == "home" && $this->strdd[0] == "login")
                {
                    $currenct_url = '';
                }
                else
                {
                    $currenct_url = base64_encode($currenct_url);
                }

        ?>
          <ul class="nav navbar-nav navbar-right ec-navbar-right">
            <li><a href="<?php echo site_url("home/login/".$currenct_url); ?>">Log in</a></li>
            <li><a href="<?php echo site_url("home/signup"); ?>">Join Now</a></li>
          </ul>

      <?php } ?>
      <ul class="nav navbar-nav ec-navbar">
        <li><a href="<?php echo site_url('content/about-us');?>" class="hidden-md">About Us</a></li>
        <li><a href="<?php echo site_url('content/how-it-works');?>">How It Works</a></li>
        <li><a href="<?php echo site_url('faq'); ?>" class="hidden-md">FAQ's</a></li>
        <li><a href="<?php echo site_url('search/campaign'); ?>">Explore Campaigns</a></li>
        <li><a href="<?php echo site_url('search/advance_search'); ?>">Property Search</a></li>
      </ul>
    </div>
  </div>
</nav>

<div id="loader-wrapper">

    <div class="loader">
       <div class="loading-bar"></div>
      <div class="loading-bar"></div>
      <div class="loading-bar"></div>  
        
    </div>

</div>
<div id="loader-wrapper-ajax">

    <div class="loader">
      <div class="loading-bar"></div>
      <div class="loading-bar"></div>
      <div class="loading-bar"></div>

    </div>

</div>
<script type="text/javascript">
  if (window.location.hash == '#_=_') {
    window.location.hash = ''; // for older browsers, leaves a # behind
    history.pushState('', document.title, window.location.pathname); // nice and clean
   
}
</script>
