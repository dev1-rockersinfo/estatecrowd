<?php 

$sitename = $site_setting['site_name'];
$session = check_user_authentication($redirect = false);
$site_logo = $site_setting['site_logo'];
$site_logo_hover = $site_setting['site_logo_hover'];

?>

<script type="text/javascript">
    function newsletter_validate() {
        //alert('hiiiii');
        //console.log(document.getElementById('subscribe_email').value);
        if (document.getElementById('subscribe_email').value == "" || document.getElementById('subscribe_email').value == "<?php echo ENTER_YOUR_EMAIL; ?>" || document.getElementById('subscribe_email').value == "<?php echo ENTER_VALID_EMAIL; ?>") {
            document.getElementById('subscribe_email').value = "<?php echo ENTER_VALID_EMAIL; ?>";
            //document.getElementById('newsletter_val').value = "enter valid email";
            return false;
        }
        else {
            var pattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
            var str = document.getElementById('subscribe_email').value;
            //alert(pattern.test(str));
            if (pattern.test(str)) {
                document.frmnewsletter.submit();
            }
            else {
                document.getElementById('subscribe_email').value = "<?php echo ENTER_VALID_EMAIL; ?>";
                //document.getElementById('newsletter_val').value = "enter valid email";
                return false;
            }
        }
    }
</script>

<footer>
  <div class="first-footer">
    <div class="container">
      <div class="row">
        <div class="col-sm-6 footer-left">
            <div class="footer-logo">
              <a href="javascript:void(0)">
                <img src="<?php echo base_url(); ?>upload/orig/<?php echo $site_logo; ?>"/>
              </a>
            </div>
            <div class="footer-text m-t-md">
              Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis.
            </div>
            <div class="footer-social-media">
              <ul class="clearfix">
               <?php
                    $facebook = facebook_setting();
                    $fb_url = '';
                    if ($facebook) {
                        $fb_url = $facebook->facebook_url;
                    }
                    $twitter = twitter_setting();
                    $twitter_url = '';
                    if ($twitter) {
                        $twitter_url = $twitter['twitter_url'];
                    }
                    $youtube = youtube_setting();
                    $youtube_url = '';
                    if ($youtube) {
                        $youtube_url = $youtube['youtube_link'];
                    }
                    $google_plus = google_plus_setting();
                    $google_plus_url = '';
                    if ($google_plus) {
                        $google_plus_url = $google_plus['google_plus_link'];
                    }
                    $linkdin = linkdin_setting();
                    $linkdin_url = '';
                    if ($linkdin) {
                        $linkdin_url = $linkdin['linkdin_url'];
                    }
                    ?>
                     <?php
                        if (isset($facebook->facebook_login_enable)) {
                            if (intval($facebook->facebook_login_enable) == 1) {
                                ?>
                <li><a href="<?php echo $fb_url; ?>" target="_blank"><i class="icon-social-facebook"></i></a></li>
                <?php
                            }
                        }
                        if (isset($twitter['twitter_enable'])) {
                            if (intval($twitter['twitter_enable']) == 1) {
                                ?>
                <li><a href="<?php echo $twitter_url; ?>" target="_blank"><i class="icon-social-twitter"></i></a></li>
                 

                                 <?php
                            }
                        }
                        if (isset($linkdin['linkdin_enable'])) {
                            if (intval($linkdin['linkdin_enable']) == 1) {
                                ?>
                <li><a href="<?php echo $linkdin_url; ?>" target="_blank"><i class="icon-social-linkedin"></i></a></li>
                  <?php
                            }
                        }
                        if (isset($google_plus['google_plus_enable'])) {
                            if (intval($google_plus['google_plus_enable']) == 1) {
                                ?>

                <li><a href="<?php echo $google_plus_url; ?>" target="_blank"><i class="icon-social-gplus"></i></a></li>

                  <?php
                            }
                        }
                        ?>
              </ul>
            </div>
        </div>
        <div class="col-sm-6 newsletter">
          <div class="footer-title">Subscribe to Newsletter</div>
           <?php $attributes = array('name' => 'frmnewsletter', 'onsubmit' => 'return newsletter_validate()', 'id' => 'frmnewsletter', 'autocomplete' => 'off');
                        echo form_open('newsletter/subscribe/', $attributes);?>
          <div class="newsletter-input">
         
            <input type="text" class="animation-out"
                                   onblur="if (this.value == '') {this.value = '<?php echo ENTER_YOUR_EMAIL; ?>';}"
                                   onfocus="if (this.value == '<?php echo ENTER_YOUR_EMAIL; ?>' || this.value=='<?php echo ENTER_VALID_EMAIL; ?>') {this.value = '';}"
                                   value="<?php echo ENTER_YOUR_EMAIL; ?>" name="subscribe_email" id="subscribe_email"/>

          <i class="icon-envelope animation-out"></i></div>
          <div class="footer-text m-t m-b">* We will not share your contact</div>
          <button class="btn btn-primary" type="submit" name="Join">Submit</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="second-footer">      
    <div class="container">
      <ul class="footer-links">
        

         <?php
                        $learn_more_category = learn_more_category($active = 1, $footer = 'yes');
                        $pages = pages_menu($active = 1, $footer = 'yes');
                        
                        if (is_array($pages)) {
                            foreach ($pages as $page) {
                                $pages_title = $page['pages_title'];
                                $slug = $page['slug'];

                                ?>
                                <li>
                                    <a href="<?php echo site_url('content/'.$slug); ?>"><?php echo $pages_title; ?></a>
                                </li>
                            <?php
                            }

                        }
                        ?>

                        <li><a href="<?php echo site_url('learn_more'); ?>"><?=HELP_CENTER?></a></li>
                        <li><a href="<?php echo site_url('faq'); ?>"><?php echo FAQ; ?> </a></li>
                        <li><a href="<?php echo site_url('contact_us'); ?>"><?php echo CONTACT_US; ?></a></li>
      </ul>
    </div>
  </div>
  <div class="third-footer">
    <div class="container">
      <div class="footer-copyright">
        ©2015 <strong>Estate Crowd</strong>  All Rights Reserved.
      </div>
    </div>
  </div>
</footer>

<!-- jQuery first, then Bootstrap JS. -->

