<section>

<div class="content_part_top">

<div class="main">

<div class="dash_whitebg clearfix">

<!--Left part-->

<?php echo $this->load->view('default/dashboard_sidebar'); ?>

<!--Left Part End-->


<!--right Part Start-->



<?php

$attributes = array('name' => 'frm_social');

echo form_open_multipart('social_networking/validate_social', $attributes);

?>

<div class="dash_right">

    <div class="dash_content">

        <?php

        if ($success != '') {

            ?>

            <div class="success_change"><?php echo $success; ?></div>

        <?php

        } else if ($error != '') {

            ?>

            <div class="validation"><?php echo $error; ?></div>

        <?php } ?>



        <div class="dash_title"><h1><?php echo MANAGE_YOUR_SOCIAL_NETWORK_DETAILS; ?>  </h1></div>

        <div class="account_form">

            <div class="step2_FI">

                <div class="w20P_FL fonts12"><?php echo FACEBOOK_URL; ?></div>

                <div class="w61P_FL">

                    <input type="text" name="facebook_url" id="facebook_url" value="<?php echo $facebook_url; ?>"
                           class="inputDefault_S">

                </div>

                <div class="clear"></div>

            </div>


            <div class="step2_FI">

                <div class="w20P_FL fonts12"><?php echo TWITTER_URL; ?></div>

                <div class="w61P_FL">

                    <div><input type="text" name="twitter_url" class="inputDefault_S" id="twitter_url"
                                value="<?php echo $twitter_url; ?>"></div>


                </div>

                <div class="clear"></div>

            </div>


            <div class="step2_FI">

                <div class="w20P_FL fonts12"><?php echo WEBSITE; ?> </div>

                <div class="w61P_FL">

                    <div><input type="text" name="user_website" id="user_website" value="<?php echo $user_website; ?>"
                                class="inputDefault_S"></div>
                </div>

                <div class="clear"></div>

            </div>


            <div class="step2_FI">

                <div class="w20P_FL fonts12"><?php echo LINKEDIN_URL; ?></div>

                <div class="w61P_FL">

                    <div><input type="text" name="linkedln_url" id="linkedln_url" value="<?php echo $linkedln_url; ?>"
                                class="inputDefault_S"></div>
                </div>

                <div class="clear"></div>

            </div>


            <div class="step2_FI">

                <div class="w20P_FL fonts12"><?php echo GOOGLE_PLUS_URL; ?></div>

                <div class="w61P_FL">

                    <div><input type="text" name="googleplus_url" id="googleplus_url"
                                value="<?php echo $googleplus_url; ?>" class="inputDefault_S"></div>
                </div>

                <div class="clear"></div>

            </div>


            <div class="step2_FI">

                <div class="w20P_FL fonts12"><?php echo BASECAMP_URL; ?></div>

                <div class="w61P_FL">

                    <div><input type="text" name="bandcamp_url" id="bandcamp_url" value="<?php echo $bandcamp_url; ?>"
                                class="inputDefault_S"></div>
                </div>

                <div class="clear"></div>

            </div>


            <div class="step2_FI">

                <div class="w20P_FL fonts12"><?php echo YOUTUBE_URL; ?></div>

                <div class="w61P_FL">

                    <div><input type="text" name="youtube_url" id="youtube_url" value="<?php echo $youtube_url; ?>"
                                class="inputDefault_S"></div>
                </div>

                <div class="clear"></div>

            </div>


            <div class="step2_FI">

                <div class="w20P_FL fonts12"><?php echo MYSPACE_URL; ?></div>

                <div class="w61P_FL">

                    <div><input type="text" name="myspace_url" id="myspace_url" value="<?php echo $myspace_url; ?>"
                                class="inputDefault_S"></div>
                </div>

                <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id; ?>"/>

                <div class="clear"></div>

            </div>


            <div class="step2_FI">

                <div class="w61P_FL Mleft20P"><input type="submit" value="<?php echo SAVE_CHANGES; ?>"
                                                     class=" save_Default fl"/>
                    <input type="button" value="<?php echo CANCEL; ?>" class="inputB_Default_S fr"
                           onClick="location.href='<?php echo site_url('social_networking'); ?>'"/>

                    <div class="clear"></div>

                </div>

                <div class="clear"></div>

            </div>

        </div>


    </div>

</div>

<?php

echo form_close();

?>

<!--right Part End-->

<div class="clear"></div>

</div>

</div>

</div>

</div>

</section>
