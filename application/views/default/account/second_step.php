<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>


<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/additional-methods.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.maskedinput.js"></script>

<section>
      <div class="db-head">
          <div class="container">
            <div class="page-title">
              <h2>Fill  Information</h2>
            </div>
          </div>
      </div>
      <div class="f-ls-full-bg-img " id="section-area">       
          <div class="container">
              <div class="row">         
              <div class="col-md-10 col-md-offset-1 ">  

    <?php if ($this->session->flashdata('error_message_account') != '') {

        ?>
        <div class="alert-danger alert alert-message">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('error_message_account'); ?>
        </div>
    <?php
    } 
    ?>
                    <div class="whiteBox first-l-s">
                             <?php
            $attributes = array('name' => 'account', 'id' => 'account', 'enctype' => 'multipart/form-data', 'class' => 'ec-form');
            echo form_open_multipart('account/second_step', $attributes)?>
               
                  <div class="row ">
                   <div class="form-heading">
                  <h3>Property Types</h3>
                   </div>
                  
                 <div class="row">
                 <div class="form-group  f-ls-check-box">
            
            <div class="row">
                    <div class="col-md-12">
                        <div class="checkbox col-md-3">
                          <label class="ec-checkbox">
                            <input type="checkbox" name="user_property_type[]" value="1" class="ut-checkbox"
                            <?php if(PropertyTypeExists(1) == 1) echo 'checked="checked"'; ?>
                            > Residential
                            <i></i>
                          </label>
                        </div>
                        <div class="checkbox col-md-3">
                          <label class="ec-checkbox">
                            <input type="checkbox" id="chk-box" name="user_property_type[]" value="2" class="ut-checkbox" <?php if(PropertyTypeExists(2) == 1) echo 'checked="checked"'; ?>> Commercial 
                            <i></i>
                          </label>
                        </div> 
                        <div class="checkbox col-md-3">
                          <label class="ec-checkbox">
                            <input type="checkbox" id="chk-box1" name="user_property_type[]" value="3" class="ut-checkbox" <?php if(PropertyTypeExists(3) == 1) echo 'checked="checked"'; ?>>Industrial 
                            <i></i>
                          </label>
                        </div> 
                        <div class="checkbox col-md-3">
                          <label class="ec-checkbox">
                            <input type="checkbox" id="chk-box1" name="user_property_type[]" value="4" class="ut-checkbox" <?php if(PropertyTypeExists(4) == 1) echo 'checked="checked"'; ?>> Rural
                            <i></i>
                          </label>
                        </div> 
                        </div>
                        </div>
                        
         </div>
                 </div>
                 <div class="row">
                 
                 <div class="form-group col-md-6 ">
                   <div class="form-group  f-ls-check-box">
             <label for="pro-type">Investment Suburbs<span class="form_star">*</span></label>
     
           <input type="text" class="form-control" name="investment_suburbs" id="investment_suburbs"
                               value="<?php echo $investment_suburbs; ?>" onkeyup="autotext();">

                <div id="autoc" class="in-autocomplete"></div>                
         </div>
                 </div>
               
                 <div class="form-group col-md-6 ">
                 <div class="range-input">
                  <label for="pro-type">Investment Range<span class="form_star">*</span></label>
                   <input id="ex2" type="text" class="span2" value="" data-slider-tooltip="hide" data-slider-min="0" data-slider-max="200" data-slider-step="10"  data-slider-value="[20,50]"/>
<div id="bar">
        <div class="row minmax-wrap">
       <div class="col-md-6"> <span> Min </span>
       <input type="text" id="minval" name="min_investment_range" value="$<?php echo $min_investment_range; ?>K" placeholder="$0K" ></div>   
       <div class="col-md-6"> <span>  Max </span>
       <input type="text" id="maxval" name="max_investment_range" value="$<?php echo $max_investment_range; ?>K" placeholder="$200K" ></div></div>
                  </div>
                  </div>
                  </div>
                 </div>
                 
                  </div>
                 
                  <div class="row">
                  <?php if(UserTypeExists(2) == 1 || UserTypeExists(4) == 1) {?>                  
                   <div class="row just-heading">
                   <div class="form-heading col-md-12">
                  <h3> Company Info</h3>
                   </div>
                   </div>
                   <div class="row">
                 <div class="form-group col-md-6">
             <label for="c-name">Company Name <span class="form_star">*</span></label>
             <input type="text" placeholder=""id="company_name" name="company_name"  value="<?php echo $company_name?>" class="form-control ">
         </div>
                   <div class="form-group col-md-6">
             <label for="website">Company Website  <span class="form_star">*</span></label>
             <input type="text" placeholder="" id="company_website" name="company_website" value="<?php echo $company_website;?>" class="form-control ">

         </div>
                 </div>    
                 <div class="row">
                 <div class="form-group col-md-6">
             <label for="cmp-add">Company Address<span class="form_star">*</span></label>
             <input type="text" placeholder="" id="company_address" name="company_address" value="<?php echo $company_address;?>"  class="form-control ">
              <div id="companylocationMap" class="gmap3 google-map"></div>
         </div>
                   <div class="form-group col-md-6">
             <label for="upload-img">Company Logo <span class="form_star">*</span></label>
            <div class="input-group browse-btn">
             <input type="text" id="image_name" class="form-control" value="<?php echo $company_logo;?>" readonly>
                <span class="input-group-btn ">
                    <span class="btn btn-primary btn-file">
                        Browse <input type="file" name="file1" id="file1" multiple>
                    </span>
                </span>
               
            </div>
            
        
         </div>
                 </div> 
                 <?php } ?>
                 <div class="row">
                 <div class="form-group col-md-12">
       <div class="btn-group pull-right">
              
                <button class="btn btn-primary btn-lg btn-block text-uppercase " type="submit">Save &amp; Finish</button>
                 </div> 
                 </div>
                 </form>
              </div>
              
               
                </div>
                  </div>
          </div>
      </div>
    </section>

<script type="text/javascript" src="<?php echo base_url();?>js/jquery.tokenize.js"></script>
      <script src="<?php echo base_url();?>js/bootstrap-slider1.js"></script>
  <script src="<?php echo base_url();?>assets_front/bootstrap-select/js/bootstrap-select.js"></script>
  
  <script type="text/javascript">
  $('#chk-box,#chk-box1').on('change', function(){ // on change of state
   if(this.checked) // if changed state is "CHECKED"
    {
        $( "#chk-info" ).show();
    }else {
             $( "#chk-info" ).hide();
          }
})
    $('#file1').change(function () {
      $("#image_name").val(this.files[0].name);
  })
  
  </script>
    <script type="text/javascript">
    $('#tokenize').tokenize();
  
  $('#ex2').slider()
  .on('slide', function(ev){
   
  $("#minval").val("$"+ev.value[0]+"K");
       $("#maxval").val("$"+ev.value[1]+"K");
   
   
  });
  $(window).bind("load", function () {
    $('#dvLoading').fadeOut(2000);
    $('#mainbg_div').fadeIn(2000);
    $('.min_height').css('minHeight', 0);
    $(function () {
        $('#companylocationMap').gmap3();

        $('#company_address').autocomplete({
            source: function () {
                $("#companylocationMap").gmap3({
                    action: 'getAddress',
                    address: $(this).val(),
                    callback: function (results) {
                        if (!results) return;
                        $('#company_address').autocomplete(
                            'display',
                            results,
                            false
                        );
                    }
                });
            },
            cb: {
                cast: function (item) {

                    return item.formatted_address;
                },
              
            }


        });



    });

});
 
      $("#autoc").hide();

    function autotext() {
        var xmlHttp;
        try {
            xmlHttp = new XMLHttpRequest();// Firefox, Opera 8.0+, Safari
        }
        catch (e) {
            try {
                xmlHttp = new ActiveXObject("Msxml2.XMLHTTP"); // Internet Explorer
            }
            catch (e) {
                try {
                    xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                catch (e) {
                    //alert("<?php //echo NO_AJAX;?>");
                    return false;
                }
            }
        }
        xmlHttp.onreadystatechange = function () {
            if (xmlHttp.readyState == 4) {
                document.getElementById('autoc').innerHTML = xmlHttp.responseText;

                if (document.getElementById('srhdiv')) {


                  $("#autoc").show();


                }
                else {
                   $("#autoc").hide();
                    
                }
            }
        }
        var text = document.getElementById('investment_suburbs').value;
        text = text.replace(/[^a-zA-Z0-9]/g, '');
        xmlHttp.open("GET", "<?php echo site_url('account/search_auto');?>/" + text, true);
        xmlHttp.send(null);
    }
    function selecttext(el) {

        var selectvalue = el.innerHTML;
       
        $("#investment_suburbs").val(selectvalue);
        $("#autoc").hide();
       
    }

    

</script>
<script type="text/javascript" src="http://code.jquery.com/jquery-migrate-1.0.0.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/map/gmap3.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/map/jquery-autocomplete.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/map/jquery-autocomplete.css"/>
