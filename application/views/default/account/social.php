
<section>
<div class="db-head">
<div class="container">
<div class="page-title">
<h2>Social</h2>
</div>
</div>
</div>
<div class="grey-bg" id="section-area">	  		
<div class="container">
<div class="row">
<div class="col-md-3 db-mc-left">
<?php echo $this->load->view('default/dashboard_sidebar'); ?>
</div>

<div class="col-md-9 db-mc-right">
<div class="row">
<div class="col-md-12">
<?php
    if ($this->session->flashdata('success_message_social') != '') {
        ?>
        <div class="alert-success alert alert-message">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('success_message_social'); ?>
        </div>
    <?php
    }  ?>
 <?php if ($this->session->flashdata('error_message_social') != '') {

        ?>
        <div class="alert-danger alert alert-message">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('error_message_social'); ?>
        </div>
    <?php
    } 
    ?>
   <?php
            $attributes = array('name' => 'account', 'id' => 'account', 'enctype' => 'multipart/form-data', 'class' => 'ec-form');
            echo form_open_multipart('account/social', $attributes)?>
<div class="row">

<div class="form-group col-md-6">
<label for="name">Facebook</label>
<input type="text" placeholder="Enter Facebook URL" id="facebook_url" name="facebook_url" value="<?php echo $facebook_url;?>" class="form-control ">
</div>
<div class="form-group col-md-6">
<label for="name">Linkedin</label>
<input type="text" placeholder="Enter Linkedin URL" id="linkedln_url" name="linkedln_url" value="<?php echo $linkedln_url;?>" class="form-control ">
</div>
</div>
<div class="row">
<div class="form-group col-md-6">
<label for="name">Google Plus</label>
<input type="text" placeholder="Enter Google Plus URL" id="googleplus_url" name="googleplus_url" value="<?php echo $googleplus_url;?>" class="form-control ">
</div>
<div class="form-group col-md-6">
<label for="name">Twitter</label>
<input type="text" placeholder="Enter Twitter URL" id="twitter_url" name="twitter_url" value="<?php echo $twitter_url;?>" class="form-control ">
</div>
</div>
<div class="row">
<div class="form-group col-md-6">
<label for="name">Website</label>
<input type="text" placeholder="Enter Website URL" id="user_website" name="user_website" value="<?php echo $user_website;?>" class="form-control ">
</div>
</div>
<div class="form-group">  
<button class="btn btn-primary text-uppercase" type="submit">Save Changes</button>
</div>
</form>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
