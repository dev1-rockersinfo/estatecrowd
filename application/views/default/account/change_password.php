<script src="<?php echo base_url() ?>js/jquery.form.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/additional-methods.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.maskedinput.js"></script>
<script type="text/javascript">
jQuery(function ($) {
	  var validator = $("#changepassword").validate({
        rules: {
            old_password: {
                required: true,
                minlength: 8,
                maxlength: 20
            },

            password: {
                required: true,
                minlength: 8,
                maxlength: 20
            },
            confirm_password: {
                required: true,
                minlength: 8,
                maxlength: 20,
                equalTo: "#password"
            }


        },
        messages: {
            old_password: {
                required: '<?php echo OLD_PASSWORD_IS_REQUIRED;?>',
                minlength: '<?php echo ENTER_ATLEAST_EIGHT_CHARACTER;?>',
                maxlength: '<?php echo YOU_CAN_NOT_ENTER_MORE_THAN_TWE_CHARACTER;?>'

            },

            password: {
                required: '<?php echo PASSWORD_IS_REQUIRED;?>',
                minlength: '<?php echo ENTER_ATLEAST_EIGHT_CHARACTER;?>',
                maxlength: '<?php echo YOU_CAN_NOT_ENTER_MORE_THAN_TWE_CHARACTER;?>'

            },

            confirm_password: {
                required: '<?php echo CONFIRM_PASSWORD_IS_REQUIRED;?>',
                minlength: '<?php echo ENTER_ATLEAST_EIGHT_CHARACTER;?>',
                maxlength: '<?php echo YOU_CAN_NOT_ENTER_MORE_THAN_TWE_CHARACTER;?>',
                equalTo: '<?php echo PASSWORD_DOES_NOT_MATCH; ?>'
            }

        },
        // the errorPlacement has to take the table layout into account
        errorPlacement: function (error, element) {
            if (element.is(":radio"))
                error.insertAfter(element.parent());
            else if (element.is(":checkbox"))
                error.insertAfter(element.next());
            else if (element.hasClass('book_job_now_price_input')) {
                error.insertAfter(element.next().next());

            }
            else {
                if (element.attr('name') == 'job_price') {
                    error.insertAfter(element.next());
                }
                else
                    error.insertAfter(element);
            }
        },

        // set this class to error-labels to indicate valid fields
        success: function (label) {
            // set &nbsp; as text for IE
            label.html("&nbsp;").addClass("valid");
        },

        onkeyup: function (element) {
            $(element).valid();
        },
        onfocusout: function (element) {
            $(element).valid();
        }

    });
  });

</script>
<section>
<div class="db-head">
<div class="container">
<div class="page-title">
<h2>Change Password</h2>
</div>
</div>
</div>

<div class="grey-bg" id="section-area">	  		
<div class="container">
<div class="row">
<div class="col-md-3 db-mc-left">
  <?php echo $this->load->view('default/dashboard_sidebar'); ?>
</div>
<div class="col-md-9 db-mc-right">
<?php
    if ($this->session->flashdata('success_message_change_password') != '') {
        ?>
        <div class="alert-success alert alert-message">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('success_message_change_password'); ?>
        </div>
    <?php
    }  ?>
 <?php if ($this->session->flashdata('error_message_change_password') != '') {

        ?>
        <div class="alert-danger alert alert-message">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('error_message_change_password'); ?>
        </div>
    <?php
    } 
    ?>
<div class="row">
<div class="col-md-3">
</div>
</div>
<div class="col-md-9">

<?php $attributes = array('name' => 'changepassword', 'id' => 'changepassword', 'class' => 'form-horizontal  dec-form');
            echo form_open_multipart('account/changepassword', $attributes)?>
<div class="form-group">
<label class="control-label col-sm-4"  for="old_password">Old Password<span class="form_star">*</span></label>
<div class="col-sm-8">
<input type="password" class="form-control" name="old_password" id="old_password" value="">
</div>
</div>
<div class="form-group">
<label class="control-label col-sm-4" for="new_password">New Password<span class="form_star">*</span></label>
<div class="col-sm-8">
<input type="password" class="form-control" name="password" id="password" value="" >
</div>
</div>
<div class="form-group">
<label class="control-label col-sm-4" for="confirm_password">Confirm Password<span class="form_star">*</span></label>
<div class="col-sm-8">
<input type="password" class="form-control" name="confirm_password" id="confirm_password" value="" >
</div>
</div>



<div class="form-group">        
<div class="col-sm-offset-4 col-sm-8">
<button type="submit" class="btn btn-primary text-uppercase">Save Changes</button>
</div>
</div>
</form>
</div>
</div>

</div>

</div>
</div>
</div>
</section>