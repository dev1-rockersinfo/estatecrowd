
<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>


<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/additional-methods.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.maskedinput.js"></script>
<script>
// validations
jQuery(function ($) {

    // validate signup form on keyup and submit
    var validator = $("#account").validate({

        rules: {
            user_name: {
                required: true,
                maxlength: 40,
                minlength: 3,
                maxWords: 1,
                minWords: 1
            },
            last_name: {
                required: true,
                maxlength: 40,
               
                maxWords: 1,
                minWords: 1
            },
           password: {
                  required: true,
                  minlength: 8,
                  maxlength: 20
              },
            email: {
                required: true,
                email: true
            },
            address: {
                required: true,
                /*address: true*/
            },
            zip_code: {
                required: true,
               
            },

            mobile_number:{
                 required: true,
                 number:true,
                  maxlength: 15,
                  minlength: 10,
            },
           user_type: {
            required : true;
           }
         }



        },
        messages: {
            user_name: {
                required: '<?php echo ENTER_FIRST_NAME;?>',
                maxlength: '<?php echo FIRST_NAME_CANNOT_BE_MORE_THAN_THIRTY_CHAR;?>',
                minlength: '<?php echo FIRST_NAME_CANNOT_BE_LESS_THAN_THREE_CHAR;?>',
                maxWords: '<?php echo ENTER_FIRST_NAME_ONLY;?>',
                minWords: '<?php echo ENTER_FIRST_NAME;?>'
            },

            last_name: {
                required: '<?php echo ENTER_LAST_NAME;?>',
                maxlength: '<?php echo LAST_NAME_CANNOT_BE_MORE_THAN_THIRTY_CHAR;?>',
               
                maxWords: '<?php echo ENTER_LAST_NAME_ONLY;?>',
                minWords: '<?php echo ENTER_LAST_NAME;?>'
            },

           password: {
                required: '<?php echo PASSWORD_IS_REQUIRED;?>',
                minlength: '<?php echo ENTER_ATLEAST_EIGHT_CHARACTER;?>',
                maxlength: '<?php echo YOU_CAN_NOT_ENTER_MORE_THAN_TWE_CHARACTER;?>'

            },
            email: {
                required: '<?php echo EMAIL_IS_REQUIRED;?>',
                email: '<?php echo EMAIL_VALID_EMAIL;?>'
                /*remote: "emails.action"*/
            },

            address: {
                required: '<?php echo THIS_FIELD_CANNOT_BE_LEFT_BLANK;?>'
            },
            zip_code: {
                required: '<?php echo ZIPCODE_IS_REQUIRED;?>',
                minlength: '<?php echo ENTER_ZIP_CODE_NOT_LESS_THAN_FIVE_CHAR;?>',
                maxlength: '<?php echo ENTER_ZIP_CODE_NOT_MORE_THAN_SEVEN_CHAR;?>'
            },
          
            mobile_number:{
                 required: 'Mobile Number is Required',
                 phoneUS :'Enter valid Phone Number',
            },
            user_type : {
              required: true,
              minlength:1,
            }



        },
        // the errorPlacement has to take the table layout into account
        errorPlacement: function (error, element) {
            if (element.is(":radio"))
                error.insertAfter(element.parent());
            else if (element.is(":checkbox"))
                error.insertAfter(element.next());
            else if (element.hasClass('book_job_now_price_input')) {
                error.insertAfter(element.next().next());

            }
            else {
                if (element.attr('name') == 'job_price') {
                    error.insertAfter(element.next());
                }
                else
                    error.insertAfter(element);
            }
        },


        // set this class to error-labels to indicate valid fields
        success: function (label) {
            // set &nbsp; as text for IE
            label.html("&nbsp;").addClass("valid");

        },

        onkeyup: function (element) {
            $(element).valid();
        },
        onfocusout: function (element) {
            $(element).valid();
        },

    });

     

});
$.validator.addMethod('cb_selectone', function(value,element){
    if(element.length>0){
        for(var i=0;i<element.length;i++){
            if($(element[i]).val('checked')) return true;
        }
        return false;
    }
    return false;
}, 'Please select at least one option');

$(window).bind("load", function () {
    $('#dvLoading').fadeOut(2000);
    $('#mainbg_div').fadeIn(2000);
    $('.min_height').css('minHeight', 0);
    $(function () {
        $('#locationMap').gmap3();

        $('#address').autocomplete({
            source: function () {
                $("#locationMap").gmap3({
                    action: 'getAddress',
                    address: $(this).val(),
                    callback: function (results) {
                        if (!results) return;
                        $('#address').autocomplete(
                            'display',
                            results,
                            false
                        );
                    }
                });
            },
            cb: {
                cast: function (item) {

                    return item.formatted_address;
                },
                select: function (item) {
                    var length_arr = item.address_components.length;

                    if (length_arr > 0) {
                        for (i = 0; i < length_arr; i++) {
                            if (item.address_components[i].types == 'postal_code') {
                                var zipcode_value = item.address_components[i].long_name;

                                $('input[name=zip_code]').val(zipcode_value);
                                if (zipcode_value != '') {
                                    $('input[name=zip_code]').removeClass("error");

                                }

                            }
                        }
                    }
               
                }
            }


        });



    });

});

 $("#autoc").hide();

    function autotext() {
        var xmlHttp;
        try {
            xmlHttp = new XMLHttpRequest();// Firefox, Opera 8.0+, Safari
        }
        catch (e) {
            try {
                xmlHttp = new ActiveXObject("Msxml2.XMLHTTP"); // Internet Explorer
            }
            catch (e) {
                try {
                    xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                catch (e) {
                    //alert("<?php //echo NO_AJAX;?>");
                    return false;
                }
            }
        }
        xmlHttp.onreadystatechange = function () {
            if (xmlHttp.readyState == 4) {
                document.getElementById('autoc').innerHTML = xmlHttp.responseText;

                if (document.getElementById('srhdiv')) {


                  $("#autoc").show();


                }
                else {
                   $("#autoc").hide();
                    
                }
            }
        }
        var text = document.getElementById('zip_code').value;
        text = text.replace(/[^a-zA-Z0-9]/g, '');
        xmlHttp.open("GET", "<?php echo site_url('account/search_auto_postcode');?>/" + text, true);
        xmlHttp.send(null);
    }
    function selecttext(el) {

        var selectvalue = el.innerHTML;
       
        $("#zip_code").val(selectvalue);
        $("#autoc").hide();
       
    }

</script>
<section>
        <div class="db-head">
            <div class="container">
                <div class="page-title">
                    <h2>Fill  Information</h2>
                </div>
            </div>
        </div>
        <div class="f-ls-full-bg-img " id="section-area">           
            <div class="container">
                <div class="row">           
                    <div class="col-md-10 col-md-offset-1 ">  

    <?php if ($this->session->flashdata('error_message_account') != '') {

        ?>
        <div class="alert-danger alert alert-message">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('error_message_account'); ?>
        </div>
    <?php
    } 
    ?>
                    <div class="whiteBox first-l-s">
                   <?php
            $attributes = array('name' => 'account', 'id' => 'account', 'enctype' => 'multipart/form-data', 'class' => 'ec-form');
            echo form_open_multipart('account/first_step', $attributes)?>
                   <div class="row">
                        <div class="form-group col-md-6">
                     <label for="f-name">First Name <span class="form_star">*</span></label>
                     <input type="text" class="form-control" name="user_name" id="user_name"
                               value="<?php echo $user_name; ?>">
                 </div>
                        <div class="form-group col-md-6">
                     <label for="l-name">Last Name <span class="form_star">*</span></label>
                      <input type="text" class="form-control" name="last_name" id="last_name"
                               value="<?php echo $last_name; ?>" >
                 </div>
                     </div>   
                  <div class="row">
                 <div class="form-group col-md-6 f-ls-check-box">
                     <label for="pro-type">User Type <span class="form_star">*</span></label>
                  
                    <div class="user-type-dash">
                        <div class="checkbox">
                          <label class="ec-checkbox">
                            <input type="checkbox" class="ut-checkbox" name="user_type[]" value="1" 
                            <?php if(UserTypeExists(1) == 1) echo 'checked="checked"'; ?>> Investor
                            <i></i>
                          </label>
                        </div>
                        <div class="checkbox">
                          <label class="ec-checkbox">
                            <input type="checkbox" class="ut-checkbox" id="chk-box" name="user_type[]" value="2"
                            <?php if(UserTypeExists(2) == 1) echo 'checked="checked"'; ?>> R/E Agent
                            <i></i>
                          </label>
                        </div> 
                        <div class="checkbox">
                          <label class="ec-checkbox">
                            <input type="checkbox" class="ut-checkbox" id="chk-box1" name="user_type[]" value="3"
                            <?php if(UserTypeExists(3) == 1) echo 'checked="checked"'; ?>> Property Owner
                            <i></i>
                          </label>
                        </div>  

                         <div class="checkbox">
                          <label class="ec-checkbox">
                            <input type="checkbox" class="ut-checkbox" id="chk-box2" name="user_type[]" value="4"
                            <?php if(UserTypeExists(4) == 1) echo 'checked="checked"'; ?>> Property Developer
                            <i></i>
                          </label>
                        </div>  
                                             
                    </div>
                      
                        
                 </div>
                   <div class="form-group col-md-6">
                     <label for="phone">Mobile Number <span class="form_star">*</span></label>
                      <input type="text" class="form-control" id="mobile_number" name="mobile_number" 
                     value="<?php  if ($mobile_number>0) echo $mobile_number;?>" >
                 </div>
                 
                 </div>
               
                  <div class="row">

                 <div class="form-group col-md-6">
                     <label for="address">Address <span class="form_star">*</span></label>
                        <input type="text" class="form-control" name="address" id="address"
                               value="<?php echo $address; ?>" autocomplete="on">
                                    

                      <div id="locationMap" class="gmap3"></div>
                   
                     </div> 
                      <div class="form-group col-md-6">
                     <label for="p-code">Postcode <span class="form_star">*</span></label>
                      <input type="text" class="form-control" name="zip_code" id="zip_code"
                               value="<?php echo $zip_code; ?>" onkeyup="autotext();" >
                                <div id="autoc" class="in-autocomplete"></div>   
                 </div>               
                 </div>
                   <div class="row">
                 <div class="form-group col-md-6">
                     <label for="email">Email <span class="form_star">*</span></label>
                    <input type="email" class="form-control" name="email" id="email" value="<?php echo $email; ?>" <?php if($email != '') {?>disabled="disabled" <?php } ?> >
                 </div>
                   
               
                 <?php if($password==''){ ?>
                   <div class="form-group col-md-6">
                     <label for="phone"> Password <span class="form_star">*</span></label>
                      <input type="password" class="form-control" id="password" name="password" 
                     value="<?php echo $password;?>" >
                   </div>
                 <?php } ?>
                 </div>
                 
                 
                 
               
                 <div class="row">
                 <div class="form-group col-md-12">
             <div class="btn-group pull-right">
               <button class="btn btn-primary btn-lg btn-block text-uppercase " type="submit">Save &amp; Continue</button>
                 </div> 
                 </div>
                         </form>
                    </div>
                    
               
                </div>
                    </div>
            </div>
        </div>
    </section>
    <script type="text/javascript" src="http://code.jquery.com/jquery-migrate-1.0.0.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/map/gmap3.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/map/jquery-autocomplete.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/map/jquery-autocomplete.css"/>