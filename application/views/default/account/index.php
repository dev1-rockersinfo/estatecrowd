<?php

$userid = get_authenticateUserID();
?>
<!--<script src="<?php echo base_url(); ?>js/jquery-1.8.2.js" type="text/javascript"></script>-->
<script src="<?php echo base_url() ?>js/jquery.form.js"></script>
<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>


<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/additional-methods.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.maskedinput.js"></script>
<script>
$(document).ready(function(){
    $('#user_skill,#user_interest,#user_about').jqEasyCounter({
        'maxChars': 200,
        'maxCharsWarning': 180
    });
});



    $(function () {
        $('.user_picture').on('change', function (event) {

            // startSpinner($('#picture_file_field_display'),$('#campaign_summary_picture_saving'));
            $('#edit_project_picture_file').children().remove();
            $(this).prependTo('#edit_project_picture_file').addClass('hidden-file-field');

            $('#user_picture').submit();
            $(this).clone(true).prependTo("#picture_file_field_display").removeClass('hidden-file-field').removeAttr('id');
        });
    });
    $(document).ready(function () {
        // bind form id and provide a simple callback function
        if ((pic = jQuery('#user_picture')).length)

            pic.ajaxForm({
                dataType: 'json',
                beforeSend: function () {
                    // alert('test');
                    $('.ploader').fadeIn();
                },
                success: function(data){    
                    if(data.msg.error){
                        $('#image_error').html(data.msg.error);
                        $('#image_error').show();
                        $('#image_success').hide();
                    }
                    if(data.msg.success){
                        $('#image_error').hide();
                        $('#image_success').html(data.msg.success);
                        $('#image_success').show();
                        $('#img1').attr('src',data.image.path);
                    }
                },
                error: onError,
                complete: function () {

                    $('.ploader').fadeOut();
                },
            });


    });

</script>
<script type="text/javascript">
    function filename(name) {
        document.getElementById('file_name').value = name.replace("C:\\fakepath\\", "");
    }


</script>

<script>
// validations
jQuery(function ($) {

   
   
    // method to check special characters in slug.
    jQuery.validator.addMethod("alphanumeric", function(value, element) {
        return this.optional(element) || /^\w+$/i.test(value);
    }, "Only letters, numbers, and underscores allowed.");

    // validate signup form on keyup and submit
    var validator = $("#account").validate({
        rules: {
            user_name: {
                required: true,
                maxlength: 40,
                minlength: 3,
                maxWords: 1,
                minWords: 1
            },

            profile_slug: {
                required: true,
                maxlength: 15,
                minlength: 3,
                maxWords: 1,
                minWords: 1,
               
            },

            last_name: {
                required: true,
                maxlength: 40,
               
                maxWords: 1,
                minWords: 1
            },
            email: {
                required: true,
                email: true
            },
            address: {
                required: true,
                /*address: true*/
            },
            zip_code: {
                required: true,
               
            },
            user_occupation:{
                maxlength: 40
            },

            mobile_number:{
                 required: true,
            }


        },
        messages: {
            user_name: {
                required: '<?php echo ENTER_FIRST_NAME;?>',
                maxlength: '<?php echo FIRST_NAME_CANNOT_BE_MORE_THAN_THIRTY_CHAR;?>',
                minlength: '<?php echo FIRST_NAME_CANNOT_BE_LESS_THAN_THREE_CHAR;?>',
                maxWords: '<?php echo ENTER_FIRST_NAME_ONLY;?>',
                minWords: '<?php echo ENTER_FIRST_NAME;?>'
            },

            last_name: {
                required: '<?php echo ENTER_LAST_NAME;?>',
                maxlength: '<?php echo LAST_NAME_CANNOT_BE_MORE_THAN_THIRTY_CHAR;?>',
               
                maxWords: '<?php echo ENTER_LAST_NAME_ONLY;?>',
                minWords: '<?php echo ENTER_LAST_NAME;?>'
            },

            profile_slug: {
                required: '<?php echo ENTER_PROFILE_SLUG;?>',
                maxWords: '<?php echo SPACE_ID_NOT_REQUIRED;?>'
            },

            email: {
                required: '<?php echo EMAIL_IS_REQUIRED;?>',
                email: '<?php echo EMAIL_VALID_EMAIL;?>'
                /*remote: "emails.action"*/
            },

            address: {
                required: '<?php echo THIS_FIELD_CANNOT_BE_LEFT_BLANK;?>'
            },
            zip_code: {
                required: '<?php echo ZIPCODE_IS_REQUIRED;?>',
                minlength: '<?php echo ENTER_ZIP_CODE_NOT_LESS_THAN_FIVE_CHAR;?>',
                maxlength: '<?php echo ENTER_ZIP_CODE_NOT_MORE_THAN_SEVEN_CHAR;?>'
            },
            user_occupation:{
                maxlength: '<?=OCCUPATION_CANNOT_MORE_THAN_39_CHARS;?>'
            },
            mobile_number:{
                 required: 'Mobile Number is Required',
            }



        },
        // the errorPlacement has to take the table layout into account
        errorPlacement: function (error, element) {
            if (element.is(":radio"))
                error.insertAfter(element.parent());
            else if (element.is(":checkbox"))
                error.insertAfter(element.next());
            else if (element.hasClass('book_job_now_price_input')) {
                error.insertAfter(element.next().next());

            }
            else {
                if (element.attr('name') == 'job_price') {
                    error.insertAfter(element.next());
                }
                else
                    error.insertAfter(element);
            }
        },


        // set this class to error-labels to indicate valid fields
        success: function (label) {
            // set &nbsp; as text for IE
            label.html("&nbsp;").addClass("valid");

        },

        onkeyup: function (element) {
            $(element).valid();
        },
        onfocusout: function (element) {
            $(element).valid();
        },

    });



});


function validate_slug() {

    var profile_slug = document.getElementById('profile_slug').value;
    var mapdata = {"profile_slug": profile_slug};
    var getStatusUrl = '<?php echo site_url('account/validate_slug'); ?>';

    $.ajax({
        url: getStatusUrl,
        dataType: 'json',
        type: 'POST',
        timeout: 99999,
        global: false,
        data: mapdata,
        success: function (data) {

            //alert(data);
            $("#already").html(data);

            //return false;
            if (data.msg.error != "") {

                alert(data.msg.error);
                $(".success_comment").hide();
                $(".error_comment").show();
                $(".error_comment").html(data.msg.error);
                return false;
            }

            if (data.msg.success != "") {

                return true;
            }

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            return false;
        }
    });
}


function CheckAbn(abnnumber) {

    var abnnumber = $("#abn_number").val();
    
    var mapdata = {"abnnumber": abnnumber};
    var getStatusUrl = '<?php echo site_url('account/CheckValidAbn'); ?>';

    $.ajax({
        url: getStatusUrl,
        dataType: 'json',
        type: 'POST',
        timeout: 99999,
        global: false,
        data: mapdata,
        success: function (data) {

       
            //return false;
            if(data.success == "success")
            {

               $(".abn-check").html('Verify <i class="fa fa-check green"></i>');
            }
            if (data.error == "error") {

                $(".abn-check").html('Verify <i class="fa fa-times red"></i></i>');
               
                return false;
            }

         

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            return false;
        }
    });
}

function commentaction(id, type, type1) {
    if (type == 'delete') {
        var delcomment = confirm('<?php echo DELETE_COMMENT_CONFIRMATION;?>');
    }
    if (type == 'delete') {
        if (delcomment) {
            var mapdata = {"comment_id": id, "type": type};
        }
    }
    var getStatusUrl = '<?php echo site_url('account/commentdelete')?>';

    $.ajax({
        url: getStatusUrl,
        dataType: 'json',
        type: 'POST',
        timeout: 99999,
        global: false,
        data: mapdata,

        success: function (data) {


            if (data.msg.error != "") {


                $(".success_comment").hide();
                $(".error_comment").show();
                $(".error_comment").html(data.msg.error);
                return;
            }

            if (data.msg.success != "") {

                $("#" + id).hide();

                if (type1 == "1") {
                    $("#recentCo").hide();
                    $("#no_record_find").show();
                }

                $(".success_comment").show();
                $(".error_comment").hide();
                $(".success_comment").html(data.msg.success);
                $(".success_comment").fadeOut(5000);
            }

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {

        }
    });
}

$(window).bind("load", function () {
    $('#dvLoading').fadeOut(2000);
    $('#mainbg_div').fadeIn(2000);
    $('.min_height').css('minHeight', 0);
    $(function () {
        $('#locationMap').gmap3();

        $('#address').autocomplete({
            source: function () {
                $("#locationMap").gmap3({
                    action: 'getAddress',
                    address: $(this).val(),
                    callback: function (results) {
                        if (!results) return;
                        $('#address').autocomplete(
                            'display',
                            results,
                            false
                        );
                    }
                });
            },
            cb: {
                cast: function (item) {

                    return item.formatted_address;
                },
                select: function (item) {
                    var length_arr = item.address_components.length;

                    if (length_arr > 0) {
                        for (i = 0; i < length_arr; i++) {
                            if (item.address_components[i].types == 'postal_code') {
                                var zipcode_value = item.address_components[i].long_name;

                                $('input[name=zip_code]').val(zipcode_value);
                                if (zipcode_value != '') {
                                    $('input[name=zip_code]').removeClass("error");

                                }

                            }
                        }
                    }
                    $("#locationMap").gmap3(
                        {action: 'clear', name: 'marker'},
                        {
                            action: 'addMarker',
                            latLng: item.geometry.location,
                            map: {center: true}
                        }
                    );
                }
            }


        });


        <?php if($address != ''){ ?>
        $('#locationMap').gmap3({
            action: 'addMarker',
            address: "<?php echo $address; ?>",
            map: {
                center: true,
                zoom: 12
            },
            marker: {
                options: {
                    draggable: true
                }
            }
        });
        <?php } ?>


    });

});

$(window).bind("load", function () {
    $('#dvLoading').fadeOut(2000);
    $('#mainbg_div').fadeIn(2000);
    $('.min_height').css('minHeight', 0);
    $(function () {
        $('#companylocationMap').gmap3();

        $('#company_address').autocomplete({
            source: function () {
                $("#companylocationMap").gmap3({
                    action: 'getAddress',
                    address: $(this).val(),
                    callback: function (results) {
                        if (!results) return;
                        $('#company_address').autocomplete(
                            'display',
                            results,
                            false
                        );
                    }
                });
            },
            cb: {
                cast: function (item) {

                    return item.formatted_address;
                },
              
            }


        });



    });

});


 $("#autoc").hide();

    function autotext() {
        var xmlHttp;
        try {
            xmlHttp = new XMLHttpRequest();// Firefox, Opera 8.0+, Safari
        }
        catch (e) {
            try {
                xmlHttp = new ActiveXObject("Msxml2.XMLHTTP"); // Internet Explorer
            }
            catch (e) {
                try {
                    xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                catch (e) {
                    //alert("<?php //echo NO_AJAX;?>");
                    return false;
                }
            }
        }
        xmlHttp.onreadystatechange = function () {
            if (xmlHttp.readyState == 4) {
                document.getElementById('autoc').innerHTML = xmlHttp.responseText;

                if (document.getElementById('srhdiv')) {


                  $("#autoc").show();


                }
                else {
                   $("#autoc").hide();
                    
                }
            }
        }
        var text = document.getElementById('zip_code').value;
        text = text.replace(/[^a-zA-Z0-9]/g, '');
        xmlHttp.open("GET", "<?php echo site_url('account/search_auto_postcode');?>/" + text, true);
        xmlHttp.send(null);
    }
    function selecttext(el) {

        var selectvalue = el.innerHTML;
       
        $("#zip_code").val(selectvalue);
        $("#autoc").hide();
       
    }
</script>




<?php

$attributes = array('id' => 'user_picture', 'name' => 'user_picture', 'enctype' => 'multipart/form-data', 'class' => 'edit_project', 'method' => 'post', 'accept-charset' => 'UTF-8');
echo form_open_multipart('account/UserImageAjax/' . $user_id . "/0", $attributes);
?>

<div class="input-item-file" id="edit_project_picture_file" style="display:none">
</div>
</form>

<section>
        <div class="db-head">
            <div class="container">
                <div class="page-title">
                    <h2>Edit Profile</h2>
                </div>
            </div>
        </div>
        <div class="grey-bg" id="section-area">         
            <div class="container">
                <div class="row">

                    <div class="col-md-3 db-mc-left">
                     
<?php echo $this->load->view('default/dashboard_sidebar'); ?>

                        
                    </div>
                   
                    <div class="col-md-9 db-mc-right">
                       <?php
    if ($this->session->flashdata('success_message_account') != '') {
        ?>
        <div class="alert-success alert alert-message">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('success_message_account'); ?>
        </div>
    <?php
    }   if ($this->session->flashdata('error_message_account') != '') {

        ?>
        <div class="alert-danger alert alert-message">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('error_message_account'); ?>
        </div>
    <?php
    } 
    ?>

    <!--snipet to display error and success message for image upload-->
    <div class="alert-danger alert alert-message" id="image_error" style="display:none">
           
    </div>
    <div class="alert-success alert alert-message" id="image_success" style="display:none">
           
    </div>
                    <div class="row">
                    <div class="col-md-3 col-md-push-9">
                    <div class="upload-user-photo">
                        <div class="user-profile-img">
                           <?php
                        if ($image != '') {
                            ?>
                            <img src="<?php echo base_url(); ?>upload/user/user_big_image/<?php echo $image; ?> "
                                 id="img1"/>
                        <?php
                        } else {
                            ?>
                            <img src="<?php echo base_url(); ?>upload/user/user_big_image/no_man.jpg " id="img1"/>
                        <?php } ?>
                        </div>
                        <label id="" class="btn btn-primary text-uppercase btn-upload">
                          

                          <?php
                        if ($image != '') {
                            ?>
                            <span > <?php echo CHANGE_IMAGE; ?> </span>
                        <?php } else { ?>
                            <span class="first_image"> <?php echo ADD_IMAGE; ?> </span>
                        <?php } ?>                    
                        <input type="file" style="margin-right:3px;" name="file1" id="file1" class="form-control user_picture">
                    </label>
                    </div>
                  </div>

                    <div class="col-md-9 col-md-pull-3">
                      <?php
            $attributes = array('name' => 'account', 'id' => 'account', 'enctype' => 'multipart/form-data', 'class' => 'dec-form form-horizontal');
            echo form_open_multipart('account/validate_account', $attributes)?>
                    <div class="form-group">
                    <label class="control-label col-sm-3" for="user_name"><?php echo FIRST_NAME; ?><span class="form_star">*</span></label>
                     <div class="col-sm-9">
                     <input type="text" class="form-control" name="user_name" id="user_name"
                               value="<?php echo $user_name; ?>">
                        </div>
                     </div>
                    <div class="form-group">
                    <label class="control-label col-sm-3" for="last_name"><?php echo LAST_NAME; ?><span class="form_star">*</span></label>
                     <div class="col-sm-9">
                     <input type="text" class="form-control" name="last_name" id="last_name"
                               value="<?php echo $last_name; ?>" >
                        </div>
                     </div>
                     <div class="form-group">
                    <label class="control-label col-sm-3" for="user_type">User Type<span class="form_star">*</span></label>
                     <div class="col-sm-9 ">
                     
                    <div class="user-type-dash">
                        <div class="checkbox">
                          <label class="ec-checkbox">
                            <input type="checkbox" class="ut-checkbox" name="user_type[]" value="1" 
                            <?php if(UserTypeExists(1) == 1) echo 'checked="checked"'; ?>> Investor
                            <i></i>
                          </label>
                        </div>
                        <div class="checkbox">
                          <label class="ec-checkbox">
                            <input type="checkbox" class="ut-checkbox" id="chk-box" name="user_type[]" value="2"
                            <?php if(UserTypeExists(2) == 1) echo 'checked="checked"'; ?>> R/E Agent
                            <i></i>
                          </label>
                        </div> 
                        <div class="checkbox">
                          <label class="ec-checkbox">
                            <input type="checkbox" class="ut-checkbox" id="chk-box1" name="user_type[]" value="3"
                            <?php if(UserTypeExists(3) == 1) echo 'checked="checked"'; ?>> Property Owner
                            <i></i>
                          </label>
                        </div>  

                      <div class="checkbox">
                          <label class="ec-checkbox">
                            <input type="checkbox" class="ut-checkbox" id="chk-box2" name="user_type[]" value="4"
                            <?php if(UserTypeExists(4) == 1) echo 'checked="checked"'; ?>> Property Developer
                            <i></i>
                          </label>
                        </div>  
                                             
                    </div>
           
                        </div>
                     </div>
                     <?php 
                     $display="none";
                      if(UserTypeExists(2) == 1 || UserTypeExists(4) == 1)
                      {
                        $display="block";
                      }

                     ?>
                       <div id="chk-info" class="form-group" style="display:<?php echo $display?>"> 
                    <label class="control-label col-sm-3" for=""></label>
                     <div class="col-sm-9">
                     
                                 <div class="col-sm-12 form-group">
                        <input type="text" class="form-control " id="company_name" name="company_name"  value="<?php echo SecureShowData($company_name)?>" placeholder="Enter company name" >
                         </div>    
                                 <div class="col-sm-12 form-group">
                        <input type="text" class="form-control" id="abn_number" name="abn_number" value="<?php echo $abn_number;?>" placeholder="Enter an  Australian Business Number (ABN) " >
                     
                            <a href="javascript://" class="abn-check" onclick="CheckAbn();"> 
                            Verify </a>
                   
                         </div>  
                         <div class="col-sm-12  form-group">
                          <input type="text" class="form-control" id="company_address" name="company_address" value="<?php echo $company_address;?>" placeholder="Enter company address" >
                       
                          <div id="companylocationMap" class="gmap3 google-map"></div>
                         </div>
                            
                        </div> 
                </div>
                     <div class="form-group">
                    <label class="control-label col-sm-3" for="pro_slug">Username<span class="form_star">*</span></label>
                     <div class="col-sm-9">
                    <input type="text" class="form-control" name="profile_slug" id="profile_slug"
                               <?php if ($profile_slug == ''){ ?>value="<?php echo $user_name . $last_name . $userid; ?>"
                               <?php } else { ?>value="<?php echo $profile_slug;
                               } ?>">
                        </div>
                     </div>
                     <div class="form-group">
                    <label class="control-label col-sm-3" for="email"><?php echo EMAIL; ?><span class="form_star">*</span></label>
                     <div class="col-sm-9">
                     <input type="email" class="form-control" name="email" id="email" value="<?php echo $email; ?>" disabled="disabled" >
                        </div>
                     </div>
                     <div class="form-group">
                    <label class="control-label col-sm-3" for="mob-num">Mobile Number<span class="form_star">*</span></label>
                     <div class="col-sm-9">
                     <input type="mob-num" class="form-control" id="mobile_number" name="mobile_number" 
                     value="<?php echo $mobile_number;?>" >
                        </div>
                     </div>
                    <div class="form-group">
                    <label class="control-label col-sm-3" for="address"><?php echo ADDRESS; ?><span class="form_star">*</span></label>
                     <div class="col-sm-9">
                     <input type="text" class="form-control" name="address" id="address"
                               value="<?php echo $address; ?>" autocomplete="on">
                        </div>
                     </div>
                     <div class="form-group">
                    <label class="control-label col-sm-3" for="address"></label>
                     <div class="col-sm-9">
                        <div class="add-map">
                      <div id="locationMap" class="gmap3 google-map"></div>
                        </div>
                        </div>
                     </div>   
                     <div class="form-group">
                    <label class="control-label col-sm-3" for="zip_code"><?php echo ZIPCODE; ?><span class="form_star">*</span></label>
                     <div class="col-sm-9">
                     <input type="text" class="form-control" name="zip_code" id="zip_code"
                               value="<?php echo $zip_code; ?>" onkeyup="autotext();" >
                                <div id="autoc" class="in-autocomplete"></div> 
                        </div>
                     </div>
                     
                     <div class="form-group">
                    <label class="control-label col-sm-3" for="abt_self"><?php echo ABOUT_YOURSELF; ?></label>
                     <div class="col-sm-9">
                    <textarea class="text-area" rows="3" name="user_about" id="user_about"><?php echo SecureShowData($user_about); ?></textarea>
                        </div>
                     </div>
                     <div class="form-group">
                    <label class="control-label col-sm-3" for="occupation"><?php echo OCCUPATION; ?></label>
                     <div class="col-sm-9">
                     <input type="text" class="form-control" name="user_occupation" id="user_occupation"
                                value="<?php echo $user_occupation; ?>" >
                        </div>
                     </div>
                     
                     
                 <div class="form-group">        
      <div class="col-sm-offset-3 col-sm-9">
        <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id; ?>"/>
        <button type="submit" class="btn btn-primary text-uppercase">Save Changes</button>
      </div>
    </div>
  </form>
</div>
                    </div>
                    
                    </div>

                        
                </div>
            </div>
        </div>
    </section>


  <script src="<?php echo base_url();?>assets_front/bootstrap-select/js/bootstrap-select.js"></script>
  
    <script type="text/javascript">
    $('#chk-box,#chk-box2').on('change', function(){ // on change of state
    if($('#chk-box,#chk-box2').is(':checked')) // if changed state is "CHECKED"
    {
        $( "#chk-info" ).show();
    }
    else 
    {
        $( "#chk-info" ).hide();
    }
})
    
    
    </script>
<script type="text/javascript" src="http://code.jquery.com/jquery-migrate-1.0.0.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/map/gmap3.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/map/jquery-autocomplete.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/map/jquery-autocomplete.css"/>
