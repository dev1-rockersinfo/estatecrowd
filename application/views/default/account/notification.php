 <section>
	  	<div class="db-head">
	      	<div class="container">
	      		<div class="page-title">
		        	<h2>Notification Centre</h2>
		        </div>
	      	</div>
	  	</div>
	  	<div class="grey-bg" id="section-area">	  		
	      	<div class="container">
	      		<div class="row">
		      		<div class="col-md-3 db-mc-left">
                    <?php echo $this->load->view('default/dashboard_sidebar'); ?>
		      			
		      		</div>

		      		<div class="col-md-9 db-mc-right">

                    <?php
    if ($this->session->flashdata('success_message_notification') != '') {
        ?>
        <div class="alert-success alert alert-message">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('success_message_notification'); ?>
        </div>
    <?php
    }  ?>

<?php $attributes = array('name' => 'notification', 'id' => 'notification');
echo form_open_multipart('account/notification', $attributes)?>
           			<ul class="notification">
  					  <li>
        <div class="row">
            <label class="col-sm-4">Campaigns you Invested in</label>

            <div class="col-sm-8">
                <fieldset>
									<span class="media-email">
										<label class="btn">
                                            <input type="checkbox" value="1" id="add_fund" name="add_fund"
                                            <?php if ($add_fund == 1){ ?>checked="checked"<?php
                                            } else {
                                            } ?>
                                            > <i class="fa fa-envelope"></i>
                                        </label>
									</span>
                    <span class="media-body">New investors</span>
                </fieldset>
                <fieldset>
									<span class="media-email">
										<label class="btn">
                                            <input type="checkbox" value="1" id="comment_alert" name="comment_alert" <?php if ($comment_alert == 1){ ?>checked="checked"<?php
                                            } else {
                                            } ?>> <i class="fa fa-envelope"></i>
                                        </label>
									</span>
                    <span class="media-body">New comments</span>
                </fieldset>
                <fieldset>
									<span class="media-email">
										<label class="btn">
                                            <input type="checkbox" value="1" id="you_follow_alert" name="you_follow_alert" <?php if ($you_follow_alert == 1){ ?>checked="checked"<?php
                                            } else {
                                            } ?>> <i class="fa fa-envelope"></i>
                                        </label>
									</span>
                    <span class="media-body">New followers</span>
                </fieldset>

                <fieldset>
									<span class="media-email">
										<label class="btn">
                                            <input type="checkbox" value="1" id="update_alert" name="update_alert"  <?php if ($update_alert == 1){ ?>checked="checked"<?php
                                            } else {
                                            } ?>> <i class="fa fa-envelope"></i>
                                        </label>
									</span>
                    <span class="media-body">Updates</span>
                </fieldset>
            </div>
        </div>
    </li>
  					  <li>
        <div class="row">
            <label class="col-sm-4">Properties you are following</label>

            <div class="col-sm-8">
                <fieldset>
									<span class="media-email">
										<label class="btn">
                                            <input type="checkbox" value="1" autocomplete="off" 
                                            name="new_pledge_alert"  <?php if ($new_pledge_alert == 1){ ?>checked="checked"<?php
                                            } else {
                                            } ?>> <i class="fa fa-envelope"></i>
                                        </label>
									</span>
                    <span class="media-body">New investors</span>
                </fieldset>
                <fieldset>
									<span class="media-email">
										<label class="btn">
                                            <input type="checkbox" value="1" autocomplete="off" 
                                            name="new_comment_alert" <?php if ($new_comment_alert == 1){ ?>checked="checked"<?php
                                            } else {
                                            } ?>> <i class="fa fa-envelope"></i>
                                        </label>
									</span>
                    <span class="media-body">New comments</span>
                </fieldset>
                <fieldset>
									<span class="media-email">
										<label class="btn">
                                            <input type="checkbox" value="1" autocomplete="off" 
                                            name="new_follow_alert" <?php if ($new_follow_alert == 1){ ?>checked="checked"<?php
                                            } else {
                                            } ?>> <i class="fa fa-envelope"></i>
                                        </label>
									</span>
                    <span class="media-body">New followers</span>
                </fieldset>
                <fieldset>
									<span class="media-email">
										<label class="btn">
                                            <input type="checkbox" value="1" autocomplete="off" 
                                            name="new_updates_alert" <?php if ($new_updates_alert == 1){ ?>checked="checked"<?php
                                            } else {
                                            } ?>> <i class="fa fa-envelope"></i>
                                        </label>
									</span>
                    <span class="media-body">Updates</span>
                </fieldset>
            </div>
        </div>
    </li>
  				<li>
        <div class="row">
            <label class="col-sm-4">Your Investment Campaigns</label>

            <div class="col-sm-8">
                <fieldset>
									<span class="media-email">
										<label class="btn">
                                            <input type="checkbox" checked="checked" value="1" autocomplete="off" name="creator_pledge_alert" <?php if ($creator_pledge_alert == 1){ ?>checked="checked"<?php
                                            } else {
                                            } ?>> <i class="fa fa-envelope"></i>
                                        </label>
									</span>
                    <span class="media-body">New investors</span>
                </fieldset>
                <fieldset>
									<span class="media-email">
										<label class="btn">
                                            <input type="checkbox" checked="checked" autocomplete="off" 
                                            value="1" name="creator_comment_alert" <?php if ($creator_comment_alert == 1){ ?>checked="checked"<?php
                                            } else {
                                            } ?>> <i class="fa fa-envelope"></i>
                                        </label>
									</span>
                    <span class="media-body">New comments</span>
                </fieldset>
                <fieldset>
									<span class="media-email">
										<label class="btn">
                                            <input type="checkbox" checked="checked" autocomplete="off" 
                                            value="1" name="creator_follow_alert" <?php if ($creator_follow_alert == 1){ ?>checked="checked"<?php
                                            } else {
                                            } ?>> <i class="fa fa-envelope"></i>
                                        </label>
									</span>
                    <span class="media-body">New followers</span>
                </fieldset>
                <fieldset>
									<span class="media-email">
										<label class="btn">
                                            <input type="checkbox" checked="checked" autocomplete="off" 
                                            value="1" name="creator_newup_alert" <?php if ($creator_newup_alert == 1){ ?>checked="checked"<?php
                                            } else {
                                            } ?>> <i class="fa fa-envelope"></i>
                                        </label>
									</span>
                    <span class="media-body">Updates</span>
                </fieldset>
            </div>
        </div>
        
    </li>	  
   					  <li>
        <div class="row">
            <label class="col-sm-4">Social notifications</label>

            <div class="col-sm-8">
                <fieldset>
									<span class="media-email">
										<label class="btn">
                                            <input type="checkbox" autocomplete="off" value="1" name="social_notification_alert" <?php if ($social_notification_alert == 1){ ?>checked="checked"<?php
                                            } else {
                                            } ?>> <i class="fa fa-envelope"></i>
                                        </label>
									</span>
                    <span class="media-body">New followers</span>
                </fieldset>

                <fieldset>
									<span class="media-email">
										<label class="btn">
                                            <input type="checkbox" autocomplete="off" value="1" name="comment_reply_alert" <?php if ($comment_reply_alert == 1){ ?>checked="checked"<?php
                                            } else {
                                            } ?>> <i class="fa fa-envelope"></i>
                                        </label>
									</span>
                    <span class="media-body">Comment reply</span>
                </fieldset>
            </div>
        </div>
    </li>
					</ul>
<div class="row">
    <div class="col-sm-8 col-md-offset-4">
        <input type="hidden" value="<?php echo $id;?>" name="id">
        <input type="submit" value="Save Settings" class="btn btn-primary">
    </div>
</div>
           </form>         
                    </div>
			      		
	      		</div>
	      	</div>
	  	</div>
    </section>
  <script src="<?php echo base_url();?>assets_front/bootstrap-select/js/bootstrap-select.js"></script>