<div class="container padTB50">
    <div class="inner-container-bg">
        <div class="no-data">
            <?php

            $reason_inactive_hidden = $properties['reason_inactive_hidden'];

            if ($reason_inactive_hidden != '') {

                $display_reason = $reason_inactive_hidden;
            } else {
                $display_reason = REASON_INACTIVE_THIS;

            } ?>
            <?php echo THIS_PROJECT_IS_HIDDEN_AND_NO_LONGER . ' ' . THIS_REASON . ' ' . $display_reason; ?>
        </div>
    </div>
</div>
