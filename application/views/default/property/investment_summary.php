   <section>
      <div class="db-head">
          <div class="container">
            <div class="page-title">
              <h2>Add Propety Details</h2>
            </div>
          </div>
      </div>
      <div class="grey-bg" id="section-area">       
          <div class="container">
            <div class="row">
              <div class="col-md-3 faqs-left">
                <?php echo $this->load->view('default/property/property_sidebar');?>
              </div>
<div class="col-md-9 add-pro-right">
     <div class="row dec-form">

     <form action="<?php echo site_url('property/investment_summary/' . $id) ?>" method="post" accept-charset="utf-8"
      id="frm_project" name="frm_project" enctype="multipart/form-data">
                    
      <div id="property-detail" class="propety-area clearfix">
        <div class="form-heading">
        <h3>Investment Summary</h3>
         </div>
           <div class="property-detail-body row">
         <div class="form-group col-md-12">
                
          <textarea id="investment_summary" name="investment_summary"> <?php echo $investment_summary;?></textarea>          <span class="create_step_error"> <?php  echo form_error('investment_summary');?></span>           
           </div>
             <div class="col-md-12 addpro-nxt ">
                      <button type="submit" class="btn btn-primary btn-small text-uppercase pull-right" > Next</button>
                      
                     </div> 
                     </div> 
                    </div>

            </form>
            </div>
          </div>
      </div>
    </section>

     <script src="<?php echo base_url();?>js/redactor.min.js"></script>

      <script type="text/javascript">
      $(document).ready(
        function()
        {
          $('#investment_summary').redactor();
          
        }
      );
      </script>