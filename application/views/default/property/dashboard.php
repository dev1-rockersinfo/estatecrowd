<?php 

$campaign_id = $properties['campaign_id'];
$property_id = $properties['property_id'];
$property_user_id = $properties['user_id'];
$property_address = $properties['property_address'];
$property_url = $properties['property_url'];
$property_site_url = site_url('properties/'.$property_url);
$status = $properties['status'];
$property_suburb = $properties['property_suburb'];
$bedrooms = $properties['bedrooms'];
$bathrooms = $properties['bathrooms'];
$property_description = SecureShowData($properties['property_description']);
$investment_summary = SecureShowData($properties['investment_summary']);
$user_type = json_decode($properties['user_type']);
$cars = $properties['cars'];
$property_type_name = $properties['property_type'];

$property_sub_type_name = $properties['property_sub_type'];

$company_name = $properties['company_name'];
$min_property_investment_range = set_currency($properties['min_property_investment_range']);
$max_property_investment_range = set_currency($properties['max_property_investment_range']);
$campaign_units = $properties['campaign_units'];
$share_available = $campaign_units - $properties['campaign_units_get'];
$investment_close_date = GetDaysLeft($properties['investment_close_date']);
$date_added = $properties['date_added'];
$user_id = $properties['user_id'];

$min_investment_amount = $properties['min_investment_amount'];
$amount_get = $properties['amount_get'];
$amount_array = array('min_investment_amount'=>$min_investment_amount,'amount_get'=>$amount_get);
$percentage = GetCampaignPercentage($amount_array);

$check_status = array(0, 1);
$click_share = 'no';

if (!in_array($status, $check_status)) {
    $click_share = 'yes';
}



if(is_file(base_path().'upload/property/small/'.$properties['cover_image']) && $properties['cover_image'] != '')
{
     $cover_image = base_url().'upload/property/small/'.$properties['cover_image'];
}
else
{
    $cover_image = base_url().'upload/property/small/no_img.jpg';
}


if(is_file(base_path().'upload/user/user_small_image/'.$properties['image']) && $properties['image'] != '')
{
     $image = base_url().'upload/user/user_small_image/'.$properties['image'];
}
else
{
    $image = base_url().'upload/user/user_small_image/no_man.jpg';
}
$is_allow_comments = true;
//$is_allow_comments = $is_property_owner;


$is_allow_updates = true;
//$is_allow_updates = $is_property_owner;
?>
    <section>
	  	<div class="db-head">
	      	<div class="container">
	      		<div class="page-title">
		        	<h2><?php echo $property_address; ?></h2>
		        </div>
	      	</div>
	  	</div>
	  	<div class="grey-bg" id="section-area">	  		
	      	<div class="container">
	      		<div class="row">
		      		<div class="col-md-3 db-cmp-left">
                   <?php echo $this->load->view('default/dashboard_sidebar'); ?>
                        
		      			
		      		</div>
<div class="col-md-9 db-cmp-right">
    <div class="campaign-db-nav">
		<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#overview" aria-controls="overview" role="tab" data-toggle="tab">Overview</a></li>
    <li role="presentation"><a href="#updates" aria-controls="updates" role="tab" data-toggle="tab">Updates</a></li>
    <li role="presentation"><a href="#comment" aria-controls="comment" role="tab" data-toggle="tab">Comment</a></li>
    <li role="presentation"><a href="#investments" aria-controls="investments" role="tab" data-toggle="tab">Investments</a></li>
     <li role="presentation"><a href="#widgets" aria-controls="Widgets" role="tab" data-toggle="tab"> Widgets</a></li>
   <li role="presentation"><a href="#share" aria-controls="share" role="tab" data-toggle="tab">Share</a></li>
   <li role="presentation"><a href="#followers" aria-controls="followers" role="tab" data-toggle="tab"> Followers</a></li>
  
  </ul>
	<div class="campaign-db-tab">
              <div class="tab-content">
             <div role="tabpanel" class="tab-pane active" id="overview">   <div class="row">
             <div class="col-md-8">
              <div class="media campaign-top-wrap">
       				 <div class="media-left">
           <img src="<?php echo $cover_image;?>">
        </div>
       				<div class="media-body">
                        <h4 class="media-heading">
                <?php echo $property_address; ?>               <span class="pro-status active-status"><?php echo property_status($status);?></span>
            </h4>

            <div class="in-cat-loc">
                    <span class="pro-category">
                        <a href="javascript:void(0)"><i class="fa fa-tag"></i> <?php echo $property_sub_type_name;?>    , <?php echo $property_type_name;?>                       </a>
                    </span> 
                    <span class="pro-location">
                    <i class="fa fa-map-marker"></i> <?php echo $property_suburb;?> </span>
            </div>

        </div>
                    
            </div>
            </div>
            <div class="proMsg">
                <a href="message_conversation.php"><i class="fa fa-comment-o"></i></a>
            </div>
            <div class="col-md-4">
            
            <div class="progressBar barometer-added pull-right">
                      <div class="barometer disp-inline" data-progress="<?php echo $percentage; ?>" data-width="106" data-height="100" data-strokewidth="9" data-stroke="#656565" data-progress-stroke="#49d850"><label class="percent"><?php echo $percentage; ?>%<span>Complete</span></label>
                      </div>
                    </div>
            
            </div>
            </div>
      
    
    		<ul class="row in-pro-status">
        <li class="col-md-4 col-sm-8">
            <div class="pro-status-bg">
            <div class="pro-status-left">
           <span class="glyphicon glyphicon-lock"></span>
            </div>
            <div class="pro-status-right">
                <h3><?php echo set_currency($amount_get);?> </h3>
                <p>Investment Secured</p>
                </div>
            </div>
        </li>
        <li class="col-md-4 col-sm-8">
        <div class="pro-status-bg">
            <div class="pro-status-left">
            <span class="glyphicon glyphicon-record"></span>
            </div>
            <div class="pro-status-right">
                <h3><?php echo set_currency($min_investment_amount);?></h3>
                <p>Investment Target</p>
                </div>
            </div>
            
        </li>
        <li class="col-md-4 col-sm-8">
        <div class="pro-status-bg">
            <div class="pro-status-left">
           <span class="glyphicon glyphicon-time"></span>
            </div>
            <div class="pro-status-right">
               <h3><?php echo $investment_close_date;?></h3>

                <p>Ended on <em><?php echo date("j M, Y", strtotime($properties['investment_close_date']));?></em></p>
                </div>
            </div>
        </li>
        
    </ul>
    
    <div class="proOverview-footer">
                    <a target="_blank" href="" role="button" class="btn btn-primary">Edit Campaign</a>
                            <a href="javascript:void(0)" class="btn btn-primary btn-gray">Delete Campaign</a>
                        <a target="_blank" href="<?php echo $property_site_url; ?>" class="btn btn-primary">View Campaign Page</a>
    </div>
             </div>
           <!--  -------------->
             <div role="tabpanel" class="tab-pane" id="updates">
                 <div class="alert alert-message alert-danger marB20" id="error_update" style="display:none">

    </div>
    <div class="alert alert-message alert-success marB20" id="success_update" style="display:none">

    </div>
    <div id="update_perk_list">
        <?php
        if ($is_allow_updates == true and $click_share == 'yes') {
            ?>
            <div class="add-btn-div"><a href="javascript://" class="btn btn-primary" onClick="createEditornew('new');"
                                        id="addnew_update"><?php echo ADD_NEW; ?></a></div>
        <?php
        }
        ?>
        <div class="add-update-div" id="addnew_box">
            <div id="div_new">

            </div>

            <!--After Click on Add new div End-->
            <div class="m-t-md btngrp">
                <a href="javascript://" class="btn btn-primary" onClick="createEditornew('add');"
                   id="ebutton_new"><?php echo SAVE; ?></a>
                <a href="javascript://" class="btn btn-primary gry" id="dbutton"
                   onClick="createEditornew('cancel');"><?php echo CANCEL ?></a>
            </div>
            <div id="update_id_value" class="hidden"></div>
        </div>
        <div id="paging">
            <ul class="paging media-list in-media-list activity-list">
                <?php
                if (is_array($property_updates)) {
                    foreach ($property_updates as $property_update) {
                        $update_id = $property_update['update_id'];
                        $property_update1 = SecureShowData($property_update['updates']);
                        $date_added = $property_update['date_added'];
                        $date_added = dateformat($property_update['date_added']);
                        //echo $date_added;
                        //echo $project_update1.'<br>';
                        ?>
                        <input type="hidden" name="update_<?php echo $update_id ?>" id="update_<?php echo $update_id ?>"
                               value="<?php echo $update_id; ?>"/>

                        <li class="media" id="updatebox_<?php echo $update_id; ?>">
                            <div class="media-body">
                                <h4 class="media-heading"><?php echo $date_added; ?></h4>

                                <div id="div_<?php echo $update_id; ?>">
                                    <?php echo $property_update1; ?>
                                </div>
                                <div class="delete-comment">
                                    <?php
                                    if ($is_allow_updates == true) {
                                        ?>
                                        <a href="javascript://" title="<?php echo EDIT; ?>"
                                           onClick="createEditornew('edit',<?php echo $update_id; ?>)"
                                           id="ebutton_<?php echo $update_id; ?>"><i
                                                class="glyphicon glyphicon-edit"></i></a>
                                        <a href="javascript://" data-toggle="tooltip" data-placement="bottom"
                                           title="<?php echo DELETE; ?>"
                                           onClick="removeEditor(<?php echo $update_id; ?>,'delete');"
                                           id="dbutton_<?php echo $update_id; ?>"><i
                                                class="glyphicon glyphicon-trash"></i></a>
                                    <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </li>
                    <?php
                    }
                } else {
                    ?>
                    <li class="no-data">
                        <?php echo THERE_IS_NO_UPDATES_FOR_THIS_PROJECT; ?>
                    </li>
                <?php } ?>
            </ul>
            <div class="pro-pagination"></div>
        </div>
    </div>
             </div>
              <!--  -------------------------- -->  
             <div role="tabpanel" class="tab-pane" id="comment">
             
             <?php if (is_array($property_comments)) {
    ?>
    <div class="alert alert-message alert-danger marB20" id="error_comment" style="display:none">

    </div>
    <div class="alert alert-message alert-success marB20" id="success_comment" style="display:none">

    </div>
<?php } ?>
<ul class="paging media-list in-media-list activity-list" id="comments_update">
<?php
//$getchild = getAllchildofcomment(39);echo '<pre/>';print_r($getchild);


if (is_array($property_comments)) {  //echo '<pre/>';print_r($project_comments);exit;
    foreach ($property_comments as $comments) {
        //$getchild = getAllchildofcomment(39) 
        $comment_user_id = $comments['user_id'];
        $comment_profile_slug = $comments['profile_slug'];

        $comment = SecureShowData($comments['comments']);
        $user = $comments['user_name'] . ' ' . $comments['last_name'];
        $date = $comments['date_added'];
        $ip = $comments['comment_ip'];
        $comment_id = $comments['comment_id'];
        $status = comment_status_name($comments['status']);
        $comment_type = $comments['comment_type'];
        $comment_type_text = '';
        if ($comment_type == 1) $comment_type_text = "Private Comment";

        $project_comment_image = $comments['image'];
        $date = dateformat($comments['date_added']);

        $statuses = array(1);
        $show_approve = 'yes';
        if (in_array($comments['status'], $statuses)) {
            $show_approve = 'no';
        }

        $statuses = array(2);
        $show_decline = 'yes';
        if (in_array($comments['status'], $statuses)) {
            $show_decline = 'no';
        }

        $statuses = array(3);
        $show_spam = 'yes';
        if (in_array($comments['status'], $statuses)) {
            $show_spam = 'no';
        }

        $statuses = array(1);
        $show_reply = 'no';
        if (in_array($comments['status'], $statuses)) {
            $show_reply = 'yes';
        }

        $statuses = array(1);
        $show_edit = 'no';
        if (in_array($comments['status'], $statuses)) {
            $show_edit = 'no';
        }
        if ($comment_user_id == $this->session->userdata('user_id')) {
            $show_edit = 'yes';
        }

        if ($is_allow_comments == false) {

            $show_edit = 'no';
            $show_reply = 'no';
            $show_decline = 'no';
            $show_spam = 'no';
            $show_approve = 'no';
        }
        ?>
        <?php $comment_status_class = "";
        if ($status == PENDING) {
            $comment_status_class = "pending-comment-status";
        }
        if ($status == APPROVED) {
            $comment_status_class = "approved-comment-status";
        }
        if ($status == DECLINE) {
            $comment_status_class = "decline-comment-status";
        }
        if ($status == SPAM) {
            $comment_status_class = "sapm-comment-status";
        }
        //$getchild = getAllchildofcomment($comments['comment_id']);echo '<pre/>';print_r($getchild);

        ?>

        <li class="media">
        <div class="media-left">
            <?php   if (is_file("upload/user/user_small_image/" . $project_comment_image)) {
                ?>
                <a href="<?php echo site_url('user/' . $comment_profile_slug); ?>"><img class="img-thumbnail"
                                                                                        src="<?php echo base_url() . 'upload/user/user_small_image/' . $project_comment_image; ?>"></a>

            <?php
            } else {
                ?>
                <a href="<?php echo site_url('user/' . $comment_profile_slug); ?>"><img class="img-thumbnail"
                                                                                        src="<?php echo base_url() . 'upload/user/user_small_image/no_man.jpg'; ?>"></a>

            <?php } ?>
        </div>
        <div class="media-body">
        <h4 class="media-heading">
            <a href="<?php echo site_url('user/' . $comment_profile_slug); ?>"><?php echo $user ?></a>
        </h4>

        <p><span
                class="comment-status <?php echo $comment_status_class; ?>"><?php echo $status; ?></span> <?php echo IP; ?>
            : <?php echo $ip; ?></p>

        <p class="private-comment"><?php echo $comment_type_text; ?></p>

        <p class="comment-text"><i class="fa fa-quote-left"></i>  <?php echo $comment; ?></p>

        <p><span class="media-time"><?php echo $date; ?></span></p>
        <?php
        $getchild = getAllchildofcomment($comments['comment_id']);
        //echo "<pre>";
        //print_r($getchild);
        if ($getchild) {
            foreach ($getchild as $gchild) {

                $child_statuses = array(1);
                $child_show_edit = 'no';
                if (in_array($gchild->status, $child_statuses)) {
                    $child_show_edit = 'no';
                }
                if ($gchild->user_id == $this->session->userdata('user_id')) {
                    $child_show_edit = 'yes';
                }

                if ($is_allow_comments == false) {

                    $child_show_edit = 'no';;
                }
                $child_comment_user_slug = $gchild->profile_slug;
                $child_comment_image = $gchild->image;
                $child_comment_name = $gchild->user_name . ' ' . $gchild->last_name;
                $child_comment_date_time = dateformat($gchild->date_added);
                $child_comment = SecureShowData($gchild->comments);
                $child_comment_hidden = $gchild->comments;
                $child_comment_id = $gchild->comment_id;
                $child_comment_user_id = $gchild->user_id;
                $child_comment_type = $gchild->comment_type;

                $child_ip = $gchild->comment_ip;

                if ($child_comment_image != '' && is_file("upload/user/user_small_image/" . $child_comment_image)) {
                    $childsrc = base_url() . 'upload/user/user_small_image/' . $child_comment_image;
                } else {
                    $childsrc = base_url() . 'upload/user/user_small_image/no_man.jpg';
                }


                ?>
                <div class="media">
                    <div class="media-left">
                        <a href="javascript://">
                            <img class="img-thumbnail" src="<?php echo $childsrc; ?>">
                        </a>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading marB5"><a
                                href="<?php echo site_url('user/' . $child_comment_user_slug); ?>"><?php echo $child_comment_name; ?></a>
                        </h4>

                        <p> <?php echo IP; ?>: <?php echo $child_ip; ?></p>

                        <p class="comment-text"><i class="fa fa-quote-left"></i> <?php echo $child_comment; ?>
                        </p>

                        <p><span class="media-time"> <?php echo $child_comment_date_time; ?></span></p>
                    </div>
                    <div class="delete-comment">


                        <?php
                        if ($child_show_edit == 'yes') {
                            ?>
                            <a href="javascript://" data-toggle="tooltip" data-placement="bottom"
                               title="<?php echo EDIT; ?>"
                               onclick="show_textarea(<?php echo $child_comment_id; ?>, 'edit')"><i
                                    class="glyphicon glyphicon-edit"></i></a>
                        <?php
                        }
                        ?>

                        <?php
                        if ($is_allow_comments == true) {
                            ?>
                            <a href="javascript://" data-toggle="tooltip" data-placement="bottom"
                               title="<?php echo DELETE; ?>"
                               onclick="commentaction(<?php echo $child_comment_id; ?>,'delete');"><i
                                    class="glyphicon glyphicon-trash"></i></a>
                        <?php
                        }
                        ?>

                    </div>

                </div>

                <div id="reply_<?php echo $child_comment_id; ?>" class="comment-reply">
                    <div class="in-form">
                        <textarea name="reply_comment" id="reply_comment_<?php echo $child_comment_id; ?>"
                                  class="form-control" rows="5" data-fieldlength="500"></textarea>
                    </div>
                    <div class="m-t-md btngrp">
                        <a href="javascript://" class="btn btn-primary "
                           id="edit_comment_<?php echo $child_comment_id; ?>"
                           onclick="commentaction(<?php echo $child_comment_id; ?>,'reply','edit');"><?php echo POST_REPLY; ?></a>
                        <a href="javascript://" class="btn btn-primary gry"
                           onclick="hide_textarea(<?php echo $child_comment_id; ?>)"><?php echo CANCEL; ?></a>
                    </div>
                </div>

                <input type="hidden" name="comment_<?php echo $child_comment_id; ?>"
                       id="comment_<?php echo $child_comment_id; ?>"
                       value="<?php echo $child_comment_hidden; ?>">
                <input type="hidden" name="comment_type_<?php echo $child_comment_id; ?>"
                       id="comment_type_<?php echo $child_comment_id; ?>"
                       value="<?php echo $child_comment_type; ?>">
                <input type="hidden" name="comment_ip_<?php echo $child_comment_id; ?>"
                       id="comment_ip_<?php echo $child_comment_id; ?>" value="<?php echo $child_ip; ?>">
                <input type="hidden" name="comment_user_id_<?php echo $child_comment_id; ?>"
                       id="comment_user_id_<?php echo $child_comment_id; ?>"
                       value="<?php echo $child_comment_user_id; ?>">
            <?php
            }

        }
        ?>

        <input type="hidden" name="comment_<?php echo $comment_id; ?>" id="comment_<?php echo $comment_id; ?>"
               value="<?php echo $comment; ?>">
        <input type="hidden" name="comment_type_<?php echo $comment_id; ?>"
               id="comment_type_<?php echo $comment_id; ?>" value="<?php echo $comment_type; ?>">
        <input type="hidden" name="comment_ip_<?php echo $comment_id; ?>"
               id="comment_ip_<?php echo $comment_id; ?>" value="<?php echo $ip; ?>">
        <input type="hidden" name="comment_user_id_<?php echo $comment_id; ?>"
               id="comment_user_id_<?php echo $comment_id; ?>" value="<?php echo $comment_user_id; ?>">

        <div class="delete-comment">
            <?php
            if ($show_approve == 'yes') {
                ?>
                <a href="javascript://" data-toggle="tooltip" data-placement="bottom"
                   title="<?php echo APPROVE; ?>"
                   onclick="commentaction(<?php echo $comment_id; ?>,'approved');"><i
                        class="glyphicon glyphicon-ok"></i></a>
            <?php
            }
            ?>

            <?php
            if ($show_reply == 'yes' && ($comment_user_id != $this->session->userdata('user_id'))) {
                ?>
                <a href="javascript://" data-toggle="tooltip" data-placement="bottom"
                   title="<?php echo REPLY; ?>" onclick="show_textarea(<?php echo $comment_id; ?>,'')"><i
                        class="glyphicon glyphicon-retweet"></i></a>
            <?php
            }
            ?>
            <?php
            if ($show_decline == 'yes' && ($comment_user_id != $this->session->userdata('user_id'))) {
                ?>
                <a href="javascript://" data-toggle="tooltip" data-placement="bottom"
                   title="<?php echo DECLINE; ?>"
                   onclick="commentaction(<?php echo $comment_id; ?>,'decline');"><i
                        class="glyphicon glyphicon-remove"></i></a>
            <?php
            }
            ?>
            <?php
            if ($show_edit == 'yes') {
                ?>
                <a href="javascript://" data-toggle="tooltip" data-placement="bottom"
                   title="<?php echo EDIT; ?>" onclick="show_textarea(<?php echo $comment_id; ?>, 'edit')"><i
                        class="glyphicon glyphicon-edit"></i></a>
            <?php
            }
            ?>

            <?php
            if ($is_allow_comments == true) {
                ?>
                <a href="javascript://" data-toggle="tooltip" data-placement="bottom"
                   title="<?php echo DELETE; ?>"
                   onclick="commentaction(<?php echo $comment_id; ?>,'delete');"><i
                        class="glyphicon glyphicon-trash"></i></a>
            <?php
            }
            ?>
            <?php
            if ($show_spam == 'yes' && $show_edit == 'no') {
                ?>
                <a href="javascript://" data-toggle="tooltip" data-placement="bottom"
                   title="<?php echo REPORT_SPAM; ?>"
                   onclick="commentaction(<?php echo $comment_id; ?>,'spam');"><i class="fa fa-exclamation"></i></a>
            <?php
            }
            ?>
        </div>
        </div>
        <div id="reply_<?php echo $comment_id; ?>" class="comment-reply">
            <div class="in-form">
                <textarea name="reply_comment" id="reply_comment_<?php echo $comment_id; ?>" class="form-control"
                          rows="5" data-fieldlength="500"></textarea>
            </div>
            <div class="m-t-md btngrp">
                <a href="javascript://" class="btn btn-primary" id="edit_comment_<?php echo $comment_id; ?>"
                   onclick="commentaction(<?php echo $comment_id; ?>,'reply','edit');"><?php echo POST_REPLY; ?></a>
                <a href="javascript://" class="btn btn-primary gry"
                   onclick="hide_textarea(<?php echo $comment_id; ?>)"><?php echo CANCEL; ?></a>
            </div>
        </div>
        </li>
    <?php
    }
} else {
    ?>
    <li class="no-data"><?php echo THERE_IS_NO_COMMENT_FOR_THIS_PROJECT; ?>.</li>
<?php } ?>
</ul>
    		</div>
            
             <div role="tabpanel" class="tab-pane" id="investments">
             <div class="donation-tab-head">
    		    <ul class="clearfix">
            <li>
                <h4><?php echo set_currency($min_investment_amount); ?> </h4>

                <p>Investment Target</p>
            </li>
            <li>
                <h4 id="total_recieve_amnt"></h4>

                <p>Total Received Payment</p>
            </li>
            <li><h4><?php echo $investment_close_date; ?></h4>

                <p><?php if ($status == 2) {
                        echo 'Ends on';
                    } else {
                        echo 'Ended';
                    } ?> <em><?php echo dateformat($properties['investment_close_date']); ?></em></p>

            </li>
        </ul>
   			 </div>
             <div class="table-responsive m-t-md">
        <table cellspacing="0" cellpadding="0" class="table">

            <thead>
            <tr>
                <th>Date</th>
                <th>Investor</th>               
                
                <th>Amount</th>
               
                <th>Status</th>
            </tr>
            </thead>

             <?php
            $total_recieve = 0;
            $total_amount = 0;
            $total_fees = 0;
            $preapproval_total_amount = 0;

            if (is_array($funder)) {

                foreach ($funder as $donor) {

                    if ($donor['profile_slug']) {
                        $campaign_profile_slug = $donor['profile_slug'];
                    } else {
                        $campaign_profile_slug = $donor['user_id'];
                    }

                    $campaign_funder_name = $donor['user_name'] . ' ' . $donor['last_name'];
                    $campaign_funder_date_time = dateformat($donor['transaction_date_time']);
                    $donor_id = $donor['donor_id'];
                    $email = $donor['email'];
                    $amount = $donor['amount'];
                    $preapproval_total_amount_display = $donor['preapproval_total_amount'];
                    $pay_fees = $donor['pay_fee'];

                 

                   
                    if (($donor['preapproval_status'] != "FAIL")) {
                        $total_recieve = $total_recieve + $donor['preapproval_total_amount'] - $pay_fees;

                        $total_amount = $total_amount + $amount;
                        $preapproval_total_amount = $preapproval_total_amount + $donor['preapproval_total_amount'];
                        $total_fees = $total_fees + $pay_fees;
                    }


                    $array = array('ON_HOLD' => PENDING,
                        'FAIL' => FAIL,
                        'SUCCESS' => SUCCESS
                    );
                    $preapproval_status = ($donor['preapproval_status'] != "") ? $array[$donor['preapproval_status']] : "";


                    $campaign_id = $donor['campaign_id'];
                    ?>

            <tbody>
               <tr>
                    <td class="no-data"><?php echo $campaign_funder_date_time;?></td>
                     <td class="no-data"><?php echo $campaign_funder_name; ?></td>
                      
                     <td class="no-data"><?php echo set_currency($preapproval_total_amount_display); ?></td>
               
                        <td class="no-data"><?php echo $preapproval_status; ?></td>
                                                              
                </tr>
             <?php }

             } ?>

                  <script>
                document.getElementById('total_recieve_amnt').innerHTML = '<?php echo set_currency($preapproval_total_amount)."-".set_currency($total_fees)."=".set_currency($total_recieve);?>';
            </script>
                      
            </tbody>

        </table>
    </div>
             </div>
             <div role="tabpanel" class="tab-pane" id="widgets">
              <?php
    if ($click_share == 'yes') {
        ?>
             <div class="row widget">
             <div class="explore-campaigns col-md-5">
             <?php
                $data['search_property'] = $properties;
                  
                $this->load->view('default/home/common_card_widget', $data);
                ?>
          </div>
          <div class="col-md-6 col-sm-6">
                <div class="embed-code">
                    <h4>Embed this card in your website or blog</h4>

                    <p>Copy the code below and paste it into your website or blog.</p>
                   <textarea name="area1" rows="7" onclick="javascript:this.select();" class="embed-textarea"
                              id="area1" readonly="readonly">
                        <div id='widgets'></div>
                        <script src='<?php echo site_url('property/widgets_code/s/red/' . $property_id); ?>'
                                type='text/javascript'></script>
                    </textarea>
                </div>
            </div>
             </div>
               <?php } else { ?>
        <div class="live-to-view"><?php echo MAKE_YOUR_PROJECT_LIVE_TO_VIEW_AND_COPY_WIDGET_CODE; ?></div>
    <?php } ?>
             </div>
             <div role="tabpanel" class="tab-pane" id="share">
             <?php
    $campaign_share_link = site_url('properties/' . $property_url . '/' . $property_id);
    $campaign_share_name = $property_address;

    $facebook_share_link = 'http://www.facebook.com/share.php?u='.$campaign_share_link.'&amp;t='.$campaign_share_name.'';

    $twitter_share_link ='http://twitter.com/home?status='.$campaign_share_name.' '.$campaign_share_link.'';

    $google_share_link ='https://plusone.google.com/_/+1/confirm?hl=en&url='.$campaign_share_link.'';

    $linkedin_share_link = 'https://www.linkedin.com/shareArticle?mini=true&url='.$campaign_share_link.'&title='.$campaign_share_name.'&summary='.$property_description.'&source=LinkedIn';

     $email_share_link = 'mailto:?subject='.$campaign_share_name.'&amp;body='.$property_description.'';
    ?>
    <?php
    if ($click_share == 'yes') {
        ?>
              <div class="co-share">
             <div class="sc-buttons row">
             <div class="col-md-12">
             <div class="col-md-4 ">
                    <div class="sc-fb">
                      <div class="social-text">
                        <a onclick="return popitup('<?php echo $facebook_share_link;?>')"
                   href="<?php echo $facebook_share_link;?>"
                   data-toggle="tooltip" data-placement="bottom">
                         <i class="fa fa-facebook fa-fw"></i>  Facebook </a></div>
                    </div>
                    </div>
                    <div class="col-md-4">
                    <div class="sc-tw">
                      <div class="social-text"> 
                      <a onclick="return popitup('<?php echo $twitter_share_link;?>')"
                   href="<?php echo $twitter_share_link;?>"
                   data-toggle="tooltip" data-placement="bottom"><i class="fa fa-twitter fa-fw"></i> Twitter </a></div>
                    </div>
                    </div>
                    <div class="col-md-4">
                    <div class="sc-in">
                      <div class="social-text"><a onclick="return popitup('<?php echo $linkedin_share_link;?>')"
                   href="<?php echo $linkedin_share_link;?>"
                   data-toggle="tooltip" data-placement="bottom"><i class="fa fa-linkedin fa-fw"></i>  Linkedin</a></div>
                      </div>
                      </div>
                      </div>
                       <div class="col-md-12">
                      <div class="col-md-4">
                      <div class="sc-gp">
                      <div class="social-text">
                      <a onclick="return popitup('<?php echo $google_share_link;?>')"
                   href="<?php echo $google_share_link;?>"
                   data-toggle="tooltip" data-placement="bottom"><i class="fa fa-google-plus fa-fw"></i> Google Plus</a></div>
                     </div>
                     </div>
                     <div class="col-md-4">
                      <div class="sc-mail">
                      <div class="social-text"><a href="<?php echo $email_share_link;?>" onclick="return popitup('<?php echo $email_share_link;?>')"
><i class="fa fa-envelope fa-fw"></i>Mail</a></div>
                     </div>
                     </div>
                     
                    </div></div>
                    </div>
                    <?php } else { ?>
                      <div class="col-md-12">
        <div class="live-to-view"><?php echo MAKE_YOUR_PROJECT_LIVE_TO_SHARE_ON_SOCIAL; ?></div>
        </div>
    <?php } ?>
             </div>
             <div role="tabpanel" class="tab-pane" id="followers">
             	<div class="campaign-followers">
    <ul class="followers-list clearfix">
     <?php

        if (is_array($follower)) {
            foreach ($follower as $property_follower) {
                $follower_name = $property_follower['user_name'] . ' ' . $property_follower['last_name'];
                $follower_date = $property_follower['property_follow_date'];
                $follower_image = $property_follower['image'];
                $property_follow_user_id = $property_follower['property_follow_user_id'];
                $property_user_id = $property_follower['user_id'];
               
                if($property_follower['profile_slug'] != '')
                {
                   $profile_slug = $property_follower['profile_slug'];
                }
                else
                {
                  $profile_slug = $property_follow_user_id;
                }

                if ($follower_image != '') {
                    if (file_exists(base_path() . 'upload/user/user_medium_image/' . $follower_image)) {
                        $propertyfollowersrc = base_url() . 'upload/user/user_medium_image/' . $follower_image;
                    } else {
                        $propertyfollowersrc = base_url() . 'upload/user/user_medium_image/no_man.jpg';
                    }
                } else {
                    $propertyfollowersrc = base_url() . 'upload/user/user_medium_image/no_man.jpg';
                }
                ?>
    	<li>
        <div class="fi">
             <a href="<?php echo site_url('user/' . $profile_slug); ?>"><img src="<?php echo $propertyfollowersrc;?>"></a>
             
             </div>
             <a href="<?php echo site_url('user/' . $profile_slug); ?>"><?php echo $follower_name;?></a>
              <span class="fi-date"><?php echo dateformat($follower_date); ?></span>
                <div id="follow_unfollow<?php echo $property_user_id; ?>">
             
             
                            <?php
                            if ($this->session->userdata('user_id')) {
                                if ($this->session->userdata('user_id') != $property_user_id) {
                                    $chk_follower = check_is_follower($property_user_id, $this->session->userdata('user_id'));

                                    if ($chk_follower) {
                                        ?>
                                         <span class="follow-btn">
                                        <a href="javascript://" class="btn btn-primary btn-small"
                                           id="followme<?php echo $property_user_id; ?>"
                                           onclick="unfollowuser(<?php echo $property_user_id; ?>)"
                                           onmouseover="set_followingtext(<?php echo $property_user_id; ?>,0);"
                                           onmouseout="set_followingtext(<?php echo $property_user_id; ?>,1);"><i
                                                class="fa fa-heart"></i> <?php echo FOLLOWING; ?></a></span>
                                    <?php
                                    } else {
                                        ?>
                                         <span class="follow-btn">
                                        <a href="javascript://" class="btn btn-primary btn-small"
                                           id="followme<?php echo $property_user_id; ?>"
                                           onclick="followuser(<?php echo $property_user_id; ?>)"><i
                                                class="fa fa-heart"></i> <?php echo FOLLOW; ?></a>
                                                </span>
                                    <?php
                                    }

                                    ?>



                                <?php } else { ?>
                                 <span class="follow-btn">
                                    <a href="javascript://" class="btn btn-primary btn-small"> <i
                                            class="fa fa-heart"></i> <?php echo FOLLOW; ?></a>
                                            </span>
                                <?php
                                }
                            }
                            ?>
            
              </div>
           </li>
        
     
           <?php
            }
        } else {
            ?>
            <li class="no-data"><?php echo NO_FOLLOWERS_FOUND; ?>.</li>
        <?php } ?>
           
          
    </ul>
    </div>
             </div>
             
                
            </div>
            </div>
            </div>
    </div>
    <input type="hidden" id="property_id"	 value="<?php echo $property_id;?>">  			</div>
    </section>
    
    <script src="<?php echo base_url();?>js/bootstrap-tabdrop.js"></script>
	<script>
	if (top.location != location) {
    top.location.href = document.location.href ;
  }
		$(function(){
			window.prettyPrint && prettyPrint();
      $('.nav-tabs:first').tabdrop();
      $('.nav-tabs:last').tabdrop({text: 'More options'});
		});
	</script>
  <script src="<?php echo base_url();?>js/redactor.min.js"></script>

	<script type="text/javascript">
	$(document).ready(
		function()
		{
			$('#redactor').redactor();
		}
	);
	$("#com-rep-icon").click(function () {
        $("#reply").show();
		  });

$("#cancel-reply").click(function () {
        $("#reply").hide();
		  });
$("#com-edit-icon").click(function () {
        $("#cancel-edit").show();
		  });

$("#cancel-edit").click(function () {
        $("#cancel-edit").hide();
		  });
 
	
	</script>

<script src="<?php echo base_url();?>js/highcharts.js"></script>
	 <script src="<?php echo base_url();?>js/jquery.nicescroll.js"></script>
      <script src="<?php echo base_url();?>js/progress-bar.js"></script>
       <script src="<?php echo base_url();?>js/raphael.js"></script>
   <script type="text/javascript">
   $(document).ready(
function() {
$(".table-responsive").niceScroll({cursorcolor:"#E1E1E1"});
}


);

function show_textarea(id, type) {
    //alert(id);
    document.getElementById("reply_" + id).style.display = "block";
//  alert(type);
    if (type == 'edit') {
        //alert('edit');
        //commentaction(id,'edit');
        var comment_value = document.getElementById("comment_" + id).value;
        var comment_area = document.getElementById("reply_comment_" + id).innerHTML = comment_value;
        //var comment_area=comment_value;
        //alert(comment_area);
        document.getElementById("edit_comment_" + id).innerHTML = 'Edit Comment';
    }
}
function hide_textarea(id) {
    document.getElementById("reply_" + id).style.display = "none";
}


function commentaction(id, type, type1) {

    var property_id_value = document.getElementById("property_id").value;
    var comment_value = document.getElementById("comment_" + id).value;
    var comment_type = document.getElementById("comment_type_" + id).value;
    var comment_ip_value = document.getElementById("comment_ip_" + id).value;
    var comment_user_id_value = document.getElementById("comment_user_id_" + id).value;
    var comment_reply_value = document.getElementById("reply_comment_" + id).value;
    //alert(comment_reply_value);

    if (type == 'delete') {
        var delcomment = confirm("<?php echo DELETE_COMMENT_CONFIRMATION;?>");
    }

    var but = document.getElementById('edit_comment_' + id).innerHTML;
    //alert(but);

    if (but == 'Edit Comment') {
        var mapdata = {
            "comment_id": id,
            "comment": comment_reply_value,
            "type": type,
            "property_id": property_id_value,
            "comment_reply": comment_reply_value,
            "comment_ip": comment_ip_value,
            "comment_user_id": comment_user_id_value,
            "type1": type1
        };

        var getStatusUrl = '<?php echo site_url('property/commentaction')?>';

        $.ajax({
            url: getStatusUrl,
            dataType: 'json',
            type: 'POST',
            timeout: 99999,
            global: false,
            data: mapdata,
            //data:{'perk[]':mapdata},
            success: function (data) {
                //alert(data);

                if (data.msg.error != "") {
                    //alert(data.msg.type);
                    $("#success_comment").hide();
                    $("#error_comment").show();
                    $("#error_comment").html(data.msg.error);
                    return;

                    /*$(".success"+data.msg.comment_id).hide();
                     $(".error"+data.msg.comment_id).show();
                     $(".error"+data.msg.comment_id).html(data.msg.error);
                     return;*/
                }

                if (data.msg.success != "") {
                    //alert(data.msg.type);
                    //alert(data.msg.success);
                    $("#success_comment").show();
                    $("#error_comment").hide();
                    $("#success_comment").html(data.msg.success);
                    $("#success_comment").fadeOut(5000);

                    /*alert(data.msg.success);
                     $(".success"+data.msg.comment_id).show();
                     $(".error"+data.msg.comment_id).hide();
                     $(".success"+data.msg.comment_id).html(data.msg.success);*/
                    updatecomments();
                }

                $("#success_comment").fadeOut(5000);
                $("#error_comment").fadeOut(5000);

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {

            }
        });

        //alert('rahul');   
    }

    else {
        if (type == 'delete') {
            if (delcomment) {
                var mapdata = {
                    "comment_id": id,
                    "comment": comment_value,
                    "type": type,
                    "property_id": property_id_value,
                    "comment_reply": comment_reply_value,
                    "comment_ip": comment_ip_value,
                    "comment_user_id": comment_user_id_value
                };
            }
        }
        if (type == 'approved' || type == 'decline' || type == 'spam') {
            if (type == 'approved') {
                var approvecnfrm = confirm("<?php echo ARE_YOU_SURE_YOU_WANT_TO_APPROVE_THIS_COMMENT;?>");
            }
            if (type == 'decline') {
                var declinecnfrm = confirm("<?php echo ARE_YOU_SURE_YOU_WANT_TO_DECLINE_THIS_COMMENT;?>");
            }
            if (type == 'spam') {
                var spamcnfrm = confirm("<?php echo ARE_YOU_SURE_YOU_WANT_TO_SPAM_THIS_COMMENT;?>");
            }

            if (approvecnfrm || declinecnfrm || spamcnfrm) {
                var mapdata = {
                    "comment_id": id,
                    "comment": comment_value,
                    "type": type,
                    "property_id": property_id_value,
                    "comment_reply": comment_reply_value,
                    "comment_ip": comment_ip_value,
                    "comment_user_id": comment_user_id_value
                };
            }
        }
        if (type == 'reply') {
            var mapdata = {
                "comment_id": id,
                "comment_type": comment_type,
                "comment": comment_reply_value,
                "type": type,
                "property_id": property_id_value,
                "comment_reply": comment_reply_value,
                "comment_ip": comment_ip_value,
                "comment_user_id": comment_user_id_value
            };
        }

        var getStatusUrl = '<?php echo site_url('property/commentaction')?>';

        $.ajax({
            url: getStatusUrl,
            dataType: 'json',
            type: 'POST',
            timeout: 99999,
            global: false,
            data: mapdata,
            //data:{'perk[]':mapdata},
            success: function (data) {

                //alert(data);

                if (data.msg.error != "") {

                    //alert(data.msg.error);
                    $("#success_comment").hide();
                    $("#error_comment").show();
                    $("#error_comment").html(data.msg.error);
                    return;
                }

                if (data.msg.success != "") {
                    // alert(data.msg.success);
                    $("#success_comment").show();
                    $("#error_comment").hide();
                    $("#success_comment").html(data.msg.success);
                    $("#success_comment").fadeOut(5000);
                    updatecomments();

                }


            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {

            }
        });

    }
}

function updatecomments() {
    var property_id_value = document.getElementById("property_id").value;
    var mapdata = {"property_id": property_id_value};

    // var mapdata = "'perk_title=' + perk_title_value" ;
    //var mapdata_description = 'perk_description=' + perk_description_value;

    var getStatusUrl = '<?php echo site_url('property/ajax_updatecomments')?>';//alert(getStatusUrl)
    $.ajax({
        url: getStatusUrl,
        dataType: 'text',
        type: 'POST',
        timeout: 99999,
        global: false,
        data: mapdata,
        //data:{'perk[]':mapdata},
        success: function (data) { //alert(data);
            document.getElementById("comments_update").innerHTML = data;

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {

        }
    });

}
 function followuser(id) {
        if (id == '') {
            return false;
        }
        //alert(id);
        var strURL = '<?php echo base_url() . 'follower/user_follower/';?>' + id;
       $.ajax({
        url: strURL,
        type: "post",
        success: function (response) {
            
            var follow_text = $.trim(response);
           // you will get response from your php page (what you echo or print)                 
             if(follow_text == 'Follow')
             {

                     var update_html = '<span class="follow-btn"><a href="javascript:void(0)" class="btn btn-primary btn-small" id="followme' + id + '" onclick="unfollowuser(' + id + ')" onmouseover="set_followingtext(' + id + ',0);" onmouseout="set_followingtext(' + id + ',1);"><i class="fa fa-heart"></i> <?php echo FOLLOWING; ?></a></span>';
                    $("#follow_unfollow"+id).html(update_html);
             }
         },
       
        });


    }
    function unfollowuser(id) {
        $('#followme').removeAttr('onmouseover').removeAttr('onmouseout');
        //alert(id);
        if (id == '') {
            return false;
        }
        var strURL = '<?php echo site_url('follower/user_unfollower');?>/' + id;
         $.ajax({
        url: strURL,
        type: "post",
        success: function (response) 
        {
             var follow_text = $.trim(response);
              if(follow_text == 'unfollow')
             {
                   var update_html = '<span class="follow-btn"><a href="javascript:void(0)" class="btn btn-primary btn-small" id="followme' + id + '"  onclick="followuser(' + id + ')"><i class="fa fa-heart"></i> <?php echo FOLLOW; ?></a></span>';
                    //savefollowerlist();

                     $("#follow_unfollow"+id).html(update_html);
               }
         },
       
        });


    }
    function set_followingtext(id, set) {
        // alert(id);
        if (set == 0) {
            document.getElementById("followme" + id).innerHTML = '<i class="fa fa-heart-o"></i> <?php echo UNFOLLOW; ?>';
        }
        else {
            document.getElementById("followme" + id).innerHTML = '<i class="fa fa-heart"></i> <?php echo FOLLOWING; ?>';
        }
    }

var editor = Array(), html = '';
var editornew = '';
var preid = '';
function createEditor(id) {


    var but = document.getElementById('ebutton_' + id).innerHTML;
    if (but == "Edit") {
        /*if ( editor )
         {
         //return;
         editor.destroy();
         editor = null;
         }*/
        document.getElementById('ebutton_' + id).innerHTML = 'Update Content';
        document.getElementById('dbutton_' + id).innerHTML = 'Cancel'
        // Create a new editor inside the <div id="editor">, setting its value to html
        html = document.getElementById("div_" + id).innerHTML;
        var config = {};//alert(id)
        editor[id] = CKEDITOR.appendTo("div_" + id, config, html);
    }
    else {

        document.getElementById('ebutton_' + id).innerHTML = 'Edit';
        document.getElementById('dbutton_' + id).innerHTML = 'Delete'
        var update = editor[id].getData();
        //alert(update);
        var property_id_value = document.getElementById('property_id').value;
        var request = jQuery.ajax({
            url: "http://equity.crowdfundingscript.com/equity/ajax_update_save",
            type: "POST",
            data: {
                updates: update,
                property_id: property_id_value,
                update_id: id
            },
            dataType: 'json',
            success: function (data)
                //complete: function(data, textStatus, errorThrown)
            {
                //alert(data);

                if (data.msg.error != "") {
                    //alert(data.msg.error);
                    $("#success_update").hide();
                    $("#error_update").show();
                    $("#error_update").html(data.msg.error);
                    return;
                }

                if (data.msg.success != "") {
                    //alert(data.msg.success);
                    $("#success_update").show();
                    $("#error_update").hide();
                    $("#success_update").html(data.msg.success);
                    updateUpdates();
                }
                $("#success_update").fadeOut(5000);
                $("#error_update").fadeOut(5000);

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                //alert('error');
            }
        });

        document.getElementById("div_" + id).innerHTML = editor[id].getData();
        updateUpdates();

    }

}
function removeEditor(id, action) {
    var but = document.getElementById('dbutton_' + id).innerHTML;
    var update_value = document.getElementById("update_" + id).value;
    var property_id_value = document.getElementById("property_id").value;
    //alert(update_value);
    //var update = editor[id].getData();
    //alert(update);

    if (but == "Cancel") {
        document.getElementById('ebutton_' + id).innerHTML = '<?php echo EDIT;?>';
        document.getElementById('dbutton_' + id).innerHTML = '<?php echo DELETE;?>'
        document.getElementById('addnew_update').innerHTML = "<?php echo ADD_NEW; ?>"
        editor[id].destroy();
        editor[id] = null;
    } else {
        var delupdate = confirm("<?php echo UPDATE_DELETE_CONFIRMATION;?>");
        //alert(id);
        if (delupdate) {

            var request = jQuery.ajax({
                url: '<?php echo site_url('property/ajax_update_save')?>',
                type: "POST",
                data: {
                    updates: update_value,
                    update_id: id,
                    property_id: property_id_value,
                    type: action
                },
                dataType: 'json',
                success: function (data)
                    //complete: function(data, textStatus, errorThrown)
                {
                    //alert(data);

                    if (data.msg.error != "") {
                        //alert(data.msg.error);
                        $("#success_update").hide();
                        $("#error_update").show();
                        $("#error_update").html(data.msg.error);
                        return;
                    }

                    if (data.msg.success != "") {
                        //alert(data.msg.success);
                        updateUpdates();
                        $("#success_update").show();
                        $("#error_update").hide();
                        $("#success_update").html(data.msg.success);
                        $("#success_update").fadeOut(5000);

                    }

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    //alert('error');
                }


            });
        }
        else {
        }
        //ajax call function same as addd

    }
}


function createEditornew(type, id) {
    
 
    var update_button = document.getElementById('addnew_update').innerHTML;
    var update_button2 = document.getElementById('ebutton_new').innerHTML


    if (preid) {
        document.getElementById('updatebox_' + preid).style.display = 'block';
    }


    if (type == "new") {
        if (update_button == "<?php echo ADD_NEW; ?>") {

            if (editornew == '') {
                //alert(123)
            }
            else {
                //alert(321)
                //editornew.destroy();
                $('#div_new').redactor('core.destroy');
            }
            document.getElementById('ebutton_new').innerHTML = '<?php echo SAVE;?>';
            document.getElementById('addnew_box').style.display = 'block';
            document.getElementById('addnew_update').innerHTML = '<?php echo CLOSE;?>'
            html = '';
            var config = {};
            editornew = $('#div_new').redactor({
                 imageUpload: '<?php echo base_url(); ?>editor/scripts/image_upload.php'
                });
        }
        if (update_button == '<?php echo CLOSE;?>') {
            $('#div_new').redactor('core.destroy');
            editornew = '';
            document.getElementById('addnew_box').style.display = 'none';
            $("#error_update").hide();
            document.getElementById('addnew_update').innerHTML = "<?php echo ADD_NEW; ?>";
        }
        PopupClose();
    }

    else if (type == "add") {
        PopupOpen();
        if (update_button2 == '<?php echo UPDATE;?>') {
            //alert('hello');
            var property_id_value = '<?php echo $property_id;?>';
            var update_id_value = document.getElementById('update_id_value').innerHTML;

            // var update = $('textarea').val();

            var update = document.getElementById('div_new').innerHTML;
            var request = jQuery.ajax({
                url: '<?php echo site_url('property/ajax_update_save')?>',
                type: "POST",
                data: {
                    updates: update,
                    property_id: property_id_value,
                    update_id: update_id_value
                },
                //dataType: "html"
                dataType: 'json',
                success: function (data)
                    //complete: function(data, textStatus, errorThrown)
                {
                    //alert(data);

                    if (data.msg.error != "") {
                        //alert(data.msg.error);
                        $("#success_update").hide();
                        $("#error_update").show();
                        $("#error_update").html(data.msg.error);
                        PopupClose();
                        return;
                    }

                    if (data.msg.success != "") {
                        updateUpdates();
                        //alert(data.msg.success);
                        $("#success_update").show();
                        $("#error_update").hide();
                        $("#success_update").html(data.msg.success);
                        $("#success_update").fadeOut(5000);
                        $('#div_new').redactor('core.destroy');
                        editornew = '';
                        //alert(editornew);

                        document.getElementById('updatebox_' + id).style.display = 'block';
                    }
                    PopupClose();

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    //alert('error');
                }

            });

            //call ajax function


        }
        else {
            var property_id_value = '<?php echo $property_id;?>';
            var update = $('textarea').val();

            var request = jQuery.ajax({
                url: '<?php echo site_url('property/ajax_update_save')?>',
                type: "POST",
                data: {
                    updates: update,
                    property_id: property_id_value,
                    update_id: id
                },
                //dataType: "html"
                dataType: 'json',
                success: function (data)
                    //complete: function(data, textStatus, errorThrown)
                {
                    //alert(data);

                    if (data.msg.error != "") {
                        //alert(data.msg.error);
                        $("#success_update").hide();
                        $("#error_update").show();
                        $("#error_update").html(data.msg.error);
                        PopupClose();
                        return;
                    }

                    if (data.msg.success != "") {
                        updateUpdates();
                        //alert(data.msg.success);
                        $("#success_update").show();

                        $("#error_update").hide();
                        $("#success_update").html(data.msg.success);
                        $("#success_update").fadeOut(5000);
                        $('#div_new').redactor('core.destroy');
                        editornew = '';
                        // alert(editornew);

                    }
                    PopupClose();

                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    //alert('error');
                }

            });
        }

    }


    else if (type == "edit") {


        if (editornew == '') {
            //alert(123)
        }
        else {
            //alert(321)
            $('#div_new').redactor('core.destroy');
        }
        document.getElementById('addnew_box').style.display = 'block';


        html = document.getElementById("div_" + id).innerHTML;
        document.getElementById('updatebox_' + id).style.display = 'none';
        $('.default').removeClass('hide_class');
        $('#updatebox_' + id).addClass('hide_class');


        var config = {};//alert(id)
        editornew = $('#div_new').redactor({
                 imageUpload: '<?php echo base_url(); ?>editor/scripts/image_upload.php'
             });
        document.getElementById('ebutton_new').innerHTML = '<?php echo UPDATE;?>'
        //  $('#ebutton_new').attr( 'onclick','createEditornew("edit")');
        document.getElementById('div_new').innerHTML = html;
        document.getElementById('update_id_value').innerHTML = id;
        //i=i+1;
      

    }
    else if (type == "cancel") {
        //editornew.destroy();
        $('#div_new').redactor('core.destroy');
        editornew = '';
        document.getElementById('addnew_box').style.display = 'none';
        document.getElementById('addnew_update').innerHTML = "<?php echo ADD_NEW; ?>";
        $('.default').removeClass('hide_class');
        $('#updatebox_' + id).addClass('hide_class');
        updateUpdates();
      
    }

    else {
        $('#div_new').redactor('core.destroy');
        editornew = '';
        document.getElementById('addnew_box').style.display = 'none';
        document.getElementById('addnew_update').innerHTML = "<?php echo ADD_NEW; ?>"
      
    }
}


function updateUpdates() {
    var property_id_value = '<?php echo $property_id;?>';
    var mapdata = {"property_id": property_id_value};

    // var mapdata = "'perk_title=' + perk_title_value" ;
    //var mapdata_description = 'perk_description=' + perk_description_value;

    var getStatusUrl = '<?php echo site_url('property/ajax_updateupdates')?>';//alert(getStatusUrl)
    $.ajax({
        url: getStatusUrl,
        dataType: 'text',
        type: 'POST',
        timeout: 99999,
        global: false,
        data: mapdata,
        //data:{'perk[]':mapdata},
        success: function (data) { //alert(data);
            document.getElementById("update_perk_list").innerHTML = data;
             PopupClose();

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {

        }
    });
}

function popitup(url) {
    newwindow = window.open(url, 'name', 'height=400,width=450');
    if (window.focus) {
        newwindow.focus()
    }
    return false;
}
   </script>
