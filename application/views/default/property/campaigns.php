 <section>
        <div class="db-head">
            <div class="container">
                <div class="page-title">
                    <h2>My Campaigns</h2>
                </div>
            </div>
        </div>
        <div class="grey-bg" id="section-area">         
            <div class="container">
                <div class="row">
                    <div class="col-md-3 db-mc-left">
                                      
<?php echo $this->load->view('default/dashboard_sidebar'); ?>
                        
                    </div>
                    <div class="col-md-9 db-mc-right">
           <ul class="db-mc-list">
                <?php 

       
           if($user_property_campaign) 
           {
                foreach($user_property_campaign as $property)
                {

                  $data['property'] = $property;
                  $data['property']['my_campaign'] = 'my_campaign';
                  $this->load->view('default/home/common_card', $data);
          
              }
            }
            else{
            ?>
                 
                 <li class="no_record_found">There are no campaign.</li>
              <?php } ?>
                 
                  
            </ul>
                        
                </div>
            </div>
        </div>
    </section>

    <script type="text/javascript">
      
    function remove_div(e, id) {
            if (confirm('<?php echo DELETE_PROJECT_CONFIRMATION;?>')) {
                if (id != '' || id != 0) {
                    $.ajax({
                        url: "<?php echo site_url('home/delete_property'); ?>/" + id,
                        xhrFields: {
                            withCredentials: true
                        }
                    });
                    $('#' + e).fadeOut(500, function () {

                        $(this).parent().remove();

                    });

                }

            }
        }

    </script>