
    <section>
        <div class="db-head">
            <div class="container">
                <div class="page-title">
                    <h2>Campaigns</h2>
                </div>
            </div>
        </div>
        <div class="grey-bg" id="section-area">         
            <div class="container">
                <div class="row">
                    <div class="col-md-3 db-mc-left">
                                       
<?php echo $this->load->view('default/dashboard_sidebar'); ?>
                        
                    </div>
                    <div class="col-md-9 db-mc-right">
           <ul class="db-mc-list">
           <?php 

       
           if($properties) 
           {
                foreach($properties as $property)
                {

                   $data['property'] = $property;
                  
                  $this->load->view('default/home/common_card', $data);
              }
            }
            ?>
                 
                 
                  
            </ul>
                        
                </div>
            </div>
        </div>
    </section>
   