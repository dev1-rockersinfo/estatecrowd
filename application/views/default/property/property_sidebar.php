	<?php 

	$segment1= $this->uri->segment(1);
	$segment2= $this->uri->segment(2);
	?>


	<div class="faqs-sidemenu">
		<ul>
			<li class="<?php if($segment1 == 'property' && $segment2 == 'add_property') echo "active";?>">
				<a href="<?php echo site_url('property/add_property/'.$id);?>">Property Detail</a>
			</li>
			<li class="<?php if($segment1 == 'property' && $segment2 == 'investment_summary') echo "active";?>">
				<a href="<?php echo site_url('property/investment_summary/'.$id);?>">Investment Summary</a>
			</li>
			<li class="<?php if($segment1 == 'property' && $segment2 == 'gallery') echo "active";?>">
			<a href="<?php echo site_url('property/gallery/'.$id);?>">Gallery</a>
			</li>
			<li class="<?php if($segment1 == 'property' && $segment2 == 'attachments') echo "active";?>">
			<a href="<?php echo site_url('property/attachments/'.$id);?>">Attachments</a>
			</li>
		</ul>
</div>

	<script src="<?php echo base_url();?>assets_front/scrolltofixed/jquery-scrolltofixed.js" type="text/javascript">
	</script>
	<script type="text/javascript">
		    $(document).ready(function() {
		        $('.faqs-sidemenu').scrollToFixed({
		                marginTop: $('#affix-nav').outerHeight(true) + 50,
		                limit: function() {
		                    var limit = $('#section-area').position().top+$('#section-area').outerHeight(true)-$('.faqs-sidemenu').outerHeight(true);
		                    return limit;
		                },
		                minWidth: 1000,
		                zIndex: 1,
		                fixed: function() {  },
		                dontCheckForPositionFixedSupport: true
		            });     
		    });   
		</script>