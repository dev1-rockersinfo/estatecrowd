<?php
$is_allow_updates = true;

$is_allow_updates = $is_property_owner;
if ($is_allow_updates == true) {
    ?>
    <div class="add-btn-div"><a href="javascript://" class="btn btn-primary" onClick="createEditornew('new');"
                                id="addnew_update"><?php echo ADD_NEW; ?></a></div>
<?php
}
?>
<div class="add-update-div" id="addnew_box">
    <div id="div_new">

    </div>

    <!--After Click on Add new div End-->
    <div class="m-t-md btngrp">
        <a href="javascript://" class="btn btn-primary" onClick="createEditornew('add');"
           id="ebutton_new"><?php echo SAVE; ?></a>
        <a href="javascript://" class="btn btn-primary gry" id="dbutton"
           onClick="createEditornew('cancel');"><?php echo CANCEL ?></a>
    </div>
    <div id="update_id_value" class="hidden"></div>
</div>
<div id="paging">
    <ul class="paging media-list in-media-list activity-list">

        <?php
        if (is_array($property_updates)) {
            foreach ($property_updates as $property_update) {
                $update_id = $property_update['update_id'];
                $property_update1 = SecureShowData($property_update['updates']);
                $date_added = $property_update['date_added'];
                $date_added = dateformat($property_update['date_added']);
                //echo $date_added;
                //echo $project_update1.'<br>';
                ?>
                <input type="hidden" name="update_<?php echo $update_id ?>" id="update_<?php echo $update_id ?>"
                       value="<?php echo $update_id; ?>"/>

                <li class="media" id="updatebox_<?php echo $update_id; ?>">
                    <div class="media-body">
                        <h4 class="media-heading"><?php echo $date_added; ?></h4>

                        <div id="div_<?php echo $update_id; ?>"><?php echo $property_update1; ?></div>
                        <div class="delete-comment">
                            <?php
                            if ($is_allow_updates == true) {
                                ?>
                                <a href="javascript://" title="<?php echo EDIT; ?>"
                                   onClick="createEditornew('edit',<?php echo $update_id; ?>)"
                                   id="ebutton_<?php echo $update_id; ?>"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="javascript://" data-toggle="tooltip" data-placement="bottom"
                                   title="<?php echo DELETE; ?>"
                                   onClick="removeEditor(<?php echo $update_id; ?>,'delete');"
                                   id="dbutton_<?php echo $update_id; ?>"><i class="glyphicon glyphicon-trash"></i></a>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                </li>
            <?php
            }
        } else {
            ?>
            <li class="no-data">
                <?php echo THERE_IS_NO_UPDATES_FOR_THIS_PROJECT; ?>
            </li>
        <?php } ?>
    </ul>
    <div class="pro-pagination"></div>
</div>
