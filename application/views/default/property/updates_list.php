<?php
if ($update_on_my_property > 0) 
{
    $count=6;
    foreach ($update_on_my_property as $property_update) {

        $update_image = $property_update['image'];
        $update_text = SecureShowData($property_update['updates']);
        $update_date_added = getDuration($property_update['date_added']);

        

        ?>
        <li>
                                            
            <div class="updates-rt">
              <h3>Update #<?php echo $count;?></h3>
            </div>
           <div class="member-role"><?php echo $update_date_added; ?></div>
            <div class="update-text">
                <?php echo $update_text; ?>
            </div>
        </li>
    <?php
    $count++;
    }
    ?>
    <?php  if ($total_row_display <= 5) {
        $display = 'none';
    } else {
        $display = 'block';
    }
    ?>
    <li class="show-more more_updates" style="display:<?php echo $display; ?>"><a href="javascript:void(0)"
                                                                                  onclick="show_more_updates(<?php echo $offset + 5; ?>)">Show
            more updates</a></li>
<?php
} else {
    ?>
    <li class="no-data"><?php echo THERE_IS_NO_UPDATES_FOR_THIS_PROJECT; ?></li>
<?php
}
?>
