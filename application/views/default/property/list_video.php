<?php


if ($video_data) {
    foreach ($video_data as $video) {
        $property_id = $video['property_id'];
        $video_id = $video['id'];
        $media_video_title = $video['media_video_title'];
        $media_video_name = $video['media_video_name'];
        $media_video_desc = $video['media_video_desc'];
        $media_video_status = $video['status'];


        $image = $video['image'];
        if ($image != '') {
            $image = $image;
        } else {
            $image = base_url() . 'images/icon-video.png';
        }
        $status_tx = PRIVATEC;
        if (intval($media_video_status) == 1) {
            $status_tx = PUBLICC;
        }


        ?>

     

            <li>
                        <div class="comments-lt">
                       <img src="<?php echo $image; ?>" alt="">
                       <span class="pri-pub"><?php echo $status_tx; ?></span>
                        </div>
                         <div class="comments-rt">
                         <h3><?php echo $media_video_title; ?></h3>    
                         <p class="comment-text">
                          <?php echo $media_video_desc; ?>
                            </p>
                         <span class="delete">
                      
                          <a href="javascript:void(0)" rel="<?php echo $video_id; ?>" class="edit edit-video"><i class="fa fa-edit"></i></a>

                        <a href="javascript:void(0)" rel="<?php echo $video_id; ?>" class=" trs delete-video"><i class="fa fa-trash"></i></a>
                         </span>
                     
                         </div>
                        </li> 


    <?php
    }
}
?>
