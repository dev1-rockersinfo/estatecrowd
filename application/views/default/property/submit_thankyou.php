<?php
$sitename = $site_setting['site_name'];
?>
<section>
        <div class="f-ls-full-bg-img thenks-submit" id="section-area">           
            <div class="container">
                <div class="row">           
                    <div class="col-md-10 col-md-offset-1 ">  
            <i class="fa fa fa-smile-o"></i>

            <h2><?php echo YOUR_CAMPAIGN; ?> <strong>"<?php echo SecureShowData($property_address); ?>
                    "</strong> <?php echo HAS_BEEN_SUBMITTED_SUCCESSFULLY; ?> </h2>
            <h4><?php echo WHAT_NEXT; ?></h4>

            <p><?php echo SecureShowData($sitename); ?> <?php echo TEAM_WILL_REVIEW_DETAIL_OF_YOUR_CAMPAIGN_AND_YOU_WILL_BE_NOTIFIED; ?></p>
            <a href="<?php echo site_url('home/dashboard'); ?>"
               class="btn btn-primary text-uppercase m-t-md"><?php echo GO_TO_DASHBOARD; ?></a>
               </div>
               </div>
        </div>
    </div>
</section>
