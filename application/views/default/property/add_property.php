  <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>js/jquery.form.js"></script>

<?php

$attributes = array(
    'id' => 'cover_image_ajax',
    'name' => 'cover_image_ajax',
    'enctype' => 'multipart/form-data',
    'class' => 'edit_project',
    'method' => 'post',
    'accept-charset' => 'UTF-8'
);
echo form_open_multipart('property/cover_image_ajax/' . $id, $attributes);
?>
<div class="sr-only" id="form_cover_image_ajax"></div>
</form>
    <section>
	  	<div class="db-head">
	      	<div class="container">
	      		<div class="page-title">
		        	<h2>Add Propety Details</h2>
		        </div>
	      	</div>
	  	</div>
	  	<div class="grey-bg" id="section-area">	  		
	      	<div class="container">
	      		<div class="row">
		      		<div class="col-md-3 faqs-left">
		      		<?php echo $this->load->view('default/property/property_sidebar');?>
		      		</div>
              <form action="<?php echo site_url('property/add_property/' . $id) ?>" method="post" accept-charset="utf-8"
      id="frm_project" name="frm_project" enctype="multipart/form-data">
		      		<div class="col-md-9 add-pro-right">
           			<div class="row dec-form">
                    <div id="property-detail" class="propety-area clearfix">
                    <div class="form-heading">
                    <h3>Property Detail</h3>
                    </div>
                    <div class="property-detail-body row">
                  	<div class="form-group col-md-6">                  
           					 <label for="">Property Address</label>
            				 <input type="text" name="property_address"  id="property_address" class="form-control" 
                     value="<?php echo $property_address;?>"> 
                      <span class="create_step_error"> <?php  echo form_error('property_address');?></span>
                      <div id="locationMap" class="gmap3 google-map"></div>
                      
                     </div>
                    <div class="form-group col-md-6">
             					 <label for="">Estate Crowd URL</label>
              				 <input type="text" id="property_url" class="form-control" name="property_url" placeholder="<?php echo base_url();?>property"   value="<?php echo $property_url;?>" >  
                       <span class="create_step_error"> <?php if ($check_url != '') { echo $check_url;
                } else {
                    echo form_error('property_url');
                } ?></span>
                     </div>
                    <div class="form-group col-md-12">
   					            <label>Property Description</label>    				
                   	<textarea id="redactor" name="property_description" name="content">	<?php echo $property_description;?></textarea>   
                    <span class="create_step_error"> <?php  echo form_error('property_description');?></span>                  
                     </div>

                    <div class="form-group col-md-6 add-pro-select">
                     <label for="property-type">Property Type</label>
                    <div class="select-search" id="searchType">

                    <input type="text" name="property_type" id="property_type" value="<?=$property_type?>" class="form-control">

                 <?php /*?>  <select class="selectpicker" name="property_type">
                 
                      <?php 
                  if($types){
                      echo '<option value="">Select</option>';
                      foreach ($types as $type) { 
                          $select = $property_type==$type['name']?'selected=selected':'';
                          ?>
                          <option value="<?=$type['name']?>" <?=$select?> ><?=$type['name']?></option>;  
                      <?php
                      }
                  }
                  ?>                
                  </select> <?php */?>

                   <span class="create_step_error"> <?php  echo form_error('property_type');?></span>
              
                </div>                     
               </div>

               <div class="form-group col-md-6 add-pro-select">
                   <label for="property-type">Property Sub Type</label>
                   <div class="select-search" id="searchType" >

                    <input type="text" name="property_sub_type" value="<?=$property_sub_type?>" id="property_sub_type" class="form-control">
                   <?php /*?><select class="selectpicker" name="property_sub_type">
                    <?php 
                      if($sub_types){
                          echo '<option value="" selected>Select</option>';
                          foreach ($sub_types as $sub_type) { 
                            $select = $property_sub_type==$sub_type['name']?'selected=selected':'';
                              ?>
                              <option value="<?=$sub_type['name']?> " <?=$select?> ><?=$sub_type['name']?></option>;  
                          <?php
                          }
                      }
                      ?>
              
                  </select> <?php */?>

                  <span class="create_step_error"> <?php  echo form_error('property_sub_type');?></span>
              
                </div>                     
                </div>   


                    <div class="form-group col-md-6 add-pro-select">   					
                     <div class="row">		

                <div class="select-search col-md-4" id="searchType ">
                 <label for="">Bedrooms </label>   
                  <select class="selectpicker" name="bedrooms">
                   <option value="1" data-tokens=""  <?php if($bedrooms == 1){ echo 'selected=selected';} ?>>1 </option>
                   <option value="2" data-tokens=""  <?php if($bedrooms == 2){ echo 'selected=selected';} ?>>2 </option>
                   <option value="3" data-tokens=""  <?php if($bedrooms == 3){ echo 'selected=selected';} ?>>3 </option>
                   <option value="4" data-tokens=""  <?php if($bedrooms == 4){ echo 'selected=selected';} ?>>4 </option>
                   <option value="5" data-tokens="five"  <?php if($bedrooms == 5){ echo 'selected=selected';} ?>>5+</option>             
                  </select>    
                  <span class="create_step_error"> <?php  echo form_error('bedrooms');?></span>      
                </div> 
                 <!--   <div id="Bedroom-Inp" class="col-md-2">  
                   <input class="form-control"></div> -->              
                  <div class="select-search col-md-4" id="searchType">
                    <label for="">Bathrooms </label>   
                  <select class="selectpicker" name="bathrooms">
                   <option value="1" data-tokens=""  <?php if($bathrooms == 1){ echo 'selected=selected';} ?>>1 </option>
                   <option value="2" data-tokens=""  <?php if($bathrooms == 2){ echo 'selected=selected';} ?>>2 </option>
                   <option value="3" data-tokens=""  <?php if($bathrooms == 3){ echo 'selected=selected';} ?>>3 </option>
                   <option value="4" data-tokens=""  <?php if($bathrooms == 4){ echo 'selected=selected';} ?>>4 </option>
                   <option value="5" data-tokens="five"  <?php if($bathrooms == 5){ echo 'selected=selected';} ?>>5+</option>                 
                  </select>
                  <span class="create_step_error"> <?php  echo form_error('bathrooms');?></span>
              
                </div> <div class="select-search col-md-4" id="searchType">
                  <label for="">Cars </label>   
                  <select class="selectpicker" name="cars">
                     <option value="1" data-tokens=""  <?php if($cars == 1){ echo 'selected=selected';} ?>>1 </option>
                   <option value="2" data-tokens=""  <?php if($cars == 2){ echo 'selected=selected';} ?>>2 </option>
                   <option value="3" data-tokens=""  <?php if($cars == 3){ echo 'selected=selected';} ?>>3 </option>
                   <option value="4" data-tokens=""  <?php if($cars == 4){ echo 'selected=selected';} ?>>4 </option>
                   <option value="5" data-tokens="five"  <?php if($cars == 5){ echo 'selected=selected';} ?>>5+</option>                
                  </select>
                  <span class="create_step_error"> <?php  echo form_error('cars');?></span>
              
                </div>
                		</div>          
                     </div>
                      <div class="col-md-6 add-pro-select">
             <label for=""> Investment Range</label>
            <div class="row form-group">
            <div class="col-md-6 form-group ">
            <input type="text" id="public-field" name="min_property_investment_range" value="<?php echo $min_property_investment_range;?>" class="form-control" placeholder="Min">  

            <span class="create_step_error"> <?php  echo form_error('min_property_investment_range');?></span>
                 </div>
            <div class="col-md-6 form-group ">
            <input type="text" id="public-field" name="max_property_investment_range" value="<?php echo $max_property_investment_range;?>" class="form-control" placeholder="Max"> 
            <span class="create_step_error"> <?php  echo form_error('max_property_investment_range');?></span>
                 </div>
            </div>
            </div>

           <div class="form-group col-md-6 browse-upload-btn" id="form_cover_photo_image">
             <label for="">Upload Cover Photo</label>
          <input type="file" class="filestyle form-control" id="cover_image" name="cover_image" data-buttonName="btn-primary" >
            <span class="create_step_error image_error" ><?php echo $cover_image_error; ?></span>              
                     
                     </div>
                      <div class="form-group col-md-6 cvr-thumb-img">          
                     <?php
                                if (is_file("upload/property/small/" . $cover_image)) {
                                    if ($cover_image) {
                                        ?>
                                        <img
                                            src="<?php echo base_url() ?>upload/property/small/<?php echo $cover_image; ?>"
                                            id="cover_ajax_image"/>
                                    <?php } else { ?>
                                        <img src="<?php echo base_url() ?>upload/property/small/no_img.jpg"
                                             id="cover_ajax_image"/>
                                    <?php
                                    }
                                } else {
                                    ?>
                                    <img src="<?php echo base_url() ?>upload/property/small/no_img.jpg"
                                         id="cover_ajax_image"/>
                                <?php } ?>                
                     </div>
               
                     <div class="form-group col-md-6">
   					 <label for="">Propery Size (Square metres )</label>
    				 <input type="text"   id="public-field" class="form-control " name="property_size" value="<?php echo $property_size;?>">    
             <span class="create_step_error"> <?php  echo form_error('property_size');?></span>              
                     
                     </div>
                     <div class="form-group col-md-6">
   					 <label for="">Lot Size (Square metres )</label>
    				 <input type="text"   id="public-field" class="form-control " name="lotsize" value="<?php echo $lotsize;?>">                  
                      <span class="create_step_error"> <?php  echo form_error('lotsize');?></span>      
                     </div>
                     
        <div class="form-group col-md-6">
   			<label for=""> Year Built</label>
        <div class="input-group calen-icon">
        <input type="text" id="year_built" name="year_built" class="form-control" value="<?php echo $year_built;?>"  aria-describedby="basic-addon2">
        
        <span class="input-group-addon" id="basic-addon2"><i class="fa fa-calendar"></i> </span>

        </div> 
          <span class="create_step_error"> <?php  echo form_error('year_built');?></span>      
        </div>
                     
                    
                     
                     <div class="form-group col-md-6 add-pro-select">
   					 <label for=""> Suburb</label>
    				<div class="select-search" id="searchType">
                      <input type="text" class="form-control" name="property_suburb" id="property_suburb"
                               value="<?php echo $property_suburb; ?>" onkeyup="autotext_suburb();">

                <div id="autoc_suburb" class="in-autocomplete"></div>      
                  <span class="create_step_error"> <?php  echo form_error('property_suburb');?></span>      
                </div>        
                     
                     </div>
                    
                    <div class="form-group col-md-6 add-pro-select">
   					 <label for=""> Postcode</label>
    		

             <input type="text" class="form-control" name="property_postcode" id="property_postcode"
                               value="<?php echo $property_postcode; ?>" onkeyup="autotext();" >
                                <div id="autoc" class="in-autocomplete"></div>   

            <span class="create_step_error"> <?php  echo form_error('property_postcode');?></span> 
                     </div>
                     
        <div class="form-group col-md-6 add-pro-select">
   					 <label for=""> Country</label>
    				 <div class="select-search" id="searchType"  >
              <select class="selectpicker" data-live-search="true" name="country_id">

               <option value=""><?php echo SELECT_COUNTRY; ?></option>
                <?php
                if ($country_data) {
                    foreach ($country_data as $country) {
                        ?>
                        <option data-tokens="" value="<?php echo $country['country_id']; ?>" <?php if ($country_id == $country['country_id']) echo "selected='selected'"; ?>><?php echo $country['country_name']; ?></option>
                    <?php
                    }
                }
                ?>
                   
                  </select>
                <span class="create_step_error"> <?php  echo form_error('country_id');?></span> 
                </div>                  
                     
                     </div>
                     <div class="col-md-12 addpro-nxt ">
                     <input type="hidden" name="property_id" id="property_id" value="<?php echo $id;?>">
                      <button type="submit" class="btn btn-primary btn-small text-uppercase pull-right" > Next</button>
                      
                     </div>
                    </div>
			      		</div>
	      		</div>
	      	</div>

          </form>
	  	</div>
    </section>
    
    
  
  <script src="<?php echo base_url();?>assets_front/bootstrap-select/js/bootstrap-select.js"></script>
	 <script src="<?php echo base_url();?>js/redactor.min.js"></script>

	<script type="text/javascript">
    $(function () {
    $('#cover_image').on('change', function (event) {

        $('#form_cover_image_ajax').children().remove();
        $(this).prependTo('#form_cover_image_ajax').addClass('hidden-file-field');
        $('#cover_image_ajax').submit();

        $('#form_cover_photo_image').children().prependTo("#form_cover_image_ajax1");
    });
});

// bind form id and provide a simple callback function
if ((pic = jQuery('#cover_image_ajax')).length)
    pic.ajaxForm({
        dataType: 'json',
        beforeSend: function () {

            PopupOpen();
        },
        success: function (result) {
            //$("html, body").animate({ scrollTop: $('#add_cover').offset().top }, 1000);

            if (result.msg.success != '') {
                $("#cover_ajax_image").attr('src', result.image.path);
                $(".image_error").hide();
                $("#cover_photo_name").val(result.image.path);
                $("#property_id").val(result.msg.property_id);
            }
            if (result.msg.error != '') {
                $(".image_error").show();
                $(".image_error").html(result.msg.error);
            }
        },

        complete: function () {
            PopupClose();
        }
    });



	$(document).ready(
		function()
		{
			$('#redactor').redactor();
		
		}
	);


$(window).bind("load", function () {
    $('#dvLoading').fadeOut(2000);
    $('#mainbg_div').fadeIn(2000);
    $('.min_height').css('minHeight', 0);
    $(function () {
        $('#locationMap').gmap3();

        $('#property_address').autocomplete({
            source: function () {
                $("#locationMap").gmap3({
                    action: 'getAddress',
                    address: $(this).val(),
                    callback: function (results) {
                        if (!results) return;
                        $('#property_address').autocomplete(
                            'display',
                            results,
                            false
                        );
                    }
                });
            },
            cb: {
                cast: function (item) {

                    return item.formatted_address;
                },
               
            }


        });


        <?php if($property_address != ''){ ?>
        $('#locationMap').gmap3({
            action: 'addMarker',
            address: "<?php echo $property_address; ?>",
            map: {
                center: true,
                zoom: 12
            },
            marker: {
                options: {
                    draggable: true
                }
            }
        });
        <?php } ?>


    });

});

	</script>
    
 <script type="text/javascript" src="<?php echo base_url();?>js/bootstrap-datepicker.js"></script>  
        <script type="text/javascript">
		   $(document).on("click", "#searchType li a", function(){ 
		 
         		var element = $(this).data("tokens");
								
				if ( element=="five") {
						$("#Bedroom-Inp").css("display","block");
						//$("#searchType Bedrooms").hide();				
						
					}
					else  {
						
					}
        	});
    var date = new Date();
    var d = new Date();        
    d.setDate(date.getDate());
    $('#year_built').datepicker( {

          format: "yyyy",
          endDate: d,   
          
      });


					</script>

<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.slugify.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {

        $('#property_url').slugify('#property_address');
    });


 $("#autoc").hide();

    function autotext() {
        var xmlHttp;
        try {
            xmlHttp = new XMLHttpRequest();// Firefox, Opera 8.0+, Safari
        }
        catch (e) {
            try {
                xmlHttp = new ActiveXObject("Msxml2.XMLHTTP"); // Internet Explorer
            }
            catch (e) {
                try {
                    xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                catch (e) {
                    //alert("<?php //echo NO_AJAX;?>");
                    return false;
                }
            }
        }
        xmlHttp.onreadystatechange = function () {
            if (xmlHttp.readyState == 4) {
                document.getElementById('autoc').innerHTML = xmlHttp.responseText;

                if (document.getElementById('srhdiv_postcode')) {


                  $("#autoc").show();


                }
                else {
                   $("#autoc").hide();
                    
                }
            }
        }
        var text = document.getElementById('property_postcode').value;
        text = text.replace(/[^a-zA-Z0-9]/g, '');
        xmlHttp.open("GET", "<?php echo site_url('property/search_auto_postcode');?>/" + text, true);
        xmlHttp.send(null);
    }
    function selecttext(el) {

        var selectvalue = el.innerHTML;
       
        $("#property_postcode").val(selectvalue);
        $("#autoc").hide();
       
    }

    $("#autoc_suburb").hide();

    function autotext_suburb() {
        var xmlHttp;
        try {
            xmlHttp = new XMLHttpRequest();// Firefox, Opera 8.0+, Safari
        }
        catch (e) {
            try {
                xmlHttp = new ActiveXObject("Msxml2.XMLHTTP"); // Internet Explorer
            }
            catch (e) {
                try {
                    xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                catch (e) {
                    //alert("<?php //echo NO_AJAX;?>");
                    return false;
                }
            }
        }
        xmlHttp.onreadystatechange = function () {
            if (xmlHttp.readyState == 4) {
                document.getElementById('autoc_suburb').innerHTML = xmlHttp.responseText;

                if (document.getElementById('srhdiv')) {


                  $("#autoc_suburb").show();


                }
                else {
                   $("#autoc_suburb").hide();
                    
                }
            }
        }
        var text = document.getElementById('property_suburb').value;
        text = text.replace(/[^a-zA-Z0-9]/g, '');
        xmlHttp.open("GET", "<?php echo site_url('property/search_auto');?>/" + text, true);
        xmlHttp.send(null);
    }
    function selecttext_suburb(el) {

        var selectvalue = el.innerHTML;
       
        $("#property_suburb").val(selectvalue);
        $("#autoc_suburb").hide();
       
    }
</script>


<script type="text/javascript" src="http://code.jquery.com/jquery-migrate-1.0.0.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/map/gmap3.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/map/jquery-autocomplete.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/map/jquery-autocomplete.css"/>