<script type="text/javascript" src="<?php echo base_url();?>js/jquery.requestAnimationFrame.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>js/jquery.mousewheel.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>js/ilightbox.js"></script>

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/ilightbox.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>dark-skin/skin.css"/>
   
<?php 




$campaign_id = $properties['campaign_id'];
$lead_investor_id = $properties['lead_investor_id'];
$property_id = $properties['property_id'];
$property_user_id = $properties['user_id'];
$property_address = $properties['property_address'];
$campaign_user_id = $properties['campaign_user_id'];
$property_url = $properties['property_url'];
$status = $properties['status'];
$property_suburb = $properties['property_suburb'];
$bedrooms = $properties['bedrooms'];
$bathrooms = $properties['bathrooms'];
$get_investment = GetPropertyInvestmentRange($property_id);

$property_description = SecureShowData($properties['property_description']);
$investment_summary = SecureShowData($properties['investment_summary']);
$user_type = json_decode($properties['user_type']);
$cars = $properties['cars'];
$property_type = $properties['property_type'];


$property_sub_type = $properties['property_sub_type'];

$company_name = $properties['company_name'];
$min_property_investment_range = set_currency($properties['min_property_investment_range']);
$max_property_investment_range = set_currency($properties['max_property_investment_range']);
$campaign_units = $properties['campaign_units'];
$share_available = $campaign_units - $properties['campaign_units_get'];
$investment_close_date = $properties['investment_close_date'];
$date_added = $properties['date_added'];
$user_id = $properties['user_id'];
$min_investment_amount = $properties['min_investment_amount'];
$amount_get = $properties['amount_get'];
$amount_array = array('min_investment_amount'=>$min_investment_amount,'amount_get'=>$amount_get);
$percentage = GetCampaignPercentage($amount_array);
$user_name = $properties['user_name'];
$last_name = $properties['last_name'];
$per_share_amount=0;
if($campaign_units > 0)
{
    $per_share_amount = round($properties['min_investment_amount'] / $campaign_units);
}

$check_status = array(0, 1);
$click_share = 'no';

if (!in_array($status, $check_status)) {
    $click_share = 'yes';
}

if(is_file(base_path().'upload/property/large/'.$properties['cover_image']) && $properties['cover_image'] != '')
{
     $cover_image = base_url().'upload/property/large/'.$properties['cover_image'];
}
else
{
    $cover_image = base_url().'upload/property/large/no_img.jpg';
}


if(is_file(base_path().'upload/user/user_small_image/'.$properties['image']) && $properties['image'] != '')
{
     $image = base_url().'upload/user/user_small_image/'.$properties['image'];
}
else
{
    $image = base_url().'upload/user/user_small_image/no_man.jpg';
}


$is_allow_invest = is_allow_invest($campaign_user_id,$status,$investment_close_date);
$invest_allow = '';

if($is_allow_invest == true && $share_available <= $campaign_units )
{
  
  $invest_allow = 'yes';
}


?>

    <section>
      <div class="banner-section">
        <div class="image-wrapper">
            <div class="image" style="background-image:url('<?php echo $cover_image;?>')"></div>
            <div class="black-background"></div>
            <div class="overlay"></div>
             <?php 
            if($gallery)
            { ?>
            <div>

            <?php 
            $gallery_image = $gallery[0]['image'];
        $image_name = $gallery[0]['image_name'];

        if(is_file(base_path().'upload/gallery/'.$gallery_image) && $gallery_image != '')
        {
            $image_small_image = base_url().'upload/gallery/'.$gallery_image;
            $image_large_image = base_url().'upload/gallery/'.$gallery_image;
        }
        else
        {
            $image_small_image = $gallery_image;
            $image_large_image = '';
        }
            ?>
               <a href="<?php echo $image_large_image;?>" data-options="thumbnail: '<?php echo $image_small_image;?>',width:1920, height:1080" class="gallery_thumbnail gallery-nav" data-caption="<?php echo $image_name;?>" >
                  </a>
            </div>

            <?php } ?>
            <div class="container">
              <div class="campaign-name">
                <div class="left-sec">
                <a href="" class="user-thumb-sm"><img src="<?php echo $image;?>"></a></div>
                <div class="right-sec">
                  <h2 class="campaignName"><?php echo $property_address; ?>
                   

                   <span class="follow" id="follow_unfollow_property">

                  
                   <?php
            if ($this->session->userdata('user_id') > 0) {
                if ($this->session->userdata('user_id') != $campaign_user_id) {
                    $chk_property_follower = check_is_property_follower($property_id, $this->session->userdata('user_id'));
                    if ($chk_property_follower) {
                        ?>
                        <a href="javascript://" id="followme_property"
                           onclick="unfollowproperty(<?php echo $property_id; ?>)"
                           onmouseover="set_followingtext_property(0);" onmouseout="set_followingtext_property(1);">
                            <i class="fa  fa-heart-o"></i>
                            <?php echo FOLLOWING; ?>
                        </a>
                    <?php
                    } else {
                        ?>
                        <a href="javascript://" id="followme_property"
                           onclick="followproperty(<?php echo $property_id; ?>)">
                            <i class="fa  fa-heart"></i>
                            <?php echo FOLLOW; ?>
                        </a>
                    <?php
                    }

                }
            } else {
                $return_url = base64_encode('properties/'.$property_url);
                ?>
                <a href="<?php echo site_url('home/login/' . $return_url); ?>"  id="followme_property">
                    <i class="fa  fa-heart"></i>
                    <?php echo FOLLOW; ?>
                </a>
            <?php } ?>

                   </span>



                   </h2>
                  <span class="location"><?php echo $property_suburb; ?> </span>
                  <?php 
                     if(UserTypeExists(2,$campaign_user_id) == 1 || UserTypeExists(4,$campaign_user_id) == 1) {    
                      if($company_name != '') {
                    ?>  
                  <span class="company-name"><a href="javascript:void(0)"><?php echo $company_name;?></a></span>
                    <?php } 
                    }
                    ?>
                </div>
              </div>
            </div>
        </div>
      </div>
    </section>
    <section>
      <div class="tab-white-box">
        <div class="property-info-area">
          <div class="container">
            <ul class="property-info clearfix">
                <li>
                  <span class="info-num"><?php echo $bedrooms?></span>
                  <span class="icon-wrapper"><img src="<?php echo base_url();?>images/icon-bed.png"></span>
                  <span class="info-name">bedrooms</span>
                </li>
                <li>
                  <span class="info-num"><?php echo $bathrooms;?></span>
                  <span class="icon-wrapper"><img src="<?php echo base_url();?>images/icon-bath.png"></span>
                  <span class="info-name">bathrooms</span>
                </li>
                <li>
                  <span class="info-num"><?php echo $cars;?></span>
                  <span class="icon-wrapper"><img src="<?php echo base_url();?>images/icon-car.png"></span>
                  <span class="info-name">cars</span>
                </li>
              </ul>
              <ul class="propMore-info clearfix">
                <li class="clearfix">
                  <div class="icon-wrapper"><img src="<?php echo base_url();?>images/icon-property-type.png"></div>
                  <div class="info-name"><h3>Mixed-Use</h3><span><?php echo $property_sub_type;?>, <?php echo $property_type;?></span></div>
                </li>
                <li class="clearfix">
                  <div class="icon-wrapper"><img src="<?php echo base_url();?>images/icon-location.png"></div>
                  <div class="info-name"><h3>Location</h3>
                  <a href="http://maps.google.com/?q=<?php echo $property_address; ?>" target="_blank"><span>View Map</span></a></div>
                </li>
              </ul>
          </div>
        </div>
        <div class="">

        </div>
      </div>
   
   <div class="content-all">
   <div class="container">
   <div class="row">
   <div class="col-md-4 col-md-push-8">
   <div class="sidebar">
   <?php if($campaign_id > 0 and $campaign_id != '') {?>


      <div class="sidebar-top clearfix">
      <div class="progressBar barometer-added">
                      <div class="barometer disp-inline" data-progress="<?php echo $percentage; ?>" data-width="106" data-height="100" data-strokewidth="9" data-stroke="#656565" data-progress-stroke="#49d850"><label class="percent"><?php echo $percentage; ?>%<span>Invested</span></label>
                      </div>
                    </div>
          <div class="inv-title">
                <div class="invested-am"><?php echo set_currency($amount_get);?></div>
                <span>Invested</span>
            </div>
      </div>
     <div class="clearfix"></div>
     <ul class="fund">

        <li>
           <span> Investment Target</span>
            <strong><?php echo set_currency($min_investment_amount);?></strong>
        </li>
        <li>
            <span>Minimum Investment</span>
            <strong><?php echo $min_property_investment_range;?></strong>
        </li>
        <li>
            <span>Total Shares</span>
          <strong> <?php echo $campaign_units;?></strong>
        </li>
                
                    <li>
                 <span> Shares Available</span>
              <strong> <?php echo $share_available;?></strong>
            </li>
            <li>
                <span>Open Date</span>
                <strong><?php echo date("jS F, Y", strtotime($date_added));?></strong>
            </li>

                        <li>
            <span>Closing Date</span>
            <strong><?php echo date("jS M, Y", strtotime($investment_close_date));?></strong>
        </li>
      
    </ul>
    <div class="int-btn">
    <?php if($invest_allow == 'yes') {?>
      <a href="<?php echo site_url('investment/investor/'.$campaign_id);?>" type="button" class="btn btn-primary btn-lg btn-block text-uppercase">I am Interested</a>

      <?php } else { ?>
       <a href="javascript://" type="button" class="btn btn-primary btn-lg btn-block text-uppercase disabled">I am Interested</a>
      <?php } ?>
      </div>
 
<?php } else { ?>
     <div class="pro-inv-range">
   <span class="title">Investment Range</span>
   <span class="amount"><?php echo $min_property_investment_range;?>  - <?php echo $max_property_investment_range;?></span>
   <span class="unit">Per Unit</span>
   </div>
     

     
    <div class="int-btn">

    <?php 

      $user_verified_data = user_verfied_status($this->session->userdata('user_id'),$property_id,$property_url);

      echo $user_verified_data['button_link'];

    ?>

      
      </div>
      <?php } ?>
</div>
    <div class="panel panel-default ec-panel m-t-md">
  <div class="panel-heading">
  <h3 class="panel-title">Followers</h3>
  </div>
  <div class="panel-body property-follower">

    <div class="followers">
 <ul class="followers-list" id="propertyfollowers">
      <?php

if ($follower) {
    

            foreach ($follower as $follower_list) {
                $follower_slug = $follower_list['profile_slug'];
                $follower_image = $follower_list['image'];
                if (!is_file("upload/user/user_small_image/" . $follower_image)) {
                    $follower_image = 'no_man.jpg';
                }
                $follower_name = $follower_list['user_name'] . ' ' . $follower_list['last_name'];

                ?>
      <li>
        <div class="fi">
             <a href="<?php echo site_url('user/' . $follower_slug); ?>"><img src="<?php echo base_url() ?>upload/user/user_small_image/<?php echo $follower_image; ?>"></a>
             
             </div>
             <a href="<?php echo site_url('user/' . $follower_slug); ?>"><?php echo $follower_name;?></a>
              
           </li>
        <?php
            }
            ?>

     
  
      <?php } else { ?>
      <div class="no-record-found"> There is no follower for this property. </div>
      <?php } ?>

     </ul>
    </div>
    </div>
 
</div>
   
      </div>
      
    <div class="col-md-8 col-md-pull-4" >
   <div class="content-box">
 <div class="pro-tab">
   <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#investment">Investment</a></li>
       <?php
        if ($update_on_my_property) {
            ?>
       <li><a data-toggle="tab" href="#update-tab">Updates</a></li>
       <?php }  ?>
     <li><a data-toggle="tab" href="#investor">Investor</a></li>
       
      <li><a data-toggle="tab" href="#comment">Comment</a></li>
      <li><a data-toggle="tab" href="#attachments">Attachments</a></li>
     
     
  </ul>
</div>

   <div class="tab-content">
  <div id="investment" class="tab-pane fade in active">
  <div class="panel panel-default ec-panel">
  <div class="panel-heading">
    <h3 class="panel-title">Property Summary</h3>
  </div>
  <div class="panel-body">
 <div class="tab-content-text">
   <?php echo $property_description;?>
    <?php 
    $common_gallery ='';
    if(is_array($gallery))
    {
      if(is_array($property_video) ) { 

        $common_gallery = array_merge($gallery,$property_video);
      }
      else {
        $common_gallery = $gallery;
      }
    }
  
    if($common_gallery){
      $count=1;

    ?>
    <div class="galery"> 
    <?php 
    $total_gallery_Count = count($common_gallery);

    foreach ($common_gallery as $key=>$image_data) 
    {
      # code...
    

          # code...
        if($count == 5 && $total_gallery_Count > 4) $class_name='fifth';
        $display = '';
        if($count > 5)
        {
          $display = 'none';
        }

        
      
        if(checkNum($count) === true)
        {
          $class_name='first';
          if($count == 3 and $total_gallery_Count == 3)
          {
             $class_name = '';
          }
          else if($count == 1 and $total_gallery_Count == 1)
          {
             $class_name = '';
          }
          else if($count == 5 and $total_gallery_Count > 4)
          {
             $class_name = 'fifth';
          }
          else if($count == 3 and $total_gallery_Count > 4)
          {
             $class_name = 'third';
          }


        }     
        else
        {
             $class_name='second';
              if($count == 4 and $total_gallery_Count > 4)
              {
                 $class_name = 'fourth';
              }
        }
        $video_url='';
        if(isset($image_data['media_video_name']))
        {
          $video_url =$image_data['media_video_name'];
        }

        $gallery_image = $image_data['image'];
        $image_name = $image_data['image_name'];

        if(is_file(base_path().'upload/gallery/'.$gallery_image) && $gallery_image != '')
        {
            $image_small_image = base_url().'upload/gallery/'.$gallery_image;
            $image_large_image = base_url().'upload/gallery/'.$gallery_image;
        }
        else
        {
            $image_small_image = $gallery_image;
            $image_large_image = $video_url;
        }
       if($count == 4 || $count == 5 && $total_gallery_Count > 4  ){
     ?>
     <div class="combo">
     <?php }  ?>
      <div class="<?php echo $class_name;?>-img image-container" style="display: <?php echo $display;?>">
        
         <a href="<?php echo $image_large_image;?>" data-options="thumbnail: '<?php echo $image_small_image;?>',width:1920, height:1080" class="gallery_thumbnail" data-caption="<?php echo $image_name;?>" >
         <span class="overlay"></span>
                    <img src="<?php echo $image_small_image;?>" alt="<?php echo $image_name;?>" />
                    <?php  if($count >= 5){?>
                    <div class="after">+<br>
                          <?php echo $total_gallery_Count - 5 ?> More</div>
                          <?php } ?>
                  </a>
       
       </div>
   <?php  if($count == 2){?>
  <div class="pad"></div>
  <?php } ?>
   <?php  if($count == 4 && $total_gallery_Count > 4){?>

    <div class="pad1"></div>
  <?php } ?>
   <?php if($count == 4 || $count == 5 && $total_gallery_Count > 4){ ?>
   </div>
   <?php }  ?>
   
    <?php 
      $count++;

   } ?>
   </div>
   <?php } ?>
  </div>
    </div>
  </div>
  

 <div class="panel panel-default ec-panel">
  <div class="panel-heading">
    <h3 class="panel-title">Suburb Snapshot – North Ryde</h3>
  </div>
  <div class="panel-body">
<div class="tab-content-text">
 <span class="name">   North Ryde, NSW 2113</span>
 
<p>Part of: <span class="sky">Ryde Council</span></p>
<p>
The size of North Ryde is approximately 5 square kilometres. It has 18 parks covering nearly 18% of total area. The population of North Ryde in 2006 was 9,266 people. By 2011 the population was 10,123 showing a population growth of 9% in the area during that time. The predominant age group in North Ryde is 0-14 years. Households in North Ryde are primarily couples with children and are likely to be repaying between $3000 - $4000 per month on mortgage repayments. In general, people in North Ryde work in a Professional occupation. In 2006, 75.0% of the homes in North Ryde were owner-occupied compared with 75.3% in 2011. (source: Australian Bureau of Statistics) 
     
    </p>
    
    </div>
    
      
   <div class="tab-content-title-2"> 
    <h3>Market Trends for North Ryde</h3></div>
     <div class="tab-content-text">
     <img class="img-responsive" src="<?php echo base_url();?>images/market-trends.jpg">
        <div class="view-report">
       
            <a href="javascript:void(0)" class="btn btn-default">Read Full Report</a>
          
             </div>
        </div>
  </div>
</div>
<?php 
  $pass_cord = json_encode(array());
  $debt_interest = 12;
   $investment_amount = $per_share_amount;

 if($campaign_id > 0 && $campaign_id != '') { ?>

<div class="panel panel-default ec-panel">


<?php 
       
        
        $debt_term_length = 4;
        $net_investors = $debt_interest;
        $share =1;
        //project calculator
        $total_share_amount = round($investment_amount*$share);
        $years = 5;
        $total_return = round(($total_share_amount*$debt_interest/100)*$years);

        //3 months quarter
        $grids = $years;
       
        $t=array();

        if($debt_interest!='' && $debt_term_length!=''){
            for($g=1;$g<=$grids;$g++){
               
                $total_return_individual = (($investment_amount*$share*$debt_interest)/100)*$g;

                $t[] = array("$g",round($total_return_individual));
                
            }
        }
      
        $pass_cord = json_encode($t); 

      //  echo $pass_cord;die;
        ?>

  <div class="panel-heading">
    <h3 class="panel-title">Projected Return Calculator</h3>
  </div>
  <div class="panel-body" id="return-calculator">
   <div class="return-calculator">
 
  <div class="in-description rc-form">
     <ul class="return-calculator ec-form row">
         <li class="col-md-2 form-group">
         <label>Shares</label>
        
          <div class="select-search" >
                  <select class="selectpicker" id="shares">
                  <?php 
                  $i=0;
                    for($i=1;$i<=$share_available;$i++) { ?>
                     
                      <option data-tokens="" value="<?php echo $i;?>"><?php echo $i;?></option>
                  <?php  }

                  ?>
                   
                               
                  </select>              
                </div>          
           </li>
         <li class="col-md-2 form-group">
         <label>Investment</label>
         <input type="text"  placeholder="$2000" readonly="readonly" name="calc_amount_select"   id="calc_amount_select" class="form-control" value="<?php echo $investment_amount;?>">                 
          </li>
        <li class="col-md-3 form-group avg-rate">
         <div class="select-search" id="searchType">
                  <select class="selectpicker">
                   <option data-tokens="">Ave Growth Rate  </option>
                   <option data-tokens="">Ave Rental Yield</option>
                 </select>              
                </div>  
                <input type="text"  placeholder="12%" value="12"  id="average_growth" class="form-control ">   
       </li>
          <li class="col-md-2 form-group">                                             
          <label>Terms</label>
                 <div class="select-search"  >
                  <select class="selectpicker" id="terms">
                   <option data-tokens="" value="5">5 Years   </option>
                   <option data-tokens="" value="10">10 Years</option>
                            
                  </select>              
                </div>  
                    
             </li>                   
              <li class="col-md-3 form-group">
                <label>Total Return</label>
                 <span class="green-color" id="total_return"><?=set_currency($total_return)?></span>
               </li>
                </ul>
<div class="project-chart">
                    <div id="container" style="width: 100%; height:300px; margin: 0 auto"></div>
                </div>
  </div>      
    </div>
  </div>
</div>
<?php  } ?>
<?php if($investment_summary != '') {?>
 <div class="panel panel-default ec-panel">
  <div class="panel-heading">
    <h3 class="panel-title">Investment Summary</h3>
  </div>
  <div class="panel-body">
   <div class="tab-content-text">
   <?php echo $investment_summary;?>
    
        
    </div>
  </div>
</div>
  <?php } ?>
 <?php if($lead_investor_id > 0) {?>
 <div class="panel panel-default ec-panel">
  <div class="panel-heading">
    <h3 class="panel-title">Lead  Investors </h3>
  </div>
  <div class="panel-body">
  <?php 
    $lead_investor_data = UserData($lead_investor_id);
    
    $lead_investor_username = $lead_investor_data[0]['user_name'].' '.$lead_investor_data[0]['last_name'];
    
    $lead_investor_profile_slug = $lead_investor_data[0]['profile_slug'];
    $lead_investor_user_about = $lead_investor_data[0]['user_about'];
    if($lead_investor_profile_slug != '')
    {
      $slug = $lead_investor_profile_slug;
    }
    else
    {
      $slug = $lead_investor_id;
    }

    if(is_file(base_path().'upload/user/user_small_image/'.$lead_investor_data[0]['image']) && $lead_investor_data[0]['image'] != '')
    {
         $lead_investor_image = base_url().'upload/user/user_small_image/'.$lead_investor_data[0]['image'];
    }
    else
    {
        $lead_investor_image = base_url().'upload/user/user_small_image/no_man.jpg';
    }


  ?>
<div class="tab-content-text">

  <div class="row investor">
    <div class="col-md-12">
    <div class="col-md-3 col-sm-6 col-xs-12 inv">
    <div class="inv-wrapper">       
          <img src="<?php echo $lead_investor_image;?>">
            </div>
            <h3><?php echo $lead_investor_username; ?></h3>
            <span>Founder</span>
        </div>
       
    
        </div>
        
   </div>
     
      
<p>
<?php echo SecureShowData($lead_investor_user_about); ?>
<br><br>

 <a href="<?php echo site_url('user/'.$slug);?>" class="btn btn-default">View Investor Profile</a>

</p>

    
    
        
    </div>
  </div>
</div>
 
 <?php } ?>
</div>
<?php  if ($update_on_my_property){ ?>
<div id="update-tab" class="tab-pane fade">
<div class="panel panel-default  ec-panel">
 
  <div class="panel-body">
    <div id="upadtes">
   <ul class="upadtes-list update_ajax">
                                <?php

                                if ($update_on_my_property > 0) {

                                  $count=1;
                                    foreach ($update_on_my_property as $property_update) {
                                        $update_image = $property_update['image'];
                                        $update_text = SecureShowData($property_update['updates']);
                                        $update_date_added = getDuration($property_update['date_added']);
                                       

                                        ?>
                                        <li>
                                            
                                            <div class="updates-rt">
                                              <h3>Update #<?php echo $count;?></h3>
                                            </div>
                                           <div class="member-role"><?php echo $update_date_added; ?></div>
                                            <div class="update-text">
                                                <?php echo $update_text; ?>
                                            </div>
                                        </li>
                                    <?php
                                      $count++;
                                    }
                                    ?>
                                    <?php
                                    if ($update_on_my_property_count <= 5) {
                                        $display = 'none';
                                    } else {
                                        $display = 'block';
                                    }
                                    ?>
                                    <li class="show-more more_updates" style="display:<?php echo $display; ?>"><a
                                            href="javascript:void(0)" onclick="show_more_updates(5)">Show more
                                            updates</a></li>
                                <?php
                                } else {
                                    ?>
                                    <li class="no-data"><?php echo THERE_IS_NO_UPDATES_FOR_THIS_PROJECT; ?></li>
                                <?php
                                }
                                ?>
                            </ul>
    </div>
    </div>
     </div>
    </div>
    <?php } ?>
   <div id="investor" class="tab-pane fade">
  <div class="panel panel-default  ec-panel">
 
  <div class="panel-body">
          <div class="investor">
                <ul class="investor-list">
                 <?php
            if ($funder > 0) {
                foreach ($funder as $campaign_funder) {

                    $campaign_funder_image = $campaign_funder['image'];
                    $preapproval_status = $campaign_funder['preapproval_status'];
                    $campaign_user_id = $campaign_funder['user_id'];

                    if ($campaign_funder['profile_slug']) {
                        $campaign_profile_slug = $campaign_funder['profile_slug'];
                    } else {
                        $campaign_profile_slug = $campaign_funder['user_id'];
                    }

                    $campaign_funder_name = $campaign_funder['user_name'] . ' ' . $campaign_funder['last_name'];
                    $campaign_funder_date_time = dateformat($campaign_funder['transaction_date_time']);
                    $campaign_funder_amount_ret = $campaign_funder['amount'];
                    if ($campaign_funder_amount_ret > 0)
                        $campaign_funder_amount = set_currency($campaign_funder['preapproval_total_amount']);
                    else $campaign_funder_amount = ANONYMOUS;
                    if ($campaign_funder['comment'] != '') {
                        $campaign_funder_comment = $campaign_funder['comment'];
                    } else {
                        $campaign_funder_comment = $campaign_funder['comment'];
                    }

                    $showlink = true;

                    if ($campaign_user_id == "" or $campaign_user_id == 0) {
                        $showlink = false;
                        $ecampaign_funder_name = ANONYMOUS;
                    }
                    if ($campaign_funder_image != '' && is_file("upload/user/user_small_image/" . $campaign_funder_image) and $showlink == true) {
                        $fundersrc = base_url() . 'upload/user/user_small_image/' . $campaign_funder_image;
                    } else {
                        $fundersrc = base_url() . 'upload/user/user_small_image/no_man.jpg';
                    }
                    if ($preapproval_status != 'FAIL') {

                        ?>
                      <li>
                        <div class="investor-lt">
                       <a href="<?php echo site_url('user/'.$campaign_profile_slug);?>"> <img src="<?php echo $fundersrc; ?>" alt=""></a>
                        </div>
                         <div class="investor-rt">
                         <a href="<?php echo site_url('user/'.$campaign_profile_slug);?>"><h3><?php echo $campaign_funder_name;?></h3></a>
                         <p><span class="investor-time"><?php echo $campaign_funder_date_time;?></span></p>
                         <span class="investor-amount"> <?php echo $campaign_funder_amount; ?> </span>
                         <p class="comment-text">
                         <?php if ($campaign_funder_comment != "") { ?>
                         <i class="fa fa-quote-left"></i> <?php echo $campaign_funder_comment;?>   </p>
                         <?php } ?>
                         </div>
                        </li>
                         <?php
                       }
            }

            } else {
                ?>
                <li class="no-data"><?php echo THERE_IS_NO_FUNDERS_FOR_THIS_PROJECT; ?>.</li>
            <?php
            }
            ?>
                      
                      
                    </ul>
          </div>
  </div>
</div>
  
   
  </div>
 
  
  <div id="comment" class="tab-pane fade">
  <div class="panel panel-default ec-panel">
  
  <div class="panel-body">
   <?php
                if ($this->session->userdata('user_id') != '') {
                   
                    if ($click_share == 'yes') {

                        ?>
  <div class="investor-comment clearfix">
  <div class="panel-heading">
    <h3 class="panel-title">Post a Comment</h3>
  </div>

  <div class="alert-danger alert alert-message error_change_comment"  style="display: none">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
             <div class="error_message_comment"></div>
           
        </div>
  <div class="alert-success alert alert-message success_change" style="display: none">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="success_message_comment"></div>
        </div>



   <textarea class="form-control char_limited_input" cols="54" data-fieldlength="500" id="comment_comment" 
   name="comment[comment]" rows="7"></textarea>

   <div class="pull-left comment"><div class="checkbox">
                          <label class="ec-checkbox">
                          
                            <input type="checkbox" class="ut-checkbox" name="comment_type" value="1" id="comment_type"> <i></i> Keep Private                            
                          </label>
                        </div>
                        </div>
   <div class="pull-right">
 <?php $comment_ip = $_SERVER['REMOTE_ADDR']; ?>
    <input type="hidden" name="property_id" id="property_id" value="<?php echo $property_id; ?>">
    <input type="hidden" name="comment_user_id" id="comment_user_id"
           value="<?php echo $user_id; ?>">
    <input type="hidden" name="comment_user_session_id" id="comment_user_session_id"
           value="<?php echo $this->session->userdata('user_id'); ?>">
    <input type="hidden" name="comment_ip" id="comment_ip"
           value="<?php echo $comment_ip; ?>">
           <input type="hidden" name="campaign_id" id="campaign_id"
           value="<?php echo $campaign_id; ?>">


   <button class="btn btn-primary " type="submit" onclick="commentadd();">Post Comment</button></div>
   
   
   </div>

   <?php } 
   } else { ?>
<div class="investor-comment clearfix">
  <div class="panel-heading">
    <h3 class="panel-title">Post a Comment</h3>
  </div>

  



   <textarea class="form-control char_limited_input" cols="54" data-fieldlength="500" id="comment_comment" 
   name="comment[comment]" rows="7"></textarea>

   <div class="pull-left comment"><div class="checkbox">
                          <label class="ec-checkbox">
                          
                            <input type="checkbox" class="ut-checkbox" name="comment_type" value="1" id="comment_type"> <i></i> Keep Private                            
                          </label>
                        </div>
                        </div>
   <div class="pull-right">
 

    <a href="<?php echo site_url('home/login' . '/' . base64_encode(current_url())); ?>"
                                       class="btn btn-primary">Post comment</a>
   </div>

   </div>

   <?php } ?>
   <div class="clearfix"></div>
    <div class="comments m-t">

                <ul class="comments-list comment_ajax" >
                <?php 
                 if (!empty($comment)) {
            foreach ($comment as $property_comment) {
                $property_comment_id = trim($property_comment['comment_id']);
                $property_comment_user_id = trim($property_comment['user_id']);


                if ($property_comment['profile_slug'] != '') {
                    $property_comment_user_slug = $property_comment['profile_slug'];
                } else {
                    $property_comment_user_slug = $property_comment['user_id'];
                }

                $property_comment_status = $property_comment['status'];
                $property_comment_type = $property_comment['comment_type'];
                $property_comment_image = $property_comment['image'];
                $property_comment_name = $property_comment['user_name'] . ' ' . $property_comment['last_name'];
                $property_comment_date_time = getDuration($property_comment['date_added']);
                $property_comment = SecureShowData($property_comment['comments']);
                if ($property_comment_type == 1 or $property_comment_type == "1") {
                    $property_comment_status = 5;
                    if ($this->session->userdata('user_id') != "") {
                        if ($property_comment_user_id == $this->session->userdata('user_id')) {
                            $property_comment_status = 1;
                        }
                        if ($user_id == $this->session->userdata('user_id')) {
                            $property_comment_status = 1;
                        }
                    } else {
                        $property_comment_status = 5;
                    }
                }
                if ($property_comment_image != '' && is_file("upload/user/user_small_image/" . $property_comment_image)) {
                    $propertysrc = base_url() . 'upload/user/user_small_image/' . $property_comment_image;
                } else {
                    $propertysrc = base_url() . 'upload/user/user_small_image/no_man.jpg';
                }
                if ($property_comment_status == 1) { ?>
                      <li>
                        <div class="comments-lt">
                       <a href="<?php echo site_url('user/' . $property_comment_user_slug); ?>"> <img src="<?php echo $propertysrc;?>" alt=""></a>
                        </div>
                         <div class="comments-rt">
                        <a href="<?php echo site_url('user/' . $property_comment_user_slug); ?>"> <h3><?php echo $property_comment_name;?></h3></a>
                         <p><span class="investor-time">Said <?php echo $property_comment_date_time;?></span></p>
                        
                         <p class="comment-text">
                         <i class="fa fa-quote-left"></i>  <?php echo $property_comment;?>   </p>

                         <?php

                            $getchild = getAllchildofcomment($property_comment_id);
                           
                            if ($getchild) {
                                foreach ($getchild as $gchild) {

                                    // $child_comment_user_slug = $gchild->profile_slug;        
                                    $child_comment_image = $gchild->image;
                                    $child_comment_name = $gchild->user_name . ' ' . $gchild->last_name;
                                    $child_comment_date_time = getDuration($gchild->date_added);
                                    $child_comment = SecureShowData($gchild->comments);

                                    if ($gchild->profile_slug != '') {
                                        $child_property_comment_user_slug = $gchild->profile_slug;
                                    } else {
                                        $child_property_comment_user_slug = $gchild->user_id;
                                    }

                                    if ($child_comment_image != '' && is_file("upload/user/user_small_image/" . $child_comment_image)) {
                                        $childsrc = base_url() . 'upload/user/user_small_image/' . $child_comment_image;
                                    } else {
                                        $childsrc = base_url() . 'upload/user/user_small_image/no_man.jpg';
                                    }


                                    ?>
                                    <div class="media">
                                        <div class="comments-lt">
                                 <a href="<?php echo site_url('user/' . $child_property_comment_user_slug); ?>"> <img src="<?php echo $childsrc;?>" alt=""></a>
                                  </div>
                                       

                                         <div class="comments-rt">
                                  <a href="<?php echo site_url('user/' . $child_property_comment_user_slug); ?>"> <h3><?php echo ucfirst($child_comment_name);;?></h3></a>
                                   <p><span class="investor-time">Said <?php echo $child_comment_date_time;?></span></p>
                                  
                                   <p class="comment-text">
                                   <i class="fa fa-quote-left"></i>  <?php echo $child_comment;?>   </p>
                                        </div>
                                    
                                <?php
                                }

                            }
                            ?>
                         </div>
                        </li>    

                        <?php 

                        } 
                      }
                        ?> 
                         <?php
            if ($comment_count <= 5) {
                $display = 'none';
            } else {
                $display = 'block';
            }
            ?>
            <li class="show-more more_comments" style="display:<?php echo $display; ?>"><a href="javascript:void(0)"
                                                                                           onclick="show_more_comments(5)"><?php echo SHOW_MORE_COMMENTS; ?></a>
            </li>
        <?php
        } else {
            ?>
            <li class="no-data"><?php echo THERE_IS_NO_COMMENT_FOR_THIS_PROJECT; ?>.</li>
        <?php
        }
        ?>
                    </ul>
          </div>
   
   
  </div>
</div>
   </div>

  <div id="attachments" class="tab-pane fade">
   <div class="panel panel-default ec-panel">
  <div class="panel-heading">
  <h3 class="panel-title">Investor Attachment</h3>
  </div>
  <div class="panel-body">
    <div class="attachment">
      <?php 
        if($attachments!=null){ ?>
          <ul class="attachment-list">
            <?php foreach ($attachments as $value) { ?>
              <li><a target="_blank" href="<?php echo base_url(); ?>upload/property/attachments/<?php echo $value["new_name"]; ?>"><i class="fa fa-file-text-o"></i> <?php echo str_replace(".pdf", "",$value["file_name"]); ?> </a></li>
            <?php } ?>
          </ul>
      <?php } else { ?>
        <div class="alert alert-danger"><h3 style="margin:0px">There are no attachments.</h3></div>
      <?php } ?>
    
    </div>
  </div>
</div>
     
   
  </div>
  
 
  </div>
</div>
</div>
   </div>
   
    
 
   </div>
   </div>
   </div>
   </div>
    </section>
    <script type="text/javascript">

          function show_more_comments(offset) {
          if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
              xmlhttp = new XMLHttpRequest();
          } else { // code for IE6, IE5
              xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
          }
          xmlhttp.onreadystatechange = function () {
              if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                  $(".comment_ajax").first().find('.more_comments').remove();
                  if (offset == 0) {
                      $(".comment_ajax").html(xmlhttp.responseText);
                  } else {
                      $(".comment_ajax").append(xmlhttp.responseText);
                  }

              }
          }
          xmlhttp.open("GET", "<?php echo $comment_url;?>/" + offset, true);
          xmlhttp.send();
      }
      function show_more_updates(offset) {
        if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else { // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                $(".update_ajax").first().find('.more_updates').remove();
                $(".update_ajax").append(xmlhttp.responseText);
            }
        }
        xmlhttp.open("GET", "<?php echo $updates_url;?>/" + offset, true);
        xmlhttp.send();
    }



         function commentadd() {
              var property_id_value = document.getElementById("property_id").value;
              var comment_user_id_value = document.getElementById("comment_user_id").value;
              var campaign_id_value = document.getElementById("campaign_id").value;
              var comment_user_session_id_value = document.getElementById("comment_user_session_id").value;
              var comment_value = document.getElementById("comment_comment").value;
              var comment_ip_value = document.getElementById("comment_ip").value;
              if ($("#comment_type").is(':checked')) {
                  var comment_type = 1;
              } else {
                  var comment_type = 0;
              }
             $(".error_change_comment .error_message_comment").html('');
             $(".success_change .success_message_comment").html('');
              var mapdata = {
                  "property_id": property_id_value,
                  "comment_user_id": comment_user_session_id_value,
                  "comment": comment_value,
                  "comment_ip": comment_ip_value,
                  "comment_type": comment_type,
                  "campaign_id":campaign_id_value,
              };
              var getStatusUrl = '<?php echo site_url('property/add_comment')?>';
              $.ajax({
                  url: getStatusUrl,
                  dataType: 'json',
                  type: 'POST',
                  timeout: 99999,
                  global: false,
                  data: mapdata,
                  success: function (data) {
                      if (data.msg.error != "") {
                          $(".success_change").hide();
                          $(".error_change_comment").show();
                          $(".error_change_comment .error_message_comment").append(data.msg.error);
                          return;
                      }
                      if (data.msg.success != "") {
                          $(".success_change").show();
                          $(".error_change_comment").hide();
                          $("#comment_comment").val('');
                          $("#comment_type").prop('checked',false);
                          $(".success_change .success_message_comment").append(data.msg.success);
                           if (data.msg.owner_cmt == "yes") {
                            $(".comment_ajax li").not('li:first').remove();
                            show_more_comments(0);
                            }
                         
                      }
                  },
                  error: function (XMLHttpRequest, textStatus, errorThrown) {
                  }
              });
        }

        function savefollowerlist(id) {
          var user_id_value = '';
          //alert(user_id_value);
          var mapdata = {"user_id": user_id_value};
          // var mapdata = "'perk_title=' + perk_title_value" ;
          //var mapdata_description = 'perk_description=' + perk_description_value;
          var getStatusUrl = '<?php echo site_url('follower/ajax_property_follower');?>/'+id;
          ////alert(getStatusUrl)
          $.ajax({
              url: getStatusUrl,
              dataType: 'text',
              type: 'POST',
              timeout: 99999,
              global: false,
              data: mapdata,
              //data:{'perk[]':mapdata},
              success: function (data) {
                  
                  document.getElementById('propertyfollowers').innerHTML = data;
              },
              error: function (XMLHttpRequest, textStatus, errorThrown) {
              }
          });
    }
    function followproperty(id) {
        if (id == '') {
            return false;
        }
        var strURL = '<?php echo base_url().'follower/property_follower_insert/';?>' + id;
         PopupOpen();
        var xmlhttp;
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                 PopupClose();
                if ($.trim(xmlhttp.responseText == '<span class="share-i"><i class="fa fa-heart"></i></span> <?=FOLLOW?>')) {
                    document.getElementById("follow_unfollow_property").innerHTML = '<a href="javascript://"id="followme_property" onclick="unfollowproperty(' + id + ')" onmouseover="set_followingtext_property(0);" onmouseout="set_followingtext_property(1);"><i class="fa  fa-heart-o"></i><?=FOLLOWING?></a>'
                savefollowerlist(id);
                }
            }
        }
        xmlhttp.open("GET", strURL, true);
        xmlhttp.send();
    }

    function unfollowproperty(id) {
        $('#followme_property').removeAttr('onmouseover').removeAttr('onmouseout');
        if (id == '') {
            return false;
        }
        var strURL = '<?php echo base_url().'follower/property_unfollower';?>/' + id;
         PopupOpen();
        var xmlhttp;
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else { // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                 PopupClose();
                if ($.trim(xmlhttp.responseText == '<i class="fa fa-heart-o"></i> <?=UNFOLLOW?>')) {
                    document.getElementById("follow_unfollow_property").innerHTML = '<a href="javascript://" id="followme_property" onclick="followproperty(' + id + ')"><i class="fa fa-heart"></i> <?=FOLLOW?></a>';
                     savefollowerlist(id);
                }
            }
        }
        xmlhttp.open("GET", strURL, true);
        xmlhttp.send();
    }

    function set_followingtext_property(set) {
        if (set == 0) {
            document.getElementById("followme_property").innerHTML = '<i class="fa fa-heart-o"></i> <?=UNFOLLOW?>';
        } else {
            document.getElementById("followme_property").innerHTML = '<i class="fa fa-heart-o"></i> <?=FOLLOWING?>';
        }
    }


    </script>
    
   
<script type="text/javascript">
$(function () {
    var cord = <?=$pass_cord?>;

    console.log(cord);
    var amount_currency = '$';
   
    // change chart on dropdown item selected.
    $('body').on('change','#shares',function(){
        var chart =$('#container').highcharts();
        chart.series[0].remove();
        var interest = '<?=$debt_interest?>';
       
        var investment_amount = '<?=$investment_amount?>';
        var shares =$(this).val();
        var total_share_amount = investment_amount * shares;
        var years = $("#terms").val();


        var total_return = ((total_share_amount*interest/100)*years).toFixed();

        //console.log(total_return);

        //3 months quarter
        var grids = years;
      
        var i=0;
        var t=[];
        var xAxisLabels = [];
        for(var g=1;g<=grids;g++){
            
            var total_return_individual = 
            ((parseFloat(interest)*parseInt(investment_amount)/100)*g*shares).toFixed();

            t[i] = [g.toString(),parseInt(total_return_individual)];
            
            xAxisLabels.push(g-1);
            i++;
        }


        chart.xAxis[0].setCategories(xAxisLabels);
        var lbl ={
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                //format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            };
        chart.addSeries({
            data: t,
            dataLabels:lbl
        });
        $("#total_return").html(amount_currency+total_return);
    });

     $('body').on('change','#terms',function(){
        var chart =$('#container').highcharts();
        chart.series[0].remove();
        var interest = '<?=$debt_interest?>';
       
        var investment_amount = '<?=$investment_amount?>';
        var years =$(this).val();
        var shares = $("#shares").val();

        var total_share_amount = investment_amount * shares;
        
       
        var total_return = ((total_share_amount*interest/100)*years).toFixed();

        //console.log(total_return);

        //3 months quarter
        var grids = years;
      
        var i=0;
        var t=[];
        var xAxisLabels = [];
        for(var g=1;g<=grids;g++){
            
            var total_return_individual = 
            ((parseFloat(interest)*parseInt(investment_amount)/100)*g*shares).toFixed();

            t[i] = [g.toString(),parseInt(total_return_individual)];
           
            xAxisLabels.push(g-1);
            i++;
        }


        chart.xAxis[0].setCategories(xAxisLabels);
        var lbl ={
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                //format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '11px',
                    fontFamily: 'Verdana, sans-serif'
                }
            };
        chart.addSeries({
            data: t,
            dataLabels:lbl
        });
       
        $("#total_return").html(amount_currency+total_return);
    });

    // high chart initialize
    $('#container').highcharts({
        chart: {
            type: 'column',
            height: 400
        },
        colors: ['#1a6790'],
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: 1,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Open Sans, Arial, Helvetica, sans-serif'
                }
            }
        },
        yAxis: {
            //floor: 0,
            //ceiling: 20000,
            title: {
                text: 'Amount'
            }
        },
        xAxis: {
           // floor: 0,
           // ceiling: 20,
           // min: 0,
            title: {
                text: 'Project Return Amount'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'Projected return: <b>'+amount_currency+'{point.y}</b>'
        },

        series: [{
            name: 'Population',
            data: cord,
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                //format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
});
</script>
<script src="<?=base_url()?>js/charts/highcharts.js"></script>
<script src="<?=base_url()?>js/charts/exporting.js"></script>

<script src="<?php echo base_url();?>assets_front/bootstrap-select/js/bootstrap-select.js"></script>

    <script src="<?php echo base_url();?>assets_front/ec-input/ec-input.js"></script>
 <script src="<?php echo base_url();?>js/progress-bar.js"></script>
 <script src="<?php echo base_url();?>js/raphael.js"></script>
 
  <script src="<?php echo base_url();?>js/bootstrap-tabdrop.js"></script>
    <script>
    if (top.location != location) {
    top.location.href = document.location.href ;
  }
        $(function(){
            window.prettyPrint && prettyPrint();
      $('.nav-tabs:first').tabdrop();
      $('.nav-tabs:last').tabdrop({text: 'More options'});
        });
    </script>

    
  <script>
      $('a.gallery_thumbnail').iLightBox({

        type: 'image', //'image', 'ajax', 'iframe', 'swf' and 'html'
        loop: true, //loop media
        arrows: true, //show arrows
        closeBtn: true, //show close button
        title: null, //title
        href: null, //link to media
        content: null, //html content
        path: 'horizontal',
        beforeShow: function(a, b) {},
        onShow: function(a, b) {},
        beforeClose: function() {},
        afterClose: function() {},
        onUpdate: function(a) {},
        template: {
            container: '<div class="iLightbox-container"></div>',
            image: '<div class="iLightbox-media"></div>',
            iframe: '<div class="iLightbox-media iLightbox-iframe"></div>',
            title: '<div class="iLightbox-details"></div>',
            error: '<div class="iLightbox-error">The requested content cannot be loaded.<br/>Please try again later.</div>',
            closeBtn: '<a href="#" class="iLightbox-close"></a>',
            prevBtn: '<div class="iLightbox-btnPrev"><a href="javascript:;"></a></div>',
            nextBtn: '<div class="iLightbox-btnNext"><a href="javascript:;"></a></div>'
        }
    });
    
     $(function () {
    
var url = document.location.toString();
if (url.match('#')) {
    $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
} 

// Change hash for page-reload
$('.nav-tabs a').on('shown.bs.tab', function (e) {
    window.location.hash = e.target.hash;
})
        
    });
    </script>