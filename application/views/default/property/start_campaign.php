<script src="<?php echo base_url(); ?>js/jquery.form.js"></script>
<?php 

$property_id = $property['property_id'];
$property_user_id = $property['user_id'];
$property_address = $property['property_address'];
$property_url = $property['property_url'];
$status = $property['status'];
$property_suburb = $property['property_suburb'];
$bedrooms = $property['bedrooms'];
$bathrooms = $property['bathrooms'];
$cars = $property['cars'];
$estimate = $property['estimate'];



if(is_file(base_path().'upload/property/commoncard/'.$property['cover_image']) && $property['cover_image'] != '')
{
     $cover_image = base_url().'upload/property/commoncard/'.$property['cover_image'];
}
else
{
    $cover_image = base_url().'upload/property/commoncard/no_img.jpg';
}
    
?>
   
    <section>
      <div class="faqs-head">
            <div class="container">
                <div class="page-title">
                    <h2>Create A Campaign</h2>
                </div>
            </div>
        </div>
    </section>
   
   
   <div class="content-all">
   <div class="container">
              <div class="investment-left col-md-8">
                <div class="panel panel-default ec-panel">
                     
                 <div class="panel-body">
                
                     <form action="<?php echo site_url('property/start_campaign/' . $property_id.'/'.$campaign_id) ?>" method="post" accept-charset="utf-8" id="frm_project" name="frm_project" class="ec-form" enctype="multipart/form-data">

            <div class="row">
              <div class="form-group col-md-12">
             <label for="card-number">Are you Propety Owner/Developer or Re Agent ?</label>
                  
                  <select class="selectpicker" name="campaign_user_type">
                  <option data-tokens="" value="" > Select </option>
                   <option data-tokens="" value="1"  <?php if($campaign_user_type == 1) echo 'selected="selected"'; ?>>Investor </option>
                   <option data-tokens="" value="2" <?php if($campaign_user_type == 2) echo 'selected="selected"'; ?>>Property Developer </option>
                   <option data-tokens="" value="3" <?php if($campaign_user_type == 3) echo 'selected="selected"'; ?>>Property Owner </option>
                   <option data-tokens="" value="4" <?php if($campaign_user_type == 4) echo 'selected="selected"'; ?>>R/E Agent </option>
                        
                  </select>          
               
                     <span class="create_step_error"> <?php  echo form_error('campaign_user_type');?></span>
                           
                  
                  
                </div>                      
                </div>
                    <div class="row c-s-radio-group core-api-wrap">                   
                 <div class="form-group col-md-6 ">
                  <label for="">Select Property Valuation </label>
                    <div class="radio radio-danger form-control">
                    <input type="radio" name="investment_amount_type" id="smi-amout" value="1" <?php if($investment_amount_type == 1) echo 'checked="checked"';?>>
                                <label for="smi-amout">
                                   Select Min. Investment Amount
                                </label>
                            </div>
<div class="form-control range-input">

<input id="ex7" type="text" data-slider-min="<?php echo $min_property_investment_range; ?>" data-slider-tooltip="hide" data-slider-max="<?php echo $max_property_investment_range; ?>" data-slider-step="10" data-slider-value="100" data-slider-enabled="true"/>


<div id="bar">      
       <input type="text" id="minval" placeholder="$100" name="min_investment_amount" value="<?php echo set_currency($min_investment_amount); ?>" >  
       <span class="create_step_error"> <?php  echo $investment_range_error;?></span>
 </div>
</div>
                 </div>
                   <div class="col-md-1"><span class="or">Or<span class="glyphicon glyphicon-resize-horizontal"></span> </span></div>
                     <div class="col-md-5 form-group core">
                        <label for="">&nbsp;</label>
                                <div class="radio radio-danger form-control">
                                <input type="radio" name="investment_amount_type" id="core-amount"  value="0" 
                                <?php if($investment_amount_type == 0){ echo 'checked="checked"';}?>>
  <label for="core-amount">
                                  Use CoreLogic Property Valuation
                                </label>
                            </div>
                          <div class="core-api">            
                         
                            <span class="value"><?php echo set_currency($estimate);?></span>
                        
                          </div>
                     
                     </div>
                </div>
                
                <div class="row">
              <div class="form-group col-md-6">
                     <label for="card-number">Campaign Type</label>
                  <div class="c-s-radio-group">
                  <div class="form-control row">
                     
                     <div class="radio radio-danger col-md-4 ">
                                <input type="radio"  value="0" id="radio1" name="campaign_type" <?php if($campaign_type == 0) echo 'checked="checked"';?>>
                                <label for="radio1">
                                    Public
                                </label>
                            </div>
                            <div class="radio radio-danger col-md-8 ">
                                <input type="radio"  value="1" id="radio2" name="campaign_type" <?php if($campaign_type == 1) echo 'checked="checked"';?>>
                                <label for="radio2">
                                    Invitation Only
                                </label>
                            </div>


              
                             <span class="create_step_error"> <?php  echo form_error('campaign_type');?></span>
                            </div>
                     </div>
                  
                </div>                      
                </div>
                <div class="row">
                <div class="form-group col-md-6">
                     <label for="no-units">Number of Units</label>
                     <input   type="text" name="campaign_units" class="form-control " id="public-field" checked="" placeholder="Min 10 Max 20" value="<?php echo $campaign_units; ?>">
                   <input style="display:none;"   type="text" class="form-control " id="invite-field" placeholder=" Max 20" value="<?php echo $campaign_units; ?>">
                       <span class="create_step_error"> <?php  echo $campaign_units_error;?>
                     </div>
                </div>
                <div class="row">
                <div class="form-group col-md-6 ">
                     <label for="">Investment Close Date</label>
                     
                  <input class="form-control" type="text" name="investment_close_date" value="<?php echo $investment_close_date; ?>" id="datepicker">
                   <span class="create_step_error"> <?php  echo form_error('investment_close_date');?>
                     </div>
                </div>
              <!--   <div class="row">
                <div class="form-group col-md-12  browse-btn">
                     <label for="upload-img">Upload Document - Property Report, Property Contract, Photoes, etc </label>
                        <input type="file" name="campaign_document" class="filestyle form-control" data-buttonName="btn-primary">       
                         <span class="create_step_error"> <?php  echo $campaign_attachment_error;?>
                         </div>
                      
                </div> -->

                 <div class="create-campain-bg">        

                <div class="row" id="image-box">
<div class="form-group col-md-12  browse-btn" >
<label for="upload-img">Upload Document - Property Report, Property Contract, Photos, etc </label>
<div class="row">
<div class="col-md-12">
<ul class="UploadList" id="load-image-list">


</ul>
</div>
</div id="picture_file_field_image">
<input type="file" id="campaign_document" name="campaign_document" class="filestyle form-control" data-buttonName="btn-primary">   

<span class="image_error create_step_error"></span>    
</div>
 
</div>
</div>

               <div class="btn-group">
               <input type="hidden" name="campaign_id" id="campaign_id" value="<?php echo $campaign_id;?>">
                 <button type="submit" class="btn btn-primary btn-lg btn-block text-uppercase ">Create</button>
                 </div>               
                    </form> 
                 </div>
                 </div>
                                         
                </div>
            
            <div class="col-md-4 c-c-sidebar">
            
            <div class="explore-campaigns">
           <div class="campaign-thumbnail">
                  <div class="campaign-img">
                    <a href="<?php echo site_url('properties/'.$property_url);?>">
                      <span class="overlay"></span>
                      <img src="<?php echo $cover_image;?>">
                    </a>
                    
                  </div>
                  <div class="campaign-caption">
                    <ul class="property-info clearfix">
                      <li>
                        <span class="info-num"><?php echo $bedrooms; ?></span>
                        <span class="icon-wrapper"><img src="<?php echo base_url();?>images/icon-bed.png"></span>
                        <span class="info-name">bedrooms</span>
                      </li>
                      <li>
                        <span class="info-num"><?php echo $bathrooms; ?></span>
                        <span class="icon-wrapper"><img src="<?php echo base_url();?>images/icon-bath.png"></span>
                        <span class="info-name">bathrooms</span>
                      </li>
                      <li>
                        <span class="info-num"><?php echo $cars; ?></span>
                        <span class="icon-wrapper"><img src="<?php echo base_url();?>images/icon-car.png"></span>
                        <span class="info-name">cars</span>
                      </li>
                    </ul>
                    <div class="campaign-details">
                      <div class="campaign-title">
                        <h2><a href="<?php echo site_url('properties/'.$property_url);?>"><?php echo $property_address; ?></a></h2>
                      </div>
                      
                    </div>
                    
                    
                    
                  </div>
                </div>
          
          </div>
            
            </div>
   </div>
   </div>
   </div>
    </section>
  
    
</script>
 <script type="text/javascript" src="<?php echo base_url();?>js/bootstrap-datepicker.js"></script>  
    <script src="<?php echo base_url();?>js/bootstrap-slider1.js"></script>
    
    
    <script type="text/javascript">
    var property_id = "<?php echo $property_id?>";
      $(document).ready(function(){

$('#ex7').slider()
  .on('slide', function(ev){
       
    $("#minval").val("$"+ev.value);  
       // $("#minval").val("$"+ev.value+"K");

  });
  



$("#smi-amout").click(function() {
    if(this.checked) {
    
        $("#minval").val("");
$(".core-api").css({"background-color": "#f8f9f9"}); 
$(".core-api > .value").css({"color": "#969696"}); 
$("#ex7").slider("enable");
$("#minval").removeAttr('readonly');
    
    }
else {
}
});
$("#core-amount").click(function() {
    if(this.checked) {
    
    $("#minval").val($('.value').text());   
    $(".core-api").css({"background-color": "#1b6b95"}); 
    $(".core-api > .value").css({"color": "#fff"}); 
        $("#ex7").slider("disable");
        $("#minval").attr('readonly','readonly');
    
    }
else {
}
});
  
  $('#radio1').on('change', function(){ // on change of state
   if(this.checked) // if changed state is "CHECKED"
    {
        $( "#public-field" ).show();
         $( "#invite-field" ).hide();
    }
                    
})
  $('#radio2').on('change', function(){ // on change of state
   if(this.checked) // if changed state is "CHECKED"
    {
      $( "#public-field" ).hide();
         $( "#invite-field" ).show();
    }else {
        
}
})


$( "#datepicker" ).datepicker({ 

    startDate: '<?php echo $site_setting['project_min_days'];?>d',
     endDate: "+<?php echo $site_setting['project_max_days'];?>d",

      });

      })

      $(function () {


    $("body").on("change", "#image", function (event) {
    });

});
$(document).ready(function () {

    list_image_data();
    


    $('input[type=file]').on('change', function (event) {

      var campaign_id = $("#campaign_id").val();

        var file_data = $("#campaign_document").prop("files")[0];   // Getting the properties of file from file field
        var form_data = new FormData();                  // Creating object of FormData class
        form_data.append("file", file_data)              // Appending parameter named file with properties of file_field to form_data
        form_data.append("property_id", property_id)    
        form_data.append("campaign_id", campaign_id)                 // Adding extra parameters to form_data
        $.ajax({
                      url: "<?php echo site_url('property/document_save'); ?>",
                      dataType: 'json',
                      cache: false,
                    
                      contentType: false,
                      processData: false,
                      data: form_data,                         // Setting the data attribute of ajax with file_data
                      type: 'post',
                      success: function(result) {

                        if(result.image_error != '')
                        {
                             $(".image_error").html(result.image_error);
                        }
                        else
                        {


                          $("#campaign_id").val(result.campaign_id);
                     
                          list_image_data();
                          BlankImageData();
                        }

                    }
             })
      });
    
    //delete record from table
    $("body").on("click", ".delete-image", function (event) {

        var image_id = $(this).attr("rel");
        var campaign_id = $("#campaign_id").val();
      
        $(this).parent().parent().remove();
        $.post("<?php echo site_url('property/delete_document'); ?>", {
            image_id: image_id,
            campaign_id: campaign_id,
            property_id: property_id,
        }, function (result) {
            BlankImageData();
            return false;

        });
    });



});
/*list investor data */
function list_image_data() {
  var campaign_id = $("#campaign_id").val();
    $.post("<?php echo site_url('property/list_document'); ?>", {campaign_id: campaign_id,property_id : property_id}, function (result) {

        $('#load-image-list').html(result);

    });
}


function BlankImageData() {
    $('#image-box').closest('.create-campain-bg').find('input,textarea,select').val('');

    $('#image-box').closest('.create-campain-bg').find("#property_id").val(property_id);
    
    $(".image_error").html('');
   
    list_image_data();
}
      
    
    </script>
     