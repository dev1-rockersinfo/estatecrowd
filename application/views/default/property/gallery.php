
<script src="<?php echo base_url(); ?>js/jquery.form.js"></script>


    <section>
	  	<div class="db-head">
	      	<div class="container">
	      		<div class="page-title">
		        	<h2>Add Propety Details</h2>
		        </div>
	      	</div>
	  	</div>
	  	<div class="grey-bg" id="section-area">	  		
	      	<div class="container">
	      		<div class="row">
		      		<div class="col-md-3 faqs-left">
		      			   <?php echo $this->load->view('default/property/property_sidebar');?>
		      		</div>
  		<div class="col-md-9 add-pro-right">
			<div class="row dec-form">
        <div id="property-detail" class="create-campain-bg propety-area clearfix">

       <div class="form-heading">
      <h3>Video Gallery </h3>
       </div>
        <div class="property-detail-body row" id="video-box">
       <div class="cc-comments gallery-media col-md-12">
       <ul class="comments-list" id="load-video-list">
                    
                    </ul>
                    </div>
                    <?php
$attributes = array(
    'id' => 'video_form',
    'name' => 'video_form',
    'enctype' => 'multipart/form-data',
    'class' => 'edit_project',
    'method' => 'post',
    'accept-charset' => 'UTF-8'
);
echo form_open_multipart('property/video_save/', $attributes);
?>

          <div class="form-group col-md-6">
         <label for="">URL</label>
         <input type="text" name="media_video_name" id="media_video_name" placeholder="Youtube or vimio link"    class="form-control "> 
 <input name="property_id" id="property_id" type="hidden" value="<?php echo $id; ?>">  
           <input name="video_id" id="video_id" type="hidden" value="0" placeholder=""
                                           class="form-control">
                                    <span class="media_video_name_error create_step_error"></span>
         </div>  
           <div class="form-group col-md-6">
         <label for="">Title</label>
         <input type="text" name="media_video_title" id="media_video_title"  class="form-control "> 
          <span class="media_video_title_error create_step_error"></span>
         </div>  
     
   <div class="form-group col-md-12">
		 <label>Description</label>            
    	 <textarea rows="5" class="text-area" name="media_video_desc" id="media_video_desc"></textarea>              
         </div>

		 <div class="col-md-6 form-group" id="video-switch">                       
       <input type="checkbox" name="media_video_status" id="media_video_status" value="1" checked>
     </div>

       

     	 <div class="col-md-6 form-group ">  
   <button class="btn btn-primary btn-small text-uppercase pull-right" id="ajax-video-add"><?php echo ADD_VIDEO; ?></button>
       <button class="btn btn-primary btn-small text-uppercase pull-right btn-cancel" id="ajax-video-cancel"
                                    style="display:none;"><?php echo CANCEL; ?></button>
   
     </div>
     </form>
    </div>
    </div>
     <div id="property-detail" class="create-campain-bg propety-area image-gallery clearfix">
 <div class="form-heading">
      <h3>Image Gallery </h3>
       </div>


 <div class="property-detail-body row" id="image-box">
   <div class="property-detail-body row">

    <div class="cc-comments gallery-media col-md-12">
       <ul class="comments-list" id="load-image-list">
                        
                        
                    </ul>
                    </div>
                    <?php
    $attributes = array(
        'id' => 'image_form',
        'name' => 'image_form',
        'enctype' => 'multipart/form-data',
        'class' => 'edit_project',
        'method' => 'post',
        'accept-charset' => 'UTF-8'
    );
    echo form_open_multipart('property/image_save/', $attributes);
    ?>
          <div class="form-group col-md-6 browse-upload-btn" id="picture_file_field_image">
         <label for="">Browse</label>
         <input type="file" class="filestyle form-control project_picture" data-iconName="glyphicon glyphicon-picture" data-buttonName="btn-primary" data-buttonText="Add Image" name="image" id="image" >  
           <span class="image_error create_step_error"></span>  
         </div>  

           <div class="form-group col-md-6">
         <label for="">Title</label>
         <input type="text"   name="image_name" id="image_name" class="form-control "> 
          <input name="property_id" id="property_id" type="hidden" value="<?php echo $id; ?>"
                                           placeholder="" class="form-control">
                                    <input name="image_id" id="image_id" type="hidden" value="0" placeholder=""
                                           class="form-control">
                                    <span class="image_name_error create_step_error"></span>
         </div>  
     
   <div class="form-group col-md-12">
		 <label>Description</label>            
    	 <textarea rows="5" name="image_desc" id="image_desc" class="text-area"></textarea>              
         </div>
		 <div id="image-switch" class="col-md-6 form-group" >                       
       <input type="checkbox" name="image_status" id="image_status" value="1" checked>
     </div>
  

     	 <div class="col-md-6 form-group">    

    <input name="property_id" id="property_id" type="hidden" value="<?php echo $id; ?>">     
    
    <button class="btn btn-primary btn-small text-uppercase pull-right" id="ajax-image-add"><?php echo ADD_IMAGE; ?></button>         
    <button class="btn btn-primary btn-small text-uppercase pull-right btn-cancel" id="ajax-image-cancel"
                                    style="display:none;"><?php echo CANCEL; ?></button>     
    
     </div>
    </form>
 <div class="col-md-12 form-group  addpro-nxt">   

       <a  href="<?php echo site_url('property/attachments/'.$id);?>" type="button" class="btn btn-primary btn-small text-uppercase pull-right">Next</a>
     </div> 

    </div>
</div>
</div>
    
       
      		
		</div>
	</div>
</div>
    </section>
   

	
<script src="<?php echo base_url();?>js/bootstrap-switch.js"></script>
	
<script type="text/javascript">

 $("[type='checkbox']").bootstrapSwitch();

 </script>

<script type="text/javascript">
var property_id = "<?php echo $id;?>";
  /****************Start: video functionality code started from here*/

$(document).ready(function () {

    // bind form id and provide a simple callback function
    var options = {
        target: '#output1',   // target element(s) to be updated with server response
        beforeSubmit: '',  // pre-submit callback
        complete: function () {

        },  // post-submit callback 
        success: after_video,
        dataType: 'json',
        
    };
    if ((pic = jQuery('#video_form')).length)
        pic.ajaxForm(options);

    //load investor data;
    list_video_data();
    $("body").on("click", "#ajax-video-add", function (event) {

        PopupOpen();
        $('#video_form').submit();
        return false;
    });
    //delete record from table
    $("body").on("click", ".delete-video", function (event) {

        var video_id = $(this).attr("rel");
        $(this).parent().parent().parent().remove();
        $.post("<?php echo site_url('property/delete_video'); ?>", {
            video_id: video_id,
            property_id: property_id
        }, function (result) {
            BlankVideoData();
            return false;

        });
    });
    $("body").on("click", "#ajax-video-cancel", function (event) {

        BlankVideoData();
        return false;

    });
    //load particular team record with form
    $("body").on("click", ".edit-video", function (event) {

        var video_id = $(this).attr("rel");
        $.post("<?php echo site_url('property/edit_video'); ?>",
            {video_id: video_id, property_id: property_id},
            function (result) {

                var result = $.parseJSON(result);
                var error = result.error;
                var msg = result.msg;
                var url = result.url;

                if (url != '') {
                    //location.href=url;
                }
                if (error == true) {
                    $("#error-msg-company-industry").html(msg);
                }
                else {
                    $("#ajax-video-cancel").show();
                    $(".media_video_name_error").html('');
                    $(".media_video_title_error").html('');
                     $(".media_video_desc_error").html(result.media_video_desc_error);
                    $("#ajax-video-add").html('<?php echo UPDATE_VIDEO;?>');
                    var video_id = result.video.id;
                    $("#video_id").val(video_id);

                    var media_video_title = result.video.media_video_title;
                    $("#media_video_title").val(media_video_title);
                    var media_video_name = result.video.media_video_name;
                    $("#media_video_name").val(media_video_name);
                    var media_video_desc = result.video.media_video_desc;
                    $("#media_video_desc").val(media_video_desc);
                    var media_video_status = result.video.status;

                    if (eval(media_video_status) == 1)
                    {
                        $('.bootstrap-switch-id-media_video_status .bootstrap-switch-container').css('margin-left','0px');
                    }
                    else 
                    {
                         $('.bootstrap-switch-id-media_video_status .bootstrap-switch-container').css('margin-left','-60px');
                    }  
                     $('#media_video_status').attr('checked',true);
                    var image = result.video.image;


                }
            });
    });


});
/*list investor data */
function list_video_data() {

    $.post("<?php echo site_url('property/list_video'); ?>", {property_id: property_id}, function (result) {

        $('#load-video-list').html(result);

    });
}
/*call before investor data inserted for merging file image and form data */
function before_video(formData, jqForm, options) {

    var data = $('#video-box').closest('.create-campain-bg').find('input,textarea,select').serializeArray();

    formData.append(data);
    var queryString = $.param(formData);
    return true;
}

// Called after successfully save investor data
function after_video(result) {
    PopupClose();
    var error = result.error;
    var msg = result.msg;
    var url = result.url;
    if (url != '') {
        location.href = url;
    }
    else if (error == true) {
        var media_video_title_error = result.media_video_title_error;
        $(".media_video_title_error").html(media_video_title_error);
        var media_video_name_error = result.media_video_name_error;
        $(".media_video_name_error").html(media_video_name_error);
        $(".media_video_desc_error").html(result.media_video_desc_error);
    }
    else {
        $('#video-box').closest('.create-campain-bg').find('input,textarea,select').val('');
        $('#video-box').closest('.create-campain-bg').find("#property_id").val(property_id);
        $("#ajax-video-cancel").hide();
        $("#ajax-video-add").html('<?php echo ADD_VIDEO;?>');
        $(".media_video_name_error").html('');
        $(".media_video_title_error").html('');
        $(".media_video_desc_error").html('');
        $('.bootstrap-switch-id-media_video_status .bootstrap-switch-container').css('margin-left','0px');
        $('#media_video_status').attr('checked',true);
        list_video_data();
        
    }


}
function BlankVideoData() {
    $('#video-box').closest('.create-campain-bg').find('input,textarea,select').val('');
    $('#video-box').closest('.create-campain-bg').find("#property_id").val(property_id);
    $("#ajax-video-cancel").hide();
    $("#ajax-video-add").html('<?php echo ADD_VIDEO;?>');
    $(".media_video_name_error").html('');
    $(".media_video_title_error").html('');
    $(".media_video_desc_error").html('');
    $('.bootstrap-switch-id-media_video_status .bootstrap-switch-container').css('margin-left','0px');
    $('#media_video_status').attr('checked',true);

    list_video_data();
}
/*End: Video functionality code started from here*/
/****************Start: image functionality code started from here*/
$(function () {


    $("body").on("change", "#image", function (event) {
    });

});
$(document).ready(function () {

    // bind form id and provide a simple callback function
    var options = {
        target: '#output1',   // target element(s) to be updated with server response
        beforeSubmit: '',  // pre-submit callback
        complete: function () {

        },  // post-submit callback 
        success: after_image,
        dataType: 'json',
    
    };
    if ((pic = jQuery('#image_form')).length)
        pic.ajaxForm(options);

    //load investor data;
    list_image_data();
    $("body").on("click", "#ajax-image-add", function (event) {
        PopupOpen();
        $('#image_form').submit();
        return false;
    });
    $("body").on("click", "#ajax-image-cancel", function (event) {

        BlankImageData();
        return false;
    });

    //delete record from table
    $("body").on("click", ".delete-image", function (event) {

        var image_id = $(this).attr("rel");
        $(this).parent().parent().parent().remove();
        $.post("<?php echo site_url('property/delete_image'); ?>", {
            image_id: image_id,
            property_id: property_id
        }, function (result) {
            BlankImageData();
            return false;

        });
    });

    //load particular team record with form
    $("body").on("click", ".edit-image", function (event) {

        var image_id = $(this).attr("rel");
        $.post("<?php echo site_url('property/edit_image'); ?>", {
            image_id: image_id,
            property_id: property_id
        }, function (result) {

            var result = $.parseJSON(result);
            var error = result.error;
            var msg = result.msg;
            var url = result.url;

            if (url != '') {
                //location.href=url;
            }
            if (error == true) {
                $("#error-msg-company-industry").html(msg);
                $(".image_name_error").html(result.image_name_error);

            }
            else {
                $("#ajax-image-cancel").show();
                $("#ajax-image-add").html("<?php echo UPDATE_IMAGE;?>");
                var image_id = result.image.property_gallery_id;
                $("#image_id").val(image_id);
                var image_name = result.image.image_name;
                $("#image_name").val(image_name);
                var image_desc = result.image.image_desc;
                $("#image_desc").val(image_desc);

                var image_status = result.image.status;

                if (eval(image_status) == 1)
                {
                    $('.bootstrap-switch-id-image_status .bootstrap-switch-container').css('margin-left','0px');
                }
                else 
                {
                     $('.bootstrap-switch-id-image_status .bootstrap-switch-container').css('margin-left','-60px');
                }  
                 $('#image_status').attr('checked',true);

                $(".image_error").html('');
                $(".image_name_error").html('');
                var image = result.image.image;
                var child = $("#picture_file_field_image").find("input[type=text]")
                //alert(child);
                child.val(image);
                 


            }
        });
    });


});
/*list investor data */
function list_image_data() {
 
    $.post("<?php echo site_url('property/list_image'); ?>", {property_id: property_id}, function (result) {

        $('#load-image-list').html(result);

    });
}
/*call before investor data inserted for merging file image and form data */
function before_image(formData, jqForm, options) {

    var data = $('#image-box').closest('.create-campain-bg').find('input,textarea,select').serializeArray();
    formData.append(data);
    var queryString = $.param(formData);
    return true;
}

// Called after successfully save investor data
function after_image(result) {

    PopupClose();
    var error = result.error;
    var msg = result.msg;
    var url = result.url;
    if (url != '') {
        location.href = url;
    }
    else if (error == true) {
        var media_image_title_error = result.media_image_title_error;
        $(".media_image_title_error").html(media_image_title_error);
        var media_image_name_error = result.media_image_name_error;
        $(".media_image_name_error").html(media_image_name_error);
        $(".image_name_error").html(result.image_name_error);
        $(".image_error").html(result.image_error);
        $(".image_desc_error").html(result.image_desc_error);
    }
    else {
        $("#ajax-image-cancel").hide();
        $('#image-box').closest('.create-campain-bg').find('input,textarea,select').val('');
        $('#image_status').val(1);
        $('#image-box').closest('.create-campain-bg').find("#property_id").val(property_id);
        //$("#property_id").val(property_id);
        $("#ajax-image-add").html("<?php echo ADD_IMAGE;?>");
        $('.bootstrap-switch-id-image_status .bootstrap-switch-container').css('margin-left','0px');
        $('#image_status').attr('checked',true);
        $(".image_error").html('');
        $(".image_name_error").html('');
        $(".image_desc_error").html('');
        list_image_data();

    }


}
function BlankImageData() {
    $('#image-box').closest('.create-campain-bg').find('input,textarea,select').val('');

    $('#image-box').closest('.create-campain-bg').find("#property_id").val(property_id);
    //$("#property_id").val(property_id);
    $("#ajax-image-add").html("<?php echo ADD_IMAGE;?>");
    $("#ajax-image-cancel").hide();
    $(".image_error").html('');
    $(".image_name_error").html('');
      $(".image_desc_error").html('');
   $('.bootstrap-switch-id-image_status .bootstrap-switch-container').css('margin-left','0px');
    $('#image_status').attr('checked',true);
    list_image_data();
}


</script>
     


