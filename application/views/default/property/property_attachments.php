<script src="<?php echo base_url(); ?>js/jquery.form.js"></script>
<style type="text/css">
  .btnApprove {
    background-color: #78cd51;
}
.btn-pd{
 padding: 2px 6px;
 font-size: 12px;
}
</style>
  <section>
  	<div class="db-head">
    	<div class="container">
    		<div class="page-title">
        	<h2>Add Propety Details</h2>
        </div>
    	</div>
  	</div>
	  <div class="grey-bg" id="section-area">	  		
      <div class="container">
	      <div class="row">
		      <div class="col-md-3 faqs-left">
		      	<?php echo $this->load->view('default/property/property_sidebar');?>
		      </div>
          <div class="col-md-9 add-pro-right">
        		<div class="row">
              <div id="property-detail" class="create-campain-bg propety-area clearfix">
                <div class="form-heading">
                  <h3>Attachments</h3>
                </div>
                <div class="property-detail-body row" id="video-box">
                  <?php
                    $attributes = array(
                        'id' => 'attachform',
                        'name' => 'attachform',
                        'enctype' => 'multipart/form-data',
                        'class' => 'edit_project dec-form',
                        'method' => 'post',
                        'accept-charset' => 'UTF-8'
                    ); echo form_open_multipart('property/atatchment_save/', $attributes);
                  ?>

                  <div class="form-group col-md-6 browse-upload-btn" id="picture_file_field_image">
                    <label for="">Browse</label>
                    <input type="file" class="filestyle form-control project_picture" data-iconName="glyphicon glyphicon-file" data-buttonName="btn-primary" data-buttonText="Add Attachment" name="attachfile" id="attachfile" accept="application/pdf">  
                    <span class="image_error create_step_error"></span>
                    <input type="hidden" name="propertyid" value="<?php echo $id; ?>" id="propertyid">
                  </div>  
                </form>
                
              </div>
           
            <div id="property-detail" class="create-campain-bg propety-area image-gallery clearfix" style="padding:10px">
              <table class="table table-condensed table-bordered">
                <thead>
                  <th>File</th>
                  <th>Status</th>
                  <th>Action</th>
                </thead>
                <tbody>
                  <?php 
                    if($attachments!=null){ ?>
                      <?php foreach ($attachments as $value) { ?>
                        <tr>
                          <td>
                            <a target="_blank" href="<?php echo site_url('upload/property/attachments/'.$value["new_name"]); ?>"><i class="fa fa-file-text-o"></i> <?php echo str_replace(".pdf", "",$value["file_name"]); ?> </a>
                          </td>
                          <td><?php if($value['isdelete']==0){ ?><label class="label label-primary">Active</label><?php } else { ?><label class="label label-danger">Inactive</label><?php } ?></td>
                          <td>
                            <?php if($value['isdelete']==0){ ?>
                             <button class="btn btn-xs btn-pd btn-danger" onclick="setval('<?php echo $value["pattachid"]; ?>',1)"><i class="glyphicon glyphicon-remove"></i></button>
                            <?php } else { ?>
                             <button class="btn btn-xs btn-pd btn-success" onclick="setval('<?php echo $value["pattachid"]; ?>',0)"><i class="glyphicon glyphicon-ok"></i></button><?php } ?>
                          </td> 
                      <?php } ?>
                  <?php } else { ?>
                    <tr><td colspan="3">There are no attachments.</td></tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
            <div class="col-md-12 form-group  addpro-nxt">
                  <a  href="<?php echo site_url('property/launch_property/'.$id);?>" type="button" class="btn btn-green btn-small text-uppercase pull-right">Finished</a>
                </div>
             </div>
		      </div>
        </div>
      </div>
    </section>
   

	
<script src="<?php echo base_url();?>js/bootstrap-switch.js"></script>
	
<script type="text/javascript">

 $("[type='checkbox']").bootstrapSwitch();


$('#attachfile').change(function() {
  $('#attachform').submit();
});
var url = "<?php echo site_url('property/updateattach/'); ?>";
var id = "<?php echo $id ?>";
function setval(pattachid,isdelete) {
  // alert(pattachid);
  window.location.href = url + "/"+pattachid+"/"+isdelete+"/"+id;
} 
</script>


