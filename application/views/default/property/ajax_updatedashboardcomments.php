<?php
//print_r($project_comments);
$site_sett = $site_setting['date_format'];

$is_allow_comments = true;
$is_allow_comments = $is_property_owner;
if (is_array($property_comments)) {  //echo '<pre/>';print_r($project_comments);exit;
    foreach ($property_comments as $comments) {
        //$getchild = getAllchildofcomment(39)
        $comment_user_id = $comments['user_id'];

        $comment = SecureShowData($comments['comments']);
        $user = $comments['user_name'] . ' ' . $comments['last_name'];
        $date = $comments['date_added'];
        $ip = $comments['comment_ip'];
        $comment_id = $comments['comment_id'];
        $status = comment_status_name($comments['status']);
        $comment_type = $comments['comment_type'];
        $comment_type_text = '';
        if ($comment_type == 1) $comment_type_text = "Private Comment";

        $project_comment_image = $comments['image'];
        $date = dateformat($comments['date_added']);

        $statuses = array(1);
        $show_approve = 'yes';
        if (in_array($comments['status'], $statuses)) {
            $show_approve = 'no';
        }

        $statuses = array(2);
        $show_decline = 'yes';
        if (in_array($comments['status'], $statuses)) {
            $show_decline = 'no';
        }

        $statuses = array(3);
        $show_spam = 'yes';
        if (in_array($comments['status'], $statuses)) {
            $show_spam = 'no';
        }

        $statuses = array(1);
        $show_reply = 'no';
        if (in_array($comments['status'], $statuses)) {
            $show_reply = 'yes';
        }

        $statuses = array(1);
        $show_edit = 'no';
        if (in_array($comments['status'], $statuses)) {
            $show_edit = 'no';
        }
        if ($comment_user_id == $this->session->userdata('user_id')) {
            $show_edit = 'yes';
        }

        if ($is_allow_comments == false) {

            $show_edit = 'no';
            $show_reply = 'no';
            $show_decline = 'no';
            $show_spam = 'no';
            $show_approve = 'no';
        }
        ?>
        <?php $comment_status_class = "";
        if ($status == PENDING) {
            $comment_status_class = "pending-comment-status";
        }
        if ($status == APPROVED) {
            $comment_status_class = "approved-comment-status";
        }
        if ($status == DECLINE) {
            $comment_status_class = "decline-comment-status";
        }
        if ($status == SPAM) {
            $comment_status_class = "sapm-comment-status";
        }
        //$getchild = getAllchildofcomment($comments['comment_id']);echo '<pre/>';print_r($getchild);

        ?>

        <li class="media">
        <div class="media-left">
            <?php    if (is_file("upload/user/user_small_image/" . $project_comment_image)) {
                ?>
                <a href="<?php echo site_url('user/' . $comment_user_id); ?>"><img class="img-thumbnail"
                                                                                   src="<?php echo base_url() . 'upload/user/user_small_image/' . $project_comment_image; ?>"></a>

            <?php
            } else {
                ?>
                <a href="<?php echo site_url('user/' . $comment_user_id); ?>"><img class="img-thumbnail"
                                                                                   src="<?php echo base_url() . 'upload/user/user_small_image/no_man.jpg'; ?>"></a>

            <?php } ?>
        </div>
        <div class="media-body">
        <h4 class="media-heading">
            <a href="<?php echo site_url('user/' . $comment_user_id); ?>"><?php echo $user ?></a>
        </h4>

        <p><span
                class="comment-status <?php echo $comment_status_class; ?>"><?php echo $status; ?></span> <?php echo IP; ?>
            : <?php echo $ip; ?></p>

        <p class="private-comment"><?php echo $comment_type_text; ?></p>

        <p class="comment-text"><i class="fa fa-quote-left"></i>  <?php echo $comment; ?></p>

        <p><span class="media-time"><?php echo $date; ?></span></p>
        <?php
        $getchild = getAllchildofcomment($comments['comment_id']);
        //echo "<pre>";
        //print_r($getchild);
        if ($getchild) {
            foreach ($getchild as $gchild) {

                $child_statuses = array(1);
                $child_show_edit = 'no';
                if (in_array($gchild->status, $child_statuses)) {
                    $child_show_edit = 'no';
                }
                if ($gchild->user_id == $this->session->userdata('user_id')) {
                    $child_show_edit = 'yes';
                }

                if ($is_allow_comments == false) {

                    $child_show_edit = 'no';;
                }
                // $child_comment_user_slug = $gchild->profile_slug;
                $child_comment_image = $gchild->image;
                $child_comment_name = $gchild->user_name . ' ' . $gchild->last_name;
                $child_comment_date_time = dateformat($gchild->date_added);
                //$child_comment=$gchild->comments;
                $child_comment = SecureShowData($gchild->comments);
                $child_comment_hidden = $gchild->comments;
                $child_comment_id = $gchild->comment_id;
                $child_comment_user_id = $gchild->user_id;
                $child_comment_type = $gchild->comment_type;

                $child_ip = $gchild->comment_ip;

                if ($child_comment_image != '' && is_file("upload/user/user_small_image/" . $child_comment_image)) {
                    $childsrc = base_url() . 'upload/user/user_small_image/' . $child_comment_image;
                } else {
                    $childsrc = base_url() . 'upload/user/user_small_image/no_man.jpg';
                }


                ?>
                <div class="media">
                    <div class="media-left">
                        <a href="javascript://">
                            <img class="img-thumbnail" src="<?php echo $childsrc; ?>">
                        </a>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading marB5"><a
                                href="javascript://"><?php echo $child_comment_name; ?></a></h4>

                        <p> <?php echo IP; ?>: <?php echo $child_ip; ?></p>

                        <p class="comment-text"><i class="fa fa-quote-left"></i> <?php echo $child_comment; ?>
                        </p>

                        <p><span class="media-time"> <?php echo $child_comment_date_time; ?></span></p>
                    </div>
                    <div class="delete-comment">


                        <?php
                        if ($child_show_edit == 'yes') {
                            ?>
                            <a href="javascript://" data-toggle="tooltip" data-placement="bottom"
                               title="<?php echo EDIT; ?>"
                               onclick="show_textarea(<?php echo $child_comment_id; ?>, 'edit')"><i
                                    class="glyphicon glyphicon-edit"></i></a>
                        <?php
                        }
                        ?>

                        <?php
                        if ($is_allow_comments == true) {
                            ?>
                            <a href="javascript://" data-toggle="tooltip" data-placement="bottom"
                               title="<?php echo DELETE; ?>"
                               onclick="commentaction(<?php echo $child_comment_id; ?>,'delete');"><i
                                    class="glyphicon glyphicon-trash"></i></a>
                        <?php
                        }
                        ?>

                    </div>

                </div>

                <div id="reply_<?php echo $child_comment_id; ?>" class="comment-reply">
                    <div class="in-form">
                        <textarea name="reply_comment" id="reply_comment_<?php echo $child_comment_id; ?>"
                                  class="form-control" rows="5" data-fieldlength="500"></textarea>
                    </div>
                    <div class="m-t-md btngrp">
                        <a href="javascript://" class="btn btn-primary"
                           id="edit_comment_<?php echo $child_comment_id; ?>"
                           onclick="commentaction(<?php echo $child_comment_id; ?>,'reply','edit');"><?php echo POST_REPLY; ?></a>
                        <a href="javascript://" class="btn btn-primary gry"
                           onclick="hide_textarea(<?php echo $child_comment_id; ?>)"><?php echo CANCEL; ?></a>
                    </div>
                </div>

                <input type="hidden" name="comment_<?php echo $child_comment_id; ?>"
                       id="comment_<?php echo $child_comment_id; ?>"
                       value="<?php echo $child_comment_hidden; ?>">
                <input type="hidden" name="comment_type_<?php echo $child_comment_id; ?>"
                       id="comment_type_<?php echo $child_comment_id; ?>"
                       value="<?php echo $child_comment_type; ?>">
                <input type="hidden" name="comment_ip_<?php echo $child_comment_id; ?>"
                       id="comment_ip_<?php echo $child_comment_id; ?>" value="<?php echo $child_ip; ?>">
                <input type="hidden" name="comment_user_id_<?php echo $child_comment_id; ?>"
                       id="comment_user_id_<?php echo $child_comment_id; ?>"
                       value="<?php echo $child_comment_user_id; ?>">
            <?php
            }

        }
        ?>

        <input type="hidden" name="comment_<?php echo $comment_id; ?>" id="comment_<?php echo $comment_id; ?>"
               value="<?php echo $comment; ?>">
        <input type="hidden" name="comment_type_<?php echo $comment_id; ?>"
               id="comment_type_<?php echo $comment_id; ?>" value="<?php echo $comment_type; ?>">
        <input type="hidden" name="comment_ip_<?php echo $comment_id; ?>"
               id="comment_ip_<?php echo $comment_id; ?>" value="<?php echo $ip; ?>">
        <input type="hidden" name="comment_user_id_<?php echo $comment_id; ?>"
               id="comment_user_id_<?php echo $comment_id; ?>" value="<?php echo $comment_user_id; ?>">

        <div class="delete-comment">
            <?php
            if ($show_approve == 'yes') {
                ?>
                <a href="javascript://" data-toggle="tooltip" data-placement="bottom"
                   title="<?php echo APPROVED_BTN; ?>"
                   onclick="commentaction(<?php echo $comment_id; ?>,'approved');"><i
                        class="glyphicon glyphicon-ok"></i></a>
            <?php
            }
            ?>

            <?php
            if ($show_reply == 'yes' && ($comment_user_id != $this->session->userdata('user_id'))) {
                ?>
                <a href="javascript://" data-toggle="tooltip" data-placement="bottom"
                   title="<?php echo REPLY; ?>" onclick="show_textarea(<?php echo $comment_id; ?>,'')"><i
                        class="glyphicon glyphicon-retweet"></i></a>
            <?php
            }
            ?>
            <?php
            if ($show_decline == 'yes' && ($comment_user_id != $this->session->userdata('user_id'))) {
                ?>
                <a href="javascript://" data-toggle="tooltip" data-placement="bottom"
                   title="<?php echo DECLINE; ?>"
                   onclick="commentaction(<?php echo $comment_id; ?>,'decline');"><i
                        class="glyphicon glyphicon-remove"></i></a>
            <?php
            }
            ?>
            <?php
            if ($show_edit == 'yes') {
                ?>
                <a href="javascript://" data-toggle="tooltip" data-placement="bottom"
                   title="<?php echo EDIT; ?>" onclick="show_textarea(<?php echo $comment_id; ?>, 'edit')"><i
                        class="glyphicon glyphicon-edit"></i></a>
            <?php
            }
            ?>

            <?php
            if ($is_allow_comments == true) {
                ?>
                <a href="javascript://" data-toggle="tooltip" data-placement="bottom"
                   title="<?php echo DELETE; ?>"
                   onclick="commentaction(<?php echo $comment_id; ?>,'delete');"><i
                        class="glyphicon glyphicon-trash"></i></a>
            <?php
            }
            ?>
            <?php
            if ($show_spam == 'yes' && $show_edit == 'no') {
                ?>
                <a href="javascript://" data-toggle="tooltip" data-placement="bottom"
                   title="<?php echo REPORT_SPAM; ?>"
                   onclick="commentaction(<?php echo $comment_id; ?>,'spam');"><i class="fa fa-exclamation"></i></a>
            <?php
            }
            ?>
        </div>
        </div>
        <div id="reply_<?php echo $comment_id; ?>" class="comment-reply">
            <div class="in-form">
                <textarea name="reply_comment" id="reply_comment_<?php echo $comment_id; ?>" class="form-control"
                          rows="5" data-fieldlength="500"></textarea>
            </div>
            <div class="m-t-md btngrp">
                <a href="javascript://" class="btn btn-primary " id="edit_comment_<?php echo $comment_id; ?>"
                   onclick="commentaction(<?php echo $comment_id; ?>,'reply','edit');"><?php echo POST_REPLY; ?></a>
                <a href="javascript://" class="btn btn-primary gry"
                   onclick="hide_textarea(<?php echo $comment_id; ?>)"><?php echo CANCEL; ?></a>
            </div>
        </div>
        </li>
    <?php
    }
} else {
    ?>
    <li class="no-data"><?php echo THERE_IS_NO_COMMENT_FOR_THIS_PROJECT; ?>.</li>
<?php } ?>
