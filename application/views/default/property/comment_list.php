<?php

if ($comment > 0) {
    foreach ($comment as $property_comment) {
        $property_comment_id = trim($property_comment['comment_id']);
        $property_comment_user_id = trim($property_comment['user_id']);
        $property_comment_status = $property_comment['status'];
        $property_comment_type = $property_comment['comment_type'];
        $property_comment_image = $property_comment['image'];
        $property_comment_name = $property_comment['user_name'] . ' ' . $property_comment['last_name'];
        $property_comment_date_time = getDuration($property_comment['date_added']);
        $property_comment = SecureShowData($property_comment['comments']);


        if (!empty($property_comment['profile_slug'])) {
            $property_comment_user_slug = $property_comment['profile_slug'];
        } else {
            $property_comment_user_slug = $property_comment_user_id;
        }

        if ($property_comment_type == 1 or $property_comment_type == "1") {
            $property_comment_status = 5;
            if ($this->session->userdata('user_id') != "") {
                if ($property_comment_user_id == $this->session->userdata('user_id')) {
                    $property_comment_status = 1;
                }
                if ($projec_user_id == $this->session->userdata('user_id')) {
                    $property_comment_status = 1;
                }
            } else {
                $property_comment_status = 5;
            }
        }
        if ($property_comment_image != '' && is_file("upload/user/user_small_image/" . $property_comment_image)) {
            $propertysrc = base_url() . 'upload/user/user_small_image/' . $property_comment_image;
        } else {
            $propertysrc = base_url() . 'upload/user/user_small_image/no_man.jpg';
        }
        if ($property_comment_status == 1) {
            ?>
           <li>
                        <div class="comments-lt">
                       <a href="<?php echo site_url('user/' . $property_comment_user_slug); ?>"> <img src="<?php echo $propertysrc;?>" alt=""></a>
                        </div>
                         <div class="comments-rt">
                        <a href="<?php echo site_url('user/' . $property_comment_user_slug); ?>"> <h3><?php echo $property_comment_name;?></h3></a>
                         <p><span class="investor-time">Said <?php echo $property_comment_date_time;?></span></p>
                        
                         <p class="comment-text">
                         <i class="fa fa-quote-left"></i>  <?php echo $property_comment;?>   </p>
                         </div>
                        </li>  
        <?php
        }

    }

    ?>

    <?php

    if ($total_row_display <= 5) {
        $display = 'none';
    } else {
        $display = 'block';
    }
    ?>
    <li class="show-more more_comments" style="display:<?php echo $display; ?>"><a href="javascript:void(0)"
                                                                                   onclick="show_more_comments(<?php echo $offset + 5; ?>)">Show
            more comments</a></li>

<?php
} else {
    ?>
    <li class="no-data"><?php echo THERE_IS_NO_COMMENT_FOR_THIS_PROJECT; ?>.</li>
<?php
}
?>
								
