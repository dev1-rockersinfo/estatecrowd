
    <section>
	  	<div class="db-head">
	      	<div class="container">
	      		<div class="page-title">
		        	<h2>Following Properties</h2>
		        </div>
	      	</div>
	  	</div>
	  	<div class="grey-bg" id="section-area">	  		
	      	<div class="container">
	      		<div class="row">
		      		<div class="col-md-3 db-fp-left">
                   <?php echo $this->load->view('default/dashboard_sidebar'); ?>
		      			
		      		</div>
		      		<div class="col-md-9 db-fp-right">
           <ul class="db-fp-list" id="propertyfollowers">
                <?php 

       
           if($property_following) 
           {
                foreach($property_following as $property)
                {

                  $data['property'] = $property;
                
                  $this->load->view('default/following/common_card', $data);
          
              }
            }
            else
            { ?>
          <li class="no_record_found">There are no following property.</li>
          <?php  }
            ?>
                 
             
                 
                 
              
            </ul>
			      		
	      		</div>
	      	</div>
	  	</div>
    </section>
    
  <script type="text/javascript">
       function unfollowproperty(id) {
        $('#followme_property').removeAttr('onmouseover').removeAttr('onmouseout');
        if (id == '') {
            return false;
        }

        if (window.confirm('Are you sure you want to unfollow this property?'))
        {
                 var strURL = '<?php echo base_url().'property/property_unfollower';?>/' + id;
             PopupOpen();
            var xmlhttp;
            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            } else { // code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                     PopupClose();
                   
                    savefollowerlist(id);
                   
                }
            }
            xmlhttp.open("GET", strURL, true);
            xmlhttp.send();
        }
        else
        {
            // They clicked no
        }
      
    }

     function savefollowerlist(id) {
          var user_id_value = '';
          //alert(user_id_value);
          var mapdata = {"user_id": user_id_value};
          // var mapdata = "'perk_title=' + perk_title_value" ;
          //var mapdata_description = 'perk_description=' + perk_description_value;
          var getStatusUrl = '<?php echo site_url('property/ajax_property_follower_list');?>/'+id;
          ////alert(getStatusUrl)
          $.ajax({
              url: getStatusUrl,
              dataType: 'text',
              type: 'POST',
              timeout: 99999,
              global: false,
              data: mapdata,
              //data:{'perk[]':mapdata},
              success: function (data) {
                  
                  document.getElementById('propertyfollowers').innerHTML = data;
              },
              error: function (XMLHttpRequest, textStatus, errorThrown) {
              }
          });
    }

  </script>
  