
<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>

<?php 
  $suburb = '';
  $investment_amount ='';
  $amount_select = '';
  if(isset($_GET['investment_amount']))
  {
    $investment_amount = $_GET['investment_amount'];
    if($investment_amount != '')
    {
      $amount_select = 'selected';
    }
  }
  $investment_suburb='';
  $suburb_select = '';
  if(isset($_GET['investment_suburb']))
  {
    $investment_suburb = $_GET['investment_suburb'];
    if($investment_suburb != '')
    {
        $suburb_select = 'selected';
    }
  }
  $investment_address = '';
  $address_select = '';
  if(isset($_GET['investment_address']))
  {
    $investment_address = $_GET['investment_address'];
  
    if($investment_address != '')
    {
      $address_select = 'selected';
    }
  }

?>
<form class="ec-form" action="<?php echo site_url('search/property')?>">
<section>
<div class="search-head-ex">
<div class="container">
<div class="page-title">
<h2>Search property </h2>
</div>
</div>
</div>
<div class="adv-search-ex">
<div class="container">
<div class="row">
<div class="col-md-12">
  
      <div class="input-group col-md-12" id="advance-search-input">
                            
                  <input type="text" class="form-control" id="investment_amount" name="investment_amount"  placeholder="how much do you want to invest?" value="<?php echo $investment_amount; ?>" style="display: <?php if($investment_amount == ''){ echo 'none';} ?>">  

                  <input type="text" class="form-control" id="investment_suburb" name="investment_suburb" style="display: <?php if($investment_suburb == ''){ echo 'none';} ?>" placeholder="What suburb do you want to invest in?" onkeyup="autotext_suburb();" value="<?php echo $investment_suburb ; ?>">

                   <div id="autoc_suburb" class="in-autocomplete"></div> 

                   <input type="text" class="form-control" id="investment_address" name="investment_address" style="display: none" placeholder="Enter address of investment property..."  style="display: <?php if($investment_address == ''){ echo 'none';} ?>" value="<?php echo $investment_address; ?>" >

                   <div id="locationMap" class="gmap3 google-map"></div>
                            </div>
     

</div>        
</div>    
</div>
</div>  
</section>

<section>

<div class="search-det-ex" id="section-area">	  		
<div class="container">
<div class="row">	      	
<div class="col-md-12 ">  
<div class="">

<div class="row">
<div class="form-group col-md-6 search-slider-ex">
<label for="f-name">Include Suburbs Within </label>
<div class="slider col-md-12">
   <input id="ex5" type="text" data-slider-min="1" data-slider-max="15" data-slider-step="1" data-slider-value="<?php echo $suburb; ?>";" name="suburb" />
    </div>
</div>
<div class="form-group col-md-6">
  <div class="select-search" id="searchType ">
                 <label for="">Bedrooms </label>   
                  <select class="selectpicker" name="bedrooms">
                   <option data-tokens="" value="1">1 </option>
                   <option data-tokens="" value="2">2 </option>
                   <option data-tokens="" value="3">3 </option>
                   <option data-tokens="" value="4">4 </option>
                   <option data-tokens="five" value="5">5+</option>             
                  </select>          
                </div>
</div>
</div>   
<div class="row">
<div class="form-group col-md-6">
    <div class="select-search " id="searchType">
                    <label for="">Bathrooms </label>   
                  <select class="selectpicker" name="bathrooms">
                   <option data-tokens="" value="1">1 </option>
                   <option data-tokens="" value="2">2 </option>
                   <option data-tokens="" value="3">3 </option>
                   <option data-tokens="" value="4">4 </option>
                   <option data-tokens="five" value="5">5+</option>             
                  </select>   
              
                </div>
                </div>
<div class="form-group col-md-6">
<div class="select-search" id="searchType">
                  <label for="">Cars </label>   
                    <select class="selectpicker" name="cars">
                   <option data-tokens="" value="1">1 </option>
                   <option data-tokens="" value="2">2 </option>
                   <option data-tokens="" value="3">3 </option>
                   <option data-tokens="" value="4">4 </option>
                   <option data-tokens="five" value="5">5+</option>             
                  </select>   
                </div>
</div>
</div>
<div class="row">
<div class="col-md-6 add-pro-select">
             <label for=""> Investment Range</label>
            <div class="row form-group">
            <div class="col-md-6 form-group ">
            <input type="text" id="public-field" name="min_property_investment_range" class="form-control" placeholder="Min">       </div>
            <div class="col-md-6 form-group ">
            <input type="text" id="public-field" name="max_property_investment_range" class="form-control" placeholder="Max">      </div>
            </div>
            </div>
 

<div class="form-group col-md-6">
  <label for="">Property Types </label>   
                 <select class="selectpicker" name="property_type">
                    <option value="House">House</option>
                    <option value="Apartment and Unit">Apartment & Unit</option>
                    <option value="Townhouse">Townhouse</option>
                    <option value="Villa">Villa</option>
                    <option value="Acreage">Acreage</option>
                    <option value="Rural">Rural</option>
                    <option value="Unit"> Block Of Units</option>
                    <option value="Retirement Living">Retirement Living</option>

                  </select>
</div>

</div>
<div class="row">

<div class="form-group col-md-6">
<label for="address">Address <span class="form_star">*</span></label>
<input type="text" placeholder="" name="property_address" id="property_address" class="form-control ">
  <div id="locationMap" class="gmap3 google-map"></div>
</div>
<div class="form-group col-md-6">
<label for="p-code">Postcode <span class="form_star">*</span></label>
<input type="text" placeholder="" name="property_postcode" id="property_postcode" class="form-control"  onkeyup="autotext();" >
                                <div id="autoc" class="in-autocomplete"></div>  
</div>               
</div>
<div class="row">
<div class="col-md-12">
<a class="btn btn-default  btn-block  " type="button" href="<?php echo site_url('search/advance_search');?>">Clear Filters</a> 
</div>
</div>

<p> &nbsp;</p>




<div class="search-form__footer">
<div class="container">
<button class="btn btn-primary btn-lg btn-block text-uppercase " type="submit">Search</button> 
</div>

</div>


</div>
</div>
</div>
</div>
</section>
</form>
<script src="<?php echo base_url();?>js/bootstrap-slider.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery.tokenize.js"></script>

<script type="text/javascript">
  
     $(document).ready(function(){
    $(".dropdown").hover(            
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeIn("slow");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");                
            },
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeOut("slow");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");                
            });
    });

     
</script>  
<script type="text/javascript">
$('#chk-box,#chk-box1').on('change', function(){ // on change of state
if(this.checked) // if changed state is "CHECKED"
{
$( "#chk-info" ).show();
}else {
$( "#chk-info" ).hide();
}
})

$('#tokenize').tokenize(); 
 
 $('select#tokenize_placeholder').tokenize({placeholder:"<span> &#xf002; </span> Enter the suburb or postcode"});
  $("#ex5").slider();
 
</script>
<script type="text/javascript">
  

 $("#autoc").hide();

    function autotext() {
        var xmlHttp;
        try {
            xmlHttp = new XMLHttpRequest();// Firefox, Opera 8.0+, Safari
        }
        catch (e) {
            try {
                xmlHttp = new ActiveXObject("Msxml2.XMLHTTP"); // Internet Explorer
            }
            catch (e) {
                try {
                    xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                catch (e) {
                    //alert("<?php //echo NO_AJAX;?>");
                    return false;
                }
            }
        }
        xmlHttp.onreadystatechange = function () {
            if (xmlHttp.readyState == 4) {
                document.getElementById('autoc').innerHTML = xmlHttp.responseText;

                if (document.getElementById('srhdiv_postcode')) {


                  $("#autoc").show();


                }
                else {
                   $("#autoc").hide();
                    
                }
            }
        }
        var text = document.getElementById('property_postcode').value;
        text = text.replace(/[^a-zA-Z0-9]/g, '');
        xmlHttp.open("GET", "<?php echo site_url('property/search_auto_postcode');?>/" + text, true);
        xmlHttp.send(null);
    }
    function selecttext(el) {

        var selectvalue = el.innerHTML;
       
        $("#property_postcode").val(selectvalue);
        $("#autoc").hide();
       
    }

$(window).bind("load", function () {
    $('#dvLoading').fadeOut(2000);
    $('#mainbg_div').fadeIn(2000);
    $('.min_height').css('minHeight', 0);
    $(function () {
        $('#locationMap').gmap3();

        $('#property_address').autocomplete({
            source: function () {
                $("#locationMap").gmap3({
                    action: 'getAddress',
                    address: $(this).val(),
                    callback: function (results) {
                        if (!results) return;
                        $('#property_address').autocomplete(
                            'display',
                            results,
                            false
                        );
                    }
                });
            },
            cb: {
                cast: function (item) {

                    return item.formatted_address;
                },
               
            }


        });




    });

});


    $("#autoc_suburb").hide();

    function autotext_suburb() {
        var xmlHttp;
        try {
            xmlHttp = new XMLHttpRequest();// Firefox, Opera 8.0+, Safari
        }
        catch (e) {
            try {
                xmlHttp = new ActiveXObject("Msxml2.XMLHTTP"); // Internet Explorer
            }
            catch (e) {
                try {
                    xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                catch (e) {
                    //alert("<?php //echo NO_AJAX;?>");
                    return false;
                }
            }
        }
        xmlHttp.onreadystatechange = function () {
            if (xmlHttp.readyState == 4) {
                document.getElementById('autoc_suburb').innerHTML = xmlHttp.responseText;

                if (document.getElementById('srhdiv')) {


                  $("#autoc_suburb").show();


                }
                else {
                   $("#autoc_suburb").hide();
                    
                }
            }
        }
        var text = document.getElementById('investment_suburb').value;
        text = text.replace(/[^a-zA-Z0-9]/g, '');
        xmlHttp.open("GET", "<?php echo site_url('search/search_auto');?>/" + text, true);
        xmlHttp.send(null);
    }
    function selecttext_suburb(el) 
    {

        var selectvalue = el.innerHTML;
       
        $("#investment_suburb").val(selectvalue);
        $("#autoc_suburb").hide();
       
    }
</script>
<script type="text/javascript">
$(document).ready(function(){
  $( ".slider-horizontal" ).mouseup(function() {
    // alert($("ex5").val());
// function sliderval(){
    var styledata = $(".slider-handle").attr('style');
    // $( "").text();
    var perce = styledata.split(":");
    var percentage = perce[1].replace("%;","");

    var calcu = Math.round((percentage*15)/100);
    if(parseFloat(percentage)<parseFloat(50)){
      calcu = parseInt(calcu)+parseInt(1);
    }
    // alert(calcu);
    $("#ex5").attr("data-slider-value",calcu);
    // $("#ex5").val(calcu);
    $("#sliderHandleVal").text(calcu);

    // var paramName = "suburb";
    // var paramValue = calcu;

    // var url = window.location.href;
    // // paramValue = paramValue.replace("$", "");
    // // paramValue= paramValue.replace("M", "");

    // var hash = location.hash;
    // url = url.replace(hash, '');
    // if (url.indexOf(paramName + "=") >= 0)
    // {
    //     var prefix = url.substring(0, url.indexOf(paramName));
    //     var suffix = url.substring(url.indexOf(paramName));
    //     suffix = suffix.substring(suffix.indexOf("=") + 1);
    //     suffix = (suffix.indexOf("&") >= 0) ? suffix.substring(suffix.indexOf("&")) : "";
    //     url = prefix + paramName + "=" + paramValue + suffix;
    //      //url = prefix + paramName + "=" + paramValue;
    // }
    // else
    // {
    //   if (url.indexOf("?") < 0)
    //       url += "?" + paramName + "=" + paramValue;
    //   else
    //       url += "&" + paramName + "=" + paramValue;
    // }
   
    // window.location.href = url + hash;
    // // alert(url + hash);


  // }
  });
});
</script>

<script type="text/javascript" src="http://code.jquery.com/jquery-migrate-1.0.0.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/map/gmap3.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/map/jquery-autocomplete.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/map/jquery-autocomplete.css"/>