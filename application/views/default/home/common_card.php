<?php

                    $property_id = $property['property_id'];
                    $property_user_id = $property['user_id'];
                    $property_address = $property['property_address'];
                    $property_url = $property['property_url'];
                    $status =0;
                    if(isset( $property['campaign_status']))$status = $property['campaign_status'];
                    if(isset( $property['status']))$status = $property['status'];
                    $my_campaign='';
                    if(isset($property['my_campaign']))
                    {
                        $my_campaign = $property['my_campaign'];
                    }
                    $is_delete = 'no';
                    if($status == 0 or $status == 1)
                    {
                        $is_delete ='yes';
                    }
                    if(is_file(base_path().'upload/property/commoncard/'.$property['cover_image']) && $property['cover_image'] != '')
                    {
                         $cover_image = base_url().'upload/property/commoncard/'.$property['cover_image'];
                    }
                    else
                    {
                        $cover_image = base_url().'upload/property/commoncard/no_img.jpg';
                    }

                    $status_class = '';
                   
                    if ($status == "0") {
                        $status_class = "draft-status";
                    }
                    if ($status == "1") {
                        $status_class = "pending-status";
                    }
                    if ($status == "2") {
                        $status_class = "active-status";
                    }
                    if ($status == "3") {
                        $status_class = "successful-status";
                    }
                    if ($status == "4") {
                        $status_class = "consol-status";
                    }
                    if ($status == "5") {
                        $status_class = "failure-status";
                    }
                    if ($status == "6") {
                        $status_class = "decline-status";
                    }
                    if ($status == "7") {
                        $status_class = "pending-status";
                    }
                     if ($status == "8") {
                       
                        $status_class = "failure-status";
                    }

            ?>
                  <li class="col-md-4"> 
                  <div class="db-mc-wrap">
                        <div class="db-mc-thumb">

                        <a href="<?php echo site_url('properties/'.$property_url);?>"><img src="<?php echo $cover_image;?>"></a>
                        <span class="overlay"></span>
                        <span class="lbl <?php echo $status_class; ?>"><?php echo property_status($status);?></span>
                        </div>
                        <div class="db-mc-title">
                        <?php if($my_campaign == 'my_campaign') {  ?>

                            <h3><a href="<?php echo site_url('property/dashboard/'.$property_id);?>"><?php echo $property_address;?></a></h3>
                        <?php } else { ?>


                            <h3><a href="<?php echo site_url('properties/'.$property_url);?>"><?php echo $property_address;?></a></h3>
                            <?php } ?>
                        
                        </div>
                        
                        <div class="db-mc-btn">
                        <ul class="db-mc-btn-grp">
                                <li><a href="<?php echo site_url('properties/'.$property_url);?>"><i class="fa fa-eye"></i> View</a></li>
                                <li><a href="<?php echo site_url('property/add_property/'.$property_id)?>"><i class="fa fa-pencil-square-o"></i> Edit</a></li>
                                <?php if($is_delete == 'yes') {?>
                                <li><a href=""><i class="fa fa-trash"></i> Delete</a></li>
                                <?php }  else {?>
                                 <li><a href="javascript://" class="disbaled"><i class="fa fa-trash"></i> Delete</a></li>
                                <?php } ?>
                               

                        </ul>   
                        </div>
                        
                        
                  </div>
                  </li>