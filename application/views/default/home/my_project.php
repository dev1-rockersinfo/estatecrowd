<div class="min_height" id="mainbg_div">
    <section>
        <div class="content_part">
            <div class="main my_pro_list">
                <?php
                if (is_array($latest_projects)) {

                    ?>
                    <div class="heading_strip"><h1><?php echo MY_PROJECTS; ?></h1></div>

                    <?php
                    foreach ($latest_projects as $latest_project) {

                        $data['latest_project'] = $latest_project;
                        $data['latest_project']['myproject'] = 'yes';
                        $this->load->view('default/home/common_card', $data);

                    }
                } else {

                    ?><br/><br/>
                    <p><strong><?php echo NO_PROJECTS_UNDER_YOUR_BELT_YET; ?></strong> <br/>

                        <?php echo BUT_YOU_HAVE_GREAT_IDEAS_ANDSO_DO_THE_PEOPLE_YOU_KNOW; ?></p>

                    <?php echo anchor('start_project/create_step1', START_YOUR_PROJECT, array('class' => 'create_pro')) ?>
                <?php
                }
                ?>
                <div class="clear"></div>

            </div>
            <div class="clear"></div>
        </div>
    </section>
</div>
