<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/additional-methods.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.maskedinput.js"></script>
<script>
    function show_div(id1, id2) {
        if ($('#' + id1).is(':visible') == false) {
            $('#' + id2).slideUp();
            $('#' + id1).slideDown();
            //$('.validation').hide();
        } else {
            $('#' + id2).hide();
            $('#' + id1).show();
        }

    }

</script>

<script>
    // validations
    jQuery(function ($) {

        // validate signup form on keyup and submit
        var validator = $("#login_frm").validate({
            rules: {
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                    minlength: 8,
                    maxlength: 20
                }


            },
            messages: {

                email: {
                    required: '<?php echo EMAIL_IS_REQUIRED;?>',
                    email: '<?php echo EMAIL_VALID_EMAIL;?>'
                    /*remote: "emails.action"*/
                },

                password: {
                    required: '<?php echo PASSWORD_IS_REQUIRED;?>',
                    minlength: '<?php echo ENTER_ATLEAST_EIGHT_CHARACTER;?>',
                    maxlength: '<?php echo YOU_CAN_NOT_ENTER_MORE_THAN_TWE_CHARACTER;?>'

                }

            },

            // the errorPlacement has to take the table layout into account
            errorPlacement: function (error, element) {
                if (element.is(":radio"))
                    error.insertAfter(element.parent());
                else if (element.is(":checkbox"))
                    error.insertAfter(element.next());
                else if (element.hasClass('book_job_now_price_input')) {
                    error.insertAfter(element.next().next());

                }
                else {
                    if (element.attr('name') == 'job_price') {
                        error.insertAfter(element.next());
                    }
                    else
                        error.insertAfter(element);
                    error.addClass('error_label');
                }
            },
            // specifying a submitHandler prevents the default submit, good for the demo
            /*	submitHandler: function() {
             alert("submitted!");
             },
             */	// set this class to error-labels to indicate valid fields
            success: function (label) {
                // set &nbsp; as text for IE
                label.html("&nbsp;").addClass("valid");

            },
            /*highlight: function(element, errorClass) {
             $(element).parent().next().find("." + errorClass).removeClass("checked");
             },*/
            /*  onsubmit: function(){
             $('.error').each(function(){
             $(this).addClass('error_label');
             });

             }, */
            onkeyup: function (element) {
                $(element).valid();
                var test_class = $(element).next().html();
                if (test_class != '')
                    $(element).next().addClass('error_label');
                else
                    $(element).next().removeClass('error_label');
            },
            onfocusout: function (element) {
                $(element).valid();
                var test_class = $(element).next().html();
                if (test_class != '')
                    $(element).next().addClass('error_label');
                else
                    $(element).next().removeClass('error_label');
            },

        });


        var validator = $("#forget_frm").validate({
            rules: {
                femail: {
                    required: true,
                    email: true
                }

            },
            messages: {
                femail: {
                    required: '<?php echo EMAIL_IS_REQUIRED;?>',
                    email: '<?php echo EMAIL_VALID_EMAIL;?>'
                    /*remote: "emails.action"*/
                },

            },
            // the errorPlacement has to take the table layout into account
            errorPlacement: function (error, element) {
                if (element.is(":radio"))
                    error.insertAfter(element.parent());
                else if (element.is(":checkbox"))
                    error.insertAfter(element.next());
                else if (element.hasClass('book_job_now_price_input')) {
                    error.insertAfter(element.next().next());

                }
                else {
                    if (element.attr('name') == 'job_price') {
                        error.insertAfter(element.next());
                    }
                    else
                        error.insertAfter(element);
                    error.addClass('error_label');
                }
            },
            // specifying a submitHandler prevents the default submit, good for the demo
            /*submitHandler: function() {
             alert("submitted!");
             },*/
            // set this class to error-labels to indicate valid fields
            success: function (label) {
                // set &nbsp; as text for IE
                label.html("&nbsp;").addClass("valid");

            },
            /*highlight: function(element, errorClass) {
             $(element).parent().next().find("." + errorClass).removeClass("checked");
             },*/
            onkeyup: function (element) {
                $(element).valid();
            },
            onfocusout: function (element) {
                $(element).valid();
            }

        });


    });

</script>
<?php


$data = array(
    'facebook' => $this->fb_connect->fb,
    'fbSession' => $this->fb_connect->fbSession,
    'user' => $this->fb_connect->user,
    'uid' => $this->fb_connect->user_id,
    'fbLogoutURL' => $this->fb_connect->fbLogoutURL,
    'fbLoginURL' => $this->fb_connect->fbLoginURL,
    'base_url' => site_url('home/facebook'),
    'appkey' => $this->fb_connect->appkey,
);
?>
<?php
$site_name = $site_setting['site_name'];

$facebook = facebook_setting();
$twitter = twitter_setting();
$linkdin = linkdin_setting();
?>



<section>
      <div class="full-bg-img login-img">
        <div class="container">
          <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
              <div class="whiteBox">

               <div id="login" <?php if ($this->strdd[1] == 'login') {
                    echo "style='display:block;'";
                } else {
                    echo "style='display:none;'";
                } ?>>
                <?php
                  if ($this->session->flashdata('error_message_login') != '') {

                ?>
                <div class="alert-danger alert alert-message">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <?php echo $this->session->flashdata('error_message_login'); ?>
                </div>
            <?php
            } 
            ?>
                <h2 class="headTitle">
                 <?php echo LOG_IN; ?>
                </h2>
                <div class="clearfix login-form">
                  <div class="email-login">
                   <?php
                        $att = array('id' => 'login_frm', 'name' => 'login_frm' ,'autocomplete' => 'off');
                        echo form_open('home/login', $att);
                        ?>
                        <input type="hidden" name="page" id="page" value="<?php echo $page; ?>" class="btn_input">
                    <div class="email-login-form">
                      <div class="input-ec">
                        <input class="ec-input-field" type="text" name="email" id="email" value="<?php echo $email; ?>"/>
                        <label class="input-label">
                          <span class="input-label-span"><?php echo EMAIL_ADDRESS; ?></span>
                        </label>
                      </div>
                      <div class="input-ec">
                        <input class="ec-input-field" type="password"  name="password" id="password" value="<?php echo $password; ?>"/>
                        <label class="input-label">
                          <span class="input-label-span"><?php echo PASSWORD; ?></span>
                        </label>
                      </div>
                    </div>
                    <div class="row m-t">
                        <div class="col-xs-6">
                          <button class="btn btn-primary text-uppercase" type="submit"><?php echo LOG_IN; ?></button>
                        </div>
                        <div class="col-xs-6 text-right m-t">
                        <a href="javascript:void(0)" class="text-primary" onclick="show_div('forget','login')"><?php echo I_FORGOT_MY_PASSWORD; ?></a>
                        </div>
                    </div>
                    </form>
                  </div>
                  <div class="social-login">
                  <div class="sc-buttons">
                   <?php if ($facebook->facebook_login_enable == 1) { ?>
                    <div class="sc-fb">
                      <div class="social-text"><i class="fa fa-facebook fa-fw"></i> 
                      <a href="<?php echo $data['fbLoginURL']; ?>"><?php echo WITH_FACEBOOK; ?> </a></div>
                    </div>
                      <?php } ?>
                       <?php if ($twitter['twitter_enable'] == 1) { ?>
                    <div class="sc-tw">
                      <div class="social-text"><i class="fa fa-twitter fa-fw"></i>
                      <a href="<?php echo site_url("home/twitter_auth"); ?>"><?php echo WITH_TWIITER; ?></a></div>
                    </div>
                     <?php } ?>

                      <?php if ($linkdin['linkdin_enable'] == 1) { ?>
                    <div class="sc-in">
                      <div class="social-text"><i class="fa fa-linkedin fa-fw"></i> 
                            <a href="<?php echo site_url("home/linkdin_auth"); ?>"><?php echo WITH_LINKEDIN; ?></a>
                      </div>
                    </div>
                     <?php } ?>
                  </div>
                  <div class="sc-divider"><span class="or">or</span></div>
                  </div>
                </div>
                <div class="whiteBox-footer">Don't have an account? 
                <a href="<?php echo site_url('home/signup')?>" class="text-primary">Join now</a></div>
              </div>
           

              <div id="forget" 
              <?php if ($this->strdd[1] == 'forget_password') {
                        echo "style='display:block;'";
                        } else {
                            echo "style='display:none;'";
                            } ?>>
                        <?php
                  if ($this->session->flashdata('success_message_login') != '') {
                      ?>
                      <div class="alert-success alert alert-message">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                          </button>
                          <?php echo $this->session->flashdata('success_message_login'); ?>
                      </div>
                  <?php
                  }   if ($this->session->flashdata('error_message_login') != '') {

                      ?>
                      <div class="alert-danger alert alert-message">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                          </button>
                          <?php echo $this->session->flashdata('error_message_login'); ?>
                      </div>
                  <?php
                  } 
                  ?>
          <h2 class="headTitle">
                 <?php echo FORGET_PASSWORD; ?>
                </h2>
                <div class="clearfix login-form forget-form">
                 <div class="email-login">
                 <?php $att = array('id' => 'forget_frm', 'name' => 'forget_frm' ,'autocomplete' => 'off');
                        echo form_open('home/forget_password', $att);
                        ?>
                       
                    <div class="email-login-form">
                      <div class="input-ec">
                        <input class="ec-input-field" type="text" name="femail" id="femail" value="<?php echo $femail; ?>"/>
                        <label class="input-label">
                          <span class="input-label-span"><?php echo EMAIL_ADDRESS; ?></span>
                        </label>
                      </div>
                    
                    </div>
                    <div class="row m-t">
                        <div class="col-xs-6">
                          <button type="submit" class="btn btn-primary text-uppercase"><?php echo RESET_MY_PASSWORD; ?></button>
                        </div>
                        <div class="col-xs-6 text-right m-t">
                       <a href="javascript://" onclick="show_div('login','forget')"><?php echo BACK_TO_LOGIN; ?></a>
                        </div>
                    </div>
                    </form>
                  </div>
                  </div>

        </div>

            </div>
          </div>
        </div>
      </div>
    </section>

    <script src="<?php echo base_url();?>assets_front/ec-input/ec-input.js"></script>