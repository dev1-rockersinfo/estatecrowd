<?php
$language_code = $this->session->userdata('lang_code');
//for german language code should be 'de'. It's temporary fix.
if($language_code=='ge') $language_code='de';

$captcha_src = ($language_code)?"https://www.google.com/recaptcha/api.js?hl=$language_code":"https://www.google.com/recaptcha/api.js";

?>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/additional-methods.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.maskedinput.js"></script>
<script src="<?=$captcha_src?>"></script>
<?php require(base_path().'application/libraries/ReCaptcha/src/autoload.php'); 

$siteKey = $site_setting['captcha_private_key'];
$secret = $site_setting['captcha_public_key'];

$recaptcha = new \ReCaptcha\ReCaptcha($secret);

?>
<script>

// validations

jQuery(function ($) {


    $("#user_name").mask("hhh?hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh", {placeholder: ""});

    // validate signup form on keyup and submit


    var validator = $("#signup_frm12").validate({

        rules: {

            user_name: {

                required: true,

                maxlength: 40,

                maxWords: 5,

                minWords: 2

            },


            email: {

                required: true,

                email: true

            },

           

            password: {

                required: true,

                minlength: 8,

                maxlength: 20

            },

            user_type: {

                required: true,

            }

        },

        messages: {

            user_name: {

                required: '<?php echo FIRST_NAME_LAST_NAME_ARE_REQUIRED;?>',

                maxlength: "<?php echo USERNAME_CANNOT_BE_MORE_THAN_CHAR;?>",

                maxWords: '<?php echo ENTER_FIRSTNAME_LASTNAME_ONLY;?>',

                minWords: '<?php echo ENTER_FIRSTNAME_LASTNAME;?>'

            },


            email: {
                required: '<?php echo EMAIL_IS_REQUIRED;?>',
                email: '<?php echo EMAIL_VALID_EMAIL;?>'
                /*remote: "emails.action"*/
            },


            password: {

                required: '<?php echo PASSWORD_IS_REQUIRED;?>',
                minlength: '<?php echo ENTER_ATLEAST_EIGHT_CHARACTER;?>',
                maxlength: '<?php echo YOU_CAN_NOT_ENTER_MORE_THAN_TWE_CHARACTER;?>'

            },
         

            user_type: {

                required: "<?php echo PLEASE_ACCEPT_TERMS_OF_USE;?>",

            }


        },

        // the errorPlacement has to take the table layout into account

        errorPlacement: function (error, element) {

            if (element.is(":radio"))

                error.insertAfter(element.parent());

            else if (element.is(":checkbox"))

                error.insertAfter(element.parent().next());

            else {


                error.insertAfter(element);

            }

        },

        // specifying a submitHandler prevents the default submit, good for the demo

        /*submitHandler: function() {

         alert("submitted!");

         },*/

        // set this class to error-labels to indicate valid fields

        success: function (label) {

            // set &nbsp; as text for IE

            label.html("&nbsp;").addClass("valid");


        },

        /*highlight: function(element, errorClass) {

         $(element).parent().next().find("." + errorClass).removeClass("checked");

         },*/

        onkeyup: function (element) {
            $(element).valid();

            var test_class = $(element).next().html();

            if (test_class != '')

                $(element).next().addClass('error_label');

            else

                $(element).next().removeClass('error_label');
        },

        onfocusout: function (element) {
            $(element).valid();

            var test_class = $(element).next().html();

            if (test_class != '')

                $(element).next().addClass('error_label');

            else

                $(element).next().removeClass('error_label');
        },


    });


});


</script>

<?php $site_name = $site_setting['site_name'];


$data = array(

    'facebook' => $this->fb_connect->fb,

    'fbSession' => $this->fb_connect->fbSession,

    'user' => $this->fb_connect->user,

    'uid' => $this->fb_connect->user_id,

    'fbLogoutURL' => $this->fb_connect->fbLogoutURL,

    'fbLoginURL' => $this->fb_connect->fbLoginURL,

    'base_url' => site_url('home/facebook'),

    'appkey' => $this->fb_connect->appkey,

);


$check_email = '';

$member_code_check = '';

$mem_email = '';

$team_member = $this->uri->segment(3);

if ($team_member == 'team_member') {

    if (isset($_COOKIE['fundraising_member'])) {

        $member_data = explode('/', $_COOKIE['fundraising_member']);

        //print_r($member_data);

        $check_email = $member_data[1];

        $member_code_check = $member_data[0];


        if ($check_email != '') {

            $mem_email = $check_email;

        } else {

            $mem_email = '';

        }

    }

}



?>
<?php


$facebook = facebook_setting();
$twitter = twitter_setting();
$linkdin = linkdin_setting();
  if ($this->session->flashdata('error_message_signup') != '') {

                ?>
                <div class="alert-danger alert alert-message">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <?php echo $this->session->flashdata('error_message_signup'); ?>
                </div>
            <?php
            } 
            ?>
 <section>
      <div class="full-bg-img login-img">
        <div class="container">
          <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
              <div class="whiteBox">
                <h2 class="headTitle">
                  Join now
                </h2>
                <div class="clearfix login-form">
                  <?php $att = array('id' => 'signup_frm', 'name' => 'signup_frm','autocomplete' => 'off');
                    echo form_open('home/signup', $att);
                    ?>
                  <div class="email-login">
                    <div class="email-login-form">
                      <div class="input-ec">
                        <input class="ec-input-field" type="text"  name="user_name" id="user_name" value="<?php echo $user_name; ?>"/>
                        <label class="input-label">
                          <span class="input-label-span"><?php echo NAME; ?></span>
                        </label>
                      </div>
                      <input type="hidden" name="referral" id="referral" value="<?php echo $this->input->get('referral'); ?>" />
                      <input type="hidden" name="syndicate" id="syndicate" value="<?php echo $this->input->get('syndicate'); ?>" />
                      <div class="input-ec">
                       
                          <input type="text" name="email" id="email" value="<?php echo $email;?>" <?php echo ($email != '') ?  'readonly="readonly"' :  ''; ?> class="ec-input-field">
                       

                        <label class="input-label">
                          <span class="input-label-span"><?php echo EMAIL_ADDRESS; ?></span>
                        </label>
                     

                      </div>
                      <div class="input-ec">
                        <input class="ec-input-field" type="password"  name="password" id="password"/>
                        <label class="input-label">
                          <span class="input-label-span">Choose Password</span>
                        </label>
                      </div>
                    

                    </div>
                      <div class="input-ec captcha-margin">
                         <?php if ($site_setting['signup_captcha'] == 1) { ?>
                     
                            <div class="pr g-recaptcha" data-sitekey="<?php echo $siteKey; ?>"></div>
                       
                      
                        <?php } ?>
                      </div>
                    <div class="user-type m-t">
                        <div class="checkbox">
                          <label class="ec-checkbox">
                            <input type="checkbox" class="ut-checkbox" name="user_type[]" value="1"> Investor
                            <i></i>
                          </label>
                        </div>
                        <div class="checkbox">
                          <label class="ec-checkbox">
                            <input type="checkbox" class="ut-checkbox" name="user_type[]" value="2"> R/E Agent
                            <i></i>
                          </label>
                        </div> 
                        <div class="checkbox">
                          <label class="ec-checkbox">
                            <input type="checkbox" class="ut-checkbox" name="user_type[]" value="3"> Property Owner
                            <i></i>
                          </label>
                        </div>  

                       <div class="checkbox">
                          <label class="ec-checkbox">
                            <input type="checkbox" class="ut-checkbox" name="user_type[]" value="4"> Property Developer
                            <i></i>
                          </label>
                        </div> 
                        <div class="checkbox">
                          <label class="ec-checkbox">
                            <input type="checkbox" class="ut-checkbox" id="selectall" name="user_type[]" value="5"> All of the above
                            <i></i>
                          </label>
                        </div>                     
                    </div>
                    <div class="m-t">
                     <input type="hidden" value="<?php echo $user_invite_code; ?>" name="user_invite_code">
                          <button class="btn btn-primary text-uppercase" type="submit">Join Now</button>
                    </div>
                    <div class="m-t lo-note">
                      By clicking Sign Up, you agree to Estate Crowd’s 
                      <a href="<?php echo site_url('content/pages/Terms-of-service'); ?>" class="text-primary">Terms of Use</a>
                        and <a href="<?php echo site_url('content/pages/privacy'); ?>" class="text-primary">Privacy Policy</a>.
                    </div>
                  </div>

                  </form>
                  <div class="social-login">
                      <div class="sc-buttons">
                   <?php if ($facebook->facebook_login_enable == 1) { ?>
                    <div class="sc-fb">
                      <div class="social-text"><i class="fa fa-facebook fa-fw"></i> 
                      <a href="<?php echo $data['fbLoginURL']; ?>"><?php echo WITH_FACEBOOK; ?> </a></div>
                    </div>
                      <?php } ?>
                       <?php if ($twitter['twitter_enable'] == 1) { ?>
                    <div class="sc-tw">
                      <div class="social-text"><i class="fa fa-twitter fa-fw"></i>
                      <a href="<?php echo site_url("home/twitter_auth"); ?>"><?php echo WITH_TWIITER; ?></a></div>
                    </div>
                     <?php } ?>

                      <?php if ($linkdin['linkdin_enable'] == 1) { ?>
                    <div class="sc-in">
                      <div class="social-text"><i class="fa fa-linkedin fa-fw"></i> 
                            <a href="<?php echo site_url("home/linkdin_auth"); ?>"><?php echo WITH_LINKEDIN; ?></a>
                      </div>
                    </div>
                     <?php } ?>
                  </div>
                  <div class="sc-divider"><span class="or">or</span></div>
                  </div>
                </div>
                <div class="whiteBox-footer">Already have an account? <a href="<?php echo site_url('home/login');?>" class="text-primary"><?php echo LOG_IN;?></a></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>


 <script src="<?php echo base_url();?>assets_front/ec-input/ec-input.js"></script>
    <script type="text/javascript">
      $(document).ready(function() {
    $('#selectall').click(function(event) { 
        if(this.checked) {
            $('.ut-checkbox').each(function() {
                this.checked = true;            
            });
        }else{
            $('.ut-checkbox').each(function() {
                this.checked = false;                       
            });         
        }
    });
    
});
    </script>
