

    <section >
	  	<div class="db-head">
	      	<div class="container">
	      		<div class="page-title">
		        	<h2>Search</h2>
		        </div>
	      	</div>
	  	</div>
	  	<div class="search-grey-bg" id="section-area">	  		
	      	<div class="container">
            <div class="search-grey-area">
	      		<div class="row">
		      		<div class="col-md-3 db-is-left">
                   <?php echo $this->load->view('default/dashboard_sidebar'); ?>
		      			
		      		</div>
                    
		      		<div class="col-md-9 db-is-right">      						
                          <div class="row inv-searh-media">
   		  <div class="col-md-4 inv-search-head">
    		  <div class="select-search" id="searchType">
                  <select class="selectpicker" id="keyword_search">
                   <option data-tokens="Enter name" value="user_name">Name</option>
                   <option data-tokens="Enter suburb" value="address">Location</option>
                   <option data-tokens="pot" value="property_type">Property Type</option>
                    <option class="noi" data-tokens="Enter Number of Investments" value="investments">Number of investments</option>
                    <option class="noi" data-tokens="soi" value="size_of_investment">Size of investment</option>                     
                    <option data-tokens=" Min. followers or greater" value="followers">Number of followers </option>
                  </select>
              
                </div>
     </div>
          <div class="inv-search-right col-md-8">
          
           <div id="custom-search-input">
                            <div class="input-group col-md-12">
                               
                                <div id="textboxOne"> 
                                <span class="input-group-btn">
                                    <button class="btn btn-danger" type="button">
                                        <span class=" glyphicon glyphicon-search"></span>
                                    </button>
                                </span>
                                <input id="searchBox"  type="text" class="ui-autocomplete-input search-query form-control hasclear" placeholder="Search By Name" onkeyup="ajaxSearch(this.value);" />
                                 
                                </div>  
                                <div id="textboxTwo" style="display:none;">
                                
                              <div class="range-input slider-wrap row">
              
              <div class="col-md-4">   <input id="ex2" type="text" class="span2" value="" data-slider-min="0" data-slider-max="200" data-slider-step="10" data-slider-value="[20,50]" />
</div>
    	
            <div class="col-md-8">
            <input class="form-control" type="text" name="min_val" id="minval" placeholder="Min" onchange="ajaxSearch(this.value,'','yes');" value="" >
            <input class="form-control" type="text" name="max_val" id="maxval" placeholder="Max" onchange="ajaxSearch('',this.value,'yes');"  value=""  ></div>       
                
                  </div>                                
                                </div>
                                <div id="textboxThree" style="display:none;">
                                <div class="col-md-12  search-pro-type">
                              <div class="select-search" >
                  <select class="selectpicker" onchange="ajaxSearch(this.value);">
                    <option value="1">Residential</option>
                    <option value="2">Commercial Family</option>
                    <option value="3">Industrial</option>
                    <option value="4">Rural</option>
                  </select>
                </div>
                </div>
                                </div>
                               
                                
                            </div>
                        </div>
                      
          
          </div>
      </div>       				
                    	<ul class="inv-search-list clearfix row" id="inv-search-list" >
                      <?php if(is_array($investors)) 
                            {


                                foreach ($investors as $key => $investor) {
                                   # code...
                                  ?>
                        <li class="col-md-3">
                        <?php 

                          if ($investor['image'] != '' ||  is_file(base_url()."upload/user/user_big_image/".$investor['image'])) {
                              $user_image = $investor['image'];
                          } else {
                              $user_image = 'no_man.jpg';
                          }

                          $last_login_data = GetLastLogin($investor['user_id']);

                          $member_last_login = $last_login_data['login_date_time'];
                          
                         

                          $total_follower = $investor['total_follower'];

                          ?>

                        <div class="inv-wrap">
                        		<div class="inv-top clearfix">
                              <a href="<?php echo site_url('user/'.$investor['profile_slug'])?>"> 
                               <img src="<?php echo base_url().'upload/user/user_big_image/'.$user_image ?>">   </a>
                               <span class="f-c"> <?php if($total_follower > 0) echo  $total_follower; else echo 0;?> Followers</span>
                                </div>
                                <div class="inv-body">
                                <span class="name"><a href="<?php echo site_url('user/'.$investor['profile_slug'])?>"><?php echo $investor['user_name'].' '.$investor['last_name'];?></a></span>
                                <span class="location"><p><i class="fa fa-map-marker"></i> 
                                <?php
                                  $string = (strlen($investor['address']) > 25) ? substr($investor['address'],0,25).'..' : $investor['address'];
                                 echo $string;?></p></span>
                              <span class="noi"><p>Investment: <span> 12</span> </p></span>
                                    <span class="soi"><p>Av. Investment Size: <span>$<?php echo $investor['min_investment_range'];?>k to $<?php echo $investor['max_investment_range'];?>k</span>
                                   
                                    </p></span>
                                     <span class="last-active"><p>Last Active:<span> <?php echo GetDuration($member_last_login);?></span></p></span>
                               
                                
                              </div>
                              
                        </div>
                        </li>

                        <?php 
                            }

                        } ?>
                        
                        </ul>
                  
			      		
	      			</div>
	      	</div>
	  	</div>
      </div>
    </section>
    
  
    
   <script src="<?php echo base_url();?>assets_front/bootstrap-select/js/bootstrap-select.js"></script>
      <script src="<?php echo base_url();?>js/bootstrap-slider1.js"></script>
    
    <script type="text/javascript">
      $(document).ready(function(){      
        

	
	$('#ex2').slider()
  .on('slide', function(ev)
  {
   
  	$("#minval").val("$"+ev.value[0]+"M");
   
    $("#maxval").val("$"+ev.value[1]+"M");
    ajaxSearch(ev.value[0],ev.value[1],'yes');
	 
	 
  });

  $('#ex2').slider()
  .on('click', function(ev)
  {
   
    $("#minval").val("$"+ev.value[0]+"M");
   
    $("#maxval").val("$"+ev.value[1]+"M");
    ajaxSearch(ev.value[0],ev.value[1],'yes');
   
   
  });
        // Change Placeholder
         $(document).on("click", "#searchType li a", function(){ 
            $("#searchBox").attr("placeholder", $(this).data("tokens"));
				var element = $(this).data("tokens");


								
				if ( element=="noi") {
						$("#textboxOne").show();
						$("#textboxTwo").hide();
						$("#textboxThree").hide();						
						
					}else if ( element=="soi") {
						$("#textboxOne").hide();
						$("#textboxTwo").show();				
						$("#textboxThree").hide();
					}
					else if ( element=="pot") {
						$("#textboxOne").hide();
						$("#textboxTwo").hide();						
						$("#textboxThree").show();
					}
					else  {
						$("#textboxOne").show();
						$("#textboxTwo").hide();
						$("#textboxThree").hide();
					}
        	});
	  	});
    
	   function ajaxSearch(val,maxval,invesment_search)
     {
      

        var keyword = $("#keyword_search").val();

        if(invesment_search == 'yes')
        {
         
          if(val == '')
          {
            val = $("#minval").val();
          }
          if(maxval == '')
          {
            maxval = $("#maxval").val();
          }

          var mapdata = {
                  "keyword": keyword,
                  "minvalue": val,
                  "maxvalue": maxval,
                 
              };
          }
        
        else
        {

          
          var mapdata = {
                  "keyword": keyword,
                  "value": val,
                 
              };
        }


        var getStatusUrl = '<?php echo site_url('search/ListInvestors')?>';
              $.ajax({
                  url: getStatusUrl,
                  dataType: 'text',
                  type: 'POST',
                  timeout: 99999,
                  global: false,
                  data: mapdata,
                  success: function (data) {
                      
                    $('#inv-search-list').empty().append(data);
                  },
                  error: function (XMLHttpRequest, textStatus, errorThrown) {
                  }
              });
     
     }
	  
		
    </script>

    


