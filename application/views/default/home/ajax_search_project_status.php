<?php
if ($user_projects) {
    ?>

    <div class="in-your-projects" id="paging">
        <ul class="in-project-list paging">
            <?php
            //echo '<pre>'; print_r($user_projects); die;
            foreach ($user_projects as $user_project) {

                $video_image = $user_project['video_image'];
                $image = $user_project['pimage'];
                if ($user_project['project_title'] != '') {
                    if (strlen($user_project['project_title']) > 30) {
                        $project_title = substr(ucfirst($user_project['project_title']), 0, 30) . '...';
                    } else {
                        $project_title = $user_project['project_title'];
                    }
                } else {
                    $project_title = 'Untitle project';
                }
                $url_project_title = $user_project['url_project_title'];
                $project_id = $user_project['project_id'];
                $project_summary = $user_project['project_summary'];
                $amount_get = set_currency($user_project['amount_get'], $project_id);
                $amount = set_currency($user_project['amount'], $project_id);
                $user_name = $user_project['user_name'];
                $user_id = $user_project['user_id'];
                $last_name = $user_project['last_name'];
                $address = $user_project['address'];
                $pimage = $user_project['pimage'];
                $date_added = $user_project['date_added'];
                $active = $user_project['active'];
                $status = status_name($active);
                $end_date = GetDaysLeft($user_project['end_date']);
                $amountdata = array('amount_get' => $user_project['amount_get'], 'amount' => $user_project['amount']);
                $percentage = GetProjectPercentage($amountdata);
                $editor = '';
                if (isset($user_project['inviteemail'])) {
                    $inviteemail = $user_project['inviteemail'];

                    if (isset($inviteemail) and $inviteemail != "") {
                        if ($user_id != $this->session->userdata('user_id')) {
                            $editor = ' - Editor';
                        }
                    }
                }

                $isedit = '';
                $arr = array(0, 1, 2);
                if (in_array($active, $arr)) {
                    $isedit = 'yes';
                }

                $isdelete = '';
                $arr = array(0, 1);
                if (in_array($active, $arr)) {
                    $isdelete = 'yes';
                }

                ?>

                <li>
                    <div id="project_<?php echo $project_id; ?>">

                        <div class="pro-thumbs">
                            <?php   if ($pimage == "") {
                                $pimage = 'no_img.jpg';
                            }
                            if (file_exists(base_path() . 'upload/project/small/' . $pimage)) {
                                ?>
                                <img class="img-thumbnail"
                                     src='<?php echo base_url('upload/project/small/' . $pimage); ?>'/>
                            <?php } else { ?>
                                <img class="img-thumbnail"
                                     src='<?php echo base_url('upload/project/small/no_img.jpg'); ?>'/>
                            <?php } ?>
                        </div>
                        <div class="project-list-content">
                            <?php $status_class = "";
                            if ($active == "0") {
                                $status_class = "draft-status";
                            }
                            if ($active == "1") {
                                $status_class = "pending-status";
                            }
                            if ($active == "2") {
                                $status_class = "active-status";
                            }
                            if ($active == "3") {
                                $status_class = "successful-status";
                            }
                            if ($active == "4") {
                                $status_class = "consol-status";
                            }
                            if ($active == "5") {
                                $status_class = "failure-status";
                            }
                            if ($active == "6") {
                                $status_class = "decline-status";
                            }
                            ?>
                            <h2><?php echo anchor('project/dashboard/' . $project_id, $project_title, 'class="anchor" , title="' . $user_project['project_title'] . '"'); ?>
                                <span
                                    class="pro-status <?php echo $status_class; ?>"><?php echo $status . $editor; ?></span>
                            </h2>

                            <p><?php echo GOAL; ?>: <?php echo $amount; ?></p>

                            <p class="pro-list-rised"><?php echo RAISED; ?>: <?php echo $amount_get; ?></p>
                            <?php /*?><p><?php echo STATUS;?>: <?php echo $status.$editor;?></p><?php */ ?>
                            <div class="project-action"><?php

                                if ($isedit == 'yes') {

                                    ?>

                                    <a href="<?php echo site_url('start_project/create_step1/' . $project_id); ?>"><i
                                            class="fa fa-edit"></i> <?php echo EDIT_PROJECT; ?></a>

                                <?php

                                } else {
                                    ?>

                                    <a href="javascript:void(0);" class="disabled"><i
                                            class="fa fa-edit"></i> <?php echo EDIT_PROJECT; ?></a>

                                <?php

                                }

                                ?>

                                <?php

                                if ($isdelete == 'yes' and $user_id == $this->session->userdata('user_id')) {

                                    ?>
                                    <a href="javascript:void(0);"
                                       onclick="remove_div('project_<?php echo $project_id; ?>',<?php echo $project_id; ?>)"><i
                                            class="fa fa-trash-o"></i> <?php echo DELETE_PROJECT; ?></a>

                                <?php

                                } else {

                                    ?>

                                    <a href="javascript:void(0);" class="disabled"
                                       onclick="alert('You can not delete this project');"><i
                                            class="fa fa-trash-o"></i> <?php echo DELETE_PROJECT; ?></a>

                                <?php } ?>

                                <a href="javascript:void(0);"><i class="fa fa-eye"></i> <?php echo VIEW_PROJECT; ?></a>
                            </div>
                        </div>
                    </div>
                </li>

            <?php
            }

            ?>
        </ul>
        <div class="pro-pagination"></div>
    </div>
<?php
} else {
    ?>
    <div class="in-dashboard-content">
        <div class="no-data">
            <p><strong><?php echo NO_PROJECTS_AVAILABLE; ?></strong>
        </div>
    </div>
<?php
}
?>
