<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/additional-methods.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.maskedinput.js"></script>
<script>
    // validations
    jQuery(function ($) {

        // validate signup form on keyup and submit

        var validator = $("#send_frm").validate({
            rules: {
                email: {
                    required: true,
                    email: true
                }

            },
            messages: {
                email: {
                    required: '<?php echo EMAIL_IS_REQUIRED;?>',
                    email: '<?php echo EMAIL_VALID_EMAIL;?>'
                    /*remote: "emails.action"*/
                }

            },
            // the errorPlacement has to take the table layout into account
            errorPlacement: function (error, element) {
                if (element.is(":radio"))
                    error.insertAfter(element.parent());
                else if (element.is(":checkbox"))
                    error.insertAfter(element.next());
                else if (element.hasClass('book_job_now_price_input')) {
                    error.insertAfter(element.next().next());

                }
                else {
                    if (element.attr('name') == 'job_price') {
                        error.insertAfter(element.next());
                    }
                    else
                        error.insertAfter(element);
                }
            },
            // specifying a submitHandler prevents the default submit, good for the demo
            /*submitHandler: function() {
             alert("submitted!");
             },*/
            // set this class to error-labels to indicate valid fields
            success: function (label) {
                // set &nbsp; as text for IE
                /*	label.html("&nbsp;").addClass("checked");*/

            },
            /*highlight: function(element, errorClass) {
             $(element).parent().next().find("." + errorClass).removeClass("checked");
             },*/
            onkeyup: function (element) {
                $(element).valid();
            },
            onfocusout: function (element) {
                $(element).valid();
            },

        });

    });

</script>
<?php $site_name = $site_setting['site_name'];
$face_data = array(
    'facebook' => $this->fb_connect->fb,
    'fbSession' => $this->fb_connect->fbSession,
    'user' => $this->fb_connect->user,
    'uid' => $this->fb_connect->user_id,
    'fbLogoutURL' => $this->fb_connect->fbLogoutURL,
    'fbLoginURL' => $this->fb_connect->fbLoginURL,
    'base_url' => site_url('home/facebook'),
    'appkey' => $this->fb_connect->appkey,
);
?>

<section>
  <div class="full-bg-img login-img">
        <div class="container">
          <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
              <div class="whiteBox">
               <?php
                  if ($this->session->flashdata('error_message_resend') != '') {

                ?>
                <div class="alert-danger alert alert-message">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <?php echo $this->session->flashdata('error_message_resend'); ?>
                </div>
            <?php
            } 
            ?>
                <div class="page-header">
                      <h2 class="headTitle"><?php echo SEND_VARIFICATION; ?></h2>
                </div>
                
                <div class="clearfix login-form forget-form">

                <div class="email-login">
                 <p><?php echo PLEASE_ENTER_EMAIL_ADDRESS_FOR_VARIFICATION_LINK; ?></p>
                    <?php
                    $att = array('id' => 'send_frm', 'name' => 'send_frm');
                    echo form_open('home/resend', $att);
                    ?>
             

                     <div class="input-ec">
                       
                          <input type="text" name="email" id="email" value="<?php echo $email;?>" class="ec-input-field">
                       

                        <label class="input-label">
                          <span class="input-label-span"><?php echo EMAIL_ADDRESS; ?></span>
                        </label>
                     

                      </div>

                    <div class="in-form-footer text-right">
                        
                        <button class="btn btn-primary text-uppercase" type="submit"><?php echo RESEND; ?></button>
                    </div>
                    </form>
                </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
</section>
   <script src="<?php echo base_url();?>assets_front/ec-input/ec-input.js"></script>