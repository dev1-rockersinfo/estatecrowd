<?php
$sitename = $site_setting['site_name'];
?>
<section>
    <div class="container padTB50">
        <div class="thenks-submit">
            <i class="fa fa fa-smile-o"></i>

            <h2><?php echo YOUR_CAMPAIGN; ?> <strong>"<?php echo $project_title; ?>
                    "</strong> <?php echo HAS_BEEN_SUBMITTED_SUCCESSFULLY; ?> </h2>
            <h4><?php echo WHAT_NEXT; ?></h4>

            <p><?php echo $sitename; ?> <?php echo TEAM_WILL_REVIEW_DETAIL_OF_YOUR_CAMPAIGN_AND_YOU_WILL_BE_NOTIFIED; ?></p>
            <a href="<?php echo site_url('home/main_dashboard'); ?>"
               class="btn-main marT20"><?php echo GO_TO_DASHBOARD; ?></a>

        </div>
    </div>
</section>
