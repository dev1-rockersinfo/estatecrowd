<script type="text/javascript">
    $(document).ready(function () {
        $('#paging').pajinate({
            items_per_page: 2,
            abort_on_small_lists: true,
            item_container_id: '.paging',
            nav_panel_id: '.pro-pagination',

        });
    });
</script>

<script>

    $(document).ready(function () {
        var mapHeight = $(".maplist").height();
        if (mapHeight >= 298) {
            $(".maplist").attr('id', 'maplist');
        }
        $("#maplist").niceScroll();
    });

</script>


<script type="text/javascript">

    function remove_div(e, id) {

        if (confirm('<?php echo DELETE_PROJECT_CONFIRMATION;?>')) {

            if (id != '' || id != 0) {

                $.ajax({

                    url: "<?php echo site_url('home/delete_project'); ?>/" + id,

                    xhrFields: {

                        withCredentials: true

                    }

                });


                $('#' + e).fadeOut(500, function () {

                    $(this).parent().remove();

                });

            }

        }


    }

    function showResult(str) {
        if (str) {
            $('#step2').show();
            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            } else {  // code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    $('#step2').hide();
                    $("#paging").html(xmlhttp.responseText);
                }
            }

            xmlhttp.open("GET", "<?php echo site_url("home/dashboard_search");?>/" + str, true);
            xmlhttp.send();
        }
    }

</script>

<?php echo $this->load->view('default/dashboard_sidebar'); ?>
<section>
<div class="container padTB50">
<div class="inner-container">
    <div class="page-header clearfix">
        <h1 class="pull-left"><?php echo YOUR_PROJECTS; ?></h1>

        <div class="projects-filter">
            <select class="selectpicker" name="filter" onchange="showResult(this.value)">
                <option value="all">All Projects</option>
                <option value="0">Draft Projects</option>
                <option value="1">Pending Projects</option>
                <option value="2">Active Projects</option>
                <option value="3">Successful Projects</option>
                <option value="4">Console Projects</option>
                <option value="5">Failure Projects</option>
                <option value="6">Decline Projects</option>
            </select>
        </div>
    </div>
    <?php
    if ($user_projects) {
        ?>

        <div class="in-your-projects" id="paging">
            <ul class="in-project-list paging" id="livesearch">
                <?php
                //echo '<pre>'; print_r($user_projects); die;
                foreach ($user_projects as $user_project) {

                    $video_image = $user_project['video_image'];
                    $image = $user_project['pimage'];
                    if ($user_project['project_title'] != '') {
                        if (strlen($user_project['project_title']) > 30) {
                            $project_title = substr(ucfirst($user_project['project_title']), 0, 30) . '...';
                        } else {
                            $project_title = $user_project['project_title'];
                        }
                    } else {
                        $project_title = 'Untitle project';
                    }
                    $url_project_title = $user_project['url_project_title'];
                    $project_id = $user_project['project_id'];
                    $project_summary = $user_project['project_summary'];
                    $amount_get = set_currency($user_project['amount_get'], $project_id);
                    $amount = set_currency($user_project['amount'], $project_id);
                    $user_name = $user_project['user_name'];
                    $user_id = $user_project['user_id'];
                    $last_name = $user_project['last_name'];
                    $address = $user_project['address'];
                    $pimage = $user_project['pimage'];
                    $date_added = $user_project['date_added'];
                    $active = $user_project['active'];
                    $status = status_name($active);
                    $end_date = GetDaysLeft($user_project['end_date']);
                    $amountdata = array('amount_get' => $user_project['amount_get'], 'amount' => $user_project['amount']);
                    $percentage = GetProjectPercentage($amountdata);
                    $editor = '';
                    if (isset($user_project['inviteemail'])) {
                        $inviteemail = $user_project['inviteemail'];

                        if (isset($inviteemail) and $inviteemail != "") {
                            if ($user_id != $this->session->userdata('user_id')) {
                                $editor = ' - Editor';
                            }
                        }
                    }

                    $isedit = '';
                    $arr = array(0, 1, 2);
                    if (in_array($active, $arr)) {
                        $isedit = 'yes';
                    }

                    $isdelete = '';
                    $arr = array(0, 1);
                    if (in_array($active, $arr)) {
                        $isdelete = 'yes';
                    }

                    ?>

                    <li>
                        <div id="project_<?php echo $project_id; ?>">

                            <div class="pro-thumbs">
                                <?php   if ($pimage == "") {
                                    $pimage = 'no_img.jpg';
                                }
                                if (file_exists(base_path() . 'upload/project/small/' . $pimage)) {
                                    ?>
                                    <img class="img-thumbnail"
                                         src='<?php echo base_url('upload/project/small/' . $pimage); ?>'/>
                                <?php } else { ?>
                                    <img class="img-thumbnail"
                                         src='<?php echo base_url('upload/project/small/no_img.jpg'); ?>'/>
                                <?php } ?>
                            </div>
                            <div class="project-list-content">
                                <?php $status_class = "";
                                if ($active == "0") {
                                    $status_class = "draft-status";
                                }
                                if ($active == "1") {
                                    $status_class = "pending-status";
                                }
                                if ($active == "2") {
                                    $status_class = "active-status";
                                }
                                if ($active == "3") {
                                    $status_class = "successful-status";
                                }
                                if ($active == "4") {
                                    $status_class = "consol-status";
                                }
                                if ($active == "5") {
                                    $status_class = "failure-status";
                                }
                                if ($active == "6") {
                                    $status_class = "decline-status";
                                }
                                ?>
                                <h2><?php echo anchor('project/dashboard/' . $project_id, $project_title, 'class="anchor" , title="' . $user_project['project_title'] . '"'); ?>
                                    <span
                                        class="pro-status <?php echo $status_class; ?>"><?php echo $status . $editor; ?></span>
                                </h2>

                                <p><?php echo GOAL; ?>: <?php echo $amount; ?></p>

                                <p class="pro-list-rised"><?php echo RAISED; ?>: <?php echo $amount_get; ?></p>
                                <?php /*?><p><?php echo STATUS;?>: <?php echo $status.$editor;?></p><?php */ ?>
                                <div class="project-action"><?php

                                    if ($isedit == 'yes') {

                                        ?>

                                        <a href="<?php echo site_url('start_project/create_step1/' . $project_id); ?>"><i
                                                class="fa fa-edit"></i> Edit Project</a>

                                    <?php

                                    } else {
                                        ?>

                                        <a href="javascript:void(0);" class="disabled"><i class="fa fa-edit"></i> Edit
                                            Project</a>

                                    <?php

                                    }

                                    ?>

                                    <?php

                                    if ($isdelete == 'yes' and $user_id == $this->session->userdata('user_id')) {

                                        ?>
                                        <a href="javascript:void(0);"
                                           onclick="remove_div('project_<?php echo $project_id; ?>',<?php echo $project_id; ?>)"><i
                                                class="fa fa-trash-o"></i> Delete Project</a>

                                    <?php

                                    } else {

                                        ?>

                                        <a href="javascript:void(0);" class="disabled"
                                           onclick="alert('You can not delete this project');"><i
                                                class="fa fa-trash-o"></i> Delete Project</a>

                                    <?php } ?>

                                    <a href="javascript:void(0);"><i class="fa fa-eye"></i> View Project</a>
                                </div>
                            </div>
                        </div>
                    </li>

                <?php
                }

                ?>
            </ul>
            <div class="pro-pagination"></div>
        </div>
    <?php
    } else {
        ?>
        <div class="in-dashboard-content">
            <div class="no-data">
                <p><strong><?php echo NO_PROJECTS_UNDER_YOUR_BELT_YET; ?></strong>
                    <?php echo BUT_YOU_HAVE_GREAT_IDEAS_ANDSO_DO_THE_PEOPLE_YOU_KNOW; ?></p>
                <?php echo anchor('start_project/create_step1', START_YOUR_PROJECT, array('class' => 'create_pro')) ?>
            </div>
        </div>
    <?php
    }
    ?>
</div>
<div class="inner-container marT50">
    <div class="row">
        <div class="col-lg-3">
            <div class="in-panel-box">
                <div class="panel-box-row">
                    <?php echo COMPLETED_DONATIONS; ?> <span><?php echo $total_completed_projects; ?></span>
                </div>
                <div class="panel-box-row">
                    <?php echo RUNNING_PROJECTS; ?> <span><?php echo $total_running_projects; ?> </span>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="in-panel-box">
                <div class="panel-box-row">
                    <?php echo PROJECT_FOLLOWERS; ?> <span><?php echo $project_follower; ?></span>
                </div>
                <div class="panel-box-row">
                    <?php echo PROJECT_FOLLOWING; ?> <span><?php echo $project_following; ?></span>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="in-panel-box">
                <div class="panel-box-row">
                    <?php echo COMPLETED_DONATIONS; ?>

                    <?php

                    if ($show_donation_list_completed) {

                        foreach ($show_donation_list_completed as $completed_donation_list) {

                            $comp_project_currency_code = $completed_donation_list['project_currency_code'];

                            $comp_project_currency_symbol = $completed_donation_list['project_currency_symbol'];

                            $comp_total = set_amount($completed_donation_list['total']);

                            ?>

                            <span><?php echo $comp_project_currency_symbol . $comp_total . ' (' . $comp_project_currency_code . ') '; ?></span>



                        <?php

                        }

                    } else {

                        ?>

                        <span>0</span>

                    <?php

                    }

                    ?>

                </div>
                <div class="panel-box-row">
                    <?php echo PENDING_DONATIONS; ?>
                    <?php
                    if ($show_donation_list_running) {

                        foreach ($show_donation_list_running as $running_donation_list) {

                            $running_project_currency_code = $running_donation_list['project_currency_code'];

                            $running_project_currency_symbol = $running_donation_list['project_currency_symbol'];

                            $running_total = set_amount($running_donation_list['total']);

                            ?>

                            <span><?php echo $running_project_currency_symbol . $running_total . ' (' . $running_project_currency_code . ') '; ?></span>

                        <?php

                        }

                    } else {

                        ?>

                        <span>0</span>

                    <?php

                    }

                    ?>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="in-panel-box">
                <div class="panel-box-row">
                    <?php echo COMPLETED_DONATIONS; ?>
                    <small>(<?php echo RECEIVED; ?>)</small>
                    <?php

                    if ($show_donation_list_rec_completed) {

                        foreach ($show_donation_list_rec_completed as $completed_donation_list_rec) {

                            $comp_project_currency_code_rec = $completed_donation_list_rec['project_currency_code'];

                            $comp_project_currency_symbol_rec = $completed_donation_list_rec['project_currency_symbol'];

                            $comp_total_rec = set_amount($completed_donation_list_rec['total']);

                            ?>

                            <span><?php echo $comp_project_currency_symbol_rec . $comp_total_rec . ' (' . $comp_project_currency_code_rec . ') '; ?></span>

                        <?php

                        }

                    } else {

                        ?>

                        <span>0</span>

                    <?php

                    }

                    ?>
                </div>
                <div class="panel-box-row">
                    <?php echo PENDING_DONATIONS; ?>
                    <small>(<?php echo RECEIVED; ?>)</small>

                    <?php

                    if ($show_donation_list_rec_running) {

                        foreach ($show_donation_list_rec_running as $running_donation_list_rec) {

                            $running_project_currency_code_rec = $running_donation_list_rec['project_currency_code'];

                            $running_project_currency_symbol_rec = $running_donation_list_rec['project_currency_symbol'];

                            $running_total_rec = set_amount($running_donation_list_rec['total']);

                            ?>

                            <span><?php echo $running_project_currency_symbol_rec . $running_total_rec . ' (' . $running_project_currency_code_rec . ') '; ?></span>

                        <?php

                        }

                    } else {

                        ?>

                        <span>0</span>

                    <?php

                    }

                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</section>

  
