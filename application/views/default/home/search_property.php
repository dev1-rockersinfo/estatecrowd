<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
<?php

  $display_investment_amount="none";
  $display_investment_suburb="none";
  $display_investment_address="none";
  // $suburb=$suburb;

    $amount_select = '';
    if($investment_amount != '')
    {
      $amount_select = 'selected';
      $display_investment_amount="block";
    }
  
    $suburb_select = '';
    if($investment_suburb != '')
    {
        $suburb_select = 'selected';
        $display_investment_suburb="block";
    }

    $address_select = '';
    if($investment_address != '')
    {
      $address_select = 'selected';
      $display_investment_address = "block";
    }

$bedrooms_array = array();
$bathrooms_array = array();
$cars_array = array();

  if($bedrooms)
  {
    $bedrooms_array = explode('--',$bedrooms);
  }
  if($bathrooms)
  {
    $bathrooms_array = explode('--',$bathrooms);
  }
  if($cars)
  {
    $cars_array = explode('--',$cars);
  }

  $investment_range_select = '';
  $min_value_select = 1;
  $max_value_select = 2;
  
    if($investment_range != '')
    {
      $investment_range_select = 'selected';

      if($investment_range == 1)
      {
          $min_property_investment_range = 1;

          $max_property_investment_range = 2;

          $min_value_select = 1;
          $max_value_select = 2;
      }
      elseif($investment_range == 2)
      {
        $min_property_investment_range = 3;

        $max_property_investment_range = 4;

        $min_value_select = 3;
        $max_value_select = 4;
      }
       elseif($investment_range == 3)
      {
          $min_property_investment_range = 5;

          $max_property_investment_range = 6;

          $min_value_select = 5;
          $max_value_select = 6;
      }
    }else{
      $investment_range = 1;
    }
  

    $min_investment_range_select = '';
    if($min_property_investment_range != '')
    {
      $min_investment_range_select = 'selected';
    }else{
      $min_property_investment_range =1;
    }
  
    $min_investment_range_select = '';
    if($max_property_investment_range != '')
    {
      $min_investment_range_select = 'selected';
    }else{
      $max_property_investment_range =2;
    }
  




?>

    <section>
     <div class="explore-top">
     <div class="container">
     <div class="row">
     

 <?php $attributes = array( 'name' => 'frmsearch', 'id' => 'frmsearch', 'method' => 'get', 'autocomplete' => 'off');
            echo form_open('search/property', $attributes);
            // echo $investment_amount;
            ?>
            <div class="explore-top-left col-md-3">
        <div class="select-search" id="searchType2">
          
            <select class="selectpicker main_search " name="main_search">
              <option value="1" <?php echo $amount_select; ?> >Investment Amount?</option>
              <option value="2" <?php echo $suburb_select; ?> >Investment Suburb</option>
              <option value="3" <?php echo $address_select ; ?> >Investment Address</option>
            </select>
        </div>
      </div>
       <div class="explore-top-right col-md-9 ">
          
        
          <div id="custom-search-input">
                            <div class="input-group col-md-12">
                             <span class="input-group-btn">
                                    <button class="btn btn-danger" type="submit">
                                        <span class=" glyphicon glyphicon-search"></span>
                                    </button>
                                </span> 

                  <input type="text" class="form-control" id="investment_amount" name="investment_amount"  placeholder="how much do you want to invest?" style="display:<?php echo $display_investment_amount;?>" value="<?php echo $investment_amount; ?>">  

                  <input type="text" class="form-control" id="investment_suburb" name="investment_suburb" style="display: <?php echo $display_investment_suburb; ?>" placeholder="What suburb do you want to invest in?" onkeyup="autotext_suburb();" value="<?php echo $investment_suburb ; ?>">

                   <div id="autoc_suburb" class="in-autocomplete"></div> 

                   <input type="text" class="form-control" id="investment_address" name="investment_address" style="display: <?php echo $display_investment_address; ?>" placeholder="Enter address of investment property..." value="<?php echo $investment_address; ?>" >

                   <div id="locationMap" class="gmap3 google-map"></div>
                            </div>
                        </div>          
          </div>
          </form>
      </div>    
     </div>
     </div>  
   
  
    </section>
    <section >
    <div class="explore-camp-search">
    <div class="container">
      
     <div class="navbar-header"><button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
       <span class="caret"></span>
           <a class="adv-fil">Advance Filter</a>
           
           
      </button>
   
       </div>
       <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <ul class="explore-camp-search-wrap" >
   
      
    <li>
    <span class="exp-search-head">Include Suburbs Within</span>
    <div class="slider">
      <input id="ex5" type="text" data-slider-min="1" data-slider-max="15" data-slider-step="1" data-slider-value="<?php echo $suburb; ?>";" />
    </div>
    </li>
    <li>
    <span class="exp-search-head">Bedrooms</span>
       <div class="CusButn" data-toggle="buttons">
  
   
    <a href="javascript://" class="<?php  if(in_array(1, $bedrooms_array)) { echo 'active'; }?>" onclick="setGetParameter('bedrooms',1)">1</a>
    <a href="javascript://" class="<?php  if(in_array(2, $bedrooms_array)) { echo 'active'; }?>" onclick="setGetParameter('bedrooms',2)">2</a>
    <a href="javascript://" class="<?php  if(in_array(3, $bedrooms_array)) { echo 'active'; }?>" onclick="setGetParameter('bedrooms',3)">3</a>
    <a href="javascript://" class="<?php  if(in_array(4, $bedrooms_array)) { echo 'active'; }?>" onclick="setGetParameter('bedrooms',4)">4</a>
    <a href="javascript://" class="<?php  if(in_array(5, $bedrooms_array)) { echo 'active'; }?>" onclick="setGetParameter('bedrooms',5)">5+</a>
  
</div>
    
    </li>
    <li>
    
    <span class="exp-search-head">Bathrooms</span>

     <div class="CusButn" data-toggle="buttons">
  
   
    <a href="javascript://" class="<?php  if(in_array(1, $bathrooms_array)) { echo 'active'; }?>" onclick="setGetParameter('bathrooms',1)">1</a>
    <a href="javascript://" class="<?php  if(in_array(2, $bathrooms_array)) { echo 'active'; }?>" onclick="setGetParameter('bathrooms',2)">2</a>
    <a href="javascript://" class="<?php  if(in_array(3, $bathrooms_array)) { echo 'active'; }?>" onclick="setGetParameter('bathrooms',3)">3</a>
    <a href="javascript://" class="<?php  if(in_array(4, $bathrooms_array)) { echo 'active'; }?>" onclick="setGetParameter('bathrooms',4)">4</a>
    <a href="javascript://" class="<?php  if(in_array(5, $bathrooms_array)) { echo 'active'; }?>" onclick="setGetParameter('bathrooms',5)">5+</a>
  
</div>
 
    
    </li>
    <li>
    
    <span class="exp-search-head">Cars</span>
    <div class="CusButn" data-toggle="buttons">
  
   
    <a href="javascript://" class="<?php  if(in_array(1, $cars_array)) { echo 'active'; }?>" onclick="setGetParameter('cars',1)">1</a>
    <a href="javascript://" class="<?php  if(in_array(2, $cars_array)) { echo 'active'; }?>" onclick="setGetParameter('cars',2)">2</a>
    <a href="javascript://" class="<?php  if(in_array(3, $cars_array)) { echo 'active'; }?>" onclick="setGetParameter('cars',3)">3</a>
    <a href="javascript://" class="<?php  if(in_array(4, $cars_array)) { echo 'active'; }?>" onclick="setGetParameter('cars',4)">4</a>
    <a href="javascript://" class="<?php  if(in_array(5, $cars_array)) { echo 'active'; }?>" onclick="setGetParameter('cars',5)">5+</a>
  
</div>
 

    </li>
   
  <li class="cmn-btn">
    <span class="exp-search-head">Investment Range</span>
    
    <div class="inr">
 <div class="btn-group" data-toggle="buttons">
  
   
        <label class="xyz" class="dropdown-toggle" data-toggle="dropdown" >
        <a href="">    
        <span class="txt" id="divID"> 
        $<?php echo $min_value_select;?>M to $<?php echo $max_value_select;?>M 
        </span> <span class="caret"></span></a>
        </label>
        <div class="dropdown-menu inner">
        <ul class="listl">
          <li><input class="inp" type="text" value="$<?php echo $min_property_investment_range; ?>M" onchange="setGetParameterSelect('min_property_investment_range',this.value)" id="fname"  >
            <input class="inp" type="text" value="$<?php echo $max_property_investment_range; ?>M" onchange="setGetParameterSelect('max_property_investment_range',this.value)"  id="fnameM"  >
         
            </li>
            <li class="<?php if($investment_range == 1){ echo 'seclected';}?>">
            <label for="one" id="one" onclick="setGetParameterSelect('investment_range','$1M')"  class="my_data_flag" >$1M to $2M <input type="radio" name="data_fl" value="$1M to $2M"></label>
            </li>
            <li class="<?php if($investment_range == 2){ echo 'seclected';}?>">
            <label for="three" id="three" onclick="setGetParameterSelect('investment_range','$2M')" class="my_data_flag" > $3M to $4M <input type="radio" name="data_fl" value="$3M to $4M"></label>
            </li>
            <li class="<?php if($investment_range == 3){ echo 'seclected';}?>">
            <label for="five" id="five" onclick="setGetParameterSelect('investment_range','$3M')" class="my_data_flag" >$5M to $6m  <input type="radio" name="data_fl" value=" $5M to $6m"> 
            </label></li>
              
              
            </ul>
            
        
      
    </div>
</div>



    </div>
    
    </li>
    <li class="cmn-btn property-type">
    <span class="exp-search-head">Property Type</span>
    <div class="select-search" id="searchType">
                  <select class="selectpicker" onchange="setGetParameterSelect('property_type',this.value)">
                    <option value="House" <?php if($property_type == 'House'){ echo 'selected="selected"';}?>>House</option>
                    <option value="Apartment and Unit" <?php if(trim($property_type) == trim('Apartment and Unit')){ echo 'selected="selected"';}?>>Apartment & Unit</option>
                    <option value="Townhouse" <?php if($property_type == 'Townhouse'){ echo 'selected="selected"';}?>>Townhouse</option>
                    <option value="Villa" <?php if($property_type == 'Villa'){ echo 'selected="selected"';}?>>Villa</option>
                    <option value="Acreage" <?php if($property_type == 'Acreage'){ echo 'selected="selected"';}?>>Acreage</option>
                    <option value="Rural" <?php if($property_type == 'Rural'){ echo 'selected="selected"';}?>>Rural</option>
                    <option value="Unit" <?php if($property_type == 'Unit'){ echo 'selected="selected"';}?>> Block Of Units</option>
                    <option value="Retirement Living" <?php if(trim($property_type) == trim('Retirement Living')){ echo 'selected="selected"';}?>>Retirement Living</option>
                     <option value="other" <?php if($property_type == 'other'){ echo 'selected="selected"';}?>>All</option> 
                  </select>
                </div>
    </li>
    
     <li>
        <a href="<?php echo site_url('search/property');?>" class="clear">Clear</a>
    </li>
    </ul>
    </div>
    </div>
    </div>
    

    </section>
    <section class="explore-content">
     <?php 
    if($investment_suburb != '' || $investment_address != '')
    { 

      if($investment_suburb != '')
      {
        $location_name = $investment_suburb;
      }
      else if($investment_address != '')
      {
        $location_name = $investment_address;
      }

      if(is_array($search_properties))
      {
        $count_properties = count($search_properties);
      }
      else
      {
        $count_properties = 0;
      }

      ?>
    <div class="container">
    <div class="explore-head clearfix">
    <div class="explore-head-left pull-left">
   
    <h1>Property in <?php echo $location_name;?></h1>
    <span class="explore-result"><?php echo $count_properties;?> Results found in  <?php echo $location_name;?></span>

   
    </div>
   <!--  <div class="explore-head-right pull-right">
    <div class="select-search" id="searchType">
                  <select class="selectpicker">
                    <option data-tokens="How much do you want to invest?">Investment Amount?</option>
                    <option data-tokens="What suburb do you want to invest in?">Investment Suburb</option>
                    <option data-tokens="Enter address of investment property...">Investment Address</option>
                  </select>
                </div>
    </div> -->
    </div>
    </div>
     <?php 
    } 
    ?>
    <div class="container">
    <div class="row" id="ajax_property">

     <?php 

 
       
           if($search_properties) 
           {
                foreach($search_properties as $search_property)
                {

                  $data['search_property'] = $search_property;
                  
                  $this->load->view('default/home/search_campaign_card', $data);
          
              }
               if($total_rows > 12)
              { ?>
             <div class="bottom-link m-t-lg">
              <a href="javascript://" class="btn btn-default more_properties" onclick="show_more_property(12)"> Show More Properties </a>
              </div>
           <?php }
           
            }
          ?>
   
         
          </div>
        
    </div>
    </section>

  <script src="<?php echo base_url();?>assets_front/bootstrap-select/js/bootstrap-select.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap-slider.js"></script>

    <script type="text/javascript">
      $(document).ready(function(){
     
        // Change Placeholder
       //$(".listl").hide();
      
      $("#ex5").slider();
      
        $(document).on("click", ".my_data_flag", function(){ 
    var chk = $(this).find('input[type="radio"]').is(":checked");
      if(chk){
          $(this).find('input[type="radio"]').prop('checked',false);
          $(this).parent('li').removeClass('seclected');
      }else{
          $(this).find('input[type="radio"]').prop('checked',true);
          var val = $(this).find('input[type="radio"]').val();
          $('.txt').html(val);
          $('.listl').find('li').removeClass('seclected');
          $(this).parent('li').addClass('seclected');
      }
      
  });
    
      $("#one").click(function(){
          $("#fname").val('$1M');
           $("#fnameM").val('$1M');

      });
       $("#three").click(function(){
          $("#fname").val('$3M');
           $("#fnameM").val('$4M');

      });
        $("#five").click(function(){
         
          $("#fname").val('$5M');
           $("#fnameM").val('$6M');

      });
   


        // Change Placeholder
     $('.main_search').on('change', function()
     {
        var selected = $(this).find("option:selected").attr("value");

        if(selected == 1)
        {
            $("#investment_amount").show();
            $("#investment_suburb").hide();
            $("#investment_address").hide(); 
            // $("#investment_suburb").val("");
            // $("#investment_address").val("");
           

        }
        else if(selected == 2)
        {
            $("#investment_amount").hide();
            $("#investment_suburb").show();
            $("#investment_address").hide();
            // $("#investment_amount").val("");
            // $("#investment_address").val("");
          
        }
        else if(selected == 3)
        {
            $("#investment_amount").hide();
            $("#investment_suburb").hide();
            $("#investment_address").show();
            // $("#investment_amount").val("");
            // $("#investment_suburb").val("");
        }
    });   
      });
    </script>
    <script type="text/javascript">

    $("#autoc_suburb").hide();

    function autotext_suburb() {
        var xmlHttp;
        try {
            xmlHttp = new XMLHttpRequest();// Firefox, Opera 8.0+, Safari
        }
        catch (e) {
            try {
                xmlHttp = new ActiveXObject("Msxml2.XMLHTTP"); // Internet Explorer
            }
            catch (e) {
                try {
                    xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                catch (e) {
                    //alert("<?php //echo NO_AJAX;?>");
                    return false;
                }
            }
        }
        xmlHttp.onreadystatechange = function () {
            if (xmlHttp.readyState == 4) {
                document.getElementById('autoc_suburb').innerHTML = xmlHttp.responseText;

                if (document.getElementById('srhdiv')) {


                  $("#autoc_suburb").show();


                }
                else {
                   $("#autoc_suburb").hide();
                    
                }
            }
        }
        var text = document.getElementById('investment_suburb').value;
        text = text.replace(/[^a-zA-Z0-9]/g, '');
        xmlHttp.open("GET", "<?php echo site_url('search/search_auto');?>/" + text, true);
        xmlHttp.send(null);
    }
    function selecttext_suburb(el) 
    {

        var selectvalue = el.innerHTML;
       
        $("#investment_suburb").val(selectvalue);
        $("#autoc_suburb").hide();
       
    }
    function setGetParameterSelect(paramName, paramValue)
    {
     

        var url = window.location.href;
        paramValue = paramValue.replace("$", "");
        paramValue= paramValue.replace("M", "");

        var hash = location.hash;
        url = url.replace(hash, '');
        if (url.indexOf(paramName + "=") >= 0)
        {
            var prefix = url.substring(0, url.indexOf(paramName));
            var suffix = url.substring(url.indexOf(paramName));
            suffix = suffix.substring(suffix.indexOf("=") + 1);
            suffix = (suffix.indexOf("&") >= 0) ? suffix.substring(suffix.indexOf("&")) : "";
            url = prefix + paramName + "=" + paramValue + suffix;
             //url = prefix + paramName + "=" + paramValue;
        }
        else
        {
          if (url.indexOf("?") < 0)
              url += "?" + paramName + "=" + paramValue;
          else
              url += "&" + paramName + "=" + paramValue;
        }
       
        window.location.href = url + hash;
    }

    function setGetParameter(paramName, paramValue)
    {


        var url = window.location.href;
       
        var hash = location.hash;
        url = url.replace(hash, '');


        if (url.indexOf(paramName + "=") >= 0)
        {
            var prefix = url.substring(0, url.indexOf(paramName));
            var suffix = url.substring(url.indexOf(paramName));


            suffix = suffix.substring(suffix.indexOf("=") + 1);

            suffix = (suffix.indexOf("&") >= 0) ? suffix.substring(suffix.indexOf("&")) : "";

            var base_url=window.location.pathname;
       
            var get_params=window.location.search.slice(1).split('&');
        
            var add_parameter_value='';

            var bedrooms ='';

            if(get_params.length>0)
            {
               
                $.each(get_params,function(index,value)
                {
                    var str_param=value.split("=");
                   
                   
                 
                    if(str_param[0]==paramName && str_param[0]!=undefined)
                    {


                        filter_textsearch=decodeURIComponent(str_param[1]);


                        
                        if(filter_textsearch  == paramValue) 
                        {
                        
                          
                           add_parameter_value = paramValue;
                        }   
                        else
                        {
                           


                            var str_parameters=filter_textsearch.split("--");

                          

                            if(filter_textsearch.length>0)
                            {


                              var isSixInArray = str_parameters.filter(function(item){return item==paramValue}).length ? true : false;

                              
                              if(isSixInArray  == true)
                              {

                                 //add_parameter_value = filter_textsearch;
                                 // add_parameter_value = filter_textsearch.replace("--"+paramValue, "");
                                temp = filter_textsearch;
                                add_parameter_value = filter_textsearch.replace("--"+paramValue, "");
                                if(temp==add_parameter_value){
                                  add_parameter_value = filter_textsearch.replace(paramValue+"--", "");
                                  }
                              }
                              else
                              {
                                $.each(str_parameters,function(index,value_param)
                                {
                                  
                                        if(value_param == paramValue)
                                        {
                                           
                                           // add_parameter_value = filter_textsearch+ "--" +paramValue;
                                            // add_parameter_value = filter_textsearch.replace("--"+paramValue, "");
                                            temp = filter_textsearch;
                                           add_parameter_value = filter_textsearch.replace("--"+paramValue, "");
                                           if(temp==add_parameter_value){
                                            add_parameter_value = filter_textsearch.replace(paramValue+"--", "");
                                           }
                                                      

                                        }
                                        else 
                                        {


                                            if(str_parameters.length < 1 )
                                            {

                                                 add_parameter_value = filter_textsearch+ paramValue;
                                            }
                                            else
                                            {

                                              add_parameter_value = filter_textsearch+ "--" +paramValue;
                                            }  

                                        }
                               
                                });
                              }
                          }

                          else
                          {

                               add_parameter_value = filter_textsearch + paramValue;

                              
                          }


                        } 



                        if(str_param[1] == paramValue )
                        {
                          
                          add_parameter_value = '';
                        }
                       
                        
                         url = prefix + paramName + "=" + add_parameter_value + suffix;


                        // return false;

                    }
                    // else
                    // {

                        
                    //     add_parameter_value=decodeURIComponent(str_param[1]); 

                    //      url = prefix + paramName + "=" + add_parameter_value + suffix;


                    //     return false;
                    // }

                     
                    
                  
                });
            }
           
           
            
        }
        else
        {
            if (url.indexOf("?") < 0)
                url += "?" + paramName + "=" + paramValue;
            else
                url += "&" + paramName + "=" + paramValue;
        }
     window.location.href = url + hash;
  }

            $(window).bind("load", function () {
            $('#dvLoading').fadeOut(2000);
            $('#mainbg_div').fadeIn(2000);
            $('.min_height').css('minHeight', 0);
            $(function () {
              $('#locationMap').gmap3();

              $('#investment_address').autocomplete({
                componentRestrictions: {country: "us"},
                  source: function () {
                      $("#locationMap").gmap3({
                          action: 'getAddress',
                          address: $(this).val(),
                          callback: function (results) {
                              if (!results) return;
                              $('#investment_address').autocomplete(
                                  'display',
                                  results,
                                  false
                              );
                          }
                      });
                  },
                  cb: {
                      cast: function (item) {

                          return item.formatted_address;
                      },
                     
                  }


              });
          });
      });

    function show_more_property(offset) {
        if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else { // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                 $("#ajax_property").first().find('.more_properties').remove();
                $("#ajax_property").append(xmlhttp.responseText);
            }
        }
        xmlhttp.open("GET", "<?php echo site_url('search/ajax_property');?>/" + offset, true);
        xmlhttp.send();
    }


    </script>
<script type="text/javascript">
$(document).ready(function(){
  $( ".slider-horizontal" ).mouseup(function() {
    // alert($("ex5").val());
// function sliderval(){
    var styledata = $(".slider-handle").attr('style');
    // $( "").text();
    var perce = styledata.split(":");
    var percentage = perce[1].replace("%;","");

    var calcu = Math.round((percentage*15)/100);
    if(parseFloat(percentage)<parseFloat(50)){
      calcu = parseInt(calcu)+parseInt(1);
    }
    // alert(calcu);
    $("#ex5").attr("data-slider-value",calcu);
    // $("#ex5").val(calcu);
    $("#sliderHandleVal").text(calcu);

    var paramName = "suburb";
    var paramValue = calcu;

    var url = window.location.href;
    // paramValue = paramValue.replace("$", "");
    // paramValue= paramValue.replace("M", "");

    var hash = location.hash;
    url = url.replace(hash, '');
    if (url.indexOf(paramName + "=") >= 0)
    {
        var prefix = url.substring(0, url.indexOf(paramName));
        var suffix = url.substring(url.indexOf(paramName));
        suffix = suffix.substring(suffix.indexOf("=") + 1);
        suffix = (suffix.indexOf("&") >= 0) ? suffix.substring(suffix.indexOf("&")) : "";
        url = prefix + paramName + "=" + paramValue + suffix;
         //url = prefix + paramName + "=" + paramValue;
    }
    else
    {
      if (url.indexOf("?") < 0)
          url += "?" + paramName + "=" + paramValue;
      else
          url += "&" + paramName + "=" + paramValue;
    }
   
    window.location.href = url + hash;
    // alert(url + hash);


  // }
  });
});
</script>
<script type="text/javascript" src="http://code.jquery.com/jquery-migrate-1.0.0.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/map/gmap3.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/map/jquery-autocomplete.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/map/jquery-autocomplete.css"/>