<div class="shadow unsupported" style="text-align: left;margin: 10% 10%;padding:3% 5%;">
    <h2>
        <?php echo THE_BROWSER_YOU_ARE_USING_INTERNET; ?>
    </h2>
    <br>

    <h2>
        <?php echo SO_PLEASE_TAKE_A_MOMENT_TO_CLICK_ONE_OF_THE_LINK_BELOW_TO_UPGRADE; ?>
    </h2>

    <a href="https://www.google.com/intl/en_uk/chrome/browser/" target="_blank"
       class="anchor fl"> <?php echo GOOGLE_CHROME; ?>  </a>

    <a href="http://www.mozilla.org/en-US/firefox/new/" target="_blank"
       class="anchor fl"> <?php echo MOZILLA_FIREFOX; ?> </a>

    <a href="http://www.apple.com/safari/" target="_blank" class="anchor fl">   <?php echo APPLE_SAFARI; ?>  </a>

    <div class="clear"></div>
</div>
