
  <section>
      <div class="db-head">
          <div class="container">
            <div class="page-title">
              <h2>Investments</h2>
            </div>
          </div>
      </div>
      <div class="grey-bg" id="section-area">       
          <div class="container">
            <div class="row">
              <div class="col-md-3 db-inv-left">
                  <?php echo $this->load->view('default/dashboard_sidebar'); ?>
                
              </div>
              <div class="col-md-9 db-inv-right">
                    
                  <div class="inv-tab clearfix">
                    <ul class="inv-tab-list clearfix">
                      <li><p>Total Investment</p>
                        <h3><?php echo set_currency($total_investment);?></h3>
                        
                        </li>
                        <li><p>Active Investment</p>
                        <h3><?php echo set_currency($total_running_investment);?> </h3>
                        
                        </li>
                        <li><p>Successful Investment</p>
                        <h3><?php echo set_currency($total_completed_investment);?>  </h3>
                        
                        </li>
                        <li><p>Unsuccessful Investment</p>
                        <h3><?php echo set_currency($total_fail_investment);?></h3>
                        
                        </li>
                    </ul>
                  </div>
                  <div class="inv-detail clearfix">
                  <ul class="inv-detail-list clearfix">

                  <?php 
                    if($total_investment_campaign)
                    {
                        foreach ($total_investment_campaign as $key => $all_campaign) 
                        {
                        # code...

                            $status = status_name($all_campaign['status']);
                            $property_address = $all_campaign['property_address'];
                            $property_url = $all_campaign['property_url'];
                             $units = $all_campaign['units'];

                              if(is_file(base_path().'upload/property/commoncard/'.$all_campaign['cover_image']) && $all_campaign['cover_image'] != '')
                            {
                                 $cover_image = base_url().'upload/property/commoncard/'.$all_campaign['cover_image'];
                            }
                            else
                            {
                                $cover_image = base_url().'upload/property/commoncard/no_img.jpg';
                            }
                     
                            $investment_close_date = GetDaysLeft($all_campaign['investment_close_date']);

                  ?>
                    <li class="clearfix">
                    <div class="inv-detail-lt">
                   <a href="<?php echo site_url('properties/'.$property_url)?>"> 
                   <img src="<?php echo $cover_image;?>"></a>
                    </div>
                    <div class="inv-detail-rt clearfix">
                     <h3>  <a href="<?php echo site_url('properties/'.$property_url)?>"> <?php echo $property_address;?></a> </h3>
                     <p>Project Status: <span><?php echo $status;?></span></p> 
                    <p>Investment Close:  <span><?php echo $investment_close_date;?></p>
          
                   
                        
                            
                    </div>   
                     <div class="dtl">
                    <ul class="dtl1 clearfix">
          <li> <span>Date</span> <?php echo dateformat($all_campaign['transaction_date_time']);?></li>
              <li>  <span>Unit</span> <?php echo $units;?></li>
                  <li>  <span>Amount</span>  <?php echo set_currency($all_campaign['preapproval_total_amount']);?></li>
                 <li> <span>Terms</span> 2 Months</li>
                <li> <span>Transaction Id </span> <?php echo $all_campaign['preapproval_key'];?></li>
                 <li> <span>Status </span> <?php echo $all_campaign['preapproval_status'];?></li>
                    

               </ul>    
                </div>        
                    </li>
                    
                <?php
                    }

                 } ?>
                  
                    
                  </ul>
                 
                  
                  <ul class="pagination pull-right">
                   <?php echo $links;?>

  </ul>
                  </div>

     
                
            </div>
          </div>
      </div>
    </section>