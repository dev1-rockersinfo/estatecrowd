<div class="main tac">
    <div class="error_404">
        <div class="img_404">
            <img src="<?php echo base_url(); ?>images/404.png"/>
        </div>
        <div class="text_404">
            <h2><?php echo SORRY; ?></h2>

            <h3>Server Error</h3>

            <p>The server took too long to display the webpage. Try again later or Contact Site administrator <a
                    id='fatal' href="javascript:void(0)">here.</a></p>
            <a href="<?php echo site_url('home/index'); ?>"
               class="inputB_Default_S"><?php echo RETURN_TO_HOME_PAGE; ?></a>
        </div>
        <div class="clear"></div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#fatal').click(function () {
            $('#contact_us').trigger('click');
        });
    });
</script>
