
    <section>
        <div class="db-head">
            <div class="container">
                <div class="page-title">
                    <h2>Dashboard</h2>
                </div>
            </div>
        </div>
        <div class="grey-bg" id="section-area">         
            <div class="container">
                <div class="row">
                    <div class="col-md-3 db-left">
                  <?php echo $this->load->view('default/dashboard_sidebar'); ?>
                        
                    </div>
                    <div class="col-md-9 db-right">
                  <div class="row">
                  <a href="<?php echo site_url('inbox');?>">
                  <div class="col-md-4 db-box-wrap">
                   <div class="db-box">              
                   <img src="<?php echo base_url();?>images/message-ico.png">
                 <span>  Messages</span>
                    </div>
                    </div>
                     </a> 
                  <a href="<?php echo site_url('home/my_campaign');?>">  
                     <div class="col-md-4 db-box-wrap">
                    <div class="db-box">                  
                   <img src="<?php echo base_url();?>images/campaign-ico.png">
                     <span>  My Campaigns</span>              
                    </div>
                    </div>
                     </a>
                     <a href="<?php echo site_url('property');?>">
                    <div class="col-md-4 db-box-wrap">
                    <div class="db-box">
                    <img src="<?php echo base_url();?>images/property-ico.png">
                       <span> Properties</span>                   
                    </div>                    
                    </div>
                    </a>
                    <a href="<?php echo site_url('crowd/crowdlist');?>">  
                    <div class="col-md-4 db-box-wrap">
                    <div class="db-box">
                    <img src="<?php echo base_url();?>images/crowds-ico.png">
                    <span>  Crowds</span>                  
                    </div>      
                    </div>
                    </a>
                     <a href="<?php echo site_url('search');?>" > 
                    <div class="col-md-4 db-box-wrap">
                    <div class="db-box">
                   <img src="<?php echo base_url();?>images/investor-ico.png">
                   <span>  Investors</span>                  
                    </div>     
                    </div>
                    </a>
                     <a href="<?php echo site_url('account');?>" > 
                    <div class="col-md-4 db-box-wrap">
                    <div class="db-box">
                               <img src="<?php echo base_url();?>images/profile-ico.png">
                     <span> Profile</span>
                    </div> 
                    </div>
                    </a>
                   
                    </div>
                        
                </div>
            </div>
        </div>
    </section>
    