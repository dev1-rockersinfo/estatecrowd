<?php
  

                    $property_id = $property['property_id'];
                    $property_user_id = $property['user_id'];
                    $property_address = $property['property_address'];
                    $property_url = $property['property_url'];
                    $status = $property['status'];
                    $bedrooms = $property['bedrooms'];
                    $min_investment_amount = $property['min_investment_amount'];
                    $bathrooms = $property['bathrooms'];
                    $cars = $property['cars'];
                    $campaign_units = $property['campaign_units'];
                    $share_available = $campaign_units - $property['campaign_units_get'];
                    $investment_close_date = GetDaysLeft($property['investment_close_date']);
                  
                    if(is_file(base_path().'upload/property/commoncard/'.$property['cover_image']) && $property['cover_image'] != '')
                    {
                         $cover_image = base_url().'upload/property/commoncard/'.$property['cover_image'];
                    }
                    else
                    {
                        $cover_image = base_url().'upload/property/commoncard/no_img.jpg';
                    }

            ?>
                   <li class="oc-campaigns-item">
                <div class="campaign-thumbnail">
                  <div class="campaign-img">
                    <a href="<?php echo site_url('properties/'.$property_url);?>">
                      <span class="overlay"></span>
                      <img src="<?php echo $cover_image;?>">
                    </a>
                    <div class="min-investment">
                       <h3><?php echo set_currency($min_investment_amount);?></h3>
                      <p>Min Investment</p>
                    </div>
                  </div>
                  <div class="campaign-caption">
                    <ul class="property-info clearfix">
                      <li>
                        <span class="info-num"><?php echo $bedrooms;?></span>
                        <span class="icon-wrapper"><img src="<?php echo base_url();?>images/icon-bed.png"></span>
                        <span class="info-name">bedrooms</span>
                      </li>
                      <li>
                        <span class="info-num"><?php echo $bathrooms;?></span>
                        <span class="icon-wrapper"><img src="<?php echo base_url();?>images/icon-bath.png"></span>
                        <span class="info-name">bathrooms</span>
                      </li>
                      <li>
                        <span class="info-num"><?php echo $cars;?></span>
                        <span class="icon-wrapper"><img src="<?php echo base_url();?>images/icon-car.png"></span>
                        <span class="info-name">cars</span>
                      </li>
                    </ul>
                    <div class="campaign-details">
                      <div class="campaign-title">
                        <h2><a href="<?php echo site_url('properties/'.$property_url);?>"><?php echo $property_address;?></a></h2>
                      </div>
                      <ul class="details-item">
                        <li class="clearfix">
                            <span class="card-item-left">Shares Available</span>
                            <span class="card-item-right"><?php echo $share_available;?>/<?php echo $campaign_units;?></span>
                            <span class="card-item-line"></span>
                        </li>
                        <li class="clearfix">
                            <span class="card-item-left">Investment Close</span>
                            <span class="card-item-right"><?php echo $investment_close_date;?></span>
                            <span class="card-item-line"></span>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </li>
          