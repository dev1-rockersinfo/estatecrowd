<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
  <body  data-spy="scroll" data-target="#affix-nav">
    <header class="navbar-transparent">
    <?php echo $this->load->view('default/common/header'); ?>
     <?php

            if ($allbanner)
            {
                    $dynamic_image_title = $allbanner['dynamic_image_title'];
                    $dynamic_image_paragraph = $allbanner['dynamic_image_paragraph'];
                    $dynamic_image_image = $allbanner['dynamic_image_image'];
                    $dynamic_color_picker = $allbanner['color_picker'];
                    $color_picker_content = $allbanner['color_picker_content'];
                   
                   
                    $dynamic_image_title = $allbanner['dynamic_image_title'];
                    $view_order = $allbanner['view_order'];
                    $dynamic_image_image = $allbanner['dynamic_image_image'];
                    
                    $slider_title = $dynamic_image_title;
                    $slider_image = base_url() . 'upload/dynamic/' . $dynamic_image_image;
                    $slider_content = $dynamic_image_paragraph;
                    $both_link = $allbanner['link'];
                    $both_link_name = $allbanner['link_name'];
                    $slider_edit = site_url('admin/project_category/edit_dynamic_slide/' . $allbanner['dynamic_slider_id']);


                    $image_check = "upload/dynamic/" . $dynamic_image_image;
                    $slider_image_background = base_url() . 'upload/dynamic/no_img.jpg';
                    if (file_exists($image_check) && $image_check != '') 
                    {
                        $slider_image_background =  $slider_image;
                    }

                   ?>
      <div class="main-slider">
        <div class="masthead">
            <div class="ec-home-brand">
               <img src="<?php echo base_url();?>images/home-logo.png">
            </div>
                        <div class="slider-text">
                            <h2>
                                    <?php
                                    $str_name=$slider_title;
                                    if (!empty($slider_title)) {
                                       
                                            $str_name = $slider_title;

                                        
                                    } 
                                    echo $str_name; ?>

                                </h2>
               <p style="color:<?php echo $dynamic_color_picker; ?>">
                                <?php
                                $str_name=$slider_content;   
                                if (strlen($slider_content) > 80) {
                                    $str_name = substr(ucfirst($slider_content), 0, 80) . '...';

                                } 
                                echo $str_name;?> </p>
            </div>

 <?php $attributes = array( 'name' => 'frmsearch', 'id' => 'frmsearch', 'method' => 'get', 'autocomplete' => 'off');
            echo form_open('search/advance_search', $attributes);
            ?>
            
            <div class="home-search-area">
              <div class="search-bg clearfix">
                <div class="select-search" id="searchType">
                  <select class="selectpicker">
                    <option  value="1">Investment Amount?</option>
                    <option  value="2">Investment Suburb</option>
                    <option  value="3">Investment Address</option>
                  </select>
                </div>
                <div class="textbox-search" style="position: relative;">
                  <input type="text" id="investment_amount" name="investment_amount"  placeholder="how much do you want to invest?">  
                  <input type="text" id="investment_suburb" name="investment_suburb" style="display: none" placeholder="What suburb do you want to invest in?" onkeyup="autotext_suburb();">

                   <div id="autoc_suburb" class="in-autocomplete"></div> 

                   <input type="text" id="investment_address" name="investment_address" style="display: none" placeholder="Enter address of investment property...">

                   <div id="locationMap" class="gmap3 google-map"></div>


                </div>
                <div class="button-search">
                  <button class="btn btn-search" type="submit">
                    <i class="icon-magnifier"></i>
                    Search
                  </button>
                </div>
              </div>
            </div>
            </form>

        </div>
        <div class="home-background-image">
          <div class="image-wrapper">
        <div class="image" style="background-image:url(<?php echo $slider_image_background;?>)"></div>
            <div class="image-mask"></div>
          </div>
        </div>


      </div>

      <?php
           

             } ?>
    </header>
    <section>
      <div class="section-mar">
        <div class="container">
          <div class="page-title">
            <h1>Syndicates Closing in 7 Days</h1>
          </div>
          <div class="home-campaigns">
            <ul class="clearfix oc-campaigns owl-carousel" id="campaignsList">
             <?php 

       // echo count($latest_properties);die;
           if($latest_properties) 
           {
                foreach($latest_properties as $property)
                {

                  $data['property'] = $property;
                  $this->load->view('default/home/campaign_card', $data);          
                }
            }
            ?>
             

            </ul>
          </div>
          <div class="bottom-link m-t-lg">
            <a href="<?php echo site_url('search/campaign');?>" class="btn btn-default">See All Campaigns</a>
          </div>
        </div>
      </div>
    </section>
    <section class="grey-bg">
      <div class="section-mar">
        <div class="container">
                  <?php if($suburbs){ ?>

          <div class="page-title">
            <h1>Top 10 Investment Suburbs</h1>
          </div>
          <div class="suburbs-list">
              <ul class="clearfix row">
              <?php foreach ($suburbs as $suburb) { 
                $name = $suburb['single_line'];
                $suburb_used = $suburb['suburb_used'];
                ?>
                
                <li class="col-md-4 col-xs-6">
                <a href="<?php echo site_url('search/campaign');?>">
                  <div class="suburbs-item">
                    <div class="suburbs-name">
                      <h2><?=$name?></h2>
                      <p><?=$suburb_used.' Campaigns'?></p>
                    </div>
                    <div class="overlay"></div>
                    <img src="<?php echo base_url();?>images/subrbs-01.png">
                  </div>
                </a>
              </li>

              <?php } ?>
              </ul>

          </div>
                    <?php } ?>         


          <div class="bottom-link m-t-lg">
            <div class="all-suburbs m-b-lg clearfix col-md-10 col-md-offset-1 " id="suburb-list" style="display:none">
         
          <ul class="all-suburbs-list">
                   <li class="col-md-3">
                 <a href="javascript:void(0)">  <span class="suburb-name">Barton, ACT</span>

                          </a>
                   </li>
                   <li class="col-md-3">
                 <a href="javascript:void(0)">  <span class="suburb-name">Darwin, NA</span>

                          </a>
                   </li>
                   <li class="col-md-3">
                 <a href="javascript:void(0)">  <span class="suburb-name">Coconut Grove, NA</span>
    
                          </a>
                   </li>
                   <li class="col-md-3">
                 <a href="javascript:void(0)">  <span class="suburb-name">Renner Springs, NT</span>
       
                          </a>
                   </li>
                   <li class="col-md-3">
                 <a href="javascript:void(0)">  <span class="suburb-name">East Perth, WA</span>
 
                          </a>
                   </li>
                   <li class="col-md-3">
                 <a href="javascript:void(0)">  <span class="suburb-name">Ernabella, SA </span>
    
                          </a>
                   </li>
                   <li class="col-md-3">
                 <a href="javascript:void(0)">  <span class="suburb-name">Girraween, WA</span>
 
                          </a>
                   </li>
                   <li class="col-md-3">
                 <a href="javascript:void(0)">  <span class="suburb-name">East Perth, WA</span>
 
                          </a>
                   </li>
                   <li class="col-md-3">
                 <a href="javascript:void(0)">  <span class="suburb-name">Delamere, WA</span>
    
                          </a>
                   </li>
                                <li class="col-md-3">
                 <a href="javascript:void(0)">  <span class="suburb-name">Barton, ACT</span>
                
                          </a>
                   </li>
                   <li class="col-md-3">
                 <a href="javascript:void(0)">  <span class="suburb-name">Darwin, NA</span>
     
                          </a>
                   </li>
                   <li class="col-md-3">
                 <a href="javascript:void(0)">  <span class="suburb-name">Coconut Grove, NA</span>
      
                          </a>
                   </li>
                   <li class="col-md-3">
                 <a href="javascript:void(0)">  <span class="suburb-name">Renner Springs, NT</span>
                </a>
                   </li>
                   <li class="col-md-3">
                 <a href="javascript:void(0)">  <span class="suburb-name">East Perth, WA</span>
  
                          </a>
                   </li>
                   <li class="col-md-3">
                 <a href="javascript:void(0)">  <span class="suburb-name">Ernabella, SA </span>
 
                          </a>
                   </li>
                   <li class="col-md-3">
                 <a href="javascript:void(0)">  <span class="suburb-name">Girraween, WA</span>
   
                          </a>
                   </li>
                   <li class="col-md-3">
                 <a href="javascript:void(0)">  <span class="suburb-name">East Perth, WA</span>
       
                          </a>
                   </li>
                   <li class="col-md-3">
                 <a href="javascript:void(0)">  <span class="suburb-name">Delamere, WA</span>
                       
                          </a>
                   </li>
                   
                  <li class="col-md-3">
                 <a href="javascript:void(0)">  <span class="suburb-name">Delamere, WA</span>
    
                          </a>
                   </li>
                                <li class="col-md-3">
                 <a href="javascript:void(0)">  <span class="suburb-name">Barton, ACT</span>
                
                          </a>
                   </li>
                   
                   </ul>
          </div>
                 <a href="javascript:toggle();" id="see-suburbs" class="btn btn-default">See All Suburbs</a> 
          </div>
           
        </div>
      </div>      
    </section>
    <section>
      <div class="video-section">
        <div class="overlay"></div>
        <div class="icon-play">
          <a href="javascript:void(0)" data-toggle="modal" data-target="#ec-video">          
            <img src="<?php echo base_url();?>images/icon-play.png">
          </a>          
        </div>
        <div class="modal fade" id="ec-video" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
       
  <div class="modal-dialog" role="document">

    <div class="modal-content">         
      <div class="modal-body">  
          <button type="button" class="close" data-dismiss="modal">&times;</button>        
     <div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/aTxGOyqKe30"></iframe>
</div>
      </div>
      
    </div>
  </div>
</div>
        <div class="video-img">
          <img src="<?php echo base_url();?>images/video-img.png">
        </div>
      </div>
    </section>
    <section>
      <div class="section-mar about-section">
        <div class="container">
          <div class="page-title">
            <h1>Estate Crowd</h1>
          </div>
          <div class="about-text row">
              <div class="col-lg-10 col-lg-offset-1">
                Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.
              </div>
          </div>
        </div>
      </div>    
    </section>
    <section class="grey-bg">
      <div class="section-mar">
        <div class="container">
          <div class="page-title">
            <h1>Fund your startup on Estate Crowd</h1>
          </div>
          <div class="row">
            <div class="col-md-8 startup-left">
              <ul class="clearfix">
                <li class="clearfix">
                  <div class="startup-icon"><img src="<?php echo base_url();?>images/icon-startup01.png"></div>
                  <div class="startup-text">Get introduced to numerous accredited investors</div>
                </li>
                <li class="clearfix">
                  <div class="startup-icon"><img src="<?php echo base_url();?>images/icon-startup02.png"></div>
                  <div class="startup-text">Rely on us for a compliant, secure process</div>
                </li>
                <li class="clearfix">
                  <div class="startup-icon"><img src="<?php echo base_url();?>images/icon-startup03.png"></div>
                  <div class="startup-text">Showcase your company and track investor interest</div>
                </li>
                <li class="clearfix">
                  <div class="startup-icon"><img src="<?php echo base_url();?>images/icon-startup04.png"></div>
                  <div class="startup-text">Get support 24/7 from your dedicated account executive</div>
                </li>
                <li class="clearfix">
                  <div class="startup-icon"><img src="<?php echo base_url();?>images/icon-startup05.png"></div>
                  <div class="startup-text">Partner with committed fundraising specialists</div>
                </li>
                <li class="clearfix">
                  <div class="startup-icon"><img src="<?php echo base_url();?>images/icon-startup06.png"></div>
                  <div class="startup-text">Spend more time running your business, not just fundraising</div>
                </li>
              </ul>
            </div>
            <div class="col-md-4 startup-right">
              <div class="startup-right-img">
                <img src="<?php echo base_url();?>images/icon-how-it-work.png">
              </div>
              <div class="startup-right-link">
                <a href="javascript:void(0)"><span>See how Estate Crowd</span> brings investors and founders together.</a>
              </div>
            </div>
          </div>
        </div>
      </div>      
    </section>

    <?php 
    if(count($latest_properties) < 4)
    {

    ?>
    <style type="text/css">
      .home-campaigns .owl-nav{
        display:none;
      }
      @media screen and (max-width: 992px) {
        .home-campaigns .owl-nav{
          display:block;
        }
      }
    </style>
  <?php } ?>
    <?php if(count($latest_properties) < 3) { ?>
    <style type="text/css">
        .home-campaigns .owl-nav{
          display:none;
        }
      @media screen and (max-width: 600px) {
        .home-campaigns .owl-nav{
          display:block;
        }
      }
    </style>
  <?php } ?>
    <?php if(count($latest_properties) < 2) { ?>
    <style type="text/css">
        .home-campaigns .owl-nav{
          display:none;
        }
    </style>
  <?php } ?>
   
</body>
    <script src="<?php echo base_url();?>assets_front/bootstrap-select/js/bootstrap-select.js"></script>
    <script src="<?php echo base_url();?>assets_front/owl-carousel/owl.carousel.min.js"></script>
    <script type="text/javascript">
      $(document).ready(function(){
        $('#campaignsList').owlCarousel({
          nav: true,
          responsiveClass:true,
          responsive:{
              0:{
                  items:1
              },
              330:{
                  items:2
              },
              660:{
                  items:3
              }, 
              1000:{
                  items:4 
              }
          }
        });


        // Change Placeholder
     $('.selectpicker').on('change', function()
     {
        var selected = $(this).find("option:selected").attr("value");
        if(selected == 1)
        {
            $("#investment_amount").show();
            $("#investment_suburb").hide();
            $("#investment_address").hide();
            $("#investment_suburb").val('');
            $("#investment_address").val('');

        }
        else if(selected == 2)
        {
            $("#investment_amount").hide();
            $("#investment_suburb").show();
            $("#investment_address").hide();
            $("#investment_amount").val('');
            $("#investment_address").val('');
        }
        else if(selected == 3)
        {
            $("#investment_amount").hide();
            $("#investment_suburb").hide();
            $("#investment_address").show();
            $("#investment_suburb").val('');
            $("#investment_amount").val('');

        }
    });   
      });
    </script>
    <script type="text/javascript">

    $("#autoc_suburb").hide();

    function autotext_suburb() {
        var xmlHttp;
        try {
            xmlHttp = new XMLHttpRequest();// Firefox, Opera 8.0+, Safari
        }
        catch (e) {
            try {
                xmlHttp = new ActiveXObject("Msxml2.XMLHTTP"); // Internet Explorer
            }
            catch (e) {
                try {
                    xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                catch (e) {
                    //alert("<?php //echo NO_AJAX;?>");
                    return false;
                }
            }
        }
        xmlHttp.onreadystatechange = function () {
            if (xmlHttp.readyState == 4) {
                document.getElementById('autoc_suburb').innerHTML = xmlHttp.responseText;

                if (document.getElementById('srhdiv')) {


                  $("#autoc_suburb").show();


                }
                else {
                   $("#autoc_suburb").hide();
                    
                }
            }
        }
        var text = document.getElementById('investment_suburb').value;
        text = text.replace(/[^a-zA-Z0-9]/g, '');
        xmlHttp.open("GET", "<?php echo site_url('search/search_auto');?>/" + text, true);
        xmlHttp.send(null);
    }
    function selecttext_suburb(el) {

        var selectvalue = el.innerHTML;
       
        $("#investment_suburb").val(selectvalue);
        $("#autoc_suburb").hide();
       
    }


    function toggle() {

    var ele = document.getElementById("suburb-list");

    var text = document.getElementById("see-suburbs");

    if(ele.style.display == "block") {

            ele.style.display = "none";

        text.innerHTML = "See All Suburbs";
    }
    else {
        ele.style.display = "block";
        text.innerHTML = "Less Suburbs";
    }       
            };


            $(window).bind("load", function () {
            $('#dvLoading').fadeOut(2000);
            $('#mainbg_div').fadeIn(2000);
            $('.min_height').css('minHeight', 0);
            $(function () {
              $('#locationMap').gmap3();

              $('#investment_address').autocomplete({
                componentRestrictions: {country: "us"},
                  source: function () {
                      $("#locationMap").gmap3({
                          action: 'getAddress',
                          address: $(this).val(),
                          callback: function (results) {
                              if (!results) return;
                              $('#investment_address').autocomplete(
                                  'display',
                                  results,
                                  false
                              );
                          }
                      });
                  },
                  cb: {
                      cast: function (item) {

                          return item.formatted_address;
                      },
                     
                  }


              });
          });
      });


    </script>

<script type="text/javascript" src="http://code.jquery.com/jquery-migrate-1.0.0.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/map/gmap3.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/map/jquery-autocomplete.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/map/jquery-autocomplete.css"/>
    