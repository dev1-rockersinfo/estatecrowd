<section>
    <div class="container padTB50">
        <div class="thenks-submit">
            <img src="<?php echo base_url(); ?>images/404.png"/>

            <h1 class="text-uppercase"><?php echo SORRY; ?></h1>

            <h3><?php echo WE_CANT_FIND_THAT_PAGE; ?></h3>

            <p><?php echo PLEASE_BE_SURE_TO_DOUBLE_CHECK_YOUR_SPELLING_TOMAKE_SURE_YOU; ?></p>
            <a href="<?php echo site_url('home/index'); ?>"
               class="btn-main marT20"><?php echo RETURN_TO_HOME_PAGE; ?></a>

        </div>
    </div>
</section>
