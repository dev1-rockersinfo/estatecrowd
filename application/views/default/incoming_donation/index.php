<?php echo $this->load->view('default/dashboard_sidebar'); ?>

<script type="text/javascript">
    $(document).ready(function () {
        $('#paging').pajinate({
            items_per_page: 10,
            abort_on_small_lists: true,
            item_container_id: '.paging',
            nav_panel_id: '.pro-pagination',

        });
    });
</script>
<section>
    <div class="container padTB50">
        <div class="sub-tab no-tab tabTitle">
            <ul class="clearfix">
                <li>
                    <a href="<?php echo site_url('transaction/index'); ?>"><?php echo MY_FUNDING; ?> </a>
                </li>
                <li class="active">
                    <a href="<?php echo site_url('incoming_donation'); ?>"><?php echo INCOMING_DONATION; ?> </a>
                </li>
            </ul>
        </div>
        <div class="inner-container-bg">
            <div class="sub-page-header-bg">
                <div class="page-header">
                    <h1><?php echo INCOMING_FUND; ?></h1>
                </div>
            </div>
            <div class="table-responsive marT20" id="paging">
                <table class="table paging common-pagination" cellpadding="0" cellspacing="0">
                    <thead>
                    <tr>
                        <th><?php echo PROJECT_TITLE; ?></th>
                        <th><?php echo END_DATE; ?></th>
                        <th><?php echo AMOUNT; ?></th>
                        <th><?php echo RAISED; ?></th>
                        <th><?php echo PROJECT_STATUS; ?></th>
                    </tr>
                    </thead>

                    <?php
                    if ($incoming_donation) {
                        $count1 = 0;
                        foreach ($incoming_donation as $incoming) {
                            if ($count1 % 2 == 0) {
                                $rowclass = 'trbg1';
                            } else {
                                $rowclass = '';
                            }
                            $company_name = $incoming['company_name'];
                            $incmg_equity_id = $incoming['equity_id'];
                            $equity_url = $incoming['equity_url'];

                            $end_date = $incoming['end_date'];
                            if ($incoming['goal'] > 0) {
                                $amount = set_currency($incoming['goal'], $incmg_equity_id);
                            } else {
                                $amount = set_currency(0, '');
                            }
                            if ($incoming['amount_get'] > 0) {
                                $amount_get = set_currency($incoming['amount_get'], $incmg_equity_id);
                            } else {
                                $amount_get = set_currency(0, '');
                            }

                            $status = $incoming['status'];
                            ?>
                            <tbody>
                            <tr>
                                <td>
                                    <a href="<?php echo site_url('projects/' . $equity_url); ?>"><?php echo $company_name; ?></a>
                                </td>
                                <td><?php echo $end_date; ?></td>
                                <td><?php echo $amount; ?></td>
                                <td><?php echo $amount_get; ?></td>
                                <td><?php /*if($status=='2' || $status==2){echo "Pending";}else{echo "Successful";}*/
                                    echo status_name($status); ?></td>
                            </tr>
                            </tbody>
                            <?php
                            $count1++;
                        }?>

                    <?php } else { ?>
                        <tbody>
                        <tr>
                            <td colspan="5">
                                <div class="no-data"><?php echo THERE_ARE_NO_INCOMING_DONATION; ?></div>
                            </td>
                        </tr>
                        </tbody>
                    <?php
                    }
                    ?>

                </table>
                <div class="pro-pagination"></div>
            </div>
        </div>
    </div>
</section>
