<?php
    $uri_segement1 = $this->uri->segment(1);
    $uri_segement2 = $this->uri->segment(2);
    
 ?>




<div class="db-sidemenu" id="accordion">
                            <ul>
                                <li>
                                 <?php 
                                    $dashboad_section = '';

                                    $collapesed_class = 'collapsed';

                                    if($uri_segement1 == 'home' || $uri_segement1 == 'inbox')
                                    {
                                        $dashboad_section = 'in';
                                        $collapesed_class = '';
                                    }
                                    

                                    ?>
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="<?php echo $collapesed_class;?>">
                                        Dashboard
                                        <span class="arrow"><i class="icon-arrow-down"></i></span>
                                    </a>
                                    


                                    <ul class="list-group panel-collapse collapse <?php echo $dashboad_section;?>" id="collapseOne">
                                    
                                        <li class="list-group-item"><a class="<?php if($uri_segement1 == 'home' && $uri_segement2 == 'dashboard'){ echo 'active';}?>" href="<?php echo site_url('home/dashboard');?>">Dashboard</a>   </li>
                                        <li class="list-group-item"><a class="<?php if($uri_segement1 == 'inbox'){ echo 'active';}?> " href="<?php echo site_url('inbox'); ?>">Messages</a>   </li>
                                        <li class="list-group-item"><a class="<?php if($uri_segement1 == 'home' && $uri_segement2 == 'my_campaign'){ echo 'active';}?> " href="<?php echo site_url('home/my_campaign');?>">My Campaigns</a>   </li>
                                        <li class="list-group-item"><a class="<?php if($uri_segement1 == 'home' && $uri_segement2 == 'property_following'){ echo 'active';}?> " href="<?php echo site_url('home/property_following');?>">Following Properties</a>   </li>
                                    </ul>
                                </li>
                                <li>

                                 <?php 
                                    $dashboad_section = '';

                                    $collapesed_class = 'collapsed';

                                    if($uri_segement1 == 'property' )
                                    {
                                        $dashboad_section = 'in';
                                        $collapesed_class = '';
                                    }
                                    

                                    ?>
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="<?php echo $collapesed_class;?>">
                                        Properties
                                        <span class="arrow"><i class="icon-arrow-down"></i></span>
                                    </a>
                                    <ul class="list-group panel-collapse collapse <?php echo $dashboad_section;?>" id="collapseTwo">
                                         <li class="list-group-item"><a class="<?php if($uri_segement1 == 'property' && $uri_segement2 == 'add_property'){ echo 'active';}?> " href="<?php echo site_url('property/add_property');?>">Add Property</a>   </li>
                                        <li class="list-group-item"><a class="<?php if($uri_segement1 == 'property' && $uri_segement2 == 'campaign'){ echo 'active';}?>"  href="<?php echo site_url('property/campaign');?>">Campaigns</a>   </li>
                                        <li class="list-group-item"><a href="<?php echo site_url('home/user_investments');?>">Investments</a>   </li>
                                        <li class="list-group-item"><a class="<?php if($uri_segement1 == 'property' && $uri_segement2 == 'property_following'){ echo 'active';}?>"  href="<?php echo site_url('follower/property_following');?>">Following</a>   </li>
                                        
                                    </ul>
                                </li>
                                
                                <li>
                                     <?php 
                                    $crowd_section = '';

                                    $collapesed_class = 'collapsed';

                                    if($uri_segement1 == 'crowd' || $uri_segement1 == 'comment')
                                    {
                                        $crowd_section = 'in';
                                        $collapesed_class = '';
                                    }
                                    

                                    ?>

                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"  class="<?php echo $collapesed_class;?>">
                                        My Crowds
                                        <span class="arrow"><i class="icon-arrow-down"></i></span>
                                    </a>
                                    <ul class="list-group panel-collapse collapse <?php echo $crowd_section;?>" id="collapseThree">
                                        <li class="list-group-item"><a class="<?php if($uri_segement1 == 'crowd' && $uri_segement2 == 'crowdlist'){ echo 'active';}?>" href="<?php echo site_url('crowd/crowdlist');?>"> List Crowds</a>   </li>
                                        <li class="list-group-item"><a class="<?php if($uri_segement1 == 'crowd' && $uri_segement2 == 'create_crowd'){ echo 'active';}?>" href="<?php echo site_url('crowd/create_crowd');?>">Create Crowd</a>   </li>
                                        <li class="list-group-item"><a class="<?php if($uri_segement1 == 'comment'){ echo 'active';}?>" href="<?php echo site_url('comment');?>"> My Comments</a>   </li>
                                        
                                    </ul>
                                </li>
                                
                                <li>

                                <?php 
                                    $investor_section = '';

                                    $collapesed_class = 'collapsed';

                                    if($uri_segement1 == 'follower' || $uri_segement1 == 'invites' || $uri_segement1 == 'search')
                                    {
                                        $investor_section = 'in';
                                        $collapesed_class = '';
                                    }
                                    

                                    ?>
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"  class="<?php echo $collapesed_class;?>">
                                        Investors 
                                        <span class="arrow"><i class="icon-arrow-down"></i></span>
                                    </a>
                                    <ul class="list-group panel-collapse collapse <?php echo $investor_section;?>" id="collapseFour">
                                        <li class="list-group-item"><a class="<?php if($uri_segement1 == 'follower' && $uri_segement2 == 'user_following'){ echo 'active';}?> " href="<?php echo site_url('follower/user_following');?>">Following</a>   </li>
                                        <li class="list-group-item"><a class="<?php if($uri_segement1 == 'follower' && $uri_segement2 == 'userFollowers'){ echo 'active';}?> " href="<?php echo site_url('follower/userFollowers');?>">Followers</a>   </li>
                                        <li class="list-group-item"><a class="<?php if($uri_segement1 == 'search' && $uri_segement2 == 'investors'){ echo 'active';}?> " href="<?php echo site_url('search/investors');?>"> Search</a>   </li>
                                         <li class="list-group-item"><a href="<?php echo site_url('invites/email');?>"> Invite</a>   </li>
                                        
                                        
                                    </ul>
                                </li>
                                
                                <li>
                                     <?php 
                                    $account_section = '';

                                    $collapesed_class = 'collapsed';

                                    if($uri_segement1 == 'account')
                                    {
                                        $account_section = 'in';
                                        $collapesed_class = '';
                                    }
                                    

                                    ?>

                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive"  class="<?php echo $collapesed_class;?>">
                                        Profile 
                                        <span class="arrow"><i class="icon-arrow-down"></i></span>
                                    </a>
                                    <ul class="list-group panel-collapse collapse <?php echo $account_section;?>" id="collapseFive">
                                        <li class="list-group-item"><a class="<?php if($uri_segement1 == 'account' && $uri_segement2 == ''){ echo 'active';}?>" href="<?php echo site_url('account');?>"> Edit Profile</a>   </li>
                                         <li class="list-group-item"><a class="<?php if($uri_segement1 == 'account' && $uri_segement2 == 'social'){ echo 'active';}?>" href="<?php echo site_url('account/social');?>"> Social</a>   </li>
                                     
                                           <li class="list-group-item"><a class="<?php if($uri_segement1 == 'account' && $uri_segement2 == 'changepassword'){ echo 'active';}?>" href="<?php echo site_url('account/changepassword');?>"> Change Password</a>   </li>

                                        <li class="list-group-item"><a class="<?php if($uri_segement1 == 'account' && $uri_segement2 == 'notification'){ echo 'active';}?>" href="<?php echo site_url('account/notification');?>">Notification Centre</a>   </li>
                               
                                        
                                    </ul>
                                </li>
                                <li>
                                <?php 
                                    $document_section = '';

                                    $collapesed_class = 'collapsed';

                                    if($uri_segement1 == 'document')
                                    {
                                        $document_section = 'in';
                                        $collapesed_class = '';
                                    }
                                    

                                    ?>

                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix"  class="<?=$collapesed_class?>">
                                        Document Centre 
                                        <span class="arrow"><i class="icon-arrow-down"></i></span>
                                    </a>
                                    <ul class="list-group panel-collapse collapse <?=$document_section?>" id="collapseSix">
                                        <li class="list-group-item"><a href="<?=site_url('document')?>" >Document Centre </a>   </li>
                                    
                               
                                        
                                    </ul>
                                </li>
                            </ul>
                                
                        </div>