 <?php 

 if(is_array($comment_data))
 {

    foreach ($comment_data as $key => $comments) 
    {
     

      if ($comments['image'] != '') {
          $user_image = $comments['image'];
      } else {
          $user_image = 'no_man.jpg';
      }
      $comment_user_id = $comments['user_id'];
      $profile_slug = $comments['profile_slug'];
      if($profile_slug != '')
      {
        $profile_url = site_url('user/'.$profile_slug);
      }
      else
      {
        $profile_url = site_url('user/'.$comment_user_id);
      }
      $comment_full_name = $comments['user_name'].' '.$comments['last_name'];
      $comment = $comments['comment'];
      $updated_date = $comments['updated_date'];
      $comment_image = $comments['comment_image'];
      $comment_id = $comments['id'];
      $property_id = $comments['property_id'];

 ?>

 <li>
            <div class="review-left">
            	<a href="<?php echo $profile_url;?>"> 
               <?php if ($user_image != '' && is_file("upload/user/user_medium_image/" . $user_image)) {
                        ?>
                        <img src="<?php echo base_url(); ?>upload/user/user_medium_image/<?php echo $user_image; ?>"/>
                    <?php
                    } else {
                        ?>
                        <img src="<?php echo base_url(); ?>upload/user/user_medium_image/no_man.jpg"/>
                    <?php } ?>
              </a>
            </div>
            <div class="review-right">
             <h3><a href="<?php echo $profile_url;?>"><?php echo $comment_full_name;?></a></h3>
             <span class="review-time"><?php echo dateformat($updated_date);?></span>
             <?php 
             if($comment_user_id == $this->session->userdata('user_id'))
             {

             ?>
            <span class="edit-view"><a href="javascript://" id="edit_comment" rel="<?php echo $comment_id;?>"><i class="fa fa-edit"> </i> Edit</a>
              <a href="javascript://" id="delete_comment" rel="<?php echo $comment_id;?>"><i class="fa fa-trash"> </i> Delete</a>
            </span>

            <?php } ?>
            <p class="comment-text">
                         <i class="fa fa-quote-left"></i> <?php echo $comment; ?>

                      
                    <?php if ($comment_image != '' && is_file("upload/crowd/" . $comment_image)) {
                        ?>
                        <img class="img-responsive" src="<?php echo base_url(); ?>upload/crowd/<?php echo $comment_image; ?>"/>
                    <?php
                    }  ?>
                          </p>

               <?php if($property_id > 0 && $property_id != '') {

                    $property_data = GetAllProperties($property_id,array('user','campaign'));

                    $property = $property_data[0];
                    $property_id = $property['property_id'];
                    $campaign_id = $property['campaign_id'];
                    $property_user_id = $property['user_id'];
                    $property_address = $property['property_address'];
                    $property_url = $property['property_url'];
                    $status = $property['status'];
                    $bedrooms = $property['bedrooms'];
                    $min_investment_amount = $property['min_investment_amount'];
                    $bathrooms = $property['bathrooms'];
                    $cars = $property['cars'];
                    $campaign_units = $property['campaign_units'];
                     $estimate = $property['estimate'];
                    $share_available = $campaign_units - $property['campaign_units_get'];
                    $investment_close_date = GetDaysLeft($property['investment_close_date']);
                  
                    if(is_file(base_path().'upload/property/commoncard/'.$property['cover_image']) && $property['cover_image'] != '')
                    {
                         $cover_image = base_url().'upload/property/commoncard/'.$property['cover_image'];
                    }
                    else
                    {
                        $cover_image = base_url().'upload/property/commoncard/no_img.jpg';
                    }
                     $show_hide_class='pro-text-hide';
                    if($campaign_id > 0 && $campaign_id != '')
                    {
                     
                      $show_hide_class ='';
                    }

                ?>
                            <div class="view-crowd-campaign">            
                      <div class="explore-campaigns">
           <div class="campaign-thumbnail">
                  <div class="campaign-img crowd-property-img">
                    <a href="javascript:void(0)">
                      <span class="overlay"></span>
                      <img src="<?php echo $cover_image;?>">
                    </a>
                    <div class="min-investment">
                     <?php if($campaign_id > 0 && $campaign_id != '')
                    {?>
                     <h3><?php echo set_currency($min_investment_amount);?></h3>

                      <p>Min Investment</p>
                       <?php }  else { ?>

                        <h3><?php echo set_currency($estimate);?></h3>

                      <p>Price / Estimate</p>
                       <?php } ?>
                    </div>
                  </div>
                  <div class="campaign-caption">
                    <ul class="property-info clearfix">
                      <li>
                        <span class="info-num"><?php echo $bedrooms;?></span>
                        <span class="icon-wrapper"><img src="<?php echo base_url() ?>images/icon-bed.png"></span>
                        <span class="info-name">bedrooms</span>
                      </li>
                      <li>
                        <span class="info-num"><?php echo $bathrooms;?></span>
                        <span class="icon-wrapper"><img src="<?php echo base_url() ?>images/icon-bath.png"></span>
                        <span class="info-name">bathrooms</span>
                      </li>
                      <li>
                        <span class="info-num"><?php echo $cars;?></span>
                        <span class="icon-wrapper"><img src="<?php echo base_url() ?>images/icon-car.png"></span>
                        <span class="info-name">cars</span>
                      </li>
                    </ul>
                    <div class="campaign-details">
                      <div class="campaign-title">
                        <h2><a href="<?php echo site_url('properties/'.$property_url);?>"><?php echo $property_address;?></a></h2>
                      </div>
                      <ul class="details-item <?php echo $show_hide_class;?>">
                        <li class="clearfix">
                            <span class="card-item-left">Shares Available</span>
                            <span class="card-item-right"><?php echo $share_available;?>/<?php echo $campaign_units;?></span>
                            <span class="card-item-line"></span>
                        </li>
                        <li class="clearfix">
                            <span class="card-item-left">Investment Close</span>
                            <span class="card-item-right"><?php echo $investment_close_date;?></span>
                            <span class="card-item-line"></span>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
          
          </div>   
          </div>  
          <?php } ?>
            </div>
            </li>  

      <?php } ?> 
            
         
            
  <?php } ?>