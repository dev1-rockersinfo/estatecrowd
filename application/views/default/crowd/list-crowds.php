
    <section>
	  	<div class="db-head">
	      	<div class="container">
	      		<div class="page-title">
		        	<h2>Crowds List</h2>
		        </div>
	      	</div>
	  	</div>
	  	<div class="grey-bg" id="section-area">	  		
	      	<div class="container">
	      		<div class="row">
		      		<div class="col-md-3 db-mc-left">
                    <?php echo $this->load->view('default/dashboard_sidebar'); ?>
		      			
		      		</div>
		      		<div class="col-md-9 db-mc-right">
           			<div class="crowd">
    					<ul class="crowd-list">
              <?php 
              if(is_array($user_crowds)) 
              { 
                  foreach ($user_crowds as $key => $crowd) {
                    # code...
                    $title = $crowd['title'];
                    $description = $crowd['description'];
                    $crowd_id = $crowd['crowd_id'];
                    $crowd_url = $crowd['crowd_url'];

                    $crowduserdata = UserData($crowd['user_id']);
                    $members = GetAllCrowdMember($crowd_id);
                    if(is_array($members))
                    {
                      $members = array_merge($crowduserdata, $members);
                    }
                    else
                    {
                      $members = $crowduserdata;
                    }
                    $count_member = 0;
                    if(is_array($members))
                    {
                      $count_member = count($members);
                    }
                  
                ?>

                    	<li class="clearfix">                        
                         <div class="crowd-lt clearfix">
                         <h3><a href="<?php echo site_url('crowds/'.$crowd_url)?>"><?php echo $title;?></a></h3>
                        <span class="crowd-desc"><?php echo $description;?> </span>
                    	</div>
                        <div class="crowd-rt">
                          		<div class="crowd-btn">
                          	  <span class="view-btn"> <a href="javascript://"><span>  <?php echo $count_member;?></span> Members </a></span> 
                              <span class="edit-crowd"><a href="<?php echo site_url('crowd/create_crowd/'.$crowd_id);?>"> <i class="fa fa-edit"></i>  </a></span>
                        	</div>
                            
                            
                          </div>
                        </li>  
                  
                  <?php
                    }

                   } else { ?>  

                    <li class="no_records_found"> There are no crowds. </li>
                  <?php } ?> 
                        
                    </ul>
  					 </div>
			      		
	      		</div>
	      	</div>
	  	</div>
    </section>
    