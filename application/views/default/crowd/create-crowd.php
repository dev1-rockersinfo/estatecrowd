<?php 
$create_text = 'Create';
if($crowd_id != '' &&  $crowd_id > 0)
{
  $create_text = 'Edit';
}

?>
    <section>
	  	<div class="db-head">
	      	<div class="container">
	      		<div class="page-title">
		        	<h2><?php echo $create_text;?> Crowd</h2>
		        </div>
	      	</div>
	  	</div>
	  	<div class="grey-bg" id="section-area">	  		
	      	<div class="container">
	      		<div class="row">
		      		<div class="col-md-3 db-mc-left">
            <?php echo $this->load->view('default/dashboard_sidebar'); ?>
		      			
		      		</div>
		      		<div class="col-md-9 db-mc-right">
                   
                    <div class="invite-mail">
                    
                    <form class="ec-form" action="<?php echo site_url('crowd/create_crowd/'.$crowd_id)?>" method="post" >
       <div class="form-group col-md-12">
   					<h3><?php echo $create_text;?> Crowd</h3> 
    				 <input type="text" name="title" class="form-control " value="<?php echo $title;?>" id="title" placeholder="Titile ">
             <span class="create_step_error"><?php echo form_error('title');?></span>
 				 </div>
                 <div class="form-group col-md-12">   					
    				<textarea class="form-control" id="description" name="description" placeholder="Description"><?php echo $description;?></textarea>

            <span class="create_step_error"><?php echo form_error('description');?></span>
                       
 				 </div>
                 <div class="form-group col-md-12">  
                
                 <div class="invite-media clearfix">	

           			
    			
                <a class="btn btn-default" data-target="#addmember" data-toggle="modal" href="" >  <i class="fa fa-plus"></i> Add Member </a>
              

<?php


 if(is_array($member_data)) 
{?>
       <h3>Members</h3> 
  <ul class="invited-list">

  <?php 

    foreach ($member_data as $key => $member_detail) {
   
      if ($member_detail['image'] != '') {
          $user_image = $member_detail['image'];
      } else {
          $user_image = 'no_man.jpg';
      }
      $comment_user_id = $member_detail['user_id'];
      $profile_slug = $member_detail['profile_slug'];
      if($profile_slug != '')
      {
        $profile_url = site_url('user/'.$profile_slug);
      }
      else
      {
        $profile_url = site_url('user/'.$comment_user_id);
      }
      $crowd_member_full_name = $member_detail['user_name'].' '.$member_detail['last_name'];
      $crowd_invite_id = $member_detail['id'];

  ?>
                   <li class="clearfix" id="remove_member_content_<?php echo $crowd_invite_id;?>">
                        <div class="invited-lt avtar-ssm">
                        <a href="<?php echo $profile_url;?>"> 
               <?php if ($user_image != '' && is_file("upload/user/user_medium_image/" . $user_image)) {
                        ?>
                        <img src="<?php echo base_url(); ?>upload/user/user_medium_image/<?php echo $user_image; ?>"/>
                    <?php
                    } else {
                        ?>
                        <img src="<?php echo base_url(); ?>upload/user/user_medium_image/no_man.jpg"/>
                    <?php } ?>
              </a>
                        </div>
                         <div class="invited-rt clearfix">
                         <h3><a href="<?php echo $profile_url;?>"><?php echo $crowd_member_full_name;?></a></h3>
                      
                      </div>
                        <div class="invited-btn">
                       <span class="remove" >
                       <a href="javascript://" class="trs" id="remove_member"  rel="<?php echo $crowd_invite_id;?>"><i class="fa fa-trash"></i></a>
                       </span>

                          </div>
                        </li>
                      
                        <?php } ?>
                        
                    </ul>

                <?php } ?>
				
                  </div>
                  
 				 </div>
                   <div class="form-group col-md-12"> 
                   <input type="hidden" name="crowd_id" id="crowd_id" value="<?php echo $crowd_id;?>"> 
                         <button class="btn btn-primary" type="submit"> <?php echo $create_text;?></button>
 				  </div>
                 
      				 </form>      
                    </div>
	      			</div>
	      	</div>
	  	</div>

         <div class="modal fade" id="addmember" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content invite-tab">
       <div class="modal-header">
       <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#byemail" aria-controls="byemail" role="tab" data-toggle="tab">By Email</a></li>
    <li role="presentation"><a href="#bysocial" aria-controls="bysocial" role="tab" data-toggle="tab">By Social</a></li>
    <li role="presentation"><a href="#investor" aria-controls="investor" role="tab" data-toggle="tab">Other Investor</a></li>
    
  </ul>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      
      </div>
<div class="modal-body clearfix">
          
           <div class="tab-content">
    <div class="tab-pane active" role="tabpanel" id="byemail">
      <div class="ec-form row">
       <div class="form-group col-md-12">
            <div id="myemail">
             <input type="text" placeholder="Email Address " name="email1" id="tags_1" class="form-control ">   
<p>Multiple email addresses should be separated with comma. </p>
<div id="show_msg" class="col-md-12">
                                        <span id="spemail1" class="invite-success"></span>
                                    </div>
                                    </div>
         </div>
                
                   <div class="form-group col-md-12">   
                    <button type="button" name="sendinvite" class="btn btn-primary"
                                   onclick="send_error();">Invite</button>
                      
         </div>
                
       </div>
    </div>
    <div role="tabpanel" class="tab-pane" id="bysocial">
    
    <div class="sc-buttons">
  
                    <div class="sc-fb">
                      <div class="social-text"><i class="fa fa-facebook fa-fw"></i> Invite From Facebook</div>
                    </div>

                   
                    <div class="sc-tw">
                      <div class="social-text"><i class="fa fa-twitter fa-fw"></i> Invite From Twitter</div>
                    </div>
                    <div class="sc-in">
                      <div class="social-text"><i class="fa fa-linkedin fa-fw"></i> Invite From Linkedin</div>
                      </div>
                      <div class="sc-gp">
                      <div class="social-text"><i class="fa fa-google-plus fa-fw"></i> Invite From Google Plus</div>
                     </div>
                    </div>

    
    </div>
    <div role="tabpanel" class="tab-pane" id="investor">
    <div class="search-investor">
    <div class="input-group input-group-lg">
   <span class="input-group-addon">
      <i class="fa fa-search"></i>
   </span>
   <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
   <input class="form-control input-lg" id="investor-search" name="investor-search" data-id="" placeholder="Search" type="text" autocomplete="off" onkeyup="autotext();">

    </div>
        <div id="autoc" class="in-autocomplete s-in-au "></div>
        <span id="suc_msg" class="invite-success" style="display: none;"></span>
        <span id="err_msg" class="invite-error" style="display: none;"></span>
        <div class="m-t-md">
            <input type="button" name="invite" id="invite_investor_btn" class="btn btn-primary" value="Invite" />
        </div>
 </div>
 </div>
             
 
  </div>

      </div>
      
    </div>
  </div>
</div>
    </section>

    <script type="text/javascript">
        function autotext() {

            var xmlHttp;
            try {
                xmlHttp = new XMLHttpRequest();// Firefox, Opera 8.0+, Safari
            }
            catch (e) {
                try {
                    xmlHttp = new ActiveXObject("Msxml2.XMLHTTP"); // Internet Explorer
                }
                catch (e) {
                    try {
                        xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    catch (e) {
                        //alert("<?php //echo NO_AJAX;?>");
                        return false;
                    }
                }
            }
            xmlHttp.onreadystatechange = function () {
                if (xmlHttp.readyState == 4) {
                    document.getElementById('autoc').innerHTML = xmlHttp.responseText;

                    if (document.getElementById('srhdiv')) {


                        $("#autoc").show();


                    }
                    else {
                        $("#autoc").hide();

                    }
                }
            }
            var text = document.getElementById('investor-search').value;

            text = text.replace(/[^a-zA-Z0-9]/g, '');
            xmlHttp.open("GET", "<?php echo site_url('crowd/search_investors');?>/"+ text, true);
            xmlHttp.send(null);
        }

        function selecttext(el) {

            var selectvalue = el.innerHTML;
            var ids = $(el).data('id');
            console.log(ids);
            $('#investor-search').data('id',ids);
            $("#investor-search").val(selectvalue);

            $("#autoc").hide();

        }

        $(document).on('click','#invite_investor_btn',function(){
            var crowd_id = $("#crowd_id").val();
            var name = $("#investor-search").val();
            var id = $("#investor-search").data('id');
            var pdata = { id:id , name: name, crowd_id: crowd_id };
            var url = "<?=site_url('crowd/invite_investor')?>";
            $.post( url,pdata)
                .done(function( data ) {
                    var response = jQuery.parseJSON(data);

                    if(response.success){
                        $('#err_msg').hide();
                        $('#suc_msg').html(response.success);
                        $('#suc_msg').show();
                    }else{
                        
                        $('#suc_msg').hide();
                        $('#err_msg').html(response.error);
                        $('#err_msg').show();
                    }
                })
                .fail(function() {
                    alert( "error" );
                })
        });

 function send_error() {

        $('#show_msg').html('');//change by darshan
        $('.get_invite_email_fild').remove();//change by darshan
        $('.invite-error').remove();//change by darshan
        var inv_email = $("#tags_1").val();
        var email_array = inv_email.split(',');

        var email_reg_exp = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;

//var recipient_message=trim(removeHTMLTags($("#recipient_message").val()));

        var recipient_message = $("#recipient_message").val();      //change by darshan  start
        if (inv_email == '') {
            alert('Email address is required');
            return false;
        }
        if (recipient_message == '') {
            alert('<?php echo RECIPIENT_MESSAGE_IS_REQUIRED ?>');
            return false;
        }
        //change by darshan  end
        for (var i = 0; i < email_array.length; i++) {

            $("#myemail").append('<input type="hidden" name="email' + i + '" id="email' + i + '" value="' + email_array[i] + '" class="get_invite_email_fild" />');
            $("#show_msg").append('<span id="spemail' + i + '" class="invite-success"></span><br>');
        }

        $('.get_invite_email_fild').each(function () {
            PopupOpen();
            var reci_email = $(this).val();
            var recipient_email = $.trim(reci_email);
            var recipient_id = $(this).attr('id');
            var siblin = $("#sp" + recipient_id);
            var crowd_id = $("#crowd_id").val();

            if (!email_reg_exp.test(recipient_email)) {
                siblin.show();

                siblin.text('Recipient email is invalid');
                siblin.removeClass('invite-success');
                siblin.addClass('invite-error');
                //return false;
            }
            else {
                siblin.hide();
                var res = $.ajax({
                    type: 'POST',

                    url: '<?php echo site_url('crowd/invite_member');?>',
                    data: {remail: recipient_email,crowd_id:crowd_id},
                    dataType: 'json',
                    cache: false,
                    async: false
                }).responseText;

                var inv_rpl = jQuery.parseJSON(res);
                //alert(inv_rpl.status);
                if (inv_rpl.status == 'success') {

                    $(this).val('');
                   

                    $('.tag').text('');
                    $('#tags_1').val('');   //change by darshan
                    $('.tag').css('display', 'none');
                    $("#crowd_id").val(inv_rpl.crowd_id);
                    siblin.show();
                    siblin.text('<?php echo INVITATION_HAS_BEEN_SENT_TO;?> ' + inv_rpl.email);
                    siblin.removeClass('invite-error');
                    siblin.addClass('invite-success');
                    PopupClose();
                    //return true;

                }
                else {
                     PopupClose();
                    siblin.show();
                    if (inv_rpl.status == 'exist') {

                        siblin.text('<?php echo YOU_ALREADY_SENT_INVITATION_TO_THIS_EMAIL;?> ' + inv_rpl.email);
                    }

                    else if (inv_rpl.status == 'not_invite') {
                        siblin.text('<?php echo YOU_CAN_NOT_INVITE_YOUR_SELF;?> ' + inv_rpl.email);
                    }

                   

                    else {

                        siblin.text('<?php echo INVITATION_IS_FAILED;?>');

                    }


                    siblin.removeClass('invite-success');
                    siblin.addClass('invite-error');
                    
                    //return false;
                }

            }

        });


    }

     //delete record from table
    $("body").on("click", "#remove_member", function (event) {

         var invite_request_id = $(this).attr("rel");

         var crowd_id = '<?php echo $crowd_id;?>';

        PopupOpen();
        if (confirm("Are you sure you want to delete member ?") == true) 
        {
            $.post("<?php echo site_url('crowd/remove_member')?>", {
            crowd_id: crowd_id,
            invite_request_id: invite_request_id,
            
        }, function (result) {
           
           $("#remove_member_content_"+invite_request_id).hide();
            PopupClose();
        });
   
         
        } else {
       
        }
       });


</script>
   
  

  