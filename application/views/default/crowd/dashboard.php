
    <section>
	  	<div class="db-head">
	      	<div class="container">
	      		<div class="page-title">
		        	<h2><?Php echo $title;?></h2>
		        </div>
	      	</div>
	  	</div>
	  	<div class="grey-bg" id="section-area">	  		
	      	<div class="container">
	      		<div class="row">
		      		<div class="col-md-3 db-mc-left">
                      <?php echo $this->load->view('default/dashboard_sidebar'); ?>
		      			
		      		</div>
		      		<div class="col-md-9 db-mc-right">
                   
                    <div class="invite-mail">
                    
                  
   <div class="form-group col-md-12">  
                
                 <div class="invite-media crowd-request clearfix">					
    			 <h3>Crowd Requests</h3>
               

					<ul class="invited-list">

          <?php if(is_array($crowd_request_data))
          {

              foreach ($crowd_request_data  as  $request_user_data) {
                # code...
                             
                   if ($request_user_data['image'] != '') {
                          $user_image = $request_user_data['image'];
                      } else {
                          $user_image = 'no_man.jpg';
                      }
                      $request_user_id = $request_user_data['user_id'];
                      $profile_slug = $request_user_data['profile_slug'];
                      if($profile_slug != '')
                      {
                        $profile_url = site_url('user/'.$profile_slug);
                      }
                      else
                      {
                        $profile_url = site_url('user/'.$comment_user_id);
                      }
                      $request_member_full_name = $request_user_data['user_name'].' '.$request_user_data['last_name'];
                      $crowd_request_id = $request_user_data['crowd_request_id'];
            ?>

                   <li class="clearfix" id="request_crowd_<?php echo $crowd_request_id;?>">
                   <div class="pull-left">
                        <div class="invited-lt avtar-ssm">
                       <a href="<?php echo $profile_url;?>" class="vc-user">
       <?php if ($user_image != '' && is_file("upload/user/user_small_image/" . $user_image)) {
                        ?>
                        <img src="<?php echo base_url(); ?>upload/user/user_small_image/<?php echo $user_image; ?>" title="<?php echo $request_member_full_name;?>">
                    <?php
                    } else {
                        ?>
                        <img src="<?php echo base_url(); ?>upload/user/user_small_image/no_man.jpg" 
                        title="<?php echo $request_member_full_name;?>">
                    <?php } ?>

                      </a>
                        </div>
                         <div class="invited-rt clearfix">
                         <h3><a href="<?php echo $profile_url;?>"><?php echo $request_member_full_name;?></a></h3>
                      
                    	</div>
                      </div>
                      <div class="pull-right">
                          <div class="btn-group btn-group-sm" role="group" aria-label="...">
                              <button type="button" onclick="updateRequest(<?php echo $crowd_request_id ?>,<?php echo $request_user_id ?>,'approve')" class="btn btn-success">Approve</button>
                              <button type="button" onclick="updateRequest(<?php echo $crowd_request_id ?>,<?php echo $request_user_id ?>,'reject')" class="btn btn-danger">Reject</button>
                          </div>

                </div>
                        </li>
                      
                        
                       
    <?php
        }

     } ?>


                        
                    </ul>
                  </div>
                  
 				 </div>
               
                 
       
                    </div>
	      			</div>
	      	</div>
	  	</div>
    </section>
    <script type="text/javascript">
   function updateRequest(crowd_request_id, request_user_id, type) {

  PopupOpen();

    var approvedata = {

            "crowd_id": '<?php echo $crowd_id;?>',
            "crowd_request_id": crowd_request_id,
            "request_user_id": request_user_id,
            "request_type": type
        };

   

    var getStatusUrl = '<?php echo site_url('crowd/updateCrowdRequest')?>';
    $.ajax({
        type: 'POST',
        url: getStatusUrl,
        data: approvedata,   // I WANT TO ADD EXTRA DATA + SERIALIZE DATA
        success: function (data) {

           $("#request_crowd_"+crowd_request_id).hide();
             PopupClose();
        }
    });

}
</script>
    