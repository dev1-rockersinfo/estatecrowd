<?php 
$title = $crowds[0]['title'];
$description = $crowds[0]['description'];
$crowd_id = $crowds[0]['crowd_id'];
$crowd_url = $crowds[0]['crowd_url'];
$crowd_user_id = $crowds[0]['user_id'];
$crowduserdata = UserData($crowds[0]['user_id']);

$crowd_share_url = site_url('crowds/'.$crowd_url);

$facebook_share_link = 'http://www.facebook.com/share.php?u='.$crowd_share_url.'&amp;t='.$title.'';

$twitter_share_link ='http://twitter.com/home?status='.$title.' '.$crowd_share_url.'';

$google_share_link ='https://plusone.google.com/_/+1/confirm?hl=en&url='.$crowd_share_url.'';

$linkedin_share_link = 'https://www.linkedin.com/shareArticle?mini=true&url='.$crowd_share_url.'&title='.$title.'&summary='.$description.'&source=LinkedIn';


 if ($loginuserdata[0]['image'] != '') {
        $user_image = $loginuserdata[0]['image'];
    } else {
        $user_image = 'no_man.jpg';
    }
$comment_allow = 'no';
$crowdmember = CheckCrowdMember($crowd_id,$this->session->userdata('user_id'));
if($crowdmember == 1 || $crowd_user_id == $this->session->userdata('user_id'))
{
  $comment_allow = 'yes';
}
?>

<?php
$attributes = array(
    'id' => 'comment_image_form',
    'name' => 'comment_image_form',
    'enctype' => 'multipart/form-data',
    'class' => 'edit_project',
    'method' => 'post',
    'accept-charset' => 'UTF-8'
);
echo form_open_multipart('crowd/save_comment/', $attributes);
?>
<input type="hidden" name="project_id" id="project_id" value="<?php echo $crowd_id ;?>">
<div class="sr-only" id="form_comment_image"></div>
</form>
    <section>
	  	<div class="db-head">
	      	<div class="container">
  <div class="col-md-8 view-crowd-title">
	      		<div class="page-title">
		        	<h2><?php echo $title;?></h2>
    <p class="des"><?php echo $description;?> </p>
    </div>
		        </div>
  <div class="col-md-4 join-this-crowd" >
  <div id="default_text_add">
  <?php 
    if($crowd_user_id != $this->session->userdata('user_id'))

    { 

        $crowd_data = crowd_status($this->session->userdata('user_id'),$crowd_id,$crowd_url);

        
        
        if($crowd_data['common_text_for_status'] != '')
        { ?>  <p class="interested-request">
        <?php 
          echo $crowd_data['common_text_for_status'];
          ?>
       </p> <?php }
        else
        {
          echo $crowd_data['button_link'];
        }

        

    } ?>
    </div>

     <div id="pending_text" style="display:none;">
              
                    <p class="interested-request"><?php echo 'Your Request is still in pending'; ?> </p>
               
            
            </div>
  </div>
	      	</div>
	  	</div>
	  		  		
	      	
    </section>
    <section class="view-crowd-content">
    <div class="container">
  			
            <div class="view-crowd-left col-md-8">  			
            <div class="panel panel-default ec-panel">
            <div class="panel-body"> 
            <div class="view-crowd-rev-wrap">
            <ul class="view-crowd-review" id="comment-list">            
           
            </ul>
            
            </div>
            <?php if($comment_allow == 'yes'){?>
            <div class="view-crowd-comment" id="add_comment">
            <h3>Comment</h3>
            <div class="com-pic">
              <?php if ($user_image != '' && is_file("upload/user/user_medium_image/" . $user_image)) {
                        ?>
                        <img src="<?php echo base_url(); ?>upload/user/user_medium_image/<?php echo $user_image; ?>"/>
                    <?php
                    } else {
                        ?>
                        <img src="<?php echo base_url(); ?>upload/user/user_medium_image/no_man.jpg"/>
                    <?php } ?>
            </div>
            <div class="com-right-area add_comment">
            <div class="tip right">
            <textarea rows="7" name="comment" id="comment" data-fieldlength="500" cols="45" class="form-control char_limited_input"></textarea>
            <span class="create_step_error comment_error"></span>
            </div>
            <div > <a href="javascript://" id="attach_file_name"> </a> <a href="javascript://" class="delete_image" style="display: none"><i class="fa fa-times"></i></a> </div>
            <div class="suggest-pro" id="SuggestArea" style="display:none">
            <div class="input-group">
  <span  class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
  <input id="property_address" name="property_address" type="text" class="ui-autocomplete-input form-control"  autocomplete="off" aria-describedby
  ="basic-addon1" placeholder="Search Property" onkeyup="autotext();">
  <div id="autoc" class="in-autocomplete"></div> 
  <span id="MemClear" class="input-group-addon"><i class="fa fa-times-circle"></i>
</span>
</div>
        
            </div>
            <div class="tip-footer">
            <div class="tale">
            <span class="attach"> <label for="fileToUpload"><i class="fa fa-paperclip"></i> Attach</label>
            <input type="File" name="comment_image" id="fileToUpload">
  
            </span>
            <span class="share" id="SuggerstPro">
            <a  href="javascript:void(0)"><i class="fa fa-lightbulb-o"></i>

              Suggest Property</a></span>
               </div>
  <div>             
 <span class="create_step_error image_error"></span></div>
            <div class="btn-group pull-right">
            <input type="hidden" name="crowd_id" id="crowd_id" value="<?php echo $crowd_id;?>">
              <input type="hidden" name="comment_id" id="comment_id" value="">
                 <button class="btn btn-primary btn-small text-uppercase" type="button" id="post_comment">Post</button>
                 </div>
             </div>
            </div>
            </div>

            <?php } ?>
            </div>
            
            
            </div> 
            
              
      
  			</div>
            
            <div class="view-crowd-right col-md-4">
<?php 






if(is_array($member_data))
{
  $member_data = array_merge($crowduserdata, $member_data);
}
else
{
  $member_data = $crowduserdata;
}




if(is_array($member_data)) { ?>

           <div class="sidebar view-crowd-sb">
   <div class="sidebar-top clearfix">      
          <div class="inv-title">
               <span class="crwd-mem-left">Members</span>
                            <span class="crwd-mem-right"><?php echo count($member_data);?></span>
                            <span class="crwd-mem-line"></span> 
                
            </div>
      </div>
     <div class="clearfix"></div>
     <div class="view-crowd-members">
     <ul>
<?php foreach ($member_data as $key => $member_user_data) {

   if ($member_user_data['image'] != '') {
          $user_image = $member_user_data['image'];
      } else {
          $user_image = 'no_man.jpg';
      }
      $comment_user_id = $member_user_data['user_id'];
      $profile_slug = $member_user_data['profile_slug'];
      if($profile_slug != '')
      {
        $profile_url = site_url('user/'.$profile_slug);
      }
      else
      {
        $profile_url = site_url('user/'.$comment_user_id);
      }
      $crowd_member_full_name = $member_user_data['user_name'].' '.$member_user_data['last_name'];
  # code...
?>

     <li>
     <a href="<?php echo $profile_url;?>" class="vc-user">
       <?php if ($user_image != '' && is_file("upload/user/user_medium_image/" . $user_image)) {
                        ?>
                        <img src="<?php echo base_url(); ?>upload/user/user_medium_image/<?php echo $user_image; ?>" title="<?php echo $crowd_member_full_name;?>"/>
                    <?php
                    } else {
                        ?>
                        <img src="<?php echo base_url(); ?>upload/user/user_medium_image/no_man.jpg" title="<?php echo $crowd_member_full_name;?>"/>
                    <?php } ?>

     </a></li>

     <?php }?>
      
      
     
     </ul>
    <!--  <div class="view-all-mem"><a href="javascript:void()">View All Members</a></div> -->
     </div>
    
   </div>

   <?php } ?>

   <div class="crowd-share sidebar ">
   <div class="sidebar-top">
   
   <span class="crwd-mem-left">Share</span>
   

   </div>
      <div class="pa-sidebar-social">
          <ul class="socail-icon">
          <li>
            <a onclick="return popitup('<?php echo $facebook_share_link;?>')" href="<?php echo $facebook_share_link;?>"   ><i class="fa fa-facebook"></i> </a>  </li>
          <li><a onclick="return popitup('<?php echo $twitter_share_link;?>')" href="<?php echo $twitter_share_link;?>"><i class="fa fa-twitter"></i></a></li>
          <li>  <a onclick="return popitup('<?php echo $linkedin_share_link;?>')" href="<?php echo $linkedin_share_link;?>" ><i class="fa fa-linkedin"></i></a></li>
          <li> <a onclick="return popitup('<?php echo $google_share_link;?>')" href="<?php echo $google_share_link;?>"><i class="fa fa-google-plus"></i></a></li>
         
          </ul>
      </div>
   </div>

            </div>
    </div>
    </section>
    <script src="<?php echo base_url(); ?>js/jquery.form.js"></script>
  
    <script type="text/javascript">

     $(document).ready(function() {
			 $("#SuggerstPro a").click(function(){
    $("#SuggestArea").show();
});
	
			 })
		 $("#MemClear").click(function(){
			 $("#SuggestArea").hide();
    $("#property_address").val('');
});
	

    $("#autoc").hide();

    function autotext() {
        var xmlHttp;
        try {
            xmlHttp = new XMLHttpRequest();// Firefox, Opera 8.0+, Safari
        }
        catch (e) {
            try {
                xmlHttp = new ActiveXObject("Msxml2.XMLHTTP"); // Internet Explorer
            }
            catch (e) {
                try {
                    xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                catch (e) {
                    //alert("<?php //echo NO_AJAX;?>");
                    return false;
                }
            }
        }
        xmlHttp.onreadystatechange = function () {
            if (xmlHttp.readyState == 4) {
                document.getElementById('autoc').innerHTML = xmlHttp.responseText;

                if (document.getElementById('srhdiv')) {


                  $("#autoc").show();


                }
                else {
                   $("#autoc").hide();
                    
                }
            }
        }
        var text = document.getElementById('property_address').value;
        text = text.replace(/[^a-zA-Z0-9]/g, '');
        xmlHttp.open("GET", "<?php echo site_url('crowd/search_auto_property');?>/" + text, true);
        xmlHttp.send(null);
    }
    function selecttext(el) {

        var selectvalue = el.innerHTML;
       
        $("#property_address").val(selectvalue);
        $("#autoc").hide();
       
    }


$(function () {

    $("body").on("change", "#fileToUpload", function (event) {

        readURL(this);
        $('#form_comment_image').children().remove();
        $(this).prependTo('#form_comment_image').addClass('hidden-file-field');
        document.getElementById("comment").focus();
        //$(this).clone(true).prependTo("#picture_file_field_display").removeClass('hidden-file-field').removeAttr('id');
    });
});

 var crowd_id = '<?php echo $crowd_id?>';

$(document).ready(function () {

 
  
    // bind form id and provide a simple callback function
    var options = {
        target: '#output1',   // target element(s) to be updated with server response

        beforeSubmit: showRequest,  // pre-submit callback
        complete: function () {

        },  // post-submit callback 
        success: showResponseSuccess,
        dataType: 'json',

    };
    if ((pic = jQuery('#comment_image_form')).length)
        pic.ajaxForm(options);

    //load team member data;
    list_comment();
    $("body").on("click", "#post_comment", function (event) {

        PopupOpen();
        $('#comment_image_form').submit();
        return false;
    });

     //delete record from table
    $("body").on("click", "#delete_comment", function (event) {

      	 var comment_id = $(this).attr("rel");
        PopupOpen();
        if (confirm("Are you sure you want to delete comment ?") == true) 
        {
        $.post("<?php echo site_url('crowd/delete_comment')?>", {
            crowd_id: crowd_id,
            comment_id: comment_id,
            
        }, function (result) {
            BlankAllData();
            PopupClose();
        });
        }
        else
        {
          
        }
    });

      //delete record from table
    $("body").on("click", ".delete_image", function (event) {

      	 var comment_id = $("#comment_id").val();
        PopupOpen();
        $.post("<?php echo site_url('crowd/delete_image_comment')?>", {
            crowd_id: crowd_id,
            comment_id: comment_id,
            
        }, function (result) {
           $(".image_error").html('');
		     $("#attach_file_name").html('');
		     $(".delete_image").hide();
		     $("#fileToUpload").val('');
            PopupClose();
        });
    });
 

    //load particular team record with form
    $("body").on("click", "#edit_comment", function (event) {



        var comment_id = $(this).attr("rel");
        // var scr = $(this).data("scr");
        // goToByScroll(scr);
        $.post("<?php echo site_url('crowd/edit_comment')?>", {
            crowd_id: crowd_id,
            comment_id : comment_id,
            
        }, function (result) {

            var result = $.parseJSON(result);
            var error = result.error;
            var msg = result.msg;
          
            if (error == true) {
               
            }
            else {

            	var comment = result.comment_data.comment;
            	var image_name = result.comment_data.image_name;
               $("#comment").val(comment);
               $("#comment_id").val(result.comment_data.id);
               if(image_name != '')
               {
               		$("#attach_file_name").html(image_name);
               		$(".delete_image").show();
               }

            }
        });
    });
    

});
/*list team member data */
function list_comment() {

    $.post("<?php echo site_url('crowd/list_comment')?>", {crowd_id: crowd_id}, function (result) {

        $('#comment-list').html(result);

    });
}
/*call before team member data inserted for merging file image and form data */
function showRequest(formData, jqForm, options) {

    var data = $('.add_comment').closest('#add_comment').find('input,textarea,select').serializeArray();


    formData.push.apply(formData, data);

    var queryString = $.param(formData);
    return true;
}

// Called after successfully save team member data
function showResponseSuccess(data) {

    //alert(data.email_error);
    PopupClose();
    var error = data.error;
    var msg = data.msg;
    var url = data.url;
   
    if (url != '') {
        //  location.href=url;
    }

    if (error == true) {

       $(".comment_error").html(data.comment_error);
       $(".image_error").html(data.image_error);
    }
    else {
      
        BlankAllData();
    }
   
   

}

// Called after successfully save team member data
function BlankAllData() {
  
    
    list_comment();
     $("#comment").val('');
     $("#comment_id").val('');
     $(".comment_error").html('');
     $(".image_error").html('');
     $("#attach_file_name").html('');
     $(".delete_image").hide();

}

//used for show image when browse click 
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        var imgpath = '<?php echo base_url()?>images/member_img.jpg';
        reader.onload = function (e) {

               if(e.target.result.indexOf("data:image/png") > -1 || e.target.result.indexOf("data:image/jpeg") > -1 || e.target.result.indexOf("data:image/gif") > -1 ){
               
                 $('#image_name').attr('src', e.target.result);
              }else{
                $('#image_name').attr('src',imgpath);
                $('.member_image_error').html("<?php echo PLEASE_UPLOAD_A_JPG_PNG_GIF_FILE;?>");
                }
           
        }

        reader.readAsDataURL(input.files[0]);
    }
}
function popitup(url) {
  
    newwindow = window.open(url, 'name', 'height=400,width=450');
    if (window.focus) {
        newwindow.focus()
    }
    return false;
}

function crowd_request(crowd_id) {

        PopupOpen();
        var mapdata = {
            "crowd_id": crowd_id,
        };
       

        var getStatusUrl = '<?php echo site_url('crowd/AddCrowdRequest')?>';
        $.ajax({
            url: getStatusUrl,
            dataType: 'json',
            type: 'POST',
            timeout: 99999,
            global: false,
            data: mapdata,
            success: function (data) {

                if (data.success != "") {
                    $("#default_text_add").hide();
                    $("#pending_text").show();
                }
                PopupClose();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
            }
        });
    }

  function leave_crowd_request(crowd_id) {


        PopupOpen();
        var mapdata = {
            "crowd_id": crowd_id,
        };
       
       
        var getStatusUrl = '<?php echo site_url('crowd/leaveCrowdRequest')?>';
        $.ajax({
            url: getStatusUrl,
            dataType: 'json',
            type: 'POST',
            timeout: 99999,
            global: false,
            data: mapdata,
            success: function (data) {

                if (data.success != "") {
                   $("#default_text_add a").html('join this crowd');
                 
                }
                PopupClose();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
            }
        });
    }

		</script>
