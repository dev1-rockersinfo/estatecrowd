<?php 

$sitename = $site_setting['site_name'];
$session = check_user_authentication($redirect = false);
$site_logo = $site_setting['site_logo'];
$site_logo_hover = $site_setting['site_logo_hover'];

$user_id = $this->session->userdata('user_id');
$user_name = $this->session->userdata('user_name');
$last_name = $this->session->userdata('last_name');

$langs = get_supported_lang();
$this->active = $this->uri->uri_string();
$this->strdd = explode("/", $this->active);
//print_r($this->strdd) ;
if (!isset($this->strdd[1])) {
    $this->strdd[1] = '';
}
if (!isset($this->strdd[2])) {
    $this->strdd[2] = '';
}

$projectControllerName=projectcontrollername();
?>
<nav id="affix-nav" class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="push" data-target="#navbar" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand ec-navbar-brand" href="<?php echo base_url();?>">
      <img src="<?php echo base_url(); ?>upload/orig/<?php echo $site_logo; ?>"/></a>
    </div>

    <div class="collapse navbar-collapse" id="navbar">
      <ul class="nav navbar-nav ec-navbar">
        <li><a href="<?php echo site_url('content/about-us');?>" class="hidden-md">About Us</a></li>
        <li><a href="<?php echo site_url('content/how-it-works');?>">How It Works</a></li>
        <li><a href="<?php echo site_url('faq'); ?>" class="hidden-md">FAQ's</a></li>
        <li><a href="<?php echo site_url('search/campaign'); ?>">Explore Campaigns</a></li>
        <li><a href="<?php echo site_url('search/advance_search'); ?>">Property Search</a></li>
      </ul>
    </div>
  </div>
</nav>