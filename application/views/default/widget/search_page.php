<style type="text/css">
  .home-search-area {
    background: #1b6b95 none repeat scroll 0 0;
    margin-top: 0px;
    text-align: center;
  }
  .nomargin{
    margin: 0px;
  }
</style>
<?php $this->load->view("default/widget/widgets_header"); ?>
<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
<?php $attributes = array( 'name' => 'frmsearch', 'id' => 'frmsearch', 'method' => 'get', 'autocomplete' => 'off'); echo form_open('api/property/widget_search', $attributes);?>
            
  <div class="home-search-area">
    <div class="search-bg clearfix">
      <div class="select-search" id="searchType">
        <select class="selectpicker">
          <option value="1">Investment Amount?</option>
          <option value="2">Investment Suburb</option>
          <option value="3">Investment Address</option>
        </select>
        <input type="hidden" id="main_search" name="main_search">  
      </div>
      <div class="textbox-search" style="position: relative;">
        
        <input type="text" id="investment_amount" name="investment_amount" value="<?php echo $this->input->get('investment_amount'); ?>" placeholder="how much do you want to invest?">  
        <input type="text" id="investment_suburb" name="investment_suburb" value="<?php echo $this->input->get('investment_suburb'); ?>" style="display: none" placeholder="What suburb do you want to invest in?" onkeyup="autotext_suburb();">

         <div id="autoc_suburb" class="in-autocomplete"></div> 

         <input type="text" id="investment_address" name="investment_address" value="<?php echo $this->input->get('investment_address'); ?>"style="display: none" placeholder="Enter address of investment property...">

         <div id="locationMap" class="gmap3 google-map"></div>


      </div>
      <div class="button-search">
        <button class="btn btn-search" type="submit">
          <i class="icon-magnifier"></i>
          Search
        </button>
      </div>
    </div>
  </div>
</form>
<div class="container">
    <div class="row" id="ajax_property">

     <?php 

 
       
           if($search_properties) 
           {
                foreach($search_properties as $search_property)
                {

                  $data['search_property'] = $search_property;
                  
                  $this->load->view('default/widget/widgets_card', $data);
          
              }
               if($total_rows > 12)
              { ?>
             <div class="bottom-link m-t-lg">
              <a href="javascript://" class="btn btn-default more_properties" onclick="show_more_property(12)"> Show More Properties </a>
              </div>
           <?php }
           
            } else {
          ?>
          <div class="row" id="ajax_property">
            <br>
            <div class="alert alert-info"><h2 class="nomargin">Search Property...</h2></div>
          </div>
          <?php } ?>
          
         
          </div>
        
    </div>


<?php $this->load->view("default/widget/widgets_footer"); ?>

<script type="text/javascript">
  $(document).ready(function(){
    var main_search = '<?php echo $this->input->get('main_search'); ?>';
    $('.selectpicker').val(main_search);
    if(main_search == 1){
      $("#main_search").val(main_search);
      $("#investment_amount").show();
      $("#investment_suburb").hide();
      $("#investment_address").hide();
      $("#investment_suburb").val('');
      $("#investment_address").val('');
    } else if(main_search == 2){
      $("#main_search").val(main_search);
      $("#investment_amount").hide();
      $("#investment_suburb").show();
      $("#investment_address").hide();
      $("#investment_amount").val('');
      $("#investment_address").val('');
    } else if(main_search == 3){
      $("#main_search").val(main_search);
      $("#investment_amount").hide();
      $("#investment_suburb").hide();
      $("#investment_address").show();
      $("#investment_suburb").val('');
      $("#investment_amount").val('');

    }
  });

  $('.selectpicker').on('change', function(){
    var selected = $(this).find("option:selected").attr("value");
    if(selected == 1){

      $("#main_search").val(selected);
      $("#investment_amount").show();
      $("#investment_suburb").hide();
      $("#investment_address").hide();
      $("#investment_suburb").val('');
      $("#investment_address").val('');
    } else if(selected == 2){
      $("#main_search").val(selected);
      $("#investment_amount").hide();
      $("#investment_suburb").show();
      $("#investment_address").hide();
      $("#investment_amount").val('');
      $("#investment_address").val('');
    } else if(selected == 3){
      $("#main_search").val(selected);
      $("#investment_amount").hide();
      $("#investment_suburb").hide();
      $("#investment_address").show();
      $("#investment_suburb").val('');
      $("#investment_amount").val('');

    }
  });

  $("#autoc_suburb").hide();

  function autotext_suburb() {
      var xmlHttp;
      try {
          xmlHttp = new XMLHttpRequest();// Firefox, Opera 8.0+, Safari
      }
      catch (e) {
          try {
              xmlHttp = new ActiveXObject("Msxml2.XMLHTTP"); // Internet Explorer
          }
          catch (e) {
              try {
                  xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
              }
              catch (e) {
                  //alert("<?php //echo NO_AJAX;?>");
                  return false;
              }
          }
      }
      xmlHttp.onreadystatechange = function () {
          if (xmlHttp.readyState == 4) {
              document.getElementById('autoc_suburb').innerHTML = xmlHttp.responseText;

              if (document.getElementById('srhdiv')) {


                $("#autoc_suburb").show();


              }
              else {
                 $("#autoc_suburb").hide();
                  
              }
          }
      }
      var text = document.getElementById('investment_suburb').value;
      text = text.replace(/[^a-zA-Z0-9]/g, '');
      xmlHttp.open("GET", "<?php echo site_url('search/search_auto');?>/" + text, true);
      xmlHttp.send(null);
  }
  function selecttext_suburb(el) {

      var selectvalue = el.innerHTML;
     
      $("#investment_suburb").val(selectvalue);
      $("#autoc_suburb").hide();
     
  }


  function toggle() {

    var ele = document.getElementById("suburb-list");

    var text = document.getElementById("see-suburbs");

    if(ele.style.display == "block") {

            ele.style.display = "none";

        text.innerHTML = "See All Suburbs";
    }
    else {
        ele.style.display = "block";
        text.innerHTML = "Less Suburbs";
    }       
  };

  $(window).bind("load", function () {
    $('#dvLoading').fadeOut(2000);
    $('#mainbg_div').fadeIn(2000);
    $('.min_height').css('minHeight', 0);
    $(function () {
      $('#locationMap').gmap3();

      $('#investment_address').autocomplete({
        componentRestrictions: {country: "us"},
        source: function () {
          $("#locationMap").gmap3({
            action: 'getAddress',
            address: $(this).val(),
            callback: function (results) {
              if (!results) return;
              $('#investment_address').autocomplete(
                'display',
                results,
                false
              );
            }
          });
        },
        cb: {
          cast: function (item) {
            return item.formatted_address;
          },             
        }
      });
    });
  });
</script>
<script type="text/javascript" src="http://code.jquery.com/jquery-migrate-1.0.0.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/map/gmap3.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/map/jquery-autocomplete.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/map/jquery-autocomplete.css"/>