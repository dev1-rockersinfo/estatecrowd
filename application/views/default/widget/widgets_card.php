<?php
  

                    $campaign_id = $search_property['campaign_id'];
                    $property_id = $search_property['property_id'];
                    $property_user_id = $search_property['user_id'];
                    $estimate = $search_property['estimate'];
                    $property_suburb = $search_property['property_suburb'];
                    $property_sub_type = $search_property['property_sub_type'];
                    $min_investment_amount = $search_property['min_investment_amount'];
                    $property_postcode = $search_property['property_postcode'];
                    $property_address = $search_property['property_address'];
                    $property_url = $search_property['property_url'];
                    $status = $search_property['status'];
                    $bedrooms = $search_property['bedrooms'];
                    $bathrooms = $search_property['bathrooms'];
                    $cars = $search_property['cars'];
                    $campaign_units = $search_property['campaign_units'];
                    $share_available = $campaign_units - $search_property['campaign_units_get'];
                    $investment_close_date = GetDaysLeft($search_property['investment_close_date']);

                  
                    if(is_file(base_path().'upload/property/commoncard/'.$search_property['cover_image']) && $search_property['cover_image'] != '')
                    {
                         $cover_image = base_url().'upload/property/commoncard/'.$search_property['cover_image'];
                    }
                    else
                    {
                        $cover_image = base_url().'upload/property/commoncard/no_img.jpg';
                    }
                    $view_text = 'VIEW PROPERTY';
                    $show_hide_class='pro-text-hide';
                    if($campaign_id > 0 && $campaign_id != '')
                    {
                      $view_text = 'VIEW CAMPAIGN';
                      $show_hide_class ='';
                    }

                    $sf_url = "https://dev-covesta.cs6.force.com/CoVestaEP/s/new-syndicate-by-property?";

                    $sf_url .= "propertyid=".$property_id."&address=".urlencode($property_address)."&suburb=".urlencode($property_suburb)."&postcode=".$property_postcode;

            ?>
                   <div class="explore-campaigns col-md-4">
           <div class="campaign-thumbnail">
                  <div class="campaign-img">
                    <a onclick="window.top.location.href = '<?php echo $sf_url; ?>';">
                      <span class="overlay"></span>
                      <img src="<?php echo $cover_image;?>">
                    </a>
                    <div class="min-investment">
                    <?php if($campaign_id > 0 && $campaign_id != '')
                    {?>
                     <h3><?php echo set_currency($min_investment_amount);?></h3>

                      <p>Min Investment</p>
                       <?php }  else { ?>

                        <h3><?php echo set_currency($estimate);?></h3>

                      <p>Price / Estimate</p>
                       <?php } ?>
                    </div>
                  </div>
                  <div class="campaign-caption">
                    <ul class="property-info clearfix">
                      <li>
                        <span class="info-num"><?php echo $bedrooms;?></span>
                        <span class="icon-wrapper"><img src="<?php echo base_url();?>images/icon-bed.png"></span>
                        <span class="info-name">bedrooms</span>
                      </li>
                      <li>
                        <span class="info-num"><?php echo $bathrooms;?></span>
                        <span class="icon-wrapper"><img src="<?php echo base_url();?>images/icon-bath.png"></span>
                        <span class="info-name">bathrooms</span>
                      </li>
                      <li>
                        <span class="info-num"><?php echo $cars;?></span>
                        <span class="icon-wrapper"><img src="<?php echo base_url();?>images/icon-car.png"></span>
                        <span class="info-name">cars</span>
                      </li>
                    </ul>
                    <div class="campaign-details">
                      <div class="campaign-title">
                        <h2><a onclick="window.top.location.href = '<?php echo $sf_url; ?>';"><?php echo $property_address;?></a></h2>
                      </div>
                      <ul class="details-item <?php echo $show_hide_class;?>">
                        <li class="clearfix">
                            <span class="card-item-left">Shares Available</span>
                            <span class="card-item-right"><?php echo $share_available;?>/<?php echo $campaign_units;?></span>
                            <span class="card-item-line"></span>
                        </li>
                        <li class="clearfix">
                            <span class="card-item-left">Investment Close</span>
                            <span class="card-item-right"><?php echo $investment_close_date;?></span>
                            <span class="card-item-line"></span>
                        </li>
                      </ul>
                    </div>
                    <div class="explore-more">
                    <!-- <a type="button" class="btn btn-primary btn-small  text-uppercase" href="<?php echo $sf_url; ?>"><?php //echo $view_text;?></a> -->
                    <button name="propertyid" type="submit" class="btn btn-primary btn-small  text-uppercase" onclick="window.top.location.href = '<?php echo $sf_url; ?>';">Select Property</button>
                    </div>
                    
                    
                  </div>
                </div>
          
          </div>