<script type="text/javascript">

    $(document).ready(function () {

        $('#paging').pajinate({

            items_per_page: 10,

            abort_on_small_lists: true,

            item_container_id: '.paging',

            nav_panel_id: '.pro-pagination',


        });

    });

</script>

<?php echo $this->load->view('default/dashboard_sidebar'); ?>



<section>

    <div class="container padTB50">

        <div class="page-header">

            <h1><?php echo REVIEW_ALL_ACTIVITY; ?></h1>

        </div>

        <div id="paging">

            <div class="inner-container-bg">

                <ul class="paging media-list in-media-list activity-list">

                    <?php

                    if ($activities) {


                        foreach ($activities as $all_activities) {

                            $datetime = $all_activities['datetime'];
                            $action_detail = $all_activities['action_detail'];
                            $action = $all_activities['action'];
                            $activity_icon = '';
                            if ($action == 'follow') $activity_icon = 'ico_follow.png';
                            if ($action == 'unfollow') $activity_icon = 'ico_unfollow.png';
                            if ($action == 'signup') $activity_icon = 'ico_verify.png';
                            if ($action == 'update_account') $activity_icon = 'ico_update.png';
                            if ($action == 'accredetial_request') $activity_icon = 'ico_acredential-request.png';
                            if ($action == 'update') $activity_icon = 'ico_update.png';
                            if ($action == 'funded') $activity_icon = 'ico_fund-added.png';
                            if ($action == 'commented') $activity_icon = 'ico_comment.png';
                            if ($action == 'draft_project') $activity_icon = 'ico_created.png';
                            if ($action == 'submit_project') $activity_icon = 'ico_submit.png';
                            if ($action == 'project_active') $activity_icon = 'icon_activate.png';
                            if ($action == 'project_inactive') $activity_icon = 'ico_inactive.png';
                            if ($action == 'project_approve') $activity_icon = 'ico_approve.png';
                            if ($action == 'project_declined') $activity_icon = 'ico_declined.png';
                            if ($action == 'project_deleted') $activity_icon = 'ico_delete.png';
                            if ($action == 'submit_by_owner') $activity_icon = 'ico_submit.png';
                            if ($action == 'submit_by_admin') $activity_icon = 'ico_admin.png';
                            if ($action == 'team_member_added') $activity_icon = 'ico_team-member.png';
                            if ($action == 'intrest_request') $activity_icon = 'ico_interest-request.png';
                            if ($action == 'access_request') $activity_icon = 'ico_acces-request.png';
                            if ($action == 'upload_contract') $activity_icon = 'ico_upload.png';
                            if ($action == 'approve_contract') $activity_icon = 'ico_approve.png';
                            if ($action == 'declined_contract') $activity_icon = 'ico_declined.png';
                            if ($action == 'acknowledge_contract') $activity_icon = 'ico_acknowledgement.png';
                            if ($action == 'approve_acknowledge_contract') $activity_icon = 'ico_approve-acknowledgement-receipt.png';
                            if ($action == 'declined_acknowledge_contract') $activity_icon = 'ico_declined.png';
                            if ($action == 'submmited_tracking') $activity_icon = 'ico_submit.png';
                            if ($action == 'confirmed_submmited_tracking') $activity_icon = 'ico_confirmed.png';
                            if ($action == 'project_success') $activity_icon = 'ico_project-success.png';
                            if ($action == 'project_unsuccess') $activity_icon = 'ico_project-unsuccessful.png';

                            ?>

                            <li class="media">

                                <div class="media-left icon-outer">
									<span class="share-i circle-ico">
									<img
                                        src="<?php echo base_url() ?>upload/activity_icon/<?php echo $activity_icon; ?>">
									</span></div>

                                <div class="media-body">

                                    <p class="comment-no"><?php echo $action_detail; ?></p>

                                    <p><span class="media-time"><?php echo getDuration($datetime); ?></span></p>

                                </div>

                            </li>

                        <?php
                        }
                    } else {
                        ?>

                        <div class="no-data"><?php echo THERE_ARE_NO_ACTIVITIES; ?></div>

                    <?php

                    }

                    ?>

                </ul>

            </div>

            <div class="pro-pagination"></div>

        </div>

    </div>

</section>

