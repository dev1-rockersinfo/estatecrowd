
<section>
<div class="db-head">
<div class="container">
<div class="page-title">
<h2>Estate Crowd User Verification</h2>
</div>
</div>
</div>
<div class="f-ls-full-bg-img " id="section-area">           
<div class="container">
<div class="row">           
<div class="col-md-10 col-md-offset-1">  
<div class="first-l-s">


 <?php
    if ($this->session->flashdata('success_message_user') != '') {
        ?>
        <div class="alert-success alert alert-message">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('success_message_user'); ?>
        </div>
            <?php
            } 
            ?>

<form class="ec-form" action="<?php echo site_url('accreditation/personal') ?>" method="post" id="personal" name="personal">
<div class="row userver-head-ex">
<div class="form-group col-md-12">
<label for="card-number">USER SELECT :</label>
<div class="c-s-radio-group">
<div class="form-control row">
<div class="radio radio-danger col-md-6 ">
<input type="radio"  value="0" id="radio1" name="user_select_type" <?php if($user_select_type == 0) { echo 'checked="checked"';}?> >
<label for="radio1">
Australian Citizen/ Permanent Resident
</label>
</div>
<div class="radio radio-danger col-md-6 ">
<input type="radio"  value="1" id="radio2" name="user_select_type" <?php if($user_select_type == 1) { echo 'checked="checked"';}?> >
<label for="radio2">
Foreign National
</label>
</div>
</div>
</div>

</div> 
</div>   
<div class="row" id="Auscitizen" style="display:<?php if($user_select_type == 0) { echo 'block'; } else { echo 'none';}?>" >
<div class="form-group col-md-6 f-ls-check-box">
<label for="">Full Name <span class="form_star">*</span></label>
<input type="text" class="form-control" name="username" id="c-name" placeholder="" value="<?php echo $username;?>">
 <span class="create_step_error"><?php echo form_error('username'); ?></span>

</div>
<div class="form-group col-md-6">
<label for="">Date of Birth <span class="form_star">*</span></label>
<input class="form-control" type="text"  name="dob" id="datepicker" placeholder="DD/MM/YYYY" value="<?php echo $dob;?>">
 <span class="create_step_error"><?php echo form_error('dob'); ?></span>

</div>
<div class="form-group col-md-6">
<label for="">Passport Number or Medicare Number <span class="form_star">*</span></label>
<input type="text" class="form-control" name="passport_number" id="c-name" placeholder=""  value="<?php echo $passport_number;?>">
 <span class="create_step_error"><?php echo form_error('passport_number'); ?></span>
</div>
<div class="form-group col-md-6">
<label for="">Drivers License Number <span class="form_star">*</span></label>
<input type="text" class="form-control" name="license_number" id="c-name" placeholder=""  value="<?php echo $license_number;?>">
 <span class="create_step_error"><?php echo form_error('license_number'); ?></span>
</div>
<div class="form-group col-md-6">
<label for="">Permanent Residency Number: (if applicable) </label>
<input type="text" class="form-control" name="residency_number" id="c-name" placeholder="" value="<?php echo $residency_number;?>">
</div>
<div class="form-group col-md-6">
<label for="phone">Phone Number <span class="form_star">*</span></label>
<input type="text" placeholder="" id="phone" name="mobile_number" class="form-control" value="<?php echo $mobile_number;?>">
<span class="create_step_error"><?php echo form_error('mobile_number'); ?></span>

</div>
<div class="form-group col-md-12 dec-form">
<label for="">Residential Address: <span class="form_star">*</span></label>
<textarea rows="3" id="" name="address" class="text-area"><?php echo $address;?></textarea>
<span class="create_step_error"><?php echo form_error('address'); ?></span>

</div>
</div>

<div class="row" id="Forcitizen" style="display:<?php if($user_select_type == 1) { echo 'block'; } else { echo 'none';}?>">
<div class="form-group col-md-12">
<label for="">Enter FIRB Approval Number: <span class="form_star">*</span></label>
<input type="text" placeholder="" name="FIRB_number" id="l-name" value="<?php echo $FIRB_number;?>" class="form-control ">
<span class="create_step_error"><?php echo form_error('FIRB_number'); ?></span>

</div>

<div class="form-group col-md-12 userver-instrc">
<p>Foreign nationals must apply to the Australian Government, Foreign

Investment Review board for approval prior to any purchase of real estate in 

Australia. A separate application is required for each and every property you 

wish to purchase. Some purchases of residential real estate do not require 

approval, including when the purchaser:<br><br>


• is an Australian or New Zealand citizen<br>

• holds an Australian permanent resident visa<br>

• has a spouse (married or de facto) who is an Australian citizen, a New 

Zealand citizen or an Australian permanent resident visa holder and 

the property is being purchased in both names as joint tenants<br>

• is buying a new dwelling from a developer who has given them a copy 

of the exemption certificate that allows developers to sell properties in 

the development to foreign persons<br>

• inherited the property<br>

• obtained the property through a court order (for example, a divorce 

settlement) under Australian law.<br><br>

For more information please go to:<a href="https://firb.gov.au/applications/apply-now/"> https://firb.gov.au/applications/apply-now/</p>
</a></div>
</div> 

<div class="row">
<div class="form-group col-md-12">
<!-- <a  href="" style="">Skip</a> -->  <div class="btn-group pull-right">
 <input type="hidden" name="accre_id" value="<?php echo $accre_id; ?>">
<button class="btn btn-primary btn-lg btn-block text-uppercase " type="submit">Submit </button>
</div> 
</div>
</form>
</div>


</div>
</div>
</div>
</div>
</section>
<script type="text/javascript" src="<?php echo base_url();?>js/bootstrap-datepicker.js"></script>  

<script type="text/javascript">
$('#chk-box,#chk-box1').on('change', function(){ // on change of state
if(this.checked) // if changed state is "CHECKED"
{
$( "#chk-info" ).show();
}else {
$( "#chk-info" ).hide();
}
})

$('#radio1').on('change', function(){ // on change of state
if(this.checked) // if changed state is "CHECKED"
{
$( "#Auscitizen" ).show();
$( "#Forcitizen" ).hide();
}

})
$('#radio2').on('change', function(){ // on change of state
if(this.checked) // if changed state is "CHECKED"
{
$( "#Auscitizen" ).hide();
$( "#Forcitizen" ).show();
}else {

}
})
var date = new Date();
    var d = new Date();        
    d.setDate(date.getDate());
$('#datepicker').datepicker({  

     endDate: d,   
});

</script>
