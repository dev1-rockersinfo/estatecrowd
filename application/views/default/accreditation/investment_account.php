<div id="loader-wrapper" style="display: none;">

    <div id="loader"></div>

</div>
<script type="text/javascript" src="js/jquery.sticky.js"></script>



<?php echo $this->load->view('default/dashboard_sidebar'); ?>

<section>
    <div class="container padTB50">
        <div class="inner-container">
            <div class="page-header clearfix">
                <h1 class="pull-left"><?php echo INVESTMENT_ACCOUNT; ?></h1>
            </div>
            <?php if ($success_msg) {
                ?>
                <div class="alert-success alert alert-message marB0"><?php echo $success_msg; ?></div>
            <?php } ?>
            <?php if ($error_msg) {
                ?>
                <div class=" alert-danger alert alert-message marB0"><?php echo $error_msg; ?></div>
            <?php } ?>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="inner-container-bg marT20 pr">
                    <div class="project-form-headline">
                        <h3><?php echo PERSONAL_INFORMATION; ?></h3>
                    </div>
                    <div class="table-responsive">
                        <table class="table table_brnone" cellpadding="0" cellspacing="0">
                            <tbody>
                            <tr>
                                <td><strong><?php echo FULL_NAME; ?></strong></td>
                                <td><?php echo $legal_first . ' ' . $legal_last; ?></td>
                            </tr>
                            <tr>
                                <td><strong><?php echo PHONE_NUMBER; ?></strong></td>
                                <td><?php echo CheckValue($phone); ?></td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                    <div class="edit-content">
                        <a href="<?php echo site_url('accreditation/personal'); ?>" data-toggle="tooltip"
                           data-placement="bottom" title="" data-original-title="<?php echo EDIT; ?>"><i
                                class="glyphicon glyphicon-edit"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="inner-container-bg marT20 pr">
                    <div class="project-form-headline">
                        <h3><?php echo ACCREDITATION_STATUS; ?></h3>
                    </div>
                    <div class="table-responsive">
                        <table class="table table_brnone" cellpadding="0" cellspacing="0">
                            <tbody>
                            <tr>
                                <td><strong><?php echo ACCREDITED; ?>?</strong></td>
                                <td><?php if ($accredited == '' || $accredited == '0') {
                                        echo NO;
                                    } elseif ($accredited == '1') {
                                        echo YES;
                                    }
                                    ?></td>
                            </tr>

                            <tr>
                                <td><strong>Status</strong></td>
                                <td>
                                    <?php if ($accreditation_status == '0') { ?>
                                        <span class="badge"><?php echo PENDING; ?></span>

                                    <?php } else if ($accreditation_status == '1') { ?>

                                        <span class="badge badge_green"><?php echo APPROVED; ?></span>

                                    <?php } else if ($accreditation_status == '2') { ?>
                                        <span class="badge badge_red"><?php echo REJECTED; ?></span>
                                    <?php } else { ?>
                                        <span class="badge badge_red"><?php echo 'NOT SUBMMITED'; ?></span>
                                    <?php } ?>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="edit-content">
                        <a href="<?php echo site_url('accreditation/status') ?>" data-toggle="tooltip"
                           data-placement="bottom" title="" onclick="commentaction('comment1_183',183,15)"
                           data-original-title="<?php echo EDIT; ?>"><i class="glyphicon glyphicon-edit"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="inner-container-bg marT20 pr">
                    <div class="project-form-headline">
                        <h3><?php echo EXTERNAL_BANK_ACCOUNTS; ?></h3>
                    </div>
                    <div class="table-responsive">
                        <table class="table" cellpadding="0" cellspacing="0">
                            <thead>
                            <tr>
                                <th><?php echo BANK_NAME; ?></th>
                                <th><?php echo ACCOUNT_NUMBER; ?></th>
                                <th><?php echo ROUTING_NUMBER; ?></th>
                                <th><?php echo ACCOUNT_TYPE; ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>‎<?php echo CheckValue($bank_name); ?></td>
                                <td><?php echo CheckValue($account_no); ?></td>
                                <td><?php echo CheckValue($routing_no); ?></td>
                                <td><?php echo CheckValue($account_type); ?></td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                    <div class="edit-content">
                        <a href="<?php echo site_url('accreditation/personal#bank_details') ?>" data-toggle="tooltip"
                           data-placement="bottom" title="" onclick="commentaction('comment1_183',183,15)"
                           data-original-title="<?php echo EDIT; ?>"><i class="glyphicon glyphicon-edit"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

 


