<section>
    <div class="container padTB50">
        <?php
        $site_setting = site_setting();
        ?>
        <?php
        $slug = '';
        if (is_array($pages)) {

            foreach ($pages as $page) {
                $title = $page['pages_title'];
                $slug = $page['slug'];
                ?>
                <div class="page-header">
                    <h1><?php echo $title; ?></h1>
                </div>
                <?php $description = $page['description'];
                $description = str_replace("{hit_target_flexible}", $site_setting['suc_flexible_fees'], $description);
                $description = str_replace("{hit_nottarget_flexible}", $site_setting['flexible_fees'], $description);
                $description = str_replace("{hit_target_fixed}", $site_setting['fixed_fees'], $description);
                ?>
                <div class="create-campain-bg content_page">
                    <p><?php echo $description; ?></p>
                </div>
            <?php
            }
        }
        ?>

    </div>
</section>
