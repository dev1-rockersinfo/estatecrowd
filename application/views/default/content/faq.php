<!-- <section>
    <div class="container padTB50">
        <div class="page-header">
            <h1><?php echo FAQ; ?></h1>
        </div>
        <div class="row">
        <div class="col-lg-3 col-md-4">
                <?php
                $data = array();
                $this->load->view('default/content/right_menu', $data);
                ?>
            </div>
            <div class="col-lg-9 col-md-8">
                <div class="create-campain-bg content_page">
                    <?php
                    if ($result) {
                        foreach ($result as $faq) {
                            $question = $faq->question;
                            $answer = $faq->answer;
                            ?>
                            <div class="marB30">
                                <h4><strong>Q. <?php echo $question; ?></strong></h4>

                                <p><?php echo $answer; ?></p>
                            </div>
                        <?php
                        }

                    }?>
                </div>
            </div>
            
        </div>
    </div>
</section> -->

 <section>
        <div class="faqs-head">
            <div class="container">
                <div class="page-title">
                    <h2>Frequently Asked Questions</h2>
                </div>
            </div>
        </div>
        <div class="grey-bg" id="section-area">         
            <div class="container">
                <div class="row">
                    <div class="col-md-3 faqs-left">
                        <div class="faqs-sidemenu">

                            <ul>
                                
                                    <?php
                                        $data_group = array();
                                        if (is_array($result)) {
                                            foreach ($result as $key => $val) {

                                                $data_group[$val['question_type']][] = $val;
                                            }
                                            foreach ($data_group as $key => $value) {
                                                $question_type = $key;

                                                $question_type_slug = $value[0]['question_type_slug'];
                                                 ?>

                                                <li>
                                                    <a href="#<?php echo $question_type_slug;?>">
                                                    <?php echo $question_type;?></a>
                                                </li>

                                            <?php
                                            }

                                        } ?>
                               
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-9 faqs-right">
                        <div class="panel-group" role="tablist" aria-multiselectable="true">
                           <?php
                                        $data_group = array();
                                        if (is_array($result)) {
                                            foreach ($result as $key => $val) {

                                                $data_group[$val['question_type']][] = $val;

                                            }
                                             $question_count = 1;
                                            foreach ($data_group as $key => $value) {

                                               
                                                $question_type = $key; 
                                               
                                                $question_type_slug = $value[0]['question_type_slug'];
                                                ?>

                                                
                                           
                        <div class="faqs-panel" id="<?php echo $question_type_slug;?>">
                            <div class="faqs-panel-head">
                                <h3><?php echo $question_type;?></h3>
                            </div>
                            <div class="faqs-panel-body">
                              <div class="panel">
                              <?php 
                              $count=1;
                                foreach ($value as $key => $val) {
                                    

                                    $question = $val['question'];
                                    $answer = $val['answer'];
                                    $active_class = '';
                                    $collapse_class = '';
                                    
                                    if($question_count == 1 && $count == 1)
                                    {
                                        $active_class = 'active';
                                        $collapse_class = 'in';

                                    }
                              ?>
                                <div class="panel-heading <?php echo $active_class;?>" role="tab" id="<?php echo $question_type_slug;?>-Head<?php echo $count;?>">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" href="#<?php echo $question_type_slug;?>-Qus<?php echo $count;?>" aria-expanded="true" aria-controls="<?php echo $question_type_slug;?>-Qus<?php echo $count;?>">
                                      <span class="arrow"><i class="icon-arrow-right"></i></span> 
                                      <?php echo $question; ?>
                                    </a>
                                  </h4>
                                </div>
                                <div id="<?php echo $question_type_slug;?>-Qus<?php echo $count;?>" class="panel-collapse collapse <?php echo  $collapse_class;?>" role="tabpanel" aria-labelledby="<?php echo $question_type_slug;?>-Head<?php echo $count;?>">
                                  <div class="panel-body">
                                    <?php echo $answer; ?>
                                  </div>
                                </div>

                                <?php

                                $count++;
                                 } ?>
                              </div>

                            </div>
                        </div>
                         <?php
                          $question_count++;
                                            }

                                        } ?>

                    </div>
                </div>
            </div>
        </div>
    </section>


    <script type="text/javascript">
        $('.panel-heading a').click(function() {
            if($(this).parents( ".panel-heading" ).hasClass( 'active' )==true) {
                $(this).parents('.panel-heading').removeClass('active');
            } else {
                $(this).parents('.panel-heading').removeClass('active');
                $(this).parents('.panel-heading').addClass('active');               
            }
        });
    </script>
    <script type="text/javascript">
        
        $('.faqs-sidemenu li').click(function(){
          
            var id = $(this).find('a').attr("href"),
              posi,
              ele,
              padding = $('#affix-nav').height();
          
            ele = $(id);
            posi = ($(ele).offset()||0).top - padding;
          
            $('html, body').animate({scrollTop:posi}, 'slow');
          
            return false;
        });
        </script>
        <script src="<?php echo base_url();?>assets_front/scrolltofixed/jquery-scrolltofixed.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('.faqs-sidemenu').scrollToFixed({
                        marginTop: $('#affix-nav').outerHeight(true) + 50,
                        limit: function() {
                            var limit = $('#section-area').position().top+$('#section-area').outerHeight(true)-$('.faqs-sidemenu').outerHeight(true);
                            return limit;
                        },
                        minWidth: 1000,
                        zIndex: 1,
                        fixed: function() {  },
                        dontCheckForPositionFixedSupport: true
                    });     
            });
        </script>
