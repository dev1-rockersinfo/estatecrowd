<?php
$sitename = $site_setting['site_name'];
$language_code = $this->session->userdata('lang_code');
//for german language code should be 'de'. It's temporary fix.
if($language_code=='ge') $language_code='de';

$captcha_src = ($language_code)?"https://www.google.com/recaptcha/api.js?hl=$language_code":"https://www.google.com/recaptcha/api.js";

?>
<script src="<?=$captcha_src?>"></script>
<?php require(base_path().'application/libraries/ReCaptcha/src/autoload.php'); 

$siteKey = $site_setting['captcha_private_key'];
$secret = $site_setting['captcha_public_key'];

$recaptcha = new \ReCaptcha\ReCaptcha($secret);

?>
<script>
    function echeck(str) {
        var at = "@"
        var dot = "."
        var lat = str.indexOf(at)
        var lstr = str.length
        var ldot = str.indexOf(dot)
        if (str.indexOf(at) == -1) {
            return false
        }
        if (str.indexOf(at) == -1 || str.indexOf(at) == 0 || str.indexOf(at) == lstr) {
            return false
        }
        if (str.indexOf(dot) == -1 || str.indexOf(dot) == 0 || str.indexOf(dot) == lstr) {
            return false
        }
        if (str.indexOf(at, (lat + 1)) != -1) {
            return false
        }
        if (str.substring(lat - 1, lat) == dot || str.substring(lat + 1, lat + 2) == dot) {
            return false
        }
        if (str.indexOf(dot, (lat + 2)) == -1) {
            return false
        }
        if (str.indexOf(" ") != -1) {
            return false
        }
        return true
    }
    function myTrim(x) {
        return x.replace(/^\s+|\s+$/gm, '');
    }

</script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/validation.css"/>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/additional-methods.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.maskedinput.js"></script>
<script>
    // validations
    jQuery(function ($) {

        var validator = $("#frm_contact").validate({
            rules: {
                yname: {
                    required: true,
                     minlength: 3,
                },
                yemail: {
                    required: true,
                    email: true
                },
                que: {
                    required: true,
                },
                message: {
                    required: true,
                },
               
                about: {
                    required: true,
                },
            },
            messages: {
                yname: {
                    required: "<?php echo PLEASE_ENTER_NAME; ?>"
                },
                yemail: {
                    required: "<?php echo PLEASE_ENTER_EMAIL; ?>",
                    email: "<?php echo PLEASE_ENTER_VALID_EMAIL_ADDRESS; ?>"
                },
                que: {
                    required: "<?php echo PLEASE_ENTER_QUESTIONS; ?>"
                },
                message: {
                    required: "<?php echo PLEASE_ENTER_QUESTION_DETAIL; ?>",
                },
               
                about: {
                    required: "<?php echo PLEASE_SELECT_QUESTION_TYPE; ?>",
                }
            },
            // the errorPlacement has to take the table layout into account
            errorPlacement: function (error, element) {
                if (element.is(":radio"))
                    error.insertAfter(element.parent());
                else if (element.is(":checkbox"))
                    error.insertAfter(element.parent().next());
                else if (element.is("select"))
                    error.insertAfter(element.next());
                else {
                    error.insertAfter(element);
                }
            },
            // specifying a submitHandler prevents the default submit, good for the demo
            /*submitHandler: function() {
             alert("submitted!");
             },*/
            // set this class to error-labels to indicate valid fields
            success: function (label) {
                // set &nbsp; as text for IE
                label.html("&nbsp;").addClass("valid");
            },
            /*highlight: function(element, errorClass) {
             $(element).parent().next().find("." + errorClass).removeClass("checked");
             },*/
            onkeyup: function (element) {
                $(element).valid();
                var test_class = $(element).next().html();
                if (test_class != '')
                    $(element).next().addClass('error_label');
                else
                    $(element).next().removeClass('error_label');
            },
            onfocusout: function (element) {
                $(element).valid();
                var test_class = $(element).next().html();
                if (test_class != '')
                    $(element).next().addClass('error_label');
                else
                    $(element).next().removeClass('error_label');
            },
        });
    });
</script>

<section>
    <div class="container padTB50">
        <div class="page-header text-center">
            <h1><?php echo SUBMIT_A_REQUEST; ?></h1>
        </div>
        <div class="row">
            <div class="col-lg-8 col-md-10 col-lg-offset-2 col-md-offset-2">
                <div class="create-campain-bg">
                    <div class="in-form create-campain-form">
                        <?php
                        $attributes = array('name' => 'frm_contact', 'id' => 'frm_contact' , 'class'=>'dec-form form-horizontal');
                        echo form_open('content/contact_us', $attributes);
                        ?>


                        <?php if ($error != '') { ?>
                            <div class="alert alert-danger alert-message"><?php echo $error; ?></div>
                        <?php } ?>
                        <?php if ($success != '') { ?>
                            <div class="alert alert-success alert-message"><?php echo $success; ?></div>
                        <?php } ?>
                        <div class="form-group col-md-12">
                          <?php echo WE_ARE_HERE_TO_HELP_TO_GET_START_PLEASE_SELECT_THE_TYPE_OF_ISSUE; ?>

                         
                                <select name="about" id="about" class="selectpicker">
                                    <option value=""><?php echo SELECT_QUESTION_TYPE; ?></option>
                                    <option
                                        value="<?php echo FUNDRAISING_ACCOUNT_LOGIN; ?>" <?php if ($about == FUNDRAISING_ACCOUNT_LOGIN) {
                                        echo 'selected="selected"';
                                    } ?>><?php echo FUNDRAISING_ACCOUNT_LOGIN; ?></option>
                                    <option
                                        value="<?php echo FUNDRAISING_PROJECT; ?>" <?php if ($about == FUNDRAISING_PROJECT) {
                                        echo 'selected="selected"';
                                    } ?>><?php echo FUNDRAISING_PROJECT; ?></option>
                                    <option value="<?php echo INTERNATIONAL; ?>" <?php if ($about == INTERNATIONAL) {
                                        echo 'selected="selected"';
                                    } ?>><?php echo INTERNATIONAL; ?></option>
                                    <option value="<?php echo OTHER; ?>" <?php if ($about == OTHER) {
                                        echo 'selected="selected"';
                                    } ?>><?php echo OTHER; ?></option>
                                </select>
                                
                        </div>

                        <div class="form-group col-md-12">
                          <?php echo SUBJECT; ?> <span class="form_star">*</span>

                           
                                <input type="text" name="que" id="que" value="<?php echo $que; ?>"
                                       class="form-control"/>

                                <p class="contact-para"><?php echo PLEASE_USE_A_FEW_WORD_TO_SUMMARIZE_YOU_QUESION; ?></p>
                          
                        </div>

                        <div class="form-group col-md-12">
                           <?php echo DESCRIPTION; ?>  <span class="form_star">*</span>

                          
                                <textarea name="message" id="message" class="form-control"
                                          rows="4"><?php echo $message; ?></textarea>

                                <p class="contact-para"><?php echo PLEASE_ENTER_ADDITIONAL_DETAIL_OF_YOUR_REQUEST; ?> <?php echo $sitename; ?>
                                    , <?php echo PLEASE_INCLUDE_THE_NAME_OF_THE_CAMPAING_AND_A_LINK; ?></p>
                           
                        </div>

                        <div class="form-group col-md-12">
                            <?php echo YOUR_NAME; ?> <span class="form_star">*</span>

                         
                                <input type="text" name="yname" id="yname" value="<?php echo $yname; ?>"
                                       class="form-control"/>
                             
                           
                        </div>

                        <div class="form-group col-md-12">
                           <?php echo YOUR_EMAIL_ADDRESS; ?> <span class="form_star">*</span>

                            
                                <input type="text" name="yemail" id="yemail" value="<?php echo $yemail; ?>"
                                       class="form-control"/>
                            
                            
                        </div>

                        <div class="form-group col-md-12">
                         <?php echo CONTACT_US_LINK; ?>
                            
                                <input type="text" class="form-control" name="links">
                           
                        </div>

                        <div class="form-group col-md-12">
                        <?php if ($site_setting['contact_us_captcha'] == 1) { ?>
                     
                            <!-- <label class="col-sm-4 control-label" for="captcha"><?php echo WHAT_IS; ?><?php echo $this->session->userdata('captcha1');?> +  <?php echo $this->session->userdata('captcha2');?> = ?</label> -->
                            <label class="captchabg" for="captcha"><?php echo SECURITY_CHECK;?></label>
                            <div class="pr g-recaptcha" data-sitekey="<?php echo $siteKey; ?>"></div>
                            <!-- <div class="col-sm-6"><input type="text" class="form-control" name="captcha" id="captcha" onKeyUp="numericFilter(this);"></div> -->
                      
                        <?php } ?>
                    </div>

                        
                        <div class="text-left form-group col-md-12">
                            <input type="submit" class="btn btn-primary" value="<?php echo SUBMIT; ?>"
                                   onclick="return form_validate();">
                        </div>
                        </form>
                    </div>


                </div>
            </div>

        </div>
    </div>
</section>

