<style type="text/css">
    body {
        padding-top: 0;
    }

    .navbar-fixed-top, .navbar-fixed-bottom {
        position: relative;
        margin-bottom: 0;
    }
</style>
<section>
    <div class="help-header">
        <div class="container">
            <h1><?php echo HELP_CENTER; ?></h1>
        </div>
    </div>
</section>
<section>
    <div class="container in-breadcrumb">
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url('content/learn_more'); ?>"><?php echo HELP_CENTER; ?></a></li>
            <li class="active"><?php echo $learn_category_name; ?></li>
        </ol>
    </div>
    <div class="whiteBG">
        <div class="container padTB50">
            <div class="page-header">
                <h1><?php echo $learn_category_name; ?></h1>
            </div>
            <div class="learn-pages">
                <ul>
                    <?php
                    if ($pages) {
                        foreach ($pages as $pag) {

                            $pages_title = $pag['pages_title'];
                            $pages_id = $pag['pages_id'];
                            ?>
                            <li><?php echo anchor('content/learn_more_page/' . $pages_id, $pages_title); ?></a></li>
                        <?php
                        }
                    } else {
                        echo NO_LEARN_MORE_CATEGORY_AVAILABLE;
                    } ?>
                </ul>
            </div>
        </div>
    </div>
</section>
