
<?php 
 $other_page ='yes';
if($dynamic_image_image != '' && is_file(base_path().'upload/content/'.$dynamic_image_image))
{

    $other_page ='';
?>

 <header class="navbar-transparent-inner">
  <?php echo $this->load->view('default/common/header'); ?>
        <div class="hiw-header">
            <div class="image-wrapper">
                <div class="image" style="background-image:url('<?php echo base_url();?>upload/content/<?php echo $dynamic_image_image;?>')"></div>
                <div class="overlay"></div>
            </div>
            <div class="content-area">
                <div class="main-text">
                    <h2><?php echo $header_title;?></h2>
                    <p><?php echo $header_content;?></p>
                    <a href="<?php echo $link;?>" class="btn btn-primary text-uppercase"><?php echo $link_name;?></a>
                </div>
            </div>
        </div>
    </header>

<?php } ?>
                    <?php
                    $site_setting = site_setting();

                    ?>

                    <?php
                    if($other_page == 'yes')
                    { ?>
                         <section>
                                <div class="section-mar">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-10 col-md-offset-1">


                    <?php } 

                    $slug = '';
                    if (is_array($pages)) {
                        foreach ($pages as $page) {
                            $title = $page['pages_title'];
                            $slug = $page['slug'];
                            ?>
                            <?php 
                            $description = $page['description'];
                            $description = str_replace("{hit_target_flexible}", $site_setting['suc_flexible_fees'], $description);
                            $description = str_replace("{hit_nottarget_flexible}", $site_setting['flexible_fees'], $description);
                            $description = str_replace("{hit_target_fixed}", $site_setting['fixed_fees'], $description);


                            ?>

                            <?php echo $description; ?>

                        <?php
                        }
                    } ?>
                    <?php
                    if($other_page == 'yes')
                    { ?>
              
        </div>
                </div>
            </div>
        </div>
    </section>

    <?php } ?>