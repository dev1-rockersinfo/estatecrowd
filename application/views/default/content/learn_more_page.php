<?php
$pages_title = $learn_pages[0]['pages_title'];
$description = $learn_pages[0]['description'];
$sub_title = $learn_pages[0]['sub_title'];
$icon_image = $learn_pages[0]['icon_image'];
$learn_category = $learn_pages[0]['learn_category'];


if ($icon_image != '' && is_file('upload/icon/' . $icon_image)) {
    $imgsrc1 = base_url() . 'upload/icon/' . $icon_image;
} else {

    $imgsrc1 = base_url() . 'images/logo.png';
}

?>
<style type="text/css">
    body {
        padding-top: 0;
    }

    .navbar-fixed-top, .navbar-fixed-bottom {
        position: relative;
        margin-bottom: 0;
    }
</style>
<section>
    <div class="help-header">
        <div class="container">
            <h1><?php echo HELP_CENTER; ?></h1>
        </div>
    </div>
</section>
<section>
    <div class="container in-breadcrumb">
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url('content/learn_more'); ?>"><?php echo HELP_CENTER; ?></a></li>
            <li>
                <a href="<?php echo site_url('content/learn_more_category/' . $learn_category) ?>"><?php echo get_title_learn_more($learn_category); ?></a>
            </li>
            <li class="active"><?php echo $pages_title; ?></li>
        </ol>
    </div>
    <div class="whiteBG">
        <div class="container padTB50">
            <div class="page-header">
                <h1><?php echo $pages_title; ?></h1>
            </div>
            <div class="row content_page">
                <div class="col-lg-9 col-md-8">
                    <div><?php echo $description; ?></div>
                </div>
                <div class="col-lg-3 col-md-4 hidden-sm hidden-xs">
                    <h3 class="marB20 marT0"><?php echo RELATED_ARTICLES; ?></h3>

                    <div class="learn-pages">
                        <ul>
                            <?php

                            if ($sub_pages) {

                                foreach ($sub_pages as $learn_pages) {

                                    ?>
                                    <li>
                                        <a href="<?php echo site_url('content/learn_more_page/' . $learn_pages['pages_id']) ?>"><?php echo $learn_pages['pages_title']; ?></a>
                                    </li>

                                <?php
                                }
                            } else {
                                echo NO_LEARN_MORE_CATEGORY_AVAILABLE;
                            }
                            ?>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
