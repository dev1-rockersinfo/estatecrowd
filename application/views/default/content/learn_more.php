<style type="text/css">
    body {
        padding-top: 0;
    }

    .navbar-fixed-top, .navbar-fixed-bottom {
        position: relative;
        margin-bottom: 0;
    }
</style>
<section>
    <div class="help-header">
        <div class="container">
            <h1><?php echo HELP_CENTER; ?></h1>
        </div>
    </div>
</section>
<section>
    <div class="help-features">
        <div class="container padTB50">
            <div class="row">
                <div class="col-md-4">
                    <a href="<?php echo site_url('content/learn_more'); ?>" class="features-item">
                        <i class="fa fa-lightbulb-o"></i>

                        <h2><?php echo KNOWLEDGE_BASE; ?></h2>

                        <p><?php echo $pages_count; ?> <?php echo ARTICLES; ?>
                            /  <?php echo get_learn_category_name('yes'); ?> <?php echo CATEGORIES; ?></p>
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="<?php echo site_url('content/faq/'); ?>" class="features-item">
                        <i class="fa fa-question"></i>

                        <h2><?php echo FAQ; ?></h2>

                        <p><?php echo $faq_count; ?> <?php echo QUESTION; ?></p>
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="<?php echo site_url('content/contact-us'); ?>" class="features-item">
                        <i class="fa fa-envelope-o"></i>

                        <h2><?php echo CONTACT_US; ?></h2>

                        <p><?php echo RESPONSE_WITHIN_24_HOURS; ?></p>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="whiteBG">
        <div class="container padTB50">
            <div class="help-contant">
                <ul>
                    <?php
                    if ($category_details) {
                        foreach ($category_details as $catdetail) {
                            $learn_category_id = $catdetail->category_id;
                            $learn_category_name = $catdetail->category_name;
                            ?>
                            <li class="col-md-4 col-sm-6"><?php echo anchor('content/learn_more_category/' . $learn_category_id, $learn_category_name); ?>
                                <ul>
                                    <?php
                                    $pages = $this->content_model->learnmore(1, $learn_category_id, array('pages_id' => 'desc'));

                                    if ($pages)
                                    {
                                    foreach ($pages as $pag)
                                    {

                                    $pages_id = $pag['pages_id'];
                                    $pages_title = $pag['pages_title'];
                                    $slug = $pag['slug'];
                                    ?>
                                    <li><?php echo anchor('content/learn_more_page/' . $pages_id, $pages_title); ?></a>
                                        <?php }
                                        } ?>
                                </ul>
                            </li>
                        <?php
                        }
                    }
                    ?>
                </ul>
            </div>
        </div>
    </div>
</section>
