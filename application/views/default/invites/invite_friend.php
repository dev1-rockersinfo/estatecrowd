<script type="text/javascript">

    function send_error() {

        var email_reg_exp = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;

//var recipient_message=trim(removeHTMLTags($("#recipient_message").val()));

        var recipient_message = $("#recipient_message").val();

        $('.invite_email_fild').each(function () {


            var recipient_email = $(this).val();

            var recipient_id = $(this).attr('id');

            var siblin = $("#sp" + recipient_id);


            if (recipient_email != '') {

                if (!email_reg_exp.test(recipient_email)) {

                    siblin.show();

                    siblin.text('<?php echo "Recipient mail is invalid";?>');

                    siblin.removeClass('success1');

                    siblin.addClass('error1');

                    return false;

                }

                else {

                    siblin.hide();

                    var res = $.ajax({

                        type: 'POST',

                        url: '<?php echo site_url('invites/sendemailinvitation');?>',

                        data: {remail: recipient_email, rmessage: recipient_message},

                        dataType: 'json',

                        cache: false,

                        async: false

                    }).responseText;


                    var inv_rpl = jQuery.parseJSON(res);


                    if (inv_rpl.status == 'success') {


                        $(this).val('');
                        $('#recipient_message').val('');

                        siblin.show();

                        siblin.text('<?php echo INVITE_SENT;?>');// change by darshan

                        siblin.removeClass('error1');

                        siblin.addClass('success1');

                        return true;

                    }

                    else {

                        siblin.show();

                        siblin.text('<?php echo INVITATION_IS_FAILED;?>'); // change by darshan


                        siblin.removeClass('success1');

                        siblin.addClass('error1');

                        return false;

                    }

                }

            }

            else {

                siblin.hide();

            }


        });


    }


    function invitebox(name, email) {



        //alert(res_email+"Hello"+res_name);

        var res_email = email;

        var res_name = name;

        $.fancybox({

            'width': '80%',

            'height': '100%',

            'autoScale': true,

            'transitionIn': 'elastic',

            'transitionOut': 'elastic',

            'type': 'iframe',

            'scrolling': 'none',

            'href': '<?php echo site_url('invites/send_invite');?>/' + res_email + '/' + res_name

        });


    }

</script>


<style type="text/css">

    span.error1 {

        color: red !important;

        font-size: 12px !important;

        font-weight: bold !important;

        margin-left: 20%;

        display: none;

    }

    span.success1 {

        color: #009900 !important;

        font-size: 12px !important;

        font-weight: bold !important;

        margin-left: 20%;

        display: none;

    }

</style>

<section>

<div class="content_part_top">

<div class="main">
<div class="dash_whitebg clearfix">

<!--Left part-->

<?php echo $this->load->view('default/dashboard_sidebar'); ?>


<!--Left Part End-->


<!--right Part Start-->

<div class="dash_right">
<div class="dash_content">

<div class="responsive-tabs">

<!--Project 1 start-->

<h2><?php echo FACEBOOK; ?></h2><!-- change by darshan -->


<div>

    <p><?php echo WE_COULDN_T_FIND_ANY_OF_YOUR_FRIENDS_FROM_FACEBOOK_BECAUSE_YOU_HAVEN_T_CONNECTED_FACEBOOK_AND_REKAUDO_CLICK_THE_BUTTON_BELOW_TO_CONNECT; ?></p>
    <!-- change by darshan -->

    <div class="Margin_TB15 invites_facebook"><img src="<?php echo base_url() . "images/invite-facebook.jpg"; ?>"/>
    </div>


    <div class="clear"></div>


</div>


<h2><?php echo EMAIL; ?></h2><!-- change by darshan -->

<div class="noti_div">

    <h4 class="fontred"><?php echo INVITE_YOUR_FRIENDS_TO . $site_setting['site_name']; ?> </h4>
    <!-- change by darshan -->


    <div class="invite_email">

        <label class="invite_email_label fonts14"><?php echo EMAIL_ADDRESS; ?>1 :</label><!-- change by darshan -->

        <input type="text" name="email1" id="email1" class="invite_email_fild" value="">

        <span id="spemail1" class="success1"></span>

        <div class="clear"></div>

    </div>


    <div class="invite_email">

        <label class="invite_email_label fonts14"><?php echo EMAIL_ADDRESS; ?>2 :</label><!-- change by darshan -->

        <input type="text" name="email2" id="email2" class="invite_email_fild" value="">

        <span id="spemail2" class="success1"></span>

        <div class="clear"></div>

    </div>


    <div class="invite_email">

        <label class="invite_email_label fonts14"><?php echo EMAIL_ADDRESS; ?>3 :</label><!-- change by darshan -->

        <input type="text" name="email3" id="email3" class="invite_email_fild" value="">

        <span id="spemail3" class="success1"></span>

        <div class="clear"></div>

    </div>


    <div class="invite_email">

        <label class="invite_email_label fonts14"><?php echo EMAIL_ADDRESS; ?>4 :</label><!-- change by darshan -->

        <input type="text" name="email4" id="email4" class="invite_email_fild" value="">

        <span id="spemail4" class="success1"></span>

        <div class="clear"></div>

    </div>


    <div class="invite_email">

        <label class="invite_email_label fonts14"><?php echo MESSAGE; ?> :</label><!-- change by darshan -->

        <textarea name="recipient_message" id="recipient_message" cols="" rows=""
                  class="invite_email_textarea"></textarea>

    </div>


    <div class="fr Margin_TB15">

        <input type="button" name="sendinvite" class=" save_Default" value="Send Invites"
               onclick="return send_error();">

    </div>

    <div class="clear"></div>


    <div class="clear"></div>

</div>


<h2>Gmail</h2>

<div>

<?php if (!$friend_list) { ?>

    <p><?php $var = sprintf(WE_COULDNT_FIND_ANY_OF_YOUR_GMAIL_CONTACTS_BECAUSE_YOU_HAVENT_CONNECTED_GMAIL_AND_REKAUDO_CLICK_THE_BUTTON_TO_TEMPORARILY_CONNECT_GMAIL_AND_REKAUDO, $site_setting['site_name'], $site_setting['site_name']);
        echo $var; ?></p><!-- change by darshan -->

    <p>

        <?php echo WE_WILL_NEVER_SEND_OUT_ANY_EMAIL_MESSAGES_WITHOUT_ASKING; ?><!-- change by darshan -->

    </p>

    <div class="find_gmail"><a href="<?php echo $gmail_connect; ?>"><img
                src="<?php echo base_url() . "images/friend-gmail.jpg"; ?>"/></a></div>



<?php } else { ?>

    <script type="text/javascript">

        $(function () {


            $.expr[":"].containsInCaseSensitive = function (el, i, m) {


                var search = m[3];

                alert(search);

                if (!search) return false;

                /*return eval("/" + search + "/i").test($(el).text());*/

                alert(eval("/" + search + "/i").test($(el).text()));

            };


            $('#query').focus().keyup(function (e) {

                if (this.value.length > 0) {

                    $('ul#abbreviations li').hide();

                    $('ul#abbreviations li:containsInCaseSensitive(' + this.value + ')').show();

                } else {

                    $('ul#abbreviations li').show();

                }

                if (e.keyCode == 13) {

                    $(this).val('');

                    $('ul#abbreviations li').show();

                }

            });


        });

    </script>



    <div class="dis-connect">

        <a href="<?php echo site_url('invites/index'); ?>"><img
                src="<?php echo base_url() . "images/disconnect-gmail.jpg"; ?>"/></a>

    </div>



    <div class="gmail-after-login">

        <h4 class="fontred Margin_TB15"><?php echo INVITE_FRIENDS; ?></h4><!-- change by darshan -->

        <div class="invite-textbox">

            <input type="text" class="inputDefault_S invite-textfilled" name="">

        </div>


        <ul id="#abbreviations">

            <?php if ($friend_list) {
                $frcnt = 1;

                foreach ($friend_list as $fr) {


                    $expfr = explode('||', $fr);


                    $fremail = '';
                    $frname = '';


                    if (isset($expfr[0])) {
                        $fremail = $expfr[0];
                    }

                    if (isset($expfr[1])) {
                        $frname = $expfr[1];
                    }
                    if ($frname == '') {
                        $frname = $expfr[0];
                    }


                    if ($fremail != '') {

                        ?>

                        <li>

                            <div class="gmain-Flist_main box-B-background">

                                <div class="Gmail-Flist-left">

                                    <img src="<?php echo base_url(); ?>upload/no_man.gif" alt="noimage"
                                         class="img border-Wbg">

                                </div>

                                <div class="gmail-email">

                                    <h2><span id="frname<?php echo $frcnt; ?>"><?php echo $frname; ?></span></h2>

                                    <p><span id="fremail<?php echo $frcnt; ?>"><?php echo $fremail; ?></span></p>

                                </div>

                                <div class="clear"></div>

                                <div class="gmail-invite-button">

                                    <a href="javascript:void(0);" class="grey-btn fonts14 padTB2 "
                                       onclick="invitebox('<?php echo $frname; ?>','<?php echo $fremail; ?>')"><?php echo INVITE; ?></a>

                                </div>

                            </div>

                        </li>



                        <?php $frcnt++;
                    }
                }
            } ?>

        </ul>

    </div>



    <?php $shfol = 0;
    $shunfol = 0;
    if ($unfollow_list) {
        $shunfol = 1;
    }
    if ($follow_list) {
        $shfol = 1;
    }

    if ($shfol == 1 || $shunfol == 1) {
        ?>

        <script type="text/javascript">

            $(document).ready(function () {

                $('#showfrmdlist').click(function () {

                    jQuery('#toglefont').hide();

                    jQuery('#showfndlist').show();

                });

            });

        </script>

        <div class="tubestart-friend">

            <h4 class="fontred Margin_TB15"><?php echo FRIENDS_ON; ?> <?php echo ucfirst($site_setting['site_name']); ?></h4>
            <!-- change by darshan -->


            <ul class="invitesul">

                <?php if ($unfollow_list) {

                    $cnt = 0;

                    foreach ($unfollow_list as $flw) {


                        $user_detail = UserData($flw);


                        if ($user_detail) {


                            $friend_user_image = $user_detail[$cnt]['image'];


                            $friend_user_profileimage = base_url() . 'upload/no_man.gif';


                            if ($friend_user_image != '') {


                                if (file_exists(base_path() . 'upload/thumb/' . $friend_user_image)) {

                                    $friend_user_profileimage = base_url() . 'upload/thumb/' . $friend_user_image;

                                } else {


                                    if (file_exists(base_path() . 'upload/orig/' . $friend_user_image)) {

                                        $friend_user_profileimage = base_url() . 'upload/orig/' . $friend_user_image;

                                    }

                                }

                            }





                            ?>



                            <li>

                                <div class="tubestart-Flist box-B-background">

                                    <div class="tube-P-photo">

                                        <a href="<?php echo site_url('member/' . $user_detail[$cnt]['user_id']); ?>"><img
                                                src="<?php echo $friend_user_profileimage; ?>"/></a>

                                    </div>

                                    <div class="tube-Fname"><a
                                            href="<?php echo site_url('member/' . $user_detail[$cnt]['user_id']); ?>"><?php echo $user_detail[$cnt]['user_name'] . ' ' . $user_detail[$cnt]['last_name']; ?></a>
                                    </div>

                                </div>

                            </li>

                        <?php
                        }
                    }

                    $cnt++;

                } ?>

            </ul>


        </div>

    <?php } ?>



<?php } ?>



<div class="clear"></div>

</div>


<h2><?php echo YAHOO_MAIL; ?></h2><!-- change by darshan -->

<div>

    <p><?php echo WE_COULDNT_FIND_ANY_OF_YOUR_GMAIL_CONTACTS_BECAUSE_YOU_HAVENT_CONNECTED_GMAIL_AND_REKAUDO_CLICK_THE_BUTTON_TO_TEMPORARILY_CONNECT_GMAIL_AND_REKAUDO; ?></p>
    <!-- change by darshan -->


    <p>

        <?php echo WE_WILL_NEVER_SEND_OUT_ANY_EMAIL_MESSAGES_WITHOUT_ASKING; ?> <!-- change by darshan -->

    </p>

    <div class="find_gmail"><a href="<?php echo $yahoo_connect; ?>"><img
                src="<?php echo base_url() . "images/friend-yahoo.jpg"; ?>"/></a></div>


    <div class="clear"></div>

</div>


</div>

</div>
</div>

<!--right Part End-->

<div class="clear"></div>
</div>
</div>

</div>

</div>

</section>
