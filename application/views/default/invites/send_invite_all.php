<script language="javascript">
    function move(tbFrom, tbTo) {
        var arrFrom = new Array();
        var arrTo = new Array();
        var arrLU = new Array();
        var i;
        for (i = 0; i < tbTo.options.length; i++) {
            arrLU[tbTo.options[i].text] = tbTo.options[i].value;
            arrTo[i] = tbTo.options[i].text;
        }
        var fLength = 0;
        var tLength = arrTo.length;
        for (i = 0; i < tbFrom.options.length; i++) {
            arrLU[tbFrom.options[i].text] = tbFrom.options[i].value;
            if (tbFrom.options[i].selected && tbFrom.options[i].value != "") {
                arrTo[tLength] = tbFrom.options[i].text;
                tLength++;
            }
            else {
                arrFrom[fLength] = tbFrom.options[i].text;
                fLength++;
            }
        }

        tbFrom.length = 0;
        tbTo.length = 0;
        var ii;

        for (ii = 0; ii < arrFrom.length; ii++) {
            var no = new Option();
            no.value = arrLU[arrFrom[ii]];
            no.text = arrFrom[ii];
            tbFrom[ii] = no;
        }

        for (ii = 0; ii < arrTo.length; ii++) {
            var no = new Option();
            no.value = arrLU[arrTo[ii]];
            no.text = arrTo[ii];
            tbTo[ii] = no;
        }
    }

    function inviteemail() {
        var recipient_name = $("#recepition_name").val();
        var recipient_email = $("#recepition_email").val();
        /*var recipient_message=trim(removeHTMLTags($("#recipient_message").val()));*/
        var recipient_message = $("#recipient_message").val();
        if (recipient_email == '') {
            return false;
        }
        else {

            var res = $.ajax({
                type: 'POST',
                url: '<?php echo site_url('invites/sendinvitation_all');?>',
                data: {rname: recipient_name, remail: recipient_email, rmessage: recipient_message},
                dataType: 'json',
                cache: false,
                async: false
            }).responseText;


            var sendrpl = jQuery.parseJSON(res);

            if (sendrpl.status == 'success') 
            {

               
                url = "<?php echo site_url('invites/gmail');?>";
                $( location ).attr("href", url);

                //setTimeout(closeme,3000);
            }
            else {
                $("#sendmsg").show();
             
                if (sendrpl.status == 'pending') {
                    $("#sendmsg").text('<?php echo YOUR_AFFILIATE_REQUEST_IS_IN_PENDING;?>'); //
                    <!-- change by darshan -->
                }
                else {
                    $("#sendmsg").text('<?php echo SORRY_AN_ERROR_OCCURES_PLEASE_TRY_AGAIN;?>'); //
                   
                }
                return false;

            }
        }

    }
</script>

<script type="text/javascript">

    function invitebox(name, email) {

        //alert(res_email+"Hello"+res_name);

        var res_email = email;

        var res_name = name;

    }

    function invitebox_all() {

        var res_email = $('#multiple_email').val();
        var res_name = $('#multiple_name').val();

        $.ajax({
            type: "POST",
            cache: false,
            url: '<?php echo site_url('invites/send_invite_all');?>',
            data: $("#selected").serializeArray(),
            success: function (data) {
                $.fancybox(data, {
                    'width': '80%',
                    'height': '100%',
                    'autoScale': true,
                    'transitionIn': 'elastic',
                    'transitionOut': 'elastic',
                    'type': 'iframe',
                    'scrolling': 'none',
                });
            }
        });


    }
    function submit_form() {
        alert('asdf');
        return false;
        document.getElementById("selected").submit();
    }

</script>




  <section>
        <div class="db-head">
            <div class="container">
                <div class="page-title">
                    <h2>Invite</h2>
                </div>
            </div>
        </div>
        <div class="grey-bg" id="section-area">         
            <div class="container">
                <div class="row">
                    <div class="col-md-3 db-mc-left">
                  <?php echo $this->load->view('default/dashboard_sidebar') ?>
                        
                    </div>
                    <div class="col-md-9 db-mc-right">
                    <div class="alert-danger alert alert-message" id="sendmsg" style="display: none;">
                 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                </div>
                    <div class="row">
                    <div class="col-md-8 invite-mail">
                    
                    <form name="" class="ec-form">
                    <div class="form-group col-md-12">
                        <label class="col-sm-12 control-label"><?php echo 'To'; ?></label>
                          <input type="hidden" name="recepition_email" id="recepition_email"
                                   value="<?php echo $email; ?>"> 
                            <input type="hidden" name="recepition_name" id="recepition_name"
                                   value="<?php echo $name; ?>"> 
                            <?php
                            $_email = explode(", ", $email);
                            $_name = explode(", ", $name);
                           
                            foreach ($_email as $key => $value) {
                                ?>
                                <div class="useraE">
                                    <?php echo $_name[$key]; ?>
                                   
                                </div>
                            <?php
                            }

                            ?>
                            <?php ?>

                    </div>
                    <div class="form-group col-md-12">
                        <label class="col-sm-12 control-label"><?php echo MESSAGE; ?></label> <!-- change by darshan -->
                       
                            <textarea name="recipient_message" id="recipient_message" cols="" rows="6"
                                      class="form-control"></textarea>
                       
                    </div>
                     <div class="form-group col-md-12">                     
                        

                          <button class="btn btn-primary" type="button"  onclick="inviteemail();"> <?php echo SEND_ALL; ?></button>
                        
                    </div>
                </form>
        
    
                    </div>
                    <div class="col-md-4 invite-frd">
                    <h3>Invite from social media</h3>
                    
                    <div class="sc-buttons">
                    <div class="sc-fb">
                      <div class="social-text"><i class="fa fa-facebook fa-fw"></i> Invite From Facebook</div>
                    </div>
                    <div class="sc-tw">
                      <div class="social-text"><i class="fa fa-twitter fa-fw"></i> Invite From Twitter</div>
                    </div>
                    <div class="sc-in">
                      <div class="social-text"><i class="fa fa-linkedin fa-fw"></i> Invite From Linkedin</div>
                      </div>
                      <?php if ($google_enable == 1) { ?>

                      <a href="<?php echo site_url("invites/gmail"); ?>">
                      <div class="sc-gp">
                      <div class="social-text"><i class="fa fa-google-plus fa-fw"></i> Invite From Google Plus</div>
                     </div>
                     </a>

                <?php } ?>
                    </div>
                    </div>
     
                        
                    </div>
            </div>
        </div>
    </section>

