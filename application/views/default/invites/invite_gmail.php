

<script language="javascript">
    function move(tbFrom, tbTo) {
        var arrFrom = new Array();
        var arrTo = new Array();
        var arrLU = new Array();
        var i;
        for (i = 0; i < tbTo.options.length; i++) {
            arrLU[tbTo.options[i].text] = tbTo.options[i].value;
            arrTo[i] = tbTo.options[i].text;
        }
        var fLength = 0;
        var tLength = arrTo.length;
        for (i = 0; i < tbFrom.options.length; i++) {
            arrLU[tbFrom.options[i].text] = tbFrom.options[i].value;
            if (tbFrom.options[i].selected && tbFrom.options[i].value != "") {
                arrTo[tLength] = tbFrom.options[i].text;
                tLength++;
            }
            else {
                arrFrom[fLength] = tbFrom.options[i].text;
                fLength++;
            }
        }

        tbFrom.length = 0;
        tbTo.length = 0;
        var ii;

        for (ii = 0; ii < arrFrom.length; ii++) {
            var no = new Option();
            no.value = arrLU[arrFrom[ii]];
            no.text = arrFrom[ii];
            tbFrom[ii] = no;
        }

        for (ii = 0; ii < arrTo.length; ii++) {
            var no = new Option();
            no.value = arrLU[arrTo[ii]];
            no.text = arrTo[ii];
            tbTo[ii] = no;
        }
    }
</script>

<script type="text/javascript">

    function invitebox(name, email) {

        //alert(res_email+"Hello"+res_name);

        var res_email = email;

        var res_name = name;


    }

    function invitebox_all() {

        var res_email = $('#multiple_email').val();
        var res_name = $('#multiple_name').val();

        $.ajax({
            type: "POST",
            cache: false,
            url: '<?php echo site_url('invites/send_invite_all');?>',
            data: $("#selected").serializeArray(),
            success: function (data) {
                $.fancybox(data, {
                    'width': '80%',
                    'height': '100%',
                    'autoScale': true,
                    'transitionIn': 'elastic',
                    'transitionOut': 'elastic',
                    'type': 'iframe',
                    'scrolling': 'none',
                });
            }
        });


    }
    function submit_form() {
        alert('asdf');
        return false;
        document.getElementById("selected").submit();
    }

</script>

<?php

$login_user_profileimage = base_url() . 'upload/no_man.gif';
if ($user_info[0]['image'] != '') {
    if (file_exists(base_path() . 'upload/thumb/' . $user_info[0]['image'])) {
        $login_user_profileimage = base_url() . 'upload/thumb/' . $user_info[0]['image'];
    } else {
        if (file_exists(base_path() . 'upload/orig/' . $user_info[0]['image'])) {
            $login_user_profileimage = base_url() . 'upload/orig/' . $user_info[0]['image'];
        }
    }
}
$facebook_setting = facebook_setting();
$data = array(
    'facebook' => $this->fb_connect->fb,
    'fbSession' => $this->fb_connect->fbSession,
    'user' => $this->fb_connect->user,
    'uid' => $this->fb_connect->user_id,
    'fbLogoutURL' => $this->fb_connect->fbLogoutURL,
    'fbLoginURL' => $this->fb_connect->fbLoginURL,
    'base_url' => site_url('home/facebook'),
    'appkey' => $this->fb_connect->appkey,
);
$response = file_get_contents('https://graph.facebook.com/' . $data['appkey']);
$response = json_decode($response);
$logoutUrl = $data['fbLogoutURL'];
$loginUrl = $data['fbLoginURL'];

?>
<div id="fb-root"></div>
<!-- <script src="http://connect.facebook.net/en_US/all.js"></script>-->
<script>

    window.fbAsyncInit = function () {
        FB.init({
            appId: '<?php echo $data['appkey']; ?>', // App ID
            status: true, // check login status
            cookie: true, // enable cookies to allow the server to access the session
            xfbml: true, // parse XFBML
            oauth: true
        });
        /*FB.login({scope: 'email'},function(response){
         if(response.status == 'connected'){
         alert('I am connected');
         }
         });*/
        FB.getLoginStatus(function (response) {
            //alert(JSON.stringify(response));
            if (response.status === 'connected') {
                var uid = response.authResponse.userID;
                var accessToken = response.authResponse.accessToken;
                $('#fb-invite-friends').show();
                $('#fb-invite-login').hide();
                init();
            } else {
                $('#fb-invite-friends').hide();
                $('#fb-invite-login').show();
                $('#fb-invite-login a.loginButton').click(function (e) {
                    e.preventDefault();
                    login();
                });
            }
        });
    };
    // Load the SDK Asynchronously

    (function (d) {
        var js, id = 'facebook-jssdk';
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement('script');
        js.id = id;
        js.async = true;
        js.src = "//connect.facebook.net/en_US/all.js";
        d.getElementsByTagName('head')[0].appendChild(js);
    }(document));
    function login() {
        FB.login(function (response) { //alert(JSON.stringify(response));
            if ($('#jfmfs-container').length <= 0) {
               
                // location.href = '<?php echo site_url('invites/email/'); ?>';
                 $("#email_form").hide();
                $("#facebook_form").show();
            }
            if (response.status) {
                init();
            } else {
                //alert('Login Failed!');
            }
        });
    }
    function send_message() {
        var redirect_url = document.URL;
        var myWindow = window.open("http://www.facebook.com/dialog/send?app_id=665394413522000&link=https://www.groupfund.me/home/signup/<?php echo $user_info[0]['unique_code'] ?>&redirect_uri=http://www.groupfund.me/invites/close", "", "top=100, left=400,width=800,height=500,scrollable=yes");
    }
    function init() {
        if ($("#jfmfs-container")) {
            FB.api('/me', function (response) {
                $("#logged-out-status").hide();
                $("#jfmfs-container").jfmfs();
                $("#invite-friends").css('display', 'inline-block');
            });
        }
    }
    $("#invite-friends").live("click", function () {
        var friendSelector = $("#jfmfs-container").data('jfmfs');
        var friendIdsAndNames = friendSelector.getSelectedIdsAndNames();
        if (friendIdsAndNames.length > 0) {
            $("#jfmfs-container").fadeTo('slow', .5);
            $("#invite-friends").fadeTo('slow', .5)
            $("#invite-friends").click(function () {
                return false;
            })

            var msg = document.getElementById('message').value;
            jQuery.ajax({
                type: 'POST',
                url: '<?php echo site_url('invites/sendinvite/'); ?>/' + msg,
                data: {facebook_invites: friendIdsAndNames},
                success: function (data) {
                    var response = FB.ui({
                        method: 'send',
                        name: "Join me on <?php echo ucfirst($site_setting['site_name']);?>!",
                        link: '<?php echo site_url('invited/'.$user_info[0]['unique_code']);?>/facebook',
                        picture: '<?php echo $login_user_profileimage; ?>',
                        description: "<?php echo ucfirst($user_info[0]['user_name']).' '.ucfirst($user_info[0]['last_name']);?> is on <?php echo ucfirst($site_setting['site_name']);?>, a place to discover, donate and post campaign. Join <?php echo ucfirst($site_setting['site_name']);?> to see <?php echo ucfirst($user_info[0]['user_name']);?>&acute;s campaigns.",
                        to: data,
                        //to: '['+data+']',
                        //to:['100001967248727','100001831222552'],
                        max_recipients: 50,
                        display: 'popup'
                    }, function (response) {//alert(JSON.stringify(response));
                        if (!response) {
                            parent.window.location.href = '<?php echo site_url('invites/email');?>';
                            return;
                        } else {
                            parent.window.location.href = '<?php echo site_url('invites/index/success');?>';
                            var request_id = response.request + "_" + response.to[0];
                        }
                    });
                    console.log(response.request_ids);
                    window.close();
                }
            });
        }
    });
 

</script>

<script type="text/javascript">
 function send_error() {

        $('#show_msg').html('');//change by darshan
        $('.get_invite_email_fild').remove();//change by darshan
        $('.invite-error').remove();//change by darshan
        var inv_email = $("#tags_1").val();
        var email_array = inv_email.split(',');

        var email_reg_exp = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;

//var recipient_message=trim(removeHTMLTags($("#recipient_message").val()));

        var recipient_message = $("#recipient_message").val();      //change by darshan  start
        if (inv_email == '') {
            alert('Email address is required');
            return false;
        }
        if (recipient_message == '') {
            alert('<?php echo RECIPIENT_MESSAGE_IS_REQUIRED ?>');
            return false;
        }
        //change by darshan  end
        for (var i = 0; i < email_array.length; i++) {

            $("#myemail").append('<input type="hidden" name="email' + i + '" id="email' + i + '" value="' + email_array[i] + '" class="get_invite_email_fild" />');
            $("#show_msg").append('<span id="spemail' + i + '" class="invite-success"></span><br>');
        }

        $('.get_invite_email_fild').each(function () {
            PopupOpen();
            var reci_email = $(this).val();
            var recipient_email = $.trim(reci_email);
            var recipient_id = $(this).attr('id');
            var siblin = $("#sp" + recipient_id);

            if (!email_reg_exp.test(recipient_email)) {
                siblin.show();

                siblin.text('Recipient email is invalid');
                siblin.removeClass('invite-success');
                siblin.addClass('invite-error');
                //return false;
            }
            else {
                siblin.hide();
                var res = $.ajax({
                    type: 'POST',

                    url: '<?php echo site_url('invites/sendemailinvitation');?>',
                    data: {remail: recipient_email, rmessage: recipient_message},
                    dataType: 'json',
                    cache: false,
                    async: false
                }).responseText;

                var inv_rpl = jQuery.parseJSON(res);
                //alert(inv_rpl.status);
                if (inv_rpl.status == 'success') {

                    $(this).val('');
                    $('#recipient_message').val('');

                    $('.tag').text('');
                    $('#tags_1').val('');   //change by darshan
                    $('.tag').css('display', 'none');
                    siblin.show();
                    siblin.text('<?php echo INVITATION_HAS_BEEN_SENT_TO;?> ' + inv_rpl.email);
                    siblin.removeClass('invite-error');
                    siblin.addClass('invite-success');
                    PopupClose();
                    //return true;

                }
                else {
                     PopupClose();
                    siblin.show();
                    if (inv_rpl.status == 'exist') {

                        siblin.text('<?php echo YOU_ALREADY_SENT_INVITATION_TO_THIS_EMAIL;?> ' + inv_rpl.email);
                    }

                    else if (inv_rpl.status == 'not_invite') {
                        siblin.text('<?php echo YOU_CAN_NOT_INVITE_YOUR_SELF;?> ' + inv_rpl.email);
                    }

                    else if (inv_rpl.status == 'message_required') {

                        siblin.text('<?php echo RECIPIENT_MESSAGE_IS_REQUIRED;?>');

                    }

                    else {

                        siblin.text('<?php echo INVITATION_IS_FAILED;?>');

                    }


                    siblin.removeClass('invite-success');
                    siblin.addClass('invite-error');
                    
                    //return false;
                }

            }

        });


    }

</script>

 <section>
        <div class="db-head">
            <div class="container">
                <div class="page-title">
                    <h2>Invite</h2>
                </div>
            </div>
        </div>
        <div class="grey-bg" id="section-area">         
            <div class="container">
                <div class="row">
                    <div class="col-md-3 db-mc-left">
                   <?php echo $this->load->view('default/dashboard_sidebar') ?>
                        
                    </div>
                    <div class="col-md-9 db-mc-right">
                    <div class="row">
                    <div class="col-md-8 invite-mail">
             <div class="inner-container-bg">
            <?php if (!$friend_list) { ?>
                <p><?php $var = sprintf(WE_COULDN_FIND_ANY_OF_YOUR_GMAIL_CONTACT_BECAUSE_YOU_HAVE_CONNECTED_GMAIL, $site_setting['site_name'], $site_setting['site_name']);
                    echo $var; ?></p>
                <p><?php echo WE_WILL_NEVER_SEND_OUT_ANY_EMAIL_MESSAGE_WITHOUT_ASKING; ?></p>

                <div class="marT20">
                    <a href="<?php echo $gmail_connect; ?>" class="social-btn connect-gm">
                        <i class="fa fa-google"></i>
                        <span><?php echo FIND_FIND_FROM_GMAIL; ?></span>
                    </a>
                </div>

            <?php } else { ?>

                <script type="text/javascript">
                    $(function () {

                        jQuery.expr[":"].icontains = jQuery.expr.createPseudo(function (arg) {
                            return function (elem) {
                                return jQuery(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
                            };
                        });

                        $('#query').focus().keyup(function (e) {
                            if (this.value.length > 0) {
                                $('ul#abbreviations li').hide();
                                $('ul#abbreviations li:icontains(' + this.value + ')').show();
                            } else {
                                $('ul#abbreviations li').show();
                            }
                            if (e.keyCode == 13) {
                                $(this).val('');
                                $('ul#abbreviations li').show();
                            }
                        });

                    });
                </script>
              
                <input type="hidden" name="multiple_email" id="multiple_email"
                       value="<?php echo base64_encode($multiple_email); ?>"/>
                <input type="hidden" name="multiple_name" id="multiple_name"
                       value="<?php echo base64_encode($multiple_name); ?>"/>
                <script type="text/javascript">
                    function selectAll(chk) {
                        if (chk) {
                            $('input:checkbox[name="check[]"]').each(function () {
                                if (!this.checked) {
                                    this.checked = true;
                                }
                            });
                        }
                        else {
                            $('input:checkbox[name="check[]"]').each(function () {
                                if (this.checked) {
                                    this.checked = false;
                                }
                            });
                        }
                    }
                </script>
                <form id="selected" name="selected" action="<?php echo site_url('invites/send_invite_all'); ?>"
                      method="POST">
                    <div id="warp">
                        <div id="ntpd">
                            <div class="marB20">
                                <h3 class="marT0">All Email List</h3>
                            </div>
                            <div id="tasks" class="gmail-friendList">
                                <ul class="row">
                                    <?php $chk_num = 1;
                                   
                                    if ($friend_list) {
                                        foreach ($friend_list as $fr) {
                                            $expfr = explode('||', $fr);
                                            $fremail = $expfr[0];
                                            $frname = $expfr[1];
                                            if ($frname == '') {
                                                $frname = $fremail;
                                            }
                                            if ($fremail != '') {
                                                ?>
                                                <li class="col-md-6 col-sm-6">
                                                    <label for="check-<?php echo $chk_num; ?>"><input type="checkbox"
                                                                                                      id="check-<?php echo $chk_num; ?>"
                                                                                                      name="check[]"
                                                                                                      value="<?php echo $fremail . '/' . $frname; ?>"><span
                                                            class="userSelected"></span><span class="gmail-userName"><i
                                                                class="glyphicon glyphicon-user"></i> <?php echo $frname; ?></span></label>
                                                </li>
                                            <?php
                                            }
                                            $chk_num++;
                                        }
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                        <div>
                            <a href="javascript:void(0);" onclick="selectAll(1);"><?php echo SELECT_ALL; ?></a> | <a
                                href="javascript:void(0);" onclick="selectAll(0);"><?php echo DESELECT_ALL; ?></a><!-- change by darshan -->
                        </div>
                        <div class="text-right form-group col-md-12">
                          
                             <button class="btn btn-primary" type="submit"><?php echo INVITE_ALL; ?></button>
                            <!-- change by darshan -->
                        </div>
                    </div>
                </form>
            <?php } ?>
        </div>
        
    
                    </div>
                    <div class="col-md-4 invite-frd">
                    <h3>Invite from social media</h3>
                    
                    <div class="sc-buttons">
                       <?php if ($facebook_enable == 1) { 

                         if (!isset($data['uid'])){ 
                        ?>
                    <div class="sc-fb">
                        <a href="javascript:login()">  <div class="social-text"><i class="fa fa-facebook fa-fw"></i> Invite From Facebook</div> </a>
                    </div>

                    <?php } else if (isset($data['uid'])){ ?>
                     <div class="sc-fb">
                        <a href="javascript:login()">  <div class="social-text"><i class="fa fa-facebook fa-fw"></i> Invite From Facebook</div> </a>
                    </div>
                    <?php } ?>
                    <?php } ?>
                    <div class="sc-tw">
                      <div class="social-text"><i class="fa fa-twitter fa-fw"></i> Invite From Twitter</div>
                    </div>
                    <div class="sc-in">
                      <div class="social-text"><i class="fa fa-linkedin fa-fw"></i> Invite From Linkedin</div>
                      </div>
                          <?php if ($google_enable == 1) { ?>

                      <a href="<?php echo site_url("invites/gmail"); ?>">
                      <div class="sc-gp">
                      <div class="social-text"><i class="fa fa-google-plus fa-fw"></i> Invite From Google Plus</div>
                     </div>
                     </a>

                <?php } ?>
                    </div>
                    </div>
     
                        
                    </div>
            </div>
        </div>
    </section>

