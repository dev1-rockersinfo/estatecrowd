<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title><?php echo $site_setting['site_name']; ?></title> <!-- change by darshan -->
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"/>

<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

<!-- Favicon -->
<link rel="icon" href="<?php echo base_url() . "images/themes/favicon.png"; ?>" sizes="32x32">

<!-- Default -->
<link rel="stylesheet" href="<?php echo base_url() . "css/style.css"; ?>" type="text/css" media="screen">

<!-- tooltip -->
<link rel="stylesheet" href="<?php echo base_url() . "css/tooltip.css"; ?>" type="text/css" media="screen">


<!-- Themes -->
<link rel="stylesheet" href="<?php echo base_url() . "css/themes.php"; ?>" type="text/css" media="screen">

<!-- Mobile Menu -->
<link rel="stylesheet" href="<?php echo base_url() . "css/responsive-nav.css"; ?>">

<!-- Responsive Stylesheets -->
<link rel="stylesheet" href="<?php echo base_url() . "css/responsive.css"; ?>" type="text/css" media="screen">


<!-- JQuery -->
<script src="http://code.jquery.com/jquery-1.8.0.min.js"></script>

<!-- Responsive -->

<script src="<?php echo base_url() . "js/responsive-nav.js"; ?>"></script>

<!-- search  --->
<script type="text/javascript" src="<?php echo base_url() . "js/demo.js"; ?>"></script>

<script type="text/javascript" src="<?php echo base_url() . "js/modernizr.js"; ?>"></script>

<link rel="stylesheet" href="<?php echo base_url() . "css/demo.css?v=2"; ?>"/>


<!-- Tabs -->
<link rel="stylesheet" href="<?php echo base_url() . "css/responsive-tabs.css"; ?>">

<script src="<?php echo base_url() . "js/responsiveTabs.js"; ?>"></script>

<!--Toggle -->

<script type="text/javascript">
    jQuery(function ($) {
        jQuery('.wrap').click(function () {
            jQuery('.search_div').slideToggle("slow");
            if (jQuery('.wrap').css('display') == 'none') {
                jQuery('.wrap').show();
            } else {
                jQuery('.wrap').hide();
            }
        });

        jQuery('.search_link').click(function () {
            jQuery('.search_div').slideToggle("slow");
            if (jQuery('.wrap').css('display') == 'none') {
                jQuery('.wrap').show();
            } else {
                jQuery('.wrap').hide();
            }
        });

        jQuery('.wrap').click(function () {
            jQuery('.log_div').slideToggle("slow");
            if (jQuery('.wrap').css('display') == 'none') {
                jQuery('.wrap').show();
            } else {
                jQuery('.wrap').hide();
            }
        });

        jQuery('.log_link').click(function () {
            jQuery('.log_div').slideToggle("slow");
            if (jQuery('.wrap').css('display') == 'none') {
                jQuery('.wrap').show();
            } else {
                jQuery('.wrap').hide();
            }
        });


    });
</script>


<!--Gallery -->
<link rel="stylesheet" href="<?php echo base_url() . "css/reset.css"; ?>"/>
<link rel="stylesheet" href="<?php echo base_url() . "css/refineslide.css"; ?>"/>
<link rel="stylesheet" href="<?php echo base_url() . "css/refineslide-theme-dark.css"; ?>"/>


<script src="<?php echo base_url() . "js/jquery.refineslide.js"; ?>"></script>
<script>
    $(function () {
        var $upper = $('#upper');

        $('#images').refineSlide({
            transition: 'cubeV',
            onInit: function () {
                var slider = this.slider,
                    $triggers = $('.translist').find('> li > a');

                $triggers.parent().find('a[href="#_' + this.slider.settings['transition'] + '"]').addClass('active');

                $triggers.on('click', function (e) {
                    e.preventDefault();

                    if (!$(this).find('.unsupported').length) {
                        $triggers.removeClass('active');
                        $(this).addClass('active');
                        slider.settings['transition'] = $(this).attr('href').replace('#_', '');
                    }
                });

                function support(result, bobble) {
                    var phrase = '';

                    if (!result) {
                        phrase = ' not';
                        $upper.find('div.bobble-' + bobble).addClass('unsupported');
                        $upper.find('div.bobble-js.bobble-css.unsupported').removeClass('bobble-css unsupported').text('JS');
                    }
                }

                support(this.slider.cssTransforms3d, '3d');
                support(this.slider.cssTransitions, 'css');
            }
        });
    });
</script>

<!--Gallery End -->

<!--pop iframe--->

<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="<?php echo base_url() . "js/jquery.fancybox.js?v=2.1.5"; ?>"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . "css/jquery.fancybox.css?v=2.1.5"; ?>"
      media="screen"/>


<script type="text/javascript">
    $(document).ready(function () {
        /*
         *  Simple image gallery. Uses default settings
         */

        $('.fancybox').fancybox();

        /*
         *  Different effects
         */

        // Change title type, overlay closing speed
        $(".fancybox-effects-a").fancybox({
            helpers: {
                title: {
                    type: 'outside'
                },
                overlay: {
                    speedOut: 0
                }
            }
        });

        // Disable opening and closing animations, change title type
        $(".fancybox-effects-b").fancybox({
            openEffect: 'none',
            closeEffect: 'none',

            helpers: {
                title: {
                    type: 'over'
                }
            }
        });

        // Set custom style, close if clicked, change title type and overlay color
        $(".fancybox-effects-c").fancybox({
            wrapCSS: 'fancybox-custom',
            closeClick: true,

            openEffect: 'none',

            helpers: {
                title: {
                    type: 'inside'
                },
                overlay: {
                    css: {
                        'background': 'rgba(238,238,238,0.85)'
                    }
                }
            }
        });

        // Remove padding, set opening and closing animations, close if clicked and disable overlay
        $(".fancybox-effects-d").fancybox({
            padding: 0,

            openEffect: 'elastic',
            openSpeed: 150,

            closeEffect: 'elastic',
            closeSpeed: 150,

            closeClick: true,

            helpers: {
                overlay: null
            }
        });

        /*
         *  Button helper. Disable animations, hide close button, change title type and content
         */

        $('.fancybox-buttons').fancybox({
            openEffect: 'none',
            closeEffect: 'none',

            prevEffect: 'none',
            nextEffect: 'none',

            closeBtn: false,

            helpers: {
                title: {
                    type: 'inside'
                },
                buttons: {}
            },

            afterLoad: function () {
                this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
            }
        });


        /*
         *  Thumbnail helper. Disable animations, hide close button, arrows and slide to next gallery item if clicked
         */

        $('.fancybox-thumbs').fancybox({
            prevEffect: 'none',
            nextEffect: 'none',

            closeBtn: false,
            arrows: false,
            nextClick: true,

            helpers: {
                thumbs: {
                    width: 50,
                    height: 50
                }
            }
        });

        /*
         *  Media helper. Group items, disable animations, hide arrows, enable media and button helpers.
         */
        $('.fancybox-media')
            .attr('rel', 'media-gallery')
            .fancybox({
                openEffect: 'none',
                closeEffect: 'none',
                prevEffect: 'none',
                nextEffect: 'none',

                arrows: false,
                helpers: {
                    media: {},
                    buttons: {}
                }
            });

        /*
         *  Open manually
         */

    });
</script>

<!--pop up iframe end-->

<!--Home Page Slider End -->

<link rel="stylesheet" href="<?php echo base_url() . "css/flexslider.css"; ?>" type="text/css" media="screen"/>

<script defer src="<?php echo base_url() . "js/jquery.flexslider.js"; ?>"></script>

<script type="text/javascript">
    /* $(function(){
     SyntaxHighlighter.all();
     });*/
    $(window).load(function () {
        $('.flexslider').flexslider({
            animation: "slide",
            start: function (slider) {
                $('body').removeClass('loading');
            }
        });
    });
</script>
<style type="text/css">
    span.error1 {
        color: red !important;
        font-size: 12px !important;
        font-weight: bold !important;
        display: none;
    }

    span.success1 {
        color: #009900 !important;
        font-size: 12px !important;
        font-weight: bold !important;
        display: none;
    }
</style>
<!--Home Page Slider -->
<style type="text/css">
    .fancybox-custom .fancybox-skin {
        box-shadow: 0 0 50px #222;
    }

    body {
        max-width: 700px;
        margin: 0 auto;
    }
</style>

<script type="text/javascript">
    function inviteemail() {
        var recipient_name = $("#recepition_name").val();
        var recipient_email = $("#recepition_email").val();
        /*var recipient_message=trim(removeHTMLTags($("#recipient_message").val()));*/
        var recipient_message = $("#recipient_message").val();
        if (recipient_email == '') {
            return false;
        }
        else {

            var res = $.ajax({
                type: 'POST',
                url: '<?php echo site_url('invites/sendinvitation');?>',
                data: {rname: recipient_name, remail: recipient_email, rmessage: recipient_message},
                dataType: 'json',
                cache: false,
                async: false
            }).responseText;


            var sendrpl = jQuery.parseJSON(res);

            if (sendrpl.status == 'success') {

                $("#sendmsg").show();
                setTimeout(closeme, 3000);
            }
            else {
                $("#sendmsg").show();
                $("#sendmsg").addClass('error1');
                $("#sendmsg").removeClass('success1');
                if (sendrpl.status == 'pending') {
                    $("#sendmsg").text('<?php echo YOUR_AFFILIATE_REQUEST_IS_IN_PENDING;?>'); //change by darshan
                }
                else {
                    $("#sendmsg").text('<?php echo SORRY_AN_ERROR_OCCURES_PLEASE_TRY_AGAIN;?>');//change by darshan
                }
                return false;

            }
        }

    }

    function closeme() {
        parent.$.fancybox.close();
        //$.fancybox.close();
    }
</script>

<section>
    <div class="contact_main_div"> <!--Left part-->
        <div class="heading_strip"><h1><?php echo INVITE_FRIENDS; ?></h1></div>
        <form name="">
            <div class="contact_div">
                <input type="hidden" name="recepition_email" id="recepition_email" value="<?php echo $email; ?>"/>
                <input type="hidden" name="recepition_name" id="recepition_name" value="<?php echo $name; ?>"/>

                <div class="contact_item">
                    <div class="contact_label"><?php echo YOUR_NAME; ?> :</div>
                    <!-- change by darshan -->
                    <div class="contact_field"><?php echo $name; ?></div>
                    <div class="clear"></div>
                </div>

                <div class="contact_item">
                    <div class="contact_label"><?php echo YOUR_EMAIL; ?> :</div>
                    <!-- change by darshan -->
                    <div class="contact_field"><?php echo $email; ?></div>
                    <div class="clear"></div>
                </div>

                <div class="contact_item">
                    <div class="contact_label"><?php echo MESSAGE; ?> :</div>
                    <!-- change by darshan -->
                    <div class="contact_field"><textarea name="recipient_message" id="recipient_message" cols="" rows=""
                                                         class="inputT_Default_S"></textarea><br/>
                        <span class="success1" id="sendmsg"><?php echo YOUR_INVITATION_SENT_SUCCESSFULLY; ?>!</span><!-- change by darshan -->
                    </div>
                    <div class="clear"></div>
                </div>


                <div class="contact_item contact_label_MLeft">
                    <input type="button" class="social_SC fl" value="<?php echo SEND; ?>" onclick="inviteemail();"><!-- change by darshan -->
                    <div class="clear"></div>
                </div>

            </div>
        </form>
        <!--Left Part End-->        </div>
    </div>
</section>
</body>
</html>
