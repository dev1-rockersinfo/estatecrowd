<?php
$facebook_setting = facebook_setting();
$data = array(
    'facebook' => $this->fb_connect->fb,
    'fbSession' => $this->fb_connect->fbSession,
    'user' => $this->fb_connect->user,
    'uid' => $this->fb_connect->user_id,
    'fbLogoutURL' => $this->fb_connect->fbLogoutURL,
    'fbLoginURL' => $this->fb_connect->fbLoginURL,
    'base_url' => site_url('home/facebook'),
    'appkey' => $this->fb_connect->appkey,
);
$response = file_get_contents('https://graph.facebook.com/' . $data['appkey']);
$response = json_decode($response);
$logoutUrl = $data['fbLogoutURL'];
$loginUrl = $data['fbLoginURL'];
$login_user_profileimage = base_url() . 'upload/no_man.gif';
if ($user_info[0]['image'] != '') {
    if (file_exists(base_path() . 'upload/thumb/' . $user_info[0]['image'])) {
        $login_user_profileimage = base_url() . 'upload/thumb/' . $user_info[0]['image'];
    } else {
        if (file_exists(base_path() . 'upload/orig/' . $user_info[0]['image'])) {
            $login_user_profileimage = base_url() . 'upload/orig/' . $user_info[0]['image'];
        }
    }
}
?>
<div id="fb-root"></div>
<!-- <script src="http://connect.facebook.net/en_US/all.js"></script>-->
<script>

    window.fbAsyncInit = function () {
        FB.init({
            appId: '<?php echo $data['appkey']; ?>', // App ID
            status: true, // check login status
            cookie: true, // enable cookies to allow the server to access the session
            xfbml: true, // parse XFBML
            oauth: true
        });
        /*FB.login({scope: 'email'},function(response){
         if(response.status == 'connected'){
         alert('I am connected');
         }
         });*/
        FB.getLoginStatus(function (response) {
            //alert(JSON.stringify(response));
            if (response.status === 'connected') {
                var uid = response.authResponse.userID;
                var accessToken = response.authResponse.accessToken;
                $('#fb-invite-friends').show();
                $('#fb-invite-login').hide();
                init();
            } else {
                $('#fb-invite-friends').hide();
                $('#fb-invite-login').show();
                $('#fb-invite-login a.loginButton').click(function (e) {
                    e.preventDefault();
                    login();
                });
            }
        });
    };
    // Load the SDK Asynchronously

    (function (d) {
        var js, id = 'facebook-jssdk';
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement('script');
        js.id = id;
        js.async = true;
        js.src = "//connect.facebook.net/en_US/all.js";
        d.getElementsByTagName('head')[0].appendChild(js);
    }(document));
    function login() {
        FB.login(function (response) { //alert(JSON.stringify(response));
            if ($('#jfmfs-container').length <= 0) {
               
                location.href = '<?php echo site_url('invites/email/'); ?>';
                 $("#email_form").hide();
                $("#facebook_form").show();
            }
            if (response.status) {
                init();
            } else {
                //alert('Login Failed!');
            }
        });
    }
    function send_message() {
        var redirect_url = document.URL;
        var myWindow = window.open("http://www.facebook.com/dialog/send?app_id=665394413522000&link=https://www.groupfund.me/home/signup/<?php echo $user_info[0]['unique_code'] ?>&redirect_uri=http://www.groupfund.me/invites/close", "", "top=100, left=400,width=800,height=500,scrollable=yes");
    }
    function init() {
        if ($("#jfmfs-container")) {
            FB.api('/me', function (response) {
                $("#logged-out-status").hide();
                $("#jfmfs-container").jfmfs();
                $("#invite-friends").css('display', 'inline-block');
            });
        }
    }
    $("#invite-friends").live("click", function () {
        var friendSelector = $("#jfmfs-container").data('jfmfs');
        var friendIdsAndNames = friendSelector.getSelectedIdsAndNames();
        if (friendIdsAndNames.length > 0) {
            $("#jfmfs-container").fadeTo('slow', .5);
            $("#invite-friends").fadeTo('slow', .5)
            $("#invite-friends").click(function () {
                return false;
            })

            var msg = document.getElementById('message').value;
            jQuery.ajax({
                type: 'POST',
                url: '<?php echo site_url('invites/sendinvite/'); ?>/' + msg,
                data: {facebook_invites: friendIdsAndNames},
                success: function (data) {
                    var response = FB.ui({
                        method: 'send',
                        name: "Join me on <?php echo ucfirst($site_setting['site_name']);?>!",
                        link: '<?php echo site_url('invited/'.$user_info[0]['unique_code']);?>/facebook',
                        picture: '<?php echo $login_user_profileimage; ?>',
                        description: "<?php echo ucfirst($user_info[0]['user_name']).' '.ucfirst($user_info[0]['last_name']);?> is on <?php echo ucfirst($site_setting['site_name']);?>, a place to discover, donate and post campaign. Join <?php echo ucfirst($site_setting['site_name']);?> to see <?php echo ucfirst($user_info[0]['user_name']);?>&acute;s campaigns.",
                        to: data,
                        //to: '['+data+']',
                        //to:['100001967248727','100001831222552'],
                        max_recipients: 50,
                        display: 'popup'
                    }, function (response) {//alert(JSON.stringify(response));
                        if (!response) {
                            parent.window.location.href = '<?php echo site_url('invites/email');?>';
                            return;
                        } else {
                            parent.window.location.href = '<?php echo site_url('invites/index/success');?>';
                            var request_id = response.request + "_" + response.to[0];
                        }
                    });
                    console.log(response.request_ids);
                    window.close();
                }
            });
        }
    });
 

</script>

<script type="text/javascript">
 function send_error() {

        $('#show_msg').html('');//change by darshan
        $('.get_invite_email_fild').remove();//change by darshan
        $('.invite-error').remove();//change by darshan
        var inv_email = $("#tags_1").val();
        var email_array = inv_email.split(',');

        var email_reg_exp = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;

//var recipient_message=trim(removeHTMLTags($("#recipient_message").val()));

        var recipient_message = $("#recipient_message").val();      //change by darshan  start
        if (inv_email == '') {
            alert('Email address is required');
            return false;
        }
        if (recipient_message == '') {
            alert('<?php echo RECIPIENT_MESSAGE_IS_REQUIRED ?>');
            return false;
        }
        //change by darshan  end
        for (var i = 0; i < email_array.length; i++) {

            $("#myemail").append('<input type="hidden" name="email' + i + '" id="email' + i + '" value="' + email_array[i] + '" class="get_invite_email_fild" />');
            $("#show_msg").append('<span id="spemail' + i + '" class="invite-success"></span><br>');
        }

        $('.get_invite_email_fild').each(function () {
            PopupOpen();
            var reci_email = $(this).val();
            var recipient_email = $.trim(reci_email);
            var recipient_id = $(this).attr('id');
            var siblin = $("#sp" + recipient_id);

            if (!email_reg_exp.test(recipient_email)) {
                siblin.show();

                siblin.text('Recipient email is invalid');
                siblin.removeClass('invite-success');
                siblin.addClass('invite-error');
                //return false;
            }
            else {
                siblin.hide();
                var res = $.ajax({
                    type: 'POST',

                    url: '<?php echo site_url('invites/sendemailinvitation');?>',
                    data: {remail: recipient_email, rmessage: recipient_message},
                    dataType: 'json',
                    cache: false,
                    async: false
                }).responseText;

                var inv_rpl = jQuery.parseJSON(res);
                //alert(inv_rpl.status);
                if (inv_rpl.status == 'success') {

                    $(this).val('');
                    $('#recipient_message').val('');

                    $('.tag').text('');
                    $('#tags_1').val('');   //change by darshan
                    $('.tag').css('display', 'none');
                    siblin.show();
                    siblin.text('<?php echo INVITATION_HAS_BEEN_SENT_TO;?> ' + inv_rpl.email);
                    siblin.removeClass('invite-error');
                    siblin.addClass('invite-success');
                    PopupClose();
                    //return true;

                }
                else {
                     PopupClose();
                    siblin.show();
                    if (inv_rpl.status == 'exist') {

                        siblin.text('<?php echo YOU_ALREADY_SENT_INVITATION_TO_THIS_EMAIL;?> ' + inv_rpl.email);
                    }

                    else if (inv_rpl.status == 'not_invite') {
                        siblin.text('<?php echo YOU_CAN_NOT_INVITE_YOUR_SELF;?> ' + inv_rpl.email);
                    }

                    else if (inv_rpl.status == 'message_required') {

                        siblin.text('<?php echo RECIPIENT_MESSAGE_IS_REQUIRED;?>');

                    }

                    else {

                        siblin.text('<?php echo INVITATION_IS_FAILED;?>');

                    }


                    siblin.removeClass('invite-success');
                    siblin.addClass('invite-error');
                    
                    //return false;
                }

            }

        });


    }

</script>



    <section>
        <div class="db-head">
            <div class="container">
                <div class="page-title">
                    <h2>Invite</h2>
                </div>
            </div>
        </div>
        <div class="grey-bg" id="section-area">         
            <div class="container">
                <div class="row">
                    <div class="col-md-3 db-mc-left">
          <?php echo $this->load->view('default/dashboard_sidebar'); ?>
                        
                    </div>


                    <div class="col-md-9 db-mc-right">
                    <div class="row">
                    <div class="col-md-8 invite-mail">
                 
                    <div class="ec-form" id="facebook_form" style="display: none;">
                       <div class="form-group col-md-12">

                            <div id="fb-invite-login">  
            <p><?php $var = sprintf(WE_COULDNOT_FIND_ANY_OF_YOUR_FRIEND_FROM_FACEBOOK_BECAUSE_YOU_HAVE_CONNECTED_FACEBOOK,$site_setting['site_name'],$site_setting['site_name']); echo $var;?></p>
            <p><?php echo WE_WILL_NEVER_SEND_OUT_ANY_EMAIL_MESSAGE_WITHOUT_ASKING;?></p>
            <div class=" marT20">
                <a href="javascript:login()" class="social-btn connect-fb" name="MyImage">
                    <i class="iconWrapper-fb"><img src="<?php echo base_url(); ?>images/social_icon1.png"></i>
                    <span><?php echo INVITE_YOUR_FRIENDS; ?></span>
                </a>   
            </div>
            </div>
            <div id="fb-root"></div>
            <script>(function (d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) return;
                    js = d.createElement(s);
                    js.id = id;
                    js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.0";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>
           
            <div id="fb-invite-friends">
                <input type="hidden" name="message" id="message" value=""/>
                <!--<input type="button" id="sendmsg" class=" save_Default mrg_btm5" onclick="send_message()" value="Send Message" />  -->
                <div class="fb-send"
                     data-href="<?php echo base_url() . 'home/signup/' . $user_info[0]['unique_code']; ?>"
                , "", "top=100, left=400,width=800,height=500,scrollable=yes" data-colorscheme="dark">
            </div>
            <!--<fb:send></fb:send>-->
        </div>
        </div>
        </div>
                  
                    <div class="ec-form" id="email_form">
                       <div class="form-group col-md-12">
                                    <?php $site_name = $site_setting['site_name']; ?>
                                <h3><?php echo INVITE_YOUR_FRIENDS_TO; ?> <?php echo $site_name; ?></h3>
                                       <div id="myemail">
                                     <input type="text" name="email1" id="tags_1" class="form-control tags" placeholder="Email Address " value="">

                                    <p><?php echo MULTIPLE_EMAIL_ADDRESS_SHOULD_BE_SEPERATED_WITH_COMMA; ?> </p>
                                     <div id="show_msg" class="col-md-12">
                                        <span id="spemail1" class="invite-success"></span>
                                    </div>
                                    </div>
                                 </div>
                
                     <div class="form-group col-md-12">                     
                        <textarea class="form-control" id="recipient_message" name="recipient_message" placeholder="Message"></textarea>
                           
                     </div>
                       <div class="form-group col-md-12">                       
                        
                               <button type="button" name="sendinvite" class="btn btn-primary"
                                   onclick="send_error();">Invite</button>
                     </div>
                
                </div>

              
    
                    </div>
                    <div class="col-md-4 invite-frd">
                    <h3>Invite from social media</h3>
                    
                    <div class="sc-buttons">
                      <?php if ($facebook_enable == 1) { 

                         if (!isset($data['uid'])){ 
                        ?>
                    <div class="sc-fb">
                        <a href="javascript:login()">  <div class="social-text"><i class="fa fa-facebook fa-fw"></i> Invite From Facebook</div> </a>
                    </div>

                    <?php } else if (isset($data['uid'])){ ?>
                     <div class="sc-fb">
                        <a href="javascript:login()">  <div class="social-text"><i class="fa fa-facebook fa-fw"></i> Invite From Facebook</div> </a>
                    </div>
                    <?php } ?>
                    <?php } ?>
                    <div class="sc-tw">
                      <div class="social-text"><i class="fa fa-twitter fa-fw"></i> Invite From Twitter</div>
                    </div>
                    <div class="sc-in">
                      <div class="social-text"><i class="fa fa-linkedin fa-fw"></i> Invite From Linkedin</div>
                      </div>
                         <?php if ($google_enable == 1) { ?>

                      <a href="<?php echo site_url("invites/gmail"); ?>">
                      <div class="sc-gp">
                      <div class="social-text"><i class="fa fa-google-plus fa-fw"></i> Invite From Google Plus</div>
                     </div>
                     </a>

                <?php } ?>
                    </div>
                    </div>
     
                        
                    </div>
            </div>
        </div>
    </section>
