<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.facebook.multifriend.select.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.facebook.multifriend.select-list.css"/>
<?php
$facebook_setting = facebook_setting();
$data = array(
    'facebook' => $this->fb_connect->fb,
    'fbSession' => $this->fb_connect->fbSession,
    'user' => $this->fb_connect->user,
    'uid' => $this->fb_connect->user_id,
    'fbLogoutURL' => $this->fb_connect->fbLogoutURL,
    'fbLoginURL' => $this->fb_connect->fbLoginURL,
    'base_url' => site_url('home/facebook'),
    'appkey' => $this->fb_connect->appkey,
);
$response = file_get_contents('https://graph.facebook.com/' . $data['appkey']);
$response = json_decode($response);
$logoutUrl = $data['fbLogoutURL'];
$loginUrl = $data['fbLoginURL'];
$login_user_profileimage = base_url() . 'upload/no_man.gif';
if ($user_info[0]['image'] != '') {
    if (file_exists(base_path() . 'upload/thumb/' . $user_info[0]['image'])) {
        $login_user_profileimage = base_url() . 'upload/thumb/' . $user_info[0]['image'];
    } else {
        if (file_exists(base_path() . 'upload/orig/' . $user_info[0]['image'])) {
            $login_user_profileimage = base_url() . 'upload/orig/' . $user_info[0]['image'];
        }
    }
}
?>
<div id="fb-root"></div>
<!-- <script src="http://connect.facebook.net/en_US/all.js"></script>-->
<script>
    window.fbAsyncInit = function () {
        FB.init({
            appId: '<?php echo $data['appkey']; ?>', // App ID
            status: true, // check login status
            cookie: true, // enable cookies to allow the server to access the session
            xfbml: true, // parse XFBML
            oauth: true
        });
        /*FB.login({scope: 'email'},function(response){
         if(response.status == 'connected'){
         alert('I am connected');
         }
         });*/
        FB.getLoginStatus(function (response) {
            //alert(JSON.stringify(response));
            if (response.status === 'connected') {
                var uid = response.authResponse.userID;
                var accessToken = response.authResponse.accessToken;
                $('#fb-invite-friends').show();
                $('#fb-invite-login').hide();
                init();
            } else {
                $('#fb-invite-friends').hide();
                $('#fb-invite-login').show();
                $('#fb-invite-login a.loginButton').click(function (e) {
                    e.preventDefault();
                    login();
                });
            }
        });
    };
    // Load the SDK Asynchronously

    (function (d) {
        var js, id = 'facebook-jssdk';
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement('script');
        js.id = id;
        js.async = true;
        js.src = "//connect.facebook.net/en_US/all.js";
        d.getElementsByTagName('head')[0].appendChild(js);
    }(document));
    function login() {
        FB.login(function (response) { //alert(JSON.stringify(response));
            if ($('#jfmfs-container').length <= 0) {
                location.href = '<?php echo site_url('invites/index/'); ?>';
            }
            if (response.status) {
                init();
            } else {
                //alert('Login Failed!');
            }
        });
    }
    function send_message() {
        var redirect_url = document.URL;
        var myWindow = window.open("http://www.facebook.com/dialog/send?app_id=665394413522000&link=https://www.groupfund.me/home/signup/<?php echo $user_info[0]['unique_code'] ?>&redirect_uri=http://www.groupfund.me/invites/close", "", "top=100, left=400,width=800,height=500,scrollable=yes");
    }
    function init() {
        if ($("#jfmfs-container")) {
            FB.api('/me', function (response) {
                $("#logged-out-status").hide();
                $("#jfmfs-container").jfmfs();
                $("#invite-friends").css('display', 'inline-block');
            });
        }
    }
    $("#invite-friends").live("click", function () {
        var friendSelector = $("#jfmfs-container").data('jfmfs');
        var friendIdsAndNames = friendSelector.getSelectedIdsAndNames();
        if (friendIdsAndNames.length > 0) {
            $("#jfmfs-container").fadeTo('slow', .5);
            $("#invite-friends").fadeTo('slow', .5)
            $("#invite-friends").click(function () {
                return false;
            })

            var msg = document.getElementById('message').value;
            jQuery.ajax({
                type: 'POST',
                url: '<?php echo site_url('invites/sendinvite/'); ?>/' + msg,
                data: {facebook_invites: friendIdsAndNames},
                success: function (data) {
                    var response = FB.ui({
                        method: 'send',
                        name: "Join me on <?php echo ucfirst($site_setting['site_name']);?>!",
                        link: '<?php echo site_url('invited/'.$user_info[0]['unique_code']);?>/facebook',
                        picture: '<?php echo $login_user_profileimage; ?>',
                        description: "<?php echo ucfirst($user_info[0]['user_name']).' '.ucfirst($user_info[0]['last_name']);?> is on <?php echo ucfirst($site_setting['site_name']);?>, a place to discover, donate and post campaign. Join <?php echo ucfirst($site_setting['site_name']);?> to see <?php echo ucfirst($user_info[0]['user_name']);?>&acute;s campaigns.",
                        to: data,
                        //to: '['+data+']',
                        //to:['100001967248727','100001831222552'],
                        max_recipients: 50,
                        display: 'popup'
                    }, function (response) {//alert(JSON.stringify(response));
                        if (!response) {
                            parent.window.location.href = '<?php echo site_url('invites/index');?>';
                            return;
                        } else {
                            parent.window.location.href = '<?php echo site_url('invites/index/success');?>';
                            var request_id = response.request + "_" + response.to[0];
                        }
                    });
                    console.log(response.request_ids);
                    window.close();
                }
            });
        }
    });
 

</script>

<?php echo $this->load->view('default/dashboard_sidebar') ?>
<section>
    <div class="container padTB50">
        <div class="sub-tab no-tab">
            <ul class="clearfix">
                <?php if ($facebook_enable == 1) { ?>
                    <li class="active"><a href="<?php echo site_url("invites/index"); ?>"><?php echo FACEBOOK; ?></a>
                    </li>
                <?php } ?>
                <li><a href="<?php echo site_url("invites/email"); ?>"><?php echo EMAIL; ?></a></li>
                <?php if ($google_enable == 1) { ?>
                    <li><a href="<?php echo site_url("invites/gmail"); ?>">Gmail</a></li>
                <?php } ?>
                
            </ul>
        </div>
        <div class="inner-container-bg">
            <p><?php echo sprintf(FACEBOOK_WILL_FETCH, $response->name);//echo $data['appkey'];?></p>
            <?php if (!isset($data['uid'])){ ?>
                 <div id="fb-invite-login"> 
            <p><?php $var = sprintf(WE_COULDNOT_FIND_ANY_OF_YOUR_FRIEND_FROM_FACEBOOK_BECAUSE_YOU_HAVE_CONNECTED_FACEBOOK,$site_setting['site_name'],$site_setting['site_name']); echo $var;?></p>
            <p><?php echo WE_WILL_NEVER_SEND_OUT_ANY_EMAIL_MESSAGE_WITHOUT_ASKING;?></p>
            <div class=" marT20">
                <a href="javascript:login()" class="social-btn connect-fb" name="MyImage">
                    <i class="iconWrapper-fb"><img src="<?php echo base_url(); ?>images/social_icon1.png"></i>
                    <span><?php echo INVITE_YOUR_FRIENDS; ?></span>
                </a>   
            </div>
            </div>
            <?php }else if (isset($data['uid'])){ ?>
            <div id="fb-invite-login">  
            <p><?php $var = sprintf(WE_COULDNOT_FIND_ANY_OF_YOUR_FRIEND_FROM_FACEBOOK_BECAUSE_YOU_HAVE_CONNECTED_FACEBOOK,$site_setting['site_name'],$site_setting['site_name']); echo $var;?></p>
            <p><?php echo WE_WILL_NEVER_SEND_OUT_ANY_EMAIL_MESSAGE_WITHOUT_ASKING;?></p>
            <div class=" marT20">
                <a href="javascript:login()" class="social-btn connect-fb" name="MyImage">
                    <i class="iconWrapper-fb"><img src="<?php echo base_url(); ?>images/social_icon1.png"></i>
                    <span><?php echo INVITE_YOUR_FRIENDS; ?></span>
                </a>   
            </div>
            </div>
            <div id="fb-root"></div>
            <script>(function (d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) return;
                    js = d.createElement(s);
                    js.id = id;
                    js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.0";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>
           
            <div id="fb-invite-friends">
                <input type="hidden" name="message" id="message" value=""/>
                <!--<input type="button" id="sendmsg" class=" save_Default mrg_btm5" onclick="send_message()" value="Send Message" />  -->
                <div class="fb-send"
                     data-href="<?php echo base_url() . 'home/signup/' . $user_info[0]['unique_code']; ?>"
                , "", "top=100, left=400,width=800,height=500,scrollable=yes" data-colorscheme="dark">
            </div>
            <!--<fb:send></fb:send>-->
        </div>
        <?php } else { ?>
            <div id="fb-invite-friends">
                <div id="username"></div>
                <a href="#" id="show-friends" style="display:none;"><?php echo "SHOW_SELECTED_FRIENDS"; ?></a>

                <div id="selected-friends"></div>
                <div id="jfmfs-container"></div>
                <input type="hidden" name="message" id="message" value=""/>
                <input type="button" id="sendmsg" class=" save_Default mrg_btm5" onclick="postToFriendByMessage()"
                       value="Send Message"/>
                <input type="button" id="postwall" class=" save_Default mrg_btm5" onclick="postToFriendByWallpost()"
                       value="Friend Wall post"/>
                <input type="button" id="postwall" class=" save_Default" onclick="postToMyWallpost()"
                       value="My Wall post"/>
            </div>
        <?php } ?>
    </div>
    </div>
</section>
