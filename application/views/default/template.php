<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<title><?php echo ($meta_title != "") ? strip_tags(str_replace(array('&nbsp;'), '', $meta_title)) : ":: Welcome to FundraisingScript ::"; ?></title>

<meta name="description"
      content="<?php echo ($meta_description != "") ? strip_tags(str_replace(array('&nbsp;'), '', $meta_description)) : "FundraisingScript"; ?>"/>

<meta name="keywords"
      content="<?php echo ($meta_keyword != '') ? strip_tags(str_replace(array('&nbsp;'), '', $meta_keyword)) : "FundraisingScript"; ?>"/>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"/>


<?php
    
    
 if(isset($share_property_detail)) { echo meta_tags($share_property_detail); } else { echo meta_tags(); } 

 if(isset($share_crowd_detail)) { echo crowd_meta_tags($share_crowd_detail); } else { echo meta_tags(); }

 ?>

<!--<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>-->


<!-- Favicon -->

<?php


$site_setting = site_setting();

$favicon_image = $site_setting['favicon_image'];
?>

<link rel="icon" href="<?php echo base_url() . 'upload/orig/' . $favicon_image; ?>"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

<!-- Favicon -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="<?php echo base_url();?>assets_front/bootstrap/css/bootstrap.min.css">

<!-- Bootstrap Select -->
<link rel="stylesheet" href="<?php echo base_url();?>assets_front/bootstrap-select/css/bootstrap-select.css">

<!-- Simple Line Icons -->
<link rel="stylesheet" href="<?php echo base_url();?>assets_front/simple-line-icons/css/simple-line-icons.css">

<!-- Font Awesome Icons -->
<link rel="stylesheet" href="<?php echo base_url();?>assets_front/font-awesome/css/font-awesome.min.css">

<!-- Push Menu -->
<link href="<?php echo base_url();?>assets_front/push-menu/css/push-menu.css" rel="stylesheet" />

<!-- Carousel -->
<link href="<?php echo base_url();?>assets_front/owl-carousel/css/owl.carousel.css" rel="stylesheet" />

<!-- Style CSS -->
<link rel="stylesheet" href="<?php echo base_url();?>css/style.css">

<!-- Responsive CSS -->
<link rel="stylesheet" href="<?php echo base_url();?>css/responsive.css">

<link rel="stylesheet" href="<?php echo base_url();?>css/circle.css">

 <link href="<?php echo base_url();?>css/tabdrop.css" rel="stylesheet">
 
 <link rel="stylesheet" href="<?php echo base_url();?>css/redactor.css" />
 <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/jquery.tokenize.css" />
 
  <link rel="stylesheet" href="<?php echo base_url();?>css/datepicker.css">
  <link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap-switch.css">
  
<script src="<?php echo base_url(); ?>js/bootstrap-tabdrop.js" type="text/javascript"></script>


<script src="<?php echo base_url();?>js/bootstrap-filestyle.min.js"></script>
<script src="<?php echo base_url();?>assets_front/bootstrap/js/bootstrap.js"></script>    
<script src="<?php echo base_url();?>assets_front/push-menu/js/push-menu.js"></script>
<script type="text/javascript">
  
     $(document).ready(function(){
    $(".dropdown").hover(            
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeIn("slow");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");                
            },
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeOut("slow");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");                
            });
    });
</script>

<!-- Character Counter -->

<script src="<?php echo base_url(); ?>js/jquery.jqEasyCharCounter.js" type="text/javascript"></script>

<script>


   $(document).ready(function() {

        window.prettyPrint && prettyPrint();

        $('.responsive-tab:first').tabdrop();

    });

</script>


<!--mover user to particular box position-->

<script type="text/javascript">

    function goToByScroll(id) {

        $('html, body').animate({scrollTop: $("#" + id).offset().top - 100}, 1000);

    }


    <!--Remove space-->

    function removetrim() {

        $(":text,textarea").each(function () {

            $(this).blur(function () {

                $(this).val($.trim($(this).val()))

            });

        });

    }

    removetrim()

    <!--Remove space-->
  

</script>


<!--pop iframe-->


<script type="text/javascript">

    $(document).ready(function () {

        <!--alert message-->

        $(".alert-message").click(function () {

            $(this).slideUp("slow");

        });


        <!--alert message-->


        <!--Tooltip-->

        $('[data-toggle="tooltip"]').tooltip();

        <!--Tooltip-->


        <!--popover-->

        $('[data-toggle="popover"]').popover();

        <!--popover-->

    });


    function LTrim(value) {


        var re = /\s*((\S+\s*)*)/;

        return value.replace(re, "$1");


    }


    // Removes ending whitespaces

    function RTrim(value) {


        var re = /((\s*\S+)*)\s*/;

        return value.replace(re, "$1");


    }


    // Removes leading and ending whitespaces

    function trim(value) {


        return LTrim(RTrim(value));


    }


    function jvencode(str) {


        var s1 = str.replace(')', 'KQWER');

        var s2 = s1.replace('(', 'WEKREV');

        return encodeURIComponent(s2);

    }

    function jvdecode(str) {


        return decodeURIComponent(str.replace(/\+/g, " "));

    }


    function isValidURL(url) {

        var RegExp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=%@!\-\/]))?/;


        if (RegExp.test(url)) {

            return true;

        } else {

            return false;

        }

    }


    function removeHTMLTags(str) {


        var strInputCode = str;


        strInputCode = strInputCode.replace(/(lt|gt);/g, function (strMatch, p1) {

            return (p1 == "lt") ? "&lt;" : "&gt;";

        });

        var strTagStrippedText = strInputCode.replace(/<\/?[^>]+(>|$)/g, "");

        return strTagStrippedText;

        // Use the alert below if you want to show the input and the output text

        //		alert("Input code:\n" + strInputCode + "\n\nOutput text:\n" + strTagStrippedText);


    }


    function limitText(limitField, limitCount, limitNum) {


        if (limitField.value.length > limitNum) {

            limitField.value = limitField.value.substring(0, limitNum);

        } else {

            limitCount.value = limitNum - limitField.value.length;

        }

    }


</script>

<script type="text/javascript">
    $(document).ready(function () {
        // Javascript to enable link to tab
        var url = document.location.toString();

        var tabName = url.split('#')[1];

        if (url.match('#')) {
            $('.sub-tab a[href=#' + tabName + ']').tab('show');
            window.scrollTo(0, 0);
        }

        // Change hash for page-reload
        $('.sub-tab a').on('shown.bs.tab', function (e) {
            window.location.hash = e.target.hash;
            window.scrollTo(0, 0);
        })
    });

</script>


<!--pop up iframe end-->

<script type="text/javascript">

    $(document).ready(function () {

        //$('.show_div').hide();

        //$('input:radio[name="method"]').removeAttr('checked');

        $(".error_change").delay(5000).slideUp("slow");

        $('input:radio[name="method"]').click(function () {

            var rad_val = $(this).attr('id');

            var sd = rad_val.split('_');

            if ($('#withdraw-amount_div_' + sd[1]).is(':visible') == false) {

                $('.show_div').slideUp();

                $('#withdraw-amount_div_' + sd[1]).slideDown();

            }

        });

        $("#success_notify").click(function () {

            $(".close").slideToggle('slow', 'linear', 'hide');

        });
       

    });

</script>

<?php

$site_logo = $site_setting['site_logo'];

$site_logo_hover = $site_setting['site_logo_hover'];

$this->active = $this->uri->uri_string();

$this->strdd = explode("/", $this->active);

//print_r($this->strdd) ;

if (!isset($this->strdd[1])) {

    $this->strdd[1] = '';

}

if (!isset($this->strdd[2])) {

    $this->strdd[2] = '';

}

$slider = 'no';

if ($this->strdd[0] == "home" or $this->strdd[0] == "") {

    $slider = 'yes';

    if (isset($this->strdd[1])) {

        //if($this->strdd[1]!='index')$slider='no';

    }

}

?>



<!--Loader Start-->

<script src="<?php echo base_url(); ?>js/heartcode-canvasloader.js" type="text/javascript"></script>

<!--Loader End-->


<!--Popup-->

<link href="<?php echo base_url(); ?>css/magnific-popup.css" rel="stylesheet"/>

<script src="<?php echo base_url(); ?>js/jquery.magnific-popup.js"></script>

<script type="text/javascript">

    $(document).ready(function () {


        $('.mfPopup').magnificPopup({

            type: 'ajax',

            alignTop: true, // as we know that popup content is tall we set scroll overflow by default to avoid jump

        });


        $('.mfPopup-inline').magnificPopup({

            type: 'inline',


            fixedContentPos: true,

            fixedBgPos: true,


            overflowY: 'auto',


            closeBtnInside: true,

            preloader: false,


            midClick: true,

            removalDelay: 300,

            mainClass: 'my-mfp-zoom-in'

        });

    });

</script>



<!--Hearder Slider End-->


<script type="text/javascript">

    $(document).ready(function () {

        $("#brand")

            .mouseover(function () {

                $(".brand-hover").show();

                $(".brand-orig").hide();

            })

            .mouseout(function () {

                $(".brand-hover").hide();

                $(".brand-orig").show();

            });

    });

</script>


<link rel="stylesheet" href="<?php echo base_url(); ?>editor/css/redactor.css"/>

<script src="<?php echo base_url(); ?>editor/js/redactor.min.js"></script>


<script type="text/javascript" src="<?php echo base_url(); ?>js/bootstrap-select.js"></script>

<script type="text/javascript">

    $(window).on('load', function () {

        $('.selectpicker').selectpicker({

            'selectedText': 'cat'

        });

    });

    $(window).bind("load", function () {

       
        $('#loader-wrapper').fadeOut(1000);

        
    });

</script>

<script type="text/javascript">
    function PopupOpen() {
        $('#loader-wrapper-ajax').fadeIn(1000);
    }
    function PopupClose() {
        $('#loader-wrapper-ajax').fadeOut(1000);
    }
    function GetFormattedDate(formatedate) {
        var todayTime = new Date(formatedate);
        var month = todayTime .getMonth()+1;
        var day = todayTime .getDate();
        var year = todayTime .getFullYear();
        return year + "-" + month + "-" + day;
    }
    
</script>


</head>

<body>


<?php echo $header; ?>

<?php echo $main_content; ?>

<?php echo $footer; ?>

</body>

</html>
