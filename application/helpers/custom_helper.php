<?php
//image cleaner function
function image_clener_db()
{


    error_reporting(0);
    $CI =& get_instance();

    $tables = array('property', 'user', 'property_gallery', 'site_setting', 'invite_members', 'categories', 'investors', 'deal_type_setting', 'file_gallery');
    $columns = array(
        array('cover_image'),
        array('image'),
        array('image'),
        array('site_logo', 'site_logo_hover', 'favicon_image', 'accredential_file'),
    
        array('member_image'),
        array('image'),
        array('investor_image'),
        array('deal_type_icon'),
        array('file_path'),
       
    );
    $paths = array(
        array('upload/property/large', 'upload/property/medium', 'upload/property/commoncard', 'upload/property/small', 'upload/orig'),
        array('upload/user/user_small_image', 'upload/orig', 'upload/user/user_big_image', 'upload/user/user_medium_image'),
        array('upload/gallery'),
        array('upload/orig', 'upload/doc'),
        
        array('upload/property/team_member'),
        array('upload/category','upload/category_search'),
        array('upload/property/investor'),
        array('upload/property/deal_icon'),
        array('upload/property/investor'),
       
    );


    $allpaths = array();


    $t = 0;
    $datacolumn = array();

    if ($tables) {

        foreach ($tables as $table) {

            $dataimage = array();

            if ($paths[$t]) {

                $pathcount = 0;
                foreach ($paths[$t] as $path) {

                    if (in_array($path, $allpaths)) {
                        $pathcount = 1;
                    }
                    $allpaths = array_unique(array_merge($allpaths, array($path)));

                    if ($columns[$t]) {

                        foreach ($columns[$t] as $column) {

                            $dataimage = array();
                            $query = $CI->db->query("SELECT $column FROM $table where $column != ''");

                            if ($query->num_rows() > 0) {
                                $images = $query->result_array();

                                if ($images) {
                                    foreach ($images as $img) {
                                        $dataimage[] = $img[$column];
                                    }
                                }
                            }

                            $datacolumn[$path][$column] = $dataimage;
                        }
                    }

                }
            }

            $t++;
        }
    }
    //  echo '<pre>'; print_r($datacolumn);
    // die;
    if ($datacolumn) {
        foreach ($datacolumn as $key => $value) {
            $folderimages = find_files($key);

            if (is_array($value)) {

                $arraycount = count($value);

                if ($arraycount == 1) {
                    foreach ($value as $cat => $res) {

                        if (is_array($folderimages) && is_array($res)) {
                            $removeimg[$key] = array_diff($folderimages, $res);
                        }
                    }
                } else {

                    $alldataimage = array();

                    foreach ($value as $cat => $res) {
                        $alldataimage = array_merge($alldataimage, $res);
                    }
                    $removeimg[$key] = array_diff($folderimages, $alldataimage);
                }

            }

        }
    }

    // echo '<pre>'; print_r($removeimg);
    //die();
    $count = 0;
    if (is_array($removeimg)) {
        foreach ($removeimg as $path => $image) {
            foreach ($image as $id => $img) {
                //echo $img.'<br>';
                if ($img == 'no_img.jpg' || $img == 'no_man.gif' || $img == 'no_man.jpg') {
                    continue;
                }
                $link = base_path() . $path . '/' . $img;
                // echo $link.'<br>';
                unlink($link);
                $count++;
            }
        }

    }
//echo $count.' files where removed or unlinked';
//die();
}

function find_files($dir)
{
    $CI =& get_instance();

    if (is_dir($dir)) {
        $root = scandir($dir);
        $result = array();
        foreach ($root as $value) {
            if ($value === '.' || $value === '..') {
                continue;
            }
            if (is_file("$dir/$value")) {
                $result[] = $value;
                continue;
            }
        }
        return $result;
    }
    return array();
}

//gets the data from a URL
function get_tiny_url($url)
{
    $ch = curl_init();
    $timeout = 5;
    curl_setopt($ch, CURLOPT_URL, 'http://tinyurl.com/api-create.php?url=' . $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    $data = curl_exec($ch);
    curl_close($ch);
    return $data;
}


//===========check follower================
function check_is_follower($follow_user_id, $follow_by_user_id)
{
    $CI =& get_instance();
    $query = $CI->db->query('select * from user_follow where follow_user_id=' . $follow_user_id . ' and follow_by_user_id=' . $follow_by_user_id);
    if ($query->num_rows() > 0) {
        return 1;
    } else {
        return 0;
    }

}

//=================== CHECK EQUITY FOLLOW=============

function check_is_property_follower($property_id, $property_follow_user_id)
{
    $CI =& get_instance();
    $query = $CI->db->query('select * from property_follower where property_id=' . $property_id . ' and property_follow_user_id=' . $property_follow_user_id);
    if ($query->num_rows() > 0) {
        return 1;
    } else {
        return 0;
    }

}

function getcurrenturl()
{
    if (isset($_SERVER["HTTPS"]) && strtolower($_SERVER["HTTPS"]) == "on") {
        $pageURL = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
    } else {
        $pageURL = "http://";
    }

    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
    }
    return $pageURL;
}

/*
    Function name :check_user_authentication()
    Parameter : none
    Return : true or false
    Use : check user login or logout
  */
function check_user_authentication($redirect = false)
{
    $CI =& get_instance();
    if ($CI->session->userdata('user_id') != '') {
        if (!stristr($_SERVER['REQUEST_URI'], "account/first_step")) {
            $userdata = UserData($CI->session->userdata('user_id'));
            $user_password = $userdata[0]['password'];
            if ($user_password == '') redirect('account/first_step');
            //if($CI->session->userdata('email')=='')redirect('account/first_step');
        }
        return $CI->session->userdata('user_id');
    } else {

        if ($redirect == true) {
            $page = base64_encode(getcurrenturl());
            redirect('home/login/' . $page);

        }
        return false;
    }

}

/*
    Function name :get_authenticateUserID()
    Parameter : none
    Return : session id
    Use : it use for session id
  */
function get_authenticateUserID()
{
    $CI =& get_instance();
    return $CI->session->userdata('user_id');
}


/*
     Function name :GetDaysLeft()
     Parameter : dateg for days or hours or minutes left
     Return : days or hours or minutes or seconds
     Use : find for days


   */
function GetDaysLeft($dateg = '',$project_timezone='')
{
    $site_settings = site_setting();

    $time_zone = $site_settings['time_zone'];

    if ($project_timezone != '')
    {
        
        date_default_timezone_set($project_timezone);
    }
    else
    {
        date_default_timezone_set($time_zone);
    }

    $date1 = $dateg;
    $date2 = date("Y-m-d H:i:s");
    $diff = strtotime($date1) - strtotime($date2);
    //return $diff;
   $test = floor($diff / (60 * 60 * 24));
   $no_time='';
    $str ='';
    if (strtotime(date('Y-m-d', strtotime($dateg))) > strtotime(date('Y-m-d'))) {
       
        $temp = floor($diff / (60 * 60 * 24));
        $diff2 = abs(strtotime($date1) - strtotime($date2));
    
        $hours = floor(($diff2 - $temp * 60 * 60 * 24) / (60 * 60));
        if ($temp > 1) {
            
            $str = ($dateg != "0000-00-00 00:00:00") ? $test . ' ' . "<span>" . DAYS_LEFT . "</span>" : "<span>0 </span>" . DAYS_LEFT;
        } 
        else if($temp == 1){
           
            $str = ($dateg != "0000-00-00 00:00:00") ? $test . ' ' . "<span>" . DAY_LEFT . "</span>" : "<span>0 </span>" . DAY_LEFT;
        }
        else if($temp == 0 and $hours > 0 and $hours < 24){
            
            $str = ($dateg != "0000-00-00 00:00:00") ? $hours . ' ' . "<span>" . HOURS_LEFT . "</span>" : "<span>0 </span>" . DAY_LEFT;
        }
        else if($hours == 1){
             
            $str = ($dateg != "0000-00-00 00:00:00") ? $hours . ' ' . "<span>" . HOUR_LEFT . "</span>" : "<span>0 </span>" . DAY_LEFT;
        }
        else if($temp < 0 && $hours < 0){
            $str = ($dateg != "0000-00-00 00:00:00") ? $no_time . ' ' . "<span>" . NO_TIME_LEFT . "</span>" : "<span>0 </span>" . DAY_LEFT;
        }
        else {
            $str = " <span>" . NO_TIME_LEFT . "</span>";
        }
        

    } else {
        
        $dategg = $dateg;
        $date2 = date('Y-m-d H:i:s');

        if (strtotime(date('Y-m-d H:i:s', strtotime($dateg))) > strtotime(date('Y-m-d H:i:s'))) {

            $diff2 = abs(strtotime($dategg) - strtotime($date2));
            $day1 = floor($diff2 / (60 * 60 * 24)).'days';


            $hours = floor(($diff2 - $day1 * 60 * 60 * 24) / (60 * 60));
            $minuts = floor(($diff2 - $day1 * 60 * 60 * 24 - $hours * 60 * 60) / 60);
            $seconds = floor(($diff2 - $day1 * 60 * 60 * 24 - $hours * 60 * 60 - $minuts * 60));

            // if ($hours != 0 && $hours > 1 || $minuts != 0 || $seconds != 0) {


            //     if ($hours != 0) {
            //         $str = "" . $hours . ' ' . "<span>" . HOURS_LEFT . "</span>";
            //     } elseif ($minuts != 0) {
            //         $str = "" . $minuts . ' ' . "<span>" . MINUTES_LEFT . "</span>";
            //     } else {
            //         $str = "" . $seconds . ' ' . "<span>" . SECONDS_LEFT . "</span>";
            //     }

            // } 
            if($day1 == 0 and $hours > 0 and $hours < 24)
            {
                $str = ($dateg != "0000-00-00 00:00:00") ? $hours . ' ' . "<span>" . HOURS_LEFT . "</span>" : "<span>0 </span>" . DAY_LEFT;
            }
            else if($hours == 1){
                $str = ($dateg != "0000-00-00 00:00:00") ? $hours . ' ' . "<span>" . HOUR_LEFT . "</span>" : "<span>0 </span>" . DAY_LEFT;
            }
            else if($hours < 1 && $minuts > 0){
                  $str = ($dateg != "0000-00-00 00:00:00") ? $minuts . ' ' . "<span>" . MINUTES_LEFT . "</span>" : "<span>0 </span>" . DAY_LEFT;
            }
            else if($day1 < 0 && $hours < 0){
                $str = ($dateg != "0000-00-00 00:00:00") ? $no_time . ' ' . "<span>" . NO_TIME_LEFT . "</span>" : "<span>0 </span>" . DAY_LEFT;
            }

            else {
                $str = " <span>" . NO_TIME_LEFT . "</span>";
            }
        } else {
            $str = " <span>" . NO_TIME_LEFT . "</span>";
        }

    }
     date_default_timezone_set($time_zone);
    return $str;
}
/*
Function name :ImageUpload()
Parameter : files
Return : secure file
Use : image uploading
*/
function ImageUpload($files, $gallery = false, $pitch = false)
{


    $new_img = '';
    $rand = rand(0, 100000);
    $image_settings = get_image_setting_data();
    if (trim($files["userfile"]["tmp_name"]) != "") {

        $CI =& get_instance();

        $base_path = $CI->config->slash_item('base_path');
       
        $path_parts = pathinfo($files['userfile']['name']);
        $date = new DateTime();
        
       
        if ($gallery == false) {

             $file_extention = $path_parts['extension'];
            $new_img = $rand .'-property-'.$date->getTimestamp().'.'.$file_extention;
           
            move_uploaded_file($files["userfile"]["tmp_name"], $base_path . "upload/orig/" . $new_img);

            $image_info = getimagesize($base_path . "upload/orig/" . $new_img);
            $image_width = $image_info[0];
            $image_height = $image_info[1];
            require_once $base_path . 'thumbgenerator/ThumbLib.inc.php';

            $case = 1;
            if ($pitch == true) $case = 2;//for pitch image

            switch ($case) {
                case 1:
                    $new_w = $image_settings['p_thumb_width']; //You can change these to fit the width and height you want
                    $new_h = $image_settings['p_thumb_height'];
                    $aspect_ratio = $image_settings['p_ratio'];

                    if ($aspect_ratio) {
                        if ($image_height > $image_width) {
                            $ratio = $new_h / $image_height;
                            $new_h = $new_h;
                            $new_w = $image_width * $ratio;
                        } else {
                            $ratio = $new_w / $image_width;
                            $new_w = $new_w;
                            $new_h = $image_height * $ratio;
                        }
                    }

                    $thumb = PhpThumbFactory::create($base_path . "upload/orig/" . $new_img);
                    $thumb->adaptiveResize($new_w, $new_h);
                    $cache_path = $base_path . "upload/property/commoncard/" . $new_img;
                    $thumb->save($cache_path);


                    $new_w = $image_settings['p_small_width']; //You can change these to fit the width and height you want
                    $new_h = $image_settings['p_small_height'];

                    if ($aspect_ratio) {
                        if ($image_height > $image_width) {
                            $ratio = $new_h / $image_height;
                            $new_h = $new_h;
                            $new_w = $image_width * $ratio;
                        } else {
                            $ratio = $new_w / $image_width;
                            $new_w = $new_w;
                            $new_h = $image_height * $ratio;
                        }
                    }

                    $thumb = PhpThumbFactory::create($base_path . "upload/orig/" . $new_img);
                    $thumb->adaptiveResize($new_w, $new_h);
                    $cache_path = $base_path . "upload/property/small/" . $new_img;
                    $thumb->save($cache_path);


                    $new_w = $image_settings['p_medium_width']; //You can change these to fit the width and height you want
                    $new_h = $image_settings['p_medium_height'];

                    if ($aspect_ratio) {
                        if ($image_height > $image_width) {
                            $ratio = $new_h / $image_height;
                            $new_h = $new_h;
                            $new_w = $image_width * $ratio;
                        } else {
                            $ratio = $new_w / $image_width;
                            $new_w = $new_w;
                            $new_h = $image_height * $ratio;
                        }
                    }

                    $thumb = PhpThumbFactory::create($base_path . "upload/orig/" . $new_img);
                    $thumb->adaptiveResize($new_w, $new_h);
                    $cache_path = $base_path . "upload/property/medium/" . $new_img;
                    $thumb->save($cache_path);

                    $new_w = $image_settings['p_large_width']; //You can change these to fit the width and height you want
                    $new_h = $image_settings['p_large_height'];

                    if ($aspect_ratio) {
                        if ($image_height > $image_width) {
                            $ratio = $new_h / $image_height;
                            $new_h = $new_h;
                            $new_w = $image_width * $ratio;
                        } else {
                            $ratio = $new_w / $image_width;
                            $new_w = $new_w;
                            $new_h = $image_height * $ratio;
                        }
                    }

                    $thumb = PhpThumbFactory::create($base_path . "upload/orig/" . $new_img);
                    $thumb->adaptiveResize($new_w, $new_h);
                    $cache_path = $base_path . "upload/property/large/" . $new_img;
                    $thumb->save($cache_path);
                    break;
                case 2:

                    $new_w = 670; //You can change these to fit the width and height you want
                    $new_h = 400;
                    $thumb = PhpThumbFactory::create($base_path . "upload/orig/" . $new_img);

                    $thumb->adaptiveResize($new_w, $new_h);
                    $cache_path = $base_path . "upload/property/medium/" . $new_img;
                    $thumb->save($cache_path);
                    break;

            }
        } else {

          
            $file_extention = $path_parts['extension'];
            $new_img = $rand .'-image-'.$date->getTimestamp().'.'.$file_extention;
           
            move_uploaded_file($files["userfile"]["tmp_name"], $base_path . "upload/gallery/" . $new_img);

        }

    }

    return $new_img;
}


/*
Function name :ImageUpload()
Parameter : files
Return : secure file
Use : image uploading
*/
function PropertyImageUpload($new_img)
{


    $CI =& get_instance();

    $base_path = $CI->config->slash_item('base_path');
   
    $image_settings = get_image_setting_data();
   
            
            $image_info = getimagesize($base_path . "upload/orig/" . $new_img);

            $image_width = $image_info[0];
            $image_height = $image_info[1];
            require_once $base_path . 'thumbgenerator/ThumbLib.inc.php';

            $case = 1;
           
            switch ($case) {
                case 1:
                    $new_w = $image_settings['p_thumb_width']; //You can change these to fit the width and height you want
                    $new_h = $image_settings['p_thumb_height'];
                    $aspect_ratio = $image_settings['p_ratio'];

                    if ($aspect_ratio) {
                        if ($image_height > $image_width) {
                            $ratio = $new_h / $image_height;
                            $new_h = $new_h;
                            $new_w = $image_width * $ratio;
                        } else {
                            $ratio = $new_w / $image_width;
                            $new_w = $new_w;
                            $new_h = $image_height * $ratio;
                        }
                    }

                    $thumb = PhpThumbFactory::create($base_path . "upload/orig/" . $new_img);
                    $thumb->adaptiveResize($new_w, $new_h);
                    $cache_path = $base_path . "upload/property/commoncard/" . $new_img;
                    $thumb->save($cache_path);


                    $new_w = $image_settings['p_small_width']; //You can change these to fit the width and height you want
                    $new_h = $image_settings['p_small_height'];

                    if ($aspect_ratio) {
                        if ($image_height > $image_width) {
                            $ratio = $new_h / $image_height;
                            $new_h = $new_h;
                            $new_w = $image_width * $ratio;
                        } else {
                            $ratio = $new_w / $image_width;
                            $new_w = $new_w;
                            $new_h = $image_height * $ratio;
                        }
                    }

                    $thumb = PhpThumbFactory::create($base_path . "upload/orig/" . $new_img);
                    $thumb->adaptiveResize($new_w, $new_h);
                    $cache_path = $base_path . "upload/property/small/" . $new_img;
                    $thumb->save($cache_path);


                    $new_w = $image_settings['p_medium_width']; //You can change these to fit the width and height you want
                    $new_h = $image_settings['p_medium_height'];

                    if ($aspect_ratio) {
                        if ($image_height > $image_width) {
                            $ratio = $new_h / $image_height;
                            $new_h = $new_h;
                            $new_w = $image_width * $ratio;
                        } else {
                            $ratio = $new_w / $image_width;
                            $new_w = $new_w;
                            $new_h = $image_height * $ratio;
                        }
                    }

                    $thumb = PhpThumbFactory::create($base_path . "upload/orig/" . $new_img);
                    $thumb->adaptiveResize($new_w, $new_h);
                    $cache_path = $base_path . "upload/property/medium/" . $new_img;
                    $thumb->save($cache_path);

                    $new_w = $image_settings['p_large_width']; //You can change these to fit the width and height you want
                    $new_h = $image_settings['p_large_height'];

                    if ($aspect_ratio) {
                        if ($image_height > $image_width) {
                            $ratio = $new_h / $image_height;
                            $new_h = $new_h;
                            $new_w = $image_width * $ratio;
                        } else {
                            $ratio = $new_w / $image_width;
                            $new_w = $new_w;
                            $new_h = $image_height * $ratio;
                        }
                    }

                    $thumb = PhpThumbFactory::create($base_path . "upload/orig/" . $new_img);
                    $thumb->adaptiveResize($new_w, $new_h);
                    $cache_path = $base_path . "upload/property/large/" . $new_img;
                    $thumb->save($cache_path);
                    break;
                case 2:

                    $new_w = 670; //You can change these to fit the width and height you want
                    $new_h = 400;
                    $thumb = PhpThumbFactory::create($base_path . "upload/orig/" . $new_img);

                    $thumb->adaptiveResize($new_w, $new_h);
                    $cache_path = $base_path . "upload/property/medium/" . $new_img;
                    $thumb->save($cache_path);
                    break;

            }
       unlink($base_path . "upload/orig/" . $new_img);
    return $new_img;
}

/*
Function name :ImageUpload()
Parameter : files
Return : secure file
Use : image uploading
*/
function UserImageUpload($files, $facebook = false)
{
    $new_img = '';
    $rand = rand(0, 100000);
    $image_settings = get_image_setting_data();
    $CI =& get_instance();
    $base_path = $CI->config->slash_item('base_path');
    require_once $base_path . 'thumbgenerator/ThumbLib.inc.php';
    if (trim($files["userfile"]["tmp_name"]) != "") {

        
        $path_parts = pathinfo($_FILES['userfile']['name']);
        $date = new DateTime();
        
        $file_extention = $path_parts['extension'];
        $new_img = $rand .'-user-'.$date->getTimestamp().'.'.$file_extention;

        move_uploaded_file($files["userfile"]["tmp_name"], $base_path . "upload/orig/" . $new_img);
        $image_info = getimagesize($base_path . "upload/orig/" . $new_img);
        $image_width = $image_info[0];
        $image_height = $image_info[1];

        $new_w = $image_settings['u_b_width']; //You can change these to fit the width and height you want
        $new_h = $image_settings['u_b_height'];
        $aspect_ratio = $image_settings['u_ratio'];

        if ($aspect_ratio) {
            if ($image_height > $image_width) {
                $ratio = $new_h / $image_height;
                $new_h = $new_h;
                $new_w = $image_width * $ratio;
            } else {
                $ratio = $new_w / $image_width;
                $new_w = $new_w;
                $new_h = $image_height * $ratio;
            }
        }

        $thumb = PhpThumbFactory::create($base_path . "upload/orig/" . $new_img);
        $thumb->adaptiveResize($new_w, $new_h);
        $cache_path = $base_path . "upload/user/user_big_image/" . $new_img;
        $thumb->save($cache_path);

        $new_w = $image_settings['u_s_width']; //You can change these to fit the width and height you want
        $new_h = $image_settings['u_s_height'];

        if ($aspect_ratio) {
            if ($image_height > $image_width) {
                $ratio = $new_h / $image_height;
                $new_h = $new_h;
                $new_w = $image_width * $ratio;
            } else {
                $ratio = $new_w / $image_width;
                $new_w = $new_w;
                $new_h = $image_height * $ratio;
            }
        }

        $thumb = PhpThumbFactory::create($base_path . "upload/orig/" . $new_img);
        $thumb->adaptiveResize($new_w, $new_h);
        $cache_path = $base_path . "upload/user/user_small_image/" . $new_img;
        $thumb->save($cache_path);

        $new_w = $image_settings['u_m_width']; //You can change these to fit the width and height you want
        $new_h = $image_settings['u_m_height'];

        if ($aspect_ratio) {
            if ($image_height > $image_width) {
                $ratio = $new_h / $image_height;
                $new_h = $new_h;
                $new_w = $image_width * $ratio;
            } else {
                $ratio = $new_w / $image_width;
                $new_w = $new_w;
                $new_h = $image_height * $ratio;
            }
        }

        $thumb = PhpThumbFactory::create($base_path . "upload/orig/" . $new_img);
        $thumb->adaptiveResize($new_w, $new_h);
        $cache_path = $base_path . "upload/user/user_medium_image/" . $new_img;
        $thumb->save($cache_path);
    }
    if ($facebook == true) {
        $new_img = $files["userfile"]["name"];
        //$new_img =$fb_uid.'.jpg';
        $outPath = $base_path . 'upload/orig/' . $new_img;
        $new_w = $image_settings['u_b_width']; //You can change these to fit the width and height you want
        $new_h = $image_settings['u_b_height'];
        $thumb = PhpThumbFactory::create($base_path . "upload/orig/" . $new_img);
        $thumb->adaptiveResize($new_w, $new_h);
        $cache_path = $base_path . "upload/user/user_big_image/" . $new_img;
        $thumb->save($cache_path);

        $new_w = $image_settings['u_s_width']; //You can change these to fit the width and height you want
        $new_h = $image_settings['u_s_width'];

        $thumb = PhpThumbFactory::create($base_path . "upload/orig/" . $new_img);
        $thumb->adaptiveResize($new_w, $new_h);
        $cache_path = $base_path . "upload/user/user_small_image/" . $new_img;
        $thumb->save($cache_path);

        $new_w = $image_settings['u_m_width']; //You can change these to fit the width and height you want
        $new_h = $image_settings['u_m_width'];

        $thumb = PhpThumbFactory::create($base_path . "upload/orig/" . $new_img);
        $thumb->adaptiveResize($new_w, $new_h);
        $cache_path = $base_path . "upload/user/user_medium_image/" . $new_img;
        $thumb->save($cache_path);
    }

    return $new_img;


}

/*
Function name :CompanyLogoUpload()
Parameter : files
Return : secure file
Use : image uploading
*/
function CompanyLogoUpload($files, $facebook = false)
{
    $new_img = '';
    $rand = rand(0, 100000);
    $image_settings = get_image_setting_data();
    $CI =& get_instance();
    $base_path = $CI->config->slash_item('base_path');
    require_once $base_path . 'thumbgenerator/ThumbLib.inc.php';
    if (trim($files["userfile"]["tmp_name"]) != "") {

        
        $path_parts = pathinfo($_FILES['userfile']['name']);
        $date = new DateTime();
        
        $file_extention = $path_parts['extension'];
        $new_img = $rand .'-company-logo-'.$date->getTimestamp().'.'.$file_extention;

        move_uploaded_file($files["userfile"]["tmp_name"], $base_path . "upload/orig/" . $new_img);
        $image_info = getimagesize($base_path . "upload/orig/" . $new_img);
        $image_width = $image_info[0];
        $image_height = $image_info[1];

        $new_w = '102'; //You can change these to fit the width and height you want
        $new_h = '95';
        $aspect_ratio = $image_settings['u_ratio'];

        if ($aspect_ratio) {
            if ($image_height > $image_width) {
                $ratio = $new_h / $image_height;
                $new_h = $new_h;
                $new_w = $image_width * $ratio;
            } else {
                $ratio = $new_w / $image_width;
                $new_w = $new_w;
                $new_h = $image_height * $ratio;
            }
        }

        $thumb = PhpThumbFactory::create($base_path . "upload/orig/" . $new_img);
        $thumb->adaptiveResize($new_w, $new_h);
        $cache_path = $base_path . "upload/company/logo/" . $new_img;
        $thumb->save($cache_path);

        unlink($base_path . "upload/orig/" . $new_img);
       
    }
  

    return $new_img;


}

/*
     Function name :GetProjectPercentage()
     Parameter : data for percentage count
     Return : percentage
     Use : find percentave for progress bar


   */
function GetCampaignPercentage($data = array())
{
    $w = 0;
    //print_r($data);
    if ($data['min_investment_amount'] == '0' or $data['min_investment_amount'] == '') {
        $w = 0;
    } else {
        /*if($data['amount_get']>=$data['amount'])
        {
            $w=100;
        }
        else
        {*/
        $w = ($data['amount_get'] / $data['min_investment_amount']) * 100;
        if ($w > 0 && $w < 1) {
            $w = 1;
        }
        //}
    }
    return round($w);
}

/*
Function name :SecurePostData()
Parameter : string
Return : secure string
Use : all input post or get data wil be purify for sql injection and cross scripting
*/
function SecurePostData($string = '')
{    
    $CI =& get_instance();
    $string = html_purify($CI->db->escape_str($string));
    $string = preg_replace('/(\r\n\r\n)$/', '', $string);
    $string = preg_replace("/\n+/", "", $string);
    $string = str_replace("\n", "<br>", $string);
    $string = str_replace("\r", "", $string);
   // $string = strip_slashes($string);
    $string = str_replace(PHP_EOL, null, $string);

    return $string;

}
/*
Function name :SecureShowData()
Parameter : string
Return : secure string
Use : all input post or get data wil be purify for sql injection and cross scripting
*/
function SecureShowData($string = '')
{
    $string = nl2br($string);
    $string = stripslashes($string);


    return $string;

}

/*
Function name :RemoveHtmltag()
Parameter : string
Return : secure string
Use : all input post or get data wil be purify for sql injection and cross scripting
*/
function RemoveHtmltag($string = '')
{
    $string = strip_tags($string);


    return $string;

}

/*End one project Details*/

/*
Function name :UserData()
Parameter : id, join table in array.
Return : user data
Use : user data of particular id and use this function to any where in site to get the user detail.
*/
function UserData($id = 0, $join = array())
{
    $CI =& get_instance();
    $cache_file_name = 'user_detail' . $id;
    $selectfields = 'user.profile_slug, user.user_id, user.salesforceuserid, user.user_name, user.last_name, user.email, user.password, user.image, user.address, user.zip_code, user.user_about, user.user_occupation, user.user_interest, user.user_skill, user.unique_code ,user.tw_screen_name, user.tw_id, user.fb_uid , user.date_added,user.enable_facebook_stream, user.facebook_url, user.facebook_wall_post, user.fb_access_token, user.enable_twitter_stream, user.twitter_url, user.autopost_site, user.tw_oauth_token_secret, user.tw_oauth_token, user.user_website, user.sf_referrer,user.sf_syndicate,user.linkedln_url, user.googleplus_url, user.bandcamp_url, user.youtube_url, user.myspace_url, user.forgot_unique_code, user.confirm_key, user.reference_user_id,user.paypal_email,user.active,user.suspend_reason,user.profile_slug,user.user_type,user.mobile_number,user.company_name,user.abn_number,user.company_address,user.company_website,user.company_logo';

    if (in_array('user_notification', $join)) {
        $selectfields .= ',user_notification.id,user_notification.you_back_alert,user_notification.you_follow_alert,user_notification.new_pledge_alert,user_notification.new_comment_alert,user_notification.new_follow_alert,user_notification.new_updates_alert,user_notification.social_notification_alert,user_notification.creator_pledge_alert,user_notification.creator_comment_alert,user_notification.creator_follow_alert,user_notification.creator_newup_alert,user_notification.user_alert,user_notification.add_fund,user_notification.project_alert,user_notification.comment_alert,user_notification.update_alert,user_notification.comment_reply_alert';
        $cache_file_name = 'user_detail_notification' . $id;
    }

    if (in_array('user_property_detail', $join)) {
        $selectfields .= ',user_property_detail.user_property_type,user_property_detail.investment_suburbs,user_property_detail.min_investment_range,user_property_detail.max_investment_range,user_property_detail.user_property_id';
       
    }


    $data = false;
    if (!$CI->simple_cache->is_cached($cache_file_name)) {

        // not cached, do our things that need caching
        $CI->db->select($selectfields);
        $CI->db->from('user');
        if (in_array('user_notification', $join)) {
            $CI->db->join('user_notification', 'user.user_id=user_notification.user_id', 'left');
        }
        if (in_array('user_property_detail', $join)) {
            $CI->db->join('user_property_detail', 'user.user_id=user_property_detail.user_id', 'left');
        }
        if ($id > 0) {
            $CI->db->where('user.user_id', $id);
        }

        $query = $CI->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $data = $result;

        }

        // store in cache
        $CI->simple_cache->cache_item($cache_file_name, $data);
    } else {
        //$data =$CI->simple_cache->get_item($cache_file_name);
        $CI->db->select($selectfields);
        $CI->db->from('user');
        if (in_array('user_notification', $join)) {
            $CI->db->join('user_notification', 'user.user_id=user_notification.user_id', 'left');
        }
         if (in_array('user_property_detail', $join)) {
            $CI->db->join('user_property_detail', 'user.user_id=user_property_detail.user_id', 'left');
        }
        if ($id > 0) {
            $CI->db->where('user.user_id', $id);
        }

        $query = $CI->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $data = $result;

        }

        // store in cache
        $CI->simple_cache->cache_item($cache_file_name, $data);
    }


    return $data;
}


/*
Function name :GetOneProject()
Parameter : files
Return : secure file
Use : image uploading
*/
function GetOneProperty($id)
{
    $CI =& get_instance();

    $data = 0;
 
    $query = $CI->db->query("select * from `property` where property_id= '".$id."'");

    if ($query->num_rows() > 0) {
        $data = $query->row_array();
    }


    return $data;
}


/*
Function name :GetOneProject()
Parameter : files
Return : secure file
Use : image uploading
*/
function GetOneCampaign($campaign_id)
{
    $CI =& get_instance();

    $data = 0;
 
    $query = $CI->db->query("select * from `campaign` where campaign_id= '".$campaign_id."'");

    if ($query->num_rows() > 0) {
        $data = $query->row_array();
    }


    return $data;
}

/*
Function name :GetOneProject()
Parameter : files
Return : secure file
Use : image uploading
*/
function GetOnePropertyCampaign($campaign_id=0,$property_id=0)
{
    $CI =& get_instance();

    $selectfields = 'campaign.*';
   
    $selectfields .= ',property.property_id,property.property_address,property.property_url';
   
    $CI->db->select($selectfields);
    $CI->db->from('campaign');

    $CI->db->join('property', 'campaign.property_id=property.property_id','left');

    if($campaign_id > 0)
    {
        $CI->db->where('campaign.campaign_id', $campaign_id);
    } 
    if($property_id > 0)
    {
        $CI->db->where('property.property_id', $property_id);
    } 
    $query = $CI->db->get();
    if ($query->num_rows() > 0) 
    {
        return $query->row_array();
    } 
    else 
    {
        return 0;
    }

    return $data;
}


/*
Function name :validatestatus_isedit()
Parameter : is_status
Return : true for active
Use : getting status for project
*/
function validatestatus_isedit($is_status = '')
{
    $status = array(0, 1);
    if (in_array($is_status, $status)) {

        return false;
    } else {
        return true;
    }
}

/*
Function name :GetOneOutsideLink()
Parameter : id
Return : single outside link
Use : getting single outside link detail
*/

function GetOneOutsideLink($id = null)
{
    $CI =& get_instance();
    $project_cnt = $CI->db->get_where("outside_link", array("project_id" => $id));

    if ($project_cnt->num_rows() > 0) {
        return $project_cnt->row_array();
    }
    return 0;

}

/**
 * Function name :goToByScroll()
 * Parameter : null
 * Return :null
 * Use : move cursor to top
 * Description :
 * mover user to particular box position
 **/
function goToByScroll($id)
{
    "$('html,body').animate({scrollTop: $('#" . $id . "').offset().top},'slow')";
}

/*
Function name :paypal_pre_failure_email()
Parameter : donar_email,
Return : secure string
Use : all failure transaction email fire from here
*/
function paypal_failure_email($donar_email, $donar_name, $project_owner_email, $project_id, $project_title, $paypal_msg1 = '', $paypal_msg2 = '', $paypal_msg3 = '')
{


    $CI =& get_instance();
    //fetch email template
    $email_template = $CI->db->query("select * from `email_template` where task='New Fund Admin Notification'");
    $email_temp = $email_template->row();

    $email_address_from = $email_temp->from_address;
    $email_address_reply = $email_temp->reply_address;
    $email_subject = YOUR_PAYMENT_FAILED;
    $email_to = $donar_email;


    $str = $paypal_msg1;
    //send email to donor
    if ($paypal_msg1 != "") email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);

    //send email to project owner
    $email_to = $project_owner_email;
    $str = $paypal_msg2;
    if ($paypal_msg2 != "") email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);


    $email_subject = YOUR_PAYMENT_FAILED . " on Project" . $project_title . "(" . $project_id . ")";
    $email_to = $email_address_from;
    //send email to admin
    $str = $paypal_msg3;
    if ($paypal_msg3 != "") email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str);
}


/*
    Function name :get_one_country()
    Parameter : id
    Return : single country detail
    Use : getting single country detail
    */

function get_one_country($id)
{
    $CI =& get_instance();
    $query = $CI->db->query("select * from country where country_id='" . $id . "'");
    if ($query->num_rows() > 0) {
        return $query->row_array();
    }
    return 0;
}

/*Get user last login detail*/
/*
Function name :GetLastLogin()
Parameter : id
Return : user last login detail
Use : Get user last login detail
*/
function GetLastLogin($id)
{

    
    $CI =& get_instance();
    $last_login = $CI->db->get_where('user_login', array('user_id' => $id));

    if ($last_login->num_rows() > 1) {

        $CI->db->where(array('user_id' => $id));
        $CI->db->order_by('login_id', 'desc');
        $CI->db->limit(1, 1);
        $query = $CI->db->get('user_login');

        $get_last_login = $query->row_array();
    } else {
        $CI->db->where(array('user_id' => $id));
        $CI->db->order_by('login_id', 'desc');
        $query = $CI->db->get('user_login');

        $get_last_login = $query->row_array();
    }

    return $get_last_login;
}


function wallet_amount()
{
    $CI = get_instance();
    $user_id = get_authenticateUserID();
    $query = $CI->db->query("SELECT SUM( debit ) AS debit, SUM( credit ) AS credit
				FROM wallet
				WHERE user_id ='" . $user_id . "'");
    /*
    echo $CI->db->last_query();
    die();	*/
    if ($query->num_rows() > 0) {
        $result = $query->row();
        $debit = $result->debit;
        $credit = $result->credit;
        $total = $debit - $credit;
        return $total;
    } else {
        return 0;
    }
}

function learn_more_category($active = 0, $footer = '')
{

    $CI =& get_instance();


    $CI->db->select('*');
    $CI->db->where('learn_more_category.active', $active);
    if ($footer != '')
        $CI->db->where('learn_more_category.footer', $footer);

    if ($_SESSION['lang_code'] != '')
        $CI->db->where('iso2', $_SESSION['lang_code']);
    if ($_SESSION['lang_folder'] != '')
        $CI->db->where('language_folder', $_SESSION['lang_folder']);

    $CI->db->from('learn_more_category');
    $CI->db->join('language', 'language.language_id = learn_more_category.language_id', 'inner');

    $CI->db->order_by('category_name', 'asc');
    //$CI->db->group_by('learn_category');
    $query = $CI->db->get();//echo $CI->db->last_query();
    if ($query->num_rows() > 0) {
        $data = $query->result_array();
    } else {
        $data = 0;
    }

    return $data;


}

function learn_more_category_right_side($active = 0, $right_menu = '')
{

    $CI =& get_instance();


    $CI->db->select('*');
    $CI->db->where('learn_more_category.active', $active);
    if ($right_menu != '')
        $CI->db->where('learn_more_category.right_side', $right_menu);

    if ($_SESSION['lang_code'] != '')
        $CI->db->where('iso2', $_SESSION['lang_code']);
    if ($_SESSION['lang_folder'] != '')
        $CI->db->where('language_folder', $_SESSION['lang_folder']);

    $CI->db->from('learn_more_category');
    $CI->db->join('language', 'language.language_id = learn_more_category.language_id', 'inner');

    $CI->db->order_by('category_name', 'asc');
    //$CI->db->group_by('learn_category');

    $query = $CI->db->get();
    if ($query->num_rows() > 0) {
        $data = $query->result_array();
    } else {
        $data = 0;
    }

    return $data;


}


function learnmore_menu($active = 0)
{
    $CI =& get_instance();
    $CI->db->select('pages_id,pages_title,slug,learn_category');
    $CI->db->where('active', $active);
    $CI->db->from('learn_more');

    $CI->db->group_by('learn_category');
    $query = $CI->db->get();

    if ($query->num_rows() > 0) {
        return $query->result_array();
    } else {
        return 0;
    }
}

function pages_menu($active = 0, $footer = '')
{
    $CI =& get_instance();


    $CI->db->select('pages_id,pages_title,slug');
    $CI->db->where('pages.active', $active);
    $CI->db->where('footer_bar', $footer);

    if ($_SESSION['lang_code'] != '')
        $CI->db->where('iso2', $_SESSION['lang_code']);
    if ($_SESSION['lang_folder'] != '')
        $CI->db->where('language_folder', $_SESSION['lang_folder']);

    $CI->db->from('pages');

    $CI->db->join('language', 'language.language_id = pages.language_id', 'inner');
    $CI->db->order_by('pages_title', 'asc');

    $query = $CI->db->get();
    if ($query->num_rows() > 0) {
        $data = $query->result_array();
    } else {
        $data = 0;
    }
//echo '<pre>'; echo $CI->db->last_query(); die;
    return $data;


}

function pages_right_menu($active = 0, $footer = '')
{
    $CI =& get_instance();
    setting_deletecache('pages_right_menu');


    $CI->db->select('pages_id,pages_title,slug');
    $CI->db->where('pages.active', $active);
    $CI->db->where('right_side', $footer);
    $CI->db->from('pages');
    if ($_SESSION['lang_code'] != '')
        $CI->db->where('iso2', $_SESSION['lang_code']);
    if ($_SESSION['lang_folder'] != '')
        $CI->db->where('language_folder', $_SESSION['lang_folder']);
    $CI->db->join('language', 'language.language_id = pages.language_id', 'inner');

    $CI->db->order_by('pages_title', 'asc');

    $query = $CI->db->get();
    if ($query->num_rows() > 0) {
        $data = $query->result_array();
    } else {
        $data = 0;
    }

    return $data;


}


/*
Function name :status_name()
Parameter : status
Return : status name in place of numeric value
Use :
*/
function property_status($status)
{
    $array = array(0 => DRAFT,
        '1' => PENDING,
        '2' => ACTIVE,
        '3' => SUCCESSFUL,
        '4' => CONSOLE_SUCCESS,
        '5' => FAILURE,
        '6' => DECLINE,
        '7' => INACTIVE_VISIBLE,
        '8' => INACTIVE_HIDDEN
    );
    return $array[$status];
}

/*
Function name :status_name()
Parameter : status
Return : status name in place of numeric value
Use :
*/
function status_name($status)
{
    $array = array(0 => DRAFT,
        '1' => PENDING,
        '2' => ACTIVE,
        '3' => SUCCESSFUL,
        '4' => CONSOLE_SUCCESS,
        '5' => FAILURE,
        '6' => DECLINE,
        '7' => INACTIVE_VISIBLE,
        '8' => INACTIVE_HIDDEN
    );
    return $array[$status];
}

/*
Function name :comment_status_name()
Parameter : status
Return : status name in place of numeric value
Use :
*/
function comment_status_name($status)
{
    $array = array(0 => PENDING,
        '1' => APPROVED,
        '2' => DECLINE,
        '3' => SPAM
    );
    return $array[$status];
}


/*
Function name :first_and_last_day()
Parameter : date
Return : return week_first_day and week_last_day fo week

*/
function first_and_last_day()
{
    $date = date('Y-m-d');
    $first_day = get_first_day_of_week($date);
    $last_day = get_last_day_of_week($date);
    $str = date('D m/d', strtotime($first_day)) . ' - ' . date('D m/d', strtotime($last_day));

    return $str;
}

function my_total_week_project_count($uid)
{
    $CI =& get_instance();

    $date = date('Y-m-d');
    $first_day = get_first_day_of_week($date);
    $last_day = get_last_day_of_week($date);

    $query = $CI->db->query("select * from project where user_id='" . $uid . "' and DATE(date_added) >= '" . $first_day . "' and DATE(date_added) <= '" . $last_day . "'");

    return $query->num_rows();
}

/*
Function name :is_allow_contribute()
Parameter : user_id,status,endate
Return : return true or false

use: will check the donor to allow donate or not

*/
function is_allow_invest($user_id, $is_status, $endate,$campaign_user_type ='')
{

    $CI =& get_instance();

    // if ($user_id == $CI->session->userdata('user_id')) 
    // {
    //     return false;
    // }

    if($campaign_user_type == 1)
    {
         return true;
    }
    else
    {
        $status = array(0, 1, 3, 4, 5, 6);
        if (in_array($is_status, $status)) 
        {
            return false;
        }
        $start_date = time();
        $end_date = strtotime($endate);
        if ($start_date > $end_date) 
        {
            return false;
        }
    }

    return true;
}

function set_amount($amt = null)
{

    $CI =& get_instance();

    $site_setting = site_setting();

    $decimal_points = $site_setting['decimal_points'];

    $amt = number_format($amt, $decimal_points);

    return $amt;


}

function get_one_transaction($id)
{
    $CI =& get_instance();
    $query = $CI->db->get_where('transaction', array('transaction_id' => $id));
    if ($query->num_rows() > 0) {
        return $query->row_array();
    }
    return 0;
}

function get_payment_gateway($id = '')
{
    $CI =& get_instance();
    $query = $CI->db->get_where('gateways_details', array('payment_gateway_id' => $id));
    if ($query->num_rows() > 0) {
        return $query->result();
    }
    return 0;
}

function gateway_update($update_array = '', $id = '')
{
    $CI =& get_instance();
    $CI->db->where('id', $id);
    $CI->db->update('gateways_details', $update_array);
}

function delete_all_cache()
{
    $dir = scandir(base_path() . 'application/cache');
    // var_dump($dir);
    // die;
    foreach ($dir as $file) {
        if (!is_dir($file)) {
            //echo $file.'<br>';
            if (unlink(base_path() . 'application/cache/' . $file)) {
                //echo 'DELETED: '.$file.'<br>';
            }
        }
    }
    return 1;
}

function delete_cach()
{
    $dir = scandir(base_path() . 'application/cache');
    // var_dump($dir);
    // die;
    foreach ($dir as $file) {
        if (!is_dir($file)) {
            //echo $file.'<br>';
            if (unlink(base_path() . 'application/cache/' . $file)) {
                //echo 'DELETED: '.$file.'<br>';
            }
        }
    }
}

/*
Fetch username from user table
 *  */
function users_url_exists($url_username = '')
{
    $CI = &get_instance();
    $query = $CI->db->query("select user_name from user where user_name='" . $url_username . "'");
    if ($query->num_rows() > 0) {
        $data = $query->row_array();
        return $data['user_name'];
    }
    return '';
}

/*
Fetch slug from pages table
 *  */
function pages_url_exists($url_slug = '')
{
    $CI = &get_instance();
    $query = $CI->db->query("select slug from pages where slug='" . $url_slug . "'");
    if ($query->num_rows() > 0) {
        $data = $query->row_array();
        return $data['slug'];
    }
    return '';
}

/*
Fetch slug from learn_more table
 *  */
function learnmore_url_exists($url_slug = '')
{
    $CI = &get_instance();
    $query = $CI->db->query("select slug from learn_more where slug='" . $url_slug . "'");
    if ($query->num_rows() > 0) {
        $data = $query->row_array();
        return $data['slug'];
    }
    return '';
}


function getAllchildofcomment($id = '')
{
    $CI =& get_instance();
    //$query = $CI->db->get_where('comment',array('parent_id' => $id));
    $query = $CI->db->query("select user.*, comment.* from user inner join comment on user.user_id = comment.user_id where comment.parent_id ='" . $id . "'");
    //print_r($CI->db->last_query()); die;
    if ($query->num_rows() > 0) {
        return $query->result();
    }
    return 0;
}

/* this function is use for get the withdraw amount from wallet*/
function walletWithdrawamount()
{
    $userid = get_authenticateUserID();
    $CI =& get_instance();
    $query = $CI->db->query("select withdraw_amount from wallet_withdraw where user_id='" . $userid . "' order by withdraw_id DESC limit 1 ");

    if ($query->num_rows() > 0) {
        $submit_amount = $query->row();
        $amount = $submit_amount->withdraw_amount;
        return $amount;
    }
    return 0;

}

function get_learn_category_name($count = 'no', $category_id = '')
{
    $CI =& get_instance();
    //$query = $CI->db->query("select * from learn_more_category where active='1'");

    $CI->db->select('*');
    $CI->db->where('learn_more_category.active', '1');

    if ($_SESSION['lang_code'] != '')
        $CI->db->where('iso2', $_SESSION['lang_code']);
    if ($_SESSION['lang_folder'] != '')
        $CI->db->where('language_folder', $_SESSION['lang_folder']);

    $CI->db->from('learn_more_category');

    $CI->db->join('language', 'language.language_id = learn_more_category.language_id', 'inner');
    if($category_id != '')
    {
        $CI->db->where('learn_more_category.category_id', $category_id);
    }
    $CI->db->order_by('category_name', 'asc');

    $query = $CI->db->get();


    if ($count == 'yes') {
        return $query->num_rows();
    } else {
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return 0;
        }
    }


}

function get_title_learn_more($id = '')
{
    $CI =& get_instance();
    $query = $CI->db->query("select category_name from learn_more_category where category_id='" . $id . "'");

    if ($query->num_rows() > 0) {
        $query = $query->row();
        return $amount = $query->category_name;
    }
    return 0;

}

function inboxCount($id)
{
    $CI =& get_instance();
    $query = $CI->db->query("select * from message_conversation where  receiver_id ='" . $id . "' and is_read=0 and ( (admin_replay='admin' and (type=1 OR type=2) ) OR (type=0 and admin_replay!='admin') )");
    //echo  $CI->db->last_query(); die;
    if ($query->num_rows() > 0) {
        return $query->num_rows();
    }
    return 0;
}

function getMessageUnreadCount($message_id,$user_id){
     $CI =& get_instance();
    $query = $CI->db->query("select * from message_conversation where  (message_id ='" . $message_id . "' OR  reply_message_id ='" . $message_id . "' ) and is_read=0 and receiver_id='".$user_id."'  ");
    //echo  $CI->db->last_query(); die;
    if ($query->num_rows() > 0) {
        return $query->num_rows();
    }
    return 0;
}

///////////////// For activities added by ritu/////////////////
function add_activities($data = array(), $date = '')
{
    $CI =& get_instance();

    if ($data) {
        $activitydate = $data['activity_date'];
        $date1 = strtotime($activitydate);
        $date2 = strtotime($date);
        //echo $data['act'] .', ';

        if ($date1 == $date2) {
            $data['is_read'] = 0;

            $value = array(

                'act' => $data['act'],
                'activity_date' => $data['activity_date'],
                'user_id' => $data['key_id'],
                'user_name' => $data['user_name'],
                'is_read' => 0
            );

            $CI->db->insert('activities', $value);

        }

    }
}


function unread_activities($user_id = '')
{

    $CI =& get_instance();
    $query = $CI->db->get_where('user_activity', array('user_id' => $user_id, 'read' => 0));
    return $query->num_rows();

}

function read_activities($user_id = '')
{
    $CI =& get_instance();
    $CI->db->where('user_id', $user_id);
    $CI->db->update('user_activity', array('read' => 1));
}

function adminsendmsg($id)
{
    $CI =& get_instance();
    $query = $CI->db->query("select * from admin where  admin_id ='" . $id . "'");
    //echo  $CI->db->last_query(); die;
    if ($query->num_rows() > 0) {
        return $query->row();
    } else {
        return 0;
    }

}

function adminmsgCount()
{
    $CI =& get_instance();
    $query = $CI->db->query("select * from message_conversation where type in(1,2) and admin_replay!='admin' and message_content != '' and is_read=0");

    if ($query->num_rows() > 0) {
        return $query->num_rows();
    }
    return null;
}


function getAdminMessageUnreadCount($message_id){
     $CI =& get_instance();
    $query = $CI->db->query("select * from message_conversation where type in(1,2) and admin_replay!='admin' and is_read=0 and (message_id ='" . $message_id . "' OR  reply_message_id ='" . $message_id . "' )   ");
    //echo  $CI->db->last_query(); die;
    if ($query->num_rows() > 0) {
        return $query->num_rows();
    }
    return 0;
}

function activity_unread()
{
    $CI =& get_instance();
    $data_unread = array('read' => '0');
    $query = $CI->db->get_where('admin_activity', $data_unread);
    //echo  $CI->db->last_query(); die;
    if ($query->num_rows() > 0) {
        return $query->num_rows();
    }
    return null;
}

function get_font($id)
{
    $CI =& get_instance();
    $query = $CI->db->get_where('font_type', array('font_type_id' => $id));
    if ($query->num_rows() > 0) {
        return $query->row_array();
    }
    return 0;

}

/*
Function name :is_allow_request_access()
Parameter : user_id,type,status
Return : return data or 0

use: will check the login user to allow send request access

*/
function is_allow_request_access($user_id, $equity_id, $type)
{
    $CI =& get_instance();

    $status = array(0, 1, 2);

    $CI->db->where_in($type, $status);
    $CI->db->where('user_id', $user_id);
    $CI->db->where('equity_id', $equity_id);
    $query = $CI->db->get('access_request');

    if ($query->num_rows() > 0) {
        return $query->result_array();
    }
    return 0;
}

/*
Function name :investor_status()
Parameter :user_id ,equity_id;
Return : array
Use : check investor status ;
*/
function user_verfied_status($user_id = 0,$property_id = 0,$property_url)
{
      $CI =& get_instance();
    $site_setting = site_setting();
   
    
    
    if ($user_id > 0) 
    {
        $query = $CI->db->get_where('accreditation', array('user_id' => $user_id));

        $row = $query->row_array();
       

       if($query->num_rows() > 0 )
       {
            $accreditation_status = $row['accreditation_status'];



            if($accreditation_status == 1)
            {
                $button_text = 'Start A Campaign';
                $button_link =  anchor('property/start_campaign/'.$property_id, $button_text, array('class' => 'btn btn-primary btn-lg btn-block text-uppercase'));

            }
            else
            {
                $button_text = 'Verify User Status';
                $button_link =  anchor('accreditation/personal', $button_text, array('class' => 'btn btn-primary btn-lg btn-block text-uppercase'));

                
            }
        }
        else
        {
            $button_text = 'Verify User Status';
                $button_link =  anchor('accreditation/personal', $button_text, array('class' => 'btn btn-primary btn-lg btn-block text-uppercase'));
        }

    }
    else
    {
        $button_text = 'Start A Campaign';
       
        $return_url = base64_encode('properties/' . $property_url);
        
        $button_link = anchor('home/login/' . $return_url, $button_text, array('class' => 'btn btn-primary btn-lg btn-block text-uppercase'));

    }

    $ret_array = array('button_text' => $button_text, 'button_link' => $button_link);
    return $ret_array;


}

/*
Function name :investor_status()
Parameter :user_id ,equity_id;
Return : array
Use : check investor status ;
*/
function crowd_status($user_id = 0,$crowd_id = 0,$crowd_url='')
{
      $CI =& get_instance();
    $site_setting = site_setting();
   
    $common_text_for_status = '';
    
    if ($user_id > 0) 
    {
        $query = $CI->db->get_where('accreditation', array('user_id' => $user_id));

        $row = $query->row_array();
       

       if($query->num_rows() > 0 )
       {
            $accreditation_status = $row['accreditation_status'];



            if($accreditation_status == 1)
            {
                $query = $CI->db->get_where('crowd_request', array('user_id' => $user_id));

                $crowd_request_data = $query->row_array();


                if($query->num_rows() > 0 )
                {
                     $button_text= '';
                     $button_link = '';
                    if($crowd_request_data['status'] == 1)
                    {
                         $common_text_for_status = 'Your Request is still in pending';
                    }
                    else if($crowd_request_data['status'] == 2)
                    {
                         $button_text= 'Leave This Crowd';
                         $button_link = anchor('javascript://', $button_text, array('class' => 'btn btn-primary btn-small pull-right text-uppercase', 'onclick' => 'leave_crowd_request(' . $crowd_id . ');'));
                    }
                    else if($crowd_request_data['status'] == 3)
                    {
                          $common_text_for_status = 'Your request is rejected by crowd owner';   
                    }
                }
                else
                {
                    $button_text = 'join this crowd';
               
                    $button_link = anchor('javascript://', $button_text, array('class' => 'btn btn-primary btn-small pull-right text-uppercase', 'onclick' => 'crowd_request(' . $crowd_id . ');'));

                }


                

            }
            else
            {
                $button_text = 'Verify User Status';
                $button_link =  anchor('accreditation/personal', $button_text, array('class' => 'btn btn-primary btn-small pull-right text-uppercase'));

                
            }
        }
        else
        {
            $button_text = 'Verify User Status';
                $button_link =  anchor('accreditation/personal', $button_text, array('class' => 'btn btn-primary btn-small pull-right text-uppercase'));
        }

    }
    else
    {
        $button_text = 'join this crowd';
       
        $return_url = base64_encode('crowds/' . $crowd_url);
        
        $button_link = anchor('home/login/' . $return_url, $button_text, array('class' => 'btn btn-primary btn-small pull-right text-uppercase'));

    }

    $ret_array = array('button_text' => $button_text, 'button_link' => $button_link,'common_text_for_status' =>$common_text_for_status);
    return $ret_array;


}

/*
Function name :investor_status()
Parameter :user_id ,equity_id;
Return : array
Use : check investor status ;
*/
function investor_status($user_id = 0, $campaign_id = 0, $invest_process_id = '')
{
    $CI =& get_instance();
    $site_setting = site_setting();
   

}   

/*
Function name :investor_status()
Parameter :user_id ,equity_id;
Return : array
Use : check investor status ;
*/
function investor_status11($user_id = 0, $property_id = 0, $property_url = '')
{

    $CI =& get_instance();
    $site_setting = site_setting();
   
    
    
    if ($user_id > 0) {
        $query = $CI->db->get_where('property', array('property_id' => $equity_id));


        $row = $query->row_array();
        $equity_owner_id = $row['user_id'];
        $status = $row['status'];
        $endate = $row['end_date'];
        $start_date = time();
        $end_date = strtotime($endate);

        if ($status != 2 || $start_date > $end_date) 
        {


        } 
        else if ($equity_owner_id == $user_id) 
        {
            

        } else {

             

            if ($accrediation_manage == 'yes') 
            {

                $interest_status = 1;
                $button_text = VERIFY_INVESTOR_STATUS;

                $button_link = anchor('accreditation/personal', $button_text, array('class' => 'btn-contribute'));
                
            
            

                //check accreditation user status
                $query = $CI->db->get_where('accreditation', array('user_id' => $user_id));
                if ($query->num_rows() > 0) {
                    $row = $query->row_array();
                    $accreditation_status = $row['accreditation_status'];  //0=pending ,1=approve ,2-rejected
                   
                    if ($accreditation_status == 1) {
                        if ($intrest_status_update != 2) {
                            $interest_status = 2;
                            $button_text = I_AM_INTRESTED;
                            $button_link = anchor('javascript://', $button_text, array('class' => 'btn-contribute', 'onclick' => 'interest_request(' . $equity_id . ');'));
                            
                            $perk_button_href='javascript://';
                            $perk_button_class='';
                            $perk_button_onclick='interest_request(' . $equity_id . ');';
                            
                        } else {
                            $interest_status = 4;
                            $button_text = INVEST_NOW;
                            
                            $perk_attach=1;

                            $button_link = anchor('startinvest/index/' . $equity_id, $button_text, array('class' => 'btn-contribute'));
                            
                            
                            $perk_button_href=site_url('startinvest/index/' . $equity_id);
                            $perk_button_class='';
                            $perk_button_onclick='';
                            
                        }


                    }

                


                //check interest request status
                $query = $CI->db->get_where('interest_request', array( 'user_id' => $user_id, 'equity_id' => $equity_id));
                if ($query->num_rows() > 0) {
                    $row = $query->row_array();
                   $intrest_status_update = $row['status'];  //0=pending ,1=pending  ,2-approve ,3 - rejected
                    //we update status if user is accreditation and if rejected or pending then its redirect to accreditation status page
                    if ($intrest_status_update == 2) {
                        $interest_status = 4;
                        $button_text = INVEST_NOW;
                        
                        $perk_attach=1;

                        $button_link = anchor('startinvest/index/' . $equity_id, $button_text, array('class' => 'btn-contribute'));
                        
                        
                        
                        $perk_button_href=site_url('startinvest/index/' . $equity_id);
                        $perk_button_class='';
                        $perk_button_onclick='';
                        
                        
                        //check investment processs
                       // $query = $CI->db->get_where('equity_investment_process', array('user_id' => $user_id,'equity_id'=>$equity_id, 'status' => 0 ,'invest_status_id < 7' ));
                       
                        $CI->db->select('*');
                        $CI->db->from('equity_investment_process');
                        $CI->db->where('user_id', $user_id);
                        $CI->db->where('equity_id', $equity_id);
                        $CI->db->where('status', '0');
                        $CI->db->where('invest_status_id <', 7);
                        $query = $CI->db->get();

                        if ($query->num_rows() > 0) {
                            
                            $investment_process_result=$query->row();
                            
                            $investment_process_id=$investment_process_result->id;
                            $interest_status = 4;
                            $button_text = INVESTMENT_IN_PROCESS;
                            
                            $perk_attach=0;
                            
                            $button_link = anchor('investment/investor/' . $equity_id.'/'.$investment_process_id, $button_text, array('class' => 'btn-contribute'));
                            
                            
                            $perk_button_href=site_url('investment/investor/' . $equity_id.'/'.$investment_process_id);
                            $perk_button_class='';
                            $perk_button_onclick='';
                        
                            
                            
                        }
                    }


                    if ($intrest_status_update == 1) {
                        $interest_status = 3;  //process in pending ,you request is pending ,please contact owner(<Send message Link>)
                        $button_text = INVEST_NOW; ///diable button
                        $button_link = anchor('javascript://', $button_text, array('class' => 'btn-contribute disabled'));
                        $common_text_for_status = YOU_REQUEST_IS_STILL_PENDING;
                        $class_add_pending = 'interested-request';
                        
                        $perk_attach=0;
                        
                        $perk_button_href='javascript://';
                        $perk_button_class='disabled';
                        $perk_button_onclick='';
                        
                        
                    }

                    if ($intrest_status_update == 3) {
                        $interest_status = 5;  //You can not access this page,you request is reejcted by project owner,please contact him(<Send message Link>)
                        $button_text = INVEST_NOW;  ///diable button
                        
                        $perk_attach=0;
                        
                        $button_link = anchor('javascript://', $button_text, array('class' => 'btn-contribute disabled'));
                        $common_text_for_status = YOU_REQUEST_IS_REJECTED_BY_OWNER;
                        $class_add_pending = 'interested-request_rejected';
                        
                        
                        $perk_button_href='javascript://';
                        $perk_button_class='disabled';
                        $perk_button_onclick='';
                        
                    }
                }
            }


            } else {
               
                $interest_status = 1;
                $button_text = I_AM_INTRESTED;

                $button_link = anchor('javascript://', $button_text, array('class' => 'btn-contribute', 'onclick' => 'interest_request(' . $equity_id . ');'));
                
                $perk_attach=0;
                
                $perk_button_href='javascript://';
                $perk_button_class='';
                $perk_button_onclick='interest_request(' . $equity_id . ');';
                
                
                $intrest_status_update = '';
                $query = $CI->db->get_where('interest_request', array('user_id' => $user_id, 'equity_id' => $equity_id));
                if ($query->num_rows() > 0) {
                    $row = $query->row_array();
                    $intrest_status_update = $row['status'];  //0=pending ,1=approve ,2-rejected
                    //we update status if user is accreditation and if rejected or pending then its redirect to accreditation status page
                }

                if ($intrest_status_update != 2) {
                    $interest_status = 1;
                    $button_text = I_AM_INTRESTED;
                    $button_link = anchor('javascript://', $button_text, array('class' => 'btn-contribute', 'onclick' => 'interest_request(' . $equity_id . ');'));
                    $perk_attach=0;
                    $perk_button_href='javascript://';
                    $perk_button_class='';
                    $perk_button_onclick='interest_request(' . $equity_id . ');';
                
                    
                    
                } else {
                    $interest_status = 4;
                    $button_text = INVEST_NOW;
                    
                    $perk_attach=1;

                    $button_link = anchor('startinvest/index/' . $equity_id, $button_text, array('class' => 'btn-contribute'));
                    
                    
                    $perk_button_href=site_url('startinvest/index/' . $equity_id);
                    $perk_button_class='';
                    $perk_button_onclick='';
                
                    
                    
                }


                //check interest request status
                $query = $CI->db->get_where('interest_request', array('user_id' => $user_id, 'equity_id' => $equity_id));
                if ($query->num_rows() > 0) {
                    $row = $query->row_array();

                    $intrest_status_update = $row['status'];  //0=pending ,1=approve ,2-rejected
                    //we update status if user is accreditation and if rejected or pending then its redirect to accreditation status page
                    if ($intrest_status_update == 2) {
                        $interest_status = 4;
                        $button_text = INVEST_NOW;
                        
                        $perk_attach=1;

                        $button_link = anchor('startinvest/index/' . $equity_id, $button_text, array('class' => 'btn-contribute'));
                        
                        
                        
                        
                        $perk_button_href=site_url('startinvest/index/' . $equity_id);
                        $perk_button_class='';
                        $perk_button_onclick='';
                        
                        
                        //check investment processs
                       // $query = $CI->db->get_where('equity_investment_process', array('user_id' => $user_id, 'equity_id' => $equity_id, 'status' => '0'));
                       
                        $CI->db->select('*');
                        $CI->db->from('equity_investment_process');
                        $CI->db->where('user_id', $user_id);
                        $CI->db->where('equity_id', $equity_id);
                        $CI->db->where('status', '0');
                        $CI->db->where('invest_status_id <', 7);
                        $query = $CI->db->get();

                        if ($query->num_rows() > 0) {
                            
                            $investment_process_result=$query->row();
                            
                            $investment_process_id=$investment_process_result->id;
                            
                            $interest_status = 4;
                            $button_text = INVESTMENT_IN_PROCESS;
                            
                            $perk_attach=0;
                            
                            $button_link = anchor('investment/investor/' . $equity_id.'/'.$investment_process_id, $button_text, array('class' => 'btn-contribute'));
                            
                            $perk_button_href=site_url('investment/investor/' . $equity_id.'/'.$investment_process_id);
                            $perk_button_class='';
                            $perk_button_onclick='';
                            
                            
                            
                        }
                    }


                    if ($intrest_status_update == 1) {
                        $interest_status = 3;  //process in pending ,you request is pending ,please contact owner(<Send message Link>)
                        $button_text = INVEST_NOW; ///diable button
                        $button_link = anchor('javascript://', $button_text, array('class' => 'btn-contribute disabled'));
                        $common_text_for_status = YOU_REQUEST_IS_STILL_PENDING;
                        
                        $perk_attach=0;
                        
                        $class_add_pending = 'interested-request';
                        
                        $perk_button_href='javascript://';
                        $perk_button_class='disabled';
                        $perk_button_onclick='';
                        
                        
                    }

                    if ($intrest_status_update == 3) {
                        $interest_status = 5;  //You can not access this page,you request is reejcted by project owner,please contact him(<Send message Link>)
                        $button_text = INVEST_NOW;  ///diable button
                        
                        $perk_attach=0;
                        
                        $button_link = anchor('javascript://', $button_text, array('class' => 'btn-contribute disabled'));
                        $common_text_for_status = YOU_REQUEST_IS_REJECTED_BY_OWNER;
                        $class_add_pending = 'interested-request_rejected';
                        
                        
                        $perk_button_href='javascript://';
                        $perk_button_class='disabled';
                        $perk_button_onclick='';
                        

                    }
                }
            }
        }


    } else {
        $return_url = base64_encode($project_url.'/' . $equity_url);
        $button_text = INVEST_NOW;
        
        $perk_attach=0;
        
        $button_link = anchor('home/login/' . $return_url, $button_text, array('class' => 'btn-contribute'));
        $common_text_for_status = THIS_COMPANY_MAY_BE_INTERESTED;
        $interest_status = 0; //0==incomplete profile 1=not accreditation user ,2=verify accreditation user  ,3-interest request pending ,4=interest request approve ,5 =interest request rejected
        
        
        $perk_button_href=site_url('home/login/' . $return_url);
        $perk_button_class='';
        $perk_button_onclick='';
        
    }
    $ret_array = array('button_text' => $button_text, 'button_link' => $button_link, 'common_text_for_status' => $common_text_for_status, 'interest_status' => $interest_status, 'class_add_pending' => $class_add_pending,'investment_process_id'=>$investment_process_id,'perk_attach'=>$perk_attach,'perk_button_href'=>$perk_button_href,'perk_button_class'=>$perk_button_class,'perk_button_onclick'=>$perk_button_onclick);
    return $ret_array;

}

/**
 * @param $type
 * @return mixed
 */
function getCategoryTitle($type,$search_msg='',$status='',$category_id_search ='')
{
    $CI =& get_instance();
    //$query = $CI->db->get_where('comment',array('parent_id' => $id));
    //$query = $CI->db->query("select * from project_category where project_category.url_category_title ='" . $type . "'");
    //print_r($CI->db->last_query()); die;
    $CI->db->select('*');
    $CI->db->from('categories');
    if ($_SESSION['lang_code'] != '')
            $CI->db->where('iso2', $_SESSION['lang_code']);
        if ($_SESSION['lang_folder'] != '')
            $CI->db->where('language_folder', $_SESSION['lang_folder']);
         $CI->db->join('language', 'language.language_id = categories.language_id', 'inner');
    if($type != '')
    {
        $CI->db->where('categories.url', $type);
    }
    if($category_id_search > 0)
    {
        $CI->db->where('categories.id', $category_id_search);
    }
    if($status != '')
    {
         $CI->db->where('categories.active', $status);
    }    
    $query = $CI->db->get();
   
    if ($query->num_rows() > 0) {

        return $result = $query->result_array();
    } else {
        //return str_replace("_", " ", $type);
        return $search_msg;
    }

}


function GetDealTypeName($deal_name = '')
{
    $CI =& get_instance();
    $query = $CI->db->query("select deal_type_name,deal_type_icon from deal_type_setting where slug='" . $deal_name . "'");

    if ($query->num_rows() > 0) {
        return $query->row_array();
    }
    return 0;

}

/*
	Function name :GetAccessRequest()
	Parameter :user_id,GetAccessRequest,joinarr,limit,order,offset,count,is_interest_request_status
	Return : Return all access request
	Use : Fetch all request from detail page
	
			
	*/

function GetPendingRequest($access_request_id = 0, $equity_id = 0)
{
    $CI =& get_instance();
    $CI->db->select('*');

    $CI->db->from('access_request');

    $CI->db->where('access_request_id', $access_request_id);
    $CI->db->where('equity_id', $equity_id);
    $CI->db->where('comments in (1)');
    $CI->db->where('updates in (1)');
    $CI->db->where('funders in (1)');
    $CI->db->where('docs_media in (1)');

    $query = $CI->db->get();
    //echo $CI->db->last_query();die;

    return $query->num_rows();

}

function CheckValue($value = '')
{
    if ($value != '') return $value;
    else return NA;
}

function addhttp($url) {
    if($url != '')
    {
        return (substr($url, 0, 7) == 'http://' || substr($url, 0, 8) == 'https://')
        ? $url 
        : 'http://'.$url;
    }
    else
    {
        return '';
    }
}


function GetInvestorStatus($user_id = '',$equity_id='')
{
    $CI =& get_instance();
    $query = $CI->db->query("select invest_status_id from equity_investment_process where user_id='" . $user_id . "' and equity_id = '".$equity_id."'");

    if ($query->num_rows() > 0) {
        return $query->row_array();
    }
    return 0;

}
function check_is_company_follower($company_id, $company_follow_user_id)
{
    $CI =& get_instance();
    $query = $CI->db->query('select * from company_follower where company_id=' . $company_id . ' and company_follow_user_id=' . $company_follow_user_id);
    if ($query->num_rows() > 0) {
        return 1;
    } else {
        return 0;
    }

}

function checkAcredated($equity=array(),$site_setting=array()){

    if(!$equity || !$site_setting) return false;
    
    $accrediation_manage = false;        
    if($site_setting['accredential_status'] == 0 && $site_setting['accrediated_manage'] == 0)
    {
        $accrediation_manage = true;
    }
    else if($site_setting['accredential_status'] == 0 && $site_setting['accrediated_manage'] == 1)
    {
        if($equity['allowed_investor'] == 'yes')
        {   
            $accrediation_manage = true;
        }
        else
        {
            $accrediation_manage = false;
        }
    }
    return $accrediation_manage;
}

// create slug
function my_str_split($string)
   {
      $slen=strlen($string);
      for($i=0; $i<$slen; $i++)
      {
         $sArray[$i]=$string{$i};
      }
      return $sArray;
   }

   function noDiacritics($string)
   {
      //cyrylic transcription
      $cyrylicFrom = array('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я', 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я');
      $cyrylicTo   = array('A', 'B', 'W', 'G', 'D', 'Ie', 'Io', 'Z', 'Z', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'Ch', 'C', 'Tch', 'Sh', 'Shtch', '', 'Y', '', 'E', 'Iu', 'Ia', 'a', 'b', 'w', 'g', 'd', 'ie', 'io', 'z', 'z', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'ch', 'c', 'tch', 'sh', 'shtch', '', 'y', '', 'e', 'iu', 'ia'); 
 
      
      $from = array("Á", "À", "Â", "Ä", "Ă", "Ā", "Ã", "Å", "Ą", "Æ", "Ć", "Ċ", "Ĉ", "Č", "Ç", "Ď", "Đ", "Ð", "É", "È", "Ė", "Ê", "Ë", "Ě", "Ē", "Ę", "Ə", "Ġ", "Ĝ", "Ğ", "Ģ", "á", "à", "â", "ä", "ă", "ā", "ã", "å", "ą", "æ", "ć", "ċ", "ĉ", "č", "ç", "ď", "đ", "ð", "é", "è", "ė", "ê", "ë", "ě", "ē", "ę", "ə", "ġ", "ĝ", "ğ", "ģ", "Ĥ", "Ħ", "I", "Í", "Ì", "İ", "Î", "Ï", "Ī", "Į", "Ĳ", "Ĵ", "Ķ", "Ļ", "Ł", "Ń", "Ň", "Ñ", "Ņ", "Ó", "Ò", "Ô", "Ö", "Õ", "Ő", "Ø", "Ơ", "Œ", "ĥ", "ħ", "ı", "í", "ì", "i", "î", "ï", "ī", "į", "ĳ", "ĵ", "ķ", "ļ", "ł", "ń", "ň", "ñ", "ņ", "ó", "ò", "ô", "ö", "õ", "ő", "ø", "ơ", "œ", "Ŕ", "Ř", "Ś", "Ŝ", "Š", "Ş", "Ť", "Ţ", "Þ", "Ú", "Ù", "Û", "Ü", "Ŭ", "Ū", "Ů", "Ų", "Ű", "Ư", "Ŵ", "Ý", "Ŷ", "Ÿ", "Ź", "Ż", "Ž", "ŕ", "ř", "ś", "ŝ", "š", "ş", "ß", "ť", "ţ", "þ", "ú", "ù", "û", "ü", "ŭ", "ū", "ů", "ų", "ű", "ư", "ŵ", "ý", "ŷ", "ÿ", "ź", "ż", "ž");
      $to   = array("A", "A", "A", "A", "A", "A", "A", "A", "A", "AE", "C", "C", "C", "C", "C", "D", "D", "D", "E", "E", "E", "E", "E", "E", "E", "E", "G", "G", "G", "G", "G", "a", "a", "a", "a", "a", "a", "a", "a", "a", "ae", "c", "c", "c", "c", "c", "d", "d", "d", "e", "e", "e", "e", "e", "e", "e", "e", "g", "g", "g", "g", "g", "H", "H", "I", "I", "I", "I", "I", "I", "I", "I", "IJ", "J", "K", "L", "L", "N", "N", "N", "N", "O", "O", "O", "O", "O", "O", "O", "O", "CE", "h", "h", "i", "i", "i", "i", "i", "i", "i", "i", "ij", "j", "k", "l", "l", "n", "n", "n", "n", "o", "o", "o", "o", "o", "o", "o", "o", "o", "R", "R", "S", "S", "S", "S", "T", "T", "T", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "W", "Y", "Y", "Y", "Z", "Z", "Z", "r", "r", "s", "s", "s", "s", "B", "t", "t", "b", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "w", "y", "y", "y", "z", "z", "z");
      
      
      $from = array_merge($from, $cyrylicFrom);
      $to   = array_merge($to, $cyrylicTo);
      
      $newstring=str_replace($from, $to, $string);   
      return $newstring;
   }

   function makeSlugs($string, $maxlen=0)
   {
      $newStringTab=array();
      $string=strtolower(noDiacritics($string));
      if(function_exists('str_split'))
      {
         $stringTab=str_split($string);
      }
      else
      {
         $stringTab=my_str_split($string);
      }

      $numbers=array("0","1","2","3","4","5","6","7","8","9","-");
      //$numbers=array("0","1","2","3","4","5","6","7","8","9");

      foreach($stringTab as $letter)
      {
         if(in_array($letter, range("a", "z")) || in_array($letter, $numbers))
         {
            $newStringTab[]=$letter;
            //print($letter);
         }
         elseif($letter==" ")
         {
            $newStringTab[]="-";
         }
      }

      if(count($newStringTab))
      {
         $newString=implode($newStringTab);
         if($maxlen>0)
         {
            $newString=substr($newString, 0, $maxlen);
         }
         
         $newString = removeDuplicates('--', '-', $newString);
      }
      else
      {
         $newString='';
      }      
      
      return $newString;
   }
   
   
   function checkSlug($sSlug)
   {
      if(ereg ("^[a-zA-Z0-9]+[a-zA-Z0-9\_\-]*$", $sSlug))
      {
         return true;
      }
      
      return false;
   }
   
   function removeDuplicates($sSearch, $sReplace, $sSubject)
   {
      $i=0;
      do{
      
         $sSubject=str_replace($sSearch, $sReplace, $sSubject);         
         $pos=strpos($sSubject, $sSearch);
         
         $i++;
         if($i>100)
         {
            die('removeDuplicates() loop error');
         }
         
      }while($pos!==false);
      
      return $sSubject;
   }
    function UserTypeExists($user_type,$user_id='')
   {
        $CI =& get_instance();
        $check_user_id = $CI->session->userdata('user_id');
        if($user_id != '')
        {
            $check_user_id = $user_id;
        }
        $query = $CI->db->query("select user_type from user where user_id='" . $check_user_id . "'");

        if ($query->num_rows() > 0) 
        {
            $row_data = $query->row_array();
            $user_type_array = json_decode($row_data['user_type']);

            if(is_array($user_type_array))
            {
                if(in_array($user_type,$user_type_array))
                {

                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                return 0;
            }
        }
        else
        {
            return 0;
        }
        

   }

   function PropertyTypeExists($user_property_type,$user_id = '')
   {
        $CI =& get_instance();
        $check_user_id = $CI->session->userdata('user_id');
        if($user_id != '')
        {
            $check_user_id = $user_id;
        }
        $query = $CI->db->query("select user_property_type from user_property_detail where user_id='" . $check_user_id . "'");

        if ($query->num_rows() > 0) 
        {
            $row_data = $query->row_array();
            $user_type_array = json_decode($row_data['user_property_type']);

            if(is_array($user_type_array) && in_array($user_property_type,$user_type_array))
            {

                return 1;
            }
            else
            {
                return 0;
            }
        }
        else
        {
            return 0;
        }
        

   }

   function GetAllSuburb($id='')
    {

    $CI =& get_instance();


    if (!$CI->simple_cache->is_cached('suburb')) {

        // not cached, do our things that need caching
        if($id>0)
        {
            $query = $CI->db->query("select * from postcodes_geo where id = ".$id." and status = 1");
        }
        else
        {
            $query = $CI->db->query("select * from postcodes_geo where status = 1");
        }
        $data = $query->result_array();
        // store in cache
        $CI->simple_cache->cache_item('suburb', $data);
    } else {
        //$data =$CI->simple_cache->get_item('site_setting');
         if($id>0)
        {
            $query = $CI->db->query("select * from postcodes_geo where id = ".$id." and status = 1");
        }
        else
        {
            $query = $CI->db->query("select * from postcodes_geo where status = 1");
        }
        $data = $query->result_array();
        // store in cache
        $CI->simple_cache->cache_item('suburb', $data);
    }

    return $data;

}


   function GetSuburblist($id='')
    {

    $CI =& get_instance();


    if (!$CI->simple_cache->is_cached('suburb')) {

        // not cached, do our things that need caching
        if($id>0)
        {
            $query = $CI->db->query("select id,suburb from postcodes_geo where id = ".$id." and status = 1");
        }
        else
        {
            $query = $CI->db->query("select id,suburb from postcodes_geo where status = 1");
        }
        $data = $query->result_array();
        // store in cache
        $CI->simple_cache->cache_item('suburb', $data);
    } else {

       
        //$data =$CI->simple_cache->get_item('site_setting');
         if($id>0)
        {
            $query = $CI->db->query("select id,suburb from postcodes_geo where id = ".$id." and status = 1");
        }
        else
        {
            $query = $CI->db->query("select id,suburb from postcodes_geo where status = 1");
        }
        $data = $query->result_array();
        // store in cache
        $CI->simple_cache->cache_item('suburb', $data);
    }

    return $data;

}


function GetPropertyTypeName($property_type_id = '')
{
    $CI =& get_instance();
    $query = $CI->db->query("select name from property_type where id='" . $property_type_id . "'");

    if ($query->num_rows() > 0) {
        return $query->row_array();
    }
    return 0;

}
function CheckUserType($user_type,$user_id)
   {
        $CI =& get_instance();
      
        $query = $CI->db->query("select user_type from user where user_id='" . $user_id . "'");

        if ($query->num_rows() > 0) 
        {
            $row_data = $query->row_array();
            $user_type_array = json_decode($row_data['user_type']);

            if(is_array($user_type_array))
            {
                if(array_intersect($user_type,$user_type_array))
                {

                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                return 0;
            }
        }
        else
        {
            return 0;
        }
   }
   function checkNum($num)
   {
    return ($num%2) ? TRUE : FALSE;

    }

    function addToFiles($url)
    {

     
        $filename = '';
        $rand = rand(0, 100000);
        $CI =& get_instance();

        $base_path = $CI->config->slash_item('base_path');

        $file = fopen($url,"rb");
        $directory = $base_path . "upload/orig/";
        $valid_exts = array("php","jpeg","gif","png","doc","docx","jpg","html","asp","xml","JPEG","bmp"); 
        
        $test_ext = explode(".",strtolower(basename($url)));
        $ext = end($test_ext);
        if(in_array($ext,$valid_exts))
        {
           
            $date = new DateTime();
            $filename = $rand .'-property-'.$date->getTimestamp().'.'.$ext;


            $newfile = fopen($directory . $filename, "wb");
            if($newfile)
            {
                while(!feof($file))
                {
                    fwrite($newfile,fread($file,1024 * 8),1024 * 8);
                }
               
                return $filename;
            }
            else
            {
                return false;
            }
        }
        else
        {
           return false;
        }

    }
    function GalleryaddToFiles($url)
    {

     
        $filename = '';
        $rand = rand(0, 100000);
        $CI =& get_instance();

        $base_path = $CI->config->slash_item('base_path');

        $file = fopen($url,"rb");
        $directory = $base_path . "upload/gallery/";
        $valid_exts = array("php","jpeg","gif","png","doc","docx","jpg","html","asp","xml","JPEG","bmp"); 
        $test_ext = explode(".",strtolower(basename($url)));
        $ext = end($test_ext);
        if(in_array($ext,$valid_exts))
        {
           
            $date = new DateTime();
            $filename = $rand .'-image-'.$date->getTimestamp().'.'.$ext;


            $newfile = fopen($directory . $filename, "wb");
            if($newfile)
            {
                while(!feof($file))
                {
                    fwrite($newfile,fread($file,1024 * 8),1024 * 8);
                }
               
                return $filename;
            }
            else
            {
                return false;
            }
        }
        else
        {
           return false;
        }

    }

function GetPropertyInvestmentRange($property_id = '')
{
    $CI =& get_instance();
    $query = $CI->db->query("select estimate,lowEstimate,highEstimate from property_investment where property_id='" . $property_id . "' order by valuationDate desc");

    if ($query->num_rows() > 0) {
        return $query->row_array();
    }
    return 0;

}

   function GetAllProperties( $property_id = '', $joinarr = array('user'))
    {
        $CI =& get_instance();
        //default Equity fields
        $selectfields = 'property.*';
        //if require to join user
        if (in_array('user', $joinarr)) {
            $selectfields .= ',user.user_id,user.user_name,user.last_name,user.address, user.fb_uid, user.facebook_wall_post, user.fb_access_token,user.image,user.email,user.user_website,user.profile_slug,user.user_occupation,user.user_type,user.company_name';
        }
        if (in_array('campaign', $joinarr)) {
            $selectfields .= ',campaign.campaign_id,campaign.campaign_user_type,campaign.min_investment_amount,campaign.campaign_type,campaign.campaign_units,campaign.investment_close_date,campaign.campaign_document,campaign.investment_amount_type,campaign.campaign_units_get,campaign.date_added,campaign.investment_close_date,campaign.lead_investor_id,campaign.amount_get,campaign.status as campaign_status,campaign.user_id as campaign_user_id';
        }

        $CI->db->select($selectfields);
        $CI->db->from('property');


        //if require to join user
        if (in_array('user', $joinarr)) {
            $CI->db->join('user', 'property.user_id=user.user_id','left');

        }
      
        if (in_array('campaign', $joinarr)) {
            
            $CI->db->join('campaign', 'property.property_id=campaign.property_id','left');
           
        }
       
        

        //check property id
        if ($property_id > 0 or $property_id != '') {
            if (is_numeric($property_id)) {
                $CI->db->where('property.property_id', $property_id);
                $CI->db->or_where('property.property_url', $property_id);
            } else {
                $CI->db->where('property.property_url', $property_id);
            }
        }

        $query = $CI->db->get();
      
       
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else {
                return 0;
            }
       
    }
  function GetAllCrowdMember($crowd_id='',$user_id='', $joinarr = array('user'), $limit = 100, $order = array('id' => 'asc'),$crowd_user_id='')
    {
        $CI =& get_instance();

        //default Equity fields
        $selectfields = 'crowd_invite_request.*';
        //if require to join user
        if (in_array('user', $joinarr)) {
            $selectfields .= ',user.user_id,user.user_name,user.last_name,user.address, user.fb_uid, user.facebook_wall_post, user.fb_access_token,user.image,user.email,user.user_website,user.profile_slug,user.user_occupation,user.user_type';
        }


        $CI->db->select($selectfields);
        $CI->db->from('crowd_invite_request');


        //if require to join user
        if (in_array('user', $joinarr)) 
        {
            $CI->db->join('user', 'crowd_invite_request.user_id=user.user_id');

        }
        $CI->db->join('crowd_request', 'crowd_invite_request.crowd_id=crowd_request.crowd_id','inner');
    
        if($user_id != '' && $user_id > 0)
        {
            $CI->db->where('crowd_invite_request.user_id',$user_id);
            $CI->db->or_where('crowd_invite_request.invite_user_id',$user_id);
        }
        if($crowd_user_id != '' && $crowd_user_id > 0)
        {
            $CI->db->where('crowd_invite_request.user_id',$crowd_user_id);
            $CI->db->or_where('crowd_invite_request.invite_user_id',$crowd_user_id);
        }

         $CI->db->where('crowd_request.status',2);
      
        if (is_array($order)) 
        {
            foreach ($order as $key => $val) 
            {
                $CI->db->order_by($key, $val);
            }
        }

       
        if ($limit > 0) $CI->db->limit($limit);

        $query = $CI->db->get();
       // echo $CI->db->last_query();
      
        if ($query->num_rows() > 0) 
        {
            
            return $query->result_array();
        } 
        else 
        {
            return 0;
        }
        

    }

function CheckCrowdMember($crowd_id = '',$user_id='')
{
    $CI =& get_instance();
    $query = $CI->db->query("select email from crowd_invite_request where crowd_id='" . $crowd_id . "' and user_id = '".$user_id."'");

    if ($query->num_rows() > 0) {
       return 1;
    }
    return 0;

}

function PropertyComments($user_id='',$property_comment_id='',$join = array('comment'),$limit = 10, $group = array('user_id'),$order = array('user_id' => 'desc'),$comment_status = '', $offset = 0, $count = 'no', $comment_id_for_parent = '0',$array_return='no',$type='')
{
     $CI =& get_instance();
     $selectfields = 'property.property_address, property.property_url,property.cover_image,property.property_id';
        //if require to join comment
        if (in_array('comment', $join)) {
            $selectfields .= ',comment.comment_id,comment.comments,comment.date_added,comment.user_id,comment.comment_ip,comment.status,comment.comment_type,comment.parent_id';
        }

        if (in_array('campaign', $join)) {
            $selectfields .= ',campaign.campaign_id';
        }


         $CI->db->select($selectfields);

        $CI->db->from('property');

        if (in_array('comment', $join)) {
            $CI->db->join('comment', 'property.property_id=comment.property_id');
        }
        if (in_array('campaign', $join)) {
            $CI->db->join('campaign', 'property.property_id=campaign.property_id','left');
             

        }


        if (in_array('comment', $join)) {
            if ($comment_status == 1) {
                $CI->db->where('comment.status in (' . $comment_status . ')');

            }
            $CI->db->where("comment.parent_id = '0' ");
        }


        if ($user_id > 0) {
            $CI->db->where('comment.user_id', $user_id);
        }
        if ($property_comment_id > 0) {
            $CI->db->where('comment.comment_id', $property_comment_id);
        }

       

      
         if (is_array($group)) {
            foreach ($group as $key => $grp) {
                $CI->db->group_by($grp);
            }
        }


        if (is_array($order)) {
            foreach ($order as $key => $val) {
                $CI->db->order_by($key, $val);
            }
        }
        if ($limit > 0 || $offset > 0) $CI->db->limit($limit, $offset);

        $query = $CI->db->get();

        if ($count == 'yes') {
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                if($array_return == 'yes')
                {
                    if($type == 'property')
                    {
                        $CI->db->select('property_id');
                        $CI->db->from('campaign');
                        $query1 = $CI->db->get();
                        $property_array = $query1->result_array();
                        $main_array = $query->result_array();
                        foreach ($main_array as $key => $main) {
                            # code...
                              if($main['campaign_id'] != '') {
                                unset($main_array[$key]);
                            }
                        }
                        return $main_array;
                     

                    }

                    else if($type == 'campaign')
                    {
                        $CI->db->select('property_id');
                        $CI->db->from('campaign');
                        $query1 = $CI->db->get();
                        $property_array = $query1->result_array();
                        $main_array = $query->result_array();
                        foreach ($main_array as $key => $main) {
                            # code...
                              if($main['campaign_id'] == '') {
                                unset($main_array[$key]);
                            }
                        }
                        return $main_array;
                     

                    }
                    else
                    {

                        return $query->result_array();
                    }
                }
                else
                { 
                    return $query->row_array();
                }
            } else {
                return 0;
            }
        }



}





function CrowdComments($user_id='',$crowd_comment_id='',$join = array('crowd_comment'),$limit = 10, $group = array('user_id'),$order = array('user_id' => 'desc'),$offset = 0, $count = 'no',$array_return='no' )
{
     $CI =& get_instance();
     $selectfields = 'crowd.title, crowd.crowd_url';
        //if require to join comment
        if (in_array('crowd_comment', $join)) {
            $selectfields .= ',crowd_comment.id,crowd_comment.comment,crowd_comment.date_added,crowd_comment.user_id,crowd_comment.image as comment_image,crowd_comment.property_id as suggest_property_id,crowd_comment.crowd_id';
        }
         $CI->db->select($selectfields);

            $CI->db->from('crowd');

        if (in_array('crowd_comment', $join)) {
            $CI->db->join('crowd_comment', 'crowd.crowd_id=crowd_comment.crowd_id');
        }
         if ($user_id > 0) {
            $CI->db->where('crowd_comment.user_id', $user_id);
        }
        if ($crowd_comment_id > 0) {
            $CI->db->where('crowd_comment.id', $crowd_comment_id);
        }
        if (is_array($group)) 
        {
            foreach ($group as $key => $grp) {
                $CI->db->group_by($grp);
            }
        }


        if (is_array($order)) {
            foreach ($order as $key => $val) {
                $CI->db->order_by($key, $val);
            }
        }
        if ($limit > 0 || $offset > 0) $CI->db->limit($limit, $offset);

        $query = $CI->db->get();

        if ($count == 'yes') {
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                if($array_return == 'yes')
                {
                    return $query->result_array();
                }
                else
                { 
                    return $query->row_array();
                }
            } else {
                return 0;
            }
        }

        


}

function ManageInvestmentSteps($campaign_id=0,$user_id=0,$check_status='')
{

    $CI =& get_instance();

    $CI->db->select('*');

    $CI->db->from('investment_process');

    if ($user_id > 0)
    {
        $CI->db->where('investment_process.user_id', $user_id);
    }
    if ($campaign_id > 0) {
        $CI->db->where('investment_process.campaign_id', $campaign_id);
    }
    if($check_status == 'yes')
    {
        $CI->db->where('investment_process.status', 0);
    }
    
    $query = $CI->db->get();

    if ($query->num_rows() > 0)
    {      
       return  $query->row_array();
    }
    else
    {
        return 0;
    }
   
           
}
  /*
    Function name :GetAllFollower()
    Use : Fetch all followers for particular project or equity or user

    */
    /**
     * @param string $is_follower_type
     * @param int $project_id
     * @param int $user_id
     * @param array $join
     * @param int $limit
     * @param array $order
     * @param string $cnt_rec
     * @return int
     */
    function GetAllFollower($is_follower_type = '', $user_id = 0, $join = array('user'), $limit = 10, $order = array('project_follow_id' => 'desc'), $cnt_rec = '')
    {
        $CI =& get_instance();

        if ($is_follower_type == 'user_follow') {
            $selectfields = 'user_follow.follower_id, user_follow.follow_user_id, user_follow.follow_by_user_id, user_follow.user_follow_date';
        }

        if (in_array('user_property_detail', $join)) 
        {
            $selectfields .= ',user_property_detail.*';
        }

        //if require to join category


        if (in_array('user', $join)) {
            $selectfields .= ',user.user_id,user.user_name,user.last_name,user.address,user.image,user.profile_slug';
        }

         if (in_array('user_property_detail', $join)) 
        {
            $this->db->join('user_property_detail', 'user.user_id=user_property_detail.user_id','left');
        }

        $CI->db->select($selectfields);

        
        $CI->db->from('user_follow');
       

        if (in_array('user', $join)) {
           
            $CI->db->join('user', 'user_follow.follow_by_user_id=user.user_id');
            
        }

       

        if ($user_id > 0) {
            $CI->db->where('user_follow.follow_user_id', $user_id);
        }

        if (is_array($order)) {
            foreach ($order as $key => $val) {
                $CI->db->order_by($key, $val);
            }
        }
        $CI->db->limit($limit);

        $query = $CI->db->get();

        if ($query->num_rows() > 0) {
            if ($cnt_rec == "count_record") {
                return $query->num_rows();
            } else {
                return $query->result_array();
            }
      
        } else {
            return 0;
        }

    }

?>
