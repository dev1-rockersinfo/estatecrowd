<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


function site_setting()
{


    $CI =& get_instance();

    if (!$CI->simple_cache->is_cached('site_setting')) {

        // not cached, do our things that need caching
        $query = $CI->db->query("select * from site_setting");
        $data = $query->row_array();
        // store in cache
        $CI->simple_cache->cache_item('site_setting', $data);
    } else {
        $data = $CI->simple_cache->get_item('site_setting');
    }

    return $data;

}



/**
 * @return mixed
 */
function meta_setting($lang_id=0)
{


    $CI =& get_instance();
    
    if($lang_id<=0)
    {		
        $lang_id=1;
        if ($_SESSION['lang_code'] != '')
         {
                 $sqllang="select * from language where iso2='".$_SESSION['lang_code']."'";
                $querylang = $CI->db->query($sqllang);
                if ($querylang->num_rows() > 0) 
                {
                        $datalang = $querylang->row_array(); 
                        $lang_id=$datalang['language_id'];
                }
         }
   }

    if (!$CI->simple_cache->is_cached('meta_setting'.$lang_id)) {

        // not cached, do our things that need caching
        $query = $CI->db->query("select * from meta_setting where language_id=".$lang_id."");
        $data = $query->row_array();
        // store in cache
        $CI->simple_cache->cache_item('meta_setting'.$lang_id, $data);
    } else {       
        $data = $CI->simple_cache->get_item('meta_setting'.$lang_id);
    }

    return $data;


}



function wepay_setting()
{

    $CI =& get_instance();


    if (!$CI->simple_cache->is_cached('wepay')) {

        // not cached, do our things that need caching
        $query = $CI->db->query("select * from wepay");
        $data = $query->row_array();
        // store in cache
        $CI->simple_cache->cache_item('wepay', $data);
    } else {
        $data = $CI->simple_cache->get_item('wepay');
    }

    return $data;


}
function payment_gateway($name)
{

    $CI =& get_instance();
    $query = $CI->db->query("select * from payment_gateway where name='".$name."'");
    $data = $query->result_array();

    return $data;


}

function facebook_setting()
{


    $CI =& get_instance();

    if (!$CI->simple_cache->is_cached('facebook_setting')) {

        // not cached, do our things that need caching
        $query = $CI->db->query("select * from facebook_setting");
        $data = $query->row();
        // store in cache
        $CI->simple_cache->cache_item('facebook_setting', $data);
    } else {
        $data = $CI->simple_cache->get_item('facebook_setting');
    }

    return $data;


}


function twitter_setting()
{


    $CI =& get_instance();

    if (!$CI->simple_cache->is_cached('twitter_setting')) {

        // not cached, do our things that need caching
        $query = $CI->db->query("select * from twitter_setting");
        $data = $query->row_array();
        // store in cache
        $CI->simple_cache->cache_item('twitter_setting', $data);
    } else {
        $data = $CI->simple_cache->get_item('twitter_setting');
    }

    return $data;


}


/*** load google setting
 *  return single record array
 **/


function google_setting()
{


    $CI =& get_instance();

    if (!$CI->simple_cache->is_cached('google_setting')) {

        // not cached, do our things that need caching
        $query = $CI->db->query("select * from google_setting");
        $data = $query->row();
        // store in cache
        $CI->simple_cache->cache_item('google_setting', $data);
    } else {
        $data = $CI->simple_cache->get_item('google_setting');
    }

    return $data;


}


/*** load yahoo setting
 *  return single record array
 **/


function email_setting()

{

    $CI =& get_instance();


    if (!$CI->simple_cache->is_cached('email_setting')) {

        // not cached, do our things that need caching
        $query = $CI->db->query("select * from email_setting");
        $data = $query->row_array();
        // store in cache
        $CI->simple_cache->cache_item('email_setting', $data);
    } else {
        $data = $CI->simple_cache->get_item('email_setting');
    }

    return $data;


}


function linkdin_setting()

{

    $CI =& get_instance();


    if (!$CI->simple_cache->is_cached('linkdin_setting')) {

        // not cached, do our things that need caching
        $query = $CI->db->query("select * from linkdin_setting");
        $data = $query->row_array();
        // store in cache
        $CI->simple_cache->cache_item('linkdin_setting', $data);
    } else {
        $data = $CI->simple_cache->get_item('linkdin_setting');
    }

    return $data;


}


function youtube_setting()

{

    $CI =& get_instance();


    if (!$CI->simple_cache->is_cached('youtube_setting')) {

        // not cached, do our things that need caching
        $query = $CI->db->query("select * from youtube_setting");
        $data = $query->row_array();
        // store in cache
        $CI->simple_cache->cache_item('youtube_setting', $data);
    } else {
        $data = $CI->simple_cache->get_item('youtube_setting');
    }

    return $data;


}


function google_plus_setting()

{

    $CI =& get_instance();

    if (!$CI->simple_cache->is_cached('google_plus_setting')) {

        // not cached, do our things that need caching
        $query = $CI->db->query("select * from google_plus_setting");
        $data = $query->row_array();
        // store in cache
        $CI->simple_cache->cache_item('google_plus_setting', $data);
    } else {
        $data = $CI->simple_cache->get_item('google_plus_setting');
    }

    return $data;

}



function get_image_setting_data()
{

    $CI =& get_instance();
    if (!$CI->simple_cache->is_cached('image_setting')) {

        // not cached, do our things that need caching
        $query = $CI->db->query("select * from image_setting");
        $data = $query->row_array();
        // store in cache
        $CI->simple_cache->cache_item('image_setting', $data);
    } else {
        $data = $CI->simple_cache->get_item('image_setting');
    }

    return $data;


}


function message_setting()
{


    $CI =& get_instance();

    if (!$CI->simple_cache->is_cached('message_setting')) {

        // not cached, do our things that need caching
        $query = $CI->db->query("select * from message_setting");
        $data = $query->row();
        // store in cache
        $CI->simple_cache->cache_item('message_setting', $data);
    } else {
        $data = $CI->simple_cache->get_item('message_setting');
    }

    return $data;


}


function get_domain_name($url)


{


    $matches = parse_url($url);


    if (isset($matches['host'])) {


        $domain = $matches['host'];


        $domain = str_replace(array('www.'), '', $domain);


        return $domain;


    }


    return $url;


}


/**
 * generate random code
 *
 * @return    string
 */


function randomCode()


{


    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";


    $pass = array();


    for ($i = 0; $i < 12; $i++) {


        $n = rand(0, strlen($alphabet) - 1); //use strlen instead of count


        $pass[$i] = $alphabet[$n];


    }


    return implode($pass); //turn the array into a string


}


function getrandomCode($length = 12)


{


    $code = '';


    //Ascii Code for number, lowercase, uppercase and special characters


    $no = range(48, 57);


    $lo = range(97, 122);


    $up = range(65, 90);


    //exclude character I, l, 1, 0, O


    $eno = array(48, 49);


    $elo = array(108);


    $eup = array(73, 79);


    $no = array_diff($no, $eno);


    $lo = array_diff($lo, $elo);


    $up = array_diff($up, $eup);


    $chr = array_merge($no, $lo, $up);


    for ($i = 1; $i <= $length; $i++) {


        $code .= chr($chr[rand(0, count($chr) - 1)]);


    }


    return $code;


}


function check_user_code($rand)

{


    $CI =& get_instance();


    $query = $CI->db->get_where('user', array('unique_code' => $rand));


    if ($query->num_rows() > 0) {


        return 1;


    }


    return 0;


}


function unique_user_code($rand, $length = 12)


{


    $chk = check_user_code($rand);


    if ($chk == 1) {


        $rand = getrandomCode($length);


        unique_user_code($rand);


    }


    return $rand;


}


/**
 * @param DateTime $date A given date
 * @param int $firstDay 0-6, Sun-Sat respectively
 * @return DateTime
 */


function get_first_day_of_week($date)


{


    $day_of_week = date('N', strtotime($date));


    $week_first_day = date('Y-m-d', strtotime($date . " - " . ($day_of_week - 1) . " days"));


    return $week_first_day;


}


function get_last_day_of_week($date)


{


    $day_of_week = date('N', strtotime($date));


    $week_last_day = date('Y-m-d', strtotime($date . " + " . (7 - $day_of_week) . " days"));


    return $week_last_day;


}


/**** create seo friendly url
 * var string $text
 **/


function clean_url($text)
{


    $text = strtolower($text);


    $code_entities_match = array('&quot;', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '+', '{', '}', '|', ':', '"', '<', '>', '?', '[', ']', '', ';', "'", ',', '.', '_', '/', '*', '+', '~', '`', '=', ' ', '---', '--', '--', '�');


    $code_entities_replace = array('', '-', '-', '', '', '', '-', '-', '', '', '', '', '', '', '', '-', '', '', '', '', '', '', '', '', '', '-', '', '-', '-', '', '', '', '', '', '-', '-', '-', '-');


    $text = str_replace($code_entities_match, $code_entities_replace, $text);


    return $text;


}


/*  create seo friendly url */


function seoUrl($string)


{


    //Unwanted:  {UPPERCASE} ; / ? : @ & = + $ , . ! ~ * ' ( )


    $string = strtolower($string);


    //Strip any unwanted characters


    $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);


    //Clean multiple dashes or whitespaces


    $string = preg_replace("/[\s-]+/", " ", $string);


    //Convert whitespaces and underscore to dash


    $string = preg_replace("/[\s_]/", "-", $string);


    return $string;


}


/*  create seo friendly url end */


/********** set amount with currency *************/


function set_currency_amount($amt = null, $project_id = 0)
{


    $CI =& get_instance();


    $site_setting = site_setting();


    $decimal_points = $site_setting['decimal_points'];


    $amount = number_format($amt, $decimal_points);
    if (!is_numeric($amount)) $amount = str_replace(',', '', $amount);

    return $amount;


}


function set_currency($amt = null, $equity_id = 0, $curr_symbol = 'no')
{

    $CI =& get_instance();


    $site_setting = site_setting();

    $currency_symbol = $site_setting['currency_symbol'];

    $currency_code = $site_setting['currency_code'];

    $decimal_points = $site_setting['decimal_points'];

    $currency_symbol_side = $site_setting['currency_symbol_side'];


    if ($equity_id != '' && $equity_id != 0) {

        $equity = GetOneEquity($equity_id);

        $currency_symbol = $equity['equity_currency_symbol'];

        $currency_code = $equity['equity_currency_code'];

        if ($currency_code == '') {
            $currency_symbol = $site_setting['currency_symbol'];
            $currency_code = $site_setting['currency_code'];
        }


    }
    if ($curr_symbol == 'yes') {
        $amt = '';
    } else {
        $amt = number_format($amt, $decimal_points);
    }


    if ($currency_symbol_side == 'left') {

        $amount = $currency_symbol . $amt;

    } elseif ($currency_symbol_side == 'left_space') {

        $amount = $currency_symbol . ' ' . $amt;


    } elseif ($currency_symbol_side == 'right') {

        $amount = $amt . $currency_symbol;

    } elseif ($currency_symbol_side == 'right_space') {

        $amount = $amt . ' ' . $currency_symbol;

    } elseif ($currency_symbol_side == 'left_code') {

        $amount = $currency_code . $amt;

    } elseif ($currency_symbol_side == 'left_space_code') {

        $amount = $currency_code . ' ' . $amt;

    } elseif ($currency_symbol_side == 'right_code') {

        $amount = $amt . $currency_code;

    } 
    elseif ($currency_symbol_side == 'left_symbol_right_space_code') {

          if ($curr_symbol == 'yes') {
             $amount = $currency_symbol . $amt;
        } else {
            $amount = $currency_symbol . $amt.' '.$currency_code;
        }
       

    }
    else {

        $amount = $amt . ' ' . $currency_code;


    }

    return $amount;


}


/************** end ***********/


function parse_youtube_url($url, $return = 'embed', $width = '', $height = '', $rel = 0)


{


    $v = '';


    $urls = parse_url($url);


    if (isset($urls['host'])) {


        //url is http://youtu.be/xxxx


        if ($urls['host'] == 'youtu.be') {


            $id = ltrim($urls['path'], '/');


        } //url is http://www.youtube.com/embed/xxxx


        else if (strpos($urls['path'], 'embed') == 1) {


            $id = end(explode('/', $urls['path']));


        } //url is xxxx only


        else if (strpos($url, '/') === false) {


            $id = $url;


        }



        //http://www.youtube.com/watch?feature=player_embedded&v=m-t4pcO99gI


        //url is http://www.youtube.com/watch?v=xxxx


        else {


            parse_str($urls['query']);


            $id = $v;


            if (!empty($feature)) {


                $id = end(explode('v=', $urls['query']));


            }


        }


        //return embed iframe


        if ($return == 'embed') {


            return '<iframe width="' . ($width ? $width : 560) . '" height="' . ($height ? $height : 349) . '" src="http://www.youtube.com/embed/' . $id . '?rel=' . $rel . '" frameborder="0" allowfullscreen></iframe>';


        } //return normal thumb


        else if ($return == 'thumb') {


            return 'http://i1.ytimg.com/vi/' . $id . '/default.jpg';


        } //return hqthumb


        else if ($return == 'hqthumb') {


            return 'http://i1.ytimg.com/vi/' . $id . '/hqdefault.jpg';


        } // else return id


        else {


            return $id;


        }


    } ///===isset


    return false;


}


/***************************End of get you tube video image************************/


/***************************get vimeo video image************************/


function getVimeoInfo($id, $info = 'thumbnail_medium')


{


    if (!function_exists('curl_init')) die('CURL is not installed!');


    $ch = curl_init();


    curl_setopt($ch, CURLOPT_URL, "http://vimeo.com/api/v2/video/$id.php");


    curl_setopt($ch, CURLOPT_HEADER, 0);


    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);


    curl_setopt($ch, CURLOPT_TIMEOUT, 10);


    $output = unserialize(curl_exec($ch));


    //$output = curl_exec($ch);


    $output = $output[0][$info];


    curl_close($ch);


    return $output;


}


function youtube_thumb_url($url)


{


    $regex = '@http\:\/\/www\.youtube\.com\/\watch\?v\=([^&]+).*@';


    $replace = 'http://img.youtube.com/vi/$1/0.jpg';


    $thumb_url = preg_replace($regex, $replace, $url);


    return $thumb_url;


}


/***************************end of vimeo video image************************/


function get_paypal_logo()


{


    $CI =& get_instance();


    $query = $CI->db->get("paypal");


    $q = $query->row();


    return $q->paypal_logo;


}


/********get original client ip
 ** return ip string
 ***/


function getRealIP()


{


    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {


        $ip = $_SERVER['HTTP_CLIENT_IP'];


    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {


        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];


    } else {


        $ip = $_SERVER['REMOTE_ADDR'];


    }


    return $ip;


}


/*********get user location from ip
 *** return array
 ***/


function getip2location($user_ip)


{


    $url = 'http://msapi.com/ipaddress_location/';


    $url .= $user_ip;


    if (function_exists('curl_init')) {


        $defaults = array(


            CURLOPT_HEADER => 0,


            CURLOPT_URL => $url,


            CURLOPT_FRESH_CONNECT => 1,


            CURLOPT_RETURNTRANSFER => 1,


            CURLOPT_FORBID_REUSE => 1,


            CURLOPT_TIMEOUT => 15


        );


        $ch = curl_init();


        curl_setopt_array($ch, $defaults);


        $result = curl_exec($ch);


        curl_close($ch);


        $response = json_decode($result);


        return $response;


    } else if (function_exists('file_get_contents')) {


        $response = json_decode(file_get_contents($url));


        return $response;


    }


    return false;


}


function randomNumber($length)


{


    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";


    $pass = array(); //remember to declare $pass as an array


    for ($i = 0; $i < $length; $i++) {


        $n = rand(0, strlen($alphabet) - 1); //use strlen instead of count


        $pass[$i] = $alphabet[$n];


    }


    return implode($pass); //turn the array into a string


}


function is_date($date)


{


    if (preg_match("/^([0-9]{2})-([0-9]{2})-([0-9]{4})$/", $date, $parts)) {


        if (checkdate($parts[2], $parts[1], $parts[3]))


            return true;


        else



            return false;


    } else



        return false;


}


/*Common mail send*/


function email_send($email_address_from, $email_address_reply, $email_to, $email_subject, $str, $email_from_name = '')


{

    $site_setting = site_setting();

    $site_name = $site_setting['site_name'];

    $subject_new = str_replace('{site_name}', $site_name, $email_subject);

    $message_new = str_replace('{site_name}', $site_name, $str);

    $data['message_new'] = str_replace('{site_name}', $site_name, $str);


    $CI =& get_instance();


    $query = $CI->db->get_where("email_setting", array('email_setting_id' => 1));


    $email_set = $query->row();


    $CI->load->library('email');


    ///////====smtp====


    if ($email_set->mailer == 'smtp') {


        $config['protocol'] = 'smtp';


        $config['smtp_host'] = trim($email_set->smtp_host);


        $config['smtp_port'] = trim($email_set->smtp_port);


        $config['smtp_timeout'] = '30';


        $config['smtp_user'] = trim($email_set->smtp_email);


        $config['smtp_pass'] = trim($email_set->smtp_password);


    } /////=====sendmail======


    elseif ($email_set->mailer == 'sendmail') {


        $config['protocol'] = 'sendmail';


        $config['mailpath'] = trim($email_set->sendmail_path);


    } /////=====php mail default======


    else {


    }


    $config['wordwrap'] = TRUE;


    $config['mailtype'] = 'html';


    $config['crlf'] = '\n\n';


    $config['newline'] = '\n\n';


    $CI->email->initialize($config);


    if ($email_from_name != '') {


        $CI->email->from($email_address_from, $email_from_name);


    } else {


        $CI->email->from($email_address_from);


    }


    $CI->email->reply_to($email_address_reply);


    $CI->email->to($email_to);


    $CI->email->subject($subject_new);


    $message_new = $CI->load->view('email_template', $data, TRUE);

    //print_r($message_new); die;

    $CI->email->message($message_new);


    $CI->email->send();


}

function humanTiming($time)
{

    $time = time() - $time; // to get the time since that moment

    $tokens = array(
        31536000 => 'year',
        2592000 => 'month',
        604800 => 'week',
        86400 => 'day',
        3600 => 'hour',
        60 => 'minute',
        1 => 'second'
    );

    foreach ($tokens as $unit => $text) {
        if ($time < $unit) continue;
        $numberOfUnits = floor($time / $unit);
        return $numberOfUnits . ' ' . $text . (($numberOfUnits > 1) ? 's' : '');
    }

}


function getDuration($date)


{

    $CI =& get_instance();


    $curdate = date('Y-m-d H:i:s');


    $diff = abs(strtotime($date) - strtotime($curdate));


    $years = floor($diff / (365 * 60 * 60 * 24));


    $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));


    $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));


    $hours = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24) / (60 * 60));


    $mins = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60) / (60));


    $ago = '';


        if ($years != 0) {
        if ($years > 1) {
            $ago = $years .' '.YEARS;
        } else {
            $ago = $years .' '.TEXT_YEAR;
        }
    } elseif ($months != 0) {
        if ($months > 1) {
            $ago = $months .' '. MONTHS;
        } else {
            $ago = $months .' '.TEXT_MONTH;
        }
    } elseif ($days != 0) {
        if ($days > 1) {
            $ago = $days . ' ' . DAYS;
        } else {
            $ago = $days . ' ' . DAYS;
        }
    } elseif ($hours != 0) {
        if ($hours > 1) {
            $ago = $hours .' '. HOURS;
        } else {
            $ago = $hours .' '. TEXT_HOUR;
        }
    } else {
        if ($mins > 1) {
            $ago = $mins .' '. MINUTES;
        } else {
            $ago = $mins .' '.TEXT_MINUTE;
        }
    }


    return $ago .' '. TEXT_AGO;


}


function dateformat($date = '')

{

    $site_setting = site_setting();


    if (strtotime($date) != "" and strtotime($date) > 0) {
        $curr_lang_date = current_languagedata();
        if (isset($curr_lang_date['iso3'])) {
            setlocale(LC_TIME, $curr_lang_date['iso3']);
        }
        return date($site_setting['date_format'], strtotime($date));

    } else {

        return "";

    }

}

function DisplayDate($date = '')

{


    if (strtotime($date) != "" and strtotime($date) > 0) {
        $curr_lang_date = current_languagedata();
        if (isset($curr_lang_date['iso3'])) {
            setlocale(LC_TIME, $curr_lang_date['iso3']);
        }
        return date('Y-m-d', strtotime($date));

    } else {

        return "";

    }

}

function timeformat($date = '')

{

    $site_setting = site_setting();


    if (strtotime($date) != "" and strtotime($date) > 0) {
        $curr_lang_date = current_languagedata();
        if (isset($curr_lang_date['iso3'])) {
            setlocale(LC_TIME, $curr_lang_date['iso3']);
        }
        return date($site_setting['time_format'], strtotime($date));

    } else {

        return "";

    }

}


/*
1.Filenmae of cache


*/
function setting_deletecache($cachename)
{
    $CI =& get_instance();
    if ($CI->simple_cache->is_cached($cachename)) {
        $CI->simple_cache->delete_item($cachename);
    }

}

/*
1.Filenmae of cache
2.Table Name of cache

*/
function setting_new_cache($cachename, $tablename)
{
    $CI =& get_instance();
    $all_settings = 'facebook_setting,google_setting,yahoo_setting,message_setting';

    if (!$CI->simple_cache->is_cached($cachename)) {
        // not cached, do our things that need caching
        $query = $CI->db->get($tablename);
        if (stristr($all_settings, $cachename)) {
            $data = $query->row();
        } else {
            $data = $query->row_array();
        }
        // store in cache
        $CI->simple_cache->cache_item($cachename, $data);
    }
}

/*
	1.Filenmae of cache

	
	*/
function property_deletecache($id = '', $url = '')
{

    setting_deletecache('home_feature_project');
    setting_deletecache('home_total_project');
    setting_deletecache('home_latest_project');
    if ($id != '') {
        setting_deletecache('project_detail' . $id);
        setting_deletecache('project_detail_all' . $id);
    }
    if ($url != '') {
        setting_deletecache('project_detail' . $url);
        setting_deletecache('project_detail_all' . $url);
    }

}

function property_other_deletecache($case = '', $property_id = '', $property_url = '')
{
    switch ($case) {
        case 'update':
       
            if ($property_id != '') {
                setting_deletecache('property_detail_all_update_count' . $property_id);
                setting_deletecache('property_detail_all_update' . $property_id);
            }
            if ($property_url != '') {
                setting_deletecache('property_detail_all_update_count' . $property_url);
                setting_deletecache('property_detail_all_update' . $property_url);
            }

            break;

        case 'video':
            if ($property_id != '') {
                setting_deletecache('property_detail_all_video' . $property_id);
            }
            if ($property_url != '') {
                setting_deletecache('property_detail_all_video' . $property_url);
            }

            break;

        case 'image':
         
            if ($property_id != '') {
                setting_deletecache('property_detail_all_gallery' . $property_id);
            }
            if ($property_url != '') {
                setting_deletecache('property_detail_all_gallery' . $property_url);
            }

            break;

        case 'perk':
            if ($property_id != '') {
                setting_deletecache('property_detail_perks' . $property_id);
            }
            if ($property_url != '') {
                setting_deletecache('property_detail_perks' . $property_url);
            }

            break;

        case 'file':
            //echo $project_id.''.$url_project_title;die;
            if ($property_id != '') {
                setting_deletecache('property_detail_all_file' . $property_id);
            }
            if ($property_url != '') {
                setting_deletecache('property_detail_all_file' . $property_url);
            }

            break;

        case 'comment':

       
            if ($property_id != '') {
                setting_deletecache('property_detail_total_comment' . $property_id);
                setting_deletecache('property_detail_comments' . $property_id);
            }
            if ($property_url != '') {
                setting_deletecache('property_detail_total_comment' . $property_url);
                setting_deletecache('property_detail_comments' . $property_url);
            }

            break;

       
        case 'project_follower':
            if ($property_id != '') {
                setting_deletecache('property_detail_followers' . $property_id);
            }
            if ($property_url != '') {
                setting_deletecache('property_detail_followers' . $property_url);
            }

            break;

        case 'project_donation':
            if ($property_id != '') {
                setting_deletecache('property_detail_all_funder_total' . $property_id);
                setting_deletecache('property_detail_all_funder' . $property_id);
            }
            if ($property_url != '') {
                setting_deletecache('property_detail_all_funder_total' . $property_url);
                setting_deletecache('property_detail_all_funder' . $property_url);
            }

            break;
    }
}

function user_deletecache($case = '', $user_id = '')
{
    switch ($case) {
        case 'all':
            if (intval($user_id) > 0) {
                setting_deletecache('user_comments_' . $user_id);
                setting_deletecache('user_followers_' . $user_id);
                setting_deletecache('user_followings_' . $user_id);
                setting_deletecache('user_projects_' . $user_id);
                setting_deletecache('user_my_donation_' . $user_id);
                setting_deletecache('user_total_completed_donations_' . $user_id);
                setting_deletecache('user_total_completed_donations_rec_' . $user_id);
                $CI =& get_instance();

                $selectfields = 'property_id,property_url';
                $CI->db->select($selectfields);
                $CI->db->from('property');
                $CI->db->where('property.user_id', $user_id);
                $query = $CI->db->get();
                $result = $query->result_array();
                if ($result) {
                    foreach ($result as $res) {

                        $property_url = $res['property_url'];
                        $property_id = $res['property_id'];

                        setting_deletecache('property_detail_all_update_count' . $property_id);
                        setting_deletecache('property_detail_all_update_count' . $property_url);

                        setting_deletecache('property_detail_all_update' . $property_id);
                        setting_deletecache('property_detail_all_update' . $property_url);

                        setting_deletecache('property_detail_all_funder_total' . $property_id);
                        setting_deletecache('property_detail_all_funder_total' . $property_url);

                        setting_deletecache('property_detail_all_funder' . $property_id);
                        setting_deletecache('property_detail_all_funder' . $property_url);

                        setting_deletecache('property_detail_total_comment' . $property_id);
                        setting_deletecache('property_detail_total_comment' . $property_url);

                        setting_deletecache('property_detail_comments' . $property_id);
                        setting_deletecache('property_detail_comments' . $property_url);

                        setting_deletecache('property_detail_followers' . $property_id);
                        setting_deletecache('property_detail_followers' . $property_url);

                        setting_deletecache('property_detail_all' . $property_id);
                        setting_deletecache('property_detail_all' . $property_url);


                    }


                }
            }
            break;
        case 'comments':
            setting_deletecache('user_comments_' . $user_id);
            break;

        case 'followers':
            setting_deletecache('user_followers_' . $user_id);
            break;

        case 'followings':
            setting_deletecache('user_followings_' . $user_id);
            break;

        case 'projects':
            setting_deletecache('user_projects_' . $user_id);
            break;

        case 'my_donation':
            setting_deletecache('user_my_donation_' . $user_id);
            setting_deletecache('user_total_completed_donations_' . $user_id);
            setting_deletecache('user_total_completed_donations_rec_' . $user_id);
            break;

    }
}

/*Stripe payment Gateway Details*/


function stripe_payment_detail()

{

    $CI =& get_instance();


    $stripe_payment_detail = $CI->db->query("select * from stripe_setting");

    $data = $stripe_payment_detail->row();


    return $data;

}


function taxonomy_setting($lang_id=0)
{

    $CI =& get_instance();
	if($lang_id<=0)
	{		$lang_id=1;
			if ($_SESSION['lang_code'] != '')
			 {
				 $sqllang="select * from language where iso2='".$_SESSION['lang_code']."'";
				$querylang = $CI->db->query($sqllang);
				if ($querylang->num_rows() > 0) 
				{
					$datalang = $querylang->row_array(); 
					$lang_id=$datalang['language_id'];
				}
			 }
	}
	

    if (!$CI->simple_cache->is_cached('taxonomy_setting'.$lang_id)) {

        // not cached, do our things that need caching
		
		 $sql="select * from taxonomy_setting where language_id=".$lang_id."";
        $query = $CI->db->query($sql);
        $data = $query->row_array();
        // store in cache
        $CI->simple_cache->cache_item('taxonomy_setting'.$lang_id, $data);
    } else {
        $data = $CI->simple_cache->get_item('taxonomy_setting'.$lang_id);
    }

    return $data;


}

/** return project controller name for route file
 * 
 * @return none
 */
function projectcontrollername()
{
	$name="realestate";//only string,not other character will work
	return $name;
}
function project_text($type = '', $status = '', $start_date = '', $enddate = '')
{

    if ($type == 'Flexible') {

        if ($status == 0 || $status == 1 || $status == 2) {

            $text = THIS_CAMPAIGN_STARTED_ON . date("F j, Y", strtotime($start_date)) . AND_WILL_CLOSE . date("F j, Y, g:i a", strtotime($enddate)) . '.';

        } else {
            $text = THIS_CAMPAIGN_STARTED_ON . date("F j, Y", strtotime($start_date)) . AND_CLOSED . date("F j, Y, g:i a", strtotime($enddate)) . '.';
        }
    } else {

        if ($status == 0 || $status == 1 || $status == 2) {
            $text = THIS_CAMPAIGN_STARTED_ON . date("F j, Y", strtotime($start_date)) . AND_WILL_CLOSE . date("F j, Y, g:i a", strtotime($enddate)) . '.';

        } else {
            $text = THIS_CAMPAIGN_STARTED_ON . date("F j, Y", strtotime($start_date)) . AND_CLOSED . date("F j, Y, g:i a", strtotime($enddate)) . '.';
        }
    }

    return $text;
}


/*End Stripe Payment details*/
/** set timezone for entire site or project wise depend on arguments
 * @param string $timezone
 * @param integer $equity_id
 * @return none
 */
function set_timezone($timezone = '', $equity_id = 0)
{
    $CI =& get_instance();
    try
    {   
        if($timezone!='')
        {
            
        }
        else if($equity_id>0)
        {
            
            $stripe_payment_detail = $CI->db->query("select project_timezone from equity where equity_id=".$equity_id);
            $data = $stripe_payment_detail->row();
            $timezone= $data->project_timezone;
        }
        else
        {
             $site_setting= site_setting();
             $timezone= $site_setting['time_zone'];
             
        }
        $query = $CI->db->get_where('timezone', array('name' => $timezone));
        if ($query->num_rows() > 0) 
        {

           if (function_exists( 'date_default_timezone_set' ))date_default_timezone_set( $timezone);

        }
    }catch (Exception $ex) 
    {
              

    }
}

function property_setting()
{

    $CI =& get_instance();


    if (!$CI->simple_cache->is_cached('property_setting')) {

        // not cached, do our things that need caching
        $query = $CI->db->query("select * from property_setting");
        $data = $query->row_array();
        // store in cache
        $CI->simple_cache->cache_item('property_setting', $data);
    } else {
        //$data =$CI->simple_cache->get_item('site_setting');
        $query = $CI->db->query("select * from property_setting");
        $data = $query->row_array();
        // store in cache
        $CI->simple_cache->cache_item('property_setting', $data);
    }

    return $data;

}   


function meta_tags($data=array()){
    
    $meta_str='';
    
    $CI =& get_instance();
    
    $site_setting=site_setting();
    $meta_setting=meta_setting();
    $facebook_setting = facebook_setting();
    $twitter_setting=twitter_setting();
    
    
    $url_name='properties';//die;

    $meta_title=$meta_setting['title'];
    $meta_description=$meta_setting['meta_description'];
    $meta_keyword=$meta_setting['meta_keyword'];
    
    $twitter_user_name=$twitter_setting['twitter_user_name'];
    $fb_api_id=$facebook_setting->facebook_application_id;    
    
    $site_name=$site_setting['site_name'];    
   
    
    $locale='en_US'; 
    // if(isset($_SESSION['lang_folder'])){
    //     if($_SESSION['lang_folder']!=''){            
    //         $lang_data=getLanguageByFolder($_SESSION['lang_folder']);
    //         if(!empty($lang_data)){
    //             $locale=$lang_data['iso3'];
    //         }
    //     }
    // }
    
    
    $share_link=$CI->config->item('base_url');
    $default_link=$share_link;
    $share_title=$meta_title;
    $share_brief=$meta_description;
    
    $share_image_link='';
    $share_video_link='';
    
    $site_logo = $site_setting['site_logo'];
    if($site_logo!='' && file_exists(base_path().'upload/orig/'.$site_logo))
    {
        $share_image_link=base_url().'upload/orig/'.$site_logo;
    }
    
    if(!empty($data)){
        
         if(isset($data[0]['property_url'])){
           if($data[0]['property_url']!=''){
               $share_link=site_url($url_name.'/'.$data[0]['property_url']);
            } 
        }
        
        
        if(isset($data[0]['property_address'])){
           if($data[0]['property_address']!=''){
               $share_title=SecureShowData($data[0]['property_address']);
            } 
        }
        
      
        $project_share_brief='';
        if(isset($data[0]['property_description'])){
           if($data[0]['property_description']!=''){
               $project_share_brief=strip_tags(SecureShowData($data[0]['property_description']));
            } 
        }
        
        
        if($project_share_brief!=''){
            $share_brief=$project_share_brief;
        }
        
        
      
        
        $pimage_type='image';
        if(isset($data[0]['video_url'])){
           if($data[0]['video_url']!=''){
               $pimage_type='video';
            } 
        }
        
       
            //===type image
            $pimage_link = '';
            if (isset($data[0]['cover_image'])) {
                if ($data[0]['cover_image'] != '') {
                    if (file_exists(base_path() . "upload/property/large/" . $data[0]['cover_image'])) {
                        $pimage_link = base_url() . 'upload/property/large/' . $data[0]['cover_image'];
                    }
                    
                     if ($pimage_link=='' && file_exists(base_path() . "upload/property/medium/" . $data[0]['cover_image'])) {
                        $pimage_link = base_url() . 'upload/property/medium/' . $data[0]['cover_image'];
                    }
                    
                    if ($pimage_link=='' && file_exists(base_path() . "upload/property/commoncard/" . $data[0]['cover_image'])) {
                        $pimage_link = base_url() . 'upload/property/commoncard/' . $data[0]['cover_image'];
                    } 
                        
                }
            }
            
           

            if ($pimage_link == '') {


             


                          if (isset($data[0]['property_id'])) {
                                if ($data[0]['property_id'] > 0) {

                                    $query = $CI->db->query("select * from property_gallery where property_id='" . $data[0]['property_id'] . "' order by property_gallery_id asc ");

                                    if ($query->num_rows() > 0) {
                                        $get_gallery = $query->result_array();

                                        $grcnt = 1;
                                        if ($get_gallery) {

                                            foreach ($get_gallery as $glr) {
                                                $project_image = $glr['image'];
                                                if ($project_image != '' && file_exists(base_path() . 'upload/gallery/' . $project_image) && $grcnt == 1) {
                                                    $pvideo_image_link = base_url() . 'upload/gallery/' . $project_image;

                                                    $grcnt = 2;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        
                    
                
            }
            if ($pimage_link != '') {
                $share_image_link = $pimage_link;
            }
            //===
        }    
        
   
    
            
    //$meta_str.='<!--http://ogp.me/-->'.PHP_EOL;
    //$meta_str.='<!-- Open Graph data -->';
    /*$meta_str.='<!--<meta property="fb:admins" content="<?php //echo FB_APP_ID; ?>" />-->'.PHP_EOL;*/
    $meta_str.='<meta property="fb:app_id" content="'.$fb_api_id.'" />'.PHP_EOL;
    $meta_str.='<meta property="og:site_name" content="'.$site_name.'" />'.PHP_EOL;
    $meta_str.='<meta property="og:type" content="website" />'.PHP_EOL;
    $meta_str.='<meta property="og:url" content="'.$share_link.'" />'.PHP_EOL;

    $meta_str.='<meta property="og:locale" content="'.$locale.'" />'.PHP_EOL;
    $meta_str.='<meta property="og:title" content="'.$share_title.'" />'.PHP_EOL;
    $meta_str.='<meta property="og:description" content="'.$share_brief.'" />'.PHP_EOL;

    /*$meta_str.='<meta property="article:publisher" content="'.$site_name.'" />'.PHP_EOL;*/
    /*$meta_str.='<!--must match with locale <meta property="og:country-name" content="<?php echo $event_country_name; ?>"/>-->'.PHP_EOL;*/

    if($share_image_link!='') { 
        $meta_str.='<meta property="og:image" content="'.$share_image_link.'" />'.PHP_EOL;
        $meta_str.='<meta property="og:image:url" content="'.$share_image_link.'" />'.PHP_EOL;
        /*$meta_str.='<!--<meta property="og:image:secure_url" content="'.$share_image_link.'" />-->'.PHP_EOL;*/
    } 

    if($share_video_link!='') { 
        $meta_str.='<meta property="og:video" content="'.$share_video_link.'" />'.PHP_EOL;
        $meta_str.='<meta property="og:video:url" content="'.$share_video_link.'" />'.PHP_EOL;
        /*$meta_str.='<!--<meta property="og:video:secure_url" content="'.$share_video_link.'" />-->'.PHP_EOL;*/
        $meta_str.='<meta property="og:video:width" content="640" />'.PHP_EOL;
        $meta_str.='<meta property="og:video:height" content="385" />'.PHP_EOL;
     } 

    //$meta_str.='<!-- http://ogp.me/#types   https://developers.facebook.com/docs/reference/opengraph   https://yoast.com/facebook-open-graph-protocol/-->'.PHP_EOL;


    //$meta_str.='<!-- Twitter Card data https://dev.twitter.com/cards/markup-->'.PHP_EOL;
    
    $meta_str.='<meta name="twitter:card" content="summary_large_image" />'.PHP_EOL;
    
    
    $meta_str.='<meta name="twitter:domain" content="'.$default_link.'">'.PHP_EOL;
    
    if($twitter_user_name!=''){
        $meta_str.='<meta name="twitter:site" content="@'.$twitter_user_name.'" />'.PHP_EOL;
        $meta_str.='<meta name="twitter:site:id" content="@'.$twitter_user_name.'" />'.PHP_EOL;
        $meta_str.='<meta name="twitter:creator" content="@'.$twitter_user_name.'" />'.PHP_EOL;
    }
    $meta_str.='<meta name="twitter:title" content="'.$share_title.'" />'.PHP_EOL;
    $meta_str.='<meta name="twitter:url" content="'.$share_link.'" />'.PHP_EOL;
    $meta_str.='<meta name="twitter:description" content="'.$share_brief.'" />'.PHP_EOL;

    /*$meta_str.='<!--twitter account of merchant-->'.PHP_EOL;
    $meta_str.='<!--<meta name="twitter:creator" content="@author_handle" />-->'.PHP_EOL;*/

    $meta_str.='<meta name="twitter:image" content="'.$share_image_link.'" />'.PHP_EOL;
    $meta_str.='<meta name="twitter:image:src" content="'.$share_image_link.'" />'.PHP_EOL;
    $meta_str.='<meta name="twitter:card:src" content="'.$share_image_link.'" />'.PHP_EOL;
 

    
    return $meta_str;
    
}


function crowd_meta_tags($data=array()){
    

    $meta_str='';
    
    $CI =& get_instance();
    
    $site_setting=site_setting();
    $meta_setting=meta_setting();
    $facebook_setting = facebook_setting();
    $twitter_setting=twitter_setting();
    
    
    $url_name='crowds';//die;

    $meta_title=$meta_setting['title'];
    $meta_description=$meta_setting['meta_description'];
    $meta_keyword=$meta_setting['meta_keyword'];
    
    $twitter_user_name=$twitter_setting['twitter_user_name'];
    $fb_api_id=$facebook_setting->facebook_application_id;    
    
    $site_name=$site_setting['site_name'];    
   
    
    $locale='en_US'; 
 
    
    $share_link=$CI->config->item('base_url');
    $default_link=$share_link;
    $share_title=$meta_title;
    $share_brief=$meta_description;
    
    $share_image_link='';
    $share_video_link='';
    
    $site_logo = $site_setting['site_logo'];
    if($site_logo!='' && file_exists(base_path().'upload/orig/'.$site_logo))
    {
        $share_image_link=base_url().'upload/orig/'.$site_logo;
    }
    

    if(!empty($data)){
        
         if(isset($data[0]['crowd_url'])){
           if($data[0]['crowd_url']!=''){
               $share_link=site_url($url_name.'/'.$data[0]['crowd_url']);
            } 
        }
        
        
        if(isset($data[0]['title'])){
           if($data[0]['title']!=''){
               $share_title=SecureShowData($data[0]['title']);
            } 
        }
        
      
        $project_share_brief='';
        if(isset($data[0]['description'])){
           if($data[0]['description']!=''){
               $project_share_brief=strip_tags(SecureShowData($data[0]['description']));
            } 
        }
        
        
        if($project_share_brief!=''){
            $share_brief=$project_share_brief;
        }
        
        

            //===
        }    
        
   
    
            
    //$meta_str.='<!--http://ogp.me/-->'.PHP_EOL;
    //$meta_str.='<!-- Open Graph data -->';
    /*$meta_str.='<!--<meta property="fb:admins" content="<?php //echo FB_APP_ID; ?>" />-->'.PHP_EOL;*/
    $meta_str.='<meta property="fb:app_id" content="'.$fb_api_id.'" />'.PHP_EOL;
    $meta_str.='<meta property="og:site_name" content="'.$site_name.'" />'.PHP_EOL;
    $meta_str.='<meta property="og:type" content="website" />'.PHP_EOL;
    $meta_str.='<meta property="og:url" content="'.$share_link.'" />'.PHP_EOL;

    $meta_str.='<meta property="og:locale" content="'.$locale.'" />'.PHP_EOL;
    $meta_str.='<meta property="og:title" content="'.$share_title.'" />'.PHP_EOL;
    $meta_str.='<meta property="og:description" content="'.$share_brief.'" />'.PHP_EOL;

    /*$meta_str.='<meta property="article:publisher" content="'.$site_name.'" />'.PHP_EOL;*/
    /*$meta_str.='<!--must match with locale <meta property="og:country-name" content="<?php echo $event_country_name; ?>"/>-->'.PHP_EOL;*/

    if($share_image_link!='') { 
        $meta_str.='<meta property="og:image" content="'.$share_image_link.'" />'.PHP_EOL;
        $meta_str.='<meta property="og:image:url" content="'.$share_image_link.'" />'.PHP_EOL;
        /*$meta_str.='<!--<meta property="og:image:secure_url" content="'.$share_image_link.'" />-->'.PHP_EOL;*/
    } 

   

    //$meta_str.='<!-- http://ogp.me/#types   https://developers.facebook.com/docs/reference/opengraph   https://yoast.com/facebook-open-graph-protocol/-->'.PHP_EOL;


    //$meta_str.='<!-- Twitter Card data https://dev.twitter.com/cards/markup-->'.PHP_EOL;
    
    $meta_str.='<meta name="twitter:card" content="summary_large_image" />'.PHP_EOL;
    
    
    $meta_str.='<meta name="twitter:domain" content="'.$default_link.'">'.PHP_EOL;
    
    if($twitter_user_name!=''){
        $meta_str.='<meta name="twitter:site" content="@'.$twitter_user_name.'" />'.PHP_EOL;
        $meta_str.='<meta name="twitter:site:id" content="@'.$twitter_user_name.'" />'.PHP_EOL;
        $meta_str.='<meta name="twitter:creator" content="@'.$twitter_user_name.'" />'.PHP_EOL;
    }
    $meta_str.='<meta name="twitter:title" content="'.$share_title.'" />'.PHP_EOL;
    $meta_str.='<meta name="twitter:url" content="'.$share_link.'" />'.PHP_EOL;
    $meta_str.='<meta name="twitter:description" content="'.$share_brief.'" />'.PHP_EOL;

    /*$meta_str.='<!--twitter account of merchant-->'.PHP_EOL;
    $meta_str.='<!--<meta name="twitter:creator" content="@author_handle" />-->'.PHP_EOL;*/

    $meta_str.='<meta name="twitter:image" content="'.$share_image_link.'" />'.PHP_EOL;
    $meta_str.='<meta name="twitter:image:src" content="'.$share_image_link.'" />'.PHP_EOL;
    $meta_str.='<meta name="twitter:card:src" content="'.$share_image_link.'" />'.PHP_EOL;
 


    
    return $meta_str;
    
}



/*End Stripe Payment details*/

function docusign_setting()
{


    $CI =& get_instance();

    if (!$CI->simple_cache->is_cached('docusign_setting')) {

        // not cached, do our things that need caching
        $query = $CI->db->query("select * from docusign_setting");
        $data = $query->row_array();
        // store in cache
        $CI->simple_cache->cache_item('docusign_setting', $data);
    } else {
        $data = $CI->simple_cache->get_item('docusign_setting');
    }

    return $data;

}

?>
