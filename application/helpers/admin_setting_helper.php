<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


/*function site_setting()

{

    $CI =& get_instance();

    $query = $CI->db->query("select * from site_setting");

    return $query->row_array();

}*/


function meta_setting()

{

    $CI =& get_instance();

    $query = $CI->db->query("select * from meta_setting");

    return $query->row_array();

}


function facebook_setting()

{

    $CI =& get_instance();

    $query = $CI->db->query("select * from facebook_setting");

    return $query->row();

}

function twitter_setting()

{

    $CI =& get_instance();

    $query = $CI->db->query("select * from twitter_setting");

    return $query->row();

}

/*** load google setting
 *  return single record array
 **/


function google_setting()

{

    $CI =& get_instance();

    $query = $CI->db->get("google_setting");

    return $query->row();

}


/*** load yahoo setting
 *  return single record array
 **/


function yahoo_setting()

{

    $CI =& get_instance();

    $query = $CI->db->get("yahoo_setting");

    return $query->row();

}

function randomNumber($length)

{

    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";

    $pass = array(); //remember to declare $pass as an array


    for ($i = 0; $i < $length; $i++) {

        $n = rand(0, strlen($alphabet) - 1); //use strlen instead of count

        $pass[$i] = $alphabet[$n];

    }

    return implode($pass); //turn the array into a string

}

function getrandomCode($length = 12)

{


    $code = '';

    //Ascii Code for number, lowercase, uppercase and special characters

    $no = range(48, 57);

    $lo = range(97, 122);

    $up = range(65, 90);


    //exclude character I, l, 1, 0, O

    $eno = array(48, 49);

    $elo = array(108);

    $eup = array(73, 79);

    $no = array_diff($no, $eno);

    $lo = array_diff($lo, $elo);

    $up = array_diff($up, $eup);

    $chr = array_merge($no, $lo, $up);


    for ($i = 1; $i <= $length; $i++) {


        $code .= chr($chr[rand(0, count($chr) - 1)]);

    }

    return $code;

}

function check_user_code($rand)
{

    $CI =& get_instance();


    $query = $CI->db->get_where('user', array('unique_code' => $rand));


    if ($query->num_rows() > 0) {

        return 1;

    }


    return 0;

}


function unique_user_code($rand, $length = 12)

{

    $chk = check_user_code($rand);


    if ($chk == 1) {

        $rand = getrandomCode($length);

        unique_user_code($rand);

    }


    return $rand;


}


?>
