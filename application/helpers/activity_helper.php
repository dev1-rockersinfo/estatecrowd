
<?php



/*
	Function name :user_activity()
	Parameter :case='',$user_id=0,$second_user_id=0,$equity_id=0
	Return : all team members list for that equity
	Use : to show the team members list
	user_id is always logged in user id
	admin=0 for frontside users
	admin=1 means for adminside activity
	*/
function user_activity($case = '', $user_id = 0, $second_user_id = 0, $campaign_id = 0)
{
    $taxonomy_setting = taxonomy_setting();

    $CI =& get_instance();
    $CI->load->model('Startequity_model');
    $datetime = date('Y-m-d H:i:s');
    $logged_in_username = '';
    $second_user_username = '';
    $project_name = '';
    if ($user_id > 0) $logged_in_username = GetUserName($user_id);
    if ($second_user_id > 0) $second_user_username = GetUserName($second_user_id);
    if ($campaign_id > 0) $project_name = GetCampaignName($campaign_id);
    switch ($case) {

        case 'follow':


            $module = 'User';
            $action = $case;
            $following = $taxonomy_setting['followers_gerund'];

            //1.Fan User

            $action_detail = sprintf(YOU_HAVE_STARTED_FOLLOWING, $following, $second_user_username);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);

            //2. Followed User			
            $action_detail = sprintf(USER_HAVE_STARTED_FOLLOWING_YOU, $logged_in_username, $following);
            $data_array = array('user_id' => $second_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
            //3. Fan user of followed user
            $follower_users_data = alluserfollowers($second_user_id);
            if (is_array($follower_users_data)) {
                foreach ($follower_users_data as $follower) {
                    $follower_user_id = $follower['user_id'];
                    if ($follower_user_id != $user_id) {
                        $follower_profile_slug = $follower['profile_slug'];
                        $action_detail = sprintf(USER_HAVE_STARTED_FOLLOWING_USER, $logged_in_username, $following, $second_user_username);
                        $data_array = array('user_id' => $follower_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
                        $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
                    }
                }
            }
            break;
        case 'unfollow':


            $module = 'User';
            $action = $case;
            $following = $taxonomy_setting['unfollowers_gerund'];

            //1.Fan User

            $action_detail = sprintf(YOU_HAVE_STARTED_UNFOLLOWING, $following, $second_user_username);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);

            //2. Followed User			
            $action_detail = sprintf(USER_HAVE_STARTED_UNFOLLOWING_YOU, $logged_in_username, $following);
            $data_array = array('user_id' => $second_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
            //3. Fan user of followed user
            $follower_users_data = alluserfollowers($second_user_id);
            if (is_array($follower_users_data)) {
                foreach ($follower_users_data as $follower) {
                    $follower_user_id = $follower['user_id'];
                    if ($follower_user_id != $user_id) {

                        $follower_profile_slug = $follower['profile_slug'];
                        $action_detail = sprintf(USER_HAVE_STARTED_UNFOLLOWING_USER, $logged_in_username, $following, $second_user_username);
                        $data_array = array('user_id' => $follower_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
                        $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
                    }
                }
            }
            break;
        case 'signup':

            $module = 'User';
            $action = $case;
            $datetime = date('Y-m-d H:i:s');
            $site_setting = site_setting();
            $site_name = $site_setting['site_name'];

            $action_detail = sprintf(USER_JOINED_SITE, $logged_in_username, $site_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('admin_activity', '', '', $data_array);

            break;
        case 'update_account':
            //1. Logged In User

            $module = 'User';
            $action = $case;

            $action_detail = YOU_UPDTAED_ACCOUNT_INFORMATION;
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);

            //2. Admin


            $action_detail = sprintf(USER_UPDTAED_ACCOUNT_INFORMATION, $logged_in_username);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('admin_activity', '', '', $data_array);
            break;
        case 'accredetial_request':
            //1. Logged In User
            $taxonomy_setting = taxonomy_setting();
            $investor = $taxonomy_setting['investor'];
            $module = 'User';
            $action = $case;

            $action_detail = sprintf(YOU_APPLIED_TO_BECOME_ACCREDITED, $investor);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);

            //2. Admin


            $str_name_url = site_url('admin/accreditation/accreditation_user_list');
            $accredited_admin_link = anchor($str_name_url, ACCREDITED);
            $action_detail = sprintf(USER_APPLIED_TO_BECOME_ACCREDITED, $logged_in_username, $accredited_admin_link, $investor);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('admin_activity', '', '', $data_array);
            break;
        default:
            break;
    }

    $date_three = date("Y-m-d", strtotime("-3 Months"));
    $firstDay = date('Y-m-d', strtotime("first day of this month"));
    $sql = "Delete from user_activity WHERE  datetime BETWEEN '" . $date_three . "' AND '" . $firstDay . "' ";
    $query = $CI->db->query($sql);

    $sql = "Delete from admin_activity WHERE  datetime BETWEEN '" . $date_three . "' AND '" . $firstDay . "' ";
    $query = $CI->db->query($sql);
}

/*
	Function name :project_activity()
	Parameter :case='',$user_id=0,$second_user_id=0,$equity_id=0
	Return : all team members list for that equity
	Use : to show the team members list
	user_id is always logged in user id
	
	*/
function project_activity($case = '', $user_id = 0, $second_user_id = 0, $campaign_id = 0, $other_data = array())
{
    $taxonomy_setting = taxonomy_setting();
    $taxonomy_project_url = $taxonomy_setting['project_url'];


    $CI =& get_instance();
    $CI->load->model('Startequity_model');
    $CI->load->model('equity_model');
    $datetime = date('Y-m-d H:i:s');
    $logged_in_username = '';
    $second_user_username = '';
    $project_name = '';
    if ($user_id > 0) $logged_in_username = GetUserName($user_id);
    if ($second_user_id > 0) $second_user_username = GetUserName($second_user_id);
    $campaign_name = '';
    $campaign_owner_id = 0;
    $campaign_owner_name = 0;

    if ($campaign_id > 0) {
        $get_project_user_detail = $CI->equity_model->GetAllEquities(0, $campaign_id, $is_featured = 0, $is_status = '', $joinarr = array('user'), $limit = 10, $order = array('equity_id' => 'desc')); // store in cache

        if (is_array($get_project_user_detail)) {
            $company_name = SecureShowData($get_project_user_detail[0]['company_name']);
			if(isset($get_project_user_detail[0]['project_name'])) $company_name = SecureShowData($get_project_user_detail[0]['project_name']);
            $campaign_owner_id = SecureShowData($get_project_user_detail[0]['user_id']);
            $equity_url = $get_project_user_detail[0]['equity_url'];
            $project_url = $taxonomy_setting['project_url'];
            if ($taxonomy_project_url != '') {
                $taxonomy_project_url = $project_url;

            }
            if (!empty($company_name)) {

                $str_name_url = site_url($taxonomy_project_url . '/' . $equity_url);

            } else {

                $str_name_url = site_url($taxonomy_project_url . '/' . $campaign_id);
            }

            $campaign_name = anchor($str_name_url, $company_name);

            $user_name = $get_project_user_detail[0]['user_name'];
            $last_name = $get_project_user_detail[0]['last_name'];
            $str_name = $user_name . ' ' . $last_name;
            $profile_slug = $get_project_user_detail[0]['profile_slug'];
            if (!empty($profile_slug)) {

                $str_name_url = site_url('user/' . $profile_slug);

            } else {

                $str_name_url = site_url('user/' . $user_id);
            }

            $campaign_owner_name = anchor($str_name_url, $str_name);
        }
    }


    switch ($case) {

        case 'follow':

            $module = 'project';
            $action = $case;

            $following = $taxonomy_setting['followers_gerund'];
            $funds = $taxonomy_setting['funds_past'];
            $comments = $taxonomy_setting['comments_past'];
            //1.Fan User

            $action_detail = sprintf(YOU_HAVE_STARTED_FOLLOWING, $following, $campaign_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);

            //2. Followed User			
            $action_detail = sprintf(USER_HAVE_STARTED_FOLLOWING_YOUR_CAMPAIGNS, $logged_in_username, $following, $campaign_name);
            $data_array = array('user_id' => $campaign_owner_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
            //3. Fan user of followed user
            $follower_users_data = allprojectfollowers($campaign_id);
            if (is_array($follower_users_data)) {
                foreach ($follower_users_data as $follower) {
                    $follower_user_id = $follower['user_id'];
                    if ($follower_user_id != $user_id and $follower_user_id > 0) {

                        $action_detail = sprintf(USER_HAVE_STARTED_FOLLOWING_USER_ON_CAMPAIGNS, $logged_in_username, $following, $campaign_name, $following);
                        $data_array = array('user_id' => $follower_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
                        $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
                    }
                }
            }
            //4. All donors of this project
            $donors_users_data = allfunders($campaign_id);
            if (is_array($donors_users_data)) {
                foreach ($donors_users_data as $donor) {
                    $donor_user_id = $donor['user_id'];
                    if ($donor_user_id != $user_id and $donor_user_id > 0) {
                        $action_detail = sprintf(USER_HAVE_STARTED_FUNDING_USER_ON_CAMPAIGNS, $logged_in_username, $following, $campaign_name, $funds);
                        $data_array = array('user_id' => $donor_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
                        $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
                    }
                }
            }

            //5. All commenter of this project
            $comments_users_data = allcommenter($campaign_id);
            if (is_array($comments_users_data)) {
                foreach ($comments_users_data as $comment) {
                    $comment_user_id = $comment['user_id'];
                    if ($comment_user_id != $user_id and $comment_user_id > 0) {
                        $action_detail = sprintf(USER_HAVE_STARTED_COMMNENT_USER_ON_CAMPAIGNS, $logged_in_username, $following, $campaign_name, $comments);
                        $data_array = array('user_id' => $comment_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
                        $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
                    }
                }
            }

            //6. All team members of this project
            $team_users_data = allteammembers($campaign_id);
            if (is_array($team_users_data)) {
                foreach ($team_users_data as $team) {
                    $team_user_id = $team['user_id'];
                    if ($team_user_id != $user_id and $team_user_id > 0) {
                        $action_detail = sprintf(USER_HAVE_STARTED_TEAMMEMBER_USER_ON_CAMPAIGNS, $logged_in_username, $following, $campaign_name);
                        $data_array = array('user_id' => $team_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
                        $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
                    }
                }
            }

            //7. Admin User 
            $admin = 1;
            $action_detail = sprintf(USER_HAVE_STARTED_FOLLOWING_ADMIN, $logged_in_username, $following, $campaign_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('admin_activity', '', '', $data_array);
            break;

        case 'unfollow':


            $module = 'project';
            $action = $case;


            $following = $taxonomy_setting['unfollowers_gerund'];
            $funds = $taxonomy_setting['funds_past'];
            $comments = $taxonomy_setting['comments_past'];
            //1.Fan User

            $action_detail = sprintf(YOU_HAVE_STARTED_UNFOLLOWING, $following, $campaign_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);

            //2. Followed User			
            $action_detail = sprintf(USER_HAVE_STARTED_UNFOLLOWING_YOUR_CAMPAIGNS, $logged_in_username, $following, $campaign_name);
            $data_array = array('user_id' => $campaign_owner_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
            //3. Fan user of followed user
            $follower_users_data = allprojectfollowers($campaign_id);
            if (is_array($follower_users_data)) {
                foreach ($follower_users_data as $follower) {
                    $follower_user_id = $follower['user_id'];
                    if ($follower_user_id != $user_id) {

                        $action_detail = sprintf(USER_HAVE_STARTED_UNFOLLOWING_USER_ON_CAMPAIGNS, $logged_in_username, $following, $campaign_name, $following);
                        $data_array = array('user_id' => $follower_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
                        $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
                    }
                }
            }
            break;

            //4. All donors of this project
            $donors_users_data = allfunders($campaign_id);
            if (is_array($donors_users_data)) {
                foreach ($donors_users_data as $donor) {
                    $donor_user_id = $donor['user_id'];
                    if ($donor_user_id != $user_id) {
                        $action_detail = sprintf(USER_HAVE_STARTED_FUNDING_USER_ON_CAMPAIGNS_UNFOLLOW, $logged_in_username, $following, $campaign_name, $funds);
                        $data_array = array('user_id' => $donor_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);

                        $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
                    }
                }


            }

            //5. All commenter of this project
            $comments_users_data = allcommenter($campaign_id);
            if (is_array($comments_users_data)) {
                foreach ($comments_users_data as $comment) {

                    $comment_user_id = $comment['user_id'];
                    if ($comment_user_id != $user_id) {
                        $action_detail = sprintf(USER_HAVE_STARTED_COMMNENT_USER_ON_CAMPAIGNS_UNFOLLOW, $logged_in_username, $following, $campaign_name, $comments);
                        $data_array = array('user_id' => $comment_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
                        $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
                    }
                }
            }

            //6. All team members of this project
            $team_users_data = allteammembers($campaign_id);
            if (is_array($team_users_data)) {
                foreach ($team_users_data as $team) {
                    $team_user_id = $team['user_id'];
                    if ($team_user_id != $user_id) {
                        $action_detail = sprintf(USER_HAVE_STARTED_TEAMMEMBER_USER_ON_CAMPAIGNS_UNFOLLOW, $logged_in_username, $following, $campaign_name);
                        $data_array = array('user_id' => $team_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
                        $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
                    }
                }
            }

            //7. Admin User 
            $admin = 1;
            $action_detail = sprintf(USER_HAVE_STARTED_FOLLOWING_ADMIN_UNFOLLOW, $logged_in_username, $following, $campaign_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('admin_activity', '', '', $data_array);
            break;
        case 'update':


            $module = 'project';
            $action = $case;


            $updates = $taxonomy_setting['updates'];
            $funds = $taxonomy_setting['funds_past'];
            $comments = $taxonomy_setting['comments_past'];
            $following = $taxonomy_setting['followers_past'];

            //1.Fan User

            $action_detail = sprintf(YOU_HAVE_STARTED_UPDATES, $updates, $campaign_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);


            //2. Fan user of followed user
            $follower_users_data = allprojectfollowers($campaign_id);
            if (is_array($follower_users_data)) {
                foreach ($follower_users_data as $follower) {
                    $follower_user_id = $follower['user_id'];
                    if ($follower_user_id != $user_id and $follower_user_id > 0) {


                        $action_detail = sprintf(USER_HAVE_STARTED_UPDATES_FOR_FOLLOWING_USER, $logged_in_username, $updates, $campaign_name, $following);
                        $data_array = array('user_id' => $follower_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
                        $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
                    }
                }
            }
            //3. All donors of this project
            $donors_users_data = allfunders($campaign_id);
            if (is_array($donors_users_data)) {
                foreach ($donors_users_data as $donor) {
                    $donor_user_id = $donor['user_id'];
                    if ($donor_user_id != $user_id and $donor_user_id > 0) {
                        $action_detail = sprintf(USER_HAVE_STARTED_UPDATED_FOR_DONOR_USER, $logged_in_username, $updates, $campaign_name, $funds);
                        $data_array = array('user_id' => $donor_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
                        $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
                    }
                }
            }

            //4. All commenter of this project
            $comments_users_data = allcommenter($campaign_id);
            if (is_array($comments_users_data)) {
                foreach ($comments_users_data as $comment) {
                    $comment_user_id = $comment['user_id'];
                    if ($comment_user_id != $user_id and $comment_user_id > 0) {

                        $action_detail = sprintf(USER_HAVE_STARTED_UPDATES_FOR_COMMENTER_USER, $logged_in_username, $updates, $campaign_name, $comments);
                        $data_array = array('user_id' => $comment_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
                        $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
                    }
                }
            }

            //5. All team members of this project
            $team_users_data = allteammembers($campaign_id);
            if (is_array($team_users_data)) {
                foreach ($team_users_data as $team) {

                    $team_user_id = $team['user_id'];
                    if ($team_user_id != $user_id and $team_user_id > 0) {
                        $action_detail = sprintf(USER_HAVE_STARTED_UPDATES_FOR_TEAMMEMBER_USER, $logged_in_username, $updates, $campaign_name);
                        $data_array = array('user_id' => $team_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
                        $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
                    }
                }
            }

            //6. Admin User 
            $admin = 1;
            $action_detail = sprintf(USER_HAVE_STARTED_UPDATE_ADMIN, $logged_in_username, $updates, $campaign_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('admin_activity', '', '', $data_array);
            break;
        case 'funded':

            $module = 'project';
            $action = $case;

            $following = $taxonomy_setting['followers_gerund'];
            $funds = $taxonomy_setting['funds_past'];
            $comments = $taxonomy_setting['comments_past'];
            $following = $taxonomy_setting['followers_past'];
            $amount = 0;
            if (isset($other_data['amount'])) $amount = $other_data['amount'];
            //1.Fan User

            $action_detail = sprintf(YOU_HAVE_STARTED_FUND, $funds, $campaign_name, $amount);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);

            //2. Project Owner		
            $action_detail = sprintf(USER_HAVE_STARTED_FUND_PROJECT_OWNER, $campaign_name, $funds, $amount, $logged_in_username);
            $data_array = array('user_id' => $campaign_owner_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
            //3. Fan user of followed user
            $follower_users_data = allprojectfollowers($campaign_id);
            if (is_array($follower_users_data)) {
                foreach ($follower_users_data as $follower) {
                    $follower_user_id = $follower['user_id'];
                    if ($follower_user_id != $user_id and $follower_user_id > 0) {

                        $action_detail = sprintf(USER_HAVE_STARTED_FUND_FOR_FOLLOWING_USER, $campaign_name, $following, $funds, $amount, $logged_in_username);
                        $data_array = array('user_id' => $follower_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
                        $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
                    }
                }
            }
            //4. All donors of this project
            $donors_users_data = allfunders($campaign_id);
            if (is_array($donors_users_data)) {
                foreach ($donors_users_data as $donor) {
                    $donor_user_id = $donor['user_id'];
                    if ($donor_user_id != $user_id and $donor_user_id > 0) {
                        $action_detail = sprintf(USER_HAVE_STARTED_FUND_FOR_DONOR_USER, $campaign_name, $funds, $funds, $amount, $logged_in_username);
                        $data_array = array('user_id' => $donor_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
                        $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
                    }
                }
            }

            //5. All commenter of this project
            $comments_users_data = allcommenter($campaign_id);
            if (is_array($comments_users_data)) {
                foreach ($comments_users_data as $comment) {
                    $comment_user_id = $comment['user_id'];
                    if ($comment_user_id != $user_id and $comment_user_id > 0) {
                        $action_detail = sprintf(USER_HAVE_STARTED_FUND_FOR_COMMENTER_USER, $campaign_name, $comments, $funds, $amount, $logged_in_username);
                        $data_array = array('user_id' => $comment_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
                        $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
                    }
                }
            }

            //6. All team members of this project
            $team_users_data = allteammembers($campaign_id);
            if (is_array($team_users_data)) {
                foreach ($team_users_data as $team) {
                    $team_user_id = $team['user_id'];
                    if ($team_user_id != $user_id and $team_user_id > 0) {
                        $action_detail = sprintf(USER_HAVE_STARTED_FUND_FOR_TEAMMEMBER_USER, $campaign_name, $funds, $amount, $logged_in_username);
                        $data_array = array('user_id' => $team_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
                        $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
                    }
                }
            }

            //7. Admin User 
            $admin = 1;
            $action_detail = sprintf(USER_HAVE_STARTED_FUND_ADMIN, $campaign_name, $funds, $amount, $logged_in_username);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('admin_activity', '', '', $data_array);
            break;
        case 'commented':

            $module = 'project';
            $action = $case;

            $funds = $taxonomy_setting['funds_past'];
            $comments = $taxonomy_setting['comments_past'];
            $following = $taxonomy_setting['followers_past'];
            //1.Fan User

            $action_detail = sprintf(YOU_HAVE_STARTED_COMMENTED, $comments, $campaign_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);

            //2. Project Owner			
            $action_detail = sprintf(USER_HAVE_STARTED_COMMENTED_PROJECT_OWNER, $logged_in_username, $comments, $campaign_name);
            $data_array = array('user_id' => $campaign_owner_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
            //3. Fan user of followed user
            $follower_users_data = allprojectfollowers($campaign_id);
            if (is_array($follower_users_data)) {
                foreach ($follower_users_data as $follower) {
                    $follower_user_id = $follower['user_id'];
                    if ($follower_user_id != $user_id and $follower_user_id > 0) {

                        $action_detail = sprintf(USER_HAVE_STARTED_COMMENTED_FOR_FOLLOWING_USER, $logged_in_username, $comments, $campaign_name, $following);
                        $data_array = array('user_id' => $follower_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
                        $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
                    }
                }
            }
            //4. All donors of this project
            $donors_users_data = allfunders($campaign_id);
            if (is_array($donors_users_data)) {
                foreach ($donors_users_data as $donor) {
                    $donor_user_id = $donor['user_id'];
                    if ($donor_user_id != $user_id and $donor_user_id > 0) {
                        $action_detail = sprintf(USER_HAVE_STARTED_COMMENTED_FOR_DONOR_USER, $logged_in_username, $comments, $campaign_name, $funds);
                        $data_array = array('user_id' => $donor_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
                        $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
                    }
                }
            }

            //5. All commenter of this project
            $comments_users_data = allcommenter($campaign_id);
            if (is_array($comments_users_data)) {
                foreach ($comments_users_data as $comment) {
                    $comment_user_id = $comment['user_id'];
                    if ($comment_user_id != $user_id and $comment_user_id > 0) {
                        $action_detail = sprintf(USER_HAVE_STARTED_COMMENTED_FOR_COMMENTER_USER, $logged_in_username, $comments, $campaign_name, $comments);
                        $data_array = array('user_id' => $comment_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
                        $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
                    }
                }
            }

            //6. All team members of this project
            $team_users_data = allteammembers($campaign_id);
            if (is_array($team_users_data)) {
                foreach ($team_users_data as $team) {
                    $team_user_id = $team['user_id'];
                    if ($team_user_id != $user_id and $team_user_id > 0) {
                        $action_detail = sprintf(USER_HAVE_STARTED_COMMENTED_FOR_TEAMMEMBER_USER, $logged_in_username, $comments, $campaign_name);
                        $data_array = array('user_id' => $team_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
                        $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
                    }
                }
            }

            //7. Admin User 
            $admin = 1;
            $action_detail = sprintf(USER_HAVE_STARTED_COMMENTED_ADMIN, $logged_in_username, $comments, $campaign_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('admin_activity', '', '', $data_array);
            break;

        case 'draft_project':
            //1. Logged In User

            $module = 'User';
            $action = $case;
            $project_name = $taxonomy_setting['project_name'];

            $action_detail = sprintf(YOU_CREATED_PROJECT_DRAFT, $project_name, $campaign_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);

            break;
        case 'submit_project':
            //1. Logged In User

            $module = 'User';
            $action = $case;
            $project_name = $taxonomy_setting['project_name'];
            $action_detail = sprintf(YOU_SUBMITED_PROJECT, $project_name, $campaign_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);

            //2. Admin
            $str_name_url = site_url('admin/equity/list_equity');
            $admin_project_link = anchor($str_name_url, $campaign_name);
            $action_detail = sprintf(YOU_SUBMITED_PROJECT_ADMIN, $logged_in_username, $project_name, $admin_project_link);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('admin_activity', '', '', $data_array);
            break;
        default:
            break;
    }

    $date_three = date("Y-m-d", strtotime("-3 Months"));
    $firstDay = date('Y-m-d', strtotime("first day of this month"));
    $sql = "Delete from user_activity WHERE  datetime BETWEEN '" . $date_three . "' AND '" . $firstDay . "' ";
    $query = $CI->db->query($sql);

    $sql = "Delete from admin_activity WHERE  datetime BETWEEN '" . $date_three . "' AND '" . $firstDay . "' ";
    $query = $CI->db->query($sql);
}

/*
	Function name :project_activity_admin()
	Parameter :case='',$user_id=0,$second_user_id=0,$equity_id=0
	Return : all team members list for that equity
	Use : to show the team members list
	user_id is always logged in user id
	
	*/
function project_activity_admin($case = '', $user_id = 0, $second_user_id = 0, $campaign_id = 0, $other_data = array())
{
    $taxonomy_setting = taxonomy_setting();
    $taxonomy_project_url = 'offering';


    $CI =& get_instance();
    $CI->load->model('Startequity_model');
    $CI->load->model('equity_model');
    $datetime = date('Y-m-d H:i:s');
    $logged_in_username = '';
    $second_user_username = '';
    $project_name = '';
    if ($user_id > 0) $logged_in_username = GetUserName($user_id);
    if ($second_user_id > 0) $second_user_username = GetUserName($second_user_id);
    $campaign_name = '';
    $campaign_owner_id = 0;
    $campaign_owner_name = 0;

    if ($campaign_id > 0) {
        $get_project_user_detail = $CI->equity_model->GetAllEquities(0, $campaign_id, $is_featured = 0, $is_status = '', $joinarr = array('user'), $limit = 10, $order = array('equity_id' => 'desc')); // store in cache

        if (is_array($get_project_user_detail)) {
            $company_name = SecureShowData($get_project_user_detail[0]['company_name']);
            $campaign_owner_id = SecureShowData($get_project_user_detail[0]['user_id']);
            $equity_url = $get_project_user_detail[0]['equity_url'];
            $project_url = $taxonomy_setting['project_url'];
            if ($taxonomy_project_url != '') {
                $taxonomy_project_url = $project_url;

            }
            if (!empty($company_name)) {

                $str_name_url = site_url($taxonomy_project_url . '/' . $equity_url);

            } else {

                $str_name_url = site_url($taxonomy_project_url . '/' . $campaign_id);
            }

            $campaign_name = anchor($str_name_url, $company_name);

            $user_name = $get_project_user_detail[0]['user_name'];
            $last_name = $get_project_user_detail[0]['last_name'];
            $str_name = $user_name . ' ' . $last_name;
            $profile_slug = $get_project_user_detail[0]['profile_slug'];
            if (!empty($profile_slug)) {

                $str_name_url = site_url('user/' . $profile_slug);

            } else {

                $str_name_url = site_url('user/' . $user_id);
            }

            $campaign_owner_name = anchor($str_name_url, $str_name);
        }
    }


    switch ($case) {
        case 'project_active':

            $module = 'project';
            $action = $case;

            $site_setting = site_setting();
            $site_name = $site_setting['site_name'];
            $funds = $taxonomy_setting['funds_past'];
            $comments = $taxonomy_setting['comments_past'];
            $following = $taxonomy_setting['followers_past'];
            $project_name = $taxonomy_setting['project_name'];


            //1. Project Owner			
            $action_detail = sprintf(ADMIN_HAVE_ACTIVE_PROJECT_OWNER, $project_name, $campaign_name, $site_name);
            $data_array = array('user_id' => $campaign_owner_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
            //2. Fan user of followed user
            $follower_users_data = allprojectfollowers($campaign_id);
            if (is_array($follower_users_data)) {
                foreach ($follower_users_data as $follower) {
                    $follower_user_id = $follower['user_id'];
                    if ($follower_user_id != $user_id and $follower_user_id > 0) {

                        $action_detail = sprintf(ADMIN_HAVE_ACTIVE_PROJECT_FOR_FOLLOWING_USER, $project_name, $campaign_name, $following, $site_name);
                        $data_array = array('user_id' => $follower_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
                        $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
                    }
                }
            }
            //3. All donors of this project
            $donors_users_data = allfunders($campaign_id);
            if (is_array($donors_users_data)) {
                foreach ($donors_users_data as $donor) {
                    $donor_user_id = $donor['user_id'];
                    if ($donor_user_id != $user_id and $donor_user_id > 0) {
                        $action_detail = sprintf(ADMIN_HAVE_ACTIVE_PROJECT_FOR_DONOR_USER, $project_name, $campaign_name, $funds, $site_name);
                        $data_array = array('user_id' => $donor_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
                        $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
                    }
                }
            }

            //4. All commenter of this project
            $comments_users_data = allcommenter($campaign_id);
            if (is_array($comments_users_data)) {
                foreach ($comments_users_data as $comment) {
                    $comment_user_id = $comment['user_id'];
                    if ($comment_user_id != $user_id and $comment_user_id > 0) {
                        $action_detail = sprintf(ADMIN_HAVE_ACTIVE_PROJECT_FOR_COMMENTER_USER, $project_name, $campaign_name, $comments, $site_name);
                        $data_array = array('user_id' => $comment_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
                        $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
                    }
                }
            }

            //5. All team members of this project
            $team_users_data = allteammembers($campaign_id);
            if (is_array($team_users_data)) {
                foreach ($team_users_data as $team) {
                    $team_user_id = $team['user_id'];
                    if ($team_user_id != $user_id and $team_user_id > 0) {
                        $action_detail = sprintf(ADMIN_HAVE_ACTIVE_PROJECT_FOR_TEAMMEMBER_USER, $project_name, $campaign_name, $site_name);
                        $data_array = array('user_id' => $team_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
                        $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
                    }
                }
            }

            //6. Admin User 
            $admin = 1;
            $action_detail = sprintf(ADMIN_HAVE_ACTIVE_PROJECT, $project_name, $campaign_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('admin_activity', '', '', $data_array);
            break;
        case 'project_inactive':

            $module = 'project';
            $action = $case;

            $site_setting = site_setting();
            $site_name = $site_setting['site_name'];
            $funds = $taxonomy_setting['funds_past'];
            $comments = $taxonomy_setting['comments_past'];
            $following = $taxonomy_setting['followers_past'];
            $project_name = $taxonomy_setting['project_name'];


            //1. Project Owner			
            $action_detail = sprintf(ADMIN_HAVE_INACTIVE_PROJECT_OWNER, $project_name, $campaign_name, $site_name);
            $data_array = array('user_id' => $campaign_owner_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
            //2. Fan user of followed user
            $follower_users_data = allprojectfollowers($campaign_id);
            if (is_array($follower_users_data)) {
                foreach ($follower_users_data as $follower) {
                    $follower_user_id = $follower['user_id'];
                    if ($follower_user_id != $user_id and $follower_user_id > 0) {

                        $action_detail = sprintf(ADMIN_HAVE_INACTIVE_PROJECT_FOR_FOLLOWING_USER, $project_name, $campaign_name, $following, $site_name);
                        $data_array = array('user_id' => $follower_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
                        $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
                    }
                }
            }
            //3. All donors of this project
            $donors_users_data = allfunders($campaign_id);
            if (is_array($donors_users_data)) {
                foreach ($donors_users_data as $donor) {
                    $donor_user_id = $donor['user_id'];
                    if ($donor_user_id != $user_id and $donor_user_id > 0) {
                        $action_detail = sprintf(ADMIN_HAVE_INACTIVE_PROJECT_FOR_DONOR_USER, $project_name, $campaign_name, $funds, $site_name);
                        $data_array = array('user_id' => $donor_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
                        $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
                    }
                }
            }

            //4. All commenter of this project
            $comments_users_data = allcommenter($campaign_id);
            if (is_array($comments_users_data)) {
                foreach ($comments_users_data as $comment) {
                    $comment_user_id = $comment['user_id'];
                    if ($comment_user_id != $user_id and $comment_user_id > 0) {
                        $action_detail = sprintf(ADMIN_HAVE_INACTIVE_PROJECT_FOR_COMMENTER_USER, $project_name, $campaign_name, $comments, $site_name);
                        $data_array = array('user_id' => $comment_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
                        $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
                    }
                }
            }

            //5. All team members of this project
            $team_users_data = allteammembers($campaign_id);
            if (is_array($team_users_data)) {
                foreach ($team_users_data as $team) {
                    $team_user_id = $team['user_id'];
                    if ($team_user_id != $user_id and $team_user_id > 0) {
                        $action_detail = sprintf(ADMIN_HAVE_INACTIVE_PROJECT_FOR_TEAMMEMBER_USER, $project_name, $campaign_name, $site_name);
                        $data_array = array('user_id' => $team_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
                        $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
                    }
                }
            }

            //6. Admin User 
            $admin = 1;
            $action_detail = sprintf(ADMIN_HAVE_INACTIVE_PROJECT, $project_name, $campaign_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('admin_activity', '', '', $data_array);
            break;


        case 'project_approve':
            //1. Owner

            $module = 'User';
            $action = $case;
            $project_name = $taxonomy_setting['project_name'];
            $site_setting = site_setting();
            $site_name = $site_setting['site_name'];
            $action_detail = sprintf(ADMIN_PROJECT_APPROVED_OWNER, $project_name, $campaign_name, $site_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);

            //2. Admin
            $action_detail = sprintf(ADMIN_PROJECT_APPROVED_ADMIN, $project_name, $campaign_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('admin_activity', '', '', $data_array);
            break;
        case 'project_declined':
            //1. Owner

            $module = 'User';
            $action = $case;
            $project_name = $taxonomy_setting['project_name'];
            $site_setting = site_setting();
            $site_name = $site_setting['site_name'];
            $action_detail = sprintf(ADMIN_PROJECT_DECLINED_OWNER, $project_name, $campaign_name, $site_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);

            //2. Admin
            $action_detail = sprintf(ADMIN_PROJECT_DECLINED_ADMIN, $project_name, $campaign_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('admin_activity', '', '', $data_array);
            break;
        case 'project_deleted':
            //1. Owner

            $module = 'User';
            $action = $case;
            $project_name = $taxonomy_setting['project_name'];
            $site_setting = site_setting();
            $site_name = $site_setting['site_name'];
            $action_detail = sprintf(ADMIN_PROJECT_DELETED_OWNER, $project_name, $campaign_name, $site_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);

            //2. Admin
            $action_detail = sprintf(ADMIN_PROJECT_DELETED_ADMIN, $project_name, $campaign_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('admin_activity', '', '', $data_array);
            break;
        default:
            break;
    }

    $date_three = date("Y-m-d", strtotime("-3 Months"));
    $firstDay = date('Y-m-d', strtotime("first day of this month"));
    $sql = "Delete from user_activity WHERE  datetime BETWEEN '" . $date_three . "' AND '" . $firstDay . "' ";
    $query = $CI->db->query($sql);

    $sql = "Delete from admin_activity WHERE  datetime BETWEEN '" . $date_three . "' AND '" . $firstDay . "' ";
    $query = $CI->db->query($sql);
}

/*
	Function name :project_activity_feedback()
	Parameter :case='',$user_id=0,$second_user_id=0,$equity_id=0
	Return : all team members list for that equity
	Use : to show the team members list
	user_id is always logged in user id
	
	*/
function project_activity_feedback($case = '', $user_id = 0, $second_user_id = 0, $campaign_id = 0, $other_data = array())
{
    $taxonomy_setting = taxonomy_setting();
    $taxonomy_project_url = 'offering';


    $CI =& get_instance();
    $CI->load->model('Startequity_model');
    $CI->load->model('equity_model');
    $datetime = date('Y-m-d H:i:s');
    $logged_in_username = '';
    $second_user_username = '';
    $project_name = '';
    if ($user_id > 0) $logged_in_username = GetUserName($user_id);
    if ($second_user_id > 0) $second_user_username = GetUserName($second_user_id);
    $campaign_name = '';
    $campaign_owner_id = 0;
    $campaign_owner_name = 0;

    if ($campaign_id > 0) {
        $get_project_user_detail = $CI->equity_model->GetAllEquities(0, $campaign_id, $is_featured = 0, $is_status = '', $joinarr = array('user'), $limit = 10, $order = array('equity_id' => 'desc')); // store in cache

        if (is_array($get_project_user_detail)) {
            $company_name = SecureShowData($get_project_user_detail[0]['company_name']);
            $campaign_owner_id = SecureShowData($get_project_user_detail[0]['user_id']);
            $equity_url = $get_project_user_detail[0]['equity_url'];
            $project_url = $taxonomy_setting['project_url'];
            if ($taxonomy_project_url != '') {
                $taxonomy_project_url = $project_url;

            }
            if (!empty($company_name)) {

                $str_name_url = site_url($taxonomy_project_url . '/' . $equity_url);

            } else {

                $str_name_url = site_url($taxonomy_project_url . '/' . $campaign_id);
            }

            $campaign_name = anchor($str_name_url, $company_name);

            $user_name = $get_project_user_detail[0]['user_name'];
            $last_name = $get_project_user_detail[0]['last_name'];
            $str_name = $user_name . ' ' . $last_name;
            $profile_slug = $get_project_user_detail[0]['profile_slug'];
            if (!empty($profile_slug)) {

                $str_name_url = site_url('user/' . $profile_slug);

            } else {

                $str_name_url = site_url('user/' . $user_id);
            }

            $campaign_owner_name = anchor($str_name_url, $str_name);
        }
    }


    switch ($case) {

        case 'submit_by_owner':
            //1. Owner

            $module = 'project';
            $action = $case;
            $project_name = $taxonomy_setting['project_name'];
            $site_setting = site_setting();
            $site_name = $site_setting['site_name'];
            $inbox_thread_link = '';
            $action_detail = sprintf(PROJECT_FEEDBACK_OWNER_SUMBIITED_BY_OWENER, $project_name, $inbox_thread_link, $campaign_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);

            //2. Admin
            $action_detail = sprintf(PROJECT_FEEDBACK_ADMIN_SUMBIITED_BY_OWENER, $logged_in_username, $project_name, $inbox_thread_link, $campaign_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('admin_activity', '', '', $data_array);
            break;
        case 'submit_by_admin':
            //1. Owner

            $module = 'project';
            $action = $case;
            $project_name = $taxonomy_setting['project_name'];
            $site_setting = site_setting();
            $site_name = $site_setting['site_name'];
            $inbox_thread_link = '';
            $action_detail = sprintf(PROJECT_FEEDBACK_OWNER_SUMBIITED_BY_ADMIN, $site_name, $project_name, $inbox_thread_link, $campaign_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);

            //2. Admin
            $action_detail = sprintf(PROJECT_FEEDBACK_ADMIN_SUMBIITED_BY_ADMIN, $project_name, $inbox_thread_link, $campaign_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('admin_activity', '', '', $data_array);
            break;
        case 'team_member_added':
            //1. Owner/Logged In user

            $module = 'User';
            $action = $case;
            $project_name = $taxonomy_setting['project_name'];
            $site_setting = site_setting();
            $site_name = $site_setting['site_name'];
            $action_detail = sprintf(TEAM_MEMBER_ADDED_OWNER, $campaign_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
            //2. All team members of this project
            $team_users_data = allteammembers($campaign_id);
            if (is_array($team_users_data)) {
                foreach ($team_users_data as $team) {
                    $team_user_id = $team['user_id'];
                    if ($team_user_id != $user_id and $team_user_id > 0) {
                        $action_detail = sprintf(TEAM_MEMBER_ADDED_USER, $logged_in_username, $campaign_name);
                        $data_array = array('user_id' => $team_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
                        $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
                    }
                }
            }

            //3. Admin
            $action_detail = sprintf(TEAM_MEMBER_ADDED_ADMIN, $logged_in_username, $campaign_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('admin_activity', '', '', $data_array);
            break;
        case 'intrest_request':


            $module = 'project';
            $action = $case;
            $project_name = $taxonomy_setting['project_name'];
            $funds = $taxonomy_setting['funds'];

            //1.Logged User

            $action_detail = sprintf(INTREST_REQUEST_USER, $funds, $campaign_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
            $str_name_url = site_url('equity/dashboard/' . $campaign_id);
            $project_intrest_link = anchor($str_name_url, 'Interest');
            //2. Project Owner			
            $action_detail = sprintf(INTREST_REQUEST_OWNER, $project_intrest_link, $funds, $campaign_name, $logged_in_username);
            $data_array = array('user_id' => $campaign_owner_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
            break;
        case 'access_request':


            $module = 'project';
            $action = $case;
            $project_name = $taxonomy_setting['project_name'];
            $funds = $taxonomy_setting['funds'];
            $access = '';
            if (isset($other_data['access'])) $access = $other_data['access'];
            //1.Logged User

            $action_detail = sprintf(ACCESS_REQUEST_USER, $campaign_name, $access);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
            $str_name_url = site_url('equity/dashboard/' . $campaign_id);
            $project_intrest_link = anchor($str_name_url, 'Access Request');
            //2. Project Owner			
            $action_detail = sprintf(ACCESS_REQUEST_OWNER, $project_intrest_link, $access, $campaign_name, $logged_in_username);
            $data_array = array('user_id' => $campaign_owner_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
            break;
        default:
            break;
    }

    $date_three = date("Y-m-d", strtotime("-3 Months"));
    $firstDay = date('Y-m-d', strtotime("first day of this month"));
    $sql = "Delete from user_activity WHERE  datetime BETWEEN '" . $date_three . "' AND '" . $firstDay . "' ";
    $query = $CI->db->query($sql);

    $sql = "Delete from admin_activity WHERE  datetime BETWEEN '" . $date_three . "' AND '" . $firstDay . "' ";
    $query = $CI->db->query($sql);
}

/*
	Function name :admin_activity()
	Parameter :case='',$user_id=0,$second_user_id=0,$equity_id=0
	Return : all team members list for that equity
	Use : to show the team members list
	user_id is always logged in user id
	
	*/
function admin_activity($case = '', $user_id = 0, $second_user_id = 0, $campaign_id = 0, $other_data = array())
{
    $taxonomy_setting = taxonomy_setting();
    $taxonomy_project_url = 'offering';


    $CI =& get_instance();
    $CI->load->model('Startequity_model');
    $CI->load->model('equity_model');
    $datetime = date('Y-m-d H:i:s');
    $logged_in_username = '';
    $second_user_username = '';
    $project_name = '';
    if ($user_id > 0) $logged_in_username = GetUserName($user_id);
    if ($second_user_id > 0) $second_user_username = GetUserName($second_user_id);
    $campaign_name = '';
    $campaign_owner_id = 0;
    $campaign_owner_name = 0;

    if ($campaign_id > 0) {
        $get_project_user_detail = $CI->equity_model->GetAllEquities(0, $campaign_id, $is_featured = 0, $is_status = '', $joinarr = array('user'), $limit = 10, $order = array('equity_id' => 'desc')); // store in cache

        if (is_array($get_project_user_detail)) {
            $company_name = SecureShowData($get_project_user_detail[0]['company_name']);
            $campaign_owner_id = SecureShowData($get_project_user_detail[0]['user_id']);
            $equity_url = $get_project_user_detail[0]['equity_url'];
            $project_url = $taxonomy_setting['project_url'];
            if ($taxonomy_project_url != '') {
                $taxonomy_project_url = $project_url;

            }
            if (!empty($company_name)) {

                $str_name_url = site_url($taxonomy_project_url . '/' . $equity_url);

            } else {

                $str_name_url = site_url($taxonomy_project_url . '/' . $campaign_id);
            }

            $campaign_name = anchor($str_name_url, $company_name);

            $user_name = $get_project_user_detail[0]['user_name'];
            $last_name = $get_project_user_detail[0]['last_name'];
            $str_name = $user_name . ' ' . $last_name;
            $profile_slug = $get_project_user_detail[0]['profile_slug'];
            if (!empty($profile_slug)) {

                $str_name_url = site_url('user/' . $profile_slug);

            } else {

                $str_name_url = site_url('user/' . $user_id);
            }

            $campaign_owner_name = anchor($str_name_url, $str_name);
        }
    }


    switch ($case) {

        case 'upload_contract':
            //1. Owner

            $module = 'project';
            $action = $case;
            $project_name = $taxonomy_setting['project_name'];
            $funds = $taxonomy_setting['funds'];
            $site_setting = site_setting();
            $site_name = $site_setting['site_name'];
            $str_name_url = site_url('startinvest/index/' . $campaign_id);
            $investmentproces_link = anchor($str_name_url, UPLOADED);

            $action_detail = sprintf(UPLOAD_CONTRACT_DOCUMENT_USER, $investmentproces_link, $funds, $campaign_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);

            //2. Admin
            $action_detail = sprintf(UPLOAD_CONTRACT_DOCUMENT_ADMIN, $logged_in_username, $investmentproces_link, $funds, $campaign_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('admin_activity', '', '', $data_array);
            break;
        case 'approve_contract':
            //1. Owner

            $module = 'project';
            $action = $case;
            $project_name = $taxonomy_setting['project_name'];
            $funds = $taxonomy_setting['funds'];
            $site_setting = site_setting();
            $site_name = $site_setting['site_name'];
            $str_name_url = site_url('startinvest/index/' . $campaign_id);
            $investmentproces_link = anchor($str_name_url, APPROVE);

            $action_detail = sprintf(APPROVED_CONTRACT_DOCUMENT_USER, $funds, $investmentproces_link, $campaign_name, $site_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);

            //2. Admin
            $action_detail = sprintf(APPROVED_CONTRACT_DOCUMENT_ADMIN, $investmentproces_link, $funds, $logged_in_username, $campaign_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('admin_activity', '', '', $data_array);
            break;
        case 'declined_contract':
            //1. Owner

            $module = 'project';
            $action = $case;
            $project_name = $taxonomy_setting['project_name'];
            $funds = $taxonomy_setting['funds'];
            $site_setting = site_setting();
            $site_name = $site_setting['site_name'];
            $str_name_url = site_url('startinvest/index/' . $campaign_id);
            $investmentproces_link = anchor($str_name_url, REJECT_COMMENT);

            $action_detail = sprintf(DECLINED_CONTRACT_DOCUMENT_USER, $funds, $investmentproces_link, $campaign_name, $site_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);

            //2. Admin
            $action_detail = sprintf(DECLINED_CONTRACT_DOCUMENT_ADMIN, $investmentproces_link, $funds, $logged_in_username, $campaign_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('admin_activity', '', '', $data_array);
            break;
        case 'acknowledge_contract':
            //1. Owner

            $module = 'project';
            $action = $case;
            $project_name = $taxonomy_setting['project_name'];
            $funds = $taxonomy_setting['funds'];
            $site_setting = site_setting();
            $site_name = $site_setting['site_name'];
            $str_name_url = site_url('startinvest/index/' . $campaign_id);
            $investmentproces_link = anchor($str_name_url, 'confirmed & uploaded');

            $action_detail = sprintf(ACKNOWLEDGEMENT_CONTRACT_DOCUMENT_USER, $investmentproces_link, $funds, $campaign_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);

            //2. Admin
            $action_detail = sprintf(ACKNOWLEDGEMENT_DOCUMENT_ADMIN, $logged_in_username, $investmentproces_link, $funds, $campaign_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('admin_activity', '', '', $data_array);
            break;
        case 'approve_acknowledge_contract':
            //1. Owner

            $module = 'project';
            $action = $case;
            $project_name = $taxonomy_setting['project_name'];
            $funds = $taxonomy_setting['funds'];
            $site_setting = site_setting();
            $site_name = $site_setting['site_name'];
            $str_name_url = site_url('startinvest/index/' . $campaign_id);
            $investmentproces_link = anchor($str_name_url, APPROVED);

            $action_detail = sprintf(APPROVE_ACKNOWLEDGEMENT_CONTRACT_DOCUMENT_USER, $funds, $investmentproces_link, $campaign_name, $site_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);

            //2. Admin
            $action_detail = sprintf(APPROVE_ACKNOWLEDGEMENT_DOCUMENT_ADMIN, $investmentproces_link, $funds, $logged_in_username, $campaign_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('admin_activity', '', '', $data_array);
            break;
        case 'declined_acknowledge_contract':
            //1. Owner

            $module = 'project';
            $action = $case;
            $project_name = $taxonomy_setting['project_name'];
            $funds = $taxonomy_setting['funds'];
            $site_setting = site_setting();
            $site_name = $site_setting['site_name'];
            $str_name_url = site_url('startinvest/index/' . $campaign_id);
            $investmentproces_link = anchor($str_name_url, DECLINED);

            $action_detail = sprintf(DECLINED_ACKNOWLEDGEMENT_CONTRACT_DOCUMENT_USER, $funds, $investmentproces_link, $campaign_name, $site_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);

            //2. Admin
            $action_detail = sprintf(DECLINED_ACKNOWLEDGEMENT_DOCUMENT_ADMIN, $investmentproces_link, $funds, $logged_in_username, $campaign_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('admin_activity', '', '', $data_array);
            break;
        case 'submmited_tracking':
            //1. Owner

            $module = 'project';
            $action = $case;
            $project_name = $taxonomy_setting['project_name'];
            $funds = $taxonomy_setting['funds'];
            $site_setting = site_setting();
            $site_name = $site_setting['site_name'];
            $str_name_url = site_url('startinvest/index/' . $campaign_id);
            $investmentproces_link = anchor($str_name_url, UPLOADED);

            $action_detail = sprintf(SUBMITTED_TRACKING_CONTRACT_DOCUMENT_USER, $site_name, $investmentproces_link, $campaign_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);

            //2. Admin
            $action_detail = sprintf(SUBMITTED_TRACKING_DOCUMENT_ADMIN, $investmentproces_link, $campaign_name, $logged_in_username);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('admin_activity', '', '', $data_array);
            break;
        case 'confirmed_submmited_tracking':
            //1. Owner

            $module = 'project';
            $action = $case;
            $project_name = $taxonomy_setting['project_name'];
            $funds = $taxonomy_setting['funds'];
            $site_setting = site_setting();
            $site_name = $site_setting['site_name'];
            $str_name_url = site_url('startinvest/index/' . $campaign_id);
            $investmentproces_link = anchor($str_name_url, APPROVED_AND_CONFIRMED);

            $action_detail = sprintf(CONFIRMED_SUBMITTED_TRACKING_CONTRACT_DOCUMENT_USER, $investmentproces_link, $campaign_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);

            //2. Admin
            $action_detail = sprintf(CONFIRMED_SUBMITTED_TRACKING_DOCUMENT_ADMIN, $logged_in_username, $investmentproces_link, $campaign_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('admin_activity', '', '', $data_array);
            break;
        case 'project_success':
            $module = 'project';
            $action = $case;

            $site_setting = site_setting();
            $site_name = $site_setting['site_name'];
            $funds = $taxonomy_setting['funds_past'];
            $comments = $taxonomy_setting['comments_past'];
            $following = $taxonomy_setting['followers_past'];
            $project_name = $taxonomy_setting['project_name'];


            //1. Project Owner			
            $action_detail = sprintf(ADMIN_HAVE_SUCCESS_PROJECT_OWNER, $project_name, $campaign_name);
            $data_array = array('user_id' => $campaign_owner_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
            //2. Fan user of followed user
            $follower_users_data = allprojectfollowers($campaign_id);
            if (is_array($follower_users_data)) {
                foreach ($follower_users_data as $follower) {
                    $follower_user_id = $follower['user_id'];
                    if ($follower_user_id != $user_id and $follower_user_id > 0) {

                        $action_detail = sprintf(ADMIN_HAVE_SUCCESS_PROJECT_FOR_FOLLOWING_USER, $project_name, $campaign_name, $following);
                        $data_array = array('user_id' => $follower_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
                        $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
                    }
                }
            }
            //3. All donors of this project
            $donors_users_data = allfunders($campaign_id);
            if (is_array($donors_users_data)) {
                foreach ($donors_users_data as $donor) {
                    $donor_user_id = $donor['user_id'];
                    if ($donor_user_id != $user_id and $donor_user_id > 0) {
                        $action_detail = sprintf(ADMIN_HAVE_SUCCESS_PROJECT_FOR_DONOR_USER, $project_name, $campaign_name, $funds);
                        $data_array = array('user_id' => $donor_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
                        $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
                    }
                }
            }

            //4. All commenter of this project
            $comments_users_data = allcommenter($campaign_id);
            if (is_array($comments_users_data)) {
                foreach ($comments_users_data as $comment) {
                    $comment_user_id = $comment['user_id'];
                    if ($comment_user_id != $user_id and $comment_user_id > 0) {
                        $action_detail = sprintf(ADMIN_HAVE_SUCCESS_PROJECT_FOR_COMMENTER_USER, $project_name, $campaign_name, $comments);
                        $data_array = array('user_id' => $comment_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
                        $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
                    }
                }
            }

            //5. All team members of this project
            $team_users_data = allteammembers($campaign_id);
            if (is_array($team_users_data)) {
                foreach ($team_users_data as $team) {
                    $team_user_id = $team['user_id'];
                    if ($team_user_id != $user_id and $team_user_id > 0) {
                        $action_detail = sprintf(ADMIN_HAVE_SUCCESS_PROJECT_FOR_TEAMMEMBER_USER, $project_name, $campaign_name);
                        $data_array = array('user_id' => $team_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
                        $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
                    }
                }
            }

            //6. Admin User 
            $admin = 1;
            $action_detail = sprintf(ADMIN_HAVE_SUCCESS_PROJECT, $project_name, $campaign_name, $campaign_owner_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('admin_activity', '', '', $data_array);


            break;
        case 'project_unsuccess':

            $module = 'project';
            $action = $case;

            $site_setting = site_setting();
            $site_name = $site_setting['site_name'];
            $funds = $taxonomy_setting['funds_past'];
            $comments = $taxonomy_setting['comments_past'];
            $following = $taxonomy_setting['followers_past'];
            $project_name = $taxonomy_setting['project_name'];


            //1. Project Owner			
            $action_detail = sprintf(ADMIN_HAVE_UNSUCCESS_PROJECT_OWNER, $project_name, $campaign_name);
            $data_array = array('user_id' => $campaign_owner_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
            //2. Fan user of followed user
            $follower_users_data = allprojectfollowers($campaign_id);
            if (is_array($follower_users_data)) {
                foreach ($follower_users_data as $follower) {
                    $follower_user_id = $follower['user_id'];
                    if ($follower_user_id != $user_id and $follower_user_id > 0) {

                        $action_detail = sprintf(ADMIN_HAVE_UNSUCCESS_PROJECT_FOR_FOLLOWING_USER, $project_name, $campaign_name, $following);
                        $data_array = array('user_id' => $follower_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
                        $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
                    }
                }
            }
            //3. All donors of this project
            $donors_users_data = allfunders($campaign_id);
            if (is_array($donors_users_data)) {
                foreach ($donors_users_data as $donor) {
                    $donor_user_id = $donor['user_id'];
                    if ($donor_user_id != $user_id and $donor_user_id > 0) {
                        $action_detail = sprintf(ADMIN_HAVE_UNSUCCESS_PROJECT_FOR_DONOR_USER, $project_name, $campaign_name, $funds);
                        $data_array = array('user_id' => $donor_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
                        $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
                    }
                }
            }

            //4. All commenter of this project
            $comments_users_data = allcommenter($campaign_id);
            if (is_array($comments_users_data)) {
                foreach ($comments_users_data as $comment) {
                    $comment_user_id = $comment['user_id'];
                    if ($comment_user_id != $user_id and $comment_user_id > 0) {
                        $action_detail = sprintf(ADMIN_HAVE_UNSUCCESS_PROJECT_FOR_COMMENTER_USER, $project_name, $campaign_name, $comments);
                        $data_array = array('user_id' => $comment_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
                        $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
                    }
                }
            }

            //5. All team members of this project
            $team_users_data = allteammembers($campaign_id);
            if (is_array($team_users_data)) {
                foreach ($team_users_data as $team) {
                    $team_user_id = $team['user_id'];
                    if ($team_user_id != $user_id and $team_user_id > 0) {
                        $action_detail = sprintf(ADMIN_HAVE_UNSUCCESS_PROJECT_FOR_TEAMMEMBER_USER, $project_name, $campaign_name);
                        $data_array = array('user_id' => $team_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
                        $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
                    }
                }
            }

            //6. Admin User 
            $admin = 1;
            $action_detail = sprintf(ADMIN_HAVE_UNSUCCESS_PROJECT, $project_name, $campaign_name, $campaign_owner_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'campaign_id' => $campaign_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('admin_activity', '', '', $data_array);

            break;
        default:
            break;
    }

    $date_three = date("Y-m-d", strtotime("-3 Months"));
    $firstDay = date('Y-m-d', strtotime("first day of this month"));
    $sql = "Delete from user_activity WHERE  datetime BETWEEN '" . $date_three . "' AND '" . $firstDay . "' ";
    $query = $CI->db->query($sql);

    $sql = "Delete from admin_activity WHERE  datetime BETWEEN '" . $date_three . "' AND '" . $firstDay . "' ";
    $query = $CI->db->query($sql);
}

/*
	Function name :GetUserName()
	Parameter :$user_id 
	Return : return username
	Use : to show username
	
	*/
function GetUserName($user_id = 0)
{

    $CI =& get_instance();

    $user_name = '';
    if ($user_id > 0) {
        $user_data = UserData($user_id, array());
        if (is_array($user_data)) {

            $user_name = $user_data[0]['user_name'];
            $last_name = $user_data[0]['last_name'];
            $str_name = $user_name . ' ' . $last_name;
            $profile_slug = $user_data[0]['profile_slug'];
            if (!empty($profile_slug)) {

                $str_name_url = site_url('user/' . $profile_slug);

            } else {

                $str_name_url = site_url('user/' . $user_id);
            }

            $user_name = anchor($str_name_url, $str_name);

        }
    }

    return $user_name;
}

/*
	Function name :GetCampaignName()
	Parameter :$campaign_id 
	Return : return campaign name
	Use : to show campaign name
	
	*/

function GetCampaignName($campaign_id = 0)
{
    $CI =& get_instance();
    $CI->load->model('equity_model');
    $taxonomy_setting = taxonomy_setting();
    $project_url = $taxonomy_setting['project_url'];

    $campaign_name = '';
    if ($campaign_id > 0) {
        $get_project_user_detail = $CI->equity_model->GetAllEquities($user_id = 0, $campaign_id, $is_featured = 0, $is_status = '', $joinarr = array('user'), $limit = 10, $order = array('equity_id' => 'desc')); // store in cache
        if (is_array($get_project_user_detail)) {
            $company_name = SecureShowData($get_project_user_detail[0]['company_name']);
            $equity_url = $get_project_user_detail[0]['equity_url'];
            if (!empty($company_name)) {

                $str_name_url = site_url($project_url.'/' . $equity_url);

            } else {

                $str_name_url = site_url($project_url.'/' . $campaign_id);
            }

            $campaign_name = anchor($str_name_url, $str_name);
        }
    }

    return $campaign_name;

}

/*
	Function name :allteammembers()
	Parameter :$equity_id 
	Return : all team members list for that equity
	Use : to show the team members list
	
	*/

function allteammembers($campaign_id = 0)
{
    $CI =& get_instance();
    $team_member_data = '';
    $CI->load->model('Startequity_model');
    if ($campaign_id > 0) $team_member_data = $CI->Startequity_model->company_team_member_data($campaign_id);

    return $team_member_data;

}

/*
	Function name :allfunders()
	Parameter :$equity_id 
	Return : all funders list for that equity
	Use : to show the funders list
	
	*/
function allfunders($campaign_id = 0)
{
    $CI =& get_instance();
    $funder_data = '';
    $CI->load->model('account_model');
    if ($campaign_id > 0) $funder_data = $CI->account_model->GetDonation('funder', 0, $campaign_id, '', $join = array('user'), $limit = 10000, $order = array('transaction_id' => 'desc'));
    return $funder_data;
}

/*
	Function name :allcommenter()
	Parameter :$equity_id 
	Return : all commenter list for that equity
	Use : to show the commenter list
	
	*/
function allcommenter($campaign_id = 0)
{

    $CI =& get_instance();
    $comment_data = '';
    $CI->load->model('account_model');
    if ($campaign_id > 0) {
        if (!$CI->simple_cache->is_cached('equity_detail_comments' . $campaign_id)) {
            $comment_data = $CI->account_model->GetUpdateCommentGallery(0, $campaign_id, '', $join = array('comment', 'user'), 10000, $group = array('comment_id' => 'comment.comment_id'), $order = array('property.property_id' => 'desc', 'comment.comment_id' => 'desc'), 1);
            $CI->simple_cache->cache_item('equity_detail_comments' . $campaign_id, $comment_data);
        } else {
            $comment_data = $CI->simple_cache->get_item('equity_detail_comments' . $campaign_id);
        }
    }
    return $comment_data;
}

/*
	Function name :allprojectfollowers()
	Parameter :$equity_id 
	Return : all followers list for that equity
	Use : to show the followers list
	
	*/
function allprojectfollowers($campaign_id = 0)
{
    $CI =& get_instance();
    $follow_data = '';
    $CI->load->model('follower_model');
    if ($campaign_id > 0) {
        if (!$CI->simple_cache->is_cached('property_detail_followers' . $campaign_id)) {
            $follow_data = $CI->follower_model->GetAllFollower($type = 'property_follow', $campaign_id, 0, $join = array('user'), $limit = 10, $order = array('property_follow_id' => 'desc'));
            $CI->simple_cache->cache_item('property_detail_followers' . $campaign_id, $follow_data);
        } else {
            $follow_data = $CI->simple_cache->get_item('property_detail_followers' . $campaign_id);
        }
    }
    return $follow_data;
}

/*
	Function name :allprojectfollowers()
	Parameter :$user_id 
	Return : all followers list for that user
	Use : to show the followers list
	
	*/
function alluserfollowers($user_id = 0)
{
    $CI =& get_instance();
    $follow_data = '';
    $CI->load->model('follower_model');
    if ($user_id > 0) {
        if (!$CI->simple_cache->is_cached('user_followers_' . $user_id)) {
            $follow_data = $CI->follower_model->GetAllFollower($type = 'user_follow', 0, $user_id, $join = array('user'), $limit = 10000, $order = array('follower_id' => 'desc'));
            $CI->simple_cache->cache_item('user_followers_' . $user_id, $follow_data);
        } else {
            $follow_data = $CI->simple_cache->get_item('user_followers_' . $user_id);
        }
    }
    return $follow_data;
}

/*
    Function name :allcompanyteammembers()
    Parameter :$company_id 
    Return : all team members list for that company
    Use : to show the team members list
    
    */

function allcompanyteammembers($company_id = 0)
{
    $CI =& get_instance();
    $team_member_data = '';
    $CI->load->model('Startequity_model');
    if ($company_id > 0) $team_member_data = $CI->Startequity_model->company_team_member_data($company_id);

    return $team_member_data;

}


/*
    Function name :allprojectfollowers()
    Parameter :$equity_id 
    Return : all followers list for that equity
    Use : to show the followers list
    
    */
function allcompanyfollowers($company_id = 0)
{
    $CI =& get_instance();
    $follow_data = '';
    $CI->load->model('follower_model');
    if ($company_id > 0) {
        if (!$CI->simple_cache->is_cached('company_detail_followers' . $company_id)) {
            $follow_data = $CI->follower_model->GetAllFollower($type = 'company_follow', 0, 0, $join = array('user'), $limit = 10, $order = array('company_follow_id' => 'desc'),'',$company_id);
            $CI->simple_cache->cache_item('company_detail_followers' . $company_id, $follow_data);
        } else {
            $follow_data = $CI->simple_cache->get_item('company_detail_followers' . $company_id);
        }
    }
    return $follow_data;
}

/*
    Function name :project_activity()
    Parameter :case='',$user_id=0,$second_user_id=0,$equity_id=0
    Return : all team members list for that equity
    Use : to show the team members list
    user_id is always logged in user id
    
    */
function company_activity($case = '', $user_id = 0, $second_user_id = 0, $company_id = 0, $other_data = array())
{
    $taxonomy_setting = taxonomy_setting();
    $taxonomy_company_url = 'company';


    $CI =& get_instance();
    $CI->load->model('Startequity_model');
    $CI->load->model('company_model');
    $datetime = date('Y-m-d H:i:s');
    $logged_in_username = '';
    $second_user_username = '';
    $project_name = '';
    if ($user_id > 0) $logged_in_username = GetUserName($user_id);
    if ($second_user_id > 0) $second_user_username = GetUserName($second_user_id);
    $campaign_name = '';
    $campaign_owner_id = 0;
    $campaign_owner_name = 0;

    if ($company_id > 0) {
        $get_company_user_detail = $CI->company_model->GetAllCompanies($user_id = 0, $company_id,$joinarr = array(
            'user'

        ), $limit = 10, $order = array(
            'company_id' => 'desc'
        ));

        if (is_array($get_company_user_detail)) {
            $company_name = SecureShowData($get_company_user_detail[0]['company_name']);
            $campaign_owner_id = SecureShowData($get_company_user_detail[0]['user_id']);
            $company_site_url = $get_company_user_detail[0]['company_url'];
            $company_url = $taxonomy_setting['company_url'];
          
            if (!empty($company_name)) {

                $str_name_url = site_url($company_url . '/' . $company_site_url);

            } else {

                $str_name_url = site_url($company_url . '/' . $company_id);
            }   
           
            $campaign_name = anchor($str_name_url, $company_name);

            $user_name = $get_company_user_detail[0]['user_name'];
            $last_name = $get_company_user_detail[0]['last_name'];
            $str_name = $user_name . ' ' . $last_name;
            $profile_slug = $get_company_user_detail[0]['profile_slug'];
            if (!empty($profile_slug)) {

                $str_name_url = site_url('user/' . $profile_slug);

            } else {

                $str_name_url = site_url('user/' . $user_id);
            }

            $campaign_owner_name = anchor($str_name_url, $str_name);
        }
    }


    switch ($case) {

        case 'follow':

            $module = 'project';
            $action = $case;

            $following = $taxonomy_setting['followers_gerund'];
            $funds = $taxonomy_setting['funds_past'];
            $comments = $taxonomy_setting['comments_past'];
            //1.Fan User

            $action_detail = sprintf(YOU_HAVE_STARTED_FOLLOWING, $following, $campaign_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'company_id' => $company_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);

            //2. Followed User          
            $action_detail = sprintf(USER_HAVE_STARTED_FOLLOWING_YOUR_CAMPAIGNS, $logged_in_username, $following, $campaign_name);
            $data_array = array('user_id' => $campaign_owner_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $user_id, 'company_id' => $company_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
            //3. Fan user of followed user
            $follower_users_data = allcompanyfollowers($company_id);
            if (is_array($follower_users_data)) {
                foreach ($follower_users_data as $follower) {
                    $follower_user_id = $follower['user_id'];
                    if ($follower_user_id != $user_id and $follower_user_id > 0) {

                        $action_detail = sprintf(USER_HAVE_STARTED_FOLLOWING_USER_ON_CAMPAIGNS, $logged_in_username, $following, $campaign_name, $following);
                        $data_array = array('user_id' => $follower_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'company_id' => $company_id);
                        $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
                    }
                }
            }
     

            //6. All team members of this project
            $team_users_data = allcompanyteammembers($company_id);
            if (is_array($team_users_data)) {
                foreach ($team_users_data as $team) {
                    $team_user_id = $team['user_id'];
                    if ($team_user_id != $user_id and $team_user_id > 0) {
                        $action_detail = sprintf(USER_HAVE_STARTED_TEAMMEMBER_USER_ON_CAMPAIGNS, $logged_in_username, $following, $campaign_name);
                        $data_array = array('user_id' => $team_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'company_id' => $company_id);
                        $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
                    }
                }
            }

            //7. Admin User 
            $admin = 1;
            $action_detail = sprintf(USER_HAVE_STARTED_FOLLOWING_ADMIN, $logged_in_username, $following, $campaign_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'company_id' => $company_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('admin_activity', '', '', $data_array);
            break;

        case 'unfollow':


            $module = 'company';
            $action = $case;


            $following = $taxonomy_setting['unfollowers_gerund'];
            $funds = $taxonomy_setting['funds_past'];
            $comments = $taxonomy_setting['comments_past'];
            //1.Fan User

            $action_detail = sprintf(YOU_HAVE_STARTED_UNFOLLOWING, $following, $campaign_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'company_id' => $company_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);

            //2. Followed User          
            $action_detail = sprintf(USER_HAVE_STARTED_UNFOLLOWING_YOUR_CAMPAIGNS, $logged_in_username, $following, $campaign_name);
            $data_array = array('user_id' => $campaign_owner_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $user_id, 'company_id' => $company_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
            //3. Fan user of followed user
            $follower_users_data = allprojectfollowers($company_id);
            if (is_array($follower_users_data)) {
                foreach ($follower_users_data as $follower) {
                    $follower_user_id = $follower['user_id'];
                    if ($follower_user_id != $user_id) {

                        $action_detail = sprintf(USER_HAVE_STARTED_UNFOLLOWING_USER_ON_CAMPAIGNS, $logged_in_username, $following, $campaign_name, $following);
                        $data_array = array('user_id' => $follower_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'company_id' => $company_id);
                        $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
                    }
                }
            }
            break;


            //6. All team members of this project
            $team_users_data = allteammembers($company_id);
            if (is_array($team_users_data)) {
                foreach ($team_users_data as $team) {
                    $team_user_id = $team['user_id'];
                    if ($team_user_id != $user_id) {
                        $action_detail = sprintf(USER_HAVE_STARTED_TEAMMEMBER_USER_ON_CAMPAIGNS_UNFOLLOW, $logged_in_username, $following, $campaign_name);
                        $data_array = array('user_id' => $team_user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'company_id' => $company_id);
                        $activity_id = $CI->Startequity_model->AddInsertUpdateTable('user_activity', '', '', $data_array);
                    }
                }
            }

            //7. Admin User 
            $admin = 1;
            $action_detail = sprintf(USER_HAVE_STARTED_FOLLOWING_ADMIN_UNFOLLOW, $logged_in_username, $following, $campaign_name);
            $data_array = array('user_id' => $user_id, 'module' => $module, 'action' => $action, 'action_detail' => $action_detail, 'datetime' => $datetime, 'second_user_id' => $second_user_id, 'company_id' => $company_id);
            $activity_id = $CI->Startequity_model->AddInsertUpdateTable('admin_activity', '', '', $data_array);
            break;
        
       
        default:
            break;
    }

    $date_three = date("Y-m-d", strtotime("-3 Months"));
    $firstDay = date('Y-m-d', strtotime("first day of this month"));
    $sql = "Delete from user_activity WHERE  datetime BETWEEN '" . $date_three . "' AND '" . $firstDay . "' ";
    $query = $CI->db->query($sql);

    $sql = "Delete from admin_activity WHERE  datetime BETWEEN '" . $date_three . "' AND '" . $firstDay . "' ";
    $query = $CI->db->query($sql);
}


?>
