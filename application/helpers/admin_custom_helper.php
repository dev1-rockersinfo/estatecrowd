<?php







function get_total_subscription($nwid = '')


{


    $CI =& get_instance();


    $query = $CI->db->query("select * from newsletter_subscribe where newsletter_id='" . $nwid . "'");


    if ($query->num_rows() > 0) {


        return $query->num_rows();


    }


    return 0;


}


//Common function to fetch data for any table in admin panel


function get_table_data($table_name = '', $order_by = array(), $array = 'no')
{


    $CI =& get_instance();


    if (is_array($order_by)) {


        foreach ($order_by as $key => $val) {


            $CI->db->order_by($key, $val);


        }


    }


    $query = $CI->db->get($table_name);


    //echo $CI->db->last_query();die();


    if ($query->num_rows() > 0) {


        if ($array == 'yes') {


            return $query->result_array();


        } else {


            return $query->result();


        }


    }


    return 0;


}


//To check admin rights and login


function get_rights($rights_name)


{


    $CI =& get_instance();


    if ($CI->session->userdata('admin_id') == "") {


        redirect('admin/home');


    }


    //echo $rights_name;


    $right_detail = $CI->db->query("select * from rights where rights_name='" . trim($rights_name) . "'");


    if ($right_detail->num_rows() > 0) {


        $right_result = $right_detail->row();


        $rights_id = $right_result->rights_id;


        $query = $CI->db->query("select * from rights_assign where rights_id='" . $rights_id . "' and admin_id='" . $CI->session->userdata('admin_id') . "'");


        if ($query->num_rows() > 0) {


            $result = $query->row();


            if ($result->rights_set == '1' || $result->rights_set == 1) {


                return 1;


            } else {


                return 0;


            }


        } else {


            return 0;


        }


    } else {


        return 0;


    }


}


//////===== front side url ======


function upload_url()


{


    $CI =& get_instance();


    return $base_path = $CI->config->slash_item('base_url_site');


}


function front_base_url()


{


    $CI =& get_instance();


    $chk_index = $CI->config->slash_item('index_page');


    if ($chk_index != '') {


        $base_path = $CI->config->slash_item('base_url_site') . $chk_index;


    } else {


        $base_path = $CI->config->slash_item('base_url_site');


    }


    return $base_path;


}


function learn_category()
{


    $CI =& get_instance();


    $query = $CI->db->query("select * from learn_more_category where active=1 order by category_id desc");


    if ($query->num_rows() > 0) {


        return $query->result_array();


    }


    return 0;


}


/********** get payment gateway details **********/


function get_normal_paypal_detail()


{


    $CI =& get_instance();


    $query = $CI->db->query("select * from site_setting where site_setting_id='2'");


    if ($query->num_rows() > 0) {


        return $query->row();


    }


    return 0;


}


function get_adaptive_paypal_detail()


{


    $CI =& get_instance();


    $query = $CI->db->query("select * from paypal where id='1'");


    if ($query->num_rows() > 0) {


        return $query->row();


    }


    return 0;


}


function get_amazon_detail()


{


    $CI =& get_instance();


    $query = $CI->db->query("select * from amazon where id='1'");


    if ($query->num_rows() > 0) {


        return $query->row();


    }


    return 0;


}


function get_wallet_detail()


{


    $CI =& get_instance();


    $query = $CI->db->query("select * from wallet_setting where wallet_id='1'");


    if ($query->num_rows() > 0) {


        return $query->row();


    }


    return 0;


}


/*******  end ************/


/*Get user detail*/


function get_user_detail($id)


{


    $CI =& get_instance();


    $query = $CI->db->get_where("user", array("user_id" => $id));


    return $query->row_array();


}


function get_admin_detail($id)


{


    $CI =& get_instance();


    $query = $CI->db->get_where("admin", array("admin_id" => $id));


    return $query->row_array();


}


function AdminUserData($id)


{


    $CI =& get_instance();


    $query = $CI->db->get_where("admin", array("admin_id" => $id));


    return $query->result_array();


}


function get_city()


{


    $CI =& get_instance();


    $CI->db->where('active', 1);


    $CI->db->order_by('city_name', 'asc');


    $query = $CI->db->get('city');


    return $query->result();


}


function get_country()


{


    $CI =& get_instance();


    $CI->db->where('active', 1);


    $CI->db->order_by('country_name', 'asc');


    $query = $CI->db->get('country');


    return $query->result();


}


function get_state()


{


    $CI =& get_instance();


    $CI->db->where('active', 1);


    $CI->db->order_by('state_name', 'asc');


    $query = $CI->db->get('state');


    return $query->result();


}


function get_city_name($city_id = '')


{


    if (is_numeric($city_id)) {


        $CI =& get_instance();


        $query = $CI->db->get_where('city', array('city_id' => $city_id));


        if ($query->num_rows() > 0) {


            $result = $query->row();


            return $result->city_name;


        }


    } else {


        return $city_id;


    }


}


function get_country_name($country_id = '')


{


    if (is_numeric($country_id)) {


        $CI =& get_instance();


        $query = $CI->db->get_where('country', array('country_id' => $country_id));


        if ($query->num_rows() > 0) {


            $result = $query->row();


            return $result->country_name;


        }


    } else {


        return $country_id;


    }


}


function get_state_name($state_id = '')


{


    if (is_numeric($state_id)) {


        $CI =& get_instance();


        $query = $CI->db->get_where('state', array('state_id' => $state_id));


        if ($query->num_rows() > 0) {


            $result = $query->row();


            return $result->state_name;


        }


    } else {


        return $state_id;


    }


}


function get_countrywise_state($country_id = '')


{


    $CI =& get_instance();


    $query = $CI->db->get_where('state', array('country_id' => $country_id));


    if ($query->num_rows() > 0) {


        return $query->result();


    }


    return 0;


}


function get_statewise_city($state_id = '')


{


    $CI =& get_instance();


    $query = $CI->db->get_where('city', array('state_id' => $state_id));


    if ($query->num_rows() > 0) {


        return $query->result();


    }


    return 0;


}


function get_project_user($id)


{


    $CI =& get_instance();


    $CI->db->select('*');


    $CI->db->where('project_id', $id);


    $CI->db->from('project');


    $CI->db->join('user', 'project.user_id = user.user_id');


    $query = $CI->db->get();


    if ($query->num_rows() > 0) {


        return $query->row_array();


    }


    return 0;


}


function get_all_project_gallery($id)


{


    $CI =& get_instance();


    $query = $CI->db->query("select * from project_gallery where project_id='" . $id . "' order by project_gallery_id asc ");


    if ($query->num_rows() > 0) {


        return $query->result();


    }


    return 0;


}


function deletecache($cachename)


{


    $CI =& get_instance();


    if ($CI->simple_cache->is_cached($cachename)) {


        $CI->simple_cache->delete_item($cachename);


    }


}


function check_latest_version() {


    $CI = & get_instance();
    error_reporting(1);
    
    $value = 'yes';
    
    $cookie = '';


    if (isset($_COOKIE["fund_latest_version"])) {
        $cookie = $_COOKIE["fund_latest_version"];
    }

    $temp = $CI->input->cookie('fund_latest_version');
    if (isset($temp)) {
        $cookie = $temp;
    }


    //open if once COMPLETE
    
    
    if ($cookie == '' && strtolower($cookie)!='yes') {

            $cookie = array(
                'name' => 'fund_latest_version',
                'value' => $value,
                'expire' => time() + (3600 * 12),
                'domain' => '',
                'path' => '/',
                'prefix' => ''
            );
            $CI->input->set_cookie($cookie);
            //setcookie("fund_latest_version",$value, time()+3600*12);


            $site_settting = site_setting();

            $site_version = $site_settting['site_version'];
            $version_pcode = $site_settting['version_pcode'];
            $version_license = $site_settting['version_license'];
            $c_url = $site_settting['version_url'];

            $domain = $_SERVER['HTTP_HOST'];


            $encoded = 'version_no=' . $site_version . '&';
            $encoded .= urlencode('version_license') . '=' . urlencode($version_license);
            $encoded .= '&' . urlencode('domain') . '=' . urlencode($domain);
            $encoded .= '&' . urlencode('type') . '=' . urlencode('check_version');
            $encoded .= '&' . urlencode('version_pcode') . '=' . urlencode(base64_encode($version_pcode));


            

            $ch = curl_init($c_url);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $encoded);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $output = curl_exec($ch);
            
            $xmlstr = $output;
            
            //var_dump($xmlstr); die;
            
            $output_version = new SimpleXMLElement($xmlstr);

            $array = (array) $output_version;
            
            //echo "<pre>"; print_r($array); //die;
            
            
            $filepath = '';
            $version = '';
            $var_test = array();


            if (isset($array['item']) && !empty($array['item'])) {
                
                $item_res=$array['item'];
                
               
                if(!isset($item_res->successcode)){
                
                    foreach($item_res as $item_row){

                        $obj = $item_row;

                        if (isset($obj->successcode)) {

                            $var_test[] = $obj->successmsg;

                            $base_pth = base_path();
                            $my_file = $base_pth . '/ver_update.xml';

                            $handle = fopen($my_file, 'w') or die('Cannot open file:  ' . $my_file);
                            fwrite($handle, $xmlstr);
                            fclose($handle);
                        }
                    }
                } else {
                    $obj = $item_res; 
                    

                    if (isset($obj->successcode)) {

                        $var_test[] = $obj->successmsg;

                        $base_pth = base_path();
                        $my_file = $base_pth . '/ver_update.xml';

                        $handle = fopen($my_file, 'w') or die('Cannot open file:  ' . $my_file);
                        fwrite($handle, $xmlstr);
                        fclose($handle);
                    }                    
                }
                
            } else {

                //maintain cookies for call request twise in day
                //setcookie("fund_latest_version",$value, time()+3600*12);               
            }


            curl_close($ch);

            return $var_test;


     }  else {
         
         $base_pth = base_path();
         $my_file = $base_pth . '/ver_update.xml';
         
         if(file_exists($my_file)){
         
           $output_version = simplexml_load_file($my_file);

            $array = (array) $output_version;
            
            //echo "<pre>"; print_r($array); //die;
            
            
            $filepath = '';
            $version = '';
            $var_test = array();


            if (isset($array['item']) && !empty($array['item'])) {
                
               
                $item_res=$array['item'];
                
                               
                if(!isset($item_res->successcode)){
                    
                    foreach($item_res as $item_row){

                        $obj = $item_row;

                        if (isset($obj->successcode)) {
                            $var_test[] = $obj->successmsg;
                        }
                    }
                } else {
                    
                    $obj = $item_res; 
                    
                    if (isset($obj->successcode)) {
                        $var_test[] = $obj->successmsg;
                    } 
                }
                
                
            }
            
            return $var_test;
         
         }
         
     }
}


function unzip($src_file, $dest_dir = false, $create_zip_name_dir = true, $overwrite = true)
{

    error_reporting(0);

    if ($zip = zip_open($src_file)) {
        if ($zip) {

            $splitter = ($create_zip_name_dir === true) ? "." : "/";
            
            if ($dest_dir === false) $dest_dir = substr($src_file, 0, strrpos($src_file, $splitter)) . "/";


            // Create the directories to the destination dir if they don't already exist
            create_dirs($dest_dir);
            
            // For every file in the zip-packet

            while ($zip_entry = zip_read($zip)) {

                // Now we're going to create the directories in the destination directories
                // If the file is not in the root dir

                $pos_last_slash = strrpos(zip_entry_name($zip_entry), "/");
                
                if ($pos_last_slash !== false) {
                    // Create the directory where the zip-entry should be saved (with a "/" at the end)
                    create_dirs($dest_dir . substr(zip_entry_name($zip_entry), 0, $pos_last_slash + 1));
                }


                // Open the entry
                if (zip_entry_open($zip, $zip_entry, "r")) {

                    // The name of the file to save on the disk
                    $file_name = $dest_dir . zip_entry_name($zip_entry);


                    // Check if the files should be overwritten or not
                    if ($overwrite === true || $overwrite === false && !is_file($file_name)) {

                        // Get the content of the zip entry
                        $fstream = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
                        file_put_contents($file_name, $fstream);

                        // Set the rights
                        chmod($file_name, 0777);

                        // echo "save: ".$file_name."<br />";
                    }

                    // Close the entry
                    zip_entry_close($zip_entry);
                }
            }

            // Close the zip-file
            zip_close($zip);
            
        }

    } else {

        return false;
    }
    
    return true;

}


function create_dirs($path)
{

    if (!is_dir($path)) {

        $directory_path = "";
        $directories = explode("/", $path);
        array_pop($directories);

        foreach ($directories as $directory) {

            $directory_path .= $directory . "/";
            if (!is_dir($directory_path)) {
                mkdir($directory_path);
                chmod($directory_path, 0777);
            }
        }
    }
}


function latest_version($limit = '', $cnt = '')
{


    $CI =& get_instance();

    $CI->db->select('*');
    $CI->db->from('version_update');
    if ($limit != '') {
        $CI->db->limit($limit);
    }

    $CI->db->order_by('ver_id', 'desc');
    $query = $CI->db->get();


    if ($query->num_rows() > 0) {
        if ($cnt != '') {
            return $query->num_rows();
        }
        return $query->result_array();
    }

    return 0;

}





function country_validate($country_data = array())
{
    $CI =& get_instance();

    $query = $CI->db->get_where('country', $country_data);
    //print_r($user_data);

    if ($query->num_rows() > 0) {

        $country_info = $query->row_array();

        return $query->row_array();

    } else {
        return FALSE;
    }

}


function CategoryPhoto($files, $facebook = false)
{
    $new_img = '';
    $rand = rand(0, 100000);
    $image_settings = get_image_setting_data();
    $CI =& get_instance();
    $base_path = $CI->config->slash_item('base_path');
    require_once $base_path . 'thumbgenerator/ThumbLib.inc.php';
    if (trim($files["userfile"]["tmp_name"]) != "") {

        $type_img = explode('/', $files['userfile']['type']);
        $new_img = 'user_' . $rand . '.' . $type_img[1];
        move_uploaded_file($files["userfile"]["tmp_name"], $base_path . "upload/orig/" . $new_img);
        $image_info = getimagesize($base_path . "upload/orig/" . $new_img);
        $image_width = $image_info[0];
        $image_height = $image_info[1];

        $new_w = '350'; //You can change these to fit the width and height you want
        $new_h = '217';
        $aspect_ratio = $image_settings['u_ratio'];

        if ($aspect_ratio) {
            if ($image_height > $image_width) {
                $ratio = $new_h / $image_height;
                $new_h = $new_h;
                $new_w = $image_width * $ratio;
            } else {
                $ratio = $new_w / $image_width;
                $new_w = $new_w;
                $new_h = $image_height * $ratio;
            }
        }

        $thumb = PhpThumbFactory::create($base_path . "upload/orig/" . $new_img);
        $thumb->adaptiveResize($new_w, $new_h);
        $cache_path = $base_path . "upload/category/" . $new_img;
        $thumb->save($cache_path);


    }

    return $new_img;


}

function LogoImageUpload($files, $image_type = 'logo', $favicon = false)
{
    $new_img = '';
    $rand = rand(0, 100000);
    $image_settings = get_image_setting_data();
    $CI =& get_instance();
    $base_path = $CI->config->slash_item('base_path');
    require_once $base_path . 'thumbgenerator/ThumbLib.inc.php';
    if (trim($files["userfile"]["tmp_name"]) != "") {

        $type_img = explode('/', $files['userfile']['type']);
        $new_img = $image_type . '_' . $rand . '.' . $type_img[1];
        move_uploaded_file($files["userfile"]["tmp_name"], $base_path . "upload/orig/" . $new_img);
        $image_info = getimagesize($base_path . "upload/orig/" . $new_img);
        $image_width = $image_info[0];
        $image_height = $image_info[1];

        $new_w = '220'; //You can change these to fit the width and height you want
        $new_h = '37';

        $thumb = PhpThumbFactory::create($base_path . "upload/orig/" . $new_img);
        $thumb->adaptiveResize($new_w, $new_h);
        $cache_path = $base_path . "upload/orig/" . $new_img;
        $thumb->save($cache_path);


    }
    if ($favicon == true) {
        $new_img = $files["userfile"]["name"];
        //$new_img =$fb_uid.'.jpg';
        $outPath = $base_path . 'upload/orig/' . $new_img;


        $new_w = '32'; //You can change these to fit the width and height you want
        $new_h = '32';

        $thumb = PhpThumbFactory::create($base_path . "upload/orig/" . $new_img);
        $thumb->adaptiveResize($new_w, $new_h);
        $cache_path = $base_path . "upload/orig/" . $new_img;
        $thumb->save($cache_path);
    }

    return $new_img;


}

function OnlyUpload($files)
{
    $rand = rand(0, 100000);
    $image_settings = get_image_setting_data();
    $image_data = array();
    $CI =& get_instance();
    $base_path = $CI->config->slash_item('base_path');
    require_once $base_path . 'thumbgenerator/ThumbLib.inc.php';
    if (trim($files["userfile"]["tmp_name"]) != "") {
        $CI =& get_instance();
        $base_path = $CI->config->slash_item('base_path');
        echo $admin_base_path = $CI->config->slash_item('admin_base_path');
        $type_img = explode('/', $files['userfile']['type']);
        $new_img = 'category_' . $rand . '.' . $type_img[1];
        //move_uploaded_file($files["userfile"]["tmp_name"],  $base_path."upload/orig/" . $new_img);
        move_uploaded_file($files["userfile"]["tmp_name"], $admin_base_path . "upload/orig/" . $new_img);

        //amazon code by rockers
        $config['upload_path'] = 'upload/orig/';//Leave blank if want to upload at root of bucket
        $config['allowed_types'] = 'jpg|jpeg|png|gif|pdf|doc|docs|zip';
        $config['remove_spaces'] = TRUE;
        $config['max_size'] = '512000';//5MB
        $CI->load->library('upload');

        if ($CI->upload->do_upload("upload/orig/", $new_img)) {
            $data = array('upload_data' => $CI->upload->data());
            $photo_file_name = $data['upload_data']['file_name'];
            //echo 'Orig file-->'.$CI->file_name = $data['upload_data']['file_name']; die;
            //$image_data['orig'] = $data['upload_data']['file_name'];
        }

        //$image_info = getimagesize($base_path."upload/orig/" . $new_img);
        $image_info = getimagesize($admin_base_path . "upload/orig/" . $new_img);
        $image_width = $image_info[0];
        $image_height = $image_info[1];


        $new_w = '1300'; //You can change these to fit the width and height you want
        $new_h = '680';
        $aspect_ratio = $image_settings['u_ratio'];

        if ($aspect_ratio) {
            if ($image_height > $image_width) {
                $ratio = $new_h / $image_height;
                $new_h = $new_h;
                $new_w = $image_width * $ratio;
            } else {
                $ratio = $new_w / $image_width;
                $new_w = $new_w;
                $new_h = $image_height * $ratio;
            }
        }

        //$thumb = PhpThumbFactory::create( $base_path."upload/orig/" . $new_img);
        $thumb = PhpThumbFactory::create($admin_base_path . "upload/orig/" . $new_img);
        $thumb->adaptiveResize($new_w, $new_h);
        $cache_path = $admin_base_path . "upload/dynamic/" . $new_img;
        $thumb->save($cache_path);

        if ($CI->upload->do_upload("upload/dynamic/", $new_img)) {
            $data = array('upload_data' => $CI->upload->data());
            $photo_file_name = $data['upload_data']['file_name'];
            //echo 'Orig file-->'.$CI->file_name = $data['upload_data']['file_name']; die;
            $image_data['orig'] = $data['upload_data']['file_name'];
        }
        $image_data['orig'] = $new_img;

    }
    @unlink('upload/orig/' . $new_img);
    //return $new_img;
    //echo '<pre>'; print_r($image_data); die();
    return $image_data;


}
function ContentImageUpload($files)
{
    $rand = rand(0, 100000);
    $image_settings = get_image_setting_data();
    $image_data = array();
    $CI =& get_instance();
    $base_path = $CI->config->slash_item('base_path');
    require_once $base_path . 'thumbgenerator/ThumbLib.inc.php';
    if (trim($files["userfile"]["tmp_name"]) != "") {
        $CI =& get_instance();
        $base_path = $CI->config->slash_item('base_path');
        echo $admin_base_path = $CI->config->slash_item('admin_base_path');
        $type_img = explode('/', $files['userfile']['type']);
        $new_img = 'content_' . $rand . '.' . $type_img[1];
        //move_uploaded_file($files["userfile"]["tmp_name"],  $base_path."upload/orig/" . $new_img);
        move_uploaded_file($files["userfile"]["tmp_name"], $admin_base_path . "upload/orig/" . $new_img);

        //amazon code by rockers
        $config['upload_path'] = 'upload/orig/';//Leave blank if want to upload at root of bucket
        $config['allowed_types'] = 'jpg|jpeg|png|gif|pdf|doc|docs|zip';
        $config['remove_spaces'] = TRUE;
        $config['max_size'] = '512000';//5MB
        $CI->load->library('upload');

        if ($CI->upload->do_upload("upload/orig/", $new_img)) {
            $data = array('upload_data' => $CI->upload->data());
            $photo_file_name = $data['upload_data']['file_name'];
            //echo 'Orig file-->'.$CI->file_name = $data['upload_data']['file_name']; die;
            //$image_data['orig'] = $data['upload_data']['file_name'];
        }

        //$image_info = getimagesize($base_path."upload/orig/" . $new_img);
        $image_info = getimagesize($admin_base_path . "upload/orig/" . $new_img);
        $image_width = $image_info[0];
        $image_height = $image_info[1];


        $new_w = '1300'; //You can change these to fit the width and height you want
        $new_h = '680';
        $aspect_ratio = $image_settings['u_ratio'];

        if ($aspect_ratio) {
            if ($image_height > $image_width) {
                $ratio = $new_h / $image_height;
                $new_h = $new_h;
                $new_w = $image_width * $ratio;
            } else {
                $ratio = $new_w / $image_width;
                $new_w = $new_w;
                $new_h = $image_height * $ratio;
            }
        }

        //$thumb = PhpThumbFactory::create( $base_path."upload/orig/" . $new_img);
        $thumb = PhpThumbFactory::create($admin_base_path . "upload/orig/" . $new_img);
        $thumb->adaptiveResize($new_w, $new_h);
        $cache_path = $admin_base_path . "upload/content/" . $new_img;
        $thumb->save($cache_path);

        if ($CI->upload->do_upload("upload/content/", $new_img)) {
            $data = array('upload_data' => $CI->upload->data());
            $photo_file_name = $data['upload_data']['file_name'];
            //echo 'Orig file-->'.$CI->file_name = $data['upload_data']['file_name']; die;
            $image_data['orig'] = $data['upload_data']['file_name'];
        }
        $image_data['orig'] = $new_img;

    }
    @unlink('upload/orig/' . $new_img);
    //return $new_img;
    //echo '<pre>'; print_r($image_data); die();
    return $image_data;


}
/*

       Function name :check_admin_authentication()

       Parameter :none

       Return : admin id or redirect page.

       Use : function to check authentication or not.

  */
function check_admin_authentication()
{

    $CI =& get_instance();

    if ($CI->session->userdata('admin_id') != '') {
        return $CI->session->userdata('admin_id');
    } else {
        redirect('admin/home/index');
    }

}

/*

       Function name :GetEquityInvestor()

       Parameter :equity_id

       Return : number of equity investors.

       Use : function to check number of equity investors.

  */
function GetEquityInvestor($campaign_id = '')
{

    $CI =& get_instance();

    $query = $CI->db->query("SELECT investment_process.*,accreditation.accreditation_status,property.property_id,property.property_address,user.user_id,user.user_name,user.last_name,user.image,user.profile_slug FROM `investment_process` join property on property.property_id=investment_process.property_id join user on user.user_id=investment_process.user_id left join accreditation on accreditation.user_id=investment_process.user_id where investment_process.campaign_id=" . $property_id . " order by investment_process.id desc ");


    if ($query->num_rows() > 0) {

        return $query->num_rows();

    }

    return 0;

}

/*
         
         Function name :GetEquityInvestor()
         
         Parameter :equity_id
         
         Return : number of equity investors.
         
         Use : function to check number of equity investors.
         
    */
function PreviousReasonEquity($equity_id = '', $type = 'decline')
{

    $CI =& get_instance();

    if ($type == 'decline') $query = $CI->db->query("SELECT reason_decline from equity where save_the_reason=1 order by date_added limit 1 ");
    else $query = $CI->db->query("SELECT reason_inactive_hidden from equity where save_the_reason_inactive=1 order by date_added limit 1 ");

    if ($query->num_rows() > 0) {

        return $query->row_array();

    }

    return 0;

}

 function  getProjectCount($type='',$val='')
 { 
    switch ($type) {
        case 'company_category':
            $CI =& get_instance();
            $query = $CI->db->query("SELECT count(equity.equity_id) as total_equity from equity join company_profile on `company_profile`.`company_id`= `equity`.`company_id` where `equity`.`status` IN (1,2,3,4,5,6,7,8) and `company_profile`.`company_category`='$val' ");
            $result =$query->result_array();
            return $result[0]['total_equity'];
            break;
        
        default:
           return false;
            break;
        return false;
    }
    return false;

 }

//Common function to fetch data for any table in admin panel


function GetSuburbData($id='',$limit=100,$order_by=array('suburb'=>'asc'))
{


    $CI =& get_instance();

    $CI->db->select('*');
    $CI->db->from('postcodes_geo');

    if (is_array($order_by)) {


        foreach ($order_by as $key => $val) {


            $CI->db->order_by($key, $val);


        }


    }

    if($id > 0 || $id != '')
    {
        $CI->db->where('id', $id);
    }

    $CI->db->group_by('postcodes_geo.suburb');

    $CI->db->limit($limit);

    $query = $CI->db->get();


    if ($query->num_rows() > 0) {

            return $query->result();

    }


    return 0;


}

?>
