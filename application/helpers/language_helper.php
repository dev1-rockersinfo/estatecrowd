<?php


function get_supported_lang()
{
    $CI =& get_instance();
    $CI->config->load('language');


    if (!$CI->simple_cache->is_cached('supported_languages')) {

        // not cached, do our things that need caching
        $query = $CI->db->query("select * from language");
        $data = $query->result_array();
        //echo "<pre>";
        //print_r($data);
        $arr = array();
        foreach ($data as $key => $lang) {
            $arr[$lang['iso2']] = $lang['language_folder'];
        }
        // store in cache
        $CI->simple_cache->cache_item('supported_languages', $arr);
    } else {
        $arr = $CI->simple_cache->get_item('supported_languages');
    }
    return $arr;

}

function getLanguageByCode($language_code=''){
    $CI =& get_instance();

    if($language_code!=''){
        $query = $CI->db->query("select * from language where iso2='".strtolower($language_code)."'");

        if($query->num_rows()>0){
           return $query->row_array();
        }
    }
    return false;
       
      
}


function base_path()
{
    $CI =& get_instance();
    return $base_path = $CI->config->slash_item('base_path');
}


function get_current_language()
{
    $CI =& get_instance();
    $CI->config->load('language');
    $default_language = $CI->config->item('default_language');


    if (!$CI->simple_cache->is_cached('default_language')) {

        // not cached, do our things that need caching
        $query = $CI->db->query("select * from language where `default`=1");
        $data = $query->row_array();
        //echo "<pre>";
        //print_r($data);
        $default_language = $data['iso2'];
        // store in cache
        $CI->simple_cache->cache_item('default_language', $default_language);
    } else {
        $default_language = $CI->simple_cache->get_item('default_language');
    }


    return $default_language;

}

function get_switch_uri()
{
    $lnk_ln = current_url() . "?lang=";
    return $lnk_ln;
}

//fetch language data
function current_languagedata()
{

    $CI =& get_instance();


    setting_deletecache('current_languagedata');
    if (!$CI->simple_cache->is_cached('current_languagedata')) {

        // not cached, do our things that need caching
        $CI->db->select('*');
        // $query = $CI->db->query("select * from language");
        if ($_SESSION['lang_code'] != '')
            $CI->db->where('iso2', $_SESSION['lang_code']);
        if ($_SESSION['lang_folder'] != '')
            $CI->db->where('language_folder', $_SESSION['lang_folder']);
        $CI->db->from('language');
        $query = $CI->db->get();
        if ($query->num_rows() > 0) {
            $data = $query->row_array();
        } else {
            $data = 0;
        }
        // store in cache
        $CI->simple_cache->cache_item('current_languagedata', $data);
    } else {
        $data = $CI->simple_cache->get_item('current_languagedata');
    }
    //echo "<pre>";
    //print_r( $data);
    return $data;

}


function GetUserLangCode($user_id=0,  $lang_id = '') {
    $CI = & get_instance();

  

    if ($user_id > 0) {
        $query = $CI->db->query("select user_language_id from user where user_id ='" . $user_id . "' and user_language_id != '' && user_language_id != 0 ");
        if ($query->num_rows() > 0) {
            $user_data = $query->row_array();
            if (isset($user_data['user_language_id'])) {
                if ($user_data['user_language_id'] > 0) {
                    $lang_id = $user_data['user_language_id'];
                }
            }
        }
    }

    if ($lang_id == '' || $lang_id==0) {
        if (isset($_SESSION['lang_code'])) {
            if ($_SESSION['lang_code'] != '') {
                $lang_code = $_SESSION['lang_code'];
                $lang_data = getLanguageByCode($lang_code);
                if (!empty($lang_data)) {
                    $lang_id = $lang_data['language_id'];
                }
            }
        }
    }

    if ($lang_id == '' || $lang_id==0) {
        $site_setting = site_setting();
        if (isset($site_setting['site_language'])) {
            if ($site_setting['site_language'] != '') {
                $lang_id = $site_setting['site_language'];
            }
        }
    }




    return $lang_id;
}



?>
