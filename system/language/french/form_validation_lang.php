<?php

$lang['required']			= "la %s champ est obligatoire.";
$lang['isset']				= "la %s domaine doit avoir une valeur.";
$lang['valid_email']		= "la %s champ doit contenir une adresse email valide.";
$lang['valid_emails']		= "la %s champ doit contenir toutes les adresses e-mail valides.";
$lang['valid_url']			= "la %s champ doit contenir une URL valide.";
$lang['valid_ip']			= "la %s champ doit contenir une adresse IP valide.";
$lang['min_length']			= "la %s domaine doit être au moins %s caractères.";
$lang['max_length']			= "la %s domaine ne peut pas dépasser %s caractères.";
$lang['exact_length']		= "la %s domaine doit être exactement %s caractères.";
$lang['alpha']				= "la %s champ ne peut contenir que des caractères alphabétiques.";
$lang['alpha_numeric']		= "la %s champ ne peut contenir que des caractères alphanumériques.";
$lang['alpha_dash']			= "la %s champ ne peut contenir que des caractères alphanumériques, souligne, et des tirets.";
$lang['numeric']			= "la %s domaine doit contenir que des chiffres.";
$lang['is_numeric']			= "la %s domaine doit contenir que des caractères numériques.";
$lang['integer']			= "la %s champ doit contenir un nombre entier.";
$lang['regex_match']		= "la %s champ n'est pas dans le bon format.";
$lang['matches']			= "la %s champ ne correspond pas à l' %s domaine.";
$lang['is_unique'] 			= "la %s champ doit contenir une valeur unique.";
$lang['is_natural']			= "la %s domaine doit contenir que des chiffres positifs.";
$lang['is_natural_no_zero']	= "la %s champ doit contenir un nombre supérieur à zéro.";
$lang['decimal']			= "la %s champ doit contenir un nombre décimal.";
$lang['less_than']			= "la %s champ doit contenir un nombre inférieur à %s.";
$lang['greater_than']		= "la %s champ doit contenir un nombre supérieur %s.";


/* End of file form_validation_lang.php */
/* Location: ./system/language/english/form_validation_lang.php */
