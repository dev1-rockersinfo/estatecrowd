ALTER TABLE `property` ADD `locality_id` INT(11) NULL AFTER `user_id`;
ALTER TABLE `property` ADD `updated_at` DATETIME NULL AFTER `wallMaterial`;
ALTER TABLE `property_setting` ADD `expire_time` DATETIME NULL ;

ALTER TABLE `postcodes_geo` CHANGE `postcode` `postcode` INT(11) NULL;

ALTER TABLE `postcodes_geo` ADD `page` INT(11) NULL AFTER `status`, ADD `total_properties` INT(11) NULL AFTER `page`;


