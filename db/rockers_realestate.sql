-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 21, 2015 at 02:37 AM
-- Server version: 5.5.42-37.1
-- PHP Version: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rockers_realestate_developmentci3`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_request`
--

CREATE TABLE IF NOT EXISTS `access_request` (
  `access_request_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `equity_id` int(11) NOT NULL,
  `updates` int(11) NOT NULL,
  `comments` int(11) NOT NULL,
  `funders` int(11) NOT NULL,
  `docs_media` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `request_name` varchar(255) DEFAULT NULL,
  `deny_reason` varchar(255) DEFAULT NULL,
  `reason_save` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `access_request`
--

INSERT INTO `access_request` (`access_request_id`, `user_id`, `equity_id`, `updates`, `comments`, `funders`, `docs_media`, `created_date`, `updated_date`, `request_name`, `deny_reason`, `reason_save`) VALUES
(1, 12, 8, 0, 0, 2, 0, '2015-10-27 17:01:43', '2015-10-27 17:03:30', 'funders', NULL, 0),
(2, 11, 24, 2, 2, 2, 2, '2015-11-30 10:38:05', '2015-11-30 10:39:47', 'comments', NULL, 0),
(3, 1, 19, 0, 1, 0, 0, '2015-12-01 13:21:20', '2015-12-01 13:21:20', 'comments', NULL, 0),
(4, 43, 1, 2, 2, 2, 2, '2015-12-01 13:25:03', '2015-12-01 13:25:28', 'updates', NULL, 0),
(5, 7, 1, 2, 2, 2, 2, '2015-12-01 15:32:43', '2015-12-02 15:21:42', 'docs_media', NULL, 0),
(6, 45, 24, 0, 0, 0, 0, '2015-12-02 10:48:12', '2015-12-02 16:14:16', 'updates', NULL, 0),
(7, 1, 24, 2, 2, 2, 2, '2015-12-02 11:06:37', '2015-12-02 18:10:42', 'comments', NULL, 0),
(8, 8, 19, 0, 1, 1, 1, '2015-12-02 11:54:44', '2015-12-02 11:54:50', 'docs_media', NULL, 0),
(9, 8, 1, 2, 2, 2, 2, '2015-12-02 11:56:50', '2015-12-02 12:00:59', 'funders', NULL, 0),
(10, 45, 26, 2, 2, 2, 2, '2015-12-02 14:11:53', '2015-12-02 14:12:21', 'docs_media', NULL, 0),
(11, 46, 24, 2, 2, 2, 2, '2015-12-02 18:10:17', '2015-12-02 18:11:15', 'updates', NULL, 0),
(12, 8, 24, 1, 1, 1, 1, '2015-12-04 19:29:13', '2015-12-04 19:29:23', 'docs_media', NULL, 0),
(13, 2, 24, 0, 1, 0, 0, '2015-12-15 11:29:51', '2015-12-15 11:29:51', 'comments', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `account_type`
--

CREATE TABLE IF NOT EXISTS `account_type` (
  `id` int(11) NOT NULL,
  `account_type_name` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `account_type`
--

INSERT INTO `account_type` (`id`, `account_type_name`, `status`) VALUES
(1, 'Checking Account', 1),
(2, 'Saving Account', 1);

-- --------------------------------------------------------

--
-- Table structure for table `accreditation`
--

CREATE TABLE IF NOT EXISTS `accreditation` (
  `id` int(11) NOT NULL,
  `accreditation_date` datetime DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `legal_first_name` varchar(50) DEFAULT NULL,
  `legal_last_name` varchar(50) DEFAULT NULL,
  `address1` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `zip` varchar(20) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `account_type` varchar(20) DEFAULT NULL,
  `bank_name` varchar(50) DEFAULT NULL,
  `account_no` bigint(255) DEFAULT NULL,
  `routing_no` bigint(255) DEFAULT NULL,
  `accreditation_status` int(2) NOT NULL,
  `accreditation_type` int(12) NOT NULL,
  `accreditation_income` bigint(255) DEFAULT NULL,
  `verification_phone_number` bigint(255) DEFAULT NULL,
  `verification_accreditation_letter` varchar(255) DEFAULT NULL,
  `accredited` int(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `accreditation`
--

INSERT INTO `accreditation` (`id`, `accreditation_date`, `user_id`, `legal_first_name`, `legal_last_name`, `address1`, `address2`, `city`, `state`, `country`, `zip`, `phone`, `account_type`, `bank_name`, `account_no`, `routing_no`, `accreditation_status`, `accreditation_type`, `accreditation_income`, `verification_phone_number`, `verification_accreditation_letter`, `accredited`) VALUES
(1, '2015-11-30 11:00:55', 11, 'Herry', 'Patel', 'Vadodara, Gujarat, India', '', 'Vadodara', 'Gujarat', 'India', '369852', '9876543210', 'Checking Account', 'Stripe US bank', 123456789001, 123456789, 1, 0, 1000000, 918866342811, '', 1),
(2, '2015-11-30 14:15:54', 12, 'Anky', 'Patel', 'Vadodara, Gujarat, India', '', 'Vadodara', 'Gujarat', 'India', '369852', '9876541230', 'Checking Account', 'US Bank', 123456789001, 123546789, 1, 0, 1000000, 0, '42454-accrediated-doc-1448873710.pdf', 1),
(3, '2015-12-02 11:52:39', 45, 'Vashu', 'Patel', 'Vadodara, Gujarat, India', '', 'vadodara', 'Gujarat', 'India', '369852', '1234567890', 'Checking Account', 'USA Bank', 123456789, 123456789, 1, 0, 1000000, 1234567890, '', 1),
(4, '2015-12-02 13:24:05', 46, 'Vikram', 'Patel', 'Vadodara, Gujarat, India', '', 'vadodara', 'Gujarat', 'India', '369852', '1234567890', 'Checking Account', 'USA Bank', 123456789, 123456789, 1, 0, 1000000, 1234567890, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE IF NOT EXISTS `activities` (
  `id` int(11) NOT NULL,
  `act` varchar(255) DEFAULT NULL,
  `activity_date` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `is_read` tinyint(1) NOT NULL COMMENT '0-unread, 1-unread'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `admin_id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `admin_type` int(10) NOT NULL DEFAULT '2',
  `company` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `active` varchar(255) DEFAULT NULL,
  `date_added` date NOT NULL,
  `login_ip` varchar(255) DEFAULT NULL,
  `forgot_unique_code` varchar(255) DEFAULT NULL,
  `request_date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `username`, `password`, `admin_type`, `company`, `email`, `active`, `date_added`, `login_ip`, `forgot_unique_code`, `request_date`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, 'Rockers Technology', 'ankit.rockersinfo@gmail.com', '1', '2014-09-06', '27.109.7.130', 'BoL3RNPFxo3MuyGQ13eG', '2015-04-24 13:52:37'),
(8, 'nilesh', '25f9e794323b453885f5181f1b624d0b', 2, NULL, 'nilesh.rockersinfo@gmail.com', '1', '2014-09-06', '1.22.81.242', '1', '0000-00-00 00:00:00'),
(37, 'ritu', '25d55ad283aa400af464c76d713c07ad', 2, NULL, 'ritu.rockersinfo@gmail.com', '1', '2015-03-16', '1.22.80.226', '1', '0000-00-00 00:00:00'),
(39, 'jayshree', '25d55ad283aa400af464c76d713c07ad', 1, NULL, 'jayshree.rockersinfo@gmail.com', '1', '2015-03-20', '123.237.164.19', '', '0000-00-00 00:00:00'),
(40, 'jayu', '25d55ad283aa400af464c76d713c07ad', 1, NULL, 'jayshree.test.rockersinfo@gmail.com', '1', '2015-03-20', '123.237.164.19', '', '0000-00-00 00:00:00'),
(41, 'Krishi', '25d55ad283aa400af464c76d713c07ad', 1, NULL, 'jayshree.test01.rockersinfo@gmail.com', '1', '2015-03-20', '123.237.164.19', '', '0000-00-00 00:00:00'),
(49, 'bhavana', '25d55ad283aa400af464c76d713c07ad', 1, NULL, 'bhavana.rockersinfo@gmail.com', '1', '2015-04-17', '123.237.161.39', '1', '0000-00-00 00:00:00'),
(52, 'kartik', '25d55ad283aa400af464c76d713c07ad', 2, NULL, 'kartik.rockersinfo@gmail.com', '1', '2015-06-30', '27.109.7.130', '', '0000-00-00 00:00:00'),
(54, 'jayu', '25d55ad283aa400af464c76d713c07ad', 2, NULL, 'jayshree.test03.rockersinfo@gmail.com', '1', '2015-07-03', '27.109.7.130', '', '0000-00-00 00:00:00'),
(57, 'krishi', '25d55ad283aa400af464c76d713c07ad', 2, NULL, 'jayshree.test@hotmail.com', '1', '2015-07-13', '27.109.7.130', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `admin_activity`
--

CREATE TABLE IF NOT EXISTS `admin_activity` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `module` varchar(255) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `datetime` datetime NOT NULL,
  `action_detail` text,
  `second_user_id` int(10) NOT NULL DEFAULT '0',
  `campaign_id` int(10) NOT NULL DEFAULT '0',
  `read` tinyint(2) NOT NULL DEFAULT '0',
  `company_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=160 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_activity`
--

INSERT INTO `admin_activity` (`id`, `user_id`, `module`, `action`, `datetime`, `action_detail`, `second_user_id`, `campaign_id`, `read`, `company_id`) VALUES
(87, 11, 'project', 'upload_contract', '2015-12-01 10:58:58', '<a href="http://mydesichef.com/realestate_development_ci3/user/herryPatel11">Herry Patel</a> has <a href="http://mydesichef.com/realestate_development_ci3/startinvest/index/24">Uploaded</a> Investment  contract copy on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 1, 0),
(88, 11, 'project', 'approve_contract', '2015-12-01 11:01:53', 'You have <a href="http://mydesichef.com/realestate_development_ci3/startinvest/index/24">Approve</a> Investment contract copy from <a href="http://mydesichef.com/realestate_development_ci3/user/herryPatel11">Herry Patel</a> on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 1, 0),
(89, 11, 'project', 'approve_acknowledge_contract', '2015-12-01 11:03:50', 'You have <a href="http://mydesichef.com/realestate_development_ci3/startinvest/index/24">Approved</a> Investment acknowledgement from <a href="http://mydesichef.com/realestate_development_ci3/user/herryPatel11">Herry Patel</a> on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 1, 0),
(90, 11, 'project', 'funded', '2015-12-01 11:03:50', '<a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a> is Invested with $500.00 USD by <a href="http://mydesichef.com/realestate_development_ci3/user/herryPatel11">Herry Patel</a>', 7, 24, 1, 0),
(91, 11, 'project', 'confirmed_submmited_tracking', '2015-12-01 11:04:32', '<a href="http://mydesichef.com/realestate_development_ci3/user/herryPatel11">Herry Patel</a> has <a href="http://mydesichef.com/realestate_development_ci3/startinvest/index/24">Approved and Confirmed</a> delivery of  share certificate/document  on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 1, 0),
(92, 41, 'User', 'signup', '2015-12-01 12:04:02', '<a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel41">rakesh patel</a> joined Real Estate', 0, 0, 1, 0),
(93, 42, 'User', 'signup', '2015-12-01 12:10:53', '<a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel42">rakesh patel</a> joined Real Estate', 0, 0, 1, 0),
(94, 43, 'User', 'signup', '2015-12-01 12:27:07', '<a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a> joined Real Estate', 0, 0, 1, 0),
(95, 1, 'project', 'follow', '2015-12-01 12:57:44', '<a href="http://mydesichef.com/realestate_development_ci3/user/JayshreePatel1">Jayshree Patel</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/development/test-project1">Test Project</a>', 0, 19, 1, 0),
(96, 43, 'User', 'update_account', '2015-12-01 13:24:38', '<a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a> has updated account information', 0, 0, 1, 0),
(97, 43, 'project', 'commented', '2015-12-01 13:26:28', '<a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a>  Commented  on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> ', 1, 1, 1, 0),
(98, 11, 'project', 'follow', '2015-12-01 13:47:16', '<a href="http://mydesichef.com/realestate_development_ci3/user/herryPatel11">Herry Patel</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a>', 0, 24, 1, 0),
(99, 12, 'project', 'follow', '2015-12-01 14:01:41', '<a href="http://mydesichef.com/realestate_development_ci3/user/ankyPatel12">Anky Patel</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a>', 0, 24, 1, 0),
(100, 7, 'User', 'update_account', '2015-12-01 15:25:22', '<a href="http://mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a> has updated account information', 0, 0, 1, 0),
(101, 0, 'project', 'follow', '2015-12-01 15:32:21', '<a href="http://mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/company/abc-corp">ABC Corp</a>', 0, 0, 1, 2),
(102, 1, 'project', 'project_inactive', '2015-12-01 15:56:44', 'You have inactivated and unpublished Development <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a>', 0, 1, 1, 0),
(103, 1, 'project', 'project_active', '2015-12-01 15:57:39', 'You have activated and published Development <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> ', 0, 1, 1, 0),
(104, 1, 'project', 'follow', '2015-12-01 18:21:22', '<a href="http://mydesichef.com/realestate_development_ci3/user/JayshreePatel1">Jayshree Patel</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a>', 0, 24, 1, 0),
(105, 1, 'project', 'follow', '2015-12-01 18:23:12', '<a href="http://mydesichef.com/realestate_development_ci3/user/JayshreePatel1">Jayshree Patel</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a>', 0, 24, 1, 0),
(106, 43, 'project', 'follow', '2015-12-01 18:24:28', '<a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/development/test-project1">Test Project</a>', 0, 19, 1, 0),
(107, 43, 'project', 'follow', '2015-12-01 18:39:54', '<a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/development/test-project1">Test Project</a>', 0, 19, 1, 0),
(108, 45, 'User', 'update_account', '2015-12-02 10:47:13', '<a href="http://mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has updated account information', 0, 0, 1, 0),
(109, 45, 'project', 'commented', '2015-12-02 11:42:41', '<a href="http://mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a>  Commented  on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> ', 7, 24, 1, 0),
(110, 7, 'project', 'update', '2015-12-02 11:48:18', '<a href="http://mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a> has posted new Update on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a>', 7, 24, 1, 0),
(111, 45, 'User', 'accredetial_request', '2015-12-02 11:51:16', '<a href="http://mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> applied to become <a href="http://mydesichef.com/realestate_development_ci3/admin/accreditation/accreditation_user_list">Accredited</a> Investor', 0, 0, 1, 0),
(112, 45, 'project', 'upload_contract', '2015-12-02 11:57:03', '<a href="http://mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has <a href="http://mydesichef.com/realestate_development_ci3/startinvest/index/24">Uploaded</a> Investment  contract copy on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 1, 0),
(113, 43, 'project', 'upload_contract', '2015-12-02 12:02:19', '<a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a> has <a href="http://mydesichef.com/realestate_development_ci3/startinvest/index/1">Uploaded</a> Investment  contract copy on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a>', 0, 1, 1, 0),
(114, 8, 'project', 'commented', '2015-12-02 12:02:58', '<a href="http://mydesichef.com/realestate_development_ci3/user/HardikJivani8">Hardik Jivani</a>  Commented  on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> ', 1, 1, 1, 0),
(115, 45, 'project', 'approve_contract', '2015-12-02 12:03:38', 'You have <a href="http://mydesichef.com/realestate_development_ci3/startinvest/index/24">Approve</a> Investment contract copy from <a href="http://mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 1, 0),
(116, 45, 'project', 'approve_acknowledge_contract', '2015-12-02 12:05:33', 'You have <a href="http://mydesichef.com/realestate_development_ci3/startinvest/index/24">Approved</a> Investment acknowledgement from <a href="http://mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 1, 0),
(117, 45, 'project', 'funded', '2015-12-02 12:05:34', '<a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> is Invested with $500.00 USD by <a href="http://mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a>', 7, 24, 1, 0),
(118, 45, 'project', 'approve_acknowledge_contract', '2015-12-02 12:05:49', 'You have <a href="http://mydesichef.com/realestate_development_ci3/startinvest/index/24">Approved</a> Investment acknowledgement from <a href="http://mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 1, 0),
(119, 45, 'project', 'approve_acknowledge_contract', '2015-12-02 12:05:50', 'You have <a href="http://mydesichef.com/realestate_development_ci3/startinvest/index/24">Approved</a> Investment acknowledgement from <a href="http://mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 1, 0),
(120, 45, 'project', 'approve_acknowledge_contract', '2015-12-02 12:05:54', 'You have <a href="http://mydesichef.com/realestate_development_ci3/startinvest/index/24">Approved</a> Investment acknowledgement from <a href="http://mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 1, 0),
(121, 45, 'project', 'approve_acknowledge_contract', '2015-12-02 12:05:55', 'You have <a href="http://mydesichef.com/realestate_development_ci3/startinvest/index/24">Approved</a> Investment acknowledgement from <a href="http://mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 1, 0),
(122, 45, 'project', 'confirmed_submmited_tracking', '2015-12-02 12:06:30', '<a href="http://mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has <a href="http://mydesichef.com/realestate_development_ci3/startinvest/index/24">Approved and Confirmed</a> delivery of  share certificate/document  on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 1, 0),
(123, 43, 'project', 'approve_contract', '2015-12-02 12:14:45', 'You have <a href="http://mydesichef.com/realestate_development_ci3/startinvest/index/1">Approve</a> Investment contract copy from <a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a> on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a>', 0, 1, 1, 0),
(124, 43, 'project', 'acknowledge_contract', '2015-12-02 12:21:51', '<a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a> has <a href="http://mydesichef.com/realestate_development_ci3/startinvest/index/1">confirmed & uploaded</a> Investment  acknowledgement information on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a>', 0, 1, 1, 0),
(125, 43, 'project', 'approve_acknowledge_contract', '2015-12-02 12:22:17', 'You have <a href="http://mydesichef.com/realestate_development_ci3/startinvest/index/1">Approved</a> Investment acknowledgement from <a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a> on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a>', 0, 1, 1, 0),
(126, 43, 'project', 'funded', '2015-12-02 12:22:17', '<a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> is Invested with $400.00 USD by <a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a>', 1, 1, 1, 0),
(127, 43, 'project', 'approve_acknowledge_contract', '2015-12-02 12:22:32', 'You have <a href="http://mydesichef.com/realestate_development_ci3/startinvest/index/1">Approved</a> Investment acknowledgement from <a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a> on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a>', 0, 1, 1, 0),
(128, 43, 'project', 'approve_acknowledge_contract', '2015-12-02 12:22:41', 'You have <a href="http://mydesichef.com/realestate_development_ci3/startinvest/index/1">Approved</a> Investment acknowledgement from <a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a> on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a>', 0, 1, 1, 0),
(129, 43, 'project', 'approve_acknowledge_contract', '2015-12-02 12:23:30', 'You have <a href="http://mydesichef.com/realestate_development_ci3/startinvest/index/1">Approved</a> Investment acknowledgement from <a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a> on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a>', 0, 1, 1, 0),
(130, 43, 'project', 'confirmed_submmited_tracking', '2015-12-02 12:25:02', '<a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a> has <a href="http://mydesichef.com/realestate_development_ci3/startinvest/index/1">Approved and Confirmed</a> delivery of  share certificate/document  on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a>', 0, 1, 1, 0),
(131, 46, 'User', 'signup', '2015-12-02 13:19:20', '<a href="http://mydesichef.com/realestate_development_ci3/user/VikramPatel46">Vikram Patel</a> joined Real Estate', 0, 0, 1, 0),
(132, 46, 'User', 'update_account', '2015-12-02 13:22:37', '<a href="http://mydesichef.com/realestate_development_ci3/user/VikramPatel46">Vikram Patel</a> has updated account information', 0, 0, 1, 0),
(133, 46, 'User', 'accredetial_request', '2015-12-02 13:24:21', '<a href="http://mydesichef.com/realestate_development_ci3/user/VikramPatel46">Vikram Patel</a> applied to become <a href="http://mydesichef.com/realestate_development_ci3/admin/accreditation/accreditation_user_list">Accredited</a> Investor', 0, 0, 1, 0),
(134, 46, 'project', 'upload_contract', '2015-12-02 13:27:35', '<a href="http://mydesichef.com/realestate_development_ci3/user/VikramPatel46">Vikram Patel</a> has <a href="http://mydesichef.com/realestate_development_ci3/startinvest/index/24">Uploaded</a> Investment  contract copy on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 1, 0),
(135, 46, 'project', 'approve_contract', '2015-12-02 13:27:55', 'You have <a href="http://mydesichef.com/realestate_development_ci3/startinvest/index/24">Approve</a> Investment contract copy from <a href="http://mydesichef.com/realestate_development_ci3/user/VikramPatel46">Vikram Patel</a> on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 1, 0),
(136, 46, 'User', 'submit_project', '2015-12-02 13:51:43', '<a href="http://mydesichef.com/realestate_development_ci3/user/VikramPatel46">Vikram Patel</a> submitted a new Development <a href="http://mydesichef.com/realestate_development_ci3/admin/equity/list_equity"><a href="http://mydesichef.com/realestate_development_ci3/development/new-project">New Project</a></a> for review', 0, 26, 1, 0),
(137, 46, 'User', 'project_approve', '2015-12-02 13:52:07', 'You have approved and published Development <a href="http://mydesichef.com/realestate_development_ci3/development/new-project">Rock Info</a>', 0, 26, 1, 0),
(138, 47, 'User', 'signup', '2015-12-04 17:42:48', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/SiddharthMuley47">Siddharth Muley</a> joined Real Estate', 0, 0, 1, 0),
(139, 45, 'project', 'upload_contract', '2015-12-18 14:56:09', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has <a href="http://www.mydesichef.com/realestate_development_ci3/startinvest/index/24">Uploaded</a> Investment  contract copy on <a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 1, 0),
(140, 45, 'project', 'upload_contract', '2015-12-18 14:57:22', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has <a href="http://www.mydesichef.com/realestate_development_ci3/startinvest/index/24">Uploaded</a> Investment  contract copy on <a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 1, 0),
(141, 45, 'project', 'approve_contract', '2015-12-18 14:58:42', 'You have <a href="http://www.mydesichef.com/realestate_development_ci3/startinvest/index/24">Approve</a> Investment contract copy from <a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> on <a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 1, 0),
(142, 45, 'project', 'approve_acknowledge_contract', '2015-12-18 14:59:27', 'You have <a href="http://www.mydesichef.com/realestate_development_ci3/startinvest/index/24">Approved</a> Investment acknowledgement from <a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> on <a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 1, 0),
(143, 45, 'project', 'funded', '2015-12-18 14:59:27', '<a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> is Invested with $100.00 USD by <a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a>', 7, 24, 1, 0),
(144, 45, 'project', 'approve_acknowledge_contract', '2015-12-18 14:59:32', 'You have <a href="http://www.mydesichef.com/realestate_development_ci3/startinvest/index/24">Approved</a> Investment acknowledgement from <a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> on <a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 1, 0),
(145, 45, 'project', 'confirmed_submmited_tracking', '2015-12-18 14:59:54', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has <a href="http://www.mydesichef.com/realestate_development_ci3/startinvest/index/24">Approved and Confirmed</a> delivery of  share certificate/document  on <a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 1, 0),
(146, 0, 'project', 'follow', '2015-12-21 11:55:53', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/company/rock-info">Rock Info</a>', 0, 0, 1, 56),
(147, 45, 'project', 'upload_contract', '2015-12-21 12:02:42', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has <a href="http://www.mydesichef.com/realestate_development_ci3/startinvest/index/24">Uploaded</a> Investment  contract copy on <a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 1, 0),
(148, 45, 'project', 'approve_contract', '2015-12-21 12:08:56', 'You have <a href="http://www.mydesichef.com/realestate_development_ci3/startinvest/index/24">Approve</a> Investment contract copy from <a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> on <a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 1, 0),
(149, 45, 'project', 'acknowledge_contract', '2015-12-21 12:09:17', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has <a href="http://www.mydesichef.com/realestate_development_ci3/startinvest/index/24">confirmed & uploaded</a> Investment  acknowledgement information on <a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 1, 0),
(150, 45, 'project', 'approve_acknowledge_contract', '2015-12-21 12:10:00', 'You have <a href="http://www.mydesichef.com/realestate_development_ci3/startinvest/index/24">Approved</a> Investment acknowledgement from <a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> on <a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 1, 0),
(151, 45, 'project', 'funded', '2015-12-21 12:10:00', '<a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> is Invested with $100.00 USD by <a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a>', 7, 24, 1, 0),
(152, 45, 'project', 'approve_acknowledge_contract', '2015-12-21 12:10:02', 'You have <a href="http://www.mydesichef.com/realestate_development_ci3/startinvest/index/24">Approved</a> Investment acknowledgement from <a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> on <a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 1, 0),
(153, 45, 'project', 'approve_acknowledge_contract', '2015-12-21 12:10:03', 'You have <a href="http://www.mydesichef.com/realestate_development_ci3/startinvest/index/24">Approved</a> Investment acknowledgement from <a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> on <a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 1, 0),
(154, 45, 'project', 'approve_acknowledge_contract', '2015-12-21 12:10:03', 'You have <a href="http://www.mydesichef.com/realestate_development_ci3/startinvest/index/24">Approved</a> Investment acknowledgement from <a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> on <a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 1, 0),
(155, 45, 'project', 'approve_acknowledge_contract', '2015-12-21 12:10:04', 'You have <a href="http://www.mydesichef.com/realestate_development_ci3/startinvest/index/24">Approved</a> Investment acknowledgement from <a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> on <a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 1, 0),
(156, 45, 'project', 'confirmed_submmited_tracking', '2015-12-21 12:11:18', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has <a href="http://www.mydesichef.com/realestate_development_ci3/startinvest/index/24">Approved and Confirmed</a> delivery of  share certificate/document  on <a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 1, 0),
(157, 45, 'project', 'upload_contract', '2015-12-21 13:41:27', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has <a href="http://www.mydesichef.com/realestate_development_ci3/startinvest/index/24">Uploaded</a> Investment  contract copy on <a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 0, 0),
(158, 45, 'project', 'approve_contract', '2015-12-21 13:47:09', 'You have <a href="http://www.mydesichef.com/realestate_development_ci3/startinvest/index/24">Approve</a> Investment contract copy from <a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> on <a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 0, 0),
(159, 45, 'project', 'acknowledge_contract', '2015-12-21 13:51:02', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has <a href="http://www.mydesichef.com/realestate_development_ci3/startinvest/index/24">confirmed & uploaded</a> Investment  acknowledgement information on <a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `admin_login`
--

CREATE TABLE IF NOT EXISTS `admin_login` (
  `login_id` int(100) NOT NULL,
  `admin_id` int(100) NOT NULL,
  `login_ip` varchar(255) DEFAULT NULL,
  `login_date` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=128 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_login`
--

INSERT INTO `admin_login` (`login_id`, `admin_id`, `login_ip`, `login_date`) VALUES
(1, 1, '27.109.7.130', '2015-10-02 16:56:38'),
(2, 1, '27.109.7.130', '2015-10-05 10:36:35'),
(3, 1, '27.109.7.130', '2015-10-05 10:49:30'),
(4, 1, '27.109.7.130', '2015-10-05 11:11:33'),
(5, 1, '27.109.7.130', '2015-10-05 11:21:19'),
(6, 1, '27.109.7.130', '2015-10-05 11:30:37'),
(7, 1, '27.109.7.130', '2015-10-05 16:03:01'),
(8, 1, '27.109.7.130', '2015-10-05 18:42:35'),
(9, 1, '27.109.7.130', '2015-10-06 13:34:56'),
(10, 1, '27.109.7.130', '2015-10-07 11:38:10'),
(11, 1, '150.107.210.187', '2015-10-07 17:54:21'),
(12, 1, '123.201.91.105', '2015-10-08 12:13:16'),
(13, 1, '123.201.91.105', '2015-10-08 14:36:04'),
(14, 1, '123.201.91.105', '2015-10-08 16:03:24'),
(15, 1, '123.201.91.105', '2015-10-08 17:30:12'),
(16, 1, '27.109.7.130', '2015-10-09 14:01:31'),
(17, 1, '27.109.7.130', '2015-10-09 16:28:01'),
(18, 1, '27.109.7.130', '2015-10-12 13:47:40'),
(19, 1, '27.109.7.130', '2015-10-12 15:20:35'),
(20, 1, '27.109.7.130', '2015-10-13 16:37:12'),
(21, 1, '27.109.7.130', '2015-10-14 17:34:26'),
(22, 1, '27.109.7.130', '2015-10-22 13:45:40'),
(23, 1, '219.91.173.161', '2015-10-26 13:16:50'),
(24, 1, '27.109.7.130', '2015-10-26 14:13:00'),
(25, 1, '27.109.7.130', '2015-10-26 14:21:16'),
(26, 1, '27.109.7.130', '2015-10-26 15:03:29'),
(27, 1, '219.91.173.161', '2015-10-26 15:06:52'),
(28, 1, '27.109.7.130', '2015-10-26 15:08:28'),
(29, 1, '27.109.7.130', '2015-10-26 19:08:16'),
(30, 1, '150.107.209.40', '2015-10-27 11:12:01'),
(31, 1, '150.107.209.40', '2015-10-27 11:13:51'),
(32, 1, '150.107.211.150', '2015-10-27 14:11:33'),
(33, 1, '27.109.7.130', '2015-10-27 16:57:19'),
(34, 1, '27.109.7.130', '2015-10-27 17:24:25'),
(35, 1, '27.109.7.130', '2015-10-28 10:53:41'),
(36, 1, '27.109.7.130', '2015-10-28 10:56:56'),
(37, 1, '27.109.7.130', '2015-10-28 11:22:43'),
(38, 1, '27.109.7.130', '2015-10-28 11:38:03'),
(39, 1, '27.109.7.130', '2015-10-28 11:38:55'),
(40, 1, '27.109.7.130', '2015-10-28 13:59:17'),
(41, 1, '27.109.7.130', '2015-10-29 10:58:47'),
(42, 1, '27.109.7.130', '2015-10-29 16:07:46'),
(43, 1, '27.109.7.130', '2015-11-19 12:02:46'),
(44, 1, '27.109.7.130', '2015-11-19 12:06:59'),
(45, 1, '27.109.7.130', '2015-11-19 14:03:51'),
(46, 1, '203.187.203.243', '2015-11-19 14:03:57'),
(47, 1, '203.187.203.243', '2015-11-19 14:06:52'),
(48, 1, '27.109.7.130', '2015-11-19 14:08:23'),
(49, 1, '203.187.203.243', '2015-11-19 14:10:04'),
(50, 1, '27.109.7.130', '2015-11-19 14:19:38'),
(51, 1, '27.109.7.130', '2015-11-19 14:23:56'),
(52, 1, '27.109.7.130', '2015-11-19 15:00:24'),
(53, 1, '27.109.7.130', '2015-11-19 15:50:11'),
(54, 1, '27.109.7.130', '2015-11-19 18:25:09'),
(55, 1, '27.109.7.130', '2015-11-19 18:42:32'),
(56, 1, '27.109.7.130', '2015-11-20 10:02:03'),
(57, 1, '27.109.7.130', '2015-11-20 11:17:12'),
(58, 1, '27.109.7.130', '2015-11-20 11:24:07'),
(59, 1, '123.201.98.3', '2015-11-20 12:05:49'),
(60, 1, '123.201.98.3', '2015-11-20 15:31:24'),
(61, 1, '123.201.98.3', '2015-11-20 15:35:16'),
(62, 1, '27.109.7.130', '2015-11-20 15:52:28'),
(63, 1, '27.109.7.130', '2015-11-20 16:11:11'),
(64, 1, '27.109.7.130', '2015-11-20 16:23:14'),
(65, 1, '203.187.234.4', '2015-11-20 16:47:51'),
(66, 1, '203.187.234.4', '2015-11-20 16:52:21'),
(67, 1, '203.187.234.4', '2015-11-20 16:58:24'),
(68, 1, '27.109.7.130', '2015-11-20 17:47:03'),
(69, 1, '123.201.100.5', '2015-11-25 10:36:08'),
(70, 1, '123.201.100.5', '2015-11-25 10:36:39'),
(71, 1, '27.109.7.130', '2015-11-25 15:27:33'),
(72, 1, '27.109.7.130', '2015-11-25 15:35:39'),
(73, 1, '27.109.7.130', '2015-11-25 15:42:52'),
(74, 1, '27.109.7.130', '2015-11-26 10:45:12'),
(75, 1, '27.109.7.130', '2015-11-26 14:52:33'),
(76, 1, '27.109.7.130', '2015-11-26 15:36:57'),
(77, 1, '27.109.7.130', '2015-11-26 15:54:12'),
(78, 1, '103.246.215.126', '2015-11-26 17:35:37'),
(79, 1, '27.109.7.130', '2015-11-26 18:58:43'),
(80, 1, '27.109.7.130', '2015-11-27 09:38:45'),
(81, 1, '27.109.7.130', '2015-11-27 10:51:38'),
(82, 1, '27.109.7.130', '2015-11-27 10:54:14'),
(83, 1, '27.109.7.130', '2015-11-27 11:12:06'),
(84, 1, '27.109.7.130', '2015-11-27 11:28:05'),
(85, 1, '27.109.7.130', '2015-11-27 13:46:21'),
(86, 1, '27.109.7.130', '2015-11-27 15:03:16'),
(87, 1, '123.201.212.222', '2015-11-27 15:25:44'),
(88, 1, '123.201.212.222', '2015-11-27 15:30:27'),
(89, 1, '123.201.58.233', '2015-11-27 17:16:08'),
(90, 1, '27.109.7.130', '2015-11-30 10:31:30'),
(91, 1, '27.109.7.130', '2015-11-30 13:19:09'),
(92, 1, '27.109.7.130', '2015-11-30 13:21:30'),
(93, 1, '27.109.7.130', '2015-11-30 13:30:30'),
(94, 1, '27.109.7.130', '2015-11-30 13:35:09'),
(95, 1, '27.109.7.130', '2015-11-30 16:52:15'),
(96, 1, '27.109.7.130', '2015-12-01 10:02:38'),
(97, 1, '27.109.7.130', '2015-12-01 10:15:09'),
(98, 1, '27.109.7.130', '2015-12-01 10:59:39'),
(99, 1, '123.201.161.193', '2015-12-01 12:07:01'),
(100, 1, '123.201.161.193', '2015-12-01 12:26:05'),
(101, 1, '27.109.7.130', '2015-12-01 13:34:13'),
(102, 1, '123.201.161.193', '2015-12-01 13:34:19'),
(103, 1, '27.109.7.130', '2015-12-01 15:40:53'),
(104, 1, '123.201.161.193', '2015-12-01 16:13:37'),
(105, 1, '27.109.7.130', '2015-12-02 10:33:29'),
(106, 1, '27.109.7.130', '2015-12-02 10:34:23'),
(107, 1, '27.109.7.130', '2015-12-02 10:38:39'),
(108, 1, '27.109.7.130', '2015-12-02 10:40:12'),
(109, 1, '27.109.7.130', '2015-12-02 11:02:17'),
(110, 1, '27.109.7.130', '2015-12-02 12:10:33'),
(111, 1, '27.109.7.130', '2015-12-02 13:53:55'),
(112, 1, '203.109.84.76', '2015-12-02 15:47:05'),
(113, 1, '27.109.7.130', '2015-12-02 17:55:13'),
(114, 1, '27.109.7.130', '2015-12-02 18:14:02'),
(115, 1, '27.109.7.130', '2015-12-02 18:31:09'),
(116, 1, '123.201.64.216', '2015-12-02 18:34:20'),
(117, 1, '27.109.7.130', '2015-12-02 18:56:54'),
(118, 1, '203.109.75.96', '2015-12-03 10:46:12'),
(119, 1, '27.109.7.130', '2015-12-03 13:49:34'),
(120, 1, '27.109.7.130', '2015-12-04 12:05:55'),
(121, 1, '123.201.160.118', '2015-12-18 14:34:38'),
(122, 1, '123.201.160.118', '2015-12-18 14:41:12'),
(123, 1, '27.109.7.130', '2015-12-18 15:37:06'),
(124, 1, '203.109.65.159', '2015-12-21 11:36:21'),
(125, 1, '27.109.7.130', '2015-12-21 13:46:42'),
(126, 1, '27.109.7.130', '2015-12-21 13:48:53'),
(127, 1, '203.109.65.159', '2015-12-21 13:49:38');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `is_parent` tinyint(1) DEFAULT NULL COMMENT '1="Parent" 0="Sub"',
  `name` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `description` text COLLATE utf8_bin,
  `image` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL COMMENT '0=''Inactive'' 1=''Active''',
  `created_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `language_id`, `is_parent`, `name`, `url`, `description`, `image`, `active`, `created_at`) VALUES
(1, 1, 1, 'Mixed Use', 'mixed-use', '       ', 'category_90563.png', 1, '2015-10-14 06:32:58'),
(2, 1, 1, 'Industrial', 'industrial', '  ', 'equity_category_15980.png', 1, '2015-10-14 06:19:06'),
(3, 1, 0, 'Retail', 'retail', ' ', 'equity_category_26537.jpeg', 1, '2015-10-09 04:29:29'),
(4, 2, 0, 'Residential', 'residential', '  ', 'equity_category_88564.jpeg', 1, '2015-10-09 04:30:50'),
(5, 2, 0, 'Heavy manufacturing', 'heavy-manufacturing', ' ', 'equity_category_80826.jpeg', 1, '2015-10-09 04:30:14'),
(6, 2, 0, 'Light Assembly', 'light-assembly', ' ', 'equity_category_32056.jpeg', 1, '2015-10-09 04:30:32'),
(13, 2, 1, 'Test', 'test', '  ', 'equity_category_70347.png', 0, '2015-10-13 04:38:02'),
(14, 1, 1, 'All Category', 'all-category', ' All Category', 'category_80364.jpeg', 2, '2015-11-27 09:49:09');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
  `city_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `city_name` varchar(255) DEFAULT NULL,
  `active` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`city_id`, `country_id`, `state_id`, `city_name`, `active`) VALUES
(1, 2, 11, 'Vadodara', '1'),
(2, 1, 9, 'San Jose', '1'),
(3, 1, 9, 'Milpitus', '1'),
(4, 1, 9, 'San Diago', '1'),
(5, 2, 155, 'patna', '1'),
(6, 2, 11, 'Baroda', '1'),
(7, 2, 11, 'Anand', '1'),
(8, 2, 11, 'bharuch', '1');

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
  `comment_id` int(11) NOT NULL,
  `equity_id` int(11) NOT NULL,
  `user_id` int(100) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `comments` text,
  `status` varchar(255) DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `comment_ip` varchar(255) DEFAULT NULL,
  `comment_type` tinyint(1) NOT NULL DEFAULT '0',
  `parent_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`comment_id`, `equity_id`, `user_id`, `username`, `email`, `photo`, `comments`, `status`, `date_added`, `comment_ip`, `comment_type`, `parent_id`) VALUES
(2, 24, 11, NULL, NULL, NULL, 'Hiii', '1', '2015-11-30 15:49:41', '27.109.7.130', 0, 0),
(3, 1, 43, NULL, NULL, NULL, 'Hi comments formating checking', '1', '2015-12-01 13:26:28', '123.201.161.193', 0, 0),
(4, 24, 45, NULL, NULL, NULL, 'New update from Testing\n', '1', '2015-12-02 11:42:41', '27.109.7.130', 0, 0),
(5, 1, 8, NULL, NULL, NULL, 'Hello,\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Integer efficitur diam vel sem fermentum, eu elementum augue aliquet. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\n\nNullam lacinia urna nisi, nec fringilla urna cursus at. Morbi cursus massa lectus, vitae auctor mauris mollis ac. Nullam tempus mattis vehicula. Ut eget urna efficitur, semper dolor non, pretium risus. Curabitur risus massa, feugiat sit amet vulputate non.', '1', '2015-12-02 12:02:58', '203.109.84.76', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `company_category`
--

CREATE TABLE IF NOT EXISTS `company_category` (
  `id` int(11) NOT NULL,
  `company_category_name` varchar(255) DEFAULT NULL,
  `company_category_desc` text NOT NULL,
  `language_id` int(11) NOT NULL,
  `url_company_category` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `company_category`
--

INSERT INTO `company_category` (`id`, `company_category_name`, `company_category_desc`, `language_id`, `url_company_category`, `image`, `status`) VALUES
(1, 'Film & Entertainment', ' gdsfgdsfg', 1, 'film--entertainment', 'category_97429.jpeg', 1),
(2, 'Funds', '', 1, 'funds4', 'category_51231.jpeg', 1),
(3, 'Small Business', '', 1, 'small-business3', 'category_69495.jpeg', 1),
(4, 'Social Enterprise', '', 1, 'social-enterprise3', 'category_88732.jpeg', 1),
(5, 'Tech Startup', '', 1, 'tech-startup3', 'category_3944.jpeg', 1),
(6, 'Other', '', 1, 'other3', 'category_41993.jpeg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `company_featured_projects`
--

CREATE TABLE IF NOT EXISTS `company_featured_projects` (
  `id` int(11) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `project_summary` varchar(255) NOT NULL,
  `project_street` varchar(255) NOT NULL,
  `project_city` varchar(255) NOT NULL,
  `project_state` varchar(255) NOT NULL,
  `project_country` varchar(255) NOT NULL,
  `project_image` varchar(255) NOT NULL,
  `company_id` int(11) NOT NULL,
  `equity_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_featured_projects`
--

INSERT INTO `company_featured_projects` (`id`, `project_name`, `project_summary`, `project_street`, `project_city`, `project_state`, `project_country`, `project_image`, `company_id`, `equity_id`, `user_id`) VALUES
(3, 'Stva Hights', 'Add your team members and advisors so that people know who is involved. Each stakeholder represents a powerful and personal way to demonstrate the strength of.', 'Manjalpur', 'vadodara', 'Gujarat', 'India', '81406-featured-image-1444038853.jpg', 6, 2, 4),
(4, 'Stva Hights Mint', 'Add your team members and advisors mint so that people know who is involved. Each stakeholder represents a powerful and personal way to demonstrate .', 'Manjalpur', 'vadodara', 'Gujarat', 'India', '', 6, 2, 4),
(6, 'Demo Medical Group', '', '22 furlong Drive Road', 'NJ', 'NJ', 'USA', '99049-featured-image-1444303315.jpg', 16, 5, 1),
(10, 'Real Estate', 'adfgdsadsfadsf', 'adsfdsf', 'asdfasdfas', 'adsfasdf', 'USA', '84125-featured-image-1450430097.png', 59, 38, 45);

-- --------------------------------------------------------

--
-- Table structure for table `company_follower`
--

CREATE TABLE IF NOT EXISTS `company_follower` (
  `company_follow_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `company_follow_user_id` int(11) NOT NULL,
  `company_follow_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_follower`
--

INSERT INTO `company_follower` (`company_follow_id`, `company_id`, `company_follow_user_id`, `company_follow_date`) VALUES
(0, 6, 3, '2015-10-02 14:24:53'),
(0, 2, 2, '2015-10-05 11:11:01'),
(0, 45, 11, '2015-11-19 16:02:15'),
(0, 50, 7, '2015-11-19 17:41:53'),
(0, 54, 11, '2015-11-27 13:53:05');

-- --------------------------------------------------------

--
-- Table structure for table `company_industry`
--

CREATE TABLE IF NOT EXISTS `company_industry` (
  `id` int(11) NOT NULL,
  `company_industry_name` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `company_industry`
--

INSERT INTO `company_industry` (`id`, `company_industry_name`, `status`) VALUES
(1, 'Hotel', 1),
(2, 'IT Professional', 1),
(3, 'Advertising &amp; Marketing ', 1),
(4, 'Aerospace', 1);

-- --------------------------------------------------------

--
-- Table structure for table `company_profile`
--

CREATE TABLE IF NOT EXISTS `company_profile` (
  `company_id` int(11) NOT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `website_url` varchar(255) DEFAULT NULL,
  `company_url` varchar(255) DEFAULT NULL,
  `year_founded` varchar(255) DEFAULT NULL,
  `headquater_city` varchar(255) DEFAULT NULL,
  `headquater_state` varchar(255) DEFAULT NULL,
  `headquater_country` varchar(255) DEFAULT NULL,
  `company_category` varchar(255) DEFAULT NULL,
  `company_industry` varchar(255) DEFAULT NULL,
  `assets_under_management` varchar(255) DEFAULT NULL,
  `square_footage` varchar(255) DEFAULT NULL,
  `company_assets` varchar(255) DEFAULT NULL,
  `company_logo` varchar(255) DEFAULT NULL,
  `company_overview` text,
  `company_email` varchar(255) DEFAULT NULL,
  `company_linkedin_url` varchar(255) DEFAULT NULL,
  `company_facebook_url` varchar(255) DEFAULT NULL,
  `company_twitter_url` varchar(255) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `company_cover_photo` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `company_profile`
--

INSERT INTO `company_profile` (`company_id`, `company_name`, `website_url`, `company_url`, `year_founded`, `headquater_city`, `headquater_state`, `headquater_country`, `company_category`, `company_industry`, `assets_under_management`, `square_footage`, `company_assets`, `company_logo`, `company_overview`, `company_email`, `company_linkedin_url`, `company_facebook_url`, `company_twitter_url`, `user_id`, `company_cover_photo`) VALUES
(1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '74008-company-1443783928.jpg', NULL, NULL, NULL, NULL, NULL, 0, ''),
(2, 'ABC Corp', 'http://mydesichef.com/real-estate-development/', 'abc-corp', '2000', 'Baroda', 'Gujarat', 'India', 'Tech Startup', NULL, '10', '10', NULL, '32536-company-1443784066.jpg', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<br /></p>', 'rockersinfo@gmail.com', '', '', '', 1, '25560-company-cover-photo-1443784128.png'),
(3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, ''),
(4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, ''),
(5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, ''),
(6, 'Digi Reality', '', 'digi-reality', '1485', 'Vadodara', 'Gujarat', 'India', 'Funds', NULL, '1000000', '7855212', NULL, '64269-company-1444037654.gif', '<p>Connect your social networks and website. These educate and demonstrate your credibility. Add your team members and advisors so that people know who is involved. nEach stakeholder represents a powerful and personal way to demonstrate nthe strength of your company.<br /></p>', 'kashyap.rockersinfo@gmail.com', '', '', '', 4, '19945-company-cover-photo-1444037711.png'),
(7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, ''),
(8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, ''),
(9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, ''),
(10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, ''),
(11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, ''),
(12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, ''),
(13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, ''),
(14, 'gdfsgdsgfsdfsafsaffsdf', '', 'gdfsgdsg', '2008', 'vadodara', 'gujart', 'USA', 'Social Enterprise', NULL, '5', '5', NULL, '70377-company-1444302456.jpg', '<p>gsdgsd</p>', 'ankit.rockersinfo@gmail.com', '', '', '', 1, '48994-company-cover-photo-1444302459.jpg'),
(15, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, ''),
(16, 'Test Company', 'http://mytestcompanyweb.com', 'test-company', '2001', 'Union', 'New  Jesrsey', 'USA', 'Social Enterprise', NULL, '20', '22', NULL, '22363-company-1448964584.jpg', '<p>test company<br /></p>', 'mihir@gmail.com', '', '', '', 1, '95-company-cover-photo-1448964594.png'),
(17, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, ''),
(18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, ''),
(19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, ''),
(20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, ''),
(21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, ''),
(22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, ''),
(23, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, ''),
(24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, ''),
(25, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, ''),
(26, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, ''),
(27, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, ''),
(28, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, ''),
(29, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, ''),
(30, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, ''),
(31, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, ''),
(32, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, ''),
(33, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, ''),
(34, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, ''),
(35, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, ''),
(36, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, ''),
(37, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, ''),
(38, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, ''),
(39, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, ''),
(40, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, ''),
(41, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, ''),
(42, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, ''),
(43, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, ''),
(44, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '54881-company-1444744720.png', NULL, NULL, NULL, NULL, NULL, 1, '37391-company-cover-photo-1444744726.png'),
(45, 'RockersInfo Technology', '', 'rockersinfo-technology', '2004', 'Vaodara', 'Gujarat', 'India', 'Funds', NULL, '1000', '120', NULL, '80375-company-1445845176.jpg', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and ntypesetting industry. Lorem Ipsum has been the industry''s standard dummyn text ever since the 1500s, when an unknown printer took a galley of ntype and scrambled it to make a type specimen book. It has survived not nonly five centuries, but also the leap into electronic typesetting, nremaining essentially unchanged. It was popularised in the 1960s with nthe release of Letraset sheets containing Lorem Ipsum passages, and moren recently with desktop publishing software like Aldus PageMaker nincluding versions of Lorem Ipsum.<br /></p>', 'spreston145@gmail.com', '', '', '', 7, '85666-company-cover-photo-1445845188.jpg'),
(46, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, ''),
(47, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, ''),
(48, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 16, ''),
(49, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, ''),
(50, 'Test Company1', '', 'test-company1', '2000', 'vadodara', 'gj', 'India', 'Other', NULL, '5', '10', NULL, '31991-company-1447928509.jpeg', '<p>This is company overview<br /></p>', 'testcompany@test.com', '', '', '', 16, '67187-company-cover-photo-1447928612.jpeg'),
(51, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, ''),
(52, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 37, ''),
(53, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, ''),
(54, 'Rockers Technology', 'http://rockersinfo.com/', 'rockers-technology', '2008', 'Vadodara', 'Gujarat', 'India', 'Other', NULL, '1000', '120', NULL, '63344-company-1448450439.jpg', '<p>Rockers Technology is U.S. registered Technology company providing \\\\r\\\\nCustom Software &amp; Application Development, Developers on Demand and \\\\r\\\\nQA/Software testing at affordable rates. We have managers in Connecticut\\\\r\\\\n and Massachusetts directing a staff in India to provide Development and\\\\r\\\\n IT expertise to clients around the world.<br /></p>', 'rockersinfo@gmail.com', 'https://www.linkedin.com/company/rockers-technologies?trk=tyah', 'https://www.facebook.com/RockersTechnology?fref=ts', 'https://twitter.com/rockersinfo', 7, '88252-company-cover-photo-1448450547.jpg'),
(55, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 43, ''),
(56, 'Rock Info', 'http://rockersinfo.com', 'rock-info', '2008', 'Vadodara', 'Gujarat', 'India', 'Tech Startup', NULL, '1200', '120', NULL, '50022-company-1449043862.jpg', '<p>Company Overview</p>', 'vatsal.test1.rockersinfo@gmail.com', '', '', '', 46, '73531-company-cover-photo-1449043871.jpeg'),
(57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, ''),
(58, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, ''),
(59, 'Rockers Info', 'http://rockersinfo.com', 'rockers-info', '2008', 'Vadodara', 'USA', 'India', 'Other', NULL, '12000', '12', NULL, '94633-company-1450430020.jpg', '<p>sdafdsfasffffsfssdfdsffdsfds<br /></p>', 'rockersinfo@gmail.com', '', '', '', 45, '60391-company-cover-photo-1450430038.png'),
(60, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 45, '');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `country_id` int(11) NOT NULL,
  `country_name` varchar(255) DEFAULT NULL,
  `fips` varchar(255) DEFAULT NULL,
  `iso2` varchar(255) DEFAULT NULL,
  `iso3` varchar(255) DEFAULT NULL,
  `ison` varchar(255) DEFAULT NULL,
  `internet` varchar(255) DEFAULT NULL,
  `capital` varchar(255) DEFAULT NULL,
  `map_ref` varchar(255) DEFAULT NULL,
  `singular` varchar(255) DEFAULT NULL,
  `plural` varchar(255) DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `currency_code` varchar(255) DEFAULT NULL,
  `population` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `comment` text,
  `active` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=219 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`country_id`, `country_name`, `fips`, `iso2`, `iso3`, `ison`, `internet`, `capital`, `map_ref`, `singular`, `plural`, `currency`, `currency_code`, `population`, `title`, `comment`, `active`) VALUES
(1, 'USA', '', '', '', '', '', '', '', '', '', '', '$', '', '', '', '1'),
(2, 'India', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '1'),
(3, 'Japan', '', '', '', '', '', '', '', '', '', '', '#', '', '', '', '0'),
(4, 'Qatar', '', '', '', '', '', 'doha', '', '', '', 'Riyal', '', '20000000', '', '', '0'),
(5, 'United Kingdom', '', '', '', '', '', 'London', '', '', '', '', '', '30000000', '', '', '0'),
(6, 'Russia', '', '', '', '', '', '', '', '', '', '', '', '50000000', '', '', '0'),
(7, 'Fiji', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(8, 'Afghanistan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '1'),
(9, 'Albania', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '1'),
(10, 'Algeria', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '1'),
(11, 'Andorra', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '1'),
(12, 'Angola', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '1'),
(13, 'Antigua and Barbuda', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '1'),
(14, 'Argentina', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '1'),
(15, 'Armenia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '1'),
(18, 'Australia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '1'),
(19, 'Austria', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '1'),
(20, 'Azerbaijan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '1'),
(21, 'Bahamas', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(22, 'Bahrain', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(23, 'Bangladesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(24, 'Barbados', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(26, 'Belarus', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(27, 'Belgium', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '1'),
(28, 'Belize', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(29, 'Benin', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(30, 'Bhutan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(31, 'Bolivia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(32, 'Bosnia and Herzegovina', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(33, 'Botswana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(34, 'Brazil', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '1'),
(35, 'Brunei ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(36, 'Bulgaria', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(37, 'Burkina Faso', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(38, 'Burma', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(39, 'Burundi', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(40, 'Cambodia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(41, 'Cameroon', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(42, 'Canada', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(43, 'Cape Verde', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(44, 'Central African Republic', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(45, 'Chad', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(46, 'Chile', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(47, 'China', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(48, 'Colombia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(49, 'Comoros', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(50, 'Congo ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(51, 'Costa Rica', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(52, 'Croatia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(53, 'Cuba', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(54, 'Cyprus', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(55, 'Czech Republic', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(56, 'Denmark', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(57, 'Djibouti', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(58, 'Dominica', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(59, 'Dominican Republic', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(60, 'Ecuador', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(61, 'Egypt', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(62, 'El Salvador', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(63, 'Equatorial Guinea', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(64, 'Eritrea', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(65, 'Estonia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(66, 'Ethiopia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(67, 'Finland', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(68, 'France', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(69, 'Gabon', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(70, 'Gambia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(71, 'Georgia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(72, 'Germany', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(73, 'Ghana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(74, 'Grenada', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(75, 'Guatemala', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(76, 'Guinea', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(77, 'Guinea-Bissau', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(78, 'Guyana', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(79, 'Haiti', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(81, 'Holy See', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(82, 'Honduras', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(83, 'Hong Kong', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(84, 'Hungary', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(85, 'Iceland', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(86, 'Indonesia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(87, 'Iran', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(88, 'Iraq', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(89, 'Ireland', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(90, 'Italy', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(91, 'Jamaica', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(92, 'Jordan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(93, 'Kazakhstan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(94, 'Kenya', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(95, 'Kiribati', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(96, 'Korea', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(97, 'Kosovo', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(98, 'Kuwait', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(99, 'Kyrgyzstan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(100, 'Laos', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(101, 'Latvia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(102, 'Lebanon', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(103, 'Lesotho', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(104, 'Liberia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(105, 'Libya', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(106, 'Liechtenstein', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(107, 'Lithuania', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(108, 'Luxembourg', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(109, 'Macau', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(110, 'Macedonia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(111, 'Madagascar', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(112, 'Malawi', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '1'),
(113, 'Malaysia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(114, 'Maldives', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(115, 'Mali', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(116, 'Malta', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(117, 'Uruguay', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(118, 'Marshall Islands', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(119, 'Mauritania', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(120, 'Mauritius', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(121, 'Mexico', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(122, 'Micronesia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(123, 'Moldova', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(124, 'Monaco', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(125, 'Mongolia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(126, 'Montenegro', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(127, 'Morocco', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(128, 'Mozambique', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(129, 'Namibia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(130, 'Nauru', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(131, 'Nepal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(132, 'Netherlands', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(133, 'Netherlands Antilles', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(134, 'New Zealand', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(135, 'Nicaragua', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(136, 'Niger', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(137, 'Nigeria', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(138, 'North Korea', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(139, 'Norway', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(140, 'Oman', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(141, 'Pakistan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(142, 'Palau', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(143, 'Palestinian Territories', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(144, 'Panama', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(145, 'Papua New Guinea', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(146, 'Paraguay', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(147, 'Peru', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(148, 'Philippines', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(149, 'Poland', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(152, 'Romania', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(154, 'Rwanda', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(155, 'Saint Kitts and Nevis', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(156, 'Saint Lucia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(157, 'Saint Vincent and the Grenadines', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(158, 'Samoa ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(159, 'San Marino', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(160, 'Sao Tome and Principe', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(161, 'Saudi Arabia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(162, 'Senegal', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(163, 'Serbia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(164, 'Seychelles', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(167, 'Sierra Leone', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(169, 'Slovakia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(170, 'Slovenia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(171, 'Solomon Islands', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(172, 'Somalia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(173, 'South Africa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(174, 'South Korea', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(175, 'South Sudan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(176, 'Spain ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(177, 'Sri Lanka', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(178, 'Sudan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(179, 'Suriname', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(180, 'Swaziland ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(181, 'Sweden', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(182, 'Switzerland', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(183, 'Syria', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(184, 'Taiwan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(185, 'Tajikistan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(186, 'Tanzania', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(187, 'Thailand ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(188, 'Timor-Leste', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(189, 'Togo', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(190, 'Tonga', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(191, 'Trinidad and Tobago', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(193, 'Tunisia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(194, 'Turkey', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(195, 'Turkmenistan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(196, 'Tuvalu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(197, 'Uganda', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(198, 'Ukraine', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(199, 'United Arab Emirates', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(200, 'Uzbekistan', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(201, 'Vanuatu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(202, 'Venezuela', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(203, 'Vietnam', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(204, 'Yemen', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(205, 'Zambia', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(206, 'Zimbabwe', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(207, 'singapore', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0'),
(210, 'puerto rico', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0'),
(211, 'Portugal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(216, 'teatsts', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(218, 'Alphabet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1');

-- --------------------------------------------------------

--
-- Table structure for table `cronjob`
--

CREATE TABLE IF NOT EXISTS `cronjob` (
  `cronjob_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `cronjob` varchar(255) DEFAULT NULL,
  `date_run` datetime NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `crons`
--

CREATE TABLE IF NOT EXISTS `crons` (
  `crons_id` int(11) NOT NULL,
  `cron_function` varchar(255) DEFAULT NULL,
  `cron_title` varchar(255) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `crons`
--

INSERT INTO `crons` (`crons_id`, `cron_function`, `cron_title`) VALUES
(1, 'set_auto_ending', 'Preapprove the Ended Project');

-- --------------------------------------------------------

--
-- Table structure for table `currency_code`
--

CREATE TABLE IF NOT EXISTS `currency_code` (
  `currency_code_id` int(50) NOT NULL,
  `currency_name` varchar(50) DEFAULT NULL,
  `currency_code` varchar(50) DEFAULT NULL,
  `currency_symbol` varchar(50) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `currency_code`
--

INSERT INTO `currency_code` (`currency_code_id`, `currency_name`, `currency_code`, `currency_symbol`, `active`) VALUES
(24, 'Canadian Dollar', 'CAD', 'CAD', 1),
(3, 'Czech Koruna', 'CZK', 'kc', 1),
(4, 'Danish Krone', 'DKK', 'kr', 1),
(5, 'Euro', 'EUR', '&euro;', 1),
(6, 'Hong Kong Dollar', 'HKD', '$', 1),
(7, 'Hungarian Forint', 'HUF', 'Ft', 1),
(8, 'Israeli New Sheqel', 'ILS', ' &#8362;', 1),
(10, 'Mexican Peso', 'MXN', '$', 1),
(11, 'Norwegian Krone', 'NOK', 'kr', 1),
(12, 'New Zealand Dollar', 'NZD', '$', 1),
(13, 'Polish Zloty', 'PLN', 'zt', 1),
(14, 'Pound Sterling', 'GBP', '&pound;', 1),
(15, 'Singapore Dollar', 'SGD', '$', 1),
(16, 'Swedish Krona', 'SEK', 'kr', 1),
(17, 'Swiss Franc', 'CHF', 'CHF', 1),
(18, 'U.S. Dollar', 'USD', '$', 1),
(19, 'testib', 'f', 'f', 0),
(27, 'Japanese Yen', 'JPY', '¥', 1);

-- --------------------------------------------------------

--
-- Table structure for table `deal_type_setting`
--

CREATE TABLE IF NOT EXISTS `deal_type_setting` (
  `deal_type_id` int(11) NOT NULL,
  `deal_type_name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `deal_type_description` varchar(255) DEFAULT NULL,
  `deal_type_icon` varchar(255) DEFAULT NULL,
  `min_price_per_share` varchar(255) DEFAULT NULL,
  `max_price_per_share` varchar(255) DEFAULT NULL,
  `min_equity_available` varchar(255) DEFAULT NULL,
  `max_equity_available` varchar(255) DEFAULT NULL,
  `min_company_valuation` varchar(255) DEFAULT NULL,
  `max_company_valuation` varchar(255) DEFAULT NULL,
  `min_convertible_interest` varchar(255) DEFAULT NULL,
  `max_convertible_interest` varchar(255) DEFAULT NULL,
  `min_convertible_term_length` varchar(255) DEFAULT NULL,
  `max_convertible_term_length` varchar(255) DEFAULT NULL,
  `min_valuation_cap` varchar(255) DEFAULT NULL,
  `max_valuation_cap` varchar(255) DEFAULT NULL,
  `min_conversation_discount` varchar(255) DEFAULT NULL,
  `max_conversation_discount` varchar(255) DEFAULT NULL,
  `min_warrant_coverage` varchar(255) DEFAULT NULL,
  `max_warrant_coverage` varchar(255) DEFAULT NULL,
  `min_debt_interest` varchar(255) DEFAULT NULL,
  `max_debt_interest` varchar(255) DEFAULT NULL,
  `min_debt_term_length` varchar(255) DEFAULT NULL,
  `max_debt_term_length` varchar(255) DEFAULT NULL,
  `min_return_percentage` varchar(255) DEFAULT NULL,
  `max_return_percentage` varchar(255) DEFAULT NULL,
  `min_maximum_return` varchar(255) DEFAULT NULL,
  `max_maximum_return` varchar(255) DEFAULT NULL,
  `status` tinyint(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `deal_type_setting`
--

INSERT INTO `deal_type_setting` (`deal_type_id`, `deal_type_name`, `slug`, `deal_type_description`, `deal_type_icon`, `min_price_per_share`, `max_price_per_share`, `min_equity_available`, `max_equity_available`, `min_company_valuation`, `max_company_valuation`, `min_convertible_interest`, `max_convertible_interest`, `min_convertible_term_length`, `max_convertible_term_length`, `min_valuation_cap`, `max_valuation_cap`, `min_conversation_discount`, `max_conversation_discount`, `min_warrant_coverage`, `max_warrant_coverage`, `min_debt_interest`, `max_debt_interest`, `min_debt_term_length`, `max_debt_term_length`, `min_return_percentage`, `max_return_percentage`, `min_maximum_return`, `max_maximum_return`, `status`) VALUES
(1, 'Equity', 'equity', 'Priced quity in your company updated', 'deal_icon74098.png', '10', '200', '8', '10', '100000', '10000000', '2.4', '4', '656', '45', '56', '65', '6', '6', '23', '65', '56', '4', '2', '8', '89', '7', '7', '8', 1),
(2, 'Convertible Note', 'convertible_note', 'Debt that can convert to equity', 'deal_icon56465.png', '', '', '', '', '', '', '10', '20', '10', '20', '1000', '20000', '10', '20', '100', '1000', '54', '45', '45', '9', '89', '56', '78', '67', 1),
(3, 'Debt', 'debt', 'Borrow from investors at set interest terms', 'dept_icon.png', '22.4', '2.4', '2.4', '2.4', '2.4', '2.4', '32.3', '13', '4', '3', '34', '5', '56', '345', '8', '56', '8', '24', '12', '48', '67', '6', '67', '78', 1),
(4, 'Revenue Share', 'revenue_share', 'Sell a portion of your future revenues to your investors', 'revenue.png', '2.43', '2.4', '2.4', '2.4', '2.4', '2.4', '2.4', '75', '3', '65', '46', '23', '5', '45', '67', '56', '434', '4', '23', '89', '55', '76', '5', '677', 1);

-- --------------------------------------------------------

--
-- Table structure for table `document_type`
--

CREATE TABLE IF NOT EXISTS `document_type` (
  `id` int(11) NOT NULL,
  `document_type_name` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `document_type`
--

INSERT INTO `document_type` (`id`, `document_type_name`, `status`) VALUES
(1, 'Investor Pitch Deck', 1),
(2, 'Business Plan', 1),
(3, 'Disclosures', 1);

-- --------------------------------------------------------

--
-- Table structure for table `dynamic_slider`
--

CREATE TABLE IF NOT EXISTS `dynamic_slider` (
  `dynamic_slider_id` int(11) NOT NULL,
  `dynamic_image_title` varchar(255) DEFAULT NULL,
  `dynamic_image_paragraph` varchar(255) DEFAULT NULL,
  `dynamic_image_image` varchar(255) DEFAULT NULL,
  `color_picker` varchar(255) DEFAULT NULL,
  `color_picker_content` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `link_name` varchar(255) DEFAULT NULL,
  `font_type_id` varchar(255) DEFAULT NULL,
  `active` varchar(1) DEFAULT NULL,
  `small_text` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dynamic_slider`
--

INSERT INTO `dynamic_slider` (`dynamic_slider_id`, `dynamic_image_title`, `dynamic_image_paragraph`, `dynamic_image_image`, `color_picker`, `color_picker_content`, `link`, `link_name`, `font_type_id`, `active`, `small_text`) VALUES
(1, 'Join an Excellant', 'Equity Funding Community', 'category_65953.jpeg', '', '', '/', 'Join Now', '0', '1', '0'),
(2, 'Invest in businesses', 'you believe in', 'category_97448.jpeg', '', '', '/', 'Browse Offerings', '0', '1', '0'),
(3, 'Raise Equity ', 'for your excellant businses ideas', 'category_41580.jpeg', '', '', '/', 'Offer Equity Now', '0', '1', '0');

-- --------------------------------------------------------

--
-- Table structure for table `email_setting`
--

CREATE TABLE IF NOT EXISTS `email_setting` (
  `email_setting_id` int(10) NOT NULL,
  `mailer` varchar(50) DEFAULT NULL,
  `sendmail_path` varchar(255) DEFAULT NULL,
  `smtp_port` varchar(50) DEFAULT NULL,
  `smtp_host` varchar(255) DEFAULT NULL,
  `smtp_email` varchar(255) DEFAULT NULL,
  `smtp_password` varchar(255) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `email_setting`
--

INSERT INTO `email_setting` (`email_setting_id`, `mailer`, `sendmail_path`, `smtp_port`, `smtp_host`, `smtp_email`, `smtp_password`) VALUES
(1, 'sendmail', '/usr/sbin/sendmail', '25', 'mail.groupfund.me', 'smtp@groupfund.me', 'smtp2123');

-- --------------------------------------------------------

--
-- Table structure for table `email_template`
--

CREATE TABLE IF NOT EXISTS `email_template` (
  `email_template_id` int(11) NOT NULL,
  `task` varchar(255) DEFAULT NULL,
  `from_address` varchar(255) DEFAULT NULL,
  `reply_address` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `message` text
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `email_template`
--

INSERT INTO `email_template` (`email_template_id`, `task`, `from_address`, `reply_address`, `subject`, `message`) VALUES
(1, 'Welcome Email', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Email Verified Successfully', '<p><strong><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2">{user_name}</span></strong>,</p>\n\n<p>Your email is verified and your account has been activated, Now start exploring and enjoying our uninterrupted services. We wish you all success while being with us.</p>\n\n<p><span style="color:rgb(153, 153, 153); font-family:arial,helvetica,sans-serif; font-size:12px">Sincerely,</span></p>\n\n<p><strong>{site_name} Team</strong></p>\n'),
(2, 'New User Join', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Sign up successfully on {site_name}', '<p><strong><span style="color:#39434d">Howdy</span> <span style="color:#3bb0d2">{user_name}</span></strong>, welcome to {site_name}!</p>\n\n<p><span style="color:rgb(38, 38, 38); font-family:arial,sans-serif">congratulations!!! We are connecting entrepreneurs with {funds_plural}&nbsp;around the world to help fund their business and fuel economic growth.</span><br />\n<br />\n<span style="color:rgb(38, 38, 38); font-family:arial,sans-serif">Verify your email address by clicking {verify_link}&nbsp;this link to verify your email. Once your email is verified you can also edit your account settings, connect with Facebook, and add pictures and information to share with our community. Adding images and completing your profile increases the trust &amp; transparency of your profile, making it more credible to other users.</span><br />\n<br />\n<span style="color:rgb(38, 38, 38); font-family:arial,sans-serif">Entrepreneurs</span><br />\n<br />\n<span style="color:rgb(38, 38, 38); font-family:arial,sans-serif">Get Started For Free: Start by creating your Fundraising Campaign. {site_name} allows you to build a powerful online pitch and engage all your stakeholders (Advisors, Investors &amp; Team) to effectively market your Deal and get funded.</span><br />\n<br />\n<span style="color:rgb(38, 38, 38); font-family:arial,sans-serif">&nbsp;&nbsp;&nbsp; - NO APPLICATION PROCESS: {create_step_link} Start your {project_name} whenever you&rsquo;re ready! {site_name} allows you to build a powerful online pitch and engage all your stakeholders (Advisors, Investors &amp; Team) to effectively market your Deal and get funded.</span><br />\n<span style="color:rgb(38, 38, 38); font-family:arial,sans-serif">&nbsp;&nbsp;&nbsp; - GLOBAL: Receive {funds_name}&nbsp;from around the world.</span><br />\n<span style="color:rgb(38, 38, 38); font-family:arial,sans-serif">&nbsp;&nbsp;&nbsp; - CUSTOMER HAPPINESS: Get fast answers to your questions from real people.</span><br />\n<br />\n<span style="color:rgb(38, 38, 38); font-family:arial,sans-serif">To Invest In Top Companies</span><br />\n<br />\n<span style="color:rgb(38, 38, 38); font-family:arial,sans-serif">{user_accrediation_link} Verify Your Accredited Status : In order to be a part of the exclusive {site_name} {funds_name} Community, you first must verify your accredited status. Once you are a verified Accredited {funds_name}&nbsp;, you will then have access to all the Deals on {site_name}.</span><br />\n<br />\n<span style="color:rgb(38, 38, 38); font-family:arial,sans-serif">If you are interested in contributing to a {project_name} and have questions to the {project_owner_name}, we encourage you to contact the {project_owner_name} by leaving a private comment on the {project_name} you are interested in {funds_gerund} to.</span></p>\n\n<p><span style="color:rgb(38, 38, 38); font-family:arial,sans-serif">Explore what&#39;s on {site_name}</span></p>\n\n<p><br />\n&nbsp;<span style="color:rgb(153, 153, 153); font-family:arial,helvetica,sans-serif; font-size:12px">Sincerely,</span>&nbsp;</p>\n\n<p><strong>{site_name} Team. </strong></p>\n'),
(3, 'Forgot Password', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Forgot Password Request, {site_name}', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2">{user_name}</span></strong>, {break}Your request has been submitted to {site_name}.{break} Your Email Address {email} and Password is {password}.{break} Now you can take login from {login_link}. {break} <span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span>.</p>\n'),
(4, 'Admin User Active', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Your Account Activated, {site_name}', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2">{user_name}</span></strong>,{break}Your account has been activated by administrator.{break}Your Email Address {email} and Password is {password}.{break}Now you can take login from {login_link}.{break}&nbsp;<span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span></p>\n'),
(5, 'Admin User Deactivate', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Your Account Deactivated, {site_name}', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2">{user_name}</span></strong>,{break}{break}Your account has been deactivated by Administrator.{break}<span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span></p>\n'),
(6, 'Admin User Delete', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Your Account Delete, {site_name}', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2">{user_name}</span></strong>,{break}{break}Your account has been deleted by Administrator.{break}{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span></p>\n'),
(7, 'Contact Us', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'New inquiry submitted: {site_name}', '<p><strong><span style="color:#39434d">Hello</span>&nbsp;<span style="color:rgb(59, 176, 210)">Administrator</span></strong>,{break}You have new inquiry by {name},{email}.{break} {message}.{break}&nbsp;<strong>Sincerely,</strong>{break}<strong>&nbsp;System Administrator.</strong></p>\n'),
(8, 'New Equity Successful Alert', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Your {project_name} submitted successfully on {site_name}', '<p><strong><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2">{user_name}</span></strong>,</p>\n\n<p>Congratulations!!! You have successfully submitted your {project_name}: &nbsp;{company_name}</p>\n\n<p>Please wait until {site_name} team reviews your {project_name} and get back to you which will take 48 business hours.</p>\n\n<p><span style="color:rgb(153, 153, 153); font-family:arial,helvetica,sans-serif; font-size:12px; line-height:1.6em">Sincerely,</span><span style="line-height:1.6em">&nbsp;</span></p>\n\n<p>{site_name}<strong> Team.</strong></p>\n'),
(9, 'Admin Project Activate Alert', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Congrats your {project_name} approved and live on {site_name}', '<p><strong><span style="color:#39434d">Hello </span> <span style="color:#3bb0d2">{user_name}</span></strong>,</p>\n\n<p>Congratulations!!!<span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">&nbsp;{site_name} team has </span>approved and&nbsp;published<span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px"> your </span>{project_name}<span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">. </span>You can see your live {project_name} from this link: {equity_page_link}.</p>\n\n<p><span style="color:rgb(153, 153, 153); font-family:arial,helvetica,sans-serif; font-size:12px">Sincerely,</span></p>\n\n<p><strong>{site_name} Team.</strong></p>\n'),
(10, 'Admin Project Declined Alert', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Boomer! your {project_name} is declined: {site_name}', '<p><strong><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2">{user_name}</span></strong>,</p>\n\n<p>Unfortunately {site_name} team has declined your {project_name} due to following reasons. Please contact {site_name} team via email here: {admin-email}&nbsp;for further information.</p>\n\n<p>Reason to decline your {project_name}<br />\n------------------------<br />\n{reason}</p>\n\n<p><span style="color:rgb(153, 153, 153); font-family:arial,helvetica,sans-serif; font-size:12px">Sincerely,</span>&nbsp;</p>\n\n<p><strong>{site_name} Team.</strong></p>\n'),
(11, 'Admin Project Delete Alert', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Your {project_name} is deleted from {site_name}', '<p><strong><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2">{user_name}</span></strong>,</p>\n\n<p>{site_name} team has deleted your {project_name}: {company_name},&nbsp;you can not resubmit your {project_name}&nbsp;but you will have to create new {project_name} again if you want to resubmit for review.</p>\n\n<p>Please contact {site_name} team via email here: {admin-email}&nbsp;for further information.</p>\n\n<p><span style="color:rgb(153, 153, 153); font-family:arial,helvetica,sans-serif; font-size:12px">Sincerely,</span></p>\n\n<p><strong>{site_name} Team.</strong></p>\n'),
(12, 'New Comment Admin Alert', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'New {comment} posted on {project_name}, {company_name}', '<p><strong><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2"> Administrator</span></strong>,</p>\n\n<p>New {comment} posted by {comment_user_name} on {project_name} {company_name}.</p>\n\n<p>{comment} : {comments}</p>\n\n<p>{comment} Profile Link : {comment_user_profile_link}</p>\n\n<p><span style="color:rgb(153, 153, 153); font-family:arial,helvetica,sans-serif; font-size:12px">Sincerely,</span>&nbsp;</p>\n\n<p><strong>{site_name} Team.</strong></p>\n'),
(13, 'New Comment Owner Alert', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'New {comment} posted on your {project_name}, {company_name}', '<p><strong><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2">{user_name}</span></strong>,</p>\n\n<p>{comment_user_name} has {comment}&nbsp; on {project_name} you created named {company_name}. Click &quot;{comment_plural}&quot; Tab to read {comment_plural} on this {project_name} &nbsp;link: {equity_page_link}</p>\n\n<p>{comment} Profile Link : {comment_user_profile_link}</p>\n\n<p>{comment_user_name}&#39;s {comment}&nbsp;<br />\n----------------------------------------<br />\n{comment}&nbsp;</p>\n\n<p><span style="color:rgb(153, 153, 153); font-family:arial,helvetica,sans-serif; font-size:12px">Sincerely,</span>&nbsp;</p>\n\n<p><strong>{site_name} Team.</strong></p>\n'),
(14, 'New Comment Poster Alert', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'New Comment posted on project, {site_name}', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello </span> <span style="color:#3bb0d2">{user_name}</span></strong>, New comment posted by {comment_user_name} on Project {project_name}. {break} Comment : {comment} {break} Comment Profile Link : {comment_user_profile_link} {break} &nbsp;<span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span></p>\n'),
(15, 'New Fund Admin Notification', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'New {funds} Added on {project_name},{company_name}', '<p><strong><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2"> Administrator</span></strong>,</p>\n\n<p>New Fund({donate_amount}) added on the {company_name} {project_page_link} by {donor_name}.</p>\n\n<p><span style="color:rgb(153, 153, 153); font-family:arial,helvetica,sans-serif; font-size:12px">Sincerely,</span>&nbsp;</p>\n\n<p><strong>{site_name} Team.</strong></p>\n'),
(16, 'New Fund Owner Notification', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'New {funds} on {project_name} you created: {company_name}', '<p><strong><span style="color:#39434d">Hello </span> <span style="color:#3bb0d2">{user_name}</span></strong>,</p>\n\n<p>New Fund&nbsp;{donate_amount}&nbsp;added on the {project_name} {project_page_link} by {donor_name}.</p>\n\n<p>{donor_name} has {funds_past} with {donate_amount} on {project_name} you created named . Click &quot;{funds_plural}&quot; Tab to view all {funds_plural} on this {project_name} link: {project_page_link}</p>\n\n<p>With this {project_name} is now,<br />\nFunded: {percentage}% {funds_past} so far<br />\nRaised: {raised_amount} of Goal: {goal}<br />\nDay Left: {days_left}</p>\n\n<p><span style="color:rgb(153, 153, 153); font-family:arial,helvetica,sans-serif; font-size:12px">Sincerely,</span>&nbsp;</p>\n\n<p><strong>{site_name} Team.</strong></p>\n'),
(17, 'New Fund Donor Notification', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Your {funds} received successfully on {company_name}', '<p><strong><span style="color:#39434d">Hello </span> <span style="color:#3bb0d2">{donor_name}</span></strong>,</p>\n\n<p>Thanks for you {funds} and for your kind support.</p>\n\n<p>Your {funds} amount: {donate_amount} received successfully on {company_name}.</p>\n\n<p>Please keep visited {company_name}: {project_page_link}&nbsp;for all latest updates.</p>\n\n<p><span style="color:rgb(153, 153, 153); font-family:arial,helvetica,sans-serif; font-size:12px">Sincerely,</span>&nbsp;</p>\n\n<p><strong>{site_name} Team.</strong></p>\n'),
(18, 'New Equity Draft Successful Alert', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Congrats on creating a {project_name} on {site_name}', '<p><strong><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2"> {user_name}</span></strong>,</p>\n\n<p>We noticed you started a new {project_name} &ndash; exciting! We&rsquo;re here to help you begin raising funds today.</p>\n\n<p>{complete_my_campaign}</p>\n\n<p><span style="color:rgb(153, 153, 153); font-family:arial,helvetica,sans-serif; font-size:12px">Sincerely,</span>&nbsp;</p>\n\n<p><strong>{site_name} Team.</strong></p>\n'),
(19, 'New Equity Post Admin Alert', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'IMPORTANT- New {project_name} created by {equity_owner_name}', '<p><strong><span style="color:#39434d">Hello </span> <span style="color:#3bb0d2"> Administrator</span></strong>,</p>\n\n<p>New {project_name}: {equity_page_link} has been created by&nbsp;&nbsp;{equity_owner_name}, please visit this link: {equity_list_link}&nbsp;to review more details.</p>\n\n<p>Here is&nbsp;{project_name}&nbsp;Summary:</p>\n\n<p>{project_name}&nbsp;Name: &nbsp;&nbsp;{equity_page_link}</p>\n\n<p>Created by:&nbsp;{equity_owner_name}</p>\n\n<p>{project_name}&nbsp;Category: {company_category}, {company_industry}</p>\n\n<p>{project_name}&nbsp;Duration: {campaign_duration_deadline}</p>\n\n<p>{project_name}&nbsp;Deal: {deal_type}</p>\n\n<p>Goal: {campaign_goal}</p>\n\n<p>You can review this &nbsp;{project_name}&nbsp;from here: &nbsp;{equity_page_link}&nbsp;and&nbsp;{equity_detail_admin_link}&nbsp;</p>\n\n<p>You can provide feedback, message&nbsp;&nbsp;{equity_owner_name}&nbsp;regarding&nbsp;{project_name}&nbsp;from here: {equity_user_messasing_link_in_admin}</p>\n\n<p>You can approve/decline&nbsp;{project_name}&nbsp;from here:&nbsp;{equity_list_link}&nbsp;</p>\n\n<p>You can feature this&nbsp;{project_name}&nbsp;from here:&nbsp;{equity_list_link}</p>\n\n<p>&nbsp;<span style="color:rgb(153, 153, 153); font-family:arial,helvetica,sans-serif; font-size:12px">Sincerely,</span>&nbsp;</p>\n\n<p><strong>{site_name} Team.</strong></p>\n'),
(20, 'Project Finish Admin Alert', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Project Finish Alert', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello </span> <span style="color:#3bb0d2"> Administrator</span></strong>, {break}{break} Project : {project_name}{project_page_link}. {break}{break} Summary : {project_summary} {break}{break} Creator : {user_name}{user_profile_link} {break}{break} Goal : {project_goal}.{break}{break} Raise : {project_total_raise}.{break}{break} Backers : {break} {backer_list}{break}{break} <span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span></p>\n'),
(21, 'Project Finish Owner Alert', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Project Finish Alert', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2"> {user_name}</span></strong>, {break}{break} Project : {project_name}{project_page_link}. {break}{break} Summary : {project_summary} {break}{break} Creator : {user_name}{user_profile_link} {break}{break} Goal : {project_goal}.{break}{break} Raise : {project_total_raise}.{break}{break} Backers : {break} {backer_list}{break}{break} <span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span></p>\n'),
(22, 'Project Finish Donor Alert', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Project Finish Alert', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2"> {donor_name}</span></strong>, {break}{break} Project : {project_name}{project_page_link}. {break}{break} Summary : {project_summary} {break}{break} Creator : {user_name}{user_profile_link} {break}{break} Goal : {project_goal}.{break}{break} Raise : {project_total_raise}.{break}{break} Backers : {break} {own_backer}{break}{break} <span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span></p>\n'),
(23, 'Change Password', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Change Password Request, {site_name}', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2"> {user_name}</span></strong>,{break} Your request has been submitted to {site_name}.{break} Your Password is updated successfully. New login details are as below: {break} Email Address: {email} {break}Password: {password}.{break} {break} {break} <span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span></p>\n'),
(24, 'Donation Cancel User Notification', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Fund cancelled on Project', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2"> {user_name}</span></strong>,{break} Fund of ({donote_amount}) on the {project_name} {project_page_link} by {donor_name} {donor_profile_link} is cancelled. {break} <span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span></p>\n'),
(25, 'Donation Cancel Donor Notification', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Fund cancelled on Project', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello </span> <span style="color:#3bb0d2">{donor_name}</span></strong>,{break} Fund of ({donote_amount}) on the {project_name} {project_page_link} is cancelled. {break}&nbsp;<span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span></p>\n'),
(26, 'Donation Cancel Admin Notification', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Fund cancelled on Project', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2"> Administrator</span></strong>,{break} Fund of ({donote_amount}) on the {project_name} {project_page_link} by {donor_name} {donor_profile_link} is cancelled. {break}&nbsp;<span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<b>&nbsp;</b><span style="line-height: 1.6em;"><strong>System Administrator</strong>{break}</span><strong><span style="line-height: 1.6em;">Thank You.</span></strong></p>\n'),
(27, 'Project Cancelled Admin Notification', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Project cancelled on {site_name}', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2"> Administrator</span></strong>,{break} The {project_name} {project_page_link} is cancelled as its end date is reached and it is not successful. {break} <span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<strong><span style="line-height: 1.6em;">System Administrator{break}</span></strong><span style="line-height: 1.6em;"><strong>Thank You</strong>.</span></p>\n'),
(28, 'Project Cancelled User Notification', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Project cancelled on {site_name}', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2"> {user_name}</span></strong>,{break} The {project_name} {project_page_link} is cancelled as its end date is reached and it is not successful. {break} <span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}&nbsp;<strong style="color: #333333;">System Administrator.</strong></p>\n'),
(29, 'Project Successful Admin Notification', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Project successful on {site_name}', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2"> Administrator</span></strong>,{break} The {project_name} {project_page_link} is successful as its end date is reached and it has achieved the successful goal amount. {break} <span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span></p>\n'),
(30, 'Project Successful User Notification', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Project successful on {site_name}', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2"> {user_name}</span></strong>,{break} The {project_name} {project_page_link} is successful as its end date is reached and it has achieved the successful goal amount. {break} <span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<strong style="color:#333333">System Administrator</strong>.</p>\n'),
(31, 'Wallet Withdraw Request', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'New Wallet Withdraw Request', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2"> Administrator</span></strong>,{break} you have new withdraw request by {name}.{break} The details are as below : {break} {details}{break}{break} <span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<strong style="color: #333333;">System Administrator</strong>.</p>\n'),
(32, 'project new updates', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', '(project_name) project new updates', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2"> {user_name}</span></strong> {break} New updates from your donate project {break} updates:{project_update}{break} {break} <span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}​&nbsp;<strong style="color: #333333;">System Administrator.</strong></p>\n'),
(33, 'Email Invitation', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Join me on {site_name}', '<p><strong><span style="color:#39434d">Hi</span>,</strong>{break}<strong> <span style="color:#3bb0d2">{login_user_name}</span></strong> has been using {site_name}, a place to discover, donate and post or share campaigns, and wants you to join and {invitation_link}start funding{end_invitation_link}.{break}{break} {invitation_link}Accept Invite{end_invitation_link}{break}{break} {message}{break} &nbsp;<span style="color:rgb(153, 153, 153); font-family:arial,helvetica,sans-serif; font-size:12px">Sincerely,</span>&nbsp;{break}<strong>{site_name} Team.</strong></p>\n'),
(34, 'New Message(admin)', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'New Message', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2"> Administrator</span></strong>,New message from{message_user_name} on Project {project_name}.{break}Project Link : {project_link}{break}Content : {content} {break}Profile Link : {message_user_profile_link}{break}<span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span></p>\n'),
(35, 'New Message(user)', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'New Message', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2"> {user_name}</span></strong>,{break} New message from {message_user_name} on Project {project_name}. {break} Project Link : {project_link} {break} Content : {content} {break} Profile Link : {message_user_profile_link} {break} <span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span></p>\n'),
(36, 'Message user profile(Admin)', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'New Message', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello </span> <span style="color:#3bb0d2">{user_name}</span></strong>,{break} New message from {message_user_name} {break} Content : {content} {break} Profile Link : {message_user_profile_link} {break}&nbsp;<span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span></p>\n'),
(37, 'Message user profile(User)', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'New Message', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2"> {user_name}</span></strong>,{break} New message from {message_user_name} {break} Content : {content} {break} Profile Link : {message_user_profile_link} {break} <span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<span style="font-weight: bold;color: #333333">{site_name} Team.</span></p>\n'),
(38, 'Member Invitation', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Join {project_name} you have been invited on {site_name}', '<p><strong><span style="color:rgb(57, 67, 77)">Hello</span>&nbsp;<span style="color:rgb(59, 176, 210)">{user_name}</span></strong>,</p>\n\n<p><br />\n{equity_owner_name} has invited you to join team as {user_role} on {project_name}: {company_name} and given&nbsp;<span style="font-family:lucida grande,verdana,verdana,arial,helvetica,sans-serif; font-size:12px">Access right:Admin-</span>{visible_admin}<span style="font-family:lucida grande,verdana,verdana,arial,helvetica,sans-serif; font-size:12px">&nbsp;, Profile:{access_status}&nbsp;, so you can access {project_name} as per your access levels.</p>\n\n<p>Click here to accept invitation and join team: {login_link}.</p>\n\n<p><span style="color:rgb(153, 153, 153); font-family:arial,helvetica,sans-serif; font-size:12px">Sincerely,</span></p>\n\n<p><strong>{site_name} Team.</strong></p>\n'),
(39, 'Send Message To Team', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'New Comment for {user_name}', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello</span></strong> from<strong><span style="color:#3bb0d2;font-size: 15px;">{user_name}</strong> National Donation Service{break} {email} received a new comment. {user_name} said: &ldquo;{comment}&rdquo; To reply to their comment go to:{project_page_link} Cheers, The National Donation Service Team Reminder: Your email address is kept private on National Donation Service. Only administrators of campaigns you contribute to and users that you message directly have access to your email address.</p>\n'),
(54, 'Forgot Password Request', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Forgot Password Request, {site_name}', '<p><strong style="font-size: 15px;"><span style="color:#39434d">Hello </span> <span style="color:#3bb0d2">{user_name}</span></strong>, {break}Your forgot password request has been submitted to {site_name}.{break} Click on this link to reset your Password : {login_link}. {break} <span style="color: rgb(153, 153, 153); font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 20px;">Sincerely,</span>&nbsp;{break}<span style="font-weight: bold;color: #333333;">{site_name} Team.</span></p>\n'),
(55, 'Email Verification', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Email verification request {site_name}', '<p>Hello <strong><span style="color:rgb(59, 176, 210)">{user_name}</span></strong>,</p>\n\n<p>Here is your email verification link again.</p>\n\n<p>Verify your email address by clicking {login_link} to verify your email. Once your email is verified you can also edit your account settings, connect with Facebook, and add pictures and information to share with our community. Adding images and completing your profile increases the trust &amp; transparency of your profile, making it more credible to other users.</p>\n\n<p>Sounds like an idea? It&rsquo;s easy to get going with your own {project_name}:</p>\n\n<p>&nbsp; &nbsp; - GET STARTED: {create_step_link} your {project_name} whenever you&rsquo;re ready!</p>\n\n<p>&nbsp; &nbsp; - GLOBAL: Receive {donation_name} from around the world.</p>\n\n<p>&nbsp; &nbsp; - CUSTOMER HAPPINESS: Get fast answers to your questions from real people.</p>\n\n<p>If you are interested in contributing to a {project_name} and have questions to the {project_name} owner, we encourage you to contact the {project_name} owner by leaving a private comment on the {project_name} you are interested in contributing to.</p>\n\n<p>Explore what&#39;s on&nbsp;<strong>{site_name}</strong></p>\n\n<div style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8000001907349px; line-height: normal;"><span style="color:rgb(51, 51, 51); font-family:sans-serif,arial,verdana,trebuchet ms; font-size:13px">Explore</span><span style="color:rgb(51, 51, 51); font-family:sans-serif,arial,verdana,trebuchet ms; font-size:13px">&nbsp;</span><strong>{project_name_explor}&nbsp;</strong></div>\n\n<p><span style="color:rgb(153, 153, 153); font-family:arial,helvetica,sans-serif; font-size:12px">Sincerely,</span>&nbsp;</p>\n\n<p><strong>{site_name} Team.</strong></p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n'),
(56, 'Admin User Suspended', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Admin suspended you to take login into site', '<p><strong><span style="color:rgb(57, 67, 77)">Hello</span>&nbsp;<span style="color:rgb(59, 176, 210)">{user_name}</span></strong>,</p>\n\n<p>Your account suspended from&nbsp;<strong>{site_name}</strong>.&nbsp;</p>\n\n<p>Message : {message}.{break}You are no longer able to log in in your account. Please contact {site_name} team via email here: {admin_email}&nbsp;for further information.</p>\n\n<p>&nbsp;</p>\n\n<p><span style="color:rgb(153, 153, 153); font-family:arial,helvetica,sans-serif; font-size:12px">Sincerely,</span></p>\n\n<p><strong>{site_name}.</strong></p>\n'),
(57, 'User Follower', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', '{following_user_name} started following you on {site_name}', '<p><strong><span style="color:rgb(57, 67, 77)">Hello&nbsp;</span><span style="color:rgb(59, 176, 210)">{user_name}</span></strong>,</p>\n\n<p>{following_user_name}&nbsp;has started following you on&nbsp;{site_name}.&nbsp;You might want to follow {following_user_name}&nbsp;. &nbsp;</p>\n\n<p><span style="color:rgb(153, 153, 153); font-family:arial,helvetica,sans-serif; font-size:12px">Sincerely,</span>&nbsp;</p>\n\n<p><strong>{site_name} Team.</strong></p>\n'),
(58, 'Send message', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'You have new message: {subject}', '<p><strong><span style="color:rgb(57, 67, 77)">Hello</span>&nbsp;<span style="color:rgb(59, 176, 210)">{user_name}</span></strong>,</p>\n\n<p>You have new private message from {message_user_name} on Date {dateadded}.&nbsp;</p>\n\n<p>Subject: {subject}&nbsp;</p>\n\n<p>Message: {message}</p>\n\n<p>{view_message_link}&nbsp;to read message.</p>\n\n<p><span style="color:rgb(153, 153, 153); font-family:arial,helvetica,sans-serif; font-size:12px">Sincerely,</span>&nbsp;</p>\n\n<p><strong>{site_name} Team.</strong></p>\n'),
(59, 'Update Profile', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Update Profile Request', '<p>Hello {user_name} {last_name} {break} You have successfully updated your profile.{break} Username : {user_name}{break} Lastname: {last_name}{break} Email: {email}{break} Password: {password} {break} Address: {address} {break} Zip&nbsp;code: {zip_code}{break} {break} System Administrator{break} Thank You.</p>\n'),
(60, 'Investmemt Document Uploaded', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Contract document uploaded by {investor_name}', '<p>Hello Administrator,</p>\n\n<p>{investor_name} has uploaded signed contract document to start investing on {project_name}: {company_name}. You can click here to download uploaded contract document: {click_here}.</p>\n\n<p>Click here to view history, details and approve/decline current step of {funds} process: {click_here}<br />\nClick here to do message conversation with {investor_name} in case if you have any question or concern: {message_to_owner}</p>\n\n<p>Sincerely,</p>\n\n<p><strong>{site_name} Team</strong></p>\n'),
(61, 'Investmemt Document Approved', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Contract document approved by {site_name} team', '<p>Hello {user_name},</p>\n\n<p>Contract document has been reviewed and approved by {site_name} team. With this Step.1 and Step.2 has been confirmed and completed. Please move forward to make {funds} through step.3.</p>\n\n<p>Click here to view history and details of {funds} process: {click_here}<br />\nClick here to do message conversation with {site_name} team in case if you have any question or concern: {message_to_admin}</p>\n\n<p>Sincerely,</p>\n\n<p><strong>{site_name} Team</strong></p>\n'),
(62, 'Payment Reciept Uploaded', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', '{funds} done and acknowledged by {investor_name}', '<p>Hello Administrator,</p>\n\n<p>{investor_name} has made investment and acknowledged on {project_name}: {company_name}. Please move forward to review payment receipt/acknowledgement documents uploaded by {investor_name} if any, approve and confirm current step.</p>\n\n<p>Click here to view history, details and approve/decline current step of investment process: {click_here}<br />\nClick here to do message conversation with {investor_name} in case if you have any question or concern: {message_to_owner}</p>\n\n<p>Sincerely,</p>\n\n<p><strong>{site_name} Team</strong></p>\n'),
(63, 'Payment Reciept Approved', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', '{funds} acknowledged and approved by {site_name} team', '<p>Hello {user_name}</p>\n\n<p>Payment receipt/acknowledgement documents has been reviewed and approved by {site_name} team. With this Step.3, Step.4 and Step.5 have been confirmed and completed. Please wait until {site_name} team sends you Share Certificates/Documents.</p>\n\n<p>Click here to view history and details of investment process: {click_here}<br />\nClick here to do message conversation with {site_name} team in case if you have any question or concern: {message_to_admin}</p>\n\n<p>Sincerely,</p>\n\n<p><strong>{site_name} Team</strong></p>\n'),
(64, 'Document Rejected', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Contract document declined by {site_name} team', '<p>Hello {user_name}</p>\n\n<p><br />\nContract document has been reviewed but declined by {site_name} team. Please get in touch with {site_name}&nbsp;team to get more details about and &nbsp;resolution of same to resubmit your contract document to be considered again.</p>\n\n<p>Click here to view history and details of {funds} process: {click_here}<br />\nClick here to do message conversation with {site_name} team in case if you have any question or concern: {message_to_admin}</p>\n\n<p>Sincerely,</p>\n\n<p><strong>{site_name} Team</strong></p>\n'),
(65, 'Admin Project Approved Alert', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Congrats your {project_name} approved and live on {site_name}', '<p><strong><span style="color:#39434d">Hello </span> <span style="color:#3bb0d2">{user_name}</span></strong>,</p>\r\n\r\n<p><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">Guess what!!!&nbsp;{site_name} team has </span>approved and&nbsp;published<span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px"> your </span>{project_name}<span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">. </span>You can see your live {project_name} from this link: {equity_page_link}.</p>\r\n\r\n<p><span style="color:rgb(153, 153, 153); font-family:arial,helvetica,sans-serif; font-size:12px">Sincerely,</span></p>\r\n\r\n<p><strong>{site_name} Team.</strong></p>\r\n'),
(66, 'update on your backed equity', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'New {updates_plural} on {project_name} you {funds_past}: {company_name}', '<p><strong><span style="color:rgb(57, 67, 77)">Hey&nbsp;</span>{user_name}</strong>,</p>\n\n<p><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">New {updates_plural} has been posted on {project_name} you already </span>{funds_past}<span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px"> named</span>&nbsp;{company_name} by {update_user_name}&nbsp;.</p>\n\n<p><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">Click &quot;{updates_plural}&quot; tab to read all {updates_plural} on this {project_name} link: </span>{equity_page_link}</p>\n\n<p><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">Thanks You</span></p>\n\n<p><strong>{site_name} Team.</strong></p>\n'),
(67, 'New updates on campaign you followed', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'New updates on campaign you followed: {company_name}', '<p><strong><span style="color:#39434d">Hello </span> {user_name}</strong>,</p>\n\n<p><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">New update(s) has been posted on campaign you already followed named</span> {company_name}&nbsp;<span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">by </span>{update_user_name}<span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">. </span></p>\n\n<p><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">Click &quot;Updates&quot; tab to read all updates on this campaign&nbsp; link: </span><span style="background-color:rgb(229, 229, 229); color:rgb(68, 68, 68); font-family:sans-serif; font-size:13.1199998855591px">{equity_page_link}</span></p>\n\n<p><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">Thanks You</span><span style="color:rgb(153, 153, 153); font-family:arial,helvetica,sans-serif; font-size:12px">,</span></p>\n\n<p><strong>{site_name} Team.</strong></p>\n'),
(68, 'Someone comment on your backed equity', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'New {comment} on {project_name} you invested: {company_name}', '<p><strong><span style="color:#39434d">Hey&nbsp;</span> {user_name}</strong>,</p>\n\n<p>{comment_user_name}&nbsp; has {comment_past} on {project_name} you already {funds_past} named {project_name}. Click &quot;{comments_plural}&quot; Tab to read {comment} on this {project} link: {equity_page_link}</p>\n\n<p>&nbsp;</p>\n\n<p>{comment_full_name}&#39;s&nbsp;{comment}</p>\n\n<p>-------------------------------------------------</p>\n\n<p>{comments}</p>\n\n<p>-------------------------------------------------</p>\n\n<p><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">Thanks You</span></p>\n\n<p><strong>{site_name} Team.</strong></p>\n'),
(69, 'New follower on campaign you created', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'New {follower_name} on {project_name} you created: {company_name}', '<p><strong><span style="color:#39434d">Hello </span> {user_name}</strong>,</p>\n\n<p>{follow_name} has started following {project_name} you created named {company_name}. You can view all followers on this {project_name} link: <span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">&nbsp;</span>{equity_page_link}</p>\n\n<p><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">Thanks You</span><span style="color:rgb(153, 153, 153); font-family:arial,helvetica,sans-serif; font-size:12px">,</span></p>\n\n<p><strong>{site_name} Team.</strong></p>\n'),
(70, 'Someone follow on your backed equity', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'New {follower_name} on {project_name} you {funds_past}: {company_name}', '<p><strong><span style="color:#39434d">Hey&nbsp;</span> {user_name},</strong></p>\n\n<p><strong>{follow_name}&nbsp;</strong><span style="color:rgb(34, 34, 34); font-family:arial,sans-serif; font-size:12.8000001907349px">&nbsp;</span><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">has started following {project_name} you &nbsp;{funds_past} </span><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px"> named</span><span style="color:rgb(34, 34, 34); font-family:arial,sans-serif; font-size:12.8000001907349px">&nbsp;&nbsp;</span><strong><em>{company_name}.</em></strong></p>\n\n<p>You can view all followers on this<span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">&nbsp;{project_name} link: </span><strong><em>&nbsp;</em></strong>{equity_page_link}</p>\n\n<p><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">Thanks You</span></p>\n\n<p><strong>{site_name} Team.</strong></p>\n'),
(71, 'New follower on campaign you followed', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'New {follower_name} on {project_name} you {followers_past} : {company_name}', '<p><strong><span style="color:#39434d">Hello </span> {user_name}</strong>,</p>\n\n<p>{follow_name} <span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">has started following {project_name} you &nbsp;{funds_past}</span><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">&nbsp;named</span> {company_name}.</p>\n\n<p><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">You can view all followers on this </span>{project_name}<span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px"> link: &nbsp;</span>{equity_page_link}</p>\n'),
(72, 'New comment on campaign you followed', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'New {comment} on {project_name} you {followers_past}, {company_name}', '<p><strong><span style="color:#39434d">Hello </span> {user_name}</strong>,</p>\n\n<p>{comment_user_name} has {comment_past} on {project_name} you already {followers_past} &nbsp;named {company_name}. Click &quot;{comments_plural}&quot; Tab to read {comment} on this {project_name} link: {equity_page_link}</p>\n\n<p>{comment_full_name}&#39;s&nbsp;{comment}</p>\n\n<p>-------------------------------------------------</p>\n\n<p>{comments}</p>\n\n<p>-------------------------------------------------</p>\n\n<p><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">Thanks You</span><span style="color:rgb(153, 153, 153); font-family:arial,helvetica,sans-serif; font-size:12px">,</span></p>\n\n<p><strong>{site_name} Team.</strong></p>\n'),
(73, 'Request access sent', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Access request sent:  {company_name}', '<p><strong><span style="color:#39434d">Hello </span> {user_name}</strong>,</p>\n\n<div class="gmail_default" style="color: rgb(34, 34, 34); font-family: arial, helvetica, sans-serif; font-size: 12.8000001907349px; line-height: normal;">Your request to access following sections has been sent to the {company_name} owner of this: {equity_page_link}. Please wait until your request to access is approved.</div>\n\n<div class="gmail_default" style="color: rgb(34, 34, 34); font-family: arial, helvetica, sans-serif; font-size: 12.8000001907349px; line-height: normal;">&nbsp;</div>\n\n<div class="gmail_default" style="color: rgb(34, 34, 34); font-family: arial, helvetica, sans-serif; font-size: 12.8000001907349px; line-height: normal;">- {section-name}&nbsp;section<br />\n&nbsp;</div>\n\n<p><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">Thanks You</span><span style="color:rgb(153, 153, 153); font-family:arial,helvetica,sans-serif; font-size:12px">,</span></p>\n\n<p><strong>{site_name} Team.</strong></p>\n'),
(74, 'Request access approved', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Access request approved:  {company_name}', '<p><strong><span style="color:#39434d">Hello </span> {user_name}</strong>,</p>\n\n<div class="gmail_default" style="color: rgb(34, 34, 34); font-size: 12.8000001907349px; line-height: normal; font-family: arial, helvetica, sans-serif;">Your request to access <span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">{section-name}</span> has been approved and now you can access following sections on {company_name}: {equity_page_link}.</div>\n\n<div class="gmail_default" style="color: rgb(34, 34, 34); font-size: 12.8000001907349px; line-height: normal; font-family: arial, helvetica, sans-serif;">&nbsp;</div>\n\n<div class="gmail_default" style="color: rgb(34, 34, 34); font-size: 12.8000001907349px; line-height: normal; font-family: arial, helvetica, sans-serif;">- {section-name}&nbsp;section<br />\n&nbsp;</div>\n\n<div class="gmail_default" style="color: rgb(34, 34, 34); font-size: 12.8000001907349px; line-height: normal; font-family: arial, helvetica, sans-serif;"><span style="font-size:12.8000001907349px; line-height:1.6em">Thanks You</span><span style="color:rgb(153, 153, 153); font-size:12px; line-height:1.6em">,</span></div>\n\n<p><strong>{site_name} Team.</strong></p>\n');
INSERT INTO `email_template` (`email_template_id`, `task`, `from_address`, `reply_address`, `subject`, `message`) VALUES
(75, 'Request access denied', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Access request denied:  {company_name}', '<p><strong><span style="color:#39434d">Hello </span> {user_name}</strong>,</p>\n\n<div class="gmail_default" style="color: rgb(34, 34, 34); font-size: 12.8000001907349px; line-height: normal; font-family: arial, helvetica, sans-serif;">\n<div class="gmail_default">Your request to access has been denied due to following reason(s) on {company_name}: {equity_page_link} by <span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">{company_name}</span> owner.</div>\n<br />\nReason to deny access</div>\n\n<div class="gmail_default" style="color: rgb(34, 34, 34); font-size: 12.8000001907349px; line-height: normal; font-family: arial, helvetica, sans-serif;">------------------</div>\n\n<div class="gmail_default" style="color: rgb(34, 34, 34); font-size: 12.8000001907349px; line-height: normal; font-family: arial, helvetica, sans-serif;">{reason}</div>\n\n<div class="gmail_default" style="color: rgb(34, 34, 34); font-size: 12.8000001907349px; line-height: normal; font-family: arial, helvetica, sans-serif;">------------------</div>\n\n<p><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px; line-height:1.6em">Thanks You</span><span style="color:rgb(153, 153, 153); font-family:arial,helvetica,sans-serif; font-size:12px; line-height:1.6em">,</span></p>\n\n<p><strong>{site_name} Team.</strong></p>\n'),
(76, 'Interest request sent', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', '{funds} request sent:  {company_name}', '<p><strong><span style="color:#39434d">Hello </span> {user_name}</strong>,</p>\n\n<p><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">Your interest to invest on </span>{company_name}<span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">: </span>{company_name_link}<span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px"> has been expressed to </span>{project_name}<span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px"> owner. Please wait until your request to invest on </span>{company_name}<span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px"> has been approved.</span></p>\n\n<p><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">Thanks You</span><span style="color:rgb(153, 153, 153); font-family:arial,helvetica,sans-serif; font-size:12px">,</span></p>\n\n<p><strong>{site_name} Team.</strong></p>\n'),
(77, 'Interest access approved', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', ' {funds} request approved: {company_name}', '<p><strong><span style="color:#39434d">Hello </span> {user_name}</strong>,</p>\n\n<p><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">Your ability to invest on </span>{company_name}<span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">:&nbsp;</span>{company_name}<span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px"> has been enabled by </span>{project_name}<span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px"> owner. Please move forward to invest on </span>{project_name}<span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">: &nbsp;</span>{equity_page_link}<span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">.</span></p>\n\n<p><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">Thanks You</span><span style="color:rgb(153, 153, 153); font-family:arial,helvetica,sans-serif; font-size:12px">,</span></p>\n\n<p><strong>{site_name} Team.</strong></p>\n'),
(78, 'Interest access denied', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', '{funds} request denied: {company_name}', '<p><strong><span style="color:#39434d">Hello </span> {user_name}</strong>,</p>\n\n<p><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">Your ability to invest on</span> {company_name}.</p>\n\n<p><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">Your ability to invest on </span>&nbsp;{company_name}<span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">: </span>{equity_page_link}<span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px"> has been disabled by </span>&nbsp;{project_name}<span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px"> owner due to following reason(s).</span></p>\n\n<p><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">Reason to deny access</span></p>\n\n<div class="gmail_default" style="color: rgb(34, 34, 34); font-family: arial, helvetica, sans-serif; font-size: 12.8000001907349px; line-height: normal;">------------------</div>\n\n<div class="gmail_default" style="color: rgb(34, 34, 34); font-family: arial, helvetica, sans-serif; font-size: 12.8000001907349px; line-height: normal;">{reason}</div>\n\n<p><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">------------------</span></p>\n\n<p><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">Thanks You</span><span style="color:rgb(153, 153, 153); font-family:arial,helvetica,sans-serif; font-size:12px">,</span></p>\n\n<p><strong>{site_name} Team.</strong></p>\n'),
(79, 'Request access received', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Request access received: {company_name}', '<p><strong><span style="color:#39434d">Hello </span> {user_name}</strong>,</p>\n\n<p><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">You have received request from {requestor_name}&nbsp;to access following sections on your {company_name}<span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">: {company_name_link}<span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">. Please review his profile/status and approve/deny request from here: {request_page_link}</span></p>\n\n<p><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">- {section-name}&nbsp;section</span></p>\n\n<p><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">Thanks You</span><span style="color:rgb(153, 153, 153); font-family:arial,helvetica,sans-serif; font-size:12px">,</span></p>\n\n<p><strong>{site_name} Team.</strong></p>\n'),
(80, 'Interest request received', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Interest access received: {company_name}', '<p><strong><span style="color:#39434d">Hello </span> {user_name}</strong>,</p>\n\n<p><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">{investor_name}</span><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">&nbsp;is interested in investing on your</span><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">&nbsp;</span>{project_name}<span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">: </span>{company_name_link}<span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">. Please review his profile/status and approve/deny </span><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">{investor_name}</span><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px"> as investor from here: {intrest_link}</span></p>\n\n<p><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">Thanks You</span><span style="color:rgb(153, 153, 153); font-family:arial,helvetica,sans-serif; font-size:12px">,</span></p>\n\n<p><strong>{site_name} Team.</strong></p>\n'),
(81, 'Admin Project deactivated Alert', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Oops! Your {project_name} is deactivated: {site_name}', '<p><strong><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2">{user_name}</span></strong>,</p>\n\n<p>Unfortunately {site_name} <span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">team has deactivated your</span> {project_name} due to following reasons. Please contact {site_name} team via email here: {admin-email}&nbsp;for further information.</p>\n\n<p>Reason to <span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">deactivate</span> your {project_name}<br />\n------------------------<br />\n{reason}</p>\n\n<p><span style="color:rgb(153, 153, 153); font-family:arial,helvetica,sans-serif; font-size:12px">Sincerely,</span>&nbsp;</p>\n\n<p><strong>{site_name} Team.</strong></p>\n'),
(82, 'New fund added on your backed project', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'New {funds} on {project_name} you {funds_past}: {company_name}', '<p><strong><span style="color:#39434d">Hey&nbsp;</span> {user_name},</strong></p>\n\n<p>&nbsp;<strong><em>{donor_name}&nbsp;</em></strong><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">has pledged with</span>&nbsp;<strong><em>({donate_amount})</em></strong><span style="color:rgb(34, 34, 34); font-family:arial,sans-serif; font-size:12.8000001907349px"> </span><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">on {project_name</span><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">} you already&nbsp; pledged named</span><span style="color:rgb(34, 34, 34); font-family:arial,sans-serif; font-size:12.8000001907349px">&nbsp;</span><strong><em>{project_name}</em></strong>.</p>\n\n<p><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">Click &quot;{funds_plural}&quot; Tab to view all {funds_plural} on this {project_name} link: </span>&nbsp;{project_page_link}</p>\n\n<div class="gmail_default" style="color: rgb(34, 34, 34); font-family: arial, helvetica, sans-serif; font-size: 12.8000001907349px; line-height: normal;">With this pledge {project_name} is now,</div>\n\n<div class="gmail_default" style="color: rgb(34, 34, 34); font-family: arial, helvetica, sans-serif; font-size: 12.8000001907349px; line-height: normal;">&nbsp;</div>\n\n<div class="gmail_default" style="color: rgb(34, 34, 34); font-family: arial, helvetica, sans-serif; font-size: 12.8000001907349px; line-height: normal;">Funded: {percentage}% {funds_past} so far</div>\n\n<div class="gmail_default" style="color: rgb(34, 34, 34); font-family: arial, helvetica, sans-serif; font-size: 12.8000001907349px; line-height: normal;">&nbsp;</div>\n\n<div class="gmail_default" style="color: rgb(34, 34, 34); font-family: arial, helvetica, sans-serif; font-size: 12.8000001907349px; line-height: normal;">Raised: {raised_amount}&nbsp;of Goal: {goal}</div>\n\n<div class="gmail_default" style="color: rgb(34, 34, 34); font-family: arial, helvetica, sans-serif; font-size: 12.8000001907349px; line-height: normal;">&nbsp;</div>\n\n<div class="gmail_default" style="color: rgb(34, 34, 34); font-family: arial, helvetica, sans-serif; font-size: 12.8000001907349px; line-height: normal;">Day Left: {days_left}</div>\n\n<p><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">Thanks You</span><span style="color:rgb(153, 153, 153); font-family:arial,helvetica,sans-serif; font-size:12px">,</span>&nbsp;</p>\n\n<p><strong>{site_name} Team.</strong></p>\n'),
(83, 'New Pledge on campaign you followed', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'New Pledge on {project_name} you followed, {company_name}', '<p><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">Hey</span><strong><span style="color:#39434d"> </span> {user_name}</strong>,</p>\n\n<p>&nbsp;<strong><em>{donor_name}&nbsp;</em></strong><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">has pledged with</span>&nbsp;<strong><em>({donate_amount})</em></strong><span style="color:rgb(34, 34, 34); font-family:arial,sans-serif; font-size:12.8000001907349px"> </span><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">on {project_name} you already followed named</span><span style="color:rgb(34, 34, 34); font-family:arial,sans-serif; font-size:12.8000001907349px">&nbsp;</span><strong><em>{company_name}</em></strong>.</p>\n\n<p><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">Click &quot;{funds_plural}&quot; Tab to view all {funds_plural</span><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">} on this {project_name} link: {</span>project_page_link<span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">}</span></p>\n\n<div class="gmail_default" style="color: rgb(34, 34, 34); font-family: arial, helvetica, sans-serif; font-size: 12.8000001907349px; line-height: normal;">With this pledge <span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">{project_name}</span> is now,</div>\n\n<div class="gmail_default" style="color: rgb(34, 34, 34); font-family: arial, helvetica, sans-serif; font-size: 12.8000001907349px; line-height: normal;">&nbsp;</div>\n\n<div class="gmail_default" style="color: rgb(34, 34, 34); font-family: arial, helvetica, sans-serif; font-size: 12.8000001907349px; line-height: normal;">Funded: {percentage}% <span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">{funds_past</span><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">}</span> so far</div>\n\n<div class="gmail_default" style="color: rgb(34, 34, 34); font-family: arial, helvetica, sans-serif; font-size: 12.8000001907349px; line-height: normal;">&nbsp;</div>\n\n<div class="gmail_default" style="color: rgb(34, 34, 34); font-family: arial, helvetica, sans-serif; font-size: 12.8000001907349px; line-height: normal;">Raised: {raised_amount}&nbsp;of Goal: {goal}</div>\n\n<div class="gmail_default" style="color: rgb(34, 34, 34); font-family: arial, helvetica, sans-serif; font-size: 12.8000001907349px; line-height: normal;">&nbsp;</div>\n\n<div class="gmail_default" style="color: rgb(34, 34, 34); font-family: arial, helvetica, sans-serif; font-size: 12.8000001907349px; line-height: normal;">Day Left: {days_left}</div>\n\n<p>&nbsp;</p>\n\n<p><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">Thanks You</span><span style="color:rgb(153, 153, 153); font-family:arial,helvetica,sans-serif; font-size:12px">,</span></p>\n\n<p><strong>{site_name} Team.</strong></p>\n'),
(84, 'You have reply on your comment on', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'You have reply on your {comment} on, {company_name}', '<p><strong><span style="color:#39434d">Hello </span> {user_name}</strong>,</p>\n\n<p>{comment_user_name} <span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">has replied on {comment} you posted on {project_name} named</span>&nbsp;{company_name}.</p>\n\n<p><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">Click &quot;{comment_plural}&quot; tab to read reply on your {comment} on this {project_name}&nbsp; link: </span>{equity_page_link}</p>\n\n<p>{comment_full_name}&#39;s&nbsp;Comment</p>\n\n<p>-------------------------------------------------</p>\n\n<p>{comments}</p>\n\n<p>-------------------------------------------------</p>\n\n<p><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">Thanks You</span><span style="color:rgb(153, 153, 153); font-family:arial,helvetica,sans-serif; font-size:12px">,</span></p>\n\n<p><strong>{site_name} Team.</strong></p>\n'),
(85, 'Deleted Team Member', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Your account has been removed from team members of {company_name}', '<p><strong><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2">{user_name}</span></strong>,&nbsp;</p>\n\n<p><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">You have been removed as {team_member_type} from {company_name_link} by {project_owner_link} as {project_name} owner of this {project_name}</span></p>\n\n<p>&nbsp;<span style="color:rgb(153, 153, 153); font-family:arial,helvetica,sans-serif; font-size:12px">Sincerely,</span>&nbsp;</p>\n\n<p><strong>{site_name} Team. </strong></p>\n'),
(86, 'Approve Accrediation request', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Your investor accreditation is approved', '<p><strong><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2">{user_name}</span></strong>,&nbsp;</p>\n\n<div style="color: rgb(34, 34, 34); font-family: arial, helvetica, sans-serif; font-size: 12.8000001907349px; line-height: normal;">You are now approved as accredited investor user on {site_name}&nbsp;and you can start {funds_gerund} on different {project_name_plural} and building your portfolio.</div>\n\n<p>&nbsp;<span style="color:rgb(153, 153, 153); font-family:arial,helvetica,sans-serif; font-size:12px">Sincerely,</span>&nbsp;</p>\n\n<p><strong>{site_name} Team. </strong></p>\n'),
(87, 'Reject Accrediation request', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Your investor accreditation is declined', '<p><strong><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2">{user_name}</span></strong>,&nbsp;</p>\n\n<div style="color: rgb(34, 34, 34); font-family: arial, helvetica, sans-serif; font-size: 12.8000001907349px; line-height: normal;"><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">Your investor accrediation has been reviewed but declined by {site_name} team.</span><br />\n&nbsp;\n<div style="color: rgb(34, 34, 34); font-family: arial, helvetica, sans-serif; font-size: 12.8000001907349px; line-height: normal;">Please contact {site_name} team via email here: {admin_email} for further information.</div>\n</div>\n\n<p>&nbsp;<span style="color:rgb(153, 153, 153); font-family:arial,helvetica,sans-serif; font-size:12px">Sincerely,</span>&nbsp;</p>\n\n<p><strong>{site_name} Team. </strong></p>\n'),
(88, 'Your Admin Account Created', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', ' Your Admin Account Created: {site_name}', '<p><strong><span style="color:#39434d">Hello</span> <span style="color:#3bb0d2">{user_name}</span></strong>,</p>\r\n\r\n<p><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">Your admin account has been created. As your account might have certain permissions to access different sections of back end system of {site_name}, Please contact {site_name} team via email here: {admin_email} for further information and to know more about your account privileges.</span></p>\r\n\r\n<p><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">Here are the login details for you</span><br />\r\n<span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">Login Link: {login_link}</span><br />\r\n<span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">Username: {username}&nbsp;</span><br />\r\n<span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">Password: {password}</span></p>\r\n\r\n<p><span style="color:rgb(153, 153, 153); font-family:arial,helvetica,sans-serif; font-size:12px">Sincerely,</span></p>\r\n\r\n<p><strong>{site_name} Team</strong></p>\r\n'),
(89, 'New updates on campaign you created', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'New {updates_plural} on {project_name} you created: {company_name}', '<p><strong><span style="color:#39434d">Hey</span>&nbsp;{user_name}</strong>,</p>\n\n<p>New {updates_plural} has been posted on {project_name} you created named {company_name}&nbsp;<span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">by </span>{update_user_name}<span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">. </span></p>\n\n<p>Click &quot;{updates_plural}&quot; tab to read all {updates_plural} on this {project_name} link:<span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px"> </span>{equity_page_link}</p>\n\n<p><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">Thanks You</span><span style="color:rgb(153, 153, 153); font-family:arial,helvetica,sans-serif; font-size:12px">,</span></p>\n\n<p><strong>{site_name} Team.</strong></p>\n'),
(90, 'Social User Join', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'Sign up successfully on {site_name}', '<p><strong><span style="color:#39434d">Hey</span> <span style="color:#3bb0d2">{user_name}</span></strong>,</p>\r\n\r\n<p>Greetings from {site_name} Team, We welcome you to our portal on your successful sign-up with us.</p>\r\n\r\n<p>Your Email Address is {email}. (Keep it confidential).</p>\r\n\r\n<p>To activate your account and to start enjoying our valuable services, please click on the link given below:</p>\r\n\r\n<p>{login_link}.</p>\r\n\r\n<p><span style="color:rgb(34, 34, 34); font-family:arial,helvetica,sans-serif; font-size:12.8000001907349px">Thanks You</span><span style="color:rgb(153, 153, 153); font-family:arial,helvetica,sans-serif; font-size:12px">,</span></p>\r\n\r\n<p><strong>{site_name} Team. </strong></p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `equity`
--

CREATE TABLE IF NOT EXISTS `equity` (
  `equity_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `equity_url` varchar(255) DEFAULT NULL,
  `project_name` varchar(255) DEFAULT NULL,
  `project_description` text,
  `project_street` varchar(255) DEFAULT NULL,
  `project_city` varchar(255) DEFAULT NULL,
  `project_state` varchar(255) DEFAULT NULL,
  `project_country` int(11) DEFAULT NULL,
  `project_property_size` int(11) DEFAULT NULL,
  `project_lot_size` varchar(255) DEFAULT NULL,
  `project_year_build` int(11) DEFAULT NULL,
  `investment_summary_text` text,
  `host_ip` varchar(255) DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `terms_service` tinyint(11) NOT NULL,
  `minimum_raise` int(11) DEFAULT NULL,
  `maximum_raise` int(11) DEFAULT NULL,
  `goal` int(11) DEFAULT NULL,
  `funding_type` varchar(255) DEFAULT NULL,
  `amount_get` decimal(10,2) NOT NULL,
  `equity_currency_code` varchar(255) DEFAULT NULL,
  `equity_currency_symbol` varchar(255) DEFAULT NULL,
  `end_date` datetime NOT NULL,
  `active_cnt` tinyint(1) NOT NULL DEFAULT '0',
  `company_fundraising` tinyint(11) NOT NULL,
  `amount_raise_current_round` int(11) DEFAULT NULL,
  `date_round_open` datetime NOT NULL,
  `deal_type_name` varchar(255) DEFAULT NULL,
  `available_shares` varchar(255) DEFAULT NULL,
  `price_per_share` varchar(255) DEFAULT NULL,
  `equity_available` int(11) DEFAULT NULL,
  `company_valuation` varchar(255) DEFAULT NULL,
  `interest` varchar(255) DEFAULT NULL,
  `term_length` varchar(255) DEFAULT NULL,
  `valuation_cap` varchar(255) DEFAULT NULL,
  `conversation_discount` varchar(255) DEFAULT NULL,
  `warrant_coverage` varchar(255) DEFAULT NULL,
  `debt_interest` varchar(255) DEFAULT NULL,
  `debt_term_length` varchar(255) DEFAULT NULL,
  `return` varchar(255) CHARACTER SET latin1 NOT NULL,
  `return_percentage` varchar(255) DEFAULT NULL,
  `maximum_return` varchar(255) DEFAULT NULL,
  `payment_frequency` varchar(255) DEFAULT NULL,
  `payment_start_date` varchar(255) DEFAULT NULL,
  `payment_start_date_text` varchar(255) DEFAULT NULL,
  `elevator_pitch` text,
  `cover_photo` varchar(255) DEFAULT NULL,
  `video_url` varchar(255) DEFAULT NULL,
  `video_image` varchar(255) DEFAULT NULL,
  `executive_summary_status` tinyint(11) NOT NULL,
  `executive_summary_file` varchar(255) DEFAULT NULL,
  `term_sheet_status` tinyint(11) NOT NULL,
  `term_sheet_file` varchar(255) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_account_type` varchar(255) DEFAULT NULL,
  `bank_account_number` varchar(255) DEFAULT NULL,
  `confirm_account_number` varchar(255) DEFAULT NULL,
  `routing_number` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `is_featured` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `view_counter` int(11) NOT NULL,
  `contract_copy_file` varchar(250) DEFAULT NULL,
  `reason_decline` varchar(255) DEFAULT NULL,
  `save_the_reason` int(11) NOT NULL,
  `save_the_reason_inactive` int(11) NOT NULL,
  `fund_inactive_note` int(11) NOT NULL,
  `reason_inactive_hidden` varchar(255) DEFAULT NULL,
  `deny_reason_access` varchar(255) DEFAULT NULL,
  `reason_save_aceess` int(11) NOT NULL,
  `deny_reason_interest` varchar(255) DEFAULT NULL,
  `reason_save_interest` int(11) NOT NULL,
  `allowed_investor` varchar(255) DEFAULT 'yes',
  `cash_flow_status` varchar(255) DEFAULT NULL,
  `project_timezone` varchar(255) DEFAULT NULL,
  `tax_relief_type` varchar(255) NOT NULL,
  `deal_highlight_title1` varchar(255) DEFAULT NULL,
  `deal_highlight_description1` text,
  `deal_highlight_title2` varchar(255) DEFAULT NULL,
  `deal_highlight_description2` text,
  `deal_highlight_title3` varchar(255) DEFAULT NULL,
  `deal_highlight_description3` text,
  `deal_highlight_title4` varchar(255) DEFAULT NULL,
  `deal_highlight_description4` text,
  `market_summary_image` varchar(100) DEFAULT NULL,
  `market_summary_text` text,
  `deal_highlight1` varchar(100) DEFAULT NULL,
  `deal_highlight2` varchar(100) DEFAULT NULL,
  `deal_highlight3` varchar(100) DEFAULT NULL,
  `deal_highlight4` varchar(100) DEFAULT NULL,
  `main_phase` varchar(255) DEFAULT NULL,
  `sub_phase` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `equity`
--

INSERT INTO `equity` (`equity_id`, `user_id`, `equity_url`, `project_name`, `project_description`, `project_street`, `project_city`, `project_state`, `project_country`, `project_property_size`, `project_lot_size`, `project_year_build`, `investment_summary_text`, `host_ip`, `date_added`, `terms_service`, `minimum_raise`, `maximum_raise`, `goal`, `funding_type`, `amount_get`, `equity_currency_code`, `equity_currency_symbol`, `end_date`, `active_cnt`, `company_fundraising`, `amount_raise_current_round`, `date_round_open`, `deal_type_name`, `available_shares`, `price_per_share`, `equity_available`, `company_valuation`, `interest`, `term_length`, `valuation_cap`, `conversation_discount`, `warrant_coverage`, `debt_interest`, `debt_term_length`, `return`, `return_percentage`, `maximum_return`, `payment_frequency`, `payment_start_date`, `payment_start_date_text`, `elevator_pitch`, `cover_photo`, `video_url`, `video_image`, `executive_summary_status`, `executive_summary_file`, `term_sheet_status`, `term_sheet_file`, `bank_name`, `bank_account_type`, `bank_account_number`, `confirm_account_number`, `routing_number`, `status`, `is_featured`, `company_id`, `view_counter`, `contract_copy_file`, `reason_decline`, `save_the_reason`, `save_the_reason_inactive`, `fund_inactive_note`, `reason_inactive_hidden`, `deny_reason_access`, `reason_save_aceess`, `deny_reason_interest`, `reason_save_interest`, `allowed_investor`, `cash_flow_status`, `project_timezone`, `tax_relief_type`, `deal_highlight_title1`, `deal_highlight_description1`, `deal_highlight_title2`, `deal_highlight_description2`, `deal_highlight_title3`, `deal_highlight_description3`, `deal_highlight_title4`, `deal_highlight_description4`, `market_summary_image`, `market_summary_text`, `deal_highlight1`, `deal_highlight2`, `deal_highlight3`, `deal_highlight4`, `main_phase`, `sub_phase`) VALUES
(1, 1, 'smile-buddies', 'Smile Buddies', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<br></p>', 'Shreenath Puram ,Chhani Road', 'Baroda', 'Gujarat', 2, 1000, NULL, 2002, '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<br></p>', '27.109.7.130', '2015-10-02 16:37:03', 1, 100, 500, 1000, 'Flexible', '400.00', 'USD', '$', '2015-12-31 00:00:00', 1, 0, 0, '0000-00-00 00:00:00', 'debt', '0', '', 0, '', '', '', '', '', '', '10', '12', '', '', '', '', '1970-01-01 05:30:00', '', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<br></p>', '92102-equity-1443784391.png', '', '', 0, '66795-executive-summary-1443784619.odt', 0, '32763-term-sheet-1443784607.odt', 'STRIPE TEST BANK US', 'Saving Account', '000123456789', NULL, '110000000', 2, 0, 2, 360, '74066logo.zip', NULL, 0, 0, 0, '', NULL, 0, 'not app', 1, 'no', '1', 'Asia/Calcutta', 'NONE', '', '', '', '', '', '', '', '', '77797-equity-1444638271.jpg', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<br></p>', NULL, NULL, NULL, NULL, NULL, NULL),
(3, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '27.109.7.130', '2015-10-05 18:06:58', 1, NULL, NULL, NULL, NULL, '0.00', NULL, NULL, '0000-00-00 00:00:00', 0, 0, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 14, 6, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, 0, 'yes', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '123.201.91.105', '2015-10-08 13:08:20', 1, NULL, NULL, NULL, NULL, '0.00', NULL, NULL, '0000-00-00 00:00:00', 0, 0, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 15, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, 0, 'yes', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 1, 'demo-medical-group', 'Demo Medical Group', '<p>Test Message<br></p>', '22 furlong Drive Road', 'Union', 'NJ', 1, 22, '22', 2006, NULL, '123.201.91.105', '2015-10-08 16:01:08', 1, 123, 123, 221, 'Flexible', '0.00', 'USD', '$', '2015-10-09 00:00:00', 0, 0, 0, '0000-00-00 00:00:00', 'equity', '10', '22', 9, '10000000', '', '', '', '', '', '', '', '', '', '', '', '1970-01-01 05:30:00', '', NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 16, 3, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, 0, 'no', NULL, 'Asia/Calcutta', 'NONE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '123.201.91.105', '2015-10-08 16:38:12', 1, NULL, NULL, NULL, NULL, '0.00', NULL, NULL, '0000-00-00 00:00:00', 0, 0, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 26, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, 0, 'yes', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '123.201.91.105', '2015-10-08 17:31:04', 1, NULL, NULL, NULL, NULL, '0.00', NULL, NULL, '0000-00-00 00:00:00', 0, 0, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, '90876-equity-1444744744.png', NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 44, 1, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, 0, 'yes', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '96316-equity-1444638187.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 7, 'rockers-branch', 'Rockers Branch', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and \ntypesetting industry. Lorem Ipsum has been the industry''s standard dummy\n text ever since the 1500s, when an unknown printer took a galley of \ntype and scrambled it to make a type specimen book. It has survived not \nonly five centuries, but also the leap into electronic typesetting, \nremaining essentially unchanged. It was popularised in the 1960s with \nthe release of Letraset sheets containing Lorem Ipsum passages, and more\n recently with desktop publishing software like Aldus PageMaker \nincluding versions of Lorem Ipsum.<br></p>', 'Karelibaug', 'Vadodara', 'Gujarat', 2, 1200, '120', 2006, '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and \ntypesetting industry. Lorem Ipsum has been the industry''s standard dummy\n text ever since the 1500s, when an unknown printer took a galley of \ntype and scrambled it to make a type specimen book. It has survived not \nonly five centuries, but also the leap into electronic typesetting, \nremaining essentially unchanged. It was popularised in the 1960s with \nthe release of Letraset sheets containing Lorem Ipsum passages, and more\n recently with desktop publishing software like Aldus PageMaker \nincluding versions of Lorem Ipsum.<br></p>', '123.201.58.117', '2015-10-22 15:22:29', 1, 100, 1000, 1000, 'Flexible', '0.00', 'USD', '$', '2015-10-31 00:00:00', 1, 0, 0, '0000-00-00 00:00:00', 'equity', '100', '10', 10, '100000', '', '', '', '', '', '', '', '', '', '', '', '1970-01-01 05:30:00', '', '<strong>Lorem Ipsum</strong> is simply dummy text of the printing and \ntypesetting industry. Lorem Ipsum has been the industry''s standard dummy\n text ever since the 1500s, when an unknown printer took a galley of \ntype and scrambled it to make a type specimen book. It has survived not \nonly five centuries, but also the leap into electronic typesetting, \nremaining essentially unchanged. It was popularised in the 1960s with \nthe release of Letraset sheets containing Lorem Ipsum passages, and more\n recently with desktop publishing software like Aldus PageMaker \nincluding versions of Lorem Ipsum.', '37540-equity-1445845392.jpg', '', '', 0, '85175-executive-summary-1445853586.docx', 0, '78059-term-sheet-1445853592.pdf', 'Stripe US bank', 'Saving Account', '000123456789', NULL, '110000000', 2, 0, 45, 199, '15732Sample file.docx', NULL, 0, 0, 0, NULL, NULL, 0, NULL, 0, 'no', '0', 'Asia/Calcutta', 'NONE', '', '', '', '', '', '', '', '', '23669-equity-1445845383.jpg', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and \ntypesetting industry. Lorem Ipsum has been the industry''s standard dummy\n text ever since the 1500s, when an unknown printer took a galley of \ntype and scrambled it to make a type specimen book. It has survived not \nonly five centuries, but also the leap into electronic typesetting, \nremaining essentially unchanged. It was popularised in the 1960s with \nthe release of Letraset sheets containing Lorem Ipsum passages, and more\n recently with desktop publishing software like Aldus PageMaker \nincluding versions of Lorem Ipsum.<br></p>', NULL, NULL, NULL, NULL, 'value add', '70% aquired'),
(9, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '27.109.7.130', '2015-10-29 16:06:09', 1, NULL, NULL, NULL, NULL, '0.00', NULL, NULL, '0000-00-00 00:00:00', 0, 0, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 2, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, 0, 'yes', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '27.109.7.130', '2015-11-03 14:46:56', 1, NULL, NULL, NULL, NULL, '0.00', NULL, NULL, '0000-00-00 00:00:00', 0, 0, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 46, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, 0, 'yes', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '123.201.12.31', '2015-11-18 16:49:55', 1, NULL, NULL, NULL, NULL, '0.00', NULL, NULL, '0000-00-00 00:00:00', 0, 0, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 2, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, 0, 'yes', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 1, 'test-project', 'Test project', '<p>this is test project<br></p>', 'abc', 'vadodara', 'gj', 2, 1000, '1000', 2000, '<p>this is investmetn summary<br></p>', '27.109.7.130', '2015-11-19 10:15:58', 1, 1000, 2000, 2000, 'Flexible', '0.00', 'USD', '$', '2015-12-03 00:00:00', 0, 0, 0, '0000-00-00 00:00:00', 'equity', '200', '10', 10, '1000000', '', '', '', '', '', '', '', '', '', '', '', '1970-01-01 05:30:00', '', '<p>this is test descp<br></p>', '4793-equity-1448964861.jpg', '', '', 0, '22832-executive-summary-1449043589.pdf', 0, '32745-term-sheet-1447914869.', 'TestBank', 'Saving Account', '123456789123', NULL, '123456789', 1, 0, 16, 19, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, 0, 'no', NULL, 'Asia/Calcutta', 'SEIS', '', '', '', '', '', '', '', '', '2406-equity-1448964874.jpg', '<p>this is market summary<br></p>', NULL, NULL, NULL, NULL, NULL, NULL),
(13, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '27.109.7.130', '2015-11-19 11:58:02', 1, NULL, NULL, NULL, NULL, '0.00', NULL, NULL, '0000-00-00 00:00:00', 0, 0, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 47, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, 0, 'yes', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '203.187.203.243', '2015-11-19 14:01:43', 1, NULL, NULL, NULL, NULL, '0.00', NULL, NULL, '0000-00-00 00:00:00', 0, 0, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 51, 1, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, 0, 'yes', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '27.109.7.130', '2015-11-19 14:32:45', 1, NULL, NULL, NULL, NULL, '0.00', NULL, NULL, '0000-00-00 00:00:00', 0, 0, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 48, 1, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, 0, 'yes', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, 16, 'test-project1', 'Test Project', '<p>this is test project description<br></p>', 'street name', 'city', 'gj', 2, 1000, '100', 2000, '<p>this is investment summary<br></p>', '27.109.7.130', '2015-11-19 15:21:49', 1, 500, 1000, 1000, 'Flexible', '0.00', 'USD', '$', '2015-11-30 00:00:00', 1, 0, 0, '0000-00-00 00:00:00', 'debt', '1000', '', 0, '', '', '', '', '', '', '10', '12', '', '', '', '', '1970-01-01 05:30:00', '', '<p>this is elevator pithc<br></p>', '87641-equity-1447929722.jpg', '', '', 0, '19571-executive-summary-1447930360.', 0, '76987-term-sheet-1447930398.', 'Testbank', 'Checking Account', '123456789123', NULL, '123456789', 2, 0, 50, 70, '54643test', NULL, 0, 0, 0, NULL, NULL, 0, NULL, 0, 'no', NULL, 'Asia/Calcutta', 'NONE', '', '', '', '', '', '', '', '', NULL, '<p>this is market summary<br></p>', NULL, NULL, NULL, NULL, NULL, NULL),
(20, 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '27.109.7.130', '2015-11-19 17:06:15', 1, NULL, NULL, NULL, NULL, '0.00', NULL, NULL, '0000-00-00 00:00:00', 0, 0, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 2, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, 0, 'yes', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '27.109.7.130', '2015-11-20 14:11:33', 1, NULL, NULL, NULL, NULL, '0.00', NULL, NULL, '0000-00-00 00:00:00', 0, 0, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 57, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, 0, 'yes', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, 37, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '27.109.7.130', '2015-11-20 18:40:56', 1, NULL, NULL, NULL, NULL, '0.00', NULL, NULL, '0000-00-00 00:00:00', 0, 0, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 52, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, 0, 'yes', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, 7, 'lorem-ipsum', 'Lorem ipsum', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam dapibus \r\ntincidunt orci, quis convallis sem hendrerit dapibus. Donec mattis \r\nefficitur dui vel suscipit. Morbi viverra vitae ex ac placerat. In ac \r\nultrices ipsum. Aenean sem quam, dapibus sit amet justo sit amet, \r\naliquam tristique mi. Proin vel dapibus lorem. Pellentesque tristique \r\nmollis augue a elementum. Phasellus cursus vitae massa eu sodales. \r\nNullam dui mauris, blandit sed convallis et, facilisis condimentum \r\nsapien. Quisque laoreet rhoncus cursus. Aliquam in augue ut odio aliquet\r\n convallis.<br></p>', 'Makarpura GIDC', 'Vadodara', 'Gujarat', 2, 1200, '120', 2008, '<p>Investment Summary - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam dapibus \r\ntincidunt orci, quis convallis sem hendrerit dapibus. Donec mattis \r\nefficitur dui vel suscipit. Morbi viverra vitae ex ac placerat. In ac \r\nultrices ipsum. Aenean sem quam, dapibus sit amet justo sit amet, \r\naliquam tristique mi. Proin vel dapibus lorem. Pellentesque tristique \r\nmollis augue a elementum. Phasellus cursus vitae massa eu sodales. \r\nNullam dui mauris, blandit sed convallis et, facilisis condimentum \r\nsapien. Quisque laoreet rhoncus cursus. Aliquam in augue ut odio aliquet\r\n convallis.\r\n<br></p>', '27.109.7.130', '2015-11-25 16:43:30', 1, 100, 1000, 5000, 'Flexible', '2400.00', 'USD', '$', '2016-01-31 00:00:00', 1, 0, 0, '0000-00-00 00:00:00', 'convertible_note', '5000', '', 0, '', '10', '10', '1000', '10', '100', '', '', '', '', '', '', '1970-01-01 05:30:00', '', '<p>Elevator Pitch - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam dapibus \r\ntincidunt orci, quis convallis sem hendrerit dapibus. Donec mattis \r\nefficitur dui vel suscipit. Morbi viverra vitae ex ac placerat. In ac \r\nultrices ipsum. Aenean sem quam, dapibus sit amet justo sit amet, \r\naliquam tristique mi. Proin vel dapibus lorem. Pellentesque tristique \r\nmollis augue a elementum. Phasellus cursus vitae massa eu sodales. \r\nNullam dui mauris, blandit sed convallis et, facilisis condimentum \r\nsapien. Quisque laoreet rhoncus cursus. Aliquam in augue ut odio aliquet\r\n convallis.\r\n<br></p>', '95269-equity-1448528012.jpg', 'https://www.youtube.com/watch?v=4PgOZNvxzjE', 'http://i1.ytimg.com/vi/4PgOZNvxzjE/hqdefault.jpg', 0, '38090-executive-summary-1448528276.pdf', 0, '83679-term-sheet-1448528280.pdf', 'Stripe US bank', 'Saving Account', '000123456789', NULL, '123456789', 2, 1, 54, 208, '15465-contract-copy-1450434189.pdf', NULL, 0, 0, 0, NULL, '', 1, NULL, 0, 'yes', NULL, 'Asia/Calcutta', 'NONE', '', '', '', '', '', '', '', '', '6626-equity-1448527973.jpg', '<p>Market Summary - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam dapibus \r\ntincidunt orci, quis convallis sem hendrerit dapibus. Donec mattis \r\nefficitur dui vel suscipit. Morbi viverra vitae ex ac placerat. In ac \r\nultrices ipsum. Aenean sem quam, dapibus sit amet justo sit amet, \r\naliquam tristique mi. Proin vel dapibus lorem. Pellentesque tristique \r\nmollis augue a elementum. Phasellus cursus vitae massa eu sodales. \r\nNullam dui mauris, blandit sed convallis et, facilisis condimentum \r\nsapien. Quisque laoreet rhoncus cursus. Aliquam in augue ut odio aliquet\r\n convallis.\r\n<br></p>', NULL, NULL, NULL, NULL, NULL, NULL),
(25, 43, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '27.109.7.130', '2015-12-01 18:09:37', 1, NULL, NULL, NULL, NULL, '0.00', NULL, NULL, '0000-00-00 00:00:00', 0, 0, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 55, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, 0, 'yes', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, 46, 'new-project', 'New Project', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<br></p>', 'Vadodara', 'Baroda', 'Gujarat', 2, 120, '120', 2008, '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<br></p>', '27.109.7.130', '2015-12-02 13:39:51', 1, 100, 1000, 5000, 'Flexible', '0.00', 'USD', '$', '2016-04-01 00:00:00', 1, 0, 0, '0000-00-00 00:00:00', 'equity', '500', '10', 9, '1000000', '', '', '', '', '', '', '', '', '', '', '', '1970-01-01 05:30:00', '', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<br></p>', '20903-equity-1449044229.jpg', '', '', 0, '90076-executive-summary-1449044359.pdf', 0, '90852-term-sheet-1449044363.pdf', 'Bank', 'Saving Account', '000123456789001', NULL, '123456789', 2, 0, 56, 23, '23262-contract-copy-1449044355.pdf', NULL, 0, 0, 0, NULL, NULL, 0, NULL, 0, 'no', '0', 'Asia/Calcutta', 'NONE', '', '', '', '', '', '', '', '', '22303-equity-1449044202.png', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<br></p>', NULL, NULL, NULL, NULL, 'value add', ''),
(27, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '27.109.7.130', '2015-12-02 15:08:40', 1, NULL, NULL, NULL, NULL, '0.00', NULL, NULL, '0000-00-00 00:00:00', 0, 0, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 2, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, 0, 'yes', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '27.109.7.130', '2015-12-02 16:09:25', 1, NULL, NULL, NULL, NULL, '0.00', NULL, NULL, '0000-00-00 00:00:00', 0, 0, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 54, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, 0, 'yes', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(29, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '27.109.7.130', '2015-12-02 16:09:59', 1, NULL, NULL, NULL, NULL, '0.00', NULL, NULL, '0000-00-00 00:00:00', 0, 0, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, 0, 'yes', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(30, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '27.109.7.130', '2015-12-02 16:11:39', 1, NULL, NULL, NULL, NULL, '0.00', NULL, NULL, '0000-00-00 00:00:00', 0, 0, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 2, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, 0, 'yes', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(31, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '27.109.7.130', '2015-12-02 16:14:09', 1, NULL, NULL, NULL, NULL, '0.00', NULL, NULL, '0000-00-00 00:00:00', 0, 0, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, 0, 'yes', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(32, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '123.201.64.216', '2015-12-02 17:13:20', 1, NULL, NULL, NULL, NULL, '0.00', NULL, NULL, '0000-00-00 00:00:00', 0, 0, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 16, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, 0, 'yes', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(33, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '123.201.64.216', '2015-12-02 17:35:24', 1, NULL, NULL, NULL, NULL, '0.00', NULL, NULL, '0000-00-00 00:00:00', 0, 0, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 2, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, 0, 'yes', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(34, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '27.109.7.130', '2015-12-02 18:30:04', 1, NULL, NULL, NULL, NULL, '0.00', NULL, NULL, '0000-00-00 00:00:00', 0, 0, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 45, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, 0, 'yes', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(35, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '123.201.64.216', '2015-12-02 18:42:05', 1, NULL, NULL, NULL, NULL, '0.00', NULL, NULL, '0000-00-00 00:00:00', 0, 0, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 14, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, 0, 'yes', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(36, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '27.109.7.130', '2015-12-02 18:57:40', 1, NULL, NULL, NULL, NULL, '0.00', NULL, NULL, '0000-00-00 00:00:00', 0, 0, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, 0, 'yes', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(37, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '123.201.95.154', '2015-12-15 15:03:08', 1, NULL, NULL, NULL, NULL, '0.00', NULL, NULL, '0000-00-00 00:00:00', 0, 0, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 58, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, 0, 'yes', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(38, 45, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '123.201.160.118', '2015-12-18 14:42:22', 1, NULL, NULL, NULL, NULL, '0.00', NULL, NULL, '0000-00-00 00:00:00', 0, 0, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 59, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, 0, 'yes', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(39, 45, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '203.109.65.159', '2015-12-21 11:26:12', 1, NULL, NULL, NULL, NULL, '0.00', NULL, NULL, '0000-00-00 00:00:00', 0, 0, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 60, 0, NULL, NULL, 0, 0, 0, NULL, NULL, 0, NULL, 0, 'yes', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `equity_categories`
--

CREATE TABLE IF NOT EXISTS `equity_categories` (
  `id` int(11) NOT NULL,
  `equity_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `is_parent` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0="Child" 1="Parent"',
  `created_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `equity_categories`
--

INSERT INTO `equity_categories` (`id`, `equity_id`, `category_id`, `is_parent`, `created_at`) VALUES
(1, 1, 3, 0, '0000-00-00 00:00:00'),
(2, 1, 2, 1, '2015-10-02 04:42:15'),
(3, 2, 3, 0, '0000-00-00 00:00:00'),
(4, 2, 4, 0, '0000-00-00 00:00:00'),
(9, 2, 1, 1, '2015-10-05 05:22:45'),
(10, 3, 3, 0, '0000-00-00 00:00:00'),
(11, 3, 4, 0, '0000-00-00 00:00:00'),
(12, 5, 3, 0, '0000-00-00 00:00:00'),
(16, 8, 5, 0, '0000-00-00 00:00:00'),
(18, 8, 2, 1, '2015-10-26 03:27:23'),
(19, 12, 3, 0, '0000-00-00 00:00:00'),
(21, 5, 1, 1, '2015-11-19 10:35:08'),
(24, 19, 0, 0, '0000-00-00 00:00:00'),
(25, 19, 1, 1, '2015-11-19 04:10:21'),
(26, 24, 5, 0, '0000-00-00 00:00:00'),
(27, 24, 4, 0, '0000-00-00 00:00:00'),
(28, 24, 2, 1, '2015-11-26 02:21:29'),
(29, 12, 1, 1, '2015-12-01 03:55:59'),
(30, 26, 5, 0, '0000-00-00 00:00:00'),
(31, 26, 2, 1, '2015-12-02 01:46:24'),
(32, 38, 3, 0, '0000-00-00 00:00:00'),
(33, 38, 5, 0, '0000-00-00 00:00:00'),
(34, 38, 6, 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `equity_company_industry`
--

CREATE TABLE IF NOT EXISTS `equity_company_industry` (
  `id` int(10) NOT NULL,
  `company_industry_id` int(10) NOT NULL,
  `equity_id` int(10) NOT NULL,
  `company_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `equity_company_industry`
--

INSERT INTO `equity_company_industry` (`id`, `company_industry_id`, `equity_id`, `company_id`) VALUES
(1, 2, 0, 2),
(2, 3, 0, 4),
(3, 1, 0, 5),
(4, 2, 0, 6),
(5, 3, 0, 6),
(7, 2, 0, 16),
(8, 1, 0, 16),
(9, 1, 0, 14),
(10, 1, 0, 45),
(11, 2, 9, 0),
(12, 2, 11, 0),
(13, 2, 12, 0),
(14, 1, 12, 0),
(15, 1, 16, 0),
(16, 1, 18, 0),
(17, 2, 0, 50),
(18, 2, 0, 54),
(19, 2, 0, 56),
(20, 2, 27, 0),
(21, 2, 28, 0),
(22, 2, 30, 0),
(23, 2, 32, 0),
(24, 1, 32, 0),
(25, 2, 33, 0),
(26, 1, 34, 0),
(27, 1, 35, 0),
(28, 2, 0, 59);

-- --------------------------------------------------------

--
-- Table structure for table `equity_contract_document`
--

CREATE TABLE IF NOT EXISTS `equity_contract_document` (
  `id` int(11) NOT NULL,
  `detail` text,
  `update_date` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `equity_contract_document`
--

INSERT INTO `equity_contract_document` (`id`, `detail`, `update_date`) VALUES
(1, 'test document', '2015-06-08');

-- --------------------------------------------------------

--
-- Table structure for table `equity_follower`
--

CREATE TABLE IF NOT EXISTS `equity_follower` (
  `equity_follow_id` int(11) NOT NULL,
  `equity_id` int(11) NOT NULL,
  `equity_follow_user_id` int(11) NOT NULL,
  `equity_follow_date` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=75 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `equity_follower`
--

INSERT INTO `equity_follower` (`equity_follow_id`, `equity_id`, `equity_follow_user_id`, `equity_follow_date`) VALUES
(2, 8, 16, '2015-11-19 17:24:54'),
(3, 8, 16, '2015-11-19 17:25:04'),
(66, 19, 7, '2015-11-26 17:06:36'),
(5, 8, 27, '2015-11-20 10:37:16'),
(59, 1, 27, '2015-11-20 11:30:02'),
(74, 19, 43, '2015-12-01 18:39:54'),
(72, 24, 1, '2015-12-01 18:23:12'),
(70, 24, 12, '2015-12-01 14:01:41'),
(68, 19, 1, '2015-12-01 12:57:44'),
(17, 19, 27, '2015-11-20 11:25:45'),
(58, 1, 27, '2015-11-20 11:30:02'),
(57, 1, 27, '2015-11-20 11:30:01'),
(56, 1, 27, '2015-11-20 11:30:01'),
(55, 1, 27, '2015-11-20 11:30:00'),
(54, 1, 27, '2015-11-20 11:30:00'),
(53, 1, 27, '2015-11-20 11:29:59'),
(52, 1, 27, '2015-11-20 11:29:59'),
(51, 1, 27, '2015-11-20 11:29:58'),
(50, 1, 27, '2015-11-20 11:29:58'),
(49, 1, 27, '2015-11-20 11:29:57'),
(48, 1, 27, '2015-11-20 11:29:57'),
(47, 1, 27, '2015-11-20 11:29:57'),
(46, 1, 27, '2015-11-20 11:29:56'),
(45, 1, 27, '2015-11-20 11:29:56'),
(44, 1, 27, '2015-11-20 11:29:55'),
(43, 1, 27, '2015-11-20 11:29:55'),
(42, 1, 27, '2015-11-20 11:29:54'),
(41, 1, 27, '2015-11-20 11:29:54'),
(40, 1, 27, '2015-11-20 11:29:53'),
(39, 1, 27, '2015-11-20 11:29:53'),
(60, 1, 27, '2015-11-20 11:30:03'),
(61, 1, 27, '2015-11-20 11:30:04'),
(62, 1, 27, '2015-11-20 11:30:06'),
(63, 1, 27, '2015-11-20 11:30:10'),
(69, 24, 11, '2015-12-01 13:47:16');

-- --------------------------------------------------------

--
-- Table structure for table `equity_gallery`
--

CREATE TABLE IF NOT EXISTS `equity_gallery` (
  `equity_gallery_id` int(100) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `image_name` varchar(255) DEFAULT NULL,
  `image_desc` text,
  `equity_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `equity_gallery`
--

INSERT INTO `equity_gallery` (`equity_gallery_id`, `image`, `date_added`, `image_name`, `image_desc`, `equity_id`, `status`) VALUES
(1, '32854-image-1448528580.png', '0000-00-00 00:00:00', 'Image Technology', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam dapibus tincidunt orci, quis convallis sem hendrerit dapibus. Donec mattis efficitur dui vel', 24, 0);

-- --------------------------------------------------------

--
-- Table structure for table `equity_highlights`
--

CREATE TABLE IF NOT EXISTS `equity_highlights` (
  `id` int(11) NOT NULL,
  `equity_id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `description` text COLLATE utf8_bin
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `equity_highlights`
--

INSERT INTO `equity_highlights` (`id`, `equity_id`, `title`, `description`) VALUES
(13, 2, 'Mail chimp free marketing campaign 2222', 'Mail chimp free marketing campaign22  Mail chimp free marketing campaignMail chimp free marketing campaignMail chimp free marketing campaignMail chimp free marketing campaignMail chimp free marketing campaignMail chimp free marketing campaign'),
(14, 2, 'Mail chimp free marketing campaign', 'Mail chimp free marketing campaignMail chimp free marketing campaignMail chimp free marketing campaignMail chimp free marketing campaignMail chimp free marketing campaignMail chimp free marketing campaignMail chimp free marketing campaign'),
(15, 3, 'Terms 1', 'Terms 1 Terms 1'),
(16, 1, 'Strong Location', 'The Midtown area is one of the strongest submarkets of Atlanta. There is low new condo supply and high professional job growth.'),
(17, 1, 'Favorable Basis', 'The sponsor is purchasing the land for $90/SF, and the Fundrise senior debt investment will feature a basis of $65/SF.'),
(18, 5, 'test', 'test'),
(19, 24, 'Deal Highlights', 'Deal Highlights - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam dapibus tincidunt orci, quis convallis sem hendrerit dapibus. Donec mattis'),
(20, 26, 'Deal 1', 'deal'),
(21, 26, 'Deal 2', 'deal');

-- --------------------------------------------------------

--
-- Table structure for table `equity_investment_process`
--

CREATE TABLE IF NOT EXISTS `equity_investment_process` (
  `id` int(11) NOT NULL,
  `equity_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `invest_status_id` int(11) NOT NULL,
  `document_name` varchar(255) DEFAULT NULL,
  `acknowledge_doc` varchar(255) DEFAULT NULL,
  `acknowledge_note` text,
  `shipment` text,
  `created_date` date NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `bank_detail` text,
  `transaction_id` varchar(200) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `equity_investment_process`
--

INSERT INTO `equity_investment_process` (`id`, `equity_id`, `user_id`, `invest_status_id`, `document_name`, `acknowledge_doc`, `acknowledge_note`, `shipment`, `created_date`, `status`, `bank_detail`, `transaction_id`) VALUES
(1, 1, 2, 10, 'Seat Designer Functionality.pdf', '66383-investor-ack-1444023825.pdf', 'Payment receipt snap', 'Step-6 note from admin', '2015-10-05', 1, 'Updated by admin:Bank Name: Bank Of Demo ,<br>\nAcc: 8989898989899 , \n<br>\nName: Demo Name , \n<br>\nRouting# 45897\n<br>\n OR Use following details to write us Check\n<br>			 \nName: Demo Name\n<br>\nAddress: 123, King St, Burlington, MA, USA\n<br>', 'INV3S7wTKBWtRHD'),
(2, 1, 3, 10, 'Stripe account creation guide.pdf', '38078-investor-ack-1444024326.pdf', 'Step-4 note', 'Step-6 note from admin', '2015-10-05', 1, 'Updated by admin:Bank Name: Bank Of Demo ,<br>\nAcc: 8989898989899 , \n<br>\nName: Demo Name , \n<br>\nRouting# 45897\n<br>\n OR Use following details to write us Check\n<br>			 \nName: Demo Name\n<br>\nAddress: 123, King St, Burlington, MA, USA\n<br>', 'INV55mYNXKZUja7'),
(3, 1, 8, 7, '74066logo.zip', '82412-investor-ack-1445867343.zip', 'dadadad', NULL, '2015-10-26', 0, 'Updated by admin:Bank Name: Bank Of Demo ,<br>\nAcc: 8989898989899 , \n<br>\nName: Demo Name , \n<br>\nRouting# 45897\n<br>\n OR Use following details to write us Check\n<br>			 \nName: Demo Name\n<br>\nAddress: 123, King St, Burlington, MA, USA\n<br>', 'INVt8mMihWAHPqM'),
(4, 8, 11, 10, 'Sample file.pdf', '50318-investor-ack-1445939111.pdf', NULL, 'You can received the certificate from the mail', '2015-10-27', 1, 'Updated by admin:Bank Name: Bank Of Demo ,<br>\nAcc: 8989898989899 , \n<br>\nName: Demo Name , \n<br>\nRouting# 45897\n<br>\n OR Use following details to write us Check\n<br>			 \nName: Demo Name\n<br>\nAddress: 123, King St, Burlington, MA, USA\n<br>', 'INVBYRXzaOaHQ59'),
(5, 8, 12, 10, 'Sample file.pdf', '56003-investor-ack-1445945405.pdf', NULL, 'abcdefg', '2015-10-27', 1, 'Updated by admin:Bank Name: Bank Of Demo ,<br>\nAcc: 8989898989899 , \n<br>\nName: Demo Name , \n<br>\nRouting# 45897\n<br>\n OR Use following details to write us Check\n<br>			 \nName: Demo Name\n<br>\nAddress: 123, King St, Burlington, MA, USA\n<br>', 'INVTn7NBeQh5ICD'),
(6, 1, 16, 10, '67630-investor-1448009352.zip', NULL, 'tets', 'test test', '2015-11-20', 1, 'Updated by admin:Bank Name: Bank Of Demo ,<br>\r\nAcc: 8989898989899 , \r\n<br>\r\nName: Demo Name , \r\n<br>\r\nRouting# 45897\r\n<br>\r\n OR Use following details to write us Check\r\n<br>			 \r\nName: Demo Name\r\n<br>\r\nAddress: 123, King St, Burlington, MA, USA\r\n<br>', 'INVAUNBb82kb6s7'),
(7, 1, 11, 4, '29207-investor-1448010943.zip', NULL, NULL, NULL, '2015-11-20', 0, 'Updated by admin:Bank Name: Bank Of Demo ,<br>\r\nAcc: 8989898989899 , \r\n<br>\r\nName: Demo Name , \r\n<br>\r\nRouting# 45897\r\n<br>\r\n OR Use following details to write us Check\r\n<br>			 \r\nName: Demo Name\r\n<br>\r\nAddress: 123, King St, Burlington, MA, USA\r\n<br>\r\n', 'INVjwuyzmUZzuq0'),
(8, 1, 16, 10, '2089-investor-1448020245.zip', NULL, 'ack', 'test', '2015-11-20', 1, 'Updated by admin:Bank Name: Bank Of Demo ,<br>\r\nAcc: 8989898989899 , \r\n<br>\r\nName: Demo Name , \r\n<br>\r\nRouting# 45897\r\n<br>\r\n OR Use following details to write us Check\r\n<br>			 \r\nName: Demo Name\r\n<br>\r\nAddress: 123, King St, Burlington, MA, USA\r\n<br>', 'INVk4Llcq8H3DmC'),
(9, 19, 37, 10, '35167-investor-1448022838.zip', NULL, 'test', 'test', '2015-11-20', 1, 'Updated by admin:Bank Name: Bank Of Demo ,<br>\r\nAcc: 8989898989899 , \r\n<br>\r\nName: Demo Name , \r\n<br>\r\nRouting# 45897\r\n<br>\r\n OR Use following details to write us Check\r\n<br>			 \r\nName: Demo Name\r\n<br>\r\nAddress: 123, King St, Burlington, MA, USA\r\n<br>', 'INVO7fnPEw2zJRW'),
(10, 19, 37, 10, '60779-investor-1448024015.zip', NULL, 'ack', 'test', '2015-11-20', 1, 'Updated by admin:Bank Name: Bank Of Demo ,<br>\r\nAcc: 8989898989899 , \r\n<br>\r\nName: Demo Name , \r\n<br>\r\nRouting# 45897\r\n<br>\r\n OR Use following details to write us Check\r\n<br>			 \r\nName: Demo Name\r\n<br>\r\nAddress: 123, King St, Burlington, MA, USA\r\n<br>', 'INVEtlP0MaeWYar'),
(11, 24, 11, 10, '98940-investor-1448871154.pdf', '12989-investor-ack-1448871493.pdf', ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam dapibus tincidunt orci, quis convallis sem hendrerit dapibus. Donec mattis efficitur dui vel suscipit. Morbi viverra vitae ex ac placerat. In ac ultrices ipsum. Aenean sem quam, dapibus sit amet justo sit amet, aliquam tristique mi.', ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam dapibus tincidunt orci, quis convallis sem hendrerit dapibus. Donec mattis efficitur dui vel suscipit. Morbi viverra vitae ex ac placerat. In ac ultrices ipsum. Aenean sem quam, dapibus sit amet justo sit amet, aliquam tristique mi.', '2015-11-30', 1, 'Updated by admin:Bank Name: Bank Of Demo ,<br>\r\nAcc: 8989898989899 , \r\n<br>\r\nName: Demo Name , \r\n<br>\r\nRouting# 45897\r\n<br>\r\n OR Use following details to write us Check\r\n<br>			 \r\nName: Demo Name\r\n<br>\r\nAddress: 123, King St, Burlington, MA, USA\r\n<br>\r\n', 'INVQ2bK2cRcOtOF'),
(12, 24, 12, 10, '67916-investor-1448874328.pdf', '1498-investor-ack-1448874540.pdf', 'Your payment acknowledgement information has been submitted successfully. Real Estate team will review and confirm your payment shortly.', 'Your payment acknowledgement information has been submitted successfully. Real Estate team will review and confirm your payment shortly.', '2015-11-30', 1, 'Updated by admin:Bank Name: Bank Of Demo ,<br>\r\nAcc: 8989898989899 , \r\n<br>\r\nName: Demo Name , \r\n<br>\r\nRouting# 45897\r\n<br>\r\n OR Use following details to write us Check\r\n<br>			 \r\nName: Demo Name\r\n<br>\r\nAddress: 123, King St, Burlington, MA, USA\r\n<br>\r\n', 'INV8Bdco0a7edgY'),
(13, 24, 11, 10, '36797-investor-1448947738.pdf', NULL, 'herrypatel365@gmail.com', 'herrrypatel365@gmail.com', '2015-12-01', 1, 'Updated by admin:Bank Name: Bank Of Demo ,<br>\r\nAcc: 8989898989899 , \r\n<br>\r\nName: Demo Name , \r\n<br>\r\nRouting# 45897\r\n<br>\r\n OR Use following details to write us Check\r\n<br>			 \r\nName: Demo Name\r\n<br>\r\nAddress: 123, King St, Burlington, MA, USA\r\n<br>', 'INVqFipS00KnHta'),
(14, 24, 45, 10, '5783-investor-1449037623.pdf', NULL, 'Sample Test', 'Text\r\n', '2015-12-02', 1, 'Updated by admin:Bank Name: Bank Of Demo ,<br>\r\nAcc: 8989898989899 , \r\n<br>\r\nName: Demo Name , \r\n<br>\r\nRouting# 45897\r\n<br>\r\n OR Use following details to write us Check\r\n<br>			 \r\nName: Demo Name\r\n<br>\r\nAddress: 123, King St, Burlington, MA, USA\r\n<br>', 'INV8SRkYfPwMImW'),
(15, 1, 43, 10, '58446-investor-1449038643.pdf', '89815-investor-ack-1449039111.jpg', 'test donation witj acknoledge', 'please check shared document', '2015-12-02', 1, 'Updated by admin:Bank Name: Bank Of Demo ,<br>\r\nAcc: 8989898989899 , \r\n<br>\r\nName: Demo Name , \r\n<br>\r\nRouting# 45897\r\n<br>\r\n OR Use following details to write us Check\r\n<br>			 \r\nName: Demo Name\r\n<br>\r\nAddress: 123, King St, Burlington, MA, USA\r\n<br>', 'INVTtx9T1J8DSOJ'),
(16, 24, 46, 4, '80503-investor-1449043055.pdf', NULL, NULL, NULL, '2015-12-02', 0, 'Updated by admin:Bank Name: Bank Of Demo ,<br>\r\nAcc: 8989898989899 , \r\n<br>\r\nName: Demo Name , \r\n<br>\r\nRouting# 45897\r\n<br>\r\n OR Use following details to write us Check\r\n<br>			 \r\nName: Demo Name\r\n<br>\r\nAddress: 123, King St, Burlington, MA, USA\r\n<br>', 'INVUqXKuZgkhpCG'),
(17, 24, 45, 10, '3318-investor-1450430769.pdf', NULL, 'sdasdfasdfafdfasff', 'asdfasdfsf', '2015-12-18', 1, 'Updated by admin:Bank Name: Bank Of Demo ,<br>\r\nAcc: 8989898989899 , \r\n<br>\r\nName: Demo Name , \r\n<br>\r\nRouting# 45897\r\n<br>\r\n OR Use following details to write us Check\r\n<br>			 \r\nName: Demo Name\r\n<br>\r\nAddress: 123, King St, Burlington, MA, USA\r\n<br>', 'INVi6TSRFtLaSEb'),
(18, 24, 45, 10, '25684-investor-1450430842.pdf', NULL, NULL, NULL, '2015-12-18', 1, NULL, 'INV3ZBiYf7gYRxI'),
(19, 24, 45, 10, '88591-investor-1450679562.pdf', '74064-investor-ack-1450679957.pdf', 'asdfasfdasf', 'sdfasdfasdff', '2015-12-21', 1, 'Updated by admin:Bank Name: Bank Of Demo ,<br>\r\nAcc: 8989898989899 , \r\n<br>\r\nName: Demo Name , \r\n<br>\r\nRouting# 45897\r\n<br>\r\n OR Use following details to write us Check\r\n<br>			 \r\nName: Demo Name\r\n<br>\r\nAddress: 123, King St, Burlington, MA, USA\r\n<br>', 'INVBjwf91KUn04L'),
(20, 24, 45, 6, '23388-investor-1450685748.pdf', '87687-investor-ack-1450686062.pdf', NULL, NULL, '2015-12-21', 0, 'Updated by admin:Bank Name: Bank Of Demo ,<br>\r\nAcc: 8989898989899 , \r\n<br>\r\nName: Demo Name , \r\n<br>\r\nRouting# 45897\r\n<br>\r\n OR Use following details to write us Check\r\n<br>			 \r\nName: Demo Name\r\n<br>\r\nAddress: 123, King St, Burlington, MA, USA\r\n<br>', 'INVaUS9Mft4Bnem');

-- --------------------------------------------------------

--
-- Table structure for table `equity_investment_process_history`
--

CREATE TABLE IF NOT EXISTS `equity_investment_process_history` (
  `id` int(11) NOT NULL,
  `document_name` varchar(255) DEFAULT NULL,
  `acknowledge_doc` varchar(255) DEFAULT NULL,
  `investment_process_id` int(11) NOT NULL,
  `invest_status_id` int(11) NOT NULL,
  `update_date` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `equity_investment_process_history`
--

INSERT INTO `equity_investment_process_history` (`id`, `document_name`, `acknowledge_doc`, `investment_process_id`, `invest_status_id`, `update_date`) VALUES
(1, 'Seat Designer Functionality.pdf', NULL, 1, 3, '2015-10-05'),
(2, NULL, '66383-investor-ack-1444023825.pdf', 1, 6, '2015-10-05'),
(3, 'Stripe account creation guide.pdf', NULL, 2, 3, '2015-10-05'),
(4, NULL, '38078-investor-ack-1444024326.pdf', 2, 6, '2015-10-05'),
(5, '74066logo.zip', NULL, 3, 3, '2015-10-26'),
(6, NULL, '82412-investor-ack-1445867343.zip', 3, 6, '2015-10-26'),
(7, 'Sample file.pdf', NULL, 4, 3, '2015-10-27'),
(8, NULL, '50318-investor-ack-1445939111.pdf', 4, 6, '2015-10-27'),
(9, 'Sample file.pdf', NULL, 5, 3, '2015-10-27'),
(10, NULL, '56003-investor-ack-1445945405.pdf', 5, 6, '2015-10-27'),
(11, '67630-investor-1448009352.zip', NULL, 6, 3, '2015-11-20'),
(12, '29207-investor-1448010943.zip', NULL, 7, 3, '2015-11-20'),
(13, '2089-investor-1448020245.zip', NULL, 8, 3, '2015-11-20'),
(14, '35167-investor-1448022838.zip', NULL, 9, 3, '2015-11-20'),
(15, '60779-investor-1448024015.zip', NULL, 10, 3, '2015-11-20'),
(16, '98940-investor-1448871154.pdf', NULL, 11, 3, '2015-11-30'),
(17, NULL, '1069-investor-ack-1448871331.pdf', 11, 6, '2015-11-30'),
(18, NULL, '12989-investor-ack-1448871493.pdf', 11, 6, '2015-11-30'),
(19, '67916-investor-1448874328.pdf', NULL, 12, 3, '2015-11-30'),
(20, NULL, '1498-investor-ack-1448874540.pdf', 12, 6, '2015-11-30'),
(21, '36797-investor-1448947738.pdf', NULL, 13, 3, '2015-12-01'),
(22, '5783-investor-1449037623.pdf', NULL, 14, 3, '2015-12-02'),
(23, '19502-investor-1449037939.pdf', NULL, 15, 3, '2015-12-02'),
(24, '21163-investor-1449038008.pdf', NULL, 15, 3, '2015-12-02'),
(25, '58446-investor-1449038643.pdf', NULL, 15, 3, '2015-12-02'),
(26, NULL, '89815-investor-ack-1449039111.jpg', 15, 6, '2015-12-02'),
(27, '80503-investor-1449043055.pdf', NULL, 16, 3, '2015-12-02'),
(28, '3318-investor-1450430769.pdf', NULL, 17, 3, '2015-12-18'),
(29, '25684-investor-1450430842.pdf', NULL, 18, 3, '2015-12-18'),
(30, '88591-investor-1450679562.pdf', NULL, 19, 3, '2015-12-21'),
(31, NULL, '74064-investor-ack-1450679957.pdf', 19, 6, '2015-12-21'),
(32, '87080-investor-1450685487.pdf', NULL, 20, 3, '2015-12-21'),
(33, '23388-investor-1450685748.pdf', NULL, 20, 3, '2015-12-21'),
(34, NULL, '87687-investor-ack-1450686062.pdf', 20, 6, '2015-12-21');

-- --------------------------------------------------------

--
-- Table structure for table `equity_invest_status`
--

CREATE TABLE IF NOT EXISTS `equity_invest_status` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `equity_invest_status`
--

INSERT INTO `equity_invest_status` (`id`, `name`) VALUES
(1, 'Processing Contract'),
(2, 'Downloaded Contract'),
(3, 'Uploaded Signed Contract'),
(4, 'Signed Contract Approve'),
(5, 'Payment Process'),
(6, 'Payment Reciept Uploaded'),
(7, 'Payment Confirmed'),
(8, 'Tracking Shipment'),
(9, 'Document Rejected'),
(10, 'Tracking Shipment Confirmed'),
(11, 'Signed Contract Rejected');

-- --------------------------------------------------------

--
-- Table structure for table `facebook_setting`
--

CREATE TABLE IF NOT EXISTS `facebook_setting` (
  `facebook_setting_id` int(11) NOT NULL,
  `facebook_application_id` varchar(255) DEFAULT NULL,
  `facebook_login_enable` varchar(255) DEFAULT NULL,
  `facebook_access_token` varchar(500) DEFAULT NULL,
  `facebook_api_key` varchar(255) DEFAULT NULL,
  `facebook_user_id` varchar(255) DEFAULT NULL,
  `facebook_secret_key` varchar(255) DEFAULT NULL,
  `facebook_user_autopost` varchar(255) DEFAULT NULL,
  `facebook_wall_post` varchar(255) DEFAULT NULL,
  `facebook_url` text,
  `fb_img` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `facebook_setting`
--

INSERT INTO `facebook_setting` (`facebook_setting_id`, `facebook_application_id`, `facebook_login_enable`, `facebook_access_token`, `facebook_api_key`, `facebook_user_id`, `facebook_secret_key`, `facebook_user_autopost`, `facebook_wall_post`, `facebook_url`, `fb_img`) VALUES
(1, '141131532914338', '1', 'BAAFcRSVO2ZAkBAGVZCxSxYVCZCOwhKOltilZCmlfZAwvagGGPN993WhIRBChlYGFUTwZBGSiyWfQnAveDRZBwp7y0iis9pxeKuembPqS9PQALZAqrcaYbu58hVYeGZB94kkkM3bvJPkjhZAbh5zXQ7IQMZAJjEKU8htTGEuyA12cyZApqKEoE3Q4sIJEUuaXX2XNjZCfKS9JJQ8dRYGOulpe6HUHWxSEoKfBWk7wZD', '141131532914338', '100004037024764', '37be16689c59bc0645257ad846d9078c', NULL, NULL, 'https://www.facebook.com/Fundraisingscript', '100004037024764.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE IF NOT EXISTS `faq` (
  `faq_id` int(11) NOT NULL,
  `faq_category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `question` varchar(255) DEFAULT NULL,
  `answer` text,
  `faq_order` int(10) NOT NULL,
  `faq_home` int(10) NOT NULL DEFAULT '0',
  `active` varchar(20) DEFAULT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `faq`
--

INSERT INTO `faq` (`faq_id`, `faq_category_id`, `language_id`, `question`, `answer`, `faq_order`, `faq_home`, `active`, `date_added`) VALUES
(27, 12, 1, 'Testing purpose', '<p>forMost contributions are non-tax-deductible. However, some campaigns may choose to offer tax deductions on donations only if they are set up as a non-profit and only receive funds into their not-profit-profit PayPal account.</p>\n', 1, 0, '1', '2014-07-02 06:04:21'),
(26, 12, 1, 'How much does Indiegogoclone cost?', '<p>Joining Indiegogoclone is free however, there is a standard platform fee levied on any funds raised through the platform, which is 3.5% of funds raised if you meet your goal or 6.5% if you do not meet your goal.</p>\n', 2, 0, '1', '2014-07-02 06:04:50'),
(25, 12, 1, 'Do you recommend connecting with Facebook or Twitter?', '<p>Yes, we do! It&#39;s a simple way for anyone to log in to Indiegogoclone.</p>\n\n<p>&nbsp;</p>\n\n<p>If you&#39;re a campaigner, it&#39;s a great way to let backers know a little bit more about you. It&#39;s an easy way to show backers you&#39;re a real person. You may also edit the privacy settings of your Facebook or Twitter account to control what your guests see.</p>\n', 3, 0, '1', '2014-07-02 06:05:32'),
(24, 12, 1, 'Does Indiegogoclone ascertain a campaign or owner’s claim?', '<p>Indiegogoclone does not ascertain a campaign&rsquo;s or owner&rsquo;s claims. All claims and responsibilities of each project are its owner&rsquo;s. People decide a project&rsquo;s legitimacy or worthiness and decide whether they want to make a donation or not</p>\n', 5, 0, '1', '2014-07-02 06:07:54'),
(29, 12, 1, 'What is indiegogo clone?', '<p>Crowd funding site</p>\n', 3, 0, '1', '2014-07-02 06:06:33'),
(23, 12, 1, 'What is Groupfund?', '<p>Groupfund&nbsp;is a fundraising service platform that provides people who want to raise money an avenue to create, launch and broadcast their campaigns. tubestart is also used by people to find inspiring campaigns from across the world that they can align with.</p>\n', 4, 0, '1', '2014-07-02 06:07:26'),
(30, 12, 1, 'How FAQ works', '\n                                                <p>Here is the way FAQ WORKS</p>\n\n<p> </p>\n\n<p>The Brabham BT19 is a Formula One racing car designed by Ron Tauranac for the British Brabham team. The BT19 competed in the 1966 and 1967 Formula One World Championships and was used by Australian driver Jack Brabham to win his third World Championship in 1966. The BT19, which Brabham referred to as his "Old Nail", was the first car bearing its driver''s name to win a World Championship race. The car was initially conceived in 1965 for a 1.5-litre (92-cubic inch) Coventry Climax engine, but never raced in this form. For the 1966 season the Fédération Internationale de l''Automobile doubled the limit on engine capacity to 3 litres (183 cu in). Australian company Repco developed a new V8 engine for Brabham''s use in 1966, but a disagreement between Brabham and Tauranac over the latter''s role in the racing team left no time to develop a new car to handle it. Instead, the existing BT19 chassis was modified for the job. Only one BT19 was built. It was bought by Repco in 2004 and put on display in the National Sports Museum in Melbourne, Australia, in 2008. It is often demonstrated at motorsport events. (Full article...)</p>\n\n<p> </p>\n\n                                       ', 4, 0, '1', '2014-07-02 06:07:40'),
(34, 12, 2, 'Combien ne Indiegogoclone coût?', '<p>Rejoindre Indiegogoclone est libre cependant, il ya une taxe de plate-forme standard per&ccedil;u sur les fonds recueillis gr&acirc;ce &agrave; la plate-forme, qui est de 3,5% des fonds lev&eacute;s si vous atteindre votre objectif ou 6,5% si vous ne r&eacute;pondez pas &agrave; votre objectif.</p>\n', 2, 0, '1', '2014-07-02 06:14:39'),
(35, 12, 2, 'Recommandez-vous connectant avec Facebook ou Twitter?', '<p>Oui, nous le faisons! C&#39;est une fa&ccedil;on simple pour quiconque de se connecter &agrave; Indiegogoclone.&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>Si vous &ecirc;tes un militant, c&#39;est une excellente fa&ccedil;on de laisser les bailleurs savent un peu plus sur vous. C&#39;est un moyen facile de montrer les bailleurs que vous &ecirc;tes une personne r&eacute;elle. Vous pouvez &eacute;galement modifier les param&egrave;tres de votre compte Facebook ou Twitter confidentialit&eacute; de contr&ocirc;ler ce que vos clients voient.</p>\n', 3, 0, '1', '2014-07-02 06:16:18'),
(36, 12, 2, 'Quelle est INDIEGOGO clone?', '<p>Foule place de financement</p>\n', 3, 0, '1', '2014-07-02 06:17:50'),
(38, 12, 2, 'Quelle est Groupfund?', '<p>Groupfund est une plate-forme de services de collecte de fonds qui offre aux personnes qui veulent amasser des fonds pour cr&eacute;er une voie, le lancement et la diffusion de leurs campagnes. tubestart est &eacute;galement utilis&eacute; par les gens &agrave; trouver des campagnes inspirantes de partout dans le monde qu&#39;ils peuvent s&#39;aligner.</p>\n', 4, 0, '1', '2014-07-02 06:21:38'),
(39, 12, 2, 'Comment FAQ œuvres', '<p>Voici la fa&ccedil;on FAQ TRAVAUX&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>La Brabham BT19 est une voiture de Formule Un de course con&ccedil;u par Ron Tauranac pour l&#39;&eacute;quipe britannique Brabham. Le BT19 particip&eacute; &agrave; la 1966 et 1967 championnat de Formule One World et a &eacute;t&eacute; utilis&eacute; par le pilote Australien Jack Brabham pour gagner son troisi&egrave;me championnat du monde en 1966. L&#39;BT19, qui Brabham appel&eacute; son &quot;vieux clou&quot;, a &eacute;t&eacute; la premi&egrave;re voiture portant son conducteur nommer &agrave; remporter une course du Championnat du Monde. La voiture a &eacute;t&eacute; initialement con&ccedil;u en 1965 pour un 1,5 litre (92 pouces-cube) moteur Coventry Climax, mais n&#39;a jamais couru sous cette forme. Pour la saison 1966, la F&eacute;d&eacute;ration Internationale de l&#39;Automobile a doubl&eacute; la limite de la capacit&eacute; du moteur de 3 litres (183 cu in). Soci&eacute;t&eacute; australienne Repco a d&eacute;velopp&eacute; un nouveau moteur V8 pour l&#39;utilisation de Brabham en 1966, mais un d&eacute;saccord entre Brabham et Tauranac sur le r&ocirc;le de l&#39;&eacute;quipe de course de celui-ci laissa pas le temps de d&eacute;velopper une nouvelle voiture pour y faire face. Au lieu de cela, le ch&acirc;ssis BT19 existant a &eacute;t&eacute; modifi&eacute; pour le travail. Un seul BT19 a &eacute;t&eacute; construit. Il a &eacute;t&eacute; achet&eacute; par Repco en 2004 et mis en exposition au Mus&eacute;e National du Sport &agrave; Melbourne, en Australie, en 2008. Elle est souvent d&eacute;montr&eacute; lors d&#39;&eacute;v&eacute;nements de sport automobile. (Article complet ...)</p>\n', 4, 0, '1', '2014-07-02 06:56:48'),
(40, 12, 2, 'Ne Indiegogoclone vérifier une campagne ou la demande du propriétaire?', '<p>Indiegogoclone does not ascertain a campaign&rsquo;s or owner&rsquo;s claims. All claims and responsibilities of each project are its owner&rsquo;s. People decide a project&rsquo;s legitimacy or worthiness and decide whether they want to make a donation or not</p>\n', 5, 0, '1', '2014-07-02 06:58:22');

-- --------------------------------------------------------

--
-- Table structure for table `faq_category`
--

CREATE TABLE IF NOT EXISTS `faq_category` (
  `faq_category_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `faq_category_name` varchar(255) DEFAULT NULL,
  `faq_category_url_name` varchar(255) DEFAULT NULL,
  `faq_category_order` int(10) NOT NULL,
  `faq_category_home` int(10) NOT NULL DEFAULT '0',
  `active` varchar(255) DEFAULT NULL,
  `language_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `faq_category`
--

INSERT INTO `faq_category` (`faq_category_id`, `parent_id`, `faq_category_name`, `faq_category_url_name`, `faq_category_order`, `faq_category_home`, `active`, `language_id`) VALUES
(3, 0, 'FundraisingScript Basics', 'fundraisingscript-basics', 1, 1, '1', 0),
(4, 0, 'Creating a Project', 'creating-a-project', 2, 1, '1', 0),
(5, 0, 'Backing a Project', 'backing-a-project', 3, 1, '1', 0),
(6, 3, 'HOW IT WORKS', 'how-it-works', 1, 0, '1', 0),
(7, 3, 'ACCOUNT SETTINGS', 'account-settings', 2, 0, '1', 0),
(8, 3, 'SITE BASICS', 'site-basics', 3, 0, '1', 0),
(9, 0, 'tst', 'tst', 0, 1, '1', 0),
(10, 0, 'tst', 'tst1', 0, 0, '1', 0),
(11, 10, 'indiegogoclone_faq', 'indiegogoclone_faq', 56, 0, '1', 0),
(12, 10, 'Indiegogoclone_faq_question', 'Indiegogoclone_faq_question', 3, 0, '1', 0),
(13, 12, 'testing purpose faq category111', 'testing-purpose-faq-category111', 1, 0, '1', 0);

-- --------------------------------------------------------

--
-- Table structure for table `featured_sponsors`
--

CREATE TABLE IF NOT EXISTS `featured_sponsors` (
  `featured_sponsors_id` int(11) NOT NULL,
  `featured_sponsors_title` varchar(255) DEFAULT NULL,
  `featured_sponsors_image` varchar(255) DEFAULT NULL,
  `active` varchar(255) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `featured_sponsors`
--

INSERT INTO `featured_sponsors` (`featured_sponsors_id`, `featured_sponsors_title`, `featured_sponsors_image`, `active`) VALUES
(9, 'wetwe', 'feature_43191.png', '1');

-- --------------------------------------------------------

--
-- Table structure for table `file_gallery`
--

CREATE TABLE IF NOT EXISTS `file_gallery` (
  `id` int(10) NOT NULL,
  `file_name` varchar(200) DEFAULT NULL,
  `file_path` varchar(200) DEFAULT NULL,
  `file_desc` text,
  `equity_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `other` varchar(200) DEFAULT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `file_gallery`
--

INSERT INTO `file_gallery` (`id`, `file_name`, `file_path`, `file_desc`, `equity_id`, `status`, `other`, `date_added`) VALUES
(1, 'Investor Pitch Deck', '16681-deal-document-1448528290.pdf', NULL, 24, 0, '', '2015-11-26 08:58:10'),
(2, 'Business', '33002-deal-document-1448528302.pdf', NULL, 24, 0, '', '2015-11-26 08:58:22'),
(3, 'Disclosures', '57358-deal-document-1448528311.pdf', NULL, 24, 0, '', '2015-11-26 08:58:31'),
(4, 'Business Plan', '99287-deal-document-1449044370.pdf', NULL, 26, 0, '', '2015-12-02 08:19:30');

-- --------------------------------------------------------

--
-- Table structure for table `font_type`
--

CREATE TABLE IF NOT EXISTS `font_type` (
  `font_type_id` int(11) NOT NULL,
  `font_type_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `font_type`
--

INSERT INTO `font_type` (`font_type_id`, `font_type_name`) VALUES
(1, 'Antiqua'),
(2, 'Arial'),
(3, 'Blackletter'),
(4, 'Calibri'),
(5, 'Comic Sans MS'),
(6, 'Courier'),
(7, 'Cursive'),
(8, 'DaunPenh'),
(9, 'Felix Titling'),
(12, 'Sans-serif'),
(14, 'Shruti'),
(15, 'Tahoma'),
(17, 'Verdana'),
(18, 'PT Sans Web Bold'),
(19, 'Lucida Console'),
(20, 'PT Sans Web Regular');

-- --------------------------------------------------------

--
-- Table structure for table `funding_source`
--

CREATE TABLE IF NOT EXISTS `funding_source` (
  `id` int(11) NOT NULL,
  `funding_source_name` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `funding_source`
--

INSERT INTO `funding_source` (`id`, `funding_source_name`, `status`) VALUES
(1, 'Self', 1),
(2, 'Bank', 1),
(3, 'Investor', 1),
(4, 'Other', 1);

-- --------------------------------------------------------

--
-- Table structure for table `funding_type`
--

CREATE TABLE IF NOT EXISTS `funding_type` (
  `id` int(11) NOT NULL,
  `funding_type_name` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `funding_type`
--

INSERT INTO `funding_type` (`id`, `funding_type_name`, `status`) VALUES
(1, 'Equity', 1),
(2, 'Debt', 1),
(3, 'Convertible Note', 1),
(4, 'Revenue Share', 1),
(5, 'Other', 1);

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE IF NOT EXISTS `gallery` (
  `gallery_id` int(50) NOT NULL,
  `gallery_name` varchar(255) DEFAULT NULL,
  `gallery_image` varchar(255) DEFAULT NULL,
  `active` varchar(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `google_plus_setting`
--

CREATE TABLE IF NOT EXISTS `google_plus_setting` (
  `google_plus_setting_id` int(11) NOT NULL,
  `google_plus_link` varchar(255) DEFAULT NULL,
  `google_plus_enable` varchar(25) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `google_plus_setting`
--

INSERT INTO `google_plus_setting` (`google_plus_setting_id`, `google_plus_link`, `google_plus_enable`) VALUES
(1, 'https://plus.google.com/102435550769166171366/', '1');

-- --------------------------------------------------------

--
-- Table structure for table `google_setting`
--

CREATE TABLE IF NOT EXISTS `google_setting` (
  `google_setting_id` int(50) NOT NULL,
  `consumer_key` varchar(255) DEFAULT NULL,
  `consumer_secret` varchar(255) DEFAULT NULL,
  `google_enable` int(50) NOT NULL DEFAULT '1',
  `google_url` text
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `google_setting`
--

INSERT INTO `google_setting` (`google_setting_id`, `consumer_key`, `consumer_secret`, `google_enable`, `google_url`) VALUES
(1, '931491159223-eh6uf46rbc4pjtgobbpuavhqnm0d0ji1.apps.googleusercontent.com', 'FOx-S7XGk1YK1SW7jTEy0SsG', 1, 'https://www.google.co.in/');

-- --------------------------------------------------------

--
-- Table structure for table `guidelines`
--

CREATE TABLE IF NOT EXISTS `guidelines` (
  `guidelines_id` int(10) NOT NULL,
  `guidelines_title` varchar(255) DEFAULT NULL,
  `guidelines_content` longtext,
  `guidelines_meta_title` text,
  `guidelines_meta_keyword` text,
  `guidelines_meta_description` text
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `guidelines`
--

INSERT INTO `guidelines` (`guidelines_id`, `guidelines_title`, `guidelines_content`, `guidelines_meta_title`, `guidelines_meta_keyword`, `guidelines_meta_description`) VALUES
(1, 'Guidelines', '<p><strong>Project Guidelines</strong></p>\r\n<ol start=KSYDOU1KSYDOU>\r\n<li>FundraisingScript is a funding platform focused on a broad spectrum of creative projects. The guidelines below articulate our mission and focus. Please note that any project that violates these guidelines will be declined or removed. Please contact us if you have any questions.</li>\r\n<li><strong>Projects. Projects. Projects.</strong>&nbsp;FundraisingScript is for the funding of projects – albums, films, specific works – that have clearly defined goals and expectations.</li>\r\n<li><strong>Projects with a creative purpose.</strong>&nbsp;FundraisingScript can be used to fund projects from the creative fields of Art, Comics, Dance, Design, Fashion, Film, Food, Games, Music, Photography, Publishing, Technology, and Theater. We currently only support projects from these categories.</li>\r\n<li><strong>No charity or cause funding.</strong>&nbsp;Examples of prohibited use include raising money for the Red Cross, funding an awareness campaign, funding a scholarship, or donating a portion of funds raised on FundraisingScript to a charity or cause.</li>\r\n<li><strong>No KSYDOUfund my lifeKSYDOU projects.</strong>&nbsp;Examples include projects to pay tuition or bills, go on vacation, or buy a new camera.</li>\r\n<li><strong>Rewards, not financial incentives.</strong>&nbsp;The FundraisingScript economy is based on the offering of rewards – copies of the work, limited editions, fun experiences. Offering financial incentives, such as ownership, financial returns (for example, a share of profits), or repayment (loans) is prohibited.</li>\r\n</ol>\r\n<div  noshade=KSYDOUnoshadeKSYDOU width=KSYDOU533KSYDOU \r\n<p><strong>Community Guidelines</strong></p>\r\n<ol start=KSYDOU1KSYDOU>\r\n<li>We rely on respectful interactions to ensure that FundraisingScript is a friendly place. Please follow the rules below.</li>\r\n<li><strong>Spread the word but donKSYSINGt spam.</strong>&nbsp;Spam includes sending unsolicited @ messages to people on Twitter. This makes everyone on FundraisingScript look bad. DonKSYSINGt do it.</li>\r\n<li><strong>DonKSYSINGt promote a project on other projectsKSYSING pages.</strong>&nbsp;Your comments will be deleted and your account may be suspended.</li>\r\n<li><strong>Be courteous and respectful.</strong>&nbsp;DonKSYSINGt harass or abuse other members.</li>\r\n<li><strong>DonKSYSINGt post obscene, hateful, or objectionable content.</strong>&nbsp;If you do we will remove it and suspend you.</li>\r\n<li><strong>DonKSYSINGt post copyrighted content without permission.</strong>&nbsp;Only post content that you have the rights to.</li>\r\n<li><strong>If you donKSYSINGt like a project, donKSYSINGt back it.</strong>&nbsp;No need to be a jerk.</li>\r\n<li>Actions that violate these rules or our&nbsp;<a href=KSYDOUhttp://spicyfund.com/fund_demo/home/content/terms-and-conditions/12KSYDOU>Terms of Use</a>&nbsp;may lead to an account being suspended or deleted. WeKSYSINGd prefer not to do that, so be cool, okay? Okay.</li>\r\n</ol>\r\n  ', 'Guidelines-FundraisingScript', 'Guidelines-FundraisingScript', 'Guidelines-FundraisingScript');

-- --------------------------------------------------------

--
-- Table structure for table `home_page`
--

CREATE TABLE IF NOT EXISTS `home_page` (
  `home_id` int(11) NOT NULL,
  `home_title` varchar(255) DEFAULT NULL,
  `home_description` text,
  `active` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `home_page`
--

INSERT INTO `home_page` (`home_id`, `home_title`, `home_description`, `active`) VALUES
(1, 'Why Us?', '<p style="font-size: 11px;"><a style="color: rgb(0, 0, 0); font-weight: bold; text-decoration: none;" href="http://www.rockersinfo.com">Rockers Technologies</a> has already created the solution for fundraising. Anyone intends to raise funds for any cause can buy this solution from us to see rocketing change in incoming funds. One can directly start own website with this solution. More than 2 years of experience in this segment and pool of experienced and skillful developers help us to bring changes in the fundraising script in case client needs some updates. We understand that each client has different objectives so we offer customized solution. You tell us your objectives, and we will come up with modified script that suits your requirements.aaaa????????????</p>\n<p style="font-size: 18px; font-weight: bold;">We will make a fundraising website for you</p>\n<p style="font-size: 11px;">We are here to give wings to your fundraising ideas. We?ll make fundraising website for you. We already have ready to use clone of Crowd Funding website like kickstart ,gofundme , firstgiving and indiegogo and we will promise you to provide dedicated support and industry standard quality.</p>', 1);

-- --------------------------------------------------------

--
-- Table structure for table `image_setting`
--

CREATE TABLE IF NOT EXISTS `image_setting` (
  `image_setting_id` int(11) NOT NULL,
  `p_small_width` int(11) NOT NULL,
  `p_small_height` int(11) NOT NULL,
  `u_s_width` int(11) NOT NULL,
  `u_s_height` int(11) NOT NULL,
  `u_b_width` int(11) NOT NULL,
  `u_b_height` int(11) NOT NULL,
  `u_m_width` int(255) NOT NULL,
  `u_m_height` int(255) NOT NULL,
  `p_medium_width` int(11) NOT NULL,
  `p_medium_height` int(11) NOT NULL,
  `p_ratio` int(11) NOT NULL,
  `u_ratio` int(11) NOT NULL,
  `g_ratio` int(11) NOT NULL,
  `p_thumb_width` varchar(255) DEFAULT NULL,
  `p_thumb_height` varchar(255) DEFAULT NULL,
  `p_large_width` int(5) NOT NULL,
  `p_large_height` int(5) NOT NULL,
  `upload_limit` int(10) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `image_setting`
--

INSERT INTO `image_setting` (`image_setting_id`, `p_small_width`, `p_small_height`, `u_s_width`, `u_s_height`, `u_b_width`, `u_b_height`, `u_m_width`, `u_m_height`, `p_medium_width`, `p_medium_height`, `p_ratio`, `u_ratio`, `g_ratio`, `p_thumb_width`, `p_thumb_height`, `p_large_width`, `p_large_height`, `upload_limit`) VALUES
(1, 263, 150, 70, 70, 160, 160, 120, 120, 1200, 500, 0, 0, 0, '360', '283', 1300, 500, 1);

-- --------------------------------------------------------

--
-- Table structure for table `interest_request`
--

CREATE TABLE IF NOT EXISTS `interest_request` (
  `interest_request_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `equity_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `payment_status` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `deny_reason_interest` varchar(255) DEFAULT NULL,
  `reason_save_interest` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `interest_request`
--

INSERT INTO `interest_request` (`interest_request_id`, `user_id`, `equity_id`, `status`, `payment_status`, `created_date`, `updated_date`, `deny_reason_interest`, `reason_save_interest`) VALUES
(1, 2, 1, 2, 1, '2015-10-05 11:07:14', '2015-10-05 11:10:21', NULL, NULL),
(2, 3, 1, 2, 1, '2015-10-05 11:19:53', '2015-10-05 11:20:06', NULL, NULL),
(3, 0, 0, 1, 0, '2015-10-09 20:05:37', '2015-10-09 20:05:37', NULL, NULL),
(4, 9, 1, 2, 0, '2015-10-26 14:25:26', '2015-10-26 14:27:38', NULL, NULL),
(5, 8, 1, 2, 0, '2015-10-26 15:09:05', '2015-10-26 15:13:52', NULL, NULL),
(6, 10, 1, 2, 0, '2015-10-26 15:14:48', '2015-12-01 13:22:02', NULL, NULL),
(7, 11, 8, 2, 1, '2015-10-27 14:24:45', '2015-10-27 14:27:56', NULL, NULL),
(8, 12, 8, 2, 1, '2015-10-27 16:58:05', '2015-11-19 15:22:07', NULL, NULL),
(9, 7, 19, 1, 0, '2015-11-19 17:41:39', '2015-11-19 17:41:39', NULL, NULL),
(10, 16, 1, 2, 1, '2015-11-20 14:03:08', '2015-11-20 16:03:48', NULL, NULL),
(11, 11, 1, 2, 0, '2015-11-20 14:39:41', '2015-11-20 16:26:51', NULL, NULL),
(12, 37, 19, 2, 1, '2015-11-20 17:54:39', '2015-11-20 17:59:35', NULL, NULL),
(13, 11, 24, 2, 1, '2015-11-30 13:37:35', '2015-11-30 13:38:21', NULL, NULL),
(14, 12, 24, 2, 1, '2015-11-30 14:31:14', '2015-11-30 14:31:34', NULL, NULL),
(15, 7, 1, 1, 0, '2015-12-01 15:32:35', '2015-12-01 15:32:35', NULL, NULL),
(16, 43, 1, 2, 1, '2015-12-02 11:41:26', '2015-12-02 11:42:07', NULL, NULL),
(17, 45, 24, 2, 1, '2015-12-02 11:54:15', '2015-12-02 11:54:35', NULL, NULL),
(18, 46, 24, 2, 0, '2015-12-02 13:25:28', '2015-12-02 13:26:05', NULL, NULL),
(19, 45, 26, 2, 0, '2015-12-02 13:53:46', '2015-12-02 13:54:17', NULL, NULL),
(20, 1, 26, 1, 0, '2015-12-02 16:25:24', '2015-12-02 16:25:24', NULL, NULL),
(21, 45, 1, 1, 0, '2015-12-21 11:58:42', '2015-12-21 11:58:42', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `investment_tax_relief`
--

CREATE TABLE IF NOT EXISTS `investment_tax_relief` (
  `id` int(11) NOT NULL,
  `investment_tax_relief_name` varchar(255) NOT NULL,
  `investment_tax_relief_desc` varchar(255) NOT NULL,
  `investment_tax_relief_link` varchar(255) NOT NULL,
  `investment_tax_relief_type` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `investment_tax_relief`
--

INSERT INTO `investment_tax_relief` (`id`, `investment_tax_relief_name`, `investment_tax_relief_desc`, `investment_tax_relief_link`, `investment_tax_relief_type`, `status`) VALUES
(1, 'seed enterprise investment scheme', 'seed enterprise investment scheme', '', 'SEIS', 1),
(2, 'enterprise investment scheme', 'enterprise investment scheme', '', 'EIS', 1);

-- --------------------------------------------------------

--
-- Table structure for table `investors`
--

CREATE TABLE IF NOT EXISTS `investors` (
  `investor_id` int(11) NOT NULL,
  `equity_id` int(11) NOT NULL,
  `investor_type` varchar(255) DEFAULT NULL,
  `investor_name` varchar(255) DEFAULT NULL,
  `investor_email` varchar(255) DEFAULT NULL,
  `investor_role` varchar(255) DEFAULT NULL,
  `investor_image` varchar(255) DEFAULT NULL,
  `investor_description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `investors`
--

INSERT INTO `investors` (`investor_id`, `equity_id`, `investor_type`, `investor_name`, `investor_email`, `investor_role`, `investor_image`, `investor_description`) VALUES
(1, 2, 'Institutional Investor', 'ssdsd', 'sdsdsdsd@gmail.com', NULL, NULL, 'Add funding from previously closed rounds. Do not include funding within your current round.'),
(2, 24, 'Institutional Investor', 'Rockers', 'herrypatel365@gmail.com', NULL, '22311-investor-1448528255.png', 'Investors Bio summary - https://www.youtube.com/watch?v=VxwsBSoenqs'),
(3, 26, 'Individual Investor', 'Vatsal Prajapati', 'vatsal.test.rockersinfo@gmail.com', NULL, NULL, 'Invester Bio');

-- --------------------------------------------------------

--
-- Table structure for table `investor_type`
--

CREATE TABLE IF NOT EXISTS `investor_type` (
  `id` int(10) NOT NULL,
  `investor_type_name` varchar(200) DEFAULT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `investor_type`
--

INSERT INTO `investor_type` (`id`, `investor_type_name`, `status`) VALUES
(1, 'Institutional Investor', 1),
(2, 'Individual Investor', 1);

-- --------------------------------------------------------

--
-- Table structure for table `invite_members`
--

CREATE TABLE IF NOT EXISTS `invite_members` (
  `id` int(10) NOT NULL,
  `user_id` int(10) DEFAULT '0',
  `admin` tinyint(2) DEFAULT '0',
  `email` varchar(100) DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  `invite_user_id` int(50) NOT NULL,
  `member_role` varchar(255) DEFAULT NULL,
  `equity_id` int(11) NOT NULL,
  `team_member_type` varchar(255) DEFAULT NULL,
  `team_member_name` varchar(255) DEFAULT NULL,
  `team_memberd_description` varchar(255) DEFAULT NULL,
  `visible_profile` varchar(255) DEFAULT NULL,
  `member_image` varchar(255) DEFAULT NULL,
  `company_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invite_members`
--

INSERT INTO `invite_members` (`id`, `user_id`, `admin`, `email`, `code`, `status`, `invite_user_id`, `member_role`, `equity_id`, `team_member_type`, `team_member_name`, `team_memberd_description`, `visible_profile`, `member_image`, `company_id`) VALUES
(1, 0, 0, NULL, NULL, 0, 1, 'Owner', 1, 'Leadership', 'Jayshree Patel', NULL, '1', NULL, 0),
(2, 0, 0, NULL, NULL, 0, 4, 'Owner', 2, 'Leadership', 'kashyap gandhi', NULL, '1', NULL, 0),
(3, 0, 1, 'jigar.rockersinfo@gmail.com', '340985696', 0, 0, 'Sr. Dev', 0, 'Leadership', 'Jigar Gandhi', 'Add your team members and advisors so that people know who is involved. Each stakeholder represents a powerful and personal way to demonstrate the strength of y', '1', NULL, 6),
(4, 0, 0, NULL, NULL, 0, 1, 'Owner', 3, 'Leadership', 'Jayshree Patel', NULL, '1', NULL, 0),
(5, 0, 0, NULL, NULL, 0, 6, 'Owner', 4, 'Leadership', 'pratik parmar', NULL, '1', NULL, 0),
(6, 0, 0, NULL, NULL, 0, 1, 'Owner', 5, 'Leadership', 'Jayshree Patel', NULL, '1', NULL, 0),
(8, 0, 1, 'ankit.rockersinfo@gmail.com', '911405735', 0, 0, 'Team Lead', 0, 'Leadership', 'Mihir Upadhyay', '', '1', NULL, 16),
(9, 0, 0, NULL, NULL, 0, 1, 'Owner', 6, 'Leadership', 'Jayshree Patel', NULL, '1', NULL, 0),
(10, 0, 0, 'mihir3018@gmail.com', '194952570', 0, 0, 'Team Lead', 0, 'Leadership', 'John Doe', 'test', '', NULL, 16),
(11, 0, 0, 'agfdg.rockersinfo@gmail.com', '231805888', 0, 0, '', 0, 'Team Member', 'ggsfdg gdfsgsdf', '', '', NULL, 41),
(12, 0, 0, 'demo@rockersinfo.com', '446712235', 0, 0, 'gfdgsdgs', 0, 'Team Member', 'gsdfg g sdg dsf', 'gdsfgdfs', '1', NULL, 42),
(13, 0, 0, 'agfdg.rockersinfo@gmail.com', '71258301', 0, 0, '', 0, 'Team Member', 'gsdfg gds fdg', 'gfdsgfd', '', NULL, 43),
(14, 0, 0, NULL, NULL, 0, 1, 'Owner', 7, 'Leadership', 'Jayshree Patel', NULL, '1', NULL, 0),
(15, 0, 1, 'darshanrockersinfo@gmail.com', '165870770', 0, 0, 'Admin', 1, 'Leadership', 'Darshan Patel', '', '1', NULL, 2),
(16, 0, 1, 'rahul.rockersinfo@gmail.com', '977570895', 0, 0, 'admin', 1, 'Leadership', 'Rahul Patel', '', '1', NULL, 2),
(17, 0, 0, NULL, NULL, 0, 7, 'Owner', 8, 'Leadership', 'Sandy Preston', NULL, '1', NULL, 0),
(18, 0, 0, NULL, NULL, 0, 1, 'Owner', 9, 'Leadership', 'Jayshree Patel', NULL, '1', NULL, 0),
(19, 0, 1, 'darshanrockersinfo@gmail.com', '165870770', 0, 0, 'Admin', 9, 'Leadership', 'Darshan Patel', '', '1', NULL, 0),
(20, 0, 1, 'rahul.rockersinfo@gmail.com', '977570895', 0, 0, 'admin', 9, 'Leadership', 'Rahul Patel', '', '1', NULL, 0),
(21, 0, 0, NULL, NULL, 0, 1, 'Owner', 10, 'Leadership', 'Jayshree Patel', NULL, '1', NULL, 0),
(22, 0, 0, NULL, NULL, 0, 1, 'Owner', 11, 'Leadership', 'Jayshree Patel', NULL, '1', NULL, 0),
(23, 0, 1, 'darshanrockersinfo@gmail.com', '165870770', 0, 0, 'Admin', 11, 'Leadership', 'Darshan Patel', '', '1', NULL, 0),
(24, 0, 1, 'rahul.rockersinfo@gmail.com', '977570895', 0, 0, 'admin', 11, 'Leadership', 'Rahul Patel', '', '1', NULL, 0),
(25, 0, 0, NULL, NULL, 0, 1, 'Owner', 12, 'Leadership', 'Jayshree Patel', NULL, '1', NULL, 0),
(26, 0, 1, 'ankit.rockersinfo@gmail.com', '911405735', 0, 0, 'Team Lead', 12, 'Leadership', 'Mihir Upadhyay', '', '1', NULL, 0),
(27, 0, 0, 'mihir3018@gmail.com', '194952570', 0, 0, 'Team Lead', 12, 'Leadership', 'John Doe', 'test', '', NULL, 0),
(28, 0, 0, NULL, NULL, 0, 13, 'Owner', 13, 'Leadership', 'admin ', NULL, '1', NULL, 0),
(29, 0, 0, NULL, NULL, 0, 1, 'Owner', 14, 'Leadership', 'Jayshree Patel', NULL, '1', NULL, 0),
(30, 0, 0, NULL, NULL, 0, 16, 'Owner', 15, 'Leadership', 'Tim Rocknew', NULL, '1', NULL, 0),
(31, 0, 0, NULL, NULL, 0, 7, 'Owner', 16, 'Leadership', 'Sandy Preston', NULL, '1', NULL, 0),
(32, 0, 0, NULL, NULL, 0, 7, 'Owner', 17, 'Leadership', 'Sandy Preston', NULL, '1', NULL, 0),
(33, 0, 0, NULL, NULL, 0, 7, 'Owner', 18, 'Leadership', 'Sandy Preston', NULL, '1', NULL, 0),
(34, 0, 0, NULL, NULL, 0, 16, 'Owner', 19, 'Leadership', 'Tim Rocknew', NULL, '1', NULL, 0),
(35, 0, 0, NULL, NULL, 0, 16, 'Owner', 20, 'Leadership', 'Tim Rocknew', NULL, '1', NULL, 0),
(36, 0, 0, NULL, NULL, 0, 1, 'Owner', 21, 'Leadership', 'Jayshree Patel', NULL, '1', NULL, 0),
(37, 0, 0, NULL, NULL, 0, 37, 'Owner', 22, 'Leadership', 'Stevenew Rocknew', NULL, '1', NULL, 0),
(38, 0, 0, NULL, NULL, 0, 7, 'Owner', 23, 'Leadership', 'Sandy Preston', NULL, '1', NULL, 0),
(39, 0, 0, NULL, NULL, 0, 7, 'Owner', 24, 'Leadership', 'Sandy Preston', NULL, '1', NULL, 0),
(40, 0, 1, 'rakesh.rockersinfo@gmail.com', '24658168', 0, 0, 'Project Manager', 24, 'Leadership', 'Rakesh Patel', 'Smart, dynamic and technically powerful project manager at Rockers Technology.He believes in making the things better with appropriate involvement of technology', '1', '83162-team-member-1448450991.png', 54),
(41, 0, 1, 'ankit.rockersinfo@gmail.com', '607703679', 0, 0, 'Developer', 24, 'Team Member', 'Ankit Patel', 'He is the man with a vision beyond imagination and holds the position of Business Development Manager at Rockers Technology.', '1', '33193-team-member-1448451057.png', 54),
(42, 0, 0, NULL, NULL, 0, 43, 'Owner', 25, 'Leadership', 'rakesh patel', NULL, '1', NULL, 0),
(43, 0, 0, NULL, NULL, 0, 46, 'Owner', 26, 'Leadership', 'Vikram Patel', NULL, '1', NULL, 0),
(44, 0, 1, 'vatsal.test.rockersinfo@gmail.com', '379285293', 0, 0, 'Developer', 26, 'Leadership', 'Vashu Patel', 'Team Member Bio', '1', NULL, 56),
(45, 0, 0, NULL, NULL, 0, 1, 'Owner', 27, 'Leadership', 'Jayshree Patel', NULL, '1', NULL, 0),
(46, 0, 1, 'darshanrockersinfo@gmail.com', '165870770', 0, 0, 'Admin', 27, 'Leadership', 'Darshan Patel', '', '1', NULL, 0),
(47, 0, 1, 'rahul.rockersinfo@gmail.com', '977570895', 0, 0, 'admin', 27, 'Leadership', 'Rahul Patel', '', '1', NULL, 0),
(48, 0, 0, NULL, NULL, 0, 7, 'Owner', 28, 'Leadership', 'Sandy Preston', NULL, '1', NULL, 0),
(49, 0, 1, 'rakesh.rockersinfo@gmail.com', '24658168', 0, 0, 'Project Manager', 28, 'Leadership', 'Rakesh Patel', 'Smart, dynamic and technically powerful project manager at Rockers Technology.He believes in making the things better with appropriate involvement of technology', '1', '83162-team-member-1448450991.png', 0),
(50, 0, 1, 'ankit.rockersinfo@gmail.com', '607703679', 0, 0, 'Developer', 28, 'Team Member', 'Ankit Patel', 'He is the man with a vision beyond imagination and holds the position of Business Development Manager at Rockers Technology.', '1', '33193-team-member-1448451057.png', 0),
(51, 0, 0, NULL, NULL, 0, 7, 'Owner', 29, 'Leadership', 'Sandy Preston', NULL, '1', NULL, 0),
(52, 0, 0, NULL, NULL, 0, 1, 'Owner', 30, 'Leadership', 'Jayshree Patel', NULL, '1', NULL, 0),
(53, 0, 1, 'darshanrockersinfo@gmail.com', '165870770', 0, 0, 'Admin', 30, 'Leadership', 'Darshan Patel', '', '1', NULL, 0),
(54, 0, 1, 'rahul.rockersinfo@gmail.com', '977570895', 0, 0, 'admin', 30, 'Leadership', 'Rahul Patel', '', '1', NULL, 0),
(55, 0, 0, NULL, NULL, 0, 1, 'Owner', 31, 'Leadership', 'Jayshree Patel', NULL, '1', NULL, 0),
(56, 0, 0, NULL, NULL, 0, 1, 'Owner', 32, 'Leadership', 'Jayshree Patel', NULL, '1', NULL, 0),
(57, 0, 1, 'ankit.rockersinfo@gmail.com', '911405735', 0, 0, 'Team Lead', 32, 'Leadership', 'Mihir Upadhyay', '', '1', NULL, 0),
(58, 0, 0, 'mihir3018@gmail.com', '194952570', 0, 0, 'Team Lead', 32, 'Leadership', 'John Doe', 'test', '', NULL, 0),
(59, 0, 0, NULL, NULL, 0, 1, 'Owner', 33, 'Leadership', 'Jayshree Patel', NULL, '1', NULL, 0),
(60, 0, 1, 'darshanrockersinfo@gmail.com', '165870770', 0, 0, 'Admin', 33, 'Leadership', 'Darshan Patel', '', '1', NULL, 0),
(61, 0, 1, 'rahul.rockersinfo@gmail.com', '977570895', 0, 0, 'admin', 33, 'Leadership', 'Rahul Patel', '', '1', NULL, 0),
(62, 0, 0, NULL, NULL, 0, 7, 'Owner', 34, 'Leadership', 'Sandy Preston', NULL, '1', NULL, 0),
(63, 0, 0, NULL, NULL, 0, 1, 'Owner', 35, 'Leadership', 'Jayshree Patel', NULL, '1', NULL, 0),
(64, 0, 0, NULL, NULL, 0, 7, 'Owner', 36, 'Leadership', 'Sandy Preston', NULL, '1', NULL, 0),
(65, 0, 0, 'darshanrockersinfo@gmail.com', '384063731', 0, 0, 'wrw', 35, 'Team Member', 'darshan patel', 'wer', '', '69867-team-member-1449065051.jpg', 14),
(66, 0, 0, NULL, NULL, 0, 8, 'Owner', 37, 'Leadership', 'Hardik Jivani', NULL, '1', NULL, 0),
(67, 0, 0, NULL, NULL, 0, 45, 'Owner', 38, 'Leadership', 'Vashu Patel', NULL, '1', NULL, 0),
(68, 0, 0, NULL, NULL, 0, 45, 'Owner', 39, 'Leadership', 'Vashu Patel', NULL, '1', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `invite_request`
--

CREATE TABLE IF NOT EXISTS `invite_request` (
  `request_id` int(100) NOT NULL,
  `invite_code` text,
  `invite_email` varchar(255) DEFAULT NULL,
  `invite_date` datetime NOT NULL,
  `invite_ip` varchar(255) DEFAULT NULL,
  `invite_by` int(100) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invite_request`
--

INSERT INTO `invite_request` (`request_id`, `invite_code`, `invite_email`, `invite_date`, `invite_ip`, `invite_by`) VALUES
(1, 'kZdqxLxekuho', 'test@test.com', '2015-11-19 17:19:55', '27.109.7.130', 16),
(2, 'kZdqxLxekuho', 'jayshree.rockersinfo@gmail.com', '2015-11-19 17:20:38', '27.109.7.130', 16),
(3, 'nj8jnnHpHZnM', 'abc@xyz.com', '2015-11-26 16:28:39', '27.109.7.130', 7);

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE IF NOT EXISTS `language` (
  `language_id` int(11) NOT NULL,
  `language_name` varchar(255) DEFAULT NULL,
  `iso2` varchar(255) DEFAULT NULL,
  `iso3` varchar(255) DEFAULT NULL,
  `active` varchar(255) DEFAULT NULL,
  `language_folder` varchar(250) DEFAULT NULL,
  `country_flag` varchar(250) DEFAULT NULL,
  `direction` varchar(50) DEFAULT NULL,
  `default` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`language_id`, `language_name`, `iso2`, `iso3`, `active`, `language_folder`, `country_flag`, `direction`, `default`) VALUES
(1, 'English', 'en', '', '1', 'english', '', 'ltr', 1),
(2, 'French', 'fr', 'fr_FR', '1', 'french', '', 'ltr', 0);

-- --------------------------------------------------------

--
-- Table structure for table `learn_more`
--

CREATE TABLE IF NOT EXISTS `learn_more` (
  `pages_id` int(11) NOT NULL,
  `parent_id` int(10) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL,
  `pages_title` varchar(255) DEFAULT NULL,
  `sub_title` varchar(255) DEFAULT NULL,
  `icon_image` varchar(255) DEFAULT NULL,
  `description` text,
  `slug` varchar(255) DEFAULT NULL,
  `active` varchar(255) DEFAULT NULL,
  `meta_keyword` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `footer_bar` varchar(20) DEFAULT NULL,
  `header_bar` varchar(20) DEFAULT NULL,
  `left_side` varchar(20) DEFAULT NULL,
  `right_side` varchar(20) DEFAULT NULL,
  `external_link` text,
  `learn_category` varchar(255) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=88 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `learn_more`
--

INSERT INTO `learn_more` (`pages_id`, `parent_id`, `language_id`, `pages_title`, `sub_title`, `icon_image`, `description`, `slug`, `active`, `meta_keyword`, `meta_description`, `footer_bar`, `header_bar`, `left_side`, `right_side`, `external_link`, `learn_category`) VALUES
(64, 0, 1, 'How to Sign Up', '', '', '<p>The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced by fox whelps. Bawds jog, flick quartz, vex nymphs. Waltz, bad nymph, for quick jigs vex! Fox nymphs grab quick-jived waltz. Brick quiz whangs jumpy veldt fox. Bright vixens jump; dozy fowl quack. Quick wafting zephyrs vex bold Jim. Quick zephyrs blow, vexing daft Jim. Sex-charged fop blew my junk TV quiz. How quickly daft jumping zebras vex.</p>\n\n<p>Two driven jocks help fax my big quiz. Quick, Baz, get my woven flax jodhpurs! &quot;Now fax quiz Jack! &quot; my brave ghost pled. Five quacking zephyrs jolt my wax bed. Flummoxed by job, kvetching W. zaps Iraq. Cozy sphinx waves quart jug of bad milk. A very bad quack might jinx zippy fowls. Few quips galvanized the mock jury box. Quick brown dogs jump over the lazy fox. The jay, pig, fox, zebra, and my wolves quack! Blowzy red vixens fight for a quick jump.</p>\n\n<p>Joaquin Phoenix was gazed by MTV for luck. A wizard&rsquo;s job is to vex chumps quickly in fog. Watch &quot;Jeopardy! &quot;, Alex Trebek&#39;s fun TV quiz game. Woven silk pyjamas exchanged for blue quartz. Brawny gods just flocked up to quiz and vex him. Adjusting quiver and bow, Zompyc[1] killed the fox. My faxed joke won a pager in the cable TV quiz show. Amazingly few discotheques provide jukeboxes. My girl wove six dozen plaid jackets before she quit. Six big devils from Japan quickly forgot how to waltz. Big July earthquakes confound zany experimental vow. Foxy parsons quiz and cajole the lovably dim wiki-girl. Have a pick: twenty six letters - no forcing a jumbled quiz! Crazy Fredericka bought many very exquisite opal jewels. Sixty zippers were quickly picked from the woven jute bag. A quick</p>\n', 'how-to-sign-up', '1', '', '', '0', '0', '0', '0', '0', '17'),
(65, 0, 1, 'Contribution Basics', '', '', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.</p>\n\n<p>Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien.</p>\n\n<p>Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede.</p>\n\n<p>Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum fringilla pede sit amet augue. In turpis. Pellentesque posuere. Praesent turpis. Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero. Phasellus dolor. Maecenas vestibulum mollis</p>\n', 'contribution-basics', '1', '', '', '0', '0', '0', '0', '0', '14'),
(66, 0, 1, 'How to Upgrade Your Perk', '', '', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.</p>\n\n<p>Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien.</p>\n\n<p>Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede.</p>\n\n<p>Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum fringilla pede sit amet augue. In turpis. Pellentesque posuere. Praesent turpis. Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero. Phasellus dolor. Maecenas vestibulum mollis</p>\n', 'upgrade-your-perk', '1', '', '', '0', '0', '0', '0', '0', '14'),
(67, 0, 1, 'How to Check on Your Perks', '', '', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.</p>\n\n<p>Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien.</p>\n\n<p>Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede.</p>\n\n<p>Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum fringilla pede sit amet augue. In turpis. Pellentesque posuere. Praesent turpis. Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero. Phasellus dolor. Maecenas vestibulum mollis</p>\n', 'check-perks', '1', '', '', '0', '0', '0', '0', '0', '14'),
(68, 0, 1, 'Common Contribution Errors', '', '', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.</p>\n\n<p>Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien.</p>\n\n<p>Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede.</p>\n\n<p>Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum fringilla pede sit amet augue. In turpis. Pellentesque posuere. Praesent turpis. Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero. Phasellus dolor. Maecenas vestibulum mollis</p>\n', 'contribution-error', '1', '', '', '0', '0', '0', '0', '0', '14'),
(69, 0, 1, 'How to Contribute Anonymously', '', '', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.</p>\n\n<p>Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien.</p>\n\n<p>Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede.</p>\n\n<p>Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum fringilla pede sit amet augue. In turpis. Pellentesque posuere. Praesent turpis. Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero. Phasellus dolor. Maecenas vestibulum mollis</p>\n', 'contribute-anonymously', '1', '', '', '0', '0', '0', '0', '0', '14'),
(70, 0, 1, 'How To Create a Campaign', '', '', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.</p>\n\n<p>Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien.</p>\n\n<p>Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede.</p>\n\n<p>Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum fringilla pede sit amet augue. In turpis. Pellentesque posuere. Praesent turpis. Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero. Phasellus dolor. Maecenas vestibulum mollis</p>\n', 'create-campaign', '1', '', '', '0', '0', '0', '0', '0', '16'),
(71, 0, 1, 'About Verifications', '', '', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.</p>\n\n<p>Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien.</p>\n\n<p>Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede.</p>\n\n<p>Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum fringilla pede sit amet augue. In turpis. Pellentesque posuere. Praesent turpis. Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero. Phasellus dolor. Maecenas vestibulum mollis</p>\n', 'about-verifications', '1', '', '', '0', '0', '0', '0', '0', '16'),
(72, 0, 1, 'How to Go Live', '', '', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.</p>\n\n<p>Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien.</p>\n\n<p>Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede.</p>\n\n<p>Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum fringilla pede sit amet augue. In turpis. Pellentesque posuere. Praesent turpis. Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero. Phasellus dolor. Maecenas vestibulum mollis</p>\n', 'how-to-golive', '1', '', '', '0', '0', '0', '0', '0', '16'),
(73, 0, 1, 'How To Add Media To Pitch Text', '', '', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.</p>\n\n<p>Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien.</p>\n\n<p>Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede.</p>\n\n<p>Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum fringilla pede sit amet augue. In turpis. Pellentesque posuere. Praesent turpis. Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero. Phasellus dolor. Maecenas vestibulum mollis</p>\n', 'How-to-add-pitch-text', '1', '', '', '0', '0', '0', '0', '0', '16'),
(74, 0, 1, 'Private or Anonymous Donations', '', '', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.</p>\n\n<p>Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien.</p>\n\n<p>Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede.</p>\n\n<p>Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum fringilla pede sit amet augue. In turpis. Pellentesque posuere. Praesent turpis. Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero. Phasellus dolor. Maecenas vestibulum mollis</p>\n', 'anonymous-donations', '1', '', '', '0', '0', '0', '0', '0', '13'),
(75, 0, 1, 'How Are Payments Processed', '', '', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.</p>\n\n<p>Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien.</p>\n\n<p>Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede.</p>\n\n<p>Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum fringilla pede sit amet augue. In turpis. Pellentesque posuere. Praesent turpis. Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero. Phasellus dolor. Maecenas vestibulum mollis</p>\n', 'payments-processed', '1', '', '', '0', '0', '0', '0', '0', '13'),
(76, 0, 1, 'How Long Do Withdrawals Take', '', '', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.</p>\n\n<p>Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien.</p>\n\n<p>Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede.</p>\n\n<p>Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum fringilla pede sit amet augue. In turpis. Pellentesque posuere. Praesent turpis. Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero. Phasellus dolor. Maecenas vestibulum mollis</p>\n', 'long-withdrawals', '1', '', '', '0', '0', '0', '0', '0', '13'),
(77, 0, 1, 'How Do Withdraw Money', '', '', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.</p>\n\n<p>Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien.</p>\n\n<p>Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede.</p>\n\n<p>Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum fringilla pede sit amet augue. In turpis. Pellentesque posuere. Praesent turpis. Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero. Phasellus dolor. Maecenas vestibulum mollis</p>\n', 'withdraw-money', '1', '', '', '0', '0', '0', '0', '0', '13');
INSERT INTO `learn_more` (`pages_id`, `parent_id`, `language_id`, `pages_title`, `sub_title`, `icon_image`, `description`, `slug`, `active`, `meta_keyword`, `meta_description`, `footer_bar`, `header_bar`, `left_side`, `right_side`, `external_link`, `learn_category`) VALUES
(78, 0, 1, 'Stripe Fees (UK Accounts)', '', '', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.</p>\n\n<p>Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien.</p>\n\n<p>Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede.</p>\n\n<p>Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum fringilla pede sit amet augue. In turpis. Pellentesque posuere. Praesent turpis. Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero. Phasellus dolor. Maecenas vestibulum mollis</p>\n', 'stripe-fees', '1', '', '', '0', '0', '0', '0', '0', '13'),
(79, 0, 1, 'International Campaigns', '', '', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.</p>\n\n<p>Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien.</p>\n\n<p>Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede.</p>\n\n<p>Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum fringilla pede sit amet augue. In turpis. Pellentesque posuere. Praesent turpis. Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero. Phasellus dolor. Maecenas vestibulum mollis</p>\n', 'international-campaigns', '1', '', '', '0', '0', '0', '0', '0', '17'),
(80, 0, 1, 'Payment Types', '', '', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.</p>\n\n<p>Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien.</p>\n\n<p>Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede.</p>\n\n<p>Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum fringilla pede sit amet augue. In turpis. Pellentesque posuere. Praesent turpis. Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero. Phasellus dolor. Maecenas vestibulum mollis</p>\n', 'payment-types', '1', '', '', '0', '0', '0', '0', '0', '17'),
(81, 0, 1, 'Share Your Campaign ', '', '', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.</p>\n\n<p>Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien.</p>\n\n<p>Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede.</p>\n\n<p>Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum fringilla pede sit amet augue. In turpis. Pellentesque posuere. Praesent turpis. Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero. Phasellus dolor. Maecenas vestibulum mollis</p>\n', 'share-campaign ', '1', '', '', '0', '0', '0', '0', '0', '15'),
(82, 0, 1, 'Relaunching a Campaign', '', '', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.</p>\n\n<p>Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien.</p>\n\n<p>Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede.</p>\n\n<p>Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum fringilla pede sit amet augue. In turpis. Pellentesque posuere. Praesent turpis. Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero. Phasellus dolor. Maecenas vestibulum mollis</p>\n', 'relaunching-campaign', '1', '', '', '0', '0', '0', '0', '0', '15'),
(83, 0, 1, 'Following Up and Sending Perks', '', '', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.</p>\n\n<p>Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien.</p>\n\n<p>Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede.</p>\n\n<p>Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum fringilla pede sit amet augue. In turpis. Pellentesque posuere. Praesent turpis. Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero. Phasellus dolor. Maecenas vestibulum mollis</p>\n', 'sending-perks', '1', '', '', '0', '0', '0', '0', '0', '15'),
(84, 0, 1, 'Deleting a Campaign', '', '', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.</p>\n\n<p>Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien.</p>\n\n<p>Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede.</p>\n\n<p>Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum fringilla pede sit amet augue. In turpis. Pellentesque posuere. Praesent turpis. Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero. Phasellus dolor. Maecenas vestibulum mollis</p>\n', 'deleting-campaign', '1', '', '', '0', '0', '0', '0', '0', '15'),
(85, 0, 1, 'Refunds', '', '', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.</p>\n\n<p>Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien.</p>\n\n<p>Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede.</p>\n\n<p>Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum fringilla pede sit amet augue. In turpis. Pellentesque posuere. Praesent turpis. Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero. Phasellus dolor. Maecenas vestibulum mollis</p>\n', 'refunds', '1', '', '', '0', '0', '0', '0', '0', '12'),
(86, 0, 1, 'How to Contribute for a Friend', '', '', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.</p>\n\n<p>Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien.</p>\n\n<p>Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede.</p>\n\n<p>Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum fringilla pede sit amet augue. In turpis. Pellentesque posuere. Praesent turpis. Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero. Phasellus dolor. Maecenas vestibulum mollis</p>\n', 'Contribute-for-friend', '1', '', '', '0', '0', '0', '0', '0', '12'),
(87, 0, 1, 'How to Contribute Anonymously', '', '', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.</p>\n\n<p>Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien.</p>\n\n<p>Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede.</p>\n\n<p>Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum fringilla pede sit amet augue. In turpis. Pellentesque posuere. Praesent turpis. Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero. Phasellus dolor. Maecenas vestibulum mollis</p>\n', 'contribute-anonymously', '1', '', '', '0', '0', '0', '0', '0', '12');

-- --------------------------------------------------------

--
-- Table structure for table `learn_more_category`
--

CREATE TABLE IF NOT EXISTS `learn_more_category` (
  `category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `category_name` varchar(255) DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `footer` varchar(255) DEFAULT NULL,
  `right_side` varchar(255) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `learn_more_category`
--

INSERT INTO `learn_more_category` (`category_id`, `language_id`, `category_name`, `active`, `footer`, `right_side`) VALUES
(12, 1, 'Manage Your Contribution', 1, '0', '0'),
(13, 1, 'Donations and Withdrawals', 1, '0', '0'),
(14, 1, 'Contributing to a Campaign', 1, '0', '0'),
(15, 1, 'Managing Your Campaign', 1, '0', '0'),
(16, 1, 'Creating a Campaign', 1, '0', '0'),
(17, 1, 'Fundraising Basics', 1, '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `linkdin_setting`
--

CREATE TABLE IF NOT EXISTS `linkdin_setting` (
  `linkdin_setting_id` int(11) NOT NULL,
  `linkdin_enable` varchar(255) DEFAULT NULL,
  `linkedin_access` varchar(255) DEFAULT NULL,
  `linkedin_secret` varchar(255) DEFAULT NULL,
  `linkdin_url` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `linkdin_setting`
--

INSERT INTO `linkdin_setting` (`linkdin_setting_id`, `linkdin_enable`, `linkedin_access`, `linkedin_secret`, `linkdin_url`) VALUES
(1, '1', '75zer2rye8h9nz', 'snOEkR5v5yCWxNmj', 'https://in.linkedin.com/');

-- --------------------------------------------------------

--
-- Table structure for table `message_conversation`
--

CREATE TABLE IF NOT EXISTS `message_conversation` (
  `message_id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `is_read` int(11) NOT NULL DEFAULT '0' COMMENT '0-unread,1-read',
  `admin_replay` varchar(255) DEFAULT NULL,
  `type` int(11) NOT NULL,
  `message_subject` text,
  `message_content` text,
  `equity_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `reply_message_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `created_id` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `message_conversation`
--

INSERT INTO `message_conversation` (`message_id`, `sender_id`, `receiver_id`, `is_read`, `admin_replay`, `type`, `message_subject`, `message_content`, `equity_id`, `date_added`, `reply_message_id`, `created`, `created_id`) VALUES
(1, 7, 1, 1, 'admin', 1, '<a href="http://mydesichef.com/real-estate-development/development/rockers-branch"></a> Feedback', '', 8, '2015-10-26 16:40:08', 0, NULL, NULL),
(2, 1, 16, 1, 'admin', 1, '<a href="http://mydesichef.com/realestate_development_ci3/development/test-project1"></a> Feedback', 'Hello.. Test message', 19, '2015-11-20 16:26:17', 0, '2015-11-20 16:26:17', 1),
(3, 1, 16, 1, 'admin', 1, '<a href="http://mydesichef.com/realestate_development_ci3/development/test-project1"></a> Feedback', 'Hello.. Test message', 19, '2015-11-20 16:26:17', 0, '2015-11-20 16:26:17', 1),
(4, 1, 16, 1, 'admin', 1, '<a href="http://mydesichef.com/realestate_development_ci3/development/test-project1"></a> Feedback', 'Abcde', 19, '2015-11-20 16:26:34', 2, '2015-11-20 16:26:34', 1),
(5, 1, 16, 1, 'admin', 1, '<a href="http://mydesichef.com/realestate_development_ci3/development/test-project1"></a> Feedback', 'Abcde', 19, '2015-11-20 16:26:34', 2, '2015-11-20 16:26:34', 1),
(6, 1, 11, 1, 'admin', 2, '<a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies"></a> Investment Process', 'Hello', 1, '2015-11-20 16:58:41', 0, '2015-11-20 16:58:41', 1),
(7, 1, 11, 1, 'admin', 2, '<a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies"></a> Investment Process', 'Hello', 1, '2015-11-20 16:58:41', 0, '2015-11-20 16:58:41', 1),
(8, 7, 1, 1, '', 1, '<a href="http://mydesichef.com/real-estate-development/development/rockers-branch"></a> Feedback', 'HI', 8, '2015-11-26 14:55:46', 1, NULL, NULL),
(9, 1, 7, 1, 'admin', 1, '<a href="http://mydesichef.com/realestate_development_ci3/development/rockers-branch"></a> Feedback', 'Hello', 8, '2015-11-26 14:56:15', 1, '2015-11-26 14:56:15', 1),
(10, 1, 7, 1, 'admin', 1, '<a href="http://mydesichef.com/realestate_development_ci3/development/rockers-branch"></a> Feedback', 'Hello', 8, '2015-11-26 14:56:15', 1, '2015-11-26 14:56:15', 1),
(11, 7, 1, 1, '', 1, '<a href="http://mydesichef.com/real-estate-development/development/rockers-branch"></a> Feedback', 'Good Morning', 8, '2015-11-26 14:57:58', 1, NULL, NULL),
(12, 7, 1, 1, 'admin', 1, '<a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum"></a> Feedback', '', 24, '2015-11-30 11:04:15', 0, NULL, NULL),
(13, 1, 46, 0, NULL, 0, 'testing message', 'test it', 0, '2015-12-02 17:16:22', 0, NULL, NULL),
(14, 1, 46, 0, '', 0, 'testing message', 'hello', 0, '2015-12-15 15:40:34', 13, NULL, NULL),
(15, 1, 46, 0, '', 0, 'testing message', 'test', 0, '2015-12-16 19:12:26', 13, NULL, NULL),
(16, 8, 1, 0, NULL, 0, 'Hello this is test message', 'Seamlessly mesh open-source channels with progressive processes. Uniquely simplify B2B value before visionary technologies. ', 0, '2015-12-16 20:00:27', 0, NULL, NULL),
(17, 1, 8, 1, '', 0, 'Hello this is test message', 'Uniquely simplify B2B value before visionary technologies. ', 0, '2015-12-16 20:00:53', 16, NULL, NULL),
(18, 8, 1, 1, '', 0, 'Hello this is test message', ' simplify B2B value before visionary technologies.   simplify B2B value before visionary technologies. ', 0, '2015-12-16 20:01:09', 16, NULL, NULL),
(19, 1, 8, 1, '', 0, 'Hello this is test message', ' simplify B2B value before visionary technologies. ', 0, '2015-12-16 20:01:20', 16, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `message_setting`
--

CREATE TABLE IF NOT EXISTS `message_setting` (
  `message_setting_id` int(11) NOT NULL,
  `email_admin_on_new_message` int(11) NOT NULL DEFAULT '1' COMMENT '0-no,1-yes',
  `email_user_on_new_message` int(11) NOT NULL DEFAULT '1' COMMENT '0-no,1-yes',
  `default_message_subject` varchar(255) DEFAULT NULL,
  `message_enable` int(11) NOT NULL DEFAULT '1' COMMENT '0-no,1-yes'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `message_setting`
--

INSERT INTO `message_setting` (`message_setting_id`, `email_admin_on_new_message`, `email_user_on_new_message`, `default_message_subject`, `message_enable`) VALUES
(1, 1, 1, 'New Message', 1);

-- --------------------------------------------------------

--
-- Table structure for table `meta_setting`
--

CREATE TABLE IF NOT EXISTS `meta_setting` (
  `meta_setting_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `meta_keyword` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `meta_setting`
--

INSERT INTO `meta_setting` (`meta_setting_id`, `title`, `meta_keyword`, `meta_description`) VALUES
(1, 'Real Estate : Your Ultimate Solution to Start Your Own Crowdfunding Platform', 'fundraising, fundraising script, fundraising scripts, fundraising script for sale, fundraising website clone, script like fundraising, site like Crowd funding , Crowd funding  ideas, Crowd funding script, scripts fundraising, unique fund', 'Choose Fundraisingscript.com as your solution to start your own crowdfunding platform. Start your own Real estate, Charity, Equity, Lending or Reward based crowdfunding platform.');

-- --------------------------------------------------------

--
-- Table structure for table `newsletter_job`
--

CREATE TABLE IF NOT EXISTS `newsletter_job` (
  `job_id` int(100) NOT NULL,
  `newsletter_id` int(100) NOT NULL,
  `send_total` int(11) DEFAULT '0',
  `job_date` date DEFAULT NULL,
  `job_start_date` date NOT NULL,
  `newsletter_type` varchar(255) DEFAULT NULL,
  `temp_date` date DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `draft` varchar(255) DEFAULT NULL,
  `sent` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `newsletter_jobs`
--

CREATE TABLE IF NOT EXISTS `newsletter_jobs` (
  `job_id` int(100) NOT NULL,
  `newsletter_id` int(100) NOT NULL,
  `send_total` int(100) NOT NULL,
  `job_date` date NOT NULL,
  `job_start_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `newsletter_report`
--

CREATE TABLE IF NOT EXISTS `newsletter_report` (
  `report_id` int(100) NOT NULL,
  `newsletter_user_id` int(100) NOT NULL,
  `job_id` int(100) NOT NULL,
  `is_fail` int(20) NOT NULL DEFAULT '0',
  `is_open` int(20) NOT NULL DEFAULT '0',
  `send_date` datetime NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `shedule_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `newsletter_setting`
--

CREATE TABLE IF NOT EXISTS `newsletter_setting` (
  `newsletter_setting_id` int(10) NOT NULL,
  `newsletter_from_name` varchar(255) DEFAULT NULL,
  `newsletter_from_address` varchar(255) DEFAULT NULL,
  `newsletter_reply_name` varchar(255) DEFAULT NULL,
  `newsletter_reply_address` varchar(255) DEFAULT NULL,
  `new_subscribe_email` varchar(255) DEFAULT NULL,
  `unsubscribe_email` varchar(255) DEFAULT NULL,
  `new_subscribe_to` varchar(255) DEFAULT NULL,
  `selected_newsletter_id` int(20) NOT NULL,
  `number_of_email_send` int(20) NOT NULL,
  `break_between_email` int(20) NOT NULL,
  `mailer` varchar(50) DEFAULT NULL,
  `sendmail_path` varchar(255) DEFAULT NULL,
  `smtp_port` int(20) DEFAULT NULL,
  `smtp_host` varchar(255) DEFAULT NULL,
  `smtp_email` varchar(255) DEFAULT NULL,
  `smtp_password` varchar(255) DEFAULT NULL,
  `break_type` varchar(50) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `newsletter_setting`
--

INSERT INTO `newsletter_setting` (`newsletter_setting_id`, `newsletter_from_name`, `newsletter_from_address`, `newsletter_reply_name`, `newsletter_reply_address`, `new_subscribe_email`, `unsubscribe_email`, `new_subscribe_to`, `selected_newsletter_id`, `number_of_email_send`, `break_between_email`, `mailer`, `sendmail_path`, `smtp_port`, `smtp_host`, `smtp_email`, `smtp_password`, `break_type`) VALUES
(1, 'Fundraising Script', 'jayshree.rockersinfo@gmail.com', 'Fundraising Script', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'jayshree.rockersinfo@gmail.com', 'all', 27, 30, 15, 'mail', '/usr/sbin/sendmail', 25, 'mail.groupfund.me', 'smtp@groupfund.me', 'smtp2123', 'hours');

-- --------------------------------------------------------

--
-- Table structure for table `newsletter_subscribe`
--

CREATE TABLE IF NOT EXISTS `newsletter_subscribe` (
  `subscribe_id` int(100) NOT NULL,
  `newsletter_user_id` int(100) NOT NULL,
  `newsletter_id` int(100) NOT NULL,
  `subscribe_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `newsletter_template`
--

CREATE TABLE IF NOT EXISTS `newsletter_template` (
  `newsletter_id` int(100) NOT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `template_content` longtext,
  `attach_file` varchar(255) DEFAULT NULL,
  `allow_subscribe_link` int(10) NOT NULL DEFAULT '0',
  `allow_unsubscribe_link` int(10) NOT NULL DEFAULT '0',
  `project_id` int(100) NOT NULL,
  `newsletter_create_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `newsletter_user`
--

CREATE TABLE IF NOT EXISTS `newsletter_user` (
  `newsletter_user_id` int(100) NOT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `user_date` datetime NOT NULL,
  `user_ip` varchar(255) DEFAULT NULL,
  `is_subscribe` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0=''not_subscriber'',1=''subscriber'''
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `newsletter_user`
--

INSERT INTO `newsletter_user` (`newsletter_user_id`, `user_name`, `email`, `user_date`, `user_ip`, `is_subscribe`) VALUES
(1, NULL, 'ghfghf@gmail.co', '2015-10-28 10:36:38', '27.109.7.130', 1),
(2, NULL, 'spreston145@gmail.com', '2015-11-30 12:16:03', '123.201.155.200', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `pages_id` int(11) NOT NULL,
  `parent_id` int(10) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL,
  `pages_title` varchar(255) DEFAULT NULL,
  `description` text,
  `slug` varchar(255) DEFAULT NULL,
  `active` varchar(255) DEFAULT NULL,
  `meta_keyword` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `footer_bar` varchar(20) DEFAULT NULL,
  `header_bar` varchar(20) DEFAULT NULL,
  `left_side` varchar(20) DEFAULT NULL,
  `right_side` varchar(20) DEFAULT NULL,
  `external_link` text
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`pages_id`, `parent_id`, `language_id`, `pages_title`, `description`, `slug`, `active`, `meta_keyword`, `meta_description`, `footer_bar`, `header_bar`, `left_side`, `right_side`, `external_link`) VALUES
(18, 0, 1, 'About Us', '<p>Our goal is to help find a financial solution for the specific needs of the student, and we believe that every student deserves an equal access to education and life luck, they provide the student with the tools and knowledge needed to succeed in life and have more opportunities for their future.&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>We believe that education is the foundation of a great nation, helping a student, you are not only giving them a better opportunity for life, you build a new generation, more humane and sensitive, help them get their tuition, pay their college loan, books and living expenses.&nbsp;</p>\n\n<p>We know for a fact That there are Thousands of educational givers and thesis-have same Concerns and desire of equality of Opportunities, thats why we put all our efforts to connect with the thesis givers students in need, So THAT givers can get to know the student Needs, dreams and aspirations.</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n', 'about-us', '1', 'Fund Me Online, online fundraising, donation website, donations online, accept donations', 'One of the BEST ways to raise money online. Get your online donation website FREE! Invite family & friends to donate online to any of your online fundraising ideas.', 'yes', '0', '0', '0', ''),
(19, 0, 1, 'Terms of Service', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\n\n<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>\n\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\n', 'Terms-of-service', '1', 'Fundraising script Terms and Conditions', 'Fundraising script Terms and Conditions', 'yes', '0', '0', '0', ''),
(20, 0, 1, 'Privacy Policy', '<h4>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h4>\n\n<p>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n\n<h4>The standard chunk of Lorem Ipsum used since the 1500s is reproduced</h4>\n\n<p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from &quot;de Finibus Bonorum et Malorum&quot; by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>\n', 'privacy', '1', 'Fundraising script Privacy Policy', 'Fundraising script Privacy Policy', 'yes', '0', '0', '0', ''),
(21, 0, 1, 'Career', '<p>Lorem Ipsum is simply dummy text of the printing industry and composition.&nbsp;</p>\n\n<p>Lorem Ipsum has been the standard dummy text of the industry since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularized in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&nbsp;</p>\n\n<p>The piece standard Lorem Ipsum used since the 1500s is reproduced true&nbsp;</p>\n\n<p>The standard chunk of Lorem Ipsum used since the 1500s is Reproduced below for Those interested. Sections 1.10.32 and 1.10.33 from &quot;Finibus Bonorum and of Malorum&quot; by Cicero are aussi Reproduced In Their exact original form, Accompanied by English versions from the 1914 translation by H. Rackham.</p>\n', 'career', '1', 'Groupfund.me career', 'Groupfund.me offer best benefits to employee', 'yes', '0', '0', 'yes', ''),
(23, 0, 1, 'Help', '<h4>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h4>\n\n<p>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n', 'help', '1', '', '', 'yes', 'yes', '0', '0', ''),
(24, 0, 1, 'Fees', '<h4>Funding Options.Lesser Fees.</h4>\n\n<p>To create and start a campaign on Indiegogoclone is totally free. All you have to do is register and start a campaign. Choosing from the funding options we have, you may go with the Elastic Funding which lets you keep all of the funds raised from your campaign, even if you don&rsquo;t hit your target. Or, you may choose to go with the inelastic Funding, which refunds the donations back to your Backers should you don&rsquo;t hit your target. Whichever way, you pay nothing till you start raising funds.</p>\n\n<h4>Elastic Fund (Flexible)</h4>\n\n<div class="fees_main">\n<div class="redbg_left">You Hit Your Target</div>\n\n<div class="fees_right">{hit_target_flexible}%</div>\n\n<div class="clear">&nbsp;</div>\n</div>\n\n<div class="fees_main">\n<div class="redbg_left">You Don&acute;t Hit Your Target</div>\n\n<div class="fees_right">\n<p><strong>{hit_nottarget_flexible}% but you get to keep what you earned.</strong></p>\n\n<p>This encourages people to set reasonable goals and promote their campaigns.</p>\n</div>\n\n<div class="clear">&nbsp;</div>\n</div>\n\n<div class="fees_main">\n<div class="redbg_left">Third Party Fee</div>\n\n<div class="fees_right">\n<p>Paypal Charges 2.9% on all transactions. Please check their <a href="http://paypal.com" target="_blank">website</a> for details.</p>\n</div>\n\n<div class="clear">&nbsp;</div>\n</div>\n\n<h4>Inelastic Funding (Fixed)</h4>\n\n<div class="fees_main">\n<div class="redbg_left">You Hit Your Target</div>\n\n<div class="fees_right">\n<p>{hit_target_fixed}%</p>\n</div>\n\n<div class="clear">&nbsp;</div>\n</div>\n\n<div class="fees_main">\n<div class="redbg_left">You Don&acute;t Hit Your Target</div>\n\n<div class="fees_right">\n<p><strong>0% and your contributions get refunded.</strong></p>\n\n<p>Third-party fees do not apply for refunded contributions.</p>\n</div>\n\n<div class="clear">&nbsp;</div>\n</div>\n\n<div class="fees_main">\n<div class="redbg_left">Third Party Fee</div>\n\n<div class="fees_right">\n<p>Paypal Charges 2.9% on all transactions. Please check their <a href="http://paypal.com" target="_blank">website</a> for details.</p>\n</div>\n\n<div class="clear">&nbsp;</div>\n</div>\n', 'fees', '1', 'fees-test', 'fees-test', 'yes', '0', '0', '0', ''),
(25, 0, 1, 'SideBar Test', '<p>sdfsdf</p>\n', 'sdf', '1', 'asd', 'asd', 'yes', '0', '0', 'yes', ''),
(27, 0, 1, 'Get Help', '<h2>We Want to Make You Happy</h2>\n\n<p><strong>We want your experience on website to be a good one.</strong>&nbsp;We&#39;re here to help you fund your campaign or contribute to one that sparks your interest. We&#39;ll support you in the process, so don&#39;t hesitate to contact us or visit our Help Center for answers to your questions.</p>\n\n<p>Search our&nbsp;<a href="javascript://" style="color: rgb(255, 0, 81); text-decoration: none;">Help Center</a>&nbsp;for information, advice and ideas. Ask us a question or tell us how we&#39;re doing. Your happiness is important to us.</p>\n', 'crowdfunding-campaign-basics', '1', 'crowdfunding', 'crowdfunding', 'yes', '0', '0', 'yes', ''),
(28, 0, 2, 'à propos de nous', '<p>Notre objectif est d&#39;aider &agrave; trouver une solution financi&egrave;re pour les besoins sp&eacute;cifiques de l&#39;&eacute;l&egrave;ve, et nous croyons que chaque &eacute;l&egrave;ve m&eacute;rite un acc&egrave;s &eacute;gal &agrave; l&#39;&eacute;ducation et &agrave; la vie de chance, ils fournir &agrave; l&#39;&eacute;tudiant les outils et les connaissances n&eacute;cessaires pour r&eacute;ussir dans la vie et avoir plus possibilit&eacute;s pour leur avenir.&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>Nous croyons que l&#39;&eacute;ducation est le fondement d&#39;une grande nation, d&#39;aider un &eacute;tudiant, vous ne sont pas seulement de leur donner une meilleure chance pour la vie, vous construisez une nouvelle g&eacute;n&eacute;ration, plus humaine et plus sensible, les aidons &agrave; leurs frais de scolarit&eacute;, payer leur pr&ecirc;t d&#39;&eacute;tudes coll&eacute;giales, des livres et des frais de subsistance.&nbsp;</p>\n\n<p>Nous savons pour un fait qu&#39;il ya des milliers de donneurs d&#39;enseignement et th&egrave;se ont les m&ecirc;mes pr&eacute;occupations et le d&eacute;sir de l&#39;&eacute;galit&eacute; des chances, c&#39;est pourquoi nous mettons tous nos efforts &agrave; se connecter avec les dispensateurs de th&egrave;se des &eacute;tudiants dans le besoin, de sorte que donneurs peuvent conna&icirc;tre le besoins des &eacute;tudiants, des r&ecirc;ves et aspirations.</p>\n', 'about_us_french', '1', 'Financer moi en ligne, collecte de fonds en ligne, site de dons, des dons en ligne, accepter des dons', 'Une des meilleures façons de recueillir des fonds en ligne. Obtenez votre site de dons en ligne GRATUIT! Invitez la famille et les amis de faire un don en ligne à toutes vos idées de collecte de fonds en ligne.', 'yes', NULL, NULL, '0', ''),
(29, 0, 2, 'carrière', '<p>Lorem Ipsum est simplement faux texte de l&#39;industrie de l&#39;imprimerie et de la composition.&nbsp;</p>\n\n<p>Lorem Ipsum est le faux texte standard de l&#39;industrie depuis les ann&eacute;es 1500, quand une imprimante inconnu a pris une cuisine de type et il brouill&eacute;s pour faire un livre sp&eacute;cimen type. Il a non seulement surv&eacute;cu &agrave; cinq si&egrave;cles, mais aussi le saut dans la composition &eacute;lectronique, reste essentiellement inchang&eacute;. Il a &eacute;t&eacute; popularis&eacute; dans les ann&eacute;es 1960 avec la sortie des feuilles Letraset contenant des passages Lorem Ipsum, et plus r&eacute;cemment avec le logiciel de PAO comme PageMaker Aldus y compris les versions de Lorem Ipsum.&nbsp;</p>\n\n<p>La norme de pi&egrave;ce Lorem Ipsum utilis&eacute; depuis les ann&eacute;es 1500 est reproduit vrai&nbsp;</p>\n\n<p>Le morceau norme de Lorem Ipsum utilis&eacute; depuis les ann&eacute;es 1500 est reproduit ci-dessous personnes int&eacute;ress&eacute;es. Les articles 1.10.32 et 1.10.33 de &quot;Finibus Bonorum et de Malorum&quot; par Cic&eacute;ron sont also reproduits dans leur forme originale exacte, accompagn&eacute; par les versions anglaises de la traduction 1914 par H. Rackham.</p>\n', 'career_french', '1', 'Groupfund.me carrière', 'Groupfund.me offrir les meilleurs avantages aux employés', 'yes', NULL, NULL, 'yes', ''),
(30, 0, 2, 'honoraires', '<p>Frais de financement Options.Lesser.&nbsp;</p>\n\n<p>Pour cr&eacute;er et lancer une campagne sur Indiegogoclone est totalement gratuit. Tout ce que vous avez &agrave; faire est de vous inscrire et de commencer une campagne. Choisissant parmi les options de financement que nous avons, vous pouvez aller avec le financement &eacute;lastique qui vous permet de garder tous les fonds recueillis &agrave; partir de votre campagne, m&ecirc;me si vous ne touchez pas votre cible. Ou, vous pouvez choisir d&#39;aller avec le financement in&eacute;lastique, qui rembourse les dons retourner &agrave; Backers devraient vous n&#39;avez pas atteint votre cible. Quelle que soit la fa&ccedil;on, vous ne payez rien jusqu&#39;&agrave; ce que vous commencez &agrave; collecter des fonds.&nbsp;</p>\n\n<p>Fonds &eacute;lastique (flexible)&nbsp;</p>\n\n<p>Vous frappez votre cible&nbsp;<br />\n{%} hit_target_flexible&nbsp;</p>\n\n<p>Vous ne touchez pas votre cible&nbsp;<br />\n{} hit_nottarget_flexible% mais vous arrivez &agrave; garder ce que vous avez gagn&eacute;.&nbsp;</p>\n\n<p>Cela encourage les gens &agrave; se fixer des objectifs raisonnables et de promouvoir leurs campagnes.&nbsp;</p>\n\n<p><br />\nFrais de tiers&nbsp;<br />\nPaypal Frais de 2,9% sur toutes les transactions. S&#39;il vous pla&icirc;t consulter leur site web pour plus de d&eacute;tails.&nbsp;</p>\n\n<p><br />\nIn&eacute;lastique financement (fixe)&nbsp;</p>\n\n<p>Vous frappez votre cible&nbsp;<br />\n{%} hit_target_fixed&nbsp;</p>\n\n<p><br />\nVous ne touchez pas votre cible&nbsp;<br />\n0% et vos contributions obtenir le remboursement.&nbsp;</p>\n\n<p>Les frais de tiers ne s&#39;appliquent pas aux cotisations rembours&eacute;es.&nbsp;</p>\n\n<p><br />\nFrais de tiers&nbsp;<br />\nPaypal Frais de 2,9% sur toutes les transactions. S&#39;il vous pla&icirc;t consulter leur site web pour plus de d&eacute;tails.</p>\n', 'fees_french', '1', 'frais essai', 'frais essai', 'yes', NULL, NULL, '0', ''),
(31, 0, 2, 'obtenir de l''aide', '<p>Nous voulons vous rendre heureux&nbsp;</p>\n\n<p>Nous voulons que votre exp&eacute;rience sur le site pour &ecirc;tre une bonne chose. Nous sommes l&agrave; pour vous aider &agrave; financer votre campagne ou contribuer &agrave; celui qui aura &eacute;veill&eacute; votre curiosit&eacute;. Nous vous accompagnons dans le processus, alors n&#39;h&eacute;sitez pas &agrave; nous contacter ou &agrave; visiter notre Centre d&#39;aide pour obtenir des r&eacute;ponses &agrave; vos questions.&nbsp;</p>\n\n<p>Rechercher notre Centre d&#39;aide pour obtenir des informations, des conseils et des id&eacute;es. Nous poser une question ou nous dire comment nous faisons. Votre bonheur est importante pour nous.</p>\n', 'crowdfunding-campaign-basics_french', '1', 'crowdfunding', 'crowdfunding', 'yes', NULL, NULL, 'yes', ''),
(32, 0, 2, 'aider', '<p>Lorem Ipsum est simplement faux texte de l&#39;industrie de l&#39;imprimerie et de la composition.&nbsp;</p>\n\n<p>Lorem Ipsum est le faux texte standard de l&#39;industrie depuis les ann&eacute;es 1500, quand une imprimante inconnu a pris une cuisine de type et il brouill&eacute;s pour faire un livre sp&eacute;cimen type. Il a non seulement surv&eacute;cu &agrave; cinq si&egrave;cles, mais aussi le saut dans la composition &eacute;lectronique, reste essentiellement inchang&eacute;. Il a &eacute;t&eacute; popularis&eacute; dans les ann&eacute;es 1960 avec la sortie des feuilles Letraset contenant des passages Lorem Ipsum, et plus r&eacute;cemment avec le logiciel de PAO comme PageMaker Aldus y compris les versions de Lorem Ipsum.</p>\n', 'help_french', '1', '', '', 'yes', NULL, NULL, '0', ''),
(33, 0, 1, 'Press', '<p>Lorem Ipsum est simplement faux texte de l&#39;industrie de l&#39;imprimerie et de la composition.&nbsp;</p>\n\n<p>Lorem Ipsum est le faux texte standard de l&#39;industrie depuis les ann&eacute;es 1500, quand une imprimante inconnu a pris une cuisine de type et il brouill&eacute;s pour faire un livre sp&eacute;cimen type. Il a non seulement surv&eacute;cu &agrave; cinq si&egrave;cles, mais aussi le saut dans la composition &eacute;lectronique, reste essentiellement inchang&eacute;. Il a &eacute;t&eacute; popularis&eacute; dans les ann&eacute;es 1960 avec la sortie des feuilles Letraset contenant des passages Lorem Ipsum, et plus r&eacute;cemment avec le logiciel de PAO comme PageMaker Aldus y compris les versions de Lorem Ipsum.&nbsp;</p>\n\n<p>Le morceau norme de Lorem Ipsum utilis&eacute; depuis les ann&eacute;es 1500 est reproduit&nbsp;</p>\n\n<p>Le morceau norme de Lorem Ipsum utilis&eacute; depuis les ann&eacute;es 1500 est reproduit ci-dessous pour ceux qui sont int&eacute;ress&eacute;s. Les articles 1.10.32 et 1.10.33 de &quot;de Finibus Bonorum et Malorum&quot; par Cic&eacute;ron sont &eacute;galement reproduites dans leur forme originale exacte, accompagn&eacute; par les versions anglaises de la traduction 1914 par H. Rackham.</p>\n', 'press-english', '1', '', '', 'yes', NULL, NULL, '0', ''),
(34, 0, 2, 'Politique de confidentialité', '<p>Lorem Ipsum is simply dummy text of the printing industry and composition.&nbsp;</p>\n\n<p>Lorem Ipsum has been the standard dummy text of the industry since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularized in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&nbsp;</p>\n\n<p>The piece standard Lorem Ipsum used since the 1500s is reproduced&nbsp;</p>\n\n<p>The piece standard Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from &quot;Finibus Bonorum and of Malorum&quot; by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>\n', 'privacy-french', '1', 'Script de collecte de fonds Politique de confidentialité', 'Script de collecte de fonds Politique de confidentialité', 'yes', NULL, NULL, '0', ''),
(36, 0, 2, 'Conditions d''utilisation', '<p>Indiegogoclone&nbsp;<br />\nConditions d&#39;utilisation (&laquo;Accord&raquo;)&nbsp;<br />\nCet accord a &eacute;t&eacute; cr&eacute;&eacute; le 10 Septembre 2013.&nbsp;<br />\nBienvenue &agrave; Indiegogoclone, le site et le service en ligne de Indiegogoclone. En acc&eacute;dant ou en utilisant notre site Web, services et Indiegogoclone, contenu fournis par ou en relation avec les services fournis par notre site Web (collectivement, &laquo;Service&raquo;), vous, en tant qu&#39;utilisateur ou membres, indiquez que vous avez lu, compris et accept&eacute; d&#39;&ecirc;tre li&eacute; par ces conditions d&#39;utilisation (&quot;Accord&quot;), la politique de confidentialit&eacute; affich&eacute;e &agrave; Indiegogoclone avec toutes les autres r&egrave;gles de fonctionnement, les politiques et les proc&eacute;dures et les modalit&eacute;s et conditions suppl&eacute;mentaires publi&eacute;es sur notre site Web (qui sont incorpor&eacute;s par r&eacute;f&eacute;rence), et vous &ecirc;tes &acirc;g&eacute; d&#39;au moins 13 ans. Les termes sont d&eacute;finis dans le pr&eacute;sent Accord.&nbsp;</p>\n\n<p>Nous nous r&eacute;servons le droit de modifier le pr&eacute;sent Accord &agrave; tout moment et sans pr&eacute;avis. Si nous faisons cela, nous afficherons l&#39;Accord modifi&eacute; sur cette page et indiquer en haut de la page la date de l&#39;accord a &eacute;t&eacute; r&eacute;vis&eacute;e pour la derni&egrave;re. Votre utilisation continue du Service apr&egrave;s de telles modifications constitue votre acceptation de la nouvelle convention. Si vous n&#39;acceptez pas toutes du pr&eacute;sent Accord ou de toute modification au pr&eacute;sent Accord, ne pas utiliser ou d&#39;acc&egrave;s (ou de continuer &agrave; l&#39;acc&egrave;s) Service ou cesser imm&eacute;diatement toute utilisation du Service.&nbsp;<br />\nQui sommes-nous?&nbsp;<br />\nIndiegogoclone est une plate-forme de crowdfunding mondiale fond&eacute;e don-pour l&#39;agriculture, l&#39;autonomisation des agriculteurs et des &eacute;leveurs pour obtenir un financement non conventionnel pour les besoins classiques. Depuis le lancement en Septembre 2013, a permis Indiegogoclone projet Propri&eacute;taires de lancer des projets dans le monde entier. Indiegogoclone est en train de changer la fa&ccedil;on dont les gens amassent des fonds pour l&#39;agriculture - cr&eacute;atives, entrepreneuriales ou projets li&eacute;s &agrave; causer.&nbsp;<br />\nNous sommes une plate-forme ouverte. Vous comprenez et reconnaissez que vous pouvez &ecirc;tre expos&eacute; &agrave; un Contenu de membre qui peuvent &ecirc;tre inexacts, offensant, ind&eacute;cent ou choquant. Contenu de membres ne doit pas &ecirc;tre consid&eacute;r&eacute;e comme &eacute;tant une approbation ou une opinion partag&eacute;e par Indiegogoclone. Vous acceptez que Indiegogoclone n&#39;est pas responsable des dommages ou pertes r&eacute;sultant de ce Contenu.&nbsp;<br />\nLes utilisateurs de notre service et des membres</p>\n', 'Terms_of_service_french', '1', 'Script de collecte de fonds Termes et Conditions', 'Script de collecte de fonds Termes et Conditions', 'yes', NULL, NULL, '0', ''),
(37, 0, 1, 'Test page', '<p>Test pageTest pageTest pageTest pageTest pageTest pageTest pageTest pageTest pageTest pageTest pageTest pageTest pageTest pageTest pageTest pageTest pageTest pageTest pageTest pageTest pageTest pageTest page</p>\n', 'page', '1', '', '', 'yes', NULL, NULL, 'yes', ''),
(38, 0, 1, 'Test page', '<p>sfadfdsfaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaasdfasdfasdfasfasdfasdfasfddfasdfasfasdfasfasdfasdfaaaaa</p>\n', 'page', '1', '', '', 'yes', NULL, NULL, 'yes', ''),
(39, 0, 1, 'Test page', '<p>ggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg</p>\n\n<h4>Funding Options.Lesser Fees.</h4>\n\n<p>To create and start a campaign on Indiegogoclone is totally free. All you have to do is register and start a campaign. Choosing from the funding options we have, you may go with the Elastic Funding which lets you keep all of the funds raised from your campaign, even if you don&rsquo;t hit your target. Or, you may choose to go with the inelastic Funding, which refunds the donations back to your Backers should you don&rsquo;t hit your target. Whichever way, you pay nothing till you start raising funds.</p>\n\n<h4>Funding Options.Lesser Fees.</h4>\n\n<p>To create and start a campaign on Indiegogoclone is totally free. All you have to do is register and start a campaign. Choosing from the funding options we have, you may go with the Elastic Funding which lets you keep all of the funds raised from your campaign, even if you don&rsquo;t hit your target. Or, you may choose to go with the inelastic Funding, which refunds the donations back to your Backers should you don&rsquo;t hit your target. Whichever way, you pay nothing till you start raising funds.</p>\n\n<h4>Funding Options.Lesser Fees.</h4>\n\n<p>To create and start a campaign on Indiegogoclone is totally free. All you have to do is register and start a campaign. Choosing from the funding options we have, you may go with the Elastic Funding which lets you keep all of the funds raised from your campaign, even if you don&rsquo;t hit your target. Or, you may choose to go with the inelastic Funding, which refunds the donations back to your Backers should you don&rsquo;t hit your target. Whichever way, you pay nothing till you start raising funds.</p>\n\n<h4>Funding Options.Lesser Fees.</h4>\n\n<p>To create and start a campaign on Indiegogoclone is totally free. All you have to do is register and start a campaign. Choosing from the funding options we have, you may go with the Elastic Funding which lets you keep all of the funds raised from your campaign, even if you don&rsquo;t hit your target. Or, you may choose to go with the inelastic Funding, which refunds the donations back to your Backers should you don&rsquo;t hit your target. Whichever way, you pay nothing till you start raising funds.</p>\n\n<h4>Funding Options.Lesser Fees.</h4>\n\n<p>To create and start a campaign on Indiegogoclone is totally free. All you have to do is register and start a campaign. Choosing from the funding options we have, you may go with the Elastic Funding which lets you keep all of the funds raised from your campaign, even if you don&rsquo;t hit your target. Or, you may choose to go with the inelastic Funding, which refunds the donations back to your Backers should you don&rsquo;t hit your target. Whichever way, you pay nothing till you start raising funds.</p>\n\n<h4>Funding Options.Lesser Fees.</h4>\n\n<p>To create and start a campaign on Indiegogoclone is totally free. All you have to do is register and start a campaign. Choosing from the funding options we have, you may go with the Elastic Funding which lets you keep all of the funds raised from your campaign, even if you don&rsquo;t hit your target. Or, you may choose to go with the inelastic Funding, which refunds the donations back to your Backers should you don&rsquo;t hit your target. Whichever way, you pay nothing till you start raising funds.</p>\n\n<h4>&nbsp;</h4>\n', 'page', '1', '', '', '0', NULL, NULL, '0', ''),
(42, 0, 1, 'Project Guidelines', '<p style="text-align:justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n\n<ul>\n	<li>Nulla et odio tristique, hendrerit lacus a, imperdiet est.</li>\n	<li>Nullam rutrum ante lobortis pellentesque interdum.</li>\n	<li>Morbi lobortis velit sed turpis venenatis semper.</li>\n	<li>Curabitur nec ligula ac sem faucibus placerat eget non quam.</li>\n</ul>\n', 'project-guidelines', '1', '', '', '0', NULL, NULL, '0', ''),
(44, 0, 1, 'Bank Terms And Condition', '<p style="text-align:justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n\n<p style="text-align:justify">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\n\n<p style="text-align:justify">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>\n\n<p style="text-align:justify">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n\n<p style="text-align:justify">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\n', 'bank-terms', '1', '', '', '0', NULL, NULL, '0', '');

-- --------------------------------------------------------

--
-- Table structure for table `perk`
--

CREATE TABLE IF NOT EXISTS `perk` (
  `perk_id` int(11) NOT NULL,
  `perk_title` varchar(255) DEFAULT NULL,
  `perk_description` text,
  `perk_amount` double(10,2) DEFAULT NULL,
  `perk_total` varchar(255) DEFAULT NULL,
  `perk_get` varchar(255) DEFAULT NULL,
  `coupon_id` int(100) DEFAULT NULL,
  `coupon_status` varchar(20) DEFAULT NULL,
  `estdate` datetime NOT NULL,
  `shipping_status` varchar(255) DEFAULT NULL,
  `equity_id` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `perk`
--

INSERT INTO `perk` (`perk_id`, `perk_title`, `perk_description`, `perk_amount`, `perk_total`, `perk_get`, `coupon_id`, `coupon_status`, `estdate`, `shipping_status`, `equity_id`) VALUES
(1, 'Goods send', 'Goods sendGoods sendGoods sendGoods sendGoods sendGoods sendGoods send', 120.00, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, 2),
(3, '123', '', 123.00, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, 5),
(4, 'Your Perks', 'Your Perks - https://www.youtube.com/watch?v=VxwsBSoenqs', 1000.00, NULL, '1', NULL, NULL, '0000-00-00 00:00:00', NULL, 24);

-- --------------------------------------------------------

--
-- Table structure for table `phases`
--

CREATE TABLE IF NOT EXISTS `phases` (
  `id` int(11) NOT NULL,
  `is_parent` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `phases`
--

INSERT INTO `phases` (`id`, `is_parent`, `name`, `status`, `created_at`) VALUES
(1, 1, 'value add', 1, '2015-10-01 01:45:27'),
(2, 0, '70% aquired', 1, '2015-10-01 01:46:03');

-- --------------------------------------------------------

--
-- Table structure for table `previous_funding`
--

CREATE TABLE IF NOT EXISTS `previous_funding` (
  `previous_funding_id` int(11) NOT NULL,
  `equity_id` int(11) NOT NULL,
  `funding_source` varchar(255) DEFAULT NULL,
  `funding_type` varchar(255) DEFAULT NULL,
  `funding_amount` int(11) NOT NULL,
  `funding_date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `previous_funding`
--

INSERT INTO `previous_funding` (`previous_funding_id`, `equity_id`, `funding_source`, `funding_type`, `funding_amount`, `funding_date`) VALUES
(1, 2, 'Self', 'Equity', 545454, '2015-10-01 00:00:00'),
(2, 8, 'Bank', 'Equity', 100, '2015-10-23 00:00:00'),
(3, 24, 'Self', 'Equity', 1000, '2015-11-26 00:00:00'),
(4, 26, 'Bank', 'Equity', 10000, '2015-12-02 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `rights`
--

CREATE TABLE IF NOT EXISTS `rights` (
  `rights_id` int(100) NOT NULL,
  `rights_name` varchar(255) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=73 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rights`
--

INSERT INTO `rights` (`rights_id`, `rights_name`) VALUES
(1, 'list_admin'),
(2, 'admin_login'),
(3, 'list_user'),
(4, 'user_login'),
(5, 'manage_dropdown'),
(6, 'list_equity'),
(12, 'list_transaction'),
(14, 'list_pages'),
(15, 'list_country'),
(18, 'add_site_setting'),
(19, 'add_meta_setting'),
(20, 'add_facebook_setting'),
(21, 'add_twitter_setting'),
(22, 'add_email_setting'),
(23, 'add_email_template'),
(24, 'assign_rights'),
(25, 'add_spam_setting'),
(26, 'spam_report'),
(27, 'spamer'),
(28, 'list_newsletter'),
(29, 'list_newsletter_user'),
(30, 'newsletter_setting'),
(31, 'newsletter_job'),
(35, 'list_faq'),
(66, 'add_banner_slider'),
(69, 'accreditation_user_list'),
(68, 'add_other_setting'),
(47, 'add_image_setting'),
(50, 'add_amount_setting'),
(51, 'list_cronjob'),
(67, 'list_deal_type_setting'),
(54, 'add_google_setting'),
(71, 'list_activity'),
(70, 'list_message'),
(65, 'add_taxonomy'),
(60, 'add_youtube_setting'),
(61, 'add_google_plus_setting'),
(62, 'add_linkdin_setting'),
(64, 'list_currency'),
(72, 'add_amount_setting');

-- --------------------------------------------------------

--
-- Table structure for table `rights_assign`
--

CREATE TABLE IF NOT EXISTS `rights_assign` (
  `assign_id` int(100) NOT NULL,
  `admin_id` int(100) NOT NULL,
  `rights_id` int(100) NOT NULL,
  `rights_set` int(10) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=1047 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rights_assign`
--

INSERT INTO `rights_assign` (`assign_id`, `admin_id`, `rights_id`, `rights_set`) VALUES
(1, 1, 1, 1),
(2, 1, 2, 1),
(3, 1, 3, 1),
(4, 1, 4, 1),
(5, 1, 5, 1),
(6, 1, 6, 1),
(7, 1, 7, 1),
(8, 1, 8, 1),
(9, 1, 9, 1),
(10, 1, 10, 1),
(11, 1, 11, 1),
(12, 1, 12, 1),
(13, 1, 13, 1),
(14, 1, 14, 1),
(15, 1, 15, 1),
(16, 1, 16, 1),
(17, 1, 17, 1),
(18, 1, 18, 1),
(19, 1, 19, 1),
(20, 1, 20, 1),
(21, 1, 21, 1),
(22, 1, 22, 1),
(23, 1, 23, 1),
(24, 1, 24, 1),
(25, 1, 25, 1),
(26, 1, 26, 1),
(27, 1, 27, 1),
(28, 1, 28, 1),
(29, 1, 29, 1),
(30, 1, 30, 1),
(31, 1, 31, 1),
(33, 1, 34, 1),
(34, 1, 35, 1),
(35, 1, 36, 1),
(36, 1, 37, 1),
(37, 1, 38, 1),
(38, 1, 39, 1),
(39, 1, 40, 1),
(40, 1, 41, 1),
(41, 1, 42, 1),
(42, 1, 47, 1),
(43, 1, 48, 1),
(44, 1, 49, 1),
(45, 1, 50, 1),
(46, 1, 51, 1),
(47, 1, 52, 1),
(48, 1, 53, 1),
(49, 1, 54, 1),
(50, 1, 55, 1),
(51, 1, 56, 1),
(52, 1, 57, 1),
(53, 1, 58, 1),
(835, 39, 62, 1),
(834, 39, 61, 1),
(833, 39, 60, 1),
(832, 39, 59, 1),
(831, 39, 58, 1),
(830, 39, 57, 1),
(829, 39, 56, 1),
(828, 39, 55, 1),
(827, 39, 54, 1),
(826, 39, 53, 1),
(825, 39, 52, 1),
(824, 39, 51, 1),
(823, 39, 50, 1),
(822, 39, 49, 1),
(821, 39, 48, 1),
(820, 39, 47, 1),
(819, 39, 42, 1),
(818, 39, 41, 1),
(817, 39, 40, 1),
(816, 39, 39, 1),
(815, 39, 38, 1),
(814, 39, 37, 1),
(813, 39, 36, 1),
(812, 39, 35, 1),
(811, 39, 34, 1),
(810, 39, 31, 1),
(809, 39, 30, 1),
(808, 39, 29, 1),
(807, 39, 28, 1),
(806, 39, 27, 1),
(805, 39, 26, 1),
(804, 39, 25, 1),
(803, 39, 24, 1),
(802, 39, 23, 1),
(801, 39, 22, 1),
(800, 39, 21, 1),
(799, 39, 20, 1),
(798, 39, 19, 1),
(797, 39, 18, 1),
(796, 39, 17, 1),
(795, 39, 16, 1),
(794, 39, 15, 1),
(793, 39, 14, 1),
(792, 39, 13, 1),
(791, 39, 12, 1),
(790, 39, 11, 1),
(789, 39, 10, 1),
(788, 39, 9, 1),
(217, 8, 62, 1),
(216, 8, 61, 1),
(215, 8, 60, 1),
(214, 8, 59, 1),
(106, 8, 1, 1),
(107, 8, 2, 1),
(108, 8, 3, 1),
(109, 8, 4, 1),
(110, 8, 5, 1),
(111, 8, 6, 1),
(112, 8, 7, 1),
(113, 8, 8, 1),
(114, 8, 9, 1),
(115, 8, 10, 1),
(116, 8, 11, 1),
(117, 8, 12, 1),
(118, 8, 13, 1),
(119, 8, 14, 1),
(120, 8, 15, 1),
(121, 8, 16, 1),
(122, 8, 17, 1),
(123, 8, 18, 1),
(124, 8, 19, 1),
(125, 8, 20, 1),
(126, 8, 21, 1),
(127, 8, 22, 1),
(128, 8, 23, 1),
(129, 8, 24, 1),
(130, 8, 25, 1),
(131, 8, 26, 1),
(132, 8, 27, 1),
(133, 8, 28, 1),
(134, 8, 29, 1),
(135, 8, 30, 1),
(136, 8, 31, 1),
(137, 8, 34, 1),
(138, 8, 35, 1),
(139, 8, 36, 1),
(140, 8, 37, 1),
(141, 8, 38, 1),
(142, 8, 39, 1),
(143, 8, 40, 1),
(144, 8, 41, 1),
(145, 8, 42, 1),
(146, 8, 47, 1),
(147, 8, 48, 1),
(148, 8, 49, 1),
(149, 8, 50, 1),
(150, 8, 51, 1),
(151, 8, 52, 1),
(152, 8, 53, 1),
(153, 8, 54, 1),
(154, 8, 55, 1),
(155, 8, 56, 1),
(156, 8, 57, 1),
(157, 8, 58, 1),
(787, 39, 8, 1),
(786, 39, 7, 1),
(785, 39, 6, 1),
(784, 39, 5, 1),
(783, 39, 4, 1),
(782, 39, 3, 1),
(781, 39, 2, 1),
(780, 39, 1, 1),
(210, 1, 59, 1),
(211, 1, 60, 1),
(212, 1, 61, 1),
(213, 1, 62, 1),
(889, 40, 59, 0),
(888, 40, 58, 0),
(887, 40, 57, 0),
(886, 40, 56, 0),
(885, 40, 55, 0),
(884, 40, 54, 0),
(883, 40, 53, 0),
(882, 40, 52, 0),
(881, 40, 51, 0),
(880, 40, 50, 0),
(879, 40, 49, 0),
(878, 40, 48, 0),
(877, 40, 47, 0),
(876, 40, 42, 0),
(875, 40, 41, 0),
(874, 40, 40, 0),
(873, 40, 39, 0),
(872, 40, 38, 0),
(871, 40, 37, 0),
(870, 40, 36, 0),
(869, 40, 35, 0),
(868, 40, 34, 0),
(867, 40, 31, 0),
(866, 40, 30, 0),
(865, 40, 29, 0),
(864, 40, 28, 0),
(863, 40, 27, 0),
(862, 40, 26, 0),
(861, 40, 25, 0),
(860, 40, 24, 0),
(859, 40, 23, 0),
(858, 40, 22, 0),
(857, 40, 21, 0),
(856, 40, 20, 0),
(855, 40, 19, 0),
(854, 40, 18, 0),
(853, 40, 17, 0),
(852, 40, 16, 0),
(851, 40, 15, 0),
(850, 40, 14, 0),
(849, 40, 13, 0),
(848, 40, 12, 1),
(847, 40, 11, 0),
(846, 40, 10, 0),
(845, 40, 9, 0),
(844, 40, 8, 0),
(843, 40, 7, 0),
(842, 40, 6, 0),
(841, 40, 5, 0),
(840, 40, 4, 0),
(839, 40, 3, 0),
(838, 40, 2, 0),
(837, 40, 1, 0),
(836, 39, 63, 1),
(779, 37, 63, 1),
(778, 37, 62, 1),
(777, 37, 61, 1),
(776, 37, 60, 1),
(775, 37, 59, 1),
(774, 37, 58, 1),
(773, 37, 57, 1),
(772, 37, 56, 1),
(771, 37, 55, 1),
(770, 37, 54, 1),
(769, 37, 53, 1),
(768, 37, 52, 1),
(767, 37, 51, 1),
(766, 37, 50, 1),
(765, 37, 49, 1),
(764, 37, 48, 1),
(763, 37, 47, 1),
(762, 37, 42, 1),
(761, 37, 41, 1),
(760, 37, 40, 1),
(759, 37, 39, 1),
(758, 37, 38, 1),
(757, 37, 37, 1),
(756, 37, 36, 1),
(755, 37, 35, 1),
(754, 37, 34, 1),
(753, 37, 31, 1),
(752, 37, 30, 1),
(751, 37, 29, 1),
(750, 37, 28, 1),
(749, 37, 27, 1),
(748, 37, 26, 1),
(747, 37, 25, 1),
(746, 37, 24, 1),
(745, 37, 23, 1),
(744, 37, 22, 1),
(743, 37, 21, 1),
(742, 37, 20, 1),
(741, 37, 19, 1),
(740, 37, 18, 1),
(739, 37, 17, 1),
(738, 37, 16, 1),
(737, 37, 15, 1),
(736, 37, 14, 1),
(735, 37, 13, 1),
(734, 37, 12, 1),
(733, 37, 11, 1),
(732, 37, 10, 1),
(731, 37, 9, 1),
(730, 37, 8, 1),
(729, 37, 7, 1),
(728, 37, 6, 1),
(727, 37, 5, 1),
(726, 37, 4, 1),
(725, 37, 3, 1),
(724, 37, 2, 1),
(723, 37, 1, 1),
(722, 1, 63, 1),
(890, 40, 60, 0),
(891, 40, 61, 0),
(892, 40, 62, 0),
(893, 40, 63, 0),
(951, 1, 64, 1),
(952, 1, 65, 1),
(953, 1, 66, 1),
(954, 1, 67, 1),
(955, 1, 68, 1),
(956, 1, 69, 1),
(1044, 57, 71, 0),
(1043, 57, 70, 0),
(1042, 57, 69, 0),
(1041, 57, 68, 0),
(1040, 57, 67, 0),
(1039, 57, 66, 0),
(1038, 57, 65, 0),
(1037, 57, 64, 0),
(1036, 57, 62, 0),
(1035, 57, 61, 0),
(1034, 57, 60, 0),
(1033, 57, 54, 0),
(1032, 57, 51, 0),
(1031, 57, 50, 0),
(1030, 57, 47, 0),
(1029, 57, 35, 0),
(1028, 57, 31, 0),
(1027, 57, 30, 0),
(1026, 57, 29, 0),
(1025, 57, 28, 0),
(1024, 57, 27, 0),
(1023, 57, 26, 0),
(1022, 57, 25, 0),
(1021, 57, 24, 0),
(1020, 57, 23, 0),
(1019, 57, 22, 0),
(1018, 57, 21, 0),
(1017, 57, 20, 0),
(1016, 57, 19, 0),
(1015, 57, 18, 0),
(1014, 57, 15, 0),
(1013, 57, 14, 1),
(1012, 57, 12, 1),
(1011, 57, 6, 1),
(1010, 57, 5, 1),
(1009, 57, 4, 1),
(1008, 57, 3, 1),
(1007, 57, 2, 1),
(1006, 57, 1, 1),
(1004, 1, 70, 1),
(1005, 1, 71, 1),
(1045, 1, 65, 1),
(1046, 1, 64, 1);

-- --------------------------------------------------------

--
-- Table structure for table `school`
--

CREATE TABLE IF NOT EXISTS `school` (
  `school_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `school_url_title` varchar(255) DEFAULT NULL,
  `description` text,
  `active` varchar(20) DEFAULT NULL,
  `school_order` int(10) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `school`
--

INSERT INTO `school` (`school_id`, `title`, `school_url_title`, `description`, `active`, `school_order`) VALUES
(1, 'Defining Your Project', 'defining-your-project', '<p>Whether it&rsquo;s a book, a film, or a piece of hardware, the one trait that every FundraisingScript campaign shares is that it is a&nbsp;<em>project.</em>&nbsp;Defining what your FundraisingScript project&nbsp;<em>is</em>&nbsp;is the first step for every creator.</p>\n<p>What are you raising funds to do? Having a focused and well-defined project with a clear beginning and end is vital. For example: recording a new album is a finite project &mdash; the project finishes when the band releases the album &mdash; but launching a music career is not. There is no end, just an ongoing effort. FundraisingScript is open only to finite projects.</p>\n<p>With a precisely defined goal, expectations are transparent for both the creator and potential backers. Backers can judge how realistic the project&rsquo;s goals are, as well as the project creator&rsquo;s ability to complete them. And for creators, the practice of defining a project&rsquo;s goal establishes the scope of the endeavor, often an important step in the creative process.</p>\n<p>FundraisingScript thrives on these open exchanges and clear explanations of goals. Make sure your project does this!</p>\n<p><strong>SENDING US YOUR IDEA</strong></p>\n<p>Before launching a project, we ask people to share their idea with us to make sure it fits our&nbsp;<a href=KSYDOUhttp://spicyfund.com/fund_demo/help/guidelinesKSYDOU>guidelines</a>&nbsp;and makes good use of the platform. If youKSYSINGre unsure if your project is a good fit for FundraisingScript (or if FundraisingScript is a good fit for your project), weKSYSINGd encourage you to peruse&nbsp;&nbsp;<a href=KSYDOUhttp://spicyfund.com/fund_demo/project/successfulKSYDOU>successful</a>&nbsp;projects in your projectKSYSINGs category. When youKSYSINGre ready, send us your proposal. WeKSYSINGd love to hear about your project!</p>', '1', 1),
(2, 'Creating Rewards', 'creating-rewards', '<p>Rewards are what backers receive in exchange for pledging to a project. The importance of creative, tangible, and fairly priced rewards cannot be overstated. Projects whose rewards are overpriced or uninspired struggle to find support.</p>\n<p><strong>DECIDING WHAT TO OFFER</strong></p>\n<p>Every project&rsquo;s primary rewards should be things made by the project itself. If the project is to record a new album, then rewards should include a copy of the CD when it&rsquo;s finished. Rewards ensure that backers will benefit from a project just as much as its creator (i.e., they get cool stuff that they helped make possible!).</p>\n<p>There are four common reward types that we see on FundraisingScript:</p>\n<ul>\n<li><em>Copies of the thing:</em>&nbsp;the album, the DVD, a print from the show. These items should be priced what they would cost in a retail environment.</li>\n<li><em>Creative collaborations:</em>&nbsp;a backer appears as a hero in the comic, everyone gets painted into the mural, two backers do the handclaps for track 3.</li>\n<li><em>Creative experiences:</em>&nbsp;a visit to the set, a phone call from the author, dinner with the cast, a concert in your backyard.</li>\n<li><em>Creative mementos:</em>&nbsp;Polaroids sent from location, thanks in the credits, meaningful tokens that tell a story.</li>\n</ul>\n<p><strong>DECIDING HOW TO PRICE</strong></p>\n<p>FundraisingScript isn&rsquo;t charity: we champion exchanges that are a mix of commerce and patronage, and the numbers bear this out. To date the most popular pledge amount is $25 and the average pledge is around $70. Small amounts are where it&rsquo;s at: projects<em>without</em>&nbsp;a reward less than $20 succeed 35% of the time, while projects&nbsp;<em>with</em>&nbsp;a reward less than $20 succeed 54% of the time.</p>\n<p>So what works? Offering something of value. Actual value considers more than just sticker price. If it&rsquo;s a limited edition or a one-of-a-kind experience, there&rsquo;s a lot of flexibility based on your audience. But if it&rsquo;s a manufactured good, then it&rsquo;s a good idea to stay reasonably close to its real-world cost.</p>\n<p>There is no magic bullet, and we encourage every project to be as creative and true to itself as possible. Put yourself in your backers&rsquo; shoes: would&nbsp;<em>you</em>&nbsp;drop the cash on your rewards? The answer to that question will tell you a lot about your project&rsquo;s potential.</p>', '1', 2),
(3, 'Setting Your Goal', 'setting-your-goal', '<p>FundraisingScript operates on an all-or-nothing funding model where projects must be fully funded or no money changes hands. Projects must set a funding goal and a length of time to reach it. There&rsquo;s no magic formula to determining the right goal or duration. Every project is different, but there are a few things to keep in mind.</p>\n<p><strong>RESEARCHING YOUR BUDGET</strong></p>\n<p>How much money do you need? Are you raising the full budget or a portion of it? Have you factored in the cost of producing rewards and delivering them to backers? Avoid later headaches by doing your research, and be as transparent as you can. Backers will appreciate it.</p>\n<p><strong>CONSIDERING YOUR NETWORKS</strong></p>\n<p>FundraisingScript is not a magical source of money. Funding comes from a variety of sources &mdash; your audience, your friends and family, your broader social networks, and, if your project does well, strangers from around the web. It&rsquo;s up to you to build that momentum for your project.</p>\n<p><strong>CHOOSING YOUR GOAL</strong></p>\n<p>Once you&rsquo;ve researched your budget and considered your reach, you&rsquo;re ready to set your funding goal. Because funding is all-or-nothing, you can always raise more than your goal but never less. Figure out how much money you need to complete the project as promised (while considering how much funding you think you can generate), and select an amount close to that.</p>\n<p><strong>SETTING YOUR PROJECT DEADLINE</strong></p>\n<p>Projects can last anywhere from one to 60 days, however a longer project duration is not necessarily better. Statistically, projects lasting 30 days or less have our highest success rates. A FundraisingScript project takes a lot of work to run, and shorter projects set a tone of confidence and help motivate your backers to join the party. Longer durations incite less urgency, encourage procrastination, and tend to fizzle out.</p>', '1', 3),
(4, 'Building Your Project', 'building-your-project', '<p>As you build your project, take your time! The average successfully funded creator spends nearly two weeks tweaking their project before launching. A thoughtful and methodical approach can pay off.</p>\n<p><strong>TITLING YOUR PROJECT</strong></p>\n<p>Your FundraisingScript project title should be simple, specific, and memorable, and it should include the title of the creative project youKSYSINGre raising funds for. Imagine your title as a distinct identity that will set it apart (KSYDOUMake my new album&rdquo; isn&rsquo;t as helpful or searchable as &ldquo;The K-Stars record their debut EP,&nbsp;<em>All Or Nothing</em>&nbsp;KSYDOU). Avoid words like KSYDOUhelp,KSYDOU KSYDOUsupport,KSYDOU or KSYDOUfund.KSYDOU They imply that youKSYSINGre asking someone to do you a favor rather than offering an experience they&rsquo;re going to love.</p>\n<p><strong>PICKING YOUR PROJECT IMAGE</strong></p>\n<p>Your project image is how you will be represented on FundraisingScript and the rest of the web. Pick something that accurately reflects your project and that looks nice, too!</p>\n<p>&nbsp;</p>\n<p><strong>WRITING YOUR SHORT DESCRIPTION</strong></p>\n<p>Your short description appears in your project&rsquo;s widget, and it&rsquo;s the best place to quickly communicate to your audience what your project is about. Stay focused and be clear on what your project hopes to accomplish. If you had to describe your project in one tweet, how would you do it?</p>\n<p><strong>WRITING YOUR BIO</strong></p>\n<p>Your bio is a great opportunity to share more about you. Why are you the one to take on this project? What prior work can you share via links? This is key to earning your backers&rsquo; trust.</p>', '1', 4),
(5, 'Promoting Your Project', 'promoting-your-project', '<p>An exceptional project can lead to outpourings of support from all corners of the web, but for most projects, support comes from within their own networks and their networks&rsquo; networks. If you want people to back your project you have to tell them about it. More than once! And in a variety of ways! Here&rsquo;s how:</p>\n<p><strong>SMART OUTREACH</strong></p>\n<p>A nice, personal message is the most effective way to let someone know about your project. Send an email to your close friends and family so they can be first to pledge, then use your personal blog, your Facebook page, and your Twitter account to tune in everyone who&rsquo;s paying attention. Don&rsquo;t overwhelm with e-blasts and group messages, but be sure to remind your networks about your projects a few times throughout the course of its duration. Take the time to contact people individually. It makes a big difference.</p>\n<p><strong>MEETING UP</strong></p>\n<p>Don&rsquo;t be afraid to take your FundraisingScript project out into the real world. Nothing connects people to an idea like seeing the twinkle in your eye when you talk about it. Host pledge parties, print posters or flyers to distribute around your community, and organize meetups to educate people about your endeavor. Be creative!</p>\n<p><strong>STOPPING THE PRESSES</strong></p>\n<p>Contact your local newspaper, TV, and radio stations and tell them about your project. Seek out like-minded blogs and online media outlets to request coverage. Writers are always looking for stories to write about, and the media has a big soft spot for DIY success stories.</p>\n<p><strong>KEEPING IT REAL</strong></p>\n<p>Whatever channel you use to tell your project&rsquo;s story, don&rsquo;t spam. This includes posting your link on other FundraisingScript project pages, @messaging people to beg for money on Twitter, link-bombing on Facebook, and generally nagging people you don&rsquo;t already know. Over-posting can alienate your friends and fans, and it makes every other FundraisingScript project look bad too. Don&rsquo;t do it!</p>', '1', 5),
(6, 'Project Updates', 'project-updates', '<p>Project updates serve as your project&rsquo;s blog. They&rsquo;re a great way to share your progress, post media, and thank your backers. Posting a project update automatically sends an email to all your backers with that update. You can choose to make each update public for everyone to see, or reserve it for just your backers to view.</p>\n<p><strong>BUILDING MOMENTUM</strong></p>\n<p>While your project is live and the clock ticking, keep your backers informed and inspired to help you spread the word. Instead of posting a link to your project and asking for pledges every day, treat your project like a story that is unfolding and update everyone on its progress. &ldquo;Pics from last night&rsquo;s show!&rdquo; or &ldquo;We found a printer for our book!&rdquo; with a link to your project is engaging and fun for everybody to follow along with.</p>\n<p><strong>SHARING THE PROCESS</strong></p>\n<p>Once your project is successfully funded, don&rsquo;t forget about all the people that helped make it possible. Let backers and spectators watch your project come to life by sharing the decisions you make with them, explaining how it feels as your goal becomes a reality, and even asking them for feedback. Keeping backers informed and engaged is an essential part of FundraisingScript.</p>\n<p><strong>CELEBRATING SUCCESS</strong></p>\n<p>Sharing reviews, press, and photos from your project out in the world &mdash; whether it&rsquo;s opening night of your play or your book on someone&rsquo;s bookshelf &mdash; is great for everyone involved. The story of your project doesn&rsquo;t end after it gets shipped out. You still have a captivated audience that&rsquo;s cheering for you. Communicating with them can be one of the most rewarding parts of the process.</p>', '1', 6),
(8, 'testinng purpose111', 'testinng-purpose111', 'define???????/??//  ', '1', 20);

-- --------------------------------------------------------

--
-- Table structure for table `shipping_detail`
--

CREATE TABLE IF NOT EXISTS `shipping_detail` (
  `shipping_detail_id` int(50) NOT NULL,
  `shipping_name` varchar(255) DEFAULT NULL,
  `shipping_country` varchar(255) DEFAULT NULL,
  `shipping_add` text,
  `shipping_city` varchar(255) DEFAULT NULL,
  `shipping_state` varchar(255) DEFAULT NULL,
  `shipping_zipcode` varchar(25) DEFAULT NULL,
  `transaction_id` int(50) NOT NULL,
  `email` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `site_setting`
--

CREATE TABLE IF NOT EXISTS `site_setting` (
  `site_setting_id` int(11) NOT NULL,
  `site_name` varchar(255) DEFAULT NULL,
  `site_version` varchar(255) DEFAULT NULL,
  `site_language` varchar(255) DEFAULT NULL,
  `currency_symbol` varchar(255) DEFAULT NULL,
  `currency_code` varchar(255) DEFAULT NULL,
  `date_format` varchar(255) DEFAULT NULL,
  `time_format` varchar(255) DEFAULT NULL,
  `date_time_format` varchar(255) DEFAULT NULL,
  `date_format_tooltip` varchar(255) DEFAULT NULL,
  `time_format_tooltip` varchar(255) DEFAULT NULL,
  `date_time_format_tooltip` varchar(255) DEFAULT NULL,
  `date_time_highlight_tooltip` varchar(255) DEFAULT NULL,
  `site_tracker` varchar(255) DEFAULT NULL,
  `robots` varchar(255) DEFAULT NULL,
  `how_it_works_video` varchar(255) DEFAULT NULL,
  `normal_paypal` int(10) NOT NULL DEFAULT '1',
  `paypal_status` varchar(255) DEFAULT NULL,
  `paypal_email` varchar(255) DEFAULT NULL,
  `paypal_url` varchar(255) DEFAULT NULL,
  `paypal_API_UserName` varchar(255) DEFAULT NULL,
  `paypal_API_Password` varchar(255) DEFAULT NULL,
  `paypal_API_Signature` varchar(255) DEFAULT NULL,
  `ending_soon` int(20) NOT NULL DEFAULT '7',
  `popular` double(10,2) NOT NULL DEFAULT '60.00',
  `most_funded` int(20) NOT NULL,
  `successful` double(10,2) NOT NULL DEFAULT '90.00',
  `small_project` int(20) NOT NULL DEFAULT '1000',
  `site_logo` varchar(255) DEFAULT NULL,
  `site_logo_hover` varchar(255) DEFAULT NULL,
  `favicon_image` varchar(255) DEFAULT NULL,
  `fund_ideas` varchar(255) DEFAULT NULL,
  `project_min_days` int(5) NOT NULL,
  `project_max_days` int(5) NOT NULL,
  `time_zone` varchar(255) DEFAULT NULL,
  `mobile_site` int(1) NOT NULL DEFAULT '1',
  `currency_symbol_side` varchar(50) DEFAULT NULL,
  `decimal_points` int(5) NOT NULL,
  `enable_funding_option` tinyint(1) NOT NULL,
  `flexible_fees` decimal(10,2) NOT NULL,
  `suc_flexible_fees` decimal(10,2) NOT NULL,
  `fixed_fees` decimal(10,2) NOT NULL,
  `instant_fees` double(10,2) NOT NULL DEFAULT '0.00',
  `minimum_goal` decimal(10,2) NOT NULL,
  `maximum_goal` double(10,2) NOT NULL DEFAULT '2000.00',
  `maximum_project_per_year` int(25) NOT NULL,
  `maximum_donate_per_project` int(25) NOT NULL,
  `enable_facebook_stream` tinyint(1) NOT NULL,
  `enable_twitter_stream` tinyint(1) NOT NULL,
  `captcha_public_key` varchar(300) DEFAULT NULL,
  `captcha_private_key` varchar(300) DEFAULT NULL,
  `num_days_bydate` int(5) NOT NULL,
  `address_limit` int(3) NOT NULL,
  `forget_time_limit` int(11) NOT NULL,
  `perk_enable` varchar(255) DEFAULT NULL,
  `payment_gateway` int(1) NOT NULL DEFAULT '0',
  `demomode` tinyint(1) NOT NULL DEFAULT '0',
  `version_license` varchar(250) DEFAULT NULL,
  `version_url` varchar(150) DEFAULT NULL,
  `version_pcode` varchar(70) DEFAULT NULL,
  `ending_days` varchar(255) DEFAULT NULL,
  `signup_captcha` int(11) NOT NULL,
  `contact_us_captcha` int(11) NOT NULL,
  `slider_setting` tinyint(11) NOT NULL,
  `bank_detail` text,
  `accredential_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '0=required ,1=not required',
  `accredential_file` varchar(250) DEFAULT NULL,
  `sign_contract_upload_by` tinyint(2) NOT NULL DEFAULT '0' COMMENT '0-project owner,1-admin',
  `accrediated_manage` int(11) NOT NULL COMMENT '0 Project owner , 1 All project',
  `contract_copy_status` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `site_setting`
--

INSERT INTO `site_setting` (`site_setting_id`, `site_name`, `site_version`, `site_language`, `currency_symbol`, `currency_code`, `date_format`, `time_format`, `date_time_format`, `date_format_tooltip`, `time_format_tooltip`, `date_time_format_tooltip`, `date_time_highlight_tooltip`, `site_tracker`, `robots`, `how_it_works_video`, `normal_paypal`, `paypal_status`, `paypal_email`, `paypal_url`, `paypal_API_UserName`, `paypal_API_Password`, `paypal_API_Signature`, `ending_soon`, `popular`, `most_funded`, `successful`, `small_project`, `site_logo`, `site_logo_hover`, `favicon_image`, `fund_ideas`, `project_min_days`, `project_max_days`, `time_zone`, `mobile_site`, `currency_symbol_side`, `decimal_points`, `enable_funding_option`, `flexible_fees`, `suc_flexible_fees`, `fixed_fees`, `instant_fees`, `minimum_goal`, `maximum_goal`, `maximum_project_per_year`, `maximum_donate_per_project`, `enable_facebook_stream`, `enable_twitter_stream`, `captcha_public_key`, `captcha_private_key`, `num_days_bydate`, `address_limit`, `forget_time_limit`, `perk_enable`, `payment_gateway`, `demomode`, `version_license`, `version_url`, `version_pcode`, `ending_days`, `signup_captcha`, `contact_us_captcha`, `slider_setting`, `bank_detail`, `accredential_status`, `accredential_file`, `sign_contract_upload_by`, `accrediated_manage`, `contract_copy_status`) VALUES
(2, 'Real Estate', '1.0.0.0', '1', '$', 'USD', 'd M,Y', 'H:i', '', '', '', '', '', '', '', '', 0, 'sandbox', 'jigar_1313046627_biz@gmail.com', 'https://www.sandbox.paypal.com/cgi-bin/webscri', 'jigar_1313046627_biz_api1.gmail.com', '1313046663', 'AMuOxt1FpmAKBEJ6exYeVH0TefE8AHDjH6WasXwi3PskdUAUPWS2au44', 30, 100.00, 1, 100.00, 1000, 'logo_62275.png', 'logo_89335.png', 'favicon_18137.png', '1', 1, 365, 'Asia/Calcutta', 0, 'left_symbol_right_space_code', 2, 2, '5.00', '5.00', '5.00', 0.00, '1.00', 5000.00, 1000, 0, 1, 1, '6LdGEBETAAAAAMnXrtn11N3Q77cwb9sQ0_oSbXmg', '6LdGEBETAAAAAAftgKxmbFO27zlGiin3P6bpWRHS', 0, 3, 24, '1', 5, 0, '8hf3rho66w7x92vpdr!!', 'http://airtaskerclone.com/versions/api/example/process/', 'REALESTATEFUND001', '10', 1, 1, 2, 'Updated by admin:Bank Name: Bank Of Demo ,<br>\r\nAcc: 8989898989899 , \r\n<br>\r\nName: Demo Name , \r\n<br>\r\nRouting# 45897\r\n<br>\r\n OR Use following details to write us Check\r\n<br>			 \r\nName: Demo Name\r\n<br>\r\nAddress: 123, King St, Burlington, MA, USA\r\n<br>', 0, 'accredential_20893.pdf', 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `slider_setting`
--

CREATE TABLE IF NOT EXISTS `slider_setting` (
  `id` int(11) NOT NULL,
  `slider_id` int(11) NOT NULL,
  `equity_id` int(11) NOT NULL,
  `view_order` int(11) NOT NULL,
  `status` tinyint(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `slider_setting`
--

INSERT INTO `slider_setting` (`id`, `slider_id`, `equity_id`, `view_order`, `status`) VALUES
(1, 1, 0, 6, 0),
(2, 2, 0, 5, 0),
(3, 3, 0, 4, 0),
(4, 0, 24, 7, 0);

-- --------------------------------------------------------

--
-- Table structure for table `spam_control`
--

CREATE TABLE IF NOT EXISTS `spam_control` (
  `spam_control_id` int(10) NOT NULL,
  `spam_report_total` int(30) NOT NULL,
  `spam_report_expire` int(30) NOT NULL,
  `total_register` int(30) NOT NULL,
  `register_expire` int(30) NOT NULL,
  `total_comment` int(30) NOT NULL,
  `comment_expire` int(30) NOT NULL,
  `total_contact` int(30) NOT NULL,
  `contact_expire` int(30) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `spam_control`
--

INSERT INTO `spam_control` (`spam_control_id`, `spam_report_total`, `spam_report_expire`, `total_register`, `register_expire`, `total_comment`, `comment_expire`, `total_contact`, `contact_expire`) VALUES
(1, 10, 10, 50, 20, 10, 10, 10, 3);

-- --------------------------------------------------------

--
-- Table structure for table `spam_inquiry`
--

CREATE TABLE IF NOT EXISTS `spam_inquiry` (
  `inquire_id` int(100) NOT NULL,
  `inquire_spam_ip` varchar(255) DEFAULT NULL,
  `inquire_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `spam_ip`
--

CREATE TABLE IF NOT EXISTS `spam_ip` (
  `spam_id` int(100) NOT NULL,
  `spam_ip` varchar(255) DEFAULT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `permenant_spam` int(10) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=3746 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `spam_report_ip`
--

CREATE TABLE IF NOT EXISTS `spam_report_ip` (
  `spam_report_id` int(100) NOT NULL,
  `spam_ip` varchar(255) DEFAULT NULL,
  `spam_user_id` int(100) NOT NULL,
  `reported_user_id` int(100) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE IF NOT EXISTS `state` (
  `state_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `state_name` varchar(255) DEFAULT NULL,
  `active` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=156 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`state_id`, `country_id`, `state_name`, `active`) VALUES
(11, 2, 'Gujarat', '1'),
(14, 117, 'Artigas', '1'),
(15, 117, 'Canelones', '1'),
(16, 117, 'Cerro Largo', '1'),
(17, 117, 'Colonia', '1'),
(18, 117, 'Durazno', '1'),
(19, 117, 'Flores', '1'),
(21, 117, 'Florida', '1'),
(22, 117, 'Lavalleja', '1'),
(23, 117, 'Maldonado', '1'),
(24, 117, 'Montevideo', '1'),
(25, 117, 'Paysandu', '1'),
(26, 117, 'Rivera', '1'),
(27, 117, 'Rocha', '1'),
(28, 117, 'Salto', '1'),
(29, 18, 'Canberra', '1'),
(30, 19, 'Vienna', '1'),
(31, 21, 'Nassau', '1'),
(32, 34, 'Brasilia', '1'),
(33, 42, 'Ottawa', '1'),
(34, 44, 'Bangui', '1'),
(36, 47, 'Beijing', '1'),
(37, 47, 'Shaanxi', '1'),
(38, 47, 'Shanghai', '1'),
(39, 51, 'San Jose', '1'),
(40, 54, 'Nicosia', '1'),
(41, 56, 'denmark', '1'),
(46, 7, 'Suva', '1'),
(47, 68, 'Paris', '1'),
(48, 68, 'Gers', '1'),
(49, 68, 'Jura', '1'),
(50, 68, 'Savoie', '1'),
(51, 71, 'Atlanta', '1'),
(52, 72, 'Berlin', '1'),
(53, 73, 'Accra', '1'),
(54, 79, 'Port-au-Prince', '1'),
(55, 83, 'Victoria', '1'),
(56, 83, 'Fragrance Harbour ', '1'),
(57, 2, 'Delhi', '1'),
(58, 90, 'Rome', '1'),
(59, 3, 'Tokyo', '1'),
(60, 3, 'Yokohama', '1'),
(61, 3, 'Osaka', '1'),
(62, 3, 'Nagoya', '1'),
(63, 3, 'Sapporo', '1'),
(64, 3, 'Kobe', '1'),
(65, 3, 'Kyoto', '1'),
(66, 3, 'Fukuoka', '1'),
(67, 3, 'Kawasaki', '1'),
(68, 3, 'Saitama', '1'),
(69, 3, 'Hiroshima', '1'),
(70, 3, 'Sendai', '1'),
(71, 3, 'Kitakyushu ', '1'),
(72, 94, 'Nairobi', '1'),
(73, 102, 'Beirut', '1'),
(74, 105, 'Tripoli ', '1'),
(75, 113, 'Kuala Lumpur', '1'),
(76, 121, 'mexico city', '1'),
(77, 124, 'Monte Carlo', '1'),
(78, 127, 'Rabat', '1'),
(79, 132, 'Amsterdam', '1'),
(80, 134, 'Wellington', '1'),
(81, 139, 'Oslo', '1'),
(82, 137, 'Abuja', '1'),
(83, 144, 'Panama City', '1'),
(84, 147, 'Lima', '1'),
(85, 150, 'Lisbon', '1'),
(86, 149, 'Warsaw', '1'),
(87, 6, 'Moscow', '1'),
(88, 207, 'singapore', '1'),
(89, 1, 'Alabama', '1'),
(90, 1, 'Alaska', '1'),
(91, 1, 'AriZona', '1'),
(92, 1, 'California', '1'),
(93, 1, 'Arlamsa', '1'),
(94, 1, 'colorado', '1'),
(95, 1, 'DC', '1'),
(96, 1, 'Delaware', '1'),
(97, 1, 'Florida', '1'),
(98, 1, 'Georgia', '1'),
(99, 1, 'Hawaii', '1'),
(100, 1, 'Idaho', '1'),
(101, 1, 'Illinois', '1'),
(102, 1, 'Indiana', '1'),
(103, 1, 'Iowa', '1'),
(104, 1, 'Kansas', '1'),
(105, 1, 'Kentucky', '1'),
(106, 1, 'Louisiana', '1'),
(107, 1, 'Maine', '1'),
(108, 1, 'Maryland', '1'),
(109, 1, 'Massachusetts', '1'),
(110, 1, 'Michigan', '1'),
(111, 1, 'Minnesota', '1'),
(112, 1, 'Mississippi', '1'),
(113, 1, 'Missouri', '1'),
(114, 1, 'montana', '1'),
(115, 1, 'Nebraska', '1'),
(116, 1, 'Nevada', '1'),
(117, 1, 'New Hampshire', '1'),
(118, 1, 'New Mexico', '1'),
(119, 1, 'New York', '1'),
(120, 1, 'New Jersey', '1'),
(121, 1, 'North Carolina', '1'),
(122, 1, 'North Dakota', '1'),
(123, 1, 'Ohio', '1'),
(124, 1, 'Oklahoma', '1'),
(125, 1, 'Oregon', '1'),
(126, 1, 'Pennsylvania', '1'),
(127, 1, 'Rhode Island', '1'),
(128, 1, 'South Carolina', '1'),
(129, 1, 'South Dakota', '1'),
(130, 1, 'Tennessee', '1'),
(131, 1, 'Texas', '1'),
(132, 1, 'Utah', '1'),
(133, 1, 'Vermont', '1'),
(134, 1, 'Virginia', '1'),
(135, 1, 'Washington', '1'),
(136, 1, 'West Virginia', '1'),
(137, 1, 'Wisconsin', '1'),
(138, 1, 'Wyoming', '1'),
(139, 173, 'Pretoria', '1'),
(140, 176, 'La Rioja', '1'),
(141, 176, 'Madrid', '1'),
(142, 176, 'Murcia', '1'),
(143, 176, 'Balearic Islands', '1'),
(144, 176, 'Aragon', '1'),
(145, 176, 'Catalonia', '1'),
(146, 176, 'Castilla-La Mancha', '1'),
(147, 176, 'Galicia', '1'),
(148, 181, 'Stockholm', '1'),
(149, 180, 'Bern', '1'),
(150, 194, 'Ankara Province', '1'),
(151, 5, 'London', '1'),
(154, 208, 'vado dara', '0'),
(155, 2, 'Bihar', '1');

-- --------------------------------------------------------

--
-- Table structure for table `taxonomy_setting`
--

CREATE TABLE IF NOT EXISTS `taxonomy_setting` (
  `taxonomy_setting_id` int(11) NOT NULL,
  `project_name` varchar(255) DEFAULT NULL,
  `project_name_plural` varchar(255) DEFAULT NULL,
  `project_url` varchar(255) DEFAULT NULL,
  `project_owner` varchar(255) DEFAULT NULL,
  `project_owner_plural` varchar(255) DEFAULT NULL,
  `funds` varchar(255) DEFAULT NULL,
  `funds_plural` varchar(255) DEFAULT NULL,
  `funds_past` varchar(255) DEFAULT NULL,
  `funds_gerund` varchar(255) DEFAULT NULL,
  `updates` varchar(255) DEFAULT NULL,
  `updates_plural` varchar(255) DEFAULT NULL,
  `updates_past` varchar(255) DEFAULT NULL,
  `updates_gerund` varchar(255) DEFAULT NULL,
  `comments` varchar(255) DEFAULT NULL,
  `comments_plural` varchar(255) DEFAULT NULL,
  `comments_past` varchar(255) DEFAULT NULL,
  `comments_gerund` varchar(255) DEFAULT NULL,
  `follower` varchar(255) DEFAULT NULL,
  `followers_plural` varchar(255) DEFAULT NULL,
  `followers_past` varchar(255) DEFAULT NULL,
  `followers_gerund` varchar(255) DEFAULT NULL,
  `unfollower` varchar(100) DEFAULT NULL,
  `unfollowers_plural` varchar(100) DEFAULT NULL,
  `unfollowers_past` varchar(100) DEFAULT NULL,
  `unfollowers_gerund` varchar(100) DEFAULT NULL,
  `investor` varchar(100) DEFAULT NULL,
  `company_name` varchar(255) NOT NULL,
  `company_url` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `taxonomy_setting`
--

INSERT INTO `taxonomy_setting` (`taxonomy_setting_id`, `project_name`, `project_name_plural`, `project_url`, `project_owner`, `project_owner_plural`, `funds`, `funds_plural`, `funds_past`, `funds_gerund`, `updates`, `updates_plural`, `updates_past`, `updates_gerund`, `comments`, `comments_plural`, `comments_past`, `comments_gerund`, `follower`, `followers_plural`, `followers_past`, `followers_gerund`, `unfollower`, `unfollowers_plural`, `unfollowers_past`, `unfollowers_gerund`, `investor`, `company_name`, `company_url`) VALUES
(1, 'Development', 'Developments', 'development', 'Company', 'Companies', 'Investment', 'Investments', 'Invested', 'Investing', 'Update', 'Updates', 'Updated', 'Updating', 'Comment', 'Comments', 'Commented', 'Commenting', 'Follower', 'Followers', 'Followed', 'Following', 'Unfollower', 'Unfollowers', 'Unfollowed', 'Unfollowing', 'Investor', 'Company', 'company');

-- --------------------------------------------------------

--
-- Table structure for table `team_member_type`
--

CREATE TABLE IF NOT EXISTS `team_member_type` (
  `id` int(11) NOT NULL,
  `team_member_type_name` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `team_member_type`
--

INSERT INTO `team_member_type` (`id`, `team_member_type_name`, `status`) VALUES
(1, 'Leadership', 1),
(2, 'Team Member', 1),
(3, 'Advisor', 0),
(4, 'Team member1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `temp_preapprove`
--

CREATE TABLE IF NOT EXISTS `temp_preapprove` (
  `temp_pre_id` int(100) NOT NULL,
  `preapprovalKey` varchar(255) DEFAULT NULL,
  `user_id` int(100) NOT NULL,
  `equity_id` int(11) NOT NULL,
  `perk_id` int(50) NOT NULL,
  `comment` text,
  `host_ip` varchar(100) DEFAULT NULL,
  `transaction_date_time` datetime NOT NULL,
  `temp_anonymous` tinyint(1) NOT NULL,
  `total_amount` decimal(10,2) NOT NULL,
  `amount` decimal(10,2) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `temp_preapprove`
--

INSERT INTO `temp_preapprove` (`temp_pre_id`, `preapprovalKey`, `user_id`, `equity_id`, `perk_id`, `comment`, `host_ip`, `transaction_date_time`, `temp_anonymous`, `total_amount`, `amount`) VALUES
(1, 'INV3S7wTKBWtRHD', 2, 1, 0, '', '27.109.7.130', '2015-10-05 11:11:16', 0, '450.00', '450.00'),
(2, 'INV55mYNXKZUja7', 3, 1, 0, '', '27.109.7.130', '2015-10-05 11:21:00', 0, '500.00', '500.00'),
(3, 'INVt8mMihWAHPqM', 8, 1, 0, '', '123.201.174.209', '2015-10-26 18:43:46', 0, '100.00', '100.00'),
(4, 'INVBYRXzaOaHQ59', 11, 8, 0, '', '150.107.211.150', '2015-10-27 14:49:32', 0, '500.00', '500.00'),
(5, 'INVTn7NBeQh5ICD', 12, 8, 0, '', '27.109.7.130', '2015-10-27 16:59:20', 0, '500.00', '500.00'),
(6, 'INVAUNBb82kb6s7', 16, 1, 0, 'comment test', '27.109.7.130', '2015-11-20 14:19:12', 0, '100.00', '100.00'),
(7, 'INVjwuyzmUZzuq0', 11, 1, 0, '', '27.109.7.130', '2015-11-20 14:45:43', 0, '100.00', '100.00'),
(8, 'INVk4Llcq8H3DmC', 16, 1, 0, '', '27.109.7.130', '2015-11-20 17:20:45', 0, '100.00', '100.00'),
(9, 'INVO7fnPEw2zJRW', 37, 19, 0, '', '27.109.7.130', '2015-11-20 18:03:58', 0, '500.00', '500.00'),
(10, 'INVEtlP0MaeWYar', 37, 19, 0, '', '27.109.7.130', '2015-11-20 18:23:35', 0, '500.00', '500.00'),
(11, 'INVQ2bK2cRcOtOF', 11, 24, 0, 'Lorem Ipsum has been the industry\\''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.', '27.109.7.130', '2015-11-30 13:42:34', 0, '200.00', '200.00'),
(12, 'INV8Bdco0a7edgY', 12, 24, 4, '', '27.109.7.130', '2015-11-30 14:35:28', 0, '1000.00', '1000.00'),
(13, 'INVqFipS00KnHta', 11, 24, 0, 'Herry Patel', '27.109.7.130', '2015-12-01 10:58:58', 0, '500.00', '500.00'),
(14, 'INV8SRkYfPwMImW', 45, 24, 0, 'vatsal.test.rockersinfo@gmail.com', '27.109.7.130', '2015-12-02 11:57:03', 0, '500.00', '500.00'),
(15, 'INVTtx9T1J8DSOJ', 43, 1, 0, 'testing donation issues', '27.109.7.130', '2015-12-02 12:02:19', 0, '400.00', '400.00'),
(16, 'INVUqXKuZgkhpCG', 46, 24, 0, 'vatsal.test1.rockersinfo@gmail.com', '27.109.7.130', '2015-12-02 13:27:35', 0, '100.00', '100.00'),
(17, 'INVi6TSRFtLaSEb', 45, 24, 0, 'adsfadfadf', '123.201.160.118', '2015-12-18 14:56:09', 0, '100.00', '100.00'),
(18, 'INV3ZBiYf7gYRxI', 45, 24, 0, '', '123.201.160.118', '2015-12-18 14:57:22', 0, '0.00', '0.00'),
(19, 'INVBjwf91KUn04L', 45, 24, 0, 'ajsjdaf', '203.109.65.159', '2015-12-21 12:02:42', 0, '100.00', '100.00'),
(20, 'INVaUS9Mft4Bnem', 45, 24, 0, '', '27.109.7.130', '2015-12-21 13:41:27', 0, '100.00', '100.00');

-- --------------------------------------------------------

--
-- Table structure for table `themes`
--

CREATE TABLE IF NOT EXISTS `themes` (
  `id` int(10) NOT NULL,
  `color_orange` varchar(10) DEFAULT NULL,
  `color_darkorange` varchar(10) DEFAULT NULL,
  `color_grey` varchar(10) DEFAULT NULL,
  `color_black` varchar(10) DEFAULT NULL,
  `color_white` varchar(10) DEFAULT NULL,
  `color_lightgrey` varchar(10) DEFAULT NULL,
  `color_blue` varchar(10) DEFAULT NULL,
  `color_darkblue` varchar(10) DEFAULT NULL,
  `color_ccc` varchar(10) DEFAULT NULL,
  `style_font1` varchar(255) DEFAULT NULL,
  `style_font2` varchar(255) DEFAULT NULL,
  `default_value` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `themes`
--

INSERT INTO `themes` (`id`, `color_orange`, `color_darkorange`, `color_grey`, `color_black`, `color_white`, `color_lightgrey`, `color_blue`, `color_darkblue`, `color_ccc`, `style_font1`, `style_font2`, `default_value`) VALUES
(1, '#f37621', '#e56712', '#4c4c4c', '#323232', '#ffffff', '#efefef', '#3bb0d2', '#32869c', '#cccccc', 'Arial, Helvetica, sans-serif', 'Arial, Helvetica, sans-serif', 0),
(2, '#f37621', '#e56712', '#4c4c4c', '#323232', '#ffffff', '#efefef', '#3bb0d2', '#32869c', '#cccccc', 'Arial, Helvetica, sans-serif', 'Arial, Helvetica, sans-serif', 1);

-- --------------------------------------------------------

--
-- Table structure for table `timezone`
--

CREATE TABLE IF NOT EXISTS `timezone` (
  `id` int(10) NOT NULL,
  `name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=579 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `timezone`
--

INSERT INTO `timezone` (`id`, `name`) VALUES
(1, 'Africa/Abidjan'),
(2, 'Africa/Asmera'),
(3, 'Africa/Blantyre'),
(4, 'Africa/Ceuta'),
(5, 'Africa/Douala'),
(6, 'Africa/Johannesburg'),
(7, 'Africa/Kinshasa'),
(8, 'Africa/Lubumbashi'),
(9, 'Africa/Mbabane'),
(10, 'Africa/Niamey'),
(11, 'Africa/Timbuktu'),
(12, 'Africa/Accra'),
(13, 'Africa/Bamako'),
(14, 'Africa/Brazzaville'),
(15, 'Africa/Conakry'),
(16, 'Africa/El_Aaiun'),
(17, 'Africa/Juba'),
(18, 'Africa/Lagos'),
(19, 'Africa/Lusaka'),
(20, 'Africa/Mogadishu'),
(21, 'Africa/Nouakchott'),
(22, 'Africa/Tripoli'),
(23, 'Africa/Addis_Ababa'),
(24, 'Africa/Bangui'),
(25, 'Africa/Bujumbura'),
(26, 'Africa/Dakar'),
(27, 'Africa/Freetown'),
(28, 'Africa/Kampala'),
(29, 'Africa/Libreville'),
(30, 'Africa/Malabo'),
(31, 'Africa/Monrovia'),
(32, 'Africa/Ouagadougou'),
(33, 'Africa/Tunis'),
(34, 'Africa/Algiers'),
(35, 'Africa/Banjul'),
(36, 'Africa/Cairo'),
(37, 'Africa/Dar_es_Salaam'),
(38, 'Africa/Gaborone'),
(39, 'Africa/Khartoum'),
(40, 'Africa/Lome'),
(41, 'Africa/Maputo'),
(42, 'Africa/Nairobi'),
(43, 'Africa/Porto-Novo'),
(44, 'Africa/Windhoek'),
(45, 'Africa/Asmara'),
(46, 'Africa/Bissau'),
(47, 'Africa/Casablanca'),
(48, 'Africa/Djibouti'),
(49, 'Africa/Harare'),
(50, 'Africa/Kigali'),
(51, 'Africa/Luanda'),
(52, 'Africa/Maseru'),
(53, 'Africa/Ndjamena'),
(54, 'Africa/Sao_Tome'),
(55, 'America/Adak'),
(56, 'America/Argentina/Buenos_Aires'),
(57, 'America/Argentina/La_Rioja'),
(58, 'America/Argentina/San_Luis'),
(59, 'America/Atikokan'),
(60, 'America/Belem'),
(61, 'America/Boise'),
(62, 'America/Caracas'),
(63, 'America/Chihuahua'),
(64, 'America/Cuiaba'),
(65, 'America/Denver'),
(66, 'America/El_Salvador'),
(67, 'America/Godthab'),
(68, 'America/Guatemala'),
(69, 'America/Hermosillo'),
(70, 'America/Indiana/Tell_City'),
(71, 'America/Inuvik'),
(72, 'America/Kentucky/Louisville'),
(73, 'America/Lima'),
(74, 'America/Managua'),
(75, 'America/Mazatlan'),
(76, 'America/Mexico_City'),
(77, 'America/Montreal'),
(78, 'America/Nome'),
(79, 'America/Ojinaga'),
(80, 'America/Port-au-Prince'),
(81, 'America/Rainy_River'),
(82, 'America/Rio_Branco'),
(83, 'America/Santo_Domingo'),
(84, 'America/St_Barthelemy'),
(85, 'America/St_Vincent'),
(86, 'America/Tijuana'),
(87, 'America/Whitehorse'),
(88, 'America/Anchorage'),
(89, 'America/Argentina/Catamarca'),
(90, 'America/Argentina/Mendoza'),
(91, 'America/Argentina/Tucuman'),
(92, 'America/Atka'),
(93, 'America/Belize'),
(94, 'America/Buenos_Aires'),
(95, 'America/Catamarca'),
(96, 'America/Coral_Harbour'),
(97, 'America/Curacao'),
(98, 'America/Detroit'),
(99, 'America/Ensenada'),
(100, 'America/Goose_Bay'),
(101, 'America/Guayaquil'),
(102, 'America/Indiana/Indianapolis'),
(103, 'America/Indiana/Vevay'),
(104, 'America/Iqaluit'),
(105, 'America/Kentucky/Monticello'),
(106, 'America/Los_Angeles'),
(107, 'America/Manaus'),
(108, 'America/Mendoza'),
(109, 'America/Miquelon'),
(110, 'America/Montserrat'),
(111, 'America/Noronha'),
(112, 'America/Panama'),
(113, 'America/Port_of_Spain'),
(114, 'America/Rankin_Inlet'),
(115, 'America/Rosario'),
(116, 'America/Sao_Paulo'),
(117, 'America/St_Johns'),
(118, 'America/Swift_Current'),
(119, 'America/Toronto'),
(120, 'America/Winnipeg'),
(121, 'America/Anguilla'),
(122, 'America/Argentina/ComodRivadavia'),
(123, 'America/Argentina/Rio_Gallegos'),
(124, 'America/Argentina/Ushuaia'),
(125, 'America/Bahia'),
(126, 'America/Blanc-Sablon'),
(127, 'America/Cambridge_Bay'),
(128, 'America/Cayenne'),
(129, 'America/Cordoba'),
(130, 'America/Danmarkshavn'),
(131, 'America/Dominica'),
(132, 'America/Fort_Wayne'),
(133, 'America/Grand_Turk'),
(134, 'America/Guyana'),
(135, 'America/Indiana/Knox'),
(136, 'America/Indiana/Vincennes'),
(137, 'America/Jamaica'),
(138, 'America/Knox_IN'),
(139, 'America/Louisville'),
(140, 'America/Marigot'),
(141, 'America/Menominee'),
(142, 'America/Moncton'),
(143, 'America/Nassau'),
(144, 'America/North_Dakota/Beulah'),
(145, 'America/Pangnirtung'),
(146, 'America/Porto_Acre'),
(147, 'America/Recife'),
(148, 'America/Santa_Isabel'),
(149, 'America/Scoresbysund'),
(150, 'America/St_Kitts'),
(151, 'America/Tegucigalpa'),
(152, 'America/Tortola'),
(153, 'America/Yakutat'),
(154, 'America/Antigua'),
(155, 'America/Argentina/Cordoba'),
(156, 'America/Argentina/Salta'),
(157, 'America/Aruba'),
(158, 'America/Bahia_Banderas'),
(159, 'America/Boa_Vista'),
(160, 'America/Campo_Grande'),
(161, 'America/Cayman'),
(162, 'America/Costa_Rica'),
(163, 'America/Dawson'),
(164, 'America/Edmonton'),
(165, 'America/Fortaleza'),
(166, 'America/Grenada'),
(167, 'America/Halifax'),
(168, 'America/Indiana/Marengo'),
(169, 'America/Indiana/Winamac'),
(170, 'America/Jujuy'),
(171, 'America/Kralendijk'),
(172, 'America/Lower_Princes'),
(173, 'America/Martinique'),
(174, 'America/Merida'),
(175, 'America/Monterrey'),
(176, 'America/New_York'),
(177, 'America/North_Dakota/Center'),
(178, 'America/Paramaribo'),
(179, 'America/Porto_Velho'),
(180, 'America/Regina'),
(181, 'America/Santarem'),
(182, 'America/Shiprock'),
(183, 'America/St_Lucia'),
(184, 'America/Thule'),
(185, 'America/Vancouver'),
(186, 'America/Yellowknife'),
(187, 'America/Araguaina'),
(188, 'America/Argentina/Jujuy'),
(189, 'America/Argentina/San_Juan'),
(190, 'America/Asuncion'),
(191, 'America/Barbados'),
(192, 'America/Bogota'),
(193, 'America/Cancun'),
(194, 'America/Chicago'),
(195, 'America/Creston'),
(196, 'America/Dawson_Creek'),
(197, 'America/Eirunepe'),
(198, 'America/Glace_Bay'),
(199, 'America/Guadeloupe'),
(200, 'America/Havana'),
(201, 'America/Indiana/Petersburg'),
(202, 'America/Indianapolis'),
(203, 'America/Juneau'),
(204, 'America/La_Paz'),
(205, 'America/Maceio'),
(206, 'America/Matamoros'),
(207, 'America/Metlakatla'),
(208, 'America/Montevideo'),
(209, 'America/Nipigon'),
(210, 'America/North_Dakota/New_Salem'),
(211, 'America/Phoenix'),
(212, 'America/Puerto_Rico'),
(213, 'America/Resolute'),
(214, 'America/Santiago'),
(215, 'America/Sitka'),
(216, 'America/St_Thomas'),
(217, 'America/Thunder_Bay'),
(218, 'America/Virgin'),
(219, 'Antarctica/Casey'),
(220, 'Antarctica/McMurdo'),
(221, 'Antarctica/Vostok'),
(222, 'Antarctica/Davis'),
(223, 'Antarctica/Palmer'),
(224, 'Antarctica/DumontDUrville'),
(225, 'Antarctica/Rothera'),
(226, 'Antarctica/Macquarie'),
(227, 'Antarctica/South_Pole'),
(228, 'Antarctica/Mawson'),
(229, 'Antarctica/Syowa'),
(230, 'Arctic/Longyearbyen'),
(231, 'Asia/Aden'),
(232, 'Asia/Aqtobe'),
(233, 'Asia/Baku'),
(234, 'Asia/Calcutta'),
(235, 'Asia/Dacca'),
(236, 'Asia/Dushanbe'),
(237, 'Asia/Hong_Kong'),
(238, 'Asia/Jayapura'),
(239, 'Asia/Kashgar'),
(240, 'Asia/Krasnoyarsk'),
(241, 'Asia/Macau'),
(242, 'Asia/Nicosia'),
(243, 'Asia/Phnom_Penh'),
(244, 'Asia/Rangoon'),
(245, 'Asia/Seoul'),
(246, 'Asia/Tbilisi'),
(247, 'Asia/Tokyo'),
(248, 'Asia/Ust-Nera'),
(249, 'Asia/Yerevan'),
(250, 'Asia/Almaty'),
(251, 'Asia/Ashgabat'),
(252, 'Asia/Bangkok'),
(253, 'Asia/Choibalsan'),
(254, 'Asia/Damascus'),
(255, 'Asia/Gaza'),
(256, 'Asia/Hovd'),
(257, 'Asia/Jerusalem'),
(258, 'Asia/Kathmandu'),
(259, 'Asia/Kuala_Lumpur'),
(260, 'Asia/Magadan'),
(261, 'Asia/Novokuznetsk'),
(262, 'Asia/Pontianak'),
(263, 'Asia/Riyadh'),
(264, 'Asia/Shanghai'),
(265, 'Asia/Tehran'),
(266, 'Asia/Ujung_Pandang'),
(267, 'Asia/Vientiane'),
(268, 'Asia/Amman'),
(269, 'Asia/Ashkhabad'),
(270, 'Asia/Beirut'),
(271, 'Asia/Chongqing'),
(272, 'Asia/Dhaka'),
(273, 'Asia/Harbin'),
(274, 'Asia/Irkutsk'),
(275, 'Asia/Kabul'),
(276, 'Asia/Katmandu'),
(277, 'Asia/Kuching'),
(278, 'Asia/Makassar'),
(279, 'Asia/Novosibirsk'),
(280, 'Asia/Pyongyang'),
(281, 'Asia/Saigon'),
(282, 'Asia/Singapore'),
(283, 'Asia/Tel_Aviv'),
(284, 'Asia/Ulaanbaatar'),
(285, 'Asia/Vladivostok'),
(286, 'Asia/Anadyr'),
(287, 'Asia/Baghdad'),
(288, 'Asia/Bishkek'),
(289, 'Asia/Chungking'),
(290, 'Asia/Dili'),
(291, 'Asia/Hebron'),
(292, 'Asia/Istanbul'),
(293, 'Asia/Kamchatka'),
(294, 'Asia/Khandyga'),
(295, 'Asia/Kuwait'),
(296, 'Asia/Manila'),
(297, 'Asia/Omsk'),
(298, 'Asia/Qatar'),
(299, 'Asia/Sakhalin'),
(300, 'Asia/Taipei'),
(301, 'Asia/Thimbu'),
(302, 'Asia/Ulan_Bator'),
(303, 'Asia/Yakutsk'),
(304, 'Asia/Aqtau'),
(305, 'Asia/Bahrain'),
(306, 'Asia/Brunei'),
(307, 'Asia/Colombo'),
(308, 'Asia/Dubai'),
(309, 'Asia/Ho_Chi_Minh'),
(310, 'Asia/Jakarta'),
(311, 'Asia/Karachi'),
(312, 'Asia/Kolkata'),
(313, 'Asia/Macao'),
(314, 'Asia/Muscat'),
(315, 'Asia/Oral'),
(316, 'Asia/Qyzylorda'),
(317, 'Asia/Samarkand'),
(318, 'Asia/Tashkent'),
(319, 'Asia/Thimphu'),
(320, 'Asia/Urumqi'),
(321, 'Asia/Yekaterinburg'),
(322, 'Atlantic/Azores'),
(323, 'Atlantic/Faroe'),
(324, 'Atlantic/St_Helena'),
(325, 'Atlantic/Bermuda'),
(326, 'Atlantic/Jan_Mayen'),
(327, 'Atlantic/Stanley'),
(328, 'Atlantic/Canary'),
(329, 'Atlantic/Madeira'),
(330, 'Atlantic/Cape_Verde'),
(331, 'Atlantic/Reykjavik'),
(332, 'Atlantic/Faeroe'),
(333, 'Atlantic/South_Georgia'),
(334, 'Australia/ACT'),
(335, 'Australia/Currie'),
(336, 'Australia/Lindeman'),
(337, 'Australia/Perth'),
(338, 'Australia/Victoria'),
(339, 'Australia/Adelaide'),
(340, 'Australia/Darwin'),
(341, 'Australia/Lord_Howe'),
(342, 'Australia/Queensland'),
(343, 'Australia/West'),
(344, 'Australia/Brisbane'),
(345, 'Australia/Eucla'),
(346, 'Australia/Melbourne'),
(347, 'Australia/South'),
(348, 'Australia/Yancowinna'),
(349, 'Australia/Broken_Hill'),
(350, 'Australia/Hobart'),
(351, 'Australia/North'),
(352, 'Australia/Sydney'),
(353, 'Australia/Canberra'),
(354, 'Australia/LHI'),
(355, 'Australia/NSW'),
(356, 'Australia/Tasmania'),
(357, 'Europe/Amsterdam'),
(358, 'Europe/Berlin'),
(359, 'Europe/Busingen'),
(360, 'Europe/Guernsey'),
(361, 'Europe/Kaliningrad'),
(362, 'Europe/Luxembourg'),
(363, 'Europe/Monaco'),
(364, 'Europe/Podgorica'),
(365, 'Europe/San_Marino'),
(366, 'Europe/Stockholm'),
(367, 'Europe/Vaduz'),
(368, 'Europe/Warsaw'),
(369, 'Europe/Andorra'),
(370, 'Europe/Bratislava'),
(371, 'Europe/Chisinau'),
(372, 'Europe/Helsinki'),
(373, 'Europe/Kiev'),
(374, 'Europe/Madrid'),
(375, 'Europe/Moscow'),
(376, 'Europe/Prague'),
(377, 'Europe/Sarajevo'),
(378, 'Europe/Tallinn'),
(379, 'Europe/Vatican'),
(380, 'Europe/Zagreb'),
(381, 'Europe/Athens'),
(382, 'Europe/Brussels'),
(383, 'Europe/Copenhagen'),
(384, 'Europe/Isle_of_Man'),
(385, 'Europe/Lisbon'),
(386, 'Europe/Malta'),
(387, 'Europe/Nicosia'),
(388, 'Europe/Riga'),
(389, 'Europe/Simferopol'),
(390, 'Europe/Tirane'),
(391, 'Europe/Vienna'),
(392, 'Europe/Zaporozhye'),
(393, 'Europe/Belfast'),
(394, 'Europe/Bucharest'),
(395, 'Europe/Dublin'),
(396, 'Europe/Istanbul'),
(397, 'Europe/Ljubljana'),
(398, 'Europe/Mariehamn'),
(399, 'Europe/Oslo'),
(400, 'Europe/Rome'),
(401, 'Europe/Skopje'),
(402, 'Europe/Tiraspol'),
(403, 'Europe/Vilnius'),
(404, 'Europe/Zurich'),
(405, 'Europe/Belgrade'),
(406, 'Europe/Budapest'),
(407, 'Europe/Gibraltar'),
(408, 'Europe/Jersey'),
(409, 'Europe/London'),
(410, 'Europe/Minsk'),
(411, 'Europe/Paris'),
(412, 'Europe/Samara'),
(413, 'Europe/Sofia'),
(414, 'Europe/Uzhgorod'),
(415, 'Europe/Volgograd'),
(416, 'Indian/Antananarivo'),
(417, 'Indian/Kerguelen'),
(418, 'Indian/Reunion'),
(419, 'Indian/Chagos'),
(420, 'Indian/Mahe'),
(421, 'Indian/Christmas'),
(422, 'Indian/Maldives'),
(423, 'Indian/Cocos'),
(424, 'Indian/Mauritius'),
(425, 'Indian/Comoro'),
(426, 'Indian/Mayotte'),
(427, 'Pacific/Apia'),
(428, 'Pacific/Efate'),
(429, 'Pacific/Galapagos'),
(430, 'Pacific/Johnston'),
(431, 'Pacific/Marquesas'),
(432, 'Pacific/Noumea'),
(433, 'Pacific/Ponape'),
(434, 'Pacific/Tahiti'),
(435, 'Pacific/Wallis'),
(436, 'Pacific/Auckland'),
(437, 'Pacific/Enderbury'),
(438, 'Pacific/Gambier'),
(439, 'Pacific/Kiritimati'),
(440, 'Pacific/Midway'),
(441, 'Pacific/Pago_Pago'),
(442, 'Pacific/Port_Moresby'),
(443, 'Pacific/Tarawa'),
(444, 'Pacific/Yap'),
(445, 'Pacific/Chatham'),
(446, 'Pacific/Fakaofo'),
(447, 'Pacific/Guadalcanal'),
(448, 'Pacific/Kosrae'),
(449, 'Pacific/Nauru'),
(450, 'Pacific/Palau'),
(451, 'Pacific/Rarotonga'),
(452, 'Pacific/Tongatapu'),
(453, 'Pacific/Chuuk'),
(454, 'Pacific/Fiji'),
(455, 'Pacific/Guam'),
(456, 'Pacific/Kwajalein'),
(457, 'Pacific/Niue'),
(458, 'Pacific/Pitcairn'),
(459, 'Pacific/Saipan'),
(460, 'Pacific/Truk'),
(461, 'Pacific/Easter'),
(462, 'Pacific/Funafuti'),
(463, 'Pacific/Honolulu'),
(464, 'Pacific/Majuro'),
(465, 'Pacific/Norfolk'),
(466, 'Pacific/Pohnpei'),
(467, 'Pacific/Samoa'),
(468, 'Pacific/Wake'),
(469, 'Brazil/Acre'),
(470, 'Canada/Central'),
(471, 'Canada/Pacific'),
(472, 'Chile/EasterIsland'),
(473, 'Eire'),
(474, 'Etc/GMT+1'),
(475, 'Etc/GMT+3'),
(476, 'Etc/GMT+8'),
(477, 'Etc/GMT-11'),
(478, 'Etc/GMT-3'),
(479, 'Etc/GMT-8'),
(480, 'Etc/Universal'),
(481, 'GB-Eire'),
(482, 'Greenwich'),
(483, 'Israel'),
(484, 'MET'),
(485, 'MST7MDT'),
(486, 'Portugal'),
(487, 'Singapore'),
(488, 'US/Aleutian'),
(489, 'US/Hawaii'),
(490, 'US/Pacific-New'),
(491, 'Brazil/DeNoronha'),
(492, 'Canada/East-Saskatchewan'),
(493, 'Canada/Saskatchewan'),
(494, 'CST6CDT'),
(495, 'EST'),
(496, 'Etc/GMT+10'),
(497, 'Etc/GMT+4'),
(498, 'Etc/GMT+9'),
(499, 'Etc/GMT-12'),
(500, 'Etc/GMT-4'),
(501, 'Etc/GMT-9'),
(502, 'Etc/UTC'),
(503, 'GMT'),
(504, 'Hongkong'),
(505, 'Jamaica'),
(506, 'Mexico/BajaNorte'),
(507, 'Navajo'),
(508, 'PRC'),
(509, 'Turkey'),
(510, 'US/Arizona'),
(511, 'US/Indiana-Starke'),
(512, 'US/Samoa'),
(513, 'Brazil/East'),
(514, 'Canada/Eastern'),
(515, 'Canada/Yukon'),
(516, 'Cuba'),
(517, 'EST5EDT'),
(518, 'Etc/GMT+11'),
(519, 'Etc/GMT+5'),
(520, 'Etc/GMT-0'),
(521, 'Etc/GMT-13'),
(522, 'Etc/GMT-5'),
(523, 'Etc/GMT0'),
(524, 'Etc/Zulu'),
(525, 'GMT+0'),
(526, 'HST'),
(527, 'Japan'),
(528, 'Mexico/BajaSur'),
(529, 'NZ'),
(530, 'PST8PDT'),
(531, 'UCT'),
(532, 'US/Central'),
(533, 'US/Michigan'),
(534, 'UTC'),
(535, 'Brazil/West'),
(536, 'Canada/Mountain'),
(537, 'CET'),
(538, 'EET'),
(539, 'Etc/GMT'),
(540, 'Etc/GMT+12'),
(541, 'Etc/GMT+6'),
(542, 'Etc/GMT-1'),
(543, 'Etc/GMT-14'),
(544, 'Etc/GMT-6'),
(545, 'Etc/Greenwich'),
(546, 'Factory'),
(547, 'GMT-0'),
(548, 'Iceland'),
(549, 'Kwajalein'),
(550, 'Mexico/General'),
(551, 'NZ-CHAT'),
(552, 'ROC'),
(553, 'Universal'),
(554, 'US/East-Indiana'),
(555, 'US/Mountain'),
(556, 'W-SU'),
(557, 'Canada/Atlantic'),
(558, 'Canada/Newfoundland'),
(559, 'Chile/Continental'),
(560, 'Egypt'),
(561, 'Etc/GMT+0'),
(562, 'Etc/GMT+2'),
(563, 'Etc/GMT+7'),
(564, 'Etc/GMT-10'),
(565, 'Etc/GMT-2'),
(566, 'Etc/GMT-7'),
(567, 'Etc/UCT'),
(568, 'GB'),
(569, 'GMT0'),
(570, 'Iran'),
(571, 'Libya'),
(572, 'MST'),
(573, 'Poland'),
(574, 'ROK'),
(575, 'US/Alaska'),
(576, 'US/Eastern'),
(577, 'US/Pacific'),
(578, 'WET');

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE IF NOT EXISTS `transaction` (
  `transaction_id` int(11) NOT NULL,
  `user_id` int(100) NOT NULL,
  `equity_id` int(11) NOT NULL,
  `perk_id` int(11) NOT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `listing_fee` varchar(255) DEFAULT NULL,
  `pay_fee` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `host_ip` varchar(255) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `paypal_email` varchar(255) DEFAULT NULL,
  `transaction_date_time` datetime NOT NULL,
  `preapproval_key` varchar(255) DEFAULT NULL,
  `preapproval_status` varchar(100) DEFAULT NULL,
  `preapproval_total_amount` varchar(100) DEFAULT NULL,
  `payment_status` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `transaction`
--

INSERT INTO `transaction` (`transaction_id`, `user_id`, `equity_id`, `perk_id`, `amount`, `listing_fee`, `pay_fee`, `email`, `host_ip`, `comment`, `paypal_email`, `transaction_date_time`, `preapproval_key`, `preapproval_status`, `preapproval_total_amount`, `payment_status`) VALUES
(1, 2, 1, 0, '450.00', NULL, NULL, NULL, '27.109.7.130', '', NULL, '2015-10-05 11:14:02', 'INV3S7wTKBWtRHD', 'SUCCESS', '450.00', 0),
(2, 3, 1, 0, '500.00', NULL, NULL, NULL, '27.109.7.130', '', NULL, '2015-10-05 11:22:23', 'INV55mYNXKZUja7', 'SUCCESS', '500.00', 0),
(3, 8, 1, 0, '100.00', NULL, NULL, NULL, '27.109.7.130', '', NULL, '2015-10-26 19:22:47', 'INVt8mMihWAHPqM', 'SUCCESS', '100.00', 0),
(4, 11, 8, 0, '500.00', NULL, NULL, NULL, '27.109.7.130', '', NULL, '2015-10-27 15:20:43', 'INVBYRXzaOaHQ59', 'SUCCESS', '500.00', 0),
(5, 12, 8, 0, '500.00', NULL, NULL, NULL, '27.109.7.130', '', NULL, '2015-10-27 17:00:20', 'INVTn7NBeQh5ICD', 'SUCCESS', '500.00', 0),
(6, 16, 1, 0, '100.00', NULL, NULL, NULL, '27.109.7.130', 'comment test', NULL, '2015-11-20 16:13:48', 'INVAUNBb82kb6s7', 'SUCCESS', '100.00', 0),
(7, 16, 1, 0, '100.00', NULL, NULL, NULL, '27.109.7.130', '', NULL, '2015-11-20 17:27:36', 'INVk4Llcq8H3DmC', 'SUCCESS', '100.00', 0),
(8, 37, 19, 0, '500.00', NULL, NULL, NULL, '27.109.7.130', '', NULL, '2015-11-20 18:08:04', 'INVO7fnPEw2zJRW', 'SUCCESS', '500.00', 0),
(9, 37, 19, 0, '500.00', NULL, NULL, NULL, '27.109.7.130', '', NULL, '2015-11-20 18:29:03', 'INVEtlP0MaeWYar', 'SUCCESS', '500.00', 0),
(10, 11, 24, 0, '200.00', NULL, NULL, NULL, '27.109.7.130', 'Lorem Ipsum has been the industry\\''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesett', NULL, '2015-11-30 13:48:55', 'INVQ2bK2cRcOtOF', 'SUCCESS', '200.00', 0),
(11, 12, 24, 4, '1000.00', NULL, NULL, NULL, '27.109.7.130', '', NULL, '2015-11-30 14:39:29', 'INV8Bdco0a7edgY', 'SUCCESS', '1000.00', 0),
(12, 11, 24, 0, '500.00', NULL, NULL, NULL, '27.109.7.130', 'Herry Patel', NULL, '2015-12-01 11:03:50', 'INVqFipS00KnHta', 'SUCCESS', '500.00', 0),
(13, 45, 24, 0, '500.00', NULL, NULL, NULL, '27.109.7.130', 'vatsal.test.rockersinfo@gmail.com', NULL, '2015-12-02 12:05:34', 'INV8SRkYfPwMImW', 'SUCCESS', '500.00', 0),
(14, 43, 1, 0, '400.00', NULL, NULL, NULL, '27.109.7.130', 'testing donation issues', NULL, '2015-12-02 12:22:17', 'INVTtx9T1J8DSOJ', 'SUCCESS', '400.00', 0),
(15, 45, 24, 0, '100.00', NULL, NULL, NULL, '123.201.160.118', 'adsfadfadf', NULL, '2015-12-18 14:59:27', 'INVi6TSRFtLaSEb', 'SUCCESS', '100.00', 0),
(16, 45, 24, 0, '100.00', NULL, NULL, NULL, '203.109.65.159', 'ajsjdaf', NULL, '2015-12-21 12:10:00', 'INVBjwf91KUn04L', 'SUCCESS', '100.00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `transaction_type`
--

CREATE TABLE IF NOT EXISTS `transaction_type` (
  `transaction_type_id` int(11) NOT NULL,
  `transaction_type_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `transaction_type`
--

INSERT INTO `transaction_type` (`transaction_type_id`, `transaction_type_name`) VALUES
(1, 'cash'),
(2, 'check'),
(3, 'credit card');

-- --------------------------------------------------------

--
-- Table structure for table `translation`
--

CREATE TABLE IF NOT EXISTS `translation` (
  `translation_id` int(11) NOT NULL,
  `language` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `translation`
--

INSERT INTO `translation` (`translation_id`, `language`) VALUES
(2, 'English'),
(9, 'French');

-- --------------------------------------------------------

--
-- Table structure for table `twitter_setting`
--

CREATE TABLE IF NOT EXISTS `twitter_setting` (
  `twitter_setting_id` int(11) NOT NULL,
  `twitter_enable` varchar(255) DEFAULT NULL,
  `twitter_user_name` varchar(255) DEFAULT NULL,
  `consumer_key` varchar(255) DEFAULT NULL,
  `consumer_secret` varchar(255) DEFAULT NULL,
  `tw_oauth_token` varchar(500) DEFAULT NULL,
  `tw_oauth_token_secret` varchar(500) DEFAULT NULL,
  `autopost_site` varchar(255) DEFAULT NULL,
  `autopost_user` varchar(255) DEFAULT NULL,
  `twitter_url` text,
  `twiter_img` varchar(255) DEFAULT NULL,
  `tw_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `twitter_setting`
--

INSERT INTO `twitter_setting` (`twitter_setting_id`, `twitter_enable`, `twitter_user_name`, `consumer_key`, `consumer_secret`, `tw_oauth_token`, `tw_oauth_token_secret`, `autopost_site`, `autopost_user`, `twitter_url`, `twiter_img`, `tw_id`) VALUES
(1, '1', NULL, 'Z5a5RLmd27m8s0TOSAnG0nics', 'O0LR0wJuh5HraHyCvZsqZNEogOLZUlfgag8Qn8KSF01Z0fVhUA', NULL, 'Chintan2Rockers', NULL, NULL, 'https://www.linkedin.com/in/fundraisingscript', 'anita_rockers.jpg', '624506841');

-- --------------------------------------------------------

--
-- Table structure for table `updates`
--

CREATE TABLE IF NOT EXISTS `updates` (
  `update_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `updates` text,
  `image` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `equity_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `updates`
--

INSERT INTO `updates` (`update_id`, `project_id`, `updates`, `image`, `status`, `date_added`, `equity_id`) VALUES
(6, 0, '\n                                    <p>abcdefghijklmnopqfxgnsgffhg<br></p>                                ', NULL, '0', '2015-11-19 15:48:30', 8),
(7, 0, '<p>New Update 26/11<br></p>', NULL, '0', '2015-11-26 15:13:40', 8),
(24, 0, '<p>Ut eget urna efficitur, semper dolor non, pretium risus. Curabitur risus massa, feugiat sit amet vulputate non, ultricies sit amet libero. Nunc eros sem, commodo ac enim at, auctor cursus mauris. Cras tincidunt, lectus at efficitur eleifend, mi turpis imperdiet dolor, vitae viverra mauris est et libero. Aliquam erat volutpat. Curabitur nec dapibus augue. Cras elementum eros quis nunc faucibus placerat elementum ac nisi.<br></p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer efficitur diam vel sem fermentum, eu elementum augue aliquet. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam lacinia urna nisi, nec fringilla urna cursus at. Morbi cursus massa lectus, vitae auctor mauris mollis ac. Nullam tempus mattis vehicula.&nbsp;<br></p>', NULL, '0', '2015-12-02 12:15:48', 1),
(25, 0, '\n                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer efficitur diam vel sem fermentum, eu elementum augue aliquet. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam lacinia urna nisi, nec fringilla urna cursus at. Morbi cursus massa lectus, vitae auctor mauris mollis ac. Nullam tempus mattis vehicula. Ut eget urna efficitur, semper dolor non, pretium risus.&nbsp;</p><p>Curabitur risus massa, feugiat sit amet vulputate non, ultricies sit amet libero. Nunc eros sem, commodo ac enim at, auctor cursus mauris. Cras tincidunt, lectus at efficitur eleifend, mi turpis imperdiet dolor, vitae viverra mauris est et libero. Aliquam erat volutpat. Curabitur nec dapibus augue. Cras elementum eros quis nunc faucibus placerat elementum ac nisi.<br></p>                                ', NULL, '0', '2015-12-02 12:15:32', 1),
(26, 0, '<p>ABCDEF<br></p>', NULL, '0', '2015-11-27 11:19:18', 8),
(27, 0, '\n                                    <p>ABCDE</p><p>New Update<br></p>                                ', NULL, '0', '2015-12-01 15:37:48', 24),
(28, 0, '<p>New Update<br></p>', NULL, '0', '2015-12-02 11:48:18', 24);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `gender` varchar(50) DEFAULT NULL,
  `is_admin` int(10) NOT NULL DEFAULT '0',
  `fb_uid` varchar(100) DEFAULT NULL,
  `tw_id` varchar(255) DEFAULT NULL,
  `tw_screen_name` varchar(255) DEFAULT NULL,
  `tw_oauth_token` text,
  `tw_oauth_token_secret` text,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `zip_code` varchar(255) DEFAULT NULL,
  `paypal_email` varchar(255) DEFAULT NULL,
  `paypal_verified` varchar(10) DEFAULT NULL,
  `signup_ip` varchar(255) DEFAULT NULL,
  `active` varchar(255) DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `amazon_token_id` varchar(255) DEFAULT NULL,
  `refund_token_id` varchar(255) DEFAULT NULL,
  `user_about` text,
  `user_website` varchar(255) DEFAULT NULL,
  `user_occupation` varchar(255) DEFAULT NULL,
  `user_interest` text,
  `user_skill` text,
  `facebook_url` varchar(255) DEFAULT NULL,
  `twitter_url` varchar(255) DEFAULT NULL,
  `linkedln_url` varchar(255) DEFAULT NULL,
  `googleplus_url` varchar(255) DEFAULT NULL,
  `bandcamp_url` varchar(255) DEFAULT NULL,
  `youtube_url` varchar(255) DEFAULT NULL,
  `myspace_url` varchar(255) DEFAULT NULL,
  `confirm_key` varchar(25) DEFAULT NULL,
  `unique_code` varchar(255) DEFAULT NULL,
  `reference_user_id` int(100) DEFAULT NULL,
  `enable_facebook_stream` tinyint(1) NOT NULL,
  `enable_twitter_stream` tinyint(1) NOT NULL,
  `image_no` varchar(50) DEFAULT NULL,
  `fb_access_token` varchar(200) DEFAULT NULL,
  `forgot_unique_code` varchar(225) DEFAULT NULL,
  `request_date` datetime NOT NULL,
  `facebook_wall_post` int(1) NOT NULL,
  `autopost_site` int(1) NOT NULL,
  `admin_active` tinyint(1) NOT NULL,
  `suspend_reason` text,
  `linkdin_id` varchar(200) DEFAULT NULL,
  `subscribe_id` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=''no newsletter'',1=''registered'',2=''subscriber''',
  `bank_account` varchar(255) DEFAULT NULL,
  `bank_account_country` varchar(255) DEFAULT NULL,
  `routing_number` int(11) NOT NULL,
  `account_number` int(11) NOT NULL,
  `bank_token_id` varchar(255) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `profile_slug` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_name`, `last_name`, `email`, `password`, `image`, `gender`, `is_admin`, `fb_uid`, `tw_id`, `tw_screen_name`, `tw_oauth_token`, `tw_oauth_token_secret`, `address`, `city`, `state`, `country`, `zip_code`, `paypal_email`, `paypal_verified`, `signup_ip`, `active`, `date_added`, `amazon_token_id`, `refund_token_id`, `user_about`, `user_website`, `user_occupation`, `user_interest`, `user_skill`, `facebook_url`, `twitter_url`, `linkedln_url`, `googleplus_url`, `bandcamp_url`, `youtube_url`, `myspace_url`, `confirm_key`, `unique_code`, `reference_user_id`, `enable_facebook_stream`, `enable_twitter_stream`, `image_no`, `fb_access_token`, `forgot_unique_code`, `request_date`, `facebook_wall_post`, `autopost_site`, `admin_active`, `suspend_reason`, `linkdin_id`, `subscribe_id`, `bank_account`, `bank_account_country`, `routing_number`, `account_number`, `bank_token_id`, `bank_name`, `profile_slug`) VALUES
(1, 'Jayshree', 'Patel', 'jayshree.rockersinfo@gmail.com', '25d55ad283aa400af464c76d713c07ad', '77654-user-1448023669.jpg', NULL, 0, NULL, NULL, NULL, NULL, NULL, 'Vadodara, Gujarat, India', NULL, NULL, NULL, 'V6B 2R4', NULL, NULL, '27.109.7.130', '1', '2015-10-02 16:36:27', NULL, NULL, '', NULL, 'Software developer', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'jN78uLuAuUbF', NULL, 0, 0, NULL, NULL, 'x4t86XSw72a8zwPoO9qf', '2015-11-19 11:32:09', 0, 0, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, 'JayshreePatel1'),
(2, 'Jayu', 'Patel', 'jayshree.test.rockersinfo@gmail.com', '25d55ad283aa400af464c76d713c07ad', '1496-user-1444022741.jpg', NULL, 0, NULL, NULL, NULL, NULL, NULL, 'Mexico, MO 65265, USA', NULL, NULL, NULL, '65265', NULL, NULL, '27.109.7.130', '1', '2015-10-05 10:52:39', NULL, NULL, 'Jr.QA', NULL, 'Jr.QA', 'Jr.QA', 'Jr.QA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'tsx9r2QEPfiT', NULL, 0, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', 0, 0, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, 'JayuPatel2'),
(4, 'kashyap', 'gandhi', 'kashyap.rockersinfo@gmail.com', '25d55ad283aa400af464c76d713c07ad', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '27.109.7.130', '1', '2015-10-05 14:26:15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'mhP2f3eZVXae', NULL, 0, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', 0, 0, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, 'kashyapgandhi4'),
(5, 'Jigar', 'Gandhi', 'jigar.rockersinfo@gmail.com', '25d55ad283aa400af464c76d713c07ad', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '27.109.7.130', '1', '2015-10-05 15:56:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'VaM8TGg26NJX', NULL, 0, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', 0, 0, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, 'JigarGandhi5'),
(6, 'pratik', 'parmar', 'pratik.parmar371987@gmail.com', '25d55ad283aa400af464c76d713c07ad', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '123.201.91.105', '1', '2015-10-08 12:15:51', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'et8VKg9qES9p', NULL, 0, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', 0, 0, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, 'pratikparmar6'),
(7, 'Sandy', 'Preston', 'spreston145@gmail.com', '25d55ad283aa400af464c76d713c07ad', 'spreston145.jpg', NULL, 0, '173898649626182', '3637391834', 'spreston145', '3637391834-nAhcJeg8332mAHmSvzopZ6Mm0M7omEJT3YJ3ks3', 'pJrgkLCkxdtVbcIQ0vJ2rsWLoi0VXB4OQPcUPsGuN7jhg', 'Vadodara, Gujarat, India', NULL, NULL, NULL, '369852', NULL, NULL, '123.201.58.117', '1', '2015-10-22 15:22:00', NULL, NULL, '', '', 'Businessman', '', '', '', '', '', '', '', '', '', 'NfLT1fXhwc0MIyr15pO7', 'nj8jnnHpHZnM', NULL, 0, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', 0, 0, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, 'sandyPreston7'),
(8, 'Hardik', 'Jivani', 'hardik.rockersinfo@gmail.com', '2a6725240e76aa916db5ada568388d6c', '18138-user-1445841791.jpg', NULL, 0, NULL, NULL, NULL, NULL, NULL, 'Vadodara, Gujarat, India', NULL, NULL, NULL, '390010', NULL, NULL, '219.91.173.161', '1', '2015-10-26 12:05:51', NULL, NULL, '', NULL, 'designer', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'ReMb2pZpjPzb', NULL, 0, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', 0, 0, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, 'HardikJivani8'),
(10, 'Sweetu', 'Patel', 'jayshree.test01.rockersinfo@gmail.com', '25d55ad283aa400af464c76d713c07ad', '41867-user-1445852548.png', NULL, 0, NULL, NULL, NULL, NULL, NULL, 'Mexico, MO 65265, USA', NULL, NULL, NULL, '65265', NULL, NULL, '27.109.7.130', '1', '2015-10-26 15:07:38', NULL, NULL, '', NULL, 'Jr.QA', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'Qj8v2HKRFem5', NULL, 0, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', 0, 0, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, 'SweetuPatel10'),
(11, 'Herry', 'Patel', 'herrypatel365@gmail.com', '25d55ad283aa400af464c76d713c07ad', NULL, NULL, 0, NULL, '3713367733', 'pa_hir', '3713367733-4p2DIP2PwSr6NIDKdoz6dsciRDbCJ9plC0FUVyw', 'WL1st2F3vWy3VMl8r8P7xVA0lcECQ5tjx0KAvCfOS5qZr', 'Vadodara, Gujarat, India', 'Vadodara', ' Gujarat', ' India', NULL, NULL, NULL, '150.107.209.40', '1', '2015-10-27 11:15:06', NULL, NULL, '', '', 'Businessman', 'Interest', 'Skills', '', '', '', '', '', '', '', 'Lo2kfxSBDbObbKeeEsfO', 'ktqErFpjEYoS', NULL, 0, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', 0, 0, 1, '', NULL, 0, NULL, NULL, 0, 0, NULL, NULL, 'herryPatel11'),
(12, 'Anky', 'Patel', 'ankypatel367@gmail.com', '25d55ad283aa400af464c76d713c07ad', 'Ankit_ankyP.jpg', NULL, 0, NULL, '3712368078', 'Ankit_ankyP', '3712368078-uJxJ7hfoaAL2iujeQC2hUH29DFohsGcTlghAKIJ', 'CLw92DNrOsVBdmpYaCPVnKJWGDu0x4PP3Xv5HxK7VmIax', 'Vadodara, Gujarat, India', NULL, NULL, NULL, '369852', NULL, NULL, '27.109.7.130', '1', '2015-10-27 16:56:28', NULL, NULL, '', '', 'Businessman', '', '', '', '', '', '', '', '', '', 'C3mgB1nIGUsS6PZ8Pzhh', 'bUDGaBFbfCuS', NULL, 0, 0, NULL, NULL, '', '0000-00-00 00:00:00', 0, 0, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, 'ankyPatel12'),
(13, 'admin', '', 'ankit.rockersinfo@gmail.com', '25d55ad283aa400af464c76d713c07ad', NULL, NULL, 0, '462187370634726', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '27.109.7.130', '1', '2015-10-28 12:12:53', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'LThf2dYWO2oaeyj8Osm7', 'r6QFVYAJyFCa', NULL, 0, 0, NULL, NULL, '', '0000-00-00 00:00:00', 0, 0, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, 'admin13'),
(16, 'Timnew', 'Rocknew', 'rockertest1@gmail.com', '25d55ad283aa400af464c76d713c07ad', '10879-user-1447926597.jpg', NULL, 0, NULL, NULL, NULL, NULL, NULL, 'add1', NULL, NULL, NULL, '123', NULL, NULL, '27.109.7.130', '1', '2015-11-19 14:13:35', NULL, NULL, '', NULL, 'occupation', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'kZdqxLxekuho', NULL, 0, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', 0, 0, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, 'TimRocknew16'),
(18, 'sandy', 'preston', 'sandy.roc145@gmail.com', '', '111601812539716.jpg', NULL, 0, '111601812539716', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '27.109.7.130', '1', '2015-11-19 14:34:10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9Jvz2dcciHLq', NULL, 0, 0, NULL, '', NULL, '0000-00-00 00:00:00', 0, 0, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, 'sandypreston18'),
(20, 'Jay', 'P', 'jayshree.test.rockersinfo.1@gmail.com', '25d55ad283aa400af464c76d713c07ad', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '27.109.7.130', NULL, '2015-11-19 18:05:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9ZApGYFNp8U4gUoG00sr', 'EtGFSYLFYx3X', NULL, 0, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', 0, 0, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, 'JayP20'),
(21, 'Ritik', 'Roshan', 'sp.reston145@gmail.com', '25d55ad283aa400af464c76d713c07ad', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '27.109.7.130', '1', '2015-11-19 18:05:28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'zbGKCTR5qd7D', NULL, 0, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', 0, 0, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, 'RitikRoshan21'),
(22, 'Salmaan', 'Khan', 'spres.ton145@gmail.com', '25d55ad283aa400af464c76d713c07ad', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '27.109.7.130', '1', '2015-11-19 18:08:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'RSTcUZNAWwA3', NULL, 0, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', 0, 0, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, 'SalmaanKhan22'),
(23, 'Amir', 'Khan', 'spresto.n145@gmail.com', '25d55ad283aa400af464c76d713c07ad', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '27.109.7.130', '1', '2015-11-19 18:08:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'JQAVVL9sHBfg', NULL, 0, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', 0, 0, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, 'AmirKhan23'),
(24, 'ShahRukh', 'Khan', 'spr.eston145@gmail.com', '25d55ad283aa400af464c76d713c07ad', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '27.109.7.130', '1', '2015-11-19 18:09:43', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'Qmx83AFMKYo3', NULL, 0, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', 0, 0, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, 'ShahRukhKhan24'),
(25, 'John', 'Abraham', 's.preston145@gmail.com', '25d55ad283aa400af464c76d713c07ad', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '27.109.7.130', '1', '2015-11-19 18:10:25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'njr3Z5UDbpNF', NULL, 0, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', 0, 0, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, 'JohnAbraham25'),
(26, 'Jay', 'P', 'jay.shree.test.rockersinfo+1@gmail.com', '25d55ad283aa400af464c76d713c07ad', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '27.109.7.130', NULL, '2015-11-19 18:18:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7edht35NYwfGXNDdmAZK', 'ruG9szCc4hkZ', NULL, 0, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', 0, 0, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, 'JayP26'),
(27, 'Jay', 'P', 'jayshree.test.rockersinfo+1@gmail.com', '25d55ad283aa400af464c76d713c07ad', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '27.109.7.130', '1', '2015-11-19 18:20:46', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'MkmbQttvYvao', NULL, 0, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', 0, 0, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, 'JayP27'),
(28, 'Jays', 'Patel', 'jayshree.test.rockersinfo+2@gmail.com', '25d55ad283aa400af464c76d713c07ad', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '27.109.7.130', '1', '2015-11-19 18:53:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'kdvoVByKzVhj', NULL, 0, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', 0, 0, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, 'JaysPatel28'),
(29, 'Jash', 'Patel', 'jayshree.test.rockersinfo+3@gmail.com', '25d55ad283aa400af464c76d713c07ad', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '27.109.7.130', '1', '2015-11-19 18:57:56', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'B6JsKedLCcdr', NULL, 0, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', 0, 0, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, 'JashPatel29'),
(30, 'Reshma', 'Patel', 'jayshree.test.rockersinfo+4@gmail.com', '25d55ad283aa400af464c76d713c07ad', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '27.109.7.130', '1', '2015-11-19 19:00:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'QcK9uxmKJXzc', NULL, 0, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', 0, 0, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, 'ReshmaPatel30'),
(31, 'Ross', 'Patel', 'jayshree.test.rockersinfo+5@gmail.com', '25d55ad283aa400af464c76d713c07ad', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '27.109.7.130', '1', '2015-11-19 19:01:56', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'VaNKKWRLX8Zd', NULL, 0, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', 0, 0, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, 'RossPatel31'),
(32, 'Kiran', 'Patel', 'jayshree.test.rockersinfo+6@gmail.com', '25d55ad283aa400af464c76d713c07ad', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '27.109.7.130', '1', '2015-11-19 19:03:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'gD98m5ENXuqj', NULL, 0, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', 0, 0, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, 'KiranPatel32'),
(37, 'Stevenew', 'Rocknew', 'rockertest2@gmail.com', '25d55ad283aa400af464c76d713c07ad', '13323-user-1448021709.jpeg', NULL, 0, NULL, NULL, NULL, NULL, NULL, 'address1', NULL, NULL, NULL, '123456', NULL, NULL, '27.109.7.130', '1', '2015-11-20 17:40:13', NULL, NULL, '', NULL, 'occupation', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'oNysyGTAXEQh', NULL, 0, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', 0, 0, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, 'Stevenew37'),
(38, 'Dev', 'Patel', 'dev1.rockersinfo@gmail.com', '25d55ad283aa400af464c76d713c07ad', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '27.109.7.130', '1', '2015-11-26 10:45:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '6ny6CHQ6MK6q', NULL, 0, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', 0, 0, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, 'DevPatel38'),
(39, 'Dev', 'Patel', 'dev1122.rockersinfo@gmail.com', '25d55ad283aa400af464c76d713c07ad', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '27.109.7.130', NULL, '2015-11-26 15:40:49', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'm2eqaFF1sJgs1lBb0nlO', '3a3hBMh8DGdR', NULL, 0, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', 0, 0, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, 'DevPatel39'),
(40, 'ABC', 'DEF', 'spreston145+1@gmail.com', '25d55ad283aa400af464c76d713c07ad', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 'Vadodara, Gujarat, India', 'Vadodara', ' Gujarat', ' India', NULL, NULL, NULL, '27.109.7.130', '1', '2015-11-30 15:29:22', NULL, NULL, '', '', '', '', '', '', '', '', '', '', '', '', NULL, 'oEosYTaYGSq4', NULL, 0, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', 0, 0, 1, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, NULL),
(43, 'rakesh', 'patel', 'rockersinfo@gmail.com', '25d55ad283aa400af464c76d713c07ad', '94778-user-1448956449.jpg', NULL, 0, NULL, NULL, NULL, NULL, NULL, 'Vadod, Gujarat 388370, India', NULL, NULL, NULL, '388370', NULL, NULL, '123.201.161.193', '1', '2015-12-01 12:26:53', NULL, NULL, 'About Rockers', NULL, 'Prgrammr', 'Coding', 'PHP', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'K6ur6xia4srn', NULL, 0, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', 0, 0, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, 'rakeshpatel43'),
(44, 'Vince', 'Preston', 'vince.preston.v@gmail.com', '25d55ad283aa400af464c76d713c07ad', '176862885993630.jpg', NULL, 0, '176862885993630', NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '369822', NULL, NULL, '27.109.7.130', '1', '2015-12-02 10:39:23', NULL, NULL, 'asdf', '', 'asdf', 'asdf', 'asdf', '', '', '', '', '', '', '', 'SCLiN2y40Fy5oCG91kSE', 'nPv5Y7Db8WGq', NULL, 0, 0, NULL, '', NULL, '0000-00-00 00:00:00', 0, 0, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, 'vincepreston44'),
(45, 'Vashu', 'Patel', 'vatsal.test.rockersinfo@gmail.com', '25d55ad283aa400af464c76d713c07ad', 'TestVatsal.jpg', NULL, 0, '116070702095183', '4419825138', 'TestVatsal', '4419825138-vZPDqbxMnlXCvTndjs0VWGJYbcI54pniv6NC4h3', 'OGigiezRi77o3A8jhZAaNzNQ3mk6s06riNptWGj6APOja', 'Vadodara, Gujarat, India', NULL, NULL, NULL, '369852', NULL, NULL, '27.109.7.130', '1', '2015-12-02 10:45:35', NULL, NULL, 'asdf', '', 'asdf', 'asdf', 'asdf', '', '', '', '', '', '', '', 'qmnasQ8K9dRCZ1RomO44', 'XUtHLRMbDFZF', NULL, 0, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', 0, 0, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, 'vashuPatel45'),
(46, 'Vikram', 'Patel', 'vatsal.test1.rockersinfo@gmail.com', '25d55ad283aa400af464c76d713c07ad', '47099-user-1449042754.JPG', NULL, 0, NULL, NULL, NULL, NULL, NULL, 'Vadodara, Gujarat, India', NULL, NULL, NULL, '369852', NULL, NULL, '27.109.7.130', '1', '2015-12-02 12:11:59', NULL, NULL, '', NULL, 'Occupation', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'FQpcpUFJDM6K', NULL, 0, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', 0, 0, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, 'VikramPatel46'),
(47, 'Siddharth', 'Muley', 'smuley1991@gmail.com', 'e6def50a41c3716ce8b456a8839521f4', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '27.109.7.130', '1', '2015-12-04 17:40:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'CvqgQm2aPYJX', NULL, 0, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', 0, 0, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, 'SiddharthMuley47');

-- --------------------------------------------------------

--
-- Table structure for table `user_activity`
--

CREATE TABLE IF NOT EXISTS `user_activity` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `module` varchar(255) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `datetime` datetime NOT NULL,
  `action_detail` text,
  `second_user_id` int(10) NOT NULL DEFAULT '0',
  `campaign_id` int(10) NOT NULL DEFAULT '0',
  `read` tinyint(2) NOT NULL DEFAULT '0',
  `company_id` int(11) NOT NULL,
  `type` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2845 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_activity`
--

INSERT INTO `user_activity` (`id`, `user_id`, `module`, `action`, `datetime`, `action_detail`, `second_user_id`, `campaign_id`, `read`, `company_id`, `type`) VALUES
(2388, 11, 'project', 'upload_contract', '2015-12-01 10:58:58', 'You have <a href="http://mydesichef.com/realestate_development_ci3/startinvest/index/24">Uploaded</a> Investment process  contract copy on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 1, 0, NULL),
(2389, 11, 'project', 'approve_contract', '2015-12-01 11:01:53', 'Your Investment contract copy has been <a href="http://mydesichef.com/realestate_development_ci3/startinvest/index/24">Approve</a> on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a> by Real Estate team', 0, 24, 1, 0, NULL),
(2390, 11, 'project', 'approve_acknowledge_contract', '2015-12-01 11:03:50', 'Your Investment  acknowledgement has been <a href="http://mydesichef.com/realestate_development_ci3/startinvest/index/24">Approved</a> on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a> by Real Estate team', 0, 24, 1, 0, NULL),
(2391, 11, 'project', 'funded', '2015-12-01 11:03:50', 'You have Invested <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a> with $500.00 USD', 7, 24, 1, 0, NULL),
(2392, 7, 'project', 'funded', '2015-12-01 11:03:50', '<a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a> you already created is Invested with $500.00 USD by <a href="http://mydesichef.com/realestate_development_ci3/user/herryPatel11">Herry Patel</a>', 11, 24, 1, 0, NULL),
(2393, 12, 'project', 'funded', '2015-12-01 11:03:50', '<a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a> you already Invested is Invested with $500.00 USD by <a href="http://mydesichef.com/realestate_development_ci3/user/herryPatel11">Herry Patel</a>', 7, 24, 1, 0, NULL),
(2394, 11, 'project', 'confirmed_submmited_tracking', '2015-12-01 11:04:32', 'You have <a href="http://mydesichef.com/realestate_development_ci3/startinvest/index/24">Approved and Confirmed</a> delivery of  share certificate/document  on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 1, 0, NULL),
(2395, 1, 'User', 'follow', '2015-12-01 12:34:04', 'You have started Following <a href="http://mydesichef.com/realestate_development_ci3/user/herryPatel11">Herry Patel</a>', 11, 0, 1, 0, NULL),
(2396, 11, 'User', 'follow', '2015-12-01 12:34:04', '<a href="http://mydesichef.com/realestate_development_ci3/user/JayshreePatel1">Jayshree Patel</a> has started Following you.', 1, 0, 1, 0, NULL),
(2397, 1, 'User', 'follow', '2015-12-01 12:35:17', 'You have started Following <a href="http://mydesichef.com/realestate_development_ci3/user/JohnAbraham25">John Abraham</a>', 25, 0, 1, 0, NULL),
(2398, 25, 'User', 'follow', '2015-12-01 12:35:17', '<a href="http://mydesichef.com/realestate_development_ci3/user/JayshreePatel1">Jayshree Patel</a> has started Following you.', 1, 0, 0, 0, NULL),
(2399, 1, 'User', 'unfollow', '2015-12-01 12:38:22', 'You have started Unfollowing <a href="http://mydesichef.com/realestate_development_ci3/user/JohnAbraham25">John Abraham</a>', 25, 0, 1, 0, NULL),
(2400, 25, 'User', 'unfollow', '2015-12-01 12:38:22', '<a href="http://mydesichef.com/realestate_development_ci3/user/JayshreePatel1">Jayshree Patel</a> has started Unfollowing you.', 1, 0, 0, 0, NULL),
(2401, 1, 'User', 'follow', '2015-12-01 12:40:46', 'You have started Following <a href="http://mydesichef.com/realestate_development_ci3/user/JohnAbraham25">John Abraham</a>', 25, 0, 1, 0, NULL),
(2402, 25, 'User', 'follow', '2015-12-01 12:40:46', '<a href="http://mydesichef.com/realestate_development_ci3/user/JayshreePatel1">Jayshree Patel</a> has started Following you.', 1, 0, 0, 0, NULL),
(2403, 1, 'User', 'unfollow', '2015-12-01 12:44:37', 'You have started Unfollowing <a href="http://mydesichef.com/realestate_development_ci3/user/JohnAbraham25">John Abraham</a>', 25, 0, 1, 0, NULL),
(2404, 25, 'User', 'unfollow', '2015-12-01 12:44:37', '<a href="http://mydesichef.com/realestate_development_ci3/user/JayshreePatel1">Jayshree Patel</a> has started Unfollowing you.', 1, 0, 0, 0, NULL),
(2405, 1, 'User', 'follow', '2015-12-01 12:46:07', 'You have started Following <a href="http://mydesichef.com/realestate_development_ci3/user/JohnAbraham25">John Abraham</a>', 25, 0, 1, 0, NULL),
(2406, 25, 'User', 'follow', '2015-12-01 12:46:07', '<a href="http://mydesichef.com/realestate_development_ci3/user/JayshreePatel1">Jayshree Patel</a> has started Following you.', 1, 0, 0, 0, NULL),
(2407, 1, 'User', 'follow', '2015-12-01 12:47:23', 'You have started Following <a href="http://mydesichef.com/realestate_development_ci3/user/AmirKhan23">Amir Khan</a>', 23, 0, 1, 0, NULL),
(2408, 23, 'User', 'follow', '2015-12-01 12:47:23', '<a href="http://mydesichef.com/realestate_development_ci3/user/JayshreePatel1">Jayshree Patel</a> has started Following you.', 1, 0, 0, 0, NULL),
(2409, 1, 'User', 'unfollow', '2015-12-01 12:47:27', 'You have started Unfollowing <a href="http://mydesichef.com/realestate_development_ci3/user/AmirKhan23">Amir Khan</a>', 23, 0, 1, 0, NULL),
(2410, 23, 'User', 'unfollow', '2015-12-01 12:47:27', '<a href="http://mydesichef.com/realestate_development_ci3/user/JayshreePatel1">Jayshree Patel</a> has started Unfollowing you.', 1, 0, 0, 0, NULL),
(2411, 1, 'project', 'unfollow', '2015-12-01 12:52:55', 'You have started Unfollowing <a href="http://mydesichef.com/realestate_development_ci3/development/test-project1">Test Company1</a>', 0, 19, 1, 0, NULL),
(2412, 16, 'project', 'unfollow', '2015-12-01 12:52:55', '<a href="http://mydesichef.com/realestate_development_ci3/user/JayshreePatel1">Jayshree Patel</a> has started Unfollowing you created <a href="http://mydesichef.com/realestate_development_ci3/development/test-project1">Test Company1</a>', 1, 19, 0, 0, NULL),
(2413, 7, 'project', 'unfollow', '2015-12-01 12:52:55', '<a href="http://mydesichef.com/realestate_development_ci3/user/JayshreePatel1">Jayshree Patel</a> has started Unfollowing <a href="http://mydesichef.com/realestate_development_ci3/development/test-project1">Test Company1</a> you already Unfollowing', 0, 19, 1, 0, NULL),
(2414, 27, 'project', 'unfollow', '2015-12-01 12:52:55', '<a href="http://mydesichef.com/realestate_development_ci3/user/JayshreePatel1">Jayshree Patel</a> has started Unfollowing <a href="http://mydesichef.com/realestate_development_ci3/development/test-project1">Test Company1</a> you already Unfollowing', 0, 19, 0, 0, NULL),
(2415, 1, 'project', 'follow', '2015-12-01 12:57:44', 'You have started Following <a href="http://mydesichef.com/realestate_development_ci3/development/test-project1">Test Project</a>', 0, 19, 1, 0, NULL),
(2416, 16, 'project', 'follow', '2015-12-01 12:57:44', '<a href="http://mydesichef.com/realestate_development_ci3/user/JayshreePatel1">Jayshree Patel</a> has started Following you created <a href="http://mydesichef.com/realestate_development_ci3/development/test-project1">Test Project</a>', 1, 19, 0, 0, NULL),
(2417, 7, 'project', 'follow', '2015-12-01 12:57:44', '<a href="http://mydesichef.com/realestate_development_ci3/user/JayshreePatel1">Jayshree Patel</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/development/test-project1">Test Project</a> you already Following', 0, 19, 1, 0, NULL),
(2418, 27, 'project', 'follow', '2015-12-01 12:57:44', '<a href="http://mydesichef.com/realestate_development_ci3/user/JayshreePatel1">Jayshree Patel</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/development/test-project1">Test Project</a> you already Following', 0, 19, 0, 0, NULL),
(2419, 37, 'project', 'follow', '2015-12-01 12:57:44', '<a href="http://mydesichef.com/realestate_development_ci3/user/JayshreePatel1">Jayshree Patel</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/development/test-project1">Test Project</a> you already Invested', 0, 19, 0, 0, NULL),
(2420, 37, 'project', 'follow', '2015-12-01 12:57:44', '<a href="http://mydesichef.com/realestate_development_ci3/user/JayshreePatel1">Jayshree Patel</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/development/test-project1">Test Project</a> you already Invested', 0, 19, 0, 0, NULL),
(2421, 1, 'project', 'access_request', '2015-12-01 13:21:20', 'You have sent your access request for <a href="http://mydesichef.com/realestate_development_ci3/development/test-project1">Test Company1</a> to access ', 0, 19, 0, 0, NULL),
(2422, 16, 'project', 'access_request', '2015-12-01 13:21:20', 'You have received <a href="http://mydesichef.com/realestate_development_ci3/equity/dashboard/19">Access Request</a> to access  on <a href="http://mydesichef.com/realestate_development_ci3/development/test-project1">Test Company1</a> from <a href="http://mydesichef.com/realestate_development_ci3/user/JayshreePatel1">Jayshree Patel</a>', 1, 19, 0, 0, NULL),
(2423, 43, 'User', 'update_account', '2015-12-01 13:24:38', 'You have updated your account information', 0, 0, 0, 0, NULL),
(2424, 43, 'project', 'access_request', '2015-12-01 13:25:03', 'You have sent your access request for <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> to access ', 0, 1, 0, 0, NULL),
(2425, 1, 'project', 'access_request', '2015-12-01 13:25:03', 'You have received <a href="http://mydesichef.com/realestate_development_ci3/equity/dashboard/1">Access Request</a> to access  on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> from <a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a>', 43, 1, 0, 0, NULL),
(2426, 43, 'project', 'commented', '2015-12-01 13:26:28', 'You have Commented on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a>', 1, 1, 0, 0, NULL),
(2427, 1, 'project', 'commented', '2015-12-01 13:26:28', '<a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a> Commented on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already created', 43, 1, 0, 0, NULL),
(2428, 27, 'project', 'commented', '2015-12-01 13:26:28', '<a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a> Commented on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already Followed', 1, 1, 0, 0, NULL),
(2429, 27, 'project', 'commented', '2015-12-01 13:26:28', '<a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a> Commented on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already Followed', 1, 1, 0, 0, NULL),
(2430, 27, 'project', 'commented', '2015-12-01 13:26:28', '<a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a> Commented on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already Followed', 1, 1, 0, 0, NULL),
(2431, 27, 'project', 'commented', '2015-12-01 13:26:28', '<a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a> Commented on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already Followed', 1, 1, 0, 0, NULL),
(2432, 27, 'project', 'commented', '2015-12-01 13:26:28', '<a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a> Commented on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already Followed', 1, 1, 0, 0, NULL),
(2433, 27, 'project', 'commented', '2015-12-01 13:26:28', '<a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a> Commented on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already Followed', 1, 1, 0, 0, NULL),
(2434, 27, 'project', 'commented', '2015-12-01 13:26:28', '<a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a> Commented on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already Followed', 1, 1, 0, 0, NULL),
(2435, 27, 'project', 'commented', '2015-12-01 13:26:28', '<a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a> Commented on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already Followed', 1, 1, 0, 0, NULL),
(2436, 27, 'project', 'commented', '2015-12-01 13:26:28', '<a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a> Commented on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already Followed', 1, 1, 0, 0, NULL),
(2437, 27, 'project', 'commented', '2015-12-01 13:26:28', '<a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a> Commented on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already Followed', 1, 1, 0, 0, NULL),
(2438, 16, 'project', 'commented', '2015-12-01 13:26:28', '<a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a> Commented on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already Invested', 1, 1, 0, 0, NULL),
(2439, 16, 'project', 'commented', '2015-12-01 13:26:28', '<a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a> Commented on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already Invested', 1, 1, 0, 0, NULL),
(2440, 8, 'project', 'commented', '2015-12-01 13:26:28', '<a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a> Commented on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already Invested', 1, 1, 0, 0, NULL),
(2441, 2, 'project', 'commented', '2015-12-01 13:26:28', '<a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a> Commented on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already Invested', 1, 1, 0, 0, NULL),
(2442, 11, 'User', 'follow', '2015-12-01 13:37:26', 'You have started Following <a href="http://mydesichef.com/realestate_development_ci3/user/TimRocknew16">Timnew Rocknew</a>', 16, 0, 1, 0, NULL),
(2443, 16, 'User', 'follow', '2015-12-01 13:37:26', '<a href="http://mydesichef.com/realestate_development_ci3/user/herryPatel11">Herry Patel</a> has started Following you.', 11, 0, 0, 0, NULL),
(2444, 23, 'User', 'follow', '2015-12-01 13:37:26', '<a href="http://mydesichef.com/realestate_development_ci3/user/herryPatel11">Herry Patel</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/user/TimRocknew16">Timnew Rocknew</a>', 16, 0, 0, 0, NULL),
(2445, 22, 'User', 'follow', '2015-12-01 13:37:26', '<a href="http://mydesichef.com/realestate_development_ci3/user/herryPatel11">Herry Patel</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/user/TimRocknew16">Timnew Rocknew</a>', 16, 0, 0, 0, NULL),
(2446, 21, 'User', 'follow', '2015-12-01 13:37:26', '<a href="http://mydesichef.com/realestate_development_ci3/user/herryPatel11">Herry Patel</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/user/TimRocknew16">Timnew Rocknew</a>', 16, 0, 0, 0, NULL),
(2447, 24, 'User', 'follow', '2015-12-01 13:37:26', '<a href="http://mydesichef.com/realestate_development_ci3/user/herryPatel11">Herry Patel</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/user/TimRocknew16">Timnew Rocknew</a>', 16, 0, 0, 0, NULL),
(2448, 25, 'User', 'follow', '2015-12-01 13:37:26', '<a href="http://mydesichef.com/realestate_development_ci3/user/herryPatel11">Herry Patel</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/user/TimRocknew16">Timnew Rocknew</a>', 16, 0, 0, 0, NULL),
(2449, 11, 'User', 'unfollow', '2015-12-01 13:37:28', 'You have started Unfollowing <a href="http://mydesichef.com/realestate_development_ci3/user/TimRocknew16">Timnew Rocknew</a>', 16, 0, 1, 0, NULL),
(2450, 16, 'User', 'unfollow', '2015-12-01 13:37:28', '<a href="http://mydesichef.com/realestate_development_ci3/user/herryPatel11">Herry Patel</a> has started Unfollowing you.', 11, 0, 0, 0, NULL),
(2451, 23, 'User', 'unfollow', '2015-12-01 13:37:28', '<a href="http://mydesichef.com/realestate_development_ci3/user/herryPatel11">Herry Patel</a> has started Unfollowing <a href="http://mydesichef.com/realestate_development_ci3/user/TimRocknew16">Timnew Rocknew</a>', 16, 0, 0, 0, NULL),
(2452, 22, 'User', 'unfollow', '2015-12-01 13:37:28', '<a href="http://mydesichef.com/realestate_development_ci3/user/herryPatel11">Herry Patel</a> has started Unfollowing <a href="http://mydesichef.com/realestate_development_ci3/user/TimRocknew16">Timnew Rocknew</a>', 16, 0, 0, 0, NULL),
(2453, 21, 'User', 'unfollow', '2015-12-01 13:37:28', '<a href="http://mydesichef.com/realestate_development_ci3/user/herryPatel11">Herry Patel</a> has started Unfollowing <a href="http://mydesichef.com/realestate_development_ci3/user/TimRocknew16">Timnew Rocknew</a>', 16, 0, 0, 0, NULL),
(2454, 24, 'User', 'unfollow', '2015-12-01 13:37:28', '<a href="http://mydesichef.com/realestate_development_ci3/user/herryPatel11">Herry Patel</a> has started Unfollowing <a href="http://mydesichef.com/realestate_development_ci3/user/TimRocknew16">Timnew Rocknew</a>', 16, 0, 0, 0, NULL),
(2455, 25, 'User', 'unfollow', '2015-12-01 13:37:28', '<a href="http://mydesichef.com/realestate_development_ci3/user/herryPatel11">Herry Patel</a> has started Unfollowing <a href="http://mydesichef.com/realestate_development_ci3/user/TimRocknew16">Timnew Rocknew</a>', 16, 0, 0, 0, NULL),
(2456, 11, 'User', 'follow', '2015-12-01 13:43:43', 'You have started Following <a href="http://mydesichef.com/realestate_development_ci3/user/JayshreePatel1">Jayshree Patel</a>', 1, 0, 1, 0, NULL),
(2457, 1, 'User', 'follow', '2015-12-01 13:43:43', '<a href="http://mydesichef.com/realestate_development_ci3/user/herryPatel11">Herry Patel</a> has started Following you.', 11, 0, 0, 0, NULL),
(2458, 27, 'User', 'follow', '2015-12-01 13:43:43', '<a href="http://mydesichef.com/realestate_development_ci3/user/herryPatel11">Herry Patel</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/user/JayshreePatel1">Jayshree Patel</a>', 1, 0, 0, 0, NULL),
(2459, 11, 'User', 'unfollow', '2015-12-01 13:43:45', 'You have started Unfollowing <a href="http://mydesichef.com/realestate_development_ci3/user/JayshreePatel1">Jayshree Patel</a>', 1, 0, 1, 0, NULL),
(2460, 1, 'User', 'unfollow', '2015-12-01 13:43:45', '<a href="http://mydesichef.com/realestate_development_ci3/user/herryPatel11">Herry Patel</a> has started Unfollowing you.', 11, 0, 0, 0, NULL),
(2461, 27, 'User', 'unfollow', '2015-12-01 13:43:45', '<a href="http://mydesichef.com/realestate_development_ci3/user/herryPatel11">Herry Patel</a> has started Unfollowing <a href="http://mydesichef.com/realestate_development_ci3/user/JayshreePatel1">Jayshree Patel</a>', 1, 0, 0, 0, NULL),
(2462, 11, 'project', 'unfollow', '2015-12-01 13:44:55', 'You have started Unfollowing <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a>', 0, 24, 1, 0, NULL),
(2463, 7, 'project', 'unfollow', '2015-12-01 13:44:55', '<a href="http://mydesichef.com/realestate_development_ci3/user/herryPatel11">Herry Patel</a> has started Unfollowing you created <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a>', 11, 24, 1, 0, NULL),
(2464, 11, 'project', 'unfollow', '2015-12-01 13:44:56', 'You have started Unfollowing <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a>', 0, 24, 1, 0, NULL),
(2465, 7, 'project', 'unfollow', '2015-12-01 13:44:56', '<a href="http://mydesichef.com/realestate_development_ci3/user/herryPatel11">Herry Patel</a> has started Unfollowing you created <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a>', 11, 24, 1, 0, NULL),
(2466, 11, 'project', 'follow', '2015-12-01 13:47:16', 'You have started Following <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a>', 0, 24, 1, 0, NULL),
(2467, 7, 'project', 'follow', '2015-12-01 13:47:16', '<a href="http://mydesichef.com/realestate_development_ci3/user/herryPatel11">Herry Patel</a> has started Following you created <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a>', 11, 24, 1, 0, NULL),
(2468, 12, 'project', 'follow', '2015-12-01 13:47:16', '<a href="http://mydesichef.com/realestate_development_ci3/user/herryPatel11">Herry Patel</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Invested', 0, 24, 1, 0, NULL),
(2469, 12, 'project', 'follow', '2015-12-01 14:01:41', 'You have started Following <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a>', 0, 24, 1, 0, NULL),
(2470, 7, 'project', 'follow', '2015-12-01 14:01:41', '<a href="http://mydesichef.com/realestate_development_ci3/user/ankyPatel12">Anky Patel</a> has started Following you created <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a>', 12, 24, 1, 0, NULL),
(2471, 11, 'project', 'follow', '2015-12-01 14:01:41', '<a href="http://mydesichef.com/realestate_development_ci3/user/ankyPatel12">Anky Patel</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Following', 0, 24, 0, 0, NULL),
(2472, 11, 'project', 'follow', '2015-12-01 14:01:41', '<a href="http://mydesichef.com/realestate_development_ci3/user/ankyPatel12">Anky Patel</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Invested', 0, 24, 0, 0, NULL),
(2473, 11, 'project', 'follow', '2015-12-01 14:01:41', '<a href="http://mydesichef.com/realestate_development_ci3/user/ankyPatel12">Anky Patel</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Invested', 0, 24, 0, 0, NULL),
(2474, 11, 'project', 'follow', '2015-12-01 14:01:41', '<a href="http://mydesichef.com/realestate_development_ci3/user/ankyPatel12">Anky Patel</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Commented', 0, 24, 0, 0, NULL),
(2475, 7, 'User', 'follow', '2015-12-01 15:25:05', 'You have started Following <a href="http://mydesichef.com/realestate_development_ci3/user/JohnAbraham25">John Abraham</a>', 25, 0, 1, 0, NULL),
(2476, 25, 'User', 'follow', '2015-12-01 15:25:05', '<a href="http://mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a> has started Following you.', 7, 0, 0, 0, NULL),
(2477, 1, 'User', 'follow', '2015-12-01 15:25:05', '<a href="http://mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/user/JohnAbraham25">John Abraham</a>', 25, 0, 0, 0, NULL),
(2478, 7, 'User', 'unfollow', '2015-12-01 15:25:06', 'You have started Unfollowing <a href="http://mydesichef.com/realestate_development_ci3/user/JohnAbraham25">John Abraham</a>', 25, 0, 1, 0, NULL),
(2479, 25, 'User', 'unfollow', '2015-12-01 15:25:06', '<a href="http://mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a> has started Unfollowing you.', 7, 0, 0, 0, NULL),
(2480, 1, 'User', 'unfollow', '2015-12-01 15:25:06', '<a href="http://mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a> has started Unfollowing <a href="http://mydesichef.com/realestate_development_ci3/user/JohnAbraham25">John Abraham</a>', 25, 0, 0, 0, NULL),
(2481, 7, 'User', 'update_account', '2015-12-01 15:25:22', 'You have updated your account information', 0, 0, 1, 0, NULL),
(2482, 0, 'project', 'follow', '2015-12-01 15:32:21', 'You have started Following <a href="http://mydesichef.com/realestate_development_ci3/company/abc-corp">ABC Corp</a>', 0, 0, 0, 2, NULL),
(2483, 1, 'project', 'follow', '2015-12-01 15:32:21', '<a href="http://mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a> has started Following you created <a href="http://mydesichef.com/realestate_development_ci3/company/abc-corp">ABC Corp</a>', 0, 0, 0, 2, NULL),
(2484, 2, 'project', 'follow', '2015-12-01 15:32:21', '<a href="http://mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/company/abc-corp">ABC Corp</a> you already Following', 0, 0, 0, 2, NULL),
(2485, 7, 'project', 'follow', '2015-12-01 15:32:21', '<a href="http://mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/company/abc-corp">ABC Corp</a> you already Following', 0, 0, 1, 2, NULL),
(2486, 0, 'company', 'unfollow', '2015-12-01 15:32:23', 'You have started Unfollowing <a href="http://mydesichef.com/realestate_development_ci3/company/abc-corp">ABC Corp</a>', 0, 0, 0, 2, NULL),
(2487, 1, 'company', 'unfollow', '2015-12-01 15:32:23', '<a href="http://mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a> has started Unfollowing you created <a href="http://mydesichef.com/realestate_development_ci3/company/abc-corp">ABC Corp</a>', 0, 0, 0, 2, NULL),
(2488, 7, 'project', 'intrest_request', '2015-12-01 15:32:35', 'You have expressed your interest to become Investment on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a>', 0, 1, 1, 0, NULL),
(2489, 1, 'project', 'intrest_request', '2015-12-01 15:32:35', 'You have received <a href="http://mydesichef.com/realestate_development_ci3/equity/dashboard/1">Interest</a> to become Investment on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> from <a href="http://mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 1, 0, 0, NULL),
(2490, 7, 'project', 'access_request', '2015-12-01 15:32:43', 'You have sent your access request for <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> to access ', 0, 1, 1, 0, NULL),
(2491, 1, 'project', 'access_request', '2015-12-01 15:32:43', 'You have received <a href="http://mydesichef.com/realestate_development_ci3/equity/dashboard/1">Access Request</a> to access  on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> from <a href="http://mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 1, 0, 0, NULL),
(2492, 1, 'project', 'project_inactive', '2015-12-01 15:56:44', 'Your Development <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> is inactivated and unpublished by Real Estate team', 1, 1, 0, 0, NULL),
(2493, 27, 'project', 'project_inactive', '2015-12-01 15:56:44', 'Development <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> you already Followed is inactivated and unpublished by Real Estate team', 0, 1, 0, 0, NULL),
(2494, 27, 'project', 'project_inactive', '2015-12-01 15:56:44', 'Development <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> you already Followed is inactivated and unpublished by Real Estate team', 0, 1, 0, 0, NULL),
(2495, 27, 'project', 'project_inactive', '2015-12-01 15:56:44', 'Development <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> you already Followed is inactivated and unpublished by Real Estate team', 0, 1, 0, 0, NULL),
(2496, 27, 'project', 'project_inactive', '2015-12-01 15:56:44', 'Development <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> you already Followed is inactivated and unpublished by Real Estate team', 0, 1, 0, 0, NULL),
(2497, 27, 'project', 'project_inactive', '2015-12-01 15:56:44', 'Development <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> you already Followed is inactivated and unpublished by Real Estate team', 0, 1, 0, 0, NULL),
(2498, 27, 'project', 'project_inactive', '2015-12-01 15:56:44', 'Development <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> you already Followed is inactivated and unpublished by Real Estate team', 0, 1, 0, 0, NULL),
(2499, 27, 'project', 'project_inactive', '2015-12-01 15:56:44', 'Development <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> you already Followed is inactivated and unpublished by Real Estate team', 0, 1, 0, 0, NULL),
(2500, 27, 'project', 'project_inactive', '2015-12-01 15:56:44', 'Development <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> you already Followed is inactivated and unpublished by Real Estate team', 0, 1, 0, 0, NULL),
(2501, 27, 'project', 'project_inactive', '2015-12-01 15:56:44', 'Development <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> you already Followed is inactivated and unpublished by Real Estate team', 0, 1, 0, 0, NULL),
(2502, 27, 'project', 'project_inactive', '2015-12-01 15:56:44', 'Development <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> you already Followed is inactivated and unpublished by Real Estate team', 0, 1, 0, 0, NULL),
(2503, 16, 'project', 'project_inactive', '2015-12-01 15:56:44', 'Development <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> you already Invested is inactivated and unpublished by Real Estate team', 0, 1, 0, 0, NULL),
(2504, 16, 'project', 'project_inactive', '2015-12-01 15:56:44', 'Development <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> you already Invested is inactivated and unpublished by Real Estate team', 0, 1, 0, 0, NULL),
(2505, 8, 'project', 'project_inactive', '2015-12-01 15:56:44', 'Development <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> you already Invested is inactivated and unpublished by Real Estate team', 0, 1, 0, 0, NULL),
(2506, 2, 'project', 'project_inactive', '2015-12-01 15:56:44', 'Development <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> you already Invested is inactivated and unpublished by Real Estate team', 0, 1, 0, 0, NULL),
(2507, 43, 'project', 'project_inactive', '2015-12-01 15:56:44', 'Development <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> you already Commented is inactivated and unpublished by Real Estate team', 0, 1, 0, 0, NULL),
(2508, 1, 'project', 'project_active', '2015-12-01 15:57:39', 'Your Development <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> is activated and published by Real Estate team', 1, 1, 0, 0, NULL),
(2509, 27, 'project', 'project_active', '2015-12-01 15:57:39', 'Development <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> you already Followed is activated and published by Real Estate team', 0, 1, 0, 0, NULL),
(2510, 27, 'project', 'project_active', '2015-12-01 15:57:39', 'Development <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> you already Followed is activated and published by Real Estate team', 0, 1, 0, 0, NULL),
(2511, 27, 'project', 'project_active', '2015-12-01 15:57:39', 'Development <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> you already Followed is activated and published by Real Estate team', 0, 1, 0, 0, NULL),
(2512, 27, 'project', 'project_active', '2015-12-01 15:57:39', 'Development <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> you already Followed is activated and published by Real Estate team', 0, 1, 0, 0, NULL),
(2513, 27, 'project', 'project_active', '2015-12-01 15:57:39', 'Development <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> you already Followed is activated and published by Real Estate team', 0, 1, 0, 0, NULL),
(2514, 27, 'project', 'project_active', '2015-12-01 15:57:39', 'Development <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> you already Followed is activated and published by Real Estate team', 0, 1, 0, 0, NULL),
(2515, 27, 'project', 'project_active', '2015-12-01 15:57:39', 'Development <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> you already Followed is activated and published by Real Estate team', 0, 1, 0, 0, NULL),
(2516, 27, 'project', 'project_active', '2015-12-01 15:57:39', 'Development <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> you already Followed is activated and published by Real Estate team', 0, 1, 0, 0, NULL),
(2517, 27, 'project', 'project_active', '2015-12-01 15:57:39', 'Development <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> you already Followed is activated and published by Real Estate team', 0, 1, 0, 0, NULL),
(2518, 27, 'project', 'project_active', '2015-12-01 15:57:39', 'Development <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> you already Followed is activated and published by Real Estate team', 0, 1, 0, 0, NULL),
(2519, 16, 'project', 'project_active', '2015-12-01 15:57:39', 'Development <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> you already Invested is activated and published by Real Estate team', 0, 1, 0, 0, NULL),
(2520, 16, 'project', 'project_active', '2015-12-01 15:57:39', 'Development <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> you already Invested is activated and published by Real Estate team', 0, 1, 0, 0, NULL),
(2521, 8, 'project', 'project_active', '2015-12-01 15:57:39', 'Development <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> you already Invested is activated and published by Real Estate team', 0, 1, 0, 0, NULL),
(2522, 2, 'project', 'project_active', '2015-12-01 15:57:39', 'Development <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> you already Invested is activated and published by Real Estate team', 0, 1, 0, 0, NULL),
(2523, 43, 'project', 'project_active', '2015-12-01 15:57:39', 'Development <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> you already Commented is activated and published by Real Estate team', 0, 1, 0, 0, NULL),
(2524, 43, 'User', 'draft_project', '2015-12-01 18:09:37', 'You have created a new Development <a href="http://mydesichef.com/realestate_development_ci3/development/25">http://mydesichef.com/realestate_development_ci3/development/25</a>', 0, 25, 0, 0, NULL),
(2525, 1, 'project', 'follow', '2015-12-01 18:21:22', 'You have started Following <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a>', 0, 24, 0, 0, NULL),
(2526, 7, 'project', 'follow', '2015-12-01 18:21:22', '<a href="http://mydesichef.com/realestate_development_ci3/user/JayshreePatel1">Jayshree Patel</a> has started Following you created <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a>', 1, 24, 0, 0, NULL),
(2527, 12, 'project', 'follow', '2015-12-01 18:21:22', '<a href="http://mydesichef.com/realestate_development_ci3/user/JayshreePatel1">Jayshree Patel</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Following', 0, 24, 0, 0, NULL),
(2528, 11, 'project', 'follow', '2015-12-01 18:21:22', '<a href="http://mydesichef.com/realestate_development_ci3/user/JayshreePatel1">Jayshree Patel</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Following', 0, 24, 0, 0, NULL),
(2529, 11, 'project', 'follow', '2015-12-01 18:21:22', '<a href="http://mydesichef.com/realestate_development_ci3/user/JayshreePatel1">Jayshree Patel</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Invested', 0, 24, 0, 0, NULL),
(2530, 12, 'project', 'follow', '2015-12-01 18:21:22', '<a href="http://mydesichef.com/realestate_development_ci3/user/JayshreePatel1">Jayshree Patel</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Invested', 0, 24, 0, 0, NULL),
(2531, 11, 'project', 'follow', '2015-12-01 18:21:22', '<a href="http://mydesichef.com/realestate_development_ci3/user/JayshreePatel1">Jayshree Patel</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Invested', 0, 24, 0, 0, NULL),
(2532, 11, 'project', 'follow', '2015-12-01 18:21:22', '<a href="http://mydesichef.com/realestate_development_ci3/user/JayshreePatel1">Jayshree Patel</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Commented', 0, 24, 0, 0, NULL),
(2533, 1, 'project', 'unfollow', '2015-12-01 18:23:08', 'You have started Unfollowing <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a>', 0, 24, 0, 0, NULL),
(2534, 7, 'project', 'unfollow', '2015-12-01 18:23:08', '<a href="http://mydesichef.com/realestate_development_ci3/user/JayshreePatel1">Jayshree Patel</a> has started Unfollowing you created <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a>', 1, 24, 0, 0, NULL),
(2535, 12, 'project', 'unfollow', '2015-12-01 18:23:08', '<a href="http://mydesichef.com/realestate_development_ci3/user/JayshreePatel1">Jayshree Patel</a> has started Unfollowing <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Unfollowing', 0, 24, 0, 0, NULL),
(2536, 11, 'project', 'unfollow', '2015-12-01 18:23:08', '<a href="http://mydesichef.com/realestate_development_ci3/user/JayshreePatel1">Jayshree Patel</a> has started Unfollowing <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Unfollowing', 0, 24, 0, 0, NULL),
(2537, 1, 'project', 'follow', '2015-12-01 18:23:12', 'You have started Following <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a>', 0, 24, 0, 0, NULL),
(2538, 7, 'project', 'follow', '2015-12-01 18:23:12', '<a href="http://mydesichef.com/realestate_development_ci3/user/JayshreePatel1">Jayshree Patel</a> has started Following you created <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a>', 1, 24, 0, 0, NULL),
(2539, 12, 'project', 'follow', '2015-12-01 18:23:12', '<a href="http://mydesichef.com/realestate_development_ci3/user/JayshreePatel1">Jayshree Patel</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Following', 0, 24, 0, 0, NULL),
(2540, 11, 'project', 'follow', '2015-12-01 18:23:12', '<a href="http://mydesichef.com/realestate_development_ci3/user/JayshreePatel1">Jayshree Patel</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Following', 0, 24, 0, 0, NULL),
(2541, 11, 'project', 'follow', '2015-12-01 18:23:12', '<a href="http://mydesichef.com/realestate_development_ci3/user/JayshreePatel1">Jayshree Patel</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Invested', 0, 24, 0, 0, NULL),
(2542, 12, 'project', 'follow', '2015-12-01 18:23:12', '<a href="http://mydesichef.com/realestate_development_ci3/user/JayshreePatel1">Jayshree Patel</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Invested', 0, 24, 0, 0, NULL),
(2543, 11, 'project', 'follow', '2015-12-01 18:23:12', '<a href="http://mydesichef.com/realestate_development_ci3/user/JayshreePatel1">Jayshree Patel</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Invested', 0, 24, 0, 0, NULL),
(2544, 11, 'project', 'follow', '2015-12-01 18:23:12', '<a href="http://mydesichef.com/realestate_development_ci3/user/JayshreePatel1">Jayshree Patel</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Commented', 0, 24, 0, 0, NULL),
(2545, 43, 'project', 'follow', '2015-12-01 18:24:28', 'You have started Following <a href="http://mydesichef.com/realestate_development_ci3/development/test-project1">Test Project</a>', 0, 19, 0, 0, NULL),
(2546, 16, 'project', 'follow', '2015-12-01 18:24:28', '<a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a> has started Following you created <a href="http://mydesichef.com/realestate_development_ci3/development/test-project1">Test Project</a>', 43, 19, 0, 0, NULL),
(2547, 1, 'project', 'follow', '2015-12-01 18:24:28', '<a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/development/test-project1">Test Project</a> you already Following', 0, 19, 0, 0, NULL),
(2548, 7, 'project', 'follow', '2015-12-01 18:24:28', '<a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/development/test-project1">Test Project</a> you already Following', 0, 19, 0, 0, NULL),
(2549, 27, 'project', 'follow', '2015-12-01 18:24:28', '<a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/development/test-project1">Test Project</a> you already Following', 0, 19, 0, 0, NULL),
(2550, 37, 'project', 'follow', '2015-12-01 18:24:28', '<a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/development/test-project1">Test Project</a> you already Invested', 0, 19, 0, 0, NULL),
(2551, 37, 'project', 'follow', '2015-12-01 18:24:28', '<a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/development/test-project1">Test Project</a> you already Invested', 0, 19, 0, 0, NULL),
(2552, 43, 'project', 'unfollow', '2015-12-01 18:39:43', 'You have started Unfollowing <a href="http://mydesichef.com/realestate_development_ci3/development/test-project1">Test Project</a>', 0, 19, 0, 0, NULL),
(2553, 16, 'project', 'unfollow', '2015-12-01 18:39:43', '<a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a> has started Unfollowing you created <a href="http://mydesichef.com/realestate_development_ci3/development/test-project1">Test Project</a>', 43, 19, 0, 0, NULL),
(2554, 1, 'project', 'unfollow', '2015-12-01 18:39:43', '<a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a> has started Unfollowing <a href="http://mydesichef.com/realestate_development_ci3/development/test-project1">Test Project</a> you already Unfollowing', 0, 19, 0, 0, NULL),
(2555, 7, 'project', 'unfollow', '2015-12-01 18:39:43', '<a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a> has started Unfollowing <a href="http://mydesichef.com/realestate_development_ci3/development/test-project1">Test Project</a> you already Unfollowing', 0, 19, 0, 0, NULL),
(2556, 27, 'project', 'unfollow', '2015-12-01 18:39:43', '<a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a> has started Unfollowing <a href="http://mydesichef.com/realestate_development_ci3/development/test-project1">Test Project</a> you already Unfollowing', 0, 19, 0, 0, NULL),
(2557, 43, 'project', 'follow', '2015-12-01 18:39:54', 'You have started Following <a href="http://mydesichef.com/realestate_development_ci3/development/test-project1">Test Project</a>', 0, 19, 0, 0, NULL),
(2558, 16, 'project', 'follow', '2015-12-01 18:39:54', '<a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a> has started Following you created <a href="http://mydesichef.com/realestate_development_ci3/development/test-project1">Test Project</a>', 43, 19, 0, 0, NULL),
(2559, 1, 'project', 'follow', '2015-12-01 18:39:54', '<a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/development/test-project1">Test Project</a> you already Following', 0, 19, 0, 0, NULL),
(2560, 7, 'project', 'follow', '2015-12-01 18:39:54', '<a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/development/test-project1">Test Project</a> you already Following', 0, 19, 0, 0, NULL),
(2561, 27, 'project', 'follow', '2015-12-01 18:39:54', '<a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/development/test-project1">Test Project</a> you already Following', 0, 19, 0, 0, NULL),
(2562, 37, 'project', 'follow', '2015-12-01 18:39:54', '<a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/development/test-project1">Test Project</a> you already Invested', 0, 19, 0, 0, NULL),
(2563, 37, 'project', 'follow', '2015-12-01 18:39:54', '<a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a> has started Following <a href="http://mydesichef.com/realestate_development_ci3/development/test-project1">Test Project</a> you already Invested', 0, 19, 0, 0, NULL),
(2564, 45, 'User', 'update_account', '2015-12-02 10:47:13', 'You have updated your account information', 0, 0, 1, 0, NULL),
(2565, 45, 'project', 'access_request', '2015-12-02 10:48:12', 'You have sent your access request for <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a> to access ', 0, 24, 1, 0, NULL),
(2566, 7, 'project', 'access_request', '2015-12-02 10:48:12', 'You have received <a href="http://mydesichef.com/realestate_development_ci3/equity/dashboard/24">Access Request</a> to access  on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a> from <a href="http://mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a>', 45, 24, 0, 0, NULL),
(2567, 1, 'project', 'access_request', '2015-12-02 11:06:37', 'You have sent your access request for <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a> to access ', 0, 24, 0, 0, NULL),
(2568, 7, 'project', 'access_request', '2015-12-02 11:06:37', 'You have received <a href="http://mydesichef.com/realestate_development_ci3/equity/dashboard/24">Access Request</a> to access  on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a> from <a href="http://mydesichef.com/realestate_development_ci3/user/JayshreePatel1">Jayshree Patel</a>', 1, 24, 0, 0, NULL),
(2569, 43, 'project', 'intrest_request', '2015-12-02 11:41:26', 'You have expressed your interest to become Investment on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a>', 0, 1, 0, 0, NULL);
INSERT INTO `user_activity` (`id`, `user_id`, `module`, `action`, `datetime`, `action_detail`, `second_user_id`, `campaign_id`, `read`, `company_id`, `type`) VALUES
(2570, 1, 'project', 'intrest_request', '2015-12-02 11:41:26', 'You have received <a href="http://mydesichef.com/realestate_development_ci3/equity/dashboard/1">Interest</a> to become Investment on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> from <a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a>', 43, 1, 0, 0, NULL),
(2571, 45, 'project', 'commented', '2015-12-02 11:42:41', 'You have Commented on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a>', 7, 24, 1, 0, NULL),
(2572, 7, 'project', 'commented', '2015-12-02 11:42:41', '<a href="http://mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> Commented on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already created', 45, 24, 0, 0, NULL),
(2573, 1, 'project', 'commented', '2015-12-02 11:42:41', '<a href="http://mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> Commented on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Followed', 7, 24, 0, 0, NULL),
(2574, 12, 'project', 'commented', '2015-12-02 11:42:41', '<a href="http://mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> Commented on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Followed', 7, 24, 0, 0, NULL),
(2575, 11, 'project', 'commented', '2015-12-02 11:42:41', '<a href="http://mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> Commented on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Followed', 7, 24, 0, 0, NULL),
(2576, 11, 'project', 'commented', '2015-12-02 11:42:41', '<a href="http://mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> Commented on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Invested', 7, 24, 0, 0, NULL),
(2577, 12, 'project', 'commented', '2015-12-02 11:42:41', '<a href="http://mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> Commented on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Invested', 7, 24, 0, 0, NULL),
(2578, 11, 'project', 'commented', '2015-12-02 11:42:41', '<a href="http://mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> Commented on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Invested', 7, 24, 0, 0, NULL),
(2579, 11, 'project', 'commented', '2015-12-02 11:42:41', '<a href="http://mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> Commented on<a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Commented', 7, 24, 0, 0, NULL),
(2580, 7, 'project', 'update', '2015-12-02 11:48:18', 'You have posted a new Update on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a>', 7, 24, 0, 0, NULL),
(2581, 1, 'project', 'update', '2015-12-02 11:48:18', '<a href="http://mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a> has posted new Update on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Followed', 7, 24, 0, 0, NULL),
(2582, 12, 'project', 'update', '2015-12-02 11:48:18', '<a href="http://mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a> has posted new Update on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Followed', 7, 24, 0, 0, NULL),
(2583, 11, 'project', 'update', '2015-12-02 11:48:18', '<a href="http://mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a> has posted new Update on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Followed', 7, 24, 0, 0, NULL),
(2584, 11, 'project', 'update', '2015-12-02 11:48:18', '<a href="http://mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a> has posted new Update on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Invested', 7, 24, 0, 0, NULL),
(2585, 12, 'project', 'update', '2015-12-02 11:48:18', '<a href="http://mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a> has posted new Update on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Invested', 7, 24, 0, 0, NULL),
(2586, 11, 'project', 'update', '2015-12-02 11:48:18', '<a href="http://mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a> has posted new Update on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Invested', 7, 24, 0, 0, NULL),
(2587, 45, 'project', 'update', '2015-12-02 11:48:18', '<a href="http://mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a> has posted new Update on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Commented', 7, 24, 1, 0, NULL),
(2588, 11, 'project', 'update', '2015-12-02 11:48:18', '<a href="http://mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a> has posted new Update on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Commented', 7, 24, 0, 0, NULL),
(2589, 45, 'User', 'accredetial_request', '2015-12-02 11:51:16', 'You applied to become accredited Investor', 0, 0, 1, 0, NULL),
(2590, 45, 'project', 'intrest_request', '2015-12-02 11:54:15', 'You have expressed your interest to become Investment on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 1, 0, NULL),
(2591, 7, 'project', 'intrest_request', '2015-12-02 11:54:15', 'You have received <a href="http://mydesichef.com/realestate_development_ci3/equity/dashboard/24">Interest</a> to become Investment on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a> from <a href="http://mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a>', 45, 24, 0, 0, NULL),
(2592, 8, 'project', 'access_request', '2015-12-02 11:54:44', 'You have sent your access request for <a href="http://mydesichef.com/realestate_development_ci3/development/test-project1">Test Company1</a> to access ', 0, 19, 0, 0, NULL),
(2593, 16, 'project', 'access_request', '2015-12-02 11:54:44', 'You have received <a href="http://mydesichef.com/realestate_development_ci3/equity/dashboard/19">Access Request</a> to access  on <a href="http://mydesichef.com/realestate_development_ci3/development/test-project1">Test Company1</a> from <a href="http://mydesichef.com/realestate_development_ci3/user/HardikJivani8">Hardik Jivani</a>', 8, 19, 0, 0, NULL),
(2594, 8, 'project', 'access_request', '2015-12-02 11:56:50', 'You have sent your access request for <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> to access ', 0, 1, 0, 0, NULL),
(2595, 1, 'project', 'access_request', '2015-12-02 11:56:50', 'You have received <a href="http://mydesichef.com/realestate_development_ci3/equity/dashboard/1">Access Request</a> to access  on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> from <a href="http://mydesichef.com/realestate_development_ci3/user/HardikJivani8">Hardik Jivani</a>', 8, 1, 0, 0, NULL),
(2596, 45, 'project', 'upload_contract', '2015-12-02 11:57:03', 'You have <a href="http://mydesichef.com/realestate_development_ci3/startinvest/index/24">Uploaded</a> Investment process  contract copy on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 1, 0, NULL),
(2597, 43, 'project', 'upload_contract', '2015-12-02 12:02:19', 'You have <a href="http://mydesichef.com/realestate_development_ci3/startinvest/index/1">Uploaded</a> Investment process  contract copy on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a>', 0, 1, 0, 0, NULL),
(2598, 8, 'project', 'commented', '2015-12-02 12:02:58', 'You have Commented on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a>', 1, 1, 0, 0, NULL),
(2599, 1, 'project', 'commented', '2015-12-02 12:02:58', '<a href="http://mydesichef.com/realestate_development_ci3/user/HardikJivani8">Hardik Jivani</a> Commented on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already created', 8, 1, 0, 0, NULL),
(2600, 27, 'project', 'commented', '2015-12-02 12:02:58', '<a href="http://mydesichef.com/realestate_development_ci3/user/HardikJivani8">Hardik Jivani</a> Commented on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already Followed', 1, 1, 0, 0, NULL),
(2601, 27, 'project', 'commented', '2015-12-02 12:02:58', '<a href="http://mydesichef.com/realestate_development_ci3/user/HardikJivani8">Hardik Jivani</a> Commented on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already Followed', 1, 1, 0, 0, NULL),
(2602, 27, 'project', 'commented', '2015-12-02 12:02:58', '<a href="http://mydesichef.com/realestate_development_ci3/user/HardikJivani8">Hardik Jivani</a> Commented on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already Followed', 1, 1, 0, 0, NULL),
(2603, 27, 'project', 'commented', '2015-12-02 12:02:58', '<a href="http://mydesichef.com/realestate_development_ci3/user/HardikJivani8">Hardik Jivani</a> Commented on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already Followed', 1, 1, 0, 0, NULL),
(2604, 27, 'project', 'commented', '2015-12-02 12:02:58', '<a href="http://mydesichef.com/realestate_development_ci3/user/HardikJivani8">Hardik Jivani</a> Commented on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already Followed', 1, 1, 0, 0, NULL),
(2605, 27, 'project', 'commented', '2015-12-02 12:02:58', '<a href="http://mydesichef.com/realestate_development_ci3/user/HardikJivani8">Hardik Jivani</a> Commented on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already Followed', 1, 1, 0, 0, NULL),
(2606, 27, 'project', 'commented', '2015-12-02 12:02:58', '<a href="http://mydesichef.com/realestate_development_ci3/user/HardikJivani8">Hardik Jivani</a> Commented on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already Followed', 1, 1, 0, 0, NULL),
(2607, 27, 'project', 'commented', '2015-12-02 12:02:58', '<a href="http://mydesichef.com/realestate_development_ci3/user/HardikJivani8">Hardik Jivani</a> Commented on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already Followed', 1, 1, 0, 0, NULL),
(2608, 27, 'project', 'commented', '2015-12-02 12:02:58', '<a href="http://mydesichef.com/realestate_development_ci3/user/HardikJivani8">Hardik Jivani</a> Commented on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already Followed', 1, 1, 0, 0, NULL),
(2609, 27, 'project', 'commented', '2015-12-02 12:02:58', '<a href="http://mydesichef.com/realestate_development_ci3/user/HardikJivani8">Hardik Jivani</a> Commented on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already Followed', 1, 1, 0, 0, NULL),
(2610, 16, 'project', 'commented', '2015-12-02 12:02:58', '<a href="http://mydesichef.com/realestate_development_ci3/user/HardikJivani8">Hardik Jivani</a> Commented on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already Invested', 1, 1, 0, 0, NULL),
(2611, 16, 'project', 'commented', '2015-12-02 12:02:58', '<a href="http://mydesichef.com/realestate_development_ci3/user/HardikJivani8">Hardik Jivani</a> Commented on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already Invested', 1, 1, 0, 0, NULL),
(2612, 2, 'project', 'commented', '2015-12-02 12:02:58', '<a href="http://mydesichef.com/realestate_development_ci3/user/HardikJivani8">Hardik Jivani</a> Commented on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already Invested', 1, 1, 0, 0, NULL),
(2613, 43, 'project', 'commented', '2015-12-02 12:02:58', '<a href="http://mydesichef.com/realestate_development_ci3/user/HardikJivani8">Hardik Jivani</a> Commented on<a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already Commented', 1, 1, 0, 0, NULL),
(2614, 45, 'project', 'approve_contract', '2015-12-02 12:03:38', 'Your Investment contract copy has been <a href="http://mydesichef.com/realestate_development_ci3/startinvest/index/24">Approve</a> on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a> by Real Estate team', 0, 24, 1, 0, NULL),
(2615, 45, 'project', 'approve_acknowledge_contract', '2015-12-02 12:05:33', 'Your Investment  acknowledgement has been <a href="http://mydesichef.com/realestate_development_ci3/startinvest/index/24">Approved</a> on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a> by Real Estate team', 0, 24, 1, 0, NULL),
(2616, 45, 'project', 'funded', '2015-12-02 12:05:34', 'You have Invested <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> with $500.00 USD', 7, 24, 1, 0, NULL),
(2617, 7, 'project', 'funded', '2015-12-02 12:05:34', '<a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already created is Invested with $500.00 USD by <a href="http://mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a>', 45, 24, 0, 0, NULL),
(2618, 1, 'project', 'funded', '2015-12-02 12:05:34', '<a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Followed is Invested with $500.00 USD by <a href="http://mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a>', 7, 24, 0, 0, NULL),
(2619, 12, 'project', 'funded', '2015-12-02 12:05:34', '<a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Followed is Invested with $500.00 USD by <a href="http://mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a>', 7, 24, 0, 0, NULL),
(2620, 11, 'project', 'funded', '2015-12-02 12:05:34', '<a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Followed is Invested with $500.00 USD by <a href="http://mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a>', 7, 24, 0, 0, NULL),
(2621, 11, 'project', 'funded', '2015-12-02 12:05:34', '<a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Invested is Invested with $500.00 USD by <a href="http://mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a>', 7, 24, 0, 0, NULL),
(2622, 12, 'project', 'funded', '2015-12-02 12:05:34', '<a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Invested is Invested with $500.00 USD by <a href="http://mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a>', 7, 24, 0, 0, NULL),
(2623, 11, 'project', 'funded', '2015-12-02 12:05:34', '<a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Invested is Invested with $500.00 USD by <a href="http://mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a>', 7, 24, 0, 0, NULL),
(2624, 11, 'project', 'funded', '2015-12-02 12:05:34', '<a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Commented is Invested with $500.00 USD by <a href="http://mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a>', 7, 24, 0, 0, NULL),
(2625, 45, 'project', 'approve_acknowledge_contract', '2015-12-02 12:05:49', 'Your Investment  acknowledgement has been <a href="http://mydesichef.com/realestate_development_ci3/startinvest/index/24">Approved</a> on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a> by Real Estate team', 0, 24, 1, 0, NULL),
(2626, 45, 'project', 'approve_acknowledge_contract', '2015-12-02 12:05:50', 'Your Investment  acknowledgement has been <a href="http://mydesichef.com/realestate_development_ci3/startinvest/index/24">Approved</a> on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a> by Real Estate team', 0, 24, 1, 0, NULL),
(2627, 45, 'project', 'approve_acknowledge_contract', '2015-12-02 12:05:54', 'Your Investment  acknowledgement has been <a href="http://mydesichef.com/realestate_development_ci3/startinvest/index/24">Approved</a> on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a> by Real Estate team', 0, 24, 1, 0, NULL),
(2628, 45, 'project', 'approve_acknowledge_contract', '2015-12-02 12:05:55', 'Your Investment  acknowledgement has been <a href="http://mydesichef.com/realestate_development_ci3/startinvest/index/24">Approved</a> on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a> by Real Estate team', 0, 24, 1, 0, NULL),
(2629, 45, 'project', 'confirmed_submmited_tracking', '2015-12-02 12:06:30', 'You have <a href="http://mydesichef.com/realestate_development_ci3/startinvest/index/24">Approved and Confirmed</a> delivery of  share certificate/document  on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 1, 0, NULL),
(2630, 43, 'project', 'approve_contract', '2015-12-02 12:14:45', 'Your Investment contract copy has been <a href="http://mydesichef.com/realestate_development_ci3/startinvest/index/1">Approve</a> on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> by Real Estate team', 0, 1, 0, 0, NULL),
(2631, 43, 'project', 'acknowledge_contract', '2015-12-02 12:21:51', 'You have <a href="http://mydesichef.com/realestate_development_ci3/startinvest/index/1">confirmed & uploaded</a> Investment  acknowledgement information on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a>', 0, 1, 0, 0, NULL),
(2632, 43, 'project', 'approve_acknowledge_contract', '2015-12-02 12:22:17', 'Your Investment  acknowledgement has been <a href="http://mydesichef.com/realestate_development_ci3/startinvest/index/1">Approved</a> on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> by Real Estate team', 0, 1, 0, 0, NULL),
(2633, 43, 'project', 'funded', '2015-12-02 12:22:17', 'You have Invested <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> with $400.00 USD', 1, 1, 0, 0, NULL),
(2634, 1, 'project', 'funded', '2015-12-02 12:22:17', '<a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already created is Invested with $400.00 USD by <a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a>', 43, 1, 0, 0, NULL),
(2635, 27, 'project', 'funded', '2015-12-02 12:22:17', '<a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already Followed is Invested with $400.00 USD by <a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a>', 1, 1, 0, 0, NULL),
(2636, 27, 'project', 'funded', '2015-12-02 12:22:17', '<a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already Followed is Invested with $400.00 USD by <a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a>', 1, 1, 0, 0, NULL),
(2637, 27, 'project', 'funded', '2015-12-02 12:22:17', '<a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already Followed is Invested with $400.00 USD by <a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a>', 1, 1, 0, 0, NULL),
(2638, 27, 'project', 'funded', '2015-12-02 12:22:17', '<a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already Followed is Invested with $400.00 USD by <a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a>', 1, 1, 0, 0, NULL),
(2639, 27, 'project', 'funded', '2015-12-02 12:22:17', '<a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already Followed is Invested with $400.00 USD by <a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a>', 1, 1, 0, 0, NULL),
(2640, 27, 'project', 'funded', '2015-12-02 12:22:17', '<a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already Followed is Invested with $400.00 USD by <a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a>', 1, 1, 0, 0, NULL),
(2641, 27, 'project', 'funded', '2015-12-02 12:22:17', '<a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already Followed is Invested with $400.00 USD by <a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a>', 1, 1, 0, 0, NULL),
(2642, 27, 'project', 'funded', '2015-12-02 12:22:17', '<a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already Followed is Invested with $400.00 USD by <a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a>', 1, 1, 0, 0, NULL),
(2643, 27, 'project', 'funded', '2015-12-02 12:22:17', '<a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already Followed is Invested with $400.00 USD by <a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a>', 1, 1, 0, 0, NULL),
(2644, 27, 'project', 'funded', '2015-12-02 12:22:17', '<a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already Followed is Invested with $400.00 USD by <a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a>', 1, 1, 0, 0, NULL),
(2645, 16, 'project', 'funded', '2015-12-02 12:22:17', '<a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already Invested is Invested with $400.00 USD by <a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a>', 1, 1, 0, 0, NULL),
(2646, 16, 'project', 'funded', '2015-12-02 12:22:17', '<a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already Invested is Invested with $400.00 USD by <a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a>', 1, 1, 0, 0, NULL),
(2647, 8, 'project', 'funded', '2015-12-02 12:22:17', '<a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already Invested is Invested with $400.00 USD by <a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a>', 1, 1, 0, 0, NULL),
(2648, 2, 'project', 'funded', '2015-12-02 12:22:17', '<a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already Invested is Invested with $400.00 USD by <a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a>', 1, 1, 0, 0, NULL),
(2649, 8, 'project', 'funded', '2015-12-02 12:22:17', '<a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">Smile Buddies</a> you already Commented is Invested with $400.00 USD by <a href="http://mydesichef.com/realestate_development_ci3/user/rakeshpatel43">rakesh patel</a>', 1, 1, 0, 0, NULL),
(2650, 43, 'project', 'approve_acknowledge_contract', '2015-12-02 12:22:32', 'Your Investment  acknowledgement has been <a href="http://mydesichef.com/realestate_development_ci3/startinvest/index/1">Approved</a> on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> by Real Estate team', 0, 1, 0, 0, NULL),
(2651, 43, 'project', 'approve_acknowledge_contract', '2015-12-02 12:22:41', 'Your Investment  acknowledgement has been <a href="http://mydesichef.com/realestate_development_ci3/startinvest/index/1">Approved</a> on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> by Real Estate team', 0, 1, 0, 0, NULL),
(2652, 43, 'project', 'approve_acknowledge_contract', '2015-12-02 12:23:30', 'Your Investment  acknowledgement has been <a href="http://mydesichef.com/realestate_development_ci3/startinvest/index/1">Approved</a> on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> by Real Estate team', 0, 1, 0, 0, NULL),
(2653, 43, 'project', 'confirmed_submmited_tracking', '2015-12-02 12:25:02', 'You have <a href="http://mydesichef.com/realestate_development_ci3/startinvest/index/1">Approved and Confirmed</a> delivery of  share certificate/document  on <a href="http://mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a>', 0, 1, 0, 0, NULL),
(2654, 46, 'User', 'update_account', '2015-12-02 13:22:37', 'You have updated your account information', 0, 0, 0, 0, NULL),
(2655, 46, 'User', 'accredetial_request', '2015-12-02 13:24:21', 'You applied to become accredited Investor', 0, 0, 0, 0, NULL),
(2656, 46, 'project', 'intrest_request', '2015-12-02 13:25:28', 'You have expressed your interest to become Investment on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 0, 0, NULL),
(2657, 7, 'project', 'intrest_request', '2015-12-02 13:25:28', 'You have received <a href="http://mydesichef.com/realestate_development_ci3/equity/dashboard/24">Interest</a> to become Investment on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a> from <a href="http://mydesichef.com/realestate_development_ci3/user/VikramPatel46">Vikram Patel</a>', 46, 24, 0, 0, NULL),
(2658, 46, 'project', 'upload_contract', '2015-12-02 13:27:35', 'You have <a href="http://mydesichef.com/realestate_development_ci3/startinvest/index/24">Uploaded</a> Investment process  contract copy on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 0, 0, NULL),
(2659, 46, 'project', 'approve_contract', '2015-12-02 13:27:55', 'Your Investment contract copy has been <a href="http://mydesichef.com/realestate_development_ci3/startinvest/index/24">Approve</a> on <a href="http://mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a> by Real Estate team', 0, 24, 0, 0, NULL),
(2660, 46, 'User', 'draft_project', '2015-12-02 13:39:51', 'You have created a new Development <a href="http://mydesichef.com/realestate_development_ci3/development/26">http://mydesichef.com/realestate_development_ci3/development/26</a>', 0, 26, 0, 0, NULL),
(2661, 46, 'User', 'submit_project', '2015-12-02 13:51:43', 'You have submitted a new Development <a href="http://mydesichef.com/realestate_development_ci3/development/new-project">New Project</a>', 0, 26, 0, 0, NULL),
(2662, 46, 'User', 'project_approve', '2015-12-02 13:52:07', 'Your Development <a href="http://mydesichef.com/realestate_development_ci3/development/new-project">Rock Info</a> is approved and published by Real Estate team', 0, 26, 0, 0, NULL),
(2663, 45, 'project', 'intrest_request', '2015-12-02 13:53:46', 'You have expressed your interest to become Investment on <a href="http://mydesichef.com/realestate_development_ci3/development/new-project">Rock Info</a>', 0, 26, 1, 0, NULL),
(2664, 46, 'project', 'intrest_request', '2015-12-02 13:53:46', 'You have received <a href="http://mydesichef.com/realestate_development_ci3/equity/dashboard/26">Interest</a> to become Investment on <a href="http://mydesichef.com/realestate_development_ci3/development/new-project">Rock Info</a> from <a href="http://mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a>', 45, 26, 0, 0, NULL),
(2665, 45, 'project', 'access_request', '2015-12-02 14:11:53', 'You have sent your access request for <a href="http://mydesichef.com/realestate_development_ci3/development/new-project">Rock Info</a> to access ', 0, 26, 1, 0, NULL),
(2666, 46, 'project', 'access_request', '2015-12-02 14:11:53', 'You have received <a href="http://mydesichef.com/realestate_development_ci3/equity/dashboard/26">Access Request</a> to access  on <a href="http://mydesichef.com/realestate_development_ci3/development/new-project">Rock Info</a> from <a href="http://mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a>', 45, 26, 0, 0, NULL),
(2667, 1, 'User', 'draft_project', '2015-12-02 15:08:40', 'You have created a new Development <a href="http://mydesichef.com/realestate_development_ci3/development/27">http://mydesichef.com/realestate_development_ci3/development/27</a>', 0, 27, 0, 0, NULL),
(2668, 7, 'User', 'draft_project', '2015-12-02 16:09:25', 'You have created a new Development <a href="http://mydesichef.com/realestate_development_ci3/development/28">http://mydesichef.com/realestate_development_ci3/development/28</a>', 0, 28, 0, 0, NULL),
(2669, 7, 'User', 'draft_project', '2015-12-02 16:09:59', 'You have created a new Development <a href="http://mydesichef.com/realestate_development_ci3/development/29">http://mydesichef.com/realestate_development_ci3/development/29</a>', 0, 29, 0, 0, NULL),
(2670, 1, 'User', 'draft_project', '2015-12-02 16:11:39', 'You have created a new Development <a href="http://mydesichef.com/realestate_development_ci3/development/30">http://mydesichef.com/realestate_development_ci3/development/30</a>', 0, 30, 0, 0, NULL),
(2671, 1, 'User', 'draft_project', '2015-12-02 16:14:09', 'You have created a new Development <a href="http://mydesichef.com/realestate_development_ci3/development/31">http://mydesichef.com/realestate_development_ci3/development/31</a>', 0, 31, 0, 0, NULL),
(2672, 1, 'project', 'intrest_request', '2015-12-02 16:25:24', 'You have expressed your interest to become Investment on <a href="http://www.mydesichef.com/realestate_development_ci3/development/new-project">Rock Info</a>', 0, 26, 0, 0, NULL),
(2673, 46, 'project', 'intrest_request', '2015-12-02 16:25:24', 'You have received <a href="http://www.mydesichef.com/realestate_development_ci3/equity/dashboard/26">Interest</a> to become Investment on <a href="http://www.mydesichef.com/realestate_development_ci3/development/new-project">Rock Info</a> from <a href="http://www.mydesichef.com/realestate_development_ci3/user/JayshreePatel1">Jayshree Patel</a>', 1, 26, 0, 0, NULL),
(2674, 1, 'User', 'draft_project', '2015-12-02 17:13:20', 'You have created a new Development <a href="http://www.mydesichef.com/realestate_development_ci3/development/32">http://www.mydesichef.com/realestate_development_ci3/development/32</a>', 0, 32, 0, 0, NULL),
(2675, 1, 'User', 'draft_project', '2015-12-02 17:35:24', 'You have created a new Development <a href="http://www.mydesichef.com/realestate_development_ci3/development/33">http://www.mydesichef.com/realestate_development_ci3/development/33</a>', 0, 33, 0, 0, NULL),
(2676, 45, 'User', 'follow', '2015-12-02 17:53:36', 'You have started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 1, 0, NULL),
(2677, 7, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following you.', 45, 0, 0, 0, NULL),
(2678, 25, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2679, 25, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2680, 25, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2681, 25, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2682, 25, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2683, 25, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2684, 25, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2685, 25, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2686, 25, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2687, 25, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2688, 25, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2689, 25, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2690, 25, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2691, 25, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2692, 25, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2693, 25, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2694, 25, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2695, 25, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2696, 25, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2697, 25, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2698, 25, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2699, 25, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2700, 25, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2701, 25, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2702, 25, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2703, 25, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2704, 25, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2705, 25, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2706, 25, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2707, 25, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2708, 25, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2709, 25, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2710, 25, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2711, 25, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2712, 25, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2713, 25, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2714, 25, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2715, 25, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2716, 25, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2717, 25, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2718, 25, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2719, 25, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2720, 1, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2721, 27, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2722, 27, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2723, 27, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2724, 27, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2725, 27, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2726, 27, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2727, 27, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2728, 27, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2729, 27, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2730, 27, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2731, 27, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2732, 27, 'User', 'follow', '2015-12-02 17:53:36', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2733, 45, 'User', 'unfollow', '2015-12-02 17:53:38', 'You have started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 1, 0, NULL),
(2734, 7, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing you.', 45, 0, 0, 0, NULL),
(2735, 25, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL);
INSERT INTO `user_activity` (`id`, `user_id`, `module`, `action`, `datetime`, `action_detail`, `second_user_id`, `campaign_id`, `read`, `company_id`, `type`) VALUES
(2736, 25, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2737, 25, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2738, 25, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2739, 25, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2740, 25, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2741, 25, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2742, 25, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2743, 25, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2744, 25, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2745, 25, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2746, 25, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2747, 25, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2748, 25, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2749, 25, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2750, 25, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2751, 25, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2752, 25, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2753, 25, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2754, 25, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2755, 25, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2756, 25, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2757, 25, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2758, 25, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2759, 25, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2760, 25, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2761, 25, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2762, 25, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2763, 25, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2764, 25, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2765, 25, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2766, 25, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2767, 25, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2768, 25, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2769, 25, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2770, 25, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2771, 25, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2772, 25, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2773, 25, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2774, 25, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2775, 25, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2776, 25, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2777, 1, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2778, 27, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2779, 27, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2780, 27, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2781, 27, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2782, 27, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2783, 27, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2784, 27, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2785, 27, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2786, 27, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2787, 27, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2788, 27, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2789, 27, 'User', 'unfollow', '2015-12-02 17:53:38', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/user/sandyPreston7">Sandy Preston</a>', 7, 0, 0, 0, NULL),
(2790, 46, 'project', 'access_request', '2015-12-02 18:10:17', 'You have sent your access request for <a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a> to access ', 0, 24, 0, 0, NULL),
(2791, 7, 'project', 'access_request', '2015-12-02 18:10:17', 'You have received <a href="http://www.mydesichef.com/realestate_development_ci3/equity/dashboard/24">Access Request</a> to access  on <a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a> from <a href="http://www.mydesichef.com/realestate_development_ci3/user/VikramPatel46">Vikram Patel</a>', 46, 24, 0, 0, NULL),
(2792, 7, 'User', 'draft_project', '2015-12-02 18:30:04', 'You have created a new Development <a href="http://www.mydesichef.com/realestate_development_ci3/development/34">http://www.mydesichef.com/realestate_development_ci3/development/34</a>', 0, 34, 0, 0, NULL),
(2793, 1, 'User', 'draft_project', '2015-12-02 18:42:05', 'You have created a new Development <a href="http://www.mydesichef.com/realestate_development_ci3/development/35">http://www.mydesichef.com/realestate_development_ci3/development/35</a>', 0, 35, 0, 0, NULL),
(2794, 7, 'User', 'draft_project', '2015-12-02 18:57:40', 'You have created a new Development <a href="http://www.mydesichef.com/realestate_development_ci3/development/36">http://www.mydesichef.com/realestate_development_ci3/development/36</a>', 0, 36, 0, 0, NULL),
(2795, 8, 'project', 'access_request', '2015-12-04 19:29:13', 'You have sent your access request for <a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a> to access ', 0, 24, 0, 0, NULL),
(2796, 7, 'project', 'access_request', '2015-12-04 19:29:13', 'You have received <a href="http://www.mydesichef.com/realestate_development_ci3/equity/dashboard/24">Access Request</a> to access  on <a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a> from <a href="http://www.mydesichef.com/realestate_development_ci3/user/HardikJivani8">Hardik Jivani</a>', 8, 24, 0, 0, NULL),
(2797, 2, 'project', 'access_request', '2015-12-15 11:29:51', 'You have sent your access request for <a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a> to access ', 0, 24, 0, 0, NULL),
(2798, 7, 'project', 'access_request', '2015-12-15 11:29:51', 'You have received <a href="http://www.mydesichef.com/realestate_development_ci3/equity/dashboard/24">Access Request</a> to access  on <a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a> from <a href="http://www.mydesichef.com/realestate_development_ci3/user/JayuPatel2">Jayu Patel</a>', 2, 24, 0, 0, NULL),
(2799, 8, 'User', 'draft_project', '2015-12-15 15:03:08', 'You have created a new Development <a href="http://www.mydesichef.com/realestate_development_ci3/development/37">http://www.mydesichef.com/realestate_development_ci3/development/37</a>', 0, 37, 0, 0, NULL),
(2800, 45, 'User', 'draft_project', '2015-12-18 14:42:22', 'You have created a new Development <a href="http://www.mydesichef.com/realestate_development_ci3/development/38">http://www.mydesichef.com/realestate_development_ci3/development/38</a>', 0, 38, 1, 0, NULL),
(2801, 45, 'project', 'upload_contract', '2015-12-18 14:56:09', 'You have <a href="http://www.mydesichef.com/realestate_development_ci3/startinvest/index/24">Uploaded</a> Investment process  contract copy on <a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 1, 0, NULL),
(2802, 45, 'project', 'upload_contract', '2015-12-18 14:57:22', 'You have <a href="http://www.mydesichef.com/realestate_development_ci3/startinvest/index/24">Uploaded</a> Investment process  contract copy on <a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 1, 0, NULL),
(2803, 45, 'project', 'approve_contract', '2015-12-18 14:58:42', 'Your Investment contract copy has been <a href="http://www.mydesichef.com/realestate_development_ci3/startinvest/index/24">Approve</a> on <a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a> by Real Estate team', 0, 24, 1, 0, NULL),
(2804, 45, 'project', 'approve_acknowledge_contract', '2015-12-18 14:59:27', 'Your Investment  acknowledgement has been <a href="http://www.mydesichef.com/realestate_development_ci3/startinvest/index/24">Approved</a> on <a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a> by Real Estate team', 0, 24, 1, 0, NULL),
(2805, 45, 'project', 'funded', '2015-12-18 14:59:27', 'You have Invested <a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> with $100.00 USD', 7, 24, 1, 0, NULL),
(2806, 7, 'project', 'funded', '2015-12-18 14:59:27', '<a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already created is Invested with $100.00 USD by <a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a>', 45, 24, 0, 0, NULL),
(2807, 1, 'project', 'funded', '2015-12-18 14:59:27', '<a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Followed is Invested with $100.00 USD by <a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a>', 7, 24, 0, 0, NULL),
(2808, 12, 'project', 'funded', '2015-12-18 14:59:27', '<a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Followed is Invested with $100.00 USD by <a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a>', 7, 24, 0, 0, NULL),
(2809, 11, 'project', 'funded', '2015-12-18 14:59:27', '<a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Followed is Invested with $100.00 USD by <a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a>', 7, 24, 0, 0, NULL),
(2810, 11, 'project', 'funded', '2015-12-18 14:59:27', '<a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Invested is Invested with $100.00 USD by <a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a>', 7, 24, 0, 0, NULL),
(2811, 12, 'project', 'funded', '2015-12-18 14:59:27', '<a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Invested is Invested with $100.00 USD by <a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a>', 7, 24, 0, 0, NULL),
(2812, 11, 'project', 'funded', '2015-12-18 14:59:27', '<a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Invested is Invested with $100.00 USD by <a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a>', 7, 24, 0, 0, NULL),
(2813, 11, 'project', 'funded', '2015-12-18 14:59:27', '<a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Commented is Invested with $100.00 USD by <a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a>', 7, 24, 0, 0, NULL),
(2814, 45, 'project', 'approve_acknowledge_contract', '2015-12-18 14:59:32', 'Your Investment  acknowledgement has been <a href="http://www.mydesichef.com/realestate_development_ci3/startinvest/index/24">Approved</a> on <a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a> by Real Estate team', 0, 24, 1, 0, NULL),
(2815, 45, 'project', 'confirmed_submmited_tracking', '2015-12-18 14:59:54', 'You have <a href="http://www.mydesichef.com/realestate_development_ci3/startinvest/index/24">Approved and Confirmed</a> delivery of  share certificate/document  on <a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 1, 0, NULL),
(2816, 45, 'User', 'draft_project', '2015-12-21 11:26:12', 'You have created a new Development <a href="http://www.mydesichef.com/realestate_development_ci3/development/39">http://www.mydesichef.com/realestate_development_ci3/development/39</a>', 0, 39, 0, 0, NULL),
(2817, 0, 'project', 'follow', '2015-12-21 11:55:53', 'You have started Following <a href="http://www.mydesichef.com/realestate_development_ci3/company/rock-info">Rock Info</a>', 0, 0, 0, 56, NULL),
(2818, 46, 'project', 'follow', '2015-12-21 11:55:53', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following you created <a href="http://www.mydesichef.com/realestate_development_ci3/company/rock-info">Rock Info</a>', 0, 0, 0, 56, NULL),
(2819, 45, 'project', 'follow', '2015-12-21 11:55:53', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Following <a href="http://www.mydesichef.com/realestate_development_ci3/company/rock-info">Rock Info</a> you already Following', 0, 0, 0, 56, NULL),
(2820, 0, 'company', 'unfollow', '2015-12-21 11:55:59', 'You have started Unfollowing <a href="http://www.mydesichef.com/realestate_development_ci3/company/rock-info">Rock Info</a>', 0, 0, 0, 56, NULL),
(2821, 46, 'company', 'unfollow', '2015-12-21 11:55:59', '<a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a> has started Unfollowing you created <a href="http://www.mydesichef.com/realestate_development_ci3/company/rock-info">Rock Info</a>', 0, 0, 0, 56, NULL),
(2822, 45, 'project', 'intrest_request', '2015-12-21 11:58:42', 'You have expressed your interest to become Investment on <a href="http://www.mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a>', 0, 1, 0, 0, NULL),
(2823, 1, 'project', 'intrest_request', '2015-12-21 11:58:42', 'You have received <a href="http://www.mydesichef.com/realestate_development_ci3/equity/dashboard/1">Interest</a> to become Investment on <a href="http://www.mydesichef.com/realestate_development_ci3/development/smile-buddies">ABC Corp</a> from <a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a>', 45, 1, 0, 0, NULL),
(2824, 45, 'project', 'upload_contract', '2015-12-21 12:02:42', 'You have <a href="http://www.mydesichef.com/realestate_development_ci3/startinvest/index/24">Uploaded</a> Investment process  contract copy on <a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 0, 0, NULL),
(2825, 45, 'project', 'approve_contract', '2015-12-21 12:08:56', 'Your Investment contract copy has been <a href="http://www.mydesichef.com/realestate_development_ci3/startinvest/index/24">Approve</a> on <a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a> by Real Estate team', 0, 24, 0, 0, NULL),
(2826, 45, 'project', 'acknowledge_contract', '2015-12-21 12:09:17', 'You have <a href="http://www.mydesichef.com/realestate_development_ci3/startinvest/index/24">confirmed & uploaded</a> Investment  acknowledgement information on <a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 0, 0, NULL),
(2827, 45, 'project', 'approve_acknowledge_contract', '2015-12-21 12:10:00', 'Your Investment  acknowledgement has been <a href="http://www.mydesichef.com/realestate_development_ci3/startinvest/index/24">Approved</a> on <a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a> by Real Estate team', 0, 24, 0, 0, NULL),
(2828, 45, 'project', 'funded', '2015-12-21 12:10:00', 'You have Invested <a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> with $100.00 USD', 7, 24, 0, 0, NULL),
(2829, 7, 'project', 'funded', '2015-12-21 12:10:00', '<a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already created is Invested with $100.00 USD by <a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a>', 45, 24, 0, 0, NULL),
(2830, 1, 'project', 'funded', '2015-12-21 12:10:00', '<a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Followed is Invested with $100.00 USD by <a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a>', 7, 24, 0, 0, NULL),
(2831, 12, 'project', 'funded', '2015-12-21 12:10:00', '<a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Followed is Invested with $100.00 USD by <a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a>', 7, 24, 0, 0, NULL),
(2832, 11, 'project', 'funded', '2015-12-21 12:10:00', '<a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Followed is Invested with $100.00 USD by <a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a>', 7, 24, 0, 0, NULL),
(2833, 11, 'project', 'funded', '2015-12-21 12:10:00', '<a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Invested is Invested with $100.00 USD by <a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a>', 7, 24, 0, 0, NULL),
(2834, 12, 'project', 'funded', '2015-12-21 12:10:00', '<a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Invested is Invested with $100.00 USD by <a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a>', 7, 24, 0, 0, NULL),
(2835, 11, 'project', 'funded', '2015-12-21 12:10:00', '<a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Invested is Invested with $100.00 USD by <a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a>', 7, 24, 0, 0, NULL),
(2836, 11, 'project', 'funded', '2015-12-21 12:10:00', '<a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Lorem ipsum</a> you already Commented is Invested with $100.00 USD by <a href="http://www.mydesichef.com/realestate_development_ci3/user/vashuPatel45">Vashu Patel</a>', 7, 24, 0, 0, NULL),
(2837, 45, 'project', 'approve_acknowledge_contract', '2015-12-21 12:10:02', 'Your Investment  acknowledgement has been <a href="http://www.mydesichef.com/realestate_development_ci3/startinvest/index/24">Approved</a> on <a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a> by Real Estate team', 0, 24, 0, 0, NULL),
(2838, 45, 'project', 'approve_acknowledge_contract', '2015-12-21 12:10:03', 'Your Investment  acknowledgement has been <a href="http://www.mydesichef.com/realestate_development_ci3/startinvest/index/24">Approved</a> on <a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a> by Real Estate team', 0, 24, 0, 0, NULL),
(2839, 45, 'project', 'approve_acknowledge_contract', '2015-12-21 12:10:03', 'Your Investment  acknowledgement has been <a href="http://www.mydesichef.com/realestate_development_ci3/startinvest/index/24">Approved</a> on <a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a> by Real Estate team', 0, 24, 0, 0, NULL),
(2840, 45, 'project', 'approve_acknowledge_contract', '2015-12-21 12:10:04', 'Your Investment  acknowledgement has been <a href="http://www.mydesichef.com/realestate_development_ci3/startinvest/index/24">Approved</a> on <a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a> by Real Estate team', 0, 24, 0, 0, NULL),
(2841, 45, 'project', 'confirmed_submmited_tracking', '2015-12-21 12:11:18', 'You have <a href="http://www.mydesichef.com/realestate_development_ci3/startinvest/index/24">Approved and Confirmed</a> delivery of  share certificate/document  on <a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 0, 0, NULL),
(2842, 45, 'project', 'upload_contract', '2015-12-21 13:41:27', 'You have <a href="http://www.mydesichef.com/realestate_development_ci3/startinvest/index/24">Uploaded</a> Investment process  contract copy on <a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 0, 0, NULL),
(2843, 45, 'project', 'approve_contract', '2015-12-21 13:47:09', 'Your Investment contract copy has been <a href="http://www.mydesichef.com/realestate_development_ci3/startinvest/index/24">Approve</a> on <a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a> by Real Estate team', 0, 24, 0, 0, NULL),
(2844, 45, 'project', 'acknowledge_contract', '2015-12-21 13:51:02', 'You have <a href="http://www.mydesichef.com/realestate_development_ci3/startinvest/index/24">confirmed & uploaded</a> Investment  acknowledgement information on <a href="http://www.mydesichef.com/realestate_development_ci3/development/lorem-ipsum">Rockers Technology</a>', 0, 24, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_follow`
--

CREATE TABLE IF NOT EXISTS `user_follow` (
  `follower_id` int(11) NOT NULL,
  `follow_user_id` int(11) NOT NULL,
  `follow_by_user_id` int(11) NOT NULL,
  `user_follow_date` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=138 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_follow`
--

INSERT INTO `user_follow` (`follower_id`, `follow_user_id`, `follow_by_user_id`, `user_follow_date`) VALUES
(11, 12, 7, '2015-11-19 15:35:33'),
(12, 12, 7, '2015-11-19 15:35:35'),
(13, 16, 25, '2015-11-19 18:20:28'),
(14, 16, 24, '2015-11-19 18:30:31'),
(15, 16, 21, '2015-11-19 18:31:00'),
(16, 16, 22, '2015-11-19 18:31:52'),
(17, 16, 23, '2015-11-19 18:32:17'),
(18, 7, 27, '2015-11-20 10:15:34'),
(19, 7, 27, '2015-11-20 10:15:40'),
(20, 7, 27, '2015-11-20 10:15:44'),
(21, 7, 27, '2015-11-20 10:15:45'),
(22, 7, 27, '2015-11-20 10:15:51'),
(23, 7, 27, '2015-11-20 10:15:52'),
(24, 7, 27, '2015-11-20 10:15:52'),
(25, 7, 27, '2015-11-20 10:15:55'),
(26, 7, 27, '2015-11-20 10:15:55'),
(27, 7, 27, '2015-11-20 10:15:57'),
(28, 7, 27, '2015-11-20 10:16:27'),
(29, 7, 27, '2015-11-20 10:16:27'),
(30, 1, 27, '2015-11-20 10:23:07'),
(38, 7, 1, '2015-11-20 11:34:06'),
(132, 25, 1, '2015-12-01 12:46:07'),
(129, 11, 1, '2015-12-01 12:34:04'),
(126, 7, 25, '2015-11-30 10:53:54'),
(125, 7, 25, '2015-11-30 10:53:54'),
(124, 7, 25, '2015-11-30 10:53:54'),
(123, 7, 25, '2015-11-30 10:53:53'),
(122, 7, 25, '2015-11-30 10:53:53'),
(121, 7, 25, '2015-11-30 10:53:53'),
(120, 7, 25, '2015-11-30 10:53:53'),
(119, 7, 25, '2015-11-30 10:53:53'),
(118, 7, 25, '2015-11-30 10:53:53'),
(117, 7, 25, '2015-11-30 10:53:52'),
(116, 7, 25, '2015-11-30 10:53:52'),
(115, 7, 25, '2015-11-30 10:53:52'),
(114, 7, 25, '2015-11-30 10:53:52'),
(113, 7, 25, '2015-11-30 10:53:52'),
(112, 7, 25, '2015-11-30 10:53:52'),
(111, 7, 25, '2015-11-30 10:53:51'),
(110, 7, 25, '2015-11-30 10:53:51'),
(109, 7, 25, '2015-11-30 10:53:51'),
(108, 7, 25, '2015-11-30 10:53:51'),
(107, 7, 25, '2015-11-30 10:53:51'),
(106, 7, 25, '2015-11-30 10:53:51'),
(105, 7, 25, '2015-11-30 10:53:50'),
(104, 7, 25, '2015-11-30 10:53:50'),
(103, 7, 25, '2015-11-30 10:53:50'),
(102, 7, 25, '2015-11-30 10:53:50'),
(101, 7, 25, '2015-11-30 10:53:50'),
(100, 7, 25, '2015-11-30 10:53:50'),
(99, 7, 25, '2015-11-30 10:53:49'),
(98, 7, 25, '2015-11-30 10:53:49'),
(97, 7, 25, '2015-11-30 10:53:49'),
(96, 7, 25, '2015-11-30 10:53:49'),
(95, 7, 25, '2015-11-30 10:53:49'),
(94, 7, 25, '2015-11-30 10:53:48'),
(93, 7, 25, '2015-11-30 10:53:48'),
(92, 7, 25, '2015-11-30 10:53:48'),
(91, 7, 25, '2015-11-30 10:53:48'),
(90, 7, 25, '2015-11-30 10:53:48'),
(89, 7, 25, '2015-11-30 10:53:47'),
(88, 7, 25, '2015-11-30 10:53:47'),
(87, 7, 25, '2015-11-30 10:53:47'),
(86, 7, 25, '2015-11-30 10:53:47'),
(85, 7, 25, '2015-11-30 10:53:46');

-- --------------------------------------------------------

--
-- Table structure for table `user_login`
--

CREATE TABLE IF NOT EXISTS `user_login` (
  `login_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `login_date_time` datetime NOT NULL,
  `login_ip` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=303 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_login`
--

INSERT INTO `user_login` (`login_id`, `user_id`, `login_date_time`, `login_ip`) VALUES
(1, 1, '2015-10-02 16:36:51', '27.109.7.130'),
(2, 1, '2015-10-02 16:49:31', '27.109.7.130'),
(3, 1, '2015-10-02 18:05:03', '27.109.7.130'),
(4, 1, '2015-10-05 10:36:20', '27.109.7.130'),
(5, 1, '2015-10-05 10:49:54', '27.109.7.130'),
(6, 2, '2015-10-05 10:54:00', '27.109.7.130'),
(7, 2, '2015-10-05 11:00:47', '27.109.7.130'),
(9, 4, '2015-10-05 14:27:20', '27.109.7.130'),
(10, 1, '2015-10-05 14:42:02', '27.109.7.130'),
(11, 5, '2015-10-05 15:56:51', '27.109.7.130'),
(12, 4, '2015-10-05 17:17:36', '27.109.7.130'),
(13, 1, '2015-10-05 18:05:33', '27.109.7.130'),
(14, 1, '2015-10-05 21:25:12', '27.109.7.130'),
(15, 1, '2015-10-06 12:44:17', '27.109.7.130'),
(16, 1, '2015-10-06 16:00:00', '27.109.7.130'),
(17, 1, '2015-10-07 11:41:58', '27.109.7.130'),
(18, 1, '2015-10-07 18:18:47', '150.107.210.187'),
(19, 1, '2015-10-07 18:43:22', '27.109.7.130'),
(20, 1, '2015-10-07 20:08:06', '27.109.7.130'),
(21, 6, '2015-10-08 12:16:20', '123.201.91.105'),
(22, 6, '2015-10-08 12:16:44', '123.201.91.105'),
(23, 6, '2015-10-08 12:19:23', '123.201.91.105'),
(24, 6, '2015-10-08 13:05:21', '123.201.91.105'),
(25, 1, '2015-10-08 16:00:24', '123.201.91.105'),
(26, 1, '2015-10-08 16:00:52', '123.201.91.105'),
(27, 1, '2015-10-08 16:36:34', '123.201.91.105'),
(28, 1, '2015-10-08 17:30:59', '123.201.91.105'),
(29, 1, '2015-10-09 10:26:46', '27.109.7.130'),
(30, 1, '2015-10-09 18:15:17', '27.109.7.130'),
(31, 1, '2015-10-12 13:51:32', '27.109.7.130'),
(32, 1, '2015-10-12 15:58:41', '27.109.7.130'),
(33, 1, '2015-10-13 19:28:25', '27.109.7.130'),
(34, 1, '2015-10-14 17:33:35', '27.109.7.130'),
(35, 4, '2015-10-22 13:46:11', '27.109.7.130'),
(36, 7, '2015-10-22 15:22:00', '123.201.58.117'),
(37, 7, '2015-10-22 16:06:29', '123.201.58.117'),
(38, 8, '2015-10-26 12:08:39', '219.91.173.161'),
(39, 8, '2015-10-26 12:22:44', '219.91.173.161'),
(40, 4, '2015-10-26 12:23:07', '27.109.7.130'),
(41, 8, '2015-10-26 12:23:34', '27.109.7.130'),
(42, 8, '2015-10-26 12:32:40', '27.109.7.130'),
(43, 7, '2015-10-26 13:07:47', '219.91.173.161'),
(44, 8, '2015-10-26 13:52:18', '27.109.7.130'),
(45, 1, '2015-10-26 14:09:10', '27.109.7.130'),
(46, 2, '2015-10-26 14:14:03', '27.109.7.130'),
(49, 1, '2015-10-26 14:26:54', '27.109.7.130'),
(52, 1, '2015-10-26 15:05:22', '27.109.7.130'),
(53, 8, '2015-10-26 15:07:27', '27.109.7.130'),
(54, 1, '2015-10-26 15:10:11', '27.109.7.130'),
(55, 1, '2015-10-26 15:10:12', '27.109.7.130'),
(56, 10, '2015-10-26 15:12:01', '27.109.7.130'),
(57, 1, '2015-10-26 15:12:59', '27.109.7.130'),
(58, 7, '2015-10-26 15:20:27', '219.91.173.161'),
(59, 8, '2015-10-26 15:50:12', '27.109.7.130'),
(60, 8, '2015-10-26 16:22:09', '27.109.7.130'),
(61, 8, '2015-10-26 18:18:05', '123.201.174.209'),
(62, 7, '2015-10-27 11:02:03', '150.107.209.40'),
(63, 11, '2015-10-27 11:15:06', '150.107.209.40'),
(64, 11, '2015-10-27 11:35:15', '150.107.209.40'),
(65, 7, '2015-10-27 14:04:02', '150.107.211.150'),
(66, 11, '2015-10-27 14:07:25', '150.107.211.150'),
(67, 7, '2015-10-27 15:01:18', '150.107.211.150'),
(68, 11, '2015-10-27 15:27:35', '27.109.7.130'),
(69, 7, '2015-10-27 15:28:08', '27.109.7.130'),
(70, 7, '2015-10-27 16:39:36', '27.109.7.130'),
(71, 11, '2015-10-27 16:55:16', '27.109.7.130'),
(72, 12, '2015-10-27 16:56:28', '27.109.7.130'),
(73, 7, '2015-10-28 10:51:53', '27.109.7.130'),
(74, 7, '2015-10-28 16:14:35', '27.109.7.130'),
(75, 7, '2015-10-29 11:31:13', '27.109.7.130'),
(76, 11, '2015-10-29 11:31:34', '27.109.7.130'),
(77, 1, '2015-10-29 16:04:43', '27.109.7.130'),
(78, 7, '2015-10-30 11:00:44', '27.109.7.130'),
(79, 1, '2015-10-30 15:08:56', '27.109.7.130'),
(80, 7, '2015-11-03 14:45:21', '27.109.7.130'),
(81, 1, '2015-11-03 14:46:44', '27.109.7.130'),
(82, 1, '2015-11-18 16:44:46', '123.201.12.31'),
(83, 1, '2015-11-19 10:12:25', '27.109.7.130'),
(84, 1, '2015-11-19 11:55:32', '27.109.7.130'),
(85, 1, '2015-11-19 11:56:52', '27.109.7.130'),
(86, 13, '2015-11-19 11:57:50', '27.109.7.130'),
(87, 1, '2015-11-19 14:01:33', '203.187.203.243'),
(91, 16, '2015-11-19 14:16:30', '27.109.7.130'),
(92, 16, '2015-11-19 14:17:04', '27.109.7.130'),
(93, 16, '2015-11-19 14:17:26', '27.109.7.130'),
(94, 16, '2015-11-19 14:17:47', '27.109.7.130'),
(96, 16, '2015-11-19 14:19:00', '27.109.7.130'),
(97, 16, '2015-11-19 14:19:36', '27.109.7.130'),
(98, 16, '2015-11-19 14:22:56', '27.109.7.130'),
(99, 7, '2015-11-19 14:25:23', '27.109.7.130'),
(100, 1, '2015-11-19 14:27:33', '203.187.203.243'),
(101, 18, '2015-11-19 14:34:10', '27.109.7.130'),
(102, 7, '2015-11-19 14:47:04', '27.109.7.130'),
(104, 16, '2015-11-19 15:07:03', '27.109.7.130'),
(105, 11, '2015-11-19 16:01:48', '27.109.7.130'),
(106, 7, '2015-11-19 16:02:30', '27.109.7.130'),
(107, 12, '2015-11-19 16:04:15', '27.109.7.130'),
(108, 7, '2015-11-19 17:16:37', '27.109.7.130'),
(109, 7, '2015-11-19 17:41:22', '27.109.7.130'),
(110, 16, '2015-11-19 17:43:57', '27.109.7.130'),
(111, 2, '2015-11-19 18:02:52', '27.109.7.130'),
(112, 21, '2015-11-19 18:06:30', '27.109.7.130'),
(113, 25, '2015-11-19 18:18:13', '27.109.7.130'),
(114, 24, '2015-11-19 18:28:03', '27.109.7.130'),
(115, 21, '2015-11-19 18:30:49', '27.109.7.130'),
(116, 22, '2015-11-19 18:31:47', '27.109.7.130'),
(117, 23, '2015-11-19 18:32:11', '27.109.7.130'),
(118, 16, '2015-11-19 18:37:06', '27.109.7.130'),
(119, 16, '2015-11-19 18:44:00', '27.109.7.130'),
(120, 16, '2015-11-19 18:46:53', '27.109.7.130'),
(121, 27, '2015-11-20 10:15:23', '27.109.7.130'),
(122, 7, '2015-11-20 10:56:12', '27.109.7.130'),
(123, 1, '2015-11-20 11:06:28', '27.109.7.130'),
(124, 1, '2015-11-20 11:06:40', '27.109.7.130'),
(126, 27, '2015-11-20 11:25:25', '27.109.7.130'),
(127, 1, '2015-11-20 11:29:03', '27.109.7.130'),
(128, 34, '2015-11-20 11:36:14', '27.109.7.130'),
(129, 7, '2015-11-20 11:39:16', '27.109.7.130'),
(132, 16, '2015-11-20 13:59:19', '27.109.7.130'),
(133, 1, '2015-11-20 14:09:01', '27.109.7.130'),
(134, 16, '2015-11-20 14:14:38', '27.109.7.130'),
(135, 1, '2015-11-20 14:23:16', '27.109.7.130'),
(136, 16, '2015-11-20 14:27:25', '27.109.7.130'),
(137, 1, '2015-11-20 14:29:55', '27.109.7.130'),
(138, 2, '2015-11-20 14:34:22', '27.109.7.130'),
(139, 16, '2015-11-20 14:36:34', '27.109.7.130'),
(140, 1, '2015-11-20 14:36:56', '27.109.7.130'),
(141, 11, '2015-11-20 14:39:30', '27.109.7.130'),
(142, 1, '2015-11-20 14:40:02', '27.109.7.130'),
(143, 11, '2015-11-20 14:41:12', '27.109.7.130'),
(144, 1, '2015-11-20 14:42:36', '27.109.7.130'),
(145, 1, '2015-11-20 14:43:45', '27.109.7.130'),
(146, 11, '2015-11-20 14:45:03', '27.109.7.130'),
(147, 1, '2015-11-20 14:46:34', '27.109.7.130'),
(148, 11, '2015-11-20 14:47:40', '27.109.7.130'),
(149, 1, '2015-11-20 14:53:27', '27.109.7.130'),
(150, 11, '2015-11-20 15:08:07', '27.109.7.130'),
(151, 16, '2015-11-20 15:52:00', '27.109.7.130'),
(152, 1, '2015-11-20 15:57:31', '27.109.7.130'),
(153, 16, '2015-11-20 15:59:24', '27.109.7.130'),
(154, 1, '2015-11-20 16:01:47', '27.109.7.130'),
(155, 16, '2015-11-20 16:05:40', '27.109.7.130'),
(156, 16, '2015-11-20 16:10:02', '27.109.7.130'),
(157, 16, '2015-11-20 16:15:37', '27.109.7.130'),
(158, 1, '2015-11-20 16:18:42', '27.109.7.130'),
(159, 13, '2015-11-20 16:22:10', '123.201.98.3'),
(160, 7, '2015-11-20 16:30:46', '123.201.98.3'),
(161, 7, '2015-11-20 16:51:16', '203.187.234.4'),
(162, 1, '2015-11-20 16:57:06', '27.109.7.130'),
(163, 11, '2015-11-20 16:58:59', '203.187.234.4'),
(164, 16, '2015-11-20 17:01:04', '27.109.7.130'),
(165, 7, '2015-11-20 17:11:45', '27.109.7.130'),
(166, 16, '2015-11-20 17:16:30', '27.109.7.130'),
(167, 1, '2015-11-20 17:21:18', '27.109.7.130'),
(168, 16, '2015-11-20 17:25:34', '27.109.7.130'),
(169, 37, '2015-11-20 17:43:15', '27.109.7.130'),
(170, 16, '2015-11-20 17:56:11', '27.109.7.130'),
(171, 1, '2015-11-20 18:17:09', '27.109.7.130'),
(172, 37, '2015-11-20 18:40:08', '27.109.7.130'),
(173, 16, '2015-11-20 19:19:32', '27.109.7.130'),
(174, 7, '2015-11-23 17:04:27', '219.91.162.103'),
(175, 7, '2015-11-25 12:14:02', '123.201.100.5'),
(176, 7, '2015-11-25 15:33:15', '27.109.7.130'),
(177, 7, '2015-11-25 15:33:51', '27.109.7.130'),
(178, 12, '2015-11-25 15:42:28', '27.109.7.130'),
(179, 7, '2015-11-25 16:14:14', '27.109.7.130'),
(180, 7, '2015-11-25 16:43:16', '27.109.7.130'),
(181, 1, '2015-11-26 10:48:12', '27.109.7.130'),
(182, 7, '2015-11-26 14:11:44', '27.109.7.130'),
(183, 13, '2015-11-26 15:43:17', '27.109.7.130'),
(184, 7, '2015-11-26 15:47:24', '27.109.7.130'),
(185, 11, '2015-11-26 16:22:08', '27.109.7.130'),
(186, 1, '2015-11-26 18:35:58', '27.109.7.130'),
(187, 1, '2015-11-27 09:53:42', '27.109.7.130'),
(188, 1, '2015-11-27 10:44:21', '27.109.7.130'),
(189, 7, '2015-11-27 11:15:11', '27.109.7.130'),
(190, 7, '2015-11-27 11:35:12', '27.109.7.130'),
(191, 11, '2015-11-27 13:46:37', '27.109.7.130'),
(192, 7, '2015-11-27 13:52:53', '27.109.7.130'),
(193, 11, '2015-11-27 15:02:50', '27.109.7.130'),
(194, 7, '2015-11-27 16:51:57', '123.201.58.233'),
(195, 7, '2015-11-27 16:54:22', '123.201.58.233'),
(196, 7, '2015-11-30 10:30:28', '27.109.7.130'),
(197, 11, '2015-11-30 10:35:35', '27.109.7.130'),
(198, 25, '2015-11-30 10:52:30', '27.109.7.130'),
(199, 11, '2015-11-30 10:57:18', '27.109.7.130'),
(200, 7, '2015-11-30 11:38:14', '27.109.7.130'),
(201, 11, '2015-11-30 11:39:34', '27.109.7.130'),
(202, 12, '2015-11-30 11:56:06', '123.201.155.200'),
(203, 12, '2015-11-30 11:57:30', '123.201.155.200'),
(204, 11, '2015-11-30 12:09:49', '123.201.155.200'),
(205, 7, '2015-11-30 13:31:32', '27.109.7.130'),
(206, 11, '2015-11-30 13:36:23', '27.109.7.130'),
(207, 11, '2015-11-30 13:40:41', '27.109.7.130'),
(208, 12, '2015-11-30 13:55:56', '27.109.7.130'),
(209, 11, '2015-11-30 14:44:57', '27.109.7.130'),
(210, 11, '2015-11-30 15:02:30', '27.109.7.130'),
(211, 7, '2015-11-30 15:03:24', '27.109.7.130'),
(212, 40, '2015-11-30 15:29:48', '27.109.7.130'),
(213, 11, '2015-11-30 15:44:42', '27.109.7.130'),
(214, 1, '2015-11-30 18:08:03', '27.109.7.130'),
(215, 1, '2015-11-30 18:19:47', '27.109.7.130'),
(216, 7, '2015-12-01 09:11:15', '27.109.7.130'),
(217, 7, '2015-12-01 10:15:04', '27.109.7.130'),
(218, 1, '2015-12-01 10:46:15', '27.109.7.130'),
(219, 7, '2015-12-01 10:50:34', '27.109.7.130'),
(220, 1, '2015-12-01 10:51:41', '27.109.7.130'),
(221, 11, '2015-12-01 10:53:43', '27.109.7.130'),
(222, 7, '2015-12-01 10:54:20', '27.109.7.130'),
(223, 1, '2015-12-01 12:04:10', '123.201.161.193'),
(224, 1, '2015-12-01 12:04:31', '123.201.161.193'),
(225, 1, '2015-12-01 12:06:39', '123.201.161.193'),
(226, 1, '2015-12-01 12:25:54', '123.201.161.193'),
(227, 1, '2015-12-01 12:27:19', '123.201.161.193'),
(228, 1, '2015-12-01 12:35:00', '123.201.161.193'),
(229, 43, '2015-12-01 13:23:04', '123.201.161.193'),
(230, 11, '2015-12-01 13:33:30', '27.109.7.130'),
(231, 12, '2015-12-01 13:54:56', '27.109.7.130'),
(232, 7, '2015-12-01 15:23:10', '27.109.7.130'),
(233, 1, '2015-12-01 15:25:51', '27.109.7.130'),
(234, 7, '2015-12-01 16:01:39', '27.109.7.130'),
(235, 1, '2015-12-01 16:51:02', '27.109.7.130'),
(236, 1, '2015-12-02 10:34:10', '27.109.7.130'),
(237, 7, '2015-12-02 10:34:54', '27.109.7.130'),
(238, 44, '2015-12-02 10:39:23', '27.109.7.130'),
(239, 44, '2015-12-02 10:42:17', '27.109.7.130'),
(240, 45, '2015-12-02 10:45:35', '27.109.7.130'),
(241, 1, '2015-12-02 10:46:40', '27.109.7.130'),
(242, 45, '2015-12-02 10:52:03', '27.109.7.130'),
(243, 7, '2015-12-02 10:58:12', '27.109.7.130'),
(244, 1, '2015-12-02 11:05:29', '27.109.7.130'),
(245, 43, '2015-12-02 11:40:03', '27.109.7.130'),
(246, 45, '2015-12-02 11:40:16', '27.109.7.130'),
(247, 45, '2015-12-02 11:40:51', '27.109.7.130'),
(248, 8, '2015-12-02 11:54:26', '203.109.84.76'),
(249, 1, '2015-12-02 11:55:21', '203.109.84.76'),
(250, 43, '2015-12-02 13:16:58', '27.109.7.130'),
(251, 46, '2015-12-02 13:18:13', '27.109.7.130'),
(252, 46, '2015-12-02 13:19:44', '27.109.7.130'),
(253, 45, '2015-12-02 13:52:41', '27.109.7.130'),
(254, 46, '2015-12-02 13:53:41', '27.109.7.130'),
(255, 45, '2015-12-02 15:17:39', '27.109.7.130'),
(256, 7, '2015-12-02 15:20:15', '27.109.7.130'),
(257, 1, '2015-12-02 15:21:11', '27.109.7.130'),
(258, 43, '2015-12-02 15:26:57', '27.109.7.130'),
(259, 1, '2015-12-02 16:03:16', '203.109.84.76'),
(260, 45, '2015-12-02 16:06:58', '27.109.7.130'),
(261, 7, '2015-12-02 16:08:50', '27.109.7.130'),
(262, 7, '2015-12-02 16:15:30', '27.109.7.130'),
(263, 1, '2015-12-02 16:19:47', '27.109.7.130'),
(264, 1, '2015-12-02 16:23:46', '203.109.84.76'),
(265, 45, '2015-12-02 17:53:15', '27.109.7.130'),
(266, 7, '2015-12-02 17:56:13', '27.109.7.130'),
(267, 1, '2015-12-02 17:59:22', '27.109.7.130'),
(268, 46, '2015-12-02 18:09:50', '27.109.7.130'),
(269, 47, '2015-12-04 17:40:52', '27.109.7.130'),
(270, 47, '2015-12-04 17:43:34', '27.109.7.130'),
(271, 8, '2015-12-04 19:28:52', '27.109.7.130'),
(272, 8, '2015-12-08 14:27:59', '123.201.174.162'),
(273, 8, '2015-12-08 17:29:20', '219.91.173.212'),
(274, 1, '2015-12-10 10:51:07', '27.109.7.130'),
(275, 2, '2015-12-15 11:28:52', '27.109.7.130'),
(276, 8, '2015-12-15 15:01:16', '123.201.95.154'),
(277, 1, '2015-12-15 15:40:10', '27.109.7.130'),
(278, 8, '2015-12-15 15:44:09', '27.109.7.130'),
(279, 1, '2015-12-16 10:27:19', '27.109.7.130'),
(280, 1, '2015-12-16 16:55:11', '27.109.7.130'),
(281, 1, '2015-12-16 19:11:47', '27.109.7.130'),
(282, 8, '2015-12-16 19:59:22', '27.109.7.130'),
(283, 1, '2015-12-16 19:59:48', '27.109.7.130'),
(284, 1, '2015-12-17 10:31:00', '27.109.7.130'),
(285, 1, '2015-12-17 16:24:51', '27.109.7.130'),
(286, 1, '2015-12-17 19:14:03', '27.109.7.130'),
(287, 1, '2015-12-18 11:02:26', '27.109.7.130'),
(288, 45, '2015-12-18 14:41:42', '123.201.160.118'),
(289, 45, '2015-12-18 15:19:46', '27.109.7.130'),
(290, 45, '2015-12-18 15:36:23', '27.109.7.130'),
(291, 7, '2015-12-18 15:42:26', '27.109.7.130'),
(292, 7, '2015-12-18 15:52:39', '27.109.7.130'),
(293, 46, '2015-12-18 15:58:05', '27.109.7.130'),
(294, 45, '2015-12-18 18:07:19', '27.109.7.130'),
(295, 8, '2015-12-18 20:37:44', '27.109.7.130'),
(296, 8, '2015-12-21 10:40:51', '27.109.7.130'),
(297, 45, '2015-12-21 11:20:58', '203.109.65.159'),
(298, 7, '2015-12-21 13:27:08', '27.109.7.130'),
(299, 11, '2015-12-21 13:38:07', '27.109.7.130'),
(300, 45, '2015-12-21 13:39:49', '27.109.7.130'),
(301, 12, '2015-12-21 13:49:10', '27.109.7.130'),
(302, 45, '2015-12-21 13:49:27', '203.109.65.159');

-- --------------------------------------------------------

--
-- Table structure for table `user_notification`
--

CREATE TABLE IF NOT EXISTS `user_notification` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `you_back_alert` tinyint(1) NOT NULL,
  `you_follow_alert` tinyint(1) NOT NULL,
  `new_pledge_alert` tinyint(1) NOT NULL,
  `new_comment_alert` tinyint(1) NOT NULL,
  `new_follow_alert` tinyint(1) NOT NULL,
  `new_updates_alert` tinyint(1) NOT NULL,
  `social_notification_alert` tinyint(1) NOT NULL,
  `comment_reply_alert` tinyint(1) NOT NULL,
  `creator_pledge_alert` tinyint(1) NOT NULL,
  `creator_comment_alert` tinyint(1) NOT NULL,
  `creator_follow_alert` tinyint(1) NOT NULL,
  `creator_newup_alert` tinyint(1) NOT NULL,
  `user_alert` tinyint(1) DEFAULT NULL,
  `add_fund` tinyint(1) DEFAULT NULL,
  `project_alert` tinyint(1) NOT NULL,
  `comment_alert` tinyint(1) NOT NULL,
  `update_alert` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_notification`
--

INSERT INTO `user_notification` (`id`, `user_id`, `you_back_alert`, `you_follow_alert`, `new_pledge_alert`, `new_comment_alert`, `new_follow_alert`, `new_updates_alert`, `social_notification_alert`, `comment_reply_alert`, `creator_pledge_alert`, `creator_comment_alert`, `creator_follow_alert`, `creator_newup_alert`, `user_alert`, `add_fund`, `project_alert`, `comment_alert`, `update_alert`) VALUES
(1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, NULL, NULL, 0, 0, 0),
(2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, NULL, NULL, 0, 0, 0),
(4, 4, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, NULL, NULL, 0, 0, 0),
(5, 5, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, NULL, NULL, 0, 0, 0),
(6, 6, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, NULL, NULL, 0, 0, 0),
(7, 7, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, 1, 0, 1, 1),
(8, 8, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, NULL, NULL, 0, 0, 0),
(10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, NULL, NULL, 0, 0, 0),
(11, 11, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, NULL, NULL, 0, 0, 0),
(12, 12, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, NULL, NULL, 0, 0, 0),
(13, 13, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, NULL, NULL, 0, 0, 0),
(16, 16, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, NULL, NULL, 0, 0, 0),
(18, 18, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, NULL, NULL, 0, 0, 0),
(20, 20, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, NULL, NULL, 0, 0, 0),
(21, 21, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, NULL, NULL, 0, 0, 0),
(22, 22, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, NULL, NULL, 0, 0, 0),
(23, 23, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, NULL, NULL, 0, 0, 0),
(24, 24, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, NULL, NULL, 0, 0, 0),
(25, 25, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, NULL, NULL, 0, 0, 0),
(26, 26, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, NULL, NULL, 0, 0, 0),
(27, 27, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, NULL, NULL, 0, 0, 0),
(28, 28, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, NULL, NULL, 0, 0, 0),
(29, 29, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, NULL, NULL, 0, 0, 0),
(30, 30, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, NULL, NULL, 0, 0, 0),
(31, 31, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, NULL, NULL, 0, 0, 0),
(32, 32, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, NULL, NULL, 0, 0, 0),
(34, 34, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, NULL, NULL, 0, 0, 0),
(37, 37, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, NULL, NULL, 0, 0, 0),
(38, 38, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, NULL, NULL, 0, 0, 0),
(39, 39, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, NULL, NULL, 0, 0, 0),
(40, 40, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, NULL, NULL, 0, 0, 0),
(43, 43, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, NULL, NULL, 0, 0, 0),
(44, 44, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, NULL, NULL, 0, 0, 0),
(45, 45, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, NULL, NULL, 0, 0, 0),
(46, 46, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, NULL, NULL, 0, 0, 0),
(47, 47, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, NULL, NULL, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_setting`
--

CREATE TABLE IF NOT EXISTS `user_setting` (
  `user_setting_id` int(11) NOT NULL,
  `login_with` varchar(255) DEFAULT NULL,
  `admin_activation` varchar(255) DEFAULT NULL,
  `email_varification` varchar(255) DEFAULT NULL,
  `auto_login` varchar(255) DEFAULT NULL,
  `notification_mail` varchar(255) DEFAULT NULL,
  `welcome_mail` varchar(255) DEFAULT NULL,
  `password_change_logout` varchar(255) DEFAULT NULL,
  `enable_openid` varchar(255) DEFAULT NULL,
  `user_switch_language` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_setting`
--

INSERT INTO `user_setting` (`user_setting_id`, `login_with`, `admin_activation`, `email_varification`, `auto_login`, `notification_mail`, `welcome_mail`, `password_change_logout`, `enable_openid`, `user_switch_language`) VALUES
(1, 'b', 'b', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `version_update`
--

CREATE TABLE IF NOT EXISTS `version_update` (
  `ver_id` int(11) NOT NULL,
  `version` varchar(255) DEFAULT NULL,
  `description` text,
  `license` varchar(255) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `date` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `version_update`
--

INSERT INTO `version_update` (`ver_id`, `version`, `description`, `license`, `product_name`, `date`) VALUES
(3, '5.0.3', 'Occasionally, we may release small updates in the form of a patch to one\n or several source file. These source files are regular zip files that \ncontain updated files.<br><br>There are two methods for updating - \nthe easiest is the one-click update. If it doesn''t work, or you just \nprefer to be more hands-on, you can follow the manual update process. <br><br>If\n you are working with a source code ,Make a backup of everything in your\n site, including the database. This is extremely important so that you \ncan roll back to a running site no matter what happens during migration.<br><br>Although\n we usually produce upgrades in order to improve a functionality, there \nare risks involved—including the possibility that the upgrade will \nworsen the website.  ', 'kiwjw4tyn3nkcf!4zcrf', 'FUND001', '2014-02-27 00:00:00'),
(4, '5.0.4', 'ASDASDFGFSDGDDDG ASDFGFSDGDDDG ASDFGFSDGDDDG ASDFGFSDGDDDG     FGFSDGDDDGASDFGFSDGDDDG  ', 'ecmxfkpon9iefode58d4', 'FUND001', '2014-02-26 00:00:00'),
(5, '5.0.4', 'Occasionally, we may release small updates in the form of a patch to one\n or several source file. These source files are regular zip files that \ncontain updated files.<br><br>There are two methods for updating - \nthe easiest is the one-click update. If it doesn''t work, or you just \nprefer to be more hands-on, you can follow the manual update process. <br><br>If\n you are working with a source code ,Make a backup of everything in your\n site, including the database. This is extremely important so that you \ncan roll back to a running site no matter what happens during migration.<br><br>Although\n we usually produce upgrades in order to improve a functionality, there \nare risks involved—including the possibility that the upgrade will \nworsen the website.  ', 'i2n0t5nehgylyed!ewmc', 'FUND001', '2014-02-25 00:00:00'),
(6, '5.0.5', 'Occasionally, we may release small updates in the form of a patch to one or several source file. These source files are regular zip files that contain updated files.<br><br>There are two methods for updating - the easiest is the one-click update. If it doesn''t work, or you just prefer to be more hands-on, you can follow the manual update process. <br><br>If you are working with a source code ,Make a backup of everything in your site, including the database. This is extremely important so that you can roll back to a running site no matter what happens during migration.<br><br>Although we usually produce upgrades in order to improve a functionality, there are risks involved—including the possibility that the upgrade will worsen the website.  ', 'phttm9vquw!yfg05!344', 'FUND001', '2014-02-19 00:00:00'),
(7, '5.0.6', 'Occasionally, we may release small updates in the form of a patch to one or several source file. These source files are regular zip files that contain updated files.<br><br>There are two methods for updating - the easiest is the one-click update. If it doesn''t work, or you just prefer to be more hands-on, you can follow the manual update process. <br><br>If you are working with a source code ,Make a backup of everything in your site, including the database. This is extremely important so that you can roll back to a running site no matter what happens during migration.<br><br>Although we usually produce upgrades in order to improve a functionality, there are risks involved—including the possibility that the upgrade will worsen the website.  ', 'sfv4u02v@cjfd%2cxbip', 'FUND001', '2014-02-18 00:00:00'),
(8, '5.0.7', 'Occasionally, we may release small updates in the form of a patch to one or several source file. These source files are regular zip files that contain updated files.<br><br>There are two methods for updating - the easiest is the one-click update. If it doesn''t work, or you just prefer to be more hands-on, you can follow the manual update process. <br><br>If you are working with a source code ,Make a backup of everything in your site, including the database. This is extremely important so that you can roll back to a running site no matter what happens during migration.<br><br>Although we usually produce upgrades in order to improve a functionality, there are risks involved—including the possibility that the upgrade will worsen the website.  ', 'e8cua2@88b1xhbkjr8s4', 'FUND001', '2014-02-24 00:00:00'),
(9, '5.0.8', 'Occasionally, we may release small updates in the form of a patch to one or several source file. These source files are regular zip files that contain updated files.<br><br>There are two methods for updating - the easiest is the one-click update. If it doesn''t work, or you just prefer to be more hands-on, you can follow the manual update process. <br><br>If you are working with a source code ,Make a backup of everything in your site, including the database. This is extremely important so that you can roll back to a running site no matter what happens during migration.<br><br>Although we usually produce upgrades in order to improve a functionality, there are risks involved—including the possibility that the upgrade will worsen the website.  ', 'u@kr!dkm6jsa53pmjsxa', 'FUND001', '2014-02-25 00:00:00'),
(10, '5.1.0', '<div>ticket no.</div><div><a rel="nofollow noreferrer" href="https://www.google.com/url?q=https%3A%2F%2Ffundraising.zendesk.com%2Ftickets%2F890%3Fcol%3D25895847%26desc%3D1%26order%3Dcreated%26page%3D1&sa=D&sntz=1&usg=AFQjCNGFx3TzST_u-gIP73XLGpGTDuZGEg" dir="ltr" target="_blank">https://fundraising.zendesk.<wbr>com/tickets/890?col=25895847&<wbr>desc=1&order=created&page=1</a><br>\n</div><div><br></div><div>1.other feature tab is working i<a href="http://groupfund.me" target="_blank"></a>. <br>\n</div><div><strong><br></strong></div><div><a rel="nofollow noreferrer" href="https://www.google.com/url?q=https%3A%2F%2Ffundraising.zendesk.com%2Ftickets%2F889%3Fcol%3D25895847%26desc%3D1%26order%3Dcreated%26page%3D1&sa=D&sntz=1&usg=AFQjCNE2ik5cqH8NSOwrKiTu4545qaNVGQ" dir="ltr" target="_blank">https://fundraising.zendesk.<wbr>com/tickets/889?col=25895847&<wbr>desc=1&order=created&page=1</a><br>\n</div><div><br></div><div>2. In admin , user list should be allow to search by  username , email address , ip address - <strong></strong></div>\n<div><br></div><div><a rel="nofollow noreferrer" href="https://www.google.com/url?q=https%3A%2F%2Ffundraising.zendesk.com%2Ftickets%2F888%3Fcol%3D25895847%26desc%3D1%26order%3Dcreated%26page%3D1&sa=D&sntz=1&usg=AFQjCNEnoigg0BPiUbxaiYc4G6etqgx-IQ" dir="ltr" target="_blank">https://fundraising.zendesk.<wbr>com/tickets/888?col=25895847&<wbr>desc=1&order=created&page=1</a><br>\n</div><div><br></div>3. forgot password id working in indigogo.<br><br><div><br></div><div>4. admin - email template - correct.</div><div>5. term and condition in sign up page - correct.</div>\n<div>6. in email template - help center is link in help link by admin (paragraph can be change from editor.)</div><div><br></div>    ', 'k9ydy!r%v49wu4146qc%', 'FUND001', '2014-02-26 00:00:00'),
(11, '5.1.1', '<div>ticket no.</div><div><a rel="nofollow noreferrer" href="https://www.google.com/url?q=https%3A%2F%2Ffundraising.zendesk.com%2Ftickets%2F890%3Fcol%3D25895847%26desc%3D1%26order%3Dcreated%26page%3D1&sa=D&sntz=1&usg=AFQjCNGFx3TzST_u-gIP73XLGpGTDuZGEg" dir="ltr" target="_blank">https://fundraising.zendesk.<wbr>com/tickets/890?col=25895847&<wbr>desc=1&order=created&page=1</a><br>\n</div><div><br></div><div>1.other feature tab is working i. <br>\n</div><div><strong><br></strong></div><div><a rel="nofollow noreferrer" href="https://www.google.com/url?q=https%3A%2F%2Ffundraising.zendesk.com%2Ftickets%2F889%3Fcol%3D25895847%26desc%3D1%26order%3Dcreated%26page%3D1&sa=D&sntz=1&usg=AFQjCNE2ik5cqH8NSOwrKiTu4545qaNVGQ" dir="ltr" target="_blank">https://fundraising.zendesk.<wbr>com/tickets/889?col=25895847&<wbr>desc=1&order=created&page=1</a><br>\n</div><div><br></div><div>2. In admin , user list should be allow to search by  username , email address , ip address - <strong></strong></div>\n<div><br></div><div><a rel="nofollow noreferrer" href="https://www.google.com/url?q=https%3A%2F%2Ffundraising.zendesk.com%2Ftickets%2F888%3Fcol%3D25895847%26desc%3D1%26order%3Dcreated%26page%3D1&sa=D&sntz=1&usg=AFQjCNEnoigg0BPiUbxaiYc4G6etqgx-IQ" dir="ltr" target="_blank">https://fundraising.zendesk.<wbr>com/tickets/888?col=25895847&<wbr>desc=1&order=created&page=1</a><br>\n</div><div><br></div>3. forgot password id working in indigogo.<br><br><div><br></div><div>4. admin - email template - correct.</div><div>5. term and condition in sign up page - correct.</div>\n<div>6. in email template - help center is link in help link by admin (paragraph can be change from editor.)</div><div><br></div>  ', '7mltg5idngk9yff7tse1', 'FUND001', '2014-02-27 00:00:00'),
(12, '5.1.2', '<div>ticket no.</div><div><a rel="nofollow noreferrer" href="https://www.google.com/url?q=https%3A%2F%2Ffundraising.zendesk.com%2Ftickets%2F890%3Fcol%3D25895847%26desc%3D1%26order%3Dcreated%26page%3D1&sa=D&sntz=1&usg=AFQjCNGFx3TzST_u-gIP73XLGpGTDuZGEg" dir="ltr" target="_blank">https://fundraising.zendesk.<wbr>com/tickets/890?col=25895847&<wbr>desc=1&order=created&page=1</a><br></div><div><br></div><div>1.other feature tab is working i. <br></div><div><strong><br></strong></div><div><a rel="nofollow noreferrer" href="https://www.google.com/url?q=https%3A%2F%2Ffundraising.zendesk.com%2Ftickets%2F889%3Fcol%3D25895847%26desc%3D1%26order%3Dcreated%26page%3D1&sa=D&sntz=1&usg=AFQjCNE2ik5cqH8NSOwrKiTu4545qaNVGQ" dir="ltr" target="_blank">https://fundraising.zendesk.<wbr>com/tickets/889?col=25895847&<wbr>desc=1&order=created&page=1</a><br></div><div><br></div><div>2. In admin , user list should be allow to search by  username , email address , ip address - <strong></strong></div><div><br></div><div><a rel="nofollow noreferrer" href="https://www.google.com/url?q=https%3A%2F%2Ffundraising.zendesk.com%2Ftickets%2F888%3Fcol%3D25895847%26desc%3D1%26order%3Dcreated%26page%3D1&sa=D&sntz=1&usg=AFQjCNEnoigg0BPiUbxaiYc4G6etqgx-IQ" dir="ltr" target="_blank">https://fundraising.zendesk.<wbr>com/tickets/888?col=25895847&<wbr>desc=1&order=created&page=1</a><br></div><div><br></div>3. forgot password id working in indigogo.<br><br><div><br></div><div>4. admin - email template - correct.</div><div>5. term and condition in sign up page - correct.</div><div>6. in email template - help center is link in help link by admin (paragraph can be change from editor.)</div>  <div><br></div>  ', 'pjkxza7ghibb5ycr1zj2', 'FUND001', '2014-03-02 00:00:00'),
(13, '5.1.3', '<div>ticket no.</div><div><a rel="nofollow noreferrer" href="https://www.google.com/url?q=https%3A%2F%2Ffundraising.zendesk.com%2Ftickets%2F890%3Fcol%3D25895847%26desc%3D1%26order%3Dcreated%26page%3D1&sa=D&sntz=1&usg=AFQjCNGFx3TzST_u-gIP73XLGpGTDuZGEg" dir="ltr" target="_blank">https://fundraising.zendesk.<wbr>com/tickets/890?col=25895847&<wbr>desc=1&order=created&page=1</a><br></div><div><br></div><div>1.other feature tab is working i. <br></div><div><strong><br></strong></div><div><a rel="nofollow noreferrer" href="https://www.google.com/url?q=https%3A%2F%2Ffundraising.zendesk.com%2Ftickets%2F889%3Fcol%3D25895847%26desc%3D1%26order%3Dcreated%26page%3D1&sa=D&sntz=1&usg=AFQjCNE2ik5cqH8NSOwrKiTu4545qaNVGQ" dir="ltr" target="_blank">https://fundraising.zendesk.<wbr>com/tickets/889?col=25895847&<wbr>desc=1&order=created&page=1</a><br></div><div><br></div><div>2. In admin , user list should be allow to search by  username , email address , ip address - <strong></strong></div><div><br></div><div><a rel="nofollow noreferrer" href="https://www.google.com/url?q=https%3A%2F%2Ffundraising.zendesk.com%2Ftickets%2F888%3Fcol%3D25895847%26desc%3D1%26order%3Dcreated%26page%3D1&sa=D&sntz=1&usg=AFQjCNEnoigg0BPiUbxaiYc4G6etqgx-IQ" dir="ltr" target="_blank">https://fundraising.zendesk.<wbr>com/tickets/888?col=25895847&<wbr>desc=1&order=created&page=1</a><br></div><div><br></div>3. forgot password id working in indigogo.<br><br><div><br></div><div>4. admin - email template - correct.</div><div>5. term and condition in sign up page - correct.</div><div>6. in email template - help center is link in help link by admin (paragraph can be change from editor.)</div>  <div><br></div>  ', 'nqocox82p6elx6@2b83t', 'FUND001', '2014-03-03 00:00:00'),
(14, '5.1.4', '<div><div><strong>-bug ticket is attached.</strong></div><div><strong>-in version update (admin) - search is working and pagination also.</strong></div><div><strong>--Done – cronjob delete and listing</strong></div><p><strong> – done active/inactive category is working in frontside<br></strong></p><p><strong>-Done  grid have shorting functionality  - arrow is display in column</strong></p>  <br></div>  ', '2y4dmjm1!pgct06xu2p@', 'FUND001', '2014-03-03 00:00:00'),
(18, '5.1.7', 'Occasionally, we may release small updates in the form of a patch to one or several source file. These source files are regular zip files that contain updated files.<br><br>There are two methods for updating - the easiest is the one-click update. If it doesn''t work, or you just prefer to be more hands-on, you can follow the manual update process. <br><br>If you are working with a source code ,Make a backup of everything in your site, including the database. This is extremely important so that you can roll back to a running site no matter what happens during migration.<br><br>Although we usually produce upgrades in order to improve a functionality, there are risks involved—including the possibility that the upgrade will worsen the website.  ', 'vct75tuk08xx2g835b3e', 'FUND001', '2014-03-14 00:00:00'),
(17, '5.1.5', '<div><strong>-bug ticket is attached.</strong></div><div><strong>-in version update (admin) - search is working and pagination also.</strong></div><div><strong>--Done – cronjob delete and listing</strong></div><p><strong> – done active/inactive category is working in frontside<br></strong></p><p><strong>-Done  grid have shorting functionality  - arrow is display in column</strong></p>  <div><strong><br></strong></div>  ', 'os23yj444smx!h2iniaa', 'FUND001', '2014-03-03 00:00:00'),
(19, '5.1.8', 'Occasionally, we may release small updates in the form of a patch to one or several source file. These source files are regular zip files that contain updated files.<br><br>There are two methods for updating - the easiest is the one-click update. If it doesn''t work, or you just prefer to be more hands-on, you can follow the manual update process. <br><br>If you are working with a source code ,Make a backup of everything in your site, including the database. This is extremely important so that you can roll back to a running site no matter what happens during migration.<br><br>Although we usually produce upgrades in order to improve a functionality, there are risks involved—including the possibility that the upgrade will worsen the website.  ', 'i%6d17j68o0k@%djbe@%', 'FUND001', '2014-03-14 00:00:00'),
(20, '5.1.9', 'Occasionally, we may release small updates in the form of a patch to one or several source file. These source files are regular zip files that contain updated files.<br><br>There are two methods for updating - the easiest is the one-click update. If it doesn''t work, or you just prefer to be more hands-on, you can follow the manual update process. <br><br>If you are working with a source code ,Make a backup of everything in your site, including the database. This is extremely important so that you can roll back to a running site no matter what happens during migration.<br><br>Although we usually produce upgrades in order to improve a functionality, there are risks involved—including the possibility that the upgrade will worsen the website.  ', 't740e9xwsnw5@9gqv5vs', 'FUND001', '2014-03-15 00:00:00'),
(21, '5.1.10', 'Occasionally, we may release small updates in the form of a patch to one or several source file. These source files are regular zip files that contain updated files.<br><br>There are two methods for updating - the easiest is the one-click update. If it doesn''t work, or you just prefer to be more hands-on, you can follow the manual update process. <br><br>If you are working with a source code ,Make a backup of everything in your site, including the database. This is extremely important so that you can roll back to a running site no matter what happens during migration.<br><br>Although we usually produce upgrades in order to improve a functionality, there are risks involved—including the possibility that the upgrade will worsen the website.  ', 'znf3o3g5hvq0nq@a%3!j', 'FUND001', '2014-03-21 00:00:00'),
(22, '5.2.2', 'Occasionally, we may release small updates in the form of a patch to one or several source file. These source files are regular zip files that contain updated files.<br><br>There are two methods for updating - the easiest is the one-click update. If it doesn''t work, or you just prefer to be more hands-on, you can follow the manual update process. <br><br>If you are working with a source code ,Make a backup of everything in your site, including the database. This is extremely important so that you can roll back to a running site no matter what happens during migration.<br><br>Although we usually produce upgrades in order to improve a functionality, there are risks involved—including the possibility that the upgrade will worsen the website.  ', 'uzqiahqda@fx1gzxlsiz', 'FUND001', '2014-04-01 00:00:00'),
(23, '5.2.1', 'Occasionally, we may release small updates in the form of a patch to one or several source file. These source files are regular zip files that contain updated files.<br><br>There are two methods for updating - the easiest is the one-click update. If it doesn''t work, or you just prefer to be more hands-on, you can follow the manual update process. <br><br>If you are working with a source code ,Make a backup of everything in your site, including the database. This is extremely important so that you can roll back to a running site no matter what happens during migration.<br><br>Although we usually produce upgrades in order to improve a functionality, there are risks involved—including the possibility that the upgrade will worsen the website.  ', 'fels380zo445e366a1b7', 'FUND001', '2014-04-01 00:00:00'),
(24, '5.2.3', 'Occasionally, we may release small updates in the form of a patch to one or several source file. These source files are regular zip files that contain updated files.<br><br>There are two methods for updating - the easiest is the one-click update. If it doesn''t work, or you just prefer to be more hands-on, you can follow the manual update process. <br><br>If you are working with a source code ,Make a backup of everything in your site, including the database. This is extremely important so that you can roll back to a running site no matter what happens during migration.<br><br>Although we usually produce upgrades in order to improve a functionality, there are risks involved—including the possibility that the upgrade will worsen the website.  ', 'e6nxx8ddnvrma85pp%5s', 'FUND001', '2014-04-05 00:00:00'),
(25, '1.0.0.2', 'Admin Logo change on mouse hover effect.<br> Dashboard URL  ', 'qk%09r3y%e0ionhzn6lk', 'FUND001', '2014-08-27 00:00:00'),
(26, '1.0.0.3', 'Admin Logo change on mouse hover effect.<br> Dashboard URL  ', 'nf61yf5bnesujo2lte@h', 'FUND001', '2014-08-27 00:00:00'),
(27, '1.0.0.6', '/home/rocku3/Desktop/Fundrasing 29/admin_css<br>/home/rocku3/Desktop/Fundrasing 29/application<br>/home/rocku3/Desktop/Fundrasing 29/css<br>/home/rocku3/Desktop/Fundrasing 29/images  ', 'c@@58yb11vsd1%!aktku', 'FUND001', '2014-08-27 00:00:00'),
(28, '1.0.0.7', 'Dashboard Listing in admin side<br>Admin side logo hover effect as like on front side  ', '8onzhjttjz3htr0a6ajk', 'FUND001', '2014-08-27 00:00:00'),
(29, '1.0.0.8', 'Dashboard Listing on admin side<br>Logo Hover effect in admin side as front side  ', '345wx@03vznidws1z7ox', 'FUND001', '2014-08-27 00:00:00'),
(30, '1.0.0.10', 'Fundraising contact us page  ', 'd0l0wi8@oytlekgecx28', 'FUND001', '2014-08-27 00:00:00'),
(31, '2.0.0.11', 'From this update you can find home controller update   ', '9xew!7xhqrfzg48sabgg', 'FUND001', '2014-09-12 00:00:00'),
(32, '2.0.0.12', 'From this version you can find following update :<br><br>1) Help document for each of the social site settings and payment gatway from their setting page in admin panel by click on help icon on left side corner after page title<br>2) When admin update information from the site setting in admin panel then default no image will be reproduce based on the updated site logo.  ', '4kx17cyr09z!xk1u@04!', 'FUND001', '2014-09-12 00:00:00'),
(33, '2.0.0.13', 'From this version you can find following updates :<br><br>1) Link In texts are change according to its functional operation. (#1016)<br>2) If current logged in user do not have it profile picture then, it should show default image in place of broken image shown. (#1015)<br>3) In admin user listing password column has been removed now. (#1012)<br>4) On sign up step2 label of textarea and buttons are changed now. (#1009)<br>5) Contact us page now show in one page, means its scroll has been removed now. (#994)  ', 'smwyqfnu3vo986iui07w', 'FUND001', '2014-09-13 00:00:00'),
(34, '2.0.0.14', 'You can find following updates from this version :<br><br>1) Change site name in place of indiegogo at create step 1<br>2) Taggin tool (1010)<br>3) Admin should able to see pending project (1023)<br>4) Admin auto logout when front side logout<br>5) Stripe fixed failure     ', '!q6tws52mu3g%zfdul2q', 'FUND001', '2014-09-15 00:00:00'),
(35, '2.0.0.15', 'Help document link for all social sign up page has been changed to global link for all the fundraising script   ', 'cfhr3mas!ve4p3j8ka9g', 'FUND001', '2014-09-18 00:00:00'),
(36, '2.0.0.16', 'From this update user can not get error for site setting page update. User will get new default image with uploaded logo   ', 'd%a9e6tp%38hlbfru6eh', 'FUND001', '2014-09-18 00:00:00'),
(37, '2.0.0.17', 'New Updates on Help Doc icons   ', '3xzcbmztgrwruhqxrgan', 'FUND001', '2014-09-23 00:00:00'),
(38, '2.0.0.18', 'New Updates with Help Doc icon   ', 'uy3bxggzyp1v25ulhm9j', 'FUND001', '2014-09-23 00:00:00'),
(39, '2.0.0.19', 'New Updates with Help Doc icon    ', '23qqfehuwxty9x5kmxym', 'FUND001', '2014-09-23 00:00:00'),
(40, '2.0.0.20', 'Help icons update with tooltips   ', 'amhio%9ynr8glhv58!rb', 'FUND001', '2014-09-23 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `video_gallery`
--

CREATE TABLE IF NOT EXISTS `video_gallery` (
  `id` int(10) NOT NULL,
  `equity_id` int(11) NOT NULL,
  `image` varchar(200) DEFAULT NULL,
  `media_video_desc` text,
  `media_video_name` varchar(200) DEFAULT NULL,
  `media_video_title` varchar(200) DEFAULT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `video_gallery`
--

INSERT INTO `video_gallery` (`id`, `equity_id`, `image`, `media_video_desc`, `media_video_name`, `media_video_title`, `status`) VALUES
(3, 14, 'http://i1.ytimg.com/vi/bwe1Wm_QtyM/hqdefault.jpg', 'test', 'https://www.youtube.com/watch?v=bwe1Wm_QtyM', 'test', 0),
(5, 25, 'http://i1.ytimg.com/vi/bwe1Wm_QtyM/hqdefault.jpg', 'tsst', 'https://www.youtube.com/watch?v=bwe1Wm_QtyM', 'test', 1),
(2, 24, 'http://i1.ytimg.com/vi/8C0YAwg2iqI/hqdefault.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam dapibus tincidunt orci, quis convallis sem hendrerit dapibus. Donec mattis efficitur dui vel', 'https://www.youtube.com/watch?v=8C0YAwg2iqI', 'Tech', 0),
(6, 24, 'http://i1.ytimg.com/vi/qYZF6oIZtfc/hqdefault.jpg', 'Tech', 'https://www.youtube.com/watch?v=qYZF6oIZtfc', 'Tech', 0),
(7, 26, 'http://i1.ytimg.com/vi/UlHH9p1Mly8/hqdefault.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an', 'https://www.youtube.com/watch?v=UlHH9p1Mly8', '! minutes', 0);

-- --------------------------------------------------------

--
-- Table structure for table `word_detecter_setting`
--

CREATE TABLE IF NOT EXISTS `word_detecter_setting` (
  `word_detecter_setting_id` int(11) NOT NULL,
  `enable_word_detecter` varchar(255) DEFAULT NULL,
  `words` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `word_detecter_setting`
--

INSERT INTO `word_detecter_setting` (`word_detecter_setting_id`, `enable_word_detecter`, `words`) VALUES
(1, 'yes', 'bomb');

-- --------------------------------------------------------

--
-- Table structure for table `youtube_setting`
--

CREATE TABLE IF NOT EXISTS `youtube_setting` (
  `youtube_setting_id` int(11) NOT NULL,
  `youtube_link` varchar(255) DEFAULT NULL,
  `youtube_enable` varchar(25) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `youtube_setting`
--

INSERT INTO `youtube_setting` (`youtube_setting_id`, `youtube_link`, `youtube_enable`) VALUES
(1, 'https://www.youtube.com/channel/UCS0OaEsFEGHSarj9-jIDdUg', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access_request`
--
ALTER TABLE `access_request`
  ADD PRIMARY KEY (`access_request_id`);

--
-- Indexes for table `account_type`
--
ALTER TABLE `account_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `accreditation`
--
ALTER TABLE `accreditation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `admin_activity`
--
ALTER TABLE `admin_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_login`
--
ALTER TABLE `admin_login`
  ADD PRIMARY KEY (`login_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`city_id`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`comment_id`);

--
-- Indexes for table `company_category`
--
ALTER TABLE `company_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_featured_projects`
--
ALTER TABLE `company_featured_projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_industry`
--
ALTER TABLE `company_industry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_profile`
--
ALTER TABLE `company_profile`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `cronjob`
--
ALTER TABLE `cronjob`
  ADD PRIMARY KEY (`cronjob_id`);

--
-- Indexes for table `crons`
--
ALTER TABLE `crons`
  ADD PRIMARY KEY (`crons_id`);

--
-- Indexes for table `currency_code`
--
ALTER TABLE `currency_code`
  ADD PRIMARY KEY (`currency_code_id`);

--
-- Indexes for table `deal_type_setting`
--
ALTER TABLE `deal_type_setting`
  ADD PRIMARY KEY (`deal_type_id`);

--
-- Indexes for table `document_type`
--
ALTER TABLE `document_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dynamic_slider`
--
ALTER TABLE `dynamic_slider`
  ADD PRIMARY KEY (`dynamic_slider_id`);

--
-- Indexes for table `email_setting`
--
ALTER TABLE `email_setting`
  ADD PRIMARY KEY (`email_setting_id`);

--
-- Indexes for table `email_template`
--
ALTER TABLE `email_template`
  ADD PRIMARY KEY (`email_template_id`);

--
-- Indexes for table `equity`
--
ALTER TABLE `equity`
  ADD PRIMARY KEY (`equity_id`);

--
-- Indexes for table `equity_categories`
--
ALTER TABLE `equity_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `equity_company_industry`
--
ALTER TABLE `equity_company_industry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `equity_contract_document`
--
ALTER TABLE `equity_contract_document`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `equity_follower`
--
ALTER TABLE `equity_follower`
  ADD PRIMARY KEY (`equity_follow_id`);

--
-- Indexes for table `equity_gallery`
--
ALTER TABLE `equity_gallery`
  ADD PRIMARY KEY (`equity_gallery_id`);

--
-- Indexes for table `equity_highlights`
--
ALTER TABLE `equity_highlights`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `equity_investment_process`
--
ALTER TABLE `equity_investment_process`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `equity_investment_process_history`
--
ALTER TABLE `equity_investment_process_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `equity_invest_status`
--
ALTER TABLE `equity_invest_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `facebook_setting`
--
ALTER TABLE `facebook_setting`
  ADD PRIMARY KEY (`facebook_setting_id`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`faq_id`);

--
-- Indexes for table `faq_category`
--
ALTER TABLE `faq_category`
  ADD PRIMARY KEY (`faq_category_id`);

--
-- Indexes for table `featured_sponsors`
--
ALTER TABLE `featured_sponsors`
  ADD PRIMARY KEY (`featured_sponsors_id`);

--
-- Indexes for table `file_gallery`
--
ALTER TABLE `file_gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `font_type`
--
ALTER TABLE `font_type`
  ADD PRIMARY KEY (`font_type_id`);

--
-- Indexes for table `funding_source`
--
ALTER TABLE `funding_source`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `funding_type`
--
ALTER TABLE `funding_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`gallery_id`);

--
-- Indexes for table `google_plus_setting`
--
ALTER TABLE `google_plus_setting`
  ADD PRIMARY KEY (`google_plus_setting_id`);

--
-- Indexes for table `google_setting`
--
ALTER TABLE `google_setting`
  ADD PRIMARY KEY (`google_setting_id`);

--
-- Indexes for table `guidelines`
--
ALTER TABLE `guidelines`
  ADD PRIMARY KEY (`guidelines_id`);

--
-- Indexes for table `home_page`
--
ALTER TABLE `home_page`
  ADD PRIMARY KEY (`home_id`);

--
-- Indexes for table `image_setting`
--
ALTER TABLE `image_setting`
  ADD PRIMARY KEY (`image_setting_id`);

--
-- Indexes for table `interest_request`
--
ALTER TABLE `interest_request`
  ADD PRIMARY KEY (`interest_request_id`);

--
-- Indexes for table `investment_tax_relief`
--
ALTER TABLE `investment_tax_relief`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `investors`
--
ALTER TABLE `investors`
  ADD PRIMARY KEY (`investor_id`);

--
-- Indexes for table `investor_type`
--
ALTER TABLE `investor_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invite_members`
--
ALTER TABLE `invite_members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invite_request`
--
ALTER TABLE `invite_request`
  ADD PRIMARY KEY (`request_id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`language_id`);

--
-- Indexes for table `learn_more`
--
ALTER TABLE `learn_more`
  ADD PRIMARY KEY (`pages_id`);

--
-- Indexes for table `learn_more_category`
--
ALTER TABLE `learn_more_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `linkdin_setting`
--
ALTER TABLE `linkdin_setting`
  ADD PRIMARY KEY (`linkdin_setting_id`);

--
-- Indexes for table `message_conversation`
--
ALTER TABLE `message_conversation`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `message_setting`
--
ALTER TABLE `message_setting`
  ADD PRIMARY KEY (`message_setting_id`);

--
-- Indexes for table `meta_setting`
--
ALTER TABLE `meta_setting`
  ADD PRIMARY KEY (`meta_setting_id`);

--
-- Indexes for table `newsletter_job`
--
ALTER TABLE `newsletter_job`
  ADD PRIMARY KEY (`job_id`);

--
-- Indexes for table `newsletter_jobs`
--
ALTER TABLE `newsletter_jobs`
  ADD PRIMARY KEY (`job_id`);

--
-- Indexes for table `newsletter_report`
--
ALTER TABLE `newsletter_report`
  ADD PRIMARY KEY (`report_id`);

--
-- Indexes for table `newsletter_setting`
--
ALTER TABLE `newsletter_setting`
  ADD PRIMARY KEY (`newsletter_setting_id`);

--
-- Indexes for table `newsletter_subscribe`
--
ALTER TABLE `newsletter_subscribe`
  ADD PRIMARY KEY (`subscribe_id`);

--
-- Indexes for table `newsletter_template`
--
ALTER TABLE `newsletter_template`
  ADD PRIMARY KEY (`newsletter_id`);

--
-- Indexes for table `newsletter_user`
--
ALTER TABLE `newsletter_user`
  ADD PRIMARY KEY (`newsletter_user_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`pages_id`);

--
-- Indexes for table `perk`
--
ALTER TABLE `perk`
  ADD PRIMARY KEY (`perk_id`);

--
-- Indexes for table `phases`
--
ALTER TABLE `phases`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `previous_funding`
--
ALTER TABLE `previous_funding`
  ADD PRIMARY KEY (`previous_funding_id`);

--
-- Indexes for table `rights`
--
ALTER TABLE `rights`
  ADD PRIMARY KEY (`rights_id`);

--
-- Indexes for table `rights_assign`
--
ALTER TABLE `rights_assign`
  ADD PRIMARY KEY (`assign_id`);

--
-- Indexes for table `school`
--
ALTER TABLE `school`
  ADD PRIMARY KEY (`school_id`);

--
-- Indexes for table `shipping_detail`
--
ALTER TABLE `shipping_detail`
  ADD PRIMARY KEY (`shipping_detail_id`);

--
-- Indexes for table `site_setting`
--
ALTER TABLE `site_setting`
  ADD PRIMARY KEY (`site_setting_id`);

--
-- Indexes for table `slider_setting`
--
ALTER TABLE `slider_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `spam_control`
--
ALTER TABLE `spam_control`
  ADD PRIMARY KEY (`spam_control_id`);

--
-- Indexes for table `spam_inquiry`
--
ALTER TABLE `spam_inquiry`
  ADD PRIMARY KEY (`inquire_id`);

--
-- Indexes for table `spam_ip`
--
ALTER TABLE `spam_ip`
  ADD PRIMARY KEY (`spam_id`);

--
-- Indexes for table `spam_report_ip`
--
ALTER TABLE `spam_report_ip`
  ADD PRIMARY KEY (`spam_report_id`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`state_id`);

--
-- Indexes for table `taxonomy_setting`
--
ALTER TABLE `taxonomy_setting`
  ADD PRIMARY KEY (`taxonomy_setting_id`);

--
-- Indexes for table `team_member_type`
--
ALTER TABLE `team_member_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_preapprove`
--
ALTER TABLE `temp_preapprove`
  ADD PRIMARY KEY (`temp_pre_id`);

--
-- Indexes for table `themes`
--
ALTER TABLE `themes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `timezone`
--
ALTER TABLE `timezone`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`transaction_id`);

--
-- Indexes for table `transaction_type`
--
ALTER TABLE `transaction_type`
  ADD PRIMARY KEY (`transaction_type_id`);

--
-- Indexes for table `translation`
--
ALTER TABLE `translation`
  ADD PRIMARY KEY (`translation_id`);

--
-- Indexes for table `twitter_setting`
--
ALTER TABLE `twitter_setting`
  ADD PRIMARY KEY (`twitter_setting_id`);

--
-- Indexes for table `updates`
--
ALTER TABLE `updates`
  ADD PRIMARY KEY (`update_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_activity`
--
ALTER TABLE `user_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_follow`
--
ALTER TABLE `user_follow`
  ADD PRIMARY KEY (`follower_id`);

--
-- Indexes for table `user_login`
--
ALTER TABLE `user_login`
  ADD PRIMARY KEY (`login_id`);

--
-- Indexes for table `user_notification`
--
ALTER TABLE `user_notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_setting`
--
ALTER TABLE `user_setting`
  ADD PRIMARY KEY (`user_setting_id`);

--
-- Indexes for table `version_update`
--
ALTER TABLE `version_update`
  ADD PRIMARY KEY (`ver_id`);

--
-- Indexes for table `video_gallery`
--
ALTER TABLE `video_gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `word_detecter_setting`
--
ALTER TABLE `word_detecter_setting`
  ADD PRIMARY KEY (`word_detecter_setting_id`);

--
-- Indexes for table `youtube_setting`
--
ALTER TABLE `youtube_setting`
  ADD PRIMARY KEY (`youtube_setting_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access_request`
--
ALTER TABLE `access_request`
  MODIFY `access_request_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `account_type`
--
ALTER TABLE `account_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `accreditation`
--
ALTER TABLE `accreditation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT for table `admin_activity`
--
ALTER TABLE `admin_activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=160;
--
-- AUTO_INCREMENT for table `admin_login`
--
ALTER TABLE `admin_login`
  MODIFY `login_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=128;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `city_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `comment_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `company_category`
--
ALTER TABLE `company_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `company_featured_projects`
--
ALTER TABLE `company_featured_projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `company_industry`
--
ALTER TABLE `company_industry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `company_profile`
--
ALTER TABLE `company_profile`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=219;
--
-- AUTO_INCREMENT for table `cronjob`
--
ALTER TABLE `cronjob`
  MODIFY `cronjob_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `crons`
--
ALTER TABLE `crons`
  MODIFY `crons_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `currency_code`
--
ALTER TABLE `currency_code`
  MODIFY `currency_code_id` int(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `deal_type_setting`
--
ALTER TABLE `deal_type_setting`
  MODIFY `deal_type_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `document_type`
--
ALTER TABLE `document_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `dynamic_slider`
--
ALTER TABLE `dynamic_slider`
  MODIFY `dynamic_slider_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `email_setting`
--
ALTER TABLE `email_setting`
  MODIFY `email_setting_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `email_template`
--
ALTER TABLE `email_template`
  MODIFY `email_template_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=91;
--
-- AUTO_INCREMENT for table `equity`
--
ALTER TABLE `equity`
  MODIFY `equity_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `equity_categories`
--
ALTER TABLE `equity_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `equity_company_industry`
--
ALTER TABLE `equity_company_industry`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `equity_contract_document`
--
ALTER TABLE `equity_contract_document`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `equity_follower`
--
ALTER TABLE `equity_follower`
  MODIFY `equity_follow_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT for table `equity_gallery`
--
ALTER TABLE `equity_gallery`
  MODIFY `equity_gallery_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `equity_highlights`
--
ALTER TABLE `equity_highlights`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `equity_investment_process`
--
ALTER TABLE `equity_investment_process`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `equity_investment_process_history`
--
ALTER TABLE `equity_investment_process_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `equity_invest_status`
--
ALTER TABLE `equity_invest_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `facebook_setting`
--
ALTER TABLE `facebook_setting`
  MODIFY `facebook_setting_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `faq_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `faq_category`
--
ALTER TABLE `faq_category`
  MODIFY `faq_category_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `featured_sponsors`
--
ALTER TABLE `featured_sponsors`
  MODIFY `featured_sponsors_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `file_gallery`
--
ALTER TABLE `file_gallery`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `font_type`
--
ALTER TABLE `font_type`
  MODIFY `font_type_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `funding_source`
--
ALTER TABLE `funding_source`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `funding_type`
--
ALTER TABLE `funding_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `gallery_id` int(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `google_plus_setting`
--
ALTER TABLE `google_plus_setting`
  MODIFY `google_plus_setting_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `google_setting`
--
ALTER TABLE `google_setting`
  MODIFY `google_setting_id` int(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `guidelines`
--
ALTER TABLE `guidelines`
  MODIFY `guidelines_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `home_page`
--
ALTER TABLE `home_page`
  MODIFY `home_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `image_setting`
--
ALTER TABLE `image_setting`
  MODIFY `image_setting_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `interest_request`
--
ALTER TABLE `interest_request`
  MODIFY `interest_request_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `investment_tax_relief`
--
ALTER TABLE `investment_tax_relief`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `investors`
--
ALTER TABLE `investors`
  MODIFY `investor_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `investor_type`
--
ALTER TABLE `investor_type`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `invite_members`
--
ALTER TABLE `invite_members`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT for table `invite_request`
--
ALTER TABLE `invite_request`
  MODIFY `request_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `learn_more`
--
ALTER TABLE `learn_more`
  MODIFY `pages_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=88;
--
-- AUTO_INCREMENT for table `learn_more_category`
--
ALTER TABLE `learn_more_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `linkdin_setting`
--
ALTER TABLE `linkdin_setting`
  MODIFY `linkdin_setting_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `message_conversation`
--
ALTER TABLE `message_conversation`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `message_setting`
--
ALTER TABLE `message_setting`
  MODIFY `message_setting_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `meta_setting`
--
ALTER TABLE `meta_setting`
  MODIFY `meta_setting_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `newsletter_job`
--
ALTER TABLE `newsletter_job`
  MODIFY `job_id` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `newsletter_jobs`
--
ALTER TABLE `newsletter_jobs`
  MODIFY `job_id` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `newsletter_report`
--
ALTER TABLE `newsletter_report`
  MODIFY `report_id` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `newsletter_setting`
--
ALTER TABLE `newsletter_setting`
  MODIFY `newsletter_setting_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `newsletter_subscribe`
--
ALTER TABLE `newsletter_subscribe`
  MODIFY `subscribe_id` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `newsletter_template`
--
ALTER TABLE `newsletter_template`
  MODIFY `newsletter_id` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `newsletter_user`
--
ALTER TABLE `newsletter_user`
  MODIFY `newsletter_user_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `pages_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `perk`
--
ALTER TABLE `perk`
  MODIFY `perk_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `phases`
--
ALTER TABLE `phases`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `previous_funding`
--
ALTER TABLE `previous_funding`
  MODIFY `previous_funding_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `rights`
--
ALTER TABLE `rights`
  MODIFY `rights_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=73;
--
-- AUTO_INCREMENT for table `rights_assign`
--
ALTER TABLE `rights_assign`
  MODIFY `assign_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1047;
--
-- AUTO_INCREMENT for table `school`
--
ALTER TABLE `school`
  MODIFY `school_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `shipping_detail`
--
ALTER TABLE `shipping_detail`
  MODIFY `shipping_detail_id` int(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `site_setting`
--
ALTER TABLE `site_setting`
  MODIFY `site_setting_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `slider_setting`
--
ALTER TABLE `slider_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `spam_control`
--
ALTER TABLE `spam_control`
  MODIFY `spam_control_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `spam_inquiry`
--
ALTER TABLE `spam_inquiry`
  MODIFY `inquire_id` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `spam_ip`
--
ALTER TABLE `spam_ip`
  MODIFY `spam_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3746;
--
-- AUTO_INCREMENT for table `spam_report_ip`
--
ALTER TABLE `spam_report_ip`
  MODIFY `spam_report_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `state_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=156;
--
-- AUTO_INCREMENT for table `taxonomy_setting`
--
ALTER TABLE `taxonomy_setting`
  MODIFY `taxonomy_setting_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `team_member_type`
--
ALTER TABLE `team_member_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `temp_preapprove`
--
ALTER TABLE `temp_preapprove`
  MODIFY `temp_pre_id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `themes`
--
ALTER TABLE `themes`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `timezone`
--
ALTER TABLE `timezone`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=579;
--
-- AUTO_INCREMENT for table `transaction`
--
ALTER TABLE `transaction`
  MODIFY `transaction_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `transaction_type`
--
ALTER TABLE `transaction_type`
  MODIFY `transaction_type_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `translation`
--
ALTER TABLE `translation`
  MODIFY `translation_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `twitter_setting`
--
ALTER TABLE `twitter_setting`
  MODIFY `twitter_setting_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `updates`
--
ALTER TABLE `updates`
  MODIFY `update_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `user_activity`
--
ALTER TABLE `user_activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2845;
--
-- AUTO_INCREMENT for table `user_follow`
--
ALTER TABLE `user_follow`
  MODIFY `follower_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=138;
--
-- AUTO_INCREMENT for table `user_login`
--
ALTER TABLE `user_login`
  MODIFY `login_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=303;
--
-- AUTO_INCREMENT for table `user_notification`
--
ALTER TABLE `user_notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `user_setting`
--
ALTER TABLE `user_setting`
  MODIFY `user_setting_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `version_update`
--
ALTER TABLE `version_update`
  MODIFY `ver_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `video_gallery`
--
ALTER TABLE `video_gallery`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `word_detecter_setting`
--
ALTER TABLE `word_detecter_setting`
  MODIFY `word_detecter_setting_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `youtube_setting`
--
ALTER TABLE `youtube_setting`
  MODIFY `youtube_setting_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
